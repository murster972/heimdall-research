package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.SensorManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.LocaleList;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.d.h;
import com.applovin.impl.sdk.e.f;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinEventTypes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class m {
    private static String j;
    private static int k;

    /* renamed from: a  reason: collision with root package name */
    private final l f1855a;
    private final r b;
    private final Context c;
    private final Map<String, Object> d;
    private final Object e = new Object();
    private final Map<String, Object> f;
    private boolean g;
    /* access modifiers changed from: private */
    public final AtomicReference<a> h = new AtomicReference<>();
    /* access modifiers changed from: private */
    public final AtomicReference<Integer> i = new AtomicReference<>();

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public boolean f1858a;
        public String b = "";
    }

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public int f1859a = -1;
        public int b = -1;
    }

    protected m(l lVar) {
        if (lVar != null) {
            this.f1855a = lVar;
            this.b = lVar.j0();
            this.c = lVar.i();
            this.d = u();
            this.f = w();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private b A() {
        b bVar = new b();
        Intent registerReceiver = this.c.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        int i2 = -1;
        int intExtra = registerReceiver != null ? registerReceiver.getIntExtra(AppLovinEventTypes.USER_COMPLETED_LEVEL, -1) : -1;
        int intExtra2 = registerReceiver != null ? registerReceiver.getIntExtra("scale", -1) : -1;
        if (intExtra <= 0 || intExtra2 <= 0) {
            bVar.b = -1;
        } else {
            bVar.b = (int) ((((float) intExtra) / ((float) intExtra2)) * 100.0f);
        }
        if (registerReceiver != null) {
            i2 = registerReceiver.getIntExtra(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, -1);
        }
        bVar.f1859a = i2;
        return bVar;
    }

    private long B() {
        List asList = Arrays.asList(o.c(Settings.Secure.getString(this.c.getContentResolver(), "enabled_accessibility_services")).split(":"));
        long j2 = asList.contains("AccessibilityMenuService") ? 256 : 0;
        if (asList.contains("SelectToSpeakService")) {
            j2 |= 512;
        }
        if (asList.contains("SoundAmplifierService")) {
            j2 |= 2;
        }
        if (asList.contains("SpeechToTextAccessibilityService")) {
            j2 |= 128;
        }
        if (asList.contains("SwitchAccessService")) {
            j2 |= 4;
        }
        if ((this.c.getResources().getConfiguration().uiMode & 48) == 32) {
            j2 |= 1024;
        }
        if (a("accessibility_enabled")) {
            j2 |= 8;
        }
        if (a("touch_exploration_enabled")) {
            j2 |= 16;
        }
        if (!g.d()) {
            return j2;
        }
        if (a("accessibility_display_inversion_enabled")) {
            j2 |= 32;
        }
        return a("skip_first_use_hints") ? j2 | 64 : j2;
    }

    private float C() {
        try {
            return Settings.System.getFloat(this.c.getContentResolver(), "font_scale");
        } catch (Settings.SettingNotFoundException e2) {
            this.b.b("DataCollector", "Error collecting font scale", e2);
            return -1.0f;
        }
    }

    private String D() {
        AudioManager audioManager = (AudioManager) this.c.getSystemService("audio");
        if (audioManager == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        if (g.e()) {
            for (AudioDeviceInfo type : audioManager.getDevices(2)) {
                String a2 = a(type.getType());
                if (!TextUtils.isEmpty(a2)) {
                    sb.append(a2);
                    sb.append(",");
                }
            }
        } else {
            if (audioManager.isWiredHeadsetOn()) {
                sb.append("headphones");
                sb.append(",");
            }
            if (audioManager.isBluetoothA2dpOn()) {
                sb.append("bluetootha2dpoutput");
            }
        }
        if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
            sb.deleteCharAt(sb.length() - 1);
        }
        String sb2 = sb.toString();
        if (TextUtils.isEmpty(sb2)) {
            this.b.b("DataCollector", "No sound outputs detected");
        }
        return sb2;
    }

    private String E() {
        if (!g.f()) {
            return null;
        }
        try {
            StringBuilder sb = new StringBuilder();
            LocaleList locales = this.c.getResources().getConfiguration().getLocales();
            for (int i2 = 0; i2 < locales.size(); i2++) {
                sb.append(locales.get(i2));
                sb.append(",");
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
                sb.deleteCharAt(sb.length() - 1);
            }
            return sb.toString();
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public Integer F() {
        AudioManager audioManager = (AudioManager) this.c.getSystemService("audio");
        if (audioManager == null) {
            return null;
        }
        return Integer.valueOf((int) (((float) audioManager.getStreamVolume(3)) * ((Float) this.f1855a.a(com.applovin.impl.sdk.c.b.O2)).floatValue()));
    }

    private double G() {
        return ((double) Math.round((((double) TimeZone.getDefault().getOffset(new Date().getTime())) * 10.0d) / 3600000.0d)) / 10.0d;
    }

    private String a(int i2) {
        if (i2 == 1) {
            return "receiver";
        }
        if (i2 == 2) {
            return "speaker";
        }
        if (i2 == 4 || i2 == 3) {
            return "headphones";
        }
        if (i2 == 8) {
            return "bluetootha2dpoutput";
        }
        if (i2 == 13 || i2 == 19 || i2 == 5 || i2 == 6 || i2 == 12 || i2 == 11) {
            return "lineout";
        }
        if (i2 == 9 || i2 == 10) {
            return "hdmioutput";
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x015a A[SYNTHETIC, Splitter:B:43:0x015a] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0282  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x02aa  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0354  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, java.lang.Object> a(java.util.Map<java.lang.String, java.lang.Object> r7, boolean r8) {
        /*
            r6 = this;
            java.lang.String r0 = "tds"
            java.lang.String r1 = "fs"
            java.lang.String r2 = "DataCollector"
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>(r7)
            android.content.Context r7 = r6.c
            android.graphics.Point r7 = com.applovin.impl.sdk.utils.g.a(r7)
            int r4 = r7.x
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            java.lang.String r5 = "dx"
            r3.put(r5, r4)
            int r7 = r7.y
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            java.lang.String r4 = "dy"
            r3.put(r4, r7)
            if (r8 == 0) goto L_0x004d
            java.util.concurrent.atomic.AtomicReference<com.applovin.impl.sdk.m$a> r7 = r6.h
            java.lang.Object r7 = r7.get()
            com.applovin.impl.sdk.m$a r7 = (com.applovin.impl.sdk.m.a) r7
            if (r7 == 0) goto L_0x0037
            r6.l()
            goto L_0x0057
        L_0x0037:
            boolean r7 = com.applovin.impl.sdk.utils.r.b()
            if (r7 == 0) goto L_0x004d
            com.applovin.impl.sdk.m$a r7 = new com.applovin.impl.sdk.m$a
            r7.<init>()
            r4 = 1
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)
            java.lang.String r5 = "inc"
            r3.put(r5, r4)
            goto L_0x0057
        L_0x004d:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.m r7 = r7.r()
            com.applovin.impl.sdk.m$a r7 = r7.j()
        L_0x0057:
            java.lang.String r4 = r7.b
            boolean r5 = com.applovin.impl.sdk.utils.o.b(r4)
            if (r5 == 0) goto L_0x0064
            java.lang.String r5 = "idfa"
            r3.put(r5, r4)
        L_0x0064:
            boolean r7 = r7.f1858a
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r4 = "dnt"
            r3.put(r4, r7)
            com.applovin.impl.sdk.i$a r7 = com.applovin.impl.sdk.i.b()
            android.content.Context r4 = r6.c
            java.lang.Boolean r7 = r7.a((android.content.Context) r4)
            if (r7 == 0) goto L_0x0080
            java.lang.String r4 = "huc"
            r3.put(r4, r7)
        L_0x0080:
            com.applovin.impl.sdk.i$a r7 = com.applovin.impl.sdk.i.a()
            android.content.Context r4 = r6.c
            java.lang.Boolean r7 = r7.a((android.content.Context) r4)
            if (r7 == 0) goto L_0x0091
            java.lang.String r4 = "aru"
            r3.put(r4, r7)
        L_0x0091:
            com.applovin.impl.sdk.i$a r7 = com.applovin.impl.sdk.i.c()
            android.content.Context r4 = r6.c
            java.lang.Boolean r7 = r7.a((android.content.Context) r4)
            if (r7 == 0) goto L_0x00a2
            java.lang.String r4 = "dns"
            r3.put(r4, r7)
        L_0x00a2:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r4 = com.applovin.impl.sdk.c.b.E2
            java.lang.Object r7 = r7.a(r4)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x00cc
            com.applovin.impl.sdk.m$b r7 = r6.A()
            int r4 = r7.f1859a
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            java.lang.String r5 = "act"
            r3.put(r5, r4)
            int r7 = r7.b
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            java.lang.String r4 = "acm"
            r3.put(r4, r7)
        L_0x00cc:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r4 = com.applovin.impl.sdk.c.b.N2
            java.lang.Object r7 = r7.a(r4)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x00e9
            boolean r7 = r6.q()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r4 = "adr"
            r3.put(r4, r7)
        L_0x00e9:
            if (r8 == 0) goto L_0x00f4
            java.util.concurrent.atomic.AtomicReference<java.lang.Integer> r7 = r6.i
            java.lang.Object r7 = r7.get()
            java.lang.Integer r7 = (java.lang.Integer) r7
            goto L_0x00f8
        L_0x00f4:
            java.lang.Integer r7 = r6.F()
        L_0x00f8:
            if (r7 == 0) goto L_0x00ff
            java.lang.String r8 = "volume"
            r3.put(r8, r7)
        L_0x00ff:
            android.content.Context r7 = r6.c     // Catch:{ SettingNotFoundException -> 0x011e }
            android.content.ContentResolver r7 = r7.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x011e }
            java.lang.String r8 = "screen_brightness"
            int r7 = android.provider.Settings.System.getInt(r7, r8)     // Catch:{ SettingNotFoundException -> 0x011e }
            float r7 = (float) r7     // Catch:{ SettingNotFoundException -> 0x011e }
            r8 = 1132396544(0x437f0000, float:255.0)
            float r7 = r7 / r8
            java.lang.String r8 = "sb"
            r4 = 1120403456(0x42c80000, float:100.0)
            float r7 = r7 * r4
            int r7 = (int) r7     // Catch:{ SettingNotFoundException -> 0x011e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ SettingNotFoundException -> 0x011e }
            r3.put(r8, r7)     // Catch:{ SettingNotFoundException -> 0x011e }
            goto L_0x0126
        L_0x011e:
            r7 = move-exception
            com.applovin.impl.sdk.r r8 = r6.b
            java.lang.String r4 = "Unable to collect screen brightness"
            r8.b(r2, r4, r7)
        L_0x0126:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.Q2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x014a
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.y.b(r7)
            java.lang.String r7 = com.applovin.impl.sdk.y.a()
            boolean r8 = com.applovin.impl.sdk.utils.o.b(r7)
            if (r8 == 0) goto L_0x014a
            java.lang.String r8 = "ua"
            r3.put(r8, r7)
        L_0x014a:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.G2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0190
            java.io.File r7 = android.os.Environment.getDataDirectory()     // Catch:{ all -> 0x0179 }
            long r7 = r7.getFreeSpace()     // Catch:{ all -> 0x0179 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x0179 }
            r3.put(r1, r7)     // Catch:{ all -> 0x0179 }
            java.io.File r7 = android.os.Environment.getDataDirectory()     // Catch:{ all -> 0x0179 }
            long r7 = r7.getTotalSpace()     // Catch:{ all -> 0x0179 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x0179 }
            r3.put(r0, r7)     // Catch:{ all -> 0x0179 }
            goto L_0x0190
        L_0x0179:
            r7 = move-exception
            r8 = -1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)
            r3.put(r1, r4)
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r3.put(r0, r8)
            com.applovin.impl.sdk.r r8 = r6.b
            java.lang.String r0 = "Unable to collect total & free space."
            r8.b(r2, r0, r7)
        L_0x0190:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.H2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x01e0
            android.content.Context r7 = r6.c
            java.lang.String r8 = "activity"
            java.lang.Object r7 = r7.getSystemService(r8)
            android.app.ActivityManager r7 = (android.app.ActivityManager) r7
            android.app.ActivityManager$MemoryInfo r8 = new android.app.ActivityManager$MemoryInfo
            r8.<init>()
            if (r7 == 0) goto L_0x01e0
            r7.getMemoryInfo(r8)
            long r0 = r8.availMem
            java.lang.Long r7 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "fm"
            r3.put(r0, r7)
            long r0 = r8.totalMem
            java.lang.Long r7 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "tm"
            r3.put(r0, r7)
            long r0 = r8.threshold
            java.lang.Long r7 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "lmt"
            r3.put(r0, r7)
            boolean r7 = r8.lowMemory
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r8 = "lm"
            r3.put(r8, r7)
        L_0x01e0:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.I2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0217
            android.content.Context r7 = r6.c
            java.lang.String r8 = "android.permission.READ_PHONE_STATE"
            boolean r7 = com.applovin.impl.sdk.utils.g.a(r8, r7)
            if (r7 == 0) goto L_0x0217
            boolean r7 = com.applovin.impl.sdk.utils.g.f()
            if (r7 == 0) goto L_0x0217
            android.content.Context r7 = r6.c
            java.lang.String r8 = "phone"
            java.lang.Object r7 = r7.getSystemService(r8)
            android.telephony.TelephonyManager r7 = (android.telephony.TelephonyManager) r7
            int r7 = r7.getDataNetworkType()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            java.lang.String r8 = "rat"
            r3.put(r8, r7)
        L_0x0217:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.c r7 = r7.h()
            com.applovin.impl.sdk.c.b<java.lang.String> r8 = com.applovin.impl.sdk.c.b.T2
            java.lang.Object r7 = r7.a(r8)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r8 = j
            boolean r8 = r7.equalsIgnoreCase(r8)
            java.lang.String r0 = "wvvc"
            if (r8 != 0) goto L_0x024d
            r8 = 0
            j = r7     // Catch:{ all -> 0x024a }
            android.content.Context r1 = r6.c     // Catch:{ all -> 0x024a }
            android.content.pm.PackageManager r1 = r1.getPackageManager()     // Catch:{ all -> 0x024a }
            android.content.pm.PackageInfo r7 = r1.getPackageInfo(r7, r8)     // Catch:{ all -> 0x024a }
            int r1 = r7.versionCode     // Catch:{ all -> 0x024a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x024a }
            r3.put(r0, r1)     // Catch:{ all -> 0x024a }
            int r7 = r7.versionCode     // Catch:{ all -> 0x024a }
            k = r7     // Catch:{ all -> 0x024a }
            goto L_0x0256
        L_0x024a:
            k = r8
            goto L_0x0256
        L_0x024d:
            int r7 = k
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r3.put(r0, r7)
        L_0x0256:
            android.content.Context r7 = r6.c
            boolean r7 = com.applovin.sdk.AppLovinSdkUtils.isTablet(r7)
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r8 = "is_tablet"
            r3.put(r8, r7)
            boolean r7 = r6.m()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r8 = "tv"
            r3.put(r8, r7)
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.F2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0291
            java.lang.String r7 = r6.D()
            boolean r8 = android.text.TextUtils.isEmpty(r7)
            if (r8 != 0) goto L_0x0291
            java.lang.String r8 = "so"
            r3.put(r8, r7)
        L_0x0291:
            java.lang.String r7 = r6.v()
            java.lang.String r8 = "orientation_lock"
            r3.put(r8, r7)
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.J2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x02b7
            boolean r7 = com.applovin.impl.sdk.utils.r.d()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r8 = "vs"
            r3.put(r8, r7)
        L_0x02b7:
            boolean r7 = com.applovin.impl.sdk.utils.g.d()
            if (r7 == 0) goto L_0x02d6
            android.content.Context r7 = r6.c
            java.lang.String r8 = "power"
            java.lang.Object r7 = r7.getSystemService(r8)
            android.os.PowerManager r7 = (android.os.PowerManager) r7
            if (r7 == 0) goto L_0x02d6
            boolean r7 = r7.isPowerSaveMode()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            java.lang.String r8 = "lpm"
            r3.put(r8, r7)
        L_0x02d6:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.U2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0301
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.utils.n r7 = r7.G()
            if (r7 == 0) goto L_0x0301
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.utils.n r7 = r7.G()
            float r7 = r7.c()
            java.lang.Float r7 = java.lang.Float.valueOf(r7)
            java.lang.String r8 = "da"
            r3.put(r8, r7)
        L_0x0301:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.b.V2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x032c
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.utils.n r7 = r7.G()
            if (r7 == 0) goto L_0x032c
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.utils.n r7 = r7.G()
            float r7 = r7.b()
            java.lang.Float r7 = java.lang.Float.valueOf(r7)
            java.lang.String r8 = "dm"
            r3.put(r8, r7)
        L_0x032c:
            com.applovin.impl.sdk.l r7 = r6.f1855a
            com.applovin.impl.sdk.g r7 = r7.H()
            int r7 = r7.a()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            java.lang.String r8 = "mute_switch"
            r3.put(r8, r7)
            com.applovin.impl.sdk.l r7 = r6.f1855a
            java.lang.String r7 = com.applovin.impl.sdk.utils.h.b((com.applovin.impl.sdk.l) r7)
            java.lang.String r8 = "network"
            r3.put(r8, r7)
            java.lang.String r7 = r6.E()
            boolean r8 = com.applovin.impl.sdk.utils.o.b(r7)
            if (r8 == 0) goto L_0x0359
            java.lang.String r8 = "kb"
            r3.put(r8, r7)
        L_0x0359:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.m.a(java.util.Map, boolean):java.util.Map");
    }

    private void a(Map<String, Object> map) {
        Map<String, String> a2;
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.K2)).booleanValue() && !map.containsKey("af")) {
            map.put("af", Long.valueOf(B()));
        }
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.L2)).booleanValue() && !map.containsKey("font")) {
            map.put("font", Float.valueOf(C()));
        }
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.Q2)).booleanValue()) {
            y.b(this.f1855a);
        }
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.P2)).booleanValue() && !map.containsKey("sua")) {
            map.put("sua", System.getProperty("http.agent"));
        }
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.M2)).booleanValue() && !map.containsKey("network_restricted")) {
            map.put("network_restricted", Boolean.valueOf(x()));
        }
        if (this.f1855a.R() && (a2 = r.a(this.f1855a.Y())) != null) {
            String str = a2.get("GraphicsMemorySizeMegabytes");
            if (o.b(str)) {
                try {
                    map.put("gms_mb", Integer.valueOf(Integer.parseInt(str)));
                } catch (NumberFormatException unused) {
                    r rVar = this.b;
                    rVar.e("DataCollector", "Graphics memory size megabytes couldn't be parsed to an integer: " + str);
                }
            }
        }
    }

    private boolean a(String str) {
        try {
            return Settings.Secure.getInt(this.c.getContentResolver(), str) == 1;
        } catch (Settings.SettingNotFoundException unused) {
            return false;
        }
    }

    private boolean a(String str, String str2) {
        for (String startsWith : e.a(str2)) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private String b(String str) {
        int length = str.length();
        int[] iArr = {11, 12, 10, 3, 2, 1, 15, 10, 15, 14};
        int length2 = iArr.length;
        char[] cArr = new char[length];
        for (int i2 = 0; i2 < length; i2++) {
            cArr[i2] = str.charAt(i2);
            for (int i3 = length2 - 1; i3 >= 0; i3--) {
                cArr[i2] = (char) (cArr[i2] ^ iArr[i3]);
            }
        }
        return new String(cArr);
    }

    private boolean m() {
        return this.c.getPackageManager().hasSystemFeature(g.d() ? "android.software.leanback" : "android.hardware.type.television");
    }

    private boolean n() {
        SensorManager sensorManager = (SensorManager) this.c.getSystemService("sensor");
        return (sensorManager == null || sensorManager.getDefaultSensor(4) == null) ? false : true;
    }

    private String o() {
        TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService("phone");
        return telephonyManager != null ? telephonyManager.getSimCountryIso().toUpperCase(Locale.ENGLISH) : "";
    }

    private String p() {
        TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        try {
            return telephonyManager.getNetworkOperatorName();
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect carrier", th);
            return "";
        }
    }

    private boolean q() {
        try {
            return r() || s();
        } catch (Throwable unused) {
            return false;
        }
    }

    private boolean r() {
        String str = Build.TAGS;
        return str != null && str.contains(b("lz}$blpz"));
    }

    private boolean s() {
        for (String b2 : new String[]{"&zpz}ld&hyy&Z|yl{|zl{'hyb", "&zk`g&z|", "&zpz}ld&k`g&z|", "&zpz}ld&qk`g&z|", "&mh}h&efjhe&qk`g&z|", "&mh}h&efjhe&k`g&z|", "&zpz}ld&zm&qk`g&z|", "&zpz}ld&k`g&oh`ezhol&z|", "&mh}h&efjhe&z|"}) {
            if (new File(b(b2)).exists()) {
                return true;
            }
        }
        return false;
    }

    private Map<String, String> t() {
        return r.b(a((Map<String, String>) null, true, false));
    }

    private Map<String, Object> u() {
        HashMap hashMap = new HashMap(32);
        hashMap.put("api_level", Integer.valueOf(Build.VERSION.SDK_INT));
        hashMap.put("brand", Build.MANUFACTURER);
        hashMap.put("brand_name", Build.BRAND);
        hashMap.put("hardware", Build.HARDWARE);
        hashMap.put("sim", Boolean.valueOf(k()));
        hashMap.put("aida", Boolean.valueOf(z()));
        hashMap.put("locale", Locale.getDefault().toString());
        hashMap.put("model", Build.MODEL);
        hashMap.put("os", Build.VERSION.RELEASE);
        hashMap.put("platform", "android");
        hashMap.put("revision", Build.DEVICE);
        hashMap.put("tz_offset", Double.valueOf(G()));
        hashMap.put("gy", Boolean.valueOf(n()));
        hashMap.put("country_code", o());
        hashMap.put("carrier", p());
        DisplayMetrics displayMetrics = this.c.getResources().getDisplayMetrics();
        if (displayMetrics != null) {
            hashMap.put("adns", Float.valueOf(displayMetrics.density));
            hashMap.put("adnsd", Integer.valueOf(displayMetrics.densityDpi));
            hashMap.put("xdpi", Float.valueOf(displayMetrics.xdpi));
            hashMap.put("ydpi", Float.valueOf(displayMetrics.ydpi));
            Point a2 = g.a(this.c);
            hashMap.put("screen_size_in", Double.valueOf(Math.sqrt(Math.pow((double) a2.x, 2.0d) + Math.pow((double) a2.y, 2.0d)) / ((double) displayMetrics.xdpi)));
        }
        hashMap.put("bt_ms", Long.valueOf(System.currentTimeMillis() - SystemClock.elapsedRealtime()));
        a((Map<String, Object>) hashMap);
        return hashMap;
    }

    private String v() {
        int b2 = r.b(this.c);
        return b2 == 1 ? "portrait" : b2 == 2 ? "landscape" : ViewProps.NONE;
    }

    private Map<String, Object> w() {
        PackageInfo packageInfo;
        HashMap hashMap = new HashMap(20);
        PackageManager packageManager = this.c.getPackageManager();
        ApplicationInfo applicationInfo = this.c.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        String str = null;
        try {
            packageInfo = packageManager.getPackageInfo(this.c.getPackageName(), 0);
            try {
                str = packageManager.getInstallerPackageName(applicationInfo.packageName);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            packageInfo = null;
        }
        hashMap.put("app_name", packageManager.getApplicationLabel(applicationInfo));
        String str2 = "";
        hashMap.put("app_version", packageInfo != null ? packageInfo.versionName : str2);
        hashMap.put("package_name", applicationInfo.packageName);
        hashMap.put("vz", o.h(applicationInfo.packageName));
        if (str != null) {
            str2 = str;
        }
        hashMap.put("installer_name", str2);
        hashMap.put("tg", q.a(this.f1855a));
        hashMap.put("debug", Boolean.valueOf(r.e(this.f1855a.i())));
        hashMap.put("ia", Long.valueOf(lastModified));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", 131);
        hashMap.put("api_did", this.f1855a.a(com.applovin.impl.sdk.c.b.f));
        hashMap.put("first_install", Boolean.valueOf(this.f1855a.l()));
        hashMap.put("first_install_v2", Boolean.valueOf(!this.f1855a.m()));
        hashMap.put("target_sdk", Integer.valueOf(applicationInfo.targetSdkVersion));
        hashMap.put("epv", Integer.valueOf(r.g()));
        return hashMap;
    }

    private boolean x() {
        ConnectivityManager connectivityManager;
        if (g.f() && (connectivityManager = (ConnectivityManager) this.c.getSystemService("connectivity")) != null) {
            try {
                return connectivityManager.getRestrictBackgroundStatus() == 3;
            } catch (Throwable th) {
                this.f1855a.j0().b("DataCollector", "Unable to collect constrained network info.", th);
            }
        }
        return false;
    }

    private a y() {
        if (z()) {
            try {
                a aVar = new a();
                AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.c);
                aVar.f1858a = advertisingIdInfo.isLimitAdTrackingEnabled();
                aVar.b = advertisingIdInfo.getId();
                return aVar;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", th);
            }
        } else {
            r.i("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }");
            return new a();
        }
    }

    private boolean z() {
        return r.e("com.google.android.gms.ads.identifier.AdvertisingIdClient");
    }

    /* access modifiers changed from: package-private */
    public String a() {
        String encodeToString = Base64.encodeToString(new JSONObject(t()).toString().getBytes(Charset.defaultCharset()), 2);
        if (!((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.v3)).booleanValue()) {
            return encodeToString;
        }
        return com.applovin.impl.sdk.utils.m.a(encodeToString, this.f1855a.h0(), r.a(this.f1855a));
    }

    public Map<String, Object> a(Map<String, String> map, boolean z, boolean z2) {
        HashMap hashMap = new HashMap(64);
        Map<String, Object> a2 = a(z);
        Map<String, Object> g2 = g();
        Map<String, Object> i2 = i();
        if (z2) {
            hashMap.put("device_info", a2);
            hashMap.put("app_info", g2);
            if (i2 != null) {
                hashMap.put("connection_info", i2);
            }
            if (map != null) {
                hashMap.put("ad_info", map);
            }
        } else {
            hashMap.putAll(a2);
            hashMap.putAll(g2);
            if (i2 != null) {
                hashMap.putAll(i2);
            }
            if (map != null) {
                hashMap.putAll(map);
            }
        }
        hashMap.put("accept", "custom_size,launch_app,video");
        hashMap.put("format", "json");
        r.a("mediation_provider", this.f1855a.b0(), (Map) hashMap);
        r.a("plugin_version", (String) this.f1855a.a(com.applovin.impl.sdk.c.b.D2), (Map) hashMap);
        if (!((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.u3)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1855a.h0());
        }
        hashMap.putAll(h());
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.b3)).booleanValue()) {
            h p = this.f1855a.p();
            hashMap.put("li", Long.valueOf(p.b(com.applovin.impl.sdk.d.g.e)));
            hashMap.put("si", Long.valueOf(p.b(com.applovin.impl.sdk.d.g.g)));
            hashMap.put("pf", Long.valueOf(p.b(com.applovin.impl.sdk.d.g.k)));
            hashMap.put("mpf", Long.valueOf(p.b(com.applovin.impl.sdk.d.g.r)));
            hashMap.put("gpf", Long.valueOf(p.b(com.applovin.impl.sdk.d.g.l)));
            hashMap.put("asoac", Long.valueOf(p.b(com.applovin.impl.sdk.d.g.p)));
        }
        hashMap.put("rid", UUID.randomUUID().toString());
        return hashMap;
    }

    public Map<String, Object> a(boolean z) {
        HashMap hashMap;
        synchronized (this.e) {
            hashMap = new HashMap(this.d);
        }
        return a((Map<String, Object>) hashMap, z);
    }

    public Map<String, Object> b() {
        return new HashMap(this.d);
    }

    public Map<String, Object> c() {
        return new HashMap(this.f);
    }

    public Map<String, Object> d() {
        return a(false);
    }

    public void e() {
        synchronized (this.e) {
            a(this.d);
        }
    }

    public boolean f() {
        return this.g;
    }

    public Map<String, Object> g() {
        HashMap hashMap = new HashMap(this.f);
        hashMap.put("test_ads", Boolean.valueOf(this.g));
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.w2)).booleanValue()) {
            r.a("cuid", this.f1855a.V(), (Map) hashMap);
        }
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.z2)).booleanValue()) {
            hashMap.put("compass_random_token", this.f1855a.W());
        }
        if (((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.B2)).booleanValue()) {
            hashMap.put("applovin_random_token", this.f1855a.X());
        }
        String name = this.f1855a.Z().getName();
        if (o.b(name)) {
            hashMap.put("user_segment_name", name);
        }
        return hashMap;
    }

    public Map<String, Object> h() {
        HashMap hashMap = new HashMap(5);
        hashMap.put("sc", this.f1855a.a(com.applovin.impl.sdk.c.b.k));
        hashMap.put("sc2", this.f1855a.a(com.applovin.impl.sdk.c.b.l));
        hashMap.put("sc3", this.f1855a.a(com.applovin.impl.sdk.c.b.m));
        hashMap.put("server_installed_at", this.f1855a.a(com.applovin.impl.sdk.c.b.n));
        r.a("persisted_data", (String) this.f1855a.a(d.z), (Map) hashMap);
        return hashMap;
    }

    public Map<String, Object> i() {
        a.b a2 = this.f1855a.n().a();
        if (a2 == null) {
            return null;
        }
        HashMap hashMap = new HashMap(4);
        hashMap.put("lrm_ts_ms", Long.valueOf(a2.a()));
        hashMap.put("lrm_url", a2.b());
        hashMap.put("lrm_ct_ms", Long.valueOf(a2.d()));
        hashMap.put("lrm_rs", Long.valueOf(a2.c()));
        return hashMap;
    }

    public a j() {
        a y = y();
        if (!((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.v2)).booleanValue()) {
            y = new a();
        } else if (y.f1858a && !((Boolean) this.f1855a.a(com.applovin.impl.sdk.c.b.u2)).booleanValue()) {
            y.b = "";
        }
        this.g = o.b(y.b) ? this.f1855a.Y().getTestDeviceAdvertisingIds().contains(y.b) : false;
        return y;
    }

    public boolean k() {
        return a(Build.DEVICE, "goldfish,vbox") || a(Build.HARDWARE, "ranchu,generic,vbox") || a(Build.MANUFACTURER, "Genymotion") || a(Build.MODEL, "Android SDK built for x86");
    }

    public void l() {
        this.f1855a.o().a((com.applovin.impl.sdk.e.a) new f(this.f1855a, new f.a() {
            public void a(a aVar) {
                m.this.h.set(aVar);
            }
        }), o.a.ADVERTISING_INFO_COLLECTION);
        this.f1855a.o().a((com.applovin.impl.sdk.e.a) new y(this.f1855a, new Runnable() {
            public void run() {
                m.this.i.set(m.this.F());
            }
        }), o.a.CACHING_OTHER);
    }
}
