package com.applovin.impl.sdk;

import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;

public class x {

    /* renamed from: a  reason: collision with root package name */
    private final l f1935a;
    /* access modifiers changed from: private */
    public final r b;
    private final Object c = new Object();
    private final Rect d = new Rect();
    private final Handler e;
    private final Runnable f;
    private final ViewTreeObserver.OnPreDrawListener g;
    /* access modifiers changed from: private */
    public final WeakReference<MaxAdView> h;
    private WeakReference<ViewTreeObserver> i = new WeakReference<>((Object) null);
    /* access modifiers changed from: private */
    public WeakReference<View> j = new WeakReference<>((Object) null);
    private int k;
    private long l;
    private long m = Long.MIN_VALUE;

    public interface a {
        void onLogVisibilityImpression();
    }

    public x(MaxAdView maxAdView, l lVar, a aVar) {
        this.f1935a = lVar;
        this.b = lVar.j0();
        this.e = new Handler(Looper.getMainLooper());
        this.h = new WeakReference<>(maxAdView);
        final WeakReference weakReference = new WeakReference(aVar);
        this.f = new Runnable() {
            public void run() {
                MaxAdView maxAdView = (MaxAdView) x.this.h.get();
                View view = (View) x.this.j.get();
                if (maxAdView != null && view != null) {
                    if (x.this.b(maxAdView, view)) {
                        x.this.b.b("VisibilityTracker", "View met visibility requirements. Logging visibility impression..");
                        x.this.a();
                        a aVar = (a) weakReference.get();
                        if (aVar != null) {
                            aVar.onLogVisibilityImpression();
                            return;
                        }
                        return;
                    }
                    x.this.b();
                }
            }
        };
        this.g = new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                x.this.b();
                x.this.c();
                return true;
            }
        };
    }

    private void a(View view) {
        View a2 = r.a((View) this.h.get());
        if (a2 == null) {
            a2 = r.a(view);
        }
        if (a2 == null) {
            this.b.b("VisibilityTracker", "Unable to set view tree observer due to no root view.");
            return;
        }
        ViewTreeObserver viewTreeObserver = a2.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            this.b.d("VisibilityTracker", "Unable to set view tree observer since the view tree observer is not alive.");
            return;
        }
        this.i = new WeakReference<>(viewTreeObserver);
        viewTreeObserver.addOnPreDrawListener(this.g);
    }

    private boolean a(View view, View view2) {
        return view2 != null && view2.getVisibility() == 0 && view.getParent() != null && view2.getWidth() > 0 && view2.getHeight() > 0 && view2.getGlobalVisibleRect(this.d) && ((long) (AppLovinSdkUtils.pxToDp(view2.getContext(), this.d.width()) * AppLovinSdkUtils.pxToDp(view2.getContext(), this.d.height()))) >= ((long) this.k);
    }

    /* access modifiers changed from: private */
    public void b() {
        this.e.postDelayed(this.f, ((Long) this.f1935a.a(b.V0)).longValue());
    }

    /* access modifiers changed from: private */
    public boolean b(View view, View view2) {
        if (!a(view, view2)) {
            return false;
        }
        if (this.m == Long.MIN_VALUE) {
            this.m = SystemClock.uptimeMillis();
        }
        return SystemClock.uptimeMillis() - this.m >= this.l;
    }

    /* access modifiers changed from: private */
    public void c() {
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.i.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnPreDrawListener(this.g);
        }
        this.i.clear();
    }

    public void a() {
        synchronized (this.c) {
            this.e.removeMessages(0);
            c();
            this.m = Long.MIN_VALUE;
            this.j.clear();
        }
    }

    public void a(com.applovin.impl.mediation.a.b bVar) {
        synchronized (this.c) {
            this.b.b("VisibilityTracker", "Tracking Visibility...");
            a();
            this.j = new WeakReference<>(bVar.B());
            this.k = bVar.G();
            this.l = bVar.I();
            a((View) this.j.get());
        }
    }
}
