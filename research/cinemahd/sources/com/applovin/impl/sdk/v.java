package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.utils.AppKilledService;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.r;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class v {
    private static final AtomicBoolean f = new AtomicBoolean();

    /* renamed from: a  reason: collision with root package name */
    final l f1929a;
    private final AtomicBoolean b = new AtomicBoolean();
    private final AtomicBoolean c = new AtomicBoolean();
    private Date d;
    private Date e;

    v(final l lVar) {
        this.f1929a = lVar;
        final Application application = (Application) lVar.i();
        application.registerActivityLifecycleCallbacks(new a() {
            public void onActivityResumed(Activity activity) {
                super.onActivityResumed(activity);
                v.this.d();
            }
        });
        application.registerComponentCallbacks(new ComponentCallbacks2() {
            public void onConfigurationChanged(Configuration configuration) {
            }

            public void onLowMemory() {
            }

            public void onTrimMemory(int i) {
                if (i == 20) {
                    v.this.e();
                }
            }
        });
        IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        application.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if ("android.intent.action.USER_PRESENT".equals(action)) {
                    if (r.c()) {
                        v.this.d();
                    }
                } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                    v.this.e();
                }
            }
        }, intentFilter);
        if (((Boolean) lVar.a(b.z1)).booleanValue() && f.compareAndSet(false, true)) {
            final Intent intent = new Intent(application, AppKilledService.class);
            application.startService(intent);
            lVar.I().registerReceiver(new AppLovinBroadcastManager.Receiver(this) {
                public void onReceive(Context context, Intent intent, Map<String, Object> map) {
                    application.stopService(intent);
                    lVar.I().unregisterReceiver(this);
                }
            }, new IntentFilter(AppKilledService.ACTION_APP_KILLED));
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.c.compareAndSet(true, false)) {
            g();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.c.compareAndSet(false, true)) {
            f();
        }
    }

    private void f() {
        this.f1929a.j0().b("SessionTracker", "Application Paused");
        this.f1929a.I().sendBroadcastSync(new Intent("com.applovin.application_paused"), (Map<String, Object>) null);
        if (!this.b.get()) {
            boolean booleanValue = ((Boolean) this.f1929a.a(b.r2)).booleanValue();
            long millis = TimeUnit.MINUTES.toMillis(((Long) this.f1929a.a(b.t2)).longValue());
            if (this.d == null || System.currentTimeMillis() - this.d.getTime() >= millis) {
                ((EventServiceImpl) this.f1929a.e0()).trackEvent("paused");
                if (booleanValue) {
                    this.d = new Date();
                }
            }
            if (!booleanValue) {
                this.d = new Date();
            }
        }
    }

    private void g() {
        this.f1929a.j0().b("SessionTracker", "Application Resumed");
        boolean booleanValue = ((Boolean) this.f1929a.a(b.r2)).booleanValue();
        long longValue = ((Long) this.f1929a.a(b.s2)).longValue();
        this.f1929a.I().sendBroadcastSync(new Intent("com.applovin.application_resumed"), (Map<String, Object>) null);
        if (!this.b.getAndSet(false)) {
            long millis = TimeUnit.MINUTES.toMillis(longValue);
            if (this.e == null || System.currentTimeMillis() - this.e.getTime() >= millis) {
                ((EventServiceImpl) this.f1929a.e0()).trackEvent("resumed");
                if (booleanValue) {
                    this.e = new Date();
                }
            }
            if (!booleanValue) {
                this.e = new Date();
            }
            this.f1929a.p().a(g.n);
        }
    }

    public boolean a() {
        return this.c.get();
    }

    public void b() {
        this.b.set(true);
    }

    public void c() {
        this.b.set(false);
    }
}
