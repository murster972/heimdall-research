package com.applovin.impl.sdk;

import android.graphics.PointF;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.adview.AdViewControllerImpl;
import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.a.h;
import com.applovin.impl.sdk.e.i;
import com.applovin.impl.sdk.e.j;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class AppLovinAdServiceImpl implements AppLovinAdService {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1740a;
    private final r b;
    private final Handler c = new Handler(Looper.getMainLooper());
    private final Map<d, b> d;
    private final Object e = new Object();

    private class a implements AppLovinAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        private final b f1743a;

        private a(b bVar) {
            this.f1743a = bVar;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            HashSet<AppLovinAdLoadListener> hashSet;
            AppLovinAdBase appLovinAdBase = (AppLovinAdBase) appLovinAd;
            d adZone = appLovinAdBase.getAdZone();
            if (!(appLovinAd instanceof h)) {
                AppLovinAdServiceImpl.this.f1740a.w().a(appLovinAdBase);
                appLovinAd = new h(adZone, AppLovinAdServiceImpl.this.f1740a);
            }
            synchronized (this.f1743a.f1744a) {
                hashSet = new HashSet<>(this.f1743a.c);
                this.f1743a.c.clear();
                this.f1743a.b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(appLovinAd, a2);
            }
        }

        public void failedToReceiveAd(int i) {
            HashSet<AppLovinAdLoadListener> hashSet;
            synchronized (this.f1743a.f1744a) {
                hashSet = new HashSet<>(this.f1743a.c);
                this.f1743a.c.clear();
                this.f1743a.b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(i, a2);
            }
        }
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        final Object f1744a;
        boolean b;
        final Collection<AppLovinAdLoadListener> c;

        private b() {
            this.f1744a = new Object();
            this.c = new HashSet();
        }

        public String toString() {
            return "AdLoadState{, isWaitingForAd=" + this.b + ", pendingAdListeners=" + this.c + '}';
        }
    }

    AppLovinAdServiceImpl(l lVar) {
        this.f1740a = lVar;
        this.b = lVar.j0();
        this.d = new HashMap(5);
        this.d.put(d.b(lVar), new b());
        this.d.put(d.c(lVar), new b());
        this.d.put(d.d(lVar), new b());
        this.d.put(d.e(lVar), new b());
        this.d.put(d.f(lVar), new b());
    }

    private b a(d dVar) {
        b bVar;
        synchronized (this.e) {
            bVar = this.d.get(dVar);
            if (bVar == null) {
                bVar = new b();
                this.d.put(dVar, bVar);
            }
        }
        return bVar;
    }

    private String a(String str, long j, int i, String str2, boolean z) {
        try {
            if (!o.b(str)) {
                return null;
            }
            if (i < 0 || i > 100) {
                i = 0;
            }
            return Uri.parse(str).buildUpon().appendQueryParameter("et_s", Long.toString(j)).appendQueryParameter("pv", Integer.toString(i)).appendQueryParameter("vid_ts", str2).appendQueryParameter("uvs", Boolean.toString(z)).build().toString();
        } catch (Throwable th) {
            r rVar = this.b;
            rVar.b("AppLovinAdService", "Unknown error parsing the video end url: " + str, th);
            return null;
        }
    }

    private String a(String str, long j, long j2, boolean z, int i) {
        if (!o.b(str)) {
            return null;
        }
        Uri.Builder appendQueryParameter = Uri.parse(str).buildUpon().appendQueryParameter("et_ms", Long.toString(j)).appendQueryParameter("vs_ms", Long.toString(j2));
        if (i != g.h) {
            appendQueryParameter.appendQueryParameter("musw_ch", Boolean.toString(z));
            appendQueryParameter.appendQueryParameter("musw_st", Boolean.toString(g.a(i)));
        }
        return appendQueryParameter.build().toString();
    }

    /* access modifiers changed from: private */
    public void a(final int i, final AppLovinAdLoadListener appLovinAdLoadListener) {
        this.c.post(new Runnable(this) {
            public void run() {
                try {
                    appLovinAdLoadListener.failedToReceiveAd(i);
                } catch (Throwable th) {
                    r.c("AppLovinAdService", "Unable to notify listener about ad load failure", th);
                }
            }
        });
    }

    private void a(Uri uri, g gVar, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl) {
        if (appLovinAdView != null) {
            if (r.a(appLovinAdView.getContext(), uri, this.f1740a)) {
                k.c(adViewControllerImpl.getAdViewEventListener(), (AppLovinAd) gVar, appLovinAdView);
            }
            adViewControllerImpl.dismissInterstitialIfRequired();
            return;
        }
        this.b.e("AppLovinAdService", "Unable to launch click - adView has been prematurely destroyed");
    }

    private void a(d dVar, a aVar) {
        AppLovinAdBase b2 = this.f1740a.w().b(dVar);
        if (b2 != null) {
            r rVar = this.b;
            rVar.b("AppLovinAdService", "Using pre-loaded ad: " + b2 + " for " + dVar);
            this.f1740a.y().a(b2, true, false);
            aVar.adReceived(b2);
            return;
        }
        a((com.applovin.impl.sdk.e.a) new j(dVar, aVar, this.f1740a));
    }

    private void a(d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (dVar == null) {
            throw new IllegalArgumentException("No zone specified");
        } else if (appLovinAdLoadListener != null) {
            r j0 = this.f1740a.j0();
            j0.b("AppLovinAdService", "Loading next ad of zone {" + dVar + "}...");
            b a2 = a(dVar);
            synchronized (a2.f1744a) {
                a2.c.add(appLovinAdLoadListener);
                if (!a2.b) {
                    a2.b = true;
                    a(dVar, new a(a2));
                } else {
                    this.b.b("AppLovinAdService", "Already waiting on an ad load...");
                }
            }
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    private void a(com.applovin.impl.sdk.d.a aVar) {
        if (o.b(aVar.a())) {
            this.f1740a.q().a(f.o().c(r.b(aVar.a())).d(o.b(aVar.b()) ? r.b(aVar.b()) : null).b(aVar.c()).a(false).c(aVar.d()).a());
            return;
        }
        this.b.d("AppLovinAdService", "Requested a postback dispatch for a null URL; nothing to do...");
    }

    private void a(com.applovin.impl.sdk.e.a aVar) {
        if (!this.f1740a.O()) {
            r.h("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
        }
        this.f1740a.z();
        this.f1740a.o().a(aVar, o.a.MAIN);
    }

    /* access modifiers changed from: private */
    public void a(final AppLovinAd appLovinAd, final AppLovinAdLoadListener appLovinAdLoadListener) {
        this.c.post(new Runnable(this) {
            public void run() {
                try {
                    appLovinAdLoadListener.adReceived(appLovinAd);
                } catch (Throwable th) {
                    r.c("AppLovinAdService", "Unable to notify listener about a newly loaded ad", th);
                }
            }
        });
    }

    private void a(List<com.applovin.impl.sdk.d.a> list) {
        if (list != null && !list.isEmpty()) {
            for (com.applovin.impl.sdk.d.a a2 : list) {
                a(a2);
            }
        }
    }

    public AppLovinAd dequeueAd(d dVar) {
        AppLovinAdBase c2 = this.f1740a.w().c(dVar);
        r rVar = this.b;
        rVar.b("AppLovinAdService", "Dequeued ad: " + c2 + " for zone: " + dVar + "...");
        return c2;
    }

    public String getBidToken() {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        String a2 = this.f1740a.r().a();
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        return a2;
    }

    public boolean hasPreloadedAd(AppLovinAdSize appLovinAdSize) {
        return this.f1740a.w().a(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.f1740a));
    }

    public boolean hasPreloadedAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            r.i("AppLovinAdService", "Unable to check if ad is preloaded - invalid zone id");
            return false;
        }
        return this.f1740a.w().a(d.a(str, this.f1740a));
    }

    public void loadNextAd(AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        a(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.f1740a), appLovinAdLoadListener);
    }

    public void loadNextAd(String str, AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        r rVar = this.b;
        rVar.b("AppLovinAdService", "Loading next ad of zone {" + str + "} with size " + appLovinAdSize);
        a(d.a(appLovinAdSize, AppLovinAdType.REGULAR, str, this.f1740a), appLovinAdLoadListener);
    }

    /* JADX WARNING: type inference failed for: r11v23, types: [com.applovin.impl.sdk.e.k] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadNextAdForAdToken(java.lang.String r11, com.applovin.sdk.AppLovinAdLoadListener r12) {
        /*
            r10 = this;
            if (r11 == 0) goto L_0x0007
            java.lang.String r11 = r11.trim()
            goto L_0x0008
        L_0x0007:
            r11 = 0
        L_0x0008:
            boolean r0 = android.text.TextUtils.isEmpty(r11)
            r1 = -8
            java.lang.String r2 = "AppLovinAdService"
            if (r0 == 0) goto L_0x001a
            java.lang.String r11 = "Invalid ad token specified"
            com.applovin.impl.sdk.r.i(r2, r11)
            r10.a((int) r1, (com.applovin.sdk.AppLovinAdLoadListener) r12)
            return
        L_0x001a:
            com.applovin.impl.sdk.a.c r0 = new com.applovin.impl.sdk.a.c
            com.applovin.impl.sdk.l r3 = r10.f1740a
            r0.<init>(r11, r3)
            com.applovin.impl.sdk.a.c$a r11 = r0.b()
            com.applovin.impl.sdk.a.c$a r3 = com.applovin.impl.sdk.a.c.a.REGULAR
            if (r11 != r3) goto L_0x004b
            com.applovin.impl.sdk.r r11 = r10.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Loading next ad for token: "
            r1.append(r3)
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            r11.b(r2, r1)
            com.applovin.impl.sdk.e.k r11 = new com.applovin.impl.sdk.e.k
            com.applovin.impl.sdk.l r1 = r10.f1740a
            r11.<init>(r0, r12, r1)
        L_0x0046:
            r10.a((com.applovin.impl.sdk.e.a) r11)
            goto L_0x00fc
        L_0x004b:
            com.applovin.impl.sdk.a.c$a r11 = r0.b()
            com.applovin.impl.sdk.a.c$a r3 = com.applovin.impl.sdk.a.c.a.AD_RESPONSE_JSON
            if (r11 != r3) goto L_0x00e5
            org.json.JSONObject r5 = r0.d()
            if (r5 == 0) goto L_0x00ce
            com.applovin.impl.sdk.l r11 = r10.f1740a
            com.applovin.impl.sdk.utils.h.f(r5, r11)
            com.applovin.impl.sdk.l r11 = r10.f1740a
            com.applovin.impl.sdk.utils.h.b((org.json.JSONObject) r5, (com.applovin.impl.sdk.l) r11)
            com.applovin.impl.sdk.l r11 = r10.f1740a
            com.applovin.impl.sdk.utils.h.a((org.json.JSONObject) r5, (com.applovin.impl.sdk.l) r11)
            com.applovin.impl.sdk.l r11 = r10.f1740a
            com.applovin.impl.sdk.utils.h.c(r5, r11)
            org.json.JSONArray r11 = new org.json.JSONArray
            r11.<init>()
            com.applovin.impl.sdk.l r1 = r10.f1740a
            java.lang.String r3 = "ads"
            org.json.JSONArray r11 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r5, (java.lang.String) r3, (org.json.JSONArray) r11, (com.applovin.impl.sdk.l) r1)
            int r11 = r11.length()
            if (r11 <= 0) goto L_0x00b2
            com.applovin.impl.sdk.r r11 = r10.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Rendering ad for token: "
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r11.b(r2, r0)
            com.applovin.impl.sdk.l r11 = r10.f1740a
            com.applovin.impl.sdk.a.d r6 = com.applovin.impl.sdk.utils.r.a((org.json.JSONObject) r5, (com.applovin.impl.sdk.l) r11)
            com.applovin.impl.sdk.a.f$a r8 = new com.applovin.impl.sdk.a.f$a
            com.applovin.impl.sdk.l r11 = r10.f1740a
            r8.<init>(r6, r12, r11)
            r11 = 1
            r8.a(r11)
            com.applovin.impl.sdk.e.p r11 = new com.applovin.impl.sdk.e.p
            com.applovin.impl.sdk.a.b r7 = com.applovin.impl.sdk.a.b.DECODED_AD_TOKEN_JSON
            com.applovin.impl.sdk.l r9 = r10.f1740a
            r4 = r11
            r4.<init>(r5, r6, r7, r8, r9)
            goto L_0x0046
        L_0x00b2:
            com.applovin.impl.sdk.r r11 = r10.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "No ad returned from the server for token: "
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r11.e(r2, r0)
            r11 = 204(0xcc, float:2.86E-43)
            r12.failedToReceiveAd(r11)
            goto L_0x00fc
        L_0x00ce:
            com.applovin.impl.sdk.r r11 = r10.b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unable to retrieve ad response JSON from token: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r11.e(r2, r0)
            goto L_0x00f9
        L_0x00e5:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r3 = "Invalid ad token specified: "
            r11.append(r3)
            r11.append(r0)
            java.lang.String r11 = r11.toString()
            com.applovin.impl.sdk.r.i(r2, r11)
        L_0x00f9:
            r12.failedToReceiveAd(r1)
        L_0x00fc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.AppLovinAdServiceImpl.loadNextAdForAdToken(java.lang.String, com.applovin.sdk.AppLovinAdLoadListener):void");
    }

    public void loadNextAdForZoneId(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (!TextUtils.isEmpty(str)) {
            r rVar = this.b;
            rVar.b("AppLovinAdService", "Loading next ad of zone {" + str + "}");
            a(d.a(str, this.f1740a), appLovinAdLoadListener);
            return;
        }
        throw new IllegalArgumentException("No zone id specified");
    }

    public void loadNextAdForZoneIds(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener) {
        List<String> a2 = e.a(list);
        if (a2 == null || a2.isEmpty()) {
            r.i("AppLovinAdService", "No zones were provided");
            a(-7, appLovinAdLoadListener);
            return;
        }
        r rVar = this.b;
        rVar.b("AppLovinAdService", "Loading next ad for zones: " + a2);
        a((com.applovin.impl.sdk.e.a) new i(a2, appLovinAdLoadListener, this.f1740a));
    }

    public void loadNextIncentivizedAd(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        r rVar = this.b;
        rVar.b("AppLovinAdService", "Loading next incentivized ad of zone {" + str + "}");
        a(d.b(str, this.f1740a), appLovinAdLoadListener);
    }

    public void preloadAd(AppLovinAdSize appLovinAdSize) {
    }

    public void preloadAdForZoneId(String str) {
    }

    public String toString() {
        return "AppLovinAdService{adLoadStates=" + this.d + '}';
    }

    public void trackAndLaunchClick(g gVar, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl, Uri uri, PointF pointF) {
        if (gVar == null) {
            this.b.e("AppLovinAdService", "Unable to track ad view click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking click on an ad...");
        a(gVar.a(pointF));
        a(uri, gVar, appLovinAdView, adViewControllerImpl);
    }

    public void trackAndLaunchVideoClick(g gVar, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        if (gVar == null) {
            this.b.e("AppLovinAdService", "Unable to track video click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking VIDEO click on an ad...");
        a(gVar.b(pointF));
        r.a(appLovinAdView.getContext(), uri, this.f1740a);
    }

    public void trackAppKilled(g gVar) {
        if (gVar == null) {
            this.b.e("AppLovinAdService", "Unable to track app killed. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking app killed during ad...");
        List<com.applovin.impl.sdk.d.a> a0 = gVar.a0();
        if (a0 == null || a0.isEmpty()) {
            r rVar = this.b;
            rVar.d("AppLovinAdService", "Unable to track app killed during AD #" + gVar.getAdIdNumber() + ". Missing app killed tracking URL.");
            return;
        }
        for (com.applovin.impl.sdk.d.a next : a0) {
            a(new com.applovin.impl.sdk.d.a(next.a(), next.b()));
        }
    }

    public void trackFullScreenAdClosed(g gVar, long j, long j2, boolean z, int i) {
        r rVar = this.b;
        if (gVar == null) {
            rVar.e("AppLovinAdService", "Unable to track ad closed. No ad specified.");
            return;
        }
        rVar.b("AppLovinAdService", "Tracking ad closed...");
        List<com.applovin.impl.sdk.d.a> Z = gVar.Z();
        if (Z == null || Z.isEmpty()) {
            r rVar2 = this.b;
            rVar2.d("AppLovinAdService", "Unable to track ad closed for AD #" + gVar.getAdIdNumber() + ". Missing ad close tracking URL." + gVar.getAdIdNumber());
            return;
        }
        for (com.applovin.impl.sdk.d.a next : Z) {
            long j3 = j;
            long j4 = j2;
            boolean z2 = z;
            int i2 = i;
            String a2 = a(next.a(), j3, j4, z2, i2);
            String a3 = a(next.b(), j3, j4, z2, i2);
            if (com.applovin.impl.sdk.utils.o.b(a2)) {
                a(new com.applovin.impl.sdk.d.a(a2, a3));
            } else {
                r rVar3 = this.b;
                rVar3.e("AppLovinAdService", "Failed to parse url: " + next.a());
            }
        }
    }

    public void trackImpression(g gVar) {
        if (gVar == null) {
            this.b.e("AppLovinAdService", "Unable to track impression click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking impression on ad...");
        a(gVar.b0());
        this.f1740a.y().a(gVar);
    }

    public void trackVideoEnd(g gVar, long j, int i, boolean z) {
        r rVar = this.b;
        if (gVar == null) {
            rVar.e("AppLovinAdService", "Unable to track video end. No ad specified");
            return;
        }
        rVar.b("AppLovinAdService", "Tracking video end on ad...");
        List<com.applovin.impl.sdk.d.a> Y = gVar.Y();
        if (Y == null || Y.isEmpty()) {
            r rVar2 = this.b;
            rVar2.d("AppLovinAdService", "Unable to submit persistent postback for AD #" + gVar.getAdIdNumber() + ". Missing video end tracking URL.");
            return;
        }
        String l = Long.toString(System.currentTimeMillis());
        for (com.applovin.impl.sdk.d.a next : Y) {
            if (com.applovin.impl.sdk.utils.o.b(next.a())) {
                long j2 = j;
                int i2 = i;
                String str = l;
                boolean z2 = z;
                String a2 = a(next.a(), j2, i2, str, z2);
                String a3 = a(next.b(), j2, i2, str, z2);
                if (a2 != null) {
                    a(new com.applovin.impl.sdk.d.a(a2, a3));
                } else {
                    r rVar3 = this.b;
                    rVar3.e("AppLovinAdService", "Failed to parse url: " + next.a());
                }
            } else {
                this.b.d("AppLovinAdService", "Requested a postback dispatch for an empty video end URL; nothing to do...");
            }
        }
    }
}
