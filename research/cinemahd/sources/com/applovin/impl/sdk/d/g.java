package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;

public class g {
    private static final Set<String> b = new HashSet(32);
    private static final Set<g> c = new HashSet(16);
    public static final g d = a("ad_req");
    public static final g e = a("ad_imp");
    public static final g f = a("ad_session_start");
    public static final g g = a("ad_imp_session");
    public static final g h = a("cached_files_expired");
    public static final g i = a("cache_drop_count");
    public static final g j = a("sdk_reset_state_count", true);
    public static final g k = a("ad_response_process_failures", true);
    public static final g l = a("response_process_failures", true);
    public static final g m = a("incent_failed_to_display_count", true);
    public static final g n = a("app_paused_and_resumed");
    public static final g o = a("ad_rendered_with_mismatched_sdk_key", true);
    public static final g p = a("ad_shown_outside_app_count");
    public static final g q = a("med_ad_req");
    public static final g r = a("med_ad_response_process_failures", true);
    public static final g s = a("med_adapters_failed_init_missing_activity", true);
    public static final g t = a("med_waterfall_ad_no_fill", true);
    public static final g u = a("med_waterfall_ad_adapter_load_failed", true);
    public static final g v = a("med_waterfall_ad_invalid_response", true);

    /* renamed from: a  reason: collision with root package name */
    private final String f1813a;

    static {
        a("fullscreen_ad_nil_vc_count");
        a("applovin_bundle_missing");
    }

    private g(String str) {
        this.f1813a = str;
    }

    private static g a(String str) {
        return a(str, false);
    }

    private static g a(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No key name specified");
        } else if (!b.contains(str)) {
            b.add(str);
            g gVar = new g(str);
            if (z) {
                c.add(gVar);
            }
            return gVar;
        } else {
            throw new IllegalArgumentException("Key has already been used: " + str);
        }
    }

    public static Set<g> b() {
        return c;
    }

    public String a() {
        return this.f1813a;
    }
}
