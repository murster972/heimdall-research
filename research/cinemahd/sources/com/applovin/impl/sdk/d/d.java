package com.applovin.impl.sdk.d;

import android.annotation.TargetApi;
import android.app.Activity;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.d.c;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.h;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final l f1809a;
    private final h b;
    private final c.b c;
    private final Object d = new Object();
    private final long e;
    private long f;
    private long g;
    private long h;

    public d(AppLovinAdBase appLovinAdBase, l lVar) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (lVar != null) {
            this.f1809a = lVar;
            this.b = lVar.p();
            this.c = lVar.A().a(appLovinAdBase);
            c.b bVar = this.c;
            bVar.a(b.d, (long) appLovinAdBase.getSource().ordinal());
            bVar.a();
            this.e = appLovinAdBase.getCreatedAtMillis();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static void a(long j, AppLovinAdBase appLovinAdBase, l lVar) {
        if (appLovinAdBase != null && lVar != null) {
            c.b a2 = lVar.A().a(appLovinAdBase);
            a2.a(b.e, j);
            a2.a();
        }
    }

    public static void a(AppLovinAdBase appLovinAdBase, l lVar) {
        if (appLovinAdBase != null && lVar != null) {
            c.b a2 = lVar.A().a(appLovinAdBase);
            a2.a(b.f, appLovinAdBase.getFetchLatencyMillis());
            a2.a(b.g, appLovinAdBase.getFetchResponseSize());
            a2.a();
        }
    }

    private void a(b bVar) {
        synchronized (this.d) {
            if (this.f > 0) {
                long currentTimeMillis = System.currentTimeMillis() - this.f;
                c.b bVar2 = this.c;
                bVar2.a(bVar, currentTimeMillis);
                bVar2.a();
            }
        }
    }

    public static void a(e eVar, AppLovinAdBase appLovinAdBase, l lVar) {
        if (appLovinAdBase != null && lVar != null && eVar != null) {
            c.b a2 = lVar.A().a(appLovinAdBase);
            a2.a(b.h, eVar.c());
            a2.a(b.i, eVar.d());
            a2.a(b.x, eVar.g());
            a2.a(b.y, eVar.h());
            a2.a(b.z, eVar.b() ? 1 : 0);
            a2.a();
        }
    }

    @TargetApi(24)
    public void a() {
        long a2 = this.b.a(g.e);
        long a3 = this.b.a(g.g);
        c.b bVar = this.c;
        bVar.a(b.m, a2);
        bVar.a(b.l, a3);
        synchronized (this.d) {
            long j = 0;
            if (this.e > 0) {
                this.f = System.currentTimeMillis();
                long k = this.f - this.f1809a.k();
                long j2 = this.f - this.e;
                long j3 = h.a(this.f1809a.i()) ? 1 : 0;
                Activity a4 = this.f1809a.D().a();
                if (g.f() && a4 != null && a4.isInMultiWindowMode()) {
                    j = 1;
                }
                c.b bVar2 = this.c;
                bVar2.a(b.k, k);
                bVar2.a(b.j, j2);
                bVar2.a(b.s, j3);
                bVar2.a(b.A, j);
            }
        }
        this.c.a();
    }

    public void a(long j) {
        c.b bVar = this.c;
        bVar.a(b.u, j);
        bVar.a();
    }

    public void b() {
        synchronized (this.d) {
            if (this.g < 1) {
                this.g = System.currentTimeMillis();
                if (this.f > 0) {
                    long j = this.g - this.f;
                    c.b bVar = this.c;
                    bVar.a(b.p, j);
                    bVar.a();
                }
            }
        }
    }

    public void b(long j) {
        c.b bVar = this.c;
        bVar.a(b.t, j);
        bVar.a();
    }

    public void c() {
        a(b.n);
    }

    public void c(long j) {
        c.b bVar = this.c;
        bVar.a(b.v, j);
        bVar.a();
    }

    public void d() {
        a(b.q);
    }

    public void d(long j) {
        synchronized (this.d) {
            if (this.h < 1) {
                this.h = j;
                c.b bVar = this.c;
                bVar.a(b.w, j);
                bVar.a();
            }
        }
    }

    public void e() {
        a(b.r);
    }

    public void f() {
        a(b.o);
    }

    public void g() {
        c.b bVar = this.c;
        bVar.a(b.B);
        bVar.a();
    }
}
