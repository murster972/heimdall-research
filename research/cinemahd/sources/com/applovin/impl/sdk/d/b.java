package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;

public class b {
    static final b A = a("smwm", "AD_SHOWN_IN_MULTIWINDOW_MODE");
    static final b B = a("vssc", "VIDEO_STREAM_STALLED_COUNT");
    public static final b C = a("wvem", "WEB_VIEW_ERROR_MESSAGES");
    public static final b D = a("wvhec", "WEB_VIEW_HTTP_ERROR_COUNT");
    public static final b E = a("wvrec", "WEB_VIEW_RENDER_ERROR_COUNT");
    public static final b F = a("wvsem", "WEB_VIEW_SSL_ERROR_MESSAGES");
    private static final Set<String> c = new HashSet(32);
    static final b d = a("sas", "AD_SOURCE");
    static final b e = a("srt", "AD_RENDER_TIME");
    static final b f = a("sft", "AD_FETCH_TIME");
    static final b g = a("sfs", "AD_FETCH_SIZE");
    static final b h = a("sadb", "AD_DOWNLOADED_BYTES");
    static final b i = a("sacb", "AD_CACHED_BYTES");
    static final b j = a("stdl", "TIME_TO_DISPLAY_FROM_LOAD");
    static final b k = a("stdi", "TIME_TO_DISPLAY_FROM_INIT");
    static final b l = a("snas", "AD_NUMBER_IN_SESSION");
    static final b m = a("snat", "AD_NUMBER_TOTAL");
    static final b n = a("stah", "TIME_AD_HIDDEN_FROM_SHOW");
    static final b o = a("stas", "TIME_TO_SKIP_FROM_SHOW");
    static final b p = a("stac", "TIME_TO_CLICK_FROM_SHOW");
    static final b q = a("stbe", "TIME_TO_EXPAND_FROM_SHOW");
    static final b r = a("stbc", "TIME_TO_CONTRACT_FROM_SHOW");
    static final b s = a("saan", "AD_SHOWN_WITH_ACTIVE_NETWORK");
    static final b t = a("suvs", "INTERSTITIAL_USED_VIDEO_STREAM");
    static final b u = a("sugs", "AD_USED_GRAPHIC_STREAM");
    static final b v = a("svpv", "INTERSTITIAL_VIDEO_PERCENT_VIEWED");
    static final b w = a("stpd", "INTERSTITIAL_PAUSED_DURATION");
    static final b x = a("shsc", "HTML_RESOURCE_CACHE_SUCCESS_COUNT");
    static final b y = a("shfc", "HTML_RESOURCE_CACHE_FAILURE_COUNT");
    static final b z = a("schc", "AD_CANCELLED_HTML_CACHING");

    /* renamed from: a  reason: collision with root package name */
    private final String f1803a;
    private final String b;

    static {
        a("sisw", "IS_STREAMING_WEBKIT");
        a("surw", "UNABLE_TO_RETRIEVE_WEBKIT_HTML_STRING");
        a("surp", "UNABLE_TO_PERSIST_WEBKIT_HTML_PLACEMENT_REPLACED_STRING");
        a("swhp", "SUCCESSFULLY_PERSISTED_WEBKIT_HTML_STRING");
        a("skr", "STOREKIT_REDIRECTED");
        a("sklf", "STOREKIT_LOAD_FAILURE");
        a("skps", "STOREKIT_PRELOAD_SKIPPED");
        a("wvruc", "WEB_VIEW_RENDERER_UNRESPONSIVE_COUNT");
    }

    private b(String str, String str2) {
        this.f1803a = str;
        this.b = str2;
    }

    private static b a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No key name specified");
        } else if (c.contains(str)) {
            throw new IllegalArgumentException("Key has already been used: " + str);
        } else if (!TextUtils.isEmpty(str2)) {
            c.add(str);
            return new b(str, str2);
        } else {
            throw new IllegalArgumentException("No debug name specified");
        }
    }

    public String a() {
        return this.f1803a;
    }

    public String b() {
        return this.b;
    }
}
