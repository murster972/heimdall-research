package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private final List<a> f1811a;
    private final Object b = new Object();
    private final l c;
    private final r d;

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        private final Long f1812a;
        private final String b;
        private final String c;
        private final String d;

        private a(String str, Throwable th) {
            this.b = str;
            this.f1812a = Long.valueOf(System.currentTimeMillis());
            String str2 = null;
            this.c = th != null ? th.getClass().getName() : null;
            this.d = th != null ? th.getMessage() : str2;
        }

        private a(JSONObject jSONObject) throws JSONException {
            this.b = jSONObject.getString("ms");
            this.f1812a = Long.valueOf(jSONObject.getLong("ts"));
            JSONObject optJSONObject = jSONObject.optJSONObject("ex");
            String str = null;
            this.c = optJSONObject != null ? optJSONObject.getString("nm") : null;
            this.d = optJSONObject != null ? optJSONObject.getString("rn") : str;
        }

        /* access modifiers changed from: private */
        public JSONObject a() throws JSONException {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("ms", this.b);
            jSONObject.put("ts", this.f1812a);
            if (!TextUtils.isEmpty(this.c)) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("nm", this.c);
                if (!TextUtils.isEmpty(this.d)) {
                    jSONObject2.put("rn", this.d);
                }
                jSONObject.put("ex", jSONObject2);
            }
            return jSONObject;
        }

        public String toString() {
            return "ErrorLog{timestampMillis=" + this.f1812a + ",message='" + this.b + '\'' + ",throwableName='" + this.c + '\'' + ",throwableReason='" + this.d + '\'' + '}';
        }
    }

    public f(l lVar) {
        this.c = lVar;
        this.d = lVar.j0();
        this.f1811a = new ArrayList();
    }

    private void d() {
        JSONArray jSONArray = new JSONArray();
        synchronized (this.b) {
            for (a next : this.f1811a) {
                try {
                    jSONArray.put(next.a());
                } catch (JSONException e) {
                    this.d.a("ErrorManager", false, "Failed to convert error log into json.", e);
                    this.f1811a.remove(next);
                }
            }
        }
        this.c.a(d.q, jSONArray.toString());
    }

    public JSONArray a() {
        JSONArray jSONArray;
        synchronized (this.b) {
            jSONArray = new JSONArray();
            for (a a2 : this.f1811a) {
                try {
                    jSONArray.put(a2.a());
                } catch (JSONException e) {
                    this.d.a("ErrorManager", false, "Failed to convert error log into json.", e);
                }
            }
        }
        return jSONArray;
    }

    public void a(String str, Throwable th) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.b) {
                if (this.f1811a.size() < ((Integer) this.c.a(b.i3)).intValue()) {
                    this.f1811a.add(new a(str, th));
                    d();
                }
            }
        }
    }

    public void b() {
        String str = (String) this.c.b(d.q, null);
        if (str != null) {
            synchronized (this.b) {
                try {
                    this.f1811a.clear();
                    JSONArray jSONArray = new JSONArray(str);
                    for (int i = 0; i < jSONArray.length(); i++) {
                        try {
                            this.f1811a.add(new a(jSONArray.getJSONObject(i)));
                        } catch (JSONException e) {
                            this.d.a("ErrorManager", false, "Failed to convert error json into a log.", e);
                        }
                    }
                } catch (JSONException e2) {
                    this.d.b("ErrorManager", "Unable to convert String to json.", e2);
                }
            }
        }
    }

    public void c() {
        synchronized (this.b) {
            this.f1811a.clear();
            this.c.b(d.q);
        }
    }
}
