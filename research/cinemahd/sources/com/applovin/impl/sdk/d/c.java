package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.t;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1804a;
    /* access modifiers changed from: private */
    public final r b;
    /* access modifiers changed from: private */
    public final Object c = new Object();
    /* access modifiers changed from: private */
    public final C0015c d = new C0015c();

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        private final l f1806a;
        private final JSONObject b;

        private a(String str, String str2, String str3, l lVar) {
            this.b = new JSONObject();
            this.f1806a = lVar;
            j.a(this.b, "pk", str, lVar);
            j.b(this.b, "ts", System.currentTimeMillis(), lVar);
            if (o.b(str2)) {
                j.a(this.b, "sk1", str2, lVar);
            }
            if (o.b(str3)) {
                j.a(this.b, "sk2", str3, lVar);
            }
        }

        /* access modifiers changed from: private */
        public String a() throws OutOfMemoryError {
            return this.b.toString();
        }

        /* access modifiers changed from: package-private */
        public void a(String str, long j) {
            b(str, j.a(this.b, str, 0, this.f1806a) + j);
        }

        /* access modifiers changed from: package-private */
        public void a(String str, String str2) {
            JSONArray b2 = j.b(this.b, str, new JSONArray(), this.f1806a);
            b2.put(str2);
            j.a(this.b, str, b2, this.f1806a);
        }

        /* access modifiers changed from: package-private */
        public void b(String str, long j) {
            j.b(this.b, str, j, this.f1806a);
        }

        public String toString() {
            return "AdEventStats{stats='" + this.b + '\'' + '}';
        }
    }

    public class b {

        /* renamed from: a  reason: collision with root package name */
        private final AppLovinAdBase f1807a;
        private final c b;

        public b(c cVar, AppLovinAdBase appLovinAdBase, c cVar2) {
            this.f1807a = appLovinAdBase;
            this.b = cVar2;
        }

        public b a(b bVar) {
            this.b.a(bVar, 1, this.f1807a);
            return this;
        }

        public b a(b bVar, long j) {
            this.b.b(bVar, j, this.f1807a);
            return this;
        }

        public b a(b bVar, String str) {
            this.b.a(bVar, str, this.f1807a);
            return this;
        }

        public void a() {
            this.b.e();
        }
    }

    /* renamed from: com.applovin.impl.sdk.d.c$c  reason: collision with other inner class name */
    private class C0015c extends LinkedHashMap<String, a> {
        private C0015c() {
        }

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, a> entry) {
            return size() > ((Integer) c.this.f1804a.a(com.applovin.impl.sdk.c.b.f3)).intValue();
        }
    }

    public c(l lVar) {
        this.f1804a = lVar;
        this.b = lVar.j0();
    }

    /* access modifiers changed from: private */
    public void a(b bVar, long j, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase != null && bVar != null && ((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.c3)).booleanValue()) {
            synchronized (this.c) {
                b(appLovinAdBase).a(((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.g3)).booleanValue() ? bVar.b() : bVar.a(), j);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase != null && bVar != null && ((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.c3)).booleanValue()) {
            synchronized (this.d) {
                b(appLovinAdBase).a(((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.g3)).booleanValue() ? bVar.b() : bVar.a(), str);
            }
        }
    }

    private void a(JSONObject jSONObject) {
        AnonymousClass1 r0 = new t<Object>(com.applovin.impl.sdk.network.b.a(this.f1804a).a(c()).c(d()).a(h.a(this.f1804a)).b("POST").a(jSONObject).d(((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.E3)).booleanValue()).b(((Integer) this.f1804a.a(com.applovin.impl.sdk.c.b.d3)).intValue()).a(((Integer) this.f1804a.a(com.applovin.impl.sdk.c.b.e3)).intValue()).a(), this.f1804a) {
            public void a(int i) {
                r a2 = c.this.b;
                a2.e("AdEventStatsManager", "Failed to submitted ad stats: " + i);
            }

            public void a(Object obj, int i) {
                r a2 = c.this.b;
                a2.b("AdEventStatsManager", "Ad stats submitted: " + i);
            }
        };
        r0.a(com.applovin.impl.sdk.c.b.Y);
        r0.b(com.applovin.impl.sdk.c.b.c0);
        this.f1804a.o().a((com.applovin.impl.sdk.e.a) r0, o.a.BACKGROUND);
    }

    private a b(AppLovinAdBase appLovinAdBase) {
        a aVar;
        synchronized (this.c) {
            String primaryKey = appLovinAdBase.getPrimaryKey();
            aVar = (a) this.d.get(primaryKey);
            if (aVar == null) {
                a aVar2 = new a(primaryKey, appLovinAdBase.getSecondaryKey1(), appLovinAdBase.getSecondaryKey2(), this.f1804a);
                this.d.put(primaryKey, aVar2);
                aVar = aVar2;
            }
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    public void b(b bVar, long j, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase != null && bVar != null && ((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.c3)).booleanValue()) {
            synchronized (this.c) {
                b(appLovinAdBase).b(((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.g3)).booleanValue() ? bVar.b() : bVar.a(), j);
            }
        }
    }

    private String c() {
        return h.a("2.0/s", this.f1804a);
    }

    private String d() {
        return h.b("2.0/s", this.f1804a);
    }

    /* access modifiers changed from: private */
    public void e() {
        if (((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.c3)).booleanValue()) {
            this.f1804a.o().b().execute(new Runnable() {
                public void run() {
                    HashSet hashSet;
                    synchronized (c.this.c) {
                        hashSet = new HashSet(c.this.d.size());
                        for (a aVar : c.this.d.values()) {
                            try {
                                hashSet.add(aVar.a());
                            } catch (OutOfMemoryError e) {
                                r a2 = c.this.b;
                                a2.b("AdEventStatsManager", "Failed to serialize " + aVar + " due to OOM error", e);
                                c.this.b();
                            }
                        }
                    }
                    c.this.f1804a.a(d.u, hashSet);
                }
            });
        }
    }

    public b a(AppLovinAdBase appLovinAdBase) {
        return new b(this, appLovinAdBase, this);
    }

    public void a() {
        if (((Boolean) this.f1804a.a(com.applovin.impl.sdk.c.b.c3)).booleanValue()) {
            Set<String> set = (Set) this.f1804a.b(d.u, new HashSet(0));
            this.f1804a.b(d.u);
            if (set == null || set.isEmpty()) {
                this.b.b("AdEventStatsManager", "No serialized ad events found");
                return;
            }
            r rVar = this.b;
            rVar.b("AdEventStatsManager", "De-serializing " + set.size() + " stat ad events");
            JSONArray jSONArray = new JSONArray();
            for (String str : set) {
                try {
                    jSONArray.put(new JSONObject(str));
                } catch (JSONException e) {
                    r rVar2 = this.b;
                    rVar2.b("AdEventStatsManager", "Failed to parse: " + str, e);
                }
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("stats", jSONArray);
                a(jSONObject);
            } catch (JSONException e2) {
                this.b.b("AdEventStatsManager", "Failed to create stats to submit", e2);
            }
        }
    }

    public void b() {
        synchronized (this.c) {
            this.b.b("AdEventStatsManager", "Clearing ad stats...");
            this.d.clear();
        }
    }
}
