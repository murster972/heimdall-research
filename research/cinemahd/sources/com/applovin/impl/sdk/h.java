package com.applovin.impl.sdk;

import android.os.Bundle;
import com.applovin.communicator.AppLovinCommunicator;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorPublisher;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.communicator.CommunicatorMessageImpl;
import com.applovin.impl.communicator.c;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.utils.BundleUtils;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONObject;

public class h implements AppLovinCommunicatorPublisher, AppLovinCommunicatorSubscriber {

    /* renamed from: a  reason: collision with root package name */
    private final l f1835a;
    private final AppLovinCommunicator b;

    h(l lVar) {
        this.f1835a = lVar;
        this.b = AppLovinCommunicator.getInstance(lVar.i());
        if (!lVar.P()) {
            this.b.a(lVar);
            this.b.subscribe((AppLovinCommunicatorSubscriber) this, c.f1597a);
        }
    }

    public void a(Bundle bundle, String str) {
        if (!this.f1835a.P()) {
            if (!"log".equals(str)) {
                r j0 = this.f1835a.j0();
                j0.b("CommunicatorService", "Sending message " + bundle + " for topic: " + str + "...");
            }
            this.b.getMessagingService().publish(CommunicatorMessageImpl.create(bundle, str, this, this.f1835a.b(b.V3).contains(str)));
        }
    }

    public void a(a aVar, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("type", str);
        bundle.putString("id", aVar.o());
        bundle.putString("network_name", aVar.d());
        bundle.putString("max_ad_unit_id", aVar.getAdUnitId());
        bundle.putString("third_party_ad_placement_id", aVar.s());
        bundle.putString("ad_format", aVar.getFormat().getLabel());
        if (o.b(aVar.getCreativeId())) {
            bundle.putString("creative_id", aVar.getCreativeId());
        }
        a(bundle, "max_ad_events");
    }

    public void a(MaxAdapter.InitializationStatus initializationStatus, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("adapter_class", str);
        bundle.putInt("init_status", initializationStatus.getCode());
        a(bundle, "adapter_initialization_status");
    }

    public void a(String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putString("value", str);
        a(bundle, str2);
    }

    public void a(String str, String str2, int i, Object obj) {
        Bundle bundle = new Bundle();
        bundle.putString("id", str);
        bundle.putString(ReportDBAdapter.ReportColumns.COLUMN_URL, str2);
        bundle.putInt("code", i);
        bundle.putBundle("body", j.a(obj));
        a(bundle, "receive_http_response");
    }

    public void a(JSONObject jSONObject, boolean z) {
        Bundle c = j.c(j.b(j.b(jSONObject, "communicator_settings", new JSONObject(), this.f1835a), "safedk_settings", new JSONObject(), this.f1835a));
        ArrayList<Bundle> a2 = j.a(com.applovin.impl.mediation.c.c.a(this.f1835a));
        Bundle bundle = new Bundle();
        bundle.putString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1835a.h0());
        bundle.putString("applovin_random_token", this.f1835a.X());
        bundle.putString("device_type", AppLovinSdkUtils.isTablet(this.f1835a.i()) ? "tablet" : "phone");
        bundle.putString("init_success", String.valueOf(z));
        bundle.putBundle("settings", c);
        bundle.putParcelableArrayList("installed_mediation_adapters", a2);
        bundle.putBoolean("debug_mode", ((Boolean) this.f1835a.a(b.u3)).booleanValue());
        a(bundle, "safedk_init");
    }

    public String getCommunicatorId() {
        return "applovin_sdk";
    }

    public void onMessageReceived(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        if ("send_http_request".equalsIgnoreCase(appLovinCommunicatorMessage.getTopic())) {
            Bundle messageData = appLovinCommunicatorMessage.getMessageData();
            Map<String, String> a2 = j.a(messageData.getBundle("query_params"));
            Map<String, Object> map = BundleUtils.toMap(messageData.getBundle("post_body"));
            Map<String, String> a3 = j.a(messageData.getBundle("headers"));
            String string = messageData.getString("id", "");
            if (!map.containsKey(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY)) {
                map.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1835a.h0());
            }
            this.f1835a.q().a(new f.a().c(messageData.getString(ReportDBAdapter.ReportColumns.COLUMN_URL)).d(messageData.getString("backup_url")).a(a2).c(map).b(a3).a(((Boolean) this.f1835a.a(b.u3)).booleanValue()).a(string).a());
        }
    }
}
