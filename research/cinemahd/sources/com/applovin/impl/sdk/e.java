package com.applovin.impl.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.utils.p;
import java.lang.ref.WeakReference;
import java.util.Map;

public class e implements AppLovinBroadcastManager.Receiver {

    /* renamed from: a  reason: collision with root package name */
    private p f1815a;
    private final Object b = new Object();
    private final l c;
    /* access modifiers changed from: private */
    public final WeakReference<a> d;
    private long e;

    public interface a {
        void onAdRefresh();
    }

    public e(l lVar, a aVar) {
        this.d = new WeakReference<>(aVar);
        this.c = lVar;
    }

    /* access modifiers changed from: private */
    public void h() {
        synchronized (this.b) {
            this.f1815a = null;
            if (!((Boolean) this.c.a(com.applovin.impl.sdk.c.a.w4)).booleanValue()) {
                this.c.I().unregisterReceiver(this);
            }
        }
    }

    private void i() {
        if (((Boolean) this.c.a(com.applovin.impl.sdk.c.a.v4)).booleanValue()) {
            d();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void j() {
        /*
            r4 = this;
            com.applovin.impl.sdk.l r0 = r4.c
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.a.v4
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x003c
            java.lang.Object r0 = r4.b
            monitor-enter(r0)
            com.applovin.impl.sdk.l r1 = r4.c     // Catch:{ all -> 0x0039 }
            com.applovin.impl.sdk.v r1 = r1.B()     // Catch:{ all -> 0x0039 }
            boolean r1 = r1.a()     // Catch:{ all -> 0x0039 }
            if (r1 == 0) goto L_0x002e
            com.applovin.impl.sdk.l r1 = r4.c     // Catch:{ all -> 0x0039 }
            com.applovin.impl.sdk.r r1 = r1.j0()     // Catch:{ all -> 0x0039 }
            java.lang.String r2 = "AdRefreshManager"
            java.lang.String r3 = "Waiting for the application to enter foreground to resume the timer."
            r1.b(r2, r3)     // Catch:{ all -> 0x0039 }
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            return
        L_0x002e:
            com.applovin.impl.sdk.utils.p r1 = r4.f1815a     // Catch:{ all -> 0x0039 }
            if (r1 == 0) goto L_0x0037
            com.applovin.impl.sdk.utils.p r1 = r4.f1815a     // Catch:{ all -> 0x0039 }
            r1.c()     // Catch:{ all -> 0x0039 }
        L_0x0037:
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            goto L_0x003c
        L_0x0039:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            throw r1
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.e.j():void");
    }

    public void a(long j) {
        synchronized (this.b) {
            c();
            this.e = j;
            this.f1815a = p.a(j, this.c, new Runnable() {
                public void run() {
                    e.this.h();
                    a aVar = (a) e.this.d.get();
                    if (aVar != null) {
                        aVar.onAdRefresh();
                    }
                }
            });
            if (!((Boolean) this.c.a(com.applovin.impl.sdk.c.a.w4)).booleanValue()) {
                this.c.I().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
                this.c.I().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
                this.c.I().registerReceiver(this, new IntentFilter("com.applovin.fullscreen_ad_displayed"));
                this.c.I().registerReceiver(this, new IntentFilter("com.applovin.fullscreen_ad_hidden"));
            }
            if (((Boolean) this.c.a(com.applovin.impl.sdk.c.a.v4)).booleanValue() && (this.c.C().b() || this.c.B().a())) {
                this.f1815a.b();
            }
        }
    }

    public boolean a() {
        boolean z;
        synchronized (this.b) {
            z = this.f1815a != null;
        }
        return z;
    }

    public long b() {
        long a2;
        synchronized (this.b) {
            a2 = this.f1815a != null ? this.f1815a.a() : -1;
        }
        return a2;
    }

    public void c() {
        synchronized (this.b) {
            if (this.f1815a != null) {
                this.f1815a.d();
                h();
            }
        }
    }

    public void d() {
        synchronized (this.b) {
            if (this.f1815a != null) {
                this.f1815a.b();
            }
        }
    }

    public void e() {
        synchronized (this.b) {
            if (this.f1815a != null) {
                this.f1815a.c();
            }
        }
    }

    public void f() {
        if (((Boolean) this.c.a(com.applovin.impl.sdk.c.a.u4)).booleanValue()) {
            d();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005e, code lost:
        if (r2 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0060, code lost:
        r0 = (com.applovin.impl.sdk.e.a) r9.d.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0068, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006a, code lost:
        r0.onAdRefresh();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r9 = this;
            com.applovin.impl.sdk.l r0 = r9.c
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.a.u4
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0071
            java.lang.Object r0 = r9.b
            monitor-enter(r0)
            com.applovin.impl.sdk.l r1 = r9.c     // Catch:{ all -> 0x006e }
            com.applovin.impl.sdk.q r1 = r1.C()     // Catch:{ all -> 0x006e }
            boolean r1 = r1.b()     // Catch:{ all -> 0x006e }
            if (r1 == 0) goto L_0x002e
            com.applovin.impl.sdk.l r1 = r9.c     // Catch:{ all -> 0x006e }
            com.applovin.impl.sdk.r r1 = r1.j0()     // Catch:{ all -> 0x006e }
            java.lang.String r2 = "AdRefreshManager"
            java.lang.String r3 = "Waiting for the full screen ad to be dismissed to resume the timer."
            r1.b(r2, r3)     // Catch:{ all -> 0x006e }
            monitor-exit(r0)     // Catch:{ all -> 0x006e }
            return
        L_0x002e:
            com.applovin.impl.sdk.utils.p r1 = r9.f1815a     // Catch:{ all -> 0x006e }
            r2 = 0
            if (r1 == 0) goto L_0x005d
            long r3 = r9.e     // Catch:{ all -> 0x006e }
            long r5 = r9.b()     // Catch:{ all -> 0x006e }
            long r3 = r3 - r5
            com.applovin.impl.sdk.l r1 = r9.c     // Catch:{ all -> 0x006e }
            com.applovin.impl.sdk.c.b<java.lang.Long> r5 = com.applovin.impl.sdk.c.a.t4     // Catch:{ all -> 0x006e }
            java.lang.Object r1 = r1.a(r5)     // Catch:{ all -> 0x006e }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ all -> 0x006e }
            long r5 = r1.longValue()     // Catch:{ all -> 0x006e }
            r7 = 0
            int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r1 < 0) goto L_0x0058
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 <= 0) goto L_0x0058
            r9.c()     // Catch:{ all -> 0x006e }
            r1 = 1
            r2 = 1
            goto L_0x005d
        L_0x0058:
            com.applovin.impl.sdk.utils.p r1 = r9.f1815a     // Catch:{ all -> 0x006e }
            r1.c()     // Catch:{ all -> 0x006e }
        L_0x005d:
            monitor-exit(r0)     // Catch:{ all -> 0x006e }
            if (r2 == 0) goto L_0x0071
            java.lang.ref.WeakReference<com.applovin.impl.sdk.e$a> r0 = r9.d
            java.lang.Object r0 = r0.get()
            com.applovin.impl.sdk.e$a r0 = (com.applovin.impl.sdk.e.a) r0
            if (r0 == 0) goto L_0x0071
            r0.onAdRefresh()
            goto L_0x0071
        L_0x006e:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x006e }
            throw r1
        L_0x0071:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.e.g():void");
    }

    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            f();
        } else if ("com.applovin.application_resumed".equals(action)) {
            g();
        } else if ("com.applovin.fullscreen_ad_displayed".equals(action)) {
            i();
        } else if ("com.applovin.fullscreen_ad_hidden".equals(action)) {
            j();
        }
    }
}
