package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

class j implements AppLovinBroadcastManager.Receiver {
    /* access modifiers changed from: private */
    public static AlertDialog c;
    /* access modifiers changed from: private */
    public static final AtomicBoolean d = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f1838a;
    private p b;

    public interface a {
        void a();

        void b();
    }

    j(k kVar, l lVar) {
        this.f1838a = kVar;
        lVar.I().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        lVar.I().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a(long j, final l lVar, final a aVar) {
        if (j > 0) {
            AlertDialog alertDialog = c;
            if (alertDialog == null || !alertDialog.isShowing()) {
                if (d.getAndSet(true)) {
                    if (j < this.b.a()) {
                        r j0 = lVar.j0();
                        j0.b("ConsentAlertManager", "Scheduling consent alert earlier (" + j + "ms) than remaining scheduled time (" + this.b.a() + "ms)");
                        this.b.d();
                    } else {
                        r j02 = lVar.j0();
                        j02.d("ConsentAlertManager", "Skip scheduling consent alert - one scheduled already with remaining time of " + this.b.a() + " milliseconds");
                        return;
                    }
                }
                r j03 = lVar.j0();
                j03.b("ConsentAlertManager", "Scheduling consent alert for " + j + " milliseconds");
                this.b = p.a(j, lVar, new Runnable() {
                    public void run() {
                        String str;
                        r rVar;
                        if (j.this.f1838a.c()) {
                            lVar.j0().e("ConsentAlertManager", "Consent dialog already showing, skip showing of consent alert");
                            return;
                        }
                        Activity a2 = lVar.D().a();
                        if (a2 == null || !h.a(lVar.i())) {
                            if (a2 == null) {
                                rVar = lVar.j0();
                                str = "No parent Activity found - rescheduling consent alert...";
                            } else {
                                rVar = lVar.j0();
                                str = "No internet available - rescheduling consent alert...";
                            }
                            rVar.e("ConsentAlertManager", str);
                            j.d.set(false);
                            j.this.a(((Long) lVar.a(b.K)).longValue(), lVar, aVar);
                            return;
                        }
                        AppLovinSdkUtils.runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog unused = j.c = new AlertDialog.Builder(lVar.D().a()).setTitle((CharSequence) lVar.a(b.L)).setMessage((CharSequence) lVar.a(b.M)).setCancelable(false).setPositiveButton((CharSequence) lVar.a(b.N), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.a();
                                        dialogInterface.dismiss();
                                        j.d.set(false);
                                    }
                                }).setNegativeButton((CharSequence) lVar.a(b.O), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.b();
                                        dialogInterface.dismiss();
                                        j.d.set(false);
                                        long longValue = ((Long) lVar.a(b.J)).longValue();
                                        AnonymousClass1 r0 = AnonymousClass1.this;
                                        j.this.a(longValue, lVar, aVar);
                                    }
                                }).create();
                                j.c.show();
                            }
                        });
                    }
                });
            }
        }
    }

    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
        if (this.b != null) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                this.b.b();
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.b.c();
            }
        }
    }
}
