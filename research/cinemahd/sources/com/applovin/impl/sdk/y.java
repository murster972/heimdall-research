package com.applovin.impl.sdk;

import android.content.Context;
import android.content.Intent;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.c.e;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.utils.g;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class y {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static WebView f1938a;
    /* access modifiers changed from: private */
    public static volatile String b;
    /* access modifiers changed from: private */
    public static volatile Map<String, String> c;

    private static class a extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        private final l f1942a;

        private a(l lVar) {
            this.f1942a = lVar;
        }

        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            this.f1942a.I().sendBroadcast(new Intent("com.applovin.render_process_gone"), (Map<String, Object>) null);
            return true;
        }
    }

    public static String a() {
        return b;
    }

    public static Map<String, String> a(long j, final l lVar) {
        if (c != null || j <= 0) {
            return b();
        }
        if (g.d()) {
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            AppLovinSdkUtils.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        y.a(lVar);
                        y.f1938a.setWebViewClient(new a(lVar) {
                            public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                                HashMap hashMap = new HashMap();
                                for (Map.Entry next : webResourceRequest.getRequestHeaders().entrySet()) {
                                    hashMap.put("AppLovin-WebView-" + ((String) next.getKey()), next.getValue());
                                }
                                Map unused = y.c = hashMap;
                                countDownLatch.countDown();
                                return super.shouldInterceptRequest(webView, webResourceRequest);
                            }
                        });
                        y.f1938a.loadUrl("https://blank");
                    } catch (Throwable th) {
                        lVar.j0().b("WebViewDataCollector", "Failed to collect WebView HTTP headers", th);
                    }
                }
            });
            try {
                countDownLatch.await(j, TimeUnit.MILLISECONDS);
            } catch (Throwable unused) {
            }
        }
        return b();
    }

    public static void a(l lVar) {
        if (f1938a == null) {
            try {
                f1938a = new WebView(lVar.i());
                f1938a.setWebViewClient(new a(lVar));
            } catch (Throwable th) {
                lVar.j0().b("WebViewDataCollector", "Failed to initialize WebView for data collection.", th);
            }
        }
    }

    public static Map<String, String> b() {
        return c != null ? c : Collections.emptyMap();
    }

    public static void b(final l lVar) {
        if (b == null) {
            final Context i = lVar.i();
            b = (String) e.b(d.e, "", i);
            if (g.b()) {
                lVar.o().a((com.applovin.impl.sdk.e.a) new com.applovin.impl.sdk.e.y(lVar, true, new Runnable() {
                    public void run() {
                        try {
                            String unused = y.b = WebSettings.getDefaultUserAgent(i);
                            e.a(d.e, y.b, i);
                        } catch (Throwable th) {
                            lVar.j0().b("WebViewDataCollector", "Failed to collect user agent", th);
                        }
                    }
                }), o.a.BACKGROUND);
            } else {
                AppLovinSdkUtils.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            y.a(lVar);
                            String unused = y.b = y.f1938a.getSettings().getUserAgentString();
                            e.a(d.e, y.b, i);
                        } catch (Throwable th) {
                            lVar.j0().b("WebViewDataCollector", "Failed to collect user agent", th);
                        }
                    }
                });
            }
        }
    }
}
