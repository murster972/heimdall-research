package com.applovin.impl.sdk;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private final String f1877a = UUID.randomUUID().toString();
    private final String b;
    private final Map<String, Object> c;
    private final long d;

    public o(String str, Map<String, String> map, Map<String, Object> map2) {
        this.b = str;
        this.c = new HashMap();
        this.c.putAll(map);
        this.c.put("applovin_sdk_super_properties", map2);
        this.d = System.currentTimeMillis();
    }

    public String a() {
        return this.b;
    }

    public Map<String, Object> b() {
        return this.c;
    }

    public long c() {
        return this.d;
    }

    public String d() {
        return this.f1877a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || o.class != obj.getClass()) {
            return false;
        }
        o oVar = (o) obj;
        if (this.d != oVar.d) {
            return false;
        }
        String str = this.b;
        if (str == null ? oVar.b != null : !str.equals(oVar.b)) {
            return false;
        }
        Map<String, Object> map = this.c;
        if (map == null ? oVar.c != null : !map.equals(oVar.c)) {
            return false;
        }
        String str2 = this.f1877a;
        String str3 = oVar.f1877a;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
    }

    public int hashCode() {
        String str = this.b;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, Object> map = this.c;
        int hashCode2 = map != null ? map.hashCode() : 0;
        long j = this.d;
        int i2 = (((hashCode + hashCode2) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str2 = this.f1877a;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        return "Event{name='" + this.b + '\'' + ", id='" + this.f1877a + '\'' + ", creationTimestampMillis=" + this.d + ", parameters=" + this.c + '}';
    }
}
