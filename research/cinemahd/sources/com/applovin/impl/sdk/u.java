package com.applovin.impl.sdk;

import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.utils.j;
import org.json.JSONObject;

public class u {

    /* renamed from: a  reason: collision with root package name */
    private final l f1886a;
    private final JSONObject b;
    private final Object c = new Object();

    public u(l lVar) {
        this.f1886a = lVar;
        this.b = j.a((String) lVar.b(d.t, "{}"), new JSONObject(), lVar);
    }

    public Integer a(String str) {
        Integer valueOf;
        synchronized (this.c) {
            if (this.b.has(str)) {
                j.a(this.b, str, j.b(this.b, str, 0, this.f1886a) + 1, this.f1886a);
            } else {
                j.a(this.b, str, 1, this.f1886a);
            }
            this.f1886a.a(d.t, this.b.toString());
            valueOf = Integer.valueOf(j.b(this.b, str, 0, this.f1886a));
        }
        return valueOf;
    }
}
