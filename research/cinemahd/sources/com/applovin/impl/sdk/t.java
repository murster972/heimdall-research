package com.applovin.impl.sdk;

import java.util.LinkedList;
import java.util.Queue;

class t {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<AppLovinAdBase> f1885a = new LinkedList();
    private final Object b = new Object();

    t() {
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int size;
        synchronized (this.b) {
            size = this.f1885a.size();
        }
        return size;
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAdBase appLovinAdBase) {
        synchronized (this.b) {
            if (a() <= 25) {
                this.f1885a.offer(appLovinAdBase);
            } else {
                r.i("AppLovinSdk", "Maximum queue capacity reached - discarding ad...");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        boolean z;
        synchronized (this.b) {
            z = a() == 0;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public AppLovinAdBase c() {
        AppLovinAdBase poll;
        synchronized (this.b) {
            poll = !b() ? this.f1885a.poll() : null;
        }
        return poll;
    }

    /* access modifiers changed from: package-private */
    public AppLovinAdBase d() {
        AppLovinAdBase peek;
        synchronized (this.b) {
            peek = this.f1885a.peek();
        }
        return peek;
    }
}
