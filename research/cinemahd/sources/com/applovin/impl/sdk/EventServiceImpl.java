package com.applovin.impl.sdk;

import android.content.Intent;
import android.text.TextUtils;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinEventParameters;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinEventTypes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class EventServiceImpl implements AppLovinEventService {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1750a;
    private final Map<String, Object> b;
    private final AtomicBoolean c = new AtomicBoolean();

    public EventServiceImpl(l lVar) {
        this.f1750a = lVar;
        if (((Boolean) lVar.a(b.l0)).booleanValue()) {
            this.b = j.a((String) this.f1750a.b(d.s, "{}"), (Map<String, Object>) new HashMap(), this.f1750a);
            return;
        }
        this.b = new HashMap();
        lVar.a(d.s, "{}");
    }

    /* access modifiers changed from: private */
    public String a() {
        return ((String) this.f1750a.a(b.d0)) + "4.0/pix";
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(o oVar, Map<String, String> map) {
        HashMap hashMap = new HashMap();
        if (map != null) {
            hashMap.putAll(map);
        }
        boolean contains = this.f1750a.b(b.j0).contains(oVar.a());
        hashMap.put("AppLovin-Event", contains ? oVar.a() : "postinstall");
        if (!contains) {
            hashMap.put("AppLovin-Sub-Event", oVar.a());
        }
        return hashMap;
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(o oVar, boolean z) {
        boolean contains = this.f1750a.b(b.j0).contains(oVar.a());
        Map<String, Object> a2 = this.f1750a.r().a((Map<String, String>) null, z, false);
        a2.put("event", contains ? oVar.a() : "postinstall");
        a2.put("event_id", oVar.d());
        a2.put("ts", Long.toString(oVar.c()));
        if (!contains) {
            a2.put("sub_event", oVar.a());
        }
        return r.b(a2);
    }

    /* access modifiers changed from: private */
    public String b() {
        return ((String) this.f1750a.a(b.e0)) + "4.0/pix";
    }

    private void c() {
        if (((Boolean) this.f1750a.a(b.l0)).booleanValue()) {
            this.f1750a.a(d.s, j.a(this.b, "{}", this.f1750a));
        }
    }

    public Map<String, Object> getSuperProperties() {
        return new HashMap(this.b);
    }

    public void maybeTrackAppOpenEvent() {
        if (this.c.compareAndSet(false, true)) {
            this.f1750a.e0().trackEvent("landing");
        }
    }

    public void setSuperProperty(Object obj, String str) {
        if (TextUtils.isEmpty(str)) {
            r.i("AppLovinEventService", "Super property key cannot be null or empty");
        } else if (obj == null) {
            this.b.remove(str);
            c();
        } else {
            List<String> b2 = this.f1750a.b(b.k0);
            if (!r.a(obj, b2, this.f1750a)) {
                r.i("AppLovinEventService", "Failed to set super property '" + obj + "' for key '" + str + "' - valid super property types include: " + b2);
                return;
            }
            this.b.put(str, r.a(obj, this.f1750a));
            c();
        }
    }

    public String toString() {
        return "EventService{}";
    }

    public void trackCheckout(String str, Map<String, String> map) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap(1);
        }
        hashMap.put(AppLovinEventParameters.CHECKOUT_TRANSACTION_IDENTIFIER, str);
        trackEvent(AppLovinEventTypes.USER_COMPLETED_CHECKOUT, hashMap);
    }

    public void trackEvent(String str) {
        trackEvent(str, new HashMap());
    }

    public void trackEvent(String str, Map<String, String> map) {
        trackEvent(str, map, (Map<String, String>) null);
    }

    public void trackEvent(String str, Map<String, String> map, final Map<String, String> map2) {
        r j0 = this.f1750a.j0();
        j0.b("AppLovinEventService", "Tracking event: \"" + str + "\" with parameters: " + map);
        final o oVar = new o(str, map, this.b);
        try {
            this.f1750a.o().a((a) new y(this.f1750a, new Runnable() {
                public void run() {
                    EventServiceImpl.this.f1750a.q().a(f.o().c(EventServiceImpl.this.a()).d(EventServiceImpl.this.b()).a((Map<String, String>) EventServiceImpl.this.a(oVar, false)).b((Map<String, String>) EventServiceImpl.this.a(oVar, (Map<String, String>) map2)).c(oVar.b()).b(((Boolean) EventServiceImpl.this.f1750a.a(b.D3)).booleanValue()).a(((Boolean) EventServiceImpl.this.f1750a.a(b.u3)).booleanValue()).a());
                }
            }), o.a.BACKGROUND);
        } catch (Throwable th) {
            r j02 = this.f1750a.j0();
            j02.b("AppLovinEventService", "Unable to track event: " + oVar, th);
        }
    }

    public void trackEventSynchronously(String str) {
        r j0 = this.f1750a.j0();
        j0.b("AppLovinEventService", "Tracking event: \"" + str + "\" synchronously");
        o oVar = new o(str, new HashMap(), this.b);
        this.f1750a.q().a(f.o().c(a()).d(b()).a(a(oVar, true)).b(a(oVar, (Map<String, String>) null)).c(oVar.b()).b(((Boolean) this.f1750a.a(b.D3)).booleanValue()).a(((Boolean) this.f1750a.a(b.u3)).booleanValue()).a());
    }

    public void trackInAppPurchase(Intent intent, Map<String, String> map) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap();
        }
        try {
            hashMap.put(AppLovinEventParameters.IN_APP_PURCHASE_DATA, intent.getStringExtra("INAPP_PURCHASE_DATA"));
            hashMap.put(AppLovinEventParameters.IN_APP_DATA_SIGNATURE, intent.getStringExtra("INAPP_DATA_SIGNATURE"));
        } catch (Throwable th) {
            r.c("AppLovinEventService", "Unable to track in app purchase - invalid purchase intent", th);
        }
        trackEvent("iap", hashMap);
    }
}
