package com.applovin.impl.mediation;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.d;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final l f1636a;
    /* access modifiers changed from: private */
    public final r b;
    /* access modifiers changed from: private */
    public final a c;
    private d d;

    public interface a {
        void a(com.applovin.impl.mediation.a.c cVar);
    }

    c(l lVar, a aVar) {
        this.f1636a = lVar;
        this.b = lVar.j0();
        this.c = aVar;
    }

    public void a() {
        this.b.b("AdHiddenCallbackTimeoutManager", "Cancelling timeout");
        d dVar = this.d;
        if (dVar != null) {
            dVar.a();
            this.d = null;
        }
    }

    public void a(final com.applovin.impl.mediation.a.c cVar, long j) {
        r rVar = this.b;
        rVar.b("AdHiddenCallbackTimeoutManager", "Scheduling in " + j + "ms...");
        this.d = d.a(j, this.f1636a, new Runnable() {
            public void run() {
                c.this.b.b("AdHiddenCallbackTimeoutManager", "Timing out...");
                c.this.c.a(cVar);
            }
        });
    }
}
