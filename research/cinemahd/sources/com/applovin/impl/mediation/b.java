package com.applovin.impl.mediation;

import com.applovin.impl.mediation.a;
import com.applovin.impl.mediation.c;
import com.applovin.impl.sdk.l;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdkUtils;

public class b implements a.C0010a, c.a {

    /* renamed from: a  reason: collision with root package name */
    private final a f1626a;
    private final c b;
    /* access modifiers changed from: private */
    public final MaxAdListener c;

    public b(l lVar, MaxAdListener maxAdListener) {
        this.c = maxAdListener;
        this.f1626a = new a(lVar);
        this.b = new c(lVar, this);
    }

    public void a(com.applovin.impl.mediation.a.c cVar) {
        this.c.onAdHidden(cVar);
    }

    public void a(MaxAd maxAd) {
        this.b.a();
        this.f1626a.a();
    }

    public void b(final com.applovin.impl.mediation.a.c cVar) {
        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                b.this.c.onAdHidden(cVar);
            }
        }, cVar.F());
    }

    public void c(com.applovin.impl.mediation.a.c cVar) {
        long D = cVar.D();
        if (D >= 0) {
            this.b.a(cVar, D);
        }
        if (cVar.E()) {
            this.f1626a.a(cVar, this);
        }
    }
}
