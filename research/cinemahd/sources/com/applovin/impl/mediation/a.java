package com.applovin.impl.mediation;

import android.app.Activity;
import android.os.Bundle;
import com.applovin.impl.mediation.a.c;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;

public class a extends com.applovin.impl.sdk.utils.a {

    /* renamed from: a  reason: collision with root package name */
    private final com.applovin.impl.sdk.a f1605a;
    private final r b;
    private C0010a c;
    private c d;
    private int e;
    private boolean f;

    /* renamed from: com.applovin.impl.mediation.a$a  reason: collision with other inner class name */
    public interface C0010a {
        void b(c cVar);
    }

    a(l lVar) {
        this.b = lVar.j0();
        this.f1605a = lVar.D();
    }

    public void a() {
        this.b.b("AdActivityObserver", "Cancelling...");
        this.f1605a.b(this);
        this.c = null;
        this.d = null;
        this.e = 0;
        this.f = false;
    }

    public void a(c cVar, C0010a aVar) {
        r rVar = this.b;
        rVar.b("AdActivityObserver", "Starting for ad " + cVar.getAdUnitId() + "...");
        a();
        this.c = aVar;
        this.d = cVar;
        this.f1605a.a(this);
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (!this.f) {
            this.f = true;
        }
        this.e++;
        this.b.b("AdActivityObserver", "Created Activity: " + activity + ", counter is " + this.e);
    }

    public void onActivityDestroyed(Activity activity) {
        if (this.f) {
            this.e--;
            this.b.b("AdActivityObserver", "Destroyed Activity: " + activity + ", counter is " + this.e);
            if (this.e <= 0) {
                this.b.b("AdActivityObserver", "Last ad Activity destroyed");
                if (this.c != null) {
                    this.b.b("AdActivityObserver", "Invoking callback...");
                    this.c.b(this.d);
                }
                a();
            }
        }
    }
}
