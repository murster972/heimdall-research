package com.applovin.impl.mediation;

import android.app.Activity;
import com.applovin.impl.mediation.b.b;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.f;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.o;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;

public class g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1692a;
    private final Map<String, b> b = new HashMap(4);
    private final Object c = new Object();
    private final Map<String, com.applovin.impl.mediation.a.a> d = new HashMap(4);
    private final Object e = new Object();

    private static class a implements MaxAdListener {

        /* renamed from: a  reason: collision with root package name */
        private final l f1694a;
        /* access modifiers changed from: private */
        public final Activity b;
        /* access modifiers changed from: private */
        public final g c;
        /* access modifiers changed from: private */
        public final b d;
        /* access modifiers changed from: private */
        public final MaxAdFormat e;
        /* access modifiers changed from: private */
        public f f;

        private a(f fVar, b bVar, MaxAdFormat maxAdFormat, g gVar, l lVar, Activity activity) {
            this.f1694a = lVar;
            this.b = activity;
            this.c = gVar;
            this.d = bVar;
            this.e = maxAdFormat;
            this.f = fVar;
        }

        public void onAdClicked(MaxAd maxAd) {
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
        }

        public void onAdDisplayed(MaxAd maxAd) {
        }

        public void onAdHidden(MaxAd maxAd) {
        }

        public void onAdLoadFailed(final String str, int i) {
            if (!this.f1694a.a(com.applovin.impl.sdk.c.a.S4, this.e) || this.d.b >= ((Integer) this.f1694a.a(com.applovin.impl.sdk.c.a.R4)).intValue()) {
                int unused = this.d.b = 0;
                this.d.f1696a.set(false);
                if (this.d.c != null) {
                    this.d.c.onAdLoadFailed(str, i);
                    MaxAdListener unused2 = this.d.c = null;
                    return;
                }
                return;
            }
            b.d(this.d);
            final int pow = (int) Math.pow(2.0d, (double) this.d.b);
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    a aVar = a.this;
                    f unused = aVar.f = new f.a(aVar.f).a("retry_delay_sec", String.valueOf(pow)).a("retry_attempt", String.valueOf(a.this.d.b)).a();
                    a.this.c.b(str, a.this.e, a.this.f, a.this.b, a.this);
                }
            }, TimeUnit.SECONDS.toMillis((long) pow));
        }

        public void onAdLoaded(MaxAd maxAd) {
            com.applovin.impl.mediation.a.a aVar = (com.applovin.impl.mediation.a.a) maxAd;
            int unused = this.d.b = 0;
            if (this.d.c != null) {
                aVar.p().c().a(this.d.c);
                this.d.c.onAdLoaded(aVar);
                MaxAdListener unused2 = this.d.c = null;
                if ((this.f1694a.b(com.applovin.impl.sdk.c.a.Q4).contains(maxAd.getAdUnitId()) || this.f1694a.a(com.applovin.impl.sdk.c.a.P4, maxAd.getFormat())) && !this.f1694a.g().a() && !o.b(this.f1694a.g().c())) {
                    this.c.b(maxAd.getAdUnitId(), maxAd.getFormat(), this.f, this.b, this);
                    return;
                }
            } else {
                this.c.a(aVar);
            }
            this.d.f1696a.set(false);
        }
    }

    private static class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final AtomicBoolean f1696a;
        /* access modifiers changed from: private */
        public int b;
        /* access modifiers changed from: private */
        public volatile MaxAdListener c;

        private b() {
            this.f1696a = new AtomicBoolean();
        }

        static /* synthetic */ int d(b bVar) {
            int i = bVar.b;
            bVar.b = i + 1;
            return i;
        }
    }

    public g(l lVar) {
        this.f1692a = lVar;
    }

    private com.applovin.impl.mediation.a.a a(String str) {
        com.applovin.impl.mediation.a.a aVar;
        synchronized (this.e) {
            aVar = this.d.get(str);
            this.d.remove(str);
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.a.a aVar) {
        synchronized (this.e) {
            if (this.d.containsKey(aVar.getAdUnitId())) {
                r.i("AppLovinSdk", "Ad in cache already: " + aVar.getAdUnitId());
            }
            this.d.put(aVar.getAdUnitId(), aVar);
        }
    }

    private b b(String str) {
        b bVar;
        synchronized (this.c) {
            bVar = this.b.get(str);
            if (bVar == null) {
                bVar = new b();
                this.b.put(str, bVar);
            }
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    public void b(String str, MaxAdFormat maxAdFormat, f fVar, Activity activity, MaxAdListener maxAdListener) {
        final String str2 = str;
        final MaxAdFormat maxAdFormat2 = maxAdFormat;
        final f fVar2 = fVar;
        final Activity activity2 = activity;
        final MaxAdListener maxAdListener2 = maxAdListener;
        this.f1692a.o().a((com.applovin.impl.sdk.e.a) new com.applovin.impl.mediation.b.b(maxAdFormat, activity, this.f1692a, new b.a() {
            public void a(JSONArray jSONArray) {
                g.this.f1692a.o().a((com.applovin.impl.sdk.e.a) new c(str2, maxAdFormat2, fVar2, jSONArray, activity2, g.this.f1692a, maxAdListener2));
            }
        }), com.applovin.impl.mediation.c.c.a(maxAdFormat));
    }

    public void a(String str, MaxAdFormat maxAdFormat, f fVar, Activity activity, MaxAdListener maxAdListener) {
        MaxAdListener maxAdListener2 = maxAdListener;
        com.applovin.impl.mediation.a.a a2 = a(str);
        if (a2 != null) {
            a2.p().c().a(maxAdListener2);
            maxAdListener2.onAdLoaded(a2);
        }
        b b2 = b(str);
        if (b2.f1696a.compareAndSet(false, true)) {
            if (a2 == null) {
                MaxAdListener unused = b2.c = maxAdListener2;
            }
            b(str, maxAdFormat, fVar, activity, new a(fVar, b2, maxAdFormat, this, this.f1692a, activity));
            return;
        }
        if (!(b2.c == null || b2.c == maxAdListener2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Attempting to load ad for same ad unit id (");
            String str2 = str;
            sb.append(str);
            sb.append(") while another ad load is already in progress!");
            r.h("MaxInterstitialAd", sb.toString());
        }
        MaxAdListener unused2 = b2.c = maxAdListener2;
    }
}
