package com.applovin.impl.mediation;

import android.text.TextUtils;
import com.applovin.impl.mediation.a.e;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapters.MediationAdapterBase;
import com.applovin.sdk.AppLovinSdk;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private final l f1698a;
    private final r b;
    private final Object c = new Object();
    private final Map<String, Class<? extends MaxAdapter>> d = new HashMap();
    private final Set<String> e = new HashSet();

    public i(l lVar) {
        if (lVar != null) {
            this.f1698a = lVar;
            this.b = lVar.j0();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private j a(e eVar, Class<? extends MaxAdapter> cls) {
        try {
            j jVar = new j(eVar, (MediationAdapterBase) cls.getConstructor(new Class[]{AppLovinSdk.class}).newInstance(new Object[]{this.f1698a.v()}), this.f1698a);
            if (jVar.d()) {
                return jVar;
            }
            r.i("MediationAdapterManager", "Adapter is disabled after initialization: " + eVar);
            return null;
        } catch (Throwable th) {
            r.c("MediationAdapterManager", "Failed to load adapter: " + eVar, th);
            return null;
        }
    }

    private Class<? extends MaxAdapter> a(String str) {
        Class<MaxAdapter> cls = MaxAdapter.class;
        try {
            Class<?> cls2 = Class.forName(str);
            if (cls.isAssignableFrom(cls2)) {
                return cls2.asSubclass(cls);
            }
            r.i("MediationAdapterManager", str + " error: not an instance of '" + cls.getName() + "'.");
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public j a(e eVar) {
        Class<? extends MaxAdapter> cls;
        r rVar;
        String str;
        if (eVar != null) {
            String d2 = eVar.d();
            String c2 = eVar.c();
            if (TextUtils.isEmpty(d2)) {
                rVar = this.b;
                str = "No adapter name provided for " + c2 + ", not loading the adapter ";
            } else if (TextUtils.isEmpty(c2)) {
                rVar = this.b;
                str = "Unable to find default classname for '" + d2 + "'";
            } else {
                synchronized (this.c) {
                    if (!this.e.contains(c2)) {
                        if (this.d.containsKey(c2)) {
                            cls = this.d.get(c2);
                        } else {
                            cls = a(c2);
                            if (cls == null) {
                                this.e.add(c2);
                                return null;
                            }
                        }
                        j a2 = a(eVar, cls);
                        if (a2 != null) {
                            this.b.b("MediationAdapterManager", "Loaded " + d2);
                            this.d.put(c2, cls);
                            return a2;
                        }
                        this.b.e("MediationAdapterManager", "Failed to load " + d2);
                        this.e.add(c2);
                        return null;
                    }
                    this.b.b("MediationAdapterManager", "Not attempting to load " + d2 + " due to prior errors");
                    return null;
                }
            }
            rVar.e("MediationAdapterManager", str);
            return null;
        }
        throw new IllegalArgumentException("No adapter spec specified");
    }

    public Collection<String> a() {
        Set unmodifiableSet;
        synchronized (this.c) {
            HashSet hashSet = new HashSet(this.d.size());
            for (Class<? extends MaxAdapter> name : this.d.values()) {
                hashSet.add(name.getName());
            }
            unmodifiableSet = Collections.unmodifiableSet(hashSet);
        }
        return unmodifiableSet;
    }

    public Collection<String> b() {
        Set<T> unmodifiableSet;
        synchronized (this.c) {
            unmodifiableSet = Collections.unmodifiableSet(this.e);
        }
        return unmodifiableSet;
    }
}
