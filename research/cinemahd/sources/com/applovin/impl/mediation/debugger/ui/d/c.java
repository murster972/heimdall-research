package com.applovin.impl.mediation.debugger.ui.d;

import android.content.Context;
import android.text.SpannedString;
import android.text.TextUtils;
import com.applovin.impl.sdk.utils.f;
import com.applovin.sdk.R$color;
import com.applovin.sdk.R$drawable;
import com.applovin.sdk.R$layout;

public class c {

    /* renamed from: a  reason: collision with root package name */
    protected b f1682a;
    protected boolean b;
    protected SpannedString c;
    protected SpannedString d;
    protected String e;
    protected int f;
    protected int g;
    protected int h;
    protected int i;
    protected int j;
    protected int k;
    protected boolean l;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final b f1683a;
        boolean b;
        SpannedString c;
        SpannedString d;
        String e;
        int f = 0;
        int g = 0;
        int h = -16777216;
        int i = -16777216;
        int j = 0;
        int k = 0;
        boolean l;

        public a(b bVar) {
            this.f1683a = bVar;
        }

        public a a(int i2) {
            this.g = i2;
            return this;
        }

        public a a(Context context) {
            this.g = R$drawable.applovin_ic_disclosure_arrow;
            this.k = f.a(R$color.applovin_sdk_disclosureButtonColor, context);
            return this;
        }

        public a a(SpannedString spannedString) {
            this.c = spannedString;
            return this;
        }

        public a a(String str) {
            a(!TextUtils.isEmpty(str) ? new SpannedString(str) : null);
            return this;
        }

        public a a(boolean z) {
            this.b = z;
            return this;
        }

        public c a() {
            return new c(this);
        }

        public a b(int i2) {
            this.i = i2;
            return this;
        }

        public a b(SpannedString spannedString) {
            this.d = spannedString;
            return this;
        }

        public a b(String str) {
            b(!TextUtils.isEmpty(str) ? new SpannedString(str) : null);
            return this;
        }

        public a b(boolean z) {
            this.l = z;
            return this;
        }

        public a c(int i2) {
            this.k = i2;
            return this;
        }

        public a c(String str) {
            this.e = str;
            return this;
        }
    }

    public enum b {
        SECTION(0),
        SIMPLE(1),
        DETAIL(2),
        RIGHT_DETAIL(3),
        COUNT(4);
        
        private final int f;

        private b(int i) {
            this.f = i;
        }

        public int a() {
            return this.f;
        }

        public int b() {
            if (this == SECTION) {
                return R$layout.list_section;
            }
            if (this == SIMPLE) {
                return 17367043;
            }
            return this == DETAIL ? R$layout.list_item_detail : R$layout.list_item_right_detail;
        }
    }

    private c(a aVar) {
        this.f = 0;
        this.g = 0;
        this.h = -16777216;
        this.i = -16777216;
        this.j = 0;
        this.k = 0;
        this.f1682a = aVar.f1683a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.k;
        this.l = aVar.l;
    }

    protected c(b bVar) {
        this.f = 0;
        this.g = 0;
        this.h = -16777216;
        this.i = -16777216;
        this.j = 0;
        this.k = 0;
        this.f1682a = bVar;
    }

    public static a a(b bVar) {
        return new a(bVar);
    }

    public static int n() {
        return b.COUNT.a();
    }

    public static a o() {
        return a(b.RIGHT_DETAIL);
    }

    public SpannedString a() {
        return this.d;
    }

    public boolean b() {
        return this.b;
    }

    public boolean c() {
        return this.l;
    }

    public int d() {
        return this.i;
    }

    public int e() {
        return this.f;
    }

    public int f() {
        return this.g;
    }

    public int g() {
        return this.k;
    }

    public int h() {
        return this.f1682a.a();
    }

    public int i() {
        return this.f1682a.b();
    }

    public SpannedString j() {
        return this.c;
    }

    public String k() {
        return this.e;
    }

    public int l() {
        return this.h;
    }

    public int m() {
        return this.j;
    }
}
