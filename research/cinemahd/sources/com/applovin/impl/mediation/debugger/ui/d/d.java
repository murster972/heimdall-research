package com.applovin.impl.mediation.debugger.ui.d;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.applovin.sdk.R$id;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class d extends BaseAdapter implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final LayoutInflater f1685a;
    /* access modifiers changed from: protected */
    public final Context b;
    private final List<c> c = new ArrayList();
    private Map<Integer, Integer> d = new HashMap();
    private a e;

    public interface a {
        void a(a aVar, c cVar);
    }

    protected d(Context context) {
        this.b = context;
        this.f1685a = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    private a e(int i) {
        for (int i2 = 0; i2 < a(); i2++) {
            Integer num = this.d.get(Integer.valueOf(i2));
            if (num != null) {
                if (i <= num.intValue() + a(i2)) {
                    return new a(i2, i - (num.intValue() + 1));
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract int a(int i);

    public void a(a aVar) {
        this.e = aVar;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract c b(int i);

    /* access modifiers changed from: protected */
    public abstract List<c> c(int i);

    /* renamed from: d */
    public c getItem(int i) {
        return this.c.get(i);
    }

    public int getCount() {
        return this.c.size();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public int getItemViewType(int i) {
        return getItem(i).h();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        b bVar;
        c d2 = getItem(i);
        if (view == null) {
            view = this.f1685a.inflate(d2.i(), viewGroup, false);
            bVar = new b();
            bVar.f1681a = (TextView) view.findViewById(16908308);
            bVar.b = (TextView) view.findViewById(16908309);
            bVar.c = (ImageView) view.findViewById(R$id.imageView);
            bVar.d = (ImageView) view.findViewById(R$id.detailImageView);
            view.setTag(bVar);
            view.setOnClickListener(this);
        } else {
            bVar = (b) view.getTag();
        }
        bVar.a(i);
        bVar.a(d2);
        view.setEnabled(d2.b());
        return view;
    }

    public int getViewTypeCount() {
        return c.n();
    }

    public boolean isEnabled(int i) {
        return getItem(i).b();
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        Integer valueOf = Integer.valueOf(a());
        this.d = new HashMap(valueOf.intValue());
        Integer num = 0;
        for (int i = 0; i < valueOf.intValue(); i++) {
            Integer valueOf2 = Integer.valueOf(a(i));
            if (valueOf2.intValue() != 0) {
                this.c.add(b(i));
                this.c.addAll(c(i));
                this.d.put(Integer.valueOf(i), num);
                num = Integer.valueOf(num.intValue() + valueOf2.intValue() + 1);
            }
        }
        this.c.add(new e(""));
    }

    public void onClick(View view) {
        b bVar = (b) view.getTag();
        c b2 = bVar.b();
        a e2 = e(bVar.a());
        a aVar = this.e;
        if (aVar != null && e2 != null) {
            aVar.a(e2, b2);
        }
    }
}
