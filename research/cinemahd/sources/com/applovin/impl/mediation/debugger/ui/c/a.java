package com.applovin.impl.mediation.debugger.ui.c;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;
import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.mediation.debugger.ui.a;
import com.applovin.impl.mediation.debugger.ui.c.b;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.mediation.debugger.ui.d.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxDebuggerMultiAdActivity;
import com.applovin.sdk.R$id;
import com.applovin.sdk.R$layout;

public class a extends com.applovin.impl.mediation.debugger.ui.a {

    /* renamed from: a  reason: collision with root package name */
    private b f1676a;
    private ListView b;

    public void initialize(final b bVar) {
        setTitle(bVar.h());
        this.f1676a = new b(bVar, this);
        this.f1676a.a((d.a) new d.a() {
            public void a(com.applovin.impl.mediation.debugger.ui.d.a aVar, c cVar) {
                if (aVar.a() == b.a.TEST_ADS.ordinal()) {
                    l s = bVar.s();
                    b.C0011b c = bVar.c();
                    if (b.C0011b.READY == c) {
                        a.this.startActivity(MaxDebuggerMultiAdActivity.class, s.D(), new a.C0012a<MaxDebuggerMultiAdActivity>() {
                            public void a(MaxDebuggerMultiAdActivity maxDebuggerMultiAdActivity) {
                                maxDebuggerMultiAdActivity.initialize(bVar);
                            }
                        });
                        return;
                    } else if (b.C0011b.DISABLED == c) {
                        s.g().d();
                        r.a("Restart Required", cVar.k(), (Context) a.this);
                        return;
                    }
                }
                r.a("Instructions", cVar.k(), (Context) a.this);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.list_view);
        this.b = (ListView) findViewById(R$id.listView);
        this.b.setAdapter(this.f1676a);
    }
}
