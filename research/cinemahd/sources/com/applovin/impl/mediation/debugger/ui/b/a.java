package com.applovin.impl.mediation.debugger.ui.b;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.applovin.impl.mediation.debugger.ui.a;
import com.applovin.impl.mediation.debugger.ui.b.b;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.mediation.debugger.ui.d.d;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxDebuggerAdUnitsListActivity;
import com.applovin.mediation.MaxDebuggerDetailActivity;
import com.applovin.sdk.R$id;
import com.applovin.sdk.R$layout;
import com.applovin.sdk.R$menu;

public class a extends com.applovin.impl.mediation.debugger.ui.a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public b f1668a;
    private DataSetObserver b;
    private FrameLayout c;
    private ListView d;
    private com.applovin.impl.adview.a e;

    private void a() {
        String d2 = this.f1668a.d().d().d();
        if (!TextUtils.isEmpty(d2)) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.TEXT", d2);
            intent.putExtra("android.intent.extra.TITLE", "Mediation Debugger logs");
            intent.putExtra("android.intent.extra.SUBJECT", "MAX Mediation Debugger logs");
            startActivity(Intent.createChooser(intent, (CharSequence) null));
        }
    }

    /* access modifiers changed from: private */
    public void a(final Context context) {
        if (o.b(this.f1668a.g()) && !this.f1668a.c()) {
            this.f1668a.a(true);
            runOnUiThread(new Runnable() {
                public void run() {
                    r.a(a.this.f1668a.f(), a.this.f1668a.g(), context);
                }
            });
        }
    }

    private void b() {
        c();
        this.e = new com.applovin.impl.adview.a(this, 50, 16842874);
        this.e.setColor(-3355444);
        this.c.addView(this.e, new FrameLayout.LayoutParams(-1, -1, 17));
        this.c.bringChildToFront(this.e);
        this.e.a();
    }

    /* access modifiers changed from: private */
    public void c() {
        com.applovin.impl.adview.a aVar = this.e;
        if (aVar != null) {
            aVar.b();
            this.c.removeView(this.e);
            this.e = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle("MAX Mediation Debugger");
        setContentView(R$layout.list_view);
        this.c = (FrameLayout) findViewById(16908290);
        this.d = (ListView) findViewById(R$id.listView);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R$menu.mediation_debugger_activity_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f1668a.unregisterDataSetObserver(this.b);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (R$id.action_share != menuItem.getItemId()) {
            return super.onOptionsItemSelected(menuItem);
        }
        a();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.d.setAdapter(this.f1668a);
        if (!this.f1668a.b()) {
            b();
        }
    }

    public void setListAdapter(b bVar, final com.applovin.impl.sdk.a aVar) {
        DataSetObserver dataSetObserver;
        b bVar2 = this.f1668a;
        if (!(bVar2 == null || (dataSetObserver = this.b) == null)) {
            bVar2.unregisterDataSetObserver(dataSetObserver);
        }
        this.f1668a = bVar;
        this.b = new DataSetObserver() {
            public void onChanged() {
                a.this.c();
                a aVar = a.this;
                aVar.a((Context) aVar);
            }
        };
        a((Context) this);
        this.f1668a.registerDataSetObserver(this.b);
        this.f1668a.a((d.a) new d.a() {
            public void a(com.applovin.impl.mediation.debugger.ui.d.a aVar, final c cVar) {
                int a2 = aVar.a();
                if (a2 == b.a.ADS.ordinal()) {
                    if (a.this.f1668a.e().size() > 0) {
                        a.this.startActivity(MaxDebuggerAdUnitsListActivity.class, aVar, new a.C0012a<MaxDebuggerAdUnitsListActivity>() {
                            public void a(MaxDebuggerAdUnitsListActivity maxDebuggerAdUnitsListActivity) {
                                maxDebuggerAdUnitsListActivity.initialize(a.this.f1668a.e(), a.this.f1668a.d());
                            }
                        });
                    } else {
                        r.a("No live ad units", "Please setup or enable your MAX ad units on https://applovin.com", (Context) a.this);
                    }
                } else if ((a2 == b.a.INCOMPLETE_NETWORKS.ordinal() || a2 == b.a.COMPLETED_NETWORKS.ordinal()) && (cVar instanceof com.applovin.impl.mediation.debugger.ui.b.a.a)) {
                    a.this.startActivity(MaxDebuggerDetailActivity.class, aVar, new a.C0012a<MaxDebuggerDetailActivity>(this) {
                        public void a(MaxDebuggerDetailActivity maxDebuggerDetailActivity) {
                            maxDebuggerDetailActivity.initialize(((com.applovin.impl.mediation.debugger.ui.b.a.a) cVar).p());
                        }
                    });
                }
            }
        });
    }
}
