package com.applovin.impl.mediation.debugger.ui.b.a;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.text.TextUtils;
import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.sdk.utils.f;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.R$color;
import com.applovin.sdk.R$drawable;
import com.facebook.imageutils.JfifUtil;

public class a extends c {
    private final b m;
    private final Context n;

    public a(b bVar, Context context) {
        super(c.b.DETAIL);
        this.m = bVar;
        this.n = context;
        this.c = q();
        this.d = r();
    }

    private SpannedString q() {
        return o.a(this.m.h(), b() ? -16777216 : -7829368, 18, 1);
    }

    private SpannedString r() {
        if (!b()) {
            return null;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(s());
        spannableStringBuilder.append(new SpannableString(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE));
        spannableStringBuilder.append(t());
        if (this.m.a() == b.a.INVALID_INTEGRATION) {
            spannableStringBuilder.append(new SpannableString(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE));
            spannableStringBuilder.append(o.b("Invalid Integration", -65536));
        }
        return new SpannedString(spannableStringBuilder);
    }

    private SpannedString s() {
        if (!this.m.d()) {
            return o.b("SDK Missing", -65536);
        }
        if (!TextUtils.isEmpty(this.m.i())) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(o.c("SDK\t\t\t\t\t  ", -7829368));
            spannableStringBuilder.append(o.b(this.m.i(), -16777216));
            return new SpannedString(spannableStringBuilder);
        }
        return o.b(this.m.e() ? "Retrieving SDK Version..." : "SDK Found", -16777216);
    }

    private SpannedString t() {
        if (!this.m.e()) {
            return o.b("Adapter Missing", -65536);
        }
        if (TextUtils.isEmpty(this.m.j())) {
            return o.b("Adapter Found", -16777216);
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(o.c("ADAPTER  ", -7829368));
        spannableStringBuilder.append(o.b(this.m.j(), -16777216));
        if (this.m.f()) {
            spannableStringBuilder.append(o.c("  LATEST  ", Color.rgb(JfifUtil.MARKER_FIRST_BYTE, 127, 0)));
            spannableStringBuilder.append(o.b(this.m.k(), -16777216));
        }
        return new SpannedString(spannableStringBuilder);
    }

    public boolean b() {
        return this.m.a() != b.a.MISSING;
    }

    public int e() {
        int n2 = this.m.n();
        return n2 > 0 ? n2 : R$drawable.applovin_ic_mediation_placeholder;
    }

    public int f() {
        return b() ? R$drawable.applovin_ic_disclosure_arrow : super.e();
    }

    public int g() {
        return f.a(R$color.applovin_sdk_disclosureButtonColor, this.n);
    }

    public b p() {
        return this.m;
    }

    public String toString() {
        return "MediatedNetworkListItemViewModel{text=" + this.c + ", detailText=" + this.d + ", network=" + this.m + "}";
    }
}
