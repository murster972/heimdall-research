package com.applovin.impl.mediation.debugger.a.a;

import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAdFormat;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class a implements Comparable<a> {

    /* renamed from: a  reason: collision with root package name */
    private final String f1645a;
    private final String b;
    private final MaxAdFormat c;
    private final c d;
    private final List<c> e;

    public a(JSONObject jSONObject, Map<String, b> map, l lVar) {
        this.f1645a = j.b(jSONObject, MediationMetaData.KEY_NAME, "", lVar);
        this.b = j.b(jSONObject, "display_name", "", lVar);
        this.c = r.c(j.b(jSONObject, "format", (String) null, lVar));
        JSONArray b2 = j.b(jSONObject, "waterfalls", new JSONArray(), lVar);
        this.e = new ArrayList(b2.length());
        c cVar = null;
        for (int i = 0; i < b2.length(); i++) {
            JSONObject a2 = j.a(b2, i, (JSONObject) null, lVar);
            if (a2 != null) {
                c cVar2 = new c(a2, map, lVar);
                this.e.add(cVar2);
                if (cVar == null && cVar2.c()) {
                    cVar = cVar2;
                }
            }
        }
        this.d = cVar;
    }

    private c g() {
        if (!this.e.isEmpty()) {
            return this.e.get(0);
        }
        return null;
    }

    /* renamed from: a */
    public int compareTo(a aVar) {
        return this.b.compareToIgnoreCase(aVar.b);
    }

    public String a() {
        return this.f1645a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        MaxAdFormat maxAdFormat = this.c;
        return maxAdFormat != null ? maxAdFormat.getDisplayName() : "Unknown";
    }

    public MaxAdFormat d() {
        return this.c;
    }

    public c e() {
        c cVar = this.d;
        return cVar != null ? cVar : g();
    }

    public String f() {
        return "\n---------- " + this.b + " ----------" + "\nIdentifier - " + this.f1645a + "\nFormat     - " + c();
    }
}
