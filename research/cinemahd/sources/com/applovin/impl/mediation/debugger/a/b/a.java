package com.applovin.impl.mediation.debugger.a.b;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.r;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f1649a;
    private final String b;
    private final boolean c;

    a(JSONObject jSONObject, l lVar) {
        boolean z;
        this.f1649a = j.b(jSONObject, MediationMetaData.KEY_NAME, "", lVar);
        this.b = j.b(jSONObject, "description", "", lVar);
        List a2 = j.a(jSONObject, "existence_classes", (List) null, lVar);
        if (a2 != null) {
            z = false;
            Iterator it2 = a2.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (r.e((String) it2.next())) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
        } else {
            z = r.e(j.b(jSONObject, "existence_class", "", lVar));
        }
        this.c = z;
    }

    public String a() {
        return this.f1649a;
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }
}
