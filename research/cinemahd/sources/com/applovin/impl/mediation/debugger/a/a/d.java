package com.applovin.impl.mediation.debugger.a.a;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import org.json.JSONObject;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final String f1648a;
    private final String b;

    public d(JSONObject jSONObject, l lVar) {
        this.f1648a = j.b(jSONObject, "id", "", lVar);
        this.b = j.b(jSONObject, "price", (String) null, lVar);
    }

    public String a() {
        return this.f1648a;
    }

    public String b() {
        return this.b;
    }
}
