package com.applovin.impl.mediation.debugger.ui.c;

import android.content.Context;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.mediation.debugger.ui.d.d;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.f;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.sdk.R$color;
import com.applovin.sdk.R$drawable;
import java.util.ArrayList;
import java.util.List;

public class b extends d {
    private final List<c> f;
    private final List<c> g;
    private final List<c> h;
    private final List<c> i;
    private final List<c> j;
    private SpannedString k;

    public enum a {
        INTEGRATIONS,
        PERMISSIONS,
        CONFIGURATION,
        DEPENDENCIES,
        TEST_ADS,
        COUNT
    }

    b(com.applovin.impl.mediation.debugger.a.b.b bVar, Context context) {
        super(context);
        if (bVar.a() == b.a.INVALID_INTEGRATION) {
            SpannableString spannableString = new SpannableString("Tap for more information");
            spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, spannableString.length(), 33);
            this.k = new SpannedString(spannableString);
        } else {
            this.k = new SpannedString("");
        }
        this.f = a(bVar);
        this.g = a(bVar.p());
        this.h = a(bVar.r());
        this.i = b(bVar.q());
        this.j = e(bVar);
        notifyDataSetChanged();
    }

    private int a(boolean z) {
        return z ? R$drawable.applovin_ic_check_mark : R$drawable.applovin_ic_x_mark;
    }

    private c a(b.C0011b bVar) {
        c.a o = c.o();
        if (bVar == b.C0011b.READY) {
            o.a(this.b);
        }
        o.a("Test Mode");
        o.b(bVar.a());
        o.b(bVar.b());
        o.c(bVar.c());
        o.a(true);
        return o.a();
    }

    private List<c> a(com.applovin.impl.mediation.debugger.a.b.b bVar) {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(b(bVar));
        arrayList.add(c(bVar));
        arrayList.add(d(bVar));
        return arrayList;
    }

    private List<c> a(com.applovin.impl.mediation.debugger.a.b.c cVar) {
        ArrayList arrayList = new ArrayList(1);
        if (cVar.a()) {
            boolean b = cVar.b();
            c.a a2 = c.a(b ? c.b.RIGHT_DETAIL : c.b.DETAIL);
            a2.a("Cleartext Traffic");
            a2.b(b ? null : this.k);
            a2.c(cVar.c());
            a2.a(a(b));
            a2.c(b(b));
            a2.a(!b);
            arrayList.add(a2.a());
        }
        return arrayList;
    }

    private List<c> a(List<com.applovin.impl.mediation.debugger.a.b.d> list) {
        ArrayList arrayList = new ArrayList(list.size());
        if (list.size() > 0) {
            for (com.applovin.impl.mediation.debugger.a.b.d next : list) {
                boolean c = next.c();
                c.a a2 = c.a(c ? c.b.RIGHT_DETAIL : c.b.DETAIL);
                a2.a(next.a());
                a2.b(c ? null : this.k);
                a2.c(next.b());
                a2.a(a(c));
                a2.c(b(c));
                a2.a(!c);
                arrayList.add(a2.a());
            }
        }
        return arrayList;
    }

    private int b(boolean z) {
        return f.a(z ? R$color.applovin_sdk_checkmarkColor : R$color.applovin_sdk_xmarkColor, this.b);
    }

    private c b(com.applovin.impl.mediation.debugger.a.b.b bVar) {
        c.a o = c.o();
        o.a("SDK");
        o.b(bVar.i());
        if (TextUtils.isEmpty(bVar.i())) {
            o.a(a(bVar.d()));
            o.c(b(bVar.d()));
        }
        return o.a();
    }

    private List<c> b(List<com.applovin.impl.mediation.debugger.a.b.a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        if (list.size() > 0) {
            for (com.applovin.impl.mediation.debugger.a.b.a next : list) {
                boolean c = next.c();
                c.a a2 = c.a(c ? c.b.RIGHT_DETAIL : c.b.DETAIL);
                a2.a(next.a());
                a2.b(c ? null : this.k);
                a2.c(next.b());
                a2.a(a(c));
                a2.c(b(c));
                a2.a(!c);
                arrayList.add(a2.a());
            }
        }
        return arrayList;
    }

    private c c(com.applovin.impl.mediation.debugger.a.b.b bVar) {
        c.a o = c.o();
        o.a("Adapter");
        o.b(bVar.j());
        if (TextUtils.isEmpty(bVar.j())) {
            o.a(a(bVar.e()));
            o.c(b(bVar.e()));
        }
        return o.a();
    }

    private c c(List<String> list) {
        c.a o = c.o();
        o.a("Region/VPN Required");
        o.b(e.a(list, ", ", list.size()));
        return o.a();
    }

    private c d(com.applovin.impl.mediation.debugger.a.b.b bVar) {
        String str;
        String str2;
        boolean e = e(bVar.b());
        boolean z = false;
        if (bVar.s().a().b()) {
            str2 = "Please ensure that you are initializing the AppLovin MAX SDK with an Activity Context.";
            str = "Initialize with Activity Context";
            e = false;
            z = true;
        } else {
            str = "Adapter Initialized";
            str2 = null;
        }
        c.a o = c.o();
        o.a(str);
        o.c(str2);
        o.a(a(e));
        o.c(b(e));
        o.a(z);
        return o.a();
    }

    private List<c> e(com.applovin.impl.mediation.debugger.a.b.b bVar) {
        ArrayList arrayList = new ArrayList(2);
        if (bVar.c() != b.C0011b.NOT_SUPPORTED) {
            if (bVar.m() != null) {
                arrayList.add(c(bVar.m()));
            }
            arrayList.add(a(bVar.c()));
        }
        return arrayList;
    }

    private boolean e(int i2) {
        return (i2 == MaxAdapter.InitializationStatus.INITIALIZED_FAILURE.getCode() || i2 == MaxAdapter.InitializationStatus.INITIALIZING.getCode()) ? false : true;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return a.COUNT.ordinal();
    }

    /* access modifiers changed from: protected */
    public int a(int i2) {
        return (i2 == a.INTEGRATIONS.ordinal() ? this.f : i2 == a.PERMISSIONS.ordinal() ? this.g : i2 == a.CONFIGURATION.ordinal() ? this.h : i2 == a.DEPENDENCIES.ordinal() ? this.i : this.j).size();
    }

    /* access modifiers changed from: protected */
    public c b(int i2) {
        return i2 == a.INTEGRATIONS.ordinal() ? new com.applovin.impl.mediation.debugger.ui.d.e("INTEGRATIONS") : i2 == a.PERMISSIONS.ordinal() ? new com.applovin.impl.mediation.debugger.ui.d.e("PERMISSIONS") : i2 == a.CONFIGURATION.ordinal() ? new com.applovin.impl.mediation.debugger.ui.d.e("CONFIGURATION") : i2 == a.DEPENDENCIES.ordinal() ? new com.applovin.impl.mediation.debugger.ui.d.e("DEPENDENCIES") : new com.applovin.impl.mediation.debugger.ui.d.e("TEST ADS");
    }

    /* access modifiers changed from: protected */
    public List<c> c(int i2) {
        return i2 == a.INTEGRATIONS.ordinal() ? this.f : i2 == a.PERMISSIONS.ordinal() ? this.g : i2 == a.CONFIGURATION.ordinal() ? this.h : i2 == a.DEPENDENCIES.ordinal() ? this.i : this.j;
    }

    public String toString() {
        return "MediatedNetworkListAdapter{}";
    }
}
