package com.applovin.impl.mediation.debugger.ui.b.a;

import android.content.Context;
import android.text.SpannedString;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.sdk.i;

public class b extends c {
    private final i.a m;
    private final Context n;
    private final boolean o;

    public b(i.a aVar, boolean z, Context context) {
        super(c.b.RIGHT_DETAIL);
        this.m = aVar;
        this.n = context;
        this.c = new SpannedString(aVar.a());
        this.o = z;
    }

    public SpannedString a() {
        return new SpannedString(this.m.b(this.n));
    }

    public boolean b() {
        return true;
    }

    public boolean c() {
        Boolean a2 = this.m.a(this.n);
        if (a2 != null) {
            return a2.equals(Boolean.valueOf(this.o));
        }
        return false;
    }
}
