package com.applovin.impl.mediation.debugger.a.b;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.j;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f1653a;
    private final boolean b;
    private final boolean c;
    private final String d;

    public c(JSONObject jSONObject, l lVar) {
        this.f1653a = com.applovin.impl.sdk.utils.c.a(lVar.i()).a();
        JSONObject b2 = j.b(jSONObject, "cleartext_traffic", (JSONObject) null, lVar);
        boolean z = false;
        if (b2 != null) {
            this.b = true;
            this.d = j.b(b2, "description", "", lVar);
            if (h.a()) {
                this.c = true;
                return;
            }
            List a2 = j.a(b2, "domains", (List) new ArrayList(), lVar);
            if (a2.size() > 0) {
                Iterator it2 = a2.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (!h.a((String) it2.next())) {
                            break;
                        }
                    } else {
                        z = true;
                        break;
                    }
                }
            }
            this.c = z;
            return;
        }
        this.b = false;
        this.d = "";
        this.c = h.a();
    }

    public boolean a() {
        return this.b;
    }

    public boolean b() {
        return this.c;
    }

    public String c() {
        return this.f1653a ? this.d : "You must include an entry in your AndroidManifest.xml to point to your network_security_config.xml.\n\nFor more information, visit: https://developer.android.com/training/articles/security-config";
    }
}
