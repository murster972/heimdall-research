package com.applovin.impl.mediation.debugger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.mediation.debugger.ui.b.b;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxDebuggerActivity;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.facebook.react.uimanager.ViewProps;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class a implements a.c<JSONObject> {
    /* access modifiers changed from: private */
    public static WeakReference<MaxDebuggerActivity> i;
    /* access modifiers changed from: private */
    public static final AtomicBoolean j = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1640a;
    private final r b;
    /* access modifiers changed from: private */
    public final b c;
    private final Map<String, com.applovin.impl.mediation.debugger.a.b.b> d = new HashMap();
    private final StringBuilder e = new StringBuilder("");
    private final AtomicBoolean f = new AtomicBoolean();
    private boolean g;
    private final Context h;

    public a(l lVar) {
        this.f1640a = lVar;
        this.b = lVar.j0();
        this.h = lVar.i();
        this.c = new b(this.h);
    }

    private List<com.applovin.impl.mediation.debugger.a.b.b> a(JSONObject jSONObject, l lVar) {
        JSONArray b2 = j.b(jSONObject, "networks", new JSONArray(), lVar);
        ArrayList arrayList = new ArrayList(b2.length());
        for (int i2 = 0; i2 < b2.length(); i2++) {
            JSONObject a2 = j.a(b2, i2, (JSONObject) null, lVar);
            if (a2 != null) {
                com.applovin.impl.mediation.debugger.a.b.b bVar = new com.applovin.impl.mediation.debugger.a.b.b(a2, lVar);
                arrayList.add(bVar);
                this.d.put(bVar.l(), bVar);
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    private List<com.applovin.impl.mediation.debugger.a.a.a> a(JSONObject jSONObject, List<com.applovin.impl.mediation.debugger.a.b.b> list, l lVar) {
        JSONArray b2 = j.b(jSONObject, "ad_units", new JSONArray(), lVar);
        ArrayList arrayList = new ArrayList(b2.length());
        for (int i2 = 0; i2 < b2.length(); i2++) {
            JSONObject a2 = j.a(b2, i2, (JSONObject) null, lVar);
            if (a2 != null) {
                arrayList.add(new com.applovin.impl.mediation.debugger.a.a.a(a2, this.d, lVar));
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    private void a(List<com.applovin.impl.mediation.debugger.a.b.b> list) {
        boolean z;
        Iterator<com.applovin.impl.mediation.debugger.a.b.b> it2 = list.iterator();
        while (true) {
            if (!it2.hasNext()) {
                z = false;
                break;
            }
            com.applovin.impl.mediation.debugger.a.b.b next = it2.next();
            if (next.e() && next.a() == b.a.INVALID_INTEGRATION) {
                z = true;
                break;
            }
        }
        if (z) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    new AlertDialog.Builder(a.this.f1640a.D().a()).setTitle("MAX Integration Incomplete").setMessage("Some SDKs are not configured correctly. Check the Mediation Debugger to diagnose the issue.").setPositiveButton("Show Mediation Debugger", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            a.this.c();
                        }
                    }).setNegativeButton("DISMISS", (DialogInterface.OnClickListener) null).create().show();
                }
            }, TimeUnit.SECONDS.toMillis(2));
        }
    }

    /* access modifiers changed from: private */
    public boolean g() {
        WeakReference<MaxDebuggerActivity> weakReference = i;
        return (weakReference == null || weakReference.get() == null) ? false : true;
    }

    public void a() {
        if (this.f1640a.Q() && this.f.compareAndSet(false, true)) {
            this.f1640a.o().a((com.applovin.impl.sdk.e.a) new com.applovin.impl.mediation.debugger.b.a(this, this.f1640a), o.a.MEDIATION_MAIN);
        }
    }

    public void a(int i2) {
        r rVar = this.b;
        rVar.e("MediationDebuggerService", "Unable to fetch mediation debugger info: server returned " + i2);
        r.i("AppLovinSdk", "Unable to show mediation debugger.");
        this.c.a((List<com.applovin.impl.mediation.debugger.a.b.b>) null, (List<com.applovin.impl.mediation.debugger.a.a.a>) null, (String) null, (String) null, (String) null, this.f1640a);
        this.f.set(false);
    }

    public void a(JSONObject jSONObject, int i2) {
        Map<String, String> a2;
        List<com.applovin.impl.mediation.debugger.a.b.b> a3 = a(jSONObject, this.f1640a);
        List<com.applovin.impl.mediation.debugger.a.a.a> a4 = a(jSONObject, a3, this.f1640a);
        JSONObject b2 = j.b(jSONObject, "alert", (JSONObject) null, this.f1640a);
        this.c.a(a3, a4, j.b(b2, "title", (String) null, this.f1640a), j.b(b2, "message", (String) null, this.f1640a), j.b(jSONObject, "account_email", (String) null, this.f1640a), this.f1640a);
        if (b()) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    a.this.c();
                }
            }, TimeUnit.SECONDS.toMillis(2));
        } else {
            a(a3);
        }
        StringBuilder sb = new StringBuilder("\n========== MEDIATION DEBUGGER ==========");
        sb.append("\n========== APP INFO ==========");
        sb.append("\nDev Build - " + com.applovin.impl.sdk.utils.r.e(this.h));
        StringBuilder sb2 = new StringBuilder();
        sb2.append("\nTest Mode - ");
        sb2.append(this.f1640a.g().a() ? ViewProps.ENABLED : "disabled");
        sb.append(sb2.toString());
        sb.append("\nTarget SDK - " + this.f1640a.r().g().get("target_sdk"));
        sb.append("\n========== MAX ==========");
        String str = AppLovinSdk.VERSION;
        String str2 = (String) this.f1640a.a(com.applovin.impl.sdk.c.b.D2);
        String f2 = com.applovin.impl.sdk.utils.r.f();
        sb.append("\nSDK Version - " + str);
        StringBuilder sb3 = new StringBuilder();
        sb3.append("\nPlugin Version - ");
        if (!com.applovin.impl.sdk.utils.o.b(str2)) {
            str2 = "None";
        }
        sb3.append(str2);
        sb.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder();
        sb4.append("\nAd Review Version - ");
        if (!com.applovin.impl.sdk.utils.o.b(f2)) {
            f2 = "Disabled";
        }
        sb4.append(f2);
        sb.append(sb4.toString());
        if (this.f1640a.R() && (a2 = com.applovin.impl.sdk.utils.r.a(this.f1640a.Y())) != null) {
            String str3 = a2.get("UnityVersion");
            StringBuilder sb5 = new StringBuilder();
            sb5.append("\nUnity Version - ");
            if (!com.applovin.impl.sdk.utils.o.b(str3)) {
                str3 = "None";
            }
            sb5.append(str3);
            sb.append(sb5.toString());
        }
        sb.append("\n========== PRIVACY ==========");
        sb.append(i.a(this.h));
        sb.append("\n========== NETWORKS ==========");
        for (com.applovin.impl.mediation.debugger.a.b.b t : a3) {
            String sb6 = sb.toString();
            String t2 = t.t();
            if (sb6.length() + t2.length() >= ((Integer) this.f1640a.a(com.applovin.impl.sdk.c.b.t)).intValue()) {
                r.f("MediationDebuggerService", sb6);
                this.e.append(sb6);
                sb.setLength(1);
            }
            sb.append(t2);
        }
        sb.append("\n========== AD UNITS ==========");
        for (com.applovin.impl.mediation.debugger.a.a.a f3 : a4) {
            String sb7 = sb.toString();
            String f4 = f3.f();
            if (sb7.length() + f4.length() >= ((Integer) this.f1640a.a(com.applovin.impl.sdk.c.b.t)).intValue()) {
                r.f("MediationDebuggerService", sb7);
                this.e.append(sb7);
                sb.setLength(1);
            }
            sb.append(f4);
        }
        sb.append("\n========== END ==========");
        r.f("MediationDebuggerService", sb.toString());
        this.e.append(sb.toString());
    }

    public void a(boolean z) {
        this.g = z;
    }

    public boolean b() {
        return this.g;
    }

    public void c() {
        a();
        if (g() || !j.compareAndSet(false, true)) {
            r.i("AppLovinSdk", "Mediation debugger is already showing");
            return;
        }
        this.f1640a.D().a(new com.applovin.impl.sdk.utils.a() {
            public void onActivityDestroyed(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    r.f("AppLovinSdk", "Mediation debugger destroyed");
                    a.this.f1640a.D().b(this);
                    WeakReference unused = a.i = null;
                }
            }

            public void onActivityStarted(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    r.f("AppLovinSdk", "Started mediation debugger");
                    if (!a.this.g() || a.i.get() != activity) {
                        MaxDebuggerActivity maxDebuggerActivity = (MaxDebuggerActivity) activity;
                        WeakReference unused = a.i = new WeakReference(maxDebuggerActivity);
                        maxDebuggerActivity.setListAdapter(a.this.c, a.this.f1640a.D());
                    }
                    a.j.set(false);
                }
            }
        });
        Intent intent = new Intent(this.h, MaxDebuggerActivity.class);
        intent.setFlags(268435456);
        r.f("AppLovinSdk", "Starting mediation debugger...");
        this.h.startActivity(intent);
    }

    public String d() {
        return this.e.toString();
    }

    public String toString() {
        return "MediationDebuggerService{, listAdapter=" + this.c + "}";
    }
}
