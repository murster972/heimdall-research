package com.applovin.impl.mediation.debugger.ui.b;

import android.content.Context;
import android.text.TextUtils;
import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.mediation.debugger.ui.d.d;
import com.applovin.impl.mediation.debugger.ui.d.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.f;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.R$color;
import com.applovin.sdk.R$drawable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class b extends d {
    private l f;
    private List<com.applovin.impl.mediation.debugger.a.a.a> g;
    private String h;
    private String i;
    private String j;
    private final AtomicBoolean k = new AtomicBoolean();
    private boolean l = false;
    private List<c> m = new ArrayList();
    private List<c> n = new ArrayList();
    private List<c> o = new ArrayList();
    private List<c> p = new ArrayList();
    private List<c> q = new ArrayList();
    private List<c> r = new ArrayList();
    private List<c> s = new ArrayList();

    public enum a {
        APP_INFO,
        MAX,
        PRIVACY,
        ADS,
        INCOMPLETE_NETWORKS,
        COMPLETED_NETWORKS,
        MISSING_NETWORKS,
        COUNT
    }

    public b(Context context) {
        super(context);
    }

    private c a(String str, String str2) {
        c.a o2 = c.o();
        o2.a(str);
        if (o.b(str2)) {
            o2.b(str2);
        } else {
            o2.a(R$drawable.applovin_ic_x_mark);
            o2.c(f.a(R$color.applovin_sdk_xmarkColor, this.b));
        }
        return o2.a();
    }

    private void a(List<com.applovin.impl.mediation.debugger.a.b.b> list) {
        List<c> list2;
        this.f.j0().b("MediationDebuggerListAdapter", "Updating networks...");
        for (com.applovin.impl.mediation.debugger.a.b.b next : list) {
            com.applovin.impl.mediation.debugger.ui.b.a.a aVar = new com.applovin.impl.mediation.debugger.ui.b.a.a(next, this.b);
            if (next.a() == b.a.INCOMPLETE_INTEGRATION || next.a() == b.a.INVALID_INTEGRATION) {
                list2 = this.q;
            } else if (next.a() == b.a.COMPLETE) {
                list2 = this.r;
            } else if (next.a() == b.a.MISSING) {
                list2 = this.s;
            }
            list2.add(aVar);
        }
    }

    private List<c> h() {
        String str;
        ArrayList arrayList = new ArrayList(3);
        c.a o2 = c.o();
        o2.a("Package Name");
        o2.b(this.b.getPackageName());
        arrayList.add(o2.a());
        try {
            str = this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 0).versionName;
        } catch (Throwable unused) {
            str = null;
        }
        if (!TextUtils.isEmpty(str)) {
            c.a o3 = c.o();
            o3.a("App Version");
            o3.b(str);
            arrayList.add(o3.a());
        }
        if (!TextUtils.isEmpty(this.j)) {
            c.a o4 = c.o();
            o4.a("Account");
            o4.b(this.j);
            arrayList.add(o4.a());
        }
        return arrayList;
    }

    private List<c> i() {
        Map<String, String> a2;
        ArrayList arrayList = new ArrayList(4);
        String str = AppLovinSdk.VERSION;
        String str2 = (String) this.f.a(com.applovin.impl.sdk.c.b.D2);
        c.a o2 = c.o();
        o2.a("SDK Version");
        o2.b(str);
        arrayList.add(o2.a());
        c.a o3 = c.o();
        o3.a("Plugin Version");
        if (!o.b(str2)) {
            str2 = "None";
        }
        o3.b(str2);
        arrayList.add(o3.a());
        arrayList.add(a("Ad Review Version", r.f()));
        if (this.f.R() && (a2 = r.a(this.f.Y())) != null) {
            String str3 = a2.get("UnityVersion");
            if (!o.b(str3)) {
                str3 = "None";
            }
            arrayList.add(a("Unity Version", str3));
        }
        return arrayList;
    }

    private List<c> j() {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(new com.applovin.impl.mediation.debugger.ui.b.a.b(i.a(), true, this.b));
        arrayList.add(new com.applovin.impl.mediation.debugger.ui.b.a.b(i.b(), false, this.b));
        arrayList.add(new com.applovin.impl.mediation.debugger.ui.b.a.b(i.c(), true, this.b));
        return arrayList;
    }

    private List<c> k() {
        ArrayList arrayList = new ArrayList(1);
        c.a o2 = c.o();
        o2.a("View Ad Units (" + this.g.size() + ")");
        o2.a(this.b);
        o2.a(true);
        arrayList.add(o2.a());
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return a.COUNT.ordinal();
    }

    /* access modifiers changed from: protected */
    public int a(int i2) {
        return (i2 == a.APP_INFO.ordinal() ? this.m : i2 == a.MAX.ordinal() ? this.n : i2 == a.PRIVACY.ordinal() ? this.o : i2 == a.ADS.ordinal() ? this.p : i2 == a.INCOMPLETE_NETWORKS.ordinal() ? this.q : i2 == a.COMPLETED_NETWORKS.ordinal() ? this.r : this.s).size();
    }

    public void a(List<com.applovin.impl.mediation.debugger.a.b.b> list, List<com.applovin.impl.mediation.debugger.a.a.a> list2, String str, String str2, String str3, l lVar) {
        this.f = lVar;
        this.g = list2;
        this.h = str;
        this.i = str2;
        this.j = str3;
        if (list != null && this.k.compareAndSet(false, true)) {
            this.m.addAll(h());
            this.n.addAll(i());
            this.o.addAll(j());
            this.p.addAll(k());
            a(list);
        }
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                b.this.notifyDataSetChanged();
            }
        });
    }

    public void a(boolean z) {
        this.l = z;
    }

    /* access modifiers changed from: protected */
    public c b(int i2) {
        return i2 == a.APP_INFO.ordinal() ? new e("APP INFO") : i2 == a.MAX.ordinal() ? new e("MAX") : i2 == a.PRIVACY.ordinal() ? new e("PRIVACY") : i2 == a.ADS.ordinal() ? new e("ADS") : i2 == a.INCOMPLETE_NETWORKS.ordinal() ? new e("INCOMPLETE INTEGRATIONS") : i2 == a.COMPLETED_NETWORKS.ordinal() ? new e("COMPLETED INTEGRATIONS") : new e("MISSING INTEGRATIONS");
    }

    public boolean b() {
        return this.k.get();
    }

    /* access modifiers changed from: protected */
    public List<c> c(int i2) {
        return i2 == a.APP_INFO.ordinal() ? this.m : i2 == a.MAX.ordinal() ? this.n : i2 == a.PRIVACY.ordinal() ? this.o : i2 == a.ADS.ordinal() ? this.p : i2 == a.INCOMPLETE_NETWORKS.ordinal() ? this.q : i2 == a.COMPLETED_NETWORKS.ordinal() ? this.r : this.s;
    }

    public boolean c() {
        return this.l;
    }

    public l d() {
        return this.f;
    }

    public List<com.applovin.impl.mediation.debugger.a.a.a> e() {
        return this.g;
    }

    public String f() {
        return this.h;
    }

    public String g() {
        return this.i;
    }

    public String toString() {
        return "MediationDebuggerListAdapter{isInitialized=" + this.k.get() + "}";
    }
}
