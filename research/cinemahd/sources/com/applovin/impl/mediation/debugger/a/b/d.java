package com.applovin.impl.mediation.debugger.a.b;

import android.content.Context;
import com.applovin.impl.sdk.utils.g;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final String f1654a;
    private final String b;
    private final boolean c;

    d(String str, String str2, Context context) {
        this.f1654a = str.replace("android.permission.", "");
        this.b = str2;
        this.c = g.a(str, context);
    }

    public String a() {
        return this.f1654a;
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }
}
