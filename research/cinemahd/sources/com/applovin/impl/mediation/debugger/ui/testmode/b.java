package com.applovin.impl.mediation.debugger.ui.testmode;

import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import org.json.JSONArray;
import org.json.JSONObject;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final l f1689a;
    private boolean b;
    private boolean c;
    private String d = "";

    public b(l lVar) {
        this.f1689a = lVar;
        this.b = ((Boolean) lVar.b(d.C, false)).booleanValue();
        lVar.b(d.C);
    }

    public void a(String str, boolean z) {
        if (z) {
            this.f1689a.a(d.B, str);
        } else {
            this.d = str;
        }
    }

    public void a(JSONObject jSONObject) {
        if (!this.b) {
            this.b = j.a(this.f1689a.r().j().b, j.b(jSONObject, "test_mode_idfas", new JSONArray(), this.f1689a)) || this.f1689a.r().f() || this.f1689a.r().k();
        }
    }

    public void a(boolean z) {
        this.c = z;
    }

    public boolean a() {
        return this.b;
    }

    public boolean b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }

    public void d() {
        this.f1689a.a(d.C, true);
    }
}
