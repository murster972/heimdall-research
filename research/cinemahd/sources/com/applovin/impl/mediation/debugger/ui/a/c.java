package com.applovin.impl.mediation.debugger.ui.a;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.widget.ListView;
import com.applovin.impl.mediation.debugger.a.a.b;
import com.applovin.impl.mediation.debugger.ui.a;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.mediation.debugger.ui.d.d;
import com.applovin.impl.mediation.debugger.ui.d.e;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.o;
import com.applovin.mediation.MaxDebuggerAdUnitDetailActivity;
import com.applovin.sdk.R$id;
import com.applovin.sdk.R$layout;
import java.util.ArrayList;
import java.util.List;

public class c extends a {

    /* renamed from: a  reason: collision with root package name */
    private d f1662a;
    /* access modifiers changed from: private */
    public List<com.applovin.impl.mediation.debugger.ui.d.c> b;
    private ListView c;

    private List<com.applovin.impl.mediation.debugger.ui.d.c> a(List<com.applovin.impl.mediation.debugger.a.a.a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (com.applovin.impl.mediation.debugger.a.a.a next : list) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(o.c("ID\t\t\t\t\t\t", -7829368));
            spannableStringBuilder.append(o.b(next.a(), -16777216));
            spannableStringBuilder.append(new SpannedString(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE));
            spannableStringBuilder.append(o.c("FORMAT  ", -7829368));
            spannableStringBuilder.append(o.b(next.c(), -16777216));
            c.a a2 = com.applovin.impl.mediation.debugger.ui.d.c.a(c.b.DETAIL);
            a2.a(o.a(next.b(), -16777216, 18, 1));
            a2.b(new SpannedString(spannableStringBuilder));
            a2.a((Context) this);
            a2.a(true);
            arrayList.add(a2.a());
        }
        return arrayList;
    }

    public void initialize(final List<com.applovin.impl.mediation.debugger.a.a.a> list, final l lVar) {
        this.b = a(list);
        this.f1662a = new d(this) {
            /* access modifiers changed from: protected */
            public int a() {
                return 1;
            }

            /* access modifiers changed from: protected */
            public int a(int i) {
                return list.size();
            }

            /* access modifiers changed from: protected */
            public com.applovin.impl.mediation.debugger.ui.d.c b(int i) {
                return new e("");
            }

            /* access modifiers changed from: protected */
            public List<com.applovin.impl.mediation.debugger.ui.d.c> c(int i) {
                return c.this.b;
            }
        };
        this.f1662a.a((d.a) new d.a() {
            public void a(final com.applovin.impl.mediation.debugger.ui.d.a aVar, com.applovin.impl.mediation.debugger.ui.d.c cVar) {
                c.this.startActivity(MaxDebuggerAdUnitDetailActivity.class, lVar.D(), new a.C0012a<MaxDebuggerAdUnitDetailActivity>() {
                    public void a(MaxDebuggerAdUnitDetailActivity maxDebuggerAdUnitDetailActivity) {
                        maxDebuggerAdUnitDetailActivity.initialize((com.applovin.impl.mediation.debugger.a.a.a) list.get(aVar.b()), (b) null, lVar);
                    }
                });
            }
        });
        this.f1662a.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle("Ad Units");
        setContentView(R$layout.list_view);
        this.c = (ListView) findViewById(R$id.listView);
        this.c.setAdapter(this.f1662a);
    }
}
