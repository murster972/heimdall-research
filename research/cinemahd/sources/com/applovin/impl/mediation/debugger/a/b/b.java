package com.applovin.impl.mediation.debugger.a.b;

import android.text.TextUtils;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.MaxRewardedInterstitialAdapter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b implements AppLovinCommunicatorSubscriber, Comparable<b> {

    /* renamed from: a  reason: collision with root package name */
    private final l f1650a;
    private final a b;
    private int c;
    private final boolean d;
    private final boolean e;
    private final boolean f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private final String j;
    private final String k;
    private final String l;
    private final String m;
    private final String n;
    private final String o;
    private final int p;
    private final List<MaxAdFormat> q;
    private final List<d> r;
    private final List<a> s;
    private final List<String> t;
    private final c u;

    public enum a {
        MISSING("MISSING"),
        INCOMPLETE_INTEGRATION("INCOMPLETE INTEGRATION"),
        INVALID_INTEGRATION("INVALID INTEGRATION"),
        COMPLETE("COMPLETE");
        
        private final String e;

        private a(String str) {
            this.e = str;
        }

        /* access modifiers changed from: private */
        public String a() {
            return this.e;
        }
    }

    /* renamed from: com.applovin.impl.mediation.debugger.a.b.b$b  reason: collision with other inner class name */
    public enum C0011b {
        NOT_SUPPORTED("Not Supported", -65536, "This network does not support test mode."),
        INVALID_INTEGRATION("Invalid Integration", -65536, "Please address all the integration issue(s) marked in red above."),
        NOT_INITIALIZED("Not Initialized", -65536, "Please configure this network in your MAX dashboard."),
        DISABLED("Enable", -16776961, "Please re-launch the app to enable test ads."),
        READY("", -16776961, "");
        
        private final String f;
        private final int g;
        private final String h;

        private C0011b(String str, int i2, String str2) {
            this.f = str;
            this.g = i2;
            this.h = str2;
        }

        public String a() {
            return this.f;
        }

        public int b() {
            return this.g;
        }

        public String c() {
            return this.h;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x011e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public b(org.json.JSONObject r11, com.applovin.impl.sdk.l r12) {
        /*
            r10 = this;
            r10.<init>()
            r10.f1650a = r12
            java.lang.String r0 = ""
            java.lang.String r1 = "name"
            java.lang.String r1 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r1, (java.lang.String) r0, (com.applovin.impl.sdk.l) r12)
            r10.j = r1
            java.lang.String r1 = "display_name"
            java.lang.String r1 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r1, (java.lang.String) r0, (com.applovin.impl.sdk.l) r12)
            r10.k = r1
            java.lang.String r1 = "adapter_class"
            java.lang.String r2 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r1, (java.lang.String) r0, (com.applovin.impl.sdk.l) r12)
            r10.l = r2
            java.lang.String r2 = "latest_adapter_version"
            java.lang.String r2 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r2, (java.lang.String) r0, (com.applovin.impl.sdk.l) r12)
            r10.o = r2
            java.util.List r2 = r10.a((org.json.JSONObject) r11)
            r10.t = r2
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>()
            java.lang.String r3 = "configuration"
            org.json.JSONObject r2 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r3, (org.json.JSONObject) r2, (com.applovin.impl.sdk.l) r12)
            java.util.List r3 = r10.a(r2, r12)
            r10.r = r3
            java.util.List r3 = r10.b(r2, r12)
            r10.s = r3
            com.applovin.impl.mediation.debugger.a.b.c r3 = new com.applovin.impl.mediation.debugger.a.b.c
            r3.<init>(r2, r12)
            r10.u = r3
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>()
            java.lang.String r3 = "test_mode"
            org.json.JSONObject r2 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r3, (org.json.JSONObject) r2, (com.applovin.impl.sdk.l) r12)
            r3 = 1
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r3)
            java.lang.String r5 = "supported"
            java.lang.Boolean r2 = com.applovin.impl.sdk.utils.j.a((org.json.JSONObject) r2, (java.lang.String) r5, (java.lang.Boolean) r4, (com.applovin.impl.sdk.l) r12)
            boolean r2 = r2.booleanValue()
            r10.h = r2
            r2 = 0
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r2)
            java.lang.String r5 = "test_mode_requires_init"
            java.lang.Boolean r4 = com.applovin.impl.sdk.utils.j.a((org.json.JSONObject) r11, (java.lang.String) r5, (java.lang.Boolean) r4, (com.applovin.impl.sdk.l) r12)
            boolean r4 = r4.booleanValue()
            r10.i = r4
            java.lang.String r4 = "existence_class"
            java.lang.String r4 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r4, (java.lang.String) r0, (com.applovin.impl.sdk.l) r12)
            boolean r4 = com.applovin.impl.sdk.utils.r.e((java.lang.String) r4)
            r10.d = r4
            java.util.List r4 = java.util.Collections.emptyList()
            java.lang.String r5 = r10.l
            com.applovin.mediation.adapter.MaxAdapter r5 = com.applovin.impl.mediation.c.c.a(r5, r12)
            if (r5 == 0) goto L_0x00d4
            r10.e = r3
            java.lang.String r6 = r5.getAdapterVersion()     // Catch:{ all -> 0x00b0 }
            java.lang.String r7 = r5.getSdkVersion()     // Catch:{ all -> 0x00ad }
            if (r7 == 0) goto L_0x00a1
            java.lang.String r7 = r5.getSdkVersion()     // Catch:{ all -> 0x00ad }
            goto L_0x00a2
        L_0x00a1:
            r7 = r0
        L_0x00a2:
            java.util.List r4 = r10.a((com.applovin.mediation.adapter.MaxAdapter) r5)     // Catch:{ all -> 0x00ab }
            boolean r5 = r5.isBeta()     // Catch:{ all -> 0x00ab }
            goto L_0x00d9
        L_0x00ab:
            r5 = move-exception
            goto L_0x00b3
        L_0x00ad:
            r5 = move-exception
            r7 = r0
            goto L_0x00b3
        L_0x00b0:
            r5 = move-exception
            r6 = r0
            r7 = r6
        L_0x00b3:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Failed to load adapter for network "
            r8.append(r9)
            java.lang.String r9 = r10.j
            r8.append(r9)
            java.lang.String r9 = ". Please check that you have a compatible network SDK integrated. Error: "
            r8.append(r9)
            r8.append(r5)
            java.lang.String r5 = r8.toString()
            java.lang.String r8 = "MediatedNetwork"
            com.applovin.impl.sdk.r.i(r8, r5)
            goto L_0x00d8
        L_0x00d4:
            r10.e = r2
            r6 = r0
            r7 = r6
        L_0x00d8:
            r5 = 0
        L_0x00d9:
            r10.n = r6
            r10.m = r7
            r10.q = r4
            r4 = 0
            java.lang.String r7 = "alternative_network"
            org.json.JSONObject r11 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r7, (org.json.JSONObject) r4, (com.applovin.impl.sdk.l) r12)
            java.lang.String r11 = com.applovin.impl.sdk.utils.j.b((org.json.JSONObject) r11, (java.lang.String) r1, (java.lang.String) r0, (com.applovin.impl.sdk.l) r12)
            boolean r11 = com.applovin.impl.sdk.utils.r.e((java.lang.String) r11)
            r10.g = r11
            com.applovin.impl.mediation.debugger.a.b.b$a r11 = r10.u()
            r10.b = r11
            java.lang.String r11 = r10.o
            boolean r11 = r6.equals(r11)
            if (r11 != 0) goto L_0x0101
            if (r5 != 0) goto L_0x0101
            goto L_0x0102
        L_0x0101:
            r3 = 0
        L_0x0102:
            r10.f = r3
            android.content.Context r11 = r12.i()
            java.lang.String r12 = r10.j
            java.lang.String r0 = "_"
            int r12 = r12.lastIndexOf(r0)
            r0 = -1
            if (r12 == r0) goto L_0x011e
            java.lang.String r0 = r10.j
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r12 = r0.substring(r2, r12)
            goto L_0x0124
        L_0x011e:
            java.lang.String r12 = r10.j
            java.lang.String r12 = r12.toLowerCase()
        L_0x0124:
            android.content.res.Resources r0 = r11.getResources()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "applovin_ic_mediation_"
            r1.append(r2)
            r1.append(r12)
            java.lang.String r12 = r1.toString()
            java.lang.String r1 = r11.getPackageName()
            java.lang.String r2 = "drawable"
            int r12 = r0.getIdentifier(r12, r2, r1)
            r10.p = r12
            com.applovin.mediation.adapter.MaxAdapter$InitializationStatus r12 = com.applovin.mediation.adapter.MaxAdapter.InitializationStatus.INITIALIZED_UNKNOWN
            int r12 = r12.getCode()
            r10.c = r12
            com.applovin.communicator.AppLovinCommunicator r11 = com.applovin.communicator.AppLovinCommunicator.getInstance(r11)
            java.lang.String r12 = "adapter_initialization_status"
            r11.subscribe((com.applovin.communicator.AppLovinCommunicatorSubscriber) r10, (java.lang.String) r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.mediation.debugger.a.b.b.<init>(org.json.JSONObject, com.applovin.impl.sdk.l):void");
    }

    private List<MaxAdFormat> a(MaxAdapter maxAdapter) {
        ArrayList arrayList = new ArrayList(5);
        if (maxAdapter instanceof MaxInterstitialAdapter) {
            arrayList.add(MaxAdFormat.INTERSTITIAL);
        }
        if (maxAdapter instanceof MaxRewardedAdapter) {
            arrayList.add(MaxAdFormat.REWARDED);
        }
        if (maxAdapter instanceof MaxRewardedInterstitialAdapter) {
            arrayList.add(MaxAdFormat.REWARDED_INTERSTITIAL);
        }
        if (maxAdapter instanceof MaxAdViewAdapter) {
            arrayList.add(MaxAdFormat.BANNER);
            arrayList.add(MaxAdFormat.LEADER);
            arrayList.add(MaxAdFormat.MREC);
        }
        return arrayList;
    }

    private List<String> a(JSONObject jSONObject) {
        return j.a(j.b(jSONObject, "supported_regions", (JSONArray) null, this.f1650a), (List) null);
    }

    private List<d> a(JSONObject jSONObject, l lVar) {
        ArrayList arrayList = new ArrayList();
        JSONObject b2 = j.b(jSONObject, "permissions", new JSONObject(), lVar);
        Iterator<String> keys = b2.keys();
        while (keys.hasNext()) {
            try {
                String next = keys.next();
                arrayList.add(new d(next, b2.getString(next), lVar.i()));
            } catch (JSONException unused) {
            }
        }
        return arrayList;
    }

    private List<a> b(JSONObject jSONObject, l lVar) {
        ArrayList arrayList = new ArrayList();
        JSONArray b2 = j.b(jSONObject, "dependencies", new JSONArray(), lVar);
        for (int i2 = 0; i2 < b2.length(); i2++) {
            JSONObject a2 = j.a(b2, i2, (JSONObject) null, lVar);
            if (a2 != null) {
                arrayList.add(new a(a2, lVar));
            }
        }
        return arrayList;
    }

    private a u() {
        if (!this.d && !this.e) {
            return a.MISSING;
        }
        for (d c2 : this.r) {
            if (!c2.c()) {
                return a.INVALID_INTEGRATION;
            }
        }
        for (a c3 : this.s) {
            if (!c3.c()) {
                return a.INVALID_INTEGRATION;
            }
        }
        if (this.u.a() && !this.u.b()) {
            return a.INVALID_INTEGRATION;
        }
        if (this.d) {
            if (this.e) {
                return a.COMPLETE;
            }
            if (this.g) {
                return a.MISSING;
            }
        }
        return a.INCOMPLETE_INTEGRATION;
    }

    /* renamed from: a */
    public int compareTo(b bVar) {
        return this.k.compareToIgnoreCase(bVar.k);
    }

    public a a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public C0011b c() {
        return !this.h ? C0011b.NOT_SUPPORTED : this.b == a.INVALID_INTEGRATION ? C0011b.INVALID_INTEGRATION : !this.f1650a.g().a() ? C0011b.DISABLED : (!this.i || !(this.c == MaxAdapter.InitializationStatus.INITIALIZED_FAILURE.getCode() || this.c == MaxAdapter.InitializationStatus.INITIALIZING.getCode())) ? C0011b.READY : C0011b.NOT_INITIALIZED;
    }

    public boolean d() {
        return this.d;
    }

    public boolean e() {
        return this.e;
    }

    public boolean f() {
        return this.f;
    }

    public String g() {
        return this.j;
    }

    public String getCommunicatorId() {
        return "MediatedNetwork";
    }

    public String h() {
        return this.k;
    }

    public String i() {
        return this.m;
    }

    public String j() {
        return this.n;
    }

    public String k() {
        return this.o;
    }

    public String l() {
        return this.l;
    }

    public List<String> m() {
        return this.t;
    }

    public int n() {
        return this.p;
    }

    public List<MaxAdFormat> o() {
        return this.q;
    }

    public void onMessageReceived(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        if (this.l.equals(appLovinCommunicatorMessage.getMessageData().getString("adapter_class", ""))) {
            this.c = appLovinCommunicatorMessage.getMessageData().getInt("init_status", 0);
        }
    }

    public List<d> p() {
        return this.r;
    }

    public List<a> q() {
        return this.s;
    }

    public final c r() {
        return this.u;
    }

    public final l s() {
        return this.f1650a;
    }

    public final String t() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n---------- ");
        sb.append(this.j);
        sb.append(" ----------");
        sb.append("\nStatus  - ");
        sb.append(this.b.a());
        sb.append("\nSDK     - ");
        String str = "UNAVAILABLE";
        sb.append((!this.d || TextUtils.isEmpty(this.m)) ? str : this.m);
        sb.append("\nAdapter - ");
        if (this.e && !TextUtils.isEmpty(this.n)) {
            str = this.n;
        }
        sb.append(str);
        if (this.u.a() && !this.u.b()) {
            sb.append("\n* ");
            sb.append(this.u.c());
        }
        for (d next : p()) {
            if (!next.c()) {
                sb.append("\n* MISSING ");
                sb.append(next.a());
                sb.append(": ");
                sb.append(next.b());
            }
        }
        for (a next2 : q()) {
            if (!next2.c()) {
                sb.append("\n* MISSING ");
                sb.append(next2.a());
                sb.append(": ");
                sb.append(next2.b());
            }
        }
        return sb.toString();
    }

    public String toString() {
        return "MediatedNetwork{name=" + this.j + ", displayName=" + this.k + ", sdkAvailable=" + this.d + ", sdkVersion=" + this.m + ", adapterAvailable=" + this.e + ", adapterVersion=" + this.n + "}";
    }
}
