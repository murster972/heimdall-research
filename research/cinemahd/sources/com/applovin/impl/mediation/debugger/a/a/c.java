package com.applovin.impl.mediation.debugger.a.a;

import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f1647a;
    private final List<b> b;
    private final List<b> c;

    c(JSONObject jSONObject, Map<String, b> map, l lVar) {
        j.b(jSONObject, MediationMetaData.KEY_NAME, "", lVar);
        this.f1647a = j.a(jSONObject, "default", (Boolean) false, lVar).booleanValue();
        this.b = a("bidders", jSONObject, map, lVar);
        this.c = a("waterfall", jSONObject, map, lVar);
    }

    private List<b> a(String str, JSONObject jSONObject, Map<String, b> map, l lVar) {
        ArrayList arrayList = new ArrayList();
        JSONArray b2 = j.b(jSONObject, str, new JSONArray(), lVar);
        for (int i = 0; i < b2.length(); i++) {
            JSONObject a2 = j.a(b2, i, (JSONObject) null, lVar);
            if (a2 != null) {
                String b3 = j.b(a2, "adapter_class", "", lVar);
                b bVar = map.get(b3);
                if (bVar == null) {
                    r j0 = lVar.j0();
                    j0.e("AdUnitWaterfall", "Failed to retrieve network info for adapter class: " + b3);
                } else {
                    arrayList.add(new b(a2, bVar, lVar));
                }
            }
        }
        return arrayList;
    }

    public List<b> a() {
        return this.b;
    }

    public List<b> b() {
        return this.c;
    }

    public boolean c() {
        return this.f1647a;
    }
}
