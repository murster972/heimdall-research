package com.applovin.impl.mediation.debugger.ui.testmode;

import android.content.Context;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.applovin.impl.mediation.c.c;
import com.applovin.impl.mediation.debugger.a.b.b;
import com.applovin.impl.mediation.debugger.ui.testmode.AdControlButton;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.applovin.mediation.ads.MaxRewardedInterstitialAd;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.R$id;
import com.applovin.sdk.R$layout;

public class a extends com.applovin.impl.mediation.debugger.ui.a implements AdControlButton.a, MaxAdViewAdListener, MaxRewardedAdListener {

    /* renamed from: a  reason: collision with root package name */
    private b f1688a;
    private l b;
    private MaxAdView c;
    private MaxAdView d;
    private MaxInterstitialAd e;
    private MaxRewardedInterstitialAd f;
    private MaxRewardedAd g;
    private String h;
    private AdControlButton i;
    private AdControlButton j;
    private AdControlButton k;
    private AdControlButton l;
    private AdControlButton m;

    private AdControlButton a(String str) {
        if (str.equals("test_mode_banner") || str.equals("test_mode_leader")) {
            return this.i;
        }
        if (str.equals("test_mode_mrec")) {
            return this.j;
        }
        if (str.equals("test_mode_interstitial")) {
            return this.k;
        }
        if (str.equals("test_mode_rewarded_interstitial")) {
            return this.l;
        }
        if (str.equals(this.h)) {
            return this.m;
        }
        throw new IllegalArgumentException("Invalid test mode ad unit identifier provided " + str);
    }

    private void a() {
        String str;
        MaxAdFormat maxAdFormat;
        boolean isTablet = AppLovinSdkUtils.isTablet(this);
        FrameLayout frameLayout = (FrameLayout) findViewById(R$id.banner_ad_view_container);
        if (isTablet) {
            maxAdFormat = MaxAdFormat.LEADER;
            ((TextView) findViewById(R$id.banner_label)).setText("Leader");
            str = "test_mode_leader";
        } else {
            maxAdFormat = MaxAdFormat.BANNER;
            str = "test_mode_banner";
        }
        if (this.f1688a.o().contains(maxAdFormat)) {
            this.c = new MaxAdView(str, maxAdFormat, this.b.v(), this);
            this.c.setListener(this);
            frameLayout.addView(this.c, new FrameLayout.LayoutParams(AppLovinSdkUtils.dpToPx(this, maxAdFormat.getSize().getWidth()), AppLovinSdkUtils.dpToPx(this, maxAdFormat.getSize().getHeight())));
            this.i = (AdControlButton) findViewById(R$id.banner_control_button);
            this.i.setOnClickListener(this);
            this.i.setFormat(maxAdFormat);
            return;
        }
        findViewById(R$id.banner_control_view).setVisibility(8);
        frameLayout.setVisibility(8);
    }

    private void a(MaxAdFormat maxAdFormat) {
        MaxAdView maxAdView;
        this.b.g().a(this.f1688a.g(), false);
        if (MaxAdFormat.BANNER == maxAdFormat || MaxAdFormat.LEADER == maxAdFormat) {
            maxAdView = this.c;
        } else if (MaxAdFormat.MREC == maxAdFormat) {
            maxAdView = this.d;
        } else if (MaxAdFormat.INTERSTITIAL == maxAdFormat) {
            this.e.loadAd();
            return;
        } else if (MaxAdFormat.REWARDED_INTERSTITIAL == maxAdFormat) {
            this.f.loadAd();
            return;
        } else if (MaxAdFormat.REWARDED == maxAdFormat) {
            this.g.loadAd();
            return;
        } else {
            return;
        }
        maxAdView.loadAd();
    }

    private void b() {
        FrameLayout frameLayout = (FrameLayout) findViewById(R$id.mrec_ad_view_container);
        if (this.f1688a.o().contains(MaxAdFormat.MREC)) {
            this.d = new MaxAdView("test_mode_mrec", MaxAdFormat.MREC, this.b.v(), this);
            this.d.setListener(this);
            frameLayout.addView(this.d, new FrameLayout.LayoutParams(-1, -1));
            this.j = (AdControlButton) findViewById(R$id.mrec_control_button);
            this.j.setOnClickListener(this);
            this.j.setFormat(MaxAdFormat.MREC);
            return;
        }
        findViewById(R$id.mrec_control_view).setVisibility(8);
        frameLayout.setVisibility(8);
    }

    private void b(MaxAdFormat maxAdFormat) {
        if (MaxAdFormat.INTERSTITIAL == maxAdFormat) {
            this.e.showAd();
        } else if (MaxAdFormat.REWARDED_INTERSTITIAL == maxAdFormat) {
            this.f.showAd();
        } else if (MaxAdFormat.REWARDED == maxAdFormat) {
            this.g.showAd();
        }
    }

    private void c() {
        if (this.f1688a.o().contains(MaxAdFormat.INTERSTITIAL)) {
            this.e = new MaxInterstitialAd("test_mode_interstitial", this.b.v(), this);
            this.e.setListener(this);
            this.k = (AdControlButton) findViewById(R$id.interstitial_control_button);
            this.k.setOnClickListener(this);
            this.k.setFormat(MaxAdFormat.INTERSTITIAL);
            return;
        }
        findViewById(R$id.interstitial_control_view).setVisibility(8);
    }

    private void d() {
        if (this.f1688a.o().contains(MaxAdFormat.REWARDED)) {
            this.h = "test_mode_rewarded_" + this.f1688a.g();
            this.g = MaxRewardedAd.getInstance(this.h, this.b.v(), this);
            this.g.setListener(this);
            this.m = (AdControlButton) findViewById(R$id.rewarded_control_button);
            this.m.setOnClickListener(this);
            this.m.setFormat(MaxAdFormat.REWARDED);
            return;
        }
        findViewById(R$id.rewarded_control_view).setVisibility(8);
    }

    public void initialize(b bVar) {
        this.f1688a = bVar;
        this.b = bVar.s();
    }

    public void onAdClicked(MaxAd maxAd) {
    }

    public void onAdCollapsed(MaxAd maxAd) {
    }

    public void onAdDisplayFailed(MaxAd maxAd, int i2) {
        AdControlButton a2 = a(maxAd.getAdUnitId());
        a2.setControlState(AdControlButton.b.LOAD);
        r.a("", "Failed to display " + a2.getFormat().getLabel() + " with error code: " + i2, (Context) this);
    }

    public void onAdDisplayed(MaxAd maxAd) {
    }

    public void onAdExpanded(MaxAd maxAd) {
    }

    public void onAdHidden(MaxAd maxAd) {
    }

    public void onAdLoadFailed(String str, int i2) {
        AdControlButton a2 = a(str);
        a2.setControlState(AdControlButton.b.LOAD);
        r.a("", "Failed to load " + a2.getFormat().getLabel() + " with error code: " + i2, (Context) this);
    }

    public void onAdLoaded(MaxAd maxAd) {
        a(maxAd.getAdUnitId()).setControlState(c.d(maxAd.getFormat()) ? AdControlButton.b.LOAD : AdControlButton.b.SHOW);
    }

    public void onClick(AdControlButton adControlButton) {
        if (AdControlButton.b.LOAD == adControlButton.getControlState()) {
            adControlButton.setControlState(AdControlButton.b.LOADING);
            a(adControlButton.getFormat());
        } else if (AdControlButton.b.SHOW == adControlButton.getControlState()) {
            adControlButton.setControlState(AdControlButton.b.LOAD);
            b(adControlButton.getFormat());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.mediation_debugger_multi_ad_activity);
        setTitle(this.f1688a.h() + " Test Ads");
        a();
        b();
        c();
        d();
        findViewById(R$id.rewarded_interstitial_control_view).setVisibility(8);
        try {
            setRequestedOrientation(7);
        } catch (Throwable th) {
            com.applovin.impl.sdk.r.c("AppLovinSdk", "Failed to set portrait orientation", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.b.g().a("", false);
        MaxAdView maxAdView = this.c;
        if (maxAdView != null) {
            maxAdView.destroy();
        }
        MaxAdView maxAdView2 = this.d;
        if (maxAdView2 != null) {
            maxAdView2.destroy();
        }
        MaxInterstitialAd maxInterstitialAd = this.e;
        if (maxInterstitialAd != null) {
            maxInterstitialAd.destroy();
        }
        MaxRewardedAd maxRewardedAd = this.g;
        if (maxRewardedAd != null) {
            maxRewardedAd.destroy();
        }
    }

    public void onRewardedVideoCompleted(MaxAd maxAd) {
    }

    public void onRewardedVideoStarted(MaxAd maxAd) {
    }

    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
    }
}
