package com.applovin.impl.mediation.debugger.ui.a;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.applovin.impl.mediation.debugger.a.a.b;
import com.applovin.impl.mediation.debugger.ui.a;
import com.applovin.impl.mediation.debugger.ui.a.b;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.mediation.debugger.ui.d.d;
import com.applovin.impl.mediation.debugger.ui.testmode.AdControlButton;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxDebuggerAdUnitDetailActivity;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.applovin.mediation.ads.MaxRewardedInterstitialAd;
import com.applovin.sdk.R$id;
import com.applovin.sdk.R$layout;

public class a extends com.applovin.impl.mediation.debugger.ui.a implements AdControlButton.a, MaxAdViewAdListener, MaxRewardedAdListener {

    /* renamed from: a  reason: collision with root package name */
    private l f1656a;
    private com.applovin.impl.mediation.debugger.a.a.a b;
    private b c;
    private b d;
    /* access modifiers changed from: private */
    public MaxAdView e;
    private MaxInterstitialAd f;
    private MaxRewardedInterstitialAd g;
    private MaxRewardedAd h;
    /* access modifiers changed from: private */
    public d i;
    private ListView j;
    private View k;
    private AdControlButton l;
    private TextView m;

    private void a() {
        String a2 = this.b.a();
        if (this.b.d().isAdViewAd()) {
            this.e = new MaxAdView(a2, this.b.d(), this.f1656a.v(), this);
            this.e.setListener(this);
        } else if (MaxAdFormat.INTERSTITIAL == this.b.d()) {
            this.f = new MaxInterstitialAd(a2, this.f1656a.v(), this);
            this.f.setListener(this);
        } else if (MaxAdFormat.REWARDED_INTERSTITIAL == this.b.d()) {
            this.g = new MaxRewardedInterstitialAd(a2, this.f1656a.v(), this);
            this.g.setListener(this);
        } else if (MaxAdFormat.REWARDED == this.b.d()) {
            this.h = MaxRewardedAd.getInstance(a2, this.f1656a.v(), this);
            this.h.setListener(this);
        }
    }

    private void a(DialogInterface.OnShowListener onShowListener) {
        if (this.i == null) {
            this.i = new d(this.e, this.b.d(), this);
            this.i.setOnShowListener(onShowListener);
            this.i.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    a.this.e.stopAutoRefresh();
                    d unused = a.this.i = null;
                }
            });
            this.i.show();
        }
    }

    private void a(MaxAdFormat maxAdFormat) {
        if (this.d != null) {
            this.f1656a.g().a(this.d.b(), false);
            this.f1656a.g().a(true);
        }
        if (maxAdFormat.isAdViewAd()) {
            this.e.loadAd();
        } else if (MaxAdFormat.INTERSTITIAL == this.b.d()) {
            this.f.loadAd();
        } else if (MaxAdFormat.REWARDED_INTERSTITIAL == this.b.d()) {
            this.g.loadAd();
        } else if (MaxAdFormat.REWARDED == this.b.d()) {
            this.h.loadAd();
        }
    }

    private void b(MaxAdFormat maxAdFormat) {
        if (maxAdFormat.isAdViewAd()) {
            a((DialogInterface.OnShowListener) new DialogInterface.OnShowListener() {
                public void onShow(DialogInterface dialogInterface) {
                    a.this.e.startAutoRefresh();
                }
            });
        } else if (MaxAdFormat.INTERSTITIAL == this.b.d()) {
            this.f.showAd();
        } else if (MaxAdFormat.REWARDED_INTERSTITIAL == this.b.d()) {
            this.g.showAd();
        } else if (MaxAdFormat.REWARDED == this.b.d()) {
            this.h.showAd();
        }
    }

    public void initialize(final com.applovin.impl.mediation.debugger.a.a.a aVar, b bVar, final l lVar) {
        this.f1656a = lVar;
        this.b = aVar;
        this.d = bVar;
        this.c = new b(aVar, bVar, this);
        this.c.a((d.a) new d.a() {
            public void a(com.applovin.impl.mediation.debugger.ui.d.a aVar, final c cVar) {
                if (cVar instanceof b.a) {
                    a.this.startActivity(MaxDebuggerAdUnitDetailActivity.class, lVar.D(), new a.C0012a<MaxDebuggerAdUnitDetailActivity>() {
                        public void a(MaxDebuggerAdUnitDetailActivity maxDebuggerAdUnitDetailActivity) {
                            com.applovin.impl.mediation.debugger.a.a.b q = ((b.a) cVar).q();
                            AnonymousClass1 r1 = AnonymousClass1.this;
                            maxDebuggerAdUnitDetailActivity.initialize(aVar, q, lVar);
                        }
                    });
                }
            }
        });
        a();
    }

    public void onAdClicked(MaxAd maxAd) {
    }

    public void onAdCollapsed(MaxAd maxAd) {
    }

    public void onAdDisplayFailed(MaxAd maxAd, int i2) {
        this.l.setControlState(AdControlButton.b.LOAD);
        this.m.setText("");
        r.a("", "Failed to display with error code: " + i2, (Context) this);
    }

    public void onAdDisplayed(MaxAd maxAd) {
    }

    public void onAdExpanded(MaxAd maxAd) {
    }

    public void onAdHidden(MaxAd maxAd) {
    }

    public void onAdLoadFailed(String str, int i2) {
        this.l.setControlState(AdControlButton.b.LOAD);
        this.m.setText("");
        if (204 == i2) {
            r.a("No Fill", "No fills often happen in live environments. Please make sure to use the Mediation Debugger test mode before you go live.", (Context) this);
            return;
        }
        r.a("", "Failed to load with error code: " + i2, (Context) this);
    }

    public void onAdLoaded(MaxAd maxAd) {
        TextView textView = this.m;
        textView.setText(maxAd.getNetworkName() + " ad loaded");
        this.l.setControlState(AdControlButton.b.SHOW);
        if (maxAd.getFormat().isAdViewAd()) {
            a((DialogInterface.OnShowListener) null);
        }
    }

    public void onClick(AdControlButton adControlButton) {
        if (this.f1656a.g().a()) {
            r.a("Not Supported", "Ad loads are not supported while Test Mode is enabled. Please restart the app.", (Context) this);
        } else if (AdControlButton.b.LOAD == adControlButton.getControlState()) {
            adControlButton.setControlState(AdControlButton.b.LOADING);
            a(this.b.d());
        } else if (AdControlButton.b.SHOW == adControlButton.getControlState()) {
            if (!this.b.d().isAdViewAd()) {
                adControlButton.setControlState(AdControlButton.b.LOAD);
            }
            b(this.b.d());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.mediation_debugger_ad_unit_detail_activity);
        setTitle(this.c.b());
        this.j = (ListView) findViewById(R$id.listView);
        this.k = findViewById(R$id.ad_presenter_view);
        this.l = (AdControlButton) findViewById(R$id.ad_control_button);
        this.m = (TextView) findViewById(R$id.status_textview);
        this.j.setAdapter(this.c);
        this.m.setText(this.f1656a.g().a() ? "Not Supported while Test Mode is enabled" : "Tap to load an ad");
        this.m.setTypeface(Typeface.DEFAULT_BOLD);
        this.l.setOnClickListener(this);
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.setPadding(0, 10, 0, 0);
        shapeDrawable.getPaint().setColor(-1);
        shapeDrawable.getPaint().setShadowLayer((float) 10, 0.0f, (float) -10, 855638016);
        shapeDrawable.setShape(new RectShape());
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{shapeDrawable});
        layerDrawable.setLayerInset(0, 0, 10, 0, 0);
        this.k.setBackground(layerDrawable);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.d != null) {
            this.f1656a.g().a("", false);
            this.f1656a.g().a(false);
        }
        MaxAdView maxAdView = this.e;
        if (maxAdView != null) {
            maxAdView.destroy();
        }
        MaxInterstitialAd maxInterstitialAd = this.f;
        if (maxInterstitialAd != null) {
            maxInterstitialAd.destroy();
        }
        MaxRewardedAd maxRewardedAd = this.h;
        if (maxRewardedAd != null) {
            maxRewardedAd.destroy();
        }
    }

    public void onRewardedVideoCompleted(MaxAd maxAd) {
    }

    public void onRewardedVideoStarted(MaxAd maxAd) {
    }

    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
    }
}
