package com.applovin.impl.mediation.debugger.a.a;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final com.applovin.impl.mediation.debugger.a.b.b f1646a;
    private final String b;
    private final String c;
    private final d d;
    private final List<d> e;

    public b(JSONObject jSONObject, com.applovin.impl.mediation.debugger.a.b.b bVar, l lVar) {
        this.f1646a = bVar;
        this.b = j.b(jSONObject, MediationMetaData.KEY_NAME, "", lVar);
        this.c = j.b(jSONObject, "display_name", "", lVar);
        JSONObject b2 = j.b(jSONObject, "bidder_placement", (JSONObject) null, lVar);
        if (b2 != null) {
            this.d = new d(b2, lVar);
        } else {
            this.d = null;
        }
        JSONArray b3 = j.b(jSONObject, "placements", new JSONArray(), lVar);
        this.e = new ArrayList(b3.length());
        for (int i = 0; i < b3.length(); i++) {
            JSONObject a2 = j.a(b3, i, (JSONObject) null, lVar);
            if (a2 != null) {
                this.e.add(new d(a2, lVar));
            }
        }
    }

    public com.applovin.impl.mediation.debugger.a.b.b a() {
        return this.f1646a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public d d() {
        return this.d;
    }

    public boolean e() {
        return this.d != null;
    }

    public List<d> f() {
        return this.e;
    }
}
