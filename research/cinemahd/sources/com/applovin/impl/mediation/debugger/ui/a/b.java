package com.applovin.impl.mediation.debugger.ui.a;

import android.content.Context;
import android.text.SpannedString;
import android.text.TextUtils;
import com.applovin.impl.mediation.debugger.ui.d.c;
import com.applovin.impl.mediation.debugger.ui.d.d;
import com.applovin.impl.mediation.debugger.ui.d.e;
import com.applovin.impl.sdk.utils.o;
import java.util.ArrayList;
import java.util.List;

class b extends d {
    private final com.applovin.impl.mediation.debugger.a.a.a f;
    private final com.applovin.impl.mediation.debugger.a.a.b g;
    private final List<c> h = c();
    private final List<c> i = d();
    private final List<c> j = e();

    class a extends com.applovin.impl.mediation.debugger.ui.b.a.a {
        private final com.applovin.impl.mediation.debugger.a.a.b o;

        a(b bVar, com.applovin.impl.mediation.debugger.a.a.b bVar2, String str, boolean z) {
            super(bVar2.a(), bVar.b);
            this.o = bVar2;
            this.c = o.a(bVar2.c(), -16777216, 18, 1);
            this.d = !TextUtils.isEmpty(str) ? new SpannedString(str) : null;
            this.b = z;
        }

        public boolean b() {
            return this.b;
        }

        public int d() {
            return -12303292;
        }

        public com.applovin.impl.mediation.debugger.a.a.b q() {
            return this.o;
        }
    }

    /* renamed from: com.applovin.impl.mediation.debugger.ui.a.b$b  reason: collision with other inner class name */
    enum C0013b {
        INFO,
        BIDDERS,
        WATERFALL,
        COUNT
    }

    b(com.applovin.impl.mediation.debugger.a.a.a aVar, com.applovin.impl.mediation.debugger.a.a.b bVar, Context context) {
        super(context);
        this.f = aVar;
        this.g = bVar;
        notifyDataSetChanged();
    }

    private List<c> c() {
        ArrayList arrayList = new ArrayList(2);
        arrayList.add(f());
        arrayList.add(g());
        if (this.g != null) {
            arrayList.add(h());
        }
        return arrayList;
    }

    private List<c> d() {
        com.applovin.impl.mediation.debugger.a.a.b bVar = this.g;
        if (bVar != null && !bVar.e()) {
            return new ArrayList();
        }
        List<com.applovin.impl.mediation.debugger.a.a.b> a2 = this.f.e().a();
        ArrayList arrayList = new ArrayList(a2.size());
        for (com.applovin.impl.mediation.debugger.a.a.b next : a2) {
            com.applovin.impl.mediation.debugger.a.a.b bVar2 = this.g;
            if (bVar2 == null || bVar2.b().equals(next.b())) {
                arrayList.add(new a(this, next, next.d() != null ? next.d().a() : "", this.g == null));
            }
        }
        return arrayList;
    }

    private List<c> e() {
        com.applovin.impl.mediation.debugger.a.a.b bVar = this.g;
        if (bVar != null && bVar.e()) {
            return new ArrayList();
        }
        List<com.applovin.impl.mediation.debugger.a.a.b> b = this.f.e().b();
        ArrayList arrayList = new ArrayList(b.size());
        for (com.applovin.impl.mediation.debugger.a.a.b next : b) {
            com.applovin.impl.mediation.debugger.a.a.b bVar2 = this.g;
            if (bVar2 == null || bVar2.b().equals(next.b())) {
                arrayList.add(new a(this, next, (String) null, this.g == null));
                for (com.applovin.impl.mediation.debugger.a.a.d next2 : next.f()) {
                    c.a o = c.o();
                    o.a(next2.a());
                    o.b(next2.b());
                    o.b(true);
                    arrayList.add(o.a());
                }
            }
        }
        return arrayList;
    }

    private c f() {
        c.a o = c.o();
        o.a("ID");
        o.b(this.f.a());
        return o.a();
    }

    private c g() {
        c.a o = c.o();
        o.a("Ad Format");
        o.b(this.f.c());
        return o.a();
    }

    private c h() {
        c.a o = c.o();
        o.a("Selected Network");
        o.b(this.g.c());
        return o.a();
    }

    /* access modifiers changed from: protected */
    public int a() {
        return C0013b.COUNT.ordinal();
    }

    /* access modifiers changed from: protected */
    public int a(int i2) {
        return (i2 == C0013b.INFO.ordinal() ? this.h : i2 == C0013b.BIDDERS.ordinal() ? this.i : this.j).size();
    }

    /* access modifiers changed from: protected */
    public c b(int i2) {
        return i2 == C0013b.INFO.ordinal() ? new e("INFO") : i2 == C0013b.BIDDERS.ordinal() ? new e("BIDDERS") : new e("WATERFALL");
    }

    public String b() {
        return this.f.b();
    }

    /* access modifiers changed from: protected */
    public List<c> c(int i2) {
        return i2 == C0013b.INFO.ordinal() ? this.h : i2 == C0013b.BIDDERS.ordinal() ? this.i : this.j;
    }
}
