package com.applovin.impl.mediation.debugger.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.R$style;

public class a extends Activity {

    /* renamed from: com.applovin.impl.mediation.debugger.ui.a$a  reason: collision with other inner class name */
    public interface C0012a<T extends Activity> {
        void a(T t);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (!r.e((Context) this)) {
            setTheme(R$style.com_applovin_mediation_MaxDebuggerActivity_Theme_Live);
        }
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void startActivity(final Class cls, final com.applovin.impl.sdk.a aVar, final C0012a aVar2) {
        aVar.a(new com.applovin.impl.sdk.utils.a(this) {
            public void onActivityCreated(Activity activity, Bundle bundle) {
                if (cls.isInstance(activity)) {
                    aVar2.a(activity);
                    aVar.b(this);
                }
            }
        });
        startActivity(new Intent(this, cls));
    }
}
