package com.applovin.impl.mediation.debugger.b;

import android.os.Build;
import com.applovin.impl.mediation.c.c;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.e.t;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.e.a {
    /* access modifiers changed from: private */
    public final a.c<JSONObject> f;

    public a(a.c<JSONObject> cVar, l lVar) {
        super("TaskFetchMediationDebuggerInfo", lVar, true);
        this.f = cVar;
    }

    private JSONObject a(l lVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("installed_mediation_adapters", c.a(lVar));
        } catch (JSONException e) {
            a("Failed to create mediation debugger request post body", e);
        }
        return jSONObject;
    }

    private JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("installed_mediation_adapters", c.a(this.f1817a));
        } catch (JSONException e) {
            a("Failed to construct JSON body", e);
        }
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> e() {
        HashMap hashMap = new HashMap();
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", String.valueOf(131));
        if (!((Boolean) this.f1817a.a(b.u3)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1817a.h0());
        }
        Map<String, Object> g = this.f1817a.r().g();
        hashMap.put("package_name", String.valueOf(g.get("package_name")));
        hashMap.put("app_version", String.valueOf(g.get("app_version")));
        hashMap.put("platform", "android");
        hashMap.put("os", Build.VERSION.RELEASE);
        return hashMap;
    }

    public void run() {
        AnonymousClass1 r1 = new t<JSONObject>(com.applovin.impl.sdk.network.b.a(this.f1817a).b("POST").a(com.applovin.impl.mediation.c.b.i(this.f1817a)).c(com.applovin.impl.mediation.c.b.j(this.f1817a)).a(e()).a(a(this.f1817a)).a(new JSONObject()).b(((Long) this.f1817a.a(com.applovin.impl.sdk.c.a.l4)).intValue()).a(f()).a(), this.f1817a, d()) {
            public void a(int i) {
                a.this.f.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                a.this.f.a(jSONObject, i);
            }
        };
        r1.a(com.applovin.impl.sdk.c.a.h4);
        r1.b(com.applovin.impl.sdk.c.a.i4);
        this.f1817a.o().a((com.applovin.impl.sdk.e.a) r1);
    }
}
