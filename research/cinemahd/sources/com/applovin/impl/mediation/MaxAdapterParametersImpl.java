package com.applovin.impl.mediation;

import android.content.Context;
import android.os.Bundle;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.mediation.a.e;
import com.applovin.impl.mediation.a.g;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;

public class MaxAdapterParametersImpl implements MaxAdapterInitializationParameters, MaxAdapterResponseParameters, MaxAdapterSignalCollectionParameters {

    /* renamed from: a  reason: collision with root package name */
    private Bundle f1598a;
    private Boolean b;
    private Boolean c;
    private Boolean d;
    private boolean e;
    private String f;
    private String g;
    private MaxAdFormat h;

    private MaxAdapterParametersImpl() {
    }

    static MaxAdapterParametersImpl a(a aVar, Context context) {
        MaxAdapterParametersImpl a2 = a(aVar);
        a2.f = aVar.s();
        a2.g = aVar.r();
        return a2;
    }

    static MaxAdapterParametersImpl a(e eVar) {
        MaxAdapterParametersImpl maxAdapterParametersImpl = new MaxAdapterParametersImpl();
        maxAdapterParametersImpl.b = eVar.g();
        maxAdapterParametersImpl.c = eVar.h();
        maxAdapterParametersImpl.d = eVar.i();
        maxAdapterParametersImpl.f1598a = eVar.k();
        maxAdapterParametersImpl.e = eVar.f();
        return maxAdapterParametersImpl;
    }

    static MaxAdapterParametersImpl a(g gVar, MaxAdFormat maxAdFormat, Context context) {
        MaxAdapterParametersImpl a2 = a(gVar);
        a2.h = maxAdFormat;
        return a2;
    }

    public MaxAdFormat getAdFormat() {
        return this.h;
    }

    public String getBidResponse() {
        return this.g;
    }

    public Bundle getServerParameters() {
        return this.f1598a;
    }

    public String getThirdPartyAdPlacementId() {
        return this.f;
    }

    public Boolean hasUserConsent() {
        return this.b;
    }

    public Boolean isAgeRestrictedUser() {
        return this.c;
    }

    public Boolean isDoNotSell() {
        return this.d;
    }

    public boolean isTesting() {
        return this.e;
    }
}
