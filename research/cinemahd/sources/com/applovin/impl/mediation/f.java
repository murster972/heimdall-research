package com.applovin.impl.mediation;

import android.os.Bundle;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f1690a;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Bundle f1691a;

        public a() {
            this((f) null);
        }

        public a(f fVar) {
            this.f1691a = new Bundle();
            if (fVar != null) {
                for (String str : fVar.a().keySet()) {
                    a(str, fVar.a().getString(str));
                }
            }
        }

        public a a(String str) {
            if (str != null) {
                this.f1691a.remove(str);
                return this;
            }
            throw new IllegalArgumentException("No key specified.");
        }

        public a a(String str, String str2) {
            if (str != null) {
                this.f1691a.putString(str, str2);
                return this;
            }
            throw new IllegalArgumentException("No key specified");
        }

        public f a() {
            return new f(this);
        }
    }

    private f(a aVar) {
        this.f1690a = new Bundle(aVar.f1691a);
    }

    public Bundle a() {
        return this.f1690a;
    }

    public String toString() {
        return "MediatedRequestParameters{extraParameters=" + this.f1690a + '}';
    }
}
