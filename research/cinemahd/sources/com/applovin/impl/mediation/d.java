package com.applovin.impl.mediation;

import android.os.Bundle;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxRewardedAdListener;

public interface d extends MaxAdListener, MaxAdViewAdListener, MaxRewardedAdListener {
    void a(MaxAd maxAd, Bundle bundle);

    void a(MaxAd maxAd, e eVar);

    void a(MaxAdListener maxAdListener);

    void a(String str, e eVar);

    void b(MaxAd maxAd, Bundle bundle);
}
