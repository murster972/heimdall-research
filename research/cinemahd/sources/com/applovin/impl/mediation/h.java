package com.applovin.impl.mediation;

import android.app.Activity;
import com.applovin.impl.mediation.a.e;
import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final l f1697a;
    private final r b;
    private final AtomicBoolean c = new AtomicBoolean();
    private final JSONArray d = new JSONArray();
    private final LinkedHashSet<String> e = new LinkedHashSet<>();
    private final Object f = new Object();
    private boolean g;

    public h(l lVar) {
        this.f1697a = lVar;
        this.b = lVar.j0();
    }

    public void a(Activity activity) {
        boolean z = true;
        if (this.c.compareAndSet(false, true)) {
            if (activity != null) {
                z = false;
            }
            this.g = z;
            this.f1697a.o().a((a) new com.applovin.impl.mediation.b.a(activity, this.f1697a));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar, long j, MaxAdapter.InitializationStatus initializationStatus, String str) {
        boolean z;
        if (initializationStatus != null && initializationStatus != MaxAdapter.InitializationStatus.INITIALIZING) {
            synchronized (this.f) {
                z = !a(eVar);
                if (z) {
                    this.e.add(eVar.c());
                    JSONObject jSONObject = new JSONObject();
                    j.a(jSONObject, "class", eVar.c(), this.f1697a);
                    j.a(jSONObject, "init_status", String.valueOf(initializationStatus.getCode()), this.f1697a);
                    j.a(jSONObject, "error_message", JSONObject.quote(str), this.f1697a);
                    this.d.put(jSONObject);
                }
            }
            if (z) {
                this.f1697a.a(eVar);
                this.f1697a.b().maybeScheduleAdapterInitializationPostback(eVar, j, initializationStatus, str);
                this.f1697a.F().a(initializationStatus, eVar.c());
            }
        }
    }

    public void a(e eVar, Activity activity) {
        j a2 = this.f1697a.k0().a(eVar);
        if (a2 != null) {
            r rVar = this.b;
            rVar.c("MediationAdapterInitializationManager", "Initializing adapter " + eVar);
            a2.a((MaxAdapterInitializationParameters) MaxAdapterParametersImpl.a(eVar), activity);
        }
    }

    public boolean a() {
        return this.c.get();
    }

    /* access modifiers changed from: package-private */
    public boolean a(e eVar) {
        boolean contains;
        synchronized (this.f) {
            contains = this.e.contains(eVar.c());
        }
        return contains;
    }

    public boolean b() {
        return this.g;
    }

    public LinkedHashSet<String> c() {
        LinkedHashSet<String> linkedHashSet;
        synchronized (this.f) {
            linkedHashSet = this.e;
        }
        return linkedHashSet;
    }

    public JSONArray d() {
        JSONArray jSONArray;
        synchronized (this.f) {
            jSONArray = this.d;
        }
        return jSONArray;
    }
}
