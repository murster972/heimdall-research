package com.applovin.impl.mediation.ads;

import com.applovin.impl.mediation.f;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdk;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private static l f1625a;
    protected final MaxAdFormat adFormat;
    protected MaxAdListener adListener = null;
    protected final String adUnitId;
    protected final f.a loadRequestBuilder;
    protected final r logger;
    protected final l sdk;
    protected final String tag;

    protected a(String str, MaxAdFormat maxAdFormat, String str2, l lVar) {
        this.adUnitId = str;
        this.adFormat = maxAdFormat;
        this.sdk = lVar;
        this.tag = str2;
        this.logger = lVar.j0();
        this.loadRequestBuilder = new f.a();
    }

    public static void logApiCall(String str, String str2) {
        l lVar = f1625a;
        if (lVar != null) {
            lVar.j0().b(str, str2);
            return;
        }
        for (AppLovinSdk appLovinSdk : AppLovinSdk.a()) {
            l lVar2 = appLovinSdk.coreSdk;
            if (!lVar2.P()) {
                lVar2.j0().b(str, str2);
                f1625a = lVar2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(com.applovin.impl.mediation.a.a aVar) {
        com.applovin.impl.sdk.utils.l lVar = new com.applovin.impl.sdk.utils.l();
        lVar.a().a("MAX Ad").a(aVar).a();
        r.f(this.tag, lVar.toString());
    }

    public String getAdUnitId() {
        return this.adUnitId;
    }

    public void logApiCall(String str) {
        this.logger.b(this.tag, str);
    }

    public void setExtraParameter(String str, String str2) {
        if (str != null) {
            this.loadRequestBuilder.a(str, str2);
            return;
        }
        throw new IllegalArgumentException("No key specified");
    }

    public void setListener(MaxAdListener maxAdListener) {
        r rVar = this.logger;
        String str = this.tag;
        rVar.b(str, "Setting listener: " + maxAdListener);
        this.adListener = maxAdListener;
    }
}
