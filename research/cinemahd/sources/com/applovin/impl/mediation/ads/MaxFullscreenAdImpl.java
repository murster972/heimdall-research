package com.applovin.impl.mediation.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.SystemClock;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.sdk.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.k;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class MaxFullscreenAdImpl extends a implements b.a {
    private final a b;
    /* access modifiers changed from: private */
    public final com.applovin.impl.sdk.b c;
    /* access modifiers changed from: private */
    public final com.applovin.impl.mediation.b d;
    /* access modifiers changed from: private */
    public final Object e = new Object();
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.a.c f = null;
    private c g = c.IDLE;
    /* access modifiers changed from: private */
    public final AtomicBoolean h = new AtomicBoolean();
    protected final b listenerWrapper;

    public interface a {
        Activity getActivity();
    }

    private class b implements MaxAdListener, MaxRewardedAdListener {
        private b() {
        }

        public void onAdClicked(MaxAd maxAd) {
            k.d(MaxFullscreenAdImpl.this.adListener, maxAd);
        }

        public void onAdDisplayFailed(final MaxAd maxAd, final int i) {
            MaxFullscreenAdImpl.this.a(c.IDLE, (Runnable) new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.c.a();
                    MaxFullscreenAdImpl.this.a();
                    MaxFullscreenAdImpl.this.sdk.c().b((com.applovin.impl.mediation.a.a) maxAd);
                    k.a(MaxFullscreenAdImpl.this.adListener, maxAd, i);
                }
            });
        }

        public void onAdDisplayed(MaxAd maxAd) {
            MaxFullscreenAdImpl.this.c.a();
            k.b(MaxFullscreenAdImpl.this.adListener, maxAd);
        }

        public void onAdHidden(final MaxAd maxAd) {
            MaxFullscreenAdImpl.this.d.a(maxAd);
            MaxFullscreenAdImpl.this.a(c.IDLE, (Runnable) new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.a();
                    MaxFullscreenAdImpl.this.sdk.c().b((com.applovin.impl.mediation.a.a) maxAd);
                    k.c(MaxFullscreenAdImpl.this.adListener, maxAd);
                }
            });
        }

        public void onAdLoadFailed(final String str, final int i) {
            MaxFullscreenAdImpl.this.b();
            MaxFullscreenAdImpl.this.a(c.IDLE, (Runnable) new Runnable() {
                public void run() {
                    k.a(MaxFullscreenAdImpl.this.adListener, str, i);
                }
            });
        }

        public void onAdLoaded(final MaxAd maxAd) {
            MaxFullscreenAdImpl.this.a((com.applovin.impl.mediation.a.c) maxAd);
            if (MaxFullscreenAdImpl.this.h.compareAndSet(true, false)) {
                MaxFullscreenAdImpl.this.loadRequestBuilder.a("expired_ad_ad_unit_id");
            } else {
                MaxFullscreenAdImpl.this.a(c.READY, (Runnable) new Runnable() {
                    public void run() {
                        k.a(MaxFullscreenAdImpl.this.adListener, maxAd);
                    }
                });
            }
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            k.f(MaxFullscreenAdImpl.this.adListener, maxAd);
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            k.e(MaxFullscreenAdImpl.this.adListener, maxAd);
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            k.a(MaxFullscreenAdImpl.this.adListener, maxAd, maxReward);
        }
    }

    public enum c {
        IDLE,
        LOADING,
        READY,
        SHOWING,
        DESTROYED
    }

    public MaxFullscreenAdImpl(String str, MaxAdFormat maxAdFormat, a aVar, String str2, l lVar) {
        super(str, maxAdFormat, str2, lVar);
        this.b = aVar;
        this.listenerWrapper = new b();
        this.c = new com.applovin.impl.sdk.b(lVar, this);
        this.d = new com.applovin.impl.mediation.b(lVar, this.listenerWrapper);
        r.f(str2, "Created new " + str2 + " (" + this + ")");
    }

    /* access modifiers changed from: private */
    public void a() {
        com.applovin.impl.mediation.a.c cVar;
        synchronized (this.e) {
            cVar = this.f;
            this.f = null;
        }
        this.sdk.b().destroyAd(cVar);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.a.c cVar) {
        long C = cVar.C() - (SystemClock.elapsedRealtime() - cVar.v());
        if (C > TimeUnit.SECONDS.toMillis(2)) {
            this.f = cVar;
            r rVar = this.logger;
            String str = this.tag;
            rVar.b(str, "Handle ad loaded for regular ad: " + cVar);
            r rVar2 = this.logger;
            String str2 = this.tag;
            rVar2.b(str2, "Scheduling ad expiration " + TimeUnit.MILLISECONDS.toSeconds(C) + " seconds from now for " + getAdUnitId() + "...");
            this.c.a(C);
            return;
        }
        this.logger.b(this.tag, "Loaded an expired ad, running expire logic...");
        onAdExpired();
    }

    private void a(com.applovin.impl.mediation.a.c cVar, Context context, final Runnable runnable) {
        if (cVar == null || !cVar.O() || h.a(context) || !(context instanceof Activity)) {
            runnable.run();
            return;
        }
        AlertDialog create = new AlertDialog.Builder(context).setTitle(cVar.P()).setMessage(cVar.z()).setPositiveButton(cVar.A(), (DialogInterface.OnClickListener) null).create();
        create.setOnDismissListener(new DialogInterface.OnDismissListener(this) {
            public void onDismiss(DialogInterface dialogInterface) {
                runnable.run();
            }
        });
        create.show();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x014e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c r7, java.lang.Runnable r8) {
        /*
            r6 = this;
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = r6.g
            java.lang.Object r1 = r6.e
            monitor-enter(r1)
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0179 }
            r3 = 1
            r4 = 0
            if (r0 != r2) goto L_0x003e
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0012
        L_0x000f:
            r4 = 1
            goto L_0x0122
        L_0x0012:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0017
            goto L_0x000f
        L_0x0017:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0024
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = "No ad is loading or loaded"
        L_0x001f:
            com.applovin.impl.sdk.r.i(r0, r2)     // Catch:{ all -> 0x0179 }
            goto L_0x0122
        L_0x0024:
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0179 }
            r3.<init>()     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            r3.append(r7)     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0179 }
        L_0x0039:
            r0.e(r2, r3)     // Catch:{ all -> 0x0179 }
            goto L_0x0122
        L_0x003e:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0179 }
            if (r0 != r2) goto L_0x0079
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0047
            goto L_0x000f
        L_0x0047:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0050
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = "An ad is already loading"
            goto L_0x001f
        L_0x0050:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0055
            goto L_0x000f
        L_0x0055:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x005e
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = "An ad is not ready to be shown yet"
            goto L_0x001f
        L_0x005e:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0063
            goto L_0x000f
        L_0x0063:
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0179 }
            r3.<init>()     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            r3.append(r7)     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0179 }
            goto L_0x0039
        L_0x0079:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0179 }
            if (r0 != r2) goto L_0x00b8
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0082
            goto L_0x000f
        L_0x0082:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x008b
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = "An ad is already loaded"
            goto L_0x001f
        L_0x008b:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x0096
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = "An ad is already marked as ready"
            goto L_0x0039
        L_0x0096:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x009c
            goto L_0x000f
        L_0x009c:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x00a2
            goto L_0x000f
        L_0x00a2:
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0179 }
            r3.<init>()     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            r3.append(r7)     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0179 }
            goto L_0x0039
        L_0x00b8:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0179 }
            if (r0 != r2) goto L_0x00ff
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x00c2
            goto L_0x000f
        L_0x00c2:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x00cc
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = "Can not load another ad while the ad is showing"
            goto L_0x001f
        L_0x00cc:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x00d8
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = "An ad is already showing, ignoring"
            goto L_0x0039
        L_0x00d8:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x00e2
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = "The ad is already showing, not showing another one"
            goto L_0x001f
        L_0x00e2:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0179 }
            if (r7 != r0) goto L_0x00e8
            goto L_0x000f
        L_0x00e8:
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0179 }
            r3.<init>()     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            r3.append(r7)     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0179 }
            goto L_0x0039
        L_0x00ff:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0179 }
            if (r0 != r2) goto L_0x0109
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = "No operations are allowed on a destroyed instance"
            goto L_0x001f
        L_0x0109:
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0179 }
            r3.<init>()     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "Unknown state: "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r5 = r6.g     // Catch:{ all -> 0x0179 }
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0179 }
            goto L_0x0039
        L_0x0122:
            if (r4 == 0) goto L_0x014e
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0179 }
            r3.<init>()     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "Transitioning from "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r5 = r6.g     // Catch:{ all -> 0x0179 }
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = " to "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            r3.append(r7)     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "..."
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0179 }
            r0.b(r2, r3)     // Catch:{ all -> 0x0179 }
            r6.g = r7     // Catch:{ all -> 0x0179 }
            goto L_0x0170
        L_0x014e:
            com.applovin.impl.sdk.r r0 = r6.logger     // Catch:{ all -> 0x0179 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0179 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0179 }
            r3.<init>()     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = "Not allowed transition from "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r5 = r6.g     // Catch:{ all -> 0x0179 }
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            java.lang.String r5 = " to "
            r3.append(r5)     // Catch:{ all -> 0x0179 }
            r3.append(r7)     // Catch:{ all -> 0x0179 }
            java.lang.String r7 = r3.toString()     // Catch:{ all -> 0x0179 }
            r0.d(r2, r7)     // Catch:{ all -> 0x0179 }
        L_0x0170:
            monitor-exit(r1)     // Catch:{ all -> 0x0179 }
            if (r4 == 0) goto L_0x0178
            if (r8 == 0) goto L_0x0178
            r8.run()
        L_0x0178:
            return
        L_0x0179:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0179 }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.a(com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c, java.lang.Runnable):void");
    }

    /* access modifiers changed from: private */
    public void b() {
        com.applovin.impl.mediation.a.c cVar;
        if (this.h.compareAndSet(true, false)) {
            synchronized (this.e) {
                cVar = this.f;
                this.f = null;
            }
            this.sdk.b().destroyAd(cVar);
            this.loadRequestBuilder.a("expired_ad_ad_unit_id");
        }
    }

    public void destroy() {
        a(c.DESTROYED, (Runnable) new Runnable() {
            public void run() {
                synchronized (MaxFullscreenAdImpl.this.e) {
                    if (MaxFullscreenAdImpl.this.f != null) {
                        r rVar = MaxFullscreenAdImpl.this.logger;
                        String str = MaxFullscreenAdImpl.this.tag;
                        rVar.b(str, "Destroying ad for '" + MaxFullscreenAdImpl.this.adUnitId + "'; current ad: " + MaxFullscreenAdImpl.this.f + "...");
                        MaxFullscreenAdImpl.this.sdk.b().destroyAd(MaxFullscreenAdImpl.this.f);
                    }
                }
            }
        });
    }

    public boolean isReady() {
        boolean z;
        synchronized (this.e) {
            z = this.f != null && this.f.n() && this.g == c.READY;
        }
        return z;
    }

    public void loadAd(final Activity activity) {
        r rVar = this.logger;
        String str = this.tag;
        rVar.b(str, "Loading ad for '" + this.adUnitId + "'...");
        if (isReady()) {
            r rVar2 = this.logger;
            String str2 = this.tag;
            rVar2.b(str2, "An ad is already loaded for '" + this.adUnitId + "'");
            k.a(this.adListener, (MaxAd) this.f);
            return;
        }
        a(c.LOADING, (Runnable) new Runnable() {
            public void run() {
                Activity activity = activity;
                if (activity == null) {
                    activity = MaxFullscreenAdImpl.this.sdk.L();
                }
                Activity activity2 = activity;
                MediationServiceImpl b2 = MaxFullscreenAdImpl.this.sdk.b();
                MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
                b2.loadAd(maxFullscreenAdImpl.adUnitId, maxFullscreenAdImpl.adFormat, maxFullscreenAdImpl.loadRequestBuilder.a(), activity2, MaxFullscreenAdImpl.this.listenerWrapper);
            }
        });
    }

    public void onAdExpired() {
        r rVar = this.logger;
        String str = this.tag;
        rVar.b(str, "Ad expired " + getAdUnitId());
        this.h.set(true);
        Activity activity = this.b.getActivity();
        if (activity == null && (activity = this.sdk.D().a()) == null) {
            b();
            this.listenerWrapper.onAdLoadFailed(this.adUnitId, MaxErrorCodes.NO_ACTIVITY);
            return;
        }
        this.loadRequestBuilder.a("expired_ad_ad_unit_id", getAdUnitId());
        this.sdk.b().loadAd(this.adUnitId, this.adFormat, this.loadRequestBuilder.a(), activity, this.listenerWrapper);
    }

    public void showAd(final String str, final Activity activity) {
        if (activity == null) {
            activity = this.sdk.L();
        }
        if (activity == null) {
            throw new IllegalArgumentException("Attempting to show ad without a valid activity.");
        } else if (((Boolean) this.sdk.a(com.applovin.impl.sdk.c.a.E4)).booleanValue() && (this.sdk.C().a() || this.sdk.C().b())) {
            r.i(this.tag, "Attempting to show ad when another fullscreen ad is already showing");
            k.a(this.adListener, (MaxAd) this.f, -23);
        } else if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.c.a.F4)).booleanValue() || h.a((Context) activity)) {
            a(this.f);
            a(this.f, (Context) activity, (Runnable) new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.a(c.SHOWING, (Runnable) new Runnable() {
                        public void run() {
                            MaxFullscreenAdImpl.this.d.c(MaxFullscreenAdImpl.this.f);
                            MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
                            r rVar = maxFullscreenAdImpl.logger;
                            String str = maxFullscreenAdImpl.tag;
                            rVar.b(str, "Showing ad for '" + MaxFullscreenAdImpl.this.adUnitId + "'; loaded ad: " + MaxFullscreenAdImpl.this.f + "...");
                            MediationServiceImpl b = MaxFullscreenAdImpl.this.sdk.b();
                            com.applovin.impl.mediation.a.c b2 = MaxFullscreenAdImpl.this.f;
                            AnonymousClass3 r2 = AnonymousClass3.this;
                            b.showFullscreenAd(b2, str, activity, MaxFullscreenAdImpl.this.listenerWrapper);
                        }
                    });
                }
            });
        } else {
            r.i(this.tag, "Attempting to show ad with no internet connection");
            k.a(this.adListener, (MaxAd) this.f, -5201);
        }
    }

    public String toString() {
        return this.tag + "{adUnitId='" + this.adUnitId + '\'' + ", adListener=" + this.adListener + ", isReady=" + isReady() + '}';
    }
}
