package com.applovin.impl.mediation.ads;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.sdk.e;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.s;
import com.applovin.impl.sdk.w;
import com.applovin.impl.sdk.x;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;
import com.facebook.common.time.Clock;
import java.util.concurrent.TimeUnit;

public class MaxAdViewImpl extends a implements e.a, x.a {
    /* access modifiers changed from: private */
    public final Activity b;
    /* access modifiers changed from: private */
    public final MaxAdView c;
    private final View d;
    private long e = Clock.MAX_TIME;
    private com.applovin.impl.mediation.a.b f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public final a h;
    /* access modifiers changed from: private */
    public final c i;
    /* access modifiers changed from: private */
    public final e j;
    /* access modifiers changed from: private */
    public final w k;
    /* access modifiers changed from: private */
    public final x l;
    /* access modifiers changed from: private */
    public final Object m = new Object();
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.a.b n = null;
    private boolean o;
    private boolean p;
    /* access modifiers changed from: private */
    public boolean q = false;

    private class a extends b {
        private a() {
            super();
        }

        public void onAdLoadFailed(String str, int i) {
            k.a(MaxAdViewImpl.this.adListener, str, i);
            MaxAdViewImpl.this.a(i);
        }

        public void onAdLoaded(MaxAd maxAd) {
            if (MaxAdViewImpl.this.q) {
                MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                r rVar = maxAdViewImpl.logger;
                String str = maxAdViewImpl.tag;
                rVar.b(str, "Pre-cache ad with ad unit ID '" + MaxAdViewImpl.this.adUnitId + "' loaded after MaxAdView was destroyed. Destroying the ad.");
                MaxAdViewImpl.this.sdk.b().destroyAd(maxAd);
            } else if (maxAd instanceof com.applovin.impl.mediation.a.b) {
                com.applovin.impl.mediation.a.b bVar = (com.applovin.impl.mediation.a.b) maxAd;
                bVar.c(MaxAdViewImpl.this.g);
                MaxAdViewImpl.this.a(bVar);
                if (bVar.J()) {
                    long K = bVar.K();
                    r j0 = MaxAdViewImpl.this.sdk.j0();
                    String str2 = MaxAdViewImpl.this.tag;
                    j0.b(str2, "Scheduling banner ad refresh " + K + " milliseconds from now for '" + MaxAdViewImpl.this.adUnitId + "'...");
                    MaxAdViewImpl.this.j.a(K);
                }
                k.a(MaxAdViewImpl.this.adListener, maxAd);
            } else {
                MaxAdViewImpl maxAdViewImpl2 = MaxAdViewImpl.this;
                r rVar2 = maxAdViewImpl2.logger;
                String str3 = maxAdViewImpl2.tag;
                rVar2.e(str3, "Not a MediatedAdViewAd received: " + maxAd);
                onAdLoadFailed(MaxAdViewImpl.this.adUnitId, -5201);
            }
        }
    }

    private abstract class b implements MaxAdListener, MaxAdViewAdListener {
        private b() {
        }

        public void onAdClicked(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.n)) {
                k.d(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdCollapsed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.n)) {
                if (MaxAdViewImpl.this.n.L()) {
                    MaxAdViewImpl.this.startAutoRefresh();
                }
                k.h(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
            if (maxAd.equals(MaxAdViewImpl.this.n)) {
                k.a(MaxAdViewImpl.this.adListener, maxAd, i);
            }
        }

        public void onAdDisplayed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.n)) {
                k.b(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdExpanded(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.n)) {
                if (MaxAdViewImpl.this.n.L()) {
                    MaxAdViewImpl.this.stopAutoRefresh();
                }
                k.g(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdHidden(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.n)) {
                k.c(MaxAdViewImpl.this.adListener, maxAd);
            }
        }
    }

    private class c extends b {
        private c() {
            super();
        }

        public void onAdLoadFailed(String str, int i) {
            MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
            r rVar = maxAdViewImpl.logger;
            String str2 = maxAdViewImpl.tag;
            rVar.b(str2, "Failed to pre-cache ad for refresh with error code " + i);
            MaxAdViewImpl.this.a(i);
        }

        public void onAdLoaded(MaxAd maxAd) {
            if (MaxAdViewImpl.this.q) {
                MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                r rVar = maxAdViewImpl.logger;
                String str = maxAdViewImpl.tag;
                rVar.b(str, "Ad with ad unit ID '" + MaxAdViewImpl.this.adUnitId + "' loaded after MaxAdView was destroyed. Destroying the ad.");
                MaxAdViewImpl.this.sdk.b().destroyAd(maxAd);
                return;
            }
            MaxAdViewImpl maxAdViewImpl2 = MaxAdViewImpl.this;
            maxAdViewImpl2.logger.b(maxAdViewImpl2.tag, "Successfully pre-cached ad for refresh");
            MaxAdViewImpl.this.a(maxAd);
        }
    }

    public MaxAdViewImpl(String str, MaxAdFormat maxAdFormat, MaxAdView maxAdView, View view, l lVar, Activity activity) {
        super(str, maxAdFormat, "MaxAdView", lVar);
        if (activity != null) {
            this.b = activity;
            this.c = maxAdView;
            this.d = view;
            this.h = new a();
            this.i = new c();
            this.j = new e(lVar, this);
            this.k = new w(maxAdView, lVar);
            this.l = new x(maxAdView, lVar, this);
            r rVar = this.logger;
            String str2 = this.tag;
            rVar.b(str2, "Created new MaxAdView (" + this + ")");
            return;
        }
        throw new IllegalArgumentException("No activity specified");
    }

    /* access modifiers changed from: private */
    public void a() {
        com.applovin.impl.mediation.a.b bVar;
        MaxAdView maxAdView = this.c;
        if (maxAdView != null) {
            com.applovin.impl.sdk.utils.b.a(maxAdView, this.d);
        }
        this.l.a();
        synchronized (this.m) {
            bVar = this.n;
        }
        if (bVar != null) {
            this.sdk.J().b(bVar);
            this.sdk.b().destroyAd(bVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.sdk.b(com.applovin.impl.sdk.c.a.s4).contains(String.valueOf(i2))) {
            r j0 = this.sdk.j0();
            String str = this.tag;
            j0.b(str, "Ignoring banner ad refresh for error code '" + i2 + "'...");
            return;
        }
        this.o = true;
        long longValue = ((Long) this.sdk.a(com.applovin.impl.sdk.c.a.r4)).longValue();
        if (longValue >= 0) {
            r j02 = this.sdk.j0();
            String str2 = this.tag;
            j02.b(str2, "Scheduling failed banner ad refresh " + longValue + " milliseconds from now for '" + this.adUnitId + "'...");
            this.j.a(longValue);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        if (com.applovin.impl.sdk.utils.r.a(j2, ((Long) this.sdk.a(com.applovin.impl.sdk.c.a.B4)).longValue())) {
            r rVar = this.logger;
            String str = this.tag;
            rVar.b(str, "Undesired flags matched - current: " + Long.toBinaryString(j2) + ", undesired: " + Long.toBinaryString(j2));
            this.logger.b(this.tag, "Waiting for refresh timer to manually fire request");
            this.o = true;
            return;
        }
        this.logger.b(this.tag, "No undesired viewability flags matched - scheduling viewability");
        this.o = false;
        b();
    }

    /* access modifiers changed from: private */
    public void a(AnimatorListenerAdapter animatorListenerAdapter) {
        com.applovin.impl.mediation.a.b bVar = this.n;
        if (bVar == null || bVar.B() == null) {
            animatorListenerAdapter.onAnimationEnd((Animator) null);
            return;
        }
        View B = this.n.B();
        B.animate().alpha(0.0f).setDuration(((Long) this.sdk.a(com.applovin.impl.sdk.c.a.y4)).longValue()).setListener(animatorListenerAdapter).start();
    }

    private void a(View view, com.applovin.impl.mediation.a.b bVar) {
        int z = bVar.z();
        int A = bVar.A();
        int i2 = -1;
        int dpToPx = z == -1 ? -1 : AppLovinSdkUtils.dpToPx(view.getContext(), z);
        if (A != -1) {
            i2 = AppLovinSdkUtils.dpToPx(view.getContext(), A);
        }
        int height = this.c.getHeight();
        int width = this.c.getWidth();
        if ((height > 0 && height < i2) || (width > 0 && width < dpToPx)) {
            int pxToDp = AppLovinSdkUtils.pxToDp(view.getContext(), height);
            r.h("AppLovinSdk", "\n**************************************************\n`MaxAdView` size " + AppLovinSdkUtils.pxToDp(view.getContext(), width) + "x" + pxToDp + " dp smaller than required size: " + z + "x" + A + " dp\n**************************************************\n");
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new RelativeLayout.LayoutParams(dpToPx, i2);
        } else {
            layoutParams.width = dpToPx;
            layoutParams.height = i2;
        }
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            this.logger.b(this.tag, "Pinning ad view to MAX ad view with width: " + dpToPx + " and height: " + i2 + ".");
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
            for (int addRule : s.a(this.c.getGravity(), 10, 14)) {
                layoutParams2.addRule(addRule);
            }
        }
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void a(final com.applovin.impl.mediation.a.b bVar) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                String str;
                r rVar;
                String str2;
                final View B = bVar.B();
                if (B != null) {
                    final MaxAdView d = MaxAdViewImpl.this.c;
                    if (d != null) {
                        MaxAdViewImpl.this.a((AnimatorListenerAdapter) new AnimatorListenerAdapter() {
                            public void onAnimationEnd(Animator animator) {
                                super.onAnimationEnd(animator);
                                MaxAdViewImpl.this.a();
                                AnonymousClass2 r4 = AnonymousClass2.this;
                                MaxAdViewImpl.this.a(bVar);
                                MaxAdViewImpl.this.sdk.J().a((Object) bVar);
                                if (bVar.H()) {
                                    MaxAdViewImpl.this.l.a(bVar);
                                }
                                AnonymousClass2 r42 = AnonymousClass2.this;
                                MaxAdViewImpl.this.a(bVar, B, d);
                                synchronized (MaxAdViewImpl.this.m) {
                                    com.applovin.impl.mediation.a.b unused = MaxAdViewImpl.this.n = bVar;
                                }
                                MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                                maxAdViewImpl.logger.b(maxAdViewImpl.tag, "Scheduling impression for ad manually...");
                                MaxAdViewImpl.this.sdk.b().maybeScheduleRawAdImpressionPostback(bVar);
                                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                                    public void run() {
                                        long a2 = MaxAdViewImpl.this.k.a(bVar);
                                        if (!bVar.H()) {
                                            AnonymousClass2 r2 = AnonymousClass2.this;
                                            MaxAdViewImpl.this.a(bVar, a2);
                                        }
                                        MaxAdViewImpl.this.a(a2);
                                    }
                                }, bVar.C());
                            }
                        });
                        return;
                    }
                    MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                    rVar = maxAdViewImpl.logger;
                    str2 = maxAdViewImpl.tag;
                    str = "Max ad view does not have a parent View";
                } else {
                    MaxAdViewImpl maxAdViewImpl2 = MaxAdViewImpl.this;
                    rVar = maxAdViewImpl2.logger;
                    str2 = maxAdViewImpl2.tag;
                    str = "Max ad does not have a loaded ad view";
                }
                rVar.e(str2, str);
                MaxAdViewImpl.this.h.onAdDisplayFailed(bVar, -5201);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.a.b bVar, long j2) {
        this.logger.b(this.tag, "Scheduling viewability impression for ad...");
        this.sdk.b().maybeScheduleViewabilityAdImpressionPostback(bVar, j2);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.a.b bVar, View view, MaxAdView maxAdView) {
        view.setAlpha(0.0f);
        if (bVar.M() != Clock.MAX_TIME) {
            this.d.setBackgroundColor((int) bVar.M());
        } else {
            long j2 = this.e;
            if (j2 != Clock.MAX_TIME) {
                this.d.setBackgroundColor((int) j2);
            } else {
                this.d.setBackgroundColor(0);
            }
        }
        maxAdView.addView(view);
        a(view, bVar);
        view.animate().alpha(1.0f).setDuration(((Long) this.sdk.a(com.applovin.impl.sdk.c.a.x4)).longValue()).start();
    }

    /* access modifiers changed from: private */
    public void a(MaxAd maxAd) {
        this.sdk.J().a((Object) maxAd);
        if (this.p) {
            this.p = false;
            r rVar = this.logger;
            String str = this.tag;
            rVar.b(str, "Rendering precache request ad: " + maxAd.getAdUnitId() + "...");
            this.h.onAdLoaded(maxAd);
            return;
        }
        this.f = (com.applovin.impl.mediation.a.b) maxAd;
    }

    /* access modifiers changed from: private */
    public void a(final MaxAdListener maxAdListener) {
        if (d()) {
            r.i(this.tag, "Unable to load new ad; ad is already destroyed");
            k.a(this.adListener, this.adUnitId, -1);
            return;
        }
        AppLovinSdkUtils.runOnUiThread(true, new Runnable() {
            public void run() {
                if (MaxAdViewImpl.this.n != null) {
                    long a2 = MaxAdViewImpl.this.k.a(MaxAdViewImpl.this.n);
                    MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                    maxAdViewImpl.loadRequestBuilder.a("visible_ad_ad_unit_id", maxAdViewImpl.n.getAdUnitId()).a("viewability_flags", String.valueOf(a2));
                } else {
                    MaxAdViewImpl.this.loadRequestBuilder.a("visible_ad_ad_unit_id").a("viewability_flags");
                }
                MaxAdViewImpl maxAdViewImpl2 = MaxAdViewImpl.this;
                r rVar = maxAdViewImpl2.logger;
                String str = maxAdViewImpl2.tag;
                rVar.b(str, "Loading banner ad for '" + MaxAdViewImpl.this.adUnitId + "' and notifying " + maxAdListener + "...");
                MediationServiceImpl b2 = MaxAdViewImpl.this.sdk.b();
                MaxAdViewImpl maxAdViewImpl3 = MaxAdViewImpl.this;
                b2.loadAd(maxAdViewImpl3.adUnitId, maxAdViewImpl3.adFormat, maxAdViewImpl3.loadRequestBuilder.a(), MaxAdViewImpl.this.b, maxAdListener);
            }
        });
    }

    private void b() {
        if (c()) {
            long longValue = ((Long) this.sdk.a(com.applovin.impl.sdk.c.a.C4)).longValue();
            r rVar = this.logger;
            String str = this.tag;
            rVar.b(str, "Scheduling refresh precache request in " + TimeUnit.MICROSECONDS.toSeconds(longValue) + " seconds...");
            this.sdk.o().a(new y(this.sdk, new Runnable() {
                public void run() {
                    MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                    maxAdViewImpl.a((MaxAdListener) maxAdViewImpl.i);
                }
            }), com.applovin.impl.mediation.c.c.a(this.adFormat), longValue);
        }
    }

    private boolean c() {
        return ((Long) this.sdk.a(com.applovin.impl.sdk.c.a.C4)).longValue() > 0;
    }

    private boolean d() {
        boolean z;
        synchronized (this.m) {
            z = this.q;
        }
        return z;
    }

    public void destroy() {
        a();
        if (this.f != null) {
            this.sdk.J().b(this.f);
            this.sdk.b().destroyAd(this.f);
        }
        synchronized (this.m) {
            this.q = true;
        }
        this.j.c();
    }

    public String getPlacement() {
        return this.g;
    }

    public void loadAd() {
        r rVar = this.logger;
        String str = this.tag;
        rVar.b(str, "" + this + " Loading ad for " + this.adUnitId + "...");
        if (d()) {
            r.i(this.tag, "Unable to load new ad; ad is already destroyed");
            k.a(this.adListener, this.adUnitId, -1);
        } else if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.c.a.D4)).booleanValue() || !this.j.a()) {
            a((MaxAdListener) this.h);
        } else {
            String str2 = this.tag;
            r.i(str2, "Unable to load a new ad. An ad refresh has already been scheduled in " + TimeUnit.MILLISECONDS.toSeconds(this.j.b()) + " seconds.");
        }
    }

    public void onAdRefresh() {
        String str;
        String str2;
        r rVar;
        this.p = false;
        if (this.f != null) {
            r rVar2 = this.logger;
            String str3 = this.tag;
            rVar2.b(str3, "Refreshing for cached ad: " + this.f.getAdUnitId() + "...");
            this.h.onAdLoaded(this.f);
            this.f = null;
            return;
        }
        if (!c()) {
            rVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network...";
        } else if (this.o) {
            rVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network due to viewability requirements not met for refresh request...";
        } else {
            this.logger.e(this.tag, "Ignoring attempt to refresh ad - either still waiting for precache or did not attempt request due to visibility requirement not met");
            this.p = true;
            return;
        }
        rVar.b(str2, str);
        loadAd();
    }

    public void onLogVisibilityImpression() {
        a(this.n, this.k.a(this.n));
    }

    public void onWindowVisibilityChanged(int i2) {
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.c.a.w4)).booleanValue() && this.j.a()) {
            if (s.a(i2)) {
                this.logger.b(this.tag, "Ad view visible");
                this.j.g();
                return;
            }
            this.logger.b(this.tag, "Ad view hidden");
            this.j.f();
        }
    }

    public void setPlacement(String str) {
        this.g = str;
    }

    public void setPublisherBackgroundColor(int i2) {
        this.e = (long) i2;
    }

    public void startAutoRefresh() {
        this.j.e();
        r rVar = this.logger;
        String str = this.tag;
        rVar.b(str, "Resumed auto-refresh with remaining time: " + this.j.b());
    }

    public void stopAutoRefresh() {
        if (this.n != null) {
            r rVar = this.logger;
            String str = this.tag;
            rVar.b(str, "Pausing auto-refresh with remaining time: " + this.j.b());
            this.j.d();
            return;
        }
        r.h(this.tag, "Stopping auto-refresh has no effect until after the first ad has been loaded.");
    }

    public String toString() {
        return "MaxAdView{adUnitId='" + this.adUnitId + '\'' + ", adListener=" + this.adListener + ", isDestroyed=" + d() + '}';
    }
}
