package com.applovin.impl.mediation.c;

import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.j;
import org.json.JSONObject;

public class b extends h {
    private static final String[] e = {"ads", "settings", "auto_init_adapters", "test_mode_idfas", "test_mode_auto_init_adapters"};
    private static final String[] f = {"ads", "settings", "signal_providers"};

    public static String g(l lVar) {
        return h.a((String) lVar.a(a.h4), "1.0/mediate", lVar);
    }

    public static void g(JSONObject jSONObject, l lVar) {
        if (j.a(jSONObject, "signal_providers")) {
            JSONObject d = j.d(jSONObject);
            j.a(d, e);
            lVar.a(d.x, d.toString());
        }
    }

    public static String h(l lVar) {
        return h.a((String) lVar.a(a.i4), "1.0/mediate", lVar);
    }

    public static void h(JSONObject jSONObject, l lVar) {
        if (j.a(jSONObject, "auto_init_adapters") || j.a(jSONObject, "test_mode_auto_init_adapters")) {
            JSONObject d = j.d(jSONObject);
            j.a(d, f);
            lVar.a(d.y, d.toString());
        }
    }

    public static String i(l lVar) {
        return h.a((String) lVar.a(a.h4), "1.0/mediate_debug", lVar);
    }

    public static String j(l lVar) {
        return h.a((String) lVar.a(a.i4), "1.0/mediate_debug", lVar);
    }
}
