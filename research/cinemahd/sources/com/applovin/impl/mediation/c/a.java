package com.applovin.impl.mediation.c;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.k;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;

public class a implements MaxAdListener, MaxAdViewAdListener, MaxRewardedAdListener {

    /* renamed from: a  reason: collision with root package name */
    private final MaxAdListener f1638a;

    public a(MaxAdListener maxAdListener, l lVar) {
        this.f1638a = maxAdListener;
    }

    public void onAdClicked(MaxAd maxAd) {
        k.d(this.f1638a, maxAd);
    }

    public void onAdCollapsed(MaxAd maxAd) {
        k.h(this.f1638a, maxAd);
    }

    public void onAdDisplayFailed(MaxAd maxAd, int i) {
        k.a(this.f1638a, maxAd, i);
    }

    public void onAdDisplayed(MaxAd maxAd) {
        k.b(this.f1638a, maxAd);
    }

    public void onAdExpanded(MaxAd maxAd) {
        k.g(this.f1638a, maxAd);
    }

    public void onAdHidden(MaxAd maxAd) {
        k.c(this.f1638a, maxAd);
    }

    public void onRewardedVideoCompleted(MaxAd maxAd) {
        k.f(this.f1638a, maxAd);
    }

    public void onRewardedVideoStarted(MaxAd maxAd) {
        k.e(this.f1638a, maxAd);
    }

    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
        k.a(this.f1638a, maxAd, maxReward);
    }
}
