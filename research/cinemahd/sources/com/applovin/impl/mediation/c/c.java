package com.applovin.impl.mediation.c;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.unity3d.ads.metadata.MediationMetaData;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final List<String> f1639a = new ArrayList();
    private static JSONArray b;

    static {
        f1639a.add("com.applovin.mediation.adapters.AdColonyMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.AmazonMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.AmazonBiddingMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.AppLovinMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.ByteDanceMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.ChartboostMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.FacebookMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.GoogleMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.GoogleAdManagerMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.HyprMXMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.IMobileMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.InMobiMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.InneractiveMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.IronSourceMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.LeadboltMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.LineMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.MadvertiseMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.MaioMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.MintegralMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.MoPubMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.MyTargetMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.NendMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.OguryMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.OguryPresageMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.SayGamesMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.SmaatoMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.SnapMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.TapjoyMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.TencentMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.UnityAdsMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.VerizonAdsMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.VungleMediationAdapter");
        f1639a.add("com.applovin.mediation.adapters.YandexMediationAdapter");
    }

    public static o.a a(MaxAdFormat maxAdFormat) {
        return maxAdFormat == MaxAdFormat.INTERSTITIAL ? o.a.MEDIATION_INTERSTITIAL : maxAdFormat == MaxAdFormat.REWARDED ? o.a.MEDIATION_INCENTIVIZED : maxAdFormat == MaxAdFormat.REWARDED_INTERSTITIAL ? o.a.MEDIATION_REWARDED_INTERSTITIAL : o.a.MEDIATION_BANNER;
    }

    public static MaxAdapter a(String str, l lVar) {
        Class<MaxAdapter> cls = MaxAdapter.class;
        if (TextUtils.isEmpty(str)) {
            lVar.j0().e("AppLovinSdk", "Failed to create adapter instance. No class name provided");
            return null;
        }
        try {
            Class<?> cls2 = Class.forName(str);
            if (cls.isAssignableFrom(cls2)) {
                return (MaxAdapter) cls2.getConstructor(new Class[]{AppLovinSdk.class}).newInstance(new Object[]{lVar.v()});
            }
            r j0 = lVar.j0();
            j0.e("AppLovinSdk", str + " error: not an instance of '" + cls.getName() + "'.");
            return null;
        } catch (ClassNotFoundException unused) {
        } catch (Throwable th) {
            r j02 = lVar.j0();
            j02.b("AppLovinSdk", "Failed to load: " + str, th);
        }
    }

    public static AppLovinSdkUtils.Size a(int i, MaxAdFormat maxAdFormat, Activity activity) {
        if (i < 0) {
            try {
                Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                defaultDisplay.getMetrics(displayMetrics);
                i = AppLovinSdkUtils.pxToDp(activity, displayMetrics.widthPixels);
            } catch (Throwable th) {
                r.c("MediationUtils", "Failed to get adaptive banner size. Will fallback to using format specific ad view ad size.", th);
                return maxAdFormat.getSize();
            }
        }
        Class<?> cls = Class.forName("com.google.android.gms.ads.AdSize");
        Method method = cls.getMethod("getCurrentOrientationAnchoredAdaptiveBannerAdSize", new Class[]{Context.class, Integer.TYPE});
        Method method2 = cls.getMethod("getWidth", new Class[0]);
        Method method3 = cls.getMethod("getHeight", new Class[0]);
        Object invoke = method.invoke((Object) null, new Object[]{activity, Integer.valueOf(i)});
        return new AppLovinSdkUtils.Size(((Integer) method2.invoke(invoke, new Object[0])).intValue(), ((Integer) method3.invoke(invoke, new Object[0])).intValue());
    }

    public static JSONArray a(l lVar) {
        JSONArray jSONArray;
        if (!((Boolean) lVar.a(a.O4)).booleanValue() && (jSONArray = b) != null) {
            return jSONArray;
        }
        if (b != null) {
            b(lVar);
            return b;
        }
        b = new JSONArray();
        for (String next : f1639a) {
            MaxAdapter a2 = a(next, lVar);
            if (a2 != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("class", next);
                    jSONObject.put("sdk_version", a2.getSdkVersion());
                    jSONObject.put(MediationMetaData.KEY_VERSION, a2.getAdapterVersion());
                } catch (Throwable unused) {
                }
                b.put(jSONObject);
            }
        }
        return b;
    }

    public static boolean a(Object obj) {
        return (obj instanceof g) && com.applovin.impl.sdk.utils.o.b(((g) obj).k());
    }

    public static String b(MaxAdFormat maxAdFormat) {
        return maxAdFormat.getLabel();
    }

    private static void b(l lVar) {
        MaxAdapter a2;
        for (int i = 0; i < b.length(); i++) {
            JSONObject a3 = j.a(b, i, (JSONObject) null, lVar);
            String b2 = j.b(a3, "class", "", lVar);
            if (!com.applovin.impl.sdk.utils.o.b(j.b(a3, "sdk_version", "", lVar)) && (a2 = a(b2, lVar)) != null) {
                j.a(a3, "sdk_version", a2.getSdkVersion(), lVar);
            }
        }
    }

    public static boolean b(Object obj) {
        return (obj instanceof com.applovin.impl.mediation.a.a) && "APPLOVIN".equals(((com.applovin.impl.mediation.a.a) obj).e());
    }

    public static boolean c(MaxAdFormat maxAdFormat) {
        return maxAdFormat == MaxAdFormat.INTERSTITIAL || maxAdFormat == MaxAdFormat.REWARDED || maxAdFormat == MaxAdFormat.REWARDED_INTERSTITIAL;
    }

    public static boolean d(MaxAdFormat maxAdFormat) {
        return maxAdFormat == MaxAdFormat.BANNER || maxAdFormat == MaxAdFormat.MREC || maxAdFormat == MaxAdFormat.LEADER;
    }
}
