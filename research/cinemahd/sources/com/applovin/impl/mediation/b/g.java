package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.a.c;
import com.applovin.impl.sdk.e.aa;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import org.json.JSONObject;

public class g extends aa {
    private final c f;

    public g(c cVar, l lVar) {
        super("TaskValidateMaxReward", lVar);
        this.f = cVar;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        this.f.a(com.applovin.impl.sdk.b.c.a((i < 400 || i >= 500) ? "network_timeout" : "rejected"));
    }

    /* access modifiers changed from: protected */
    public void a(com.applovin.impl.sdk.b.c cVar) {
        this.f.a(cVar);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        j.a(jSONObject, "ad_unit_id", this.f.getAdUnitId(), this.f1817a);
        j.a(jSONObject, "placement", this.f.getPlacement(), this.f1817a);
        j.a(jSONObject, "ad_format", com.applovin.impl.mediation.c.c.b(this.f.getFormat()), this.f1817a);
        String K = this.f.K();
        if (!o.b(K)) {
            K = "NO_MCODE";
        }
        j.a(jSONObject, "mcode", K, this.f1817a);
        String J = this.f.J();
        if (!o.b(J)) {
            J = "NO_BCODE";
        }
        j.a(jSONObject, "bcode", J, this.f1817a);
    }

    /* access modifiers changed from: protected */
    public String e() {
        return "2.0/mvr";
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return this.f.L();
    }
}
