package com.applovin.impl.mediation.b;

import android.net.Uri;
import com.applovin.impl.mediation.a.e;
import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class d extends a {
    private final String f;
    private final e g;
    private final Map<String, String> h;
    private final Map<String, String> i;
    private final com.applovin.impl.mediation.e j;

    public d(String str, Map<String, String> map, com.applovin.impl.mediation.e eVar, e eVar2, l lVar) {
        super("TaskFireMediationPostbacks", lVar);
        this.f = str + "_urls";
        this.h = r.c(map);
        this.j = eVar != null ? eVar : com.applovin.impl.mediation.e.EMPTY;
        this.g = eVar2;
        HashMap hashMap = new HashMap(7);
        hashMap.put("AppLovin-Event-Type", str);
        hashMap.put("AppLovin-Ad-Network-Name", eVar2.d());
        if (eVar2 instanceof com.applovin.impl.mediation.a.a) {
            com.applovin.impl.mediation.a.a aVar = (com.applovin.impl.mediation.a.a) eVar2;
            hashMap.put("AppLovin-Ad-Unit-Id", aVar.getAdUnitId());
            hashMap.put("AppLovin-Ad-Format", aVar.getFormat().getLabel());
            hashMap.put("AppLovin-Third-Party-Ad-Placement-ID", aVar.s());
        }
        if (eVar != null) {
            hashMap.put("AppLovin-Error-Code", String.valueOf(eVar.getErrorCode()));
            hashMap.put("AppLovin-Error-Message", eVar.getErrorMessage());
        }
        this.i = hashMap;
    }

    private String a(String str, com.applovin.impl.mediation.e eVar) {
        int i2;
        String str2;
        if (eVar instanceof MaxAdapterError) {
            MaxAdapterError maxAdapterError = (MaxAdapterError) eVar;
            i2 = maxAdapterError.getThirdPartySdkErrorCode();
            str2 = maxAdapterError.getThirdPartySdkErrorMessage();
        } else {
            i2 = 0;
            str2 = "";
        }
        return str.replace("{ERROR_CODE}", String.valueOf(eVar.getErrorCode())).replace("{ERROR_MESSAGE}", o.f(eVar.getErrorMessage())).replace("{THIRD_PARTY_SDK_ERROR_CODE}", String.valueOf(i2)).replace("{THIRD_PARTY_SDK_ERROR_MESSAGE}", o.f(str2));
    }

    private List<String> a(List<String> list, Map<String, String> map, Map<String, String> map2, com.applovin.impl.mediation.e eVar) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator<String> it2 = list.iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            for (String next2 : map.keySet()) {
                next = next.replace(next2, this.g.e(map.get(next2)));
            }
            arrayList.add(a(b(next, map2), eVar));
        }
        return arrayList;
    }

    private void a(String str, Map<String, Object> map) {
        a().q().a(f.o().c(str).b("POST").b(this.i).a(false).c(map).b(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.a.W4)).booleanValue()).a());
    }

    private void a(List<String> list) {
        if (!list.isEmpty()) {
            for (String c : list) {
                a().q().a(f.o().c(c).a(false).b(this.i).a());
            }
        }
    }

    private String b(String str, Map<String, String> map) {
        for (String next : map.keySet()) {
            str = str.replace(next, map.get(next));
        }
        return str;
    }

    private void b(List<String> list) {
        if (!list.isEmpty()) {
            for (String d : list) {
                g.a b = g.b(a());
                b.d(d);
                b.f(false);
                b.d(this.i);
                a().u().dispatchPostbackRequest(b.a(), o.a.MEDIATION_POSTBACKS, new AppLovinPostbackListener() {
                    public void onPostbackFailure(String str, int i) {
                        d dVar = d.this;
                        dVar.d("Failed to fire postback with code: " + i + " and url: " + str);
                    }

                    public void onPostbackSuccess(String str) {
                    }
                });
            }
        }
    }

    private Map<String, String> e() {
        try {
            return j.a(new JSONObject((String) this.f1817a.a(com.applovin.impl.sdk.c.a.m4)));
        } catch (JSONException unused) {
            return Collections.EMPTY_MAP;
        }
    }

    public void run() {
        List<String> d = this.g.d(this.f);
        Map<String, String> e = e();
        if (((Boolean) a().a(com.applovin.impl.sdk.c.a.T4)).booleanValue()) {
            for (String b : d) {
                Uri parse = Uri.parse(a(b(b, this.h), this.j));
                Uri.Builder clearQuery = parse.buildUpon().clearQuery();
                HashMap hashMap = new HashMap(e.size());
                for (String next : parse.getQueryParameterNames()) {
                    String queryParameter = parse.getQueryParameter(next);
                    if (e.containsKey(queryParameter)) {
                        hashMap.put(next, this.g.e(e.get(queryParameter)));
                    } else {
                        clearQuery.appendQueryParameter(next, queryParameter);
                    }
                }
                a(clearQuery.build().toString(), (Map<String, Object>) hashMap);
            }
            return;
        }
        List<String> a2 = a(d, e, this.h, this.j);
        if (((Boolean) a().a(com.applovin.impl.sdk.c.a.n4)).booleanValue()) {
            a(a2);
        } else {
            b(a2);
        }
    }
}
