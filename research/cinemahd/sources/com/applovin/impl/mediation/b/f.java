package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.a.c;
import com.applovin.impl.sdk.e.v;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import org.json.JSONObject;

public class f extends v {
    private final c f;

    public f(c cVar, l lVar) {
        super("TaskReportMaxReward", lVar);
        this.f = cVar;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        a("Failed to report reward for mediated ad: " + this.f + " - error code: " + i);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        j.a(jSONObject, "ad_unit_id", this.f.getAdUnitId(), this.f1817a);
        j.a(jSONObject, "placement", this.f.getPlacement(), this.f1817a);
        String K = this.f.K();
        if (!o.b(K)) {
            K = "NO_MCODE";
        }
        j.a(jSONObject, "mcode", K, this.f1817a);
        String J = this.f.J();
        if (!o.b(J)) {
            J = "NO_BCODE";
        }
        j.a(jSONObject, "bcode", J, this.f1817a);
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        a("Reported reward successfully for mediated ad: " + this.f);
    }

    /* access modifiers changed from: protected */
    public String e() {
        return "2.0/mcr";
    }

    /* access modifiers changed from: protected */
    public com.applovin.impl.sdk.b.c h() {
        return this.f.N();
    }

    /* access modifiers changed from: protected */
    public void i() {
        d("No reward result was found for mediated ad: " + this.f);
    }
}
