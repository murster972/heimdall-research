package com.applovin.impl.mediation.b;

import android.app.Activity;
import android.content.Context;
import com.applovin.impl.mediation.c.c;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.d.h;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.d;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class e extends com.applovin.impl.sdk.e.a {
    private static final AtomicBoolean m = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final String f;
    /* access modifiers changed from: private */
    public final MaxAdFormat g;
    private final JSONObject h;
    private final List<com.applovin.impl.mediation.a.a> i;
    /* access modifiers changed from: private */
    public final MaxAdListener j;
    /* access modifiers changed from: private */
    public final WeakReference<Activity> k;
    /* access modifiers changed from: private */
    public boolean l = false;

    private class a extends com.applovin.impl.sdk.e.a {
        /* access modifiers changed from: private */
        public final int f;
        private final com.applovin.impl.mediation.a.a g;
        private final List<com.applovin.impl.mediation.a.a> h;

        a(int i2, List<com.applovin.impl.mediation.a.a> list) {
            super(e.this.b(), e.this.f1817a);
            this.f = i2;
            this.g = list.get(i2);
            this.h = list;
        }

        /* access modifiers changed from: private */
        public void e() {
            int i2;
            e eVar;
            if (this.f < this.h.size() - 1) {
                this.f1817a.o().a((com.applovin.impl.sdk.e.a) new a(this.f + 1, this.h), c.a(e.this.g));
                return;
            }
            if (e.this.l) {
                eVar = e.this;
                i2 = MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED;
            } else {
                eVar = e.this;
                i2 = 204;
            }
            eVar.a(i2);
        }

        /* access modifiers changed from: private */
        public void e(String str) {
        }

        public void run() {
            a("Loading ad " + (this.f + 1) + " of " + this.h.size() + ": " + this.g.d());
            e("started to load ad");
            this.f1817a.b().loadThirdPartyMediatedAd(e.this.f, this.g, e.this.k.get() != null ? (Activity) e.this.k.get() : this.f1817a.L(), new com.applovin.impl.mediation.c.a(e.this.j, this.f1817a) {
                public void onAdLoadFailed(String str, int i) {
                    a aVar = a.this;
                    aVar.a("Ad failed to load with error code: " + i);
                    if (i != 204) {
                        boolean unused = e.this.l = true;
                    }
                    a aVar2 = a.this;
                    aVar2.e("failed to load ad: " + i);
                    a.this.e();
                }

                public void onAdLoaded(MaxAd maxAd) {
                    a.this.e("loaded ad");
                    a aVar = a.this;
                    e.this.a(maxAd, aVar.f);
                }
            });
        }
    }

    e(String str, MaxAdFormat maxAdFormat, JSONObject jSONObject, Activity activity, l lVar, MaxAdListener maxAdListener) {
        super("TaskProcessMediationWaterfall:" + str + ":" + maxAdFormat.getLabel(), lVar);
        this.f = str;
        this.g = maxAdFormat;
        this.h = jSONObject;
        this.j = maxAdListener;
        this.k = new WeakReference<>(activity);
        this.i = new ArrayList(jSONObject.length());
        JSONArray b = j.b(jSONObject, "ads", new JSONArray(), lVar);
        for (int i2 = 0; i2 < b.length(); i2++) {
            this.i.add(com.applovin.impl.mediation.a.a.a(j.a(b, i2, (JSONObject) null, lVar), jSONObject, lVar));
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        h p;
        g gVar;
        if (i2 == 204) {
            p = this.f1817a.p();
            gVar = g.t;
        } else if (i2 == -5001) {
            p = this.f1817a.p();
            gVar = g.u;
        } else {
            p = this.f1817a.p();
            gVar = g.v;
        }
        p.a(gVar);
        b("Waterfall failed to load with error code " + i2);
        k.a(this.j, this.f, i2);
    }

    /* access modifiers changed from: private */
    public void a(MaxAd maxAd, int i2) {
        final Float f2;
        com.applovin.impl.mediation.a.a aVar = (com.applovin.impl.mediation.a.a) maxAd;
        this.f1817a.c().a(aVar);
        List<com.applovin.impl.mediation.a.a> list = this.i;
        List<com.applovin.impl.mediation.a.a> subList = list.subList(1, list.size());
        long longValue = ((Long) this.f1817a.a(com.applovin.impl.sdk.c.a.U4)).longValue();
        float f3 = 1.0f;
        for (final com.applovin.impl.mediation.a.a next : subList) {
            Float q = next.q();
            if (q != null) {
                f3 *= q.floatValue();
                f2 = Float.valueOf(f3);
            } else {
                f2 = null;
            }
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    e.this.f1817a.b().maybeScheduleAdLossPostback(next, f2);
                }
            }, TimeUnit.SECONDS.toMillis(longValue));
        }
        b("Waterfall loaded for " + aVar.d());
        k.a(this.j, maxAd);
    }

    public void run() {
        if (this.h.optBoolean("is_testing", false) && !this.f1817a.g().a() && m.compareAndSet(false, true)) {
            AppLovinSdkUtils.runOnUiThread(new Runnable() {
                public void run() {
                    r.a("MAX SDK Not Initialized In Test Mode", "Test ads may not load. Please force close and restart the app if you experience issues.", (Context) e.this.k.get());
                }
            });
        }
        if (this.i.size() > 0) {
            a("Starting waterfall for " + this.i.size() + " ad(s)...");
            this.f1817a.o().a((com.applovin.impl.sdk.e.a) new a(0, this.i));
            return;
        }
        c("No ads were returned from the server");
        r.a(this.f, this.g, this.h, this.f1817a);
        JSONObject b = j.b(this.h, "settings", new JSONObject(), this.f1817a);
        long a2 = j.a(b, "alfdcs", 0, this.f1817a);
        if (a2 > 0) {
            long millis = TimeUnit.SECONDS.toMillis(a2);
            AnonymousClass2 r4 = new Runnable() {
                public void run() {
                    e.this.a(204);
                }
            };
            if (j.a(b, "alfdcs_iba", (Boolean) false, this.f1817a).booleanValue()) {
                d.a(millis, this.f1817a, r4);
            } else {
                AppLovinSdkUtils.runOnUiThreadDelayed(r4, millis);
            }
        } else {
            a(204);
        }
    }
}
