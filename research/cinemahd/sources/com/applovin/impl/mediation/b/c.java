package com.applovin.impl.mediation.b;

import android.app.Activity;
import com.applovin.impl.mediation.f;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.d.h;
import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.e.t;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.y;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinWebViewActivity;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c extends a {
    private final String f;
    private final MaxAdFormat g;
    private final f h;
    private final JSONArray i;
    private final Activity j;
    private final MaxAdListener k;

    public c(String str, MaxAdFormat maxAdFormat, f fVar, JSONArray jSONArray, Activity activity, l lVar, MaxAdListener maxAdListener) {
        super("TaskFetchMediatedAd " + str, lVar);
        this.f = str;
        this.g = maxAdFormat;
        this.h = fVar;
        this.i = jSONArray;
        this.j = activity;
        this.k = maxAdListener;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        boolean z = i2 != 204;
        r j0 = this.f1817a.j0();
        String b = b();
        Boolean valueOf = Boolean.valueOf(z);
        j0.a(b, valueOf, "Unable to fetch " + this.f + " ad: server returned " + i2);
        if (i2 == -800) {
            this.f1817a.p().a(g.r);
        }
        k.a(this.k, this.f, i2);
    }

    private void a(h hVar) {
        long b = hVar.b(g.f);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b > TimeUnit.MINUTES.toMillis((long) ((Integer) this.f1817a.a(b.q2)).intValue())) {
            hVar.b(g.f, currentTimeMillis);
            hVar.c(g.g);
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        try {
            com.applovin.impl.sdk.utils.h.b(jSONObject, this.f1817a);
            com.applovin.impl.sdk.utils.h.a(jSONObject, this.f1817a);
            com.applovin.impl.sdk.utils.h.c(jSONObject, this.f1817a);
            com.applovin.impl.sdk.utils.h.f(jSONObject, this.f1817a);
            com.applovin.impl.mediation.c.b.g(jSONObject, this.f1817a);
            com.applovin.impl.mediation.c.b.h(jSONObject, this.f1817a);
            if (this.g != com.applovin.impl.sdk.utils.r.c(j.b(jSONObject, "ad_format", (String) null, this.f1817a))) {
                r.i(b(), "Ad format requested does not match ad unit id's format.");
            }
            this.f1817a.o().a((a) b(jSONObject));
        } catch (Throwable th) {
            a("Unable to process mediated ad response", th);
            throw new RuntimeException("Unable to process ad: " + th);
        }
    }

    private e b(JSONObject jSONObject) {
        return new e(this.f, this.g, jSONObject, this.j, this.f1817a, this.k);
    }

    private void c(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("loaded", new JSONArray(this.f1817a.k0().a()));
            jSONObject2.put("failed", new JSONArray(this.f1817a.k0().b()));
            jSONObject.put("classname_info", jSONObject2);
            jSONObject.put("initialized_adapters", this.f1817a.a().d());
            jSONObject.put("initialized_adapter_classnames", new JSONArray(this.f1817a.a().c()));
            jSONObject.put("installed_mediation_adapters", com.applovin.impl.mediation.c.c.a(this.f1817a));
        } catch (Exception e) {
            a("Failed to populate adapter classnames", e);
            throw new RuntimeException("Failed to populate classnames: " + e);
        }
    }

    private void d(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = this.i;
        if (jSONArray != null) {
            jSONObject.put("signal_data", jSONArray);
        }
    }

    private String e() {
        return com.applovin.impl.mediation.c.b.g(this.f1817a);
    }

    private void e(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("ad_unit_id", this.f);
        jSONObject2.put("ad_format", com.applovin.impl.mediation.c.c.b(this.g));
        Map<String, String> a2 = j.a(this.h.a());
        String a3 = this.f1817a.c().a(this.f);
        if (o.b(a3)) {
            a2.put("previous_winning_network", a3);
        }
        jSONObject2.put("extra_parameters", j.a((Map<String, ?>) a2));
        jSONObject2.put("n", String.valueOf(this.f1817a.E().a(this.f)));
        jSONObject.put("ad_info", jSONObject2);
    }

    private String f() {
        return com.applovin.impl.mediation.c.b.h(this.f1817a);
    }

    private Map<String, String> g() {
        HashMap hashMap = new HashMap(2);
        hashMap.put("AppLovin-Ad-Unit-Id", this.f);
        hashMap.put("AppLovin-Ad-Format", this.g.getLabel());
        return hashMap;
    }

    private JSONObject h() throws JSONException {
        JSONObject jSONObject = new JSONObject(this.f1817a.r().a((Map<String, String>) null, false, true));
        e(jSONObject);
        d(jSONObject);
        c(jSONObject);
        return jSONObject;
    }

    public void run() {
        a("Fetching next ad for ad unit id: " + this.f + " and format: " + this.g);
        if (((Boolean) this.f1817a.a(b.J2)).booleanValue() && com.applovin.impl.sdk.utils.r.d()) {
            a("User is connected to a VPN");
        }
        h p = this.f1817a.p();
        p.a(g.q);
        if (p.b(g.f) == 0) {
            p.b(g.f, System.currentTimeMillis());
        }
        try {
            JSONObject h2 = h();
            HashMap hashMap = new HashMap();
            hashMap.put("rid", UUID.randomUUID().toString());
            if (!((Boolean) this.f1817a.a(b.u3)).booleanValue()) {
                hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1817a.h0());
            }
            if (this.f1817a.g().a()) {
                hashMap.put("test_mode", DiskLruCache.VERSION_1);
            }
            String c = this.f1817a.g().c();
            if (o.b(c)) {
                hashMap.put("filter_ad_network", c);
                if (this.f1817a.g().b()) {
                    hashMap.put("force_ad_network", c);
                }
            }
            HashMap hashMap2 = new HashMap();
            if (((Boolean) this.f1817a.a(b.R2)).booleanValue()) {
                hashMap2.putAll(y.a(((Long) this.f1817a.a(b.S2)).longValue(), this.f1817a));
            }
            hashMap2.putAll(g());
            a(p);
            b.a c2 = com.applovin.impl.sdk.network.b.a(this.f1817a).b("POST").b((Map<String, String>) hashMap2).a(e()).c(f()).a((Map<String, String>) hashMap).a(h2).d(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.a.V4)).booleanValue()).a(new JSONObject()).b(((Long) this.f1817a.a(com.applovin.impl.sdk.c.a.k4)).intValue()).a(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.d2)).intValue()).c(((Long) this.f1817a.a(com.applovin.impl.sdk.c.a.j4)).intValue());
            c2.e(true);
            AnonymousClass1 r1 = new t<JSONObject>(c2.a(), this.f1817a) {
                public void a(int i) {
                    c.this.a(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    if (i == 200) {
                        j.b(jSONObject, "ad_fetch_latency_millis", this.k.a(), this.f1817a);
                        j.b(jSONObject, "ad_fetch_response_size", this.k.b(), this.f1817a);
                        c.this.a(jSONObject);
                        return;
                    }
                    c.this.a(i);
                }
            };
            r1.a(com.applovin.impl.sdk.c.a.h4);
            r1.b(com.applovin.impl.sdk.c.a.i4);
            this.f1817a.o().a((a) r1);
        } catch (Throwable th) {
            a("Unable to fetch ad " + this.f, th);
            throw new RuntimeException("Unable to fetch ad: " + th);
        }
    }
}
