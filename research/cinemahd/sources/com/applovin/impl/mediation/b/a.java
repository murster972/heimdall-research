package com.applovin.impl.mediation.b;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.a.e;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinMediationProvider;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.e.a {
    /* access modifiers changed from: private */
    public final Activity f;

    public a(Activity activity, l lVar) {
        super("TaskAutoInitAdapters", lVar, true);
        this.f = activity;
    }

    private List<e> a(JSONArray jSONArray, JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(new e(j.a(jSONArray, i, (JSONObject) null, this.f1817a), jSONObject, this.f1817a));
        }
        return arrayList;
    }

    public void run() {
        String str;
        String str2 = (String) this.f1817a.a(d.y);
        if (o.b(str2)) {
            try {
                JSONObject jSONObject = new JSONObject(str2);
                List<e> a2 = a(j.b(jSONObject, this.f1817a.g().a() ? "test_mode_auto_init_adapters" : "auto_init_adapters", new JSONArray(), this.f1817a), jSONObject);
                if (a2.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Auto-initing ");
                    sb.append(a2.size());
                    sb.append(" adapters");
                    sb.append(this.f1817a.g().a() ? " in test mode" : "");
                    sb.append("...");
                    a(sb.toString());
                    if (TextUtils.isEmpty(this.f1817a.b0())) {
                        this.f1817a.c(AppLovinMediationProvider.MAX);
                    } else if (!this.f1817a.Q()) {
                        r.i("AppLovinSdk", "Auto-initing adapters for non-MAX mediation provider: " + this.f1817a.b0());
                    }
                    if (this.f == null) {
                        r.i("AppLovinSdk", "\n**********\nFailed to initialize 3rd-party SDKs. Please make sure to initialize the AppLovin SDK with an Activity context.\n**********\n");
                        this.f1817a.p().b(g.s, 1);
                        return;
                    }
                    for (final e next : a2) {
                        this.f1817a.o().b().execute(new Runnable() {
                            public void run() {
                                a aVar = a.this;
                                aVar.a("Auto-initing adapter: " + next);
                                a.this.f1817a.a().a(next, a.this.f);
                            }
                        });
                    }
                }
            } catch (JSONException e) {
                th = e;
                str = "Failed to parse auto-init adapters JSON";
                a(str, th);
            } catch (Throwable th) {
                th = th;
                str = "Failed to auto-init adapters";
                a(str, th);
            }
        }
    }
}
