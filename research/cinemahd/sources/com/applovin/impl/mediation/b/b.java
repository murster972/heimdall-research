package com.applovin.impl.mediation.b;

import android.app.Activity;
import com.applovin.impl.mediation.a.f;
import com.applovin.impl.mediation.a.g;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.mediation.MaxAdFormat;
import com.facebook.common.util.UriUtil;
import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b extends com.applovin.impl.sdk.e.a {
    private static String i;
    /* access modifiers changed from: private */
    public final MaxAdFormat f;
    /* access modifiers changed from: private */
    public final Activity g;
    private final a h;

    public interface a {
        void a(JSONArray jSONArray);
    }

    static {
        try {
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(a("APPLOVIN_NETWORK", "com.applovin.mediation.adapters.AppLovinMediationAdapter"));
            a("FACEBOOK_NETWORK", "com.applovin.mediation.adapters.FacebookMediationAdapter").put("run_on_ui_thread", false);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("signal_providers", jSONArray);
            i = jSONObject.toString();
        } catch (JSONException unused) {
        }
    }

    public b(MaxAdFormat maxAdFormat, Activity activity, l lVar, a aVar) {
        super("TaskCollectSignals", lVar);
        this.f = maxAdFormat;
        this.g = activity;
        this.h = aVar;
    }

    private static JSONObject a(String str, String str2) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(MediationMetaData.KEY_NAME, str);
        jSONObject.put("class", str2);
        jSONObject.put("adapter_timeout_ms", HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT);
        jSONObject.put("max_signal_length", 32768);
        jSONObject.put("scode", "");
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public void a(final g gVar, final f.a aVar) {
        AnonymousClass2 r0 = new Runnable() {
            public void run() {
                b.this.f1817a.b().collectSignal(b.this.f, gVar, b.this.g, aVar);
            }
        };
        if (gVar.j()) {
            a("Running signal collection for " + gVar + " on the main thread");
            this.g.runOnUiThread(r0);
            return;
        }
        a("Running signal collection for " + gVar + " on the background thread");
        r0.run();
    }

    private void a(Collection<f> collection) {
        String str;
        String d;
        JSONArray jSONArray = new JSONArray();
        for (f next : collection) {
            try {
                JSONObject jSONObject = new JSONObject();
                g a2 = next.a();
                jSONObject.put(MediationMetaData.KEY_NAME, a2.d());
                jSONObject.put("class", a2.c());
                jSONObject.put("adapter_version", next.c());
                jSONObject.put("sdk_version", next.b());
                JSONObject jSONObject2 = new JSONObject();
                if (o.b(next.e())) {
                    str = "error_message";
                    d = next.e();
                } else {
                    str = "signal";
                    d = next.d();
                }
                jSONObject2.put(str, d);
                jSONObject.put(UriUtil.DATA_SCHEME, jSONObject2);
                jSONArray.put(jSONObject);
                a("Collected signal from " + a2);
            } catch (JSONException e) {
                a("Failed to create signal data", e);
            }
        }
        a(jSONArray);
    }

    private void a(JSONArray jSONArray) {
        a aVar = this.h;
        if (aVar != null) {
            aVar.a(jSONArray);
        }
    }

    private void a(JSONArray jSONArray, JSONObject jSONObject) throws JSONException, InterruptedException {
        List a2 = e.a(jSONArray.length());
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        CountDownLatch countDownLatch = new CountDownLatch(jSONArray.length());
        ScheduledExecutorService b = this.f1817a.o().b();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            final g gVar = new g(jSONArray.getJSONObject(i2), jSONObject, this.f1817a);
            final AtomicBoolean atomicBoolean2 = atomicBoolean;
            final List list = a2;
            final CountDownLatch countDownLatch2 = countDownLatch;
            b.execute(new Runnable() {
                public void run() {
                    b.this.a(gVar, (f.a) new f.a() {
                        public void a(f fVar) {
                            if (atomicBoolean2.get() && fVar != null) {
                                list.add(fVar);
                            }
                            countDownLatch2.countDown();
                        }
                    });
                }
            });
        }
        countDownLatch.await(((Long) this.f1817a.a(com.applovin.impl.sdk.c.a.o4)).longValue(), TimeUnit.MILLISECONDS);
        atomicBoolean.set(false);
        a((Collection<f>) a2);
    }

    private void b(String str, Throwable th) {
        a("No signals collected: " + str, th);
        a(new JSONArray());
    }

    public void run() {
        String str;
        try {
            JSONObject jSONObject = new JSONObject((String) this.f1817a.b(d.x, i));
            JSONArray b = j.b(jSONObject, "signal_providers", (JSONArray) null, this.f1817a);
            if (b.length() == 0) {
                b("No signal providers found", (Throwable) null);
            } else {
                a(b, jSONObject);
            }
        } catch (JSONException e) {
            th = e;
            str = "Failed to parse signals JSON";
            b(str, th);
        } catch (InterruptedException e2) {
            th = e2;
            str = "Failed to wait for signals";
            b(str, th);
        } catch (Throwable th) {
            th = th;
            str = "Failed to collect signals";
            b(str, th);
        }
    }
}
