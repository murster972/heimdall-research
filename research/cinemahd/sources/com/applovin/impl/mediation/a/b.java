package com.applovin.impl.mediation.a;

import android.view.View;
import com.applovin.impl.mediation.j;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAdFormat;
import org.json.JSONObject;

public class b extends a {
    private b(b bVar, j jVar) {
        super(bVar.b(), bVar.a(), jVar, bVar.f1606a);
    }

    public b(JSONObject jSONObject, JSONObject jSONObject2, l lVar) {
        super(jSONObject, jSONObject2, (j) null, lVar);
    }

    public int A() {
        int b = b("ad_view_height", -2);
        if (b != -2) {
            return b;
        }
        MaxAdFormat format = getFormat();
        MaxAdFormat maxAdFormat = MaxAdFormat.BANNER;
        if (format == maxAdFormat || format == (maxAdFormat = MaxAdFormat.LEADER) || format == (maxAdFormat = MaxAdFormat.MREC)) {
            return maxAdFormat.getSize().getHeight();
        }
        throw new IllegalStateException("Invalid ad format");
    }

    public View B() {
        j jVar;
        if (!n() || (jVar = this.h) == null) {
            return null;
        }
        View a2 = jVar.a();
        if (a2 != null) {
            return a2;
        }
        throw new IllegalStateException("Ad-view based ad is missing an ad view");
    }

    public long C() {
        return b("viewability_imp_delay_ms", ((Long) this.f1606a.a(com.applovin.impl.sdk.c.b.M0)).longValue());
    }

    public int D() {
        return b("viewability_min_width", ((Integer) this.f1606a.a(getFormat() == MaxAdFormat.BANNER ? com.applovin.impl.sdk.c.b.N0 : getFormat() == MaxAdFormat.MREC ? com.applovin.impl.sdk.c.b.P0 : com.applovin.impl.sdk.c.b.R0)).intValue());
    }

    public int E() {
        return b("viewability_min_height", ((Integer) this.f1606a.a(getFormat() == MaxAdFormat.BANNER ? com.applovin.impl.sdk.c.b.O0 : getFormat() == MaxAdFormat.MREC ? com.applovin.impl.sdk.c.b.Q0 : com.applovin.impl.sdk.c.b.S0)).intValue());
    }

    public float F() {
        return a("viewability_min_alpha", ((Float) this.f1606a.a(com.applovin.impl.sdk.c.b.T0)).floatValue() / 100.0f);
    }

    public int G() {
        return b("viewability_min_pixels", -1);
    }

    public boolean H() {
        return G() >= 0;
    }

    public long I() {
        return b("viewability_timer_min_visible_ms", ((Long) this.f1606a.a(com.applovin.impl.sdk.c.b.U0)).longValue());
    }

    public boolean J() {
        return K() >= 0;
    }

    public long K() {
        long b = b("ad_refresh_ms", -1);
        return b >= 0 ? b : a("ad_refresh_ms", ((Long) this.f1606a.a(a.q4)).longValue());
    }

    public boolean L() {
        return b("proe", (Boolean) this.f1606a.a(a.M4)).booleanValue();
    }

    public long M() {
        return r.f(b("bg_color", (String) null));
    }

    public a a(j jVar) {
        return new b(this, jVar);
    }

    public int z() {
        int b = b("ad_view_width", -2);
        if (b != -2) {
            return b;
        }
        MaxAdFormat format = getFormat();
        MaxAdFormat maxAdFormat = MaxAdFormat.BANNER;
        if (format == maxAdFormat || format == (maxAdFormat = MaxAdFormat.LEADER) || format == (maxAdFormat = MaxAdFormat.MREC)) {
            return maxAdFormat.getSize().getWidth();
        }
        throw new IllegalStateException("Invalid ad format");
    }
}
