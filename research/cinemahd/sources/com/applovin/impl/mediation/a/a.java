package com.applovin.impl.mediation.a;

import android.os.Bundle;
import android.os.SystemClock;
import com.applovin.impl.mediation.c.c;
import com.applovin.impl.mediation.j;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.BundleUtils;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public abstract class a extends e implements MaxAd {
    private final AtomicBoolean g = new AtomicBoolean();
    protected j h;

    protected a(JSONObject jSONObject, JSONObject jSONObject2, j jVar, l lVar) {
        super(jSONObject, jSONObject2, lVar);
        this.h = jVar;
    }

    public static a a(JSONObject jSONObject, JSONObject jSONObject2, l lVar) {
        String b = com.applovin.impl.sdk.utils.j.b(jSONObject2, "ad_format", (String) null, lVar);
        MaxAdFormat c = r.c(b);
        if (c.d(c)) {
            return new b(jSONObject, jSONObject2, lVar);
        }
        if (c == MaxAdFormat.NATIVE) {
            return new d(jSONObject, jSONObject2, lVar);
        }
        if (c.c(c)) {
            return new c(jSONObject, jSONObject2, lVar);
        }
        throw new IllegalArgumentException("Unsupported ad format: " + b);
    }

    private long z() {
        return b("load_started_time_ms", 0);
    }

    public abstract a a(j jVar);

    public void a(Bundle bundle) {
        if (bundle != null && bundle.containsKey("creative_id")) {
            c("creative_id", BundleUtils.getString("creative_id", bundle));
        }
    }

    public String getAdUnitId() {
        return a("ad_unit_id", "");
    }

    public String getCreativeId() {
        return b("creative_id", (String) null);
    }

    public MaxAdFormat getFormat() {
        return r.c(b("ad_format", a("ad_format", (String) null)));
    }

    public String getNetworkName() {
        return b("network_name", "");
    }

    public boolean n() {
        j jVar = this.h;
        return jVar != null && jVar.d() && this.h.e();
    }

    public String o() {
        return a("event_id", "");
    }

    public j p() {
        return this.h;
    }

    public Float q() {
        return a("r_mbr", (Float) null);
    }

    public String r() {
        return b("bid_response", (String) null);
    }

    public String s() {
        return b("third_party_ad_placement_id", (String) null);
    }

    public long t() {
        if (z() > 0) {
            return v() - z();
        }
        return -1;
    }

    public String toString() {
        return "MediatedAd{thirdPartyAdPlacementId=" + s() + ", adUnitId=" + getAdUnitId() + ", format=" + getFormat().getLabel() + ", networkName='" + getNetworkName() + "'}";
    }

    public void u() {
        c("load_started_time_ms", SystemClock.elapsedRealtime());
    }

    public long v() {
        return b("load_completed_time_ms", 0);
    }

    public void w() {
        c("load_completed_time_ms", SystemClock.elapsedRealtime());
    }

    public AtomicBoolean x() {
        return this.g;
    }

    public void y() {
        this.h = null;
    }
}
