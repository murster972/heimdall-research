package com.applovin.impl.mediation.a;

import android.os.SystemClock;
import com.applovin.impl.mediation.j;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.l;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class c extends a {
    private final AtomicReference<com.applovin.impl.sdk.b.c> i;
    private final AtomicBoolean j;
    private final AtomicBoolean k;

    private c(c cVar, j jVar) {
        super(cVar.b(), cVar.a(), jVar, cVar.f1606a);
        this.k = new AtomicBoolean();
        this.i = cVar.i;
        this.j = cVar.j;
    }

    public c(JSONObject jSONObject, JSONObject jSONObject2, l lVar) {
        super(jSONObject, jSONObject2, (j) null, lVar);
        this.k = new AtomicBoolean();
        this.i = new AtomicReference<>();
        this.j = new AtomicBoolean();
    }

    public String A() {
        return b("nia_button_title", a("nia_button_title", ""));
    }

    public AtomicBoolean B() {
        return this.k;
    }

    public long C() {
        long b = b("ad_expiration_ms", -1);
        return b >= 0 ? b : a("ad_expiration_ms", ((Long) this.f1606a.a(a.G4)).longValue());
    }

    public long D() {
        long b = b("ad_hidden_timeout_ms", -1);
        return b >= 0 ? b : a("ad_hidden_timeout_ms", ((Long) this.f1606a.a(a.J4)).longValue());
    }

    public boolean E() {
        if (b("schedule_ad_hidden_on_ad_dismiss", (Boolean) false).booleanValue()) {
            return true;
        }
        return a("schedule_ad_hidden_on_ad_dismiss", (Boolean) this.f1606a.a(a.K4)).booleanValue();
    }

    public long F() {
        long b = b("ad_hidden_on_ad_dismiss_callback_delay_ms", -1);
        return b >= 0 ? b : a("ad_hidden_on_ad_dismiss_callback_delay_ms", ((Long) this.f1606a.a(a.L4)).longValue());
    }

    public long G() {
        if (v() > 0) {
            return SystemClock.elapsedRealtime() - v();
        }
        return -1;
    }

    public long H() {
        long b = b("fullscreen_display_delay_ms", -1);
        return b >= 0 ? b : ((Long) this.f1606a.a(a.z4)).longValue();
    }

    public long I() {
        return b("ahdm", ((Long) this.f1606a.a(a.A4)).longValue());
    }

    public String J() {
        return b("bcode", "");
    }

    public String K() {
        return a("mcode", "");
    }

    public boolean L() {
        return this.j.get();
    }

    public void M() {
        this.j.set(true);
    }

    public com.applovin.impl.sdk.b.c N() {
        return this.i.getAndSet((Object) null);
    }

    public boolean O() {
        return b("show_nia", a("show_nia", (Boolean) false)).booleanValue();
    }

    public String P() {
        return b("nia_title", a("nia_title", ""));
    }

    public a a(j jVar) {
        return new c(this, jVar);
    }

    public void a(com.applovin.impl.sdk.b.c cVar) {
        this.i.set(cVar);
    }

    public String z() {
        return b("nia_message", a("nia_message", ""));
    }
}
