package com.applovin.impl.mediation.a;

import com.applovin.impl.mediation.j;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private final g f1607a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    public interface a {
        void a(f fVar);
    }

    private f(g gVar, j jVar, String str, String str2) {
        this.f1607a = gVar;
        this.e = str2;
        if (str != null) {
            this.d = str.substring(0, Math.min(str.length(), gVar.n()));
        } else {
            this.d = null;
        }
        if (jVar != null) {
            this.b = jVar.f();
            this.c = jVar.g();
            return;
        }
        this.b = null;
        this.c = null;
    }

    public static f a(g gVar, j jVar, String str) {
        if (gVar == null) {
            throw new IllegalArgumentException("No spec specified");
        } else if (jVar != null) {
            return new f(gVar, jVar, str, (String) null);
        } else {
            throw new IllegalArgumentException("No adapterWrapper specified");
        }
    }

    public static f a(g gVar, String str) {
        return b(gVar, (j) null, str);
    }

    public static f b(g gVar, j jVar, String str) {
        if (gVar != null) {
            return new f(gVar, jVar, (String) null, str);
        }
        throw new IllegalArgumentException("No spec specified");
    }

    public g a() {
        return this.f1607a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SignalCollectionResult{mSignalProviderSpec=");
        sb.append(this.f1607a);
        sb.append(", mSdkVersion='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", mAdapterVersion='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", mSignalDataLength='");
        String str = this.d;
        sb.append(str != null ? str.length() : 0);
        sb.append('\'');
        sb.append(", mErrorMessage=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
