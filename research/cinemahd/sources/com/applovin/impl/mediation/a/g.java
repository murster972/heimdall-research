package com.applovin.impl.mediation.a;

import com.applovin.impl.sdk.l;
import org.json.JSONObject;

public class g extends e {
    public g(JSONObject jSONObject, JSONObject jSONObject2, l lVar) {
        super(jSONObject, jSONObject2, lVar);
    }

    /* access modifiers changed from: package-private */
    public int n() {
        return b("max_signal_length", 2048);
    }

    public boolean o() {
        return b("only_collect_signal_when_initialized", (Boolean) false).booleanValue();
    }

    public String toString() {
        return "SignalProviderSpec{adObject=" + b() + '}';
    }
}
