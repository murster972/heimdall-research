package com.applovin.impl.mediation.a;

import android.os.Bundle;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {

    /* renamed from: a  reason: collision with root package name */
    protected final l f1606a;
    private final JSONObject b;
    private final JSONObject c;
    private final Object d = new Object();
    private final Object e = new Object();
    private volatile String f;

    public e(JSONObject jSONObject, JSONObject jSONObject2, l lVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (jSONObject2 == null) {
            throw new IllegalArgumentException("No full response specified");
        } else if (jSONObject != null) {
            this.f1606a = lVar;
            this.b = jSONObject2;
            this.c = jSONObject;
        } else {
            throw new IllegalArgumentException("No ad object specified");
        }
    }

    private int n() {
        return b("mute_state", a("mute_state", ((Integer) this.f1606a.a(a.N4)).intValue()));
    }

    /* access modifiers changed from: protected */
    public float a(String str, float f2) {
        float a2;
        synchronized (this.d) {
            a2 = j.a(this.c, str, f2, this.f1606a);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public int a(String str, int i) {
        int b2;
        synchronized (this.e) {
            b2 = j.b(this.b, str, i, this.f1606a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long a(String str, long j) {
        long a2;
        synchronized (this.e) {
            a2 = j.a(this.b, str, j, this.f1606a);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public Boolean a(String str, Boolean bool) {
        Boolean a2;
        synchronized (this.e) {
            a2 = j.a(this.b, str, bool, this.f1606a);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public Float a(String str, Float f2) {
        Float a2;
        synchronized (this.d) {
            a2 = j.a(this.c, str, f2, this.f1606a);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public String a(String str, String str2) {
        String b2;
        synchronized (this.e) {
            b2 = j.b(this.b, str, str2, this.f1606a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONArray a(String str, JSONArray jSONArray) {
        JSONArray b2;
        synchronized (this.e) {
            b2 = j.b(this.b, str, jSONArray, this.f1606a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject a() {
        JSONObject jSONObject;
        synchronized (this.e) {
            jSONObject = this.b;
        }
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public JSONObject a(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.d) {
            b2 = j.b(this.c, str, jSONObject, this.f1606a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        boolean has;
        synchronized (this.d) {
            has = this.c.has(str);
        }
        return has;
    }

    /* access modifiers changed from: protected */
    public int b(String str, int i) {
        int b2;
        synchronized (this.d) {
            b2 = j.b(this.c, str, i, this.f1606a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long b(String str, long j) {
        long a2;
        synchronized (this.d) {
            a2 = j.a(this.c, str, j, this.f1606a);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public Boolean b(String str, Boolean bool) {
        Boolean a2;
        synchronized (this.d) {
            a2 = j.a(this.c, str, bool, this.f1606a);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public Object b(String str) {
        Object opt;
        synchronized (this.d) {
            opt = this.c.opt(str);
        }
        return opt;
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        String b2;
        synchronized (this.d) {
            b2 = j.b(this.c, str, str2, this.f1606a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONArray b(String str, JSONArray jSONArray) {
        JSONArray b2;
        synchronized (this.d) {
            b2 = j.b(this.c, str, jSONArray, this.f1606a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject b() {
        JSONObject jSONObject;
        synchronized (this.d) {
            jSONObject = this.c;
        }
        return jSONObject;
    }

    public String c() {
        return b("class", (String) null);
    }

    public void c(String str) {
        this.f = str;
    }

    /* access modifiers changed from: protected */
    public void c(String str, long j) {
        synchronized (this.d) {
            j.b(this.c, str, j, this.f1606a);
        }
    }

    /* access modifiers changed from: protected */
    public void c(String str, String str2) {
        synchronized (this.d) {
            j.a(this.c, str, str2, this.f1606a);
        }
    }

    public String d() {
        return b(MediationMetaData.KEY_NAME, (String) null);
    }

    public List<String> d(String str) {
        if (str != null) {
            List a2 = j.a(a(str, new JSONArray()), Collections.EMPTY_LIST);
            List a3 = j.a(b(str, new JSONArray()), Collections.EMPTY_LIST);
            ArrayList arrayList = new ArrayList(a2.size() + a3.size());
            arrayList.addAll(a2);
            arrayList.addAll(a3);
            return arrayList;
        }
        throw new IllegalArgumentException("No key specified");
    }

    public String e() {
        return d().split("_")[0];
    }

    public String e(String str) {
        String b2 = b(str, "");
        return o.b(b2) ? b2 : a(str, "");
    }

    public boolean f() {
        return b("is_testing", (Boolean) false).booleanValue();
    }

    public Boolean g() {
        return a("huc") ? b("huc", (Boolean) false) : a("huc", (Boolean) null);
    }

    public String getPlacement() {
        return this.f;
    }

    public Boolean h() {
        return a("aru") ? b("aru", (Boolean) false) : a("aru", (Boolean) null);
    }

    public Boolean i() {
        return a("dns") ? b("dns", (Boolean) false) : a("dns", (Boolean) null);
    }

    public boolean j() {
        return b("run_on_ui_thread", (Boolean) true).booleanValue();
    }

    public Bundle k() {
        Bundle c2 = b("server_parameters") instanceof JSONObject ? j.c(a("server_parameters", (JSONObject) null)) : new Bundle();
        int n = n();
        if (n != -1) {
            c2.putBoolean("is_muted", n == 2 ? this.f1606a.Y().isMuted() : n == 0);
        }
        return c2;
    }

    public long l() {
        return b("adapter_timeout_ms", ((Long) this.f1606a.a(a.p4)).longValue());
    }

    public long m() {
        return b("init_completion_delay_ms", -1);
    }

    public String toString() {
        return "MediationAdapterSpec{adapterClass='" + c() + "', adapterName='" + d() + "', isTesting=" + f() + '}';
    }
}
