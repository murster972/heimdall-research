package com.applovin.impl.mediation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.mediation.a.b;
import com.applovin.impl.mediation.a.e;
import com.applovin.impl.mediation.a.f;
import com.applovin.impl.mediation.a.g;
import com.applovin.impl.mediation.b.d;
import com.applovin.impl.mediation.b.f;
import com.applovin.impl.mediation.c.c;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.k;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MediationServiceImpl implements AppLovinBroadcastManager.Receiver {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1599a;
    /* access modifiers changed from: private */
    public final r b;

    private class a implements d {

        /* renamed from: a  reason: collision with root package name */
        private final com.applovin.impl.mediation.a.a f1603a;
        /* access modifiers changed from: private */
        public MaxAdListener b;

        private a(com.applovin.impl.mediation.a.a aVar, MaxAdListener maxAdListener) {
            this.f1603a = aVar;
            this.b = maxAdListener;
        }

        public void a(MaxAd maxAd, Bundle bundle) {
            MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad via callback...");
            MediationServiceImpl.this.maybeScheduleCallbackAdImpressionPostback(this.f1603a);
            this.f1603a.a(bundle);
            MediationServiceImpl.this.f1599a.F().a(this.f1603a, "DID_DISPLAY");
            if (c.c(maxAd.getFormat())) {
                MediationServiceImpl.this.f1599a.C().a((Object) maxAd);
                MediationServiceImpl.this.f1599a.K().a((Object) maxAd);
            }
            k.b(this.b, maxAd);
        }

        public void a(MaxAd maxAd, e eVar) {
            MediationServiceImpl.this.b(this.f1603a, eVar, this.b);
            if ((maxAd.getFormat() == MaxAdFormat.REWARDED || maxAd.getFormat() == MaxAdFormat.REWARDED_INTERSTITIAL) && (maxAd instanceof com.applovin.impl.mediation.a.c)) {
                ((com.applovin.impl.mediation.a.c) maxAd).M();
            }
        }

        public void a(MaxAdListener maxAdListener) {
            this.b = maxAdListener;
        }

        public void a(String str, e eVar) {
            this.f1603a.w();
            MediationServiceImpl.this.a(this.f1603a, eVar, this.b);
        }

        public void b(MaxAd maxAd, Bundle bundle) {
            this.f1603a.w();
            this.f1603a.a(bundle);
            MediationServiceImpl.this.b(this.f1603a);
            MediationServiceImpl.this.f1599a.F().a(this.f1603a, "DID_LOAD");
            k.a(this.b, maxAd);
        }

        public void onAdClicked(MaxAd maxAd) {
            MediationServiceImpl.this.f1599a.F().a((com.applovin.impl.mediation.a.a) maxAd, "DID_CLICKED");
            MediationServiceImpl.this.c(this.f1603a);
            k.d(this.b, maxAd);
        }

        public void onAdCollapsed(MaxAd maxAd) {
            k.h(this.b, maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
            MediationServiceImpl.this.b(this.f1603a, new e(i), this.b);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            a(maxAd, (Bundle) null);
        }

        public void onAdExpanded(MaxAd maxAd) {
            k.g(this.b, maxAd);
        }

        public void onAdHidden(final MaxAd maxAd) {
            MediationServiceImpl.this.f1599a.F().a((com.applovin.impl.mediation.a.a) maxAd, "DID_HIDE");
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    if (c.c(maxAd.getFormat())) {
                        MediationServiceImpl.this.f1599a.C().b((Object) maxAd);
                        MediationServiceImpl.this.f1599a.K().a();
                    }
                    k.c(a.this.b, maxAd);
                }
            }, maxAd instanceof com.applovin.impl.mediation.a.c ? ((com.applovin.impl.mediation.a.c) maxAd).I() : 0);
        }

        public void onAdLoadFailed(String str, int i) {
            this.f1603a.w();
            MediationServiceImpl.this.a(this.f1603a, new e(i), this.b);
        }

        public void onAdLoaded(MaxAd maxAd) {
            b(maxAd, (Bundle) null);
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            k.f(this.b, maxAd);
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            k.e(this.b, maxAd);
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            k.a(this.b, maxAd, maxReward);
            MediationServiceImpl.this.f1599a.o().a((com.applovin.impl.sdk.e.a) new f((com.applovin.impl.mediation.a.c) maxAd, MediationServiceImpl.this.f1599a), o.a.MEDIATION_REWARD);
        }
    }

    public MediationServiceImpl(l lVar) {
        this.f1599a = lVar;
        this.b = lVar.j0();
        lVar.I().registerReceiver(this, new IntentFilter("com.applovin.render_process_gone"));
    }

    private void a(com.applovin.impl.mediation.a.a aVar) {
        a("mpreload", (e) aVar);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.a.a aVar, e eVar, MaxAdListener maxAdListener) {
        a(eVar, aVar);
        destroyAd(aVar);
        k.a(maxAdListener, aVar.getAdUnitId(), eVar.getErrorCode());
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.a.c cVar, MaxAdListener maxAdListener) {
        long longValue = ((Long) this.f1599a.a(com.applovin.impl.sdk.c.a.I4)).longValue();
        if (longValue > 0) {
            final com.applovin.impl.mediation.a.c cVar2 = cVar;
            final long j = longValue;
            final MaxAdListener maxAdListener2 = maxAdListener;
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    if (!cVar2.x().get()) {
                        r.i("MediationService", "Ad (" + cVar2.e() + ") has not been displayed after " + j + "ms. Failing ad display...");
                        MediationServiceImpl.this.b(cVar2, new e(-5201, "Adapter did not call adDisplayed."), maxAdListener2);
                        MediationServiceImpl.this.f1599a.C().b((Object) cVar2);
                        MediationServiceImpl.this.f1599a.K().a();
                    }
                }
            }, longValue);
        }
    }

    private void a(e eVar, com.applovin.impl.mediation.a.a aVar) {
        long t = aVar.t();
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(t));
        a("mlerr", (Map<String, String>) hashMap, eVar, (e) aVar);
    }

    private void a(String str, e eVar) {
        a(str, (Map<String, String>) Collections.EMPTY_MAP, (e) null, eVar);
    }

    /* access modifiers changed from: private */
    public void a(String str, g gVar, j jVar) {
        HashMap hashMap = new HashMap(2);
        com.applovin.impl.sdk.utils.r.a("{ADAPTER_VERSION}", jVar.g(), (Map) hashMap);
        com.applovin.impl.sdk.utils.r.a("{SDK_VERSION}", jVar.f(), (Map) hashMap);
        a("serr", (Map<String, String>) hashMap, new e(str), (e) gVar);
    }

    private void a(String str, Map<String, String> map, e eVar) {
        a(str, map, (e) null, eVar);
    }

    private void a(String str, Map<String, String> map, e eVar, e eVar2) {
        HashMap hashMap = new HashMap(map);
        hashMap.put("{PLACEMENT}", eVar2.getPlacement() != null ? eVar2.getPlacement() : "");
        this.f1599a.o().a((com.applovin.impl.sdk.e.a) new d(str, hashMap, eVar, eVar2, this.f1599a), o.a.MEDIATION_POSTBACKS);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.a.a aVar) {
        long t = aVar.t();
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(t));
        a("load", (Map<String, String>) hashMap, (e) aVar);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.a.a aVar, e eVar, MaxAdListener maxAdListener) {
        this.f1599a.F().a(aVar, "DID_FAIL_DISPLAY");
        maybeScheduleAdDisplayErrorPostback(eVar, aVar);
        if (aVar.x().compareAndSet(false, true)) {
            k.a(maxAdListener, (MaxAd) aVar, eVar.getErrorCode());
        }
    }

    /* access modifiers changed from: private */
    public void c(com.applovin.impl.mediation.a.a aVar) {
        a("mclick", (e) aVar);
    }

    public void collectSignal(MaxAdFormat maxAdFormat, final g gVar, Activity activity, final f.a aVar) {
        String str;
        r rVar;
        String str2;
        StringBuilder sb;
        if (gVar == null) {
            throw new IllegalArgumentException("No spec specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (aVar != null) {
            final j a2 = this.f1599a.k0().a((e) gVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(gVar, maxAdFormat, activity.getApplicationContext());
                a2.a((MaxAdapterInitializationParameters) a3, activity);
                AnonymousClass2 r1 = new MaxSignalCollectionListener() {
                    public void onSignalCollected(String str) {
                        aVar.a(com.applovin.impl.mediation.a.f.a(gVar, a2, str));
                    }

                    public void onSignalCollectionFailed(String str) {
                        MediationServiceImpl.this.a(str, gVar, a2);
                        aVar.a(com.applovin.impl.mediation.a.f.b(gVar, a2, str));
                    }
                };
                if (!gVar.o()) {
                    rVar = this.b;
                    sb = new StringBuilder();
                    str2 = "Collecting signal for adapter: ";
                } else if (this.f1599a.a().a((e) gVar)) {
                    rVar = this.b;
                    sb = new StringBuilder();
                    str2 = "Collecting signal for now-initialized adapter: ";
                } else {
                    r rVar2 = this.b;
                    rVar2.e("MediationService", "Skip collecting signal for not-initialized adapter: " + a2.b());
                    str = "Adapter not initialized yet";
                }
                sb.append(str2);
                sb.append(a2.b());
                rVar.b("MediationService", sb.toString());
                a2.a(a3, gVar, activity, r1);
                return;
            }
            str = "Could not load adapter";
            aVar.a(com.applovin.impl.mediation.a.f.a(gVar, str));
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    public void destroyAd(MaxAd maxAd) {
        if (maxAd instanceof com.applovin.impl.mediation.a.a) {
            r rVar = this.b;
            rVar.c("MediationService", "Destroying " + maxAd);
            com.applovin.impl.mediation.a.a aVar = (com.applovin.impl.mediation.a.a) maxAd;
            j p = aVar.p();
            if (p != null) {
                p.h();
                aVar.y();
            }
        }
    }

    public void loadAd(String str, MaxAdFormat maxAdFormat, f fVar, Activity activity, MaxAdListener maxAdListener) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAdListener != null) {
            if (TextUtils.isEmpty(this.f1599a.b0())) {
                r.i("AppLovinSdk", "Mediation provider is null. Please set AppLovin SDK mediation provider via AppLovinSdk.getInstance(context).setMediationProvider()");
            }
            if (!this.f1599a.O()) {
                r.h("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
            }
            this.f1599a.z();
            if (str.length() != 16 && com.applovin.impl.sdk.utils.r.e(this.f1599a.i()) && !str.startsWith("test_mode") && !this.f1599a.h0().startsWith("05TMD")) {
                com.applovin.impl.sdk.utils.r.a("Invalid Ad Unit Length", "Please double-check the ad unit " + str + " for " + maxAdFormat.getLabel(), (Context) activity);
            }
            this.f1599a.f().a(str, maxAdFormat, fVar, activity, maxAdListener);
        } else {
            throw new IllegalArgumentException("No listener specified");
        }
    }

    public void loadThirdPartyMediatedAd(String str, com.applovin.impl.mediation.a.a aVar, Activity activity, MaxAdListener maxAdListener) {
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (activity != null) {
            r rVar = this.b;
            rVar.b("MediationService", "Loading " + aVar + "...");
            this.f1599a.F().a(aVar, "WILL_LOAD");
            a(aVar);
            j a2 = this.f1599a.k0().a((e) aVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(aVar, activity.getApplicationContext());
                a2.a((MaxAdapterInitializationParameters) a3, activity);
                com.applovin.impl.mediation.a.a a4 = aVar.a(a2);
                a2.a(str, a4);
                a4.u();
                a2.a(str, a3, a4, activity, new a(a4, maxAdListener));
                return;
            }
            r rVar2 = this.b;
            rVar2.d("MediationService", "Failed to load " + aVar + ": adapter not loaded");
            a(aVar, new e((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED), maxAdListener);
        } else {
            throw new IllegalArgumentException("A valid Activity is required");
        }
    }

    public void maybeScheduleAdDisplayErrorPostback(e eVar, com.applovin.impl.mediation.a.a aVar) {
        a("mierr", (Map<String, String>) Collections.EMPTY_MAP, eVar, (e) aVar);
    }

    public void maybeScheduleAdLossPostback(com.applovin.impl.mediation.a.a aVar, Float f) {
        String f2 = f != null ? f.toString() : "";
        HashMap hashMap = new HashMap(1);
        hashMap.put("{MBR}", f2);
        a("mloss", (Map<String, String>) hashMap, (e) aVar);
    }

    public void maybeScheduleAdapterInitializationPostback(e eVar, long j, MaxAdapter.InitializationStatus initializationStatus, String str) {
        HashMap hashMap = new HashMap(3);
        hashMap.put("{INIT_STATUS}", String.valueOf(initializationStatus.getCode()));
        hashMap.put("{INIT_TIME_MS}", String.valueOf(j));
        a("minit", (Map<String, String>) hashMap, new e(str), eVar);
    }

    public void maybeScheduleCallbackAdImpressionPostback(com.applovin.impl.mediation.a.a aVar) {
        a("mcimp", (e) aVar);
    }

    public void maybeScheduleRawAdImpressionPostback(com.applovin.impl.mediation.a.a aVar) {
        this.f1599a.F().a(aVar, "WILL_DISPLAY");
        HashMap hashMap = new HashMap(1);
        if (aVar instanceof com.applovin.impl.mediation.a.c) {
            hashMap.put("{TIME_TO_SHOW_MS}", String.valueOf(((com.applovin.impl.mediation.a.c) aVar).G()));
        }
        a("mimp", (Map<String, String>) hashMap, (e) aVar);
    }

    public void maybeScheduleViewabilityAdImpressionPostback(b bVar, long j) {
        HashMap hashMap = new HashMap(1);
        hashMap.put("{VIEWABILITY_FLAGS}", String.valueOf(j));
        hashMap.put("{USED_VIEWABILITY_TIMER}", String.valueOf(bVar.H()));
        a("mvimp", (Map<String, String>) hashMap, (e) bVar);
    }

    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
        if ("com.applovin.render_process_gone".equals(intent.getAction())) {
            Object c = this.f1599a.C().c();
            if (c instanceof com.applovin.impl.mediation.a.a) {
                maybeScheduleAdDisplayErrorPostback(MaxAdapterError.WEBVIEW_ERROR, (com.applovin.impl.mediation.a.a) c);
            }
        }
    }

    public void showFullscreenAd(MaxAd maxAd, String str, Activity activity, MaxAdListener maxAdListener) {
        if (maxAd == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAd instanceof com.applovin.impl.mediation.a.c) {
            this.f1599a.C().a(true);
            final com.applovin.impl.mediation.a.c cVar = (com.applovin.impl.mediation.a.c) maxAd;
            final j p = cVar.p();
            if (p != null) {
                cVar.c(str);
                long H = cVar.H();
                r rVar = this.b;
                rVar.c("MediationService", "Showing ad " + maxAd.getAdUnitId() + " with delay of " + H + "ms...");
                final Activity activity2 = activity;
                final MaxAdListener maxAdListener2 = maxAdListener;
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        if (cVar.getFormat() == MaxAdFormat.REWARDED || cVar.getFormat() == MaxAdFormat.REWARDED_INTERSTITIAL) {
                            MediationServiceImpl.this.f1599a.o().a((com.applovin.impl.sdk.e.a) new com.applovin.impl.mediation.b.g(cVar, MediationServiceImpl.this.f1599a), o.a.MEDIATION_REWARD);
                        }
                        p.a((com.applovin.impl.mediation.a.a) cVar, activity2);
                        MediationServiceImpl.this.f1599a.C().a(false);
                        MediationServiceImpl.this.a(cVar, maxAdListener2);
                        MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad manually...");
                        MediationServiceImpl.this.maybeScheduleRawAdImpressionPostback(cVar);
                    }
                }, H);
                return;
            }
            this.f1599a.C().a(false);
            r rVar2 = this.b;
            rVar2.d("MediationService", "Failed to show " + maxAd + ": adapter not found");
            r.i("MediationService", "There may be an integration problem with the adapter for ad unit id '" + cVar.getAdUnitId() + "'. Please check if you have a supported version of that SDK integrated into your project.");
            throw new IllegalStateException("Could not find adapter for provided ad");
        } else {
            r.i("MediationService", "Unable to show ad for '" + maxAd.getAdUnitId() + "': only REWARDED or INTERSTITIAL ads are eligible for showFullscreenAd(). " + maxAd.getFormat() + " ad was provided.");
            throw new IllegalArgumentException("Provided ad is not a MediatedFullscreenAd");
        }
    }
}
