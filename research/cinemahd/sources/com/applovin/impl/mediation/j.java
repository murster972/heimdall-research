package com.applovin.impl.mediation;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import com.applovin.impl.mediation.a.e;
import com.applovin.impl.mediation.a.g;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.MaxRewardedInterstitialAdapter;
import com.applovin.mediation.adapter.MaxSignalProvider;
import com.applovin.mediation.adapter.listeners.MaxAdViewAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

public class j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Handler f1699a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final l b;
    /* access modifiers changed from: private */
    public final r c;
    private final String d;
    /* access modifiers changed from: private */
    public final e e;
    /* access modifiers changed from: private */
    public final String f;
    /* access modifiers changed from: private */
    public MaxAdapter g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.a.a i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public final a k = new a();
    /* access modifiers changed from: private */
    public MaxAdapterResponseParameters l;
    private final AtomicBoolean m = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public final AtomicBoolean o = new AtomicBoolean(false);

    private class a implements MaxAdViewAdapterListener, MaxInterstitialAdapterListener, MaxRewardedAdapterListener, MaxRewardedInterstitialAdapterListener {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public d f1716a;

        private a() {
        }

        /* access modifiers changed from: private */
        public void a(d dVar) {
            if (dVar != null) {
                this.f1716a = dVar;
                return;
            }
            throw new IllegalArgumentException("No listener specified");
        }

        /* access modifiers changed from: private */
        public void a(String str, int i) {
            a(str, new MaxAdapterError(i));
        }

        private void a(String str, final Bundle bundle) {
            j.this.o.set(true);
            a(str, (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    if (j.this.n.compareAndSet(false, true)) {
                        a.this.f1716a.b(j.this.i, bundle);
                    }
                }
            });
        }

        private void a(final String str, final MaxAdListener maxAdListener, final Runnable runnable) {
            j.this.f1699a.post(new Runnable(this) {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Exception e) {
                        MaxAdListener maxAdListener = maxAdListener;
                        String name = maxAdListener != null ? maxAdListener.getClass().getName() : null;
                        r.c("MediationAdapterWrapper", "Failed to forward call (" + str + ") to " + name, e);
                    }
                }
            });
        }

        private void a(String str, final MaxAdapterError maxAdapterError) {
            a(str, (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    if (j.this.n.compareAndSet(false, true)) {
                        a.this.f1716a.a(j.this.h, (e) maxAdapterError);
                    }
                }
            });
        }

        /* access modifiers changed from: private */
        public void b(String str, int i) {
            b(str, new MaxAdapterError(i));
        }

        private void b(String str, final Bundle bundle) {
            if (j.this.i.x().compareAndSet(false, true)) {
                a(str, (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                    public void run() {
                        a.this.f1716a.a((MaxAd) j.this.i, bundle);
                    }
                });
            }
        }

        private void b(String str, final MaxAdapterError maxAdapterError) {
            a(str, (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.a((MaxAd) j.this.i, (e) maxAdapterError);
                }
            });
        }

        public void onAdViewAdClicked() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": adview ad clicked");
            a("onAdViewAdClicked", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdClicked(j.this.i);
                }
            });
        }

        public void onAdViewAdCollapsed() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": adview ad collapsed");
            a("onAdViewAdCollapsed", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdCollapsed(j.this.i);
                }
            });
        }

        public void onAdViewAdDisplayFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": adview ad failed to display with code: " + maxAdapterError);
            b("onAdViewAdDisplayFailed", maxAdapterError);
        }

        public void onAdViewAdDisplayed() {
            onAdViewAdDisplayed((Bundle) null);
        }

        public void onAdViewAdDisplayed(Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": adview ad displayed with extra info: " + bundle);
            b("onAdViewAdDisplayed", bundle);
        }

        public void onAdViewAdExpanded() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": adview ad expanded");
            a("onAdViewAdExpanded", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdExpanded(j.this.i);
                }
            });
        }

        public void onAdViewAdHidden() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": adview ad hidden");
            a("onAdViewAdHidden", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdHidden(j.this.i);
                }
            });
        }

        public void onAdViewAdLoadFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": adview ad ad failed to load with code: " + maxAdapterError);
            a("onAdViewAdLoadFailed", maxAdapterError);
        }

        public void onAdViewAdLoaded(View view) {
            onAdViewAdLoaded(view, (Bundle) null);
        }

        public void onAdViewAdLoaded(View view, Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": adview ad loaded with extra info: " + bundle);
            View unused = j.this.j = view;
            a("onAdViewAdLoaded", bundle);
        }

        public void onInterstitialAdClicked() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": interstitial ad clicked");
            a("onInterstitialAdClicked", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdClicked(j.this.i);
                }
            });
        }

        public void onInterstitialAdDisplayFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": interstitial ad failed to display with code " + maxAdapterError);
            b("onInterstitialAdDisplayFailed", maxAdapterError);
        }

        public void onInterstitialAdDisplayed() {
            onInterstitialAdDisplayed((Bundle) null);
        }

        public void onInterstitialAdDisplayed(Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": interstitial ad displayed with extra info: " + bundle);
            b("onInterstitialAdDisplayed", bundle);
        }

        public void onInterstitialAdHidden() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": interstitial ad hidden");
            a("onInterstitialAdHidden", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdHidden(j.this.i);
                }
            });
        }

        public void onInterstitialAdLoadFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": interstitial ad failed to load with error " + maxAdapterError);
            a("onInterstitialAdLoadFailed", maxAdapterError);
        }

        public void onInterstitialAdLoaded() {
            onInterstitialAdLoaded((Bundle) null);
        }

        public void onInterstitialAdLoaded(Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": interstitial ad loaded with extra info: " + bundle);
            a("onInterstitialAdLoaded", bundle);
        }

        public void onRewardedAdClicked() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded ad clicked");
            a("onRewardedAdClicked", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdClicked(j.this.i);
                }
            });
        }

        public void onRewardedAdDisplayFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": rewarded ad display failed with error: " + maxAdapterError);
            b("onRewardedAdDisplayFailed", maxAdapterError);
        }

        public void onRewardedAdDisplayed() {
            onRewardedAdDisplayed((Bundle) null);
        }

        public void onRewardedAdDisplayed(Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded ad displayed with extra info: " + bundle);
            b("onRewardedAdDisplayed", bundle);
        }

        public void onRewardedAdHidden() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded ad hidden");
            a("onRewardedAdHidden", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdHidden(j.this.i);
                }
            });
        }

        public void onRewardedAdLoadFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": rewarded ad failed to load with error: " + maxAdapterError);
            a("onRewardedAdLoadFailed", maxAdapterError);
        }

        public void onRewardedAdLoaded() {
            onRewardedAdLoaded((Bundle) null);
        }

        public void onRewardedAdLoaded(Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded ad loaded with extra info: " + bundle);
            a("onRewardedAdLoaded", bundle);
        }

        public void onRewardedAdVideoCompleted() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded video completed");
            a("onRewardedAdVideoCompleted", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onRewardedVideoCompleted(j.this.i);
                }
            });
        }

        public void onRewardedAdVideoStarted() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded video started");
            a("onRewardedAdVideoStarted", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onRewardedVideoStarted(j.this.i);
                }
            });
        }

        public void onRewardedInterstitialAdClicked() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded interstitial ad clicked");
            a("onRewardedInterstitialAdClicked", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdClicked(j.this.i);
                }
            });
        }

        public void onRewardedInterstitialAdDisplayFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": rewarded interstitial ad display failed with error: " + maxAdapterError);
            b("onRewardedInterstitialAdDisplayFailed", maxAdapterError);
        }

        public void onRewardedInterstitialAdDisplayed() {
            onRewardedInterstitialAdDisplayed((Bundle) null);
        }

        public void onRewardedInterstitialAdDisplayed(Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded interstitial ad displayed with extra info: " + bundle);
            b("onRewardedInterstitialAdDisplayed", bundle);
        }

        public void onRewardedInterstitialAdHidden() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded interstitial ad hidden");
            a("onRewardedInterstitialAdHidden", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onAdHidden(j.this.i);
                }
            });
        }

        public void onRewardedInterstitialAdLoadFailed(MaxAdapterError maxAdapterError) {
            r f = j.this.c;
            f.d("MediationAdapterWrapper", j.this.f + ": rewarded ad failed to load with error: " + maxAdapterError);
            a("onRewardedInterstitialAdLoadFailed", maxAdapterError);
        }

        public void onRewardedInterstitialAdLoaded() {
            onRewardedInterstitialAdLoaded((Bundle) null);
        }

        public void onRewardedInterstitialAdLoaded(Bundle bundle) {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded interstitial ad loaded with extra info: " + bundle);
            a("onRewardedInterstitialAdLoaded", bundle);
        }

        public void onRewardedInterstitialAdVideoCompleted() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded interstitial completed");
            a("onRewardedInterstitialAdVideoCompleted", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onRewardedVideoCompleted(j.this.i);
                }
            });
        }

        public void onRewardedInterstitialAdVideoStarted() {
            r f = j.this.c;
            f.c("MediationAdapterWrapper", j.this.f + ": rewarded interstitial started");
            a("onRewardedInterstitialAdVideoStarted", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                public void run() {
                    a.this.f1716a.onRewardedVideoStarted(j.this.i);
                }
            });
        }

        public void onUserRewarded(final MaxReward maxReward) {
            if (j.this.i instanceof com.applovin.impl.mediation.a.c) {
                final com.applovin.impl.mediation.a.c cVar = (com.applovin.impl.mediation.a.c) j.this.i;
                if (cVar.B().compareAndSet(false, true)) {
                    r f = j.this.c;
                    f.c("MediationAdapterWrapper", j.this.f + ": user was rewarded: " + maxReward);
                    a("onUserRewarded", (MaxAdListener) this.f1716a, (Runnable) new Runnable() {
                        public void run() {
                            a.this.f1716a.onUserRewarded(cVar, maxReward);
                        }
                    });
                }
            }
        }
    }

    private static class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final g f1737a;
        /* access modifiers changed from: private */
        public final MaxSignalCollectionListener b;
        /* access modifiers changed from: private */
        public final AtomicBoolean c = new AtomicBoolean();

        b(g gVar, MaxSignalCollectionListener maxSignalCollectionListener) {
            this.f1737a = gVar;
            this.b = maxSignalCollectionListener;
        }
    }

    private class c extends com.applovin.impl.sdk.e.a {
        private c() {
            super("TaskTimeoutMediatedAd", j.this.b);
        }

        public void run() {
            if (!j.this.n.get()) {
                d(j.this.f + " is timing out " + j.this.i + "...");
                this.f1817a.e().a(j.this.i);
                j.this.k.a(b(), -5101);
            }
        }
    }

    private class d extends com.applovin.impl.sdk.e.a {
        private final b f;

        private d(b bVar) {
            super("TaskTimeoutSignalCollection", j.this.b);
            this.f = bVar;
        }

        public void run() {
            if (!this.f.c.get()) {
                d(j.this.f + " is timing out " + this.f.f1737a + "...");
                j jVar = j.this;
                jVar.b("The adapter (" + j.this.f + ") timed out", this.f);
            }
        }
    }

    j(e eVar, MaxAdapter maxAdapter, l lVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("No adapter name specified");
        } else if (maxAdapter == null) {
            throw new IllegalArgumentException("No adapter specified");
        } else if (lVar != null) {
            this.d = eVar.d();
            this.g = maxAdapter;
            this.b = lVar;
            this.c = lVar.j0();
            this.e = eVar;
            this.f = maxAdapter.getClass().getSimpleName();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        r rVar = this.c;
        rVar.c("MediationAdapterWrapper", "Marking " + this.f + " as disabled due to: " + str);
        this.m.set(false);
    }

    /* access modifiers changed from: private */
    public void a(String str, b bVar) {
        if (bVar.c.compareAndSet(false, true) && bVar.b != null) {
            bVar.b.onSignalCollected(str);
        }
    }

    private void a(final String str, final Runnable runnable) {
        AnonymousClass5 r0 = new Runnable() {
            public void run() {
                try {
                    r f = j.this.c;
                    f.b("MediationAdapterWrapper", j.this.f + ": running " + str + "...");
                    runnable.run();
                    r f2 = j.this.c;
                    f2.b("MediationAdapterWrapper", j.this.f + ": finished " + str + "");
                } catch (Throwable th) {
                    r.c("MediationAdapterWrapper", "Unable to run adapter operation " + str + ", marking " + j.this.f + " as disabled", th);
                    j jVar = j.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fail_");
                    sb.append(str);
                    jVar.a(sb.toString());
                }
            }
        };
        if (this.e.j()) {
            this.f1699a.post(r0);
        } else {
            r0.run();
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, b bVar) {
        if (bVar.c.compareAndSet(false, true) && bVar.b != null) {
            bVar.b.onSignalCollectionFailed(str);
        }
    }

    public View a() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void a(final com.applovin.impl.mediation.a.a aVar, final Activity activity) {
        final Runnable runnable;
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (aVar.p() == null) {
            this.k.b("ad_show", -5201);
        } else if (aVar.p() != this) {
            throw new IllegalArgumentException("Mediated ad belongs to a different adapter");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (!this.m.get()) {
            r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is disabled. Showing ads with this adapter is disabled.");
            this.k.b("ad_show", -5103);
        } else if (e()) {
            if (aVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.g instanceof MaxInterstitialAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxInterstitialAdapter) j.this.g).showInterstitialAd(j.this.l, activity, j.this.k);
                        }
                    };
                } else {
                    r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an interstitial adapter.");
                    this.k.b("showFullscreenAd", -5104);
                    return;
                }
            } else if (aVar.getFormat() == MaxAdFormat.REWARDED) {
                if (this.g instanceof MaxRewardedAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxRewardedAdapter) j.this.g).showRewardedAd(j.this.l, activity, j.this.k);
                        }
                    };
                } else {
                    r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an incentivized adapter.");
                    this.k.b("showFullscreenAd", -5104);
                    return;
                }
            } else if (aVar.getFormat() != MaxAdFormat.REWARDED_INTERSTITIAL) {
                throw new IllegalStateException("Failed to show " + aVar + ": " + aVar.getFormat() + " is not a supported ad format");
            } else if (this.g instanceof MaxRewardedInterstitialAdapter) {
                runnable = new Runnable() {
                    public void run() {
                        ((MaxRewardedInterstitialAdapter) j.this.g).showRewardedInterstitialAd(j.this.l, activity, j.this.k);
                    }
                };
            } else {
                r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an incentivized adapter.");
                this.k.b("showFullscreenAd", -5104);
                return;
            }
            a("ad_render", (Runnable) new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Throwable th) {
                        r.c("MediationAdapterWrapper", "Failed to start displaying ad" + aVar, th);
                        j.this.k.b("ad_render", (int) MaxAdapterError.ERROR_CODE_UNSPECIFIED);
                    }
                }
            });
        } else {
            throw new IllegalStateException("Mediation adapter '" + this.f + "' does not have an ad loaded. Please load an ad first");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final MaxAdapterInitializationParameters maxAdapterInitializationParameters, final Activity activity) {
        a("initialize", (Runnable) new Runnable() {
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                j.this.g.initialize(maxAdapterInitializationParameters, activity, new MaxAdapter.OnCompletionListener() {
                    public void onCompletion(final MaxAdapter.InitializationStatus initializationStatus, final String str) {
                        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                            public void run() {
                                long elapsedRealtime = SystemClock.elapsedRealtime();
                                AnonymousClass1 r2 = AnonymousClass1.this;
                                j.this.b.a().a(j.this.e, elapsedRealtime - elapsedRealtime, initializationStatus, str);
                            }
                        }, j.this.e.m());
                    }
                });
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters, g gVar, Activity activity, MaxSignalCollectionListener maxSignalCollectionListener) {
        if (maxSignalCollectionListener == null) {
            throw new IllegalArgumentException("No callback specified");
        } else if (!this.m.get()) {
            r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is disabled. Signal collection ads with this adapter is disabled.");
            maxSignalCollectionListener.onSignalCollectionFailed("The adapter (" + this.f + ") is disabled");
        } else {
            final b bVar = new b(gVar, maxSignalCollectionListener);
            MaxAdapter maxAdapter = this.g;
            if (maxAdapter instanceof MaxSignalProvider) {
                final MaxSignalProvider maxSignalProvider = (MaxSignalProvider) maxAdapter;
                final MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters2 = maxAdapterSignalCollectionParameters;
                final Activity activity2 = activity;
                final g gVar2 = gVar;
                a("collect_signal", (Runnable) new Runnable() {
                    public void run() {
                        StringBuilder sb;
                        maxSignalProvider.collectSignal(maxAdapterSignalCollectionParameters2, activity2, new MaxSignalCollectionListener() {
                            public void onSignalCollected(String str) {
                                AnonymousClass3 r0 = AnonymousClass3.this;
                                j.this.a(str, bVar);
                            }

                            public void onSignalCollectionFailed(String str) {
                                AnonymousClass3 r0 = AnonymousClass3.this;
                                j.this.b(str, bVar);
                            }
                        });
                        if (bVar.c.get()) {
                            return;
                        }
                        if (gVar2.l() == 0) {
                            r f2 = j.this.c;
                            f2.b("MediationAdapterWrapper", "Failing signal collection " + gVar2 + " since it has 0 timeout");
                            j jVar = j.this;
                            jVar.b("The adapter (" + j.this.f + ") has 0 timeout", bVar);
                            return;
                        }
                        int i = (gVar2.l() > 0 ? 1 : (gVar2.l() == 0 ? 0 : -1));
                        r f3 = j.this.c;
                        if (i > 0) {
                            sb.append("Setting timeout ");
                            sb.append(gVar2.l());
                            sb.append("ms. for ");
                            sb.append(gVar2);
                            f3.b("MediationAdapterWrapper", sb.toString());
                            j.this.b.o().a(new d(bVar), o.a.MEDIATION_TIMEOUT, gVar2.l());
                            return;
                        }
                        sb = new StringBuilder();
                        sb.append("Negative timeout set for ");
                        sb.append(gVar2);
                        sb.append(", not scheduling a timeout");
                        f3.b("MediationAdapterWrapper", sb.toString());
                    }
                });
                return;
            }
            b("The adapter (" + this.f + ") does not support signal collection", bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, com.applovin.impl.mediation.a.a aVar) {
        this.h = str;
        this.i = aVar;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, final MaxAdapterResponseParameters maxAdapterResponseParameters, final com.applovin.impl.mediation.a.a aVar, final Activity activity, d dVar) {
        final Runnable runnable;
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (!this.m.get()) {
            r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' was disabled due to earlier failures. Loading ads with this adapter is disabled.");
            dVar.onAdLoadFailed(str, -5103);
        } else {
            this.l = maxAdapterResponseParameters;
            this.k.a(dVar);
            if (aVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.g instanceof MaxInterstitialAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxInterstitialAdapter) j.this.g).loadInterstitialAd(maxAdapterResponseParameters, activity, j.this.k);
                        }
                    };
                } else {
                    r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an interstitial adapter.");
                    this.k.a("loadAd", -5104);
                    return;
                }
            } else if (aVar.getFormat() == MaxAdFormat.REWARDED) {
                if (this.g instanceof MaxRewardedAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxRewardedAdapter) j.this.g).loadRewardedAd(maxAdapterResponseParameters, activity, j.this.k);
                        }
                    };
                } else {
                    r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not a rewarded adapter.");
                    this.k.a("loadAd", -5104);
                    return;
                }
            } else if (aVar.getFormat() == MaxAdFormat.REWARDED_INTERSTITIAL) {
                if (this.g instanceof MaxRewardedInterstitialAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxRewardedInterstitialAdapter) j.this.g).loadRewardedInterstitialAd(maxAdapterResponseParameters, activity, j.this.k);
                        }
                    };
                } else {
                    r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not a rewarded interstitial adapter.");
                    this.k.a("loadAd", -5104);
                    return;
                }
            } else if (!com.applovin.impl.mediation.c.c.d(aVar.getFormat())) {
                throw new IllegalStateException("Failed to load " + aVar + ": " + aVar.getFormat() + " is not a supported ad format");
            } else if (this.g instanceof MaxAdViewAdapter) {
                runnable = new Runnable() {
                    public void run() {
                        ((MaxAdViewAdapter) j.this.g).loadAdViewAd(maxAdapterResponseParameters, aVar.getFormat(), activity, j.this.k);
                    }
                };
            } else {
                r.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an adview-based adapter.");
                this.k.a("loadAd", -5104);
                return;
            }
            a("ad_load", (Runnable) new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Throwable th) {
                        r.c("MediationAdapterWrapper", "Failed start loading " + aVar, th);
                        j.this.k.a("loadAd", -1);
                    }
                    if (!j.this.n.get()) {
                        long l = j.this.e.l();
                        if (l > 0) {
                            r f = j.this.c;
                            f.b("MediationAdapterWrapper", "Setting timeout " + l + "ms. for " + aVar);
                            j.this.b.o().a(new c(), o.a.MEDIATION_TIMEOUT, l);
                            return;
                        }
                        r f2 = j.this.c;
                        f2.b("MediationAdapterWrapper", "Negative timeout set for " + aVar + ", not scheduling a timeout");
                    }
                }
            });
        }
    }

    public String b() {
        return this.d;
    }

    public d c() {
        return this.k.f1716a;
    }

    public boolean d() {
        return this.m.get();
    }

    public boolean e() {
        return this.n.get() && this.o.get();
    }

    public String f() {
        MaxAdapter maxAdapter = this.g;
        if (maxAdapter == null) {
            return null;
        }
        try {
            return maxAdapter.getSdkVersion();
        } catch (Throwable th) {
            r.c("MediationAdapterWrapper", "Unable to get adapter's SDK version, marking " + this + " as disabled", th);
            a("fail_sdk_version");
            return null;
        }
    }

    public String g() {
        MaxAdapter maxAdapter = this.g;
        if (maxAdapter == null) {
            return null;
        }
        try {
            return maxAdapter.getAdapterVersion();
        } catch (Throwable th) {
            r.c("MediationAdapterWrapper", "Unable to get adapter version, marking " + this + " as disabled", th);
            a("fail_adapter_version");
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        a("destroy", (Runnable) new Runnable() {
            public void run() {
                j.this.a("destroy");
                j.this.g.onDestroy();
                MaxAdapter unused = j.this.g = null;
            }
        });
    }

    public String toString() {
        return "MediationAdapterWrapper{adapterTag='" + this.f + "'" + '}';
    }
}
