package com.applovin.impl.a;

import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.r;
import com.applovin.impl.sdk.utils.t;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

public class c {
    private static final List<String> f = Arrays.asList(new String[]{"video/mp4", "video/webm", "video/3gpp", "video/x-matroska"});

    /* renamed from: a  reason: collision with root package name */
    protected List<t> f1439a = new ArrayList();
    private final JSONObject b;
    private final JSONObject c;
    private final b d;
    private final long e = System.currentTimeMillis();

    public c(JSONObject jSONObject, JSONObject jSONObject2, b bVar, l lVar) {
        this.b = jSONObject;
        this.c = jSONObject2;
        this.d = bVar;
    }

    public int a() {
        return this.f1439a.size();
    }

    public List<t> b() {
        return this.f1439a;
    }

    public JSONObject c() {
        return this.b;
    }

    public JSONObject d() {
        return this.c;
    }

    public b e() {
        return this.d;
    }

    public long f() {
        return this.e;
    }

    public List<String> g() {
        List<String> a2 = e.a(j.b(this.b, "vast_preferred_video_types", (String) null, (l) null));
        return !a2.isEmpty() ? a2 : f;
    }

    public int h() {
        return r.a(this.b);
    }
}
