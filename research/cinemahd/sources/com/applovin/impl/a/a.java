package com.applovin.impl.a;

import android.net.Uri;
import com.applovin.impl.a.j;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.o;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class a extends g {
    private final String o;
    private final String p;
    private final f q;
    private final long r;
    private final j s;
    private final b t;
    private final String u;
    private final Set<g> v;
    private final Set<g> w;

    /* renamed from: com.applovin.impl.a.a$a  reason: collision with other inner class name */
    public static class C0007a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public JSONObject f1435a;
        /* access modifiers changed from: private */
        public JSONObject b;
        /* access modifiers changed from: private */
        public com.applovin.impl.sdk.a.b c;
        /* access modifiers changed from: private */
        public l d;
        /* access modifiers changed from: private */
        public long e;
        /* access modifiers changed from: private */
        public String f;
        /* access modifiers changed from: private */
        public String g;
        /* access modifiers changed from: private */
        public f h;
        /* access modifiers changed from: private */
        public j i;
        /* access modifiers changed from: private */
        public b j;
        /* access modifiers changed from: private */
        public Set<g> k;
        /* access modifiers changed from: private */
        public Set<g> l;

        private C0007a() {
        }

        public C0007a a(long j2) {
            this.e = j2;
            return this;
        }

        public C0007a a(b bVar) {
            this.j = bVar;
            return this;
        }

        public C0007a a(f fVar) {
            this.h = fVar;
            return this;
        }

        public C0007a a(j jVar) {
            this.i = jVar;
            return this;
        }

        public C0007a a(com.applovin.impl.sdk.a.b bVar) {
            this.c = bVar;
            return this;
        }

        public C0007a a(l lVar) {
            if (lVar != null) {
                this.d = lVar;
                return this;
            }
            throw new IllegalArgumentException("No sdk specified.");
        }

        public C0007a a(String str) {
            this.f = str;
            return this;
        }

        public C0007a a(Set<g> set) {
            this.k = set;
            return this;
        }

        public C0007a a(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.f1435a = jSONObject;
                return this;
            }
            throw new IllegalArgumentException("No ad object specified.");
        }

        public a a() {
            return new a(this);
        }

        public C0007a b(String str) {
            this.g = str;
            return this;
        }

        public C0007a b(Set<g> set) {
            this.l = set;
            return this;
        }

        public C0007a b(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.b = jSONObject;
                return this;
            }
            throw new IllegalArgumentException("No full ad response specified.");
        }
    }

    public enum b {
        COMPANION_AD,
        VIDEO
    }

    public enum c {
        IMPRESSION,
        VIDEO_CLICK,
        COMPANION_CLICK,
        VIDEO,
        COMPANION,
        ERROR
    }

    private a(C0007a aVar) {
        super(aVar.f1435a, aVar.b, aVar.c, aVar.d);
        this.o = aVar.f;
        this.q = aVar.h;
        this.p = aVar.g;
        this.s = aVar.i;
        this.t = aVar.j;
        this.v = aVar.k;
        this.w = aVar.l;
        Uri m0 = m0();
        this.u = m0 != null ? m0.toString() : "";
        this.r = aVar.e;
    }

    public static C0007a O0() {
        return new C0007a();
    }

    private String P0() {
        String stringFromAdObject = getStringFromAdObject("vimp_url", (String) null);
        if (stringFromAdObject != null) {
            return stringFromAdObject.replace("{CLCODE}", getClCode());
        }
        return null;
    }

    private j.a Q0() {
        j.a[] values = j.a.values();
        int intValue = ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.r3)).intValue();
        return (intValue < 0 || intValue >= values.length) ? j.a.UNSPECIFIED : values[intValue];
    }

    private Set<g> R0() {
        j jVar = this.s;
        return jVar != null ? jVar.d() : Collections.emptySet();
    }

    private Set<g> S0() {
        b bVar = this.t;
        return bVar != null ? bVar.c() : Collections.emptySet();
    }

    private Set<g> a(b bVar, String[] strArr) {
        b bVar2;
        j jVar;
        if (strArr == null || strArr.length <= 0) {
            return Collections.emptySet();
        }
        Map<String, Set<g>> map = null;
        if (bVar == b.VIDEO && (jVar = this.s) != null) {
            map = jVar.e();
        } else if (bVar == b.COMPANION_AD && (bVar2 = this.t) != null) {
            map = bVar2.d();
        }
        HashSet hashSet = new HashSet();
        if (map != null && !map.isEmpty()) {
            for (String str : strArr) {
                if (map.containsKey(str)) {
                    hashSet.addAll(map.get(str));
                }
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public void A() {
    }

    public boolean B0() {
        return getBooleanFromAdObject("vast_fire_click_trackers_on_html_clicks", false);
    }

    public String C0() {
        return getStringFromAdObject("html_template", "");
    }

    public Uri D0() {
        String stringFromAdObject = getStringFromAdObject("html_template_url", (String) null);
        if (o.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public boolean E0() {
        return getBooleanFromAdObject("cache_companion_ad", true);
    }

    public boolean F0() {
        return getBooleanFromAdObject("cache_video", true);
    }

    public void G0() {
        synchronized (this.adObjectLock) {
            this.adObject.remove("vast_is_streaming");
        }
    }

    public b H0() {
        return "companion_ad".equalsIgnoreCase(getStringFromAdObject("vast_first_caching_operation", "companion_ad")) ? b.COMPANION_AD : b.VIDEO;
    }

    public boolean I0() {
        return getBooleanFromAdObject("vast_immediate_ad_load", true);
    }

    public Uri J0() {
        j jVar = this.s;
        if (jVar != null) {
            return jVar.c();
        }
        return null;
    }

    public f K0() {
        return this.q;
    }

    public j L0() {
        return this.s;
    }

    public k M0() {
        j jVar = this.s;
        if (jVar != null) {
            return jVar.a(Q0());
        }
        return null;
    }

    public b N0() {
        return this.t;
    }

    public Set<g> a(c cVar, String str) {
        return a(cVar, new String[]{str});
    }

    public Set<g> a(c cVar, String[] strArr) {
        r j0 = this.sdk.j0();
        j0.b("VastAd", "Retrieving trackers of type '" + cVar + "' and events '" + strArr + "'...");
        if (cVar == c.IMPRESSION) {
            return this.v;
        }
        if (cVar == c.VIDEO_CLICK) {
            return R0();
        }
        if (cVar == c.COMPANION_CLICK) {
            return S0();
        }
        if (cVar == c.VIDEO) {
            return a(b.VIDEO, strArr);
        }
        if (cVar == c.COMPANION) {
            return a(b.COMPANION_AD, strArr);
        }
        if (cVar == c.ERROR) {
            return this.w;
        }
        r j02 = this.sdk.j0();
        j02.e("VastAd", "Failed to retrieve trackers of invalid type '" + cVar + "' and events '" + strArr + "'");
        return Collections.emptySet();
    }

    public void a(String str) {
        synchronized (this.adObjectLock) {
            com.applovin.impl.sdk.utils.j.a(this.adObject, "html_template", str, this.sdk);
        }
    }

    public boolean b() {
        return getBooleanFromAdObject("video_clickable", false) && J0() != null;
    }

    public List<com.applovin.impl.sdk.d.a> b0() {
        List<com.applovin.impl.sdk.d.a> a2;
        synchronized (this.adObjectLock) {
            a2 = com.applovin.impl.sdk.utils.r.a("vimp_urls", this.adObject, getClCode(), e.a("{SOC}", String.valueOf(T())), P0(), c0(), x0(), this.sdk);
        }
        return a2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a) || !super.equals(obj)) {
            return false;
        }
        a aVar = (a) obj;
        String str = this.o;
        if (str == null ? aVar.o != null : !str.equals(aVar.o)) {
            return false;
        }
        String str2 = this.p;
        if (str2 == null ? aVar.p != null : !str2.equals(aVar.p)) {
            return false;
        }
        f fVar = this.q;
        if (fVar == null ? aVar.q != null : !fVar.equals(aVar.q)) {
            return false;
        }
        j jVar = this.s;
        if (jVar == null ? aVar.s != null : !jVar.equals(aVar.s)) {
            return false;
        }
        b bVar = this.t;
        if (bVar == null ? aVar.t != null : !bVar.equals(aVar.t)) {
            return false;
        }
        Set<g> set = this.v;
        if (set == null ? aVar.v != null : !set.equals(aVar.v)) {
            return false;
        }
        Set<g> set2 = this.w;
        Set<g> set3 = aVar.w;
        return set2 != null ? set2.equals(set3) : set3 == null;
    }

    public long getCreatedAtMillis() {
        return this.r;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        r0 = r0.a();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasVideoUrl() {
        /*
            r2 = this;
            com.applovin.impl.a.j r0 = r2.s
            r1 = 0
            if (r0 == 0) goto L_0x0012
            java.util.List r0 = r0.a()
            if (r0 == 0) goto L_0x0012
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0012
            r1 = 1
        L_0x0012:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.a.a.hasVideoUrl():boolean");
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.o;
        int i = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.p;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        f fVar = this.q;
        int hashCode4 = (hashCode3 + (fVar != null ? fVar.hashCode() : 0)) * 31;
        j jVar = this.s;
        int hashCode5 = (hashCode4 + (jVar != null ? jVar.hashCode() : 0)) * 31;
        b bVar = this.t;
        int hashCode6 = (hashCode5 + (bVar != null ? bVar.hashCode() : 0)) * 31;
        Set<g> set = this.v;
        int hashCode7 = (hashCode6 + (set != null ? set.hashCode() : 0)) * 31;
        Set<g> set2 = this.w;
        if (set2 != null) {
            i = set2.hashCode();
        }
        return hashCode7 + i;
    }

    public String k0() {
        return this.u;
    }

    public boolean l0() {
        return getBooleanFromAdObject("vast_is_streaming", false);
    }

    public Uri m0() {
        k M0 = M0();
        if (M0 != null) {
            return M0.b();
        }
        return null;
    }

    public Uri n0() {
        return J0();
    }

    public String toString() {
        return "VastAd{title='" + this.o + '\'' + ", adDescription='" + this.p + '\'' + ", systemInfo=" + this.q + ", videoCreative=" + this.s + ", companionAd=" + this.t + ", impressionTrackers=" + this.v + ", errorTrackers=" + this.w + '}';
    }
}
