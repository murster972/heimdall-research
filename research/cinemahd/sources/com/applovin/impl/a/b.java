package com.applovin.impl.a;

import android.net.Uri;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private int f1438a;
    private int b;
    private Uri c;
    private e d;
    private Set<g> e = new HashSet();
    private Map<String, Set<g>> f = new HashMap();

    private b() {
    }

    public static b a(t tVar, b bVar, c cVar, l lVar) {
        t b2;
        if (tVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (lVar != null) {
            if (bVar == null) {
                try {
                    bVar = new b();
                } catch (Throwable th) {
                    lVar.j0().b("VastCompanionAd", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (bVar.f1438a == 0 && bVar.b == 0) {
                int a2 = o.a(tVar.b().get("width"));
                int a3 = o.a(tVar.b().get("height"));
                if (a2 > 0 && a3 > 0) {
                    bVar.f1438a = a2;
                    bVar.b = a3;
                }
            }
            bVar.d = e.a(tVar, bVar.d, lVar);
            if (bVar.c == null && (b2 = tVar.b("CompanionClickThrough")) != null) {
                String c2 = b2.c();
                if (o.b(c2)) {
                    bVar.c = Uri.parse(c2);
                }
            }
            i.a(tVar.a("CompanionClickTracking"), bVar.e, cVar, lVar);
            i.a(tVar, bVar.f, cVar, lVar);
            return bVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public Uri a() {
        return this.c;
    }

    public e b() {
        return this.d;
    }

    public Set<g> c() {
        return this.e;
    }

    public Map<String, Set<g>> d() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f1438a != bVar.f1438a || this.b != bVar.b) {
            return false;
        }
        Uri uri = this.c;
        if (uri == null ? bVar.c != null : !uri.equals(bVar.c)) {
            return false;
        }
        e eVar = this.d;
        if (eVar == null ? bVar.d != null : !eVar.equals(bVar.d)) {
            return false;
        }
        Set<g> set = this.e;
        if (set == null ? bVar.e != null : !set.equals(bVar.e)) {
            return false;
        }
        Map<String, Set<g>> map = this.f;
        Map<String, Set<g>> map2 = bVar.f;
        return map != null ? map.equals(map2) : map2 == null;
    }

    public int hashCode() {
        int i = ((this.f1438a * 31) + this.b) * 31;
        Uri uri = this.c;
        int i2 = 0;
        int hashCode = (i + (uri != null ? uri.hashCode() : 0)) * 31;
        e eVar = this.d;
        int hashCode2 = (hashCode + (eVar != null ? eVar.hashCode() : 0)) * 31;
        Set<g> set = this.e;
        int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
        Map<String, Set<g>> map = this.f;
        if (map != null) {
            i2 = map.hashCode();
        }
        return hashCode3 + i2;
    }

    public String toString() {
        return "VastCompanionAd{width=" + this.f1438a + ", height=" + this.b + ", destinationUri=" + this.c + ", nonVideoResource=" + this.d + ", clickTrackers=" + this.e + ", eventTrackers=" + this.f + '}';
    }
}
