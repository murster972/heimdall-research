package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import java.util.Locale;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private Uri f1449a;
    private Uri b;
    private a c;
    private String d;
    private int e;
    private int f;
    private int g;

    public enum a {
        Progressive,
        Streaming
    }

    private k() {
    }

    private static a a(String str) {
        if (o.b(str)) {
            if ("progressive".equalsIgnoreCase(str)) {
                return a.Progressive;
            }
            if ("streaming".equalsIgnoreCase(str)) {
                return a.Streaming;
            }
        }
        return a.Progressive;
    }

    public static k a(t tVar, l lVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (lVar != null) {
            try {
                String c2 = tVar.c();
                if (URLUtil.isValidUrl(c2)) {
                    Uri parse = Uri.parse(c2);
                    k kVar = new k();
                    kVar.f1449a = parse;
                    kVar.b = parse;
                    kVar.g = o.a(tVar.b().get("bitrate"));
                    kVar.c = a(tVar.b().get("delivery"));
                    kVar.f = o.a(tVar.b().get("height"));
                    kVar.e = o.a(tVar.b().get("width"));
                    kVar.d = tVar.b().get("type").toLowerCase(Locale.ENGLISH);
                    return kVar;
                }
                lVar.j0().e("VastVideoFile", "Unable to create video file. Could not find URL.");
                return null;
            } catch (Throwable th) {
                lVar.j0().b("VastVideoFile", "Error occurred while initializing", th);
                return null;
            }
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public Uri a() {
        return this.f1449a;
    }

    public void a(Uri uri) {
        this.b = uri;
    }

    public Uri b() {
        return this.b;
    }

    public String c() {
        return this.d;
    }

    public int d() {
        return this.g;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof k)) {
            return false;
        }
        k kVar = (k) obj;
        if (this.e != kVar.e || this.f != kVar.f || this.g != kVar.g) {
            return false;
        }
        Uri uri = this.f1449a;
        if (uri == null ? kVar.f1449a != null : !uri.equals(kVar.f1449a)) {
            return false;
        }
        Uri uri2 = this.b;
        if (uri2 == null ? kVar.b != null : !uri2.equals(kVar.b)) {
            return false;
        }
        if (this.c != kVar.c) {
            return false;
        }
        String str = this.d;
        String str2 = kVar.d;
        return str != null ? str.equals(str2) : str2 == null;
    }

    public int hashCode() {
        Uri uri = this.f1449a;
        int i = 0;
        int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
        Uri uri2 = this.b;
        int hashCode2 = (hashCode + (uri2 != null ? uri2.hashCode() : 0)) * 31;
        a aVar = this.c;
        int hashCode3 = (hashCode2 + (aVar != null ? aVar.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i = str.hashCode();
        }
        return ((((((hashCode3 + i) * 31) + this.e) * 31) + this.f) * 31) + this.g;
    }

    public String toString() {
        return "VastVideoFile{sourceVideoUri=" + this.f1449a + ", videoUri=" + this.b + ", deliveryType=" + this.c + ", fileType='" + this.d + '\'' + ", width=" + this.e + ", height=" + this.f + ", bitrate=" + this.g + '}';
    }
}
