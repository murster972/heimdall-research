package com.applovin.impl.a;

import android.annotation.TargetApi;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private List<k> f1447a;
    private List<String> b;
    private int c;
    private Uri d;
    private final Set<g> e = new HashSet();
    private final Map<String, Set<g>> f = new HashMap();

    public enum a {
        UNSPECIFIED,
        LOW,
        MEDIUM,
        HIGH
    }

    private j() {
        List<k> list = Collections.EMPTY_LIST;
        this.f1447a = list;
        this.b = list;
    }

    private j(c cVar) {
        List<k> list = Collections.EMPTY_LIST;
        this.f1447a = list;
        this.b = list;
        this.b = cVar.g();
    }

    private static int a(String str, l lVar) {
        try {
            List<String> a2 = e.a(str, ":");
            if (a2.size() == 3) {
                return (int) (TimeUnit.HOURS.toSeconds((long) o.a(a2.get(0))) + TimeUnit.MINUTES.toSeconds((long) o.a(a2.get(1))) + ((long) o.a(a2.get(2))));
            }
        } catch (Throwable unused) {
            r j0 = lVar.j0();
            j0.e("VastVideoCreative", "Unable to parse duration from \"" + str + "\"");
        }
        return 0;
    }

    public static j a(t tVar, j jVar, c cVar, l lVar) {
        t b2;
        List<k> a2;
        t b3;
        int a3;
        if (tVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("No context specified.");
        } else if (lVar != null) {
            if (jVar == null) {
                try {
                    jVar = new j(cVar);
                } catch (Throwable th) {
                    lVar.j0().b("VastVideoCreative", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (jVar.c == 0 && (b3 = tVar.b("Duration")) != null && (a3 = a(b3.c(), lVar)) > 0) {
                jVar.c = a3;
            }
            t b4 = tVar.b("MediaFiles");
            if (!(b4 == null || (a2 = a(b4, lVar)) == null || a2.size() <= 0)) {
                if (jVar.f1447a != null) {
                    a2.addAll(jVar.f1447a);
                }
                jVar.f1447a = a2;
            }
            t b5 = tVar.b("VideoClicks");
            if (b5 != null) {
                if (jVar.d == null && (b2 = b5.b("ClickThrough")) != null) {
                    String c2 = b2.c();
                    if (o.b(c2)) {
                        jVar.d = Uri.parse(c2);
                    }
                }
                i.a(b5.a("ClickTracking"), jVar.e, cVar, lVar);
            }
            i.a(tVar, jVar.f, cVar, lVar);
            return jVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static List<k> a(t tVar, l lVar) {
        List<t> a2 = tVar.a("MediaFile");
        ArrayList arrayList = new ArrayList(a2.size());
        List<String> a3 = e.a((String) lVar.a(b.p3));
        List<String> a4 = e.a((String) lVar.a(b.o3));
        for (t a5 : a2) {
            k a6 = k.a(a5, lVar);
            if (a6 != null) {
                try {
                    String c2 = a6.c();
                    if (!o.b(c2) || a3.contains(c2)) {
                        if (((Boolean) lVar.a(b.q3)).booleanValue()) {
                            String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(a6.b().toString());
                            if (o.b(fileExtensionFromUrl) && !a4.contains(fileExtensionFromUrl)) {
                            }
                        }
                        r j0 = lVar.j0();
                        j0.d("VastVideoCreative", "Video file not supported: " + a6);
                    }
                    arrayList.add(a6);
                } catch (Throwable th) {
                    r j02 = lVar.j0();
                    j02.b("VastVideoCreative", "Failed to validate video file: " + a6, th);
                }
            }
        }
        return arrayList;
    }

    public k a(a aVar) {
        List<k> list = this.f1447a;
        if (list == null || list.size() == 0) {
            return null;
        }
        List arrayList = new ArrayList(3);
        for (String next : this.b) {
            for (k next2 : this.f1447a) {
                String c2 = next2.c();
                if (o.b(c2) && next.equalsIgnoreCase(c2)) {
                    arrayList.add(next2);
                }
            }
            if (!arrayList.isEmpty()) {
                break;
            }
        }
        if (arrayList.isEmpty()) {
            arrayList = this.f1447a;
        }
        if (g.c()) {
            Collections.sort(arrayList, new Comparator<k>(this) {
                @TargetApi(19)
                /* renamed from: a */
                public int compare(k kVar, k kVar2) {
                    return Integer.compare(kVar.d(), kVar2.d());
                }
            });
        }
        return (k) arrayList.get(aVar == a.LOW ? 0 : aVar == a.MEDIUM ? arrayList.size() / 2 : arrayList.size() - 1);
    }

    public List<k> a() {
        return this.f1447a;
    }

    public int b() {
        return this.c;
    }

    public Uri c() {
        return this.d;
    }

    public Set<g> d() {
        return this.e;
    }

    public Map<String, Set<g>> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        if (this.c != jVar.c) {
            return false;
        }
        List<k> list = this.f1447a;
        if (list == null ? jVar.f1447a != null : !list.equals(jVar.f1447a)) {
            return false;
        }
        Uri uri = this.d;
        if (uri == null ? jVar.d != null : !uri.equals(jVar.d)) {
            return false;
        }
        Set<g> set = this.e;
        if (set == null ? jVar.e != null : !set.equals(jVar.e)) {
            return false;
        }
        Map<String, Set<g>> map = this.f;
        Map<String, Set<g>> map2 = jVar.f;
        return map != null ? map.equals(map2) : map2 == null;
    }

    public int hashCode() {
        List<k> list = this.f1447a;
        int i = 0;
        int hashCode = (((list != null ? list.hashCode() : 0) * 31) + this.c) * 31;
        Uri uri = this.d;
        int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
        Set<g> set = this.e;
        int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
        Map<String, Set<g>> map = this.f;
        if (map != null) {
            i = map.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        return "VastVideoCreative{videoFiles=" + this.f1447a + ", durationSeconds=" + this.c + ", destinationUri=" + this.d + ", clickTrackers=" + this.e + ", eventTrackers=" + this.f + '}';
    }
}
