package com.applovin.impl.a;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import com.unity3d.ads.metadata.MediationMetaData;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private String f1443a;
    private String b;

    private f() {
    }

    public static f a(t tVar, f fVar, l lVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (lVar != null) {
            if (fVar == null) {
                try {
                    fVar = new f();
                } catch (Throwable th) {
                    lVar.j0().b("VastSystemInfo", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (!o.b(fVar.f1443a)) {
                String c = tVar.c();
                if (o.b(c)) {
                    fVar.f1443a = c;
                }
            }
            if (!o.b(fVar.b)) {
                String str = tVar.b().get(MediationMetaData.KEY_VERSION);
                if (o.b(str)) {
                    fVar.b = str;
                }
            }
            return fVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public String a() {
        return this.f1443a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        String str = this.f1443a;
        if (str == null ? fVar.f1443a != null : !str.equals(fVar.f1443a)) {
            return false;
        }
        String str2 = this.b;
        String str3 = fVar.b;
        return str2 != null ? str2.equals(str3) : str3 == null;
    }

    public int hashCode() {
        String str = this.f1443a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "VastSystemInfo{name='" + this.f1443a + '\'' + ", version='" + this.b + '\'' + '}';
    }
}
