package com.applovin.impl.a;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import com.facebook.react.uimanager.ViewProps;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private String f1444a;
    private String b;
    private String c;
    private long d = -1;
    private int e = -1;

    private g() {
    }

    private static int a(String str, c cVar) {
        if (ViewProps.START.equalsIgnoreCase(str)) {
            return 0;
        }
        if ("firstQuartile".equalsIgnoreCase(str)) {
            return 25;
        }
        if ("midpoint".equalsIgnoreCase(str)) {
            return 50;
        }
        if ("thirdQuartile".equalsIgnoreCase(str)) {
            return 75;
        }
        if (!"complete".equalsIgnoreCase(str)) {
            return -1;
        }
        if (cVar != null) {
            return cVar.h();
        }
        return 95;
    }

    public static g a(t tVar, c cVar, l lVar) {
        TimeUnit timeUnit;
        long seconds;
        if (tVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (lVar != null) {
            try {
                String c2 = tVar.c();
                if (o.b(c2)) {
                    g gVar = new g();
                    gVar.c = c2;
                    gVar.f1444a = tVar.b().get("id");
                    gVar.b = tVar.b().get("event");
                    gVar.e = a(gVar.a(), cVar);
                    String str = tVar.b().get("offset");
                    if (o.b(str)) {
                        String trim = str.trim();
                        if (trim.contains("%")) {
                            gVar.e = o.a(trim.substring(0, trim.length() - 1));
                        } else if (trim.contains(":")) {
                            List<String> a2 = e.a(trim, ":");
                            int size = a2.size();
                            if (size > 0) {
                                long j = 0;
                                int i = size - 1;
                                for (int i2 = i; i2 >= 0; i2--) {
                                    String str2 = a2.get(i2);
                                    if (o.d(str2)) {
                                        int parseInt = Integer.parseInt(str2);
                                        if (i2 == i) {
                                            seconds = (long) parseInt;
                                        } else {
                                            if (i2 == size - 2) {
                                                timeUnit = TimeUnit.MINUTES;
                                            } else if (i2 == size - 3) {
                                                timeUnit = TimeUnit.HOURS;
                                            }
                                            seconds = timeUnit.toSeconds((long) parseInt);
                                        }
                                        j += seconds;
                                    }
                                }
                                gVar.d = j;
                                gVar.e = -1;
                            }
                        } else {
                            lVar.j0().e("VastTracker", "Unable to parse time offset from rawOffsetString = " + trim);
                        }
                    }
                    return gVar;
                }
                lVar.j0().e("VastTracker", "Unable to create tracker. Could not find URL.");
                return null;
            } catch (Throwable th) {
                lVar.j0().b("VastTracker", "Error occurred while initializing", th);
                return null;
            }
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public String a() {
        return this.b;
    }

    public boolean a(long j, int i) {
        boolean z = this.d >= 0;
        boolean z2 = j >= this.d;
        boolean z3 = this.e >= 0;
        boolean z4 = i >= this.e;
        if (!z || !z2) {
            return z3 && z4;
        }
        return true;
    }

    public String b() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        if (this.d != gVar.d || this.e != gVar.e) {
            return false;
        }
        String str = this.f1444a;
        if (str == null ? gVar.f1444a != null : !str.equals(gVar.f1444a)) {
            return false;
        }
        String str2 = this.b;
        if (str2 == null ? gVar.b == null : str2.equals(gVar.b)) {
            return this.c.equals(gVar.c);
        }
        return false;
    }

    public int hashCode() {
        String str = this.f1444a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        long j = this.d;
        return ((((((hashCode + i) * 31) + this.c.hashCode()) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.e;
    }

    public String toString() {
        return "VastTracker{identifier='" + this.f1444a + '\'' + ", event='" + this.b + '\'' + ", uriString='" + this.c + '\'' + ", offsetSeconds=" + this.d + ", offsetPercent=" + this.e + '}';
    }
}
