package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private static DateFormat f1446a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
    private static Random b = new Random(System.currentTimeMillis());

    public static Uri a(String str, long j, Uri uri, d dVar, l lVar) {
        if (URLUtil.isValidUrl(str)) {
            try {
                String replace = str.replace("[ERRORCODE]", Integer.toString(dVar.a()));
                if (j >= 0) {
                    replace = replace.replace("[CONTENTPLAYHEAD]", a(j));
                }
                if (uri != null) {
                    replace = replace.replace("[ASSETURI]", uri.toString());
                }
                return Uri.parse(replace.replace("[CACHEBUSTING]", a()).replace("[TIMESTAMP]", b()));
            } catch (Throwable th) {
                r j0 = lVar.j0();
                j0.b("VastUtils", "Unable to replace macros in URL string " + str, th);
                return null;
            }
        } else {
            lVar.j0().e("VastUtils", "Unable to replace macros in invalid URL string.");
            return null;
        }
    }

    public static d a(a aVar) {
        if (b(aVar) || c(aVar)) {
            return null;
        }
        return d.GENERAL_WRAPPER_ERROR;
    }

    private static String a() {
        return Integer.toString(b.nextInt(89999999) + 10000000);
    }

    private static String a(long j) {
        if (j <= 0) {
            return "00:00:00.000";
        }
        long hours = TimeUnit.SECONDS.toHours(j);
        long seconds = j % TimeUnit.MINUTES.toSeconds(1);
        return String.format(Locale.US, "%02d:%02d:%02d.000", new Object[]{Long.valueOf(hours), Long.valueOf(TimeUnit.SECONDS.toMinutes(j) % TimeUnit.MINUTES.toSeconds(1)), Long.valueOf(seconds)});
    }

    public static String a(c cVar) {
        t c;
        if (cVar != null) {
            List<t> b2 = cVar.b();
            int size = cVar.b().size();
            if (size <= 0 || (c = b2.get(size - 1).c("VASTAdTagURI")) == null) {
                return null;
            }
            return c.c();
        }
        throw new IllegalArgumentException("Unable to get resolution uri string for fetching the next wrapper or inline response in the chain");
    }

    public static String a(t tVar, String str, String str2) {
        t b2 = tVar.b(str);
        if (b2 != null) {
            String c = b2.c();
            if (o.b(c)) {
                return c;
            }
        }
        return str2;
    }

    private static Set<g> a(c cVar, l lVar) {
        if (cVar == null) {
            return null;
        }
        List<t> b2 = cVar.b();
        HashSet hashSet = new HashSet(b2.size());
        for (t next : b2) {
            t c = next.c("Wrapper");
            if (c == null) {
                c = next.c("InLine");
            }
            a((Set<g>) hashSet, c != null ? c.a("Error") : next.a("Error"), cVar, lVar);
        }
        r j0 = lVar.j0();
        j0.b("VastUtils", "Retrieved " + hashSet.size() + " top level error trackers: " + hashSet);
        return hashSet;
    }

    private static Set<g> a(Set<g> set, List<t> list, c cVar, l lVar) {
        if (list != null) {
            for (t a2 : list) {
                g a3 = g.a(a2, cVar, lVar);
                if (a3 != null) {
                    set.add(a3);
                }
            }
        }
        return set;
    }

    public static void a(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, d dVar, int i, l lVar) {
        if (lVar != null) {
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.failedToReceiveAd(i);
            }
            a(a(cVar, lVar), dVar, lVar);
            return;
        }
        throw new IllegalArgumentException("Unable to handle failure. No sdk specified.");
    }

    public static void a(t tVar, Map<String, Set<g>> map, c cVar, l lVar) {
        List<t> a2;
        r j0;
        String str;
        if (lVar != null) {
            if (tVar == null) {
                j0 = lVar.j0();
                str = "Unable to render event trackers; null node provided";
            } else if (map == null) {
                j0 = lVar.j0();
                str = "Unable to render event trackers; null event trackers provided";
            } else {
                t b2 = tVar.b("TrackingEvents");
                if (b2 != null && (a2 = b2.a("Tracking")) != null) {
                    for (t next : a2) {
                        String str2 = next.b().get("event");
                        if (o.b(str2)) {
                            g a3 = g.a(next, cVar, lVar);
                            if (a3 != null) {
                                Set set = map.get(str2);
                                if (set != null) {
                                    set.add(a3);
                                } else {
                                    HashSet hashSet = new HashSet();
                                    hashSet.add(a3);
                                    map.put(str2, hashSet);
                                }
                            }
                        } else {
                            r j02 = lVar.j0();
                            j02.e("VastUtils", "Could not find event for tracking node = " + next);
                        }
                    }
                    return;
                }
                return;
            }
            j0.e("VastUtils", str);
            return;
        }
        throw new IllegalArgumentException("Unable to render event trackers. No sdk specified.");
    }

    public static void a(List<t> list, Set<g> set, c cVar, l lVar) {
        r j0;
        String str;
        if (lVar != null) {
            if (list == null) {
                j0 = lVar.j0();
                str = "Unable to render trackers; null nodes provided";
            } else if (set == null) {
                j0 = lVar.j0();
                str = "Unable to render trackers; null trackers provided";
            } else {
                for (t a2 : list) {
                    g a3 = g.a(a2, cVar, lVar);
                    if (a3 != null) {
                        set.add(a3);
                    }
                }
                return;
            }
            j0.e("VastUtils", str);
            return;
        }
        throw new IllegalArgumentException("Unable to render trackers. No sdk specified.");
    }

    public static void a(Set<g> set, long j, Uri uri, d dVar, l lVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("Unable to fire trackers. No sdk specified.");
        } else if (set != null && !set.isEmpty()) {
            for (g b2 : set) {
                Uri a2 = a(b2.b(), j, uri, dVar, lVar);
                if (a2 != null) {
                    lVar.q().a(f.o().c(a2.toString()).a(false).a(), false);
                }
            }
        }
    }

    public static void a(Set<g> set, d dVar, l lVar) {
        a(set, -1, (Uri) null, dVar, lVar);
    }

    public static void a(Set<g> set, l lVar) {
        a(set, -1, (Uri) null, d.UNSPECIFIED, lVar);
    }

    public static boolean a(t tVar) {
        if (tVar != null) {
            return tVar.c("Wrapper") != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains a wrapper response");
    }

    private static String b() {
        f1446a.setTimeZone(TimeZone.getDefault());
        return f1446a.format(new Date());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        r1 = (r1 = r1.L0()).a();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(com.applovin.impl.a.a r1) {
        /*
            r0 = 0
            if (r1 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.applovin.impl.a.j r1 = r1.L0()
            if (r1 == 0) goto L_0x0017
            java.util.List r1 = r1.a()
            if (r1 == 0) goto L_0x0017
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0017
            r0 = 1
        L_0x0017:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.a.i.b(com.applovin.impl.a.a):boolean");
    }

    public static boolean b(t tVar) {
        if (tVar != null) {
            return tVar.c("InLine") != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains an inline response");
    }

    public static boolean c(a aVar) {
        b N0;
        e b2;
        if (aVar == null || (N0 = aVar.N0()) == null || (b2 = N0.b()) == null) {
            return false;
        }
        return b2.b() != null || o.b(b2.c());
    }
}
