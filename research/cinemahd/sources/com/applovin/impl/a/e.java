package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private a f1441a;
    private Uri b;
    private String c;

    public enum a {
        UNSPECIFIED,
        STATIC,
        IFRAME,
        HTML
    }

    private e() {
    }

    static e a(t tVar, e eVar, l lVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (lVar != null) {
            if (eVar == null) {
                try {
                    eVar = new e();
                } catch (Throwable th) {
                    lVar.j0().b("VastNonVideoResource", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (eVar.b == null && !o.b(eVar.c)) {
                String a2 = a(tVar, "StaticResource");
                if (URLUtil.isValidUrl(a2)) {
                    eVar.b = Uri.parse(a2);
                    eVar.f1441a = a.STATIC;
                    return eVar;
                }
                String a3 = a(tVar, "IFrameResource");
                if (o.b(a3)) {
                    eVar.f1441a = a.IFRAME;
                    if (URLUtil.isValidUrl(a3)) {
                        eVar.b = Uri.parse(a3);
                    } else {
                        eVar.c = a3;
                    }
                    return eVar;
                }
                String a4 = a(tVar, "HTMLResource");
                if (o.b(a4)) {
                    eVar.f1441a = a.HTML;
                    if (URLUtil.isValidUrl(a4)) {
                        eVar.b = Uri.parse(a4);
                    } else {
                        eVar.c = a4;
                    }
                }
            }
            return eVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static String a(t tVar, String str) {
        t b2 = tVar.b(str);
        if (b2 != null) {
            return b2.c();
        }
        return null;
    }

    public a a() {
        return this.f1441a;
    }

    public void a(Uri uri) {
        this.b = uri;
    }

    public void a(String str) {
        this.c = str;
    }

    public Uri b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        if (this.f1441a != eVar.f1441a) {
            return false;
        }
        Uri uri = this.b;
        if (uri == null ? eVar.b != null : !uri.equals(eVar.b)) {
            return false;
        }
        String str = this.c;
        String str2 = eVar.c;
        return str != null ? str.equals(str2) : str2 == null;
    }

    public int hashCode() {
        a aVar = this.f1441a;
        int i = 0;
        int hashCode = (aVar != null ? aVar.hashCode() : 0) * 31;
        Uri uri = this.b;
        int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
        String str = this.c;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "VastNonVideoResource{type=" + this.f1441a + ", resourceUri=" + this.b + ", resourceContents='" + this.c + '\'' + '}';
    }
}
