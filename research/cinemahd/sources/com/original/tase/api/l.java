package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class l implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MvDatabase f5858a;
    private final /* synthetic */ String b;

    public /* synthetic */ l(MvDatabase mvDatabase, String str) {
        this.f5858a = mvDatabase;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return TraktUserApi.f().c(this.f5858a, this.b);
    }
}
