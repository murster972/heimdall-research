package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class d implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MvDatabase f5850a;
    private final /* synthetic */ String b;

    public /* synthetic */ d(MvDatabase mvDatabase, String str) {
        this.f5850a = mvDatabase;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return TraktUserApi.f().b(this.f5850a, this.b);
    }
}
