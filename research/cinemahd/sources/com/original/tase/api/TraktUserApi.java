package com.original.tase.api;

import android.app.Activity;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.database.entitys.TvWatchedEpisode;
import com.google.gson.Gson;
import com.movie.FreeMoviesApp;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.event.trakt.TrackSyncFaild;
import com.original.tase.event.trakt.TraktSyncSuccess;
import com.original.tase.event.trakt.TraktWaitingToVerifyEvent;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.original.tase.helper.trakt.TraktHelper;
import com.original.tase.model.trakt.TraktCredentialsInfo;
import com.original.tase.model.trakt.TraktGetDeviceCodeResult;
import com.original.tase.model.trakt.TraktGetTokenResult;
import com.original.tase.model.trakt.TraktUserInfo;
import com.original.tase.utils.Utils;
import com.uwetrottmann.trakt5.TraktV2;
import com.uwetrottmann.trakt5.entities.BaseEpisode;
import com.uwetrottmann.trakt5.entities.BaseMovie;
import com.uwetrottmann.trakt5.entities.BaseSeason;
import com.uwetrottmann.trakt5.entities.BaseShow;
import com.uwetrottmann.trakt5.entities.CalendarShowEntry;
import com.uwetrottmann.trakt5.entities.MovieIds;
import com.uwetrottmann.trakt5.entities.ShowIds;
import com.uwetrottmann.trakt5.entities.SyncEpisode;
import com.uwetrottmann.trakt5.entities.SyncItems;
import com.uwetrottmann.trakt5.entities.SyncMovie;
import com.uwetrottmann.trakt5.entities.SyncResponse;
import com.uwetrottmann.trakt5.entities.SyncSeason;
import com.uwetrottmann.trakt5.entities.SyncShow;
import com.uwetrottmann.trakt5.entities.UserSlug;
import com.uwetrottmann.trakt5.enums.Extended;
import com.uwetrottmann.trakt5.services.Sync;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.threeten.bp.LocalDate;
import org.threeten.bp.OffsetDateTime;
import retrofit2.Call;

public class TraktUserApi {
    private static volatile TraktUserApi b;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Hashtable<String, String> f5840a = new Hashtable<>();

    private TraktUserApi() {
        this.f5840a.put(TraktV2.HEADER_CONTENT_TYPE, TraktV2.CONTENT_TYPE_JSON);
        this.f5840a.put(TraktV2.HEADER_TRAKT_API_KEY, "9d67bbba437a1e94273db1f54b56fb84ff616b66db7e47c309bc19ccdf954c89");
        this.f5840a.put(TraktV2.HEADER_TRAKT_API_VERSION, String.valueOf(2));
        TraktCredentialsInfo b2 = TraktCredentialsHelper.b();
        if (b2.isValid()) {
            Hashtable<String, String> hashtable = this.f5840a;
            hashtable.put("Authorization", "Bearer " + b2.getAccessToken());
        }
    }

    public static TraktUserApi f() {
        if (b == null) {
            synchronized (TraktUserApi.class) {
                b = new TraktUserApi();
            }
        }
        return b;
    }

    public List<BaseShow> b() throws IOException {
        return TraktHelper.a().sync().collectionShows(Extended.FULL).execute().body();
    }

    public List<BaseMovie> c() throws IOException {
        return TraktHelper.a().users().watchedMovies(UserSlug.ME, Extended.FULL).execute().body();
    }

    public List<BaseShow> d() throws IOException {
        return TraktHelper.a().users().watchedShows(UserSlug.ME, Extended.FULL).execute().body();
    }

    public Observable<Boolean> e() {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            public void subscribe(ObservableEmitter<Boolean> observableEmitter) throws Exception {
                TraktGetTokenResult traktGetTokenResult;
                ObservableEmitter<Boolean> observableEmitter2 = observableEmitter;
                if (TraktCredentialsHelper.b().isValid()) {
                    observableEmitter2.onNext(true);
                    observableEmitter.onComplete();
                    return;
                }
                HttpHelper e = HttpHelper.e();
                String a2 = e.a("https://api.trakt.tv/oauth/device/code", "{\"client_id\":\"" + Utils.a("5b1832a33f14f850e8f6b3cc1928709071ba9f9b05ae55816d3cb1a41c0399e1", new boolean[0]) + "\"}", false, (Map<String, String>[]) new Map[]{TraktUserApi.this.f5840a});
                if (a2.isEmpty()) {
                    observableEmitter2.onNext(false);
                    observableEmitter.onComplete();
                    return;
                }
                TraktGetDeviceCodeResult traktGetDeviceCodeResult = (TraktGetDeviceCodeResult) new Gson().a(a2, TraktGetDeviceCodeResult.class);
                String verification_url = traktGetDeviceCodeResult.getVerification_url();
                String user_code = traktGetDeviceCodeResult.getUser_code();
                String device_code = traktGetDeviceCodeResult.getDevice_code();
                int expires_in = traktGetDeviceCodeResult.getExpires_in();
                int interval = traktGetDeviceCodeResult.getInterval();
                RxBus.b().a(new TraktWaitingToVerifyEvent(verification_url, user_code));
                int i = 0;
                while (true) {
                    traktGetTokenResult = null;
                    if (i >= expires_in || observableEmitter.isDisposed()) {
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                        if (((float) i) % ((float) interval) == 0.0f) {
                            HttpHelper e2 = HttpHelper.e();
                            String a3 = e2.a("https://api.trakt.tv/oauth/device/token", "{\"client_id\":\"" + Utils.a("5b1832a33f14f850e8f6b3cc1928709071ba9f9b05ae55816d3cb1a41c0399e1", new boolean[0]) + "\",\"client_secret\":\"" + Utils.a("04b1b371036a69beebecf1ca4a1a38ff3212ff5b014b75c1eb2897cfe5621f72", new boolean[0]) + "\",\"code\":\"" + Utils.a(device_code, new boolean[0]) + "\"}", false, (Map<String, String>[]) new Map[]{TraktUserApi.this.f5840a});
                            if (a3.contains("access_token")) {
                                traktGetTokenResult = (TraktGetTokenResult) new Gson().a(a3, TraktGetTokenResult.class);
                                break;
                            }
                        }
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                    }
                    i++;
                }
                if (traktGetTokenResult == null || traktGetTokenResult.getAccess_token() == null || traktGetTokenResult.getAccess_token().isEmpty()) {
                    observableEmitter2.onNext(false);
                    observableEmitter.onComplete();
                    return;
                }
                String access_token = traktGetTokenResult.getAccess_token();
                Hashtable a4 = TraktUserApi.this.f5840a;
                a4.put("Authorization", "Bearer " + access_token);
                TraktCredentialsInfo traktCredentialsInfo = new TraktCredentialsInfo();
                traktCredentialsInfo.setAccessToken(access_token);
                traktCredentialsInfo.setRefreshToken(traktGetTokenResult.getRefresh_token());
                try {
                    traktCredentialsInfo.setUser(((TraktUserInfo) new Gson().a(HttpHelper.e().a("https://api.trakt.tv/users/me", (Map<String, String>[]) new Map[]{TraktUserApi.this.f5840a}), TraktUserInfo.class)).getUsername());
                } catch (Throwable th2) {
                    Logger.a(th2, "Unable to get trakt user info", new boolean[0]);
                }
                TraktCredentialsHelper.a(traktCredentialsInfo);
                observableEmitter2.onNext(true);
                observableEmitter.onComplete();
            }
        });
    }

    public void a(MovieEntity movieEntity, boolean z) throws IOException {
        Call<SyncResponse> call;
        MovieIds movieIds = new MovieIds();
        movieIds.imdb = movieEntity.getImdbIDStr();
        movieIds.tmdb = Integer.valueOf((int) movieEntity.getTmdbID());
        SyncMovie id = new SyncMovie().id(movieIds);
        id.watched_at = movieEntity.getCreatedDate();
        SyncItems syncItems = new SyncItems();
        syncItems.movies(id);
        Sync sync = TraktHelper.a().sync();
        if (z) {
            call = sync.addItemsToWatchedHistory(syncItems);
        } else {
            call = sync.deleteItemsFromWatchedHistory(syncItems);
        }
        call.execute();
    }

    public List<CalendarShowEntry> b(String str) throws IOException {
        return TraktHelper.a().calendars().myShows(str, 1).execute().body();
    }

    public Observable<Boolean> c(final MvDatabase mvDatabase, final String str) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>(this) {
            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                List<BaseMovie> c = TraktUserApi.f().c();
                if (c != null) {
                    ArrayList arrayList = new ArrayList();
                    for (BaseMovie a2 : c) {
                        arrayList.add(TraktUserApi.a(a2));
                    }
                    observableEmitter.onNext(arrayList);
                }
                observableEmitter.onComplete();
            }
        }).map(new Function<List<MovieEntity>, Boolean>() {
            /* renamed from: a */
            public Boolean apply(List<MovieEntity> list) throws Exception {
                List<MovieEntity> a2 = mvDatabase.m().a((Boolean) false);
                if (str.equals("Merge")) {
                    ArrayList arrayList = new ArrayList();
                    for (MovieEntity next : a2) {
                        boolean z = false;
                        for (MovieEntity a3 : list) {
                            if (TraktUserApi.this.a(next, a3)) {
                                z = true;
                            }
                        }
                        if (!z) {
                            arrayList.add(next);
                        }
                    }
                    TraktUserApi.this.a((List<MovieEntity>) arrayList, true);
                } else {
                    for (MovieEntity watched_at : a2) {
                        watched_at.setWatched_at((OffsetDateTime) null);
                    }
                    mvDatabase.m().a((MovieEntity[]) a2.toArray(new MovieEntity[a2.size()]));
                }
                mvDatabase.m().a((MovieEntity[]) list.toArray(new MovieEntity[list.size()]));
                return true;
            }
        }).subscribeOn(Schedulers.b());
    }

    public Observable<Boolean> d(final MvDatabase mvDatabase, final String str) {
        return Observable.create(new ObservableOnSubscribe<List<BaseShow>>(this) {
            public void subscribe(ObservableEmitter<List<BaseShow>> observableEmitter) throws Exception {
                List<BaseShow> d = TraktUserApi.f().d();
                if (d != null) {
                    observableEmitter.onNext(d);
                }
                observableEmitter.onComplete();
            }
        }).map(new Function<List<BaseShow>, Boolean>() {
            /* renamed from: a */
            public Boolean apply(List<BaseShow> list) throws Exception {
                ArrayList arrayList = new ArrayList();
                ArrayList<TvWatchedEpisode> arrayList2 = new ArrayList<>();
                for (BaseShow next : list) {
                    arrayList.add(TraktUserApi.a(next));
                    for (BaseSeason next2 : next.seasons) {
                        for (BaseEpisode baseEpisode : next2.episodes) {
                            TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                            tvWatchedEpisode.a(baseEpisode.number.intValue());
                            tvWatchedEpisode.c(next2.number.intValue());
                            Integer num = next.show.ids.tmdb;
                            long j = 0;
                            tvWatchedEpisode.c(num != null ? (long) num.intValue() : 0);
                            tvWatchedEpisode.a(next.show.ids.imdb);
                            Integer num2 = next.show.ids.tvdb;
                            tvWatchedEpisode.e(num2 != null ? (long) num2.intValue() : 0);
                            Integer num3 = next.show.ids.trakt;
                            if (num3 != null) {
                                j = (long) num3.intValue();
                            }
                            tvWatchedEpisode.d(j);
                            arrayList2.add(tvWatchedEpisode);
                        }
                    }
                }
                List<TvWatchedEpisode> a2 = mvDatabase.o().a();
                if (str.contains("Merge")) {
                    ArrayList arrayList3 = new ArrayList();
                    for (TvWatchedEpisode next3 : a2) {
                        boolean z = false;
                        for (TvWatchedEpisode a3 : arrayList2) {
                            if (TraktUserApi.this.a(next3, a3)) {
                                z = true;
                            }
                        }
                        if (!z) {
                            arrayList3.add(next3);
                        }
                    }
                    TraktUserApi.this.a((List<TvWatchedEpisode>) arrayList3, true, mvDatabase);
                }
                mvDatabase.m().a((MovieEntity[]) arrayList.toArray(new MovieEntity[arrayList.size()]));
                mvDatabase.o().b((TvWatchedEpisode[]) arrayList2.toArray(new TvWatchedEpisode[arrayList2.size()]));
                return true;
            }
        }).subscribeOn(Schedulers.b());
    }

    public void b(MovieEntity movieEntity) throws IOException {
        if (movieEntity.getTV().booleanValue()) {
            SyncShow syncShow = new SyncShow();
            syncShow.ids = ShowIds.tmdb((int) movieEntity.getTmdbID());
            TraktHelper.a().sync().deleteItemsFromCollection(new SyncItems().shows(syncShow)).execute();
            return;
        }
        SyncMovie syncMovie = new SyncMovie();
        syncMovie.ids = MovieIds.tmdb((int) movieEntity.getTmdbID());
        TraktHelper.a().sync().deleteItemsFromCollection(new SyncItems().movies(syncMovie)).execute();
    }

    static /* synthetic */ List c(List list) throws Exception {
        ArrayList arrayList = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            arrayList.add(a((BaseShow) it2.next()));
        }
        return arrayList;
    }

    public void c(CompositeDisposable compositeDisposable, Activity activity, final MvDatabase mvDatabase) {
        com.utils.Utils.a(activity, (int) R.string.sync_trakt_begin);
        final String string = FreeMoviesApp.l().getString("pref_trakt_sync_mode", "Merge");
        compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Integer>(this) {
            public void subscribe(ObservableEmitter<Integer> observableEmitter) throws Exception {
                observableEmitter.onNext(Integer.valueOf(!string.equals("Merge") ? mvDatabase.m().e() : 0));
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).flatMap(new b(mvDatabase, string)).flatMap(new k(mvDatabase, string)).observeOn(AndroidSchedulers.a()).subscribe(new o(activity), new j(activity)));
    }

    public Observable<Boolean> b(MvDatabase mvDatabase, String str) {
        return Observable.create(new ObservableOnSubscribe<List<BaseShow>>(this) {
            public void subscribe(ObservableEmitter<List<BaseShow>> observableEmitter) throws Exception {
                List<BaseShow> b = TraktUserApi.f().b();
                if (b != null) {
                    observableEmitter.onNext(b);
                }
                observableEmitter.onComplete();
            }
        }).map(g.f5853a).map(new p(this, mvDatabase, str)).subscribeOn(Schedulers.b());
    }

    public void a(List<MovieEntity> list, boolean z) throws IOException {
        Call<SyncResponse> call;
        ArrayList arrayList = new ArrayList();
        SyncItems syncItems = new SyncItems();
        for (MovieEntity next : list) {
            MovieIds movieIds = new MovieIds();
            movieIds.imdb = next.getImdbIDStr();
            movieIds.tmdb = Integer.valueOf((int) next.getTmdbID());
            SyncMovie id = new SyncMovie().id(movieIds);
            id.watched_at = next.getCreatedDate();
            arrayList.add(id);
        }
        syncItems.movies((List<SyncMovie>) arrayList);
        Sync sync = TraktHelper.a().sync();
        if (z) {
            call = sync.addItemsToWatchedHistory(syncItems);
        } else {
            call = sync.deleteItemsFromWatchedHistory(syncItems);
        }
        call.execute();
    }

    public /* synthetic */ Boolean b(MvDatabase mvDatabase, String str, List list) throws Exception {
        boolean z;
        List<MovieEntity> a2 = mvDatabase.m().a(true);
        ArrayList arrayList = new ArrayList();
        if (str.equals("Merge")) {
            for (MovieEntity next : a2) {
                Iterator it2 = list.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (a(next, (MovieEntity) it2.next())) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    arrayList.add(next);
                }
                b((List<MovieEntity>) arrayList, mvDatabase);
            }
        }
        Iterator it3 = list.iterator();
        while (it3.hasNext()) {
            MovieEntity movieEntity = (MovieEntity) it3.next();
            try {
                mvDatabase.m().a(movieEntity);
            } catch (Exception e) {
                System.out.println(e.fillInStackTrace());
            }
        }
        return true;
    }

    static /* synthetic */ void c(Activity activity, Boolean bool) throws Exception {
        com.utils.Utils.a(activity, (int) R.string.sucess_sync_trakt);
        RxBus.b().a(new TraktSyncSuccess());
    }

    static /* synthetic */ void c(Activity activity, Throwable th) throws Exception {
        com.utils.Utils.a(activity, (int) R.string.failed_sync_trakt);
        RxBus.b().a(new TrackSyncFaild());
    }

    public void a(MovieEntity movieEntity, int i, int i2, boolean z) throws IOException {
        Call<SyncResponse> call;
        ShowIds showIds = new ShowIds();
        showIds.imdb = movieEntity.getImdbIDStr();
        if (movieEntity.getTmdbID() > 0) {
            showIds.tmdb = Integer.valueOf((int) movieEntity.getTmdbID());
        }
        if (movieEntity.getTvdbID() > 0) {
            showIds.tvdb = Integer.valueOf((int) movieEntity.getTvdbID());
        }
        SyncShow id = new SyncShow().id(showIds);
        id.watched_at = movieEntity.getCreatedDate();
        id.seasons(new SyncSeason().number(i).episodes(new SyncEpisode().number(i2)));
        SyncItems syncItems = new SyncItems();
        syncItems.shows(id);
        Sync sync = TraktHelper.a().sync();
        if (z) {
            call = sync.addItemsToWatchedHistory(syncItems);
        } else {
            call = sync.deleteItemsFromWatchedHistory(syncItems);
        }
        call.execute();
    }

    public void b(List<MovieEntity> list, MvDatabase mvDatabase) {
        ArrayList arrayList = new ArrayList();
        SyncItems syncItems = new SyncItems();
        for (MovieEntity next : list) {
            ShowIds showIds = new ShowIds();
            if (next.getImdbIDStr() != null) {
                showIds.imdb = next.getImdbIDStr();
            }
            if (next.getTmdbID() > 0) {
                showIds.tmdb = Integer.valueOf((int) next.getTmdbID());
            }
            if (next.getTvdbID() > 0) {
                showIds.tvdb = Integer.valueOf((int) next.getTvdbID());
            }
            new SyncShow().id(showIds).collected_at = next.getCreatedDate();
            arrayList.addAll(a(mvDatabase.o().a(next.getTmdbID(), next.getImdbIDStr(), next.getTraktID(), next.getTvdbID()), mvDatabase));
        }
        syncItems.shows((List<SyncShow>) arrayList);
        TraktHelper.a().sync().addItemsToCollection(syncItems);
    }

    public void a(MovieEntity movieEntity, SeasonEntity seasonEntity, boolean z) throws IOException {
        Call<SyncResponse> call;
        ShowIds showIds = new ShowIds();
        showIds.imdb = movieEntity.getImdbIDStr();
        showIds.tmdb = Integer.valueOf((int) movieEntity.getTmdbID());
        SyncShow id = new SyncShow().id(showIds);
        SyncSeason number = new SyncSeason().number(seasonEntity.g());
        number.episodes = new ArrayList();
        for (int i = 1; i <= seasonEntity.b(); i++) {
            SyncEpisode syncEpisode = new SyncEpisode();
            syncEpisode.season = Integer.valueOf(seasonEntity.g());
            syncEpisode.number = Integer.valueOf(i);
            number.episodes.add(syncEpisode);
        }
        id.seasons(number);
        SyncItems syncItems = new SyncItems();
        syncItems.shows(id);
        Sync sync = TraktHelper.a().sync();
        if (z) {
            call = sync.addItemsToWatchedHistory(syncItems);
        } else {
            call = sync.deleteItemsFromWatchedHistory(syncItems);
        }
        call.execute();
    }

    static /* synthetic */ List b(List list) throws Exception {
        ArrayList arrayList = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            arrayList.add(a((BaseMovie) it2.next()));
        }
        return arrayList;
    }

    public void b(CompositeDisposable compositeDisposable, Activity activity, final MvDatabase mvDatabase) {
        com.utils.Utils.a(activity, (int) R.string.sync_trakt_begin);
        final String string = FreeMoviesApp.l().getString("pref_trakt_sync_mode", "Merge");
        compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Integer>(this) {
            public void subscribe(ObservableEmitter<Integer> observableEmitter) throws Exception {
                observableEmitter.onNext(Integer.valueOf(!string.equals("Merge") ? mvDatabase.m().b() : 0));
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).flatMap(new r(mvDatabase, string)).flatMap(new d(mvDatabase, string)).observeOn(AndroidSchedulers.a()).subscribe(new n(activity), new e(activity)));
    }

    static /* synthetic */ void b(Activity activity, Boolean bool) throws Exception {
        com.utils.Utils.a(activity, (int) R.string.sucess_sync_trakt);
        RxBus.b().a(new TraktSyncSuccess());
    }

    public List<BaseMovie> a() throws IOException {
        return TraktHelper.a().sync().collectionMovies(Extended.FULL).execute().body();
    }

    public List<CalendarShowEntry> a(String str) throws IOException {
        return TraktHelper.a().calendars().shows(str, 1).execute().body();
    }

    static /* synthetic */ void b(Activity activity, Throwable th) throws Exception {
        com.utils.Utils.a(activity, (int) R.string.failed_sync_trakt);
        RxBus.b().a(new TrackSyncFaild());
    }

    public void a(MovieEntity movieEntity) throws IOException {
        if (movieEntity.getTV().booleanValue()) {
            SyncShow syncShow = new SyncShow();
            syncShow.ids = ShowIds.tmdb((int) movieEntity.getTmdbID());
            syncShow.collected_at = movieEntity.getCreatedDate();
            TraktHelper.a().sync().addItemsToCollection(new SyncItems().shows(syncShow)).execute();
            return;
        }
        SyncMovie syncMovie = new SyncMovie();
        syncMovie.collected_at = movieEntity.getCreatedDate();
        syncMovie.ids = MovieIds.tmdb((int) movieEntity.getTmdbID());
        TraktHelper.a().sync().addItemsToCollection(new SyncItems().movies(syncMovie)).execute();
    }

    public static MovieEntity a(BaseShow baseShow) {
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setName(baseShow.show.title);
        OffsetDateTime offsetDateTime = baseShow.show.first_aired;
        movieEntity.setRealeaseDate(offsetDateTime != null ? offsetDateTime.toString() : "1970-1-1");
        movieEntity.setGenres(baseShow.show.genres);
        movieEntity.setTV(true);
        Integer num = baseShow.show.ids.tmdb;
        long j = 0;
        movieEntity.setTmdbID(num != null ? (long) num.intValue() : 0);
        movieEntity.setImdbIDStr(baseShow.show.ids.imdb);
        Integer num2 = baseShow.show.ids.trakt;
        movieEntity.setTraktID(num2 != null ? (long) num2.intValue() : 0);
        Integer num3 = baseShow.show.ids.tvdb;
        if (num3 != null) {
            j = (long) num3.intValue();
        }
        movieEntity.setTvdbID(j);
        movieEntity.setVote(baseShow.show.rating);
        movieEntity.setOverview(baseShow.show.overview);
        OffsetDateTime offsetDateTime2 = baseShow.last_watched_at;
        if (offsetDateTime2 != null) {
            movieEntity.setWatched_at(offsetDateTime2);
        }
        OffsetDateTime offsetDateTime3 = baseShow.last_collected_at;
        if (offsetDateTime3 != null) {
            movieEntity.setCollected_at(offsetDateTime3);
        }
        return movieEntity;
    }

    public static MovieEntity a(BaseMovie baseMovie) {
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setName(baseMovie.movie.title);
        LocalDate localDate = baseMovie.movie.released;
        movieEntity.setRealeaseDate(localDate != null ? localDate.toString() : "1970-1-1");
        movieEntity.setGenres(baseMovie.movie.genres);
        movieEntity.setTV(false);
        Integer num = baseMovie.movie.ids.tmdb;
        long j = 0;
        movieEntity.setTmdbID(num != null ? (long) num.intValue() : 0);
        movieEntity.setImdbIDStr(baseMovie.movie.ids.imdb);
        Integer num2 = baseMovie.movie.ids.trakt;
        if (num2 != null) {
            j = (long) num2.intValue();
        }
        movieEntity.setTraktID(j);
        movieEntity.setVote(baseMovie.movie.rating);
        movieEntity.setOverview(baseMovie.movie.overview);
        OffsetDateTime offsetDateTime = baseMovie.last_watched_at;
        if (offsetDateTime != null) {
            movieEntity.setWatched_at(offsetDateTime);
        }
        OffsetDateTime offsetDateTime2 = baseMovie.collected_at;
        if (offsetDateTime2 != null) {
            movieEntity.setCollected_at(offsetDateTime2);
        }
        return movieEntity;
    }

    /* access modifiers changed from: package-private */
    public boolean a(MovieEntity movieEntity, MovieEntity movieEntity2) {
        if (movieEntity == null || movieEntity2 == null) {
            return false;
        }
        boolean z = movieEntity.getTmdbID() > 0 && movieEntity2.getTmdbID() > 0 && movieEntity.getTmdbID() == movieEntity2.getTmdbID();
        boolean equals = (movieEntity.getImdbIDStr() == null || movieEntity2.getImdbIDStr() == null) ? false : movieEntity.getImdbIDStr().equals(movieEntity2.getImdbIDStr());
        boolean z2 = movieEntity.getTvdbID() > 0 && movieEntity2.getTvdbID() > 0 && movieEntity.getTvdbID() == movieEntity2.getTvdbID();
        if ((movieEntity.getTraktID() > 0 && movieEntity2.getTraktID() > 0 && movieEntity.getTraktID() == movieEntity2.getTraktID()) || equals || z2 || z) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(TvWatchedEpisode tvWatchedEpisode, TvWatchedEpisode tvWatchedEpisode2) {
        if (tvWatchedEpisode == null || tvWatchedEpisode2 == null) {
            return false;
        }
        boolean z = tvWatchedEpisode.g() > 0 && tvWatchedEpisode2.g() > 0 && tvWatchedEpisode.g() == tvWatchedEpisode2.g();
        boolean equals = (tvWatchedEpisode.c() == null || tvWatchedEpisode2.c() == null) ? false : tvWatchedEpisode.c().equals(tvWatchedEpisode2.c());
        boolean z2 = tvWatchedEpisode.i() > 0 && tvWatchedEpisode2.i() > 0 && tvWatchedEpisode.i() == tvWatchedEpisode2.i();
        if ((tvWatchedEpisode.h() > 0 && tvWatchedEpisode2.h() > 0 && tvWatchedEpisode.h() == tvWatchedEpisode2.h()) || equals || z2 || z) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0053 A[EDGE_INSN: B:82:0x0053->B:13:0x0053 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.uwetrottmann.trakt5.entities.SyncShow> a(java.util.List<com.database.entitys.TvWatchedEpisode> r14, com.database.MvDatabase r15) {
        /*
            r13 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r1 = r14.iterator()
        L_0x0009:
            boolean r2 = r1.hasNext()
            r3 = 0
            if (r2 == 0) goto L_0x00c4
            java.lang.Object r2 = r1.next()
            com.database.entitys.TvWatchedEpisode r2 = (com.database.entitys.TvWatchedEpisode) r2
            java.util.Iterator r4 = r0.iterator()
        L_0x001a:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x0053
            java.lang.Object r5 = r4.next()
            com.uwetrottmann.trakt5.entities.SyncShow r5 = (com.uwetrottmann.trakt5.entities.SyncShow) r5
            com.uwetrottmann.trakt5.entities.ShowIds r6 = r5.ids
            java.lang.String r6 = r6.imdb
            java.lang.String r7 = r2.c()
            if (r6 == r7) goto L_0x0052
            com.uwetrottmann.trakt5.entities.ShowIds r6 = r5.ids
            java.lang.Integer r6 = r6.tmdb
            int r6 = r6.intValue()
            long r6 = (long) r6
            long r8 = r2.g()
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0052
            com.uwetrottmann.trakt5.entities.ShowIds r6 = r5.ids
            java.lang.Integer r6 = r6.tvdb
            int r6 = r6.intValue()
            long r6 = (long) r6
            long r8 = r2.i()
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 != 0) goto L_0x001a
        L_0x0052:
            r3 = r5
        L_0x0053:
            if (r3 != 0) goto L_0x0009
            com.uwetrottmann.trakt5.entities.ShowIds r3 = new com.uwetrottmann.trakt5.entities.ShowIds
            r3.<init>()
            java.lang.String r4 = r2.c()
            r3.imdb = r4
            long r4 = r2.g()
            r6 = 0
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 <= 0) goto L_0x0075
            long r4 = r2.g()
            int r5 = (int) r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r5)
            r3.tmdb = r4
        L_0x0075:
            long r4 = r2.i()
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 <= 0) goto L_0x0088
            long r4 = r2.i()
            int r2 = (int) r4
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r3.tvdb = r2
        L_0x0088:
            com.uwetrottmann.trakt5.entities.SyncShow r2 = new com.uwetrottmann.trakt5.entities.SyncShow
            r2.<init>()
            com.uwetrottmann.trakt5.entities.SyncShow r2 = r2.id(r3)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r2.seasons = r4
            com.database.daos.MovieDAO r5 = r15.m()     // Catch:{ Exception -> 0x00bf }
            java.lang.Integer r4 = r3.tmdb     // Catch:{ Exception -> 0x00bf }
            int r4 = r4.intValue()     // Catch:{ Exception -> 0x00bf }
            long r6 = (long) r4     // Catch:{ Exception -> 0x00bf }
            java.lang.String r8 = r3.imdb     // Catch:{ Exception -> 0x00bf }
            java.lang.Integer r4 = r3.trakt     // Catch:{ Exception -> 0x00bf }
            int r4 = r4.intValue()     // Catch:{ Exception -> 0x00bf }
            long r9 = (long) r4     // Catch:{ Exception -> 0x00bf }
            java.lang.Integer r3 = r3.tvdb     // Catch:{ Exception -> 0x00bf }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x00bf }
            long r11 = (long) r3     // Catch:{ Exception -> 0x00bf }
            com.database.entitys.MovieEntity r3 = r5.a(r6, r8, r9, r11)     // Catch:{ Exception -> 0x00bf }
            if (r3 == 0) goto L_0x00bf
            org.threeten.bp.OffsetDateTime r3 = r3.getCreatedDate()     // Catch:{ Exception -> 0x00bf }
            r2.collected_at = r3     // Catch:{ Exception -> 0x00bf }
        L_0x00bf:
            r0.add(r2)
            goto L_0x0009
        L_0x00c4:
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            java.util.Iterator r15 = r14.iterator()
        L_0x00cd:
            boolean r1 = r15.hasNext()
            if (r1 == 0) goto L_0x0154
            java.lang.Object r1 = r15.next()
            com.database.entitys.TvWatchedEpisode r1 = (com.database.entitys.TvWatchedEpisode) r1
            java.util.Iterator r2 = r0.iterator()
        L_0x00dd:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x0116
            java.lang.Object r4 = r2.next()
            com.uwetrottmann.trakt5.entities.SyncShow r4 = (com.uwetrottmann.trakt5.entities.SyncShow) r4
            com.uwetrottmann.trakt5.entities.ShowIds r5 = r4.ids
            java.lang.String r5 = r5.imdb
            java.lang.String r6 = r1.c()
            if (r5 == r6) goto L_0x0117
            com.uwetrottmann.trakt5.entities.ShowIds r5 = r4.ids
            java.lang.Integer r5 = r5.tmdb
            int r5 = r5.intValue()
            long r5 = (long) r5
            long r7 = r1.g()
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x0117
            com.uwetrottmann.trakt5.entities.ShowIds r5 = r4.ids
            java.lang.Integer r5 = r5.tvdb
            int r5 = r5.intValue()
            long r5 = (long) r5
            long r7 = r1.i()
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 != 0) goto L_0x00dd
            goto L_0x0117
        L_0x0116:
            r4 = r3
        L_0x0117:
            java.util.List<com.uwetrottmann.trakt5.entities.SyncSeason> r2 = r4.seasons
            java.util.Iterator r2 = r2.iterator()
        L_0x011d:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0136
            java.lang.Object r5 = r2.next()
            com.uwetrottmann.trakt5.entities.SyncSeason r5 = (com.uwetrottmann.trakt5.entities.SyncSeason) r5
            java.lang.Integer r6 = r5.number
            int r6 = r6.intValue()
            int r7 = r1.e()
            if (r6 != r7) goto L_0x011d
            goto L_0x0137
        L_0x0136:
            r5 = r3
        L_0x0137:
            if (r5 != 0) goto L_0x00cd
            com.uwetrottmann.trakt5.entities.SyncSeason r2 = new com.uwetrottmann.trakt5.entities.SyncSeason
            r2.<init>()
            int r1 = r1.e()
            com.uwetrottmann.trakt5.entities.SyncSeason r1 = r2.number(r1)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r1.episodes = r2
            java.util.List<com.uwetrottmann.trakt5.entities.SyncSeason> r2 = r4.seasons
            r2.add(r1)
            goto L_0x00cd
        L_0x0154:
            java.util.Iterator r14 = r14.iterator()
        L_0x0158:
            boolean r15 = r14.hasNext()
            if (r15 == 0) goto L_0x01f8
            java.lang.Object r15 = r14.next()
            com.database.entitys.TvWatchedEpisode r15 = (com.database.entitys.TvWatchedEpisode) r15
            java.util.Iterator r1 = r0.iterator()
        L_0x0168:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x01a1
            java.lang.Object r2 = r1.next()
            com.uwetrottmann.trakt5.entities.SyncShow r2 = (com.uwetrottmann.trakt5.entities.SyncShow) r2
            com.uwetrottmann.trakt5.entities.ShowIds r4 = r2.ids
            java.lang.String r4 = r4.imdb
            java.lang.String r5 = r15.c()
            if (r4 == r5) goto L_0x01a2
            com.uwetrottmann.trakt5.entities.ShowIds r4 = r2.ids
            java.lang.Integer r4 = r4.tmdb
            int r4 = r4.intValue()
            long r4 = (long) r4
            long r6 = r15.g()
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01a2
            com.uwetrottmann.trakt5.entities.ShowIds r4 = r2.ids
            java.lang.Integer r4 = r4.tvdb
            int r4 = r4.intValue()
            long r4 = (long) r4
            long r6 = r15.i()
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 != 0) goto L_0x0168
            goto L_0x01a2
        L_0x01a1:
            r2 = r3
        L_0x01a2:
            java.util.List<com.uwetrottmann.trakt5.entities.SyncSeason> r1 = r2.seasons
            java.util.Iterator r1 = r1.iterator()
        L_0x01a8:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x01c1
            java.lang.Object r2 = r1.next()
            com.uwetrottmann.trakt5.entities.SyncSeason r2 = (com.uwetrottmann.trakt5.entities.SyncSeason) r2
            java.lang.Integer r4 = r2.number
            int r4 = r4.intValue()
            int r5 = r15.e()
            if (r4 != r5) goto L_0x01a8
            goto L_0x01c2
        L_0x01c1:
            r2 = r3
        L_0x01c2:
            java.util.List<com.uwetrottmann.trakt5.entities.SyncEpisode> r1 = r2.episodes
            java.util.Iterator r1 = r1.iterator()
        L_0x01c8:
            boolean r4 = r1.hasNext()
            if (r4 == 0) goto L_0x01e1
            java.lang.Object r4 = r1.next()
            com.uwetrottmann.trakt5.entities.SyncEpisode r4 = (com.uwetrottmann.trakt5.entities.SyncEpisode) r4
            java.lang.Integer r5 = r4.number
            int r5 = r5.intValue()
            int r6 = r15.b()
            if (r5 != r6) goto L_0x01c8
            goto L_0x01e2
        L_0x01e1:
            r4 = r3
        L_0x01e2:
            if (r4 != 0) goto L_0x0158
            java.util.List<com.uwetrottmann.trakt5.entities.SyncEpisode> r1 = r2.episodes
            com.uwetrottmann.trakt5.entities.SyncEpisode r2 = new com.uwetrottmann.trakt5.entities.SyncEpisode
            r2.<init>()
            int r15 = r15.b()
            com.uwetrottmann.trakt5.entities.SyncEpisode r15 = r2.number(r15)
            r1.add(r15)
            goto L_0x0158
        L_0x01f8:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.api.TraktUserApi.a(java.util.List, com.database.MvDatabase):java.util.List");
    }

    public void a(List<TvWatchedEpisode> list, boolean z, MvDatabase mvDatabase) throws IOException {
        SyncItems syncItems = new SyncItems();
        syncItems.shows(a(list, mvDatabase));
        Sync sync = TraktHelper.a().sync();
        if (z) {
            sync.addItemsToWatchedHistory(syncItems).execute();
        } else {
            sync.deleteItemsFromWatchedHistory(syncItems).execute();
        }
    }

    public Observable<Boolean> a(MvDatabase mvDatabase, String str) {
        return Observable.create(new ObservableOnSubscribe<List<BaseMovie>>(this) {
            public void subscribe(ObservableEmitter<List<BaseMovie>> observableEmitter) throws Exception {
                List<BaseMovie> a2 = TraktUserApi.f().a();
                if (a2 != null) {
                    observableEmitter.onNext(a2);
                }
                observableEmitter.onComplete();
            }
        }).map(a.f5847a).subscribeOn(Schedulers.b()).map(new h(this, mvDatabase, str));
    }

    public /* synthetic */ Boolean a(MvDatabase mvDatabase, String str, List list) throws Exception {
        boolean z;
        List<MovieEntity> a2 = mvDatabase.m().a(false);
        ArrayList arrayList = new ArrayList();
        if (str.equals("Merge")) {
            for (MovieEntity next : a2) {
                Iterator it2 = list.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (a(next, (MovieEntity) it2.next())) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    arrayList.add(next);
                }
                a((List<MovieEntity>) arrayList);
            }
        }
        mvDatabase.m().a((MovieEntity[]) list.toArray(new MovieEntity[list.size()]));
        return true;
    }

    public void a(List<MovieEntity> list) {
        ArrayList arrayList = new ArrayList();
        SyncItems syncItems = new SyncItems();
        for (MovieEntity next : list) {
            MovieIds movieIds = new MovieIds();
            if (next.getImdbIDStr() != null) {
                movieIds.imdb = next.getImdbIDStr();
            }
            if (next.getTmdbID() > 0) {
                movieIds.tmdb = Integer.valueOf((int) next.getTmdbID());
            }
            SyncMovie id = new SyncMovie().id(movieIds);
            id.collected_at = next.getCreatedDate();
            arrayList.add(id);
        }
        syncItems.movies((List<SyncMovie>) arrayList);
        TraktHelper.a().sync().addItemsToCollection(syncItems);
    }

    public void a(CompositeDisposable compositeDisposable, Activity activity, final MvDatabase mvDatabase) {
        com.utils.Utils.a(activity, (int) R.string.sync_trakt_begin);
        final String string = FreeMoviesApp.l().getString("pref_trakt_sync_mode", "Merge");
        compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Integer>(this) {
            public void subscribe(ObservableEmitter<Integer> observableEmitter) throws Exception {
                observableEmitter.onNext(Integer.valueOf(!string.equals("Merge") ? mvDatabase.m().c() : 0));
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).flatMap(new l(mvDatabase, string)).flatMap(new f(mvDatabase, string)).flatMap(new c(mvDatabase, string)).flatMap(new m(mvDatabase, string)).observeOn(AndroidSchedulers.a()).subscribe(new i(activity), new q(activity)));
    }

    static /* synthetic */ void a(Activity activity, Boolean bool) throws Exception {
        com.utils.Utils.a(activity, (int) R.string.sucess_sync_trakt);
        RxBus.b().a(new TraktSyncSuccess());
    }

    static /* synthetic */ void a(Activity activity, Throwable th) throws Exception {
        com.utils.Utils.a(activity, (int) R.string.failed_sync_trakt);
        RxBus.b().a(new TrackSyncFaild());
    }
}
