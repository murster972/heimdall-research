package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class b implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MvDatabase f5848a;
    private final /* synthetic */ String b;

    public /* synthetic */ b(MvDatabase mvDatabase, String str) {
        this.f5848a = mvDatabase;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return TraktUserApi.f().c(this.f5848a, this.b);
    }
}
