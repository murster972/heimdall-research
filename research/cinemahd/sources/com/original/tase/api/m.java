package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class m implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MvDatabase f5859a;
    private final /* synthetic */ String b;

    public /* synthetic */ m(MvDatabase mvDatabase, String str) {
        this.f5859a = mvDatabase;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return TraktUserApi.f().b(this.f5859a, this.b);
    }
}
