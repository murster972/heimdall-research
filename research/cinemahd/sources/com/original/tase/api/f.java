package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class f implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MvDatabase f5852a;
    private final /* synthetic */ String b;

    public /* synthetic */ f(MvDatabase mvDatabase, String str) {
        this.f5852a = mvDatabase;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return TraktUserApi.f().d(this.f5852a, this.b);
    }
}
