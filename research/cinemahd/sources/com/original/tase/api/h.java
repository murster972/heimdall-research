package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class h implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TraktUserApi f5854a;
    private final /* synthetic */ MvDatabase b;
    private final /* synthetic */ String c;

    public /* synthetic */ h(TraktUserApi traktUserApi, MvDatabase mvDatabase, String str) {
        this.f5854a = traktUserApi;
        this.b = mvDatabase;
        this.c = str;
    }

    public final Object apply(Object obj) {
        return this.f5854a.a(this.b, this.c, (List) obj);
    }
}
