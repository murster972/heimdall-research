package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class p implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TraktUserApi f5862a;
    private final /* synthetic */ MvDatabase b;
    private final /* synthetic */ String c;

    public /* synthetic */ p(TraktUserApi traktUserApi, MvDatabase mvDatabase, String str) {
        this.f5862a = traktUserApi;
        this.b = mvDatabase;
        this.c = str;
    }

    public final Object apply(Object obj) {
        return this.f5862a.b(this.b, this.c, (List) obj);
    }
}
