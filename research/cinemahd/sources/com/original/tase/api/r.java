package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class r implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MvDatabase f5864a;
    private final /* synthetic */ String b;

    public /* synthetic */ r(MvDatabase mvDatabase, String str) {
        this.f5864a = mvDatabase;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return TraktUserApi.f().a(this.f5864a, this.b);
    }
}
