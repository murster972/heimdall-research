package com.original.tase.api;

import com.database.MvDatabase;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class c implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MvDatabase f5849a;
    private final /* synthetic */ String b;

    public /* synthetic */ c(MvDatabase mvDatabase, String str) {
        this.f5849a = mvDatabase;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return TraktUserApi.f().a(this.f5849a, this.b);
    }
}
