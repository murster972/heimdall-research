package com.original.tase.socket;

import android.app.Activity;
import com.ads.videoreward.AdsManager;
import com.facebook.ads.AudienceNetworkActivity;
import com.google.gson.Gson;
import com.movie.FreeMoviesApp;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.model.socket.UserPlayerPluginInfo;
import com.original.tase.model.socket.UserResponces;
import com.utils.Utils;
import com.xuhao.didi.core.pojo.OriginalData;
import com.xuhao.didi.socket.client.sdk.OkSocket;
import com.xuhao.didi.socket.client.sdk.client.ConnectionInfo;
import com.xuhao.didi.socket.client.sdk.client.action.SocketActionAdapter;
import com.xuhao.didi.socket.client.sdk.client.connection.IConnectionManager;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client {
    private static volatile Client mClient;
    public List<IConnectionManager> iConnectionManagerList = new ArrayList();
    boolean isresponce = false;
    private CompositeDisposable mSubscriptions = new CompositeDisposable();
    public IConnectionManager manager = null;
    /* access modifiers changed from: private */
    public String mdata = "";
    private int port = 19811;

    private Client() {
    }

    public static Client getIntance() {
        if (mClient == null) {
            synchronized (Client.class) {
                if (mClient == null) {
                    mClient = new Client();
                }
            }
        }
        return mClient;
    }

    public void autoconnect(final ObservableEmitter<String> observableEmitter) {
        String o = Utils.o();
        int lastIndexOf = o.lastIndexOf(".");
        String str = o.substring(0, lastIndexOf) + ".%s";
        o.substring(lastIndexOf + 1, o.length());
        this.isresponce = false;
        for (int i = 0; i <= 255; i++) {
            IConnectionManager open = OkSocket.open(new ConnectionInfo(String.format(str, new Object[]{Integer.valueOf(i)}), this.port));
            this.iConnectionManagerList.add(open);
            open.registerReceiver(new SocketActionAdapter() {
                public void onSocketConnectionFailed(ConnectionInfo connectionInfo, String str, Exception exc) {
                }

                public void onSocketConnectionSuccess(ConnectionInfo connectionInfo, String str) {
                    Logger.a("Client", connectionInfo.getIp().toString());
                    observableEmitter.onNext(connectionInfo.getIp());
                    Client.this.isresponce = true;
                }

                public void onSocketDisconnection(ConnectionInfo connectionInfo, String str, Exception exc) {
                    observableEmitter.onComplete();
                }

                public void onSocketReadResponse(ConnectionInfo connectionInfo, String str, OriginalData originalData) {
                }
            });
            open.connect();
        }
        while (!this.isresponce) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void connect(final ObservableEmitter<? super Object> observableEmitter, final String str) {
        IConnectionManager iConnectionManager = this.manager;
        if (iConnectionManager == null || !iConnectionManager.isConnect()) {
            this.manager = OkSocket.open(new ConnectionInfo(str, this.port));
            this.manager.registerReceiver(new SocketActionAdapter() {
                public void onSocketConnectionFailed(ConnectionInfo connectionInfo, String str, Exception exc) {
                    observableEmitter.onNext(new UserPlayerPluginInfo(connectionInfo.getIp(), str, false, ""));
                }

                public void onSocketConnectionSuccess(ConnectionInfo connectionInfo, String str) {
                    Logger.a("Client", connectionInfo.getIp().toString());
                    observableEmitter.onNext(new UserPlayerPluginInfo(connectionInfo.getIp(), str, true, ""));
                }

                public void onSocketDisconnection(ConnectionInfo connectionInfo, String str, Exception exc) {
                    observableEmitter.onComplete();
                }

                public void onSocketReadResponse(ConnectionInfo connectionInfo, String str, OriginalData originalData) {
                    byte[] bodyBytes = originalData.getBodyBytes();
                    String str2 = new String(Arrays.copyOfRange(bodyBytes, 0, bodyBytes.length), Charset.forName(AudienceNetworkActivity.WEBVIEW_ENCODING));
                    try {
                        UserResponces userResponces = (UserResponces) new Gson().a(str2, UserResponces.class);
                        if (userResponces != null) {
                            observableEmitter.onNext(userResponces);
                        }
                    } catch (Throwable unused) {
                    }
                    Logger.a("Client", str2);
                }
            });
            this.manager.connect();
            return;
        }
        observableEmitter.onNext(new UserPlayerPluginInfo(this.manager.getLocalConnectionInfo().getIp(), str, true, ""));
    }

    public Observable<Object> createObservable(final String str) {
        return Observable.create(new ObservableOnSubscribe<Object>() {
            public void setSubscriber(ObservableEmitter<? super Object> observableEmitter) {
                Client.this.connect(observableEmitter, str);
            }

            public void subscribe(ObservableEmitter<Object> observableEmitter) throws Exception {
                setSubscriber(observableEmitter);
            }
        });
    }

    public void disconnect() {
        IConnectionManager iConnectionManager = this.manager;
        if (iConnectionManager != null && iConnectionManager.isConnect()) {
            this.manager.disconnect();
        }
    }

    public void disconnectall() {
        String o = Utils.o();
        int lastIndexOf = o.lastIndexOf(".");
        StringBuilder sb = new StringBuilder();
        sb.append(o.substring(0, lastIndexOf));
        sb.append(".%s");
        sb.toString();
        o.substring(lastIndexOf + 1, o.length());
        this.isresponce = false;
        for (int i = 0; i < this.iConnectionManagerList.size(); i++) {
            if (this.iConnectionManagerList.get(i).isConnect()) {
                this.iConnectionManagerList.get(i).disconnect();
            }
        }
        this.iConnectionManagerList.clear();
    }

    public boolean iconnected() {
        return this.manager.isConnect();
    }

    public void senddata(String str, final Activity activity) {
        this.mdata = str;
        this.mSubscriptions.b(getIntance().createObservable(FreeMoviesApp.l().getString("ip_player_plugin", "")).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new Consumer<Object>() {
            public void accept(Object obj) throws Exception {
                if (obj instanceof UserPlayerPluginInfo) {
                    UserPlayerPluginInfo userPlayerPluginInfo = (UserPlayerPluginInfo) obj;
                    if (userPlayerPluginInfo.iConnect) {
                        FreeMoviesApp.l().edit().putBoolean("use_player_plugin", true).apply();
                        FreeMoviesApp.l().edit().putString("ip_player_plugin", userPlayerPluginInfo.serverIP).apply();
                        if (!Client.this.mdata.isEmpty()) {
                            Client client = Client.this;
                            client.manager.send(new Data(client.mdata));
                            String unused = Client.this.mdata = "";
                        }
                    }
                } else if (obj instanceof UserResponces) {
                    UserResponces userResponces = (UserResponces) obj;
                    int i = userResponces.code;
                    if (i == 200) {
                        Utils.a(activity, I18N.a(R.string.player_plugin_connected));
                        AdsManager.l().k();
                    } else if (i == 404) {
                        Utils.a(activity, I18N.a(R.string.player_plugin_try));
                    }
                    Logger.a("Client", "" + userResponces.code);
                }
            }
        }));
    }
}
