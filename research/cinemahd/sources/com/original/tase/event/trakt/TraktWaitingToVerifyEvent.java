package com.original.tase.event.trakt;

public class TraktWaitingToVerifyEvent {

    /* renamed from: a  reason: collision with root package name */
    private String f5868a;
    private String b;

    public TraktWaitingToVerifyEvent(String str, String str2) {
        this.b = str;
        this.f5868a = str2;
    }

    public String a() {
        return this.f5868a;
    }

    public String b() {
        return this.b;
    }
}
