package com.original.tase.event;

public class ApiDebridWaitingToVerifyEvent {

    /* renamed from: a  reason: collision with root package name */
    private String f5867a;
    private String b;

    public ApiDebridWaitingToVerifyEvent(String str, String str2) {
        this.b = str;
        this.f5867a = str2;
    }

    public String a() {
        return this.f5867a;
    }

    public String b() {
        return this.b;
    }
}
