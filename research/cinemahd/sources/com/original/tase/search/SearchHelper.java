package com.original.tase.search;

import com.facebook.common.util.UriUtil;
import com.google.gson.Gson;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.search.model.DuckduckResult;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import com.squareup.duktape.Duktape;
import com.utils.Getlink.Provider.BaseProvider;
import com.vungle.warren.model.CookieDBAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SearchHelper {
    public static String a(String str, String str2, String str3, String str4, String str5) {
        if (str4.contains(UriUtil.HTTP_SCHEME)) {
            str4 = BaseProvider.h(str4);
        }
        if (!str3.isEmpty()) {
            str3 = " " + str3;
        }
        if (!str5.isEmpty()) {
            str5 = str5 + " ";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put("referer", "https://www.bing.com" + "/");
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, "SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1");
        String format = String.format("https://www.bing.com" + "/search/?q=%s", new Object[]{Utils.a(String.format("site:%s %s%s%s", new Object[]{str4, str5, str, str3}), new boolean[0])});
        Iterator it2 = Jsoup.b(HttpHelper.e().a(format + "##forceNoCache##", (Map<String, String>[]) new Map[]{hashMap})).g("ol#b_results").b("a[h][href]").iterator();
        while (it2.hasNext()) {
            try {
                Element element = (Element) it2.next();
                String b = element.b("href");
                String lowerCase = TitleHelper.a(element.G().replaceAll("<[^>]*>", ""), "").toLowerCase();
                String lowerCase2 = TitleHelper.a(str5 + str + str2 + str3, "").toLowerCase();
                StringBuilder sb = new StringBuilder();
                sb.append(str5);
                sb.append(str);
                sb.append(str3);
                String lowerCase3 = TitleHelper.a(sb.toString(), "").toLowerCase();
                if ((lowerCase.startsWith(lowerCase2) && b.contains(str4)) || (lowerCase.startsWith(lowerCase3) && b.contains(str4))) {
                    return b;
                }
            } catch (Throwable unused) {
            }
        }
        return "";
    }

    public static List<String> b(String str, String str2, String str3, String str4, String str5) {
        ArrayList arrayList = new ArrayList();
        if (str4.contains(UriUtil.HTTP_SCHEME)) {
            str4 = BaseProvider.h(str4);
        }
        if (!str3.isEmpty()) {
            str3 = " " + str3;
        }
        if (!str5.isEmpty()) {
            str5 = str5 + " ";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put("referer", "https://www.bing.com" + "/");
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, "SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1");
        String format = String.format("https://www.bing.com" + "/search/?q=%s", new Object[]{Utils.a(String.format("site:%s %s%s%s", new Object[]{str4, str5, str, str3}), new boolean[0])});
        Iterator it2 = Jsoup.b(HttpHelper.e().a(format + "##forceNoCache##", (Map<String, String>[]) new Map[]{hashMap})).g("ol#b_results").b("a[h][href]").iterator();
        while (it2.hasNext()) {
            try {
                Element element = (Element) it2.next();
                String b = element.b("href");
                String lowerCase = TitleHelper.a(element.G().replaceAll("<[^>]*>", ""), "").toLowerCase();
                String lowerCase2 = TitleHelper.a(str5 + str + str2 + str3, "").toLowerCase();
                StringBuilder sb = new StringBuilder();
                sb.append(str5);
                sb.append(str);
                sb.append(str3);
                String lowerCase3 = TitleHelper.a(sb.toString(), "").toLowerCase();
                if ((lowerCase.startsWith(lowerCase2) && b.contains(str4)) || (lowerCase.startsWith(lowerCase3) && b.contains(str4))) {
                    arrayList.add(b);
                }
            } catch (Throwable unused) {
            }
        }
        return arrayList;
    }

    public static List<String> c(String str, String str2, String str3, String str4, String str5) {
        String str6;
        if (str4.contains(UriUtil.HTTP_SCHEME)) {
            str4 = BaseProvider.h(str4);
        }
        if (!str3.isEmpty()) {
            str6 = " " + str3;
        } else {
            str6 = "";
        }
        if (!str5.isEmpty()) {
            str5 = str5 + " ";
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put("referer", "https://duckduckgo.com/");
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, "SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1;");
        String a2 = Regex.a(HttpHelper.e().a("https://duckduckgo.com/?q=" + Utils.a(String.format("site:%s inurl:%s%s%s", new Object[]{str4, str5, str, str6}), new boolean[0]) + "##forceNoCache##", (Map<String, String>[]) new Map[]{hashMap}), "nrj\\(['\"](\\/d.+[^'\"])['\"]\\)", 1);
        if (a2.startsWith("/")) {
            a2 = "https://duckduckgo.com" + a2;
        }
        String a3 = Regex.a(HttpHelper.e().a(a2, (Map<String, String>[]) new Map[]{hashMap}), "DDG.pageLayout.load\\('d',(.+[\\]])\\);DDG", 1);
        if (!a3.isEmpty()) {
            try {
                DuckduckResult duckduckResult = (DuckduckResult) new Gson().a(String.format("{\"data\": %s}", new Object[]{a3}), DuckduckResult.class);
                if (duckduckResult != null) {
                    for (DuckduckResult.DataBean next : duckduckResult.a()) {
                        String lowerCase = TitleHelper.a(next.b().replaceAll("<[^>]*>", ""), "").toLowerCase();
                        String lowerCase2 = TitleHelper.a(str5 + str + str2 + str6, "").toLowerCase();
                        StringBuilder sb = new StringBuilder();
                        sb.append(str5);
                        sb.append(str);
                        sb.append(str6);
                        String lowerCase3 = TitleHelper.a(sb.toString(), "").toLowerCase();
                        if ((lowerCase.startsWith(lowerCase2) && next.a().contains(str4)) || (lowerCase.startsWith(lowerCase3) && next.a().contains(str4))) {
                            arrayList.add(next.c());
                        }
                    }
                }
            } catch (Throwable unused) {
                Logger.a("SearchHelper exeption ", str4);
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.addAll(b(str, str2, str3, str4, str5));
        } else if (arrayList.isEmpty()) {
            arrayList.addAll(e(str, str2, str3, str4, str5));
        } else if (arrayList.isEmpty()) {
            arrayList.addAll(d(str, str2, str3, str4, str5));
        }
        return arrayList;
    }

    public static List<String> d(String str, String str2, String str3, String str4, String str5) {
        String str6;
        ArrayList arrayList = new ArrayList();
        String l = com.utils.Utils.l();
        if (str4.contains(UriUtil.HTTP_SCHEME)) {
            str4 = BaseProvider.h(str4);
        }
        if (!str3.isEmpty()) {
            str3 = " " + str3;
        }
        if (!str5.isEmpty()) {
            str5 = str5 + " ";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put("referer", l + "/");
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, "SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1");
        String format = String.format(l + "/search?q=%s", new Object[]{Utils.a(String.format("site:%s %s%s%s", new Object[]{str4, str5, str, str3}), new boolean[0])});
        Iterator it2 = Jsoup.b(HttpHelper.e().a(format + "##forceNoCache##", (Map<String, String>[]) new Map[]{hashMap})).g("div[class=r]").iterator();
        while (it2.hasNext()) {
            try {
                Element h = ((Element) it2.next()).h("a");
                String b = h.b("href");
                Element h2 = h.h("h3");
                if (h != null) {
                    str6 = h2.G();
                } else {
                    str6 = h.h("span").G();
                }
                String lowerCase = TitleHelper.a(str6.replaceAll("<[^>]*>", ""), "").toLowerCase();
                String lowerCase2 = TitleHelper.a(str5 + str + str2 + str3, "").toLowerCase();
                StringBuilder sb = new StringBuilder();
                sb.append(str5);
                sb.append(str);
                sb.append(str3);
                String lowerCase3 = TitleHelper.a(sb.toString(), "").toLowerCase();
                if (((lowerCase.startsWith(lowerCase2) && b.contains(str4)) || (lowerCase.startsWith(lowerCase3) && b.contains(str4))) && !arrayList.contains(b)) {
                    arrayList.add(b);
                }
            } catch (Throwable unused) {
            }
        }
        return arrayList;
    }

    /* JADX INFO: finally extract failed */
    public static ArrayList e(String str, String str2, String str3, String str4, String str5) {
        String str6;
        String str7;
        String str8 = str;
        String str9 = str4;
        String h = str9.contains(UriUtil.HTTP_SCHEME) ? BaseProvider.h(str4) : str9;
        if (!str3.isEmpty()) {
            str6 = " " + str3;
        } else {
            str6 = str3;
        }
        if (!str5.isEmpty()) {
            str7 = str5 + " ";
        } else {
            str7 = str5;
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put("referer", "https://yandex.com" + "/");
        String a2 = Utils.a(String.format("site:%s inurl:%s%s%s", new Object[]{h, str7, str8, str6}), new boolean[0]);
        String a3 = HttpHelper.e().a("https://yandex.com" + "/", (Map<String, String>[]) new Map[]{hashMap});
        Regex.a(a3, "['\"]reqid['\"]\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
        String a4 = Regex.a(a3, "['\"]msid['\"]\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
        Duktape create = Duktape.create();
        try {
            Object evaluate = create.evaluate("function _generateReqID(n){return(n=n||_random(9)+_random(9))+Date.now().toString().slice(-7)+_random(7)}function _random(n){return Math.random().toString().slice(2,Math.min(n,10)+2)}_generateReqID();");
            create.close();
            String format = String.format("https://yandex.com" + "/search/?msid=%s&text=%s&suggest_reqid=%s", new Object[]{a4, a2, evaluate.toString()});
            Iterator it2 = Jsoup.b(HttpHelper.e().a(format + "##forceNoCache##", (Map<String, String>[]) new Map[]{hashMap})).g("li.serp-item").b("a[data-bem][href]").iterator();
            while (it2.hasNext()) {
                try {
                    Element element = (Element) it2.next();
                    String b = element.b("href");
                    String lowerCase = TitleHelper.a(element.h("div.organic__url-text").G().replaceAll("<[^>]*>", ""), "").toLowerCase();
                    StringBuilder sb = new StringBuilder();
                    sb.append(str7);
                    sb.append(str8);
                    try {
                        sb.append(str2);
                        sb.append(str6);
                        String lowerCase2 = TitleHelper.a(sb.toString(), "").toLowerCase();
                        String lowerCase3 = TitleHelper.a(str7 + str8 + str6, "").toLowerCase();
                        if (((lowerCase.startsWith(lowerCase2) && b.contains(h)) || (lowerCase.startsWith(lowerCase3) && b.contains(h))) && !arrayList.contains(b)) {
                            arrayList.add(b);
                        }
                    } catch (Throwable unused) {
                    }
                } catch (Throwable unused2) {
                    String str10 = str2;
                }
            }
            return arrayList;
        } catch (Throwable th) {
            create.close();
            throw th;
        }
    }
}
