package com.original.tase.search.model;

import java.util.List;

public class DuckduckResult {

    /* renamed from: a  reason: collision with root package name */
    private List<DataBean> f5893a;

    public static class DataBean {

        /* renamed from: a  reason: collision with root package name */
        private String f5894a;
        private String b;
        private String c;

        public String a() {
            return this.f5894a;
        }

        public String b() {
            return this.b;
        }

        public String c() {
            return this.c;
        }
    }

    public List<DataBean> a() {
        return this.f5893a;
    }
}
