package com.original.tase.utils;

import com.original.tase.Logger;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static ArrayList<String> a(String str, String str2, boolean z) {
        Matcher matcher;
        ArrayList<String> arrayList = new ArrayList<>();
        if (str != null && !str.isEmpty()) {
            if (z) {
                matcher = Pattern.compile(str2, 32).matcher(str);
            } else {
                matcher = Pattern.compile(str2).matcher(str);
            }
            while (matcher.find()) {
                arrayList.add(matcher.group());
            }
        }
        return arrayList;
    }

    public static ArrayList<ArrayList<String>> b(String str, String str2, int i) {
        int i2;
        Matcher matcher = str == null ? null : Pattern.compile(str2).matcher(str);
        if (matcher == null) {
            i2 = i;
        } else {
            i2 = Math.max(matcher.groupCount(), i);
        }
        ArrayList<ArrayList<String>> arrayList = new ArrayList<>();
        if (i2 == 0) {
            arrayList.add(0, new ArrayList());
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList.add(i3, new ArrayList());
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.add(new ArrayList());
        }
        if (matcher != null && !str.isEmpty()) {
            while (matcher.find()) {
                if (i == 0) {
                    try {
                        arrayList.get(0).add(matcher.group(0));
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                    }
                } else {
                    int i4 = 0;
                    while (i4 < i) {
                        i4++;
                        arrayList.get(i4).add(matcher.group(i4));
                    }
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.add(new ArrayList());
            }
        }
        return arrayList;
    }

    public static String a(String str, String str2, int i) {
        if (str != null && !str.isEmpty()) {
            Matcher matcher = Pattern.compile(str2).matcher(str);
            if (matcher.find()) {
                return matcher.group(i);
            }
        }
        return "";
    }

    public static String a(String str, String str2, int i, boolean z) {
        Matcher matcher;
        if (str != null && !str.isEmpty()) {
            if (z) {
                matcher = Pattern.compile(str2, 32).matcher(str);
            } else {
                matcher = Pattern.compile(str2).matcher(str);
            }
            if (matcher.find()) {
                return matcher.group(i);
            }
        }
        return "";
    }

    public static ArrayList<ArrayList<String>> b(String str, String str2, int i, int i2) {
        int i3;
        Matcher matcher = str == null ? null : Pattern.compile(str2, i2).matcher(str);
        if (matcher == null) {
            i3 = i;
        } else {
            i3 = Math.max(matcher.groupCount(), i);
        }
        ArrayList<ArrayList<String>> arrayList = new ArrayList<>();
        if (i3 == 0) {
            arrayList.add(0, new ArrayList());
        } else {
            for (int i4 = 0; i4 < i3; i4++) {
                arrayList.add(i4, new ArrayList());
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.add(new ArrayList());
        }
        if (matcher != null && !str.isEmpty()) {
            while (matcher.find()) {
                if (i == 0) {
                    try {
                        arrayList.get(0).add(matcher.group(0));
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                    }
                } else {
                    int i5 = 0;
                    while (i5 < i) {
                        i5++;
                        arrayList.get(i5).add(matcher.group(i5));
                    }
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.add(new ArrayList());
            }
        }
        return arrayList;
    }

    public static String a(String str, String str2, int i, int i2) {
        if (str != null && !str.isEmpty()) {
            Matcher matcher = Pattern.compile(str2, i2).matcher(str);
            if (matcher.find()) {
                return matcher.group(i);
            }
        }
        return "";
    }

    public static ArrayList<ArrayList<String>> b(String str, String str2, int i, boolean z) {
        int i2;
        Matcher matcher = str == null ? null : z ? Pattern.compile(str2, 32).matcher(str) : Pattern.compile(str2).matcher(str);
        if (matcher == null) {
            i2 = i;
        } else {
            i2 = Math.max(matcher.groupCount(), i);
        }
        ArrayList<ArrayList<String>> arrayList = new ArrayList<>();
        if (i2 == 0) {
            arrayList.add(0, new ArrayList());
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList.add(i3, new ArrayList());
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.add(new ArrayList());
        }
        if (matcher != null && !str.isEmpty()) {
            while (matcher.find()) {
                if (i == 0) {
                    try {
                        arrayList.get(0).add(matcher.group(0));
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                    }
                } else {
                    int i4 = 0;
                    while (i4 < i) {
                        i4++;
                        arrayList.get(i4).add(matcher.group(i4));
                    }
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.add(new ArrayList());
            }
        }
        return arrayList;
    }
}
