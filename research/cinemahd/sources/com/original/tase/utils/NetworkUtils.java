package com.original.tase.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.movie.FreeMoviesApp;
import com.original.tase.Logger;
import com.utils.Utils;

public class NetworkUtils {
    public static boolean a() {
        ConnectivityManager connectivityManager;
        try {
            connectivityManager = (ConnectivityManager) FreeMoviesApp.a(Utils.i()).getSystemService("connectivity");
        } catch (Throwable th) {
            Logger.a(th, true);
            connectivityManager = null;
        }
        if (connectivityManager == null) {
            return true;
        }
        try {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }
        } catch (Throwable th2) {
            Logger.a(th2, true);
        }
        try {
            NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
            if (networkInfo2 != null && networkInfo2.isConnected()) {
                return true;
            }
        } catch (Throwable th3) {
            Logger.a(th3, true);
        }
        try {
            NetworkInfo networkInfo3 = connectivityManager.getNetworkInfo(9);
            if (networkInfo3 != null && networkInfo3.isConnected()) {
                return true;
            }
        } catch (Throwable th4) {
            Logger.a(th4, true);
        }
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Throwable th5) {
            Logger.a(th5, true);
        }
    }
}
