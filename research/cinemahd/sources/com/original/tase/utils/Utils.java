package com.original.tase.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.StatFs;
import com.original.tase.Logger;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Utils {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f5896a = Pattern.compile("[-+]?\\d+");

    public static String a(int i) {
        String valueOf = String.valueOf(i);
        if (valueOf.length() != 1) {
            return valueOf;
        }
        return "0" + valueOf;
    }

    public static boolean b(String str) {
        return f5896a.matcher(str).matches();
    }

    public static boolean b(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public static String a(String str, int i) {
        return new DecimalFormat(str).format((long) i);
    }

    public static Map<String, String> a(URL url) {
        if (url.getQuery() == null) {
            return new HashMap();
        }
        return a(url.getQuery());
    }

    public static Map<String, String> a(String str) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        String str2 = "";
        if (str.contains("&")) {
            for (String str3 : str.split("&")) {
                int indexOf = str3.indexOf("=");
                if (indexOf != -1) {
                    try {
                        int i = indexOf + 1;
                        linkedHashMap.put(URLDecoder.decode(str3.substring(0, indexOf), "UTF-8"), str3.length() >= i ? URLDecoder.decode(str3.substring(i), "UTF-8") : str2);
                    } catch (UnsupportedEncodingException unused) {
                        String substring = str3.substring(0, indexOf);
                        int i2 = indexOf + 1;
                        linkedHashMap.put(substring, str3.length() >= i2 ? str3.substring(i2) : str2);
                    }
                }
            }
            return linkedHashMap;
        }
        int indexOf2 = str.indexOf("=");
        if (indexOf2 != -1) {
            String str4 = null;
            try {
                str4 = URLDecoder.decode(str.substring(0, indexOf2), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int i3 = indexOf2 + 1;
            if (str.length() >= i3) {
                try {
                    str2 = URLDecoder.decode(str.substring(i3), "UTF-8");
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }
            linkedHashMap.put(str4, str2);
        }
        return linkedHashMap;
    }

    public static String a(String str, boolean... zArr) {
        if (!(zArr != null && zArr.length > 0 && zArr[0]) && (str.trim().toLowerCase().startsWith("http://") || str.trim().toLowerCase().startsWith("https://"))) {
            try {
                URL url = new URL(str);
                return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef()).toURL().toString();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (Throwable th2) {
            Logger.a(th2, new boolean[0]);
            return str.replace(":", "%3A").replace("/", "%2F").replace("#", "%23").replace("?", "%3F").replace("&", "%24").replace("@", "%40").replace("%", "%25").replace("+", "%2B").replace(" ", "+").replace(";", "%3B").replace("=", "%3D").replace("$", "%26").replace(",", "%2C").replace("<", "%3C").replace(">", "%3E").replace("~", "%25").replace("^", "%5E").replace("`", "%60").replace("\\", "%5C").replace("[", "%5B").replace("]", "%5D").replace("{", "%7B").replace("|", "%7C").replace("\"", "%22");
        }
    }

    public static String a(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            sb.append((String) next.getKey());
            sb.append("=");
            sb.append(a((String) next.getValue(), new boolean[0]));
            sb.append("&");
        }
        String sb2 = sb.toString();
        if (sb2.isEmpty()) {
            return sb2;
        }
        return sb2.substring(0, sb2.length() - 1);
    }

    @SuppressLint({"NewApi"})
    public static long a(File file) {
        long j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            int i = Build.VERSION.SDK_INT >= 18 ? 1 : null;
            j = ((i != null ? statFs.getBlockSizeLong() : (long) statFs.getBlockSize()) * (i != null ? statFs.getBlockCountLong() : (long) statFs.getBlockCount())) / 50;
        } catch (IllegalArgumentException unused) {
            j = 10485760;
        }
        return Math.max(Math.min(j, 52428800), 10485760);
    }

    public static <T> ArrayList<T> a(List<T> list) {
        return new ArrayList<>(new LinkedHashSet(list));
    }

    public static String[] a(String[] strArr, String[] strArr2) {
        try {
            int length = strArr.length;
            int length2 = strArr2.length;
            String[] strArr3 = new String[(length + length2)];
            System.arraycopy(strArr, 0, strArr3, 0, length);
            System.arraycopy(strArr2, 0, strArr3, length, length2);
            return strArr3;
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return strArr;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x003e, code lost:
        r5.startActivity(com.original.tase.utils.IntentUtils.a(r5, new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse("https://play.google.com/store/apps/details?id=" + r6)), com.original.tase.I18N.a(com.yoku.marumovie.R.string.choose_browser)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0022 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r5, java.lang.String r6) {
        /*
            java.lang.String r0 = "https://play.google.com/store/apps/details?id="
            java.lang.String r1 = "android.intent.action.VIEW"
            android.content.Intent r2 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x0022 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x0022 }
            r3.<init>()     // Catch:{ ActivityNotFoundException -> 0x0022 }
            java.lang.String r4 = "market://details?id="
            r3.append(r4)     // Catch:{ ActivityNotFoundException -> 0x0022 }
            r3.append(r6)     // Catch:{ ActivityNotFoundException -> 0x0022 }
            java.lang.String r3 = r3.toString()     // Catch:{ ActivityNotFoundException -> 0x0022 }
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ ActivityNotFoundException -> 0x0022 }
            r2.<init>(r1, r3)     // Catch:{ ActivityNotFoundException -> 0x0022 }
            r5.startActivity(r2)     // Catch:{ ActivityNotFoundException -> 0x0022 }
            goto L_0x0064
        L_0x0022:
            android.content.Intent r2 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x003e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x003e }
            r3.<init>()     // Catch:{ ActivityNotFoundException -> 0x003e }
            r3.append(r0)     // Catch:{ ActivityNotFoundException -> 0x003e }
            r3.append(r6)     // Catch:{ ActivityNotFoundException -> 0x003e }
            java.lang.String r3 = r3.toString()     // Catch:{ ActivityNotFoundException -> 0x003e }
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ ActivityNotFoundException -> 0x003e }
            r2.<init>(r1, r3)     // Catch:{ ActivityNotFoundException -> 0x003e }
            r5.startActivity(r2)     // Catch:{ ActivityNotFoundException -> 0x003e }
            goto L_0x0064
        L_0x003e:
            android.content.Intent r2 = new android.content.Intent
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r0)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            android.net.Uri r6 = android.net.Uri.parse(r6)
            r2.<init>(r1, r6)
            r6 = 2131755186(0x7f1000b2, float:1.9141244E38)
            java.lang.String r6 = com.original.tase.I18N.a(r6)
            android.content.Intent r6 = com.original.tase.utils.IntentUtils.a(r5, r2, r6)
            r5.startActivity(r6)
        L_0x0064:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.utils.Utils.a(android.content.Context, java.lang.String):void");
    }
}
