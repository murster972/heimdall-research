package com.original.tase.utils;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import com.original.tase.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IntentUtils {
    @SuppressLint({"WrongConstant"})
    public static Intent a(Context context, Intent intent, String str) {
        Intent createChooser = Intent.createChooser(intent, str);
        try {
            ArrayList arrayList = new ArrayList();
            Intent intent2 = new Intent(intent.getAction());
            intent2.setDataAndType(intent.getData(), intent.getType());
            List<ResolveInfo> a2 = a(context, intent2);
            if (!a2.isEmpty()) {
                for (ResolveInfo next : a2) {
                    if (next.activityInfo != null) {
                        ActivityInfo activityInfo = next.activityInfo;
                        Intent intent3 = new Intent(intent);
                        intent3.addFlags(50331648);
                        intent3.setComponent(new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name));
                        arrayList.add(intent3);
                    }
                }
                createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
            }
            return createChooser;
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return null;
        }
    }

    private static List<ResolveInfo> a(Context context, Intent intent) {
        ActivityInfo activityInfo;
        PackageManager packageManager = context.getPackageManager();
        Intent a2 = a(intent);
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        if (queryIntentActivities.isEmpty() && a2 != null) {
            queryIntentActivities = packageManager.queryIntentActivities(a2, 0);
        } else if (queryIntentActivities.size() == 1) {
            ResolveInfo resolveInfo = queryIntentActivities.get(0);
            if (resolveInfo != null && (activityInfo = resolveInfo.activityInfo) != null && !activityInfo.packageName.equals(context.getPackageName())) {
                return queryIntentActivities;
            }
            queryIntentActivities = a2 != null ? packageManager.queryIntentActivities(a2, 0) : new ArrayList<>();
        }
        if (!queryIntentActivities.isEmpty()) {
            Collections.sort(queryIntentActivities, new ResolveInfo.DisplayNameComparator(packageManager));
        }
        return queryIntentActivities;
    }

    private static Intent a(Intent intent) {
        if (intent.getData() == null) {
            return null;
        }
        return new Intent(intent).setData(a(intent.getData()));
    }

    private static Uri a(Uri uri) {
        return new Uri.Builder().scheme(uri.getScheme()).build();
    }
}
