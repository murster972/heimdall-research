package com.original.tase.utils;

import com.applovin.sdk.AppLovinEventTypes;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.Map;
import okhttp3.Response;

public class SourceObservableUtils {
    public static MediaSource a(MediaSource mediaSource) {
        String str;
        String str2;
        try {
            String lowerCase = mediaSource.getStreamLink().trim().toLowerCase();
            HashMap<String, String> hashMap = new HashMap<>();
            String str3 = "";
            if (!Regex.a(lowerCase, "//[^/]*(ntcdn|micetop)\\.[^/]{2,8}/", 1).isEmpty()) {
                HashMap<String, String> playHeader = mediaSource.getPlayHeader();
                if (playHeader != null) {
                    hashMap = playHeader;
                }
                if (hashMap.containsKey("Referer")) {
                    str2 = hashMap.get("Referer");
                } else {
                    str2 = hashMap.containsKey("referer") ? hashMap.get("referer") : str3;
                }
                if (str2 != null) {
                    str3 = str2.toLowerCase();
                }
                if (lowerCase.contains("vidcdn_pro/") && !str3.contains("vidnode.net")) {
                    hashMap.put("Referer", "https://vidnode.net/");
                } else if (lowerCase.contains("s7_ntcdn_us/") && !str3.contains("m4ufree.info")) {
                    hashMap.put("Referer", "http://m4ufree.info/");
                }
                if (!hashMap.containsKey("User-Agent") && !hashMap.containsKey("user-agent")) {
                    hashMap.put("User-Agent", Constants.f5838a);
                }
                mediaSource.setPlayHeader(hashMap);
            } else if (Regex.a(lowerCase, "//[^/]*(vidcdn)\\.pro/stream/", 1).isEmpty() || lowerCase.contains(".m3u8")) {
                if (mediaSource.getPlayHeader() == null) {
                    mediaSource.setPlayHeader(new HashMap());
                }
                if (!mediaSource.getPlayHeader().containsKey("User-Agent")) {
                    mediaSource.getPlayHeader().put("User-Agent", Constants.f5838a);
                }
            } else {
                HashMap<String, String> playHeader2 = mediaSource.getPlayHeader();
                if (playHeader2.containsKey("Referer")) {
                    str = playHeader2.get("Referer");
                } else {
                    str = playHeader2.containsKey("referer") ? playHeader2.get("referer") : str3;
                }
                if (str != null) {
                    str3 = str.toLowerCase();
                }
                if (!str3.contains("vidnode.net")) {
                    playHeader2.put("Referer", "https://vidnode.net/");
                }
                if (!playHeader2.containsKey("User-Agent") && !playHeader2.containsKey("user-agent")) {
                    playHeader2.put("User-Agent", Constants.f5838a);
                }
                mediaSource.setPlayHeader(playHeader2);
            }
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
        }
        return mediaSource;
    }

    public static Observable<MediaSource> b(final MediaSource mediaSource) {
        return Observable.create(new ObservableOnSubscribe<MediaSource>() {
            public void subscribe(ObservableEmitter<MediaSource> observableEmitter) {
                try {
                    if (!mediaSource.isTorrent()) {
                        if (mediaSource.getFileSize() > 0) {
                            if (!mediaSource.isAlldebrid() && !mediaSource.isRealdebrid()) {
                                if (mediaSource.isPremiumize()) {
                                }
                            }
                        }
                        String streamLink = mediaSource.getStreamLink();
                        boolean a2 = SourceObservableUtils.a(streamLink);
                        HashMap<String, String> hashMap = new HashMap<>();
                        if (mediaSource.getPlayHeader() != null) {
                            hashMap.putAll(mediaSource.getPlayHeader());
                            hashMap = SourceUtils.b(hashMap);
                            hashMap.put("Range", "bytes=0-1");
                        }
                        Response a3 = HttpHelper.e().a(streamLink, a2, (Map<String, String>) hashMap);
                        if (a3 != null && a3.code() < 400) {
                            if (!mediaSource.isHLS()) {
                                if (!SourceObservableUtils.a(a3, mediaSource)) {
                                    long a4 = HttpHelper.a(a3, true, new boolean[0]);
                                    if (a4 > 20971520) {
                                        mediaSource.setFileSize(a4);
                                        mediaSource.setResolved(true);
                                        observableEmitter.onNext(mediaSource);
                                    }
                                    a3.body().close();
                                }
                            }
                            mediaSource.setHLS(true);
                            mediaSource.setResolved(true);
                            observableEmitter.onNext(mediaSource);
                            a3.body().close();
                        }
                        observableEmitter.onComplete();
                        return;
                    }
                    observableEmitter.onNext(mediaSource);
                    observableEmitter.onComplete();
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                }
            }
        }).subscribeOn(Schedulers.b());
    }

    public static boolean a(String str) {
        String lowerCase = str.trim().toLowerCase();
        return !GoogleVideoHelper.e(str) && !lowerCase.contains("amazonaws") && !lowerCase.contains("amazon.") && !lowerCase.contains("yandex") && !lowerCase.contains("mediafire.") && !lowerCase.contains("/docs/securesc/") && !lowerCase.contains("googleapis") && !lowerCase.contains("hulu.so") && !lowerCase.contains("cloudfront") && !lowerCase.contains("cdn.") && !lowerCase.contains("dfcdn.") && !lowerCase.contains("ntcdn.") && !lowerCase.contains("micetop.") && !lowerCase.contains("/proxy") && !lowerCase.contains("proxy/") && !lowerCase.contains("5.189.155.231") && !lowerCase.contains("streamcherry.xyz") && !lowerCase.contains("up2stream") && !lowerCase.contains("real-debrid") && !lowerCase.contains("juicyapi") && !lowerCase.contains("fruity") && !lowerCase.contains("streamy.") && !lowerCase.contains("moviestime") && !lowerCase.contains("m4ufree") && !lowerCase.contains("gomovieshd.to") && !lowerCase.contains("/videoplayback?data=") && !lowerCase.contains("html5player") && !lowerCase.contains("anyplayer.") && !lowerCase.contains("gdplayer") && (!lowerCase.contains("mehliz") || lowerCase.contains(AppLovinEventTypes.USER_EXECUTED_SEARCH) || lowerCase.contains("/?s=")) && !lowerCase.contains("gphoto.stream") && !lowerCase.contains("azmovies") && !lowerCase.contains("dropboxusercontent.") && !lowerCase.contains("archive.org");
    }

    public static boolean a(Response response, MediaSource mediaSource) {
        try {
            if (response.request() != null) {
                String httpUrl = response.request().url().toString();
                if (!httpUrl.toLowerCase().contains(".m3u8")) {
                    return false;
                }
                mediaSource.setStreamLink(httpUrl);
                return true;
            }
        } catch (Throwable th) {
            Logger.a(th, false);
        }
        return false;
    }
}
