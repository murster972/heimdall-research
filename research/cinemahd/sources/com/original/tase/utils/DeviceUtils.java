package com.original.tase.utils;

import android.os.Build;
import com.movie.FreeMoviesApp;

public class DeviceUtils {
    public static boolean a(boolean... zArr) {
        return (!(zArr != null && zArr.length > 0 && zArr[0]) && FreeMoviesApp.l().getBoolean("pref_force_tv_mode", false)) || b() || a() || (Build.MANUFACTURER.equals("OUYA") && Build.MODEL.toLowerCase().startsWith("ouya_")) || (Build.MANUFACTURER.equalsIgnoreCase("xiaomi") && Build.MODEL.toLowerCase().contains("mibox")) || (Build.MANUFACTURER.equalsIgnoreCase("asus") && Build.MODEL.equalsIgnoreCase("nexus player")) || (Build.MANUFACTURER.equalsIgnoreCase("sony") && Build.MODEL.toLowerCase().contains("bravia")) || Build.MANUFACTURER.equalsIgnoreCase("amlogic") || Build.MANUFACTURER.equalsIgnoreCase("MBX") || Build.MANUFACTURER.equalsIgnoreCase("Netxeon") || Build.MANUFACTURER.equalsIgnoreCase("DroidSticks") || Build.MANUFACTURER.equalsIgnoreCase("MINIX") || (Build.MANUFACTURER.equalsIgnoreCase("rockchip") || Build.MANUFACTURER.equalsIgnoreCase("Unblocktech") || Build.MANUFACTURER.equalsIgnoreCase("Skystream"));
    }

    public static boolean b() {
        return Build.MANUFACTURER.trim().toLowerCase().contains("amazon") && Build.MODEL.trim().toLowerCase().contains("aft");
    }

    public static boolean a() {
        return Build.MANUFACTURER.toLowerCase().contains("nvidia") && Build.MODEL.toLowerCase().contains("shield") && !Build.MODEL.toLowerCase().contains("tablet");
    }
}
