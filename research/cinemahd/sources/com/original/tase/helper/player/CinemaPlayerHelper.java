package com.original.tase.helper.player;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.original.tase.model.media.MediaSource;
import java.util.ArrayList;

public class CinemaPlayerHelper extends BasePlayerHelper {
    public int a() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j) {
        return null;
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        return null;
    }

    /* access modifiers changed from: protected */
    public String a(Context context) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Activity activity) {
    }

    public String b() {
        return "Cinema";
    }

    /* access modifiers changed from: protected */
    public void b(Activity activity) {
    }

    public String c() {
        return "Cinema Player (Built-in)";
    }
}
