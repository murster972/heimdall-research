package com.original.tase.helper.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import androidx.appcompat.app.AlertDialog;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.SourceUtils;
import com.original.tase.utils.Utils;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CLeafPlayerHelper extends BasePlayerHelper implements DialogInterface.OnClickListener {
    static String f = "org.yoku.cleaf.debug";

    public CLeafPlayerHelper() {
        f = "org.yoku.cleaf";
    }

    public int a() {
        return 32123;
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j) {
        String a2 = a((Context) activity);
        if (a2 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? "application/x-mpegURL" : "video/*");
        intent.setPackage(a2);
        intent.setClassName(a2, "org.yoku.cleaf.exo.PlayerActivity");
        intent.putExtra("param_title", mediaSource.getMovieName());
        intent.putExtra("param_subtitle", GlobalVariable.c().a().getAds() != null);
        HashMap<String, String> a3 = SourceUtils.a(mediaSource.getPlayHeader());
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : a3.entrySet()) {
            arrayList.add(next.getKey());
            arrayList.add(next.getValue());
        }
        intent.putExtra("param_headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
        return intent;
    }

    public String b() {
        return "Cleaf";
    }

    /* access modifiers changed from: protected */
    public void b(final Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            final AlertDialog a2 = new AlertDialog.Builder(activity).a();
            a2.setTitle(I18N.a(R.string.cleafplayer_not_installed));
            a2.a((CharSequence) I18N.a(R.string.cleafplayer_not_installed_message));
            a2.c(R.drawable.egg_error);
            a2.a(-1, I18N.a(R.string.install), new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    String cleaf_download_url = GlobalVariable.c().a().getCleaf_download_url();
                    if (cleaf_download_url == null || cleaf_download_url.isEmpty()) {
                        Utils.a((Context) activity, CLeafPlayerHelper.f);
                    } else {
                        com.utils.Utils.b(activity, cleaf_download_url);
                    }
                }
            });
            a2.a(-2, I18N.a(R.string.cancel), this);
            a2.setOnShowListener(new DialogInterface.OnShowListener(this) {
                public void onShow(DialogInterface dialogInterface) {
                    try {
                        a2.b(-1).setTextColor(Color.parseColor("#FFFFBB33"));
                        a2.b(-2).setTextColor(Color.parseColor("#AAAAAA"));
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                    }
                }
            });
            if (!activity.isFinishing()) {
                a2.show();
            }
        }
    }

    public String c() {
        return "CLeafPlayer";
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        String a2 = a((Context) activity);
        if (a2 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(streamLink));
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? "application/x-mpegURL" : "video/*");
        intent.setPackage(a2);
        intent.setClassName(a2, f);
        intent.putExtra("param_title", mediaSource.getMovieName());
        intent.putExtra("param_subtitle", GlobalVariable.c().a().getAds() != null);
        intent.putStringArrayListExtra("param_subtitles_path", arrayList2);
        intent.putStringArrayListExtra("param_subtitles_name", arrayList2);
        float floatValue = Float.valueOf(FreeMoviesApp.l().getString("pref_cc_subs_font_scale", "1.00")).floatValue();
        String string = FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF");
        intent.putExtra("param_font_size", floatValue);
        intent.putExtra("param_font_COLOR", string);
        HashMap<String, String> a3 = SourceUtils.a(mediaSource.getPlayHeader());
        ArrayList arrayList3 = new ArrayList();
        for (Map.Entry next : a3.entrySet()) {
            arrayList3.add(next.getKey());
            arrayList3.add(next.getValue());
        }
        intent.putExtra("param_headers", (String[]) arrayList3.toArray(new String[arrayList3.size()]));
        return intent;
    }

    /* access modifiers changed from: protected */
    public String a(Context context) {
        if (Utils.b(context, f)) {
            return f;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog a2 = new AlertDialog.Builder(activity).a();
            a2.setTitle(I18N.a(R.string.cleafplayer_unable_to_start));
            a2.setTitle(I18N.a(R.string.cleafplayer_unable_to_start_message));
            a2.a(-1, I18N.a(R.string.ok), this);
            if (!activity.isFinishing()) {
                a2.show();
            }
        }
    }
}
