package com.original.tase.helper.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import androidx.appcompat.app.AlertDialog;
import com.facebook.react.uimanager.ViewProps;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.SourceUtils;
import com.original.tase.utils.Utils;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MXPlayerHelper extends BasePlayerHelper {

    class C48261 implements DialogInterface.OnClickListener {
        C48261(MXPlayerHelper mXPlayerHelper, MXPlayerHelper mXPlayerHelper2) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    class C48283 implements DialogInterface.OnClickListener {
        C48283(MXPlayerHelper mXPlayerHelper, MXPlayerHelper mXPlayerHelper2) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    public int a() {
        return 90;
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j) {
        String a2 = a((Context) activity);
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? "application/x-mpegURL" : "video/*");
        intent.setPackage(a2);
        if (a2.equals("com.mxtech.videoplayer.ad")) {
            intent.setClassName(a2, "com.mxtech.videoplayer.ad.ActivityScreen");
        } else if (a2.equals("com.mxtech.videoplayer.pro")) {
            intent.setClassName(a2, "com.mxtech.videoplayer.ActivityScreen");
        }
        intent.putExtra("return_result", true);
        intent.putExtra("end_by", "user");
        intent.putExtra("suppress_error_message", false);
        intent.putExtra("secure_uri", true);
        intent.putExtra("title", str);
        if (j > -1) {
            if (j <= 0) {
                j = 1;
            }
            try {
                intent.putExtra(ViewProps.POSITION, (int) j);
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
        }
        HashMap<String, String> playHeader = mediaSource.getPlayHeader();
        if (playHeader != null && playHeader.size() > 0) {
            HashMap<String, String> a3 = SourceUtils.a(playHeader);
            ArrayList arrayList = new ArrayList();
            for (Map.Entry next : a3.entrySet()) {
                arrayList.add(next.getKey());
                arrayList.add(next.getValue());
            }
            intent.putExtra("headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
        }
        return intent;
    }

    public String b() {
        return "MX";
    }

    /* access modifiers changed from: protected */
    public void b(final Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog a2 = new AlertDialog.Builder(activity).a();
            a2.setTitle(I18N.a(R.string.mxplayer_not_installed));
            a2.a((CharSequence) I18N.a(R.string.mxplayer_not_installed_message));
            a2.a(-1, I18N.a(R.string.install), new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utils.a((Context) activity, "com.mxtech.videoplayer.ad");
                }
            });
            a2.a(-2, I18N.a(R.string.cancel), new C48283(this, this));
            if (!activity.isFinishing()) {
                a2.show();
            }
        }
    }

    public String c() {
        return "MX Player";
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        String a2 = a((Context) activity);
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator<String> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            arrayList3.add(Uri.parse("file://" + it2.next()));
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), "video/*");
        intent.setPackage(a2);
        if (a2.equals("com.mxtech.videoplayer.ad")) {
            intent.setClassName(a2, "com.mxtech.videoplayer.ad.ActivityScreen");
        } else if (a2.equals("com.mxtech.videoplayer.pro")) {
            intent.setClassName(a2, "com.mxtech.videoplayer.ActivityScreen");
        }
        intent.putExtra("return_result", true);
        intent.putExtra("end_by", "user");
        intent.putExtra("suppress_error_message", false);
        intent.putExtra("secure_uri", true);
        intent.putExtra("subs.enable", true);
        intent.putExtra("title", str);
        intent.putExtra("subs", (Parcelable[]) arrayList3.toArray(new Uri[arrayList3.size()]));
        intent.putExtra("subs.name", (String[]) arrayList2.toArray(new String[arrayList2.size()]));
        if (j > -1) {
            if (j <= 0) {
                j = 1;
            }
            try {
                intent.putExtra(ViewProps.POSITION, (int) j);
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
        }
        HashMap<String, String> playHeader = mediaSource.getPlayHeader();
        if (playHeader != null && playHeader.size() > 0) {
            HashMap<String, String> a3 = SourceUtils.a(playHeader);
            ArrayList arrayList4 = new ArrayList();
            for (Map.Entry next : a3.entrySet()) {
                arrayList4.add(next.getKey());
                arrayList4.add(next.getValue());
            }
            intent.putExtra("headers", (String[]) arrayList4.toArray(new String[arrayList4.size()]));
        }
        return intent;
    }

    /* access modifiers changed from: protected */
    public String a(Context context) {
        if (Utils.b(context, "com.mxtech.videoplayer.pro")) {
            return "com.mxtech.videoplayer.pro";
        }
        if (Utils.b(context, "com.mxtech.videoplayer.ad")) {
            return "com.mxtech.videoplayer.ad";
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog a2 = new AlertDialog.Builder(activity).a();
            a2.setTitle(I18N.a(R.string.mxplayer_unable_to_start));
            a2.a((CharSequence) I18N.a(R.string.mxplayer_unable_to_start_message));
            a2.a(-1, I18N.a(R.string.ok), new C48261(this, this));
            if (!activity.isFinishing()) {
                a2.show();
            }
        }
    }
}
