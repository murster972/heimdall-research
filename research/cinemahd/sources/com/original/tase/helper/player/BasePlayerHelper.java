package com.original.tase.helper.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import com.movie.FreeMoviesApp;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import com.utils.cast.CastHelper;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public abstract class BasePlayerHelper {

    /* renamed from: a  reason: collision with root package name */
    protected String f5880a;
    protected long b;
    protected ArrayList<String> c;
    protected MediaSource d;
    protected Fragment e;

    public interface OnChoosePlayListener {
        void a(int i, MediaSource mediaSource);
    }

    public interface OnChoosePlayerListener {
        void choosePlayer(String str);
    }

    /* access modifiers changed from: private */
    public boolean b(Activity activity, Intent intent) {
        if (a((Context) activity) == null) {
            b(activity);
            return false;
        } else if (intent != null) {
            return a(activity, intent);
        } else {
            a(activity);
            return false;
        }
    }

    public static BasePlayerHelper d() {
        return new CinemaPlayerHelper();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.original.tase.helper.player.BasePlayerHelper e() {
        /*
            android.content.SharedPreferences r0 = com.movie.FreeMoviesApp.l()
            com.original.tase.helper.player.BasePlayerHelper r1 = d()
            java.lang.String r1 = r1.b()
            java.lang.String r2 = "pref_choose_default_player"
            java.lang.String r0 = r0.getString(r2, r1)
            int r1 = r0.hashCode()
            r2 = 2475(0x9ab, float:3.468E-42)
            r3 = 2
            r4 = 1
            if (r1 == r2) goto L_0x003b
            r2 = 85069(0x14c4d, float:1.19207E-40)
            if (r1 == r2) goto L_0x0031
            r2 = 65193505(0x3e2c621, float:1.3328574E-36)
            if (r1 == r2) goto L_0x0027
            goto L_0x0045
        L_0x0027:
            java.lang.String r1 = "Cleaf"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0045
            r0 = 2
            goto L_0x0046
        L_0x0031:
            java.lang.String r1 = "VLC"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0045
            r0 = 1
            goto L_0x0046
        L_0x003b:
            java.lang.String r1 = "MX"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0045
            r0 = 0
            goto L_0x0046
        L_0x0045:
            r0 = -1
        L_0x0046:
            if (r0 == 0) goto L_0x005a
            if (r0 == r4) goto L_0x0054
            if (r0 == r3) goto L_0x004e
            r0 = 0
            return r0
        L_0x004e:
            com.original.tase.helper.player.CLeafPlayerHelper r0 = new com.original.tase.helper.player.CLeafPlayerHelper
            r0.<init>()
            return r0
        L_0x0054:
            com.original.tase.helper.player.VLCPlayerHelper r0 = new com.original.tase.helper.player.VLCPlayerHelper
            r0.<init>()
            return r0
        L_0x005a:
            com.original.tase.helper.player.MXPlayerHelper r0 = new com.original.tase.helper.player.MXPlayerHelper
            r0.<init>()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.player.BasePlayerHelper.e():com.original.tase.helper.player.BasePlayerHelper");
    }

    public static BasePlayerHelper[] f() {
        return new BasePlayerHelper[]{new CinemaPlayerHelper(), new CLeafPlayerHelper(), new MXPlayerHelper(), new VLCPlayerHelper()};
    }

    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract Intent a(Activity activity, MediaSource mediaSource, String str, long j);

    /* access modifiers changed from: protected */
    public abstract Intent a(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2);

    /* access modifiers changed from: protected */
    public abstract String a(Context context);

    /* access modifiers changed from: protected */
    public abstract void a(Activity activity);

    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract void b(Activity activity);

    public abstract String c();

    public static void a(final Activity activity, final OnChoosePlayerListener onChoosePlayerListener) {
        FreeMoviesApp.l().edit().putBoolean("choose_default_video_player_dialog_shown", true).apply();
        String string = FreeMoviesApp.l().getString("pref_choose_default_player", d().b());
        if (string == null || string.isEmpty()) {
            string = "Cinema";
        }
        BasePlayerHelper[] f = f();
        final ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (BasePlayerHelper basePlayerHelper : f) {
            arrayList.add(basePlayerHelper.b());
            arrayList2.add(basePlayerHelper.c());
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.b((CharSequence) I18N.a(R.string.pref_choose_default_player));
        builder.a(true);
        builder.a((CharSequence[]) arrayList2.toArray(new String[arrayList2.size()]), arrayList.indexOf(string), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String str = (String) arrayList.get(i);
                FreeMoviesApp.l().edit().putString("pref_choose_default_player", str).apply();
                OnChoosePlayerListener onChoosePlayerListener = onChoosePlayerListener;
                if (onChoosePlayerListener != null) {
                    onChoosePlayerListener.choosePlayer(str);
                }
                if (!activity.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!activity.isFinishing() && dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.c();
    }

    public boolean a(Activity activity, Fragment fragment, MediaSource mediaSource, String str, long j) {
        Activity activity2 = activity;
        if (j > 0) {
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.a((CharSequence) "Do you wish to resume the last positison?");
                builder.a(true);
                final Activity activity3 = activity;
                final MediaSource mediaSource2 = mediaSource;
                final String str2 = str;
                final long j2 = j;
                final Fragment fragment2 = fragment;
                builder.b((CharSequence) "Resume", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent a2 = BasePlayerHelper.this.a(activity3, mediaSource2, str2, j2);
                        BasePlayerHelper basePlayerHelper = BasePlayerHelper.this;
                        basePlayerHelper.d = mediaSource2;
                        basePlayerHelper.f5880a = str2;
                        basePlayerHelper.c = null;
                        basePlayerHelper.b = j2;
                        basePlayerHelper.e = fragment2;
                        boolean unused = basePlayerHelper.b(activity3, a2);
                    }
                });
                final Activity activity4 = activity;
                final MediaSource mediaSource3 = mediaSource;
                final String str3 = str;
                final Fragment fragment3 = fragment;
                builder.a((CharSequence) "Start over", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent a2 = BasePlayerHelper.this.a(activity4, mediaSource3, str3, 0);
                        BasePlayerHelper basePlayerHelper = BasePlayerHelper.this;
                        basePlayerHelper.d = mediaSource3;
                        basePlayerHelper.f5880a = str3;
                        basePlayerHelper.c = null;
                        basePlayerHelper.b = 0;
                        basePlayerHelper.e = fragment3;
                        boolean unused = basePlayerHelper.b(activity4, a2);
                    }
                });
                builder.c();
            } catch (Throwable th) {
                Logger.a(th.getMessage());
                a(activity);
                return false;
            }
        } else {
            Intent a2 = a(activity, mediaSource, str, j);
            this.e = fragment;
            b(activity, a2);
        }
        return true;
    }

    public boolean a(Activity activity, Fragment fragment, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        Activity activity2 = activity;
        this.e = fragment;
        if (j > 0) {
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity2);
                builder.a((CharSequence) "Do you wish to resume the last positison?");
                builder.a(true);
                final Activity activity3 = activity;
                final MediaSource mediaSource2 = mediaSource;
                final String str2 = str;
                final long j2 = j;
                final ArrayList<String> arrayList3 = arrayList;
                final ArrayList<String> arrayList4 = arrayList2;
                builder.b((CharSequence) "Resume", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent a2 = BasePlayerHelper.this.a(activity3, mediaSource2, str2, j2, arrayList3, arrayList4);
                        BasePlayerHelper basePlayerHelper = BasePlayerHelper.this;
                        basePlayerHelper.d = mediaSource2;
                        basePlayerHelper.f5880a = str2;
                        basePlayerHelper.c = arrayList4;
                        basePlayerHelper.b = j2;
                        boolean unused = basePlayerHelper.b(activity3, a2);
                    }
                });
                final Activity activity4 = activity;
                final MediaSource mediaSource3 = mediaSource;
                final String str3 = str;
                final ArrayList<String> arrayList5 = arrayList;
                final ArrayList<String> arrayList6 = arrayList2;
                builder.a((CharSequence) "Start over", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent a2 = BasePlayerHelper.this.a(activity4, mediaSource3, str3, 0, arrayList5, arrayList6);
                        BasePlayerHelper basePlayerHelper = BasePlayerHelper.this;
                        basePlayerHelper.d = mediaSource3;
                        basePlayerHelper.f5880a = str3;
                        basePlayerHelper.c = arrayList6;
                        basePlayerHelper.b = 0;
                        boolean unused = basePlayerHelper.b(activity4, a2);
                    }
                });
                builder.c();
            } catch (Throwable th) {
                Logger.a(th, false);
                a(activity);
            }
        } else {
            b(activity2, a(activity, mediaSource, str, j, arrayList, arrayList2));
        }
        return false;
    }

    private boolean a(Activity activity, Intent intent) {
        try {
            if (this.e != null) {
                this.e.startActivityForResult(intent, a());
                return true;
            }
            activity.startActivityForResult(intent, a());
            return true;
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            a(activity);
            return false;
        }
    }

    public static void a(final Activity activity, final MediaSource mediaSource, final OnChoosePlayListener onChoosePlayListener) {
        ArrayList arrayList = new ArrayList();
        final ArrayList arrayList2 = new ArrayList();
        String string = FreeMoviesApp.l().getString("pref_choose_default_action", "Always ask");
        LinkedHashMap<String, Integer> e2 = Utils.e();
        for (String next : e2.keySet()) {
            arrayList.add(next);
            arrayList2.add(e2.get(next));
        }
        if (string.equalsIgnoreCase("Always ask") || CastHelper.a(Utils.i())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.b((CharSequence) mediaSource.toString2());
            builder.a((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    int intValue = ((Integer) arrayList2.get(i)).intValue();
                    OnChoosePlayListener onChoosePlayListener = onChoosePlayListener;
                    if (onChoosePlayListener != null) {
                        onChoosePlayListener.a(intValue, mediaSource);
                    }
                    if (!activity.isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.c();
            return;
        }
        int intValue = e2.get(string).intValue();
        if (onChoosePlayListener != null) {
            onChoosePlayListener.a(intValue, mediaSource);
        }
    }
}
