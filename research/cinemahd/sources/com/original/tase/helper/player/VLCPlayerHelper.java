package com.original.tase.helper.player;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AlertDialog;
import com.facebook.react.uimanager.ViewProps;
import com.original.tase.I18N;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Utils;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.Iterator;

public class VLCPlayerHelper extends BasePlayerHelper {
    private boolean f = false;

    class C48291 implements DialogInterface.OnClickListener {
        C48291(VLCPlayerHelper vLCPlayerHelper, VLCPlayerHelper vLCPlayerHelper2) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    class C48313 implements DialogInterface.OnClickListener {
        C48313(VLCPlayerHelper vLCPlayerHelper, VLCPlayerHelper vLCPlayerHelper2) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    public int a() {
        return 431;
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j) {
        String a2 = a((Context) activity);
        if (a2 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), mediaSource.isHLS() ? "application/x-mpegURL" : "video/*");
        intent.setPackage(a2);
        boolean z = this.f;
        intent.setComponent(new ComponentName(a2, "org.videolan.vlc.gui.video.VideoPlayerActivity"));
        intent.putExtra("from_start", j == 0);
        intent.putExtra("title", str);
        if (j > -1) {
            intent.putExtra(ViewProps.POSITION, j);
        }
        return intent;
    }

    public String b() {
        return "VLC";
    }

    /* access modifiers changed from: protected */
    public void b(final Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog a2 = new AlertDialog.Builder(activity).a();
            a2.setTitle(I18N.a(R.string.vlc_player_not_installed));
            a2.a((CharSequence) I18N.a(R.string.vlc_player_not_installed_message));
            a2.a(-1, I18N.a(R.string.install), new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utils.a((Context) activity, "org.videolan.vlc");
                }
            });
            a2.a(-2, I18N.a(R.string.cancel), new C48313(this, this));
            if (!activity.isFinishing()) {
                a2.show();
            }
        }
    }

    public String c() {
        return "VLC Player";
    }

    /* access modifiers changed from: protected */
    public Intent a(Activity activity, MediaSource mediaSource, String str, long j, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        String a2 = a((Context) activity);
        if (a2 == null) {
            return null;
        }
        String streamLink = mediaSource.getStreamLink();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(streamLink), "video/*");
        intent.setPackage(a2);
        boolean z = this.f;
        intent.setComponent(new ComponentName(a2, "org.videolan.vlc.gui.video.VideoPlayerActivity"));
        intent.putExtra("from_start", j == 0);
        intent.putExtra("PLAY_EXTRA_SUBTITLES_LOCATION", true);
        intent.putExtra("title", str);
        if (j > -1) {
            intent.putExtra(ViewProps.POSITION, j);
        }
        Iterator<String> it2 = arrayList.iterator();
        ArrayList arrayList3 = new ArrayList();
        while (it2.hasNext()) {
            String next = it2.next();
            if (!next.endsWith(".nfo")) {
                arrayList3.add(next);
            }
        }
        if (arrayList3.size() > 0 && arrayList3.get(0) != null) {
            intent.putExtra("subtitles_location", (String) arrayList3.get(0));
        }
        return intent;
    }

    /* access modifiers changed from: protected */
    public String a(Context context) {
        if (Utils.b(context, "org.videolan.vlc")) {
            return "org.videolan.vlc";
        }
        if (!Utils.b(context, "org.videolan.vlc.debug")) {
            return null;
        }
        this.f = true;
        return "org.videolan.vlc.debug";
    }

    /* access modifiers changed from: protected */
    public void a(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AlertDialog a2 = new AlertDialog.Builder(activity).a();
            a2.setTitle(I18N.a(R.string.vlc_player_unable_to_start));
            a2.a((CharSequence) I18N.a(R.string.vlc_player_unable_to_start_message));
            a2.a(-1, I18N.a(R.string.ok), new C48291(this, this));
            if (!activity.isFinishing()) {
                a2.show();
            }
        }
    }
}
