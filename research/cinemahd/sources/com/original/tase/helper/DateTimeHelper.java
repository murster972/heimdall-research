package com.original.tase.helper;

import com.original.tase.Logger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;

public class DateTimeHelper {
    public static long a() {
        try {
            return new Date().getTime();
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return 0;
        }
    }

    public static String b(DateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return dateTime.toString(DateTimeFormat.forPattern("yyyy-MM-dd"));
    }

    public static String c() {
        try {
            return String.valueOf(new Date().getTime());
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return "";
        }
    }

    public static DateTime d(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        return DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(str);
    }

    public static String b() {
        return DateTime.now().toDateTime(DateTimeZone.forID("UTC")).toString("yyyy-MM-dd", Locale.US);
    }

    public static boolean a(DateTime dateTime) {
        if (dateTime == null) {
            return true;
        }
        DateTime dateTime2 = DateTime.now().toDateTime(DateTimeZone.forID("UTC"));
        if (dateTime.isBefore((ReadableInstant) dateTime2) || dateTime.isEqual((ReadableInstant) dateTime2)) {
            return true;
        }
        return false;
    }

    public static String b(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        try {
            Date parse = simpleDateFormat.parse(str);
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a");
            simpleDateFormat2.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat2.format(parse);
        } catch (ParseException unused) {
            return str;
        }
    }

    public static String c(String str) {
        try {
            Long valueOf = Long.valueOf(Long.parseLong(str));
            Calendar instance = Calendar.getInstance();
            TimeZone timeZone = TimeZone.getDefault();
            instance.setTimeInMillis(valueOf.longValue() * 1000);
            instance.add(14, timeZone.getOffset(instance.getTimeInMillis()));
            Date time = instance.getTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a");
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat.format(time);
        } catch (Throwable unused) {
            return str;
        }
    }

    public static String a(long j) {
        try {
            Calendar instance = Calendar.getInstance();
            TimeZone timeZone = TimeZone.getDefault();
            instance.setTimeInMillis(1000 * j);
            instance.add(14, timeZone.getOffset(instance.getTimeInMillis()));
            Date time = instance.getTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a");
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat.format(time);
        } catch (Throwable unused) {
            return "" + j;
        }
    }

    public static DateTime a(String str, String str2) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        return DateTimeFormat.forPattern(str2).parseDateTime(str);
    }

    public static boolean a(String str) {
        return !a(a(str, "MMM dd, yyyy HH:mm:ss a"));
    }
}
