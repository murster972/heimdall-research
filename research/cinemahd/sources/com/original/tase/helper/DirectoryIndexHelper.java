package com.original.tase.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DirectoryIndexHelper {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f5869a = {"(.*?){delim}(\\d{4}){delim}.*?(\\d{3,4})p{delim}(.*)", "(.*?){delim}(\\d{3,4})p{delim}.*?(\\d{4}){delim}(.*)", "(.*?){delim}(\\d{4}){delim}(.*)", "(.*?){delim}(\\d{3,4})p{delim}(.*)", "(.*)(\\.[A-Z\\d]{3}$)", "(.*)"};
    private static final String[] b = {"(.*?){delim}S(\\d+){delim}*E(\\d+)(?:E\\d+)*.*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}(\\d+)x(\\d+)(?:-\\d+)*.*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}SEASON{delim}*(\\d+){delim}*EPISODE{delim}*(\\d+).*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}\\[S(\\d+)\\]{delim}*\\[E(\\d+)(?:E\\d+)*\\].*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}S(\\d+){delim}*EP(\\d+)(?:EP\\d+)*.*?{delim}(\\d{3,4})p{delim}?(.*)", "(.*?){delim}S(\\d+){delim}*E(\\d+)(?:E\\d+)*{delim}?(.*)", "(.*?){delim}(\\d+)x(\\d+)(?:-\\d+)*{delim}?(.*)", "(.*?){delim}SEASON{delim}*(\\d+){delim}*EPISODE{delim}*(\\d+){delim}?(.*)", "(.*?){delim}\\[S(\\d+)\\]{delim}*\\[E(\\d+)(?:E\\d+)*\\]{delim}?(.*)", "(.*?){delim}S(\\d+){delim}*EP(\\d+)(?:E\\d+)*{delim}?(.*)", "(.*?){delim}(\\d{3,4})p{delim}?(.*)", "(.*)"};

    public static class ParsedLinkModel {

        /* renamed from: a  reason: collision with root package name */
        private String f5870a = "HQ";
        private String b = "";
        private boolean c = false;
        private int d = -1;
        private final String e;
        private int f = -1;
        private int g = -1;
        private final int h;

        public ParsedLinkModel(int i, String str) {
            this.h = i;
            this.e = str;
        }

        public boolean a() {
            return this.c;
        }

        public void b(int i) {
            this.d = i;
        }

        public void c(int i) {
            this.g = i;
        }

        public String toString() {
            return "ParsedLinkModel{type=" + this.h + ", title='" + this.e + '\'' + ", year=" + this.g + ", season=" + this.f + ", episode=" + this.d + ", quality='" + this.f5870a + '\'' + ", extra='" + this.b + '\'' + '}';
        }

        public void a(boolean z) {
            this.c = z;
        }

        public void b(String str) {
            this.f5870a = str;
        }

        public String c() {
            if (a()) {
                this.b += "-3d";
            }
            if (this.b != null && !this.f5870a.equalsIgnoreCase("4K") && (this.b.trim().toLowerCase().contains("4k") || this.b.trim().toLowerCase().contains("ultrahd"))) {
                this.f5870a = "4K";
            } else if (this.b != null && this.f5870a.equalsIgnoreCase("HQ") && (this.b.trim().toLowerCase().contains("brrip") || this.b.trim().toLowerCase().contains("bdrip") || this.b.trim().toLowerCase().contains("web-dl"))) {
                this.f5870a = "HD";
            }
            return this.f5870a;
        }

        public void a(int i) {
            this.f = i;
        }

        public String b() {
            return this.b;
        }

        public void a(String str) {
            this.b = str;
        }
    }

    public DirectoryIndexHelper() {
        this("\\s*<a\\s+href=\"([^\"]+)\">([^<]+)</a>");
    }

    private boolean c(String str) {
        return str.toLowerCase().contains("3d");
    }

    public ParsedLinkModel a(String str) {
        return a(str, f5869a, 1);
    }

    public ParsedLinkModel b(String str) {
        return a(str, b, 0);
    }

    public DirectoryIndexHelper(String str) {
    }

    private ParsedLinkModel a(String str, String[] strArr, int i) {
        String str2;
        try {
            str2 = URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException unused) {
            str2 = URLDecoder.decode(str);
        }
        if (str2.contains("/")) {
            String[] split = str2.split("\\/");
            if (split.length > 1) {
                str2 = split[split.length - 1];
            }
        }
        for (int i2 = 0; i2 < strArr.length; i2++) {
            Matcher matcher = Pattern.compile(strArr[i2].replace("{delim}", "[._ -]"), 34).matcher(str2);
            int groupCount = matcher.groupCount();
            while (matcher.find()) {
                if (matcher.matches()) {
                    if (i == 0) {
                        if (groupCount == 5) {
                            String group = matcher.group(1);
                            String group2 = matcher.group(2);
                            String group3 = matcher.group(3);
                            String group4 = matcher.group(4);
                            String group5 = matcher.group(5);
                            ParsedLinkModel parsedLinkModel = new ParsedLinkModel(i, group);
                            parsedLinkModel.a(Integer.parseInt(group2));
                            parsedLinkModel.b(Integer.parseInt(group3));
                            parsedLinkModel.b(group4);
                            parsedLinkModel.a(group5);
                            parsedLinkModel.a(c(str2));
                            return parsedLinkModel;
                        } else if (groupCount == 4) {
                            String group6 = matcher.group(1);
                            String group7 = matcher.group(2);
                            String group8 = matcher.group(3);
                            String group9 = matcher.group(4);
                            ParsedLinkModel parsedLinkModel2 = new ParsedLinkModel(i, group6);
                            parsedLinkModel2.a(Integer.parseInt(group7));
                            parsedLinkModel2.b(Integer.parseInt(group8));
                            parsedLinkModel2.a(c(str2));
                            parsedLinkModel2.a(group9);
                            return parsedLinkModel2;
                        } else if (groupCount == 3) {
                            String group10 = matcher.group(1);
                            String group11 = matcher.group(2);
                            String group12 = matcher.group(3);
                            ParsedLinkModel parsedLinkModel3 = new ParsedLinkModel(i, group10);
                            parsedLinkModel3.b(group11);
                            parsedLinkModel3.a(group12);
                            parsedLinkModel3.a(c(str2));
                            return parsedLinkModel3;
                        } else if (groupCount == 1) {
                            ParsedLinkModel parsedLinkModel4 = new ParsedLinkModel(i, matcher.group(1));
                            parsedLinkModel4.a(c(str2));
                            return parsedLinkModel4;
                        }
                    } else if (i == 1) {
                        if (i2 == 0) {
                            String group13 = matcher.group(1);
                            String group14 = matcher.group(2);
                            String group15 = matcher.group(3);
                            String group16 = matcher.group(4);
                            ParsedLinkModel parsedLinkModel5 = new ParsedLinkModel(i, group13);
                            parsedLinkModel5.c(Integer.parseInt(group14));
                            parsedLinkModel5.b(group15);
                            parsedLinkModel5.a(group16);
                            parsedLinkModel5.a(c(str2));
                            return parsedLinkModel5;
                        } else if (i2 == 1) {
                            String group17 = matcher.group(1);
                            String group18 = matcher.group(2);
                            String group19 = matcher.group(3);
                            String group20 = matcher.group(4);
                            ParsedLinkModel parsedLinkModel6 = new ParsedLinkModel(i, group17);
                            parsedLinkModel6.b(group18);
                            parsedLinkModel6.c(Integer.parseInt(group19));
                            parsedLinkModel6.a(group20);
                            parsedLinkModel6.a(c(str2));
                            return parsedLinkModel6;
                        } else if (i2 == 2) {
                            String group21 = matcher.group(1);
                            String group22 = matcher.group(2);
                            String group23 = matcher.group(3);
                            ParsedLinkModel parsedLinkModel7 = new ParsedLinkModel(i, group21);
                            parsedLinkModel7.c(Integer.parseInt(group22));
                            parsedLinkModel7.a(group23);
                            parsedLinkModel7.a(c(str2));
                            return parsedLinkModel7;
                        } else if (i2 == 3) {
                            String group24 = matcher.group(1);
                            String group25 = matcher.group(2);
                            String group26 = matcher.group(3);
                            ParsedLinkModel parsedLinkModel8 = new ParsedLinkModel(i, group24);
                            parsedLinkModel8.b(group25);
                            parsedLinkModel8.a(group26);
                            parsedLinkModel8.a(c(str2));
                            return parsedLinkModel8;
                        } else if (i2 >= 4) {
                            ParsedLinkModel parsedLinkModel9 = new ParsedLinkModel(i, matcher.group(1));
                            parsedLinkModel9.a(c(str2));
                            return parsedLinkModel9;
                        }
                    }
                    return null;
                }
            }
        }
        return null;
    }
}
