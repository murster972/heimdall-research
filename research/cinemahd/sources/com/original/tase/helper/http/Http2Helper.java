package com.original.tase.helper.http;

import com.original.Constants;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Http2Helper {
    private static volatile Http2Helper b;
    private static final TrustManager[] c = {new X509TrustManager() {
        public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }};
    private static final SSLContext d;
    private static final SSLSocketFactory e = d.getSocketFactory();

    /* renamed from: a  reason: collision with root package name */
    private OkHttpClient f5874a;

    static {
        try {
            d = SSLContext.getInstance("SSL");
            d.init((KeyManager[]) null, c, new SecureRandom());
        } catch (KeyManagementException | NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    private Http2Helper() {
        if (this.f5874a == null) {
            this.f5874a = a();
        }
    }

    private OkHttpClient a() {
        return a(new OkHttpClient());
    }

    public static Http2Helper b() {
        if (b == null) {
            synchronized (Http2Helper.class) {
                if (b == null) {
                    b = new Http2Helper();
                }
            }
        }
        return b;
    }

    public static OkHttpClient a(OkHttpClient okHttpClient) {
        OkHttpClient.Builder newBuilder = okHttpClient.newBuilder();
        newBuilder.sslSocketFactory(e, (X509TrustManager) c[0]);
        newBuilder.hostnameVerifier(new HostnameVerifier() {
            public boolean verify(String str, SSLSession sSLSession) {
                return true;
            }
        });
        return newBuilder.build();
    }

    public String a(String str, Map<String, String>... mapArr) {
        if (str.isEmpty()) {
            return null;
        }
        Request.Builder tag = new Request.Builder().url(str).addHeader("User-Agent", Constants.f5838a).tag(HttpHelper.d);
        if (!(mapArr == null || mapArr.length <= 0 || mapArr[0] == null)) {
            for (Map.Entry next : mapArr[0].entrySet()) {
                if (((String) next.getKey()).toLowerCase().equals("user-agent")) {
                    tag.header("User-Agent", (String) next.getValue());
                } else {
                    tag.addHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        try {
            Response execute = this.f5874a.newCall(tag.url(str).build()).execute();
            if (execute == null || execute.code() != 200) {
                return null;
            }
            return execute.body().string();
        } catch (Throwable unused) {
            return null;
        }
    }
}
