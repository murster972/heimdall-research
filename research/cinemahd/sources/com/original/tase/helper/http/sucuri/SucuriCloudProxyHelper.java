package com.original.tase.helper.http.sucuri;

import android.util.Base64;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.squareup.duktape.Duktape;
import com.uwetrottmann.thetvdb.TheTvdb;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import okhttp3.CacheControl;
import okhttp3.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SucuriCloudProxyHelper {
    public static boolean a(String str, String str2) {
        Iterator it2 = Jsoup.b(str2).g("script").iterator();
        while (it2.hasNext()) {
            String u = ((Element) it2.next()).u();
            if (c(u)) {
                HttpHelper.e().c(str, a(u));
                return true;
            }
        }
        return false;
    }

    public static String b(String str, String str2) {
        String a2 = HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0]);
        if (!a(str, a2)) {
            return a2;
        }
        return HttpHelper.e().a(new Request.Builder().get().url(str2).tag(HttpHelper.d).addHeader("User-Agent", Constants.f5838a).addHeader("Referer", str).addHeader("Host", str.replace("https://", "").replace("http://", "").replace("/", "")).addHeader("Cookie", HttpHelper.e().a(str2)).addHeader(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8").addHeader(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.6,en;q=0.4").cacheControl(CacheControl.FORCE_NETWORK).build());
    }

    public static boolean c(String str) {
        return str.contains("sucuri_cloudproxy_js");
    }

    private static String b(String str) {
        return "document = {cookie: \"\"};\nlocation = {reload: function(){/*Empty method of location.reload()*/}};\n\n{jsCode};\n\nDuktape.enc('base64', document.cookie.toString());".replace("{jsCode}", str);
    }

    /* JADX INFO: finally extract failed */
    private static String a(String str) {
        Object obj;
        Duktape create = Duktape.create();
        try {
            obj = create.evaluate(b(str));
            create.close();
        } catch (Throwable th) {
            try {
                Logger.a(th, new boolean[0]);
                create.close();
                obj = null;
            } catch (Throwable th2) {
                create.close();
                throw th2;
            }
        }
        if (obj != null) {
            try {
                return new String(Base64.decode(obj.toString(), 0), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
