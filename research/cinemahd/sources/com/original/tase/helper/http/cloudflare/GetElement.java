package com.original.tase.helper.http.cloudflare;

interface GetElement {
    String byID(String str);
}
