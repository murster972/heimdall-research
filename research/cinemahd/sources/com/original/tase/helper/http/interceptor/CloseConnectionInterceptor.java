package com.original.tase.helper.http.interceptor;

import com.original.tase.Logger;
import com.vungle.warren.ui.JavascriptBridge;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class CloseConnectionInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        String header = request.header("X-Request-CC");
        boolean z = header != null && !header.isEmpty() && header.equalsIgnoreCase("true");
        Response proceed = chain.proceed(request.newBuilder().removeHeader("X-Request-CC").build());
        if (z) {
            try {
                return proceed.newBuilder().removeHeader("X-Request-CC").addHeader("Connection", JavascriptBridge.MraidHandler.CLOSE_ACTION).build();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                return proceed;
            }
        } else {
            try {
                return proceed.newBuilder().removeHeader("X-Request-CC").build();
            } catch (Throwable th2) {
                Logger.a(th2, new boolean[0]);
                return proceed;
            }
        }
    }
}
