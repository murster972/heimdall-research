package com.original.tase.helper.http.cloudflare;

public class CloudflareException extends Exception {
    public CloudflareException(String str) {
        super(str);
    }
}
