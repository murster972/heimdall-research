package com.original.tase.helper.http;

import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.original.Constants;
import com.original.tase.Logger;
import com.utils.Utils;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import okhttp3.Call;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.TlsVersion;

public class HttpHelper {
    private static volatile HttpHelper c;
    public static final Object d = new Object();

    /* renamed from: a  reason: collision with root package name */
    private CookieJar f5875a;
    private OkHttpClient b;

    private HttpHelper() {
        if (this.f5875a == null) {
            this.f5875a = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(Utils.i()));
        }
        if (this.b == null) {
            this.b = a();
        }
    }

    private static OkHttpClient.Builder a(OkHttpClient.Builder builder) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(ConnectionSpec.CLEARTEXT);
        arrayList.add(ConnectionSpec.MODERN_TLS);
        arrayList.add(d());
        arrayList.add(ConnectionSpec.COMPATIBLE_TLS);
        return builder.connectionSpecs(arrayList);
    }

    private static ConnectionSpec d() {
        return new ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS).tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1).cipherSuites(CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_RSA_WITH_3DES_EDE_CBC_SHA).build();
    }

    public static HttpHelper e() {
        if (c == null) {
            synchronized (HttpHelper.class) {
                c = new HttpHelper();
            }
        }
        return c;
    }

    public synchronized OkHttpClient b() {
        return this.b;
    }

    @SafeVarargs
    public final ResponseBody c(String str, Map<String, String>... mapArr) {
        if (str.isEmpty()) {
            return null;
        }
        Request.Builder tag = new Request.Builder().url(str).addHeader("User-Agent", Constants.f5838a).tag(d);
        if (!(mapArr == null || mapArr.length <= 0 || mapArr[0] == null)) {
            for (Map.Entry next : mapArr[0].entrySet()) {
                if (((String) next.getKey()).toLowerCase().equals("user-agent")) {
                    tag.header("User-Agent", (String) next.getValue());
                } else {
                    tag.addHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        Response a2 = a(tag.build(), new int[0]);
        if (a2 == null) {
            return null;
        }
        if (a2.code() != 404) {
            return a2.body();
        }
        if (a2.body() != null) {
            a2.body().close();
        }
        return null;
    }

    public String b(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", str2);
        return a(str, (Map<String, String>[]) new Map[]{hashMap});
    }

    @SafeVarargs
    public final String b(String str, String str2, Map<String, String>... mapArr) {
        if (str.isEmpty()) {
            return "";
        }
        Request.Builder tag = new Request.Builder().url(str).addHeader("User-Agent", Constants.f5838a).tag(d);
        if (!(mapArr == null || mapArr.length <= 0 || mapArr[0] == null)) {
            for (Map.Entry next : mapArr[0].entrySet()) {
                if (((String) next.getKey()).toLowerCase().equals("user-agent")) {
                    tag.header("User-Agent", (String) next.getValue());
                } else {
                    tag.addHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        if (str2 != null && !str2.isEmpty()) {
            tag.addHeader("Referer", str2);
        }
        return a(tag.build());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002d, code lost:
        if (r7.containsKey("content-range") != false) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b4, code lost:
        if (r7.containsKey("content-length") != false) goto L_0x00b6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0136 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(okhttp3.Response r12, boolean r13, boolean... r14) {
        /*
            java.lang.String r0 = "/"
            java.lang.String r1 = "Content-Range"
            java.lang.String r2 = "Content-Length"
            r3 = -1
            if (r12 != 0) goto L_0x000b
            return r3
        L_0x000b:
            r5 = 0
            r6 = 1
            okhttp3.Headers r7 = r12.headers()     // Catch:{ all -> 0x0121 }
            if (r7 == 0) goto L_0x0127
            okhttp3.Headers r7 = r12.headers()     // Catch:{ all -> 0x0121 }
            java.util.Map r7 = r7.toMultimap()     // Catch:{ all -> 0x0121 }
            if (r7 == 0) goto L_0x0127
            r8 = 0
            if (r13 == 0) goto L_0x00a8
            boolean r10 = r7.containsKey(r1)     // Catch:{ all -> 0x0121 }
            java.lang.String r11 = "content-range"
            if (r10 != 0) goto L_0x002f
            boolean r10 = r7.containsKey(r11)     // Catch:{ all -> 0x0121 }
            if (r10 == 0) goto L_0x00a8
        L_0x002f:
            boolean r2 = r7.containsKey(r1)     // Catch:{ all -> 0x0121 }
            if (r2 == 0) goto L_0x003c
            java.lang.Object r1 = r7.get(r1)     // Catch:{ all -> 0x0121 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x0121 }
            goto L_0x0042
        L_0x003c:
            java.lang.Object r1 = r7.get(r11)     // Catch:{ all -> 0x0121 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x0121 }
        L_0x0042:
            if (r1 == 0) goto L_0x0127
            int r2 = r1.size()     // Catch:{ all -> 0x0121 }
            if (r2 <= 0) goto L_0x0127
            java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x0121 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x0127
            boolean r2 = r1.isEmpty()     // Catch:{ all -> 0x0121 }
            if (r2 != 0) goto L_0x0127
            r2 = 0
            boolean r7 = r1.contains(r0)     // Catch:{ all -> 0x0121 }
            if (r7 == 0) goto L_0x0063
            java.lang.String[] r2 = r1.split(r0)     // Catch:{ all -> 0x0121 }
        L_0x0063:
            if (r2 == 0) goto L_0x006f
            int r0 = r2.length     // Catch:{ all -> 0x0121 }
            r7 = 2
            if (r0 != r7) goto L_0x006f
            r0 = r2[r6]     // Catch:{ all -> 0x0121 }
            java.lang.String r1 = r0.trim()     // Catch:{ all -> 0x0121 }
        L_0x006f:
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x0121 }
            if (r0 != 0) goto L_0x0127
            boolean r0 = com.original.tase.utils.Utils.b(r1)     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x0127
            java.lang.Long r0 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0121 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0121 }
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 > 0) goto L_0x00a7
            okhttp3.ResponseBody r2 = r12.body()     // Catch:{ all -> 0x011c }
            long r0 = r2.contentLength()     // Catch:{ all -> 0x011c }
            if (r14 == 0) goto L_0x00a7
            int r2 = r14.length     // Catch:{ all -> 0x011c }
            if (r2 <= 0) goto L_0x00a7
            okhttp3.Response r2 = r12.networkResponse()     // Catch:{ all -> 0x011c }
            if (r2 == 0) goto L_0x00a7
            okhttp3.Response r2 = r12.networkResponse()     // Catch:{ all -> 0x011c }
            boolean[] r3 = new boolean[r6]     // Catch:{ all -> 0x011c }
            r3[r5] = r6     // Catch:{ all -> 0x011c }
            long r12 = a((okhttp3.Response) r2, (boolean) r13, (boolean[]) r3)     // Catch:{ all -> 0x011c }
            return r12
        L_0x00a7:
            return r0
        L_0x00a8:
            boolean r0 = r7.containsKey(r2)     // Catch:{ all -> 0x0121 }
            java.lang.String r1 = "content-length"
            if (r0 != 0) goto L_0x00b6
            boolean r0 = r7.containsKey(r1)     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x0127
        L_0x00b6:
            boolean r0 = r7.containsKey(r2)     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x00c3
            java.lang.Object r0 = r7.get(r2)     // Catch:{ all -> 0x0121 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x0121 }
            goto L_0x00c9
        L_0x00c3:
            java.lang.Object r0 = r7.get(r1)     // Catch:{ all -> 0x0121 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x0121 }
        L_0x00c9:
            if (r0 == 0) goto L_0x0127
            int r1 = r0.size()     // Catch:{ all -> 0x0121 }
            if (r1 <= 0) goto L_0x0127
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x0121 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x0127
            boolean r1 = r0.isEmpty()     // Catch:{ all -> 0x0121 }
            if (r1 != 0) goto L_0x0127
            boolean r1 = com.original.tase.utils.Utils.b(r0)     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x0127
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0121 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0121 }
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 >= 0) goto L_0x0120
            okhttp3.ResponseBody r2 = r12.body()     // Catch:{ all -> 0x011c }
            if (r2 == 0) goto L_0x0120
            okhttp3.ResponseBody r2 = r12.body()     // Catch:{ all -> 0x011c }
            long r0 = r2.contentLength()     // Catch:{ all -> 0x011c }
            if (r14 == 0) goto L_0x011b
            int r2 = r14.length     // Catch:{ all -> 0x011c }
            if (r2 <= 0) goto L_0x011b
            int r2 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r2 > 0) goto L_0x011b
            okhttp3.Response r2 = r12.networkResponse()     // Catch:{ all -> 0x011c }
            if (r2 == 0) goto L_0x011b
            okhttp3.Response r2 = r12.networkResponse()     // Catch:{ all -> 0x011c }
            boolean[] r3 = new boolean[r6]     // Catch:{ all -> 0x011c }
            r3[r5] = r6     // Catch:{ all -> 0x011c }
            long r12 = a((okhttp3.Response) r2, (boolean) r13, (boolean[]) r3)     // Catch:{ all -> 0x011c }
            return r12
        L_0x011b:
            return r0
        L_0x011c:
            r2 = move-exception
            r3 = r0
            r0 = r2
            goto L_0x0122
        L_0x0120:
            return r0
        L_0x0121:
            r0 = move-exception
        L_0x0122:
            boolean[] r1 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r1)
        L_0x0127:
            okhttp3.ResponseBody r0 = r12.body()
            if (r0 == 0) goto L_0x0136
            okhttp3.ResponseBody r13 = r12.body()
            long r3 = r13.contentLength()
            goto L_0x014e
        L_0x0136:
            if (r14 == 0) goto L_0x014e
            int r14 = r14.length
            if (r14 <= 0) goto L_0x014e
            okhttp3.Response r14 = r12.networkResponse()
            if (r14 == 0) goto L_0x014e
            okhttp3.Response r12 = r12.networkResponse()
            boolean[] r14 = new boolean[r6]
            r14[r5] = r6
            long r12 = a((okhttp3.Response) r12, (boolean) r13, (boolean[]) r14)
            return r12
        L_0x014e:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            okhttp3.Request r12 = r12.request()
            okhttp3.HttpUrl r12 = r12.url()
            r13.append(r12)
            java.lang.String r12 = " size = "
            r13.append(r12)
            r13.append(r3)
            java.lang.String r12 = r13.toString()
            java.lang.String r13 = "HttpHelper "
            com.original.tase.Logger.a((java.lang.String) r13, (java.lang.String) r12)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.a(okhttp3.Response, boolean, boolean[]):long");
    }

    public void c() {
        for (Call cancel : this.b.dispatcher().queuedCalls()) {
            cancel.cancel();
        }
        for (Call cancel2 : this.b.dispatcher().runningCalls()) {
            cancel2.cancel();
        }
        this.b.dispatcher().executorService().shutdown();
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0090  */
    @java.lang.SafeVarargs
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.original.tase.model.HttpHeaderBodyResult b(java.lang.String r6, java.util.Map<java.lang.String, java.lang.String>... r7) {
        /*
            r5 = this;
            okhttp3.Request$Builder r0 = new okhttp3.Request$Builder
            r0.<init>()
            okhttp3.Request$Builder r6 = r0.url((java.lang.String) r6)
            java.lang.String r0 = com.original.Constants.f5838a
            java.lang.String r1 = "User-Agent"
            okhttp3.Request$Builder r6 = r6.addHeader(r1, r0)
            java.lang.Object r0 = d
            okhttp3.Request$Builder r6 = r6.tag(r0)
            r0 = 0
            if (r7 == 0) goto L_0x0063
            int r2 = r7.length
            if (r2 <= 0) goto L_0x0063
            r2 = r7[r0]
            if (r2 == 0) goto L_0x0063
            r7 = r7[r0]
            java.util.Set r7 = r7.entrySet()
            java.util.Iterator r7 = r7.iterator()
        L_0x002b:
            boolean r2 = r7.hasNext()
            if (r2 == 0) goto L_0x0063
            java.lang.Object r2 = r7.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r3 = r2.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r3 = r3.toLowerCase()
            java.lang.String r4 = "user-agent"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0053
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = (java.lang.String) r2
            r6.header(r1, r2)
            goto L_0x002b
        L_0x0053:
            java.lang.Object r3 = r2.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = (java.lang.String) r2
            r6.addHeader(r3, r2)
            goto L_0x002b
        L_0x0063:
            okhttp3.Request r6 = r6.build()
            int[] r7 = new int[r0]
            okhttp3.Response r6 = r5.a((okhttp3.Request) r6, (int[]) r7)
            if (r6 != 0) goto L_0x0071
            r6 = 0
            return r6
        L_0x0071:
            int r7 = r6.code()
            r1 = 404(0x194, float:5.66E-43)
            if (r7 == r1) goto L_0x0088
            okhttp3.ResponseBody r7 = r6.body()     // Catch:{ all -> 0x0082 }
            java.lang.String r7 = r7.string()     // Catch:{ all -> 0x0082 }
            goto L_0x008a
        L_0x0082:
            r7 = move-exception
            boolean[] r0 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r7, (boolean[]) r0)
        L_0x0088:
            java.lang.String r7 = ""
        L_0x008a:
            okhttp3.ResponseBody r0 = r6.body()
            if (r0 == 0) goto L_0x0097
            okhttp3.ResponseBody r0 = r6.body()
            r0.close()
        L_0x0097:
            com.original.tase.model.HttpHeaderBodyResult r0 = new com.original.tase.model.HttpHeaderBodyResult
            okhttp3.Headers r6 = r6.headers()
            java.util.Map r6 = r6.toMultimap()
            r0.<init>(r6, r7)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.b(java.lang.String, java.util.Map[]):com.original.tase.model.HttpHeaderBodyResult");
    }

    public static class Network {
        /* JADX WARNING: type inference failed for: r0v0 */
        /* JADX WARNING: type inference failed for: r0v1, types: [java.net.HttpURLConnection] */
        /* JADX WARNING: type inference failed for: r0v2 */
        /* JADX WARNING: type inference failed for: r0v4 */
        /* JADX WARNING: type inference failed for: r0v6, types: [java.io.InputStream] */
        /* JADX WARNING: type inference failed for: r0v7 */
        /* JADX WARNING: type inference failed for: r0v10 */
        /* JADX WARNING: type inference failed for: r0v11 */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:(3:15|16|17)|(2:20|21)|22|23) */
        /* JADX WARNING: Can't wrap try/catch for region: R(5:27|28|(2:31|32)|33|34) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0057 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0069 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0072  */
        /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static java.lang.String a(java.lang.String r3, boolean r4, java.util.Map<java.lang.String, java.lang.String> r5) throws java.io.IOException, java.net.URISyntaxException {
            /*
                r0 = 0
                java.net.URL r1 = new java.net.URL     // Catch:{ all -> 0x006f }
                r1.<init>(r3)     // Catch:{ all -> 0x006f }
                java.net.URLConnection r1 = r1.openConnection()     // Catch:{ all -> 0x006f }
                java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ all -> 0x006f }
                r2 = 20000(0x4e20, float:2.8026E-41)
                r1.setConnectTimeout(r2)     // Catch:{ all -> 0x005b }
                r1.setReadTimeout(r2)     // Catch:{ all -> 0x005b }
                r2 = 0
                r1.setInstanceFollowRedirects(r2)     // Catch:{ all -> 0x005b }
                if (r4 == 0) goto L_0x001f
                java.lang.String r4 = "HEAD"
                r1.setRequestMethod(r4)     // Catch:{ all -> 0x005b }
            L_0x001f:
                if (r5 == 0) goto L_0x0045
                java.util.Set r4 = r5.entrySet()     // Catch:{ all -> 0x005b }
                java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x005b }
            L_0x0029:
                boolean r5 = r4.hasNext()     // Catch:{ all -> 0x005b }
                if (r5 == 0) goto L_0x0045
                java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x005b }
                java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x005b }
                java.lang.Object r2 = r5.getKey()     // Catch:{ all -> 0x005b }
                java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x005b }
                java.lang.Object r5 = r5.getValue()     // Catch:{ all -> 0x005b }
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x005b }
                r1.addRequestProperty(r2, r5)     // Catch:{ all -> 0x005b }
                goto L_0x0029
            L_0x0045:
                java.lang.String r3 = a(r3, r1)     // Catch:{ all -> 0x005b }
                if (r1 == 0) goto L_0x005a
                java.io.InputStream r4 = r1.getInputStream()     // Catch:{ Exception -> 0x0051 }
                r0 = r4
                goto L_0x0052
            L_0x0051:
            L_0x0052:
                if (r0 == 0) goto L_0x0057
                r0.close()     // Catch:{ IOException -> 0x0057 }
            L_0x0057:
                r1.disconnect()     // Catch:{ all -> 0x005b }
            L_0x005a:
                return r3
            L_0x005b:
                r3 = move-exception
                if (r1 == 0) goto L_0x006c
                java.io.InputStream r0 = r1.getInputStream()     // Catch:{ Exception -> 0x0063 }
                goto L_0x0064
            L_0x0063:
            L_0x0064:
                if (r0 == 0) goto L_0x0069
                r0.close()     // Catch:{ IOException -> 0x0069 }
            L_0x0069:
                r1.disconnect()     // Catch:{ all -> 0x006d }
            L_0x006c:
                throw r3     // Catch:{ all -> 0x006d }
            L_0x006d:
                r0 = r1
                goto L_0x0070
            L_0x006f:
            L_0x0070:
                if (r0 == 0) goto L_0x007e
                java.io.InputStream r3 = r0.getInputStream()
                if (r3 == 0) goto L_0x007b
                r3.close()
            L_0x007b:
                r0.disconnect()
            L_0x007e:
                java.lang.String r3 = ""
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.Network.a(java.lang.String, boolean, java.util.Map):java.lang.String");
        }

        public static String a(String str, HttpURLConnection httpURLConnection) throws IOException, URISyntaxException {
            URI uri = new URI(str.replace(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "").replace("\r", ""));
            int responseCode = httpURLConnection.getResponseCode();
            String headerField = httpURLConnection.getHeaderField("Location");
            if (responseCode < 300 || responseCode >= 400) {
                return null;
            }
            try {
                return uri.resolve(headerField).toString();
            } catch (IllegalArgumentException unused) {
                throw new URISyntaxException(headerField, "Unable to parse invalid URL");
            }
        }
    }

    public void c(String str, String str2) {
        String[] strArr;
        if (!str.isEmpty() && !str2.isEmpty()) {
            if (!str.endsWith("/")) {
                str = str + "/";
            }
            HttpUrl parse = HttpUrl.parse(str);
            if (parse != null) {
                CookieJar cookieJar = this.b.cookieJar();
                if (str2.contains("|||")) {
                    strArr = str2.split("\\|\\|\\|");
                } else {
                    strArr = new String[]{str2};
                }
                for (String parse2 : strArr) {
                    Cookie parse3 = Cookie.parse(parse, parse2);
                    if (parse3 != null) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(parse3);
                        cookieJar.saveFromResponse(parse, arrayList);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x00a8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.OkHttpClient a() {
        /*
            r5 = this;
            okhttp3.OkHttpClient$Builder r0 = new okhttp3.OkHttpClient$Builder
            r0.<init>()
            okhttp3.Cache r1 = new okhttp3.Cache
            android.content.Context r2 = com.utils.Utils.i()
            java.io.File r2 = r2.getCacheDir()
            android.content.Context r3 = com.utils.Utils.i()
            java.io.File r3 = r3.getCacheDir()
            long r3 = com.original.tase.utils.Utils.a((java.io.File) r3)
            r1.<init>(r2, r3)
            okhttp3.OkHttpClient$Builder r0 = r0.cache(r1)
            com.original.tase.helper.http.TLSSocketFactory r1 = new com.original.tase.helper.http.TLSSocketFactory     // Catch:{ KeyManagementException -> 0x0032, NoSuchAlgorithmException -> 0x002d, KeyStoreException -> 0x0028 }
            r1.<init>()     // Catch:{ KeyManagementException -> 0x0032, NoSuchAlgorithmException -> 0x002d, KeyStoreException -> 0x0028 }
            goto L_0x0037
        L_0x0028:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x002d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x0032:
            r1 = move-exception
            r1.printStackTrace()
        L_0x0036:
            r1 = 0
        L_0x0037:
            com.original.tase.helper.http.HttpHelper$3 r2 = new com.original.tase.helper.http.HttpHelper$3
            r2.<init>(r5)
            r0.hostnameVerifier(r2)
            com.original.tase.helper.http.interceptor.ForceNoCacheSegmentInterceptor r2 = new com.original.tase.helper.http.interceptor.ForceNoCacheSegmentInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addInterceptor(r2)
            com.original.tase.helper.http.interceptor.PostRewriteResponseCodeInterceptor r2 = new com.original.tase.helper.http.interceptor.PostRewriteResponseCodeInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addNetworkInterceptor(r2)
            com.original.tase.helper.http.interceptor.HeadersInterceptor r2 = new com.original.tase.helper.http.interceptor.HeadersInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addInterceptor(r2)
            com.original.tase.helper.http.interceptor.PostRedirectInterceptor r2 = new com.original.tase.helper.http.interceptor.PostRedirectInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addInterceptor(r2)
            com.original.tase.helper.http.interceptor.CloseConnectionInterceptor r2 = new com.original.tase.helper.http.interceptor.CloseConnectionInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addInterceptor(r2)
            com.original.tase.helper.http.cloudflare.CloudflareInterceptor r2 = new com.original.tase.helper.http.cloudflare.CloudflareInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addInterceptor(r2)
            com.original.tase.helper.http.interceptor.CacheInterceptor r2 = new com.original.tase.helper.http.interceptor.CacheInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addNetworkInterceptor(r2)
            com.original.tase.helper.http.interceptor.RemoveHeadersInterceptor r2 = new com.original.tase.helper.http.interceptor.RemoveHeadersInterceptor
            r2.<init>()
            okhttp3.OkHttpClient$Builder r0 = r0.addInterceptor(r2)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.SECONDS
            r3 = 20
            okhttp3.OkHttpClient$Builder r0 = r0.connectTimeout(r3, r2)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.SECONDS
            okhttp3.OkHttpClient$Builder r0 = r0.readTimeout(r3, r2)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.SECONDS
            okhttp3.OkHttpClient$Builder r0 = r0.writeTimeout(r3, r2)
            r2 = 1
            okhttp3.OkHttpClient$Builder r0 = r0.followRedirects(r2)
            okhttp3.CookieJar r2 = r5.f5875a
            okhttp3.OkHttpClient$Builder r0 = r0.cookieJar(r2)
            if (r1 == 0) goto L_0x00af
            javax.net.ssl.X509TrustManager r2 = r1.a()
            r0.sslSocketFactory(r1, r2)
        L_0x00af:
            okhttp3.OkHttpClient$Builder r0 = a((okhttp3.OkHttpClient.Builder) r0)
            okhttp3.OkHttpClient r0 = r0.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.a():okhttp3.OkHttpClient");
    }

    public Response a(String str, boolean z, Map<String, String> map) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        Request.Builder tag = new Request.Builder().url(str).addHeader("User-Agent", Constants.f5838a).tag(d);
        if (z) {
            tag = tag.head();
        }
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                if (((String) next.getKey()).toLowerCase().equals("user-agent")) {
                    tag.header("User-Agent", (String) next.getValue());
                } else {
                    tag.addHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        return a(tag.build(), new int[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:0x010d A[Catch:{ all -> 0x0167, all -> 0x0170 }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x010e A[ADDED_TO_REGION, Catch:{ all -> 0x0167, all -> 0x0170 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Response a(okhttp3.Request r11, int... r12) {
        /*
            r10 = this;
            r0 = 0
            if (r12 == 0) goto L_0x000a
            int r1 = r12.length
            if (r1 > 0) goto L_0x0007
            goto L_0x000a
        L_0x0007:
            r12 = r12[r0]
            goto L_0x000b
        L_0x000a:
            r12 = 0
        L_0x000b:
            r1 = 1
            if (r12 <= 0) goto L_0x0010
            r2 = 1
            goto L_0x0011
        L_0x0010:
            r2 = 0
        L_0x0011:
            r3 = 0
            okhttp3.OkHttpClient r4 = r10.b     // Catch:{ all -> 0x00fd }
            okhttp3.HttpUrl r5 = r11.url()     // Catch:{ all -> 0x00fd }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00fd }
            java.lang.String r6 = "/api.thetvdb.com/"
            boolean r5 = r5.contains(r6)     // Catch:{ all -> 0x00fd }
            if (r5 == 0) goto L_0x0041
            okhttp3.OkHttpClient$Builder r4 = r4.newBuilder()     // Catch:{ all -> 0x00fd }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00fd }
            r6 = 30
            okhttp3.OkHttpClient$Builder r4 = r4.connectTimeout(r6, r5)     // Catch:{ all -> 0x00fd }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00fd }
            okhttp3.OkHttpClient$Builder r4 = r4.writeTimeout(r6, r5)     // Catch:{ all -> 0x00fd }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00fd }
            okhttp3.OkHttpClient$Builder r4 = r4.readTimeout(r6, r5)     // Catch:{ all -> 0x00fd }
            okhttp3.OkHttpClient r4 = r4.build()     // Catch:{ all -> 0x00fd }
            goto L_0x007d
        L_0x0041:
            okhttp3.HttpUrl r5 = r11.url()     // Catch:{ all -> 0x00fd }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00fd }
            java.lang.String r6 = ".gomovieshd.to/"
            boolean r5 = r5.contains(r6)     // Catch:{ all -> 0x00fd }
            if (r5 != 0) goto L_0x0061
            okhttp3.HttpUrl r5 = r11.url()     // Catch:{ all -> 0x00fd }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00fd }
            java.lang.String r6 = ".cmovieshd.com/"
            boolean r5 = r5.contains(r6)     // Catch:{ all -> 0x00fd }
            if (r5 == 0) goto L_0x007d
        L_0x0061:
            okhttp3.OkHttpClient$Builder r4 = r4.newBuilder()     // Catch:{ all -> 0x00fd }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00fd }
            r6 = 10
            okhttp3.OkHttpClient$Builder r4 = r4.connectTimeout(r6, r5)     // Catch:{ all -> 0x00fd }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00fd }
            okhttp3.OkHttpClient$Builder r4 = r4.readTimeout(r6, r5)     // Catch:{ all -> 0x00fd }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00fd }
            okhttp3.OkHttpClient$Builder r4 = r4.writeTimeout(r6, r5)     // Catch:{ all -> 0x00fd }
            okhttp3.OkHttpClient r4 = r4.build()     // Catch:{ all -> 0x00fd }
        L_0x007d:
            okhttp3.Call r4 = r4.newCall(r11)     // Catch:{ all -> 0x00fd }
            okhttp3.Response r4 = r4.execute()     // Catch:{ all -> 0x00fd }
            if (r4 != 0) goto L_0x0088
            return r3
        L_0x0088:
            int r5 = r4.code()     // Catch:{ all -> 0x00f7 }
            r6 = 429(0x1ad, float:6.01E-43)
            if (r5 != r6) goto L_0x00f6
            if (r2 != 0) goto L_0x0094
            r5 = 1
            goto L_0x0095
        L_0x0094:
            r5 = 0
        L_0x0095:
            okhttp3.HttpUrl r6 = r11.url()     // Catch:{ all -> 0x00f7 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x00f7 }
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.k(r6)     // Catch:{ all -> 0x00f7 }
            if (r7 == 0) goto L_0x00c4
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.j(r6)     // Catch:{ all -> 0x00f7 }
            if (r7 == 0) goto L_0x00ac
            r7 = 20
            goto L_0x00b5
        L_0x00ac:
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.b(r6)     // Catch:{ all -> 0x00f7 }
            if (r7 == 0) goto L_0x00b4
            r7 = 5
            goto L_0x00b5
        L_0x00b4:
            r7 = 2
        L_0x00b5:
            boolean r8 = com.original.tase.helper.GoogleVideoHelper.e(r6)     // Catch:{ all -> 0x00f7 }
            if (r8 != 0) goto L_0x00c1
            boolean r6 = com.original.tase.helper.GoogleVideoHelper.f(r6)     // Catch:{ all -> 0x00f7 }
            if (r6 == 0) goto L_0x00c4
        L_0x00c1:
            if (r12 >= r7) goto L_0x00c4
            r5 = 1
        L_0x00c4:
            if (r5 == 0) goto L_0x00f5
            okhttp3.ResponseBody r5 = r4.body()     // Catch:{ all -> 0x00f7 }
            if (r5 == 0) goto L_0x00d3
            okhttp3.ResponseBody r5 = r4.body()     // Catch:{ all -> 0x00f7 }
            r5.close()     // Catch:{ all -> 0x00f7 }
        L_0x00d3:
            int r12 = r12 + 1
            okhttp3.Request$Builder r5 = r11.newBuilder()     // Catch:{ all -> 0x00ec }
            okhttp3.CacheControl r6 = okhttp3.CacheControl.FORCE_NETWORK     // Catch:{ all -> 0x00ec }
            okhttp3.Request$Builder r5 = r5.cacheControl(r6)     // Catch:{ all -> 0x00ec }
            okhttp3.Request r5 = r5.build()     // Catch:{ all -> 0x00ec }
            int[] r6 = new int[r1]     // Catch:{ all -> 0x00ec }
            r6[r0] = r12     // Catch:{ all -> 0x00ec }
            okhttp3.Response r11 = r10.a((okhttp3.Request) r5, (int[]) r6)     // Catch:{ all -> 0x00ec }
            return r11
        L_0x00ec:
            r5 = move-exception
            boolean[] r6 = new boolean[r1]     // Catch:{ all -> 0x00f7 }
            r6[r0] = r1     // Catch:{ all -> 0x00f7 }
            com.original.tase.Logger.a((java.lang.Throwable) r5, (boolean[]) r6)     // Catch:{ all -> 0x00f7 }
            return r4
        L_0x00f5:
            return r3
        L_0x00f6:
            return r4
        L_0x00f7:
            r5 = move-exception
            r9 = r5
            r5 = r12
            r12 = r4
            r4 = r9
            goto L_0x0100
        L_0x00fd:
            r4 = move-exception
            r5 = r12
            r12 = r3
        L_0x0100:
            boolean[] r6 = new boolean[r1]     // Catch:{ all -> 0x0170 }
            boolean r7 = r4 instanceof java.lang.StackOverflowError     // Catch:{ all -> 0x0170 }
            r6[r0] = r7     // Catch:{ all -> 0x0170 }
            com.original.tase.Logger.a((java.lang.Throwable) r4, (boolean[]) r6)     // Catch:{ all -> 0x0170 }
            boolean r6 = r4 instanceof java.lang.StackOverflowError     // Catch:{ all -> 0x0170 }
            if (r6 == 0) goto L_0x010e
            return r3
        L_0x010e:
            if (r12 == 0) goto L_0x011d
            okhttp3.ResponseBody r6 = r12.body()     // Catch:{ all -> 0x0170 }
            if (r6 == 0) goto L_0x011d
            okhttp3.ResponseBody r12 = r12.body()     // Catch:{ all -> 0x0170 }
            r12.close()     // Catch:{ all -> 0x0170 }
        L_0x011d:
            if (r2 == 0) goto L_0x0120
            return r3
        L_0x0120:
            boolean r12 = r4 instanceof java.io.IOException     // Catch:{ all -> 0x0170 }
            if (r12 == 0) goto L_0x013c
            java.lang.String r12 = r4.getMessage()     // Catch:{ all -> 0x0170 }
            if (r12 == 0) goto L_0x013c
            java.lang.String r12 = r4.getMessage()     // Catch:{ all -> 0x0170 }
            java.lang.String r12 = r12.toLowerCase()     // Catch:{ all -> 0x0170 }
            java.lang.String r2 = "unexpected end of stream on"
            boolean r12 = r12.contains(r2)     // Catch:{ all -> 0x0170 }
            if (r12 == 0) goto L_0x013c
            r12 = 1
            goto L_0x013d
        L_0x013c:
            r12 = 0
        L_0x013d:
            if (r12 == 0) goto L_0x016f
            int r5 = r5 + r1
            okhttp3.Request$Builder r11 = r11.newBuilder()     // Catch:{ all -> 0x0167 }
            java.lang.String r12 = "X-Request-CC"
            java.lang.String r2 = "true"
            okhttp3.Request$Builder r11 = r11.addHeader(r12, r2)     // Catch:{ all -> 0x0167 }
            java.lang.String r12 = "Connection"
            java.lang.String r2 = "close"
            okhttp3.Request$Builder r11 = r11.addHeader(r12, r2)     // Catch:{ all -> 0x0167 }
            okhttp3.CacheControl r12 = okhttp3.CacheControl.FORCE_NETWORK     // Catch:{ all -> 0x0167 }
            okhttp3.Request$Builder r11 = r11.cacheControl(r12)     // Catch:{ all -> 0x0167 }
            okhttp3.Request r11 = r11.build()     // Catch:{ all -> 0x0167 }
            int[] r12 = new int[r1]     // Catch:{ all -> 0x0167 }
            r12[r0] = r5     // Catch:{ all -> 0x0167 }
            okhttp3.Response r11 = r10.a((okhttp3.Request) r11, (int[]) r12)     // Catch:{ all -> 0x0167 }
            return r11
        L_0x0167:
            r11 = move-exception
            boolean[] r12 = new boolean[r1]     // Catch:{ all -> 0x0170 }
            r12[r0] = r1     // Catch:{ all -> 0x0170 }
            com.original.tase.Logger.a((java.lang.Throwable) r11, (boolean[]) r12)     // Catch:{ all -> 0x0170 }
        L_0x016f:
            return r3
        L_0x0170:
            r11 = move-exception
            boolean[] r12 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r11, (boolean[]) r12)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.a(okhttp3.Request, int[]):okhttp3.Response");
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ce A[Catch:{ all -> 0x00ba, all -> 0x00dc }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(okhttp3.Request r9) {
        /*
            r8 = this;
            r0 = 0
            int[] r1 = new int[r0]
            okhttp3.Response r1 = r8.a((okhttp3.Request) r9, (int[]) r1)
            java.lang.String r2 = ""
            if (r1 != 0) goto L_0x000c
            return r2
        L_0x000c:
            okhttp3.HttpUrl r3 = r9.url()
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "yesmovie"
            boolean r3 = r3.contains(r4)
            if (r3 == 0) goto L_0x0055
            java.lang.String r3 = "set-cookie"
            java.lang.String r3 = r1.header(r3)
            if (r3 == 0) goto L_0x0055
            boolean r4 = r3.isEmpty()
            if (r4 != 0) goto L_0x0055
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            okhttp3.HttpUrl r5 = r9.url()
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r8.a((java.lang.String) r5)
            r4.append(r5)
            java.lang.String r5 = ";"
            r4.append(r5)
            okhttp3.HttpUrl r5 = r9.url()
            java.lang.String r5 = r5.toString()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r8.c((java.lang.String) r4, (java.lang.String) r3)
        L_0x0055:
            int r3 = r1.code()
            r4 = 404(0x194, float:5.66E-43)
            if (r3 == r4) goto L_0x0115
            int r3 = r1.code()
            r4 = 400(0x190, float:5.6E-43)
            if (r3 != r4) goto L_0x0067
            goto L_0x0115
        L_0x0067:
            int r3 = r1.code()
            r4 = 500(0x1f4, float:7.0E-43)
            if (r3 < r4) goto L_0x0088
            okhttp3.HttpUrl r3 = r9.url()     // Catch:{ all -> 0x0082 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0082 }
            java.lang.String r4 = "/api.thetvdb.com/"
            boolean r3 = r3.contains(r4)     // Catch:{ all -> 0x0082 }
            if (r3 == 0) goto L_0x0088
            java.lang.String r9 = "TvdbApi Error"
            return r9
        L_0x0082:
            r3 = move-exception
            boolean[] r4 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r3, (boolean[]) r4)
        L_0x0088:
            java.lang.String r3 = r9.method()     // Catch:{ all -> 0x00dc }
            java.lang.String r4 = "GET"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ all -> 0x00dc }
            if (r3 != 0) goto L_0x00a0
            java.lang.String r3 = r9.method()     // Catch:{ all -> 0x00dc }
            java.lang.String r4 = "POST"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ all -> 0x00dc }
            if (r3 == 0) goto L_0x00e2
        L_0x00a0:
            java.lang.String r3 = "Range"
            java.util.List r3 = r9.headers(r3)     // Catch:{ all -> 0x00ba }
            int r3 = r3.size()     // Catch:{ all -> 0x00ba }
            if (r3 > 0) goto L_0x00b8
            java.lang.String r3 = "range"
            java.util.List r3 = r9.headers(r3)     // Catch:{ all -> 0x00ba }
            int r3 = r3.size()     // Catch:{ all -> 0x00ba }
            if (r3 <= 0) goto L_0x00c0
        L_0x00b8:
            r3 = 1
            goto L_0x00c1
        L_0x00ba:
            r3 = move-exception
            boolean[] r4 = new boolean[r0]     // Catch:{ all -> 0x00dc }
            com.original.tase.Logger.a((java.lang.Throwable) r3, (boolean[]) r4)     // Catch:{ all -> 0x00dc }
        L_0x00c0:
            r3 = 0
        L_0x00c1:
            boolean[] r4 = new boolean[r0]     // Catch:{ all -> 0x00dc }
            long r3 = a((okhttp3.Response) r1, (boolean) r3, (boolean[]) r4)     // Catch:{ all -> 0x00dc }
            r5 = 10485760(0xa00000, double:5.180654E-317)
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 < 0) goto L_0x00e2
            okhttp3.ResponseBody r3 = r1.body()     // Catch:{ all -> 0x00dc }
            if (r3 == 0) goto L_0x00db
            okhttp3.ResponseBody r3 = r1.body()     // Catch:{ all -> 0x00dc }
            r3.close()     // Catch:{ all -> 0x00dc }
        L_0x00db:
            return r2
        L_0x00dc:
            r3 = move-exception
            boolean[] r4 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r3, (boolean[]) r4)
        L_0x00e2:
            okhttp3.ResponseBody r3 = r1.body()     // Catch:{ all -> 0x00eb }
            java.lang.String r2 = r3.string()     // Catch:{ all -> 0x00eb }
            goto L_0x00f1
        L_0x00eb:
            r3 = move-exception
            boolean[] r0 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r3, (boolean[]) r0)
        L_0x00f1:
            okhttp3.ResponseBody r0 = r1.body()
            if (r0 != 0) goto L_0x00f8
            return r2
        L_0x00f8:
            okhttp3.ResponseBody r0 = r1.body()
            r0.close()
            java.lang.String r0 = "Attention Required! | Cloudflare"
            boolean r0 = r2.contains(r0)
            if (r0 == 0) goto L_0x0114
            okhttp3.HttpUrl r9 = r9.url()
            java.lang.String r9 = r9.toString()
            java.lang.String r0 = "Need Verify Recaptcha"
            com.original.tase.Logger.a((java.lang.String) r0, (java.lang.String) r9)
        L_0x0114:
            return r2
        L_0x0115:
            okhttp3.ResponseBody r9 = r1.body()     // Catch:{ all -> 0x011e }
            java.lang.String r2 = r9.string()     // Catch:{ all -> 0x011e }
            goto L_0x0124
        L_0x011e:
            r9 = move-exception
            boolean[] r0 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r9, (boolean[]) r0)
        L_0x0124:
            okhttp3.ResponseBody r9 = r1.body()
            if (r9 == 0) goto L_0x0131
            okhttp3.ResponseBody r9 = r1.body()
            r9.close()
        L_0x0131:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.a(okhttp3.Request):java.lang.String");
    }

    @SafeVarargs
    public final String a(String str, Map<String, String>... mapArr) {
        if (str.isEmpty()) {
            return "";
        }
        Request.Builder tag = new Request.Builder().url(str).addHeader("User-Agent", Constants.f5838a).tag(d);
        if (!(mapArr == null || mapArr.length <= 0 || mapArr[0] == null)) {
            for (Map.Entry next : mapArr[0].entrySet()) {
                if (((String) next.getKey()).toLowerCase().equals("user-agent")) {
                    tag.header("User-Agent", (String) next.getValue());
                } else {
                    tag.addHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        return a(tag.build());
    }

    @SafeVarargs
    public final String a(String str, String str2, String str3, Map<String, String>... mapArr) {
        if (str.isEmpty()) {
            return "";
        }
        Request.Builder tag = new Request.Builder().url(str).addHeader("User-Agent", str2).tag(d);
        if (!(mapArr == null || mapArr.length <= 0 || mapArr[0] == null)) {
            for (Map.Entry next : mapArr[0].entrySet()) {
                if (((String) next.getKey()).toLowerCase().equals("user-agent")) {
                    tag.header("User-Agent", (String) next.getValue());
                } else {
                    tag.addHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        if (str3 != null && !str3.isEmpty()) {
            tag.addHeader("Referer", str3);
        }
        return a(tag.build());
    }

    public String a(String str, boolean z, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", str2);
        return a(str, z, (Map<String, String>[]) new Map[]{hashMap});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003d, code lost:
        if (r6.containsKey("range") != false) goto L_0x003f;
     */
    @java.lang.SafeVarargs
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r8, boolean r9, java.util.Map<java.lang.String, java.lang.String>... r10) {
        /*
            r7 = this;
            r0 = 0
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ all -> 0x0054 }
            r6.<init>()     // Catch:{ all -> 0x0054 }
            if (r10 == 0) goto L_0x0014
            int r1 = r10.length     // Catch:{ all -> 0x0054 }
            if (r1 <= 0) goto L_0x0014
            r1 = r10[r0]     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0014
            r10 = r10[r0]     // Catch:{ all -> 0x0054 }
            r6.putAll(r10)     // Catch:{ all -> 0x0054 }
        L_0x0014:
            if (r9 == 0) goto L_0x001f
            boolean r10 = com.original.tase.utils.SourceObservableUtils.a((java.lang.String) r8)     // Catch:{ all -> 0x0054 }
            if (r10 == 0) goto L_0x001d
            goto L_0x001f
        L_0x001d:
            r4 = 0
            goto L_0x0020
        L_0x001f:
            r4 = r9
        L_0x0020:
            java.lang.String r9 = "Range"
            if (r4 != 0) goto L_0x0029
            java.lang.String r10 = "bytes=0-1"
            r6.put(r9, r10)     // Catch:{ all -> 0x0054 }
        L_0x0029:
            okhttp3.Response r10 = r7.a((java.lang.String) r8, (boolean) r4, (java.util.Map<java.lang.String, java.lang.String>) r6)     // Catch:{ all -> 0x0054 }
            if (r10 != 0) goto L_0x004b
            if (r4 != 0) goto L_0x004b
            boolean r1 = r6.containsKey(r9)     // Catch:{ all -> 0x0054 }
            java.lang.String r2 = "range"
            if (r1 != 0) goto L_0x003f
            boolean r1 = r6.containsKey(r2)     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x004b
        L_0x003f:
            r6.remove(r9)     // Catch:{ all -> 0x0054 }
            r6.remove(r2)     // Catch:{ all -> 0x0054 }
            okhttp3.Response r9 = r7.a((java.lang.String) r8, (boolean) r4, (java.util.Map<java.lang.String, java.lang.String>) r6)     // Catch:{ all -> 0x0054 }
            r2 = r9
            goto L_0x004c
        L_0x004b:
            r2 = r10
        L_0x004c:
            r5 = 1
            r1 = r7
            r3 = r8
            java.lang.String r8 = r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0054 }
            goto L_0x005a
        L_0x0054:
            r9 = move-exception
            boolean[] r10 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r9, (boolean[]) r10)
        L_0x005a:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.a(java.lang.String, boolean, java.util.Map[]):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004e, code lost:
        if (a(r9, r3) == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0083, code lost:
        if (a(r9, r1) == false) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00bc, code lost:
        r8 = r8.header("location", "");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(okhttp3.Response r8, java.lang.String r9, boolean r10, boolean r11, java.util.Map<java.lang.String, java.lang.String> r12) {
        /*
            r7 = this;
            r0 = 0
            if (r8 == 0) goto L_0x000f
            okhttp3.Response r1 = r8.priorResponse()     // Catch:{ all -> 0x0008 }
            goto L_0x0010
        L_0x0008:
            r8 = move-exception
            boolean[] r10 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r8, (boolean[]) r10)
            return r9
        L_0x000f:
            r1 = 0
        L_0x0010:
            if (r8 == 0) goto L_0x001f
            okhttp3.ResponseBody r2 = r8.body()
            if (r2 == 0) goto L_0x001f
            okhttp3.ResponseBody r2 = r8.body()
            r2.close()
        L_0x001f:
            if (r1 == 0) goto L_0x002e
            okhttp3.ResponseBody r2 = r1.body()
            if (r2 == 0) goto L_0x002e
            okhttp3.ResponseBody r2 = r1.body()
            r2.close()
        L_0x002e:
            java.lang.String r2 = ""
            if (r8 == 0) goto L_0x0051
            okhttp3.ResponseBody r3 = r8.body()
            if (r3 == 0) goto L_0x0051
            okhttp3.Request r3 = r8.request()
            okhttp3.HttpUrl r3 = r3.url()
            java.lang.String r3 = r3.toString()
            boolean r4 = r3.isEmpty()
            if (r4 != 0) goto L_0x0051
            boolean r4 = r7.a((java.lang.String) r9, (java.lang.String) r3)
            if (r4 != 0) goto L_0x0051
            goto L_0x0052
        L_0x0051:
            r3 = r2
        L_0x0052:
            boolean r4 = r3.isEmpty()
            java.lang.String r5 = "location"
            if (r4 != 0) goto L_0x0060
            boolean r4 = r7.a((java.lang.String) r3, (java.lang.String) r9)
            if (r4 == 0) goto L_0x0086
        L_0x0060:
            if (r1 == 0) goto L_0x0086
            java.lang.String r4 = "Location"
            java.lang.String r4 = r8.header(r4, r2)
            if (r4 == 0) goto L_0x0073
            boolean r6 = r4.isEmpty()
            if (r6 == 0) goto L_0x0071
            goto L_0x0073
        L_0x0071:
            r1 = r4
            goto L_0x0077
        L_0x0073:
            java.lang.String r1 = r1.header(r5, r2)
        L_0x0077:
            if (r1 == 0) goto L_0x0086
            boolean r4 = r1.isEmpty()
            if (r4 != 0) goto L_0x0086
            boolean r4 = r7.a((java.lang.String) r9, (java.lang.String) r1)
            if (r4 != 0) goto L_0x0086
            goto L_0x0087
        L_0x0086:
            r1 = r3
        L_0x0087:
            if (r11 == 0) goto L_0x00b5
            boolean r11 = r1.isEmpty()
            if (r11 != 0) goto L_0x0095
            boolean r11 = r7.a((java.lang.String) r9, (java.lang.String) r1)
            if (r11 == 0) goto L_0x00b5
        L_0x0095:
            java.util.HashMap r11 = new java.util.HashMap     // Catch:{ all -> 0x00b4 }
            r11.<init>()     // Catch:{ all -> 0x00b4 }
            if (r12 == 0) goto L_0x009f
            r11.putAll(r12)     // Catch:{ all -> 0x00b4 }
        L_0x009f:
            boolean r12 = com.original.tase.utils.SourceObservableUtils.a((java.lang.String) r9)     // Catch:{ all -> 0x00b4 }
            if (r12 == 0) goto L_0x00a6
            r0 = r10
        L_0x00a6:
            if (r10 != 0) goto L_0x00af
            java.lang.String r10 = "Range"
            java.lang.String r12 = "bytes=0-1"
            r11.put(r10, r12)     // Catch:{ all -> 0x00b4 }
        L_0x00af:
            java.lang.String r1 = com.original.tase.helper.http.HttpHelper.Network.a(r9, r0, r11)     // Catch:{ all -> 0x00b4 }
            goto L_0x00b5
        L_0x00b4:
        L_0x00b5:
            if (r1 == 0) goto L_0x00ba
            r1.isEmpty()
        L_0x00ba:
            if (r8 == 0) goto L_0x00c9
            java.lang.String r8 = r8.header(r5, r2)
            if (r8 == 0) goto L_0x00c9
            boolean r9 = r8.isEmpty()
            if (r9 != 0) goto L_0x00c9
            goto L_0x00ca
        L_0x00c9:
            r8 = r1
        L_0x00ca:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.HttpHelper.a(okhttp3.Response, java.lang.String, boolean, boolean, java.util.Map):java.lang.String");
    }

    public boolean a(String str, String str2) {
        return a(HttpUrl.parse(str), HttpUrl.parse(str2));
    }

    public boolean a(HttpUrl httpUrl, HttpUrl httpUrl2) {
        boolean z;
        boolean z2;
        if (!(httpUrl == null || httpUrl2 == null)) {
            try {
                if (!(httpUrl.encodedQuery() == null && httpUrl2.encodedQuery() == null) && (httpUrl.encodedQuery() == null || httpUrl2.encodedQuery() == null || !httpUrl.encodedQuery().equals(httpUrl2.encodedQuery()))) {
                    z = false;
                } else {
                    z = true;
                }
                if (!(httpUrl.encodedFragment() == null && httpUrl2.encodedFragment() == null) && (httpUrl.encodedFragment() == null || httpUrl2.encodedFragment() == null || !httpUrl.encodedFragment().equals(httpUrl2.encodedFragment()))) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                if (!httpUrl.encodedUsername().equals(httpUrl2.encodedUsername()) || !httpUrl.encodedPassword().equals(httpUrl2.encodedPassword()) || !httpUrl.username().equals(httpUrl2.username()) || httpUrl.password() != httpUrl2.password() || !httpUrl.encodedPath().equals(httpUrl2.encodedPath()) || !z2 || !z) {
                    return false;
                }
                return true;
            } catch (Throwable th) {
                Logger.a(th, false);
            }
        }
        return false;
    }

    @SafeVarargs
    public final String a(String str, String str2, Map<String, String>... mapArr) {
        return a(str, RequestBody.create(str2, MediaType.parse("application/x-www-form-urlencoded")), true, mapArr);
    }

    @SafeVarargs
    public final String a(String str, String str2, boolean z, Map<String, String>... mapArr) {
        RequestBody requestBody;
        if (z) {
            requestBody = RequestBody.create(str2, MediaType.parse("application/x-www-form-urlencoded"));
        } else {
            if (mapArr.length > 0) {
                Map<String, String> map = mapArr[0];
                if (map.containsKey(TraktV2.HEADER_CONTENT_TYPE)) {
                    requestBody = RequestBody.create(str2, MediaType.parse(map.get(TraktV2.HEADER_CONTENT_TYPE)));
                } else if (map.containsKey("content-type")) {
                    requestBody = RequestBody.create(str2, MediaType.parse(map.get("content-type")));
                }
            }
            requestBody = null;
        }
        if (requestBody == null) {
            requestBody = RequestBody.create(str2, MediaType.parse("application/x-www-form-urlencoded"));
        }
        return a(str, requestBody, z, mapArr);
    }

    @SafeVarargs
    private final String a(String str, RequestBody requestBody, boolean z, Map<String, String>... mapArr) {
        if (str.isEmpty()) {
            return "";
        }
        Request.Builder tag = new Request.Builder().url(str).post(requestBody).addHeader("User-Agent", Constants.f5838a).tag(d);
        if (!(mapArr == null || mapArr.length <= 0 || mapArr[0] == null)) {
            for (Map.Entry next : mapArr[0].entrySet()) {
                if (((String) next.getKey()).toLowerCase().equals("user-agent")) {
                    tag.header("User-Agent", (String) next.getValue());
                } else {
                    tag.addHeader((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        if (z) {
            tag.addHeader(TraktV2.HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded; charset=UTF-8");
        }
        return a(tag.build());
    }

    public void a(Object obj) {
        for (Call next : this.b.dispatcher().queuedCalls()) {
            if (next.request().tag() != null && next.request().tag().equals(obj)) {
                next.cancel();
            }
        }
        for (Call next2 : this.b.dispatcher().runningCalls()) {
            if (next2.request().tag() != null && next2.request().tag().equals(obj)) {
                next2.cancel();
            }
        }
    }

    public String a(String str) {
        HttpUrl parse = HttpUrl.parse(str);
        StringBuilder sb = new StringBuilder();
        if (parse != null) {
            for (Cookie next : this.b.cookieJar().loadForRequest(parse)) {
                sb.append(next.name());
                sb.append("=");
                sb.append(next.value());
                sb.append(";");
            }
        }
        return sb.toString();
    }

    public static String a(String str, HashMap hashMap) {
        try {
            return Network.a(str, false, hashMap);
        } catch (IOException | URISyntaxException unused) {
            return str;
        }
    }
}
