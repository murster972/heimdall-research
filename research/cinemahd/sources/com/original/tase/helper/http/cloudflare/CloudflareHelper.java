package com.original.tase.helper.http.cloudflare;

import android.util.Log;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.squareup.duktape.DuktapeException;
import com.utils.Utils;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import java.io.IOException;
import java.net.URLEncoder;
import okhttp3.CacheControl;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.ws.WebSocketProtocol;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class CloudflareHelper {

    /* renamed from: a  reason: collision with root package name */
    private static String f5877a = Deobfuscator$app$ProductionRelease.a(175);

    static class C50351 implements GetElement {

        /* renamed from: a  reason: collision with root package name */
        String f5878a;

        public C50351(String str) {
            this.f5878a = str;
        }

        public String byID(String str) {
            Document b = Jsoup.b(this.f5878a);
            Elements g = b.g(Deobfuscator$app$ProductionRelease.a(83) + str + Deobfuscator$app$ProductionRelease.a(84));
            if (g != null) {
                return g.c();
            }
            return Deobfuscator$app$ProductionRelease.a(85);
        }
    }

    static class LOGG implements console {
        LOGG() {
        }

        public void log(String str) {
            Log.d(Deobfuscator$app$ProductionRelease.a(86), str);
        }
    }

    private static String a(String str, String str2) {
        Duktape create = Duktape.create();
        create.set(Deobfuscator$app$ProductionRelease.a(87), GetElement.class, new C50351(str2));
        create.set(Deobfuscator$app$ProductionRelease.a(88), console.class, new LOGG());
        Object evaluate = create.evaluate(str);
        if (evaluate == null) {
            return Deobfuscator$app$ProductionRelease.a(89);
        }
        return evaluate.toString();
    }

    public static Request a(Response response) throws InterruptedException, IOException, CloudflareException, DuktapeException {
        String str;
        String str2;
        Request request = response.request();
        HttpUrl url = request.url();
        String scheme = url.scheme();
        String host = url.host();
        ResponseBody body = response.body();
        String string = body != null ? body.string() : Deobfuscator$app$ProductionRelease.a(90);
        if (body != null) {
            try {
                body.close();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
        }
        if (string.contains(Deobfuscator$app$ProductionRelease.a(91))) {
            return request.newBuilder().build();
        }
        String a2 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(92), 1);
        if (a2.isEmpty()) {
            a2 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(93), 1);
        }
        String a3 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(94), 1);
        if (a3.isEmpty()) {
            a3 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(95), 1);
        }
        String a4 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(96), 1);
        if (a4.isEmpty()) {
            a3 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(97), 1);
        }
        String a5 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(98), 1);
        if (a5.isEmpty()) {
            a3 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(99), 1);
        }
        String a6 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(100), 1);
        String str3 = scheme + Deobfuscator$app$ProductionRelease.a(101) + host + Deobfuscator$app$ProductionRelease.a(102);
        if (f5877a.isEmpty()) {
            f5877a = Utils.ccc();
        }
        String replace = String.format(f5877a, new Object[]{str3, str3, a6}).replace(Deobfuscator$app$ProductionRelease.a(103), Deobfuscator$app$ProductionRelease.a(104)).replace(Deobfuscator$app$ProductionRelease.a(105), Deobfuscator$app$ProductionRelease.a(106)).replace(Deobfuscator$app$ProductionRelease.a(107), Deobfuscator$app$ProductionRelease.a(108));
        if (a2.isEmpty() || a4.isEmpty() || a6.isEmpty()) {
            throw new CloudflareException(Deobfuscator$app$ProductionRelease.a(109));
        }
        String a7 = a(replace, string);
        if (!a7.isEmpty()) {
            String a8 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(111), 1);
            if (a8.isEmpty() || !a8.toLowerCase().equals(Deobfuscator$app$ProductionRelease.a(112))) {
                if (string.contains(Deobfuscator$app$ProductionRelease.a(151))) {
                    str = scheme + Deobfuscator$app$ProductionRelease.a(152) + host + Deobfuscator$app$ProductionRelease.a(153) + a2 + Deobfuscator$app$ProductionRelease.a(154) + a7 + Deobfuscator$app$ProductionRelease.a(155) + URLEncoder.encode(a4, Deobfuscator$app$ProductionRelease.a(156)) + Deobfuscator$app$ProductionRelease.a(157) + URLEncoder.encode(a3, Deobfuscator$app$ProductionRelease.a(158));
                } else {
                    str = host;
                }
                String str4 = Constants.f5838a;
                String header = request.header(Deobfuscator$app$ProductionRelease.a(159));
                if (header == null || header.isEmpty()) {
                    header = request.header(Deobfuscator$app$ProductionRelease.a(160));
                }
                if (header != null && !header.isEmpty()) {
                    str4 = header;
                }
                String httpUrl = request.url().toString();
                Thread.sleep(5000);
                return new Request.Builder().get().url(str).cacheControl(CacheControl.FORCE_NETWORK).tag(HttpHelper.d).addHeader(Deobfuscator$app$ProductionRelease.a(161), Deobfuscator$app$ProductionRelease.a(162)).addHeader(Deobfuscator$app$ProductionRelease.a(163), Deobfuscator$app$ProductionRelease.a(164)).addHeader(Deobfuscator$app$ProductionRelease.a(165), str4).addHeader(Deobfuscator$app$ProductionRelease.a(166), host).addHeader(Deobfuscator$app$ProductionRelease.a(167), httpUrl).build();
            }
            String a9 = Regex.a(string, Deobfuscator$app$ProductionRelease.a(113), 1);
            Regex.a(string, Deobfuscator$app$ProductionRelease.a(114), 1);
            String str5 = scheme + Deobfuscator$app$ProductionRelease.a(115) + host;
            if (a9.startsWith(Deobfuscator$app$ProductionRelease.a(116))) {
                str2 = scheme + Deobfuscator$app$ProductionRelease.a(117) + host + a9.replaceAll(Deobfuscator$app$ProductionRelease.a(118), Deobfuscator$app$ProductionRelease.a(119));
            } else if (!a9.contains(scheme)) {
                str2 = str3 + a9.replaceAll(Deobfuscator$app$ProductionRelease.a(120), Deobfuscator$app$ProductionRelease.a(121));
            } else {
                str2 = host;
            }
            String.format(Deobfuscator$app$ProductionRelease.a(122), new Object[]{URLEncoder.encode(a5, Deobfuscator$app$ProductionRelease.a(123)), a2, URLEncoder.encode(a4, Deobfuscator$app$ProductionRelease.a(124)), a7});
            FormBody build = new FormBody.Builder().add(Deobfuscator$app$ProductionRelease.a(125), URLEncoder.encode(a5, Deobfuscator$app$ProductionRelease.a(WebSocketProtocol.PAYLOAD_SHORT))).add(Deobfuscator$app$ProductionRelease.a(127), a2).add(Deobfuscator$app$ProductionRelease.a(128), URLEncoder.encode(a4, Deobfuscator$app$ProductionRelease.a(129))).add(Deobfuscator$app$ProductionRelease.a(130), a7).build();
            String header2 = request.header(Deobfuscator$app$ProductionRelease.a(131));
            if (header2 == null || header2.isEmpty()) {
                header2 = request.header(Deobfuscator$app$ProductionRelease.a(132));
            }
            if (header2 != null && !header2.isEmpty()) {
                scheme = header2;
            }
            String httpUrl2 = request.url().toString();
            Thread.sleep(5000);
            return new Request.Builder().get().url(str2).post(build).cacheControl(CacheControl.FORCE_NETWORK).tag(HttpHelper.d).addHeader(Deobfuscator$app$ProductionRelease.a(133), Deobfuscator$app$ProductionRelease.a(134)).addHeader(Deobfuscator$app$ProductionRelease.a(135), Deobfuscator$app$ProductionRelease.a(136)).addHeader(Deobfuscator$app$ProductionRelease.a(137), scheme).addHeader(Deobfuscator$app$ProductionRelease.a(138), HttpHelper.e().a(httpUrl2) + Deobfuscator$app$ProductionRelease.a(139)).addHeader(Deobfuscator$app$ProductionRelease.a(140), str5).addHeader(Deobfuscator$app$ProductionRelease.a(141), Deobfuscator$app$ProductionRelease.a(142)).addHeader(Deobfuscator$app$ProductionRelease.a(143), Deobfuscator$app$ProductionRelease.a(144)).addHeader(Deobfuscator$app$ProductionRelease.a(145), Deobfuscator$app$ProductionRelease.a(146)).addHeader(Deobfuscator$app$ProductionRelease.a(147), Deobfuscator$app$ProductionRelease.a(148)).addHeader(Deobfuscator$app$ProductionRelease.a(149), host).addHeader(Deobfuscator$app$ProductionRelease.a(150), httpUrl2).build();
        }
        throw new CloudflareException(Deobfuscator$app$ProductionRelease.a(110));
    }
}
