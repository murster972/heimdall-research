package com.original.tase.helper.http.interceptor;

import java.util.concurrent.TimeUnit;
import okhttp3.CacheControl;
import okhttp3.Interceptor;

public class CacheInterceptor implements Interceptor {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f5879a = {"oauth", ".apk", "app-release", "cleafplayer", "movie_token", "update_en", "update", "isAppDead", "showMultipleInterstitials", "showMultipleInterstitialsFromOgury", "multipleAdsAllowedNetworks", "secondAdAllowedNetworks", "loadOguryMaxTrialCount", "isMuteAllowed", "isAutoLoadOguryAllowed", "isAutoCloseOguryAllowed", "isOpenloadEnabled", "latestVersionCode", "av", "frame", "iframe", "token", "ajax_new.php"};
    private static final String b = new CacheControl.Builder().maxAge(2, TimeUnit.DAYS).build().toString();
    private static final String[] c = {"zeromedia.cloud", "www1.cartoonhd.care", "cartoonhd.global", "123moviessite.com/", "www.flixanity.site", "flixanity.mobi", "cartoonhd.com", "kingmovies.is", "cmovieshd.com", "cmovieshd.net", "pmovies.to", "watchonline.pro", "watchfilm.to", "onlinemovies.tube", "afdah.tv", "afdah.to", "mvgee.com", "vumoo.com", "vumoo.li", "chillax.ws", "hollymoviehd.com", "www.hollymoviehd.com", "streamango.com", "streamcherry.com", "fruitstreams.com", "openload.co", "openload.io", "openload.tv", "openload.stream", "openload.link", "oload.tv", "oload.stream", "oload.link", "oload.xyz", "oloadcdn.net", "streamdor.co", "embed.streamdor.co", "api.streamdor.co", "real-debrid.com", "alldebrid.com", "up2stream.me", "pelispedia.vip", "pelispedia.video", "www1.pelispedia.tv", "player.pelispedia.tv", "www.pelispedia.tv", "api.pelispedia.tv", "cloud.pelispedia.tv", "cloud.pelispedia.vip", "pelispedia.tv", "fmovies.is", "fmovies.se", "tunemovie.com", "vivo.to", "html5player.to", "tunefiles.com", "amazonaws.com", "www.dizimvar1.com", "dizimvar1.com", "www.dizimvarx.com", "dizimvarx.com", "minhateca.com.br", "putstream.com", "movieocean.net", "api.movieocean.net", "terrariumtv.com", "dauth.terrariumtv.com", "putlocker.sk", "putlockertv.se", "vidlink.org", "pubfilm.is", "player.pubfilm.is", "streamcherry.xyz", "googleusercontent.com", "ip-api.com", "llnwi.net", "llnwd.net", "llnw.net", "fruity.pw", "cdn1.fruity.pw", "orange.fruity.pw", "streamy.pw", "dobby.streamy.pw", "nagini.streamy.pw", "stream.moviestime.is", "www.scnsrc.me", "gdplayer.site", "getmypopcornnow.xyz", "gphoto.stream", "hulu.so", "mycdn.me", "player.miradetodo.io", "jw.miradetodo.io", "xvidstage.com", "faststream.ws", "www.ddlvalley.me", "ddlvalley.me", "gomovieshd.to", "master.gomovieshd.to", "hevcbluray.info", "hevcbluray.net"};
    private static final String d = new CacheControl.Builder().noCache().build().toString();
    private static final String e = new CacheControl.Builder().maxAge(60, TimeUnit.MINUTES).build().toString();

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a0 A[LOOP:0: B:29:0x00a0->B:32:0x00ca, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Response intercept(okhttp3.Interceptor.Chain r18) throws java.io.IOException {
        /*
            r17 = this;
            r1 = r18
            okhttp3.Request r2 = r18.request()
            okhttp3.HttpUrl r0 = r2.url()
            java.lang.String r3 = r0.host()
            okhttp3.HttpUrl r0 = r2.url()
            java.lang.String r4 = r0.toString()
            java.lang.String r5 = "##forceNoCache##"
            boolean r6 = r4.contains(r5)
            r7 = 0
            r8 = 1
            if (r6 != 0) goto L_0x0054
            okhttp3.HttpUrl r0 = r2.url()     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x0054
            okhttp3.CacheControl r0 = r2.cacheControl()     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x0054
            okhttp3.CacheControl r0 = okhttp3.CacheControl.FORCE_NETWORK     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x0054
            okhttp3.CacheControl r0 = r2.cacheControl()     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004e }
            okhttp3.CacheControl r9 = okhttp3.CacheControl.FORCE_NETWORK     // Catch:{ all -> 0x004e }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x004e }
            boolean r0 = r0.equalsIgnoreCase(r9)     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x0054
            r6 = 1
            goto L_0x0054
        L_0x004e:
            r0 = move-exception
            boolean[] r9 = new boolean[r7]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)
        L_0x0054:
            java.lang.String r0 = " "
            java.lang.String r9 = ""
            if (r6 != 0) goto L_0x0075
            java.lang.String r10 = r2.method()
            if (r10 == 0) goto L_0x0075
            java.lang.String r10 = r2.method()
            java.lang.String r10 = r10.trim()
            java.lang.String r10 = r10.replace(r0, r9)
            java.lang.String r11 = "GET"
            boolean r10 = r10.equalsIgnoreCase(r11)
            if (r10 != 0) goto L_0x0075
            r6 = 1
        L_0x0075:
            if (r6 != 0) goto L_0x00cc
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.lang.String r11 = "Range"
            java.util.List r12 = r2.headers(r11)
            if (r12 == 0) goto L_0x008b
            java.util.List r11 = r2.headers(r11)
            r10.addAll(r11)
        L_0x008b:
            java.lang.String r11 = "range"
            java.util.List r11 = r2.headers(r11)
            if (r11 == 0) goto L_0x009c
            java.lang.String r11 = "range"
            java.util.List r11 = r2.headers(r11)
            r10.addAll(r11)
        L_0x009c:
            java.util.Iterator r10 = r10.iterator()
        L_0x00a0:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x00cc
            java.lang.Object r11 = r10.next()
            java.lang.String r11 = (java.lang.String) r11
            java.lang.String r11 = r11.trim()
            java.lang.String r11 = r11.toLowerCase()
            java.lang.String r11 = r11.replace(r0, r9)
            java.lang.String r12 = "\r"
            java.lang.String r11 = r11.replace(r12, r9)
            java.lang.String r12 = "\n"
            java.lang.String r11 = r11.replace(r12, r9)
            java.lang.String r12 = "bytes=0-1"
            boolean r11 = r11.contains(r12)
            if (r11 == 0) goto L_0x00a0
        L_0x00cc:
            if (r6 != 0) goto L_0x00db
            java.lang.String[] r0 = c
            java.util.List r0 = java.util.Arrays.asList(r0)
            boolean r0 = r0.contains(r3)
            if (r0 == 0) goto L_0x00db
            r6 = 1
        L_0x00db:
            java.lang.String r10 = "hollymoviehd"
            if (r6 != 0) goto L_0x00ea
            java.lang.String r0 = r4.toLowerCase()
            boolean r0 = r0.contains(r10)
            if (r0 == 0) goto L_0x00ea
            r6 = 1
        L_0x00ea:
            if (r6 != 0) goto L_0x0105
            java.lang.String r0 = "/cdn-cgi/l/chk_jschl"
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x0104
            java.lang.String r0 = "/cdn-cgi/l/chk_captcha"
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x0104
            java.lang.String r0 = "__cf_chl_jschl_tk"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x0105
        L_0x0104:
            r6 = 1
        L_0x0105:
            if (r6 != 0) goto L_0x0130
            java.lang.String r0 = "mehliz"
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0130
            java.lang.String r0 = "?s="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x012f
            java.lang.String r0 = "&s="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x012f
            java.lang.String r0 = "?search="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x012f
            java.lang.String r0 = "&search="
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x0130
        L_0x012f:
            r6 = 1
        L_0x0130:
            if (r6 != 0) goto L_0x016b
            java.lang.String r0 = "hevcbluray"
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x016b
            java.lang.String r0 = "?s="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x016a
            java.lang.String r0 = "&s="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x016a
            java.lang.String r0 = "?search="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x016a
            java.lang.String r0 = "&search="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x016a
            java.lang.String r0 = "?d="
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x016a
            java.lang.String r0 = "&d="
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x016b
        L_0x016a:
            r6 = 1
        L_0x016b:
            if (r6 != 0) goto L_0x0176
            java.lang.String r0 = "/suggest.php?ajax=1"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x0176
            r6 = 1
        L_0x0176:
            if (r6 != 0) goto L_0x0181
            java.lang.String r0 = "vidlink.org"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x0181
            r6 = 1
        L_0x0181:
            if (r6 != 0) goto L_0x018c
            java.lang.String r0 = "real-debrid"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x018c
            r6 = 1
        L_0x018c:
            if (r6 != 0) goto L_0x0197
            java.lang.String r0 = "alldebrid.com"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x0197
            r6 = 1
        L_0x0197:
            if (r6 != 0) goto L_0x01a2
            java.lang.String r0 = "/customsearch/"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x01a2
            r6 = 1
        L_0x01a2:
            if (r6 != 0) goto L_0x01ab
            boolean r0 = com.original.tase.helper.GoogleVideoHelper.e(r4)
            if (r0 == 0) goto L_0x01ab
            r6 = 1
        L_0x01ab:
            if (r6 != 0) goto L_0x01b6
            java.lang.String r0 = "minhateca.com.br"
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x01b6
            r6 = 1
        L_0x01b6:
            if (r6 != 0) goto L_0x01ef
            java.lang.String r0 = "google"
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x01ee
            java.lang.String r0 = "picasa"
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x01ee
            java.lang.String r0 = "blogspot"
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x01ee
            java.lang.String r0 = "youtube"
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x01ee
            java.lang.String r0 = "youtu.be"
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x01ee
            java.lang.String r0 = "googleapis"
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x01ee
            boolean r0 = com.original.tase.helper.GoogleVideoHelper.k(r4)
            if (r0 == 0) goto L_0x01ef
        L_0x01ee:
            r6 = 1
        L_0x01ef:
            if (r6 != 0) goto L_0x01fa
            java.lang.String r0 = "drive.google.com/uc?"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x01fa
            r6 = 1
        L_0x01fa:
            if (r6 != 0) goto L_0x020d
            java.lang.String r0 = "/proxy"
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x020c
            java.lang.String r0 = "proxy/"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x020d
        L_0x020c:
            r6 = 1
        L_0x020d:
            if (r6 != 0) goto L_0x0218
            java.lang.String r0 = "cloudfront"
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0218
            r6 = 1
        L_0x0218:
            if (r6 != 0) goto L_0x022b
            java.lang.String r0 = ".m4ufree."
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x022a
            java.lang.String r0 = "m4ukido."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x022b
        L_0x022a:
            r6 = 1
        L_0x022b:
            if (r6 != 0) goto L_0x0236
            java.lang.String r0 = "gdplayer."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0236
            r6 = 1
        L_0x0236:
            if (r6 != 0) goto L_0x0251
            java.lang.String r0 = "mehliz"
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0251
            java.lang.String r0 = "cdn."
            boolean r0 = r4.contains(r0)
            if (r0 != 0) goto L_0x0250
            java.lang.String r0 = ".php"
            boolean r0 = r4.contains(r0)
            if (r0 == 0) goto L_0x0251
        L_0x0250:
            r6 = 1
        L_0x0251:
            if (r6 != 0) goto L_0x025c
            java.lang.String r0 = "gomovieshd.to"
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x025c
            r6 = 1
        L_0x025c:
            if (r6 != 0) goto L_0x0267
            java.lang.String r0 = "mycdn."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0267
            r6 = 1
        L_0x0267:
            if (r6 != 0) goto L_0x0278
            java.lang.String r0 = "vidcdn."
            boolean r0 = r3.contains(r0)
            if (r0 != 0) goto L_0x0277
            boolean r0 = com.original.tase.helper.VidCDNHelper.a(r4)
            if (r0 == 0) goto L_0x0278
        L_0x0277:
            r6 = 1
        L_0x0278:
            if (r6 != 0) goto L_0x0283
            java.lang.String r0 = "ahcdn."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0283
            r6 = 1
        L_0x0283:
            if (r6 != 0) goto L_0x028e
            java.lang.String r0 = "ntcdn."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x028e
            r6 = 1
        L_0x028e:
            if (r6 != 0) goto L_0x0299
            java.lang.String r0 = "micetop."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0299
            r6 = 1
        L_0x0299:
            if (r6 != 0) goto L_0x02a4
            java.lang.String r0 = "fbcdn."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x02a4
            r6 = 1
        L_0x02a4:
            if (r6 != 0) goto L_0x02af
            java.lang.String r0 = "amazonaws."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x02af
            r6 = 1
        L_0x02af:
            if (r6 != 0) goto L_0x02ba
            java.lang.String r0 = "yandex."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x02ba
            r6 = 1
        L_0x02ba:
            if (r6 != 0) goto L_0x02c5
            java.lang.String r0 = "amazon."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x02c5
            r6 = 1
        L_0x02c5:
            if (r6 != 0) goto L_0x02d0
            java.lang.String r0 = "anyplayer."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x02d0
            r6 = 1
        L_0x02d0:
            if (r6 != 0) goto L_0x02db
            java.lang.String r0 = "fruity."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x02db
            r6 = 1
        L_0x02db:
            if (r6 != 0) goto L_0x02e6
            java.lang.String r0 = "streamy."
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x02e6
            r6 = 1
        L_0x02e6:
            if (r6 != 0) goto L_0x0302
            okhttp3.HttpUrl r0 = r2.url()
            java.lang.String r0 = r0.toString()
            java.lang.String[] r8 = f5879a
            int r11 = r8.length
            r12 = 0
        L_0x02f4:
            if (r12 >= r11) goto L_0x0302
            r13 = r8[r12]
            boolean r13 = r0.contains(r13)
            if (r13 == 0) goto L_0x02ff
            goto L_0x0302
        L_0x02ff:
            int r12 = r12 + 1
            goto L_0x02f4
        L_0x0302:
            java.lang.String r8 = "x-cache-hit"
            java.lang.String r11 = "x-cache"
            java.lang.String r12 = "c3-cache-control"
            java.lang.String r13 = "pragma"
            java.lang.String r14 = "X-Cache-Hit"
            java.lang.String r15 = "X-Cache"
            java.lang.String r7 = "C3-Cache-Control"
            r16 = r3
            java.lang.String r3 = "Pragma"
            if (r6 == 0) goto L_0x03ce
            okhttp3.Request$Builder r0 = r2.newBuilder()
            okhttp3.Request$Builder r0 = r0.removeHeader(r3)
            okhttp3.Request$Builder r0 = r0.removeHeader(r7)
            okhttp3.Request$Builder r0 = r0.removeHeader(r15)
            okhttp3.Request$Builder r0 = r0.removeHeader(r14)
            okhttp3.Request$Builder r0 = r0.removeHeader(r13)
            okhttp3.Request$Builder r0 = r0.removeHeader(r12)
            okhttp3.Request$Builder r0 = r0.removeHeader(r11)
            okhttp3.Request$Builder r0 = r0.removeHeader(r8)
            okhttp3.CacheControl r2 = okhttp3.CacheControl.FORCE_NETWORK
            okhttp3.Request$Builder r2 = r0.cacheControl(r2)
            boolean r0 = r4.contains(r5)     // Catch:{ all -> 0x0350 }
            if (r0 == 0) goto L_0x0357
            java.lang.String r0 = r4.replace(r5, r9)     // Catch:{ all -> 0x0350 }
            okhttp3.Request$Builder r0 = r2.url((java.lang.String) r0)     // Catch:{ all -> 0x0350 }
            r2 = r0
            goto L_0x0357
        L_0x0350:
            r0 = move-exception
            r5 = 0
            boolean[] r6 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r6)
        L_0x0357:
            java.lang.String r0 = r4.toLowerCase()     // Catch:{ all -> 0x036f }
            boolean r0 = r0.contains(r10)     // Catch:{ all -> 0x036f }
            if (r0 == 0) goto L_0x0376
            java.lang.String r0 = "If-Modified-Since"
            okhttp3.Request$Builder r0 = r2.removeHeader(r0)     // Catch:{ all -> 0x036f }
            java.lang.String r5 = "if-modified-since"
            okhttp3.Request$Builder r0 = r0.removeHeader(r5)     // Catch:{ all -> 0x036f }
            r2 = r0
            goto L_0x0376
        L_0x036f:
            r0 = move-exception
            r5 = 0
            boolean[] r6 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r6)
        L_0x0376:
            okhttp3.Request r0 = r2.build()
            okhttp3.Response r0 = r1.proceed(r0)
            okhttp3.Response$Builder r0 = r0.newBuilder()
            okhttp3.Response$Builder r0 = r0.removeHeader(r3)
            okhttp3.Response$Builder r0 = r0.removeHeader(r7)
            okhttp3.Response$Builder r0 = r0.removeHeader(r15)
            okhttp3.Response$Builder r0 = r0.removeHeader(r14)
            okhttp3.Response$Builder r0 = r0.removeHeader(r13)
            okhttp3.Response$Builder r0 = r0.removeHeader(r12)
            okhttp3.Response$Builder r0 = r0.removeHeader(r11)
            okhttp3.Response$Builder r0 = r0.removeHeader(r8)
            java.lang.String r1 = d
            java.lang.String r2 = "Cache-Control"
            okhttp3.Response$Builder r1 = r0.addHeader(r2, r1)
            java.lang.String r0 = r4.toLowerCase()     // Catch:{ all -> 0x03c2 }
            boolean r0 = r0.contains(r10)     // Catch:{ all -> 0x03c2 }
            if (r0 == 0) goto L_0x03c9
            java.lang.String r0 = "If-Modified-Since"
            okhttp3.Response$Builder r0 = r1.removeHeader(r0)     // Catch:{ all -> 0x03c2 }
            java.lang.String r2 = "if-modified-since"
            okhttp3.Response$Builder r0 = r0.removeHeader(r2)     // Catch:{ all -> 0x03c2 }
            r1 = r0
            goto L_0x03c9
        L_0x03c2:
            r0 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)
        L_0x03c9:
            okhttp3.Response r0 = r1.build()
            return r0
        L_0x03ce:
            java.lang.String r0 = "moviegrabber."
            r6 = r16
            boolean r0 = r6.contains(r0)
            if (r0 == 0) goto L_0x03db
            java.lang.String r0 = b
            goto L_0x03dd
        L_0x03db:
            java.lang.String r0 = e
        L_0x03dd:
            r6 = r0
            okhttp3.Request$Builder r2 = r2.newBuilder()
            boolean r0 = r4.contains(r5)     // Catch:{ all -> 0x03f2 }
            if (r0 == 0) goto L_0x03f9
            java.lang.String r0 = r4.replace(r5, r9)     // Catch:{ all -> 0x03f2 }
            okhttp3.Request$Builder r0 = r2.url((java.lang.String) r0)     // Catch:{ all -> 0x03f2 }
            r2 = r0
            goto L_0x03f9
        L_0x03f2:
            r0 = move-exception
            r4 = 0
            boolean[] r4 = new boolean[r4]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r4)
        L_0x03f9:
            okhttp3.Request r0 = r2.build()
            okhttp3.Response r0 = r1.proceed(r0)
            okhttp3.Response$Builder r1 = r0.newBuilder()
            okhttp3.Response$Builder r1 = r1.removeHeader(r3)
            okhttp3.Response$Builder r1 = r1.removeHeader(r7)
            okhttp3.Response$Builder r1 = r1.removeHeader(r15)
            okhttp3.Response$Builder r1 = r1.removeHeader(r14)
            okhttp3.Response$Builder r1 = r1.removeHeader(r13)
            okhttp3.Response$Builder r1 = r1.removeHeader(r12)
            okhttp3.Response$Builder r1 = r1.removeHeader(r11)
            okhttp3.Response$Builder r1 = r1.removeHeader(r8)
            boolean r0 = r0.isSuccessful()
            if (r0 != 0) goto L_0x042d
            java.lang.String r6 = d
        L_0x042d:
            java.lang.String r0 = "Cache-Control"
            okhttp3.Response$Builder r0 = r1.addHeader(r0, r6)
            okhttp3.Response r0 = r0.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.http.interceptor.CacheInterceptor.intercept(okhttp3.Interceptor$Chain):okhttp3.Response");
    }
}
