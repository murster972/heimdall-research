package com.original.tase.helper.http.cloudflare;

import com.original.tase.Logger;
import com.squareup.duktape.DuktapeException;
import java.io.IOException;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Response;

public class CloudflareInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response proceed = chain.proceed(chain.request());
        if (proceed.code() != 503 || proceed.header("Server") == null || !proceed.header("Server").toLowerCase().contains("cloudflare")) {
            return proceed;
        }
        try {
            return chain.proceed(CloudflareHelper.a(proceed));
        } catch (InterruptedException e) {
            Logger.a((Throwable) e, new boolean[0]);
            return chain.proceed(proceed.request().newBuilder().cacheControl(CacheControl.FORCE_NETWORK).build());
        } catch (IOException e2) {
            Logger.a((Throwable) e2, new boolean[0]);
            return chain.proceed(proceed.request().newBuilder().cacheControl(CacheControl.FORCE_NETWORK).build());
        } catch (CloudflareException e3) {
            Logger.a((Throwable) e3, new boolean[0]);
            return chain.proceed(proceed.request().newBuilder().cacheControl(CacheControl.FORCE_NETWORK).build());
        } catch (DuktapeException e4) {
            Logger.a("DuktapeException", e4.getMessage());
            return chain.proceed(proceed.request().newBuilder().cacheControl(CacheControl.FORCE_NETWORK).build());
        }
    }
}
