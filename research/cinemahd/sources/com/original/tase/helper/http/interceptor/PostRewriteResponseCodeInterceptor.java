package com.original.tase.helper.http.interceptor;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class PostRewriteResponseCodeInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        if (!request.method().equalsIgnoreCase("POST")) {
            return chain.proceed(request);
        }
        Response proceed = chain.proceed(request);
        if (proceed.code() != 301 && proceed.code() != 302) {
            return proceed;
        }
        return proceed.newBuilder().code(proceed.code() == 301 ? 308 : 307).build();
    }
}
