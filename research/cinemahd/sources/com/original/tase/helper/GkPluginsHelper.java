package com.original.tase.helper;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.original.tase.Logger;
import java.util.HashMap;
import java.util.Iterator;

public class GkPluginsHelper {

    private static class GkLink {

        /* renamed from: a  reason: collision with root package name */
        private String f5871a;
        private String b;

        private GkLink() {
        }

        public String a() {
            return this.f5871a;
        }

        public String b() {
            return this.b;
        }
    }

    public static HashMap<String, String> a(String str) {
        JsonElement a2;
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            JsonElement a3 = new JsonParser().a(str);
            if (a3 != null) {
                if (a3.l() && (a2 = a3.f().a("link")) != null) {
                    if (!a2.k()) {
                        if (a2.j()) {
                            Iterator<JsonElement> it2 = a2.e().iterator();
                            while (it2.hasNext()) {
                                GkLink gkLink = (GkLink) new Gson().a(it2.next(), GkLink.class);
                                if (gkLink.b() != null && !gkLink.b().isEmpty()) {
                                    String str2 = null;
                                    String b = gkLink.b();
                                    if (gkLink.a() != null) {
                                        if (!gkLink.a().isEmpty()) {
                                            str2 = gkLink.a().replace("P", "p");
                                        }
                                    }
                                    if (str2.isEmpty()) {
                                        if (GoogleVideoHelper.k(b)) {
                                            str2 = GoogleVideoHelper.h(b);
                                        } else {
                                            str2 = "HQ";
                                        }
                                    }
                                    hashMap.put(b, str2);
                                }
                            }
                        } else if (a2.i() != null) {
                            hashMap.put(a2.i(), "HQ");
                        }
                    }
                }
                return hashMap;
            }
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return hashMap;
        }
        return hashMap;
    }
}
