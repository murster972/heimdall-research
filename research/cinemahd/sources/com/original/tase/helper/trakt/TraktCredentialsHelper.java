package com.original.tase.helper.trakt;

import android.content.SharedPreferences;
import com.movie.FreeMoviesApp;
import com.original.tase.model.trakt.TraktCredentialsInfo;

public class TraktCredentialsHelper {
    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    public static synchronized void a(com.original.tase.model.trakt.TraktCredentialsInfo r3) {
        /*
            java.lang.Class<com.original.tase.helper.trakt.TraktCredentialsHelper> r0 = com.original.tase.helper.trakt.TraktCredentialsHelper.class
            monitor-enter(r0)
            monitor-enter(r0)     // Catch:{ all -> 0x0019 }
            java.lang.String r1 = r3.getUser()     // Catch:{ all -> 0x0016 }
            java.lang.String r2 = r3.getAccessToken()     // Catch:{ all -> 0x0016 }
            java.lang.String r3 = r3.getRefreshToken()     // Catch:{ all -> 0x0016 }
            a(r1, r2, r3)     // Catch:{ all -> 0x0016 }
            monitor-exit(r0)     // Catch:{ all -> 0x0016 }
            monitor-exit(r0)
            return
        L_0x0016:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0016 }
            throw r3     // Catch:{ all -> 0x0019 }
        L_0x0019:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.trakt.TraktCredentialsHelper.a(com.original.tase.model.trakt.TraktCredentialsInfo):void");
    }

    public static TraktCredentialsInfo b() {
        SharedPreferences l = FreeMoviesApp.l();
        TraktCredentialsInfo traktCredentialsInfo = new TraktCredentialsInfo();
        traktCredentialsInfo.setUser(l.getString("trakt_user", (String) null));
        traktCredentialsInfo.setAccessToken(l.getString("trakt_access_token", (String) null));
        traktCredentialsInfo.setRefreshToken(l.getString("trakt_refresh_token", (String) null));
        return traktCredentialsInfo;
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    public static synchronized void a(java.lang.String r3, java.lang.String r4, java.lang.String r5) {
        /*
            java.lang.Class<com.original.tase.helper.trakt.TraktCredentialsHelper> r0 = com.original.tase.helper.trakt.TraktCredentialsHelper.class
            monitor-enter(r0)
            monitor-enter(r0)     // Catch:{ all -> 0x0024 }
            android.content.SharedPreferences r1 = com.movie.FreeMoviesApp.l()     // Catch:{ all -> 0x0021 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ all -> 0x0021 }
            java.lang.String r2 = "trakt_user"
            r1.putString(r2, r3)     // Catch:{ all -> 0x0021 }
            java.lang.String r3 = "trakt_access_token"
            r1.putString(r3, r4)     // Catch:{ all -> 0x0021 }
            java.lang.String r3 = "trakt_refresh_token"
            r1.putString(r3, r5)     // Catch:{ all -> 0x0021 }
            r1.apply()     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)
            return
        L_0x0021:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            throw r3     // Catch:{ all -> 0x0024 }
        L_0x0024:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.trakt.TraktCredentialsHelper.a(java.lang.String, java.lang.String, java.lang.String):void");
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    public static synchronized void a() {
        /*
            java.lang.Class<com.original.tase.helper.trakt.TraktCredentialsHelper> r0 = com.original.tase.helper.trakt.TraktCredentialsHelper.class
            monitor-enter(r0)
            monitor-enter(r0)     // Catch:{ all -> 0x0025 }
            android.content.SharedPreferences r1 = com.movie.FreeMoviesApp.l()     // Catch:{ all -> 0x0022 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ all -> 0x0022 }
            java.lang.String r2 = "trakt_user"
            r3 = 0
            r1.putString(r2, r3)     // Catch:{ all -> 0x0022 }
            java.lang.String r2 = "trakt_access_token"
            r1.putString(r2, r3)     // Catch:{ all -> 0x0022 }
            java.lang.String r2 = "trakt_refresh_token"
            r1.putString(r2, r3)     // Catch:{ all -> 0x0022 }
            r1.apply()     // Catch:{ all -> 0x0022 }
            monitor-exit(r0)     // Catch:{ all -> 0x0022 }
            monitor-exit(r0)
            return
        L_0x0022:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0022 }
            throw r1     // Catch:{ all -> 0x0025 }
        L_0x0025:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.trakt.TraktCredentialsHelper.a():void");
    }
}
