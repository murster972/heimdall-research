package com.original.tase.helper.trakt;

import com.movie.data.api.trakt.TraktV2Cachced;
import com.original.tase.model.trakt.TraktCredentialsInfo;
import com.uwetrottmann.trakt5.TraktV2;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;

public class TraktHelper {

    /* renamed from: a  reason: collision with root package name */
    public static String f5892a = Deobfuscator$app$ProductionRelease.a(177);
    public static String b = Deobfuscator$app$ProductionRelease.a(178);
    public static String c = Deobfuscator$app$ProductionRelease.a(179);

    public static TraktV2 a() {
        TraktV2Cachced traktV2Cachced = new TraktV2Cachced(f5892a, b, c);
        TraktCredentialsInfo b2 = TraktCredentialsHelper.b();
        return b2.isValid() ? traktV2Cachced.accessToken(b2.getAccessToken()).refreshToken(b2.getRefreshToken()) : traktV2Cachced;
    }
}
