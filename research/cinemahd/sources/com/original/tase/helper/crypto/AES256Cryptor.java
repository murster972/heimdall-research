package com.original.tase.helper.crypto;

import android.util.Base64;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES256Cryptor {
    public static String a(String str, String str2) {
        try {
            byte[] bArr = new byte[32];
            byte[] bArr2 = new byte[16];
            byte[] a2 = a(8);
            a(str2.getBytes("UTF-8"), 256, 128, a2, bArr, bArr2);
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
            byte[] doFinal = instance.doFinal(str.getBytes("UTF-8"));
            byte[] bytes = "Salted__".getBytes("UTF-8");
            byte[] bArr3 = new byte[(bytes.length + a2.length + doFinal.length)];
            System.arraycopy(bytes, 0, bArr3, 0, bytes.length);
            System.arraycopy(a2, 0, bArr3, bytes.length, a2.length);
            System.arraycopy(doFinal, 0, bArr3, bytes.length + a2.length, doFinal.length);
            return new String(Base64.encode(bArr3, 0));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] a(int i) {
        byte[] bArr = new byte[i];
        new SecureRandom().nextBytes(bArr);
        return bArr;
    }

    private static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2, byte[] bArr3, byte[] bArr4) throws NoSuchAlgorithmException {
        return a(bArr, i, i2, bArr2, 1, "MD5", bArr3, bArr4);
    }

    private static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2, int i3, String str, byte[] bArr3, byte[] bArr4) throws NoSuchAlgorithmException {
        int i4 = i / 32;
        int i5 = i2 / 32;
        int i6 = i4 + i5;
        byte[] bArr5 = new byte[(i6 * 4)];
        MessageDigest instance = MessageDigest.getInstance(str);
        byte[] bArr6 = null;
        int i7 = 0;
        while (i7 < i6) {
            if (bArr6 != null) {
                instance.update(bArr6);
            }
            instance.update(bArr);
            bArr6 = instance.digest(bArr2);
            instance.reset();
            for (int i8 = 1; i8 < i3; i8++) {
                bArr6 = instance.digest(bArr6);
                instance.reset();
            }
            System.arraycopy(bArr6, 0, bArr5, i7 * 4, Math.min(bArr6.length, (i6 - i7) * 4));
            i7 += bArr6.length / 4;
        }
        int i9 = i4 * 4;
        System.arraycopy(bArr5, 0, bArr3, 0, i9);
        System.arraycopy(bArr5, i9, bArr4, 0, i5 * 4);
        return bArr5;
    }
}
