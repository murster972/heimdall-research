package com.original.tase.helper.crypto;

import com.google.ar.core.ImageMetadata;
import com.original.tase.Logger;
import com.utils.Utils;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncrypter {

    /* renamed from: a  reason: collision with root package name */
    private static String f5872a = "";
    private static byte[] b = {0, 1, 2, 3, 4, 5, 6};
    private static int c = ImageMetadata.CONTROL_AE_ANTIBANDING_MODE;
    private static int d = 256;
    public static SecretKey e = null;

    private AESEncrypter() {
    }

    public static String a(String str) {
        int length = 16 - (str.length() % 16);
        if (length <= 0 || length >= 16) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(str.length() + length);
        stringBuffer.insert(0, str);
        while (length > 0) {
            stringBuffer.append(" ");
            length--;
        }
        return stringBuffer.toString();
    }

    public static String b(String str) {
        byte[] bArr;
        try {
            String a2 = a(str);
            a();
            SecretKeySpec secretKeySpec = new SecretKeySpec(e.getEncoded(), "DES");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS5Padding");
            instance.init(1, secretKeySpec);
            bArr = instance.doFinal(a2.getBytes());
        } catch (Exception unused) {
            bArr = null;
        }
        return new String(Base64.a(bArr));
    }

    public static String c(String str) {
        byte[] bArr;
        try {
            a();
            SecretKeySpec secretKeySpec = new SecretKeySpec(e.getEncoded(), "DES");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS5Padding");
            instance.init(2, secretKeySpec);
            bArr = instance.doFinal(Base64.a(str));
        } catch (Exception unused) {
            bArr = null;
        }
        return new String(bArr);
    }

    public static SecretKey a() {
        SecretKey secretKey = e;
        if (secretKey == null || secretKey.isDestroyed()) {
            f5872a = Utils.b();
            Logger.a("AESEncrypter key", f5872a);
            try {
                e = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec(f5872a.toCharArray(), b, c, d));
            } catch (Exception unused) {
            }
        }
        return e;
    }

    public static String a(String str, String str2) {
        byte[] bArr;
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(str2.substring(0, 16).getBytes("UTF-8"), "DES");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS5Padding");
            instance.init(2, secretKeySpec);
            bArr = instance.doFinal(Base64.a(str));
        } catch (Exception unused) {
            bArr = null;
        }
        return new String(bArr);
    }
}
