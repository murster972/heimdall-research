package com.original.tase.helper.js;

import android.util.Base64;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.original.tase.Logger;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import java.util.ArrayList;
import java.util.Iterator;

public class JsUnpacker {

    private interface Unpacker {
        String unpack(String str);
    }

    public static ArrayList<String> m30916(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(m30917(str));
        arrayList.addAll(m30918(str));
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x007e A[Catch:{ all -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0080 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<java.lang.String> m30917(java.lang.String r16) {
        /*
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.String r0 = "\\}\\s*\\('(.*)',\\s*(.*?),\\s*(\\d+),\\s*'([^']+)'\\.split\\('\\|'\\)"
            r2 = 4
            r3 = r16
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b(r3, r0, r2)
            r2 = 0
            java.lang.Object r3 = r0.get(r2)
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            r4 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r4)
            java.lang.Object r6 = r0.get(r4)
            java.util.ArrayList r6 = (java.util.ArrayList) r6
            r7 = 2
            java.lang.Object r7 = r0.get(r7)
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            r8 = 3
            java.lang.Object r0 = r0.get(r8)
            r8 = r0
            java.util.ArrayList r8 = (java.util.ArrayList) r8
            r9 = 0
        L_0x0030:
            int r0 = r3.size()
            if (r9 >= r0) goto L_0x00e3
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x00d9 }
            r0.<init>()     // Catch:{ all -> 0x00d9 }
            java.lang.Object r10 = r3.get(r9)     // Catch:{ all -> 0x00d9 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ all -> 0x00d9 }
            java.lang.Object r11 = r6.get(r9)     // Catch:{ all -> 0x00d9 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x00d9 }
            java.lang.Object r12 = r7.get(r9)     // Catch:{ all -> 0x00d9 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x00d9 }
            java.lang.Object r13 = r8.get(r9)     // Catch:{ all -> 0x00d9 }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x00d9 }
            r14 = 36
            boolean r15 = com.original.tase.utils.Utils.b(r11)     // Catch:{ all -> 0x00d9 }
            if (r15 == 0) goto L_0x005f
            int r14 = java.lang.Integer.parseInt(r11)     // Catch:{ all -> 0x00d9 }
        L_0x005f:
            int r11 = java.lang.Integer.parseInt(r12)     // Catch:{ all -> 0x00d9 }
            java.lang.String r12 = "\\|"
            java.lang.String[] r12 = r13.split(r12)     // Catch:{ all -> 0x00d9 }
        L_0x0069:
            if (r11 <= 0) goto L_0x0084
            int r11 = r11 + -1
            java.lang.String r15 = m30919(r11, r14)     // Catch:{ all -> 0x00d9 }
            int r13 = r12.length     // Catch:{ all -> 0x00d9 }
            if (r13 <= r11) goto L_0x007b
            r13 = r12[r11]     // Catch:{ all -> 0x00d9 }
            if (r13 != 0) goto L_0x0079
            goto L_0x007b
        L_0x0079:
            r13 = r5
            goto L_0x007c
        L_0x007b:
            r13 = 0
        L_0x007c:
            if (r13 == 0) goto L_0x0080
            r13 = r12[r11]     // Catch:{ all -> 0x00d9 }
        L_0x0080:
            r0.put(r15, r13)     // Catch:{ all -> 0x00d9 }
            goto L_0x0069
        L_0x0084:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d9 }
            r11.<init>()     // Catch:{ all -> 0x00d9 }
            char[] r10 = r10.toCharArray()     // Catch:{ all -> 0x00d9 }
            int r12 = r10.length     // Catch:{ all -> 0x00d9 }
            r13 = 0
        L_0x008f:
            if (r13 >= r12) goto L_0x00bd
            char r14 = r10[r13]     // Catch:{ all -> 0x00d9 }
            java.lang.String r15 = java.lang.String.valueOf(r14)     // Catch:{ all -> 0x00d9 }
            java.lang.String r2 = "\\b(\\w+)\\b"
            java.lang.String r2 = com.original.tase.utils.Regex.a((java.lang.String) r15, (java.lang.String) r2, (int) r4)     // Catch:{ all -> 0x00d6 }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x00d6 }
            if (r2 != 0) goto L_0x00a5
            r2 = r5
            goto L_0x00a6
        L_0x00a5:
            r2 = 0
        L_0x00a6:
            if (r2 == 0) goto L_0x00b6
            java.lang.String r2 = java.lang.String.valueOf(r14)     // Catch:{ all -> 0x00d6 }
            java.lang.Object r2 = r0.get(r2)     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x00d6 }
            r11.append(r2)     // Catch:{ all -> 0x00d6 }
            goto L_0x00b9
        L_0x00b6:
            r11.append(r14)     // Catch:{ all -> 0x00d6 }
        L_0x00b9:
            int r13 = r13 + 1
            r2 = 0
            goto L_0x008f
        L_0x00bd:
            java.lang.String r0 = r11.toString()     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = "\\'"
            java.lang.String r10 = "'"
            java.lang.String r0 = r0.replace(r2, r10)     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = "\\\""
            java.lang.String r10 = "\""
            java.lang.String r0 = r0.replace(r2, r10)     // Catch:{ all -> 0x00d6 }
            r1.add(r0)     // Catch:{ all -> 0x00d6 }
            r2 = 0
            goto L_0x00df
        L_0x00d6:
            r0 = move-exception
            r2 = 0
            goto L_0x00da
        L_0x00d9:
            r0 = move-exception
        L_0x00da:
            boolean[] r10 = new boolean[r2]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r10)
        L_0x00df:
            int r9 = r9 + 1
            goto L_0x0030
        L_0x00e3:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.helper.js.JsUnpacker.m30917(java.lang.String):java.util.ArrayList");
    }

    public static ArrayList<String> m30918(String str) {
        String str2;
        Duktape create = Duktape.create();
        ArrayList<String> arrayList = new ArrayList<>();
        String str3 = "";
        try {
            create.evaluate("//\n// Unpacker for Dean Edward's p.a.c.k.e.r, a part of javascript beautifier\n// written by Einar Lielmanis <einar@jsbeautifier.org>\n// edited by NitroXenon for Terrarium TV\n//\n// Coincidentally, it can defeat a couple of other eval-based compressors.\n//\n// usage:\n//\n// if (P_A_C_K_E_R.detect(some_string)) {\n//     var unpacked = P_A_C_K_E_R.unpack(some_string);\n// }\n//\n//\nvar Unpacker = {\n    PATTERN: /(eval\\s*\\(\\(?function\\(.*?(,0,\\{\\}\\)\\)|split\\('\\|'\\)\\)\\)))/g,\n\n    unpack: function(html) {\n        var results = [];\n        var matches = getMatches(html, Unpacker.PATTERN, 1)\n        for (var i = 0; i < matches.length; i++) {\n            try {\n               var match = matches[i];\n               results.push(P_A_C_K_E_R.unpack(match));\n            } catch (lErr) {}\n        }\n\n        return Duktape.enc('base64', JSON.stringify(results));\n    },\n}\n\nvar P_A_C_K_E_R = {\n    PATTERN: /eval\\s*\\(\\(?function\\(.*?(,0,\\{\\}\\)\\)|split\\('\\|'\\)\\)\\))/g,\n\n    detect: function(str) {\n        return (P_A_C_K_E_R.get_chunks(str).length > 0);\n    },\n\n    get_chunks: function(str) {\n        var chunks = str.match(P_A_C_K_E_R.PATTERN);\n        return chunks ? chunks : [];\n    },\n\n    unpack: function(str) {\n        var chunks = P_A_C_K_E_R.get_chunks(str),\n            chunk;\n        for (var i = 0; i < chunks.length; i++) {\n            chunk = chunks[i].replace(/\\n$/, '');\n            str = str.split(chunk).join(P_A_C_K_E_R.unpack_chunk(chunk));\n        }\n        return str;\n    },\n\n    unpack_chunk: function(str) {\n        var unpacked_source = '';\n        var __eval = eval;\n        if (P_A_C_K_E_R.detect(str)) {\n            try {\n                eval = function(s) { // jshint ignore:line\n                    unpacked_source += s;\n                    return unpacked_source;\n                }; // jshint ignore:line\n                __eval(str);\n                if (typeof unpacked_source === 'string' && unpacked_source) {\n                    str = unpacked_source;\n                }\n            } catch (e) {\n                //console.log(e.toString());\n            }\n        }\n        eval = __eval; // jshint ignore:line\n        return str;\n    },\n};\n\nfunction getMatches(string, regex, index) {\n    index || (index = 1); // default to the first capturing group\n    var matches = [];\n    var match;\n    while (match = regex.exec(string)) {\n        matches.push(match[index]);\n    }\n    return matches;\n}");
            str3 = ((Unpacker) create.get("Unpacker", Unpacker.class)).unpack(str);
            str2 = new String(Base64.decode(str3, 0), "UTF-8");
        } catch (Throwable th) {
            create.close();
            throw th;
        }
        create.close();
        if (str2.isEmpty()) {
            return arrayList;
        }
        try {
            Iterator<JsonElement> it2 = new JsonParser().a(str2).e().iterator();
            while (it2.hasNext()) {
                arrayList.add(it2.next().i());
            }
        } catch (Throwable th2) {
            Logger.a(th2, new boolean[0]);
        }
        return arrayList;
    }

    private static String m30919(int i, int i2) throws Exception {
        String substring = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".substring(0, i2);
        if (substring.length() > i2) {
            throw new Exception("base " + i2 + " exceeds table length " + substring.length());
        } else if (i == 0) {
            return Character.toString(substring.charAt(0));
        } else {
            StringBuilder sb = new StringBuilder();
            while (i > 0) {
                sb.insert(0, Character.toString(substring.charAt(i % i2)));
                i /= i2;
            }
            return sb.toString();
        }
    }

    public static boolean m30920(String str) {
        return !Regex.a(str, "(eval\\s*\\(\\(?function\\(.*?(,0,\\{\\}\\)\\)|split\\('\\|'\\)\\)\\)))", 1, true).isEmpty();
    }
}
