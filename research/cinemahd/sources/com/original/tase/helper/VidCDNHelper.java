package com.original.tase.helper;

import com.original.tase.utils.Regex;

public class VidCDNHelper {
    public static boolean a(String str) {
        return str != null && !str.isEmpty() && !Regex.a(str, "//([^/]*vidcdn\\.pro)/", 1, 34).isEmpty();
    }
}
