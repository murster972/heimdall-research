package com.original.tase;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBus {
    private static volatile RxBus b;

    /* renamed from: a  reason: collision with root package name */
    private PublishSubject<Object> f5839a = PublishSubject.b();

    public static RxBus b() {
        RxBus rxBus = b;
        if (rxBus == null) {
            synchronized (RxBus.class) {
                rxBus = b;
                if (rxBus == null) {
                    rxBus = new RxBus();
                    b = rxBus;
                }
            }
        }
        return rxBus;
    }

    public void a(Object obj) {
        this.f5839a.onNext(obj);
    }

    public Observable<Object> a() {
        return this.f5839a;
    }
}
