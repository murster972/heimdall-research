package com.original.tase;

import com.movie.FreeMoviesApp;
import com.utils.Utils;

public class I18N {
    public static String a(int i) {
        return FreeMoviesApp.a(Utils.i()).getResources().getString(i);
    }

    public static String a(int i, Object... objArr) {
        return FreeMoviesApp.a(Utils.i()).getResources().getString(i, objArr);
    }
}
