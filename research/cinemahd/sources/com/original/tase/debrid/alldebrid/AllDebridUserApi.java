package com.original.tase.debrid.alldebrid;

import com.original.Constants;
import com.utils.Utils;
import java.util.HashMap;

public class AllDebridUserApi {
    private static volatile AllDebridUserApi b;

    /* renamed from: a  reason: collision with root package name */
    private boolean f5865a = false;

    public static HashMap<String, String> c() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("User-Agent", Constants.f5838a);
        return hashMap;
    }

    public static AllDebridUserApi d() {
        if (b == null) {
            synchronized (AllDebridUserApi.class) {
                if (b == null) {
                    b = new AllDebridUserApi();
                    b.a();
                }
            }
        }
        return b;
    }

    public void a() {
        this.f5865a = Utils.a(Utils.RDTYPE.ALL_DEBRID);
    }

    public boolean b() {
        return this.f5865a;
    }
}
