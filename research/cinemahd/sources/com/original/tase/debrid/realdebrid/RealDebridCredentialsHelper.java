package com.original.tase.debrid.realdebrid;

import android.content.SharedPreferences;
import com.movie.FreeMoviesApp;
import com.original.tase.Logger;
import com.original.tase.model.debrid.realdebrid.RealDebridCredentialsInfo;
import com.utils.Utils;

public class RealDebridCredentialsHelper {
    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    public static synchronized void a(com.original.tase.model.debrid.realdebrid.RealDebridCredentialsInfo r4) {
        /*
            java.lang.Class<com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper> r0 = com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.class
            monitor-enter(r0)
            monitor-enter(r0)     // Catch:{ all -> 0x001d }
            java.lang.String r1 = r4.getAccessToken()     // Catch:{ all -> 0x001a }
            java.lang.String r2 = r4.getRefreshToken()     // Catch:{ all -> 0x001a }
            java.lang.String r3 = r4.getClientId()     // Catch:{ all -> 0x001a }
            java.lang.String r4 = r4.getClientSecret()     // Catch:{ all -> 0x001a }
            a(r1, r2, r3, r4)     // Catch:{ all -> 0x001a }
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            monitor-exit(r0)
            return
        L_0x001a:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            throw r4     // Catch:{ all -> 0x001d }
        L_0x001d:
            r4 = move-exception
            monitor-exit(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.a(com.original.tase.model.debrid.realdebrid.RealDebridCredentialsInfo):void");
    }

    public static boolean b() {
        return !Utils.a(Utils.RDTYPE.REAL_DEBRID);
    }

    public static synchronized RealDebridCredentialsInfo c() {
        RealDebridCredentialsInfo realDebridCredentialsInfo;
        synchronized (RealDebridCredentialsHelper.class) {
            realDebridCredentialsInfo = new RealDebridCredentialsInfo();
            try {
                SharedPreferences l = FreeMoviesApp.l();
                realDebridCredentialsInfo.setAccessToken(l.getString("real_debrid_access_token", (String) null));
                realDebridCredentialsInfo.setRefreshToken(l.getString("real_debrid_refresh_token", (String) null));
                realDebridCredentialsInfo.setClientId(l.getString("real_debrid_last_clientID", (String) null));
                realDebridCredentialsInfo.setClientSecret(l.getString("real_debrid_client_secret", (String) null));
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
        }
        return realDebridCredentialsInfo;
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    public static synchronized void a(java.lang.String r3, java.lang.String r4, java.lang.String r5, java.lang.String r6) {
        /*
            java.lang.Class<com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper> r0 = com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.class
            monitor-enter(r0)
            monitor-enter(r0)     // Catch:{ all -> 0x0031 }
            android.content.SharedPreferences r1 = com.movie.FreeMoviesApp.l()     // Catch:{ all -> 0x0024 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ all -> 0x0024 }
            java.lang.String r2 = "real_debrid_access_token"
            r1.putString(r2, r3)     // Catch:{ all -> 0x0024 }
            java.lang.String r3 = "real_debrid_refresh_token"
            r1.putString(r3, r4)     // Catch:{ all -> 0x0024 }
            java.lang.String r3 = "real_debrid_last_clientID"
            r1.putString(r3, r5)     // Catch:{ all -> 0x0024 }
            java.lang.String r3 = "real_debrid_client_secret"
            r1.putString(r3, r6)     // Catch:{ all -> 0x0024 }
            r1.apply()     // Catch:{ all -> 0x0024 }
            goto L_0x002b
        L_0x0024:
            r3 = move-exception
            r4 = 0
            boolean[] r4 = new boolean[r4]     // Catch:{ all -> 0x002e }
            com.original.tase.Logger.a((java.lang.Throwable) r3, (boolean[]) r4)     // Catch:{ all -> 0x002e }
        L_0x002b:
            monitor-exit(r0)     // Catch:{ all -> 0x002e }
            monitor-exit(r0)
            return
        L_0x002e:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002e }
            throw r3     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    public static synchronized void a() {
        /*
            java.lang.Class<com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper> r0 = com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.class
            monitor-enter(r0)
            monitor-enter(r0)     // Catch:{ all -> 0x0032 }
            android.content.SharedPreferences r1 = com.movie.FreeMoviesApp.l()     // Catch:{ all -> 0x0025 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = "real_debrid_access_token"
            r3 = 0
            r1.putString(r2, r3)     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = "real_debrid_refresh_token"
            r1.putString(r2, r3)     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = "real_debrid_client_secret"
            r1.putString(r2, r3)     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = "real_debrid_last_clientID"
            r1.putString(r2, r3)     // Catch:{ all -> 0x0025 }
            r1.apply()     // Catch:{ all -> 0x0025 }
            goto L_0x002c
        L_0x0025:
            r1 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]     // Catch:{ all -> 0x002f }
            com.original.tase.Logger.a((java.lang.Throwable) r1, (boolean[]) r2)     // Catch:{ all -> 0x002f }
        L_0x002c:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            monitor-exit(r0)
            return
        L_0x002f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            throw r1     // Catch:{ all -> 0x0032 }
        L_0x0032:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.a():void");
    }
}
