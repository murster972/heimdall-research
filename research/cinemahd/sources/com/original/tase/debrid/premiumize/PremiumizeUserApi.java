package com.original.tase.debrid.premiumize;

import com.utils.Utils;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class PremiumizeUserApi {
    private static volatile PremiumizeUserApi b;

    /* renamed from: a  reason: collision with root package name */
    private boolean f5866a = false;

    public static PremiumizeUserApi c() {
        if (b == null) {
            synchronized (PremiumizeUserApi.class) {
                if (b == null) {
                    b = new PremiumizeUserApi();
                    b.a();
                }
            }
        }
        return b;
    }

    public void a() {
        this.f5866a = Utils.a(Utils.RDTYPE.PREMIUMIZE);
    }

    public boolean b() {
        return this.f5866a;
    }

    public Observable<Boolean> a(String str) {
        return Observable.create(new ObservableOnSubscribe<Boolean>(this) {
            public void subscribe(ObservableEmitter<Boolean> observableEmitter) throws Exception {
                observableEmitter.onNext(true);
                observableEmitter.onComplete();
            }
        });
    }
}
