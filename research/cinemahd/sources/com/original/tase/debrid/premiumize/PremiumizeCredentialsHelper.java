package com.original.tase.debrid.premiumize;

import android.content.SharedPreferences;
import com.movie.FreeMoviesApp;
import com.original.tase.Logger;
import com.original.tase.model.debrid.premiumize.PremiumizeCredentialsInfo;

public class PremiumizeCredentialsHelper {
    public static synchronized void a(PremiumizeCredentialsInfo premiumizeCredentialsInfo) {
        synchronized (PremiumizeCredentialsHelper.class) {
            synchronized (PremiumizeCredentialsInfo.class) {
                a(premiumizeCredentialsInfo.getAccessToken(), premiumizeCredentialsInfo.getPremium_until());
            }
        }
    }

    public static PremiumizeCredentialsInfo b() {
        PremiumizeCredentialsInfo premiumizeCredentialsInfo = new PremiumizeCredentialsInfo();
        try {
            SharedPreferences l = FreeMoviesApp.l();
            premiumizeCredentialsInfo.setAccessToken(l.getString("premiumize_apikey", (String) null));
            premiumizeCredentialsInfo.setPremium_until(l.getLong("premiumize_premium_until2", 0));
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
        }
        return premiumizeCredentialsInfo;
    }

    public static synchronized void a(String str, long j) {
        synchronized (PremiumizeCredentialsHelper.class) {
            synchronized (PremiumizeCredentialsInfo.class) {
                try {
                    SharedPreferences.Editor edit = FreeMoviesApp.l().edit();
                    edit.putString("premiumize_apikey", str);
                    edit.putLong("premiumize_premium_until2", j);
                    edit.apply();
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                }
            }
        }
    }

    public static synchronized void a() {
        synchronized (PremiumizeCredentialsHelper.class) {
            synchronized (PremiumizeCredentialsInfo.class) {
                try {
                    SharedPreferences.Editor edit = FreeMoviesApp.l().edit();
                    edit.putString("premiumize_apikey", (String) null);
                    edit.putString("premiumize_premium_until2", (String) null);
                    edit.apply();
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                }
            }
        }
    }
}
