package com.original.tase.model.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.format.Formatter;
import com.movie.FreeMoviesApp;
import com.movie.data.model.cinema.SyncLink;
import com.movie.data.model.realdebrid.MagnetObject;
import com.movie.ui.activity.SettingsActivity;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.BaseResolver;
import com.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;

public class MediaSource implements Parcelable, Comparable {
    public static final Parcelable.Creator<MediaSource> CREATOR = new Parcelable.Creator<MediaSource>() {
        public MediaSource createFromParcel(Parcel parcel) {
            return new MediaSource(parcel);
        }

        public MediaSource[] newArray(int i) {
            return new MediaSource[i];
        }
    };
    private static HashMap<Integer, String> streamTypeHashMap;
    private boolean alldebrid;
    private boolean debrid;
    private long duration = 0;
    private String extension;
    private String externalName;
    private long fileSize;
    private String filename;
    private Boolean hls;
    private String hostName;
    private boolean isCachedLink = false;
    private boolean isPlayed = false;
    private boolean isRawTorrent = false;
    private boolean isResolved = false;
    private boolean isTorrent = false;
    private ArrayList<MagnetObject> magnetObjects;
    private String movieName;
    private int nLeek = 0;
    private int nSeek = 0;
    private boolean needToSync = true;
    private String originalLink;
    private HashMap<String, String> playHeader;
    private boolean premiumize;
    private String providerName;
    private String quality;
    private boolean realdebrid;
    private long requestTime;
    private String streamLink;
    private String torrentFileID = "";

    protected MediaSource(Parcel parcel) {
        Boolean bool;
        boolean z = false;
        this.premiumize = parcel.readByte() != 0;
        this.realdebrid = parcel.readByte() != 0;
        this.alldebrid = parcel.readByte() != 0;
        this.fileSize = parcel.readLong();
        byte readByte = parcel.readByte();
        if (readByte == 0) {
            bool = null;
        } else {
            bool = Boolean.valueOf(readByte == 1);
        }
        this.hls = bool;
        this.hostName = parcel.readString();
        this.providerName = parcel.readString();
        this.quality = parcel.readString();
        this.streamLink = parcel.readString();
        this.movieName = parcel.readString();
        this.filename = parcel.readString();
        this.extension = parcel.readString();
        this.playHeader = (HashMap) parcel.readSerializable();
        setOriginalLink(parcel.readString());
        this.needToSync = parcel.readByte() != 0;
        this.duration = parcel.readLong();
        this.isTorrent = parcel.readByte() != 0;
        this.isRawTorrent = parcel.readByte() != 0;
        this.nSeek = parcel.readInt();
        this.nLeek = parcel.readInt();
        this.isCachedLink = parcel.readByte() != 0;
        this.isResolved = parcel.readByte() != 0 ? true : z;
        this.torrentFileID = parcel.readString();
        this.externalName = parcel.readString();
        this.magnetObjects = parcel.readArrayList(MagnetObject.class.getClassLoader());
    }

    public MediaSource cloneDeeply() {
        Parcel obtain = Parcel.obtain();
        obtain.writeValue(this);
        obtain.setDataPosition(0);
        return (MediaSource) obtain.readValue(MediaSource.class.getClassLoader());
    }

    public int compareTo(Object obj) {
        MediaSource mediaSource = (MediaSource) obj;
        if (getPriority() > mediaSource.getPriority()) {
            return -1;
        }
        if (getPriority() < mediaSource.getPriority()) {
            return 1;
        }
        if (getFileSize() > mediaSource.getFileSize()) {
            return -1;
        }
        if (getFileSize() < mediaSource.getFileSize()) {
            return 1;
        }
        return 0;
    }

    public SyncLink.Link convertToSynLink() {
        SyncLink.Link link = new SyncLink.Link();
        int i = 1;
        if (!this.isTorrent || this.magnetObjects.size() <= 0) {
            link.l = getOriginalLink();
        } else {
            link.l = Regex.a(getMagnetObjects().get(0).getMagnet(), "(magnet:\\?xt=urn:btih:[^&.]+)", 1).toLowerCase();
        }
        link.d = String.valueOf(getDuration());
        if (!isRealdebrid()) {
            i = isPremiumize() ? 2 : isAlldebrid() ? 4 : 0;
        }
        link.p = String.valueOf(i);
        link.t = String.valueOf(getStreamType());
        link.z = String.valueOf(this.fileSize);
        link.q = getQuality();
        link.h = getProviderName().replace(" ", "_");
        return link;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || MediaSource.class != obj.getClass()) {
            return false;
        }
        MediaSource mediaSource = (MediaSource) obj;
        String str = this.streamLink;
        if (str != null ? !str.equals(mediaSource.streamLink) : mediaSource.streamLink != null) {
            return false;
        }
        HashMap<String, String> hashMap = this.playHeader;
        if (hashMap != null) {
            return hashMap.equals(mediaSource.playHeader);
        }
        if (mediaSource.playHeader == null) {
            return true;
        }
        return false;
    }

    public long getDuration() {
        return this.duration;
    }

    public String getExtension() {
        return this.extension;
    }

    public long getFileSize() {
        return this.fileSize;
    }

    public String getFileSizeString() {
        if (this.fileSize < 0) {
            return "";
        }
        return "[" + Formatter.formatFileSize(Utils.i(), this.fileSize) + "]";
    }

    public String getFilename() {
        return this.filename;
    }

    public Boolean getHLSBase() {
        return this.hls;
    }

    public String getHostName() {
        return this.hostName;
    }

    public ArrayList<MagnetObject> getMagnetObjects() {
        return this.magnetObjects;
    }

    public String getMovieName() {
        return this.movieName;
    }

    public String getOriginalLink() {
        return this.originalLink;
    }

    public HashMap<String, String> getPlayHeader() {
        return this.playHeader;
    }

    public int getPriority() {
        if (SettingsActivity.h == null) {
            SettingsActivity.h = Boolean.valueOf(FreeMoviesApp.l().getBoolean("pref_show_sort_link_by_size2", true));
        }
        if (SettingsActivity.i == null) {
            SettingsActivity.i = Boolean.valueOf(FreeMoviesApp.l().getBoolean("pref_show_sort_link_by_quality", true));
        }
        if (SettingsActivity.i.booleanValue()) {
            return getQualityPriority();
        }
        if (SettingsActivity.h.booleanValue()) {
            return 0;
        }
        if (SettingsActivity.g.isEmpty()) {
            SettingsActivity.g = FreeMoviesApp.l().getString("pref_choose_host_priority3", BaseResolver.c());
        }
        String str = this.hostName;
        if (isRealdebrid()) {
            str = "RealDebird";
        }
        if (isAlldebrid()) {
            str = "AllDebird";
        }
        if (isPremiumize()) {
            str = "PREMIUMIZE";
        }
        int indexOf = SettingsActivity.g.indexOf(str);
        return indexOf != -1 ? (SettingsActivity.g.length() - indexOf) + getQualityPriority() : indexOf;
    }

    public String getProviderName() {
        return this.providerName;
    }

    public String getQuality() {
        return this.quality;
    }

    public int getQualityPriority() {
        if (this.quality.contains("4K") || this.quality.contains("2K") || this.quality.contains("1440")) {
            return 10;
        }
        if (this.quality.contains("1080")) {
            return 9;
        }
        if (this.quality.contains("720")) {
            return 8;
        }
        if (this.quality.contains("HD")) {
            return 7;
        }
        if (this.quality.contains("480")) {
            return 6;
        }
        return this.quality.contains("HQ") ? 5 : 0;
    }

    public long getRequestTime() {
        return this.requestTime;
    }

    public String getStreamLink() {
        return this.streamLink;
    }

    public long getStreamType() {
        if (isDebrid()) {
            return 8;
        }
        if (isHLS()) {
            return 16;
        }
        if (this.originalLink.contains("drive.google")) {
            return 2;
        }
        if (GoogleVideoHelper.k(this.originalLink)) {
            return 4;
        }
        if (this.originalLink.contains("magnet:") || this.originalLink.contains(".torrent")) {
            return 34359738368L;
        }
        if (this.originalLink.contains("openload") || this.originalLink.contains("oload.")) {
            return 32;
        }
        if (this.originalLink.contains("vidlink")) {
            return 2199023255552L;
        }
        if (this.originalLink.contains("downace")) {
            return PlaybackStateCompat.ACTION_SET_REPEAT_MODE;
        }
        if (this.originalLink.contains("userscloud")) {
            return 68719476736L;
        }
        if (this.originalLink.contains("uptobox") || this.originalLink.contains("uptostream")) {
            return 512;
        }
        if (this.originalLink.contains("rapidvideo") || this.originalLink.contains("raptu")) {
            return 128;
        }
        if (this.originalLink.contains("streamango") || this.originalLink.contains("streamcherry")) {
            return 64;
        }
        if (this.originalLink.contains("vidto.me")) {
            return 70368744177664L;
        }
        if (this.originalLink.contains("vidlox")) {
            return 4398046511104L;
        }
        if (this.originalLink.contains("vidtodo") || this.originalLink.contains("vidstodo")) {
            return 140737488355328L;
        }
        if (this.originalLink.contains("vodlock")) {
            return 4503599627370496L;
        }
        if (this.originalLink.contains("powvideo")) {
            return 4294967296L;
        }
        if (this.originalLink.contains("estream")) {
            return PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE_ENABLED;
        }
        if (this.originalLink.contains("daclips")) {
            return PlaybackStateCompat.ACTION_PREPARE_FROM_URI;
        }
        if (this.originalLink.contains("movpod")) {
            return 134217728;
        }
        if (this.originalLink.contains("thevideo.me")) {
            return 17179869184L;
        }
        if (this.originalLink.contains("vidzi")) {
            return 2251799813685248L;
        }
        if (this.originalLink.contains("vidoza")) {
            return 17592186044416L;
        }
        if (this.originalLink.contains("them4ufree")) {
            return 8589934592L;
        }
        if (this.originalLink.contains("vidup.me") || this.originalLink.contains("vidup.io") || this.originalLink.contains("vidup.tv")) {
            return 1125899906842624L;
        }
        if (this.originalLink.contains("ok.ru")) {
            return 562949953421312L;
        }
        if (this.originalLink.contains("vidstreaming") || this.originalLink.contains("vidcloud.icu")) {
            return 35184372088832L;
        }
        if (this.originalLink.contains("vidcloud") || this.originalLink.contains("loadvid")) {
            return 1024;
        }
        if (this.originalLink.contains("vcstream")) {
            return 274877906944L;
        }
        if (this.originalLink.contains("gorillavid")) {
            return PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE;
        }
        if (this.originalLink.contains("yourupload")) {
            return 36028797018963968L;
        }
        if (this.originalLink.contains("entervideo")) {
            return PlaybackStateCompat.ACTION_PLAY_FROM_URI;
        }
        if (this.originalLink.contains("mp4upload")) {
            return 268435456;
        }
        if (this.originalLink.contains("fastplay.")) {
            return PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
        }
        if (this.originalLink.contains("vshare.eu")) {
            return 9007199254740992L;
        }
        if (this.originalLink.contains("thevideobee.to")) {
            return PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH;
        }
        if (this.originalLink.contains("novamov.com") || this.originalLink.contains("auroravid.to")) {
            return 1073741824;
        }
        if (this.originalLink.contains("nowvideo.sx")) {
            return 2147483648L;
        }
        if (this.originalLink.contains("putload.tv")) {
            return 256;
        }
        if (this.originalLink.contains("cloudvideo")) {
            return PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH;
        }
        if (this.originalLink.contains("vidmoly")) {
            return 8796093022208L;
        }
        if (this.originalLink.contains("gounlimited")) {
            return 4194304;
        }
        if (this.originalLink.contains("fembed")) {
            return PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM;
        }
        if (this.originalLink.contains("jawcloud")) {
            return 33554432;
        }
        if (this.originalLink.contains("watchvideo")) {
            return 16384;
        }
        if (this.originalLink.contains("clipwatching")) {
            return PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID;
        }
        if (this.originalLink.contains("idtbox")) {
            return 16777216;
        }
        if (this.originalLink.contains("nofile")) {
            return 536870912;
        }
        if (this.originalLink.contains("xstreamcdn")) {
            return 18014398509481984L;
        }
        if (this.originalLink.contains("viduplayer")) {
            return 281474976710656L;
        }
        if (this.originalLink.contains("streamx.live")) {
            return 8388608;
        }
        if (this.originalLink.contains("vcdn.io")) {
            return 137438953472L;
        }
        if (this.originalLink.contains("jetload")) {
            return 67108864;
        }
        return this.originalLink.contains("verystream") ? 549755813888L : 144115188075855872L;
    }

    public String getStringToBeCompared() {
        String str;
        String str2 = getQuality().trim().toLowerCase().replace("4k", "2160p").replace("2K", "1440p").replace("quadhd", "1440p").replace("hd", "720p").replace("sd", "480p").replace("hq", "360p") + " [" + this.fileSize + "]";
        if (GoogleVideoHelper.k(getStreamLink())) {
            str = str2 + " [AAA]";
        } else if (isDebrid()) {
            str = str2 + " [BBB]";
        } else {
            str = str2 + " [XXX]";
        }
        String str3 = str + " - " + this.providerName + " [" + this.hostName + "]";
        if (!isHLS()) {
            return str3;
        }
        return str3 + " [HLS]";
    }

    public String getTorrentFileID() {
        return this.torrentFileID;
    }

    public int hashCode() {
        String str = this.streamLink;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        HashMap<String, String> hashMap = this.playHeader;
        if (hashMap != null) {
            i = hashMap.hashCode();
        }
        return hashCode + i;
    }

    public boolean isAlldebrid() {
        return this.alldebrid;
    }

    public boolean isCachedLink() {
        return this.isCachedLink;
    }

    public boolean isDebrid() {
        return isAlldebrid() || isRealdebrid() || isPremiumize();
    }

    public boolean isHD() {
        return this.quality.contains("HD") || this.quality.contains("1080") || this.quality.contains("720") || this.quality.contains("4K") || this.quality.contains("2K") || this.quality.contains("1440");
    }

    public boolean isHLS() {
        Boolean bool = this.hls;
        return (bool != null && bool.booleanValue()) || (this.hls == null && this.streamLink.trim().toLowerCase().contains(".m3u8"));
    }

    public boolean isNeedToSync() {
        return this.needToSync;
    }

    public boolean isPlayed() {
        return this.isPlayed;
    }

    public boolean isPremiumize() {
        return this.premiumize;
    }

    public boolean isRawTorrent() {
        return this.isRawTorrent;
    }

    public boolean isRealdebrid() {
        return this.realdebrid;
    }

    public boolean isResolved() {
        return this.isResolved;
    }

    public boolean isTorrent() {
        return this.isTorrent;
    }

    public void setAlldebrid(boolean z) {
        this.alldebrid = z;
    }

    public void setCachedLink(boolean z) {
        this.isCachedLink = z;
    }

    public void setDebrid(boolean z) {
        this.debrid = z;
    }

    public void setDuration(long j) {
        this.duration = j;
    }

    public void setExtension(String str) {
        this.extension = str;
    }

    public void setFileSize(long j) {
        this.fileSize = j;
    }

    public void setFilename(String str) {
        this.filename = str;
    }

    public void setHLS(boolean z) {
        this.hls = Boolean.valueOf(z);
    }

    public void setHostName(String str) {
        this.hostName = str;
    }

    public void setMagnetObjects(ArrayList<MagnetObject> arrayList) {
        this.magnetObjects = arrayList;
    }

    public void setMovieName(String str) {
        this.movieName = str;
    }

    public void setNeedToSync(boolean z) {
        this.needToSync = z;
    }

    public void setOriginalLink(String str) {
        if (str != null && str.endsWith(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE)) {
            str = str.replace(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "");
        }
        this.originalLink = str;
    }

    public void setPlayHeader(HashMap<String, String> hashMap) {
        this.playHeader = hashMap;
    }

    public void setPlayed(boolean z) {
        this.isPlayed = z;
    }

    public void setPremiumize(boolean z) {
        this.premiumize = z;
    }

    public void setProviderName(String str) {
        this.providerName = str;
    }

    public void setQuality(int i) {
        setQuality(String.valueOf(i));
    }

    public void setRawTorrent(boolean z) {
        this.isRawTorrent = z;
    }

    public void setRealdebrid(boolean z) {
        this.realdebrid = z;
    }

    public void setRequestTime(long j) {
        this.requestTime = j;
    }

    public void setResolved(boolean z) {
        this.isResolved = z;
    }

    public void setStreamLink(String str) {
        if (this.streamLink == null && this.originalLink == null) {
            this.originalLink = str;
        }
        this.streamLink = str;
    }

    public void setTorrent(boolean z) {
        this.isTorrent = z;
    }

    public void setTorrentFileID(String str) {
        this.torrentFileID = str;
    }

    public String toString() {
        String str = this.quality + " - [" + this.hostName + "] ";
        if (isHLS()) {
            str = str + " [HLS] ";
        }
        String str2 = str + getFileSizeString();
        return (str2.length() <= 0 || !str2.endsWith(" ")) ? str2 : str2.substring(0, str2.length() - 1);
    }

    public String toString2() {
        String str;
        String str2 = this.quality + " - " + this.providerName + " [" + this.hostName + "] ";
        String str3 = this.externalName;
        if (str3 != null && !str3.isEmpty()) {
            str2 = this.quality + " - " + this.externalName;
        }
        if (FreeMoviesApp.l().getBoolean("pref_show_file_name_if_available", false) && (str = this.filename) != null && !str.isEmpty()) {
            str2 = this.quality + " - " + this.filename;
        }
        if (isHLS()) {
            str2 = str2 + "[HLS] ";
        }
        if (this.realdebrid) {
            str2 = str2 + "[DEB] ";
        }
        if (this.alldebrid) {
            str2 = str2 + "[ALL-DEB] ";
        }
        if (this.premiumize) {
            str2 = str2 + "[PREMIUMIZE] ";
        }
        if (this.isRawTorrent) {
            str2 = str2 + "[S/L:" + this.nSeek + "/" + this.nLeek + "][Need torrent player] ";
        }
        String str4 = str2 + getFileSizeString();
        return (str4.length() <= 0 || !str4.endsWith(" ")) ? str4 : str4.substring(0, str4.length() - 1);
    }

    public String toStringAllObjs() {
        return "MediaSource { providerName='" + this.providerName + '\'' + ", hostName='" + this.hostName + '\'' + ", originalLink=" + this.originalLink + '\'' + ", requestTime=" + this.requestTime + '\'' + ", quality='" + this.quality + '\'' + ", streamLink='" + this.streamLink + '\'' + ", playHeader=" + this.playHeader + '\'' + ", fileSize=" + this.fileSize + '\'' + ", alldebrid=" + this.alldebrid + '\'' + ", premiumize=" + this.premiumize + '\'' + ", realdebrid=" + this.realdebrid + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.premiumize ? (byte) 1 : 0);
        parcel.writeByte(this.realdebrid ? (byte) 1 : 0);
        parcel.writeByte(this.alldebrid ? (byte) 1 : 0);
        parcel.writeLong(this.fileSize);
        Boolean bool = this.hls;
        parcel.writeByte((byte) (bool == null ? 0 : bool.booleanValue() ? 1 : 2));
        parcel.writeString(this.hostName);
        parcel.writeString(this.providerName);
        parcel.writeString(this.quality);
        parcel.writeString(this.streamLink);
        parcel.writeString(this.movieName);
        parcel.writeString(this.filename);
        parcel.writeString(this.extension);
        parcel.writeSerializable(this.playHeader);
        parcel.writeString(getOriginalLink());
        parcel.writeByte(this.needToSync ? (byte) 1 : 0);
        parcel.writeLong(this.duration);
        parcel.writeByte(this.isTorrent ? (byte) 1 : 0);
        parcel.writeByte(this.isRawTorrent ? (byte) 1 : 0);
        parcel.writeInt(this.nSeek);
        parcel.writeInt(this.nLeek);
        parcel.writeByte(this.isCachedLink ? (byte) 1 : 0);
        parcel.writeByte(this.isResolved ? (byte) 1 : 0);
        parcel.writeString(this.torrentFileID);
        parcel.writeString(this.externalName);
        parcel.writeList(this.magnetObjects);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008a, code lost:
        if (r9 != 47689303) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0093, code lost:
        if (r0.equals("1440p") != false) goto L_0x00a9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setQuality(java.lang.String r9) {
        /*
            r8 = this;
            java.lang.String r0 = r9.trim()
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r1 = "p"
            boolean r2 = r0.endsWith(r1)
            java.lang.String r3 = "4K"
            java.lang.String r4 = "2K"
            r5 = -1
            r6 = 2
            r7 = 1
            if (r2 != 0) goto L_0x0067
            boolean r2 = com.original.tase.utils.Utils.b(r0)
            if (r2 == 0) goto L_0x0067
            int r9 = r0.hashCode()
            r2 = 1511391(0x170fdf, float:2.11791E-39)
            if (r9 == r2) goto L_0x0045
            r2 = 1513189(0x1716e5, float:2.12043E-39)
            if (r9 == r2) goto L_0x003b
            r2 = 1538361(0x177939, float:2.155703E-39)
            if (r9 == r2) goto L_0x0031
            goto L_0x004b
        L_0x0031:
            java.lang.String r9 = "2160"
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x004b
            r5 = 2
            goto L_0x004b
        L_0x003b:
            java.lang.String r9 = "1600"
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x004b
            r5 = 1
            goto L_0x004b
        L_0x0045:
            java.lang.String r9 = "1440"
            boolean r9 = r0.equals(r9)
        L_0x004b:
            if (r5 == r7) goto L_0x0064
            if (r5 == r6) goto L_0x0061
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r0)
            r9.append(r1)
            java.lang.String r9 = r9.toString()
            r8.quality = r9
            return
        L_0x0061:
            r8.quality = r3
            return
        L_0x0064:
            r8.quality = r4
            return
        L_0x0067:
            boolean r2 = r0.endsWith(r1)
            if (r2 == 0) goto L_0x00b6
            java.lang.String r2 = ""
            java.lang.String r1 = r0.replace(r1, r2)
            boolean r1 = com.original.tase.utils.Utils.b(r1)
            if (r1 == 0) goto L_0x00b6
            int r9 = r0.hashCode()
            r1 = 46853233(0x2caec71, float:2.9816943E-37)
            if (r9 == r1) goto L_0x008d
            r1 = 46908971(0x2cbc62b, float:2.9941912E-37)
            if (r9 == r1) goto L_0x0096
            r1 = 47689303(0x2d7ae57, float:3.1691477E-37)
            if (r9 == r1) goto L_0x00a0
            goto L_0x00a9
        L_0x008d:
            java.lang.String r9 = "1440p"
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x0096
            goto L_0x00a9
        L_0x0096:
            java.lang.String r9 = "1600p"
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x00a0
            r5 = 1
            goto L_0x00a9
        L_0x00a0:
            java.lang.String r9 = "2160p"
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x00a9
            r5 = 2
        L_0x00a9:
            if (r5 == r7) goto L_0x00b3
            if (r5 == r6) goto L_0x00b0
            r8.quality = r0
            return
        L_0x00b0:
            r8.quality = r3
            return
        L_0x00b3:
            r8.quality = r4
            return
        L_0x00b6:
            java.lang.String r1 = "quadhd"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00c2
            r8.quality = r4
            goto L_0x0144
        L_0x00c2:
            java.lang.String r1 = "4k"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x013e
            java.lang.String r1 = "2k"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x013e
            java.lang.String r1 = "hd"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x013e
            java.lang.String r1 = "hq"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x013e
            java.lang.String r1 = "sd"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00eb
            goto L_0x013e
        L_0x00eb:
            java.lang.String r1 = "cam"
            boolean r1 = r0.contains(r1)
            if (r1 != 0) goto L_0x0139
            java.lang.String r1 = "ts"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x00fc
            goto L_0x0139
        L_0x00fc:
            java.lang.String r1 = "vod"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0134
            java.lang.String r1 = "dvd"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x010d
            goto L_0x0134
        L_0x010d:
            java.lang.String r1 = "7"
            boolean r1 = r0.startsWith(r1)
            if (r1 != 0) goto L_0x011d
            java.lang.String r1 = "6"
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x011f
        L_0x011d:
            java.lang.String r9 = "720p"
        L_0x011f:
            java.lang.String r1 = "4"
            boolean r1 = r0.startsWith(r1)
            if (r1 != 0) goto L_0x012f
            java.lang.String r1 = "5"
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x0131
        L_0x012f:
            java.lang.String r9 = "480p"
        L_0x0131:
            r8.quality = r9
            goto L_0x0144
        L_0x0134:
            java.lang.String r9 = "HQ"
            r8.quality = r9
            goto L_0x0144
        L_0x0139:
            java.lang.String r9 = "CAM"
            r8.quality = r9
            goto L_0x0144
        L_0x013e:
            java.lang.String r9 = r0.toUpperCase()
            r8.quality = r9
        L_0x0144:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.original.tase.model.media.MediaSource.setQuality(java.lang.String):void");
    }

    public MediaSource(MediaSource mediaSource) {
        this.providerName = mediaSource.getProviderName();
        this.hostName = mediaSource.getHostName();
        this.quality = mediaSource.getQuality();
        this.streamLink = mediaSource.getStreamLink();
        this.playHeader = mediaSource.getPlayHeader();
        this.fileSize = mediaSource.getFileSize();
        this.hls = mediaSource.getHLSBase();
        this.realdebrid = mediaSource.isRealdebrid();
        this.alldebrid = mediaSource.isAlldebrid();
        this.premiumize = mediaSource.isPremiumize();
        this.movieName = mediaSource.movieName;
        this.filename = mediaSource.filename;
        this.extension = mediaSource.extension;
        setOriginalLink(mediaSource.originalLink);
        this.needToSync = mediaSource.needToSync;
        this.duration = mediaSource.duration;
        this.isTorrent = mediaSource.isTorrent;
        this.isRawTorrent = mediaSource.isRawTorrent;
        this.nSeek = mediaSource.nSeek;
        this.nLeek = mediaSource.nLeek;
        this.isCachedLink = mediaSource.isCachedLink;
        this.isResolved = mediaSource.isResolved;
        this.torrentFileID = mediaSource.torrentFileID;
        this.externalName = mediaSource.externalName;
        this.magnetObjects = mediaSource.magnetObjects;
    }

    public MediaSource(String str, String str2, boolean z) {
        this.providerName = str;
        this.hostName = str2;
        this.fileSize = -1;
        this.movieName = "";
        this.extension = "";
        this.hls = null;
        this.debrid = false;
        this.alldebrid = false;
        this.premiumize = false;
        if (this.providerName.isEmpty() || this.hostName.isEmpty()) {
            this.quality = "HQ";
        }
    }
}
