package com.original.tase.model;

import java.util.HashMap;

public class ResolveResult {
    private boolean alldebrid;
    private boolean debrid;
    private long filesize;
    private HashMap<String, String> playHeader;
    private boolean premiumize;
    private boolean realdebrid;
    private String resolvedLink;
    private String resolvedQuality;
    private String resolverFileName;
    private String resolverName;

    public ResolveResult(String str, String str2, String str3) {
        this.resolverName = str;
        this.resolvedLink = str2;
        this.resolvedQuality = str3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ResolveResult.class != obj.getClass()) {
            return false;
        }
        ResolveResult resolveResult = (ResolveResult) obj;
        if (isDebrid() != resolveResult.isDebrid()) {
            return false;
        }
        String str = this.resolverName;
        if (str != null) {
            if (!str.equals(resolveResult.resolverName)) {
                return false;
            }
        } else if (resolveResult.resolverName != null) {
            return false;
        }
        String str2 = this.resolvedLink;
        if (str2 != null) {
            if (!str2.equals(resolveResult.resolvedLink)) {
                return false;
            }
        } else if (resolveResult.resolvedLink != null) {
            return false;
        }
        String str3 = this.resolvedQuality;
        if (str3 != null) {
            if (!str3.equals(resolveResult.resolvedQuality)) {
                return false;
            }
        } else if (resolveResult.resolvedQuality != null) {
            return false;
        }
        HashMap<String, String> hashMap = this.playHeader;
        if (hashMap != null) {
            return hashMap.equals(resolveResult.playHeader);
        }
        if (resolveResult.playHeader != null) {
            return false;
        }
        return true;
    }

    public long getFilesize() {
        return this.filesize;
    }

    public HashMap<String, String> getPlayHeader() {
        return this.playHeader;
    }

    public String getResolvedLink() {
        return this.resolvedLink;
    }

    public String getResolvedQuality() {
        return this.resolvedQuality;
    }

    public String getResolverFileName() {
        return this.resolverFileName;
    }

    public String getResolverName() {
        return this.resolverName;
    }

    public int hashCode() {
        String str = this.resolverName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.resolvedLink;
        int hashCode2 = ((str2 != null ? str2.hashCode() : 0) + hashCode) * 31;
        String str3 = this.resolvedQuality;
        int hashCode3 = ((str3 != null ? str3.hashCode() : 0) + hashCode2) * 31;
        HashMap<String, String> hashMap = this.playHeader;
        if (hashMap != null) {
            i = hashMap.hashCode();
        }
        return ((i + hashCode3) * 31) + (isDebrid() ? 1 : 0);
    }

    public boolean isAlldebrid() {
        return this.alldebrid;
    }

    public boolean isDebrid() {
        this.debrid = this.alldebrid || this.realdebrid || this.premiumize;
        return this.debrid;
    }

    public boolean isPremiumize() {
        return this.premiumize;
    }

    public boolean isRealdebrid() {
        return this.realdebrid;
    }

    public void setAlldebrid(boolean z) {
        this.alldebrid = z;
    }

    public void setFilesize(long j) {
        this.filesize = j;
    }

    public void setPlayHeader(HashMap<String, String> hashMap) {
        this.playHeader = hashMap;
    }

    public void setPremiumize(boolean z) {
        this.premiumize = z;
    }

    public void setRealdebrid(boolean z) {
        this.realdebrid = z;
    }

    public ResolveResult setResolvedLink(String str) {
        this.resolvedLink = str;
        return this;
    }

    public void setResolvedQuality(String str) {
        this.resolvedQuality = str;
    }

    public void setResolverFileName(String str) {
        this.resolverFileName = str;
    }

    public ResolveResult setResolverName(String str) {
        this.resolverName = str;
        return this;
    }

    public String toString() {
        return "ResolveResult{resolverName='" + this.resolverName + '\'' + ", resolvedLink='" + this.resolvedLink + '\'' + ", resolvedQuality='" + this.resolvedQuality + '\'' + ", playHeader=" + this.playHeader + ", debrid=" + this.debrid + '}';
    }

    public ResolveResult(String str, String str2, int i) {
        this(str, str2, String.valueOf(i));
    }
}
