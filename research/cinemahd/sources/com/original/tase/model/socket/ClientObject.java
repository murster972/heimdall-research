package com.original.tase.model.socket;

import com.google.gson.Gson;

public class ClientObject {
    private boolean ishsl;
    private String originalLink;
    private String player;
    private String streamlink;
    private double timeplay;
    private String title;
    private boolean useheader;
    private String useragent;

    public ClientObject(String str, String str2, boolean z, String str3, double d, String str4, String str5, boolean z2) {
        this.player = str;
        this.streamlink = str2;
        this.ishsl = z;
        this.title = str3;
        this.timeplay = d;
        this.originalLink = str4;
        this.useragent = str5;
        this.useheader = z2;
    }

    public String getFilename() {
        return this.title;
    }

    public String getOriginalLink() {
        return this.originalLink;
    }

    public String getPlayer() {
        return this.player;
    }

    public String getStreamlink() {
        return this.streamlink;
    }

    public double getTimeplay() {
        return this.timeplay;
    }

    public String getTitle() {
        return this.title;
    }

    public String getuseragent() {
        return this.useragent;
    }

    public boolean isIsHSL() {
        return this.ishsl;
    }

    public boolean isUseheader() {
        return this.useheader;
    }

    public boolean ishsl() {
        return this.ishsl;
    }

    public void setFilename(String str) {
        this.title = str;
    }

    public void setHSL(boolean z) {
        this.ishsl = z;
    }

    public void setIsHSL(boolean z) {
        this.ishsl = z;
    }

    public void setOriginalLink(String str) {
        this.originalLink = str;
    }

    public void setPlayer(String str) {
        this.player = str;
    }

    public void setStreamlink(String str) {
        this.streamlink = str;
    }

    public void setTimeplay(double d) {
        this.timeplay = d;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setUseheader(boolean z) {
        this.useheader = z;
    }

    public void setuseragent(String str) {
        this.useragent = str;
    }

    public String toString() {
        return new Gson().a((Object) this);
    }
}
