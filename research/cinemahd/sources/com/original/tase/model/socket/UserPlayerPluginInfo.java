package com.original.tase.model.socket;

public class UserPlayerPluginInfo {
    public String IP;
    public boolean iConnect;
    public String serverIP;
    public String userName;

    public UserPlayerPluginInfo(String str, String str2, boolean z, String str3) {
        this.IP = str;
        this.iConnect = z;
        this.serverIP = str2;
        this.userName = str3;
    }

    public UserPlayerPluginInfo(String str, boolean z) {
        this.IP = str;
        this.iConnect = z;
        this.userName = "";
    }
}
