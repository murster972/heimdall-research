package com.original.tase.model.socket;

import com.google.gson.Gson;

public class UserResponces {
    public static final int USER_RESPONCE_FAIL = 404;
    public static final int USER_RESPONCE_SUCCSES = 200;
    public int code;
    public String messge;

    public UserResponces(int i, String str) {
        this.code = i;
        this.messge = str;
    }

    public String toJson(UserResponces userResponces) {
        return new Gson().a((Object) userResponces);
    }

    public UserResponces toObject(String str) {
        return (UserResponces) new Gson().a(str, UserResponces.class);
    }

    public String toJson() {
        return new Gson().a((Object) this);
    }
}
