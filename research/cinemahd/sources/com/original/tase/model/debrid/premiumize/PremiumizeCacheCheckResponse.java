package com.original.tase.model.debrid.premiumize;

import java.util.ArrayList;
import java.util.List;

public class PremiumizeCacheCheckResponse {
    private List<CacheCheckResponseSingle> consolidatedResponse;
    public String[] filename;
    public String[] filesize;
    public String[] response;
    public String status;
    public String[] transcoded;

    public class CacheCheckResponseSingle {
        public String filename;
        public Long filesize;
        public boolean response;
        public String transcoded;

        public CacheCheckResponseSingle(PremiumizeCacheCheckResponse premiumizeCacheCheckResponse, int i) {
            String[] strArr = premiumizeCacheCheckResponse.response;
            this.response = strArr != null && strArr.length > i && "true".equals(strArr[i]);
            String[] strArr2 = premiumizeCacheCheckResponse.transcoded;
            String str = null;
            this.transcoded = (strArr2 == null || strArr2.length <= i) ? null : strArr2[i];
            String[] strArr3 = premiumizeCacheCheckResponse.filename;
            if (strArr3 != null && strArr3.length > i) {
                str = strArr3[i];
            }
            this.filename = str;
            String[] strArr4 = premiumizeCacheCheckResponse.filesize;
            if (strArr4 != null && strArr4.length > i) {
                try {
                    this.filesize = Long.valueOf(Long.parseLong(strArr4[i]));
                } catch (Exception unused) {
                }
            }
        }
    }

    public List<CacheCheckResponseSingle> consolidate() {
        String[] strArr;
        if (!"success".equals(this.status) || (strArr = this.response) == null || strArr.length <= 0) {
            return null;
        }
        if (this.consolidatedResponse == null) {
            this.consolidatedResponse = new ArrayList();
            for (int i = 0; i < this.response.length; i++) {
                this.consolidatedResponse.add(new CacheCheckResponseSingle(this, i));
            }
        }
        return this.consolidatedResponse;
    }

    public CacheCheckResponseSingle getFirst() {
        if (consolidate() == null || consolidate().size() <= 0) {
            return null;
        }
        return consolidate().get(0);
    }
}
