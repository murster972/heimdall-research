package com.original.tase.model.debrid.premiumize;

public class PremiumizeCreateFolderPesponse {
    private String id;
    private String status;

    public String getId() {
        return this.id;
    }

    public String getStatus() {
        return this.status;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setStatus(String str) {
        this.status = str;
    }
}
