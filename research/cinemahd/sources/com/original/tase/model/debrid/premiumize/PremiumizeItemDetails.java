package com.original.tase.model.debrid.premiumize;

public class PremiumizeItemDetails {
    private String acodec;
    private int created_at;
    private String folder_id;
    private String id;
    private String ip;
    private String link;
    private String mime_type;
    private String name;
    private String opensubtitles_hash;
    private int size;
    private String stream_link;
    private String transcode_status;
    private String type;
    private String vcodec;

    public String getAcodec() {
        return this.acodec;
    }

    public int getCreated_at() {
        return this.created_at;
    }

    public String getFolder_id() {
        return this.folder_id;
    }

    public String getId() {
        return this.id;
    }

    public String getIp() {
        return this.ip;
    }

    public String getLink() {
        return this.link;
    }

    public String getMime_type() {
        return this.mime_type;
    }

    public String getName() {
        return this.name;
    }

    public String getOpensubtitles_hash() {
        return this.opensubtitles_hash;
    }

    public int getSize() {
        return this.size;
    }

    public String getStream_link() {
        return this.stream_link;
    }

    public String getTranscode_status() {
        return this.transcode_status;
    }

    public String getType() {
        return this.type;
    }

    public String getVcodec() {
        return this.vcodec;
    }

    public void setAcodec(String str) {
        this.acodec = str;
    }

    public void setCreated_at(int i) {
        this.created_at = i;
    }

    public void setFolder_id(String str) {
        this.folder_id = str;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setIp(String str) {
        this.ip = str;
    }

    public void setLink(String str) {
        this.link = str;
    }

    public void setMime_type(String str) {
        this.mime_type = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setOpensubtitles_hash(String str) {
        this.opensubtitles_hash = str;
    }

    public void setSize(int i) {
        this.size = i;
    }

    public void setStream_link(String str) {
        this.stream_link = str;
    }

    public void setTranscode_status(String str) {
        this.transcode_status = str;
    }

    public void setType(String str) {
        this.type = str;
    }

    public void setVcodec(String str) {
        this.vcodec = str;
    }
}
