package com.original.tase.exception;

public class FailedToSyncTraktCollectionsException extends RuntimeException {
    private int mMediaType;
    private int mTransferType;

    public String toString() {
        return "FailedToSyncTraktCollectionsException{mTransferType=" + this.mTransferType + ", mMediaType=" + this.mMediaType + '}';
    }
}
