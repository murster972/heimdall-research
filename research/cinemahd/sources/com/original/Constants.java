package com.original;

import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import java.util.HashMap;

public class Constants {

    /* renamed from: a  reason: collision with root package name */
    public static String f5838a = Deobfuscator$app$ProductionRelease.a(82);

    static {
        Deobfuscator$app$ProductionRelease.a(52);
        Deobfuscator$app$ProductionRelease.a(53);
        Deobfuscator$app$ProductionRelease.a(54);
        Deobfuscator$app$ProductionRelease.a(55);
        Deobfuscator$app$ProductionRelease.a(56);
        Deobfuscator$app$ProductionRelease.a(57);
        Deobfuscator$app$ProductionRelease.a(58);
        Deobfuscator$app$ProductionRelease.a(59);
        Deobfuscator$app$ProductionRelease.a(60);
        Deobfuscator$app$ProductionRelease.a(61);
        Deobfuscator$app$ProductionRelease.a(62);
        Deobfuscator$app$ProductionRelease.a(63);
        Deobfuscator$app$ProductionRelease.a(64);
        Deobfuscator$app$ProductionRelease.a(65);
        Deobfuscator$app$ProductionRelease.a(66);
        Deobfuscator$app$ProductionRelease.a(67);
        Deobfuscator$app$ProductionRelease.a(68);
        Deobfuscator$app$ProductionRelease.a(69);
        Deobfuscator$app$ProductionRelease.a(70);
        Deobfuscator$app$ProductionRelease.a(71);
        Deobfuscator$app$ProductionRelease.a(72);
        Deobfuscator$app$ProductionRelease.a(73);
        Deobfuscator$app$ProductionRelease.a(74);
        Deobfuscator$app$ProductionRelease.a(75);
        Deobfuscator$app$ProductionRelease.a(76);
        Deobfuscator$app$ProductionRelease.a(77);
        Deobfuscator$app$ProductionRelease.a(78);
        Deobfuscator$app$ProductionRelease.a(79);
        Deobfuscator$app$ProductionRelease.a(80);
        Deobfuscator$app$ProductionRelease.a(81);
    }

    public static HashMap<String, String> a() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Deobfuscator$app$ProductionRelease.a(50), Deobfuscator$app$ProductionRelease.a(51));
        return hashMap;
    }
}
