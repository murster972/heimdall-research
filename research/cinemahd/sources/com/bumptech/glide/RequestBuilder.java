package com.bumptech.glide;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.ErrorRequestCoordinator;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.RequestCoordinator;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.SingleRequest;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.bumptech.glide.util.Executors;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public class RequestBuilder<TranscodeType> extends BaseRequestOptions<RequestBuilder<TranscodeType>> implements Cloneable, ModelTypes<RequestBuilder<TranscodeType>> {
    private final Context A;
    private final RequestManager B;
    private final Class<TranscodeType> C;
    private final GlideContext D;
    private TransitionOptions<?, ? super TranscodeType> E;
    private Object F;
    private List<RequestListener<TranscodeType>> G;
    private RequestBuilder<TranscodeType> H;
    private RequestBuilder<TranscodeType> I;
    private Float J;
    private boolean K = true;
    private boolean L;
    private boolean M;

    /* renamed from: com.bumptech.glide.RequestBuilder$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2113a = new int[ImageView.ScaleType.values().length];
        static final /* synthetic */ int[] b = new int[Priority.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        static {
            /*
                com.bumptech.glide.Priority[] r0 = com.bumptech.glide.Priority.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.bumptech.glide.Priority r2 = com.bumptech.glide.Priority.LOW     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                com.bumptech.glide.Priority r3 = com.bumptech.glide.Priority.NORMAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x002a }
                com.bumptech.glide.Priority r4 = com.bumptech.glide.Priority.HIGH     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = b     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.bumptech.glide.Priority r5 = com.bumptech.glide.Priority.IMMEDIATE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                android.widget.ImageView$ScaleType[] r4 = android.widget.ImageView.ScaleType.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                f2113a = r4
                int[] r4 = f2113a     // Catch:{ NoSuchFieldError -> 0x0048 }
                android.widget.ImageView$ScaleType r5 = android.widget.ImageView.ScaleType.CENTER_CROP     // Catch:{ NoSuchFieldError -> 0x0048 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
            L_0x0048:
                int[] r0 = f2113a     // Catch:{ NoSuchFieldError -> 0x0052 }
                android.widget.ImageView$ScaleType r4 = android.widget.ImageView.ScaleType.CENTER_INSIDE     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r0 = f2113a     // Catch:{ NoSuchFieldError -> 0x005c }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_CENTER     // Catch:{ NoSuchFieldError -> 0x005c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
            L_0x005c:
                int[] r0 = f2113a     // Catch:{ NoSuchFieldError -> 0x0066 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_START     // Catch:{ NoSuchFieldError -> 0x0066 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
            L_0x0066:
                int[] r0 = f2113a     // Catch:{ NoSuchFieldError -> 0x0071 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_END     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = f2113a     // Catch:{ NoSuchFieldError -> 0x007c }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_XY     // Catch:{ NoSuchFieldError -> 0x007c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007c }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007c }
            L_0x007c:
                int[] r0 = f2113a     // Catch:{ NoSuchFieldError -> 0x0087 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.CENTER     // Catch:{ NoSuchFieldError -> 0x0087 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0087 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0087 }
            L_0x0087:
                int[] r0 = f2113a     // Catch:{ NoSuchFieldError -> 0x0093 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.MATRIX     // Catch:{ NoSuchFieldError -> 0x0093 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0093 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0093 }
            L_0x0093:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.RequestBuilder.AnonymousClass1.<clinit>():void");
        }
    }

    static {
        RequestOptions requestOptions = (RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().a(DiskCacheStrategy.b)).a(Priority.LOW)).a(true);
    }

    @SuppressLint({"CheckResult"})
    protected RequestBuilder(Glide glide, RequestManager requestManager, Class<TranscodeType> cls, Context context) {
        this.B = requestManager;
        this.C = cls;
        this.A = context;
        this.E = requestManager.b(cls);
        this.D = glide.f();
        a(requestManager.c());
        a((BaseRequestOptions<?>) requestManager.d());
    }

    public RequestBuilder<TranscodeType> b(RequestListener<TranscodeType> requestListener) {
        if (v()) {
            return clone().b(requestListener);
        }
        this.G = null;
        return a(requestListener);
    }

    @SuppressLint({"CheckResult"})
    private void a(List<RequestListener<Object>> list) {
        for (RequestListener<Object> a2 : list) {
            a(a2);
        }
    }

    public RequestBuilder<TranscodeType> clone() {
        RequestBuilder<TranscodeType> requestBuilder = (RequestBuilder) super.clone();
        requestBuilder.E = requestBuilder.E.clone();
        List<RequestListener<TranscodeType>> list = requestBuilder.G;
        if (list != null) {
            requestBuilder.G = new ArrayList(list);
        }
        RequestBuilder<TranscodeType> requestBuilder2 = requestBuilder.H;
        if (requestBuilder2 != null) {
            requestBuilder.H = requestBuilder2.clone();
        }
        RequestBuilder<TranscodeType> requestBuilder3 = requestBuilder.I;
        if (requestBuilder3 != null) {
            requestBuilder.I = requestBuilder3.clone();
        }
        return requestBuilder;
    }

    public RequestBuilder<TranscodeType> a(BaseRequestOptions<?> baseRequestOptions) {
        Preconditions.a(baseRequestOptions);
        return (RequestBuilder) super.a(baseRequestOptions);
    }

    private RequestBuilder<TranscodeType> b(Object obj) {
        if (v()) {
            return clone().b(obj);
        }
        this.F = obj;
        this.L = true;
        return (RequestBuilder) H();
    }

    public RequestBuilder<TranscodeType> a(TransitionOptions<?, ? super TranscodeType> transitionOptions) {
        if (v()) {
            return clone().a(transitionOptions);
        }
        Preconditions.a(transitionOptions);
        this.E = transitionOptions;
        this.K = false;
        return (RequestBuilder) H();
    }

    private <Y extends Target<TranscodeType>> Y b(Y y, RequestListener<TranscodeType> requestListener, BaseRequestOptions<?> baseRequestOptions, Executor executor) {
        Preconditions.a(y);
        if (this.L) {
            Request a2 = a(y, requestListener, baseRequestOptions, executor);
            Request request = y.getRequest();
            if (!a2.isEquivalentTo(request) || a(baseRequestOptions, request)) {
                this.B.a((Target<?>) y);
                y.setRequest(a2);
                this.B.a(y, a2);
                return y;
            }
            Preconditions.a(request);
            if (!request.isRunning()) {
                request.begin();
            }
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    public RequestBuilder<TranscodeType> a(RequestListener<TranscodeType> requestListener) {
        if (v()) {
            return clone().a(requestListener);
        }
        if (requestListener != null) {
            if (this.G == null) {
                this.G = new ArrayList();
            }
            this.G.add(requestListener);
        }
        return (RequestBuilder) H();
    }

    public RequestBuilder<TranscodeType> a(Object obj) {
        return b(obj);
    }

    public RequestBuilder<TranscodeType> a(String str) {
        return b((Object) str);
    }

    public <Y extends Target<TranscodeType>> Y a(Y y) {
        return a(y, (RequestListener) null, Executors.b());
    }

    /* access modifiers changed from: package-private */
    public <Y extends Target<TranscodeType>> Y a(Y y, RequestListener<TranscodeType> requestListener, Executor executor) {
        b(y, requestListener, this, executor);
        return y;
    }

    private boolean a(BaseRequestOptions<?> baseRequestOptions, Request request) {
        return !baseRequestOptions.w() && request.isComplete();
    }

    private Priority b(Priority priority) {
        int i = AnonymousClass1.b[priority.ordinal()];
        if (i == 1) {
            return Priority.NORMAL;
        }
        if (i == 2) {
            return Priority.HIGH;
        }
        if (i == 3 || i == 4) {
            return Priority.IMMEDIATE;
        }
        throw new IllegalArgumentException("unknown priority: " + n());
    }

    public ViewTarget<ImageView, TranscodeType> a(ImageView imageView) {
        BaseRequestOptions baseRequestOptions;
        Util.a();
        Preconditions.a(imageView);
        if (!B() && z() && imageView.getScaleType() != null) {
            switch (AnonymousClass1.f2113a[imageView.getScaleType().ordinal()]) {
                case 1:
                    baseRequestOptions = clone().E();
                    break;
                case 2:
                    baseRequestOptions = clone().F();
                    break;
                case 3:
                case 4:
                case 5:
                    baseRequestOptions = clone().G();
                    break;
                case 6:
                    baseRequestOptions = clone().F();
                    break;
            }
        }
        baseRequestOptions = this;
        ViewTarget<ImageView, TranscodeType> a2 = this.D.a(imageView, this.C);
        b(a2, (RequestListener) null, baseRequestOptions, Executors.b());
        return a2;
    }

    /* JADX WARNING: type inference failed for: r27v0, types: [com.bumptech.glide.request.BaseRequestOptions<?>, com.bumptech.glide.request.BaseRequestOptions] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.bumptech.glide.request.Request b(java.lang.Object r19, com.bumptech.glide.request.target.Target<TranscodeType> r20, com.bumptech.glide.request.RequestListener<TranscodeType> r21, com.bumptech.glide.request.RequestCoordinator r22, com.bumptech.glide.TransitionOptions<?, ? super TranscodeType> r23, com.bumptech.glide.Priority r24, int r25, int r26, com.bumptech.glide.request.BaseRequestOptions<?> r27, java.util.concurrent.Executor r28) {
        /*
            r18 = this;
            r11 = r18
            r12 = r19
            r5 = r22
            r13 = r24
            com.bumptech.glide.RequestBuilder<TranscodeType> r0 = r11.H
            if (r0 == 0) goto L_0x0096
            boolean r1 = r11.M
            if (r1 != 0) goto L_0x008e
            com.bumptech.glide.TransitionOptions<?, ? super TranscodeType> r1 = r0.E
            boolean r0 = r0.K
            if (r0 == 0) goto L_0x0019
            r14 = r23
            goto L_0x001a
        L_0x0019:
            r14 = r1
        L_0x001a:
            com.bumptech.glide.RequestBuilder<TranscodeType> r0 = r11.H
            boolean r0 = r0.x()
            if (r0 == 0) goto L_0x0029
            com.bumptech.glide.RequestBuilder<TranscodeType> r0 = r11.H
            com.bumptech.glide.Priority r0 = r0.n()
            goto L_0x002d
        L_0x0029:
            com.bumptech.glide.Priority r0 = r11.b((com.bumptech.glide.Priority) r13)
        L_0x002d:
            r15 = r0
            com.bumptech.glide.RequestBuilder<TranscodeType> r0 = r11.H
            int r0 = r0.k()
            com.bumptech.glide.RequestBuilder<TranscodeType> r1 = r11.H
            int r1 = r1.j()
            boolean r2 = com.bumptech.glide.util.Util.b((int) r25, (int) r26)
            if (r2 == 0) goto L_0x0050
            com.bumptech.glide.RequestBuilder<TranscodeType> r2 = r11.H
            boolean r2 = r2.C()
            if (r2 != 0) goto L_0x0050
            int r0 = r27.k()
            int r1 = r27.j()
        L_0x0050:
            r16 = r0
            r17 = r1
            com.bumptech.glide.request.ThumbnailRequestCoordinator r10 = new com.bumptech.glide.request.ThumbnailRequestCoordinator
            r10.<init>(r12, r5)
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            r4 = r27
            r5 = r10
            r6 = r23
            r7 = r24
            r8 = r25
            r9 = r26
            r13 = r10
            r10 = r28
            com.bumptech.glide.request.Request r10 = r0.a((java.lang.Object) r1, r2, r3, (com.bumptech.glide.request.BaseRequestOptions<?>) r4, (com.bumptech.glide.request.RequestCoordinator) r5, r6, (com.bumptech.glide.Priority) r7, (int) r8, (int) r9, (java.util.concurrent.Executor) r10)
            r0 = 1
            r11.M = r0
            com.bumptech.glide.RequestBuilder<TranscodeType> r9 = r11.H
            r0 = r9
            r4 = r13
            r5 = r14
            r6 = r15
            r7 = r16
            r8 = r17
            r12 = r10
            r10 = r28
            com.bumptech.glide.request.Request r0 = r0.a((java.lang.Object) r1, r2, r3, (com.bumptech.glide.request.RequestCoordinator) r4, r5, (com.bumptech.glide.Priority) r6, (int) r7, (int) r8, (com.bumptech.glide.request.BaseRequestOptions<?>) r9, (java.util.concurrent.Executor) r10)
            r1 = 0
            r11.M = r1
            r13.a(r12, r0)
            return r13
        L_0x008e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()"
            r0.<init>(r1)
            throw r0
        L_0x0096:
            java.lang.Float r0 = r11.J
            if (r0 == 0) goto L_0x00d6
            com.bumptech.glide.request.ThumbnailRequestCoordinator r14 = new com.bumptech.glide.request.ThumbnailRequestCoordinator
            r14.<init>(r12, r5)
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            r4 = r27
            r5 = r14
            r6 = r23
            r7 = r24
            r8 = r25
            r9 = r26
            r10 = r28
            com.bumptech.glide.request.Request r15 = r0.a((java.lang.Object) r1, r2, r3, (com.bumptech.glide.request.BaseRequestOptions<?>) r4, (com.bumptech.glide.request.RequestCoordinator) r5, r6, (com.bumptech.glide.Priority) r7, (int) r8, (int) r9, (java.util.concurrent.Executor) r10)
            com.bumptech.glide.request.BaseRequestOptions r0 = r27.clone()
            java.lang.Float r1 = r11.J
            float r1 = r1.floatValue()
            com.bumptech.glide.request.BaseRequestOptions r4 = r0.a((float) r1)
            com.bumptech.glide.Priority r7 = r11.b((com.bumptech.glide.Priority) r13)
            r0 = r18
            r1 = r19
            com.bumptech.glide.request.Request r0 = r0.a((java.lang.Object) r1, r2, r3, (com.bumptech.glide.request.BaseRequestOptions<?>) r4, (com.bumptech.glide.request.RequestCoordinator) r5, r6, (com.bumptech.glide.Priority) r7, (int) r8, (int) r9, (java.util.concurrent.Executor) r10)
            r14.a(r15, r0)
            return r14
        L_0x00d6:
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            r4 = r27
            r5 = r22
            r6 = r23
            r7 = r24
            r8 = r25
            r9 = r26
            r10 = r28
            com.bumptech.glide.request.Request r0 = r0.a((java.lang.Object) r1, r2, r3, (com.bumptech.glide.request.BaseRequestOptions<?>) r4, (com.bumptech.glide.request.RequestCoordinator) r5, r6, (com.bumptech.glide.Priority) r7, (int) r8, (int) r9, (java.util.concurrent.Executor) r10)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.RequestBuilder.b(java.lang.Object, com.bumptech.glide.request.target.Target, com.bumptech.glide.request.RequestListener, com.bumptech.glide.request.RequestCoordinator, com.bumptech.glide.TransitionOptions, com.bumptech.glide.Priority, int, int, com.bumptech.glide.request.BaseRequestOptions, java.util.concurrent.Executor):com.bumptech.glide.request.Request");
    }

    private Request a(Target<TranscodeType> target, RequestListener<TranscodeType> requestListener, BaseRequestOptions<?> baseRequestOptions, Executor executor) {
        return a(new Object(), target, requestListener, (RequestCoordinator) null, this.E, baseRequestOptions.n(), baseRequestOptions.k(), baseRequestOptions.j(), baseRequestOptions, executor);
    }

    private Request a(Object obj, Target<TranscodeType> target, RequestListener<TranscodeType> requestListener, RequestCoordinator requestCoordinator, TransitionOptions<?, ? super TranscodeType> transitionOptions, Priority priority, int i, int i2, BaseRequestOptions<?> baseRequestOptions, Executor executor) {
        ErrorRequestCoordinator errorRequestCoordinator;
        ErrorRequestCoordinator errorRequestCoordinator2;
        if (this.I != null) {
            errorRequestCoordinator2 = new ErrorRequestCoordinator(obj, requestCoordinator);
            errorRequestCoordinator = errorRequestCoordinator2;
        } else {
            Object obj2 = obj;
            errorRequestCoordinator = null;
            errorRequestCoordinator2 = requestCoordinator;
        }
        Request b = b(obj, target, requestListener, errorRequestCoordinator2, transitionOptions, priority, i, i2, baseRequestOptions, executor);
        if (errorRequestCoordinator == null) {
            return b;
        }
        int k = this.I.k();
        int j = this.I.j();
        if (Util.b(i, i2) && !this.I.C()) {
            k = baseRequestOptions.k();
            j = baseRequestOptions.j();
        }
        RequestBuilder<TranscodeType> requestBuilder = this.I;
        ErrorRequestCoordinator errorRequestCoordinator3 = errorRequestCoordinator;
        errorRequestCoordinator3.a(b, requestBuilder.a(obj, target, requestListener, (RequestCoordinator) errorRequestCoordinator3, requestBuilder.E, requestBuilder.n(), k, j, (BaseRequestOptions<?>) this.I, executor));
        return errorRequestCoordinator3;
    }

    private Request a(Object obj, Target<TranscodeType> target, RequestListener<TranscodeType> requestListener, BaseRequestOptions<?> baseRequestOptions, RequestCoordinator requestCoordinator, TransitionOptions<?, ? super TranscodeType> transitionOptions, Priority priority, int i, int i2, Executor executor) {
        Context context = this.A;
        GlideContext glideContext = this.D;
        return SingleRequest.a(context, glideContext, obj, this.F, this.C, baseRequestOptions, i, i2, priority, target, requestListener, this.G, requestCoordinator, glideContext.d(), transitionOptions.a(), executor);
    }
}
