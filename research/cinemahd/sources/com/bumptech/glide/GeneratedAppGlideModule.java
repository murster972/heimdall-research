package com.bumptech.glide;

import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.module.AppGlideModule;
import java.util.Set;

abstract class GeneratedAppGlideModule extends AppGlideModule {
    GeneratedAppGlideModule() {
    }

    /* access modifiers changed from: package-private */
    public abstract Set<Class<?>> b();

    /* access modifiers changed from: package-private */
    public RequestManagerRetriever.RequestManagerFactory c() {
        return null;
    }
}
