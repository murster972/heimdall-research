package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SupportRequestManagerFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private final ActivityFragmentLifecycle f2341a;
    private final RequestManagerTreeNode b;
    private final Set<SupportRequestManagerFragment> c;
    private SupportRequestManagerFragment d;
    private RequestManager e;
    private Fragment f;

    private class SupportFragmentRequestManagerTreeNode implements RequestManagerTreeNode {
        SupportFragmentRequestManagerTreeNode() {
        }

        public Set<RequestManager> a() {
            Set<SupportRequestManagerFragment> e = SupportRequestManagerFragment.this.e();
            HashSet hashSet = new HashSet(e.size());
            for (SupportRequestManagerFragment next : e) {
                if (next.g() != null) {
                    hashSet.add(next.g());
                }
            }
            return hashSet;
        }

        public String toString() {
            return super.toString() + "{fragment=" + SupportRequestManagerFragment.this + "}";
        }
    }

    public SupportRequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }

    private void b(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.c.remove(supportRequestManagerFragment);
    }

    private boolean c(Fragment fragment) {
        Fragment i = i();
        while (true) {
            Fragment parentFragment = fragment.getParentFragment();
            if (parentFragment == null) {
                return false;
            }
            if (parentFragment.equals(i)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    private Fragment i() {
        Fragment parentFragment = getParentFragment();
        return parentFragment != null ? parentFragment : this.f;
    }

    private void j() {
        SupportRequestManagerFragment supportRequestManagerFragment = this.d;
        if (supportRequestManagerFragment != null) {
            supportRequestManagerFragment.b(this);
            this.d = null;
        }
    }

    public void a(RequestManager requestManager) {
        this.e = requestManager;
    }

    /* access modifiers changed from: package-private */
    public Set<SupportRequestManagerFragment> e() {
        SupportRequestManagerFragment supportRequestManagerFragment = this.d;
        if (supportRequestManagerFragment == null) {
            return Collections.emptySet();
        }
        if (equals(supportRequestManagerFragment)) {
            return Collections.unmodifiableSet(this.c);
        }
        HashSet hashSet = new HashSet();
        for (SupportRequestManagerFragment next : this.d.e()) {
            if (c(next.i())) {
                hashSet.add(next);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    /* access modifiers changed from: package-private */
    public ActivityFragmentLifecycle f() {
        return this.f2341a;
    }

    public RequestManager g() {
        return this.e;
    }

    public RequestManagerTreeNode h() {
        return this.b;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        FragmentManager b2 = b((Fragment) this);
        if (b2 != null) {
            try {
                a(getContext(), b2);
            } catch (IllegalStateException e2) {
                if (Log.isLoggable("SupportRMFragment", 5)) {
                    Log.w("SupportRMFragment", "Unable to register fragment with root", e2);
                }
            }
        } else if (Log.isLoggable("SupportRMFragment", 5)) {
            Log.w("SupportRMFragment", "Unable to register fragment with root, ancestor detached");
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f2341a.a();
        j();
    }

    public void onDetach() {
        super.onDetach();
        this.f = null;
        j();
    }

    public void onStart() {
        super.onStart();
        this.f2341a.b();
    }

    public void onStop() {
        super.onStop();
        this.f2341a.c();
    }

    public String toString() {
        return super.toString() + "{parent=" + i() + "}";
    }

    @SuppressLint({"ValidFragment"})
    public SupportRequestManagerFragment(ActivityFragmentLifecycle activityFragmentLifecycle) {
        this.b = new SupportFragmentRequestManagerTreeNode();
        this.c = new HashSet();
        this.f2341a = activityFragmentLifecycle;
    }

    private void a(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.c.add(supportRequestManagerFragment);
    }

    private static FragmentManager b(Fragment fragment) {
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getFragmentManager();
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        FragmentManager b2;
        this.f = fragment;
        if (fragment != null && fragment.getContext() != null && (b2 = b(fragment)) != null) {
            a(fragment.getContext(), b2);
        }
    }

    private void a(Context context, FragmentManager fragmentManager) {
        j();
        this.d = Glide.a(context).h().a(fragmentManager);
        if (!equals(this.d)) {
            this.d.a(this);
        }
    }
}
