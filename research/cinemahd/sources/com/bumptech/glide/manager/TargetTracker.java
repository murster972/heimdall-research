package com.bumptech.glide.manager;

import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.util.Util;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public final class TargetTracker implements LifecycleListener {

    /* renamed from: a  reason: collision with root package name */
    private final Set<Target<?>> f2343a = Collections.newSetFromMap(new WeakHashMap());

    public void a(Target<?> target) {
        this.f2343a.add(target);
    }

    public void b(Target<?> target) {
        this.f2343a.remove(target);
    }

    public void onDestroy() {
        for (T onDestroy : Util.a(this.f2343a)) {
            onDestroy.onDestroy();
        }
    }

    public void onStart() {
        for (T onStart : Util.a(this.f2343a)) {
            onStart.onStart();
        }
    }

    public void onStop() {
        for (T onStop : Util.a(this.f2343a)) {
            onStop.onStop();
        }
    }

    public void a() {
        this.f2343a.clear();
    }

    public List<Target<?>> b() {
        return Util.a(this.f2343a);
    }
}
