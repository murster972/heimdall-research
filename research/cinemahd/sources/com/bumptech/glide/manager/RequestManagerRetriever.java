package com.bumptech.glide.manager;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import androidx.collection.ArrayMap;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.GlideExperiments;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.bitmap.HardwareConfigState;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import java.util.HashMap;
import java.util.Map;

public class RequestManagerRetriever implements Handler.Callback {
    private static final RequestManagerFactory g = new RequestManagerFactory() {
        public RequestManager a(Glide glide, Lifecycle lifecycle, RequestManagerTreeNode requestManagerTreeNode, Context context) {
            return new RequestManager(glide, lifecycle, requestManagerTreeNode, context);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private volatile RequestManager f2339a;
    final Map<FragmentManager, RequestManagerFragment> b = new HashMap();
    final Map<androidx.fragment.app.FragmentManager, SupportRequestManagerFragment> c = new HashMap();
    private final Handler d;
    private final RequestManagerFactory e;
    private final FrameWaiter f;

    public interface RequestManagerFactory {
        RequestManager a(Glide glide, Lifecycle lifecycle, RequestManagerTreeNode requestManagerTreeNode, Context context);
    }

    public RequestManagerRetriever(RequestManagerFactory requestManagerFactory, GlideExperiments glideExperiments) {
        new ArrayMap();
        new ArrayMap();
        new Bundle();
        this.e = requestManagerFactory == null ? g : requestManagerFactory;
        this.d = new Handler(Looper.getMainLooper(), this);
        this.f = a(glideExperiments);
    }

    private static FrameWaiter a(GlideExperiments glideExperiments) {
        if (!HardwareConfigState.h || !HardwareConfigState.g) {
            return new DoNothingFirstFrameWaiter();
        }
        if (glideExperiments.a(GlideBuilder.WaitForFramesAfterTrimMemory.class)) {
            return new FirstFrameAndAfterTrimMemoryWaiter();
        }
        return new FirstFrameWaiter();
    }

    private static Activity b(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return b(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    private RequestManager c(Context context) {
        if (this.f2339a == null) {
            synchronized (this) {
                if (this.f2339a == null) {
                    this.f2339a = this.e.a(Glide.a(context.getApplicationContext()), new ApplicationLifecycle(), new EmptyRequestManagerTreeNode(), context.getApplicationContext());
                }
            }
        }
        return this.f2339a;
    }

    private static boolean d(Context context) {
        Activity b2 = b(context);
        return b2 == null || !b2.isFinishing();
    }

    public boolean handleMessage(Message message) {
        Object obj;
        int i = message.what;
        Object obj2 = null;
        boolean z = true;
        if (i == 1) {
            obj2 = (FragmentManager) message.obj;
            obj = this.b.remove(obj2);
        } else if (i != 2) {
            z = false;
            obj = null;
        } else {
            obj2 = (androidx.fragment.app.FragmentManager) message.obj;
            obj = this.c.remove(obj2);
        }
        if (z && obj == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj2);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    public RequestManagerFragment b(Activity activity) {
        return a(activity.getFragmentManager(), (Fragment) null);
    }

    public RequestManager a(Context context) {
        if (context != null) {
            if (Util.d() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return a((FragmentActivity) context);
                }
                if (context instanceof Activity) {
                    return a((Activity) context);
                }
                if (context instanceof ContextWrapper) {
                    ContextWrapper contextWrapper = (ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return a(contextWrapper.getBaseContext());
                    }
                }
            }
            return c(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    @TargetApi(17)
    private static void c(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    public RequestManager a(FragmentActivity fragmentActivity) {
        if (Util.c()) {
            return a(fragmentActivity.getApplicationContext());
        }
        c((Activity) fragmentActivity);
        this.f.a(fragmentActivity);
        return a((Context) fragmentActivity, fragmentActivity.getSupportFragmentManager(), (androidx.fragment.app.Fragment) null, d(fragmentActivity));
    }

    public RequestManager a(androidx.fragment.app.Fragment fragment) {
        Preconditions.a(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (Util.c()) {
            return a(fragment.getContext().getApplicationContext());
        }
        if (fragment.getActivity() != null) {
            this.f.a(fragment.getActivity());
        }
        return a(fragment.getContext(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    public RequestManager a(Activity activity) {
        if (Util.c()) {
            return a(activity.getApplicationContext());
        }
        if (activity instanceof FragmentActivity) {
            return a((FragmentActivity) activity);
        }
        c(activity);
        this.f.a(activity);
        return a((Context) activity, activity.getFragmentManager(), (Fragment) null, d(activity));
    }

    private RequestManagerFragment a(FragmentManager fragmentManager, Fragment fragment) {
        RequestManagerFragment requestManagerFragment = (RequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (requestManagerFragment != null) {
            return requestManagerFragment;
        }
        RequestManagerFragment requestManagerFragment2 = this.b.get(fragmentManager);
        if (requestManagerFragment2 != null) {
            return requestManagerFragment2;
        }
        RequestManagerFragment requestManagerFragment3 = new RequestManagerFragment();
        requestManagerFragment3.a(fragment);
        this.b.put(fragmentManager, requestManagerFragment3);
        fragmentManager.beginTransaction().add(requestManagerFragment3, "com.bumptech.glide.manager").commitAllowingStateLoss();
        this.d.obtainMessage(1, fragmentManager).sendToTarget();
        return requestManagerFragment3;
    }

    @Deprecated
    private RequestManager a(Context context, FragmentManager fragmentManager, Fragment fragment, boolean z) {
        RequestManagerFragment a2 = a(fragmentManager, fragment);
        RequestManager c2 = a2.c();
        if (c2 == null) {
            c2 = this.e.a(Glide.a(context), a2.b(), a2.d(), context);
            if (z) {
                c2.onStart();
            }
            a2.a(c2);
        }
        return c2;
    }

    /* access modifiers changed from: package-private */
    public SupportRequestManagerFragment a(androidx.fragment.app.FragmentManager fragmentManager) {
        return a(fragmentManager, (androidx.fragment.app.Fragment) null);
    }

    private SupportRequestManagerFragment a(androidx.fragment.app.FragmentManager fragmentManager, androidx.fragment.app.Fragment fragment) {
        SupportRequestManagerFragment supportRequestManagerFragment = (SupportRequestManagerFragment) fragmentManager.b("com.bumptech.glide.manager");
        if (supportRequestManagerFragment != null) {
            return supportRequestManagerFragment;
        }
        SupportRequestManagerFragment supportRequestManagerFragment2 = this.c.get(fragmentManager);
        if (supportRequestManagerFragment2 != null) {
            return supportRequestManagerFragment2;
        }
        SupportRequestManagerFragment supportRequestManagerFragment3 = new SupportRequestManagerFragment();
        supportRequestManagerFragment3.a(fragment);
        this.c.put(fragmentManager, supportRequestManagerFragment3);
        fragmentManager.b().a((androidx.fragment.app.Fragment) supportRequestManagerFragment3, "com.bumptech.glide.manager").b();
        this.d.obtainMessage(2, fragmentManager).sendToTarget();
        return supportRequestManagerFragment3;
    }

    private RequestManager a(Context context, androidx.fragment.app.FragmentManager fragmentManager, androidx.fragment.app.Fragment fragment, boolean z) {
        SupportRequestManagerFragment a2 = a(fragmentManager, fragment);
        RequestManager g2 = a2.g();
        if (g2 == null) {
            g2 = this.e.a(Glide.a(context), a2.f(), a2.h(), context);
            if (z) {
                g2.onStart();
            }
            a2.a(g2);
        }
        return g2;
    }
}
