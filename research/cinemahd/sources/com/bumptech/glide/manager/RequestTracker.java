package com.bumptech.glide.manager;

import android.util.Log;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.util.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public class RequestTracker {

    /* renamed from: a  reason: collision with root package name */
    private final Set<Request> f2340a = Collections.newSetFromMap(new WeakHashMap());
    private final List<Request> b = new ArrayList();
    private boolean c;

    public boolean a(Request request) {
        boolean z = true;
        if (request == null) {
            return true;
        }
        boolean remove = this.f2340a.remove(request);
        if (!this.b.remove(request) && !remove) {
            z = false;
        }
        if (z) {
            request.clear();
        }
        return z;
    }

    public void b(Request request) {
        this.f2340a.add(request);
        if (!this.c) {
            request.begin();
            return;
        }
        request.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.b.add(request);
    }

    public void c() {
        this.c = true;
        for (T t : Util.a(this.f2340a)) {
            if (t.isRunning()) {
                t.pause();
                this.b.add(t);
            }
        }
    }

    public void d() {
        for (T t : Util.a(this.f2340a)) {
            if (!t.isComplete() && !t.isCleared()) {
                t.clear();
                if (!this.c) {
                    t.begin();
                } else {
                    this.b.add(t);
                }
            }
        }
    }

    public void e() {
        this.c = false;
        for (T t : Util.a(this.f2340a)) {
            if (!t.isComplete() && !t.isRunning()) {
                t.begin();
            }
        }
        this.b.clear();
    }

    public String toString() {
        return super.toString() + "{numRequests=" + this.f2340a.size() + ", isPaused=" + this.c + "}";
    }

    public void a() {
        for (T a2 : Util.a(this.f2340a)) {
            a(a2);
        }
        this.b.clear();
    }

    public void b() {
        this.c = true;
        for (T t : Util.a(this.f2340a)) {
            if (t.isRunning() || t.isComplete()) {
                t.clear();
                this.b.add(t);
            }
        }
    }
}
