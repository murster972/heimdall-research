package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Deprecated
public class RequestManagerFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private final ActivityFragmentLifecycle f2337a;
    private final RequestManagerTreeNode b;
    private final Set<RequestManagerFragment> c;
    private RequestManager d;
    private RequestManagerFragment e;
    private Fragment f;

    private class FragmentRequestManagerTreeNode implements RequestManagerTreeNode {
        FragmentRequestManagerTreeNode() {
        }

        public Set<RequestManager> a() {
            Set<RequestManagerFragment> a2 = RequestManagerFragment.this.a();
            HashSet hashSet = new HashSet(a2.size());
            for (RequestManagerFragment next : a2) {
                if (next.c() != null) {
                    hashSet.add(next.c());
                }
            }
            return hashSet;
        }

        public String toString() {
            return super.toString() + "{fragment=" + RequestManagerFragment.this + "}";
        }
    }

    public RequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }

    @TargetApi(17)
    private Fragment e() {
        Fragment parentFragment = Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? parentFragment : this.f;
    }

    private void f() {
        RequestManagerFragment requestManagerFragment = this.e;
        if (requestManagerFragment != null) {
            requestManagerFragment.b(this);
            this.e = null;
        }
    }

    public void a(RequestManager requestManager) {
        this.d = requestManager;
    }

    /* access modifiers changed from: package-private */
    public ActivityFragmentLifecycle b() {
        return this.f2337a;
    }

    public RequestManager c() {
        return this.d;
    }

    public RequestManagerTreeNode d() {
        return this.b;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            a(activity);
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f2337a.a();
        f();
    }

    public void onDetach() {
        super.onDetach();
        f();
    }

    public void onStart() {
        super.onStart();
        this.f2337a.b();
    }

    public void onStop() {
        super.onStop();
        this.f2337a.c();
    }

    public String toString() {
        return super.toString() + "{parent=" + e() + "}";
    }

    @SuppressLint({"ValidFragment"})
    RequestManagerFragment(ActivityFragmentLifecycle activityFragmentLifecycle) {
        this.b = new FragmentRequestManagerTreeNode();
        this.c = new HashSet();
        this.f2337a = activityFragmentLifecycle;
    }

    private void a(RequestManagerFragment requestManagerFragment) {
        this.c.add(requestManagerFragment);
    }

    private void b(RequestManagerFragment requestManagerFragment) {
        this.c.remove(requestManagerFragment);
    }

    @TargetApi(17)
    private boolean b(Fragment fragment) {
        Fragment parentFragment = getParentFragment();
        while (true) {
            Fragment parentFragment2 = fragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragment)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(17)
    public Set<RequestManagerFragment> a() {
        if (equals(this.e)) {
            return Collections.unmodifiableSet(this.c);
        }
        if (this.e == null || Build.VERSION.SDK_INT < 17) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet();
        for (RequestManagerFragment next : this.e.a()) {
            if (b(next.getParentFragment())) {
                hashSet.add(next);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment fragment) {
        this.f = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            a(fragment.getActivity());
        }
    }

    private void a(Activity activity) {
        f();
        this.e = Glide.a((Context) activity).h().b(activity);
        if (!equals(this.e)) {
            this.e.a(this);
        }
    }
}
