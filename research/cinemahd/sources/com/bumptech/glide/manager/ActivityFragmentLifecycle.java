package com.bumptech.glide.manager;

import com.bumptech.glide.util.Util;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

class ActivityFragmentLifecycle implements Lifecycle {

    /* renamed from: a  reason: collision with root package name */
    private final Set<LifecycleListener> f2334a = Collections.newSetFromMap(new WeakHashMap());
    private boolean b;
    private boolean c;

    ActivityFragmentLifecycle() {
    }

    public void a(LifecycleListener lifecycleListener) {
        this.f2334a.remove(lifecycleListener);
    }

    public void b(LifecycleListener lifecycleListener) {
        this.f2334a.add(lifecycleListener);
        if (this.c) {
            lifecycleListener.onDestroy();
        } else if (this.b) {
            lifecycleListener.onStart();
        } else {
            lifecycleListener.onStop();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.b = false;
        for (T onStop : Util.a(this.f2334a)) {
            onStop.onStop();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = true;
        for (T onDestroy : Util.a(this.f2334a)) {
            onDestroy.onDestroy();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.b = true;
        for (T onStart : Util.a(this.f2334a)) {
            onStart.onStart();
        }
    }
}
