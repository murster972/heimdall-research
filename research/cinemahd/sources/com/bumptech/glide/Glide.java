package com.bumptech.glide;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.load.data.InputStreamRewinder;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.model.AssetUriLoader;
import com.bumptech.glide.load.model.ByteArrayLoader;
import com.bumptech.glide.load.model.ByteBufferEncoder;
import com.bumptech.glide.load.model.ByteBufferFileLoader;
import com.bumptech.glide.load.model.DataUrlLoader;
import com.bumptech.glide.load.model.FileLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.MediaStoreFileLoader;
import com.bumptech.glide.load.model.ResourceLoader;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.model.StringLoader;
import com.bumptech.glide.load.model.UnitModelLoader;
import com.bumptech.glide.load.model.UriLoader;
import com.bumptech.glide.load.model.UrlUriLoader;
import com.bumptech.glide.load.model.stream.HttpGlideUrlLoader;
import com.bumptech.glide.load.model.stream.MediaStoreImageThumbLoader;
import com.bumptech.glide.load.model.stream.MediaStoreVideoThumbLoader;
import com.bumptech.glide.load.model.stream.QMediaStoreUriLoader;
import com.bumptech.glide.load.model.stream.UrlLoader;
import com.bumptech.glide.load.resource.bitmap.BitmapDrawableDecoder;
import com.bumptech.glide.load.resource.bitmap.BitmapDrawableEncoder;
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder;
import com.bumptech.glide.load.resource.bitmap.ByteBufferBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.ByteBufferBitmapImageDecoderResourceDecoder;
import com.bumptech.glide.load.resource.bitmap.DefaultImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.resource.bitmap.ExifInterfaceImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.InputStreamBitmapImageDecoderResourceDecoder;
import com.bumptech.glide.load.resource.bitmap.ParcelFileDescriptorBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.ResourceBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.StreamBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.UnitBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.VideoDecoder;
import com.bumptech.glide.load.resource.bytes.ByteBufferRewinder;
import com.bumptech.glide.load.resource.drawable.ResourceDrawableDecoder;
import com.bumptech.glide.load.resource.drawable.UnitDrawableDecoder;
import com.bumptech.glide.load.resource.file.FileDecoder;
import com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawableEncoder;
import com.bumptech.glide.load.resource.gif.GifFrameResourceDecoder;
import com.bumptech.glide.load.resource.gif.StreamGifDecoder;
import com.bumptech.glide.load.resource.transcode.BitmapBytesTranscoder;
import com.bumptech.glide.load.resource.transcode.BitmapDrawableTranscoder;
import com.bumptech.glide.load.resource.transcode.DrawableBytesTranscoder;
import com.bumptech.glide.load.resource.transcode.GifDrawableBytesTranscoder;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.module.GlideModule;
import com.bumptech.glide.module.ManifestParser;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ImageViewTargetFactory;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Glide implements ComponentCallbacks2 {
    private static volatile Glide i;
    private static volatile boolean j;

    /* renamed from: a  reason: collision with root package name */
    private final BitmapPool f2104a;
    private final MemoryCache b;
    private final GlideContext c;
    private final Registry d;
    private final ArrayPool e;
    private final RequestManagerRetriever f;
    private final ConnectivityMonitorFactory g;
    private final List<RequestManager> h = new ArrayList();

    public interface RequestOptionsFactory {
        RequestOptions build();
    }

    Glide(Context context, Engine engine, MemoryCache memoryCache, BitmapPool bitmapPool, ArrayPool arrayPool, RequestManagerRetriever requestManagerRetriever, ConnectivityMonitorFactory connectivityMonitorFactory, int i2, RequestOptionsFactory requestOptionsFactory, Map<Class<?>, TransitionOptions<?, ?>> map, List<RequestListener<Object>> list, GlideExperiments glideExperiments) {
        ResourceDecoder resourceDecoder;
        ResourceDecoder resourceDecoder2;
        Class<GifDecoder> cls;
        Context context2 = context;
        BitmapPool bitmapPool2 = bitmapPool;
        ArrayPool arrayPool2 = arrayPool;
        Class<GifDecoder> cls2 = GifDecoder.class;
        Class<byte[]> cls3 = byte[].class;
        MemoryCategory memoryCategory = MemoryCategory.NORMAL;
        this.f2104a = bitmapPool2;
        this.e = arrayPool2;
        this.b = memoryCache;
        this.f = requestManagerRetriever;
        this.g = connectivityMonitorFactory;
        Resources resources = context.getResources();
        this.d = new Registry();
        this.d.a((ImageHeaderParser) new DefaultImageHeaderParser());
        if (Build.VERSION.SDK_INT >= 27) {
            this.d.a((ImageHeaderParser) new ExifInterfaceImageHeaderParser());
        }
        List<ImageHeaderParser> a2 = this.d.a();
        ByteBufferGifDecoder byteBufferGifDecoder = new ByteBufferGifDecoder(context2, a2, bitmapPool2, arrayPool2);
        ResourceDecoder<ParcelFileDescriptor, Bitmap> c2 = VideoDecoder.c(bitmapPool);
        Downsampler downsampler = new Downsampler(this.d.a(), resources.getDisplayMetrics(), bitmapPool2, arrayPool2);
        if (!glideExperiments.a(GlideBuilder.EnableImageDecoderForBitmaps.class) || Build.VERSION.SDK_INT < 28) {
            resourceDecoder = new ByteBufferBitmapDecoder(downsampler);
            resourceDecoder2 = new StreamBitmapDecoder(downsampler, arrayPool2);
        } else {
            resourceDecoder2 = new InputStreamBitmapImageDecoderResourceDecoder();
            resourceDecoder = new ByteBufferBitmapImageDecoderResourceDecoder();
        }
        ResourceDrawableDecoder resourceDrawableDecoder = new ResourceDrawableDecoder(context2);
        ResourceLoader.StreamFactory streamFactory = new ResourceLoader.StreamFactory(resources);
        ResourceLoader.UriFactory uriFactory = new ResourceLoader.UriFactory(resources);
        Class<byte[]> cls4 = cls3;
        ResourceLoader.FileDescriptorFactory fileDescriptorFactory = new ResourceLoader.FileDescriptorFactory(resources);
        ResourceLoader.AssetFileDescriptorFactory assetFileDescriptorFactory = new ResourceLoader.AssetFileDescriptorFactory(resources);
        BitmapEncoder bitmapEncoder = new BitmapEncoder(arrayPool2);
        ResourceLoader.UriFactory uriFactory2 = uriFactory;
        BitmapBytesTranscoder bitmapBytesTranscoder = new BitmapBytesTranscoder();
        GifDrawableBytesTranscoder gifDrawableBytesTranscoder = new GifDrawableBytesTranscoder();
        ContentResolver contentResolver = context.getContentResolver();
        ResourceLoader.FileDescriptorFactory fileDescriptorFactory2 = fileDescriptorFactory;
        ResourceLoader.StreamFactory streamFactory2 = streamFactory;
        ResourceDrawableDecoder resourceDrawableDecoder2 = resourceDrawableDecoder;
        this.d.a(ByteBuffer.class, new ByteBufferEncoder()).a(InputStream.class, new StreamEncoder(arrayPool2)).a("Bitmap", ByteBuffer.class, Bitmap.class, resourceDecoder).a("Bitmap", InputStream.class, Bitmap.class, resourceDecoder2);
        if (ParcelFileDescriptorRewinder.c()) {
            cls = cls2;
            this.d.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new ParcelFileDescriptorBitmapDecoder(downsampler));
        } else {
            cls = cls2;
        }
        Class<GifDecoder> cls5 = cls;
        ResourceDrawableDecoder resourceDrawableDecoder3 = resourceDrawableDecoder2;
        this.d.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, c2).a("Bitmap", AssetFileDescriptor.class, Bitmap.class, VideoDecoder.a(bitmapPool)).a(Bitmap.class, Bitmap.class, UnitModelLoader.Factory.b()).a("Bitmap", Bitmap.class, Bitmap.class, new UnitBitmapDecoder()).a(Bitmap.class, bitmapEncoder).a("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new BitmapDrawableDecoder(resources, resourceDecoder)).a("BitmapDrawable", InputStream.class, BitmapDrawable.class, new BitmapDrawableDecoder(resources, resourceDecoder2)).a("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new BitmapDrawableDecoder(resources, c2)).a(BitmapDrawable.class, new BitmapDrawableEncoder(bitmapPool2, bitmapEncoder)).a("Gif", InputStream.class, GifDrawable.class, new StreamGifDecoder(a2, byteBufferGifDecoder, arrayPool2)).a("Gif", ByteBuffer.class, GifDrawable.class, byteBufferGifDecoder).a(GifDrawable.class, new GifDrawableEncoder()).a(cls5, cls5, UnitModelLoader.Factory.b()).a("Bitmap", cls5, Bitmap.class, new GifFrameResourceDecoder(bitmapPool2)).a(Uri.class, Drawable.class, resourceDrawableDecoder3).a(Uri.class, Bitmap.class, new ResourceBitmapDecoder(resourceDrawableDecoder3, bitmapPool2)).a((DataRewinder.Factory<?>) new ByteBufferRewinder.Factory()).a(File.class, ByteBuffer.class, new ByteBufferFileLoader.Factory()).a(File.class, InputStream.class, new FileLoader.StreamFactory()).a(File.class, File.class, new FileDecoder()).a(File.class, ParcelFileDescriptor.class, new FileLoader.FileDescriptorFactory()).a(File.class, File.class, UnitModelLoader.Factory.b()).a((DataRewinder.Factory<?>) new InputStreamRewinder.Factory(arrayPool2));
        if (ParcelFileDescriptorRewinder.c()) {
            this.d.a((DataRewinder.Factory<?>) new ParcelFileDescriptorRewinder.Factory());
        }
        ResourceLoader.StreamFactory streamFactory3 = streamFactory2;
        ResourceLoader.FileDescriptorFactory fileDescriptorFactory3 = fileDescriptorFactory2;
        ResourceLoader.UriFactory uriFactory3 = uriFactory2;
        ResourceLoader.AssetFileDescriptorFactory assetFileDescriptorFactory2 = assetFileDescriptorFactory;
        Context context3 = context;
        this.d.a(Integer.TYPE, InputStream.class, streamFactory3).a(Integer.TYPE, ParcelFileDescriptor.class, fileDescriptorFactory3).a(Integer.class, InputStream.class, streamFactory3).a(Integer.class, ParcelFileDescriptor.class, fileDescriptorFactory3).a(Integer.class, Uri.class, uriFactory3).a(Integer.TYPE, AssetFileDescriptor.class, assetFileDescriptorFactory2).a(Integer.class, AssetFileDescriptor.class, assetFileDescriptorFactory2).a(Integer.TYPE, Uri.class, uriFactory3).a(String.class, InputStream.class, new DataUrlLoader.StreamFactory()).a(Uri.class, InputStream.class, new DataUrlLoader.StreamFactory()).a(String.class, InputStream.class, new StringLoader.StreamFactory()).a(String.class, ParcelFileDescriptor.class, new StringLoader.FileDescriptorFactory()).a(String.class, AssetFileDescriptor.class, new StringLoader.AssetFileDescriptorFactory()).a(Uri.class, InputStream.class, new AssetUriLoader.StreamFactory(context.getAssets())).a(Uri.class, ParcelFileDescriptor.class, new AssetUriLoader.FileDescriptorFactory(context.getAssets())).a(Uri.class, InputStream.class, new MediaStoreImageThumbLoader.Factory(context3)).a(Uri.class, InputStream.class, new MediaStoreVideoThumbLoader.Factory(context3));
        if (Build.VERSION.SDK_INT >= 29) {
            this.d.a(Uri.class, InputStream.class, new QMediaStoreUriLoader.InputStreamFactory(context3));
            this.d.a(Uri.class, ParcelFileDescriptor.class, new QMediaStoreUriLoader.FileDescriptorFactory(context3));
        }
        ContentResolver contentResolver2 = contentResolver;
        Class<byte[]> cls6 = cls4;
        BitmapBytesTranscoder bitmapBytesTranscoder2 = bitmapBytesTranscoder;
        GifDrawableBytesTranscoder gifDrawableBytesTranscoder2 = gifDrawableBytesTranscoder;
        this.d.a(Uri.class, InputStream.class, new UriLoader.StreamFactory(contentResolver2)).a(Uri.class, ParcelFileDescriptor.class, new UriLoader.FileDescriptorFactory(contentResolver2)).a(Uri.class, AssetFileDescriptor.class, new UriLoader.AssetFileDescriptorFactory(contentResolver2)).a(Uri.class, InputStream.class, new UrlUriLoader.StreamFactory()).a(URL.class, InputStream.class, new UrlLoader.StreamFactory()).a(Uri.class, File.class, new MediaStoreFileLoader.Factory(context3)).a(GlideUrl.class, InputStream.class, new HttpGlideUrlLoader.Factory()).a(cls6, ByteBuffer.class, new ByteArrayLoader.ByteBufferFactory()).a(cls6, InputStream.class, new ByteArrayLoader.StreamFactory()).a(Uri.class, Uri.class, UnitModelLoader.Factory.b()).a(Drawable.class, Drawable.class, UnitModelLoader.Factory.b()).a(Drawable.class, Drawable.class, new UnitDrawableDecoder()).a(Bitmap.class, BitmapDrawable.class, new BitmapDrawableTranscoder(resources)).a(Bitmap.class, cls6, bitmapBytesTranscoder2).a(Drawable.class, cls6, new DrawableBytesTranscoder(bitmapPool2, bitmapBytesTranscoder2, gifDrawableBytesTranscoder2)).a(GifDrawable.class, cls6, gifDrawableBytesTranscoder2);
        if (Build.VERSION.SDK_INT >= 23) {
            ResourceDecoder<ByteBuffer, Bitmap> b2 = VideoDecoder.b(bitmapPool);
            this.d.a(ByteBuffer.class, Bitmap.class, b2);
            this.d.a(ByteBuffer.class, BitmapDrawable.class, new BitmapDrawableDecoder(resources, b2));
        }
        Context context4 = context;
        ArrayPool arrayPool3 = arrayPool;
        this.c = new GlideContext(context4, arrayPool3, this.d, new ImageViewTargetFactory(), requestOptionsFactory, map, list, engine, glideExperiments, i2);
    }

    public static Glide a(Context context) {
        if (i == null) {
            GeneratedAppGlideModule b2 = b(context.getApplicationContext());
            synchronized (Glide.class) {
                if (i == null) {
                    a(context, b2);
                }
            }
        }
        return i;
    }

    private static void b(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        a(context, new GlideBuilder(), generatedAppGlideModule);
    }

    public BitmapPool c() {
        return this.f2104a;
    }

    /* access modifiers changed from: package-private */
    public ConnectivityMonitorFactory d() {
        return this.g;
    }

    public Context e() {
        return this.c.getBaseContext();
    }

    /* access modifiers changed from: package-private */
    public GlideContext f() {
        return this.c;
    }

    public Registry g() {
        return this.d;
    }

    public RequestManagerRetriever h() {
        return this.f;
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public void onLowMemory() {
        a();
    }

    public void onTrimMemory(int i2) {
        a(i2);
    }

    private static GeneratedAppGlideModule b(Context context) {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(new Class[]{Context.class}).newInstance(new Object[]{context.getApplicationContext()});
        } catch (ClassNotFoundException unused) {
            if (Log.isLoggable("Glide", 5)) {
                Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
            return null;
        } catch (InstantiationException e2) {
            a((Exception) e2);
            throw null;
        } catch (IllegalAccessException e3) {
            a((Exception) e3);
            throw null;
        } catch (NoSuchMethodException e4) {
            a((Exception) e4);
            throw null;
        } catch (InvocationTargetException e5) {
            a((Exception) e5);
            throw null;
        }
    }

    private static RequestManagerRetriever c(Context context) {
        Preconditions.a(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return a(context).h();
    }

    public static RequestManager d(Context context) {
        return c(context).a(context);
    }

    private static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!j) {
            j = true;
            b(context, generatedAppGlideModule);
            j = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    public ArrayPool b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void b(RequestManager requestManager) {
        synchronized (this.h) {
            if (this.h.contains(requestManager)) {
                this.h.remove(requestManager);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    private static void a(Context context, GlideBuilder glideBuilder, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<GlideModule> emptyList = Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.a()) {
            emptyList = new ManifestParser(applicationContext).a();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.b().isEmpty()) {
            Set<Class<?>> b2 = generatedAppGlideModule.b();
            Iterator<GlideModule> it2 = emptyList.iterator();
            while (it2.hasNext()) {
                GlideModule next = it2.next();
                if (b2.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it2.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            for (GlideModule glideModule : emptyList) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + glideModule.getClass());
            }
        }
        glideBuilder.a(generatedAppGlideModule != null ? generatedAppGlideModule.c() : null);
        for (GlideModule a2 : emptyList) {
            a2.a(applicationContext, glideBuilder);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, glideBuilder);
        }
        Glide a3 = glideBuilder.a(applicationContext);
        for (GlideModule next2 : emptyList) {
            try {
                next2.a(applicationContext, a3, a3.d);
            } catch (AbstractMethodError e2) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + next2.getClass().getName(), e2);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, a3, a3.d);
        }
        applicationContext.registerComponentCallbacks(a3);
        i = a3;
    }

    private static void a(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    public void a() {
        Util.a();
        this.b.a();
        this.f2104a.a();
        this.e.a();
    }

    public void a(int i2) {
        Util.a();
        synchronized (this.h) {
            for (RequestManager onTrimMemory : this.h) {
                onTrimMemory.onTrimMemory(i2);
            }
        }
        this.b.a(i2);
        this.f2104a.a(i2);
        this.e.a(i2);
    }

    public static RequestManager a(FragmentActivity fragmentActivity) {
        return c(fragmentActivity).a(fragmentActivity);
    }

    public static RequestManager a(Fragment fragment) {
        return c(fragment.getContext()).a(fragment);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Target<?> target) {
        synchronized (this.h) {
            for (RequestManager b2 : this.h) {
                if (b2.b(target)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RequestManager requestManager) {
        synchronized (this.h) {
            if (!this.h.contains(requestManager)) {
                this.h.add(requestManager);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
