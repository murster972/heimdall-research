package com.bumptech.glide.load;

import androidx.collection.ArrayMap;
import com.bumptech.glide.util.CachedHashCodeArrayMap;
import java.security.MessageDigest;

public final class Options implements Key {
    private final ArrayMap<Option<?>, Object> b = new CachedHashCodeArrayMap();

    public void a(Options options) {
        this.b.a(options.b);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Options) {
            return this.b.equals(((Options) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        return "Options{values=" + this.b + '}';
    }

    public <T> Options a(Option<T> option, T t) {
        this.b.put(option, t);
        return this;
    }

    public <T> T a(Option<T> option) {
        return this.b.containsKey(option) ? this.b.get(option) : option.a();
    }

    public void a(MessageDigest messageDigest) {
        for (int i = 0; i < this.b.size(); i++) {
            a(this.b.b(i), this.b.d(i), messageDigest);
        }
    }

    private static <T> void a(Option<T> option, Object obj, MessageDigest messageDigest) {
        option.a(obj, messageDigest);
    }
}
