package com.bumptech.glide.load.data;

import java.io.IOException;

public interface DataRewinder<T> {

    public interface Factory<T> {
        DataRewinder<T> a(T t);

        Class<T> a();
    }

    T a() throws IOException;

    void b();
}
