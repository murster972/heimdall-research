package com.bumptech.glide.load.data.mediastore;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.ExifOrientationStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ThumbFetcher implements DataFetcher<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final Uri f2155a;
    private final ThumbnailStreamOpener b;
    private InputStream c;

    static class ImageThumbnailQuery implements ThumbnailQuery {
        private static final String[] b = {"_data"};

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f2156a;

        ImageThumbnailQuery(ContentResolver contentResolver) {
            this.f2156a = contentResolver;
        }

        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f2156a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, (String) null);
        }
    }

    static class VideoThumbnailQuery implements ThumbnailQuery {
        private static final String[] b = {"_data"};

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f2157a;

        VideoThumbnailQuery(ContentResolver contentResolver) {
            this.f2157a = contentResolver;
        }

        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f2157a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, (String) null);
        }
    }

    ThumbFetcher(Uri uri, ThumbnailStreamOpener thumbnailStreamOpener) {
        this.f2155a = uri;
        this.b = thumbnailStreamOpener;
    }

    public static ThumbFetcher a(Context context, Uri uri) {
        return a(context, uri, new ImageThumbnailQuery(context.getContentResolver()));
    }

    public static ThumbFetcher b(Context context, Uri uri) {
        return a(context, uri, new VideoThumbnailQuery(context.getContentResolver()));
    }

    private InputStream d() throws FileNotFoundException {
        InputStream b2 = this.b.b(this.f2155a);
        int a2 = b2 != null ? this.b.a(this.f2155a) : -1;
        return a2 != -1 ? new ExifOrientationStream(b2, a2) : b2;
    }

    public DataSource c() {
        return DataSource.LOCAL;
    }

    public void cancel() {
    }

    private static ThumbFetcher a(Context context, Uri uri, ThumbnailQuery thumbnailQuery) {
        return new ThumbFetcher(uri, new ThumbnailStreamOpener(Glide.a(context).g().a(), thumbnailQuery, Glide.a(context).b(), context.getContentResolver()));
    }

    public void b() {
        InputStream inputStream = this.c;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }

    public void a(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
        try {
            this.c = d();
            dataCallback.a(this.c);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            dataCallback.a((Exception) e);
        }
    }

    public Class<InputStream> a() {
        return InputStream.class;
    }
}
