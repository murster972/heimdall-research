package com.bumptech.glide.load.data;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;

public interface DataFetcher<T> {

    public interface DataCallback<T> {
        void a(Exception exc);

        void a(T t);
    }

    Class<T> a();

    void a(Priority priority, DataCallback<? super T> dataCallback);

    void b();

    DataSource c();

    void cancel();
}
