package com.bumptech.glide.load.data;

import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.google.ar.core.ImageMetadata;
import java.io.IOException;
import java.io.OutputStream;

public final class BufferedOutputStream extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final OutputStream f2145a;
    private byte[] b;
    private ArrayPool c;
    private int d;

    public BufferedOutputStream(OutputStream outputStream, ArrayPool arrayPool) {
        this(outputStream, arrayPool, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE);
    }

    private void s() throws IOException {
        int i = this.d;
        if (i > 0) {
            this.f2145a.write(this.b, 0, i);
            this.d = 0;
        }
    }

    private void t() throws IOException {
        if (this.d == this.b.length) {
            s();
        }
    }

    private void u() {
        byte[] bArr = this.b;
        if (bArr != null) {
            this.c.put(bArr);
            this.b = null;
        }
    }

    /* JADX INFO: finally extract failed */
    public void close() throws IOException {
        try {
            flush();
            this.f2145a.close();
            u();
        } catch (Throwable th) {
            this.f2145a.close();
            throw th;
        }
    }

    public void flush() throws IOException {
        s();
        this.f2145a.flush();
    }

    public void write(int i) throws IOException {
        byte[] bArr = this.b;
        int i2 = this.d;
        this.d = i2 + 1;
        bArr[i2] = (byte) i;
        t();
    }

    BufferedOutputStream(OutputStream outputStream, ArrayPool arrayPool, int i) {
        this.f2145a = outputStream;
        this.c = arrayPool;
        this.b = (byte[]) arrayPool.a(i, byte[].class);
    }

    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            if (this.d != 0 || i4 < this.b.length) {
                int min = Math.min(i4, this.b.length - this.d);
                System.arraycopy(bArr, i5, this.b, this.d, min);
                this.d += min;
                i3 += min;
                t();
            } else {
                this.f2145a.write(bArr, i5, i4);
                return;
            }
        } while (i3 < i2);
    }
}
