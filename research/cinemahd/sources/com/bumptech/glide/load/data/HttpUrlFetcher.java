package com.bumptech.glide.load.data;

import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.HttpException;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.util.ContentLengthInputStream;
import com.bumptech.glide.util.LogTime;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

public class HttpUrlFetcher implements DataFetcher<InputStream> {
    static final HttpUrlConnectionFactory g = new DefaultHttpUrlConnectionFactory();

    /* renamed from: a  reason: collision with root package name */
    private final GlideUrl f2149a;
    private final int b;
    private final HttpUrlConnectionFactory c;
    private HttpURLConnection d;
    private InputStream e;
    private volatile boolean f;

    private static class DefaultHttpUrlConnectionFactory implements HttpUrlConnectionFactory {
        DefaultHttpUrlConnectionFactory() {
        }

        public HttpURLConnection a(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    }

    interface HttpUrlConnectionFactory {
        HttpURLConnection a(URL url) throws IOException;
    }

    public HttpUrlFetcher(GlideUrl glideUrl, int i) {
        this(glideUrl, i, g);
    }

    private static boolean b(int i) {
        return i / 100 == 3;
    }

    public void a(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
        StringBuilder sb;
        long a2 = LogTime.a();
        try {
            dataCallback.a(a(this.f2149a.d(), 0, (URL) null, this.f2149a.b()));
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(LogTime.a(a2));
                Log.v("HttpUrlFetcher", sb.toString());
            }
        } catch (IOException e2) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to load data for url", e2);
            }
            dataCallback.a((Exception) e2);
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
            }
        } catch (Throwable th) {
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + LogTime.a(a2));
            }
            throw th;
        }
    }

    public DataSource c() {
        return DataSource.REMOTE;
    }

    public void cancel() {
        this.f = true;
    }

    HttpUrlFetcher(GlideUrl glideUrl, int i, HttpUrlConnectionFactory httpUrlConnectionFactory) {
        this.f2149a = glideUrl;
        this.b = i;
        this.c = httpUrlConnectionFactory;
    }

    private InputStream b(HttpURLConnection httpURLConnection) throws HttpException {
        try {
            if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
                this.e = ContentLengthInputStream.a(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
            } else {
                if (Log.isLoggable("HttpUrlFetcher", 3)) {
                    Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
                }
                this.e = httpURLConnection.getInputStream();
            }
            return this.e;
        } catch (IOException e2) {
            throw new HttpException("Failed to obtain InputStream", a(httpURLConnection), e2);
        }
    }

    public void b() {
        InputStream inputStream = this.e;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
        HttpURLConnection httpURLConnection = this.d;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.d = null;
    }

    private InputStream a(URL url, int i, URL url2, Map<String, String> map) throws HttpException {
        if (i < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new HttpException("In re-direct loop", -1);
                    }
                } catch (URISyntaxException unused) {
                }
            }
            this.d = a(url, map);
            try {
                this.d.connect();
                this.e = this.d.getInputStream();
                if (this.f) {
                    return null;
                }
                int a2 = a(this.d);
                if (a(a2)) {
                    return b(this.d);
                }
                if (b(a2)) {
                    String headerField = this.d.getHeaderField("Location");
                    if (!TextUtils.isEmpty(headerField)) {
                        try {
                            URL url3 = new URL(url, headerField);
                            b();
                            return a(url3, i + 1, url, map);
                        } catch (MalformedURLException e2) {
                            throw new HttpException("Bad redirect url: " + headerField, a2, e2);
                        }
                    } else {
                        throw new HttpException("Received empty or null redirect url", a2);
                    }
                } else if (a2 == -1) {
                    throw new HttpException(a2);
                } else {
                    try {
                        throw new HttpException(this.d.getResponseMessage(), a2);
                    } catch (IOException e3) {
                        throw new HttpException("Failed to get a response message", a2, e3);
                    }
                }
            } catch (IOException e4) {
                throw new HttpException("Failed to connect or obtain data", a(this.d), e4);
            }
        } else {
            throw new HttpException("Too many (> 5) redirects!", -1);
        }
    }

    private static int a(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getResponseCode();
        } catch (IOException e2) {
            if (!Log.isLoggable("HttpUrlFetcher", 3)) {
                return -1;
            }
            Log.d("HttpUrlFetcher", "Failed to get a response code", e2);
            return -1;
        }
    }

    private HttpURLConnection a(URL url, Map<String, String> map) throws HttpException {
        try {
            HttpURLConnection a2 = this.c.a(url);
            for (Map.Entry next : map.entrySet()) {
                a2.addRequestProperty((String) next.getKey(), (String) next.getValue());
            }
            a2.setConnectTimeout(this.b);
            a2.setReadTimeout(this.b);
            a2.setUseCaches(false);
            a2.setDoInput(true);
            a2.setInstanceFollowRedirects(false);
            return a2;
        } catch (IOException e2) {
            throw new HttpException("URL.openConnection threw", 0, e2);
        }
    }

    private static boolean a(int i) {
        return i / 100 == 2;
    }

    public Class<InputStream> a() {
        return InputStream.class;
    }
}
