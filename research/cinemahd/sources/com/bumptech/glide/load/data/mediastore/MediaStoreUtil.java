package com.bumptech.glide.load.data.mediastore;

import android.net.Uri;
import com.vungle.warren.model.Advertisement;

public final class MediaStoreUtil {
    private MediaStoreUtil() {
    }

    public static boolean a(int i, int i2) {
        return i != Integer.MIN_VALUE && i2 != Integer.MIN_VALUE && i <= 512 && i2 <= 384;
    }

    public static boolean a(Uri uri) {
        return b(uri) && !d(uri);
    }

    public static boolean b(Uri uri) {
        return uri != null && "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    public static boolean c(Uri uri) {
        return b(uri) && d(uri);
    }

    private static boolean d(Uri uri) {
        return uri.getPathSegments().contains(Advertisement.KEY_VIDEO);
    }
}
