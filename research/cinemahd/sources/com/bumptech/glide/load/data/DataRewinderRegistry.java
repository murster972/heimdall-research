package com.bumptech.glide.load.data;

import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.util.Preconditions;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DataRewinderRegistry {
    private static final DataRewinder.Factory<?> b = new DataRewinder.Factory<Object>() {
        public DataRewinder<Object> a(Object obj) {
            return new DefaultRewinder(obj);
        }

        public Class<Object> a() {
            throw new UnsupportedOperationException("Not implemented");
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Map<Class<?>, DataRewinder.Factory<?>> f2146a = new HashMap();

    private static final class DefaultRewinder implements DataRewinder<Object> {

        /* renamed from: a  reason: collision with root package name */
        private final Object f2147a;

        DefaultRewinder(Object obj) {
            this.f2147a = obj;
        }

        public Object a() {
            return this.f2147a;
        }

        public void b() {
        }
    }

    public synchronized void a(DataRewinder.Factory<?> factory) {
        this.f2146a.put(factory.a(), factory);
    }

    public synchronized <T> DataRewinder<T> a(T t) {
        DataRewinder.Factory<?> factory;
        Preconditions.a(t);
        factory = this.f2146a.get(t.getClass());
        if (factory == null) {
            Iterator<DataRewinder.Factory<?>> it2 = this.f2146a.values().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                DataRewinder.Factory<?> next = it2.next();
                if (next.a().isAssignableFrom(t.getClass())) {
                    factory = next;
                    break;
                }
            }
        }
        if (factory == null) {
            factory = b;
        }
        return factory.a(t);
    }
}
