package com.bumptech.glide.load.data;

import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class InputStreamRewinder implements DataRewinder<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final RecyclableBufferedInputStream f2150a;

    public static final class Factory implements DataRewinder.Factory<InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final ArrayPool f2151a;

        public Factory(ArrayPool arrayPool) {
            this.f2151a = arrayPool;
        }

        public DataRewinder<InputStream> a(InputStream inputStream) {
            return new InputStreamRewinder(inputStream, this.f2151a);
        }

        public Class<InputStream> a() {
            return InputStream.class;
        }
    }

    public InputStreamRewinder(InputStream inputStream, ArrayPool arrayPool) {
        this.f2150a = new RecyclableBufferedInputStream(inputStream, arrayPool);
        this.f2150a.mark(5242880);
    }

    public void b() {
        this.f2150a.t();
    }

    public void c() {
        this.f2150a.s();
    }

    public InputStream a() throws IOException {
        this.f2150a.reset();
        return this.f2150a;
    }
}
