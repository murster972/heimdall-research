package com.bumptech.glide.load.model.stream;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.mediastore.MediaStoreUtil;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.signature.ObjectKey;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public final class QMediaStoreUriLoader<DataT> implements ModelLoader<Uri, DataT> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2275a;
    private final ModelLoader<File, DataT> b;
    private final ModelLoader<Uri, DataT> c;
    private final Class<DataT> d;

    private static abstract class Factory<DataT> implements ModelLoaderFactory<Uri, DataT> {

        /* renamed from: a  reason: collision with root package name */
        private final Context f2276a;
        private final Class<DataT> b;

        Factory(Context context, Class<DataT> cls) {
            this.f2276a = context;
            this.b = cls;
        }

        public final ModelLoader<Uri, DataT> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new QMediaStoreUriLoader(this.f2276a, multiModelLoaderFactory.a(File.class, this.b), multiModelLoaderFactory.a(Uri.class, this.b), this.b);
        }

        public final void a() {
        }
    }

    public static final class FileDescriptorFactory extends Factory<ParcelFileDescriptor> {
        public FileDescriptorFactory(Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }

    public static final class InputStreamFactory extends Factory<InputStream> {
        public InputStreamFactory(Context context) {
            super(context, InputStream.class);
        }
    }

    QMediaStoreUriLoader(Context context, ModelLoader<File, DataT> modelLoader, ModelLoader<Uri, DataT> modelLoader2, Class<DataT> cls) {
        this.f2275a = context.getApplicationContext();
        this.b = modelLoader;
        this.c = modelLoader2;
        this.d = cls;
    }

    public ModelLoader.LoadData<DataT> a(Uri uri, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(uri), new QMediaStoreUriFetcher(this.f2275a, this.b, this.c, uri, i, i2, options, this.d));
    }

    public boolean a(Uri uri) {
        return Build.VERSION.SDK_INT >= 29 && MediaStoreUtil.b(uri);
    }

    private static final class QMediaStoreUriFetcher<DataT> implements DataFetcher<DataT> {
        private static final String[] k = {"_data"};

        /* renamed from: a  reason: collision with root package name */
        private final Context f2277a;
        private final ModelLoader<File, DataT> b;
        private final ModelLoader<Uri, DataT> c;
        private final Uri d;
        private final int e;
        private final int f;
        private final Options g;
        private final Class<DataT> h;
        private volatile boolean i;
        private volatile DataFetcher<DataT> j;

        QMediaStoreUriFetcher(Context context, ModelLoader<File, DataT> modelLoader, ModelLoader<Uri, DataT> modelLoader2, Uri uri, int i2, int i3, Options options, Class<DataT> cls) {
            this.f2277a = context.getApplicationContext();
            this.b = modelLoader;
            this.c = modelLoader2;
            this.d = uri;
            this.e = i2;
            this.f = i3;
            this.g = options;
            this.h = cls;
        }

        private ModelLoader.LoadData<DataT> d() throws FileNotFoundException {
            if (Environment.isExternalStorageLegacy()) {
                return this.b.a(a(this.d), this.e, this.f, this.g);
            }
            return this.c.a(f() ? MediaStore.setRequireOriginal(this.d) : this.d, this.e, this.f, this.g);
        }

        private DataFetcher<DataT> e() throws FileNotFoundException {
            ModelLoader.LoadData d2 = d();
            if (d2 != null) {
                return d2.c;
            }
            return null;
        }

        private boolean f() {
            return this.f2277a.checkSelfPermission("android.permission.ACCESS_MEDIA_LOCATION") == 0;
        }

        public void a(Priority priority, DataFetcher.DataCallback<? super DataT> dataCallback) {
            try {
                DataFetcher<DataT> e2 = e();
                if (e2 == null) {
                    dataCallback.a((Exception) new IllegalArgumentException("Failed to build fetcher for: " + this.d));
                    return;
                }
                this.j = e2;
                if (this.i) {
                    cancel();
                } else {
                    e2.a(priority, dataCallback);
                }
            } catch (FileNotFoundException e3) {
                dataCallback.a((Exception) e3);
            }
        }

        public void b() {
            DataFetcher<DataT> dataFetcher = this.j;
            if (dataFetcher != null) {
                dataFetcher.b();
            }
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
            this.i = true;
            DataFetcher<DataT> dataFetcher = this.j;
            if (dataFetcher != null) {
                dataFetcher.cancel();
            }
        }

        public Class<DataT> a() {
            return this.h;
        }

        private File a(Uri uri) throws FileNotFoundException {
            Cursor cursor = null;
            try {
                cursor = this.f2277a.getContentResolver().query(uri, k, (String) null, (String[]) null, (String) null);
                if (cursor == null || !cursor.moveToFirst()) {
                    throw new FileNotFoundException("Failed to media store entry for: " + uri);
                }
                String string = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
                if (!TextUtils.isEmpty(string)) {
                    return new File(string);
                }
                throw new FileNotFoundException("File path was empty in media store for: " + uri);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }
}
