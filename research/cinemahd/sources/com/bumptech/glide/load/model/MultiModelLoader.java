package com.bumptech.glide.load.model;

import androidx.core.util.Pools$Pool;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class MultiModelLoader<Model, Data> implements ModelLoader<Model, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final List<ModelLoader<Model, Data>> f2249a;
    private final Pools$Pool<List<Throwable>> b;

    MultiModelLoader(List<ModelLoader<Model, Data>> list, Pools$Pool<List<Throwable>> pools$Pool) {
        this.f2249a = list;
        this.b = pools$Pool;
    }

    public ModelLoader.LoadData<Data> a(Model model, int i, int i2, Options options) {
        ModelLoader.LoadData a2;
        int size = this.f2249a.size();
        ArrayList arrayList = new ArrayList(size);
        Key key = null;
        for (int i3 = 0; i3 < size; i3++) {
            ModelLoader modelLoader = this.f2249a.get(i3);
            if (modelLoader.a(model) && (a2 = modelLoader.a(model, i, i2, options)) != null) {
                key = a2.f2245a;
                arrayList.add(a2.c);
            }
        }
        if (arrayList.isEmpty() || key == null) {
            return null;
        }
        return new ModelLoader.LoadData<>(key, new MultiFetcher(arrayList, this.b));
    }

    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.f2249a.toArray()) + '}';
    }

    static class MultiFetcher<Data> implements DataFetcher<Data>, DataFetcher.DataCallback<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final List<DataFetcher<Data>> f2250a;
        private final Pools$Pool<List<Throwable>> b;
        private int c = 0;
        private Priority d;
        private DataFetcher.DataCallback<? super Data> e;
        private List<Throwable> f;
        private boolean g;

        MultiFetcher(List<DataFetcher<Data>> list, Pools$Pool<List<Throwable>> pools$Pool) {
            this.b = pools$Pool;
            Preconditions.a(list);
            this.f2250a = list;
        }

        private void d() {
            if (!this.g) {
                if (this.c < this.f2250a.size() - 1) {
                    this.c++;
                    a(this.d, this.e);
                    return;
                }
                Preconditions.a(this.f);
                this.e.a((Exception) new GlideException("Fetch failed", (List<Throwable>) new ArrayList(this.f)));
            }
        }

        public void a(Priority priority, DataFetcher.DataCallback<? super Data> dataCallback) {
            this.d = priority;
            this.e = dataCallback;
            this.f = this.b.acquire();
            this.f2250a.get(this.c).a(priority, this);
            if (this.g) {
                cancel();
            }
        }

        public void b() {
            List<Throwable> list = this.f;
            if (list != null) {
                this.b.release(list);
            }
            this.f = null;
            for (DataFetcher<Data> b2 : this.f2250a) {
                b2.b();
            }
        }

        public DataSource c() {
            return this.f2250a.get(0).c();
        }

        public void cancel() {
            this.g = true;
            for (DataFetcher<Data> cancel : this.f2250a) {
                cancel.cancel();
            }
        }

        public Class<Data> a() {
            return this.f2250a.get(0).a();
        }

        public void a(Data data) {
            if (data != null) {
                this.e.a(data);
            } else {
                d();
            }
        }

        public void a(Exception exc) {
            List<Throwable> list = this.f;
            Preconditions.a(list);
            list.add(exc);
            d();
        }
    }

    public boolean a(Model model) {
        for (ModelLoader<Model, Data> a2 : this.f2249a) {
            if (a2.a(model)) {
                return true;
            }
        }
        return false;
    }
}
