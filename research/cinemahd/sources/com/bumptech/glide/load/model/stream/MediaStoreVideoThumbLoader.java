package com.bumptech.glide.load.model.stream;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.mediastore.MediaStoreUtil;
import com.bumptech.glide.load.data.mediastore.ThumbFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.load.resource.bitmap.VideoDecoder;
import com.bumptech.glide.signature.ObjectKey;
import java.io.InputStream;

public class MediaStoreVideoThumbLoader implements ModelLoader<Uri, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2273a;

    public static class Factory implements ModelLoaderFactory<Uri, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final Context f2274a;

        public Factory(Context context) {
            this.f2274a = context;
        }

        public ModelLoader<Uri, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new MediaStoreVideoThumbLoader(this.f2274a);
        }

        public void a() {
        }
    }

    public MediaStoreVideoThumbLoader(Context context) {
        this.f2273a = context.getApplicationContext();
    }

    public ModelLoader.LoadData<InputStream> a(Uri uri, int i, int i2, Options options) {
        if (!MediaStoreUtil.a(i, i2) || !a(options)) {
            return null;
        }
        return new ModelLoader.LoadData<>(new ObjectKey(uri), ThumbFetcher.b(this.f2273a, uri));
    }

    private boolean a(Options options) {
        Long l = (Long) options.a(VideoDecoder.d);
        return l != null && l.longValue() == -1;
    }

    public boolean a(Uri uri) {
        return MediaStoreUtil.c(uri);
    }
}
