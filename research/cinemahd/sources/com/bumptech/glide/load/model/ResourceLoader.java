package com.bumptech.glide.load.model;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import java.io.InputStream;

public class ResourceLoader<Data> implements ModelLoader<Integer, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final ModelLoader<Uri, Data> f2253a;
    private final Resources b;

    public static final class AssetFileDescriptorFactory implements ModelLoaderFactory<Integer, AssetFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final Resources f2254a;

        public AssetFileDescriptorFactory(Resources resources) {
            this.f2254a = resources;
        }

        public ModelLoader<Integer, AssetFileDescriptor> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ResourceLoader(this.f2254a, multiModelLoaderFactory.a(Uri.class, AssetFileDescriptor.class));
        }

        public void a() {
        }
    }

    public static class FileDescriptorFactory implements ModelLoaderFactory<Integer, ParcelFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final Resources f2255a;

        public FileDescriptorFactory(Resources resources) {
            this.f2255a = resources;
        }

        public ModelLoader<Integer, ParcelFileDescriptor> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ResourceLoader(this.f2255a, multiModelLoaderFactory.a(Uri.class, ParcelFileDescriptor.class));
        }

        public void a() {
        }
    }

    public static class StreamFactory implements ModelLoaderFactory<Integer, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final Resources f2256a;

        public StreamFactory(Resources resources) {
            this.f2256a = resources;
        }

        public ModelLoader<Integer, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ResourceLoader(this.f2256a, multiModelLoaderFactory.a(Uri.class, InputStream.class));
        }

        public void a() {
        }
    }

    public static class UriFactory implements ModelLoaderFactory<Integer, Uri> {

        /* renamed from: a  reason: collision with root package name */
        private final Resources f2257a;

        public UriFactory(Resources resources) {
            this.f2257a = resources;
        }

        public ModelLoader<Integer, Uri> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ResourceLoader(this.f2257a, UnitModelLoader.a());
        }

        public void a() {
        }
    }

    public ResourceLoader(Resources resources, ModelLoader<Uri, Data> modelLoader) {
        this.b = resources;
        this.f2253a = modelLoader;
    }

    private Uri b(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (!Log.isLoggable("ResourceLoader", 5)) {
                return null;
            }
            Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            return null;
        }
    }

    public boolean a(Integer num) {
        return true;
    }

    public ModelLoader.LoadData<Data> a(Integer num, int i, int i2, Options options) {
        Uri b2 = b(num);
        if (b2 == null) {
            return null;
        }
        return this.f2253a.a(b2, i, i2, options);
    }
}
