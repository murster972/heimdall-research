package com.bumptech.glide.load.model;

import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.util.Util;
import java.util.Queue;

public class ModelCache<A, B> {

    /* renamed from: a  reason: collision with root package name */
    private final LruCache<ModelKey<A>, B> f2243a;

    public ModelCache() {
        this(250);
    }

    public B a(A a2, int i, int i2) {
        ModelKey a3 = ModelKey.a(a2, i, i2);
        B a4 = this.f2243a.a(a3);
        a3.a();
        return a4;
    }

    public ModelCache(long j) {
        this.f2243a = new LruCache<ModelKey<A>, B>(this, j) {
            /* access modifiers changed from: protected */
            public void a(ModelKey<A> modelKey, B b) {
                modelKey.a();
            }
        };
    }

    public void a(A a2, int i, int i2, B b) {
        this.f2243a.b(ModelKey.a(a2, i, i2), b);
    }

    static final class ModelKey<A> {
        private static final Queue<ModelKey<?>> d = Util.a(0);

        /* renamed from: a  reason: collision with root package name */
        private int f2244a;
        private int b;
        private A c;

        private ModelKey() {
        }

        static <A> ModelKey<A> a(A a2, int i, int i2) {
            ModelKey<A> poll;
            synchronized (d) {
                poll = d.poll();
            }
            if (poll == null) {
                poll = new ModelKey<>();
            }
            poll.b(a2, i, i2);
            return poll;
        }

        private void b(A a2, int i, int i2) {
            this.c = a2;
            this.b = i;
            this.f2244a = i2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof ModelKey)) {
                return false;
            }
            ModelKey modelKey = (ModelKey) obj;
            if (this.b == modelKey.b && this.f2244a == modelKey.f2244a && this.c.equals(modelKey.c)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((this.f2244a * 31) + this.b) * 31) + this.c.hashCode();
        }

        public void a() {
            synchronized (d) {
                d.offer(this);
            }
        }
    }
}
