package com.bumptech.glide.load.model;

import android.net.Uri;
import android.text.TextUtils;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Preconditions;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;

public class GlideUrl implements Key {
    private final Headers b;
    private final URL c;
    private final String d;
    private String e;
    private URL f;
    private volatile byte[] g;
    private int h;

    public GlideUrl(URL url) {
        this(url, Headers.f2237a);
    }

    private byte[] e() {
        if (this.g == null) {
            this.g = a().getBytes(Key.f2141a);
        }
        return this.g;
    }

    private String f() {
        if (TextUtils.isEmpty(this.e)) {
            String str = this.d;
            if (TextUtils.isEmpty(str)) {
                URL url = this.c;
                Preconditions.a(url);
                str = url.toString();
            }
            this.e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.e;
    }

    private URL g() throws MalformedURLException {
        if (this.f == null) {
            this.f = new URL(f());
        }
        return this.f;
    }

    public String a() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        URL url = this.c;
        Preconditions.a(url);
        return url.toString();
    }

    public Map<String, String> b() {
        return this.b.a();
    }

    public String c() {
        return f();
    }

    public URL d() throws MalformedURLException {
        return g();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof GlideUrl)) {
            return false;
        }
        GlideUrl glideUrl = (GlideUrl) obj;
        if (!a().equals(glideUrl.a()) || !this.b.equals(glideUrl.b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.h == 0) {
            this.h = a().hashCode();
            this.h = (this.h * 31) + this.b.hashCode();
        }
        return this.h;
    }

    public String toString() {
        return a();
    }

    public GlideUrl(String str) {
        this(str, Headers.f2237a);
    }

    public void a(MessageDigest messageDigest) {
        messageDigest.update(e());
    }

    public GlideUrl(URL url, Headers headers) {
        Preconditions.a(url);
        this.c = url;
        this.d = null;
        Preconditions.a(headers);
        this.b = headers;
    }

    public GlideUrl(String str, Headers headers) {
        this.c = null;
        Preconditions.a(str);
        this.d = str;
        Preconditions.a(headers);
        this.b = headers;
    }
}
