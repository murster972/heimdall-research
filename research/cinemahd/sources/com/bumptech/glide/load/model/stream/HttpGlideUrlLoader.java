package com.bumptech.glide.load.model.stream;

import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.HttpUrlFetcher;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelCache;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;

public class HttpGlideUrlLoader implements ModelLoader<GlideUrl, InputStream> {
    public static final Option<Integer> b = Option.a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);

    /* renamed from: a  reason: collision with root package name */
    private final ModelCache<GlideUrl, GlideUrl> f2269a;

    public static class Factory implements ModelLoaderFactory<GlideUrl, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final ModelCache<GlideUrl, GlideUrl> f2270a = new ModelCache<>(500);

        public ModelLoader<GlideUrl, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new HttpGlideUrlLoader(this.f2270a);
        }

        public void a() {
        }
    }

    public HttpGlideUrlLoader() {
        this((ModelCache<GlideUrl, GlideUrl>) null);
    }

    public boolean a(GlideUrl glideUrl) {
        return true;
    }

    public HttpGlideUrlLoader(ModelCache<GlideUrl, GlideUrl> modelCache) {
        this.f2269a = modelCache;
    }

    public ModelLoader.LoadData<InputStream> a(GlideUrl glideUrl, int i, int i2, Options options) {
        ModelCache<GlideUrl, GlideUrl> modelCache = this.f2269a;
        if (modelCache != null) {
            GlideUrl a2 = modelCache.a(glideUrl, 0, 0);
            if (a2 == null) {
                this.f2269a.a(glideUrl, 0, 0, glideUrl);
            } else {
                glideUrl = a2;
            }
        }
        return new ModelLoader.LoadData<>(glideUrl, new HttpUrlFetcher(glideUrl, ((Integer) options.a(b)).intValue()));
    }
}
