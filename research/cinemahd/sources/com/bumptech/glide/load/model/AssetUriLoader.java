package com.bumptech.glide.load.model;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.FileDescriptorAssetPathFetcher;
import com.bumptech.glide.load.data.StreamAssetPathFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;
import com.facebook.common.util.UriUtil;
import java.io.InputStream;

public class AssetUriLoader<Data> implements ModelLoader<Uri, Data> {
    private static final int c = 22;

    /* renamed from: a  reason: collision with root package name */
    private final AssetManager f2225a;
    private final AssetFetcherFactory<Data> b;

    public interface AssetFetcherFactory<Data> {
        DataFetcher<Data> a(AssetManager assetManager, String str);
    }

    public static class FileDescriptorFactory implements ModelLoaderFactory<Uri, ParcelFileDescriptor>, AssetFetcherFactory<ParcelFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final AssetManager f2226a;

        public FileDescriptorFactory(AssetManager assetManager) {
            this.f2226a = assetManager;
        }

        public ModelLoader<Uri, ParcelFileDescriptor> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new AssetUriLoader(this.f2226a, this);
        }

        public void a() {
        }

        public DataFetcher<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new FileDescriptorAssetPathFetcher(assetManager, str);
        }
    }

    public static class StreamFactory implements ModelLoaderFactory<Uri, InputStream>, AssetFetcherFactory<InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final AssetManager f2227a;

        public StreamFactory(AssetManager assetManager) {
            this.f2227a = assetManager;
        }

        public ModelLoader<Uri, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new AssetUriLoader(this.f2227a, this);
        }

        public void a() {
        }

        public DataFetcher<InputStream> a(AssetManager assetManager, String str) {
            return new StreamAssetPathFetcher(assetManager, str);
        }
    }

    public AssetUriLoader(AssetManager assetManager, AssetFetcherFactory<Data> assetFetcherFactory) {
        this.f2225a = assetManager;
        this.b = assetFetcherFactory;
    }

    public ModelLoader.LoadData<Data> a(Uri uri, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(uri), this.b.a(this.f2225a, uri.toString().substring(c)));
    }

    public boolean a(Uri uri) {
        if (!UriUtil.LOCAL_FILE_SCHEME.equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
