package com.bumptech.glide.load.model;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class LazyHeaders implements Headers {
    private final Map<String, List<LazyHeaderFactory>> b;
    private volatile Map<String, String> c;

    public static final class Builder {
        private static final String b = b();
        private static final Map<String, List<LazyHeaderFactory>> c;

        /* renamed from: a  reason: collision with root package name */
        private Map<String, List<LazyHeaderFactory>> f2238a = c;

        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(b)) {
                hashMap.put("User-Agent", Collections.singletonList(new StringHeaderFactory(b)));
            }
            c = Collections.unmodifiableMap(hashMap);
        }

        static String b() {
            String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == 9) && charAt < 127) {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }

        public LazyHeaders a() {
            return new LazyHeaders(this.f2238a);
        }
    }

    static final class StringHeaderFactory implements LazyHeaderFactory {

        /* renamed from: a  reason: collision with root package name */
        private final String f2239a;

        StringHeaderFactory(String str) {
            this.f2239a = str;
        }

        public String a() {
            return this.f2239a;
        }

        public boolean equals(Object obj) {
            if (obj instanceof StringHeaderFactory) {
                return this.f2239a.equals(((StringHeaderFactory) obj).f2239a);
            }
            return false;
        }

        public int hashCode() {
            return this.f2239a.hashCode();
        }

        public String toString() {
            return "StringHeaderFactory{value='" + this.f2239a + '\'' + '}';
        }
    }

    LazyHeaders(Map<String, List<LazyHeaderFactory>> map) {
        this.b = Collections.unmodifiableMap(map);
    }

    private Map<String, String> b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.b.entrySet()) {
            String a2 = a((List) next.getValue());
            if (!TextUtils.isEmpty(a2)) {
                hashMap.put(next.getKey(), a2);
            }
        }
        return hashMap;
    }

    public Map<String, String> a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = Collections.unmodifiableMap(b());
                }
            }
        }
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj instanceof LazyHeaders) {
            return this.b.equals(((LazyHeaders) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        return "LazyHeaders{headers=" + this.b + '}';
    }

    private String a(List<LazyHeaderFactory> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String a2 = list.get(i).a();
            if (!TextUtils.isEmpty(a2)) {
                sb.append(a2);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }
}
