package com.bumptech.glide.load.model;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.util.Preconditions;
import java.util.Collections;
import java.util.List;

public interface ModelLoader<Model, Data> {

    public static class LoadData<Data> {

        /* renamed from: a  reason: collision with root package name */
        public final Key f2245a;
        public final List<Key> b;
        public final DataFetcher<Data> c;

        public LoadData(Key key, DataFetcher<Data> dataFetcher) {
            this(key, Collections.emptyList(), dataFetcher);
        }

        public LoadData(Key key, List<Key> list, DataFetcher<Data> dataFetcher) {
            Preconditions.a(key);
            this.f2245a = key;
            Preconditions.a(list);
            this.b = list;
            Preconditions.a(dataFetcher);
            this.c = dataFetcher;
        }
    }

    LoadData<Data> a(Model model, int i, int i2, Options options);

    boolean a(Model model);
}
