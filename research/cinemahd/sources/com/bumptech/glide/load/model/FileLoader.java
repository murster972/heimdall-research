package com.bumptech.glide.load.model;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FileLoader<Data> implements ModelLoader<File, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final FileOpener<Data> f2234a;

    public static class Factory<Data> implements ModelLoaderFactory<File, Data> {

        /* renamed from: a  reason: collision with root package name */
        private final FileOpener<Data> f2235a;

        public Factory(FileOpener<Data> fileOpener) {
            this.f2235a = fileOpener;
        }

        public final ModelLoader<File, Data> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new FileLoader(this.f2235a);
        }

        public final void a() {
        }
    }

    public static class FileDescriptorFactory extends Factory<ParcelFileDescriptor> {
        public FileDescriptorFactory() {
            super(new FileOpener<ParcelFileDescriptor>() {
                public ParcelFileDescriptor a(File file) throws FileNotFoundException {
                    return ParcelFileDescriptor.open(file, 268435456);
                }

                public void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                    parcelFileDescriptor.close();
                }

                public Class<ParcelFileDescriptor> a() {
                    return ParcelFileDescriptor.class;
                }
            });
        }
    }

    public interface FileOpener<Data> {
        Class<Data> a();

        Data a(File file) throws FileNotFoundException;

        void a(Data data) throws IOException;
    }

    public static class StreamFactory extends Factory<InputStream> {
        public StreamFactory() {
            super(new FileOpener<InputStream>() {
                public InputStream a(File file) throws FileNotFoundException {
                    return new FileInputStream(file);
                }

                public void a(InputStream inputStream) throws IOException {
                    inputStream.close();
                }

                public Class<InputStream> a() {
                    return InputStream.class;
                }
            });
        }
    }

    public FileLoader(FileOpener<Data> fileOpener) {
        this.f2234a = fileOpener;
    }

    public boolean a(File file) {
        return true;
    }

    public ModelLoader.LoadData<Data> a(File file, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(file), new FileFetcher(file, this.f2234a));
    }

    private static final class FileFetcher<Data> implements DataFetcher<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final File f2236a;
        private final FileOpener<Data> b;
        private Data c;

        FileFetcher(File file, FileOpener<Data> fileOpener) {
            this.f2236a = file;
            this.b = fileOpener;
        }

        public void a(Priority priority, DataFetcher.DataCallback<? super Data> dataCallback) {
            try {
                this.c = this.b.a(this.f2236a);
                dataCallback.a(this.c);
            } catch (FileNotFoundException e) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e);
                }
                dataCallback.a((Exception) e);
            }
        }

        public void b() {
            Data data = this.c;
            if (data != null) {
                try {
                    this.b.a(data);
                } catch (IOException unused) {
                }
            }
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Data> a() {
            return this.b.a();
        }
    }
}
