package com.bumptech.glide.load.model;

import androidx.core.util.Pools$Pool;
import com.bumptech.glide.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelLoaderRegistry {

    /* renamed from: a  reason: collision with root package name */
    private final MultiModelLoaderFactory f2246a;
    private final ModelLoaderCache b;

    private static class ModelLoaderCache {

        /* renamed from: a  reason: collision with root package name */
        private final Map<Class<?>, Entry<?>> f2247a = new HashMap();

        private static class Entry<Model> {

            /* renamed from: a  reason: collision with root package name */
            final List<ModelLoader<Model, ?>> f2248a;

            public Entry(List<ModelLoader<Model, ?>> list) {
                this.f2248a = list;
            }
        }

        ModelLoaderCache() {
        }

        public void a() {
            this.f2247a.clear();
        }

        public <Model> void a(Class<Model> cls, List<ModelLoader<Model, ?>> list) {
            if (this.f2247a.put(cls, new Entry(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        public <Model> List<ModelLoader<Model, ?>> a(Class<Model> cls) {
            Entry entry = this.f2247a.get(cls);
            if (entry == null) {
                return null;
            }
            return entry.f2248a;
        }
    }

    public ModelLoaderRegistry(Pools$Pool<List<Throwable>> pools$Pool) {
        this(new MultiModelLoaderFactory(pools$Pool));
    }

    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        this.f2246a.a(cls, cls2, modelLoaderFactory);
        this.b.a();
    }

    public synchronized <Model, Data> void b(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        a(this.f2246a.b(cls, cls2, modelLoaderFactory));
        this.b.a();
    }

    private ModelLoaderRegistry(MultiModelLoaderFactory multiModelLoaderFactory) {
        this.b = new ModelLoaderCache();
        this.f2246a = multiModelLoaderFactory;
    }

    private <Model, Data> void a(List<ModelLoaderFactory<? extends Model, ? extends Data>> list) {
        for (ModelLoaderFactory<? extends Model, ? extends Data> a2 : list) {
            a2.a();
        }
    }

    private synchronized <A> List<ModelLoader<A, ?>> b(Class<A> cls) {
        List<ModelLoader<A, ?>> a2;
        a2 = this.b.a(cls);
        if (a2 == null) {
            a2 = Collections.unmodifiableList(this.f2246a.a(cls));
            this.b.a(cls, a2);
        }
        return a2;
    }

    public <A> List<ModelLoader<A, ?>> a(A a2) {
        List b2 = b(b(a2));
        if (!b2.isEmpty()) {
            int size = b2.size();
            List<ModelLoader<A, ?>> emptyList = Collections.emptyList();
            boolean z = true;
            for (int i = 0; i < size; i++) {
                ModelLoader modelLoader = (ModelLoader) b2.get(i);
                if (modelLoader.a(a2)) {
                    if (z) {
                        emptyList = new ArrayList<>(size - i);
                        z = false;
                    }
                    emptyList.add(modelLoader);
                }
            }
            if (!emptyList.isEmpty()) {
                return emptyList;
            }
            throw new Registry.NoModelLoaderAvailableException(a2, b2);
        }
        throw new Registry.NoModelLoaderAvailableException(a2);
    }

    private static <A> Class<A> b(A a2) {
        return a2.getClass();
    }

    public synchronized List<Class<?>> a(Class<?> cls) {
        return this.f2246a.b(cls);
    }
}
