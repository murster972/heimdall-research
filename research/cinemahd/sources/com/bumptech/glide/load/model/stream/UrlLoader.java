package com.bumptech.glide.load.model.stream;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import java.net.URL;

public class UrlLoader implements ModelLoader<URL, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final ModelLoader<GlideUrl, InputStream> f2278a;

    public static class StreamFactory implements ModelLoaderFactory<URL, InputStream> {
        public ModelLoader<URL, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new UrlLoader(multiModelLoaderFactory.a(GlideUrl.class, InputStream.class));
        }

        public void a() {
        }
    }

    public UrlLoader(ModelLoader<GlideUrl, InputStream> modelLoader) {
        this.f2278a = modelLoader;
    }

    public boolean a(URL url) {
        return true;
    }

    public ModelLoader.LoadData<InputStream> a(URL url, int i, int i2, Options options) {
        return this.f2278a.a(new GlideUrl(url), i, i2, options);
    }
}
