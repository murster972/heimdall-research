package com.bumptech.glide.load.model;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class ByteArrayLoader<Data> implements ModelLoader<byte[], Data> {

    /* renamed from: a  reason: collision with root package name */
    private final Converter<Data> f2228a;

    public static class ByteBufferFactory implements ModelLoaderFactory<byte[], ByteBuffer> {
        public ModelLoader<byte[], ByteBuffer> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ByteArrayLoader(new Converter<ByteBuffer>(this) {
                public ByteBuffer a(byte[] bArr) {
                    return ByteBuffer.wrap(bArr);
                }

                public Class<ByteBuffer> a() {
                    return ByteBuffer.class;
                }
            });
        }

        public void a() {
        }
    }

    public interface Converter<Data> {
        Class<Data> a();

        Data a(byte[] bArr);
    }

    public static class StreamFactory implements ModelLoaderFactory<byte[], InputStream> {
        public ModelLoader<byte[], InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ByteArrayLoader(new Converter<InputStream>(this) {
                public InputStream a(byte[] bArr) {
                    return new ByteArrayInputStream(bArr);
                }

                public Class<InputStream> a() {
                    return InputStream.class;
                }
            });
        }

        public void a() {
        }
    }

    public ByteArrayLoader(Converter<Data> converter) {
        this.f2228a = converter;
    }

    public boolean a(byte[] bArr) {
        return true;
    }

    private static class Fetcher<Data> implements DataFetcher<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final byte[] f2229a;
        private final Converter<Data> b;

        Fetcher(byte[] bArr, Converter<Data> converter) {
            this.f2229a = bArr;
            this.b = converter;
        }

        public void a(Priority priority, DataFetcher.DataCallback<? super Data> dataCallback) {
            dataCallback.a(this.b.a(this.f2229a));
        }

        public void b() {
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Data> a() {
            return this.b.a();
        }
    }

    public ModelLoader.LoadData<Data> a(byte[] bArr, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(bArr), new Fetcher(bArr, this.f2228a));
    }
}
