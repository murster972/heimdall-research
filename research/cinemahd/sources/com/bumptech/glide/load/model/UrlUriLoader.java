package com.bumptech.glide.load.model;

import android.net.Uri;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.facebook.common.util.UriUtil;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UrlUriLoader<Data> implements ModelLoader<Uri, Data> {
    private static final Set<String> b = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{UriUtil.HTTP_SCHEME, UriUtil.HTTPS_SCHEME})));

    /* renamed from: a  reason: collision with root package name */
    private final ModelLoader<GlideUrl, Data> f2267a;

    public static class StreamFactory implements ModelLoaderFactory<Uri, InputStream> {
        public ModelLoader<Uri, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new UrlUriLoader(multiModelLoaderFactory.a(GlideUrl.class, InputStream.class));
        }

        public void a() {
        }
    }

    public UrlUriLoader(ModelLoader<GlideUrl, Data> modelLoader) {
        this.f2267a = modelLoader;
    }

    public ModelLoader.LoadData<Data> a(Uri uri, int i, int i2, Options options) {
        return this.f2267a.a(new GlideUrl(uri.toString()), i, i2, options);
    }

    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
