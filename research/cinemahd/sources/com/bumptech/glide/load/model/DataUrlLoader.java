package com.bumptech.glide.load.model;

import android.util.Base64;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class DataUrlLoader<Model, Data> implements ModelLoader<Model, Data> {

    /* renamed from: a  reason: collision with root package name */
    private final DataDecoder<Data> f2231a;

    public interface DataDecoder<Data> {
        Class<Data> a();

        void a(Data data) throws IOException;

        Data decode(String str) throws IllegalArgumentException;
    }

    public static final class StreamFactory<Model> implements ModelLoaderFactory<Model, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final DataDecoder<InputStream> f2233a = new DataDecoder<InputStream>(this) {
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }

            public InputStream decode(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }

            public Class<InputStream> a() {
                return InputStream.class;
            }
        };

        public ModelLoader<Model, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new DataUrlLoader(this.f2233a);
        }

        public void a() {
        }
    }

    public DataUrlLoader(DataDecoder<Data> dataDecoder) {
        this.f2231a = dataDecoder;
    }

    public ModelLoader.LoadData<Data> a(Model model, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(model), new DataUriFetcher(model.toString(), this.f2231a));
    }

    private static final class DataUriFetcher<Data> implements DataFetcher<Data> {

        /* renamed from: a  reason: collision with root package name */
        private final String f2232a;
        private final DataDecoder<Data> b;
        private Data c;

        DataUriFetcher(String str, DataDecoder<Data> dataDecoder) {
            this.f2232a = str;
            this.b = dataDecoder;
        }

        public void a(Priority priority, DataFetcher.DataCallback<? super Data> dataCallback) {
            try {
                this.c = this.b.decode(this.f2232a);
                dataCallback.a(this.c);
            } catch (IllegalArgumentException e) {
                dataCallback.a((Exception) e);
            }
        }

        public void b() {
            try {
                this.b.a(this.c);
            } catch (IOException unused) {
            }
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Data> a() {
            return this.b.a();
        }
    }

    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }
}
