package com.bumptech.glide.load.model;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;

public class UnitModelLoader<Model> implements ModelLoader<Model, Model> {

    /* renamed from: a  reason: collision with root package name */
    private static final UnitModelLoader<?> f2260a = new UnitModelLoader<>();

    public static class Factory<Model> implements ModelLoaderFactory<Model, Model> {

        /* renamed from: a  reason: collision with root package name */
        private static final Factory<?> f2261a = new Factory<>();

        public static <T> Factory<T> b() {
            return f2261a;
        }

        public ModelLoader<Model, Model> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return UnitModelLoader.a();
        }

        public void a() {
        }
    }

    private static class UnitFetcher<Model> implements DataFetcher<Model> {

        /* renamed from: a  reason: collision with root package name */
        private final Model f2262a;

        UnitFetcher(Model model) {
            this.f2262a = model;
        }

        public void a(Priority priority, DataFetcher.DataCallback<? super Model> dataCallback) {
            dataCallback.a(this.f2262a);
        }

        public void b() {
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Model> a() {
            return this.f2262a.getClass();
        }
    }

    public static <T> UnitModelLoader<T> a() {
        return f2260a;
    }

    public boolean a(Model model) {
        return true;
    }

    public ModelLoader.LoadData<Model> a(Model model, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(model), new UnitFetcher(model));
    }
}
