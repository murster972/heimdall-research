package com.bumptech.glide.load.model;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.AssetFileDescriptorLocalUriFetcher;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.FileDescriptorLocalUriFetcher;
import com.bumptech.glide.load.data.StreamLocalUriFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;
import com.facebook.common.util.UriUtil;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UriLoader<Data> implements ModelLoader<Uri, Data> {
    private static final Set<String> b = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{UriUtil.LOCAL_FILE_SCHEME, UriUtil.QUALIFIED_RESOURCE_SCHEME, "content"})));

    /* renamed from: a  reason: collision with root package name */
    private final LocalUriFetcherFactory<Data> f2263a;

    public static final class AssetFileDescriptorFactory implements ModelLoaderFactory<Uri, AssetFileDescriptor>, LocalUriFetcherFactory<AssetFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f2264a;

        public AssetFileDescriptorFactory(ContentResolver contentResolver) {
            this.f2264a = contentResolver;
        }

        public ModelLoader<Uri, AssetFileDescriptor> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new UriLoader(this);
        }

        public void a() {
        }

        public DataFetcher<AssetFileDescriptor> a(Uri uri) {
            return new AssetFileDescriptorLocalUriFetcher(this.f2264a, uri);
        }
    }

    public static class FileDescriptorFactory implements ModelLoaderFactory<Uri, ParcelFileDescriptor>, LocalUriFetcherFactory<ParcelFileDescriptor> {

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f2265a;

        public FileDescriptorFactory(ContentResolver contentResolver) {
            this.f2265a = contentResolver;
        }

        public DataFetcher<ParcelFileDescriptor> a(Uri uri) {
            return new FileDescriptorLocalUriFetcher(this.f2265a, uri);
        }

        public void a() {
        }

        public ModelLoader<Uri, ParcelFileDescriptor> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new UriLoader(this);
        }
    }

    public interface LocalUriFetcherFactory<Data> {
        DataFetcher<Data> a(Uri uri);
    }

    public static class StreamFactory implements ModelLoaderFactory<Uri, InputStream>, LocalUriFetcherFactory<InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final ContentResolver f2266a;

        public StreamFactory(ContentResolver contentResolver) {
            this.f2266a = contentResolver;
        }

        public DataFetcher<InputStream> a(Uri uri) {
            return new StreamLocalUriFetcher(this.f2266a, uri);
        }

        public void a() {
        }

        public ModelLoader<Uri, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new UriLoader(this);
        }
    }

    public UriLoader(LocalUriFetcherFactory<Data> localUriFetcherFactory) {
        this.f2263a = localUriFetcherFactory;
    }

    public ModelLoader.LoadData<Data> a(Uri uri, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(uri), this.f2263a.a(uri));
    }

    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
