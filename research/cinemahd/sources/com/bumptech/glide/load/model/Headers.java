package com.bumptech.glide.load.model;

import com.bumptech.glide.load.model.LazyHeaders;
import java.util.Collections;
import java.util.Map;

public interface Headers {

    /* renamed from: a  reason: collision with root package name */
    public static final Headers f2237a = new LazyHeaders.Builder().a();

    static {
        new Headers() {
            public Map<String, String> a() {
                return Collections.emptyMap();
            }
        };
    }

    Map<String, String> a();
}
