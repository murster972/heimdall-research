package com.bumptech.glide.load.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.mediastore.MediaStoreUtil;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;
import java.io.File;
import java.io.FileNotFoundException;

public final class MediaStoreFileLoader implements ModelLoader<Uri, File> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2240a;

    public static final class Factory implements ModelLoaderFactory<Uri, File> {

        /* renamed from: a  reason: collision with root package name */
        private final Context f2241a;

        public Factory(Context context) {
            this.f2241a = context;
        }

        public ModelLoader<Uri, File> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new MediaStoreFileLoader(this.f2241a);
        }

        public void a() {
        }
    }

    public MediaStoreFileLoader(Context context) {
        this.f2240a = context;
    }

    public ModelLoader.LoadData<File> a(Uri uri, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(uri), new FilePathFetcher(this.f2240a, uri));
    }

    public boolean a(Uri uri) {
        return MediaStoreUtil.b(uri);
    }

    private static class FilePathFetcher implements DataFetcher<File> {
        private static final String[] c = {"_data"};

        /* renamed from: a  reason: collision with root package name */
        private final Context f2242a;
        private final Uri b;

        FilePathFetcher(Context context, Uri uri) {
            this.f2242a = context;
            this.b = uri;
        }

        public void a(Priority priority, DataFetcher.DataCallback<? super File> dataCallback) {
            Cursor query = this.f2242a.getContentResolver().query(this.b, c, (String) null, (String[]) null, (String) null);
            String str = null;
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                dataCallback.a((Exception) new FileNotFoundException("Failed to find file path for: " + this.b));
                return;
            }
            dataCallback.a(new File(str));
        }

        public void b() {
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<File> a() {
            return File.class;
        }
    }
}
