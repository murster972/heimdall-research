package com.bumptech.glide.load.model.stream;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.mediastore.MediaStoreUtil;
import com.bumptech.glide.load.data.mediastore.ThumbFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.signature.ObjectKey;
import java.io.InputStream;

public class MediaStoreImageThumbLoader implements ModelLoader<Uri, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2271a;

    public static class Factory implements ModelLoaderFactory<Uri, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final Context f2272a;

        public Factory(Context context) {
            this.f2272a = context;
        }

        public ModelLoader<Uri, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new MediaStoreImageThumbLoader(this.f2272a);
        }

        public void a() {
        }
    }

    public MediaStoreImageThumbLoader(Context context) {
        this.f2271a = context.getApplicationContext();
    }

    public ModelLoader.LoadData<InputStream> a(Uri uri, int i, int i2, Options options) {
        if (MediaStoreUtil.a(i, i2)) {
            return new ModelLoader.LoadData<>(new ObjectKey(uri), ThumbFetcher.a(this.f2271a, uri));
        }
        return null;
    }

    public boolean a(Uri uri) {
        return MediaStoreUtil.a(uri);
    }
}
