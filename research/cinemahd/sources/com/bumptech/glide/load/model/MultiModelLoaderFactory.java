package com.bumptech.glide.load.model;

import androidx.core.util.Pools$Pool;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MultiModelLoaderFactory {
    private static final Factory e = new Factory();
    private static final ModelLoader<Object, Object> f = new EmptyModelLoader();

    /* renamed from: a  reason: collision with root package name */
    private final List<Entry<?, ?>> f2251a;
    private final Factory b;
    private final Set<Entry<?, ?>> c;
    private final Pools$Pool<List<Throwable>> d;

    private static class EmptyModelLoader implements ModelLoader<Object, Object> {
        EmptyModelLoader() {
        }

        public ModelLoader.LoadData<Object> a(Object obj, int i, int i2, Options options) {
            return null;
        }

        public boolean a(Object obj) {
            return false;
        }
    }

    private static class Entry<Model, Data> {

        /* renamed from: a  reason: collision with root package name */
        private final Class<Model> f2252a;
        final Class<Data> b;
        final ModelLoaderFactory<? extends Model, ? extends Data> c;

        public Entry(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
            this.f2252a = cls;
            this.b = cls2;
            this.c = modelLoaderFactory;
        }

        public boolean a(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }

        public boolean a(Class<?> cls) {
            return this.f2252a.isAssignableFrom(cls);
        }
    }

    static class Factory {
        Factory() {
        }

        public <Model, Data> MultiModelLoader<Model, Data> a(List<ModelLoader<Model, Data>> list, Pools$Pool<List<Throwable>> pools$Pool) {
            return new MultiModelLoader<>(list, pools$Pool);
        }
    }

    public MultiModelLoaderFactory(Pools$Pool<List<Throwable>> pools$Pool) {
        this(pools$Pool, e);
    }

    /* access modifiers changed from: package-private */
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        a(cls, cls2, modelLoaderFactory, true);
    }

    /* access modifiers changed from: package-private */
    public synchronized <Model, Data> List<ModelLoaderFactory<? extends Model, ? extends Data>> b(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        List<ModelLoaderFactory<? extends Model, ? extends Data>> b2;
        b2 = b(cls, cls2);
        a(cls, cls2, modelLoaderFactory);
        return b2;
    }

    MultiModelLoaderFactory(Pools$Pool<List<Throwable>> pools$Pool, Factory factory) {
        this.f2251a = new ArrayList();
        this.c = new HashSet();
        this.d = pools$Pool;
        this.b = factory;
    }

    private <Model, Data> void a(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory, boolean z) {
        Entry entry = new Entry(cls, cls2, modelLoaderFactory);
        List<Entry<?, ?>> list = this.f2251a;
        list.add(z ? list.size() : 0, entry);
    }

    /* access modifiers changed from: package-private */
    public synchronized <Model, Data> List<ModelLoaderFactory<? extends Model, ? extends Data>> b(Class<Model> cls, Class<Data> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        Iterator<Entry<?, ?>> it2 = this.f2251a.iterator();
        while (it2.hasNext()) {
            Entry next = it2.next();
            if (next.a(cls, cls2)) {
                it2.remove();
                arrayList.add(b((Entry<?, ?>) next));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public synchronized <Model> List<ModelLoader<Model, ?>> a(Class<Model> cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (Entry next : this.f2251a) {
                if (!this.c.contains(next)) {
                    if (next.a(cls)) {
                        this.c.add(next);
                        arrayList.add(a((Entry<?, ?>) next));
                        this.c.remove(next);
                    }
                }
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public synchronized List<Class<?>> b(Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (Entry next : this.f2251a) {
            if (!arrayList.contains(next.b) && next.a(cls)) {
                arrayList.add(next.b);
            }
        }
        return arrayList;
    }

    public synchronized <Model, Data> ModelLoader<Model, Data> a(Class<Model> cls, Class<Data> cls2) {
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (Entry next : this.f2251a) {
                if (this.c.contains(next)) {
                    z = true;
                } else if (next.a(cls, cls2)) {
                    this.c.add(next);
                    arrayList.add(a((Entry<?, ?>) next));
                    this.c.remove(next);
                }
            }
            if (arrayList.size() > 1) {
                return this.b.a(arrayList, this.d);
            } else if (arrayList.size() == 1) {
                return (ModelLoader) arrayList.get(0);
            } else if (z) {
                return a();
            } else {
                throw new Registry.NoModelLoaderAvailableException((Class<?>) cls, (Class<?>) cls2);
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
    }

    private <Model, Data> ModelLoaderFactory<Model, Data> b(Entry<?, ?> entry) {
        return entry.c;
    }

    private <Model, Data> ModelLoader<Model, Data> a(Entry<?, ?> entry) {
        ModelLoader<? extends Model, ? extends Data> a2 = entry.c.a(this);
        Preconditions.a(a2);
        return a2;
    }

    private static <Model, Data> ModelLoader<Model, Data> a() {
        return f;
    }
}
