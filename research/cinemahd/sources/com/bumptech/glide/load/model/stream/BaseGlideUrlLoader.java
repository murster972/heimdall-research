package com.bumptech.glide.load.model.stream;

import android.text.TextUtils;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.load.model.ModelCache;
import com.bumptech.glide.load.model.ModelLoader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class BaseGlideUrlLoader<Model> implements ModelLoader<Model, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final ModelLoader<GlideUrl, InputStream> f2268a;
    private final ModelCache<Model, GlideUrl> b;

    protected BaseGlideUrlLoader(ModelLoader<GlideUrl, InputStream> modelLoader) {
        this(modelLoader, (ModelCache) null);
    }

    public ModelLoader.LoadData<InputStream> a(Model model, int i, int i2, Options options) {
        ModelCache<Model, GlideUrl> modelCache = this.b;
        GlideUrl a2 = modelCache != null ? modelCache.a(model, i, i2) : null;
        if (a2 == null) {
            String d = d(model, i, i2, options);
            if (TextUtils.isEmpty(d)) {
                return null;
            }
            GlideUrl glideUrl = new GlideUrl(d, c(model, i, i2, options));
            ModelCache<Model, GlideUrl> modelCache2 = this.b;
            if (modelCache2 != null) {
                modelCache2.a(model, i, i2, glideUrl);
            }
            a2 = glideUrl;
        }
        List<String> b2 = b(model, i, i2, options);
        ModelLoader.LoadData<InputStream> a3 = this.f2268a.a(a2, i, i2, options);
        return (a3 == null || b2.isEmpty()) ? a3 : new ModelLoader.LoadData<>(a3.f2245a, a(b2), a3.c);
    }

    /* access modifiers changed from: protected */
    public List<String> b(Model model, int i, int i2, Options options) {
        return Collections.emptyList();
    }

    /* access modifiers changed from: protected */
    public Headers c(Model model, int i, int i2, Options options) {
        return Headers.f2237a;
    }

    /* access modifiers changed from: protected */
    public abstract String d(Model model, int i, int i2, Options options);

    protected BaseGlideUrlLoader(ModelLoader<GlideUrl, InputStream> modelLoader, ModelCache<Model, GlideUrl> modelCache) {
        this.f2268a = modelLoader;
        this.b = modelCache;
    }

    private static List<Key> a(Collection<String> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (String glideUrl : collection) {
            arrayList.add(new GlideUrl(glideUrl));
        }
        return arrayList;
    }
}
