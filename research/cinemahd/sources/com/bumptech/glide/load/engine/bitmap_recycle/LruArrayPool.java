package com.bumptech.glide.load.engine.bitmap_recycle;

import android.util.Log;
import com.bumptech.glide.util.Preconditions;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public final class LruArrayPool implements ArrayPool {

    /* renamed from: a  reason: collision with root package name */
    private final GroupedLinkedMap<Key, Object> f2202a;
    private final KeyPool b;
    private final Map<Class<?>, NavigableMap<Integer, Integer>> c;
    private final Map<Class<?>, ArrayAdapterInterface<?>> d;
    private final int e;
    private int f;

    private static final class KeyPool extends BaseKeyPool<Key> {
        KeyPool() {
        }

        /* access modifiers changed from: package-private */
        public Key a(int i, Class<?> cls) {
            Key key = (Key) b();
            key.a(i, cls);
            return key;
        }

        /* access modifiers changed from: protected */
        public Key a() {
            return new Key(this);
        }
    }

    public LruArrayPool() {
        this.f2202a = new GroupedLinkedMap<>();
        this.b = new KeyPool();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = 4194304;
    }

    private boolean c(int i) {
        return i <= this.e / 2;
    }

    public synchronized <T> T a(int i, Class<T> cls) {
        Key key;
        Integer ceilingKey = b((Class<?>) cls).ceilingKey(Integer.valueOf(i));
        if (a(i, ceilingKey)) {
            key = this.b.a(ceilingKey.intValue(), cls);
        } else {
            key = this.b.a(i, cls);
        }
        return a(key, cls);
    }

    public synchronized <T> T b(int i, Class<T> cls) {
        return a(this.b.a(i, cls), cls);
    }

    public synchronized <T> void put(T t) {
        Class<?> cls = t.getClass();
        ArrayAdapterInterface<?> a2 = a(cls);
        int a3 = a2.a(t);
        int a4 = a2.a() * a3;
        if (c(a4)) {
            Key a5 = this.b.a(a3, cls);
            this.f2202a.a(a5, t);
            NavigableMap<Integer, Integer> b2 = b(cls);
            Integer num = (Integer) b2.get(Integer.valueOf(a5.b));
            Integer valueOf = Integer.valueOf(a5.b);
            int i = 1;
            if (num != null) {
                i = 1 + num.intValue();
            }
            b2.put(valueOf, Integer.valueOf(i));
            this.f += a4;
            b();
        }
    }

    private static final class Key implements Poolable {

        /* renamed from: a  reason: collision with root package name */
        private final KeyPool f2203a;
        int b;
        private Class<?> c;

        Key(KeyPool keyPool) {
            this.f2203a = keyPool;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, Class<?> cls) {
            this.b = i;
            this.c = cls;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Key)) {
                return false;
            }
            Key key = (Key) obj;
            if (this.b == key.b && this.c == key.c) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i = this.b * 31;
            Class<?> cls = this.c;
            return i + (cls != null ? cls.hashCode() : 0);
        }

        public String toString() {
            return "Key{size=" + this.b + "array=" + this.c + '}';
        }

        public void a() {
            this.f2203a.a(this);
        }
    }

    private boolean c() {
        int i = this.f;
        return i == 0 || this.e / i >= 2;
    }

    private void b() {
        b(this.e);
    }

    private void c(int i, Class<?> cls) {
        NavigableMap<Integer, Integer> b2 = b(cls);
        Integer num = (Integer) b2.get(Integer.valueOf(i));
        if (num == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", this: " + this);
        } else if (num.intValue() == 1) {
            b2.remove(Integer.valueOf(i));
        } else {
            b2.put(Integer.valueOf(i), Integer.valueOf(num.intValue() - 1));
        }
    }

    private void b(int i) {
        while (this.f > i) {
            Object a2 = this.f2202a.a();
            Preconditions.a(a2);
            ArrayAdapterInterface a3 = a(a2);
            this.f -= a3.a(a2) * a3.a();
            c(a3.a(a2), a2.getClass());
            if (Log.isLoggable(a3.getTag(), 2)) {
                Log.v(a3.getTag(), "evicted: " + a3.a(a2));
            }
        }
    }

    private <T> T a(Key key, Class<T> cls) {
        ArrayAdapterInterface<T> a2 = a(cls);
        T a3 = a(key);
        if (a3 != null) {
            this.f -= a2.a(a3) * a2.a();
            c(a2.a(a3), cls);
        }
        if (a3 != null) {
            return a3;
        }
        if (Log.isLoggable(a2.getTag(), 2)) {
            Log.v(a2.getTag(), "Allocated " + key.b + " bytes");
        }
        return a2.newArray(key.b);
    }

    public LruArrayPool(int i) {
        this.f2202a = new GroupedLinkedMap<>();
        this.b = new KeyPool();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = i;
    }

    private NavigableMap<Integer, Integer> b(Class<?> cls) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(cls);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.c.put(cls, treeMap);
        return treeMap;
    }

    private <T> T a(Key key) {
        return this.f2202a.a(key);
    }

    private boolean a(int i, Integer num) {
        return num != null && (c() || num.intValue() <= i * 8);
    }

    public synchronized void a() {
        b(0);
    }

    public synchronized void a(int i) {
        if (i >= 40) {
            try {
                a();
            } catch (Throwable th) {
                throw th;
            }
        } else if (i >= 20 || i == 15) {
            b(this.e / 2);
        }
    }

    private <T> ArrayAdapterInterface<T> a(T t) {
        return a(t.getClass());
    }

    private <T> ArrayAdapterInterface<T> a(Class<T> cls) {
        ArrayAdapterInterface<T> arrayAdapterInterface = this.d.get(cls);
        if (arrayAdapterInterface == null) {
            if (cls.equals(int[].class)) {
                arrayAdapterInterface = new IntegerArrayAdapter();
            } else if (cls.equals(byte[].class)) {
                arrayAdapterInterface = new ByteArrayAdapter();
            } else {
                throw new IllegalArgumentException("No array pool found for: " + cls.getSimpleName());
            }
            this.d.put(cls, arrayAdapterInterface);
        }
        return arrayAdapterInterface;
    }
}
