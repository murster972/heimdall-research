package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.util.Preconditions;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class DiskCacheWriteLocker {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, WriteLock> f2208a = new HashMap();
    private final WriteLockPool b = new WriteLockPool();

    private static class WriteLock {

        /* renamed from: a  reason: collision with root package name */
        final Lock f2209a = new ReentrantLock();
        int b;

        WriteLock() {
        }
    }

    DiskCacheWriteLocker() {
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        WriteLock writeLock;
        synchronized (this) {
            writeLock = this.f2208a.get(str);
            if (writeLock == null) {
                writeLock = this.b.a();
                this.f2208a.put(str, writeLock);
            }
            writeLock.b++;
        }
        writeLock.f2209a.lock();
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        WriteLock writeLock;
        synchronized (this) {
            WriteLock writeLock2 = this.f2208a.get(str);
            Preconditions.a(writeLock2);
            writeLock = writeLock2;
            if (writeLock.b >= 1) {
                writeLock.b--;
                if (writeLock.b == 0) {
                    WriteLock remove = this.f2208a.remove(str);
                    if (remove.equals(writeLock)) {
                        this.b.a(remove);
                    } else {
                        throw new IllegalStateException("Removed the wrong lock, expected to remove: " + writeLock + ", but actually removed: " + remove + ", safeKey: " + str);
                    }
                }
            } else {
                throw new IllegalStateException("Cannot release a lock that is not held, safeKey: " + str + ", interestedThreads: " + writeLock.b);
            }
        }
        writeLock.f2209a.unlock();
    }

    private static class WriteLockPool {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<WriteLock> f2210a = new ArrayDeque();

        WriteLockPool() {
        }

        /* access modifiers changed from: package-private */
        public WriteLock a() {
            WriteLock poll;
            synchronized (this.f2210a) {
                poll = this.f2210a.poll();
            }
            return poll == null ? new WriteLock() : poll;
        }

        /* access modifiers changed from: package-private */
        public void a(WriteLock writeLock) {
            synchronized (this.f2210a) {
                if (this.f2210a.size() < 10) {
                    this.f2210a.offer(writeLock);
                }
            }
        }
    }
}
