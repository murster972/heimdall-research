package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.util.Preconditions;
import java.security.MessageDigest;
import java.util.Map;

class EngineKey implements Key {
    private final Object b;
    private final int c;
    private final int d;
    private final Class<?> e;
    private final Class<?> f;
    private final Key g;
    private final Map<Class<?>, Transformation<?>> h;
    private final Options i;
    private int j;

    EngineKey(Object obj, Key key, int i2, int i3, Map<Class<?>, Transformation<?>> map, Class<?> cls, Class<?> cls2, Options options) {
        Preconditions.a(obj);
        this.b = obj;
        Preconditions.a(key, "Signature must not be null");
        this.g = key;
        this.c = i2;
        this.d = i3;
        Preconditions.a(map);
        this.h = map;
        Preconditions.a(cls, "Resource class must not be null");
        this.e = cls;
        Preconditions.a(cls2, "Transcode class must not be null");
        this.f = cls2;
        Preconditions.a(options);
        this.i = options;
    }

    public void a(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof EngineKey)) {
            return false;
        }
        EngineKey engineKey = (EngineKey) obj;
        if (!this.b.equals(engineKey.b) || !this.g.equals(engineKey.g) || this.d != engineKey.d || this.c != engineKey.c || !this.h.equals(engineKey.h) || !this.e.equals(engineKey.e) || !this.f.equals(engineKey.f) || !this.i.equals(engineKey.i)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.j == 0) {
            this.j = this.b.hashCode();
            this.j = (this.j * 31) + this.g.hashCode();
            this.j = (this.j * 31) + this.c;
            this.j = (this.j * 31) + this.d;
            this.j = (this.j * 31) + this.h.hashCode();
            this.j = (this.j * 31) + this.e.hashCode();
            this.j = (this.j * 31) + this.f.hashCode();
            this.j = (this.j * 31) + this.i.hashCode();
        }
        return this.j;
    }

    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }
}
