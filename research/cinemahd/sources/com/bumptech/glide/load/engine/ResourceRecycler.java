package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class ResourceRecycler {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2194a;
    private final Handler b = new Handler(Looper.getMainLooper(), new ResourceRecyclerCallback());

    private static final class ResourceRecyclerCallback implements Handler.Callback {
        ResourceRecyclerCallback() {
        }

        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((Resource) message.obj).recycle();
            return true;
        }
    }

    ResourceRecycler() {
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Resource<?> resource, boolean z) {
        if (!this.f2194a) {
            if (!z) {
                this.f2194a = true;
                resource.recycle();
                this.f2194a = false;
            }
        }
        this.b.obtainMessage(1, resource).sendToTarget();
    }
}
