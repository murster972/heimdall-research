package com.bumptech.glide.load.engine.cache;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import com.facebook.common.util.ByteConstants;

public final class MemorySizeCalculator {

    /* renamed from: a  reason: collision with root package name */
    private final int f2214a;
    private final int b;
    private final Context c;
    private final int d;

    public static final class Builder {
        static final int i = (Build.VERSION.SDK_INT < 26 ? 4 : 1);

        /* renamed from: a  reason: collision with root package name */
        final Context f2215a;
        ActivityManager b;
        ScreenDimensions c;
        float d = 2.0f;
        float e = ((float) i);
        float f = 0.4f;
        float g = 0.33f;
        int h = 4194304;

        public Builder(Context context) {
            this.f2215a = context;
            this.b = (ActivityManager) context.getSystemService("activity");
            this.c = new DisplayMetricsScreenDimensions(context.getResources().getDisplayMetrics());
            if (Build.VERSION.SDK_INT >= 26 && MemorySizeCalculator.a(this.b)) {
                this.e = 0.0f;
            }
        }

        public MemorySizeCalculator a() {
            return new MemorySizeCalculator(this);
        }
    }

    private static final class DisplayMetricsScreenDimensions implements ScreenDimensions {

        /* renamed from: a  reason: collision with root package name */
        private final DisplayMetrics f2216a;

        DisplayMetricsScreenDimensions(DisplayMetrics displayMetrics) {
            this.f2216a = displayMetrics;
        }

        public int a() {
            return this.f2216a.heightPixels;
        }

        public int b() {
            return this.f2216a.widthPixels;
        }
    }

    interface ScreenDimensions {
        int a();

        int b();
    }

    MemorySizeCalculator(Builder builder) {
        int i;
        this.c = builder.f2215a;
        if (a(builder.b)) {
            i = builder.h / 2;
        } else {
            i = builder.h;
        }
        this.d = i;
        int a2 = a(builder.b, builder.f, builder.g);
        float b2 = (float) (builder.c.b() * builder.c.a() * 4);
        int round = Math.round(builder.e * b2);
        int round2 = Math.round(b2 * builder.d);
        int i2 = a2 - this.d;
        int i3 = round2 + round;
        if (i3 <= i2) {
            this.b = round2;
            this.f2214a = round;
        } else {
            float f = (float) i2;
            float f2 = builder.e;
            float f3 = builder.d;
            float f4 = f / (f2 + f3);
            this.b = Math.round(f3 * f4);
            this.f2214a = Math.round(f4 * builder.e);
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Calculation complete, Calculated memory cache size: ");
            sb.append(a(this.b));
            sb.append(", pool size: ");
            sb.append(a(this.f2214a));
            sb.append(", byte array size: ");
            sb.append(a(this.d));
            sb.append(", memory class limited? ");
            sb.append(i3 > a2);
            sb.append(", max size: ");
            sb.append(a(a2));
            sb.append(", memoryClass: ");
            sb.append(builder.b.getMemoryClass());
            sb.append(", isLowMemoryDevice: ");
            sb.append(a(builder.b));
            Log.d("MemorySizeCalculator", sb.toString());
        }
    }

    public int a() {
        return this.d;
    }

    public int b() {
        return this.f2214a;
    }

    public int c() {
        return this.b;
    }

    private static int a(ActivityManager activityManager, float f, float f2) {
        float memoryClass = (float) (activityManager.getMemoryClass() * ByteConstants.KB * ByteConstants.KB);
        if (a(activityManager)) {
            f = f2;
        }
        return Math.round(memoryClass * f);
    }

    private String a(int i) {
        return Formatter.formatFileSize(this.c, (long) i);
    }

    @TargetApi(19)
    static boolean a(ActivityManager activityManager) {
        if (Build.VERSION.SDK_INT >= 19) {
            return activityManager.isLowRamDevice();
        }
        return true;
    }
}
