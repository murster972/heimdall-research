package com.bumptech.glide.load.engine;

import android.os.Process;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.EngineResource;
import com.bumptech.glide.util.Preconditions;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

final class ActiveResources {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f2159a;
    final Map<Key, ResourceWeakReference> b;
    private final ReferenceQueue<EngineResource<?>> c;
    private EngineResource.ResourceListener d;
    private volatile boolean e;
    private volatile DequeuedResourceCallback f;

    interface DequeuedResourceCallback {
        void a();
    }

    static final class ResourceWeakReference extends WeakReference<EngineResource<?>> {

        /* renamed from: a  reason: collision with root package name */
        final Key f2162a;
        final boolean b;
        Resource<?> c;

        ResourceWeakReference(Key key, EngineResource<?> engineResource, ReferenceQueue<? super EngineResource<?>> referenceQueue, boolean z) {
            super(engineResource, referenceQueue);
            Resource<?> resource;
            Preconditions.a(key);
            this.f2162a = key;
            if (!engineResource.d() || !z) {
                resource = null;
            } else {
                Resource<?> c2 = engineResource.c();
                Preconditions.a(c2);
                resource = c2;
            }
            this.c = resource;
            this.b = engineResource.d();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.c = null;
            clear();
        }
    }

    ActiveResources(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new ThreadFactory() {
            public Thread newThread(final Runnable runnable) {
                return new Thread(new Runnable(this) {
                    public void run() {
                        Process.setThreadPriority(10);
                        runnable.run();
                    }
                }, "glide-active-resources");
            }
        }));
    }

    /* access modifiers changed from: package-private */
    public void a(EngineResource.ResourceListener resourceListener) {
        synchronized (resourceListener) {
            synchronized (this) {
                this.d = resourceListener;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.bumptech.glide.load.engine.EngineResource<?> b(com.bumptech.glide.load.Key r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            java.util.Map<com.bumptech.glide.load.Key, com.bumptech.glide.load.engine.ActiveResources$ResourceWeakReference> r0 = r1.b     // Catch:{ all -> 0x001b }
            java.lang.Object r2 = r0.get(r2)     // Catch:{ all -> 0x001b }
            com.bumptech.glide.load.engine.ActiveResources$ResourceWeakReference r2 = (com.bumptech.glide.load.engine.ActiveResources.ResourceWeakReference) r2     // Catch:{ all -> 0x001b }
            if (r2 != 0) goto L_0x000e
            r2 = 0
            monitor-exit(r1)
            return r2
        L_0x000e:
            java.lang.Object r0 = r2.get()     // Catch:{ all -> 0x001b }
            com.bumptech.glide.load.engine.EngineResource r0 = (com.bumptech.glide.load.engine.EngineResource) r0     // Catch:{ all -> 0x001b }
            if (r0 != 0) goto L_0x0019
            r1.a((com.bumptech.glide.load.engine.ActiveResources.ResourceWeakReference) r2)     // Catch:{ all -> 0x001b }
        L_0x0019:
            monitor-exit(r1)
            return r0
        L_0x001b:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.ActiveResources.b(com.bumptech.glide.load.Key):com.bumptech.glide.load.engine.EngineResource");
    }

    ActiveResources(boolean z, Executor executor) {
        this.b = new HashMap();
        this.c = new ReferenceQueue<>();
        this.f2159a = z;
        executor.execute(new Runnable() {
            public void run() {
                ActiveResources.this.a();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Key key, EngineResource<?> engineResource) {
        ResourceWeakReference put = this.b.put(key, new ResourceWeakReference(key, engineResource, this.c, this.f2159a));
        if (put != null) {
            put.a();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Key key) {
        ResourceWeakReference remove = this.b.remove(key);
        if (remove != null) {
            remove.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ResourceWeakReference resourceWeakReference) {
        synchronized (this) {
            this.b.remove(resourceWeakReference.f2162a);
            if (resourceWeakReference.b) {
                if (resourceWeakReference.c != null) {
                    this.d.a(resourceWeakReference.f2162a, new EngineResource(resourceWeakReference.c, true, false, resourceWeakReference.f2162a, this.d));
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        while (!this.e) {
            try {
                a((ResourceWeakReference) this.c.remove());
                DequeuedResourceCallback dequeuedResourceCallback = this.f;
                if (dequeuedResourceCallback != null) {
                    dequeuedResourceCallback.a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
