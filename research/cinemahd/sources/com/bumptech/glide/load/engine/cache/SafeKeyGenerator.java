package com.bumptech.glide.load.engine.cache;

import androidx.core.util.Pools$Pool;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.util.pool.FactoryPools;
import com.bumptech.glide.util.pool.StateVerifier;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SafeKeyGenerator {

    /* renamed from: a  reason: collision with root package name */
    private final LruCache<Key, String> f2217a = new LruCache<>(1000);
    private final Pools$Pool<PoolableDigestContainer> b = FactoryPools.a(10, new FactoryPools.Factory<PoolableDigestContainer>(this) {
        public PoolableDigestContainer create() {
            try {
                return new PoolableDigestContainer(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    });

    private static final class PoolableDigestContainer implements FactoryPools.Poolable {

        /* renamed from: a  reason: collision with root package name */
        final MessageDigest f2218a;
        private final StateVerifier b = StateVerifier.b();

        PoolableDigestContainer(MessageDigest messageDigest) {
            this.f2218a = messageDigest;
        }

        public StateVerifier b() {
            return this.b;
        }
    }

    private String b(Key key) {
        PoolableDigestContainer acquire = this.b.acquire();
        Preconditions.a(acquire);
        PoolableDigestContainer poolableDigestContainer = acquire;
        try {
            key.a(poolableDigestContainer.f2218a);
            return Util.a(poolableDigestContainer.f2218a.digest());
        } finally {
            this.b.release(poolableDigestContainer);
        }
    }

    public String a(Key key) {
        String a2;
        synchronized (this.f2217a) {
            a2 = this.f2217a.a(key);
        }
        if (a2 == null) {
            a2 = b(key);
        }
        synchronized (this.f2217a) {
            this.f2217a.b(key, a2);
        }
        return a2;
    }
}
