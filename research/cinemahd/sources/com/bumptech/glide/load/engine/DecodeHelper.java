package com.bumptech.glide.load.engine;

import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.resource.UnitTransformation;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class DecodeHelper<Transcode> {

    /* renamed from: a  reason: collision with root package name */
    private final List<ModelLoader.LoadData<?>> f2165a = new ArrayList();
    private final List<Key> b = new ArrayList();
    private GlideContext c;
    private Object d;
    private int e;
    private int f;
    private Class<?> g;
    private DecodeJob.DiskCacheProvider h;
    private Options i;
    private Map<Class<?>, Transformation<?>> j;
    private Class<Transcode> k;
    private boolean l;
    private boolean m;
    private Key n;
    private Priority o;
    private DiskCacheStrategy p;
    private boolean q;
    private boolean r;

    DecodeHelper() {
    }

    /* access modifiers changed from: package-private */
    public <R> void a(GlideContext glideContext, Object obj, Key key, int i2, int i3, DiskCacheStrategy diskCacheStrategy, Class<?> cls, Class<R> cls2, Priority priority, Options options, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, DecodeJob.DiskCacheProvider diskCacheProvider) {
        this.c = glideContext;
        this.d = obj;
        this.n = key;
        this.e = i2;
        this.f = i3;
        this.p = diskCacheStrategy;
        this.g = cls;
        this.h = diskCacheProvider;
        this.k = cls2;
        this.o = priority;
        this.i = options;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    /* access modifiers changed from: package-private */
    public ArrayPool b() {
        return this.c.a();
    }

    /* access modifiers changed from: package-private */
    public boolean c(Class<?> cls) {
        return a(cls) != null;
    }

    /* access modifiers changed from: package-private */
    public DiskCache d() {
        return this.h.a();
    }

    /* access modifiers changed from: package-private */
    public DiskCacheStrategy e() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public List<ModelLoader.LoadData<?>> g() {
        if (!this.l) {
            this.l = true;
            this.f2165a.clear();
            List a2 = this.c.g().a(this.d);
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                ModelLoader.LoadData a3 = ((ModelLoader) a2.get(i2)).a(this.d, this.e, this.f, this.i);
                if (a3 != null) {
                    this.f2165a.add(a3);
                }
            }
        }
        return this.f2165a;
    }

    /* access modifiers changed from: package-private */
    public Class<?> h() {
        return this.d.getClass();
    }

    /* access modifiers changed from: package-private */
    public Options i() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public Priority j() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public List<Class<?>> k() {
        return this.c.g().b(this.d.getClass(), this.g, this.k);
    }

    /* access modifiers changed from: package-private */
    public Key l() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public Class<?> m() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public int n() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean o() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public <Z> Transformation<Z> b(Class<Z> cls) {
        Transformation<Z> transformation = this.j.get(cls);
        if (transformation == null) {
            Iterator<Map.Entry<Class<?>, Transformation<?>>> it2 = this.j.entrySet().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Map.Entry next = it2.next();
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    transformation = (Transformation) next.getValue();
                    break;
                }
            }
        }
        if (transformation != null) {
            return transformation;
        }
        if (!this.j.isEmpty() || !this.q) {
            return UnitTransformation.a();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    /* access modifiers changed from: package-private */
    public List<Key> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<ModelLoader.LoadData<?>> g2 = g();
            int size = g2.size();
            for (int i2 = 0; i2 < size; i2++) {
                ModelLoader.LoadData loadData = g2.get(i2);
                if (!this.b.contains(loadData.f2245a)) {
                    this.b.add(loadData.f2245a);
                }
                for (int i3 = 0; i3 < loadData.b.size(); i3++) {
                    if (!this.b.contains(loadData.b.get(i3))) {
                        this.b.add(loadData.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public boolean b(Resource<?> resource) {
        return this.c.g().b(resource);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.f2165a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    /* access modifiers changed from: package-private */
    public <Data> LoadPath<Data, ?, Transcode> a(Class<Data> cls) {
        return this.c.g().a(cls, this.g, this.k);
    }

    /* access modifiers changed from: package-private */
    public <Z> ResourceEncoder<Z> a(Resource<Z> resource) {
        return this.c.g().a(resource);
    }

    /* access modifiers changed from: package-private */
    public List<ModelLoader<File, ?>> a(File file) throws Registry.NoModelLoaderAvailableException {
        return this.c.g().a(file);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Key key) {
        List<ModelLoader.LoadData<?>> g2 = g();
        int size = g2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (g2.get(i2).f2245a.equals(key)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public <X> Encoder<X> a(X x) throws Registry.NoSourceEncoderAvailableException {
        return this.c.g().c(x);
    }
}
