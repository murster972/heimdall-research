package com.bumptech.glide.load.engine;

public interface Resource<Z> {
    Class<Z> a();

    Z get();

    int getSize();

    void recycle();
}
