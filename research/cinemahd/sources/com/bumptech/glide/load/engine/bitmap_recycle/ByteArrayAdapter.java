package com.bumptech.glide.load.engine.bitmap_recycle;

public final class ByteArrayAdapter implements ArrayAdapterInterface<byte[]> {
    public int a() {
        return 1;
    }

    public String getTag() {
        return "ByteArrayPool";
    }

    public int a(byte[] bArr) {
        return bArr.length;
    }

    public byte[] newArray(int i) {
        return new byte[i];
    }
}
