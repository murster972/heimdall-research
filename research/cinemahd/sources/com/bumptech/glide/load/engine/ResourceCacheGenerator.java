package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.DataFetcherGenerator;
import com.bumptech.glide.load.model.ModelLoader;
import java.io.File;
import java.util.List;

class ResourceCacheGenerator implements DataFetcherGenerator, DataFetcher.DataCallback<Object> {

    /* renamed from: a  reason: collision with root package name */
    private final DataFetcherGenerator.FetcherReadyCallback f2193a;
    private final DecodeHelper<?> b;
    private int c;
    private int d = -1;
    private Key e;
    private List<ModelLoader<File, ?>> f;
    private int g;
    private volatile ModelLoader.LoadData<?> h;
    private File i;
    private ResourceCacheKey j;

    ResourceCacheGenerator(DecodeHelper<?> decodeHelper, DataFetcherGenerator.FetcherReadyCallback fetcherReadyCallback) {
        this.b = decodeHelper;
        this.f2193a = fetcherReadyCallback;
    }

    private boolean b() {
        return this.g < this.f.size();
    }

    public boolean a() {
        List<Key> c2 = this.b.c();
        boolean z = false;
        if (c2.isEmpty()) {
            return false;
        }
        List<Class<?>> k = this.b.k();
        if (!k.isEmpty()) {
            while (true) {
                if (this.f == null || !b()) {
                    this.d++;
                    if (this.d >= k.size()) {
                        this.c++;
                        if (this.c >= c2.size()) {
                            return false;
                        }
                        this.d = 0;
                    }
                    Key key = c2.get(this.c);
                    Class cls = k.get(this.d);
                    this.j = new ResourceCacheKey(this.b.b(), key, this.b.l(), this.b.n(), this.b.f(), this.b.b(cls), cls, this.b.i());
                    this.i = this.b.d().a(this.j);
                    File file = this.i;
                    if (file != null) {
                        this.e = key;
                        this.f = this.b.a(file);
                        this.g = 0;
                    }
                } else {
                    this.h = null;
                    while (!z && b()) {
                        List<ModelLoader<File, ?>> list = this.f;
                        int i2 = this.g;
                        this.g = i2 + 1;
                        this.h = list.get(i2).a(this.i, this.b.n(), this.b.f(), this.b.i());
                        if (this.h != null && this.b.c(this.h.c.a())) {
                            this.h.c.a(this.b.j(), this);
                            z = true;
                        }
                    }
                    return z;
                }
            }
        } else if (File.class.equals(this.b.m())) {
            return false;
        } else {
            throw new IllegalStateException("Failed to find any load path from " + this.b.h() + " to " + this.b.m());
        }
    }

    public void cancel() {
        ModelLoader.LoadData<?> loadData = this.h;
        if (loadData != null) {
            loadData.c.cancel();
        }
    }

    public void a(Object obj) {
        this.f2193a.a(this.e, obj, this.h.c, DataSource.RESOURCE_DISK_CACHE, this.j);
    }

    public void a(Exception exc) {
        this.f2193a.a(this.j, exc, this.h.c, DataSource.RESOURCE_DISK_CACHE);
    }
}
