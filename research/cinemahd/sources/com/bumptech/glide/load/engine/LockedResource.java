package com.bumptech.glide.load.engine;

import androidx.core.util.Pools$Pool;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.pool.FactoryPools;
import com.bumptech.glide.util.pool.StateVerifier;

final class LockedResource<Z> implements Resource<Z>, FactoryPools.Poolable {
    private static final Pools$Pool<LockedResource<?>> e = FactoryPools.a(20, new FactoryPools.Factory<LockedResource<?>>() {
        public LockedResource<?> create() {
            return new LockedResource<>();
        }
    });

    /* renamed from: a  reason: collision with root package name */
    private final StateVerifier f2192a = StateVerifier.b();
    private Resource<Z> b;
    private boolean c;
    private boolean d;

    LockedResource() {
    }

    private void a(Resource<Z> resource) {
        this.d = false;
        this.c = true;
        this.b = resource;
    }

    static <Z> LockedResource<Z> b(Resource<Z> resource) {
        LockedResource<Z> acquire = e.acquire();
        Preconditions.a(acquire);
        LockedResource<Z> lockedResource = acquire;
        lockedResource.a(resource);
        return lockedResource;
    }

    private void d() {
        this.b = null;
        e.release(this);
    }

    /* access modifiers changed from: package-private */
    public synchronized void c() {
        this.f2192a.a();
        if (this.c) {
            this.c = false;
            if (this.d) {
                recycle();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }

    public Z get() {
        return this.b.get();
    }

    public int getSize() {
        return this.b.getSize();
    }

    public synchronized void recycle() {
        this.f2192a.a();
        this.d = true;
        if (!this.c) {
            this.b.recycle();
            d();
        }
    }

    public StateVerifier b() {
        return this.f2192a;
    }

    public Class<Z> a() {
        return this.b.a();
    }
}
