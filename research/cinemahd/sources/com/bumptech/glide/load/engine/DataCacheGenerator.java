package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.DataFetcherGenerator;
import com.bumptech.glide.load.model.ModelLoader;
import java.io.File;
import java.util.List;

class DataCacheGenerator implements DataFetcherGenerator, DataFetcher.DataCallback<Object> {

    /* renamed from: a  reason: collision with root package name */
    private final List<Key> f2163a;
    private final DecodeHelper<?> b;
    private final DataFetcherGenerator.FetcherReadyCallback c;
    private int d;
    private Key e;
    private List<ModelLoader<File, ?>> f;
    private int g;
    private volatile ModelLoader.LoadData<?> h;
    private File i;

    DataCacheGenerator(DecodeHelper<?> decodeHelper, DataFetcherGenerator.FetcherReadyCallback fetcherReadyCallback) {
        this(decodeHelper.c(), decodeHelper, fetcherReadyCallback);
    }

    private boolean b() {
        return this.g < this.f.size();
    }

    public boolean a() {
        while (true) {
            boolean z = false;
            if (this.f == null || !b()) {
                this.d++;
                if (this.d >= this.f2163a.size()) {
                    return false;
                }
                Key key = this.f2163a.get(this.d);
                this.i = this.b.d().a(new DataCacheKey(key, this.b.l()));
                File file = this.i;
                if (file != null) {
                    this.e = key;
                    this.f = this.b.a(file);
                    this.g = 0;
                }
            } else {
                this.h = null;
                while (!z && b()) {
                    List<ModelLoader<File, ?>> list = this.f;
                    int i2 = this.g;
                    this.g = i2 + 1;
                    this.h = list.get(i2).a(this.i, this.b.n(), this.b.f(), this.b.i());
                    if (this.h != null && this.b.c(this.h.c.a())) {
                        this.h.c.a(this.b.j(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    public void cancel() {
        ModelLoader.LoadData<?> loadData = this.h;
        if (loadData != null) {
            loadData.c.cancel();
        }
    }

    DataCacheGenerator(List<Key> list, DecodeHelper<?> decodeHelper, DataFetcherGenerator.FetcherReadyCallback fetcherReadyCallback) {
        this.d = -1;
        this.f2163a = list;
        this.b = decodeHelper;
        this.c = fetcherReadyCallback;
    }

    public void a(Object obj) {
        this.c.a(this.e, obj, this.h.c, DataSource.DATA_DISK_CACHE, this.e);
    }

    public void a(Exception exc) {
        this.c.a(this.e, exc, this.h.c, DataSource.DATA_DISK_CACHE);
    }
}
