package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Key;
import java.util.HashMap;
import java.util.Map;

final class Jobs {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Key, EngineJob<?>> f2190a = new HashMap();
    private final Map<Key, EngineJob<?>> b = new HashMap();

    Jobs() {
    }

    /* access modifiers changed from: package-private */
    public EngineJob<?> a(Key key, boolean z) {
        return a(z).get(key);
    }

    /* access modifiers changed from: package-private */
    public void b(Key key, EngineJob<?> engineJob) {
        Map<Key, EngineJob<?>> a2 = a(engineJob.f());
        if (engineJob.equals(a2.get(key))) {
            a2.remove(key);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Key key, EngineJob<?> engineJob) {
        a(engineJob.f()).put(key, engineJob);
    }

    private Map<Key, EngineJob<?>> a(boolean z) {
        return z ? this.b : this.f2190a;
    }
}
