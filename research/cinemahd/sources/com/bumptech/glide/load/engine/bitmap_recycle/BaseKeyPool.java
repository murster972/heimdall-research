package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.load.engine.bitmap_recycle.Poolable;
import com.bumptech.glide.util.Util;
import java.util.Queue;

abstract class BaseKeyPool<T extends Poolable> {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<T> f2199a = Util.a(20);

    BaseKeyPool() {
    }

    /* access modifiers changed from: package-private */
    public abstract T a();

    public void a(T t) {
        if (this.f2199a.size() < 20) {
            this.f2199a.offer(t);
        }
    }

    /* access modifiers changed from: package-private */
    public T b() {
        T t = (Poolable) this.f2199a.poll();
        return t == null ? a() : t;
    }
}
