package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.util.Util;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

final class ResourceCacheKey implements Key {
    private static final LruCache<Class<?>, byte[]> j = new LruCache<>(50);
    private final ArrayPool b;
    private final Key c;
    private final Key d;
    private final int e;
    private final int f;
    private final Class<?> g;
    private final Options h;
    private final Transformation<?> i;

    ResourceCacheKey(ArrayPool arrayPool, Key key, Key key2, int i2, int i3, Transformation<?> transformation, Class<?> cls, Options options) {
        this.b = arrayPool;
        this.c = key;
        this.d = key2;
        this.e = i2;
        this.f = i3;
        this.i = transformation;
        this.g = cls;
        this.h = options;
    }

    public void a(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.b(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.e).putInt(this.f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(bArr);
        Transformation<?> transformation = this.i;
        if (transformation != null) {
            transformation.a(messageDigest);
        }
        this.h.a(messageDigest);
        messageDigest.update(a());
        this.b.put(bArr);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ResourceCacheKey)) {
            return false;
        }
        ResourceCacheKey resourceCacheKey = (ResourceCacheKey) obj;
        if (this.f != resourceCacheKey.f || this.e != resourceCacheKey.e || !Util.b((Object) this.i, (Object) resourceCacheKey.i) || !this.g.equals(resourceCacheKey.g) || !this.c.equals(resourceCacheKey.c) || !this.d.equals(resourceCacheKey.d) || !this.h.equals(resourceCacheKey.h)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        Transformation<?> transformation = this.i;
        if (transformation != null) {
            hashCode = (hashCode * 31) + transformation.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + '\'' + ", options=" + this.h + '}';
    }

    private byte[] a() {
        byte[] a2 = j.a(this.g);
        if (a2 != null) {
            return a2;
        }
        byte[] bytes = this.g.getName().getBytes(Key.f2141a);
        j.b(this.g, bytes);
        return bytes;
    }
}
