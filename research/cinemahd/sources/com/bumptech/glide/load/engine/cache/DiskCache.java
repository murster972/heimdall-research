package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.Key;
import java.io.File;

public interface DiskCache {

    public interface Factory {
        DiskCache build();
    }

    public interface Writer {
        boolean a(File file);
    }

    File a(Key key);

    void a(Key key, Writer writer);
}
