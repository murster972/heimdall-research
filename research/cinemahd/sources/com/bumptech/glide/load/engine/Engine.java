package com.bumptech.glide.load.engine;

import android.util.Log;
import androidx.core.util.Pools$Pool;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.EngineResource;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.DiskCacheAdapter;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.engine.executor.GlideExecutor;
import com.bumptech.glide.request.ResourceCallback;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.pool.FactoryPools;
import java.util.Map;
import java.util.concurrent.Executor;

public class Engine implements EngineJobListener, MemoryCache.ResourceRemovedListener, EngineResource.ResourceListener {
    private static final boolean i = Log.isLoggable("Engine", 2);

    /* renamed from: a  reason: collision with root package name */
    private final Jobs f2175a;
    private final EngineKeyFactory b;
    private final MemoryCache c;
    private final EngineJobFactory d;
    private final ResourceRecycler e;
    private final LazyDiskCacheProvider f;
    private final DecodeJobFactory g;
    private final ActiveResources h;

    static class DecodeJobFactory {

        /* renamed from: a  reason: collision with root package name */
        final DecodeJob.DiskCacheProvider f2176a;
        final Pools$Pool<DecodeJob<?>> b = FactoryPools.a(150, new FactoryPools.Factory<DecodeJob<?>>() {
            public DecodeJob<?> create() {
                DecodeJobFactory decodeJobFactory = DecodeJobFactory.this;
                return new DecodeJob<>(decodeJobFactory.f2176a, decodeJobFactory.b);
            }
        });
        private int c;

        DecodeJobFactory(DecodeJob.DiskCacheProvider diskCacheProvider) {
            this.f2176a = diskCacheProvider;
        }

        /* access modifiers changed from: package-private */
        public <R> DecodeJob<R> a(GlideContext glideContext, Object obj, EngineKey engineKey, Key key, int i, int i2, Class<?> cls, Class<R> cls2, Priority priority, DiskCacheStrategy diskCacheStrategy, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, boolean z3, Options options, DecodeJob.Callback<R> callback) {
            DecodeJob<R> acquire = this.b.acquire();
            Preconditions.a(acquire);
            DecodeJob<R> decodeJob = acquire;
            int i3 = this.c;
            int i4 = i3;
            this.c = i3 + 1;
            decodeJob.a(glideContext, obj, engineKey, key, i, i2, cls, cls2, priority, diskCacheStrategy, map, z, z2, z3, options, callback, i4);
            return decodeJob;
        }
    }

    static class EngineJobFactory {

        /* renamed from: a  reason: collision with root package name */
        final GlideExecutor f2178a;
        final GlideExecutor b;
        final GlideExecutor c;
        final GlideExecutor d;
        final EngineJobListener e;
        final EngineResource.ResourceListener f;
        final Pools$Pool<EngineJob<?>> g = FactoryPools.a(150, new FactoryPools.Factory<EngineJob<?>>() {
            public EngineJob<?> create() {
                EngineJobFactory engineJobFactory = EngineJobFactory.this;
                return new EngineJob(engineJobFactory.f2178a, engineJobFactory.b, engineJobFactory.c, engineJobFactory.d, engineJobFactory.e, engineJobFactory.f, engineJobFactory.g);
            }
        });

        EngineJobFactory(GlideExecutor glideExecutor, GlideExecutor glideExecutor2, GlideExecutor glideExecutor3, GlideExecutor glideExecutor4, EngineJobListener engineJobListener, EngineResource.ResourceListener resourceListener) {
            this.f2178a = glideExecutor;
            this.b = glideExecutor2;
            this.c = glideExecutor3;
            this.d = glideExecutor4;
            this.e = engineJobListener;
            this.f = resourceListener;
        }

        /* access modifiers changed from: package-private */
        public <R> EngineJob<R> a(Key key, boolean z, boolean z2, boolean z3, boolean z4) {
            EngineJob<R> acquire = this.g.acquire();
            Preconditions.a(acquire);
            EngineJob<R> engineJob = acquire;
            engineJob.a(key, z, z2, z3, z4);
            return engineJob;
        }
    }

    private static class LazyDiskCacheProvider implements DecodeJob.DiskCacheProvider {

        /* renamed from: a  reason: collision with root package name */
        private final DiskCache.Factory f2180a;
        private volatile DiskCache b;

        LazyDiskCacheProvider(DiskCache.Factory factory) {
            this.f2180a = factory;
        }

        public DiskCache a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.f2180a.build();
                    }
                    if (this.b == null) {
                        this.b = new DiskCacheAdapter();
                    }
                }
            }
            return this.b;
        }
    }

    public class LoadStatus {

        /* renamed from: a  reason: collision with root package name */
        private final EngineJob<?> f2181a;
        private final ResourceCallback b;

        LoadStatus(ResourceCallback resourceCallback, EngineJob<?> engineJob) {
            this.b = resourceCallback;
            this.f2181a = engineJob;
        }

        public void a() {
            synchronized (Engine.this) {
                this.f2181a.c(this.b);
            }
        }
    }

    public Engine(MemoryCache memoryCache, DiskCache.Factory factory, GlideExecutor glideExecutor, GlideExecutor glideExecutor2, GlideExecutor glideExecutor3, GlideExecutor glideExecutor4, boolean z) {
        this(memoryCache, factory, glideExecutor, glideExecutor2, glideExecutor3, glideExecutor4, (Jobs) null, (EngineKeyFactory) null, (ActiveResources) null, (EngineJobFactory) null, (DecodeJobFactory) null, (ResourceRecycler) null, z);
    }

    private EngineResource<?> b(Key key) {
        EngineResource<?> b2 = this.h.b(key);
        if (b2 != null) {
            b2.b();
        }
        return b2;
    }

    private EngineResource<?> c(Key key) {
        EngineResource<?> a2 = a(key);
        if (a2 != null) {
            a2.b();
            this.h.a(key, a2);
        }
        return a2;
    }

    public <R> LoadStatus a(GlideContext glideContext, Object obj, Key key, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, DiskCacheStrategy diskCacheStrategy, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, Options options, boolean z3, boolean z4, boolean z5, boolean z6, ResourceCallback resourceCallback, Executor executor) {
        long a2 = i ? LogTime.a() : 0;
        EngineKey a3 = this.b.a(obj, key, i2, i3, map, cls, cls2, options);
        synchronized (this) {
            EngineResource<?> a4 = a(a3, z3, a2);
            if (a4 == null) {
                LoadStatus a5 = a(glideContext, obj, key, i2, i3, cls, cls2, priority, diskCacheStrategy, map, z, z2, options, z3, z4, z5, z6, resourceCallback, executor, a3, a2);
                return a5;
            }
            resourceCallback.onResourceReady(a4, DataSource.MEMORY_CACHE, false);
            return null;
        }
    }

    Engine(MemoryCache memoryCache, DiskCache.Factory factory, GlideExecutor glideExecutor, GlideExecutor glideExecutor2, GlideExecutor glideExecutor3, GlideExecutor glideExecutor4, Jobs jobs, EngineKeyFactory engineKeyFactory, ActiveResources activeResources, EngineJobFactory engineJobFactory, DecodeJobFactory decodeJobFactory, ResourceRecycler resourceRecycler, boolean z) {
        this.c = memoryCache;
        DiskCache.Factory factory2 = factory;
        this.f = new LazyDiskCacheProvider(factory);
        ActiveResources activeResources2 = activeResources == null ? new ActiveResources(z) : activeResources;
        this.h = activeResources2;
        activeResources2.a((EngineResource.ResourceListener) this);
        this.b = engineKeyFactory == null ? new EngineKeyFactory() : engineKeyFactory;
        this.f2175a = jobs == null ? new Jobs() : jobs;
        this.d = engineJobFactory == null ? new EngineJobFactory(glideExecutor, glideExecutor2, glideExecutor3, glideExecutor4, this, this) : engineJobFactory;
        this.g = decodeJobFactory == null ? new DecodeJobFactory(this.f) : decodeJobFactory;
        this.e = resourceRecycler == null ? new ResourceRecycler() : resourceRecycler;
        memoryCache.a((MemoryCache.ResourceRemovedListener) this);
    }

    public void b(Resource<?> resource) {
        if (resource instanceof EngineResource) {
            ((EngineResource) resource).e();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    private <R> LoadStatus a(GlideContext glideContext, Object obj, Key key, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, DiskCacheStrategy diskCacheStrategy, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, Options options, boolean z3, boolean z4, boolean z5, boolean z6, ResourceCallback resourceCallback, Executor executor, EngineKey engineKey, long j) {
        ResourceCallback resourceCallback2 = resourceCallback;
        Executor executor2 = executor;
        EngineKey engineKey2 = engineKey;
        long j2 = j;
        EngineJob<?> a2 = this.f2175a.a((Key) engineKey2, z6);
        if (a2 != null) {
            a2.a(resourceCallback2, executor2);
            if (i) {
                a("Added to existing load", j2, (Key) engineKey2);
            }
            return new LoadStatus(resourceCallback2, a2);
        }
        EngineJob a3 = this.d.a(engineKey, z3, z4, z5, z6);
        EngineJob engineJob = a3;
        EngineKey engineKey3 = engineKey2;
        DecodeJob<R> a4 = this.g.a(glideContext, obj, engineKey, key, i2, i3, cls, cls2, priority, diskCacheStrategy, map, z, z2, z6, options, a3);
        this.f2175a.a((Key) engineKey3, (EngineJob<?>) engineJob);
        EngineJob engineJob2 = engineJob;
        EngineKey engineKey4 = engineKey3;
        ResourceCallback resourceCallback3 = resourceCallback;
        engineJob2.a(resourceCallback3, executor);
        engineJob2.b(a4);
        if (i) {
            a("Started new load", j, (Key) engineKey4);
        }
        return new LoadStatus(resourceCallback3, engineJob2);
    }

    private EngineResource<?> a(EngineKey engineKey, boolean z, long j) {
        if (!z) {
            return null;
        }
        EngineResource<?> b2 = b((Key) engineKey);
        if (b2 != null) {
            if (i) {
                a("Loaded resource from active resources", j, (Key) engineKey);
            }
            return b2;
        }
        EngineResource<?> c2 = c(engineKey);
        if (c2 == null) {
            return null;
        }
        if (i) {
            a("Loaded resource from cache", j, (Key) engineKey);
        }
        return c2;
    }

    private static void a(String str, long j, Key key) {
        Log.v("Engine", str + " in " + LogTime.a(j) + "ms, key: " + key);
    }

    private EngineResource<?> a(Key key) {
        Resource<?> a2 = this.c.a(key);
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof EngineResource) {
            return (EngineResource) a2;
        }
        return new EngineResource<>(a2, true, true, key, this);
    }

    public synchronized void a(EngineJob<?> engineJob, Key key, EngineResource<?> engineResource) {
        if (engineResource != null) {
            if (engineResource.d()) {
                this.h.a(key, engineResource);
            }
        }
        this.f2175a.b(key, engineJob);
    }

    public synchronized void a(EngineJob<?> engineJob, Key key) {
        this.f2175a.b(key, engineJob);
    }

    public void a(Resource<?> resource) {
        this.e.a(resource, true);
    }

    public void a(Key key, EngineResource<?> engineResource) {
        this.h.a(key);
        if (engineResource.d()) {
            this.c.a(key, engineResource);
        } else {
            this.e.a(engineResource, false);
        }
    }
}
