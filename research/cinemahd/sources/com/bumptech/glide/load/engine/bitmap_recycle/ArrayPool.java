package com.bumptech.glide.load.engine.bitmap_recycle;

public interface ArrayPool {
    <T> T a(int i, Class<T> cls);

    void a();

    void a(int i);

    <T> T b(int i, Class<T> cls);

    <T> void put(T t);
}
