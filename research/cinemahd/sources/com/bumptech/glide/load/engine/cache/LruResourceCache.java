package com.bumptech.glide.load.engine.cache;

import android.annotation.SuppressLint;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.util.LruCache;

public class LruResourceCache extends LruCache<Key, Resource<?>> implements MemoryCache {
    private MemoryCache.ResourceRemovedListener d;

    public LruResourceCache(long j) {
        super(j);
    }

    public /* bridge */ /* synthetic */ Resource a(Key key, Resource resource) {
        return (Resource) super.b(key, resource);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void a(Key key, Resource<?> resource) {
        MemoryCache.ResourceRemovedListener resourceRemovedListener = this.d;
        if (resourceRemovedListener != null && resource != null) {
            resourceRemovedListener.a(resource);
        }
    }

    public /* bridge */ /* synthetic */ Resource a(Key key) {
        return (Resource) super.c(key);
    }

    public void a(MemoryCache.ResourceRemovedListener resourceRemovedListener) {
        this.d = resourceRemovedListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int b(Resource<?> resource) {
        if (resource == null) {
            return super.b(null);
        }
        return resource.getSize();
    }

    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            a();
        } else if (i >= 20 || i == 15) {
            a(b() / 2);
        }
    }
}
