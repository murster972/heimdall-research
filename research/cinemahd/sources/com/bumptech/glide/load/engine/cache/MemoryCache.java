package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.Resource;

public interface MemoryCache {

    public interface ResourceRemovedListener {
        void a(Resource<?> resource);
    }

    Resource<?> a(Key key);

    Resource<?> a(Key key, Resource<?> resource);

    void a();

    void a(int i);

    void a(ResourceRemovedListener resourceRemovedListener);
}
