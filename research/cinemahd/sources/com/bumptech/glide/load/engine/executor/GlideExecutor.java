package com.bumptech.glide.load.engine.executor;

import android.os.Process;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class GlideExecutor implements ExecutorService {
    private static final long b = TimeUnit.SECONDS.toMillis(10);
    private static volatile int c;

    /* renamed from: a  reason: collision with root package name */
    private final ExecutorService f2219a;

    private static final class DefaultThreadFactory implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        private final String f2221a;
        final UncaughtThrowableStrategy b;
        final boolean c;
        private int d;

        DefaultThreadFactory(String str, UncaughtThrowableStrategy uncaughtThrowableStrategy, boolean z) {
            this.f2221a = str;
            this.b = uncaughtThrowableStrategy;
            this.c = z;
        }

        public synchronized Thread newThread(Runnable runnable) {
            AnonymousClass1 r0;
            r0 = new Thread(runnable, "glide-" + this.f2221a + "-thread-" + this.d) {
                public void run() {
                    Process.setThreadPriority(9);
                    if (DefaultThreadFactory.this.c) {
                        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                    }
                    try {
                        super.run();
                    } catch (Throwable th) {
                        DefaultThreadFactory.this.b.a(th);
                    }
                }
            };
            this.d = this.d + 1;
            return r0;
        }
    }

    public interface UncaughtThrowableStrategy {

        /* renamed from: a  reason: collision with root package name */
        public static final UncaughtThrowableStrategy f2223a = new UncaughtThrowableStrategy() {
            public void a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        };
        public static final UncaughtThrowableStrategy b = f2223a;

        static {
            new UncaughtThrowableStrategy() {
                public void a(Throwable th) {
                }
            };
            new UncaughtThrowableStrategy() {
                public void a(Throwable th) {
                    if (th != null) {
                        throw new RuntimeException("Request threw uncaught throwable", th);
                    }
                }
            };
        }

        void a(Throwable th);
    }

    GlideExecutor(ExecutorService executorService) {
        this.f2219a = executorService;
    }

    public static int a() {
        if (c == 0) {
            c = Math.min(4, RuntimeCompat.a());
        }
        return c;
    }

    public static Builder b() {
        int i = a() >= 4 ? 2 : 1;
        Builder builder = new Builder(true);
        builder.a(i);
        builder.a("animation");
        return builder;
    }

    public static GlideExecutor c() {
        return b().a();
    }

    public static Builder d() {
        Builder builder = new Builder(true);
        builder.a(1);
        builder.a("disk-cache");
        return builder;
    }

    public static GlideExecutor e() {
        return d().a();
    }

    public static Builder f() {
        Builder builder = new Builder(false);
        builder.a(a());
        builder.a("source");
        return builder;
    }

    public static GlideExecutor g() {
        return f().a();
    }

    public static GlideExecutor h() {
        return new GlideExecutor(new ThreadPoolExecutor(0, Integer.MAX_VALUE, b, TimeUnit.MILLISECONDS, new SynchronousQueue(), new DefaultThreadFactory("source-unlimited", UncaughtThrowableStrategy.b, false)));
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        return this.f2219a.awaitTermination(j, timeUnit);
    }

    public void execute(Runnable runnable) {
        this.f2219a.execute(runnable);
    }

    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
        return this.f2219a.invokeAll(collection);
    }

    public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
        return this.f2219a.invokeAny(collection);
    }

    public boolean isShutdown() {
        return this.f2219a.isShutdown();
    }

    public boolean isTerminated() {
        return this.f2219a.isTerminated();
    }

    public void shutdown() {
        this.f2219a.shutdown();
    }

    public List<Runnable> shutdownNow() {
        return this.f2219a.shutdownNow();
    }

    public Future<?> submit(Runnable runnable) {
        return this.f2219a.submit(runnable);
    }

    public String toString() {
        return this.f2219a.toString();
    }

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final boolean f2220a;
        private int b;
        private int c;
        private UncaughtThrowableStrategy d = UncaughtThrowableStrategy.b;
        private String e;
        private long f;

        Builder(boolean z) {
            this.f2220a = z;
        }

        public Builder a(int i) {
            this.b = i;
            this.c = i;
            return this;
        }

        public Builder a(String str) {
            this.e = str;
            return this;
        }

        public GlideExecutor a() {
            if (!TextUtils.isEmpty(this.e)) {
                ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(this.b, this.c, this.f, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new DefaultThreadFactory(this.e, this.d, this.f2220a));
                if (this.f != 0) {
                    threadPoolExecutor.allowCoreThreadTimeOut(true);
                }
                return new GlideExecutor(threadPoolExecutor);
            }
            throw new IllegalArgumentException("Name must be non-null and non-empty, but given: " + this.e);
        }
    }

    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException {
        return this.f2219a.invokeAll(collection, j, timeUnit);
    }

    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.f2219a.invokeAny(collection, j, timeUnit);
    }

    public <T> Future<T> submit(Runnable runnable, T t) {
        return this.f2219a.submit(runnable, t);
    }

    public <T> Future<T> submit(Callable<T> callable) {
        return this.f2219a.submit(callable);
    }
}
