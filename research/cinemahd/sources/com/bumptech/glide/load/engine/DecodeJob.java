package com.bumptech.glide.load.engine;

import android.os.Build;
import android.util.Log;
import androidx.core.util.Pools$Pool;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.load.engine.DataFetcherGenerator;
import com.bumptech.glide.load.engine.DecodePath;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.pool.FactoryPools;
import com.bumptech.glide.util.pool.GlideTrace;
import com.bumptech.glide.util.pool.StateVerifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class DecodeJob<R> implements DataFetcherGenerator.FetcherReadyCallback, Runnable, Comparable<DecodeJob<?>>, FactoryPools.Poolable {
    private DataSource A;
    private DataFetcher<?> B;
    private volatile DataFetcherGenerator C;
    private volatile boolean D;
    private volatile boolean E;
    private boolean F;

    /* renamed from: a  reason: collision with root package name */
    private final DecodeHelper<R> f2166a = new DecodeHelper<>();
    private final List<Throwable> b = new ArrayList();
    private final StateVerifier c = StateVerifier.b();
    private final DiskCacheProvider d;
    private final Pools$Pool<DecodeJob<?>> e;
    private final DeferredEncodeManager<?> f = new DeferredEncodeManager<>();
    private final ReleaseManager g = new ReleaseManager();
    private GlideContext h;
    private Key i;
    private Priority j;
    private EngineKey k;
    private int l;
    private int m;
    private DiskCacheStrategy n;
    private Options o;
    private Callback<R> p;
    private int q;
    private Stage r;
    private RunReason s;
    private long t;
    private boolean u;
    private Object v;
    private Thread w;
    private Key x;
    private Key y;
    private Object z;

    /* renamed from: com.bumptech.glide.load.engine.DecodeJob$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2167a = new int[RunReason.values().length];
        static final /* synthetic */ int[] b = new int[Stage.values().length];
        static final /* synthetic */ int[] c = new int[EncodeStrategy.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|12|13|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|5|6|7|9|10|11|12|13|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0070 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007a */
        static {
            /*
                com.bumptech.glide.load.EncodeStrategy[] r0 = com.bumptech.glide.load.EncodeStrategy.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                c = r0
                r0 = 1
                int[] r1 = c     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.bumptech.glide.load.EncodeStrategy r2 = com.bumptech.glide.load.EncodeStrategy.SOURCE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = c     // Catch:{ NoSuchFieldError -> 0x001f }
                com.bumptech.glide.load.EncodeStrategy r3 = com.bumptech.glide.load.EncodeStrategy.TRANSFORMED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                com.bumptech.glide.load.engine.DecodeJob$Stage[] r2 = com.bumptech.glide.load.engine.DecodeJob.Stage.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                b = r2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.bumptech.glide.load.engine.DecodeJob$Stage r3 = com.bumptech.glide.load.engine.DecodeJob.Stage.RESOURCE_CACHE     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x003c }
                com.bumptech.glide.load.engine.DecodeJob$Stage r3 = com.bumptech.glide.load.engine.DecodeJob.Stage.DATA_CACHE     // Catch:{ NoSuchFieldError -> 0x003c }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
            L_0x003c:
                r2 = 3
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x0047 }
                com.bumptech.glide.load.engine.DecodeJob$Stage r4 = com.bumptech.glide.load.engine.DecodeJob.Stage.SOURCE     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x0052 }
                com.bumptech.glide.load.engine.DecodeJob$Stage r4 = com.bumptech.glide.load.engine.DecodeJob.Stage.FINISHED     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r5 = 4
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x005d }
                com.bumptech.glide.load.engine.DecodeJob$Stage r4 = com.bumptech.glide.load.engine.DecodeJob.Stage.INITIALIZE     // Catch:{ NoSuchFieldError -> 0x005d }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x005d }
                r5 = 5
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x005d }
            L_0x005d:
                com.bumptech.glide.load.engine.DecodeJob$RunReason[] r3 = com.bumptech.glide.load.engine.DecodeJob.RunReason.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                f2167a = r3
                int[] r3 = f2167a     // Catch:{ NoSuchFieldError -> 0x0070 }
                com.bumptech.glide.load.engine.DecodeJob$RunReason r4 = com.bumptech.glide.load.engine.DecodeJob.RunReason.INITIALIZE     // Catch:{ NoSuchFieldError -> 0x0070 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0070 }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0070 }
            L_0x0070:
                int[] r0 = f2167a     // Catch:{ NoSuchFieldError -> 0x007a }
                com.bumptech.glide.load.engine.DecodeJob$RunReason r3 = com.bumptech.glide.load.engine.DecodeJob.RunReason.SWITCH_TO_SOURCE_SERVICE     // Catch:{ NoSuchFieldError -> 0x007a }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                int[] r0 = f2167a     // Catch:{ NoSuchFieldError -> 0x0084 }
                com.bumptech.glide.load.engine.DecodeJob$RunReason r1 = com.bumptech.glide.load.engine.DecodeJob.RunReason.DECODE_DATA     // Catch:{ NoSuchFieldError -> 0x0084 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0084 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0084 }
            L_0x0084:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.DecodeJob.AnonymousClass1.<clinit>():void");
        }
    }

    interface Callback<R> {
        void a(DecodeJob<?> decodeJob);

        void onLoadFailed(GlideException glideException);

        void onResourceReady(Resource<R> resource, DataSource dataSource, boolean z);
    }

    private final class DecodeCallback<Z> implements DecodePath.DecodeCallback<Z> {

        /* renamed from: a  reason: collision with root package name */
        private final DataSource f2168a;

        DecodeCallback(DataSource dataSource) {
            this.f2168a = dataSource;
        }

        public Resource<Z> a(Resource<Z> resource) {
            return DecodeJob.this.a(this.f2168a, resource);
        }
    }

    interface DiskCacheProvider {
        DiskCache a();
    }

    private enum RunReason {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    private enum Stage {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    DecodeJob(DiskCacheProvider diskCacheProvider, Pools$Pool<DecodeJob<?>> pools$Pool) {
        this.d = diskCacheProvider;
        this.e = pools$Pool;
    }

    private void b(Resource<R> resource, DataSource dataSource, boolean z2) {
        if (resource instanceof Initializable) {
            ((Initializable) resource).initialize();
        }
        LockedResource<R> lockedResource = null;
        LockedResource<R> lockedResource2 = resource;
        if (this.f.b()) {
            LockedResource<R> b2 = LockedResource.b(resource);
            lockedResource = b2;
            lockedResource2 = b2;
        }
        a(lockedResource2, dataSource, z2);
        this.r = Stage.ENCODE;
        try {
            if (this.f.b()) {
                this.f.a(this.d, this.o);
            }
            i();
        } finally {
            if (lockedResource != null) {
                lockedResource.c();
            }
        }
    }

    private void e() {
        if (Log.isLoggable("DecodeJob", 2)) {
            long j2 = this.t;
            a("Retrieved data", j2, "data: " + this.z + ", cache key: " + this.x + ", fetcher: " + this.B);
        }
        Resource<R> resource = null;
        try {
            resource = a(this.B, this.z, this.A);
        } catch (GlideException e2) {
            e2.a(this.y, this.A);
            this.b.add(e2);
        }
        if (resource != null) {
            b(resource, this.A, this.F);
        } else {
            l();
        }
    }

    private DataFetcherGenerator f() {
        int i2 = AnonymousClass1.b[this.r.ordinal()];
        if (i2 == 1) {
            return new ResourceCacheGenerator(this.f2166a, this);
        }
        if (i2 == 2) {
            return new DataCacheGenerator(this.f2166a, this);
        }
        if (i2 == 3) {
            return new SourceGenerator(this.f2166a, this);
        }
        if (i2 == 4) {
            return null;
        }
        throw new IllegalStateException("Unrecognized stage: " + this.r);
    }

    private int g() {
        return this.j.ordinal();
    }

    private void h() {
        n();
        this.p.onLoadFailed(new GlideException("Failed to load resource", (List<Throwable>) new ArrayList(this.b)));
        j();
    }

    private void i() {
        if (this.g.a()) {
            k();
        }
    }

    private void j() {
        if (this.g.b()) {
            k();
        }
    }

    private void k() {
        this.g.c();
        this.f.a();
        this.f2166a.a();
        this.D = false;
        this.h = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.k = null;
        this.p = null;
        this.r = null;
        this.C = null;
        this.w = null;
        this.x = null;
        this.z = null;
        this.A = null;
        this.B = null;
        this.t = 0;
        this.E = false;
        this.v = null;
        this.b.clear();
        this.e.release(this);
    }

    private void l() {
        this.w = Thread.currentThread();
        this.t = LogTime.a();
        boolean z2 = false;
        while (!this.E && this.C != null && !(z2 = this.C.a())) {
            this.r = a(this.r);
            this.C = f();
            if (this.r == Stage.SOURCE) {
                c();
                return;
            }
        }
        if ((this.r == Stage.FINISHED || this.E) && !z2) {
            h();
        }
    }

    private void m() {
        int i2 = AnonymousClass1.f2167a[this.s.ordinal()];
        if (i2 == 1) {
            this.r = a(Stage.INITIALIZE);
            this.C = f();
            l();
        } else if (i2 == 2) {
            l();
        } else if (i2 == 3) {
            e();
        } else {
            throw new IllegalStateException("Unrecognized run reason: " + this.s);
        }
    }

    private void n() {
        Throwable th;
        this.c.a();
        if (this.D) {
            if (this.b.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.b;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.D = true;
    }

    /* access modifiers changed from: package-private */
    public DecodeJob<R> a(GlideContext glideContext, Object obj, EngineKey engineKey, Key key, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, DiskCacheStrategy diskCacheStrategy, Map<Class<?>, Transformation<?>> map, boolean z2, boolean z3, boolean z4, Options options, Callback<R> callback, int i4) {
        this.f2166a.a(glideContext, obj, key, i2, i3, diskCacheStrategy, cls, cls2, priority, options, map, z2, z3, this.d);
        this.h = glideContext;
        this.i = key;
        this.j = priority;
        this.k = engineKey;
        this.l = i2;
        this.m = i3;
        this.n = diskCacheStrategy;
        this.u = z4;
        this.o = options;
        this.p = callback;
        this.q = i4;
        this.s = RunReason.INITIALIZE;
        this.v = obj;
        return this;
    }

    public void c() {
        this.s = RunReason.SWITCH_TO_SOURCE_SERVICE;
        this.p.a(this);
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        Stage a2 = a(Stage.INITIALIZE);
        return a2 == Stage.RESOURCE_CACHE || a2 == Stage.DATA_CACHE;
    }

    public void run() {
        GlideTrace.a("DecodeJob#run(model=%s)", this.v);
        DataFetcher<?> dataFetcher = this.B;
        try {
            if (this.E) {
                h();
                if (dataFetcher != null) {
                    dataFetcher.b();
                }
                GlideTrace.a();
                return;
            }
            m();
            if (dataFetcher != null) {
                dataFetcher.b();
            }
            GlideTrace.a();
        } catch (CallbackException e2) {
            throw e2;
        } catch (Throwable th) {
            if (dataFetcher != null) {
                dataFetcher.b();
            }
            GlideTrace.a();
            throw th;
        }
    }

    private static class ReleaseManager {

        /* renamed from: a  reason: collision with root package name */
        private boolean f2170a;
        private boolean b;
        private boolean c;

        ReleaseManager() {
        }

        /* access modifiers changed from: package-private */
        public synchronized boolean a(boolean z) {
            this.f2170a = true;
            return b(z);
        }

        /* access modifiers changed from: package-private */
        public synchronized boolean b() {
            this.c = true;
            return b(false);
        }

        /* access modifiers changed from: package-private */
        public synchronized void c() {
            this.b = false;
            this.f2170a = false;
            this.c = false;
        }

        private boolean b(boolean z) {
            return (this.c || z || this.b) && this.f2170a;
        }

        /* access modifiers changed from: package-private */
        public synchronized boolean a() {
            this.b = true;
            return b(false);
        }
    }

    private static class DeferredEncodeManager<Z> {

        /* renamed from: a  reason: collision with root package name */
        private Key f2169a;
        private ResourceEncoder<Z> b;
        private LockedResource<Z> c;

        DeferredEncodeManager() {
        }

        /* access modifiers changed from: package-private */
        public <X> void a(Key key, ResourceEncoder<X> resourceEncoder, LockedResource<X> lockedResource) {
            this.f2169a = key;
            this.b = resourceEncoder;
            this.c = lockedResource;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return this.c != null;
        }

        /* access modifiers changed from: package-private */
        public void a(DiskCacheProvider diskCacheProvider, Options options) {
            GlideTrace.a("DecodeJob.encode");
            try {
                diskCacheProvider.a().a(this.f2169a, new DataCacheWriter(this.b, this.c, options));
            } finally {
                this.c.c();
                GlideTrace.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f2169a = null;
            this.b = null;
            this.c = null;
        }
    }

    public StateVerifier b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        if (this.g.a(z2)) {
            k();
        }
    }

    /* renamed from: a */
    public int compareTo(DecodeJob<?> decodeJob) {
        int g2 = g() - decodeJob.g();
        return g2 == 0 ? this.q - decodeJob.q : g2;
    }

    public void a() {
        this.E = true;
        DataFetcherGenerator dataFetcherGenerator = this.C;
        if (dataFetcherGenerator != null) {
            dataFetcherGenerator.cancel();
        }
    }

    private void a(Resource<R> resource, DataSource dataSource, boolean z2) {
        n();
        this.p.onResourceReady(resource, dataSource, z2);
    }

    private Stage a(Stage stage) {
        int i2 = AnonymousClass1.b[stage.ordinal()];
        if (i2 != 1) {
            if (i2 == 2) {
                return this.u ? Stage.FINISHED : Stage.SOURCE;
            }
            if (i2 == 3 || i2 == 4) {
                return Stage.FINISHED;
            }
            if (i2 != 5) {
                throw new IllegalArgumentException("Unrecognized stage: " + stage);
            } else if (this.n.b()) {
                return Stage.RESOURCE_CACHE;
            } else {
                return a(Stage.RESOURCE_CACHE);
            }
        } else if (this.n.a()) {
            return Stage.DATA_CACHE;
        } else {
            return a(Stage.DATA_CACHE);
        }
    }

    public void a(Key key, Object obj, DataFetcher<?> dataFetcher, DataSource dataSource, Key key2) {
        this.x = key;
        this.z = obj;
        this.B = dataFetcher;
        this.A = dataSource;
        this.y = key2;
        boolean z2 = false;
        if (key != this.f2166a.c().get(0)) {
            z2 = true;
        }
        this.F = z2;
        if (Thread.currentThread() != this.w) {
            this.s = RunReason.DECODE_DATA;
            this.p.a(this);
            return;
        }
        GlideTrace.a("DecodeJob.decodeFromRetrievedData");
        try {
            e();
        } finally {
            GlideTrace.a();
        }
    }

    public void a(Key key, Exception exc, DataFetcher<?> dataFetcher, DataSource dataSource) {
        dataFetcher.b();
        GlideException glideException = new GlideException("Fetching data failed", (Throwable) exc);
        glideException.a(key, dataSource, dataFetcher.a());
        this.b.add(glideException);
        if (Thread.currentThread() != this.w) {
            this.s = RunReason.SWITCH_TO_SOURCE_SERVICE;
            this.p.a(this);
            return;
        }
        l();
    }

    private <Data> Resource<R> a(DataFetcher<?> dataFetcher, Data data, DataSource dataSource) throws GlideException {
        if (data == null) {
            dataFetcher.b();
            return null;
        }
        try {
            long a2 = LogTime.a();
            Resource<R> a3 = a(data, dataSource);
            if (Log.isLoggable("DecodeJob", 2)) {
                a("Decoded result " + a3, a2);
            }
            return a3;
        } finally {
            dataFetcher.b();
        }
    }

    private <Data> Resource<R> a(Data data, DataSource dataSource) throws GlideException {
        return a(data, dataSource, this.f2166a.a(data.getClass()));
    }

    private Options a(DataSource dataSource) {
        Options options = this.o;
        if (Build.VERSION.SDK_INT < 26) {
            return options;
        }
        boolean z2 = dataSource == DataSource.RESOURCE_DISK_CACHE || this.f2166a.o();
        Boolean bool = (Boolean) options.a(Downsampler.i);
        if (bool != null && (!bool.booleanValue() || z2)) {
            return options;
        }
        Options options2 = new Options();
        options2.a(this.o);
        options2.a(Downsampler.i, Boolean.valueOf(z2));
        return options2;
    }

    private <Data, ResourceType> Resource<R> a(Data data, DataSource dataSource, LoadPath<Data, ResourceType, R> loadPath) throws GlideException {
        Options a2 = a(dataSource);
        DataRewinder b2 = this.h.g().b(data);
        try {
            return loadPath.a(b2, a2, this.l, this.m, new DecodeCallback(dataSource));
        } finally {
            b2.b();
        }
    }

    private void a(String str, long j2) {
        a(str, j2, (String) null);
    }

    private void a(String str, long j2, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(LogTime.a(j2));
        sb.append(", load key: ");
        sb.append(this.k);
        if (str2 != null) {
            str3 = ", " + str2;
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        Log.v("DecodeJob", sb.toString());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v5, resolved type: com.bumptech.glide.load.engine.ResourceCacheKey} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v6, resolved type: com.bumptech.glide.load.engine.DataCacheKey} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v8, resolved type: com.bumptech.glide.load.engine.ResourceCacheKey} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: com.bumptech.glide.load.engine.ResourceCacheKey} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Z> com.bumptech.glide.load.engine.Resource<Z> a(com.bumptech.glide.load.DataSource r12, com.bumptech.glide.load.engine.Resource<Z> r13) {
        /*
            r11 = this;
            java.lang.Object r0 = r13.get()
            java.lang.Class r8 = r0.getClass()
            com.bumptech.glide.load.DataSource r0 = com.bumptech.glide.load.DataSource.RESOURCE_DISK_CACHE
            r1 = 0
            if (r12 == r0) goto L_0x0020
            com.bumptech.glide.load.engine.DecodeHelper<R> r0 = r11.f2166a
            com.bumptech.glide.load.Transformation r0 = r0.b(r8)
            com.bumptech.glide.GlideContext r2 = r11.h
            int r3 = r11.l
            int r4 = r11.m
            com.bumptech.glide.load.engine.Resource r2 = r0.a(r2, r13, r3, r4)
            r7 = r0
            r0 = r2
            goto L_0x0022
        L_0x0020:
            r0 = r13
            r7 = r1
        L_0x0022:
            boolean r2 = r13.equals(r0)
            if (r2 != 0) goto L_0x002b
            r13.recycle()
        L_0x002b:
            com.bumptech.glide.load.engine.DecodeHelper<R> r13 = r11.f2166a
            boolean r13 = r13.b((com.bumptech.glide.load.engine.Resource<?>) r0)
            if (r13 == 0) goto L_0x0040
            com.bumptech.glide.load.engine.DecodeHelper<R> r13 = r11.f2166a
            com.bumptech.glide.load.ResourceEncoder r1 = r13.a(r0)
            com.bumptech.glide.load.Options r13 = r11.o
            com.bumptech.glide.load.EncodeStrategy r13 = r1.a(r13)
            goto L_0x0042
        L_0x0040:
            com.bumptech.glide.load.EncodeStrategy r13 = com.bumptech.glide.load.EncodeStrategy.NONE
        L_0x0042:
            r10 = r1
            com.bumptech.glide.load.engine.DecodeHelper<R> r1 = r11.f2166a
            com.bumptech.glide.load.Key r2 = r11.x
            boolean r1 = r1.a((com.bumptech.glide.load.Key) r2)
            r2 = 1
            r1 = r1 ^ r2
            com.bumptech.glide.load.engine.DiskCacheStrategy r3 = r11.n
            boolean r12 = r3.a(r1, r12, r13)
            if (r12 == 0) goto L_0x00b3
            if (r10 == 0) goto L_0x00a5
            int[] r12 = com.bumptech.glide.load.engine.DecodeJob.AnonymousClass1.c
            int r1 = r13.ordinal()
            r12 = r12[r1]
            if (r12 == r2) goto L_0x0092
            r1 = 2
            if (r12 != r1) goto L_0x007b
            com.bumptech.glide.load.engine.ResourceCacheKey r12 = new com.bumptech.glide.load.engine.ResourceCacheKey
            com.bumptech.glide.load.engine.DecodeHelper<R> r13 = r11.f2166a
            com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool r2 = r13.b()
            com.bumptech.glide.load.Key r3 = r11.x
            com.bumptech.glide.load.Key r4 = r11.i
            int r5 = r11.l
            int r6 = r11.m
            com.bumptech.glide.load.Options r9 = r11.o
            r1 = r12
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x009b
        L_0x007b:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unknown strategy: "
            r0.append(r1)
            r0.append(r13)
            java.lang.String r13 = r0.toString()
            r12.<init>(r13)
            throw r12
        L_0x0092:
            com.bumptech.glide.load.engine.DataCacheKey r12 = new com.bumptech.glide.load.engine.DataCacheKey
            com.bumptech.glide.load.Key r13 = r11.x
            com.bumptech.glide.load.Key r1 = r11.i
            r12.<init>(r13, r1)
        L_0x009b:
            com.bumptech.glide.load.engine.LockedResource r0 = com.bumptech.glide.load.engine.LockedResource.b(r0)
            com.bumptech.glide.load.engine.DecodeJob$DeferredEncodeManager<?> r13 = r11.f
            r13.a(r12, r10, r0)
            goto L_0x00b3
        L_0x00a5:
            com.bumptech.glide.Registry$NoResultEncoderAvailableException r12 = new com.bumptech.glide.Registry$NoResultEncoderAvailableException
            java.lang.Object r13 = r0.get()
            java.lang.Class r13 = r13.getClass()
            r12.<init>(r13)
            throw r12
        L_0x00b3:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.DecodeJob.a(com.bumptech.glide.load.DataSource, com.bumptech.glide.load.engine.Resource):com.bumptech.glide.load.engine.Resource");
    }
}
