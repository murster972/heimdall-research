package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.DataFetcherGenerator;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.util.LogTime;
import java.util.Collections;
import java.util.List;

class SourceGenerator implements DataFetcherGenerator, DataFetcherGenerator.FetcherReadyCallback {

    /* renamed from: a  reason: collision with root package name */
    private final DecodeHelper<?> f2195a;
    private final DataFetcherGenerator.FetcherReadyCallback b;
    private int c;
    private DataCacheGenerator d;
    private Object e;
    private volatile ModelLoader.LoadData<?> f;
    private DataCacheKey g;

    SourceGenerator(DecodeHelper<?> decodeHelper, DataFetcherGenerator.FetcherReadyCallback fetcherReadyCallback) {
        this.f2195a = decodeHelper;
        this.b = fetcherReadyCallback;
    }

    private void b(final ModelLoader.LoadData<?> loadData) {
        this.f.c.a(this.f2195a.j(), new DataFetcher.DataCallback<Object>() {
            public void a(Object obj) {
                if (SourceGenerator.this.a((ModelLoader.LoadData<?>) loadData)) {
                    SourceGenerator.this.a((ModelLoader.LoadData<?>) loadData, obj);
                }
            }

            public void a(Exception exc) {
                if (SourceGenerator.this.a((ModelLoader.LoadData<?>) loadData)) {
                    SourceGenerator.this.a((ModelLoader.LoadData<?>) loadData, exc);
                }
            }
        });
    }

    public boolean a() {
        Object obj = this.e;
        if (obj != null) {
            this.e = null;
            a(obj);
        }
        DataCacheGenerator dataCacheGenerator = this.d;
        if (dataCacheGenerator != null && dataCacheGenerator.a()) {
            return true;
        }
        this.d = null;
        this.f = null;
        boolean z = false;
        while (!z && b()) {
            List<ModelLoader.LoadData<?>> g2 = this.f2195a.g();
            int i = this.c;
            this.c = i + 1;
            this.f = g2.get(i);
            if (this.f != null && (this.f2195a.e().a(this.f.c.c()) || this.f2195a.c(this.f.c.a()))) {
                b(this.f);
                z = true;
            }
        }
        return z;
    }

    public void c() {
        throw new UnsupportedOperationException();
    }

    public void cancel() {
        ModelLoader.LoadData<?> loadData = this.f;
        if (loadData != null) {
            loadData.c.cancel();
        }
    }

    private boolean b() {
        return this.c < this.f2195a.g().size();
    }

    /* access modifiers changed from: package-private */
    public boolean a(ModelLoader.LoadData<?> loadData) {
        ModelLoader.LoadData<?> loadData2 = this.f;
        return loadData2 != null && loadData2 == loadData;
    }

    /* JADX INFO: finally extract failed */
    private void a(Object obj) {
        long a2 = LogTime.a();
        try {
            Encoder<X> a3 = this.f2195a.a(obj);
            DataCacheWriter dataCacheWriter = new DataCacheWriter(a3, obj, this.f2195a.i());
            this.g = new DataCacheKey(this.f.f2245a, this.f2195a.l());
            this.f2195a.d().a(this.g, dataCacheWriter);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.g + ", data: " + obj + ", encoder: " + a3 + ", duration: " + LogTime.a(a2));
            }
            this.f.c.b();
            this.d = new DataCacheGenerator(Collections.singletonList(this.f.f2245a), this.f2195a, this);
        } catch (Throwable th) {
            this.f.c.b();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ModelLoader.LoadData<?> loadData, Object obj) {
        DiskCacheStrategy e2 = this.f2195a.e();
        if (obj == null || !e2.a(loadData.c.c())) {
            DataFetcherGenerator.FetcherReadyCallback fetcherReadyCallback = this.b;
            Key key = loadData.f2245a;
            DataFetcher<Data> dataFetcher = loadData.c;
            fetcherReadyCallback.a(key, obj, dataFetcher, dataFetcher.c(), this.g);
            return;
        }
        this.e = obj;
        this.b.c();
    }

    /* access modifiers changed from: package-private */
    public void a(ModelLoader.LoadData<?> loadData, Exception exc) {
        DataFetcherGenerator.FetcherReadyCallback fetcherReadyCallback = this.b;
        DataCacheKey dataCacheKey = this.g;
        DataFetcher<Data> dataFetcher = loadData.c;
        fetcherReadyCallback.a(dataCacheKey, exc, dataFetcher, dataFetcher.c());
    }

    public void a(Key key, Object obj, DataFetcher<?> dataFetcher, DataSource dataSource, Key key2) {
        this.b.a(key, obj, dataFetcher, this.f.c.c(), key);
    }

    public void a(Key key, Exception exc, DataFetcher<?> dataFetcher, DataSource dataSource) {
        this.b.a(key, exc, dataFetcher, this.f.c.c());
    }
}
