package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.load.engine.bitmap_recycle.Poolable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class GroupedLinkedMap<K extends Poolable, V> {

    /* renamed from: a  reason: collision with root package name */
    private final LinkedEntry<K, V> f2200a = new LinkedEntry<>();
    private final Map<K, LinkedEntry<K, V>> b = new HashMap();

    private static class LinkedEntry<K, V> {

        /* renamed from: a  reason: collision with root package name */
        final K f2201a;
        private List<V> b;
        LinkedEntry<K, V> c;
        LinkedEntry<K, V> d;

        LinkedEntry() {
            this((Object) null);
        }

        public V a() {
            int b2 = b();
            if (b2 > 0) {
                return this.b.remove(b2 - 1);
            }
            return null;
        }

        public int b() {
            List<V> list = this.b;
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        LinkedEntry(K k) {
            this.d = this;
            this.c = this;
            this.f2201a = k;
        }

        public void a(V v) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            this.b.add(v);
        }
    }

    GroupedLinkedMap() {
    }

    private void b(LinkedEntry<K, V> linkedEntry) {
        c(linkedEntry);
        LinkedEntry<K, V> linkedEntry2 = this.f2200a;
        linkedEntry.d = linkedEntry2.d;
        linkedEntry.c = linkedEntry2;
        d(linkedEntry);
    }

    private static <K, V> void c(LinkedEntry<K, V> linkedEntry) {
        LinkedEntry<K, V> linkedEntry2 = linkedEntry.d;
        linkedEntry2.c = linkedEntry.c;
        linkedEntry.c.d = linkedEntry2;
    }

    private static <K, V> void d(LinkedEntry<K, V> linkedEntry) {
        linkedEntry.c.d = linkedEntry;
        linkedEntry.d.c = linkedEntry;
    }

    public void a(K k, V v) {
        LinkedEntry linkedEntry = this.b.get(k);
        if (linkedEntry == null) {
            linkedEntry = new LinkedEntry(k);
            b(linkedEntry);
            this.b.put(k, linkedEntry);
        } else {
            k.a();
        }
        linkedEntry.a(v);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (LinkedEntry<K, V> linkedEntry = this.f2200a.c; !linkedEntry.equals(this.f2200a); linkedEntry = linkedEntry.c) {
            z = true;
            sb.append('{');
            sb.append(linkedEntry.f2201a);
            sb.append(':');
            sb.append(linkedEntry.b());
            sb.append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        return sb.toString();
    }

    public V a(K k) {
        LinkedEntry linkedEntry = this.b.get(k);
        if (linkedEntry == null) {
            linkedEntry = new LinkedEntry(k);
            this.b.put(k, linkedEntry);
        } else {
            k.a();
        }
        a(linkedEntry);
        return linkedEntry.a();
    }

    public V a() {
        for (LinkedEntry<K, V> linkedEntry = this.f2200a.d; !linkedEntry.equals(this.f2200a); linkedEntry = linkedEntry.d) {
            V a2 = linkedEntry.a();
            if (a2 != null) {
                return a2;
            }
            c(linkedEntry);
            this.b.remove(linkedEntry.f2201a);
            ((Poolable) linkedEntry.f2201a).a();
        }
        return null;
    }

    private void a(LinkedEntry<K, V> linkedEntry) {
        c(linkedEntry);
        LinkedEntry<K, V> linkedEntry2 = this.f2200a;
        linkedEntry.d = linkedEntry2;
        linkedEntry.c = linkedEntry2.c;
        d(linkedEntry);
    }
}
