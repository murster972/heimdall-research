package com.bumptech.glide.load.engine.bitmap_recycle;

interface ArrayAdapterInterface<T> {
    int a();

    int a(T t);

    String getTag();

    T newArray(int i);
}
