package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Preconditions;

class EngineResource<Z> implements Resource<Z> {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f2187a;
    private final boolean b;
    private final Resource<Z> c;
    private final ResourceListener d;
    private final Key e;
    private int f;
    private boolean g;

    interface ResourceListener {
        void a(Key key, EngineResource<?> engineResource);
    }

    EngineResource(Resource<Z> resource, boolean z, boolean z2, Key key, ResourceListener resourceListener) {
        Preconditions.a(resource);
        this.c = resource;
        this.f2187a = z;
        this.b = z2;
        this.e = key;
        Preconditions.a(resourceListener);
        this.d = resourceListener;
    }

    public Class<Z> a() {
        return this.c.a();
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        if (!this.g) {
            this.f++;
        } else {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        }
    }

    /* access modifiers changed from: package-private */
    public Resource<Z> c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.f2187a;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        boolean z;
        synchronized (this) {
            if (this.f > 0) {
                z = true;
                int i = this.f - 1;
                this.f = i;
                if (i != 0) {
                    z = false;
                }
            } else {
                throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
            }
        }
        if (z) {
            this.d.a(this.e, this);
        }
    }

    public Z get() {
        return this.c.get();
    }

    public int getSize() {
        return this.c.getSize();
    }

    public synchronized void recycle() {
        if (this.f > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (!this.g) {
            this.g = true;
            if (this.b) {
                this.c.recycle();
            }
        } else {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        }
    }

    public synchronized String toString() {
        return "EngineResource{isMemoryCacheable=" + this.f2187a + ", listener=" + this.d + ", key=" + this.e + ", acquired=" + this.f + ", isRecycled=" + this.g + ", resource=" + this.c + '}';
    }
}
