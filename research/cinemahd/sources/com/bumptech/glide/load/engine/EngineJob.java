package com.bumptech.glide.load.engine;

import androidx.core.util.Pools$Pool;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.EngineResource;
import com.bumptech.glide.load.engine.executor.GlideExecutor;
import com.bumptech.glide.request.ResourceCallback;
import com.bumptech.glide.util.Executors;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.pool.FactoryPools;
import com.bumptech.glide.util.pool.StateVerifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

class EngineJob<R> implements DecodeJob.Callback<R>, FactoryPools.Poolable {
    private static final EngineResourceFactory z = new EngineResourceFactory();

    /* renamed from: a  reason: collision with root package name */
    final ResourceCallbacksAndExecutors f2182a;
    private final StateVerifier b;
    private final EngineResource.ResourceListener c;
    private final Pools$Pool<EngineJob<?>> d;
    private final EngineResourceFactory e;
    private final EngineJobListener f;
    private final GlideExecutor g;
    private final GlideExecutor h;
    private final GlideExecutor i;
    private final GlideExecutor j;
    private final AtomicInteger k;
    private Key l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    private Resource<?> q;
    DataSource r;
    private boolean s;
    GlideException t;
    private boolean u;
    EngineResource<?> v;
    private DecodeJob<R> w;
    private volatile boolean x;
    private boolean y;

    private class CallLoadFailed implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final ResourceCallback f2183a;

        CallLoadFailed(ResourceCallback resourceCallback) {
            this.f2183a = resourceCallback;
        }

        public void run() {
            synchronized (this.f2183a.getLock()) {
                synchronized (EngineJob.this) {
                    if (EngineJob.this.f2182a.a(this.f2183a)) {
                        EngineJob.this.a(this.f2183a);
                    }
                    EngineJob.this.c();
                }
            }
        }
    }

    private class CallResourceReady implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final ResourceCallback f2184a;

        CallResourceReady(ResourceCallback resourceCallback) {
            this.f2184a = resourceCallback;
        }

        public void run() {
            synchronized (this.f2184a.getLock()) {
                synchronized (EngineJob.this) {
                    if (EngineJob.this.f2182a.a(this.f2184a)) {
                        EngineJob.this.v.b();
                        EngineJob.this.b(this.f2184a);
                        EngineJob.this.c(this.f2184a);
                    }
                    EngineJob.this.c();
                }
            }
        }
    }

    static class EngineResourceFactory {
        EngineResourceFactory() {
        }

        public <R> EngineResource<R> a(Resource<R> resource, boolean z, Key key, EngineResource.ResourceListener resourceListener) {
            return new EngineResource(resource, z, true, key, resourceListener);
        }
    }

    static final class ResourceCallbackAndExecutor {

        /* renamed from: a  reason: collision with root package name */
        final ResourceCallback f2185a;
        final Executor b;

        ResourceCallbackAndExecutor(ResourceCallback resourceCallback, Executor executor) {
            this.f2185a = resourceCallback;
            this.b = executor;
        }

        public boolean equals(Object obj) {
            if (obj instanceof ResourceCallbackAndExecutor) {
                return this.f2185a.equals(((ResourceCallbackAndExecutor) obj).f2185a);
            }
            return false;
        }

        public int hashCode() {
            return this.f2185a.hashCode();
        }
    }

    static final class ResourceCallbacksAndExecutors implements Iterable<ResourceCallbackAndExecutor> {

        /* renamed from: a  reason: collision with root package name */
        private final List<ResourceCallbackAndExecutor> f2186a;

        ResourceCallbacksAndExecutors() {
            this(new ArrayList(2));
        }

        private static ResourceCallbackAndExecutor c(ResourceCallback resourceCallback) {
            return new ResourceCallbackAndExecutor(resourceCallback, Executors.a());
        }

        /* access modifiers changed from: package-private */
        public void a(ResourceCallback resourceCallback, Executor executor) {
            this.f2186a.add(new ResourceCallbackAndExecutor(resourceCallback, executor));
        }

        /* access modifiers changed from: package-private */
        public void b(ResourceCallback resourceCallback) {
            this.f2186a.remove(c(resourceCallback));
        }

        /* access modifiers changed from: package-private */
        public void clear() {
            this.f2186a.clear();
        }

        /* access modifiers changed from: package-private */
        public boolean isEmpty() {
            return this.f2186a.isEmpty();
        }

        public Iterator<ResourceCallbackAndExecutor> iterator() {
            return this.f2186a.iterator();
        }

        /* access modifiers changed from: package-private */
        public int size() {
            return this.f2186a.size();
        }

        ResourceCallbacksAndExecutors(List<ResourceCallbackAndExecutor> list) {
            this.f2186a = list;
        }

        /* access modifiers changed from: package-private */
        public boolean a(ResourceCallback resourceCallback) {
            return this.f2186a.contains(c(resourceCallback));
        }

        /* access modifiers changed from: package-private */
        public ResourceCallbacksAndExecutors a() {
            return new ResourceCallbacksAndExecutors(new ArrayList(this.f2186a));
        }
    }

    EngineJob(GlideExecutor glideExecutor, GlideExecutor glideExecutor2, GlideExecutor glideExecutor3, GlideExecutor glideExecutor4, EngineJobListener engineJobListener, EngineResource.ResourceListener resourceListener, Pools$Pool<EngineJob<?>> pools$Pool) {
        this(glideExecutor, glideExecutor2, glideExecutor3, glideExecutor4, engineJobListener, resourceListener, pools$Pool, z);
    }

    private GlideExecutor g() {
        if (this.n) {
            return this.i;
        }
        return this.o ? this.j : this.h;
    }

    private boolean h() {
        return this.u || this.s || this.x;
    }

    private synchronized void i() {
        if (this.l != null) {
            this.f2182a.clear();
            this.l = null;
            this.v = null;
            this.q = null;
            this.u = false;
            this.x = false;
            this.s = false;
            this.y = false;
            this.w.a(false);
            this.w = null;
            this.t = null;
            this.r = null;
            this.d.release(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized EngineJob<R> a(Key key, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.l = key;
        this.m = z2;
        this.n = z3;
        this.o = z4;
        this.p = z5;
        return this;
    }

    public synchronized void b(DecodeJob<R> decodeJob) {
        this.w = decodeJob;
        (decodeJob.d() ? this.g : g()).execute(decodeJob);
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(ResourceCallback resourceCallback) {
        boolean z2;
        this.b.a();
        this.f2182a.b(resourceCallback);
        if (this.f2182a.isEmpty()) {
            a();
            if (!this.s) {
                if (!this.u) {
                    z2 = false;
                    if (z2 && this.k.get() == 0) {
                        i();
                    }
                }
            }
            z2 = true;
            i();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r4.f.a(r4, r1, (com.bumptech.glide.load.engine.EngineResource<?>) null);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r0.hasNext() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r1 = r0.next();
        r1.b.execute(new com.bumptech.glide.load.engine.EngineJob.CallLoadFailed(r4, r1.f2185a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.bumptech.glide.util.pool.StateVerifier r0 = r4.b     // Catch:{ all -> 0x0066 }
            r0.a()     // Catch:{ all -> 0x0066 }
            boolean r0 = r4.x     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x000f
            r4.i()     // Catch:{ all -> 0x0066 }
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            return
        L_0x000f:
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbacksAndExecutors r0 = r4.f2182a     // Catch:{ all -> 0x0066 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x005e
            boolean r0 = r4.u     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0056
            r0 = 1
            r4.u = r0     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.Key r1 = r4.l     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbacksAndExecutors r2 = r4.f2182a     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbacksAndExecutors r2 = r2.a()     // Catch:{ all -> 0x0066 }
            int r3 = r2.size()     // Catch:{ all -> 0x0066 }
            int r3 = r3 + r0
            r4.a((int) r3)     // Catch:{ all -> 0x0066 }
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.engine.EngineJobListener r0 = r4.f
            r3 = 0
            r0.a(r4, r1, r3)
            java.util.Iterator r0 = r2.iterator()
        L_0x0039:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0052
            java.lang.Object r1 = r0.next()
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbackAndExecutor r1 = (com.bumptech.glide.load.engine.EngineJob.ResourceCallbackAndExecutor) r1
            java.util.concurrent.Executor r2 = r1.b
            com.bumptech.glide.load.engine.EngineJob$CallLoadFailed r3 = new com.bumptech.glide.load.engine.EngineJob$CallLoadFailed
            com.bumptech.glide.request.ResourceCallback r1 = r1.f2185a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x0039
        L_0x0052:
            r4.c()
            return
        L_0x0056:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = "Already failed once"
            r0.<init>(r1)     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = "Received an exception without any callbacks to notify"
            r0.<init>(r1)     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.EngineJob.d():void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r5.f.a(r5, r0, r2);
        r0 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (r0.hasNext() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
        r1 = r0.next();
        r1.b.execute(new com.bumptech.glide.load.engine.EngineJob.CallResourceReady(r5, r1.f2185a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e() {
        /*
            r5 = this;
            monitor-enter(r5)
            com.bumptech.glide.util.pool.StateVerifier r0 = r5.b     // Catch:{ all -> 0x007c }
            r0.a()     // Catch:{ all -> 0x007c }
            boolean r0 = r5.x     // Catch:{ all -> 0x007c }
            if (r0 == 0) goto L_0x0014
            com.bumptech.glide.load.engine.Resource<?> r0 = r5.q     // Catch:{ all -> 0x007c }
            r0.recycle()     // Catch:{ all -> 0x007c }
            r5.i()     // Catch:{ all -> 0x007c }
            monitor-exit(r5)     // Catch:{ all -> 0x007c }
            return
        L_0x0014:
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbacksAndExecutors r0 = r5.f2182a     // Catch:{ all -> 0x007c }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x007c }
            if (r0 != 0) goto L_0x0074
            boolean r0 = r5.s     // Catch:{ all -> 0x007c }
            if (r0 != 0) goto L_0x006c
            com.bumptech.glide.load.engine.EngineJob$EngineResourceFactory r0 = r5.e     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.engine.Resource<?> r1 = r5.q     // Catch:{ all -> 0x007c }
            boolean r2 = r5.m     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.Key r3 = r5.l     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.engine.EngineResource$ResourceListener r4 = r5.c     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.engine.EngineResource r0 = r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x007c }
            r5.v = r0     // Catch:{ all -> 0x007c }
            r0 = 1
            r5.s = r0     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbacksAndExecutors r1 = r5.f2182a     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbacksAndExecutors r1 = r1.a()     // Catch:{ all -> 0x007c }
            int r2 = r1.size()     // Catch:{ all -> 0x007c }
            int r2 = r2 + r0
            r5.a((int) r2)     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.Key r0 = r5.l     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.engine.EngineResource<?> r2 = r5.v     // Catch:{ all -> 0x007c }
            monitor-exit(r5)     // Catch:{ all -> 0x007c }
            com.bumptech.glide.load.engine.EngineJobListener r3 = r5.f
            r3.a(r5, r0, r2)
            java.util.Iterator r0 = r1.iterator()
        L_0x004f:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0068
            java.lang.Object r1 = r0.next()
            com.bumptech.glide.load.engine.EngineJob$ResourceCallbackAndExecutor r1 = (com.bumptech.glide.load.engine.EngineJob.ResourceCallbackAndExecutor) r1
            java.util.concurrent.Executor r2 = r1.b
            com.bumptech.glide.load.engine.EngineJob$CallResourceReady r3 = new com.bumptech.glide.load.engine.EngineJob$CallResourceReady
            com.bumptech.glide.request.ResourceCallback r1 = r1.f2185a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x004f
        L_0x0068:
            r5.c()
            return
        L_0x006c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x007c }
            java.lang.String r1 = "Already have resource"
            r0.<init>(r1)     // Catch:{ all -> 0x007c }
            throw r0     // Catch:{ all -> 0x007c }
        L_0x0074:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x007c }
            java.lang.String r1 = "Received a resource without any callbacks to notify"
            r0.<init>(r1)     // Catch:{ all -> 0x007c }
            throw r0     // Catch:{ all -> 0x007c }
        L_0x007c:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x007c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.EngineJob.e():void");
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.p;
    }

    public void onLoadFailed(GlideException glideException) {
        synchronized (this) {
            this.t = glideException;
        }
        d();
    }

    public void onResourceReady(Resource<R> resource, DataSource dataSource, boolean z2) {
        synchronized (this) {
            this.q = resource;
            this.r = dataSource;
            this.y = z2;
        }
        e();
    }

    EngineJob(GlideExecutor glideExecutor, GlideExecutor glideExecutor2, GlideExecutor glideExecutor3, GlideExecutor glideExecutor4, EngineJobListener engineJobListener, EngineResource.ResourceListener resourceListener, Pools$Pool<EngineJob<?>> pools$Pool, EngineResourceFactory engineResourceFactory) {
        this.f2182a = new ResourceCallbacksAndExecutors();
        this.b = StateVerifier.b();
        this.k = new AtomicInteger();
        this.g = glideExecutor;
        this.h = glideExecutor2;
        this.i = glideExecutor3;
        this.j = glideExecutor4;
        this.f = engineJobListener;
        this.c = resourceListener;
        this.d = pools$Pool;
        this.e = engineResourceFactory;
    }

    /* access modifiers changed from: package-private */
    public void b(ResourceCallback resourceCallback) {
        try {
            resourceCallback.onResourceReady(this.v, this.r, this.y);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(ResourceCallback resourceCallback, Executor executor) {
        this.b.a();
        this.f2182a.a(resourceCallback, executor);
        boolean z2 = true;
        if (this.s) {
            a(1);
            executor.execute(new CallResourceReady(resourceCallback));
        } else if (this.u) {
            a(1);
            executor.execute(new CallLoadFailed(resourceCallback));
        } else {
            if (this.x) {
                z2 = false;
            }
            Preconditions.a(z2, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    public StateVerifier b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        EngineResource<?> engineResource;
        synchronized (this) {
            this.b.a();
            Preconditions.a(h(), "Not yet complete!");
            int decrementAndGet = this.k.decrementAndGet();
            Preconditions.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                engineResource = this.v;
                i();
            } else {
                engineResource = null;
            }
        }
        if (engineResource != null) {
            engineResource.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ResourceCallback resourceCallback) {
        try {
            resourceCallback.onLoadFailed(this.t);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (!h()) {
            this.x = true;
            this.w.a();
            this.f.a(this, this.l);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(int i2) {
        Preconditions.a(h(), "Not yet complete!");
        if (this.k.getAndAdd(i2) == 0 && this.v != null) {
            this.v.b();
        }
    }

    public void a(DecodeJob<?> decodeJob) {
        g().execute(decodeJob);
    }
}
