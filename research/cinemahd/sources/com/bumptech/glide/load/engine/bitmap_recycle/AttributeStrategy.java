package com.bumptech.glide.load.engine.bitmap_recycle;

import android.graphics.Bitmap;
import com.bumptech.glide.util.Util;

class AttributeStrategy implements LruPoolStrategy {

    /* renamed from: a  reason: collision with root package name */
    private final KeyPool f2197a = new KeyPool();
    private final GroupedLinkedMap<Key, Bitmap> b = new GroupedLinkedMap<>();

    static class KeyPool extends BaseKeyPool<Key> {
        KeyPool() {
        }

        /* access modifiers changed from: package-private */
        public Key a(int i, int i2, Bitmap.Config config) {
            Key key = (Key) b();
            key.a(i, i2, config);
            return key;
        }

        /* access modifiers changed from: protected */
        public Key a() {
            return new Key(this);
        }
    }

    AttributeStrategy() {
    }

    private static String d(Bitmap bitmap) {
        return c(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    public void a(Bitmap bitmap) {
        this.b.a(this.f2197a.a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    public String b(Bitmap bitmap) {
        return d(bitmap);
    }

    public int c(Bitmap bitmap) {
        return Util.a(bitmap);
    }

    public Bitmap removeLast() {
        return this.b.a();
    }

    public String toString() {
        return "AttributeStrategy:\n  " + this.b;
    }

    static String c(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    public String b(int i, int i2, Bitmap.Config config) {
        return c(i, i2, config);
    }

    static class Key implements Poolable {

        /* renamed from: a  reason: collision with root package name */
        private final KeyPool f2198a;
        private int b;
        private int c;
        private Bitmap.Config d;

        public Key(KeyPool keyPool) {
            this.f2198a = keyPool;
        }

        public void a(int i, int i2, Bitmap.Config config) {
            this.b = i;
            this.c = i2;
            this.d = config;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Key)) {
                return false;
            }
            Key key = (Key) obj;
            if (this.b == key.b && this.c == key.c && this.d == key.d) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i = ((this.b * 31) + this.c) * 31;
            Bitmap.Config config = this.d;
            return i + (config != null ? config.hashCode() : 0);
        }

        public String toString() {
            return AttributeStrategy.c(this.b, this.c, this.d);
        }

        public void a() {
            this.f2198a.a(this);
        }
    }

    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.b.a(this.f2197a.a(i, i2, config));
    }
}
