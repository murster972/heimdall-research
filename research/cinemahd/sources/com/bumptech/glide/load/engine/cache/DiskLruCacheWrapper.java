package com.bumptech.glide.load.engine.cache;

import android.util.Log;
import com.bumptech.glide.disklrucache.DiskLruCache;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.cache.DiskCache;
import java.io.File;
import java.io.IOException;

public class DiskLruCacheWrapper implements DiskCache {

    /* renamed from: a  reason: collision with root package name */
    private final SafeKeyGenerator f2212a;
    private final File b;
    private final long c;
    private final DiskCacheWriteLocker d = new DiskCacheWriteLocker();
    private DiskLruCache e;

    @Deprecated
    protected DiskLruCacheWrapper(File file, long j) {
        this.b = file;
        this.c = j;
        this.f2212a = new SafeKeyGenerator();
    }

    public static DiskCache a(File file, long j) {
        return new DiskLruCacheWrapper(file, j);
    }

    private synchronized DiskLruCache a() throws IOException {
        if (this.e == null) {
            this.e = DiskLruCache.a(this.b, 1, 1, this.c);
        }
        return this.e;
    }

    public File a(Key key) {
        String a2 = this.f2212a.a(key);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + a2 + " for for Key: " + key);
        }
        try {
            DiskLruCache.Value f = a().f(a2);
            if (f != null) {
                return f.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    public void a(Key key, DiskCache.Writer writer) {
        DiskLruCache.Editor e2;
        String a2 = this.f2212a.a(key);
        this.d.a(a2);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + a2 + " for for Key: " + key);
            }
            try {
                DiskLruCache a3 = a();
                if (a3.f(a2) == null) {
                    e2 = a3.e(a2);
                    if (e2 != null) {
                        if (writer.a(e2.a(0))) {
                            e2.c();
                        }
                        e2.b();
                        this.d.b(a2);
                        return;
                    }
                    throw new IllegalStateException("Had two simultaneous puts for: " + a2);
                }
            } catch (IOException e3) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e3);
                }
            } catch (Throwable th) {
                e2.b();
                throw th;
            }
        } finally {
            this.d.b(a2);
        }
    }
}
