package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.gif.GifFrameLoader;
import com.bumptech.glide.util.Preconditions;
import java.nio.ByteBuffer;
import java.util.List;

public class GifDrawable extends Drawable implements GifFrameLoader.FrameCallback, Animatable, Animatable2Compat {

    /* renamed from: a  reason: collision with root package name */
    private final GifState f2321a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private int f;
    private int g;
    private boolean h;
    private Paint i;
    private Rect j;
    private List<Animatable2Compat.AnimationCallback> k;

    static final class GifState extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        final GifFrameLoader f2322a;

        GifState(GifFrameLoader gifFrameLoader) {
            this.f2322a = gifFrameLoader;
        }

        public int getChangingConfigurations() {
            return 0;
        }

        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }

        public Drawable newDrawable() {
            return new GifDrawable(this);
        }
    }

    public GifDrawable(Context context, GifDecoder gifDecoder, Transformation<Bitmap> transformation, int i2, int i3, Bitmap bitmap) {
        this(new GifState(new GifFrameLoader(Glide.a(context), gifDecoder, i2, i3, transformation, bitmap)));
    }

    private Drawable.Callback h() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        return callback;
    }

    private Rect i() {
        if (this.j == null) {
            this.j = new Rect();
        }
        return this.j;
    }

    private Paint j() {
        if (this.i == null) {
            this.i = new Paint(2);
        }
        return this.i;
    }

    private void k() {
        List<Animatable2Compat.AnimationCallback> list = this.k;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.k.get(i2).a(this);
            }
        }
    }

    private void l() {
        this.f = 0;
    }

    private void m() {
        Preconditions.a(!this.d, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.f2321a.f2322a.f() == 1) {
            invalidateSelf();
        } else if (!this.b) {
            this.b = true;
            this.f2321a.f2322a.a((GifFrameLoader.FrameCallback) this);
            invalidateSelf();
        }
    }

    private void n() {
        this.b = false;
        this.f2321a.f2322a.b(this);
    }

    public void a(Transformation<Bitmap> transformation, Bitmap bitmap) {
        this.f2321a.f2322a.a(transformation, bitmap);
    }

    public ByteBuffer b() {
        return this.f2321a.f2322a.b();
    }

    public Bitmap c() {
        return this.f2321a.f2322a.e();
    }

    public int d() {
        return this.f2321a.f2322a.f();
    }

    public void draw(Canvas canvas) {
        if (!this.d) {
            if (this.h) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), i());
                this.h = false;
            }
            canvas.drawBitmap(this.f2321a.f2322a.c(), (Rect) null, i(), j());
        }
    }

    public int e() {
        return this.f2321a.f2322a.d();
    }

    public int f() {
        return this.f2321a.f2322a.h();
    }

    public void g() {
        this.d = true;
        this.f2321a.f2322a.a();
    }

    public Drawable.ConstantState getConstantState() {
        return this.f2321a;
    }

    public int getIntrinsicHeight() {
        return this.f2321a.f2322a.g();
    }

    public int getIntrinsicWidth() {
        return this.f2321a.f2322a.i();
    }

    public int getOpacity() {
        return -2;
    }

    public boolean isRunning() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.h = true;
    }

    public void setAlpha(int i2) {
        j().setAlpha(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        j().setColorFilter(colorFilter);
    }

    public boolean setVisible(boolean z, boolean z2) {
        Preconditions.a(!this.d, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.e = z;
        if (!z) {
            n();
        } else if (this.c) {
            m();
        }
        return super.setVisible(z, z2);
    }

    public void start() {
        this.c = true;
        l();
        if (this.e) {
            m();
        }
    }

    public void stop() {
        this.c = false;
        n();
    }

    public void a() {
        if (h() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (e() == d() - 1) {
            this.f++;
        }
        int i2 = this.g;
        if (i2 != -1 && this.f >= i2) {
            k();
            stop();
        }
    }

    GifDrawable(GifState gifState) {
        this.e = true;
        this.g = -1;
        Preconditions.a(gifState);
        this.f2321a = gifState;
    }
}
