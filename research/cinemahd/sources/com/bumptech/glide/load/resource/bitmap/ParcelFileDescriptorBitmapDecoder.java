package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import java.io.IOException;

public final class ParcelFileDescriptorBitmapDecoder implements ResourceDecoder<ParcelFileDescriptor, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final Downsampler f2301a;

    public ParcelFileDescriptorBitmapDecoder(Downsampler downsampler) {
        this.f2301a = downsampler;
    }

    public boolean a(ParcelFileDescriptor parcelFileDescriptor, Options options) {
        return this.f2301a.a(parcelFileDescriptor);
    }

    public Resource<Bitmap> a(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, Options options) throws IOException {
        return this.f2301a.a(parcelFileDescriptor, i, i2, options);
    }
}
