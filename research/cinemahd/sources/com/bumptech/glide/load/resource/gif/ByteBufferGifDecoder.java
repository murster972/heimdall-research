package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.gifdecoder.GifHeader;
import com.bumptech.glide.gifdecoder.GifHeaderParser;
import com.bumptech.glide.gifdecoder.StandardGifDecoder;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.UnitTransformation;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Util;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;

public class ByteBufferGifDecoder implements ResourceDecoder<ByteBuffer, GifDrawable> {
    private static final GifDecoderFactory f = new GifDecoderFactory();
    private static final GifHeaderParserPool g = new GifHeaderParserPool();

    /* renamed from: a  reason: collision with root package name */
    private final Context f2318a;
    private final List<ImageHeaderParser> b;
    private final GifHeaderParserPool c;
    private final GifDecoderFactory d;
    private final GifBitmapProvider e;

    static class GifDecoderFactory {
        GifDecoderFactory() {
        }

        /* access modifiers changed from: package-private */
        public GifDecoder a(GifDecoder.BitmapProvider bitmapProvider, GifHeader gifHeader, ByteBuffer byteBuffer, int i) {
            return new StandardGifDecoder(bitmapProvider, gifHeader, byteBuffer, i);
        }
    }

    public ByteBufferGifDecoder(Context context, List<ImageHeaderParser> list, BitmapPool bitmapPool, ArrayPool arrayPool) {
        this(context, list, bitmapPool, arrayPool, g, f);
    }

    ByteBufferGifDecoder(Context context, List<ImageHeaderParser> list, BitmapPool bitmapPool, ArrayPool arrayPool, GifHeaderParserPool gifHeaderParserPool, GifDecoderFactory gifDecoderFactory) {
        this.f2318a = context.getApplicationContext();
        this.b = list;
        this.d = gifDecoderFactory;
        this.e = new GifBitmapProvider(bitmapPool, arrayPool);
        this.c = gifHeaderParserPool;
    }

    static class GifHeaderParserPool {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<GifHeaderParser> f2319a = Util.a(0);

        GifHeaderParserPool() {
        }

        /* access modifiers changed from: package-private */
        public synchronized GifHeaderParser a(ByteBuffer byteBuffer) {
            GifHeaderParser poll;
            poll = this.f2319a.poll();
            if (poll == null) {
                poll = new GifHeaderParser();
            }
            return poll.a(byteBuffer);
        }

        /* access modifiers changed from: package-private */
        public synchronized void a(GifHeaderParser gifHeaderParser) {
            gifHeaderParser.a();
            this.f2319a.offer(gifHeaderParser);
        }
    }

    public boolean a(ByteBuffer byteBuffer, Options options) throws IOException {
        return !((Boolean) options.a(GifOptions.b)).booleanValue() && ImageHeaderParserUtils.a(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }

    public GifDrawableResource a(ByteBuffer byteBuffer, int i, int i2, Options options) {
        GifHeaderParser a2 = this.c.a(byteBuffer);
        try {
            return a(byteBuffer, i, i2, a2, options);
        } finally {
            this.c.a(a2);
        }
    }

    private GifDrawableResource a(ByteBuffer byteBuffer, int i, int i2, GifHeaderParser gifHeaderParser, Options options) {
        Bitmap.Config config;
        long a2 = LogTime.a();
        try {
            GifHeader b2 = gifHeaderParser.b();
            if (b2.b() > 0) {
                if (b2.c() == 0) {
                    if (options.a(GifOptions.f2326a) == DecodeFormat.PREFER_RGB_565) {
                        config = Bitmap.Config.RGB_565;
                    } else {
                        config = Bitmap.Config.ARGB_8888;
                    }
                    Bitmap.Config config2 = config;
                    GifDecoder a3 = this.d.a(this.e, b2, byteBuffer, a(b2, i, i2));
                    a3.a(config2);
                    a3.b();
                    Bitmap a4 = a3.a();
                    if (a4 == null) {
                        if (Log.isLoggable("BufferGifDecoder", 2)) {
                            Log.v("BufferGifDecoder", "Decoded GIF from stream in " + LogTime.a(a2));
                        }
                        return null;
                    }
                    GifDrawableResource gifDrawableResource = new GifDrawableResource(new GifDrawable(this.f2318a, a3, UnitTransformation.a(), i, i2, a4));
                    if (Log.isLoggable("BufferGifDecoder", 2)) {
                        Log.v("BufferGifDecoder", "Decoded GIF from stream in " + LogTime.a(a2));
                    }
                    return gifDrawableResource;
                }
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + LogTime.a(a2));
            }
        }
    }

    private static int a(GifHeader gifHeader, int i, int i2) {
        int i3;
        int min = Math.min(gifHeader.a() / i2, gifHeader.d() / i);
        if (min == 0) {
            i3 = 0;
        } else {
            i3 = Integer.highestOneBit(min);
        }
        int max = Math.max(1, i3);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + gifHeader.d() + "x" + gifHeader.a() + "]");
        }
        return max;
    }
}
