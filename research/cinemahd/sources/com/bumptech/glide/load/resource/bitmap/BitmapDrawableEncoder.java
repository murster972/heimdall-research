package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import java.io.File;

public class BitmapDrawableEncoder implements ResourceEncoder<BitmapDrawable> {

    /* renamed from: a  reason: collision with root package name */
    private final BitmapPool f2283a;
    private final ResourceEncoder<Bitmap> b;

    public BitmapDrawableEncoder(BitmapPool bitmapPool, ResourceEncoder<Bitmap> resourceEncoder) {
        this.f2283a = bitmapPool;
        this.b = resourceEncoder;
    }

    public boolean a(Resource<BitmapDrawable> resource, File file, Options options) {
        return this.b.a(new BitmapResource(resource.get().getBitmap(), this.f2283a), file, options);
    }

    public EncodeStrategy a(Options options) {
        return this.b.a(options);
    }
}
