package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

public final class GifBitmapProvider implements GifDecoder.BitmapProvider {

    /* renamed from: a  reason: collision with root package name */
    private final BitmapPool f2320a;
    private final ArrayPool b;

    public GifBitmapProvider(BitmapPool bitmapPool, ArrayPool arrayPool) {
        this.f2320a = bitmapPool;
        this.b = arrayPool;
    }

    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.f2320a.b(i, i2, config);
    }

    public int[] b(int i) {
        ArrayPool arrayPool = this.b;
        if (arrayPool == null) {
            return new int[i];
        }
        return (int[]) arrayPool.a(i, int[].class);
    }

    public void a(Bitmap bitmap) {
        this.f2320a.a(bitmap);
    }

    public byte[] a(int i) {
        ArrayPool arrayPool = this.b;
        if (arrayPool == null) {
            return new byte[i];
        }
        return (byte[]) arrayPool.a(i, byte[].class);
    }

    public void a(byte[] bArr) {
        ArrayPool arrayPool = this.b;
        if (arrayPool != null) {
            arrayPool.put(bArr);
        }
    }

    public void a(int[] iArr) {
        ArrayPool arrayPool = this.b;
        if (arrayPool != null) {
            arrayPool.put(iArr);
        }
    }
}
