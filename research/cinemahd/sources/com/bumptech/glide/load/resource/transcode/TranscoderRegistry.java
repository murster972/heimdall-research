package com.bumptech.glide.load.resource.transcode;

import java.util.ArrayList;
import java.util.List;

public class TranscoderRegistry {

    /* renamed from: a  reason: collision with root package name */
    private final List<Entry<?, ?>> f2331a = new ArrayList();

    private static final class Entry<Z, R> {

        /* renamed from: a  reason: collision with root package name */
        private final Class<Z> f2332a;
        private final Class<R> b;
        final ResourceTranscoder<Z, R> c;

        Entry(Class<Z> cls, Class<R> cls2, ResourceTranscoder<Z, R> resourceTranscoder) {
            this.f2332a = cls;
            this.b = cls2;
            this.c = resourceTranscoder;
        }

        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.f2332a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    public synchronized <Z, R> void a(Class<Z> cls, Class<R> cls2, ResourceTranscoder<Z, R> resourceTranscoder) {
        this.f2331a.add(new Entry(cls, cls2, resourceTranscoder));
    }

    public synchronized <Z, R> List<Class<R>> b(Class<Z> cls, Class<R> cls2) {
        ArrayList arrayList = new ArrayList();
        if (cls2.isAssignableFrom(cls)) {
            arrayList.add(cls2);
            return arrayList;
        }
        for (Entry<?, ?> a2 : this.f2331a) {
            if (a2.a(cls, cls2)) {
                arrayList.add(cls2);
            }
        }
        return arrayList;
    }

    public synchronized <Z, R> ResourceTranscoder<Z, R> a(Class<Z> cls, Class<R> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return UnitTranscoder.a();
        }
        for (Entry next : this.f2331a) {
            if (next.a(cls, cls2)) {
                return next.c;
            }
        }
        throw new IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
    }
}
