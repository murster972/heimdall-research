package com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

public final class HardwareConfigState {
    public static final boolean g = (Build.VERSION.SDK_INT < 29);
    public static final boolean h;
    private static final File i = new File("/proc/self/fd");
    private static volatile HardwareConfigState j;
    private static volatile int k = -1;

    /* renamed from: a  reason: collision with root package name */
    private final boolean f2296a = e();
    private final int b;
    private final int c;
    private int d;
    private boolean e = true;
    private final AtomicBoolean f = new AtomicBoolean(false);

    static {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 26) {
            z = false;
        }
        h = z;
    }

    HardwareConfigState() {
        if (Build.VERSION.SDK_INT >= 28) {
            this.b = 20000;
            this.c = 0;
            return;
        }
        this.b = 700;
        this.c = 128;
    }

    public static HardwareConfigState b() {
        if (j == null) {
            synchronized (HardwareConfigState.class) {
                if (j == null) {
                    j = new HardwareConfigState();
                }
            }
        }
        return j;
    }

    private int c() {
        if (k != -1) {
            return k;
        }
        return this.b;
    }

    private synchronized boolean d() {
        int i2 = this.d + 1;
        this.d = i2;
        if (i2 >= 50) {
            boolean z = false;
            this.d = 0;
            int length = i.list().length;
            long c2 = (long) c();
            if (((long) length) < c2) {
                z = true;
            }
            this.e = z;
            if (!this.e && Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + c2);
            }
        }
        return this.e;
    }

    private static boolean e() {
        return !f() && !g();
    }

    private static boolean f() {
        if (Build.VERSION.SDK_INT != 26) {
            return false;
        }
        for (String startsWith : Arrays.asList(new String[]{"SC-04J", "SM-N935", "SM-J720", "SM-G570F", "SM-G570M", "SM-G960", "SM-G965", "SM-G935", "SM-G930", "SM-A520", "SM-A720F", "moto e5", "moto e5 play", "moto e5 plus", "moto e5 cruise", "moto g(6) forge", "moto g(6) play"})) {
            if (Build.MODEL.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private static boolean g() {
        if (Build.VERSION.SDK_INT != 27) {
            return false;
        }
        return Arrays.asList(new String[]{"LG-M250", "LG-M320", "LG-Q710AL", "LG-Q710PL", "LGM-K121K", "LGM-K121L", "LGM-K121S", "LGM-X320K", "LGM-X320L", "LGM-X320S", "LGM-X401L", "LGM-X401S", "LM-Q610.FG", "LM-Q610.FGN", "LM-Q617.FG", "LM-Q617.FGN", "LM-Q710.FG", "LM-Q710.FGN", "LM-X220PM", "LM-X220QMA", "LM-X410PM"}).contains(Build.MODEL);
    }

    public boolean a(int i2, int i3, boolean z, boolean z2) {
        if (!z) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by caller");
            }
            return false;
        } else if (!this.f2296a) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by device model");
            }
            return false;
        } else if (!h) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by sdk");
            }
            return false;
        } else if (a()) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by app state");
            }
            return false;
        } else if (z2) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed because exif orientation is required");
            }
            return false;
        } else {
            int i4 = this.c;
            if (i2 < i4) {
                if (Log.isLoggable("HardwareConfig", 2)) {
                    Log.v("HardwareConfig", "Hardware config disallowed because width is too small");
                }
                return false;
            } else if (i3 < i4) {
                if (Log.isLoggable("HardwareConfig", 2)) {
                    Log.v("HardwareConfig", "Hardware config disallowed because height is too small");
                }
                return false;
            } else if (d()) {
                return true;
            } else {
                if (Log.isLoggable("HardwareConfig", 2)) {
                    Log.v("HardwareConfig", "Hardware config disallowed because there are insufficient FDs");
                }
                return false;
            }
        }
    }

    private boolean a() {
        return g && !this.f.get();
    }

    /* access modifiers changed from: package-private */
    @TargetApi(26)
    public boolean a(int i2, int i3, BitmapFactory.Options options, boolean z, boolean z2) {
        boolean a2 = a(i2, i3, z, z2);
        if (a2) {
            options.inPreferredConfig = Bitmap.Config.HARDWARE;
            options.inMutable = false;
        }
        return a2;
    }
}
