package com.bumptech.glide.load.resource;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Preconditions;

public class SimpleResource<T> implements Resource<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final T f2281a;

    public SimpleResource(T t) {
        Preconditions.a(t);
        this.f2281a = t;
    }

    public Class<T> a() {
        return this.f2281a.getClass();
    }

    public final T get() {
        return this.f2281a;
    }

    public final int getSize() {
        return 1;
    }

    public void recycle() {
    }
}
