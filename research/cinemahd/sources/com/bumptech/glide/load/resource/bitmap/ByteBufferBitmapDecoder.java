package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ByteBufferBitmapDecoder implements ResourceDecoder<ByteBuffer, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final Downsampler f2286a;

    public ByteBufferBitmapDecoder(Downsampler downsampler) {
        this.f2286a = downsampler;
    }

    public boolean a(ByteBuffer byteBuffer, Options options) {
        return this.f2286a.a(byteBuffer);
    }

    public Resource<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, Options options) throws IOException {
        return this.f2286a.a(ByteBufferUtil.c(byteBuffer), i, i2, options);
    }
}
