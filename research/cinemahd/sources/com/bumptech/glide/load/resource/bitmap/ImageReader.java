package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.data.InputStreamRewinder;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.Preconditions;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

interface ImageReader {

    public static final class InputStreamImageReader implements ImageReader {

        /* renamed from: a  reason: collision with root package name */
        private final InputStreamRewinder f2297a;
        private final ArrayPool b;
        private final List<ImageHeaderParser> c;

        InputStreamImageReader(InputStream inputStream, List<ImageHeaderParser> list, ArrayPool arrayPool) {
            Preconditions.a(arrayPool);
            this.b = arrayPool;
            Preconditions.a(list);
            this.c = list;
            this.f2297a = new InputStreamRewinder(inputStream, arrayPool);
        }

        public Bitmap a(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeStream(this.f2297a.a(), (Rect) null, options);
        }

        public int b() throws IOException {
            return ImageHeaderParserUtils.a(this.c, this.f2297a.a(), this.b);
        }

        public ImageHeaderParser.ImageType c() throws IOException {
            return ImageHeaderParserUtils.b(this.c, this.f2297a.a(), this.b);
        }

        public void a() {
            this.f2297a.c();
        }
    }

    public static final class ParcelFileDescriptorImageReader implements ImageReader {

        /* renamed from: a  reason: collision with root package name */
        private final ArrayPool f2298a;
        private final List<ImageHeaderParser> b;
        private final ParcelFileDescriptorRewinder c;

        ParcelFileDescriptorImageReader(ParcelFileDescriptor parcelFileDescriptor, List<ImageHeaderParser> list, ArrayPool arrayPool) {
            Preconditions.a(arrayPool);
            this.f2298a = arrayPool;
            Preconditions.a(list);
            this.b = list;
            this.c = new ParcelFileDescriptorRewinder(parcelFileDescriptor);
        }

        public Bitmap a(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeFileDescriptor(this.c.a().getFileDescriptor(), (Rect) null, options);
        }

        public void a() {
        }

        public int b() throws IOException {
            return ImageHeaderParserUtils.a(this.b, this.c, this.f2298a);
        }

        public ImageHeaderParser.ImageType c() throws IOException {
            return ImageHeaderParserUtils.b(this.b, this.c, this.f2298a);
        }
    }

    Bitmap a(BitmapFactory.Options options) throws IOException;

    void a();

    int b() throws IOException;

    ImageHeaderParser.ImageType c() throws IOException;
}
