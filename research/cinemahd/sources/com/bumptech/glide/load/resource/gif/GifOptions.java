package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Option;

public final class GifOptions {

    /* renamed from: a  reason: collision with root package name */
    public static final Option<DecodeFormat> f2326a = Option.a("com.bumptech.glide.load.resource.gif.GifOptions.DecodeFormat", DecodeFormat.c);
    public static final Option<Boolean> b = Option.a("com.bumptech.glide.load.resource.gif.GifOptions.DisableAnimation", false);

    private GifOptions() {
    }
}
