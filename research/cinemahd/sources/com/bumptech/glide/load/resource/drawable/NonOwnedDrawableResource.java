package com.bumptech.glide.load.resource.drawable;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.Resource;

final class NonOwnedDrawableResource extends DrawableResource<Drawable> {
    private NonOwnedDrawableResource(Drawable drawable) {
        super(drawable);
    }

    static Resource<Drawable> a(Drawable drawable) {
        if (drawable != null) {
            return new NonOwnedDrawableResource(drawable);
        }
        return null;
    }

    public int getSize() {
        return Math.max(1, this.f2316a.getIntrinsicWidth() * this.f2316a.getIntrinsicHeight() * 4);
    }

    public void recycle() {
    }

    public Class<Drawable> a() {
        return this.f2316a.getClass();
    }
}
