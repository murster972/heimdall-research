package com.bumptech.glide.load.resource.bytes;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Preconditions;

public class BytesResource implements Resource<byte[]> {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f2314a;

    public BytesResource(byte[] bArr) {
        Preconditions.a(bArr);
        this.f2314a = bArr;
    }

    public Class<byte[]> a() {
        return byte[].class;
    }

    public int getSize() {
        return this.f2314a.length;
    }

    public void recycle() {
    }

    public byte[] get() {
        return this.f2314a;
    }
}
