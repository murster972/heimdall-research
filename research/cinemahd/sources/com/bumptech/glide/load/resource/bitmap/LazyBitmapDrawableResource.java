package com.bumptech.glide.load.resource.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Preconditions;

public final class LazyBitmapDrawableResource implements Resource<BitmapDrawable>, Initializable {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f2300a;
    private final Resource<Bitmap> b;

    private LazyBitmapDrawableResource(Resources resources, Resource<Bitmap> resource) {
        Preconditions.a(resources);
        this.f2300a = resources;
        Preconditions.a(resource);
        this.b = resource;
    }

    public static Resource<BitmapDrawable> a(Resources resources, Resource<Bitmap> resource) {
        if (resource == null) {
            return null;
        }
        return new LazyBitmapDrawableResource(resources, resource);
    }

    public int getSize() {
        return this.b.getSize();
    }

    public void initialize() {
        Resource<Bitmap> resource = this.b;
        if (resource instanceof Initializable) {
            ((Initializable) resource).initialize();
        }
    }

    public void recycle() {
        this.b.recycle();
    }

    public Class<BitmapDrawable> a() {
        return BitmapDrawable.class;
    }

    public BitmapDrawable get() {
        return new BitmapDrawable(this.f2300a, this.b.get());
    }
}
