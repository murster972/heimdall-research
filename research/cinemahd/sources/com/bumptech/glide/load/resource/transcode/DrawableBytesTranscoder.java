package com.bumptech.glide.load.resource.transcode;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.load.resource.gif.GifDrawable;

public final class DrawableBytesTranscoder implements ResourceTranscoder<Drawable, byte[]> {

    /* renamed from: a  reason: collision with root package name */
    private final BitmapPool f2330a;
    private final ResourceTranscoder<Bitmap, byte[]> b;
    private final ResourceTranscoder<GifDrawable, byte[]> c;

    public DrawableBytesTranscoder(BitmapPool bitmapPool, ResourceTranscoder<Bitmap, byte[]> resourceTranscoder, ResourceTranscoder<GifDrawable, byte[]> resourceTranscoder2) {
        this.f2330a = bitmapPool;
        this.b = resourceTranscoder;
        this.c = resourceTranscoder2;
    }

    private static Resource<GifDrawable> a(Resource<Drawable> resource) {
        return resource;
    }

    public Resource<byte[]> a(Resource<Drawable> resource, Options options) {
        Drawable drawable = resource.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(BitmapResource.a(((BitmapDrawable) drawable).getBitmap(), this.f2330a), options);
        }
        if (!(drawable instanceof GifDrawable)) {
            return null;
        }
        ResourceTranscoder<GifDrawable, byte[]> resourceTranscoder = this.c;
        a(resource);
        return resourceTranscoder.a(resource, options);
    }
}
