package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public final class RoundedCorners extends BitmapTransformation {
    private static final byte[] c = "com.bumptech.glide.load.resource.bitmap.RoundedCorners".getBytes(Key.f2141a);
    private final int b;

    public RoundedCorners(int i) {
        Preconditions.a(i > 0, "roundingRadius must be greater than 0.");
        this.b = i;
    }

    /* access modifiers changed from: protected */
    public Bitmap a(BitmapPool bitmapPool, Bitmap bitmap, int i, int i2) {
        return TransformationUtils.b(bitmapPool, bitmap, this.b);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof RoundedCorners) || this.b != ((RoundedCorners) obj).b) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Util.a("com.bumptech.glide.load.resource.bitmap.RoundedCorners".hashCode(), Util.b(this.b));
    }

    public void a(MessageDigest messageDigest) {
        messageDigest.update(c);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }
}
