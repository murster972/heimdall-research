package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.util.ExceptionPassthroughInputStream;
import com.bumptech.glide.util.MarkEnforcingInputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamBitmapDecoder implements ResourceDecoder<InputStream, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final Downsampler f2304a;
    private final ArrayPool b;

    static class UntrustedCallbacks implements Downsampler.DecodeCallbacks {

        /* renamed from: a  reason: collision with root package name */
        private final RecyclableBufferedInputStream f2305a;
        private final ExceptionPassthroughInputStream b;

        UntrustedCallbacks(RecyclableBufferedInputStream recyclableBufferedInputStream, ExceptionPassthroughInputStream exceptionPassthroughInputStream) {
            this.f2305a = recyclableBufferedInputStream;
            this.b = exceptionPassthroughInputStream;
        }

        public void a() {
            this.f2305a.s();
        }

        public void a(BitmapPool bitmapPool, Bitmap bitmap) throws IOException {
            IOException s = this.b.s();
            if (s != null) {
                if (bitmap != null) {
                    bitmapPool.a(bitmap);
                }
                throw s;
            }
        }
    }

    public StreamBitmapDecoder(Downsampler downsampler, ArrayPool arrayPool) {
        this.f2304a = downsampler;
        this.b = arrayPool;
    }

    public boolean a(InputStream inputStream, Options options) {
        return this.f2304a.a(inputStream);
    }

    public Resource<Bitmap> a(InputStream inputStream, int i, int i2, Options options) throws IOException {
        RecyclableBufferedInputStream recyclableBufferedInputStream;
        boolean z;
        if (inputStream instanceof RecyclableBufferedInputStream) {
            recyclableBufferedInputStream = (RecyclableBufferedInputStream) inputStream;
            z = false;
        } else {
            recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, this.b);
            z = true;
        }
        ExceptionPassthroughInputStream b2 = ExceptionPassthroughInputStream.b(recyclableBufferedInputStream);
        try {
            return this.f2304a.a((InputStream) new MarkEnforcingInputStream(b2), i, i2, options, (Downsampler.DecodeCallbacks) new UntrustedCallbacks(recyclableBufferedInputStream, b2));
        } finally {
            b2.t();
            if (z) {
                recyclableBufferedInputStream.t();
            }
        }
    }
}
