package com.bumptech.glide.load.resource.bytes;

import com.bumptech.glide.load.data.DataRewinder;
import java.nio.ByteBuffer;

public class ByteBufferRewinder implements DataRewinder<ByteBuffer> {

    /* renamed from: a  reason: collision with root package name */
    private final ByteBuffer f2313a;

    public static class Factory implements DataRewinder.Factory<ByteBuffer> {
        public DataRewinder<ByteBuffer> a(ByteBuffer byteBuffer) {
            return new ByteBufferRewinder(byteBuffer);
        }

        public Class<ByteBuffer> a() {
            return ByteBuffer.class;
        }
    }

    public ByteBufferRewinder(ByteBuffer byteBuffer) {
        this.f2313a = byteBuffer;
    }

    public void b() {
    }

    public ByteBuffer a() {
        this.f2313a.position(0);
        return this.f2313a;
    }
}
