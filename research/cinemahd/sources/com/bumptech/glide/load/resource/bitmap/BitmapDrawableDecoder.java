package com.bumptech.glide.load.resource.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Preconditions;
import java.io.IOException;

public class BitmapDrawableDecoder<DataType> implements ResourceDecoder<DataType, BitmapDrawable> {

    /* renamed from: a  reason: collision with root package name */
    private final ResourceDecoder<DataType, Bitmap> f2282a;
    private final Resources b;

    public BitmapDrawableDecoder(Resources resources, ResourceDecoder<DataType, Bitmap> resourceDecoder) {
        Preconditions.a(resources);
        this.b = resources;
        Preconditions.a(resourceDecoder);
        this.f2282a = resourceDecoder;
    }

    public boolean a(DataType datatype, Options options) throws IOException {
        return this.f2282a.a(datatype, options);
    }

    public Resource<BitmapDrawable> a(DataType datatype, int i, int i2, Options options) throws IOException {
        return LazyBitmapDrawableResource.a(this.b, this.f2282a.a(datatype, i, i2, options));
    }
}
