package com.bumptech.glide.load.resource;

import android.annotation.SuppressLint;
import android.graphics.ColorSpace;
import android.graphics.ImageDecoder;
import android.os.Build;
import android.util.Log;
import android.util.Size;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.PreferredColorSpace;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.resource.bitmap.HardwareConfigState;
import java.io.IOException;

public abstract class ImageDecoderResourceDecoder<T> implements ResourceDecoder<ImageDecoder.Source, T> {

    /* renamed from: a  reason: collision with root package name */
    final HardwareConfigState f2279a = HardwareConfigState.b();

    /* access modifiers changed from: protected */
    public abstract Resource<T> a(ImageDecoder.Source source, int i, int i2, ImageDecoder.OnHeaderDecodedListener onHeaderDecodedListener) throws IOException;

    public final boolean a(ImageDecoder.Source source, Options options) {
        return true;
    }

    public final Resource<T> a(ImageDecoder.Source source, int i, int i2, Options options) throws IOException {
        final DecodeFormat decodeFormat = (DecodeFormat) options.a(Downsampler.f);
        final DownsampleStrategy downsampleStrategy = (DownsampleStrategy) options.a(DownsampleStrategy.f);
        final boolean z = options.a(Downsampler.i) != null && ((Boolean) options.a(Downsampler.i)).booleanValue();
        final PreferredColorSpace preferredColorSpace = (PreferredColorSpace) options.a(Downsampler.g);
        final int i3 = i;
        final int i4 = i2;
        return a(source, i, i2, (ImageDecoder.OnHeaderDecodedListener) new ImageDecoder.OnHeaderDecodedListener() {
            @SuppressLint({"Override"})
            public void onHeaderDecoded(ImageDecoder imageDecoder, ImageDecoder.ImageInfo imageInfo, ImageDecoder.Source source) {
                boolean z = true;
                if (ImageDecoderResourceDecoder.this.f2279a.a(i3, i4, z, false)) {
                    imageDecoder.setAllocator(3);
                } else {
                    imageDecoder.setAllocator(1);
                }
                if (decodeFormat == DecodeFormat.PREFER_RGB_565) {
                    imageDecoder.setMemorySizePolicy(0);
                }
                imageDecoder.setOnPartialImageListener(new ImageDecoder.OnPartialImageListener(this) {
                    public boolean onPartialImage(ImageDecoder.DecodeException decodeException) {
                        return false;
                    }
                });
                Size size = imageInfo.getSize();
                int i = i3;
                if (i == Integer.MIN_VALUE) {
                    i = size.getWidth();
                }
                int i2 = i4;
                if (i2 == Integer.MIN_VALUE) {
                    i2 = size.getHeight();
                }
                float b2 = downsampleStrategy.b(size.getWidth(), size.getHeight(), i, i2);
                int round = Math.round(((float) size.getWidth()) * b2);
                int round2 = Math.round(((float) size.getHeight()) * b2);
                if (Log.isLoggable("ImageDecoder", 2)) {
                    Log.v("ImageDecoder", "Resizing from [" + size.getWidth() + "x" + size.getHeight() + "] to [" + round + "x" + round2 + "] scaleFactor: " + b2);
                }
                imageDecoder.setTargetSize(round, round2);
                int i3 = Build.VERSION.SDK_INT;
                if (i3 >= 28) {
                    if (preferredColorSpace != PreferredColorSpace.DISPLAY_P3 || imageInfo.getColorSpace() == null || !imageInfo.getColorSpace().isWideGamut()) {
                        z = false;
                    }
                    imageDecoder.setTargetColorSpace(ColorSpace.get(z ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB));
                } else if (i3 >= 26) {
                    imageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace.Named.SRGB));
                }
            }
        });
    }
}
