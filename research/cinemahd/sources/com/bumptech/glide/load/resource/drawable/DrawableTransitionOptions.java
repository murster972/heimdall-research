package com.bumptech.glide.load.resource.drawable;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;

public final class DrawableTransitionOptions extends TransitionOptions<DrawableTransitionOptions, Drawable> {
    public DrawableTransitionOptions a(DrawableCrossFadeFactory drawableCrossFadeFactory) {
        return (DrawableTransitionOptions) a(drawableCrossFadeFactory);
    }

    public DrawableTransitionOptions b() {
        return a(new DrawableCrossFadeFactory.Builder());
    }

    public DrawableTransitionOptions a(DrawableCrossFadeFactory.Builder builder) {
        return a(builder.a());
    }
}
