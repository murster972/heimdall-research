package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.bumptech.glide.util.Preconditions;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

class GifFrameLoader {

    /* renamed from: a  reason: collision with root package name */
    private final GifDecoder f2323a;
    private final Handler b;
    private final List<FrameCallback> c;
    final RequestManager d;
    private final BitmapPool e;
    private boolean f;
    private boolean g;
    private boolean h;
    private RequestBuilder<Bitmap> i;
    private DelayTarget j;
    private boolean k;
    private DelayTarget l;
    private Bitmap m;
    private DelayTarget n;
    private OnEveryFrameListener o;
    private int p;
    private int q;
    private int r;

    static class DelayTarget extends CustomTarget<Bitmap> {
        private final Handler d;
        final int e;
        private final long f;
        private Bitmap g;

        DelayTarget(Handler handler, int i, long j) {
            this.d = handler;
            this.e = i;
            this.f = j;
        }

        /* access modifiers changed from: package-private */
        public Bitmap a() {
            return this.g;
        }

        public void onLoadCleared(Drawable drawable) {
            this.g = null;
        }

        /* renamed from: a */
        public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
            this.g = bitmap;
            this.d.sendMessageAtTime(this.d.obtainMessage(1, this), this.f);
        }
    }

    public interface FrameCallback {
        void a();
    }

    private class FrameLoaderCallback implements Handler.Callback {
        FrameLoaderCallback() {
        }

        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                GifFrameLoader.this.a((DelayTarget) message.obj);
                return true;
            } else if (i != 2) {
                return false;
            } else {
                GifFrameLoader.this.d.a((Target<?>) (DelayTarget) message.obj);
                return false;
            }
        }
    }

    interface OnEveryFrameListener {
        void a();
    }

    GifFrameLoader(Glide glide, GifDecoder gifDecoder, int i2, int i3, Transformation<Bitmap> transformation, Bitmap bitmap) {
        this(glide.c(), Glide.d(glide.e()), gifDecoder, (Handler) null, a(Glide.d(glide.e()), i2, i3), transformation, bitmap);
    }

    private static Key j() {
        return new ObjectKey(Double.valueOf(Math.random()));
    }

    private void k() {
        if (this.f && !this.g) {
            if (this.h) {
                Preconditions.a(this.n == null, "Pending target must be null when starting from the first frame");
                this.f2323a.f();
                this.h = false;
            }
            DelayTarget delayTarget = this.n;
            if (delayTarget != null) {
                this.n = null;
                a(delayTarget);
                return;
            }
            this.g = true;
            long uptimeMillis = SystemClock.uptimeMillis() + ((long) this.f2323a.e());
            this.f2323a.b();
            this.l = new DelayTarget(this.b, this.f2323a.g(), uptimeMillis);
            this.i.a((BaseRequestOptions<?>) RequestOptions.b(j())).a((Object) this.f2323a).a(this.l);
        }
    }

    private void l() {
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.e.a(bitmap);
            this.m = null;
        }
    }

    private void m() {
        if (!this.f) {
            this.f = true;
            this.k = false;
            k();
        }
    }

    private void n() {
        this.f = false;
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.bumptech.glide.load.Transformation, java.lang.Object, com.bumptech.glide.load.Transformation<android.graphics.Bitmap>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.bumptech.glide.load.Transformation<android.graphics.Bitmap> r3, android.graphics.Bitmap r4) {
        /*
            r2 = this;
            com.bumptech.glide.util.Preconditions.a(r3)
            r0 = r3
            com.bumptech.glide.load.Transformation r0 = (com.bumptech.glide.load.Transformation) r0
            com.bumptech.glide.util.Preconditions.a(r4)
            r0 = r4
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            r2.m = r0
            com.bumptech.glide.RequestBuilder<android.graphics.Bitmap> r0 = r2.i
            com.bumptech.glide.request.RequestOptions r1 = new com.bumptech.glide.request.RequestOptions
            r1.<init>()
            com.bumptech.glide.request.BaseRequestOptions r3 = r1.a((com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r3)
            com.bumptech.glide.RequestBuilder r3 = r0.a((com.bumptech.glide.request.BaseRequestOptions<?>) r3)
            r2.i = r3
            int r3 = com.bumptech.glide.util.Util.a((android.graphics.Bitmap) r4)
            r2.p = r3
            int r3 = r4.getWidth()
            r2.q = r3
            int r3 = r4.getHeight()
            r2.r = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.resource.gif.GifFrameLoader.a(com.bumptech.glide.load.Transformation, android.graphics.Bitmap):void");
    }

    /* access modifiers changed from: package-private */
    public void b(FrameCallback frameCallback) {
        this.c.remove(frameCallback);
        if (this.c.isEmpty()) {
            n();
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap c() {
        DelayTarget delayTarget = this.j;
        return delayTarget != null ? delayTarget.a() : this.m;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        DelayTarget delayTarget = this.j;
        if (delayTarget != null) {
            return delayTarget.e;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public Bitmap e() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.f2323a.c();
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public int h() {
        return this.f2323a.h() + this.p;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public ByteBuffer b() {
        return this.f2323a.d().asReadOnlyBuffer();
    }

    GifFrameLoader(BitmapPool bitmapPool, RequestManager requestManager, GifDecoder gifDecoder, Handler handler, RequestBuilder<Bitmap> requestBuilder, Transformation<Bitmap> transformation, Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = requestManager;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new FrameLoaderCallback()) : handler;
        this.e = bitmapPool;
        this.b = handler;
        this.i = requestBuilder;
        this.f2323a = gifDecoder;
        a(transformation, bitmap);
    }

    /* access modifiers changed from: package-private */
    public void a(FrameCallback frameCallback) {
        if (this.k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.c.contains(frameCallback)) {
            boolean isEmpty = this.c.isEmpty();
            this.c.add(frameCallback);
            if (isEmpty) {
                m();
            }
        } else {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c.clear();
        l();
        n();
        DelayTarget delayTarget = this.j;
        if (delayTarget != null) {
            this.d.a((Target<?>) delayTarget);
            this.j = null;
        }
        DelayTarget delayTarget2 = this.l;
        if (delayTarget2 != null) {
            this.d.a((Target<?>) delayTarget2);
            this.l = null;
        }
        DelayTarget delayTarget3 = this.n;
        if (delayTarget3 != null) {
            this.d.a((Target<?>) delayTarget3);
            this.n = null;
        }
        this.f2323a.clear();
        this.k = true;
    }

    /* access modifiers changed from: package-private */
    public void a(DelayTarget delayTarget) {
        OnEveryFrameListener onEveryFrameListener = this.o;
        if (onEveryFrameListener != null) {
            onEveryFrameListener.a();
        }
        this.g = false;
        if (this.k) {
            this.b.obtainMessage(2, delayTarget).sendToTarget();
        } else if (this.f) {
            if (delayTarget.a() != null) {
                l();
                DelayTarget delayTarget2 = this.j;
                this.j = delayTarget;
                for (int size = this.c.size() - 1; size >= 0; size--) {
                    this.c.get(size).a();
                }
                if (delayTarget2 != null) {
                    this.b.obtainMessage(2, delayTarget2).sendToTarget();
                }
            }
            k();
        } else if (this.h) {
            this.b.obtainMessage(2, delayTarget).sendToTarget();
        } else {
            this.n = delayTarget;
        }
    }

    private static RequestBuilder<Bitmap> a(RequestManager requestManager, int i2, int i3) {
        return requestManager.a().a((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) RequestOptions.b(DiskCacheStrategy.f2174a).b(true)).a(true)).a(i2, i3));
    }
}
