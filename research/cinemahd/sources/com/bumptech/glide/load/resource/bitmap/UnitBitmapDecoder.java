package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Util;

public final class UnitBitmapDecoder implements ResourceDecoder<Bitmap, Bitmap> {

    private static final class NonOwnedBitmapResource implements Resource<Bitmap> {

        /* renamed from: a  reason: collision with root package name */
        private final Bitmap f2308a;

        NonOwnedBitmapResource(Bitmap bitmap) {
            this.f2308a = bitmap;
        }

        public Class<Bitmap> a() {
            return Bitmap.class;
        }

        public int getSize() {
            return Util.a(this.f2308a);
        }

        public void recycle() {
        }

        public Bitmap get() {
            return this.f2308a;
        }
    }

    public boolean a(Bitmap bitmap, Options options) {
        return true;
    }

    public Resource<Bitmap> a(Bitmap bitmap, int i, int i2, Options options) {
        return new NonOwnedBitmapResource(bitmap);
    }
}
