package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.util.Preconditions;
import java.security.MessageDigest;

public class GifDrawableTransformation implements Transformation<GifDrawable> {
    private final Transformation<Bitmap> b;

    public GifDrawableTransformation(Transformation<Bitmap> transformation) {
        Preconditions.a(transformation);
        this.b = transformation;
    }

    public Resource<GifDrawable> a(Context context, Resource<GifDrawable> resource, int i, int i2) {
        GifDrawable gifDrawable = resource.get();
        BitmapResource bitmapResource = new BitmapResource(gifDrawable.c(), Glide.a(context).c());
        Resource<Bitmap> a2 = this.b.a(context, bitmapResource, i, i2);
        if (!bitmapResource.equals(a2)) {
            bitmapResource.recycle();
        }
        gifDrawable.a(this.b, a2.get());
        return resource;
    }

    public boolean equals(Object obj) {
        if (obj instanceof GifDrawableTransformation) {
            return this.b.equals(((GifDrawableTransformation) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
