package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;

public class BitmapResource implements Resource<Bitmap>, Initializable {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f2285a;
    private final BitmapPool b;

    public BitmapResource(Bitmap bitmap, BitmapPool bitmapPool) {
        Preconditions.a(bitmap, "Bitmap must not be null");
        this.f2285a = bitmap;
        Preconditions.a(bitmapPool, "BitmapPool must not be null");
        this.b = bitmapPool;
    }

    public static BitmapResource a(Bitmap bitmap, BitmapPool bitmapPool) {
        if (bitmap == null) {
            return null;
        }
        return new BitmapResource(bitmap, bitmapPool);
    }

    public int getSize() {
        return Util.a(this.f2285a);
    }

    public void initialize() {
        this.f2285a.prepareToDraw();
    }

    public void recycle() {
        this.b.a(this.f2285a);
    }

    public Class<Bitmap> a() {
        return Bitmap.class;
    }

    public Bitmap get() {
        return this.f2285a;
    }
}
