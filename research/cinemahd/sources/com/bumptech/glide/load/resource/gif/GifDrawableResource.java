package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.resource.drawable.DrawableResource;

public class GifDrawableResource extends DrawableResource<GifDrawable> implements Initializable {
    public GifDrawableResource(GifDrawable gifDrawable) {
        super(gifDrawable);
    }

    public Class<GifDrawable> a() {
        return GifDrawable.class;
    }

    public int getSize() {
        return ((GifDrawable) this.f2316a).f();
    }

    public void initialize() {
        ((GifDrawable) this.f2316a).c().prepareToDraw();
    }

    public void recycle() {
        ((GifDrawable) this.f2316a).stop();
        ((GifDrawable) this.f2316a).g();
    }
}
