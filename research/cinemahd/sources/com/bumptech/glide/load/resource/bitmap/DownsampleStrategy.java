package com.bumptech.glide.load.resource.bitmap;

import android.os.Build;
import com.bumptech.glide.load.Option;

public abstract class DownsampleStrategy {

    /* renamed from: a  reason: collision with root package name */
    public static final DownsampleStrategy f2292a = new FitCenter();
    public static final DownsampleStrategy b = new CenterInside();
    public static final DownsampleStrategy c = new CenterOutside();
    public static final DownsampleStrategy d = new None();
    public static final DownsampleStrategy e = c;
    public static final Option<DownsampleStrategy> f = Option.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", e);
    static final boolean g = (Build.VERSION.SDK_INT >= 19);

    private static class AtLeast extends DownsampleStrategy {
        AtLeast() {
        }

        public SampleSizeRounding a(int i, int i2, int i3, int i4) {
            return SampleSizeRounding.QUALITY;
        }

        public float b(int i, int i2, int i3, int i4) {
            int min = Math.min(i2 / i4, i / i3);
            if (min == 0) {
                return 1.0f;
            }
            return 1.0f / ((float) Integer.highestOneBit(min));
        }
    }

    private static class AtMost extends DownsampleStrategy {
        AtMost() {
        }

        public SampleSizeRounding a(int i, int i2, int i3, int i4) {
            return SampleSizeRounding.MEMORY;
        }

        public float b(int i, int i2, int i3, int i4) {
            int ceil = (int) Math.ceil((double) Math.max(((float) i2) / ((float) i4), ((float) i) / ((float) i3)));
            int i5 = 1;
            int max = Math.max(1, Integer.highestOneBit(ceil));
            if (max >= ceil) {
                i5 = 0;
            }
            return 1.0f / ((float) (max << i5));
        }
    }

    private static class CenterInside extends DownsampleStrategy {
        CenterInside() {
        }

        public SampleSizeRounding a(int i, int i2, int i3, int i4) {
            if (b(i, i2, i3, i4) == 1.0f) {
                return SampleSizeRounding.QUALITY;
            }
            return DownsampleStrategy.f2292a.a(i, i2, i3, i4);
        }

        public float b(int i, int i2, int i3, int i4) {
            return Math.min(1.0f, DownsampleStrategy.f2292a.b(i, i2, i3, i4));
        }
    }

    private static class CenterOutside extends DownsampleStrategy {
        CenterOutside() {
        }

        public SampleSizeRounding a(int i, int i2, int i3, int i4) {
            return SampleSizeRounding.QUALITY;
        }

        public float b(int i, int i2, int i3, int i4) {
            return Math.max(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }
    }

    private static class FitCenter extends DownsampleStrategy {
        FitCenter() {
        }

        public SampleSizeRounding a(int i, int i2, int i3, int i4) {
            if (DownsampleStrategy.g) {
                return SampleSizeRounding.QUALITY;
            }
            return SampleSizeRounding.MEMORY;
        }

        public float b(int i, int i2, int i3, int i4) {
            if (DownsampleStrategy.g) {
                return Math.min(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
            }
            int max = Math.max(i2 / i4, i / i3);
            if (max == 0) {
                return 1.0f;
            }
            return 1.0f / ((float) Integer.highestOneBit(max));
        }
    }

    private static class None extends DownsampleStrategy {
        None() {
        }

        public SampleSizeRounding a(int i, int i2, int i3, int i4) {
            return SampleSizeRounding.QUALITY;
        }

        public float b(int i, int i2, int i3, int i4) {
            return 1.0f;
        }
    }

    public enum SampleSizeRounding {
        MEMORY,
        QUALITY
    }

    static {
        new AtLeast();
        new AtMost();
    }

    public abstract SampleSizeRounding a(int i, int i2, int i3, int i4);

    public abstract float b(int i, int i2, int i3, int i4);
}
