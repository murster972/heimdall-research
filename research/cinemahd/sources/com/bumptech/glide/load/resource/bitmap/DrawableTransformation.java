package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import java.security.MessageDigest;

public class DrawableTransformation implements Transformation<Drawable> {
    private final Transformation<Bitmap> b;
    private final boolean c;

    public DrawableTransformation(Transformation<Bitmap> transformation, boolean z) {
        this.b = transformation;
        this.c = z;
    }

    public Transformation<BitmapDrawable> a() {
        return this;
    }

    public Resource<Drawable> a(Context context, Resource<Drawable> resource, int i, int i2) {
        BitmapPool c2 = Glide.a(context).c();
        Drawable drawable = resource.get();
        Resource<Bitmap> a2 = DrawableToBitmapConverter.a(c2, drawable, i, i2);
        if (a2 != null) {
            Resource<Bitmap> a3 = this.b.a(context, a2, i, i2);
            if (!a3.equals(a2)) {
                return a(context, a3);
            }
            a3.recycle();
            return resource;
        } else if (!this.c) {
            return resource;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof DrawableTransformation) {
            return this.b.equals(((DrawableTransformation) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    private Resource<Drawable> a(Context context, Resource<Bitmap> resource) {
        return LazyBitmapDrawableResource.a(context.getResources(), resource);
    }

    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
