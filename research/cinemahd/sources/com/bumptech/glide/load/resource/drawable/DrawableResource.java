package com.bumptech.glide.load.resource.drawable;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.util.Preconditions;

public abstract class DrawableResource<T extends Drawable> implements Resource<T>, Initializable {

    /* renamed from: a  reason: collision with root package name */
    protected final T f2316a;

    public DrawableResource(T t) {
        Preconditions.a(t);
        this.f2316a = (Drawable) t;
    }

    public void initialize() {
        T t = this.f2316a;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof GifDrawable) {
            ((GifDrawable) t).c().prepareToDraw();
        }
    }

    public final T get() {
        Drawable.ConstantState constantState = this.f2316a.getConstantState();
        if (constantState == null) {
            return this.f2316a;
        }
        return constantState.newDrawable();
    }
}
