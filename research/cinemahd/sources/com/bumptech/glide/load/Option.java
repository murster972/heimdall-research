package com.bumptech.glide.load;

import com.bumptech.glide.util.Preconditions;
import java.security.MessageDigest;

public final class Option<T> {
    private static final CacheKeyUpdater<Object> e = new CacheKeyUpdater<Object>() {
        public void a(byte[] bArr, Object obj, MessageDigest messageDigest) {
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final T f2142a;
    private final CacheKeyUpdater<T> b;
    private final String c;
    private volatile byte[] d;

    public interface CacheKeyUpdater<T> {
        void a(byte[] bArr, T t, MessageDigest messageDigest);
    }

    private Option(String str, T t, CacheKeyUpdater<T> cacheKeyUpdater) {
        Preconditions.a(str);
        this.c = str;
        this.f2142a = t;
        Preconditions.a(cacheKeyUpdater);
        this.b = cacheKeyUpdater;
    }

    public static <T> Option<T> a(String str) {
        return new Option<>(str, (Object) null, b());
    }

    private static <T> CacheKeyUpdater<T> b() {
        return e;
    }

    private byte[] c() {
        if (this.d == null) {
            this.d = this.c.getBytes(Key.f2141a);
        }
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Option) {
            return this.c.equals(((Option) obj).c);
        }
        return false;
    }

    public int hashCode() {
        return this.c.hashCode();
    }

    public String toString() {
        return "Option{key='" + this.c + '\'' + '}';
    }

    public static <T> Option<T> a(String str, T t) {
        return new Option<>(str, t, b());
    }

    public static <T> Option<T> a(String str, T t, CacheKeyUpdater<T> cacheKeyUpdater) {
        return new Option<>(str, t, cacheKeyUpdater);
    }

    public T a() {
        return this.f2142a;
    }

    public void a(T t, MessageDigest messageDigest) {
        this.b.a(c(), t, messageDigest);
    }
}
