package com.bumptech.glide.util;

import androidx.collection.ArrayMap;
import androidx.collection.SimpleArrayMap;

public final class CachedHashCodeArrayMap<K, V> extends ArrayMap<K, V> {
    private int i;

    public V a(int i2, V v) {
        this.i = 0;
        return super.a(i2, v);
    }

    public V c(int i2) {
        this.i = 0;
        return super.c(i2);
    }

    public void clear() {
        this.i = 0;
        super.clear();
    }

    public int hashCode() {
        if (this.i == 0) {
            this.i = super.hashCode();
        }
        return this.i;
    }

    public V put(K k, V v) {
        this.i = 0;
        return super.put(k, v);
    }

    public void a(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.i = 0;
        super.a(simpleArrayMap);
    }
}
