package com.bumptech.glide.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<T, Y> {

    /* renamed from: a  reason: collision with root package name */
    private final Map<T, Entry<Y>> f2375a = new LinkedHashMap(100, 0.75f, true);
    private long b;
    private long c;

    static final class Entry<Y> {

        /* renamed from: a  reason: collision with root package name */
        final Y f2376a;
        final int b;

        Entry(Y y, int i) {
            this.f2376a = y;
            this.b = i;
        }
    }

    public LruCache(long j) {
        this.b = j;
    }

    public synchronized Y a(T t) {
        Entry entry;
        entry = this.f2375a.get(t);
        return entry != null ? entry.f2376a : null;
    }

    /* access modifiers changed from: protected */
    public void a(T t, Y y) {
    }

    /* access modifiers changed from: protected */
    public int b(Y y) {
        return 1;
    }

    public synchronized long b() {
        return this.b;
    }

    public synchronized Y c(T t) {
        Entry remove = this.f2375a.remove(t);
        if (remove == null) {
            return null;
        }
        this.c -= (long) remove.b;
        return remove.f2376a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0049, code lost:
        return r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized Y b(T r8, Y r9) {
        /*
            r7 = this;
            monitor-enter(r7)
            int r0 = r7.b(r9)     // Catch:{ all -> 0x004a }
            long r1 = (long) r0     // Catch:{ all -> 0x004a }
            long r3 = r7.b     // Catch:{ all -> 0x004a }
            r5 = 0
            int r6 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r6 < 0) goto L_0x0012
            r7.a(r8, r9)     // Catch:{ all -> 0x004a }
            monitor-exit(r7)
            return r5
        L_0x0012:
            if (r9 == 0) goto L_0x0019
            long r3 = r7.c     // Catch:{ all -> 0x004a }
            long r3 = r3 + r1
            r7.c = r3     // Catch:{ all -> 0x004a }
        L_0x0019:
            java.util.Map<T, com.bumptech.glide.util.LruCache$Entry<Y>> r1 = r7.f2375a     // Catch:{ all -> 0x004a }
            if (r9 != 0) goto L_0x001f
            r2 = r5
            goto L_0x0024
        L_0x001f:
            com.bumptech.glide.util.LruCache$Entry r2 = new com.bumptech.glide.util.LruCache$Entry     // Catch:{ all -> 0x004a }
            r2.<init>(r9, r0)     // Catch:{ all -> 0x004a }
        L_0x0024:
            java.lang.Object r0 = r1.put(r8, r2)     // Catch:{ all -> 0x004a }
            com.bumptech.glide.util.LruCache$Entry r0 = (com.bumptech.glide.util.LruCache.Entry) r0     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x0041
            long r1 = r7.c     // Catch:{ all -> 0x004a }
            int r3 = r0.b     // Catch:{ all -> 0x004a }
            long r3 = (long) r3     // Catch:{ all -> 0x004a }
            long r1 = r1 - r3
            r7.c = r1     // Catch:{ all -> 0x004a }
            Y r1 = r0.f2376a     // Catch:{ all -> 0x004a }
            boolean r9 = r1.equals(r9)     // Catch:{ all -> 0x004a }
            if (r9 != 0) goto L_0x0041
            Y r9 = r0.f2376a     // Catch:{ all -> 0x004a }
            r7.a(r8, r9)     // Catch:{ all -> 0x004a }
        L_0x0041:
            r7.c()     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x0048
            Y r5 = r0.f2376a     // Catch:{ all -> 0x004a }
        L_0x0048:
            monitor-exit(r7)
            return r5
        L_0x004a:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.util.LruCache.b(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public void a() {
        a(0);
    }

    /* access modifiers changed from: protected */
    public synchronized void a(long j) {
        while (this.c > j) {
            Iterator<Map.Entry<T, Entry<Y>>> it2 = this.f2375a.entrySet().iterator();
            Map.Entry next = it2.next();
            Entry entry = (Entry) next.getValue();
            this.c -= (long) entry.b;
            Object key = next.getKey();
            it2.remove();
            a(key, entry.f2376a);
        }
    }

    private void c() {
        a(this.b);
    }
}
