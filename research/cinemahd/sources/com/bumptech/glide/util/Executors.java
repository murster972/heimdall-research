package com.bumptech.glide.util;

import java.util.concurrent.Executor;

public final class Executors {

    /* renamed from: a  reason: collision with root package name */
    private static final Executor f2373a = new Executor() {
        public void execute(Runnable runnable) {
            Util.a(runnable);
        }
    };
    private static final Executor b = new Executor() {
        public void execute(Runnable runnable) {
            runnable.run();
        }
    };

    private Executors() {
    }

    public static Executor a() {
        return b;
    }

    public static Executor b() {
        return f2373a;
    }
}
