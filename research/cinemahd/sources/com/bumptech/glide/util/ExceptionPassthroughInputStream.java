package com.bumptech.glide.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public final class ExceptionPassthroughInputStream extends InputStream {
    private static final Queue<ExceptionPassthroughInputStream> c = Util.a(0);

    /* renamed from: a  reason: collision with root package name */
    private InputStream f2372a;
    private IOException b;

    ExceptionPassthroughInputStream() {
    }

    public static ExceptionPassthroughInputStream b(InputStream inputStream) {
        ExceptionPassthroughInputStream poll;
        synchronized (c) {
            poll = c.poll();
        }
        if (poll == null) {
            poll = new ExceptionPassthroughInputStream();
        }
        poll.a(inputStream);
        return poll;
    }

    /* access modifiers changed from: package-private */
    public void a(InputStream inputStream) {
        this.f2372a = inputStream;
    }

    public int available() throws IOException {
        return this.f2372a.available();
    }

    public void close() throws IOException {
        this.f2372a.close();
    }

    public void mark(int i) {
        this.f2372a.mark(i);
    }

    public boolean markSupported() {
        return this.f2372a.markSupported();
    }

    public int read() throws IOException {
        try {
            return this.f2372a.read();
        } catch (IOException e) {
            this.b = e;
            throw e;
        }
    }

    public synchronized void reset() throws IOException {
        this.f2372a.reset();
    }

    public IOException s() {
        return this.b;
    }

    public long skip(long j) throws IOException {
        try {
            return this.f2372a.skip(j);
        } catch (IOException e) {
            this.b = e;
            throw e;
        }
    }

    public void t() {
        this.b = null;
        this.f2372a = null;
        synchronized (c) {
            c.offer(this);
        }
    }

    public int read(byte[] bArr) throws IOException {
        try {
            return this.f2372a.read(bArr);
        } catch (IOException e) {
            this.b = e;
            throw e;
        }
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        try {
            return this.f2372a.read(bArr, i, i2);
        } catch (IOException e) {
            this.b = e;
            throw e;
        }
    }
}
