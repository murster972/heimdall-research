package com.bumptech.glide.util.pool;

public abstract class StateVerifier {
    public static StateVerifier b() {
        return new DefaultStateVerifier();
    }

    public abstract void a();

    /* access modifiers changed from: package-private */
    public abstract void a(boolean z);

    private static class DefaultStateVerifier extends StateVerifier {

        /* renamed from: a  reason: collision with root package name */
        private volatile boolean f2383a;

        DefaultStateVerifier() {
            super();
        }

        public void a() {
            if (this.f2383a) {
                throw new IllegalStateException("Already released");
            }
        }

        public void a(boolean z) {
            this.f2383a = z;
        }
    }

    private StateVerifier() {
    }
}
