package com.bumptech.glide.util.pool;

import android.util.Log;
import androidx.core.util.Pools$Pool;
import androidx.core.util.Pools$SynchronizedPool;
import java.util.ArrayList;
import java.util.List;

public final class FactoryPools {

    /* renamed from: a  reason: collision with root package name */
    private static final Resetter<Object> f2381a = new Resetter<Object>() {
        public void a(Object obj) {
        }
    };

    public interface Factory<T> {
        T create();
    }

    private static final class FactoryPool<T> implements Pools$Pool<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Factory<T> f2382a;
        private final Resetter<T> b;
        private final Pools$Pool<T> c;

        FactoryPool(Pools$Pool<T> pools$Pool, Factory<T> factory, Resetter<T> resetter) {
            this.c = pools$Pool;
            this.f2382a = factory;
            this.b = resetter;
        }

        public T acquire() {
            T acquire = this.c.acquire();
            if (acquire == null) {
                acquire = this.f2382a.create();
                if (Log.isLoggable("FactoryPools", 2)) {
                    Log.v("FactoryPools", "Created new " + acquire.getClass());
                }
            }
            if (acquire instanceof Poolable) {
                ((Poolable) acquire).b().a(false);
            }
            return acquire;
        }

        public boolean release(T t) {
            if (t instanceof Poolable) {
                ((Poolable) t).b().a(true);
            }
            this.b.a(t);
            return this.c.release(t);
        }
    }

    public interface Poolable {
        StateVerifier b();
    }

    public interface Resetter<T> {
        void a(T t);
    }

    private FactoryPools() {
    }

    public static <T extends Poolable> Pools$Pool<T> a(int i, Factory<T> factory) {
        return a(new Pools$SynchronizedPool(i), factory);
    }

    public static <T> Pools$Pool<List<T>> b() {
        return a(20);
    }

    public static <T> Pools$Pool<List<T>> a(int i) {
        return a(new Pools$SynchronizedPool(i), new Factory<List<T>>() {
            public List<T> create() {
                return new ArrayList();
            }
        }, new Resetter<List<T>>() {
            public void a(List<T> list) {
                list.clear();
            }
        });
    }

    private static <T extends Poolable> Pools$Pool<T> a(Pools$Pool<T> pools$Pool, Factory<T> factory) {
        return a(pools$Pool, factory, a());
    }

    private static <T> Pools$Pool<T> a(Pools$Pool<T> pools$Pool, Factory<T> factory, Resetter<T> resetter) {
        return new FactoryPool(pools$Pool, factory, resetter);
    }

    private static <T> Resetter<T> a() {
        return f2381a;
    }
}
