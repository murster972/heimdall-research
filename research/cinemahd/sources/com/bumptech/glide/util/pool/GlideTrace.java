package com.bumptech.glide.util.pool;

public final class GlideTrace {
    private GlideTrace() {
    }

    public static void a() {
    }

    public static void a(String str) {
    }

    public static void a(String str, Object obj) {
    }

    public static void a(String str, Object obj, Object obj2, Object obj3) {
    }
}
