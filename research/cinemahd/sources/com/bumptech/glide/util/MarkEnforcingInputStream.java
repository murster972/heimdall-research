package com.bumptech.glide.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MarkEnforcingInputStream extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private int f2377a = Integer.MIN_VALUE;

    public MarkEnforcingInputStream(InputStream inputStream) {
        super(inputStream);
    }

    private long h(long j) {
        int i = this.f2377a;
        if (i == 0) {
            return -1;
        }
        return (i == Integer.MIN_VALUE || j <= ((long) i)) ? j : (long) i;
    }

    private void i(long j) {
        int i = this.f2377a;
        if (i != Integer.MIN_VALUE && j != -1) {
            this.f2377a = (int) (((long) i) - j);
        }
    }

    public int available() throws IOException {
        int i = this.f2377a;
        if (i == Integer.MIN_VALUE) {
            return super.available();
        }
        return Math.min(i, super.available());
    }

    public synchronized void mark(int i) {
        super.mark(i);
        this.f2377a = i;
    }

    public int read() throws IOException {
        if (h(1) == -1) {
            return -1;
        }
        int read = super.read();
        i(1);
        return read;
    }

    public synchronized void reset() throws IOException {
        super.reset();
        this.f2377a = Integer.MIN_VALUE;
    }

    public long skip(long j) throws IOException {
        long h = h(j);
        if (h == -1) {
            return 0;
        }
        long skip = super.skip(h);
        i(skip);
        return skip;
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        int h = (int) h((long) i2);
        if (h == -1) {
            return -1;
        }
        int read = super.read(bArr, i, h);
        i((long) read);
        return read;
    }
}
