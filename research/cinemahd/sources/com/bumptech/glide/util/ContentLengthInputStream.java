package com.bumptech.glide.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ContentLengthInputStream extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private final long f2371a;
    private int b;

    private ContentLengthInputStream(InputStream inputStream, long j) {
        super(inputStream);
        this.f2371a = j;
    }

    public static InputStream a(InputStream inputStream, long j) {
        return new ContentLengthInputStream(inputStream, j);
    }

    private int b(int i) throws IOException {
        if (i >= 0) {
            this.b += i;
        } else if (this.f2371a - ((long) this.b) > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.f2371a + ", but read: " + this.b);
        }
        return i;
    }

    public synchronized int available() throws IOException {
        return (int) Math.max(this.f2371a - ((long) this.b), (long) this.in.available());
    }

    public synchronized int read() throws IOException {
        int read;
        read = super.read();
        b(read >= 0 ? 1 : -1);
        return read;
    }

    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int read;
        read = super.read(bArr, i, i2);
        b(read);
        return read;
    }
}
