package com.bumptech.glide;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.manager.ConnectivityMonitor;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.manager.LifecycleListener;
import com.bumptech.glide.manager.RequestManagerTreeNode;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.manager.TargetTracker;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.util.Util;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class RequestManager implements ComponentCallbacks2, LifecycleListener, ModelTypes<RequestBuilder<Drawable>> {
    private static final RequestOptions l = ((RequestOptions) RequestOptions.b((Class<?>) Bitmap.class).D());

    /* renamed from: a  reason: collision with root package name */
    protected final Glide f2114a;
    protected final Context b;
    final Lifecycle c;
    private final RequestTracker d;
    private final RequestManagerTreeNode e;
    private final TargetTracker f;
    private final Runnable g;
    private final ConnectivityMonitor h;
    private final CopyOnWriteArrayList<RequestListener<Object>> i;
    private RequestOptions j;
    private boolean k;

    private class RequestManagerConnectivityListener implements ConnectivityMonitor.ConnectivityListener {

        /* renamed from: a  reason: collision with root package name */
        private final RequestTracker f2116a;

        RequestManagerConnectivityListener(RequestTracker requestTracker) {
            this.f2116a = requestTracker;
        }

        public void a(boolean z) {
            if (z) {
                synchronized (RequestManager.this) {
                    this.f2116a.d();
                }
            }
        }
    }

    static {
        RequestOptions requestOptions = (RequestOptions) RequestOptions.b((Class<?>) GifDrawable.class).D();
        RequestOptions requestOptions2 = (RequestOptions) ((RequestOptions) RequestOptions.b(DiskCacheStrategy.b).a(Priority.LOW)).a(true);
    }

    public RequestManager(Glide glide, Lifecycle lifecycle, RequestManagerTreeNode requestManagerTreeNode, Context context) {
        this(glide, lifecycle, requestManagerTreeNode, new RequestTracker(), glide.d(), context);
    }

    private void c(Target<?> target) {
        boolean b2 = b(target);
        Request request = target.getRequest();
        if (!b2 && !this.f2114a.a(target) && request != null) {
            target.setRequest((Request) null);
            request.clear();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void a(RequestOptions requestOptions) {
        this.j = (RequestOptions) ((RequestOptions) requestOptions.clone()).a();
    }

    public RequestBuilder<Drawable> b() {
        return a(Drawable.class);
    }

    /* access modifiers changed from: package-private */
    public synchronized RequestOptions d() {
        return this.j;
    }

    public synchronized void e() {
        this.d.b();
    }

    public synchronized void f() {
        e();
        for (RequestManager e2 : this.e.a()) {
            e2.e();
        }
    }

    public synchronized void g() {
        this.d.c();
    }

    public synchronized void h() {
        this.d.e();
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public synchronized void onDestroy() {
        this.f.onDestroy();
        for (Target<?> a2 : this.f.b()) {
            a(a2);
        }
        this.f.a();
        this.d.a();
        this.c.a(this);
        this.c.a(this.h);
        Util.b(this.g);
        this.f2114a.b(this);
    }

    public void onLowMemory() {
    }

    public synchronized void onStart() {
        h();
        this.f.onStart();
    }

    public synchronized void onStop() {
        g();
        this.f.onStop();
    }

    public void onTrimMemory(int i2) {
        if (i2 == 60 && this.k) {
            f();
        }
    }

    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.d + ", treeNode=" + this.e + "}";
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean b(Target<?> target) {
        Request request = target.getRequest();
        if (request == null) {
            return true;
        }
        if (!this.d.a(request)) {
            return false;
        }
        this.f.b(target);
        target.setRequest((Request) null);
        return true;
    }

    public RequestBuilder<Bitmap> a() {
        return a(Bitmap.class).a((BaseRequestOptions<?>) l);
    }

    RequestManager(Glide glide, Lifecycle lifecycle, RequestManagerTreeNode requestManagerTreeNode, RequestTracker requestTracker, ConnectivityMonitorFactory connectivityMonitorFactory, Context context) {
        this.f = new TargetTracker();
        this.g = new Runnable() {
            public void run() {
                RequestManager requestManager = RequestManager.this;
                requestManager.c.b(requestManager);
            }
        };
        this.f2114a = glide;
        this.c = lifecycle;
        this.e = requestManagerTreeNode;
        this.d = requestTracker;
        this.b = context;
        this.h = connectivityMonitorFactory.a(context.getApplicationContext(), new RequestManagerConnectivityListener(requestTracker));
        if (Util.c()) {
            Util.a(this.g);
        } else {
            lifecycle.b(this);
        }
        lifecycle.b(this.h);
        this.i = new CopyOnWriteArrayList<>(glide.f().b());
        a(glide.f().c());
        glide.a(this);
    }

    public RequestBuilder<Drawable> a(String str) {
        return b().a(str);
    }

    public <ResourceType> RequestBuilder<ResourceType> a(Class<ResourceType> cls) {
        return new RequestBuilder<>(this.f2114a, this, cls, this.b);
    }

    public void a(Target<?> target) {
        if (target != null) {
            c(target);
        }
    }

    /* access modifiers changed from: package-private */
    public List<RequestListener<Object>> c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Target<?> target, Request request) {
        this.f.a(target);
        this.d.b(request);
    }

    /* access modifiers changed from: package-private */
    public <T> TransitionOptions<?, T> b(Class<T> cls) {
        return this.f2114a.f().a(cls);
    }
}
