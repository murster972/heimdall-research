package com.bumptech.glide.integration.okhttp3;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.HttpException;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.util.ContentLengthInputStream;
import com.bumptech.glide.util.Preconditions;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OkHttpStreamFetcher implements DataFetcher<InputStream>, Callback {

    /* renamed from: a  reason: collision with root package name */
    private final Call.Factory f2130a;
    private final GlideUrl b;
    private InputStream c;
    private ResponseBody d;
    private DataFetcher.DataCallback<? super InputStream> e;
    private volatile Call f;

    public OkHttpStreamFetcher(Call.Factory factory, GlideUrl glideUrl) {
        this.f2130a = factory;
        this.b = glideUrl;
    }

    public void a(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
        Request.Builder url = new Request.Builder().url(this.b.c());
        for (Map.Entry next : this.b.b().entrySet()) {
            url.addHeader((String) next.getKey(), (String) next.getValue());
        }
        Request build = url.build();
        this.e = dataCallback;
        this.f = this.f2130a.newCall(build);
        this.f.enqueue(this);
    }

    public void b() {
        try {
            if (this.c != null) {
                this.c.close();
            }
        } catch (IOException unused) {
        }
        ResponseBody responseBody = this.d;
        if (responseBody != null) {
            responseBody.close();
        }
        this.e = null;
    }

    public DataSource c() {
        return DataSource.REMOTE;
    }

    public void cancel() {
        Call call = this.f;
        if (call != null) {
            call.cancel();
        }
    }

    public void onFailure(Call call, IOException iOException) {
        if (Log.isLoggable("OkHttpFetcher", 3)) {
            Log.d("OkHttpFetcher", "OkHttp failed to obtain result", iOException);
        }
        this.e.a((Exception) iOException);
    }

    public void onResponse(Call call, Response response) {
        this.d = response.body();
        if (response.isSuccessful()) {
            ResponseBody responseBody = this.d;
            Preconditions.a(responseBody);
            this.c = ContentLengthInputStream.a(this.d.byteStream(), responseBody.contentLength());
            this.e.a(this.c);
            return;
        }
        this.e.a((Exception) new HttpException(response.message(), response.code()));
    }

    public Class<InputStream> a() {
        return InputStream.class;
    }
}
