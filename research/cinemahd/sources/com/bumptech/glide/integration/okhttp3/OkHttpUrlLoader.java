package com.bumptech.glide.integration.okhttp3;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import okhttp3.Call;
import okhttp3.OkHttpClient;

public class OkHttpUrlLoader implements ModelLoader<GlideUrl, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final Call.Factory f2131a;

    public static class Factory implements ModelLoaderFactory<GlideUrl, InputStream> {
        private static volatile Call.Factory b;

        /* renamed from: a  reason: collision with root package name */
        private final Call.Factory f2132a;

        public Factory() {
            this(b());
        }

        private static Call.Factory b() {
            if (b == null) {
                synchronized (Factory.class) {
                    if (b == null) {
                        b = new OkHttpClient();
                    }
                }
            }
            return b;
        }

        public ModelLoader<GlideUrl, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new OkHttpUrlLoader(this.f2132a);
        }

        public void a() {
        }

        public Factory(Call.Factory factory) {
            this.f2132a = factory;
        }
    }

    public OkHttpUrlLoader(Call.Factory factory) {
        this.f2131a = factory;
    }

    public boolean a(GlideUrl glideUrl) {
        return true;
    }

    public ModelLoader.LoadData<InputStream> a(GlideUrl glideUrl, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(glideUrl, new OkHttpStreamFetcher(this.f2131a, glideUrl));
    }
}
