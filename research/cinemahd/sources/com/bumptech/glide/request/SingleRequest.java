package com.bumptech.glide.request;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.drawable.DrawableDecoderCompat;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.TransitionFactory;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.util.pool.StateVerifier;
import java.util.List;
import java.util.concurrent.Executor;

public final class SingleRequest<R> implements Request, SizeReadyCallback, ResourceCallback {
    private static final boolean D = Log.isLoggable("Request", 2);
    private int A;
    private boolean B;
    private RuntimeException C;

    /* renamed from: a  reason: collision with root package name */
    private final String f2357a;
    private final StateVerifier b;
    private final Object c;
    private final RequestListener<R> d;
    private final RequestCoordinator e;
    private final Context f;
    private final GlideContext g;
    private final Object h;
    private final Class<R> i;
    private final BaseRequestOptions<?> j;
    private final int k;
    private final int l;
    private final Priority m;
    private final Target<R> n;
    private final List<RequestListener<R>> o;
    private final TransitionFactory<? super R> p;
    private final Executor q;
    private Resource<R> r;
    private Engine.LoadStatus s;
    private long t;
    private volatile Engine u;
    private Status v;
    private Drawable w;
    private Drawable x;
    private Drawable y;
    private int z;

    private enum Status {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CLEARED
    }

    private SingleRequest(Context context, GlideContext glideContext, Object obj, Object obj2, Class<R> cls, BaseRequestOptions<?> baseRequestOptions, int i2, int i3, Priority priority, Target<R> target, RequestListener<R> requestListener, List<RequestListener<R>> list, RequestCoordinator requestCoordinator, Engine engine, TransitionFactory<? super R> transitionFactory, Executor executor) {
        this.f2357a = D ? String.valueOf(super.hashCode()) : null;
        this.b = StateVerifier.b();
        this.c = obj;
        this.f = context;
        this.g = glideContext;
        this.h = obj2;
        this.i = cls;
        this.j = baseRequestOptions;
        this.k = i2;
        this.l = i3;
        this.m = priority;
        this.n = target;
        this.d = requestListener;
        this.o = list;
        this.e = requestCoordinator;
        this.u = engine;
        this.p = transitionFactory;
        this.q = executor;
        this.v = Status.PENDING;
        if (this.C == null && glideContext.e().a(GlideBuilder.LogRequestOrigins.class)) {
            this.C = new RuntimeException("Glide request origin trace");
        }
    }

    public static <R> SingleRequest<R> a(Context context, GlideContext glideContext, Object obj, Object obj2, Class<R> cls, BaseRequestOptions<?> baseRequestOptions, int i2, int i3, Priority priority, Target<R> target, RequestListener<R> requestListener, List<RequestListener<R>> list, RequestCoordinator requestCoordinator, Engine engine, TransitionFactory<? super R> transitionFactory, Executor executor) {
        return new SingleRequest(context, glideContext, obj, obj2, cls, baseRequestOptions, i2, i3, priority, target, requestListener, list, requestCoordinator, engine, transitionFactory, executor);
    }

    private boolean b() {
        RequestCoordinator requestCoordinator = this.e;
        return requestCoordinator == null || requestCoordinator.canNotifyCleared(this);
    }

    private boolean c() {
        RequestCoordinator requestCoordinator = this.e;
        return requestCoordinator == null || requestCoordinator.canNotifyStatusChanged(this);
    }

    private boolean d() {
        RequestCoordinator requestCoordinator = this.e;
        return requestCoordinator == null || requestCoordinator.canSetImage(this);
    }

    private void e() {
        a();
        this.b.a();
        this.n.removeCallback(this);
        Engine.LoadStatus loadStatus = this.s;
        if (loadStatus != null) {
            loadStatus.a();
            this.s = null;
        }
    }

    private Drawable f() {
        if (this.w == null) {
            this.w = this.j.e();
            if (this.w == null && this.j.d() > 0) {
                this.w = a(this.j.d());
            }
        }
        return this.w;
    }

    private Drawable g() {
        if (this.y == null) {
            this.y = this.j.f();
            if (this.y == null && this.j.g() > 0) {
                this.y = a(this.j.g());
            }
        }
        return this.y;
    }

    private Drawable h() {
        if (this.x == null) {
            this.x = this.j.l();
            if (this.x == null && this.j.m() > 0) {
                this.x = a(this.j.m());
            }
        }
        return this.x;
    }

    private boolean i() {
        RequestCoordinator requestCoordinator = this.e;
        return requestCoordinator == null || !requestCoordinator.getRoot().isAnyResourceSet();
    }

    private void j() {
        RequestCoordinator requestCoordinator = this.e;
        if (requestCoordinator != null) {
            requestCoordinator.onRequestFailed(this);
        }
    }

    private void k() {
        RequestCoordinator requestCoordinator = this.e;
        if (requestCoordinator != null) {
            requestCoordinator.onRequestSuccess(this);
        }
    }

    private void l() {
        if (c()) {
            Drawable drawable = null;
            if (this.h == null) {
                drawable = g();
            }
            if (drawable == null) {
                drawable = f();
            }
            if (drawable == null) {
                drawable = h();
            }
            this.n.onLoadFailed(drawable);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a7, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void begin() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.c
            monitor-enter(r0)
            r4.a()     // Catch:{ all -> 0x00b0 }
            com.bumptech.glide.util.pool.StateVerifier r1 = r4.b     // Catch:{ all -> 0x00b0 }
            r1.a()     // Catch:{ all -> 0x00b0 }
            long r1 = com.bumptech.glide.util.LogTime.a()     // Catch:{ all -> 0x00b0 }
            r4.t = r1     // Catch:{ all -> 0x00b0 }
            java.lang.Object r1 = r4.h     // Catch:{ all -> 0x00b0 }
            if (r1 != 0) goto L_0x003c
            int r1 = r4.k     // Catch:{ all -> 0x00b0 }
            int r2 = r4.l     // Catch:{ all -> 0x00b0 }
            boolean r1 = com.bumptech.glide.util.Util.b((int) r1, (int) r2)     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x0027
            int r1 = r4.k     // Catch:{ all -> 0x00b0 }
            r4.z = r1     // Catch:{ all -> 0x00b0 }
            int r1 = r4.l     // Catch:{ all -> 0x00b0 }
            r4.A = r1     // Catch:{ all -> 0x00b0 }
        L_0x0027:
            android.graphics.drawable.Drawable r1 = r4.g()     // Catch:{ all -> 0x00b0 }
            if (r1 != 0) goto L_0x002f
            r1 = 5
            goto L_0x0030
        L_0x002f:
            r1 = 3
        L_0x0030:
            com.bumptech.glide.load.engine.GlideException r2 = new com.bumptech.glide.load.engine.GlideException     // Catch:{ all -> 0x00b0 }
            java.lang.String r3 = "Received null model"
            r2.<init>(r3)     // Catch:{ all -> 0x00b0 }
            r4.a((com.bumptech.glide.load.engine.GlideException) r2, (int) r1)     // Catch:{ all -> 0x00b0 }
            monitor-exit(r0)     // Catch:{ all -> 0x00b0 }
            return
        L_0x003c:
            com.bumptech.glide.request.SingleRequest$Status r1 = r4.v     // Catch:{ all -> 0x00b0 }
            com.bumptech.glide.request.SingleRequest$Status r2 = com.bumptech.glide.request.SingleRequest.Status.RUNNING     // Catch:{ all -> 0x00b0 }
            if (r1 == r2) goto L_0x00a8
            com.bumptech.glide.request.SingleRequest$Status r1 = r4.v     // Catch:{ all -> 0x00b0 }
            com.bumptech.glide.request.SingleRequest$Status r2 = com.bumptech.glide.request.SingleRequest.Status.COMPLETE     // Catch:{ all -> 0x00b0 }
            if (r1 != r2) goto L_0x0052
            com.bumptech.glide.load.engine.Resource<R> r1 = r4.r     // Catch:{ all -> 0x00b0 }
            com.bumptech.glide.load.DataSource r2 = com.bumptech.glide.load.DataSource.MEMORY_CACHE     // Catch:{ all -> 0x00b0 }
            r3 = 0
            r4.onResourceReady(r1, r2, r3)     // Catch:{ all -> 0x00b0 }
            monitor-exit(r0)     // Catch:{ all -> 0x00b0 }
            return
        L_0x0052:
            com.bumptech.glide.request.SingleRequest$Status r1 = com.bumptech.glide.request.SingleRequest.Status.WAITING_FOR_SIZE     // Catch:{ all -> 0x00b0 }
            r4.v = r1     // Catch:{ all -> 0x00b0 }
            int r1 = r4.k     // Catch:{ all -> 0x00b0 }
            int r2 = r4.l     // Catch:{ all -> 0x00b0 }
            boolean r1 = com.bumptech.glide.util.Util.b((int) r1, (int) r2)     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x0068
            int r1 = r4.k     // Catch:{ all -> 0x00b0 }
            int r2 = r4.l     // Catch:{ all -> 0x00b0 }
            r4.onSizeReady(r1, r2)     // Catch:{ all -> 0x00b0 }
            goto L_0x006d
        L_0x0068:
            com.bumptech.glide.request.target.Target<R> r1 = r4.n     // Catch:{ all -> 0x00b0 }
            r1.getSize(r4)     // Catch:{ all -> 0x00b0 }
        L_0x006d:
            com.bumptech.glide.request.SingleRequest$Status r1 = r4.v     // Catch:{ all -> 0x00b0 }
            com.bumptech.glide.request.SingleRequest$Status r2 = com.bumptech.glide.request.SingleRequest.Status.RUNNING     // Catch:{ all -> 0x00b0 }
            if (r1 == r2) goto L_0x0079
            com.bumptech.glide.request.SingleRequest$Status r1 = r4.v     // Catch:{ all -> 0x00b0 }
            com.bumptech.glide.request.SingleRequest$Status r2 = com.bumptech.glide.request.SingleRequest.Status.WAITING_FOR_SIZE     // Catch:{ all -> 0x00b0 }
            if (r1 != r2) goto L_0x0088
        L_0x0079:
            boolean r1 = r4.c()     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x0088
            com.bumptech.glide.request.target.Target<R> r1 = r4.n     // Catch:{ all -> 0x00b0 }
            android.graphics.drawable.Drawable r2 = r4.h()     // Catch:{ all -> 0x00b0 }
            r1.onLoadStarted(r2)     // Catch:{ all -> 0x00b0 }
        L_0x0088:
            boolean r1 = D     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x00a6
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b0 }
            r1.<init>()     // Catch:{ all -> 0x00b0 }
            java.lang.String r2 = "finished run method in "
            r1.append(r2)     // Catch:{ all -> 0x00b0 }
            long r2 = r4.t     // Catch:{ all -> 0x00b0 }
            double r2 = com.bumptech.glide.util.LogTime.a(r2)     // Catch:{ all -> 0x00b0 }
            r1.append(r2)     // Catch:{ all -> 0x00b0 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00b0 }
            r4.a((java.lang.String) r1)     // Catch:{ all -> 0x00b0 }
        L_0x00a6:
            monitor-exit(r0)     // Catch:{ all -> 0x00b0 }
            return
        L_0x00a8:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00b0 }
            java.lang.String r2 = "Cannot restart a running request"
            r1.<init>(r2)     // Catch:{ all -> 0x00b0 }
            throw r1     // Catch:{ all -> 0x00b0 }
        L_0x00b0:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00b0 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.SingleRequest.begin():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0037, code lost:
        r4.u.b((com.bumptech.glide.load.engine.Resource<?>) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void clear() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.c
            monitor-enter(r0)
            r4.a()     // Catch:{ all -> 0x003d }
            com.bumptech.glide.util.pool.StateVerifier r1 = r4.b     // Catch:{ all -> 0x003d }
            r1.a()     // Catch:{ all -> 0x003d }
            com.bumptech.glide.request.SingleRequest$Status r1 = r4.v     // Catch:{ all -> 0x003d }
            com.bumptech.glide.request.SingleRequest$Status r2 = com.bumptech.glide.request.SingleRequest.Status.CLEARED     // Catch:{ all -> 0x003d }
            if (r1 != r2) goto L_0x0013
            monitor-exit(r0)     // Catch:{ all -> 0x003d }
            return
        L_0x0013:
            r4.e()     // Catch:{ all -> 0x003d }
            com.bumptech.glide.load.engine.Resource<R> r1 = r4.r     // Catch:{ all -> 0x003d }
            r2 = 0
            if (r1 == 0) goto L_0x0020
            com.bumptech.glide.load.engine.Resource<R> r1 = r4.r     // Catch:{ all -> 0x003d }
            r4.r = r2     // Catch:{ all -> 0x003d }
            goto L_0x0021
        L_0x0020:
            r1 = r2
        L_0x0021:
            boolean r2 = r4.b()     // Catch:{ all -> 0x003d }
            if (r2 == 0) goto L_0x0030
            com.bumptech.glide.request.target.Target<R> r2 = r4.n     // Catch:{ all -> 0x003d }
            android.graphics.drawable.Drawable r3 = r4.h()     // Catch:{ all -> 0x003d }
            r2.onLoadCleared(r3)     // Catch:{ all -> 0x003d }
        L_0x0030:
            com.bumptech.glide.request.SingleRequest$Status r2 = com.bumptech.glide.request.SingleRequest.Status.CLEARED     // Catch:{ all -> 0x003d }
            r4.v = r2     // Catch:{ all -> 0x003d }
            monitor-exit(r0)     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x003c
            com.bumptech.glide.load.engine.Engine r0 = r4.u
            r0.b((com.bumptech.glide.load.engine.Resource<?>) r1)
        L_0x003c:
            return
        L_0x003d:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003d }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.SingleRequest.clear():void");
    }

    public Object getLock() {
        this.b.a();
        return this.c;
    }

    public boolean isAnyResourceSet() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == Status.COMPLETE;
        }
        return z2;
    }

    public boolean isCleared() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == Status.CLEARED;
        }
        return z2;
    }

    public boolean isComplete() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == Status.COMPLETE;
        }
        return z2;
    }

    public boolean isEquivalentTo(Request request) {
        int i2;
        int i3;
        Object obj;
        Class<R> cls;
        BaseRequestOptions<?> baseRequestOptions;
        Priority priority;
        int size;
        int i4;
        int i5;
        Object obj2;
        Class<R> cls2;
        BaseRequestOptions<?> baseRequestOptions2;
        Priority priority2;
        int size2;
        Request request2 = request;
        if (!(request2 instanceof SingleRequest)) {
            return false;
        }
        synchronized (this.c) {
            i2 = this.k;
            i3 = this.l;
            obj = this.h;
            cls = this.i;
            baseRequestOptions = this.j;
            priority = this.m;
            size = this.o != null ? this.o.size() : 0;
        }
        SingleRequest singleRequest = (SingleRequest) request2;
        synchronized (singleRequest.c) {
            i4 = singleRequest.k;
            i5 = singleRequest.l;
            obj2 = singleRequest.h;
            cls2 = singleRequest.i;
            baseRequestOptions2 = singleRequest.j;
            priority2 = singleRequest.m;
            size2 = singleRequest.o != null ? singleRequest.o.size() : 0;
        }
        return i2 == i4 && i3 == i5 && Util.a(obj, obj2) && cls.equals(cls2) && baseRequestOptions.equals(baseRequestOptions2) && priority == priority2 && size == size2;
    }

    public boolean isRunning() {
        boolean z2;
        synchronized (this.c) {
            if (this.v != Status.RUNNING) {
                if (this.v != Status.WAITING_FOR_SIZE) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    public void onLoadFailed(GlideException glideException) {
        a(glideException, 5);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004f, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        r5.u.b(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00aa, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ac, code lost:
        r5.u.b(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResourceReady(com.bumptech.glide.load.engine.Resource<?> r6, com.bumptech.glide.load.DataSource r7, boolean r8) {
        /*
            r5 = this;
            com.bumptech.glide.util.pool.StateVerifier r0 = r5.b
            r0.a()
            r0 = 0
            java.lang.Object r1 = r5.c     // Catch:{ all -> 0x00bc }
            monitor-enter(r1)     // Catch:{ all -> 0x00bc }
            r5.s = r0     // Catch:{ all -> 0x00b2 }
            if (r6 != 0) goto L_0x002f
            com.bumptech.glide.load.engine.GlideException r6 = new com.bumptech.glide.load.engine.GlideException     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b2 }
            r7.<init>()     // Catch:{ all -> 0x00b2 }
            java.lang.String r8 = "Expected to receive a Resource<R> with an object of "
            r7.append(r8)     // Catch:{ all -> 0x00b2 }
            java.lang.Class<R> r8 = r5.i     // Catch:{ all -> 0x00b2 }
            r7.append(r8)     // Catch:{ all -> 0x00b2 }
            java.lang.String r8 = " inside, but instead got null."
            r7.append(r8)     // Catch:{ all -> 0x00b2 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00b2 }
            r6.<init>(r7)     // Catch:{ all -> 0x00b2 }
            r5.onLoadFailed(r6)     // Catch:{ all -> 0x00b2 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b2 }
            return
        L_0x002f:
            java.lang.Object r2 = r6.get()     // Catch:{ all -> 0x00b2 }
            if (r2 == 0) goto L_0x005c
            java.lang.Class<R> r3 = r5.i     // Catch:{ all -> 0x00b2 }
            java.lang.Class r4 = r2.getClass()     // Catch:{ all -> 0x00b2 }
            boolean r3 = r3.isAssignableFrom(r4)     // Catch:{ all -> 0x00b2 }
            if (r3 != 0) goto L_0x0042
            goto L_0x005c
        L_0x0042:
            boolean r3 = r5.d()     // Catch:{ all -> 0x00b2 }
            if (r3 != 0) goto L_0x0057
            r5.r = r0     // Catch:{ all -> 0x00ba }
            com.bumptech.glide.request.SingleRequest$Status r7 = com.bumptech.glide.request.SingleRequest.Status.COMPLETE     // Catch:{ all -> 0x00ba }
            r5.v = r7     // Catch:{ all -> 0x00ba }
            monitor-exit(r1)     // Catch:{ all -> 0x00ba }
            if (r6 == 0) goto L_0x0056
            com.bumptech.glide.load.engine.Engine r7 = r5.u
            r7.b((com.bumptech.glide.load.engine.Resource<?>) r6)
        L_0x0056:
            return
        L_0x0057:
            r5.a(r6, r2, r7, r8)     // Catch:{ all -> 0x00b2 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b2 }
            return
        L_0x005c:
            r5.r = r0     // Catch:{ all -> 0x00ba }
            com.bumptech.glide.load.engine.GlideException r7 = new com.bumptech.glide.load.engine.GlideException     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r8.<init>()     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = "Expected to receive an object of "
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.Class<R> r0 = r5.i     // Catch:{ all -> 0x00ba }
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = " but instead got "
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            if (r2 == 0) goto L_0x007b
            java.lang.Class r0 = r2.getClass()     // Catch:{ all -> 0x00ba }
            goto L_0x007d
        L_0x007b:
            java.lang.String r0 = ""
        L_0x007d:
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = "{"
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            r8.append(r2)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = "} inside Resource{"
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            r8.append(r6)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = "}."
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            if (r2 == 0) goto L_0x009a
            java.lang.String r0 = ""
            goto L_0x009c
        L_0x009a:
            java.lang.String r0 = " To indicate failure return a null Resource object, rather than a Resource object containing null data."
        L_0x009c:
            r8.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x00ba }
            r7.<init>(r8)     // Catch:{ all -> 0x00ba }
            r5.onLoadFailed(r7)     // Catch:{ all -> 0x00ba }
            monitor-exit(r1)     // Catch:{ all -> 0x00ba }
            if (r6 == 0) goto L_0x00b1
            com.bumptech.glide.load.engine.Engine r7 = r5.u
            r7.b((com.bumptech.glide.load.engine.Resource<?>) r6)
        L_0x00b1:
            return
        L_0x00b2:
            r6 = move-exception
            r7 = r6
            r6 = r0
        L_0x00b5:
            monitor-exit(r1)     // Catch:{ all -> 0x00ba }
            throw r7     // Catch:{ all -> 0x00b7 }
        L_0x00b7:
            r7 = move-exception
            r0 = r6
            goto L_0x00bd
        L_0x00ba:
            r7 = move-exception
            goto L_0x00b5
        L_0x00bc:
            r7 = move-exception
        L_0x00bd:
            if (r0 == 0) goto L_0x00c4
            com.bumptech.glide.load.engine.Engine r6 = r5.u
            r6.b((com.bumptech.glide.load.engine.Resource<?>) r0)
        L_0x00c4:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.SingleRequest.onResourceReady(com.bumptech.glide.load.engine.Resource, com.bumptech.glide.load.DataSource, boolean):void");
    }

    public void onSizeReady(int i2, int i3) {
        Object obj;
        this.b.a();
        Object obj2 = this.c;
        synchronized (obj2) {
            try {
                if (D) {
                    a("Got onSizeReady in " + LogTime.a(this.t));
                }
                if (this.v == Status.WAITING_FOR_SIZE) {
                    this.v = Status.RUNNING;
                    float q2 = this.j.q();
                    this.z = a(i2, q2);
                    this.A = a(i3, q2);
                    if (D) {
                        a("finished setup for calling load in " + LogTime.a(this.t));
                    }
                    obj = obj2;
                    try {
                        this.s = this.u.a(this.g, this.h, this.j.p(), this.z, this.A, this.j.o(), this.i, this.m, this.j.c(), this.j.s(), this.j.A(), this.j.y(), this.j.i(), this.j.w(), this.j.u(), this.j.t(), this.j.h(), this, this.q);
                        if (this.v != Status.RUNNING) {
                            this.s = null;
                        }
                        if (D) {
                            a("finished onSizeReady in " + LogTime.a(this.t));
                        }
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                obj = obj2;
                throw th;
            }
        }
    }

    public void pause() {
        synchronized (this.c) {
            if (isRunning()) {
                clear();
            }
        }
    }

    private void a() {
        if (this.B) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    private Drawable a(int i2) {
        return DrawableDecoderCompat.a((Context) this.g, i2, this.j.r() != null ? this.j.r() : this.f.getTheme());
    }

    private static int a(int i2, float f2) {
        return i2 == Integer.MIN_VALUE ? i2 : Math.round(f2 * ((float) i2));
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ab A[Catch:{ all -> 0x00bc }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.bumptech.glide.load.engine.Resource<R> r10, R r11, com.bumptech.glide.load.DataSource r12, boolean r13) {
        /*
            r9 = this;
            boolean r13 = r9.i()
            com.bumptech.glide.request.SingleRequest$Status r0 = com.bumptech.glide.request.SingleRequest.Status.COMPLETE
            r9.v = r0
            r9.r = r10
            com.bumptech.glide.GlideContext r10 = r9.g
            int r10 = r10.f()
            r0 = 3
            if (r10 > r0) goto L_0x006a
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r0 = "Finished loading "
            r10.append(r0)
            java.lang.Class r0 = r11.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r10.append(r0)
            java.lang.String r0 = " from "
            r10.append(r0)
            r10.append(r12)
            java.lang.String r0 = " for "
            r10.append(r0)
            java.lang.Object r0 = r9.h
            r10.append(r0)
            java.lang.String r0 = " with size ["
            r10.append(r0)
            int r0 = r9.z
            r10.append(r0)
            java.lang.String r0 = "x"
            r10.append(r0)
            int r0 = r9.A
            r10.append(r0)
            java.lang.String r0 = "] in "
            r10.append(r0)
            long r0 = r9.t
            double r0 = com.bumptech.glide.util.LogTime.a(r0)
            r10.append(r0)
            java.lang.String r0 = " ms"
            r10.append(r0)
            java.lang.String r10 = r10.toString()
            java.lang.String r0 = "Glide"
            android.util.Log.d(r0, r10)
        L_0x006a:
            r10 = 1
            r9.B = r10
            r6 = 0
            java.util.List<com.bumptech.glide.request.RequestListener<R>> r0 = r9.o     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x0092
            java.util.List<com.bumptech.glide.request.RequestListener<R>> r0 = r9.o     // Catch:{ all -> 0x00bc }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x00bc }
            r8 = 0
        L_0x0079:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x0093
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x00bc }
            com.bumptech.glide.request.RequestListener r0 = (com.bumptech.glide.request.RequestListener) r0     // Catch:{ all -> 0x00bc }
            java.lang.Object r2 = r9.h     // Catch:{ all -> 0x00bc }
            com.bumptech.glide.request.target.Target<R> r3 = r9.n     // Catch:{ all -> 0x00bc }
            r1 = r11
            r4 = r12
            r5 = r13
            boolean r0 = r0.onResourceReady(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00bc }
            r8 = r8 | r0
            goto L_0x0079
        L_0x0092:
            r8 = 0
        L_0x0093:
            com.bumptech.glide.request.RequestListener<R> r0 = r9.d     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x00a7
            com.bumptech.glide.request.RequestListener<R> r0 = r9.d     // Catch:{ all -> 0x00bc }
            java.lang.Object r2 = r9.h     // Catch:{ all -> 0x00bc }
            com.bumptech.glide.request.target.Target<R> r3 = r9.n     // Catch:{ all -> 0x00bc }
            r1 = r11
            r4 = r12
            r5 = r13
            boolean r0 = r0.onResourceReady(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x00a7
            goto L_0x00a8
        L_0x00a7:
            r10 = 0
        L_0x00a8:
            r10 = r10 | r8
            if (r10 != 0) goto L_0x00b6
            com.bumptech.glide.request.transition.TransitionFactory<? super R> r10 = r9.p     // Catch:{ all -> 0x00bc }
            com.bumptech.glide.request.transition.Transition r10 = r10.build(r12, r13)     // Catch:{ all -> 0x00bc }
            com.bumptech.glide.request.target.Target<R> r12 = r9.n     // Catch:{ all -> 0x00bc }
            r12.onResourceReady(r11, r10)     // Catch:{ all -> 0x00bc }
        L_0x00b6:
            r9.B = r6
            r9.k()
            return
        L_0x00bc:
            r10 = move-exception
            r9.B = r6
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.SingleRequest.a(com.bumptech.glide.load.engine.Resource, java.lang.Object, com.bumptech.glide.load.DataSource, boolean):void");
    }

    /* JADX INFO: finally extract failed */
    private void a(GlideException glideException, int i2) {
        boolean z2;
        this.b.a();
        synchronized (this.c) {
            glideException.a((Exception) this.C);
            int f2 = this.g.f();
            if (f2 <= i2) {
                Log.w("Glide", "Load failed for " + this.h + " with size [" + this.z + "x" + this.A + "]", glideException);
                if (f2 <= 4) {
                    glideException.a("Glide");
                }
            }
            this.s = null;
            this.v = Status.FAILED;
            boolean z3 = true;
            this.B = true;
            try {
                if (this.o != null) {
                    z2 = false;
                    for (RequestListener<R> onLoadFailed : this.o) {
                        z2 |= onLoadFailed.onLoadFailed(glideException, this.h, this.n, i());
                    }
                } else {
                    z2 = false;
                }
                if (this.d == null || !this.d.onLoadFailed(glideException, this.h, this.n, i())) {
                    z3 = false;
                }
                if (!z2 && !z3) {
                    l();
                }
                this.B = false;
                j();
            } catch (Throwable th) {
                this.B = false;
                throw th;
            }
        }
    }

    private void a(String str) {
        Log.v("Request", str + " this: " + this.f2357a);
    }
}
