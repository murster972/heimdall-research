package com.bumptech.glide.request;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class RequestOptions extends BaseRequestOptions<RequestOptions> {
    public static RequestOptions b(DiskCacheStrategy diskCacheStrategy) {
        return (RequestOptions) new RequestOptions().a(diskCacheStrategy);
    }

    public static RequestOptions b(Key key) {
        return (RequestOptions) new RequestOptions().a(key);
    }

    public static RequestOptions b(Class<?> cls) {
        return (RequestOptions) new RequestOptions().a(cls);
    }
}
