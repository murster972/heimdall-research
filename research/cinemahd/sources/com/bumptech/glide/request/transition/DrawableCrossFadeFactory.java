package com.bumptech.glide.request.transition;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.DataSource;

public class DrawableCrossFadeFactory implements TransitionFactory<Drawable> {

    /* renamed from: a  reason: collision with root package name */
    private final int f2364a;
    private final boolean b;
    private DrawableCrossFadeTransition c;

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final int f2365a;
        private boolean b;

        public Builder() {
            this(300);
        }

        public DrawableCrossFadeFactory a() {
            return new DrawableCrossFadeFactory(this.f2365a, this.b);
        }

        public Builder(int i) {
            this.f2365a = i;
        }
    }

    protected DrawableCrossFadeFactory(int i, boolean z) {
        this.f2364a = i;
        this.b = z;
    }

    private Transition<Drawable> a() {
        if (this.c == null) {
            this.c = new DrawableCrossFadeTransition(this.f2364a, this.b);
        }
        return this.c;
    }

    public Transition<Drawable> build(DataSource dataSource, boolean z) {
        if (dataSource == DataSource.MEMORY_CACHE) {
            return NoTransition.a();
        }
        return a();
    }
}
