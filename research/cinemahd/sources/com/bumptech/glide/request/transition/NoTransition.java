package com.bumptech.glide.request.transition;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.request.transition.Transition;

public class NoTransition<R> implements Transition<R> {

    /* renamed from: a  reason: collision with root package name */
    static final NoTransition<?> f2367a = new NoTransition<>();
    private static final TransitionFactory<?> b = new NoAnimationFactory();

    public static class NoAnimationFactory<R> implements TransitionFactory<R> {
        public Transition<R> build(DataSource dataSource, boolean z) {
            return NoTransition.f2367a;
        }
    }

    public static <R> Transition<R> a() {
        return f2367a;
    }

    public static <R> TransitionFactory<R> b() {
        return b;
    }

    public boolean transition(Object obj, Transition.ViewAdapter viewAdapter) {
        return false;
    }
}
