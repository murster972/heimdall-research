package com.bumptech.glide.request.transition;

import android.content.Context;
import android.view.animation.Animation;

interface ViewTransition$ViewTransitionAnimationFactory {
    Animation build(Context context);
}
