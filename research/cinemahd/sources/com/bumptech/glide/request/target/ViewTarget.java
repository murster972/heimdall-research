package com.bumptech.glide.request.target;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import com.bumptech.glide.R$id;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.util.Preconditions;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Deprecated
public abstract class ViewTarget<T extends View, Z> extends BaseTarget<Z> {
    private static int g = R$id.glide_custom_view_target_tag;
    protected final T b;
    private final SizeDeterminer c;
    private View.OnAttachStateChangeListener d;
    private boolean e;
    private boolean f;

    public ViewTarget(T t) {
        Preconditions.a(t);
        this.b = (View) t;
        this.c = new SizeDeterminer(t);
    }

    private void a(Object obj) {
        this.b.setTag(g, obj);
    }

    private void b() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.d;
        if (onAttachStateChangeListener != null && !this.f) {
            this.b.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f = true;
        }
    }

    private void c() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.d;
        if (onAttachStateChangeListener != null && this.f) {
            this.b.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f = false;
        }
    }

    public Request getRequest() {
        Object a2 = a();
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof Request) {
            return (Request) a2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    public void getSize(SizeReadyCallback sizeReadyCallback) {
        this.c.a(sizeReadyCallback);
    }

    public T getView() {
        return this.b;
    }

    public void onLoadCleared(Drawable drawable) {
        super.onLoadCleared(drawable);
        this.c.b();
        if (!this.e) {
            c();
        }
    }

    public void onLoadStarted(Drawable drawable) {
        super.onLoadStarted(drawable);
        b();
    }

    public void removeCallback(SizeReadyCallback sizeReadyCallback) {
        this.c.b(sizeReadyCallback);
    }

    public void setRequest(Request request) {
        a(request);
    }

    public String toString() {
        return "Target for: " + this.b;
    }

    static final class SizeDeterminer {
        static Integer e;

        /* renamed from: a  reason: collision with root package name */
        private final View f2362a;
        private final List<SizeReadyCallback> b = new ArrayList();
        boolean c;
        private SizeDeterminerLayoutListener d;

        private static final class SizeDeterminerLayoutListener implements ViewTreeObserver.OnPreDrawListener {

            /* renamed from: a  reason: collision with root package name */
            private final WeakReference<SizeDeterminer> f2363a;

            SizeDeterminerLayoutListener(SizeDeterminer sizeDeterminer) {
                this.f2363a = new WeakReference<>(sizeDeterminer);
            }

            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                SizeDeterminer sizeDeterminer = (SizeDeterminer) this.f2363a.get();
                if (sizeDeterminer == null) {
                    return true;
                }
                sizeDeterminer.a();
                return true;
            }
        }

        SizeDeterminer(View view) {
            this.f2362a = view;
        }

        private static int a(Context context) {
            if (e == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                Preconditions.a(windowManager);
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return e.intValue();
        }

        private boolean a(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        private void b(int i, int i2) {
            Iterator it2 = new ArrayList(this.b).iterator();
            while (it2.hasNext()) {
                ((SizeReadyCallback) it2.next()).onSizeReady(i, i2);
            }
        }

        private int c() {
            int paddingTop = this.f2362a.getPaddingTop() + this.f2362a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.f2362a.getLayoutParams();
            return a(this.f2362a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop);
        }

        private int d() {
            int paddingLeft = this.f2362a.getPaddingLeft() + this.f2362a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.f2362a.getLayoutParams();
            return a(this.f2362a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft);
        }

        /* access modifiers changed from: package-private */
        public void b(SizeReadyCallback sizeReadyCallback) {
            this.b.remove(sizeReadyCallback);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            ViewTreeObserver viewTreeObserver = this.f2362a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.d);
            }
            this.d = null;
            this.b.clear();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (!this.b.isEmpty()) {
                int d2 = d();
                int c2 = c();
                if (a(d2, c2)) {
                    b(d2, c2);
                    b();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(SizeReadyCallback sizeReadyCallback) {
            int d2 = d();
            int c2 = c();
            if (a(d2, c2)) {
                sizeReadyCallback.onSizeReady(d2, c2);
                return;
            }
            if (!this.b.contains(sizeReadyCallback)) {
                this.b.add(sizeReadyCallback);
            }
            if (this.d == null) {
                ViewTreeObserver viewTreeObserver = this.f2362a.getViewTreeObserver();
                this.d = new SizeDeterminerLayoutListener(this);
                viewTreeObserver.addOnPreDrawListener(this.d);
            }
        }

        private boolean a(int i, int i2) {
            return a(i) && a(i2);
        }

        private int a(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.f2362a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.f2362a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return a(this.f2362a.getContext());
        }
    }

    private Object a() {
        return this.b.getTag(g);
    }
}
