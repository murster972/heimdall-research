package com.bumptech.glide.request.target;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.request.transition.Transition;

public abstract class ImageViewTarget<Z> extends ViewTarget<ImageView, Z> implements Transition.ViewAdapter {
    private Animatable h;

    public ImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    private void b(Z z) {
        if (z instanceof Animatable) {
            this.h = (Animatable) z;
            this.h.start();
            return;
        }
        this.h = null;
    }

    private void c(Z z) {
        a(z);
        b(z);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Z z);

    public Drawable getCurrentDrawable() {
        return ((ImageView) this.b).getDrawable();
    }

    public void onLoadCleared(Drawable drawable) {
        super.onLoadCleared(drawable);
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.stop();
        }
        c((Object) null);
        setDrawable(drawable);
    }

    public void onLoadFailed(Drawable drawable) {
        super.onLoadFailed(drawable);
        c((Object) null);
        setDrawable(drawable);
    }

    public void onLoadStarted(Drawable drawable) {
        super.onLoadStarted(drawable);
        c((Object) null);
        setDrawable(drawable);
    }

    public void onResourceReady(Z z, Transition<? super Z> transition) {
        if (transition == null || !transition.transition(z, this)) {
            c(z);
        } else {
            b(z);
        }
    }

    public void onStart() {
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.start();
        }
    }

    public void onStop() {
        Animatable animatable = this.h;
        if (animatable != null) {
            animatable.stop();
        }
    }

    public void setDrawable(Drawable drawable) {
        ((ImageView) this.b).setImageDrawable(drawable);
    }
}
