package com.bumptech.glide.request;

import com.bumptech.glide.request.RequestCoordinator;

public final class ErrorRequestCoordinator implements RequestCoordinator, Request {

    /* renamed from: a  reason: collision with root package name */
    private final Object f2355a;
    private final RequestCoordinator b;
    private volatile Request c;
    private volatile Request d;
    private RequestCoordinator.RequestState e;
    private RequestCoordinator.RequestState f;

    public ErrorRequestCoordinator(Object obj, RequestCoordinator requestCoordinator) {
        RequestCoordinator.RequestState requestState = RequestCoordinator.RequestState.CLEARED;
        this.e = requestState;
        this.f = requestState;
        this.f2355a = obj;
        this.b = requestCoordinator;
    }

    private boolean b() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.canNotifyStatusChanged(this);
    }

    private boolean c() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.canSetImage(this);
    }

    public void a(Request request, Request request2) {
        this.c = request;
        this.d = request2;
    }

    public void begin() {
        synchronized (this.f2355a) {
            if (this.e != RequestCoordinator.RequestState.RUNNING) {
                this.e = RequestCoordinator.RequestState.RUNNING;
                this.c.begin();
            }
        }
    }

    public boolean canNotifyCleared(Request request) {
        boolean z;
        synchronized (this.f2355a) {
            z = a() && a(request);
        }
        return z;
    }

    public boolean canNotifyStatusChanged(Request request) {
        boolean z;
        synchronized (this.f2355a) {
            z = b() && a(request);
        }
        return z;
    }

    public boolean canSetImage(Request request) {
        boolean z;
        synchronized (this.f2355a) {
            z = c() && a(request);
        }
        return z;
    }

    public void clear() {
        synchronized (this.f2355a) {
            this.e = RequestCoordinator.RequestState.CLEARED;
            this.c.clear();
            if (this.f != RequestCoordinator.RequestState.CLEARED) {
                this.f = RequestCoordinator.RequestState.CLEARED;
                this.d.clear();
            }
        }
    }

    public RequestCoordinator getRoot() {
        RequestCoordinator root;
        synchronized (this.f2355a) {
            root = this.b != null ? this.b.getRoot() : this;
        }
        return root;
    }

    public boolean isAnyResourceSet() {
        boolean z;
        synchronized (this.f2355a) {
            if (!this.c.isAnyResourceSet()) {
                if (!this.d.isAnyResourceSet()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    public boolean isCleared() {
        boolean z;
        synchronized (this.f2355a) {
            z = this.e == RequestCoordinator.RequestState.CLEARED && this.f == RequestCoordinator.RequestState.CLEARED;
        }
        return z;
    }

    public boolean isComplete() {
        boolean z;
        synchronized (this.f2355a) {
            if (this.e != RequestCoordinator.RequestState.SUCCESS) {
                if (this.f != RequestCoordinator.RequestState.SUCCESS) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    public boolean isEquivalentTo(Request request) {
        if (!(request instanceof ErrorRequestCoordinator)) {
            return false;
        }
        ErrorRequestCoordinator errorRequestCoordinator = (ErrorRequestCoordinator) request;
        if (!this.c.isEquivalentTo(errorRequestCoordinator.c) || !this.d.isEquivalentTo(errorRequestCoordinator.d)) {
            return false;
        }
        return true;
    }

    public boolean isRunning() {
        boolean z;
        synchronized (this.f2355a) {
            if (this.e != RequestCoordinator.RequestState.RUNNING) {
                if (this.f != RequestCoordinator.RequestState.RUNNING) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRequestFailed(com.bumptech.glide.request.Request r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f2355a
            monitor-enter(r0)
            com.bumptech.glide.request.Request r1 = r2.d     // Catch:{ all -> 0x002f }
            boolean r3 = r3.equals(r1)     // Catch:{ all -> 0x002f }
            if (r3 != 0) goto L_0x0020
            com.bumptech.glide.request.RequestCoordinator$RequestState r3 = com.bumptech.glide.request.RequestCoordinator.RequestState.FAILED     // Catch:{ all -> 0x002f }
            r2.e = r3     // Catch:{ all -> 0x002f }
            com.bumptech.glide.request.RequestCoordinator$RequestState r3 = r2.f     // Catch:{ all -> 0x002f }
            com.bumptech.glide.request.RequestCoordinator$RequestState r1 = com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING     // Catch:{ all -> 0x002f }
            if (r3 == r1) goto L_0x001e
            com.bumptech.glide.request.RequestCoordinator$RequestState r3 = com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING     // Catch:{ all -> 0x002f }
            r2.f = r3     // Catch:{ all -> 0x002f }
            com.bumptech.glide.request.Request r3 = r2.d     // Catch:{ all -> 0x002f }
            r3.begin()     // Catch:{ all -> 0x002f }
        L_0x001e:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return
        L_0x0020:
            com.bumptech.glide.request.RequestCoordinator$RequestState r3 = com.bumptech.glide.request.RequestCoordinator.RequestState.FAILED     // Catch:{ all -> 0x002f }
            r2.f = r3     // Catch:{ all -> 0x002f }
            com.bumptech.glide.request.RequestCoordinator r3 = r2.b     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x002d
            com.bumptech.glide.request.RequestCoordinator r3 = r2.b     // Catch:{ all -> 0x002f }
            r3.onRequestFailed(r2)     // Catch:{ all -> 0x002f }
        L_0x002d:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return
        L_0x002f:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.ErrorRequestCoordinator.onRequestFailed(com.bumptech.glide.request.Request):void");
    }

    public void onRequestSuccess(Request request) {
        synchronized (this.f2355a) {
            if (request.equals(this.c)) {
                this.e = RequestCoordinator.RequestState.SUCCESS;
            } else if (request.equals(this.d)) {
                this.f = RequestCoordinator.RequestState.SUCCESS;
            }
            if (this.b != null) {
                this.b.onRequestSuccess(this);
            }
        }
    }

    public void pause() {
        synchronized (this.f2355a) {
            if (this.e == RequestCoordinator.RequestState.RUNNING) {
                this.e = RequestCoordinator.RequestState.PAUSED;
                this.c.pause();
            }
            if (this.f == RequestCoordinator.RequestState.RUNNING) {
                this.f = RequestCoordinator.RequestState.PAUSED;
                this.d.pause();
            }
        }
    }

    private boolean a() {
        RequestCoordinator requestCoordinator = this.b;
        return requestCoordinator == null || requestCoordinator.canNotifyCleared(this);
    }

    private boolean a(Request request) {
        return request.equals(this.c) || (this.e == RequestCoordinator.RequestState.FAILED && request.equals(this.d));
    }
}
