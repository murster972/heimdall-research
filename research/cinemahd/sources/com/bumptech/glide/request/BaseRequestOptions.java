package com.bumptech.glide.request;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.signature.EmptySignature;
import com.bumptech.glide.util.CachedHashCodeArrayMap;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import com.google.ar.core.ImageMetadata;
import java.util.Map;
import okhttp3.internal.http2.Http2;

public abstract class BaseRequestOptions<T extends BaseRequestOptions<T>> implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f2354a;
    private float b = 1.0f;
    private DiskCacheStrategy c = DiskCacheStrategy.c;
    private Priority d = Priority.NORMAL;
    private Drawable e;
    private int f;
    private Drawable g;
    private int h;
    private boolean i = true;
    private int j = -1;
    private int k = -1;
    private Key l = EmptySignature.a();
    private boolean m;
    private boolean n = true;
    private Drawable o;
    private int p;
    private Options q = new Options();
    private Map<Class<?>, Transformation<?>> r = new CachedHashCodeArrayMap();
    private Class<?> s = Object.class;
    private boolean t;
    private Resources.Theme u;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y = true;
    private boolean z;

    private T I() {
        return this;
    }

    private static boolean b(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.bumptech.glide.load.Transformation, com.bumptech.glide.load.Transformation<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private T c(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy r2, com.bumptech.glide.load.Transformation<android.graphics.Bitmap> r3) {
        /*
            r1 = this;
            r0 = 0
            com.bumptech.glide.request.BaseRequestOptions r2 = r1.a((com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) r2, (com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r3, (boolean) r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.c(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy, com.bumptech.glide.load.Transformation):com.bumptech.glide.request.BaseRequestOptions");
    }

    public final boolean A() {
        return this.m;
    }

    public final boolean B() {
        return c(2048);
    }

    public final boolean C() {
        return Util.b(this.k, this.j);
    }

    public T D() {
        this.t = true;
        I();
        return this;
    }

    public T E() {
        return a(DownsampleStrategy.c, (Transformation<Bitmap>) new CenterCrop());
    }

    public T F() {
        return c(DownsampleStrategy.b, new CenterInside());
    }

    public T G() {
        return c(DownsampleStrategy.f2292a, new FitCenter());
    }

    /* access modifiers changed from: protected */
    public final T H() {
        if (!this.t) {
            I();
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    public T a(float f2) {
        if (this.v) {
            return clone().a(f2);
        }
        if (f2 < 0.0f || f2 > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.b = f2;
        this.f2354a |= 2;
        return H();
    }

    public T b(boolean z2) {
        if (this.v) {
            return clone().b(z2);
        }
        this.z = z2;
        this.f2354a |= 1048576;
        return H();
    }

    public final int d() {
        return this.f;
    }

    public final Drawable e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BaseRequestOptions)) {
            return false;
        }
        BaseRequestOptions baseRequestOptions = (BaseRequestOptions) obj;
        if (Float.compare(baseRequestOptions.b, this.b) == 0 && this.f == baseRequestOptions.f && Util.b((Object) this.e, (Object) baseRequestOptions.e) && this.h == baseRequestOptions.h && Util.b((Object) this.g, (Object) baseRequestOptions.g) && this.p == baseRequestOptions.p && Util.b((Object) this.o, (Object) baseRequestOptions.o) && this.i == baseRequestOptions.i && this.j == baseRequestOptions.j && this.k == baseRequestOptions.k && this.m == baseRequestOptions.m && this.n == baseRequestOptions.n && this.w == baseRequestOptions.w && this.x == baseRequestOptions.x && this.c.equals(baseRequestOptions.c) && this.d == baseRequestOptions.d && this.q.equals(baseRequestOptions.q) && this.r.equals(baseRequestOptions.r) && this.s.equals(baseRequestOptions.s) && Util.b((Object) this.l, (Object) baseRequestOptions.l) && Util.b((Object) this.u, (Object) baseRequestOptions.u)) {
            return true;
        }
        return false;
    }

    public final Drawable f() {
        return this.o;
    }

    public final int g() {
        return this.p;
    }

    public final boolean h() {
        return this.x;
    }

    public int hashCode() {
        return Util.a((Object) this.u, Util.a((Object) this.l, Util.a((Object) this.s, Util.a((Object) this.r, Util.a((Object) this.q, Util.a((Object) this.d, Util.a((Object) this.c, Util.a(this.x, Util.a(this.w, Util.a(this.n, Util.a(this.m, Util.a(this.k, Util.a(this.j, Util.a(this.i, Util.a((Object) this.o, Util.a(this.p, Util.a((Object) this.g, Util.a(this.h, Util.a((Object) this.e, Util.a(this.f, Util.a(this.b)))))))))))))))))))));
    }

    public final Options i() {
        return this.q;
    }

    public final int j() {
        return this.j;
    }

    public final int k() {
        return this.k;
    }

    public final Drawable l() {
        return this.g;
    }

    public final int m() {
        return this.h;
    }

    public final Priority n() {
        return this.d;
    }

    public final Class<?> o() {
        return this.s;
    }

    public final Key p() {
        return this.l;
    }

    public final float q() {
        return this.b;
    }

    public final Resources.Theme r() {
        return this.u;
    }

    public final Map<Class<?>, Transformation<?>> s() {
        return this.r;
    }

    public final boolean t() {
        return this.z;
    }

    public final boolean u() {
        return this.w;
    }

    /* access modifiers changed from: protected */
    public final boolean v() {
        return this.v;
    }

    public final boolean w() {
        return this.i;
    }

    public final boolean x() {
        return c(8);
    }

    /* access modifiers changed from: package-private */
    public boolean y() {
        return this.y;
    }

    public final boolean z() {
        return this.n;
    }

    public final DiskCacheStrategy c() {
        return this.c;
    }

    public T clone() {
        try {
            T t2 = (BaseRequestOptions) super.clone();
            t2.q = new Options();
            t2.q.a(this.q);
            t2.r = new CachedHashCodeArrayMap();
            t2.r.putAll(this.r);
            t2.t = false;
            t2.v = false;
            return t2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    private boolean c(int i2) {
        return b(this.f2354a, i2);
    }

    public T b(int i2) {
        if (this.v) {
            return clone().b(i2);
        }
        this.h = i2;
        this.f2354a |= 128;
        this.g = null;
        this.f2354a &= -65;
        return H();
    }

    public T a(DiskCacheStrategy diskCacheStrategy) {
        if (this.v) {
            return clone().a(diskCacheStrategy);
        }
        Preconditions.a(diskCacheStrategy);
        this.c = diskCacheStrategy;
        this.f2354a |= 4;
        return H();
    }

    public T a(Priority priority) {
        if (this.v) {
            return clone().a(priority);
        }
        Preconditions.a(priority);
        this.d = priority;
        this.f2354a |= 8;
        return H();
    }

    public T b() {
        return b(DownsampleStrategy.c, (Transformation<Bitmap>) new CenterCrop());
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.bumptech.glide.load.Transformation, com.bumptech.glide.load.Transformation<android.graphics.Bitmap>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T b(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy r2, com.bumptech.glide.load.Transformation<android.graphics.Bitmap> r3) {
        /*
            r1 = this;
            boolean r0 = r1.v
            if (r0 == 0) goto L_0x000d
            com.bumptech.glide.request.BaseRequestOptions r0 = r1.clone()
            com.bumptech.glide.request.BaseRequestOptions r2 = r0.b((com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) r2, (com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r3)
            return r2
        L_0x000d:
            r1.a((com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) r2)
            com.bumptech.glide.request.BaseRequestOptions r2 = r1.a((com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r3)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.b(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy, com.bumptech.glide.load.Transformation):com.bumptech.glide.request.BaseRequestOptions");
    }

    public T a(int i2) {
        if (this.v) {
            return clone().a(i2);
        }
        this.f = i2;
        this.f2354a |= 32;
        this.e = null;
        this.f2354a &= -17;
        return H();
    }

    public T a(boolean z2) {
        if (this.v) {
            return clone().a(true);
        }
        this.i = !z2;
        this.f2354a |= 256;
        return H();
    }

    public T a(int i2, int i3) {
        if (this.v) {
            return clone().a(i2, i3);
        }
        this.k = i2;
        this.j = i3;
        this.f2354a |= AdRequest.MAX_CONTENT_URL_LENGTH;
        return H();
    }

    public T a(Key key) {
        if (this.v) {
            return clone().a(key);
        }
        Preconditions.a(key);
        this.l = key;
        this.f2354a |= ByteConstants.KB;
        return H();
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [com.bumptech.glide.load.Option, java.lang.Object, com.bumptech.glide.load.Option<Y>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Y> T a(com.bumptech.glide.load.Option<Y> r2, Y r3) {
        /*
            r1 = this;
            boolean r0 = r1.v
            if (r0 == 0) goto L_0x000d
            com.bumptech.glide.request.BaseRequestOptions r0 = r1.clone()
            com.bumptech.glide.request.BaseRequestOptions r2 = r0.a(r2, r3)
            return r2
        L_0x000d:
            com.bumptech.glide.util.Preconditions.a(r2)
            com.bumptech.glide.util.Preconditions.a(r3)
            com.bumptech.glide.load.Options r0 = r1.q
            r0.a(r2, r3)
            com.bumptech.glide.request.BaseRequestOptions r2 = r1.H()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.a(com.bumptech.glide.load.Option, java.lang.Object):com.bumptech.glide.request.BaseRequestOptions");
    }

    public T a(Class<?> cls) {
        if (this.v) {
            return clone().a(cls);
        }
        Preconditions.a(cls);
        this.s = cls;
        this.f2354a |= 4096;
        return H();
    }

    public T a(DownsampleStrategy downsampleStrategy) {
        Option option = DownsampleStrategy.f;
        Preconditions.a(downsampleStrategy);
        return a(option, downsampleStrategy);
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.bumptech.glide.load.Transformation, com.bumptech.glide.load.Transformation<android.graphics.Bitmap>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy r2, com.bumptech.glide.load.Transformation<android.graphics.Bitmap> r3) {
        /*
            r1 = this;
            boolean r0 = r1.v
            if (r0 == 0) goto L_0x000d
            com.bumptech.glide.request.BaseRequestOptions r0 = r1.clone()
            com.bumptech.glide.request.BaseRequestOptions r2 = r0.a((com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) r2, (com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r3)
            return r2
        L_0x000d:
            r1.a((com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) r2)
            r2 = 0
            com.bumptech.glide.request.BaseRequestOptions r2 = r1.a((com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r3, (boolean) r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy, com.bumptech.glide.load.Transformation):com.bumptech.glide.request.BaseRequestOptions");
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [com.bumptech.glide.load.Transformation, com.bumptech.glide.load.Transformation<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private T a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy r1, com.bumptech.glide.load.Transformation<android.graphics.Bitmap> r2, boolean r3) {
        /*
            r0 = this;
            if (r3 == 0) goto L_0x0007
            com.bumptech.glide.request.BaseRequestOptions r1 = r0.b((com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) r1, (com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r2)
            goto L_0x000b
        L_0x0007:
            com.bumptech.glide.request.BaseRequestOptions r1 = r0.a((com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) r1, (com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r2)
        L_0x000b:
            r2 = 1
            r1.y = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy, com.bumptech.glide.load.Transformation, boolean):com.bumptech.glide.request.BaseRequestOptions");
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [com.bumptech.glide.load.Transformation, com.bumptech.glide.load.Transformation<android.graphics.Bitmap>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public T a(com.bumptech.glide.load.Transformation<android.graphics.Bitmap> r2) {
        /*
            r1 = this;
            r0 = 1
            com.bumptech.glide.request.BaseRequestOptions r2 = r1.a((com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r2, (boolean) r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.a(com.bumptech.glide.load.Transformation):com.bumptech.glide.request.BaseRequestOptions");
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.bumptech.glide.load.Transformation, com.bumptech.glide.load.Transformation<android.graphics.Bitmap>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public T a(com.bumptech.glide.load.Transformation<android.graphics.Bitmap> r3, boolean r4) {
        /*
            r2 = this;
            boolean r0 = r2.v
            if (r0 == 0) goto L_0x000d
            com.bumptech.glide.request.BaseRequestOptions r0 = r2.clone()
            com.bumptech.glide.request.BaseRequestOptions r3 = r0.a((com.bumptech.glide.load.Transformation<android.graphics.Bitmap>) r3, (boolean) r4)
            return r3
        L_0x000d:
            com.bumptech.glide.load.resource.bitmap.DrawableTransformation r0 = new com.bumptech.glide.load.resource.bitmap.DrawableTransformation
            r0.<init>(r3, r4)
            java.lang.Class<android.graphics.Bitmap> r1 = android.graphics.Bitmap.class
            r2.a(r1, r3, (boolean) r4)
            java.lang.Class<android.graphics.drawable.Drawable> r1 = android.graphics.drawable.Drawable.class
            r2.a(r1, r0, (boolean) r4)
            java.lang.Class<android.graphics.drawable.BitmapDrawable> r1 = android.graphics.drawable.BitmapDrawable.class
            r0.a()
            r2.a(r1, r0, (boolean) r4)
            java.lang.Class<com.bumptech.glide.load.resource.gif.GifDrawable> r0 = com.bumptech.glide.load.resource.gif.GifDrawable.class
            com.bumptech.glide.load.resource.gif.GifDrawableTransformation r1 = new com.bumptech.glide.load.resource.gif.GifDrawableTransformation
            r1.<init>(r3)
            r2.a(r0, r1, (boolean) r4)
            com.bumptech.glide.request.BaseRequestOptions r3 = r2.H()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.a(com.bumptech.glide.load.Transformation, boolean):com.bumptech.glide.request.BaseRequestOptions");
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Class<Y>, java.lang.Object, java.lang.Class] */
    /* JADX WARNING: type inference failed for: r3v0, types: [com.bumptech.glide.load.Transformation<Y>, com.bumptech.glide.load.Transformation, java.lang.Object] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Y> T a(java.lang.Class<Y> r2, com.bumptech.glide.load.Transformation<Y> r3, boolean r4) {
        /*
            r1 = this;
            boolean r0 = r1.v
            if (r0 == 0) goto L_0x000d
            com.bumptech.glide.request.BaseRequestOptions r0 = r1.clone()
            com.bumptech.glide.request.BaseRequestOptions r2 = r0.a(r2, r3, (boolean) r4)
            return r2
        L_0x000d:
            com.bumptech.glide.util.Preconditions.a(r2)
            com.bumptech.glide.util.Preconditions.a(r3)
            java.util.Map<java.lang.Class<?>, com.bumptech.glide.load.Transformation<?>> r0 = r1.r
            r0.put(r2, r3)
            int r2 = r1.f2354a
            r2 = r2 | 2048(0x800, float:2.87E-42)
            r1.f2354a = r2
            r2 = 1
            r1.n = r2
            int r3 = r1.f2354a
            r0 = 65536(0x10000, float:9.18355E-41)
            r3 = r3 | r0
            r1.f2354a = r3
            r3 = 0
            r1.y = r3
            if (r4 == 0) goto L_0x0036
            int r3 = r1.f2354a
            r4 = 131072(0x20000, float:1.83671E-40)
            r3 = r3 | r4
            r1.f2354a = r3
            r1.m = r2
        L_0x0036:
            com.bumptech.glide.request.BaseRequestOptions r2 = r1.H()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.BaseRequestOptions.a(java.lang.Class, com.bumptech.glide.load.Transformation, boolean):com.bumptech.glide.request.BaseRequestOptions");
    }

    public T a(BaseRequestOptions<?> baseRequestOptions) {
        if (this.v) {
            return clone().a(baseRequestOptions);
        }
        if (b(baseRequestOptions.f2354a, 2)) {
            this.b = baseRequestOptions.b;
        }
        if (b(baseRequestOptions.f2354a, 262144)) {
            this.w = baseRequestOptions.w;
        }
        if (b(baseRequestOptions.f2354a, 1048576)) {
            this.z = baseRequestOptions.z;
        }
        if (b(baseRequestOptions.f2354a, 4)) {
            this.c = baseRequestOptions.c;
        }
        if (b(baseRequestOptions.f2354a, 8)) {
            this.d = baseRequestOptions.d;
        }
        if (b(baseRequestOptions.f2354a, 16)) {
            this.e = baseRequestOptions.e;
            this.f = 0;
            this.f2354a &= -33;
        }
        if (b(baseRequestOptions.f2354a, 32)) {
            this.f = baseRequestOptions.f;
            this.e = null;
            this.f2354a &= -17;
        }
        if (b(baseRequestOptions.f2354a, 64)) {
            this.g = baseRequestOptions.g;
            this.h = 0;
            this.f2354a &= -129;
        }
        if (b(baseRequestOptions.f2354a, 128)) {
            this.h = baseRequestOptions.h;
            this.g = null;
            this.f2354a &= -65;
        }
        if (b(baseRequestOptions.f2354a, 256)) {
            this.i = baseRequestOptions.i;
        }
        if (b(baseRequestOptions.f2354a, (int) AdRequest.MAX_CONTENT_URL_LENGTH)) {
            this.k = baseRequestOptions.k;
            this.j = baseRequestOptions.j;
        }
        if (b(baseRequestOptions.f2354a, (int) ByteConstants.KB)) {
            this.l = baseRequestOptions.l;
        }
        if (b(baseRequestOptions.f2354a, 4096)) {
            this.s = baseRequestOptions.s;
        }
        if (b(baseRequestOptions.f2354a, 8192)) {
            this.o = baseRequestOptions.o;
            this.p = 0;
            this.f2354a &= -16385;
        }
        if (b(baseRequestOptions.f2354a, (int) Http2.INITIAL_MAX_FRAME_SIZE)) {
            this.p = baseRequestOptions.p;
            this.o = null;
            this.f2354a &= -8193;
        }
        if (b(baseRequestOptions.f2354a, 32768)) {
            this.u = baseRequestOptions.u;
        }
        if (b(baseRequestOptions.f2354a, (int) ImageMetadata.CONTROL_AE_ANTIBANDING_MODE)) {
            this.n = baseRequestOptions.n;
        }
        if (b(baseRequestOptions.f2354a, 131072)) {
            this.m = baseRequestOptions.m;
        }
        if (b(baseRequestOptions.f2354a, 2048)) {
            this.r.putAll(baseRequestOptions.r);
            this.y = baseRequestOptions.y;
        }
        if (b(baseRequestOptions.f2354a, (int) ImageMetadata.LENS_APERTURE)) {
            this.x = baseRequestOptions.x;
        }
        if (!this.n) {
            this.r.clear();
            this.f2354a &= -2049;
            this.m = false;
            this.f2354a &= -131073;
            this.y = true;
        }
        this.f2354a |= baseRequestOptions.f2354a;
        this.q.a(baseRequestOptions.q);
        return H();
    }

    public T a() {
        if (!this.t || this.v) {
            this.v = true;
            return D();
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }
}
