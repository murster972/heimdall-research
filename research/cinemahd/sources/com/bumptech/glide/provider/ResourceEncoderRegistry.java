package com.bumptech.glide.provider;

import com.bumptech.glide.load.ResourceEncoder;
import java.util.ArrayList;
import java.util.List;

public class ResourceEncoderRegistry {

    /* renamed from: a  reason: collision with root package name */
    private final List<Entry<?>> f2352a = new ArrayList();

    private static final class Entry<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Class<T> f2353a;
        final ResourceEncoder<T> b;

        Entry(Class<T> cls, ResourceEncoder<T> resourceEncoder) {
            this.f2353a = cls;
            this.b = resourceEncoder;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Class<?> cls) {
            return this.f2353a.isAssignableFrom(cls);
        }
    }

    public synchronized <Z> void a(Class<Z> cls, ResourceEncoder<Z> resourceEncoder) {
        this.f2352a.add(new Entry(cls, resourceEncoder));
    }

    public synchronized <Z> ResourceEncoder<Z> a(Class<Z> cls) {
        int size = this.f2352a.size();
        for (int i = 0; i < size; i++) {
            Entry entry = this.f2352a.get(i);
            if (entry.a(cls)) {
                return entry.b;
            }
        }
        return null;
    }
}
