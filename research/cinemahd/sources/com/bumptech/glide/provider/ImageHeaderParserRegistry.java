package com.bumptech.glide.provider;

import com.bumptech.glide.load.ImageHeaderParser;
import java.util.ArrayList;
import java.util.List;

public final class ImageHeaderParserRegistry {

    /* renamed from: a  reason: collision with root package name */
    private final List<ImageHeaderParser> f2347a = new ArrayList();

    public synchronized List<ImageHeaderParser> a() {
        return this.f2347a;
    }

    public synchronized void a(ImageHeaderParser imageHeaderParser) {
        this.f2347a.add(imageHeaderParser);
    }
}
