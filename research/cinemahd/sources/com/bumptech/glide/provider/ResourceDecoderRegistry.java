package com.bumptech.glide.provider;

import com.bumptech.glide.load.ResourceDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResourceDecoderRegistry {

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f2350a = new ArrayList();
    private final Map<String, List<Entry<?, ?>>> b = new HashMap();

    private static class Entry<T, R> {

        /* renamed from: a  reason: collision with root package name */
        private final Class<T> f2351a;
        final Class<R> b;
        final ResourceDecoder<T, R> c;

        public Entry(Class<T> cls, Class<R> cls2, ResourceDecoder<T, R> resourceDecoder) {
            this.f2351a = cls;
            this.b = cls2;
            this.c = resourceDecoder;
        }

        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.f2351a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    public synchronized void a(List<String> list) {
        ArrayList<String> arrayList = new ArrayList<>(this.f2350a);
        this.f2350a.clear();
        for (String add : list) {
            this.f2350a.add(add);
        }
        for (String str : arrayList) {
            if (!list.contains(str)) {
                this.f2350a.add(str);
            }
        }
    }

    public synchronized <T, R> List<Class<R>> b(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.f2350a) {
            List<Entry> list = this.b.get(str);
            if (list != null) {
                for (Entry entry : list) {
                    if (entry.a(cls, cls2) && !arrayList.contains(entry.b)) {
                        arrayList.add(entry.b);
                    }
                }
            }
        }
        return arrayList;
    }

    public synchronized <T, R> List<ResourceDecoder<T, R>> a(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.f2350a) {
            List<Entry> list = this.b.get(str);
            if (list != null) {
                for (Entry entry : list) {
                    if (entry.a(cls, cls2)) {
                        arrayList.add(entry.c);
                    }
                }
            }
        }
        return arrayList;
    }

    public synchronized <T, R> void a(String str, ResourceDecoder<T, R> resourceDecoder, Class<T> cls, Class<R> cls2) {
        a(str).add(new Entry(cls, cls2, resourceDecoder));
    }

    private synchronized List<Entry<?, ?>> a(String str) {
        List<Entry<?, ?>> list;
        if (!this.f2350a.contains(str)) {
            this.f2350a.add(str);
        }
        list = this.b.get(str);
        if (list == null) {
            list = new ArrayList<>();
            this.b.put(str, list);
        }
        return list;
    }
}
