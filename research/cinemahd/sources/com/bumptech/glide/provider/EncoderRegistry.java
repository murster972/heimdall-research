package com.bumptech.glide.provider;

import com.bumptech.glide.load.Encoder;
import java.util.ArrayList;
import java.util.List;

public class EncoderRegistry {

    /* renamed from: a  reason: collision with root package name */
    private final List<Entry<?>> f2345a = new ArrayList();

    private static final class Entry<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Class<T> f2346a;
        final Encoder<T> b;

        Entry(Class<T> cls, Encoder<T> encoder) {
            this.f2346a = cls;
            this.b = encoder;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Class<?> cls) {
            return this.f2346a.isAssignableFrom(cls);
        }
    }

    public synchronized <T> Encoder<T> a(Class<T> cls) {
        for (Entry next : this.f2345a) {
            if (next.a(cls)) {
                return next.b;
            }
        }
        return null;
    }

    public synchronized <T> void a(Class<T> cls, Encoder<T> encoder) {
        this.f2345a.add(new Entry(cls, encoder));
    }
}
