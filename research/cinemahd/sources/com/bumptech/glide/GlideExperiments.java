package com.bumptech.glide;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GlideExperiments {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Class<?>, Experiment> f2107a;

    static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Map<Class<?>, Experiment> f2108a = new HashMap();

        Builder() {
        }

        /* access modifiers changed from: package-private */
        public GlideExperiments a() {
            return new GlideExperiments(this);
        }
    }

    interface Experiment {
    }

    GlideExperiments(Builder builder) {
        this.f2107a = Collections.unmodifiableMap(new HashMap(builder.f2108a));
    }

    public boolean a(Class<? extends Experiment> cls) {
        return this.f2107a.containsKey(cls);
    }
}
