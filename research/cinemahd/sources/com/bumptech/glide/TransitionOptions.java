package com.bumptech.glide;

import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.request.transition.NoTransition;
import com.bumptech.glide.request.transition.TransitionFactory;
import com.bumptech.glide.util.Preconditions;

public abstract class TransitionOptions<CHILD extends TransitionOptions<CHILD, TranscodeType>, TranscodeType> implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private TransitionFactory<? super TranscodeType> f2117a = NoTransition.b();

    private CHILD b() {
        return this;
    }

    public final CHILD a(TransitionFactory<? super TranscodeType> transitionFactory) {
        Preconditions.a(transitionFactory);
        this.f2117a = transitionFactory;
        b();
        return this;
    }

    public final CHILD clone() {
        try {
            return (TransitionOptions) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final TransitionFactory<? super TranscodeType> a() {
        return this.f2117a;
    }
}
