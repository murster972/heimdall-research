package com.bumptech.glide;

import android.content.Context;
import androidx.collection.ArrayMap;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideExperiments;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPoolAdapter;
import com.bumptech.glide.load.engine.bitmap_recycle.LruArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.load.engine.executor.GlideExecutor;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.DefaultConnectivityMonitorFactory;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class GlideBuilder {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Class<?>, TransitionOptions<?, ?>> f2105a = new ArrayMap();
    private final GlideExperiments.Builder b = new GlideExperiments.Builder();
    private Engine c;
    private BitmapPool d;
    private ArrayPool e;
    private MemoryCache f;
    private GlideExecutor g;
    private GlideExecutor h;
    private DiskCache.Factory i;
    private MemorySizeCalculator j;
    private ConnectivityMonitorFactory k;
    private int l = 4;
    private Glide.RequestOptionsFactory m = new Glide.RequestOptionsFactory(this) {
        public RequestOptions build() {
            return new RequestOptions();
        }
    };
    private RequestManagerRetriever.RequestManagerFactory n;
    private GlideExecutor o;
    private boolean p;
    private List<RequestListener<Object>> q;

    static final class EnableImageDecoderForBitmaps implements GlideExperiments.Experiment {
        EnableImageDecoderForBitmaps() {
        }
    }

    public static final class LogRequestOrigins implements GlideExperiments.Experiment {
    }

    public static final class WaitForFramesAfterTrimMemory implements GlideExperiments.Experiment {
        private WaitForFramesAfterTrimMemory() {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RequestManagerRetriever.RequestManagerFactory requestManagerFactory) {
        this.n = requestManagerFactory;
    }

    /* access modifiers changed from: package-private */
    public Glide a(Context context) {
        if (this.g == null) {
            this.g = GlideExecutor.g();
        }
        if (this.h == null) {
            this.h = GlideExecutor.e();
        }
        if (this.o == null) {
            this.o = GlideExecutor.c();
        }
        if (this.j == null) {
            this.j = new MemorySizeCalculator.Builder(context).a();
        }
        if (this.k == null) {
            this.k = new DefaultConnectivityMonitorFactory();
        }
        if (this.d == null) {
            int b2 = this.j.b();
            if (b2 > 0) {
                this.d = new LruBitmapPool((long) b2);
            } else {
                this.d = new BitmapPoolAdapter();
            }
        }
        if (this.e == null) {
            this.e = new LruArrayPool(this.j.a());
        }
        if (this.f == null) {
            this.f = new LruResourceCache((long) this.j.c());
        }
        if (this.i == null) {
            this.i = new InternalCacheDiskCacheFactory(context);
        }
        if (this.c == null) {
            this.c = new Engine(this.f, this.i, this.h, this.g, GlideExecutor.h(), this.o, this.p);
        }
        List<RequestListener<Object>> list = this.q;
        if (list == null) {
            this.q = Collections.emptyList();
        } else {
            this.q = Collections.unmodifiableList(list);
        }
        GlideExperiments a2 = this.b.a();
        return new Glide(context, this.c, this.f, this.d, this.e, new RequestManagerRetriever(this.n, a2), this.k, this.l, this.m, this.f2105a, this.q, a2);
    }
}
