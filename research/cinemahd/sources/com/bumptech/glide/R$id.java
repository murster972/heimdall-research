package com.bumptech.glide;

public final class R$id {
    public static final int action_container = 2131296320;
    public static final int action_divider = 2131296323;
    public static final int action_image = 2131296324;
    public static final int action_text = 2131296334;
    public static final int actions = 2131296335;
    public static final int async = 2131296384;
    public static final int blocking = 2131296406;
    public static final int bottom = 2131296408;
    public static final int chronometer = 2131296502;
    public static final int end = 2131296587;
    public static final int forever = 2131296650;
    public static final int glide_custom_view_target_tag = 2131296658;
    public static final int icon = 2131296680;
    public static final int icon_group = 2131296682;

    /* renamed from: info  reason: collision with root package name */
    public static final int f2111info = 2131296704;
    public static final int italic = 2131296711;
    public static final int left = 2131296742;
    public static final int line1 = 2131296747;
    public static final int line3 = 2131296748;
    public static final int none = 2131296922;
    public static final int normal = 2131296923;
    public static final int notification_background = 2131296925;
    public static final int notification_main_column = 2131296926;
    public static final int notification_main_column_container = 2131296927;
    public static final int right = 2131296985;
    public static final int right_icon = 2131296986;
    public static final int right_side = 2131296987;
    public static final int start = 2131297066;
    public static final int tag_transition_group = 2131297103;
    public static final int tag_unhandled_key_event_manager = 2131297104;
    public static final int tag_unhandled_key_listeners = 2131297105;
    public static final int text = 2131297106;
    public static final int text2 = 2131297108;
    public static final int time = 2131297150;
    public static final int title = 2131297151;
    public static final int top = 2131297165;

    private R$id() {
    }
}
