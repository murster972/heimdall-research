package com.bumptech.glide.gifdecoder;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public interface GifDecoder {

    public interface BitmapProvider {
        Bitmap a(int i, int i2, Bitmap.Config config);

        void a(Bitmap bitmap);

        void a(byte[] bArr);

        void a(int[] iArr);

        byte[] a(int i);

        int[] b(int i);
    }

    Bitmap a();

    void a(Bitmap.Config config);

    void b();

    int c();

    void clear();

    ByteBuffer d();

    int e();

    void f();

    int g();

    int h();
}
