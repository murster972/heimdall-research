package com.bumptech.glide.gifdecoder;

import java.util.ArrayList;
import java.util.List;

public class GifHeader {

    /* renamed from: a  reason: collision with root package name */
    int[] f2127a = null;
    int b = 0;
    int c = 0;
    GifFrame d;
    final List<GifFrame> e = new ArrayList();
    int f;
    int g;
    boolean h;
    int i;
    int j;
    int k;
    int l;
    int m;

    public int a() {
        return this.g;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.b;
    }

    public int d() {
        return this.f;
    }
}
