package com.bumptech.glide.gifdecoder;

import android.graphics.Bitmap;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.facebook.imageutils.JfifUtil;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

public class StandardGifDecoder implements GifDecoder {
    private static final String u = "StandardGifDecoder";

    /* renamed from: a  reason: collision with root package name */
    private int[] f2129a;
    private final int[] b;
    private final GifDecoder.BitmapProvider c;
    private ByteBuffer d;
    private byte[] e;
    private short[] f;
    private byte[] g;
    private byte[] h;
    private byte[] i;
    private int[] j;
    private int k;
    private GifHeader l;
    private Bitmap m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private int r;
    private Boolean s;
    private Bitmap.Config t;

    public StandardGifDecoder(GifDecoder.BitmapProvider bitmapProvider, GifHeader gifHeader, ByteBuffer byteBuffer, int i2) {
        this(bitmapProvider);
        a(gifHeader, byteBuffer, i2);
    }

    private Bitmap i() {
        Boolean bool = this.s;
        Bitmap a2 = this.c.a(this.r, this.q, (bool == null || bool.booleanValue()) ? Bitmap.Config.ARGB_8888 : this.t);
        a2.setHasAlpha(true);
        return a2;
    }

    private int j() {
        int k2 = k();
        if (k2 <= 0) {
            return k2;
        }
        ByteBuffer byteBuffer = this.d;
        byteBuffer.get(this.e, 0, Math.min(k2, byteBuffer.remaining()));
        return k2;
    }

    private int k() {
        return this.d.get() & 255;
    }

    public int a(int i2) {
        if (i2 >= 0) {
            GifHeader gifHeader = this.l;
            if (i2 < gifHeader.c) {
                return gifHeader.e.get(i2).i;
            }
        }
        return -1;
    }

    public void b() {
        this.k = (this.k + 1) % this.l.c;
    }

    public int c() {
        return this.l.c;
    }

    public void clear() {
        this.l = null;
        byte[] bArr = this.i;
        if (bArr != null) {
            this.c.a(bArr);
        }
        int[] iArr = this.j;
        if (iArr != null) {
            this.c.a(iArr);
        }
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.c.a(bitmap);
        }
        this.m = null;
        this.d = null;
        this.s = null;
        byte[] bArr2 = this.e;
        if (bArr2 != null) {
            this.c.a(bArr2);
        }
    }

    public ByteBuffer d() {
        return this.d;
    }

    public int e() {
        int i2;
        if (this.l.c <= 0 || (i2 = this.k) < 0) {
            return 0;
        }
        return a(i2);
    }

    public void f() {
        this.k = -1;
    }

    public int g() {
        return this.k;
    }

    public int h() {
        return this.d.limit() + this.i.length + (this.j.length * 4);
    }

    private void b(GifFrame gifFrame) {
        GifFrame gifFrame2 = gifFrame;
        int[] iArr = this.j;
        int i2 = gifFrame2.d;
        int i3 = gifFrame2.b;
        int i4 = gifFrame2.c;
        int i5 = gifFrame2.f2126a;
        boolean z = this.k == 0;
        int i6 = this.r;
        byte[] bArr = this.i;
        int[] iArr2 = this.f2129a;
        int i7 = 0;
        byte b2 = -1;
        while (i7 < i2) {
            int i8 = (i7 + i3) * i6;
            int i9 = i8 + i5;
            int i10 = i9 + i4;
            int i11 = i8 + i6;
            if (i11 < i10) {
                i10 = i11;
            }
            int i12 = gifFrame2.c * i7;
            int i13 = i9;
            while (i13 < i10) {
                byte b3 = bArr[i12];
                byte b4 = b3 & 255;
                if (b4 != b2) {
                    int i14 = iArr2[b4];
                    if (i14 != 0) {
                        iArr[i13] = i14;
                    } else {
                        b2 = b3;
                    }
                }
                i12++;
                i13++;
                GifFrame gifFrame3 = gifFrame;
            }
            i7++;
            gifFrame2 = gifFrame;
        }
        Boolean bool = this.s;
        this.s = Boolean.valueOf((bool != null && bool.booleanValue()) || (this.s == null && z && b2 != -1));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v5, resolved type: byte} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r4v16, types: [short] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(com.bumptech.glide.gifdecoder.GifFrame r30) {
        /*
            r29 = this;
            r0 = r29
            r1 = r30
            if (r1 == 0) goto L_0x000d
            java.nio.ByteBuffer r2 = r0.d
            int r3 = r1.j
            r2.position(r3)
        L_0x000d:
            if (r1 != 0) goto L_0x0016
            com.bumptech.glide.gifdecoder.GifHeader r1 = r0.l
            int r2 = r1.f
            int r1 = r1.g
            goto L_0x001a
        L_0x0016:
            int r2 = r1.c
            int r1 = r1.d
        L_0x001a:
            int r2 = r2 * r1
            byte[] r1 = r0.i
            if (r1 == 0) goto L_0x0023
            int r1 = r1.length
            if (r1 >= r2) goto L_0x002b
        L_0x0023:
            com.bumptech.glide.gifdecoder.GifDecoder$BitmapProvider r1 = r0.c
            byte[] r1 = r1.a((int) r2)
            r0.i = r1
        L_0x002b:
            byte[] r1 = r0.i
            short[] r3 = r0.f
            r4 = 4096(0x1000, float:5.74E-42)
            if (r3 != 0) goto L_0x0037
            short[] r3 = new short[r4]
            r0.f = r3
        L_0x0037:
            short[] r3 = r0.f
            byte[] r5 = r0.g
            if (r5 != 0) goto L_0x0041
            byte[] r5 = new byte[r4]
            r0.g = r5
        L_0x0041:
            byte[] r5 = r0.g
            byte[] r6 = r0.h
            if (r6 != 0) goto L_0x004d
            r6 = 4097(0x1001, float:5.741E-42)
            byte[] r6 = new byte[r6]
            r0.h = r6
        L_0x004d:
            byte[] r6 = r0.h
            int r7 = r29.k()
            r8 = 1
            int r9 = r8 << r7
            int r10 = r9 + 1
            int r11 = r9 + 2
            int r7 = r7 + r8
            int r12 = r8 << r7
            int r12 = r12 - r8
            r13 = 0
            r14 = 0
        L_0x0060:
            if (r14 >= r9) goto L_0x006a
            r3[r14] = r13
            byte r15 = (byte) r14
            r5[r14] = r15
            int r14 = r14 + 1
            goto L_0x0060
        L_0x006a:
            byte[] r14 = r0.e
            r15 = -1
            r26 = r7
            r24 = r11
            r25 = r12
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = -1
            r22 = 0
            r23 = 0
        L_0x0083:
            if (r13 >= r2) goto L_0x0159
            if (r16 != 0) goto L_0x0094
            int r16 = r29.j()
            if (r16 > 0) goto L_0x0092
            r3 = 3
            r0.o = r3
            goto L_0x0159
        L_0x0092:
            r19 = 0
        L_0x0094:
            byte r4 = r14[r19]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r17
            int r18 = r18 + r4
            int r17 = r17 + 8
            int r19 = r19 + 1
            int r16 = r16 + -1
            r4 = r17
            r8 = r21
            r28 = r22
            r27 = r24
            r21 = r20
            r20 = r13
            r13 = r26
        L_0x00b0:
            if (r4 < r13) goto L_0x013d
            r15 = r18 & r25
            int r18 = r18 >> r13
            int r4 = r4 - r13
            if (r15 != r9) goto L_0x00c1
            r13 = r7
            r27 = r11
            r25 = r12
            r8 = -1
        L_0x00bf:
            r15 = -1
            goto L_0x00b0
        L_0x00c1:
            if (r15 != r10) goto L_0x00d6
            r17 = r4
            r26 = r13
            r13 = r20
            r20 = r21
            r24 = r27
            r22 = r28
            r4 = 4096(0x1000, float:5.74E-42)
            r15 = -1
            r21 = r8
            r8 = 1
            goto L_0x0083
        L_0x00d6:
            r0 = -1
            if (r8 != r0) goto L_0x00e7
            byte r8 = r5[r15]
            r1[r21] = r8
            int r21 = r21 + 1
            int r20 = r20 + 1
            r0 = r29
            r8 = r15
            r28 = r8
            goto L_0x00bf
        L_0x00e7:
            r0 = r27
            r24 = r4
            if (r15 < r0) goto L_0x00f6
            r4 = r28
            byte r4 = (byte) r4
            r6[r23] = r4
            int r23 = r23 + 1
            r4 = r8
            goto L_0x00f7
        L_0x00f6:
            r4 = r15
        L_0x00f7:
            if (r4 < r9) goto L_0x0102
            byte r26 = r5[r4]
            r6[r23] = r26
            int r23 = r23 + 1
            short r4 = r3[r4]
            goto L_0x00f7
        L_0x0102:
            byte r4 = r5[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r26 = r7
            byte r7 = (byte) r4
            r1[r21] = r7
        L_0x010b:
            int r21 = r21 + 1
            int r20 = r20 + 1
            if (r23 <= 0) goto L_0x0118
            int r23 = r23 + -1
            byte r27 = r6[r23]
            r1[r21] = r27
            goto L_0x010b
        L_0x0118:
            r27 = r4
            r4 = 4096(0x1000, float:5.74E-42)
            if (r0 >= r4) goto L_0x012f
            short r8 = (short) r8
            r3[r0] = r8
            r5[r0] = r7
            int r0 = r0 + 1
            r7 = r0 & r25
            if (r7 != 0) goto L_0x012f
            if (r0 >= r4) goto L_0x012f
            int r13 = r13 + 1
            int r25 = r25 + r0
        L_0x012f:
            r8 = r15
            r4 = r24
            r7 = r26
            r28 = r27
            r15 = -1
            r27 = r0
            r0 = r29
            goto L_0x00b0
        L_0x013d:
            r24 = r4
            r0 = r27
            r15 = r28
            r26 = r13
            r22 = r15
            r13 = r20
            r20 = r21
            r17 = r24
            r4 = 4096(0x1000, float:5.74E-42)
            r15 = -1
            r24 = r0
            r21 = r8
            r8 = 1
            r0 = r29
            goto L_0x0083
        L_0x0159:
            r13 = r20
            r0 = 0
            java.util.Arrays.fill(r1, r13, r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.gifdecoder.StandardGifDecoder.c(com.bumptech.glide.gifdecoder.GifFrame):void");
    }

    public StandardGifDecoder(GifDecoder.BitmapProvider bitmapProvider) {
        this.b = new int[256];
        this.t = Bitmap.Config.ARGB_8888;
        this.c = bitmapProvider;
        this.l = new GifHeader();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f7, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.graphics.Bitmap a() {
        /*
            r8 = this;
            monitor-enter(r8)
            com.bumptech.glide.gifdecoder.GifHeader r0 = r8.l     // Catch:{ all -> 0x00f8 }
            int r0 = r0.c     // Catch:{ all -> 0x00f8 }
            r1 = 3
            r2 = 1
            if (r0 <= 0) goto L_0x000d
            int r0 = r8.k     // Catch:{ all -> 0x00f8 }
            if (r0 >= 0) goto L_0x003b
        L_0x000d:
            java.lang.String r0 = u     // Catch:{ all -> 0x00f8 }
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0039
            java.lang.String r0 = u     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
            r3.<init>()     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = "Unable to decode frame, frameCount="
            r3.append(r4)     // Catch:{ all -> 0x00f8 }
            com.bumptech.glide.gifdecoder.GifHeader r4 = r8.l     // Catch:{ all -> 0x00f8 }
            int r4 = r4.c     // Catch:{ all -> 0x00f8 }
            r3.append(r4)     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = ", framePointer="
            r3.append(r4)     // Catch:{ all -> 0x00f8 }
            int r4 = r8.k     // Catch:{ all -> 0x00f8 }
            r3.append(r4)     // Catch:{ all -> 0x00f8 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00f8 }
            android.util.Log.d(r0, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0039:
            r8.o = r2     // Catch:{ all -> 0x00f8 }
        L_0x003b:
            int r0 = r8.o     // Catch:{ all -> 0x00f8 }
            r3 = 0
            if (r0 == r2) goto L_0x00d6
            int r0 = r8.o     // Catch:{ all -> 0x00f8 }
            r4 = 2
            if (r0 != r4) goto L_0x0047
            goto L_0x00d6
        L_0x0047:
            r0 = 0
            r8.o = r0     // Catch:{ all -> 0x00f8 }
            byte[] r5 = r8.e     // Catch:{ all -> 0x00f8 }
            if (r5 != 0) goto L_0x0058
            com.bumptech.glide.gifdecoder.GifDecoder$BitmapProvider r5 = r8.c     // Catch:{ all -> 0x00f8 }
            r6 = 255(0xff, float:3.57E-43)
            byte[] r5 = r5.a((int) r6)     // Catch:{ all -> 0x00f8 }
            r8.e = r5     // Catch:{ all -> 0x00f8 }
        L_0x0058:
            com.bumptech.glide.gifdecoder.GifHeader r5 = r8.l     // Catch:{ all -> 0x00f8 }
            java.util.List<com.bumptech.glide.gifdecoder.GifFrame> r5 = r5.e     // Catch:{ all -> 0x00f8 }
            int r6 = r8.k     // Catch:{ all -> 0x00f8 }
            java.lang.Object r5 = r5.get(r6)     // Catch:{ all -> 0x00f8 }
            com.bumptech.glide.gifdecoder.GifFrame r5 = (com.bumptech.glide.gifdecoder.GifFrame) r5     // Catch:{ all -> 0x00f8 }
            int r6 = r8.k     // Catch:{ all -> 0x00f8 }
            int r6 = r6 - r2
            if (r6 < 0) goto L_0x0074
            com.bumptech.glide.gifdecoder.GifHeader r7 = r8.l     // Catch:{ all -> 0x00f8 }
            java.util.List<com.bumptech.glide.gifdecoder.GifFrame> r7 = r7.e     // Catch:{ all -> 0x00f8 }
            java.lang.Object r6 = r7.get(r6)     // Catch:{ all -> 0x00f8 }
            com.bumptech.glide.gifdecoder.GifFrame r6 = (com.bumptech.glide.gifdecoder.GifFrame) r6     // Catch:{ all -> 0x00f8 }
            goto L_0x0075
        L_0x0074:
            r6 = r3
        L_0x0075:
            int[] r7 = r5.k     // Catch:{ all -> 0x00f8 }
            if (r7 == 0) goto L_0x007c
            int[] r7 = r5.k     // Catch:{ all -> 0x00f8 }
            goto L_0x0080
        L_0x007c:
            com.bumptech.glide.gifdecoder.GifHeader r7 = r8.l     // Catch:{ all -> 0x00f8 }
            int[] r7 = r7.f2127a     // Catch:{ all -> 0x00f8 }
        L_0x0080:
            r8.f2129a = r7     // Catch:{ all -> 0x00f8 }
            int[] r7 = r8.f2129a     // Catch:{ all -> 0x00f8 }
            if (r7 != 0) goto L_0x00aa
            java.lang.String r0 = u     // Catch:{ all -> 0x00f8 }
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x00a6
            java.lang.String r0 = u     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
            r1.<init>()     // Catch:{ all -> 0x00f8 }
            java.lang.String r4 = "No valid color table found for frame #"
            r1.append(r4)     // Catch:{ all -> 0x00f8 }
            int r4 = r8.k     // Catch:{ all -> 0x00f8 }
            r1.append(r4)     // Catch:{ all -> 0x00f8 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00f8 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00f8 }
        L_0x00a6:
            r8.o = r2     // Catch:{ all -> 0x00f8 }
            monitor-exit(r8)
            return r3
        L_0x00aa:
            boolean r1 = r5.f     // Catch:{ all -> 0x00f8 }
            if (r1 == 0) goto L_0x00d0
            int[] r1 = r8.f2129a     // Catch:{ all -> 0x00f8 }
            int[] r3 = r8.b     // Catch:{ all -> 0x00f8 }
            int[] r7 = r8.f2129a     // Catch:{ all -> 0x00f8 }
            int r7 = r7.length     // Catch:{ all -> 0x00f8 }
            java.lang.System.arraycopy(r1, r0, r3, r0, r7)     // Catch:{ all -> 0x00f8 }
            int[] r1 = r8.b     // Catch:{ all -> 0x00f8 }
            r8.f2129a = r1     // Catch:{ all -> 0x00f8 }
            int[] r1 = r8.f2129a     // Catch:{ all -> 0x00f8 }
            int r3 = r5.h     // Catch:{ all -> 0x00f8 }
            r1[r3] = r0     // Catch:{ all -> 0x00f8 }
            int r0 = r5.g     // Catch:{ all -> 0x00f8 }
            if (r0 != r4) goto L_0x00d0
            int r0 = r8.k     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x00d0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x00f8 }
            r8.s = r0     // Catch:{ all -> 0x00f8 }
        L_0x00d0:
            android.graphics.Bitmap r0 = r8.a(r5, r6)     // Catch:{ all -> 0x00f8 }
            monitor-exit(r8)
            return r0
        L_0x00d6:
            java.lang.String r0 = u     // Catch:{ all -> 0x00f8 }
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x00f6
            java.lang.String r0 = u     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
            r1.<init>()     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = "Unable to decode frame, status="
            r1.append(r2)     // Catch:{ all -> 0x00f8 }
            int r2 = r8.o     // Catch:{ all -> 0x00f8 }
            r1.append(r2)     // Catch:{ all -> 0x00f8 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00f8 }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00f8 }
        L_0x00f6:
            monitor-exit(r8)
            return r3
        L_0x00f8:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.gifdecoder.StandardGifDecoder.a():android.graphics.Bitmap");
    }

    public synchronized void a(GifHeader gifHeader, ByteBuffer byteBuffer, int i2) {
        if (i2 > 0) {
            int highestOneBit = Integer.highestOneBit(i2);
            this.o = 0;
            this.l = gifHeader;
            this.k = -1;
            this.d = byteBuffer.asReadOnlyBuffer();
            this.d.position(0);
            this.d.order(ByteOrder.LITTLE_ENDIAN);
            this.n = false;
            Iterator<GifFrame> it2 = gifHeader.e.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (it2.next().g == 3) {
                        this.n = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            this.p = highestOneBit;
            this.r = gifHeader.f / highestOneBit;
            this.q = gifHeader.g / highestOneBit;
            this.i = this.c.a(gifHeader.f * gifHeader.g);
            this.j = this.c.b(this.r * this.q);
        } else {
            throw new IllegalArgumentException("Sample size must be >=0, not: " + i2);
        }
    }

    public void a(Bitmap.Config config) {
        if (config == Bitmap.Config.ARGB_8888 || config == Bitmap.Config.RGB_565) {
            this.t = config;
            return;
        }
        throw new IllegalArgumentException("Unsupported format: " + config + ", must be one of " + Bitmap.Config.ARGB_8888 + " or " + Bitmap.Config.RGB_565);
    }

    private Bitmap a(GifFrame gifFrame, GifFrame gifFrame2) {
        int i2;
        int i3;
        Bitmap bitmap;
        int[] iArr = this.j;
        int i4 = 0;
        if (gifFrame2 == null) {
            Bitmap bitmap2 = this.m;
            if (bitmap2 != null) {
                this.c.a(bitmap2);
            }
            this.m = null;
            Arrays.fill(iArr, 0);
        }
        if (gifFrame2 != null && gifFrame2.g == 3 && this.m == null) {
            Arrays.fill(iArr, 0);
        }
        if (gifFrame2 != null && (i3 = gifFrame2.g) > 0) {
            if (i3 == 2) {
                if (!gifFrame.f) {
                    GifHeader gifHeader = this.l;
                    int i5 = gifHeader.l;
                    if (gifFrame.k == null || gifHeader.j != gifFrame.h) {
                        i4 = i5;
                    }
                }
                int i6 = gifFrame2.d;
                int i7 = this.p;
                int i8 = i6 / i7;
                int i9 = gifFrame2.b / i7;
                int i10 = gifFrame2.c / i7;
                int i11 = gifFrame2.f2126a / i7;
                int i12 = this.r;
                int i13 = (i9 * i12) + i11;
                int i14 = (i8 * i12) + i13;
                while (i13 < i14) {
                    int i15 = i13 + i10;
                    for (int i16 = i13; i16 < i15; i16++) {
                        iArr[i16] = i4;
                    }
                    i13 += this.r;
                }
            } else if (i3 == 3 && (bitmap = this.m) != null) {
                int i17 = this.r;
                bitmap.getPixels(iArr, 0, i17, 0, 0, i17, this.q);
            }
        }
        c(gifFrame);
        if (gifFrame.e || this.p != 1) {
            a(gifFrame);
        } else {
            b(gifFrame);
        }
        if (this.n && ((i2 = gifFrame.g) == 0 || i2 == 1)) {
            if (this.m == null) {
                this.m = i();
            }
            Bitmap bitmap3 = this.m;
            int i18 = this.r;
            bitmap3.setPixels(iArr, 0, i18, 0, 0, i18, this.q);
        }
        Bitmap i19 = i();
        int i20 = this.r;
        i19.setPixels(iArr, 0, i20, 0, 0, i20, this.q);
        return i19;
    }

    private void a(GifFrame gifFrame) {
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        GifFrame gifFrame2 = gifFrame;
        int[] iArr = this.j;
        int i7 = gifFrame2.d;
        int i8 = this.p;
        int i9 = i7 / i8;
        int i10 = gifFrame2.b / i8;
        int i11 = gifFrame2.c / i8;
        int i12 = gifFrame2.f2126a / i8;
        boolean z2 = true;
        boolean z3 = this.k == 0;
        int i13 = this.p;
        int i14 = this.r;
        int i15 = this.q;
        byte[] bArr = this.i;
        int[] iArr2 = this.f2129a;
        Boolean bool = this.s;
        int i16 = 0;
        int i17 = 0;
        int i18 = 1;
        int i19 = 8;
        while (i16 < i9) {
            Boolean bool2 = z2;
            if (gifFrame2.e) {
                if (i17 >= i9) {
                    i2 = i9;
                    i6 = i18 + 1;
                    if (i6 == 2) {
                        i17 = 4;
                    } else if (i6 == 3) {
                        i17 = 2;
                        i19 = 4;
                    } else if (i6 == 4) {
                        i17 = 1;
                        i19 = 2;
                    }
                } else {
                    i2 = i9;
                    i6 = i18;
                }
                i3 = i17 + i19;
                i18 = i6;
            } else {
                i2 = i9;
                i3 = i17;
                i17 = i16;
            }
            int i20 = i17 + i10;
            boolean z4 = i13 == 1;
            if (i20 < i15) {
                int i21 = i20 * i14;
                int i22 = i21 + i12;
                int i23 = i22 + i11;
                int i24 = i21 + i14;
                if (i24 < i23) {
                    i23 = i24;
                }
                i4 = i10;
                int i25 = i16 * i13 * gifFrame2.c;
                if (z4) {
                    int i26 = i22;
                    while (i26 < i23) {
                        int i27 = i11;
                        int i28 = iArr2[bArr[i25] & 255];
                        if (i28 != 0) {
                            iArr[i26] = i28;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i25 += i13;
                        i26++;
                        i11 = i27;
                    }
                } else {
                    i5 = i11;
                    int i29 = ((i23 - i22) * i13) + i25;
                    int i30 = i22;
                    while (i30 < i23) {
                        int i31 = i23;
                        int a2 = a(i25, i29, gifFrame2.c);
                        if (a2 != 0) {
                            iArr[i30] = a2;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i25 += i13;
                        i30++;
                        i23 = i31;
                    }
                    i16++;
                    i17 = i3;
                    i11 = i5;
                    z2 = bool2;
                    i9 = i2;
                    i10 = i4;
                }
            } else {
                i4 = i10;
            }
            i5 = i11;
            i16++;
            i17 = i3;
            i11 = i5;
            z2 = bool2;
            i9 = i2;
            i10 = i4;
        }
        if (this.s == null) {
            if (bool == null) {
                z = false;
            } else {
                z = bool.booleanValue();
            }
            this.s = Boolean.valueOf(z);
        }
    }

    private int a(int i2, int i3, int i4) {
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        for (int i10 = i2; i10 < this.p + i2; i10++) {
            byte[] bArr = this.i;
            if (i10 >= bArr.length || i10 >= i3) {
                break;
            }
            int i11 = this.f2129a[bArr[i10] & 255];
            if (i11 != 0) {
                i5 += (i11 >> 24) & JfifUtil.MARKER_FIRST_BYTE;
                i6 += (i11 >> 16) & JfifUtil.MARKER_FIRST_BYTE;
                i7 += (i11 >> 8) & JfifUtil.MARKER_FIRST_BYTE;
                i8 += i11 & JfifUtil.MARKER_FIRST_BYTE;
                i9++;
            }
        }
        int i12 = i2 + i4;
        for (int i13 = i12; i13 < this.p + i12; i13++) {
            byte[] bArr2 = this.i;
            if (i13 >= bArr2.length || i13 >= i3) {
                break;
            }
            int i14 = this.f2129a[bArr2[i13] & 255];
            if (i14 != 0) {
                i5 += (i14 >> 24) & JfifUtil.MARKER_FIRST_BYTE;
                i6 += (i14 >> 16) & JfifUtil.MARKER_FIRST_BYTE;
                i7 += (i14 >> 8) & JfifUtil.MARKER_FIRST_BYTE;
                i8 += i14 & JfifUtil.MARKER_FIRST_BYTE;
                i9++;
            }
        }
        if (i9 == 0) {
            return 0;
        }
        return ((i5 / i9) << 24) | ((i6 / i9) << 16) | ((i7 / i9) << 8) | (i8 / i9);
    }
}
