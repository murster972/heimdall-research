package com.bumptech.glide.gifdecoder;

import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class GifHeaderParser {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f2128a = new byte[256];
    private ByteBuffer b;
    private GifHeader c;
    private int d = 0;

    private boolean c() {
        return this.c.b != 0;
    }

    private int d() {
        try {
            return this.b.get() & 255;
        } catch (Exception unused) {
            this.c.b = 1;
            return 0;
        }
    }

    private void e() {
        this.c.d.f2126a = l();
        this.c.d.b = l();
        this.c.d.c = l();
        this.c.d.d = l();
        int d2 = d();
        boolean z = false;
        boolean z2 = (d2 & 128) != 0;
        int pow = (int) Math.pow(2.0d, (double) ((d2 & 7) + 1));
        GifFrame gifFrame = this.c.d;
        if ((d2 & 64) != 0) {
            z = true;
        }
        gifFrame.e = z;
        if (z2) {
            this.c.d.k = a(pow);
        } else {
            this.c.d.k = null;
        }
        this.c.d.j = this.b.position();
        o();
        if (!c()) {
            GifHeader gifHeader = this.c;
            gifHeader.c++;
            gifHeader.e.add(gifHeader.d);
        }
    }

    private void f() {
        this.d = d();
        if (this.d > 0) {
            int i = 0;
            int i2 = 0;
            while (i < this.d) {
                try {
                    i2 = this.d - i;
                    this.b.get(this.f2128a, i, i2);
                    i += i2;
                } catch (Exception e) {
                    if (Log.isLoggable("GifHeaderParser", 3)) {
                        Log.d("GifHeaderParser", "Error Reading Block n: " + i + " count: " + i2 + " blockSize: " + this.d, e);
                    }
                    this.c.b = 1;
                    return;
                }
            }
        }
    }

    private void g() {
        b(Integer.MAX_VALUE);
    }

    private void h() {
        d();
        int d2 = d();
        GifFrame gifFrame = this.c.d;
        gifFrame.g = (d2 & 28) >> 2;
        boolean z = true;
        if (gifFrame.g == 0) {
            gifFrame.g = 1;
        }
        GifFrame gifFrame2 = this.c.d;
        if ((d2 & 1) == 0) {
            z = false;
        }
        gifFrame2.f = z;
        int l = l();
        if (l < 2) {
            l = 10;
        }
        GifFrame gifFrame3 = this.c.d;
        gifFrame3.i = l * 10;
        gifFrame3.h = d();
        d();
    }

    private void i() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append((char) d());
        }
        if (!sb.toString().startsWith("GIF")) {
            this.c.b = 1;
            return;
        }
        j();
        if (this.c.h && !c()) {
            GifHeader gifHeader = this.c;
            gifHeader.f2127a = a(gifHeader.i);
            GifHeader gifHeader2 = this.c;
            gifHeader2.l = gifHeader2.f2127a[gifHeader2.j];
        }
    }

    private void j() {
        this.c.f = l();
        this.c.g = l();
        int d2 = d();
        this.c.h = (d2 & 128) != 0;
        this.c.i = (int) Math.pow(2.0d, (double) ((d2 & 7) + 1));
        this.c.j = d();
        this.c.k = d();
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void k() {
        /*
            r3 = this;
        L_0x0000:
            r3.f()
            byte[] r0 = r3.f2128a
            r1 = 0
            byte r1 = r0[r1]
            r2 = 1
            if (r1 != r2) goto L_0x001b
            byte r1 = r0[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r2 = 2
            byte r0 = r0[r2]
            r0 = r0 & 255(0xff, float:3.57E-43)
            com.bumptech.glide.gifdecoder.GifHeader r2 = r3.c
            int r0 = r0 << 8
            r0 = r0 | r1
            r2.m = r0
        L_0x001b:
            int r0 = r3.d
            if (r0 <= 0) goto L_0x0025
            boolean r0 = r3.c()
            if (r0 == 0) goto L_0x0000
        L_0x0025:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.gifdecoder.GifHeaderParser.k():void");
    }

    private int l() {
        return this.b.getShort();
    }

    private void m() {
        this.b = null;
        Arrays.fill(this.f2128a, (byte) 0);
        this.c = new GifHeader();
        this.d = 0;
    }

    private void n() {
        int d2;
        do {
            d2 = d();
            this.b.position(Math.min(this.b.position() + d2, this.b.limit()));
        } while (d2 > 0);
    }

    private void o() {
        d();
        n();
    }

    public GifHeaderParser a(ByteBuffer byteBuffer) {
        m();
        this.b = byteBuffer.asReadOnlyBuffer();
        this.b.position(0);
        this.b.order(ByteOrder.LITTLE_ENDIAN);
        return this;
    }

    public GifHeader b() {
        if (this.b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (c()) {
            return this.c;
        } else {
            i();
            if (!c()) {
                g();
                GifHeader gifHeader = this.c;
                if (gifHeader.c < 0) {
                    gifHeader.b = 1;
                }
            }
            return this.c;
        }
    }

    public void a() {
        this.b = null;
        this.c = null;
    }

    private int[] a(int i) {
        byte[] bArr = new byte[(i * 3)];
        int[] iArr = null;
        try {
            this.b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i2 < i) {
                int i4 = i3 + 1;
                int i5 = i4 + 1;
                int i6 = i5 + 1;
                int i7 = i2 + 1;
                iArr[i2] = ((bArr[i3] & 255) << 16) | -16777216 | ((bArr[i4] & 255) << 8) | (bArr[i5] & 255);
                i3 = i6;
                i2 = i7;
            }
        } catch (BufferUnderflowException e) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", e);
            }
            this.c.b = 1;
        }
        return iArr;
    }

    private void b(int i) {
        boolean z = false;
        while (!z && !c() && this.c.c <= i) {
            int d2 = d();
            if (d2 == 33) {
                int d3 = d();
                if (d3 == 1) {
                    n();
                } else if (d3 == 249) {
                    this.c.d = new GifFrame();
                    h();
                } else if (d3 == 254) {
                    n();
                } else if (d3 != 255) {
                    n();
                } else {
                    f();
                    StringBuilder sb = new StringBuilder();
                    for (int i2 = 0; i2 < 11; i2++) {
                        sb.append((char) this.f2128a[i2]);
                    }
                    if (sb.toString().equals("NETSCAPE2.0")) {
                        k();
                    } else {
                        n();
                    }
                }
            } else if (d2 == 44) {
                GifHeader gifHeader = this.c;
                if (gifHeader.d == null) {
                    gifHeader.d = new GifFrame();
                }
                e();
            } else if (d2 != 59) {
                this.c.b = 1;
            } else {
                z = true;
            }
        }
    }
}
