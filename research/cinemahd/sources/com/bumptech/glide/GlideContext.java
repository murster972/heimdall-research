package com.bumptech.glide;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ImageViewTargetFactory;
import com.bumptech.glide.request.target.ViewTarget;
import java.util.List;
import java.util.Map;

public class GlideContext extends ContextWrapper {
    static final TransitionOptions<?, ?> k = new GenericTransitionOptions();

    /* renamed from: a  reason: collision with root package name */
    private final ArrayPool f2106a;
    private final Registry b;
    private final ImageViewTargetFactory c;
    private final Glide.RequestOptionsFactory d;
    private final List<RequestListener<Object>> e;
    private final Map<Class<?>, TransitionOptions<?, ?>> f;
    private final Engine g;
    private final GlideExperiments h;
    private final int i;
    private RequestOptions j;

    public GlideContext(Context context, ArrayPool arrayPool, Registry registry, ImageViewTargetFactory imageViewTargetFactory, Glide.RequestOptionsFactory requestOptionsFactory, Map<Class<?>, TransitionOptions<?, ?>> map, List<RequestListener<Object>> list, Engine engine, GlideExperiments glideExperiments, int i2) {
        super(context.getApplicationContext());
        this.f2106a = arrayPool;
        this.b = registry;
        this.c = imageViewTargetFactory;
        this.d = requestOptionsFactory;
        this.e = list;
        this.f = map;
        this.g = engine;
        this.h = glideExperiments;
        this.i = i2;
    }

    public <T> TransitionOptions<?, T> a(Class<T> cls) {
        TransitionOptions<?, T> transitionOptions = this.f.get(cls);
        if (transitionOptions == null) {
            for (Map.Entry next : this.f.entrySet()) {
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    transitionOptions = (TransitionOptions) next.getValue();
                }
            }
        }
        return transitionOptions == null ? k : transitionOptions;
    }

    public List<RequestListener<Object>> b() {
        return this.e;
    }

    public synchronized RequestOptions c() {
        if (this.j == null) {
            this.j = (RequestOptions) this.d.build().D();
        }
        return this.j;
    }

    public Engine d() {
        return this.g;
    }

    public GlideExperiments e() {
        return this.h;
    }

    public int f() {
        return this.i;
    }

    public Registry g() {
        return this.b;
    }

    public <X> ViewTarget<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    public ArrayPool a() {
        return this.f2106a;
    }
}
