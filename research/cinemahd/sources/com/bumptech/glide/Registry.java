package com.bumptech.glide;

import androidx.core.util.Pools$Pool;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.load.data.DataRewinderRegistry;
import com.bumptech.glide.load.engine.DecodePath;
import com.bumptech.glide.load.engine.LoadPath;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.ModelLoaderRegistry;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.load.resource.transcode.TranscoderRegistry;
import com.bumptech.glide.provider.EncoderRegistry;
import com.bumptech.glide.provider.ImageHeaderParserRegistry;
import com.bumptech.glide.provider.LoadPathCache;
import com.bumptech.glide.provider.ModelToResourceClassCache;
import com.bumptech.glide.provider.ResourceDecoderRegistry;
import com.bumptech.glide.provider.ResourceEncoderRegistry;
import com.bumptech.glide.util.pool.FactoryPools;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Registry {

    /* renamed from: a  reason: collision with root package name */
    private final ModelLoaderRegistry f2112a = new ModelLoaderRegistry(this.j);
    private final EncoderRegistry b = new EncoderRegistry();
    private final ResourceDecoderRegistry c = new ResourceDecoderRegistry();
    private final ResourceEncoderRegistry d = new ResourceEncoderRegistry();
    private final DataRewinderRegistry e = new DataRewinderRegistry();
    private final TranscoderRegistry f = new TranscoderRegistry();
    private final ImageHeaderParserRegistry g = new ImageHeaderParserRegistry();
    private final ModelToResourceClassCache h = new ModelToResourceClassCache();
    private final LoadPathCache i = new LoadPathCache();
    private final Pools$Pool<List<Throwable>> j = FactoryPools.b();

    public static class MissingComponentException extends RuntimeException {
        public MissingComponentException(String str) {
            super(str);
        }
    }

    public static final class NoImageHeaderParserException extends MissingComponentException {
        public NoImageHeaderParserException() {
            super("Failed to find image header parser.");
        }
    }

    public static class NoModelLoaderAvailableException extends MissingComponentException {
        public NoModelLoaderAvailableException(Object obj) {
            super("Failed to find any ModelLoaders registered for model class: " + obj.getClass());
        }

        public <M> NoModelLoaderAvailableException(M m, List<ModelLoader<M, ?>> list) {
            super("Found ModelLoaders for model class: " + list + ", but none that handle this specific model instance: " + m);
        }

        public NoModelLoaderAvailableException(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }
    }

    public static class NoResultEncoderAvailableException extends MissingComponentException {
        public NoResultEncoderAvailableException(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    public static class NoSourceEncoderAvailableException extends MissingComponentException {
        public NoSourceEncoderAvailableException(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    public Registry() {
        a((List<String>) Arrays.asList(new String[]{"Gif", "Bitmap", "BitmapDrawable"}));
    }

    private <Data, TResource, Transcode> List<DecodePath<Data, TResource, Transcode>> c(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class next : this.c.b(cls, cls2)) {
            for (Class next2 : this.f.b(next, cls3)) {
                arrayList.add(new DecodePath(cls, next, next2, this.c.a(cls, next), this.f.a(next, next2), this.j));
            }
        }
        return arrayList;
    }

    public <Data> Registry a(Class<Data> cls, Encoder<Data> encoder) {
        this.b.a(cls, encoder);
        return this;
    }

    public <Model, Data> Registry b(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        this.f2112a.b(cls, cls2, modelLoaderFactory);
        return this;
    }

    public <Data, TResource> Registry a(Class<Data> cls, Class<TResource> cls2, ResourceDecoder<Data, TResource> resourceDecoder) {
        a("legacy_append", cls, cls2, resourceDecoder);
        return this;
    }

    public <Model, TResource, Transcode> List<Class<?>> b(Class<Model> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        List<Class<?>> a2 = this.h.a(cls, cls2, cls3);
        if (a2 == null) {
            a2 = new ArrayList<>();
            for (Class<?> b2 : this.f2112a.a((Class<?>) cls)) {
                for (Class next : this.c.b(b2, cls2)) {
                    if (!this.f.b(next, cls3).isEmpty() && !a2.contains(next)) {
                        a2.add(next);
                    }
                }
            }
            this.h.a(cls, cls2, cls3, Collections.unmodifiableList(a2));
        }
        return a2;
    }

    public <Data, TResource> Registry a(String str, Class<Data> cls, Class<TResource> cls2, ResourceDecoder<Data, TResource> resourceDecoder) {
        this.c.a(str, resourceDecoder, cls, cls2);
        return this;
    }

    public final Registry a(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.add("legacy_prepend_all");
        for (String add : list) {
            arrayList.add(add);
        }
        arrayList.add("legacy_append");
        this.c.a((List<String>) arrayList);
        return this;
    }

    public <TResource> Registry a(Class<TResource> cls, ResourceEncoder<TResource> resourceEncoder) {
        this.d.a(cls, resourceEncoder);
        return this;
    }

    public Registry a(DataRewinder.Factory<?> factory) {
        this.e.a(factory);
        return this;
    }

    public <TResource, Transcode> Registry a(Class<TResource> cls, Class<Transcode> cls2, ResourceTranscoder<TResource, Transcode> resourceTranscoder) {
        this.f.a(cls, cls2, resourceTranscoder);
        return this;
    }

    public Registry a(ImageHeaderParser imageHeaderParser) {
        this.g.a(imageHeaderParser);
        return this;
    }

    public <Model, Data> Registry a(Class<Model> cls, Class<Data> cls2, ModelLoaderFactory<Model, Data> modelLoaderFactory) {
        this.f2112a.a(cls, cls2, modelLoaderFactory);
        return this;
    }

    public <X> Encoder<X> c(X x) throws NoSourceEncoderAvailableException {
        Encoder<X> a2 = this.b.a(x.getClass());
        if (a2 != null) {
            return a2;
        }
        throw new NoSourceEncoderAvailableException(x.getClass());
    }

    public <Data, TResource, Transcode> LoadPath<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        LoadPath<Data, TResource, Transcode> a2 = this.i.a(cls, cls2, cls3);
        if (this.i.a(a2)) {
            return null;
        }
        if (a2 == null) {
            List<DecodePath<Data, TResource, Transcode>> c2 = c(cls, cls2, cls3);
            if (c2.isEmpty()) {
                a2 = null;
            } else {
                a2 = new LoadPath<>(cls, cls2, cls3, c2, this.j);
            }
            this.i.a(cls, cls2, cls3, a2);
        }
        return a2;
    }

    public boolean b(Resource<?> resource) {
        return this.d.a(resource.a()) != null;
    }

    public <X> DataRewinder<X> b(X x) {
        return this.e.a(x);
    }

    public <X> ResourceEncoder<X> a(Resource<X> resource) throws NoResultEncoderAvailableException {
        ResourceEncoder<X> a2 = this.d.a(resource.a());
        if (a2 != null) {
            return a2;
        }
        throw new NoResultEncoderAvailableException(resource.a());
    }

    public <Model> List<ModelLoader<Model, ?>> a(Model model) {
        return this.f2112a.a(model);
    }

    public List<ImageHeaderParser> a() {
        List<ImageHeaderParser> a2 = this.g.a();
        if (!a2.isEmpty()) {
            return a2;
        }
        throw new NoImageHeaderParserException();
    }
}
