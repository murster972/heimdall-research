package com.yarolegovich.lovelydialog;

import android.content.DialogInterface;
import android.view.View;

class LovelyDialogCompat$DialogOnClickListenerAdapter implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private DialogInterface.OnClickListener f6588a;

    public void a(DialogInterface dialogInterface, int i) {
        DialogInterface.OnClickListener onClickListener = this.f6588a;
        if (onClickListener != null) {
            onClickListener.onClick(dialogInterface, i);
        }
    }

    public void onClick(View view) {
    }
}
