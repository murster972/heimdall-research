package com.yarolegovich.lovelydialog;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import com.yarolegovich.lovelydialog.AbsLovelyDialog;

public class LovelyStandardDialog extends AbsLovelyDialog<LovelyStandardDialog> {
    private Button g = ((Button) b(R$id.ld_btn_yes));
    private Button h = ((Button) b(R$id.ld_btn_no));
    private Button i = ((Button) b(R$id.ld_btn_neutral));

    public enum ButtonLayout {
        HORIZONTAL(R$layout.dialog_standard),
        VERTICAL(R$layout.dialog_standard_vertical);
        
        final int layoutRes;

        private ButtonLayout(int i) {
            this.layoutRes = i;
        }
    }

    public LovelyStandardDialog(Context context, ButtonLayout buttonLayout) {
        super(context, 0, buttonLayout.layoutRes);
    }

    public LovelyStandardDialog a(int i2, View.OnClickListener onClickListener) {
        a(f(i2), onClickListener);
        return this;
    }

    /* access modifiers changed from: protected */
    public int c() {
        return ButtonLayout.HORIZONTAL.layoutRes;
    }

    public LovelyStandardDialog g(int i2) {
        this.g.setTextColor(i2);
        this.h.setTextColor(i2);
        this.i.setTextColor(i2);
        return this;
    }

    public LovelyStandardDialog h(int i2) {
        g(a(i2));
        return this;
    }

    public LovelyStandardDialog a(String str, View.OnClickListener onClickListener) {
        this.g.setVisibility(0);
        this.g.setText(str);
        this.g.setOnClickListener(new AbsLovelyDialog.ClickListenerDecorator(onClickListener, true));
        return this;
    }
}
