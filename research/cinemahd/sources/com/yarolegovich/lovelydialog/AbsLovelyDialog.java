package com.yarolegovich.lovelydialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import com.yarolegovich.lovelydialog.AbsLovelyDialog;

public abstract class AbsLovelyDialog<T extends AbsLovelyDialog> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Dialog f6586a;
    private View b;
    private ImageView c;
    private TextView d;
    private TextView e;
    private TextView f;

    protected class ClickListenerDecorator implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        private View.OnClickListener f6587a;
        private boolean b;

        protected ClickListenerDecorator(View.OnClickListener onClickListener, boolean z) {
            this.f6587a = onClickListener;
            this.b = z;
        }

        public void onClick(View view) {
            View.OnClickListener onClickListener = this.f6587a;
            if (onClickListener != null) {
                if (onClickListener instanceof LovelyDialogCompat$DialogOnClickListenerAdapter) {
                    ((LovelyDialogCompat$DialogOnClickListenerAdapter) onClickListener).a(AbsLovelyDialog.this.f6586a, view.getId());
                } else {
                    onClickListener.onClick(view);
                }
            }
            if (this.b) {
                AbsLovelyDialog.this.a();
            }
        }
    }

    public AbsLovelyDialog(Context context) {
        this(context, 0);
    }

    public T b(CharSequence charSequence) {
        this.e.setVisibility(0);
        this.e.setText(charSequence);
        return this;
    }

    /* access modifiers changed from: protected */
    public abstract int c();

    public T c(CharSequence charSequence) {
        this.d.setVisibility(0);
        this.d.setText(charSequence);
        return this;
    }

    public T d(int i) {
        b(R$id.ld_color_area).setBackgroundColor(i);
        return this;
    }

    public T e(int i) {
        d(a(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public String f(int i) {
        return this.b.getContext().getString(i);
    }

    public AbsLovelyDialog(Context context, int i) {
        this(context, i, 0);
    }

    private void a(AlertDialog.Builder builder, int i) {
        this.b = LayoutInflater.from(builder.b()).inflate(i, (ViewGroup) null);
        builder.b(this.b);
        this.f6586a = builder.a();
        this.c = (ImageView) b(R$id.ld_icon);
        this.e = (TextView) b(R$id.ld_title);
        this.f = (TextView) b(R$id.ld_message);
        this.d = (TextView) b(R$id.ld_top_title);
    }

    public Dialog d() {
        this.f6586a.show();
        return this.f6586a;
    }

    public AbsLovelyDialog(Context context, int i, int i2) {
        i2 = i2 == 0 ? c() : i2;
        if (i == 0) {
            a(new AlertDialog.Builder(context), i2);
        } else {
            a(new AlertDialog.Builder(context, i), i2);
        }
    }

    /* access modifiers changed from: protected */
    public Context b() {
        return this.b.getContext();
    }

    public T c(int i) {
        this.c.setVisibility(0);
        this.c.setImageResource(i);
        return this;
    }

    /* access modifiers changed from: protected */
    public <ViewClass extends View> ViewClass b(int i) {
        return this.b.findViewById(i);
    }

    public T a(CharSequence charSequence) {
        this.f.setVisibility(0);
        this.f.setText(charSequence);
        return this;
    }

    public T a(Drawable drawable) {
        this.c.setVisibility(0);
        this.c.setImageDrawable(drawable);
        return this;
    }

    public void a() {
        this.f6586a.dismiss();
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        return ContextCompat.a(b(), i);
    }
}
