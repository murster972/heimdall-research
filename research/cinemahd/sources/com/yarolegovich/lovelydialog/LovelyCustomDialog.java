package com.yarolegovich.lovelydialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.yarolegovich.lovelydialog.AbsLovelyDialog;

public class LovelyCustomDialog extends AbsLovelyDialog<LovelyCustomDialog> {
    private View g;

    public LovelyCustomDialog(Context context) {
        super(context);
    }

    public LovelyCustomDialog a(ViewConfigurator<View> viewConfigurator) {
        View view = this.g;
        if (view != null) {
            viewConfigurator.a(view);
            return this;
        }
        throw new IllegalStateException(f(R$string.ex_msg_dialog_view_not_set));
    }

    /* access modifiers changed from: protected */
    public int c() {
        return R$layout.dialog_custom;
    }

    public LovelyCustomDialog g(int i) {
        this.g = LayoutInflater.from(b()).inflate(i, (ViewGroup) b(R$id.ld_custom_view_container), true);
        return this;
    }

    public LovelyCustomDialog a(int i, View.OnClickListener onClickListener) {
        a(i, false, onClickListener);
        return this;
    }

    public LovelyCustomDialog a(int i, boolean z, View.OnClickListener onClickListener) {
        if (this.g != null) {
            b(i).setOnClickListener(new AbsLovelyDialog.ClickListenerDecorator(onClickListener, z));
            return this;
        }
        throw new IllegalStateException(f(R$string.ex_msg_dialog_view_not_set));
    }
}
