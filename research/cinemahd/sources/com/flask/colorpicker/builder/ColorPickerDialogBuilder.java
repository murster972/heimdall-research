package com.flask.colorpicker.builder;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.app.AlertDialog;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.R$dimen;
import com.flask.colorpicker.R$id;
import com.flask.colorpicker.R$layout;
import com.flask.colorpicker.Utils;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;

public class ColorPickerDialogBuilder {

    /* renamed from: a  reason: collision with root package name */
    private AlertDialog.Builder f3100a;
    private LinearLayout b;
    private ColorPickerView c;
    private LightnessSlider d;
    private AlphaSlider e;
    private EditText f;
    private LinearLayout g;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private Integer[] n;

    private ColorPickerDialogBuilder(Context context) {
        this(context, 0);
    }

    public ColorPickerDialogBuilder b(int i2) {
        this.n[0] = Integer.valueOf(i2);
        return this;
    }

    public ColorPickerDialogBuilder c() {
        this.h = true;
        this.i = false;
        return this;
    }

    public ColorPickerDialogBuilder d() {
        this.h = false;
        this.i = false;
        return this;
    }

    private ColorPickerDialogBuilder(Context context, int i2) {
        this.h = true;
        this.i = true;
        this.j = false;
        this.k = false;
        this.l = 1;
        this.m = 0;
        this.n = new Integer[]{null, null, null, null, null};
        this.m = a(context, R$dimen.default_slider_margin);
        int a2 = a(context, R$dimen.default_slider_margin_btw_title);
        this.f3100a = new AlertDialog.Builder(context, i2);
        this.b = new LinearLayout(context);
        this.b.setOrientation(1);
        this.b.setGravity(1);
        LinearLayout linearLayout = this.b;
        int i3 = this.m;
        linearLayout.setPadding(i3, a2, i3, i3);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 0);
        layoutParams.weight = 1.0f;
        this.c = new ColorPickerView(context);
        this.b.addView(this.c, layoutParams);
        this.f3100a.b((View) this.b);
    }

    public static ColorPickerDialogBuilder a(Context context) {
        return new ColorPickerDialogBuilder(context);
    }

    public AlertDialog b() {
        Context b2 = this.f3100a.b();
        ColorPickerView colorPickerView = this.c;
        Integer[] numArr = this.n;
        colorPickerView.a(numArr, b(numArr).intValue());
        if (this.h) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, a(b2, R$dimen.default_slider_height));
            this.d = new LightnessSlider(b2);
            this.d.setLayoutParams(layoutParams);
            this.b.addView(this.d);
            this.c.setLightnessSlider(this.d);
            this.d.setColor(a(this.n));
        }
        if (this.i) {
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, a(b2, R$dimen.default_slider_height));
            this.e = new AlphaSlider(b2);
            this.e.setLayoutParams(layoutParams2);
            this.b.addView(this.e);
            this.c.setAlphaSlider(this.e);
            this.e.setColor(a(this.n));
        }
        if (this.j) {
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
            this.f = (EditText) View.inflate(b2, R$layout.picker_edit, (ViewGroup) null);
            this.f.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            this.f.setSingleLine();
            this.f.setVisibility(8);
            this.f.setFilters(new InputFilter[]{new InputFilter.LengthFilter(this.i ? 9 : 7)});
            this.b.addView(this.f, layoutParams3);
            this.f.setText(Utils.a(a(this.n), this.i));
            this.c.setColorEdit(this.f);
        }
        if (this.k) {
            this.g = (LinearLayout) View.inflate(b2, R$layout.color_preview, (ViewGroup) null);
            this.g.setVisibility(8);
            this.b.addView(this.g);
            if (this.n.length != 0) {
                int i2 = 0;
                while (true) {
                    Integer[] numArr2 = this.n;
                    if (i2 >= numArr2.length || i2 >= this.l || numArr2[i2] == null) {
                        break;
                    }
                    LinearLayout linearLayout = (LinearLayout) View.inflate(b2, R$layout.color_selector, (ViewGroup) null);
                    ((ImageView) linearLayout.findViewById(R$id.image_preview)).setImageDrawable(new ColorDrawable(this.n[i2].intValue()));
                    this.g.addView(linearLayout);
                    i2++;
                }
            } else {
                ((ImageView) View.inflate(b2, R$layout.color_selector, (ViewGroup) null)).setImageDrawable(new ColorDrawable(-1));
            }
            this.g.setVisibility(0);
            this.c.a(this.g, b(this.n));
        }
        return this.f3100a.a();
    }

    public ColorPickerDialogBuilder a(String str) {
        this.f3100a.b((CharSequence) str);
        return this;
    }

    public ColorPickerDialogBuilder a(ColorPickerView.WHEEL_TYPE wheel_type) {
        this.c.setRenderer(ColorWheelRendererBuilder.a(wheel_type));
        return this;
    }

    public ColorPickerDialogBuilder a(int i2) {
        this.c.setDensity(i2);
        return this;
    }

    public ColorPickerDialogBuilder a(OnColorSelectedListener onColorSelectedListener) {
        this.c.a(onColorSelectedListener);
        return this;
    }

    public ColorPickerDialogBuilder a(CharSequence charSequence, final ColorPickerClickListener colorPickerClickListener) {
        this.f3100a.b(charSequence, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ColorPickerDialogBuilder.this.a(dialogInterface, colorPickerClickListener);
            }
        });
        return this;
    }

    public ColorPickerDialogBuilder a(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.f3100a.a(charSequence, onClickListener);
        return this;
    }

    public ColorPickerDialogBuilder a() {
        this.h = false;
        this.i = true;
        return this;
    }

    public ColorPickerDialogBuilder a(boolean z) {
        this.j = z;
        return this;
    }

    private int a(Integer[] numArr) {
        Integer b2 = b(numArr);
        if (b2 == null) {
            return -1;
        }
        return numArr[b2.intValue()].intValue();
    }

    private static int a(Context context, int i2) {
        return (int) (context.getResources().getDimension(i2) + 0.5f);
    }

    /* access modifiers changed from: private */
    public void a(DialogInterface dialogInterface, ColorPickerClickListener colorPickerClickListener) {
        colorPickerClickListener.a(dialogInterface, this.c.getSelectedColor(), this.c.getAllColors());
    }

    private Integer b(Integer[] numArr) {
        int i2 = 0;
        int i3 = 0;
        while (i2 < numArr.length && numArr[i2] != null) {
            i2++;
            i3 = Integer.valueOf(i2 / 2);
        }
        return i3;
    }
}
