package com.flask.colorpicker.builder;

import android.content.DialogInterface;

public interface ColorPickerClickListener {
    void a(DialogInterface dialogInterface, int i, Integer[] numArr);
}
