package com.flask.colorpicker.builder;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;

public class PaintBuilder {

    public static class PaintHolder {

        /* renamed from: a  reason: collision with root package name */
        private Paint f3103a;

        public PaintHolder a(int i) {
            this.f3103a.setColor(i);
            return this;
        }

        private PaintHolder() {
            this.f3103a = new Paint(1);
        }

        public PaintHolder a(Paint.Style style) {
            this.f3103a.setStyle(style);
            return this;
        }

        public PaintHolder a(float f) {
            this.f3103a.setStrokeWidth(f);
            return this;
        }

        public PaintHolder a(PorterDuff.Mode mode) {
            this.f3103a.setXfermode(new PorterDuffXfermode(mode));
            return this;
        }

        public PaintHolder a(Shader shader) {
            this.f3103a.setShader(shader);
            return this;
        }

        public Paint a() {
            return this.f3103a;
        }
    }

    public static PaintHolder a() {
        return new PaintHolder();
    }

    public static Shader b(int i) {
        Bitmap a2 = a(Math.max(8, (i / 2) * 2));
        Shader.TileMode tileMode = Shader.TileMode.REPEAT;
        return new BitmapShader(a2, tileMode, tileMode);
    }

    private static Bitmap a(int i) {
        Paint a2 = a().a();
        Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        int round = Math.round(((float) i) / 2.0f);
        for (int i2 = 0; i2 < 2; i2++) {
            int i3 = 0;
            while (i3 < 2) {
                if ((i2 + i3) % 2 == 0) {
                    a2.setColor(-1);
                } else {
                    a2.setColor(-3092272);
                }
                int i4 = i3 + 1;
                Canvas canvas2 = canvas;
                canvas2.drawRect((float) (i2 * round), (float) (i3 * round), (float) ((i2 + 1) * round), (float) (i4 * round), a2);
                i3 = i4;
            }
        }
        return createBitmap;
    }
}
