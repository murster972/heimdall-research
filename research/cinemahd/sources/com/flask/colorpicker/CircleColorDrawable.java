package com.flask.colorpicker;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import com.flask.colorpicker.builder.PaintBuilder;

public class CircleColorDrawable extends ColorDrawable {

    /* renamed from: a  reason: collision with root package name */
    private float f3091a;
    private Paint b = PaintBuilder.a().a(Paint.Style.STROKE).a(this.f3091a).a(-1).a();
    private Paint c = PaintBuilder.a().a(Paint.Style.FILL).a(0).a();
    private Paint d = PaintBuilder.a().a(PaintBuilder.b(16)).a();

    public CircleColorDrawable() {
    }

    public void draw(Canvas canvas) {
        canvas.drawColor(0);
        float width = ((float) canvas.getWidth()) / 2.0f;
        this.f3091a = width / 12.0f;
        this.b.setStrokeWidth(this.f3091a);
        this.c.setColor(getColor());
        canvas.drawCircle(width, width, width - (this.f3091a * 1.5f), this.d);
        canvas.drawCircle(width, width, width - (this.f3091a * 1.5f), this.c);
        canvas.drawCircle(width, width, width - this.f3091a, this.b);
    }

    public void setColor(int i) {
        super.setColor(i);
        invalidateSelf();
    }

    public CircleColorDrawable(int i) {
        super(i);
    }
}
