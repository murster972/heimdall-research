package com.flask.colorpicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.flask.colorpicker.builder.ColorWheelRendererBuilder;
import com.flask.colorpicker.builder.PaintBuilder;
import com.flask.colorpicker.renderer.ColorWheelRenderOption;
import com.flask.colorpicker.renderer.ColorWheelRenderer;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;
import java.util.ArrayList;
import java.util.Iterator;

public class ColorPickerView extends View {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f3095a;
    private Canvas b;
    private int c = 10;
    private float d = 1.0f;
    private float e = 1.0f;
    private int f = 0;
    private Integer[] g = {null, null, null, null, null};
    private int h = 0;
    private Integer i;
    private Integer j;
    private Paint k = PaintBuilder.a().a(0).a();
    private Paint l = PaintBuilder.a().a(-1).a();
    private Paint m = PaintBuilder.a().a(-16777216).a();
    private Paint n = PaintBuilder.a().a();
    private ColorCircle o;
    private ArrayList<OnColorChangedListener> p = new ArrayList<>();
    private ArrayList<OnColorSelectedListener> q = new ArrayList<>();
    private LightnessSlider r;
    private AlphaSlider s;
    private EditText t;
    private TextWatcher u = new TextWatcher() {
        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            try {
                ColorPickerView.this.a(Color.parseColor(charSequence.toString()), false);
            } catch (Exception unused) {
            }
        }
    };
    private LinearLayout v;
    private ColorWheelRenderer w;
    private int x;
    private int y;

    public enum WHEEL_TYPE {
        FLOWER,
        CIRCLE;

        public static WHEEL_TYPE a(int i) {
            if (i == 0) {
                return FLOWER;
            }
            if (i != 1) {
                return FLOWER;
            }
            return CIRCLE;
        }
    }

    public ColorPickerView(Context context) {
        super(context);
        a(context, (AttributeSet) null);
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ColorPickerPreference);
        this.c = obtainStyledAttributes.getInt(R$styleable.ColorPickerPreference_density, 10);
        this.i = Integer.valueOf(obtainStyledAttributes.getInt(R$styleable.ColorPickerPreference_initialColor, -1));
        this.j = Integer.valueOf(obtainStyledAttributes.getInt(R$styleable.ColorPickerPreference_pickerColorEditTextColor, -1));
        ColorWheelRenderer a2 = ColorWheelRendererBuilder.a(WHEEL_TYPE.a(obtainStyledAttributes.getInt(R$styleable.ColorPickerPreference_wheelType, 0)));
        this.x = obtainStyledAttributes.getResourceId(R$styleable.ColorPickerPreference_alphaSliderView, 0);
        this.y = obtainStyledAttributes.getResourceId(R$styleable.ColorPickerPreference_lightnessSliderView, 0);
        setRenderer(a2);
        setDensity(this.c);
        b(this.i.intValue(), true);
        obtainStyledAttributes.recycle();
    }

    private void b() {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (measuredHeight < measuredWidth) {
            measuredWidth = measuredHeight;
        }
        if (measuredWidth > 0) {
            if (this.f3095a == null) {
                this.f3095a = Bitmap.createBitmap(measuredWidth, measuredWidth, Bitmap.Config.ARGB_8888);
                this.b = new Canvas(this.f3095a);
                this.n.setShader(PaintBuilder.b(8));
            }
            a();
            invalidate();
        }
    }

    private void setColorPreviewColor(int i2) {
        Integer[] numArr;
        int i3;
        LinearLayout linearLayout = this.v;
        if (linearLayout != null && (numArr = this.g) != null && (i3 = this.h) <= numArr.length && numArr[i3] != null && linearLayout.getChildCount() != 0 && this.v.getVisibility() == 0) {
            View childAt = this.v.getChildAt(this.h);
            if (childAt instanceof LinearLayout) {
                ((ImageView) ((LinearLayout) childAt).findViewById(R$id.image_preview)).setImageDrawable(new CircleColorDrawable(i2));
            }
        }
    }

    private void setColorText(int i2) {
        EditText editText = this.t;
        if (editText != null) {
            editText.setText(Utils.a(i2, this.s != null));
        }
    }

    private void setColorToSliders(int i2) {
        LightnessSlider lightnessSlider = this.r;
        if (lightnessSlider != null) {
            lightnessSlider.setColor(i2);
        }
        AlphaSlider alphaSlider = this.s;
        if (alphaSlider != null) {
            alphaSlider.setColor(i2);
        }
    }

    private void setHighlightedColor(int i2) {
        int childCount = this.v.getChildCount();
        if (childCount != 0 && this.v.getVisibility() == 0) {
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = this.v.getChildAt(i3);
                if (childAt instanceof LinearLayout) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (i3 == i2) {
                        linearLayout.setBackgroundColor(-1);
                    } else {
                        linearLayout.setBackgroundColor(0);
                    }
                }
            }
        }
    }

    public Integer[] getAllColors() {
        return this.g;
    }

    public int getSelectedColor() {
        ColorCircle colorCircle = this.o;
        return Utils.a(this.e, colorCircle != null ? Color.HSVToColor(colorCircle.a(this.d)) : 0);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(this.f);
        Bitmap bitmap = this.f3095a;
        if (bitmap != null) {
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        }
        if (this.o != null) {
            float width = (((((float) canvas.getWidth()) / 2.0f) - 2.05f) / ((float) this.c)) / 2.0f;
            this.k.setColor(Color.HSVToColor(this.o.a(this.d)));
            this.k.setAlpha((int) (this.e * 255.0f));
            canvas.drawCircle(this.o.b(), this.o.c(), 2.0f * width, this.l);
            canvas.drawCircle(this.o.b(), this.o.c(), 1.5f * width, this.m);
            canvas.drawCircle(this.o.b(), this.o.c(), width, this.n);
            canvas.drawCircle(this.o.b(), this.o.c(), width, this.k);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.x != 0) {
            setAlphaSlider((AlphaSlider) getRootView().findViewById(this.x));
        }
        if (this.y != 0) {
            setLightnessSlider((LightnessSlider) getRootView().findViewById(this.y));
        }
        b();
        this.o = a(this.i.intValue());
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == 0) {
            i4 = i2;
        } else if (mode == Integer.MIN_VALUE) {
            i4 = View.MeasureSpec.getSize(i2);
        } else {
            i4 = mode == 1073741824 ? View.MeasureSpec.getSize(i2) : 0;
        }
        int mode2 = View.MeasureSpec.getMode(i3);
        if (mode2 != 0) {
            if (mode2 == Integer.MIN_VALUE) {
                i2 = View.MeasureSpec.getSize(i3);
            } else {
                i2 = mode == 1073741824 ? View.MeasureSpec.getSize(i3) : 0;
            }
        }
        if (i2 >= i4) {
            i2 = i4;
        }
        setMeasuredDimension(i2, i2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        if (r0 != 2) goto L_0x005d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r4) {
        /*
            r3 = this;
            int r0 = r4.getAction()
            r1 = 1
            if (r0 == 0) goto L_0x0038
            if (r0 == r1) goto L_0x000d
            r2 = 2
            if (r0 == r2) goto L_0x0038
            goto L_0x005d
        L_0x000d:
            int r4 = r3.getSelectedColor()
            java.util.ArrayList<com.flask.colorpicker.OnColorSelectedListener> r0 = r3.q
            if (r0 == 0) goto L_0x002b
            java.util.Iterator r0 = r0.iterator()
        L_0x0019:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x002b
            java.lang.Object r2 = r0.next()
            com.flask.colorpicker.OnColorSelectedListener r2 = (com.flask.colorpicker.OnColorSelectedListener) r2
            r2.a(r4)     // Catch:{ Exception -> 0x0029 }
            goto L_0x0019
        L_0x0029:
            goto L_0x0019
        L_0x002b:
            r3.setColorToSliders(r4)
            r3.setColorText(r4)
            r3.setColorPreviewColor(r4)
            r3.invalidate()
            goto L_0x005d
        L_0x0038:
            int r0 = r3.getSelectedColor()
            float r2 = r4.getX()
            float r4 = r4.getY()
            com.flask.colorpicker.ColorCircle r4 = r3.a((float) r2, (float) r4)
            r3.o = r4
            int r4 = r3.getSelectedColor()
            r3.a((int) r0, (int) r4)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)
            r3.i = r0
            r3.setColorToSliders(r4)
            r3.invalidate()
        L_0x005d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flask.colorpicker.ColorPickerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        b();
        this.o = a(this.i.intValue());
    }

    public void setAlphaSlider(AlphaSlider alphaSlider) {
        this.s = alphaSlider;
        if (alphaSlider != null) {
            this.s.setColorPicker(this);
            this.s.setColor(getSelectedColor());
        }
    }

    public void setAlphaValue(float f2) {
        Integer num;
        int selectedColor = getSelectedColor();
        this.e = f2;
        this.i = Integer.valueOf(Color.HSVToColor(Utils.a(this.e), this.o.a(this.d)));
        EditText editText = this.t;
        if (editText != null) {
            editText.setText(Utils.a(this.i.intValue(), this.s != null));
        }
        LightnessSlider lightnessSlider = this.r;
        if (!(lightnessSlider == null || (num = this.i) == null)) {
            lightnessSlider.setColor(num.intValue());
        }
        a(selectedColor, this.i.intValue());
        b();
        invalidate();
    }

    public void setColorEdit(EditText editText) {
        this.t = editText;
        EditText editText2 = this.t;
        if (editText2 != null) {
            editText2.setVisibility(0);
            this.t.addTextChangedListener(this.u);
            setColorEditTextColor(this.j.intValue());
        }
    }

    public void setColorEditTextColor(int i2) {
        this.j = Integer.valueOf(i2);
        EditText editText = this.t;
        if (editText != null) {
            editText.setTextColor(i2);
        }
    }

    public void setDensity(int i2) {
        this.c = Math.max(2, i2);
        invalidate();
    }

    public void setLightness(float f2) {
        Integer num;
        int selectedColor = getSelectedColor();
        this.d = f2;
        this.i = Integer.valueOf(Color.HSVToColor(Utils.a(this.e), this.o.a(f2)));
        EditText editText = this.t;
        if (editText != null) {
            editText.setText(Utils.a(this.i.intValue(), this.s != null));
        }
        AlphaSlider alphaSlider = this.s;
        if (!(alphaSlider == null || (num = this.i) == null)) {
            alphaSlider.setColor(num.intValue());
        }
        a(selectedColor, this.i.intValue());
        b();
        invalidate();
    }

    public void setLightnessSlider(LightnessSlider lightnessSlider) {
        this.r = lightnessSlider;
        if (lightnessSlider != null) {
            this.r.setColorPicker(this);
            this.r.setColor(getSelectedColor());
        }
    }

    public void setRenderer(ColorWheelRenderer colorWheelRenderer) {
        this.w = colorWheelRenderer;
        invalidate();
    }

    public void setSelectedColor(int i2) {
        Integer[] numArr = this.g;
        if (numArr != null && numArr.length >= i2) {
            this.h = i2;
            setHighlightedColor(i2);
            Integer num = this.g[i2];
            if (num != null) {
                a(num.intValue(), true);
            }
        }
    }

    public void b(int i2, boolean z) {
        float[] fArr = new float[3];
        Color.colorToHSV(i2, fArr);
        this.e = Utils.a(i2);
        this.d = fArr[2];
        this.g[this.h] = Integer.valueOf(i2);
        this.i = Integer.valueOf(i2);
        setColorPreviewColor(i2);
        setColorToSliders(i2);
        if (this.t != null && z) {
            setColorText(i2);
        }
        this.o = a(i2);
    }

    private void a() {
        this.b.drawColor(0, PorterDuff.Mode.CLEAR);
        if (this.w != null) {
            float width = ((float) this.b.getWidth()) / 2.0f;
            int i2 = this.c;
            float f2 = (width - 2.05f) - (width / ((float) i2));
            ColorWheelRenderOption b2 = this.w.b();
            b2.f3105a = this.c;
            b2.b = f2;
            b2.c = (f2 / ((float) (i2 - 1))) / 2.0f;
            b2.d = 2.05f;
            b2.e = this.e;
            b2.f = this.d;
            b2.g = this.b;
            this.w.a(b2);
            this.w.a();
        }
    }

    public ColorPickerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3) {
        ArrayList<OnColorChangedListener> arrayList = this.p;
        if (arrayList != null && i2 != i3) {
            Iterator<OnColorChangedListener> it2 = arrayList.iterator();
            while (it2.hasNext()) {
                try {
                    it2.next().a(i3);
                } catch (Exception unused) {
                }
            }
        }
    }

    private ColorCircle a(float f2, float f3) {
        ColorCircle colorCircle = null;
        double d2 = Double.MAX_VALUE;
        for (ColorCircle next : this.w.c()) {
            double a2 = next.a(f2, f3);
            if (d2 > a2) {
                colorCircle = next;
                d2 = a2;
            }
        }
        return colorCircle;
    }

    private ColorCircle a(int i2) {
        float[] fArr = new float[3];
        Color.colorToHSV(i2, fArr);
        char c2 = 1;
        char c3 = 0;
        double cos = ((double) fArr[1]) * Math.cos((((double) fArr[0]) * 3.141592653589793d) / 180.0d);
        double sin = ((double) fArr[1]) * Math.sin((((double) fArr[0]) * 3.141592653589793d) / 180.0d);
        ColorCircle colorCircle = null;
        double d2 = Double.MAX_VALUE;
        for (ColorCircle next : this.w.c()) {
            float[] a2 = next.a();
            double d3 = sin;
            double cos2 = ((double) a2[c2]) * Math.cos((((double) a2[c3]) * 3.141592653589793d) / 180.0d);
            double d4 = cos - cos2;
            double sin2 = d3 - (((double) a2[1]) * Math.sin((((double) a2[0]) * 3.141592653589793d) / 180.0d));
            double d5 = (d4 * d4) + (sin2 * sin2);
            if (d5 < d2) {
                d2 = d5;
                colorCircle = next;
            }
            c2 = 1;
            c3 = 0;
            sin = d3;
        }
        return colorCircle;
    }

    public void a(Integer[] numArr, int i2) {
        this.g = numArr;
        this.h = i2;
        Integer num = this.g[this.h];
        if (num == null) {
            num = -1;
        }
        b(num.intValue(), true);
    }

    public void a(int i2, boolean z) {
        b(i2, z);
        b();
        invalidate();
    }

    public void a(OnColorSelectedListener onColorSelectedListener) {
        this.q.add(onColorSelectedListener);
    }

    public void a(LinearLayout linearLayout, Integer num) {
        if (linearLayout != null) {
            this.v = linearLayout;
            if (num == null) {
                num = 0;
            }
            int childCount = linearLayout.getChildCount();
            if (childCount != 0 && linearLayout.getVisibility() == 0) {
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = linearLayout.getChildAt(i2);
                    if (childAt instanceof LinearLayout) {
                        LinearLayout linearLayout2 = (LinearLayout) childAt;
                        if (i2 == num.intValue()) {
                            linearLayout2.setBackgroundColor(-1);
                        }
                        ImageView imageView = (ImageView) linearLayout2.findViewById(R$id.image_preview);
                        imageView.setClickable(true);
                        imageView.setTag(Integer.valueOf(i2));
                        imageView.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                Object tag;
                                if (view != null && (tag = view.getTag()) != null && (tag instanceof Integer)) {
                                    ColorPickerView.this.setSelectedColor(((Integer) tag).intValue());
                                }
                            }
                        });
                    }
                }
            }
        }
    }
}
