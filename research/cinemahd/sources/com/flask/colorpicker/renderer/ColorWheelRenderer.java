package com.flask.colorpicker.renderer;

import com.flask.colorpicker.ColorCircle;
import java.util.List;

public interface ColorWheelRenderer {
    void a();

    void a(ColorWheelRenderOption colorWheelRenderOption);

    ColorWheelRenderOption b();

    List<ColorCircle> c();
}
