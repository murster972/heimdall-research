package com.flask.colorpicker.renderer;

import com.flask.colorpicker.ColorCircle;
import java.util.ArrayList;
import java.util.List;

public abstract class AbsColorWheelRenderer implements ColorWheelRenderer {

    /* renamed from: a  reason: collision with root package name */
    protected ColorWheelRenderOption f3104a;
    protected List<ColorCircle> b = new ArrayList();

    public void a(ColorWheelRenderOption colorWheelRenderOption) {
        this.f3104a = colorWheelRenderOption;
        this.b.clear();
    }

    public ColorWheelRenderOption b() {
        if (this.f3104a == null) {
            this.f3104a = new ColorWheelRenderOption();
        }
        return this.f3104a;
    }

    public List<ColorCircle> c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public int d() {
        return Math.round(this.f3104a.e * 255.0f);
    }

    /* access modifiers changed from: protected */
    public int a(float f, float f2) {
        return Math.max(1, (int) ((3.063052912151454d / Math.asin((double) (f2 / f))) + 0.5d));
    }
}
