package com.flask.colorpicker.renderer;

import android.graphics.Color;
import android.graphics.Paint;
import com.flask.colorpicker.ColorCircle;
import com.flask.colorpicker.builder.PaintBuilder;

public class SimpleColorWheelRenderer extends AbsColorWheelRenderer {
    private Paint c = PaintBuilder.a().a();
    private float[] d = new float[3];

    public void a() {
        int size = this.b.size();
        float width = ((float) this.f3104a.g.getWidth()) / 2.0f;
        ColorWheelRenderOption colorWheelRenderOption = this.f3104a;
        int i = colorWheelRenderOption.f3105a;
        float f = colorWheelRenderOption.b;
        int i2 = 0;
        int i3 = 0;
        while (i2 < i) {
            float f2 = (((float) i2) / ((float) (i - 1))) * f;
            float f3 = this.f3104a.c;
            int a2 = a(f2, f3);
            int i4 = i3;
            int i5 = 0;
            while (i5 < a2) {
                double d2 = (double) a2;
                int i6 = i;
                double d3 = ((((double) i5) * 6.283185307179586d) / d2) + ((3.141592653589793d / d2) * ((double) ((i2 + 1) % 2)));
                double d4 = (double) f2;
                float cos = ((float) (Math.cos(d3) * d4)) + width;
                float sin = ((float) (d4 * Math.sin(d3))) + width;
                float[] fArr = this.d;
                fArr[0] = (float) ((d3 * 180.0d) / 3.141592653589793d);
                fArr[1] = f2 / f;
                fArr[2] = this.f3104a.f;
                this.c.setColor(Color.HSVToColor(fArr));
                this.c.setAlpha(d());
                ColorWheelRenderOption colorWheelRenderOption2 = this.f3104a;
                colorWheelRenderOption2.g.drawCircle(cos, sin, f3 - colorWheelRenderOption2.d, this.c);
                if (i4 >= size) {
                    this.b.add(new ColorCircle(cos, sin, this.d));
                } else {
                    this.b.get(i4).a(cos, sin, this.d);
                }
                i4++;
                i5++;
                i = i6;
            }
            int i7 = i;
            i2++;
            i3 = i4;
        }
    }
}
