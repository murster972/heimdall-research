package com.flask.colorpicker.renderer;

import android.graphics.Canvas;

public class ColorWheelRenderOption {

    /* renamed from: a  reason: collision with root package name */
    public int f3105a;
    public float b;
    public float c;
    public float d;
    public float e;
    public float f;
    public Canvas g;
}
