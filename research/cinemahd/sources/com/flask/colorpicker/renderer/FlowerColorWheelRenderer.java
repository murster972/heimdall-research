package com.flask.colorpicker.renderer;

import android.graphics.Color;
import android.graphics.Paint;
import com.flask.colorpicker.ColorCircle;
import com.flask.colorpicker.builder.PaintBuilder;

public class FlowerColorWheelRenderer extends AbsColorWheelRenderer {
    private Paint c = PaintBuilder.a().a();
    private float[] d = new float[3];
    private float e = 1.2f;

    public void a() {
        float f;
        int size = this.b.size();
        float f2 = 2.0f;
        float width = ((float) this.f3104a.g.getWidth()) / 2.0f;
        ColorWheelRenderOption colorWheelRenderOption = this.f3104a;
        int i = colorWheelRenderOption.f3105a;
        float f3 = colorWheelRenderOption.d;
        float f4 = colorWheelRenderOption.b;
        float f5 = colorWheelRenderOption.c;
        int i2 = 0;
        int i3 = 0;
        while (i2 < i) {
            float f6 = (float) i2;
            float f7 = f6 / ((float) (i - 1));
            float f8 = (float) i;
            float f9 = (f6 - (f8 / f2)) / f8;
            float f10 = f7 * f4;
            float f11 = 1.5f + f3;
            if (i2 == 0) {
                f = 0.0f;
            } else {
                f = f9 * this.e * f5;
            }
            float max = Math.max(f11, f + f5);
            int min = Math.min(a(f10, max), i * 2);
            int i4 = i3;
            int i5 = 0;
            while (i5 < min) {
                float f12 = f5;
                int i6 = i2;
                double d2 = (double) min;
                int i7 = min;
                double d3 = ((((double) i5) * 6.283185307179586d) / d2) + ((3.141592653589793d / d2) * ((double) ((i6 + 1) % 2)));
                double d4 = (double) f10;
                float cos = ((float) (Math.cos(d3) * d4)) + width;
                float sin = ((float) (d4 * Math.sin(d3))) + width;
                float[] fArr = this.d;
                fArr[0] = (float) ((d3 * 180.0d) / 3.141592653589793d);
                fArr[1] = f10 / f4;
                fArr[2] = this.f3104a.f;
                this.c.setColor(Color.HSVToColor(fArr));
                this.c.setAlpha(d());
                this.f3104a.g.drawCircle(cos, sin, max - f3, this.c);
                int i8 = i4;
                if (i8 >= size) {
                    this.b.add(new ColorCircle(cos, sin, this.d));
                } else {
                    this.b.get(i8).a(cos, sin, this.d);
                }
                i4 = i8 + 1;
                i5++;
                i2 = i6;
                f5 = f12;
                min = i7;
            }
            i2++;
            i3 = i4;
            f5 = f5;
            f2 = 2.0f;
        }
    }
}
