package com.flask.colorpicker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

public class ColorPickerPreference extends Preference {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f3093a;
    protected boolean b;
    protected int c = 0;
    protected ColorPickerView.WHEEL_TYPE d;
    protected int e;
    private String f;
    private String g;
    private String h;
    protected ImageView i;

    public ColorPickerPreference(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    /* JADX INFO: finally extract failed */
    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.ColorPickerPreference);
        try {
            this.f3093a = obtainStyledAttributes.getBoolean(R$styleable.ColorPickerPreference_alphaSlider, false);
            this.b = obtainStyledAttributes.getBoolean(R$styleable.ColorPickerPreference_lightnessSlider, false);
            this.e = obtainStyledAttributes.getInt(R$styleable.ColorPickerPreference_density, 10);
            this.d = ColorPickerView.WHEEL_TYPE.a(obtainStyledAttributes.getInt(R$styleable.ColorPickerPreference_wheelType, 0));
            this.c = obtainStyledAttributes.getInt(R$styleable.ColorPickerPreference_initialColor, -1);
            this.f = obtainStyledAttributes.getString(R$styleable.ColorPickerPreference_pickerTitle);
            if (this.f == null) {
                this.f = "Choose color";
            }
            this.g = obtainStyledAttributes.getString(R$styleable.ColorPickerPreference_pickerButtonCancel);
            if (this.g == null) {
                this.g = "cancel";
            }
            this.h = obtainStyledAttributes.getString(R$styleable.ColorPickerPreference_pickerButtonOk);
            if (this.h == null) {
                this.h = "ok";
            }
            obtainStyledAttributes.recycle();
            setWidgetLayoutResource(R$layout.color_widget);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onBindView(View view) {
        int i2;
        super.onBindView(view);
        Resources resources = view.getContext().getResources();
        this.i = (ImageView) view.findViewById(R$id.color_indicator);
        Drawable drawable = this.i.getDrawable();
        GradientDrawable gradientDrawable = (drawable == null || !(drawable instanceof GradientDrawable)) ? null : (GradientDrawable) drawable;
        if (gradientDrawable == null) {
            gradientDrawable = new GradientDrawable();
            gradientDrawable.setShape(1);
        }
        if (isEnabled()) {
            i2 = this.c;
        } else {
            i2 = a(this.c, 0.5f);
        }
        gradientDrawable.setColor(i2);
        gradientDrawable.setStroke((int) TypedValue.applyDimension(1, 1.0f, resources.getDisplayMetrics()), a(i2, 0.8f));
        this.i.setImageDrawable(gradientDrawable);
    }

    /* access modifiers changed from: protected */
    public void onClick() {
        ColorPickerDialogBuilder a2 = ColorPickerDialogBuilder.a(getContext());
        a2.a(this.f);
        a2.b(this.c);
        a2.a(this.d);
        a2.a(this.e);
        a2.a((CharSequence) this.h, (ColorPickerClickListener) new ColorPickerClickListener() {
            public void a(DialogInterface dialogInterface, int i, Integer[] numArr) {
                ColorPickerPreference.this.a(i);
            }
        });
        a2.a((CharSequence) this.g, (DialogInterface.OnClickListener) null);
        if (!this.f3093a && !this.b) {
            a2.d();
        } else if (!this.f3093a) {
            a2.c();
        } else if (!this.b) {
            a2.a();
        }
        a2.b().show();
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean z, Object obj) {
        a(z ? getPersistedInt(0) : ((Integer) obj).intValue());
    }

    public void a(int i2) {
        if (callChangeListener(Integer.valueOf(i2))) {
            this.c = i2;
            persistInt(i2);
            notifyChanged();
        }
    }

    public static int a(int i2, float f2) {
        return Color.argb(Color.alpha(i2), Math.max((int) (((float) Color.red(i2)) * f2), 0), Math.max((int) (((float) Color.green(i2)) * f2), 0), Math.max((int) (((float) Color.blue(i2)) * f2), 0));
    }
}
