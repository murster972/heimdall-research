package com.flask.colorpicker;

import android.graphics.Color;

public class ColorCircle {

    /* renamed from: a  reason: collision with root package name */
    private float f3092a;
    private float b;
    private float[] c = new float[3];
    private float[] d;

    public ColorCircle(float f, float f2, float[] fArr) {
        a(f, f2, fArr);
    }

    public double a(float f, float f2) {
        double d2 = (double) (this.f3092a - f);
        double d3 = (double) (this.b - f2);
        return (d2 * d2) + (d3 * d3);
    }

    public float b() {
        return this.f3092a;
    }

    public float c() {
        return this.b;
    }

    public float[] a() {
        return this.c;
    }

    public float[] a(float f) {
        if (this.d == null) {
            this.d = (float[]) this.c.clone();
        }
        float[] fArr = this.d;
        float[] fArr2 = this.c;
        fArr[0] = fArr2[0];
        fArr[1] = fArr2[1];
        fArr[2] = f;
        return fArr;
    }

    public void a(float f, float f2, float[] fArr) {
        this.f3092a = f;
        this.b = f2;
        float[] fArr2 = this.c;
        fArr2[0] = fArr[0];
        fArr2[1] = fArr[1];
        fArr2[2] = fArr[2];
        Color.HSVToColor(fArr2);
    }
}
