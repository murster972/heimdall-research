package com.flask.colorpicker.slider;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.Utils;
import com.flask.colorpicker.builder.PaintBuilder;

public class AlphaSlider extends AbsCustomSlider {
    public int j;
    private Paint k = PaintBuilder.a().a();
    private Paint l = PaintBuilder.a().a();
    private Paint m = PaintBuilder.a().a();
    private Paint n = PaintBuilder.a().a(-1).a(PorterDuff.Mode.CLEAR).a();
    private ColorPickerView o;

    public AlphaSlider(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        this.k.setShader(PaintBuilder.b(this.h / 2));
    }

    public void setColor(int i) {
        this.j = i;
        this.i = Utils.a(i);
        if (this.c != null) {
            b();
            invalidate();
        }
    }

    public void setColorPicker(ColorPickerView colorPickerView) {
        this.o = colorPickerView;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        int width = canvas.getWidth();
        float height = (float) canvas.getHeight();
        canvas.drawRect(0.0f, 0.0f, (float) width, height, this.k);
        int max = Math.max(2, width / 256);
        int i = 0;
        while (i <= width) {
            float f = (float) i;
            this.l.setColor(this.j);
            this.l.setAlpha(Math.round((f / ((float) (width - 1))) * 255.0f));
            i += max;
            canvas.drawRect(f, 0.0f, (float) i, height, this.l);
        }
    }

    public AlphaSlider(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void a(float f) {
        ColorPickerView colorPickerView = this.o;
        if (colorPickerView != null) {
            colorPickerView.setAlphaValue(f);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, float f2) {
        this.m.setColor(this.j);
        this.m.setAlpha(Math.round(this.i * 255.0f));
        canvas.drawCircle(f, f2, (float) this.g, this.n);
        if (this.i < 1.0f) {
            canvas.drawCircle(f, f2, ((float) this.g) * 0.75f, this.k);
        }
        canvas.drawCircle(f, f2, ((float) this.g) * 0.75f, this.m);
    }
}
