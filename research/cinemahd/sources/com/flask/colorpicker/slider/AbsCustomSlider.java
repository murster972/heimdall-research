package com.flask.colorpicker.slider;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;
import com.flask.colorpicker.R$dimen;

public abstract class AbsCustomSlider extends View {

    /* renamed from: a  reason: collision with root package name */
    protected Bitmap f3106a;
    protected Canvas b;
    protected Bitmap c;
    protected Canvas d;
    protected OnValueChangedListener e;
    protected int f;
    protected int g = 20;
    protected int h = 5;
    protected float i = 1.0f;

    public AbsCustomSlider(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        int width = getWidth();
        int height = getHeight();
        this.c = Bitmap.createBitmap(width - (this.f * 2), this.h, Bitmap.Config.ARGB_8888);
        this.d = new Canvas(this.c);
        Bitmap bitmap = this.f3106a;
        if (bitmap == null || bitmap.getWidth() != width || this.f3106a.getHeight() != height) {
            Bitmap bitmap2 = this.f3106a;
            if (bitmap2 != null) {
                bitmap2.recycle();
            }
            this.f3106a = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            this.b = new Canvas(this.f3106a);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(float f2);

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas, float f2, float f3);

    /* access modifiers changed from: protected */
    public void b() {
        this.g = a(R$dimen.default_slider_handler_radius);
        this.h = a(R$dimen.default_slider_bar_height);
        this.f = this.g;
        if (this.c == null) {
            a();
        }
        a(this.d);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Canvas canvas2;
        super.onDraw(canvas);
        if (this.c != null && (canvas2 = this.b) != null) {
            canvas2.drawColor(0, PorterDuff.Mode.CLEAR);
            this.b.drawBitmap(this.c, (float) this.f, (float) ((getHeight() - this.c.getHeight()) / 2), (Paint) null);
            a(this.b, ((float) this.g) + (this.i * ((float) (getWidth() - (this.g * 2)))), ((float) getHeight()) / 2.0f);
            canvas.drawBitmap(this.f3106a, 0.0f, 0.0f, (Paint) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode != 0) {
            if (mode == Integer.MIN_VALUE) {
                i2 = View.MeasureSpec.getSize(i2);
            } else {
                i2 = mode == 1073741824 ? View.MeasureSpec.getSize(i2) : 0;
            }
        }
        int mode2 = View.MeasureSpec.getMode(i3);
        if (mode2 != 0) {
            if (mode2 == Integer.MIN_VALUE) {
                i3 = View.MeasureSpec.getSize(i3);
            } else {
                i3 = mode2 == 1073741824 ? View.MeasureSpec.getSize(i3) : 0;
            }
        }
        setMeasuredDimension(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        if (r0 != 2) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r4) {
        /*
            r3 = this;
            int r0 = r4.getAction()
            r1 = 1
            if (r0 == 0) goto L_0x001f
            if (r0 == r1) goto L_0x000d
            r2 = 2
            if (r0 == r2) goto L_0x001f
            goto L_0x004c
        L_0x000d:
            float r4 = r3.i
            r3.a((float) r4)
            com.flask.colorpicker.slider.OnValueChangedListener r4 = r3.e
            if (r4 == 0) goto L_0x001b
            float r0 = r3.i
            r4.a(r0)
        L_0x001b:
            r3.invalidate()
            goto L_0x004c
        L_0x001f:
            android.graphics.Bitmap r0 = r3.c
            if (r0 == 0) goto L_0x004c
            float r4 = r4.getX()
            int r0 = r3.f
            float r0 = (float) r0
            float r4 = r4 - r0
            android.graphics.Bitmap r0 = r3.c
            int r0 = r0.getWidth()
            float r0 = (float) r0
            float r4 = r4 / r0
            r3.i = r4
            r4 = 0
            float r0 = r3.i
            r2 = 1065353216(0x3f800000, float:1.0)
            float r0 = java.lang.Math.min(r0, r2)
            float r4 = java.lang.Math.max(r4, r0)
            r3.i = r4
            float r4 = r3.i
            r3.a((float) r4)
            r3.invalidate()
        L_0x004c:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flask.colorpicker.slider.AbsCustomSlider.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setOnValueChangedListener(OnValueChangedListener onValueChangedListener) {
        this.e = onValueChangedListener;
    }

    public AbsCustomSlider(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    /* access modifiers changed from: protected */
    public int a(int i2) {
        return getResources().getDimensionPixelSize(i2);
    }
}
