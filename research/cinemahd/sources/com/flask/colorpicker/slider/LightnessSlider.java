package com.flask.colorpicker.slider;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.Utils;
import com.flask.colorpicker.builder.PaintBuilder;

public class LightnessSlider extends AbsCustomSlider {
    private int j;
    private Paint k = PaintBuilder.a().a();
    private Paint l = PaintBuilder.a().a();
    private Paint m = PaintBuilder.a().a(-1).a(PorterDuff.Mode.CLEAR).a();
    private ColorPickerView n;

    public LightnessSlider(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        float[] fArr = new float[3];
        Color.colorToHSV(this.j, fArr);
        int max = Math.max(2, width / 256);
        int i = 0;
        while (i <= width) {
            float f = (float) i;
            fArr[2] = f / ((float) (width - 1));
            this.k.setColor(Color.HSVToColor(fArr));
            i += max;
            canvas.drawRect(f, 0.0f, (float) i, (float) height, this.k);
        }
    }

    public void setColor(int i) {
        this.j = i;
        this.i = Utils.b(i);
        if (this.c != null) {
            b();
            invalidate();
        }
    }

    public void setColorPicker(ColorPickerView colorPickerView) {
        this.n = colorPickerView;
    }

    public LightnessSlider(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void a(float f) {
        ColorPickerView colorPickerView = this.n;
        if (colorPickerView != null) {
            colorPickerView.setLightness(f);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, float f, float f2) {
        this.l.setColor(Utils.a(this.j, this.i));
        canvas.drawCircle(f, f2, (float) this.g, this.m);
        canvas.drawCircle(f, f2, ((float) this.g) * 0.75f, this.l);
    }
}
