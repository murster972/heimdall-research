package com.battlelancer.seriesguide.api;

import android.os.Bundle;

public class Episode {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f2099a;
    /* access modifiers changed from: private */
    public Integer b;
    /* access modifiers changed from: private */
    public Integer c;
    /* access modifiers changed from: private */
    public Integer d;
    /* access modifiers changed from: private */
    public Integer e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public Integer h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Episode f2100a = new Episode();

        public Builder a(Integer num) {
            Integer unused = this.f2100a.b = num;
            return this;
        }

        public Builder b(Integer num) {
            Integer unused = this.f2100a.c = num;
            return this;
        }

        public Builder c(Integer num) {
            Integer unused = this.f2100a.d = num;
            return this;
        }

        public Builder d(String str) {
            String unused = this.f2100a.g = str;
            return this;
        }

        public Builder e(String str) {
            String unused = this.f2100a.f2099a = str;
            return this;
        }

        public Builder a(String str) {
            String unused = this.f2100a.f = str;
            return this;
        }

        public Builder b(String str) {
            String unused = this.f2100a.j = str;
            return this;
        }

        public Builder c(String str) {
            String unused = this.f2100a.i = str;
            return this;
        }

        public Builder d(Integer num) {
            Integer unused = this.f2100a.h = num;
            return this;
        }

        public Builder e(Integer num) {
            Integer unused = this.f2100a.e = num;
            return this;
        }

        public Episode a() {
            return this.f2100a;
        }
    }

    public String f() {
        return this.g;
    }

    public Integer g() {
        return this.h;
    }

    public Bundle h() {
        Bundle bundle = new Bundle();
        bundle.putString("title", this.f2099a);
        bundle.putInt("number", this.b.intValue());
        bundle.putInt("numberAbsolute", this.c.intValue());
        bundle.putInt("season", this.d.intValue());
        bundle.putInt("tvdbid", this.e.intValue());
        bundle.putString("imdbid", this.f);
        bundle.putString("showTitle", this.g);
        bundle.putInt("showTvdbId", this.h.intValue());
        bundle.putString("showImdbId", this.i);
        bundle.putString("showFirstReleaseDate", this.j);
        return bundle;
    }

    private Episode() {
    }

    public String a() {
        return this.f;
    }

    public Integer b() {
        return this.b;
    }

    public Integer c() {
        return this.d;
    }

    public String d() {
        return this.j;
    }

    public String e() {
        return this.i;
    }

    public static Episode a(Bundle bundle) {
        return new Builder().e(bundle.getString("title")).a(Integer.valueOf(bundle.getInt("number"))).b(Integer.valueOf(bundle.getInt("numberAbsolute"))).c(Integer.valueOf(bundle.getInt("season"))).e(Integer.valueOf(bundle.getInt("tvdbid"))).a(bundle.getString("imdbid")).d(bundle.getString("showTitle")).d(Integer.valueOf(bundle.getInt("showTvdbId"))).c(bundle.getString("showImdbId")).b(bundle.getString("showFirstReleaseDate")).a();
    }
}
