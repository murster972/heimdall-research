package com.battlelancer.seriesguide.api;

import android.os.Bundle;
import com.facebook.common.time.Clock;
import java.util.Date;

public class Movie {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f2101a;
    /* access modifiers changed from: private */
    public Integer b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public Date d;

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Movie f2102a = new Movie();

        public Builder a(Integer num) {
            Integer unused = this.f2102a.b = num;
            return this;
        }

        public Builder b(String str) {
            String unused = this.f2102a.f2101a = str;
            return this;
        }

        public Builder a(String str) {
            String unused = this.f2102a.c = str;
            return this;
        }

        public Builder a(Date date) {
            Date unused = this.f2102a.d = date;
            return this;
        }

        public Movie a() {
            return this.f2102a;
        }
    }

    public String c() {
        return this.f2101a;
    }

    public Integer d() {
        return this.b;
    }

    private Movie() {
    }

    public Date b() {
        return this.d;
    }

    public String a() {
        return this.c;
    }

    public static Movie a(Bundle bundle) {
        long j = bundle.getLong("releaseDate", Clock.MAX_TIME);
        return new Builder().b(bundle.getString("title")).a(Integer.valueOf(bundle.getInt("tmdbid"))).a(bundle.getString("imdbid")).a(j == Clock.MAX_TIME ? null : new Date(j)).a();
    }
}
