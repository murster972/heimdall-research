package com.battlelancer.seriesguide.api;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import java.net.URISyntaxException;
import org.json.JSONException;
import org.json.JSONObject;

public class Action {

    /* renamed from: a  reason: collision with root package name */
    private String f2097a;
    private Intent b;
    private int c;

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private String f2098a;
        private Intent b;
        private int c;

        public Builder(String str, int i) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("Title may not be null or empty.");
            } else if (i > 0) {
                this.f2098a = str;
                this.c = i;
            } else {
                throw new IllegalArgumentException("Entity identifier may not be negative or zero.");
            }
        }

        public Builder a(Intent intent) {
            this.b = intent;
            return this;
        }

        public Action a() {
            return new Action(this.f2098a, this.b, this.c);
        }
    }

    public Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("title", this.f2097a);
        Intent intent = this.b;
        bundle.putString("viewIntent", intent != null ? intent.toUri(1) : null);
        bundle.putInt("entityIdentifier", this.c);
        return bundle;
    }

    public JSONObject b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("title", this.f2097a);
        Intent intent = this.b;
        jSONObject.put("viewIntent", intent != null ? intent.toUri(1) : null);
        jSONObject.put("entityIdentifier", this.c);
        return jSONObject;
    }

    private Action(String str, Intent intent, int i) {
        this.f2097a = str;
        this.b = intent;
        this.c = i;
    }

    public static Action a(JSONObject jSONObject) throws JSONException {
        int optInt;
        String optString = jSONObject.optString("title");
        if (TextUtils.isEmpty(optString) || (optInt = jSONObject.optInt("entityIdentifier")) <= 0) {
            return null;
        }
        Builder builder = new Builder(optString, optInt);
        try {
            String optString2 = jSONObject.optString("viewIntent");
            if (!TextUtils.isEmpty(optString2)) {
                builder.a(Intent.parseUri(optString2, 1));
            }
        } catch (URISyntaxException unused) {
        }
        return builder.a();
    }
}
