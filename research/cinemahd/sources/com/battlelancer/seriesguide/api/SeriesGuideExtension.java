package com.battlelancer.seriesguide.api;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.app.JobIntentService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public abstract class SeriesGuideExtension extends JobIntentService {
    private final String i;
    private SharedPreferences j;
    private Map<ComponentName, String> k;
    private Action l;
    private int m;
    private int n;
    private Handler o = new Handler();

    public SeriesGuideExtension(String str) {
        this.i = str;
    }

    static void b(Context context, Class cls, int i2, Intent intent) {
        JobIntentService.a(context, cls, i2, intent);
    }

    private synchronized void d(ComponentName componentName) {
        if (this.k.size() == 1) {
            g();
        }
        b(componentName);
    }

    @SuppressLint({"LogNotTimber"})
    private synchronized void f(ComponentName componentName) {
        String str = this.k.get(componentName);
        if (TextUtils.isEmpty(str)) {
            Log.w("SeriesGuideExtension", "Not active, canceling update, id=" + this.i);
            return;
        }
        Intent putExtra = new Intent("com.battlelancer.seriesguide.api.action.PUBLISH_ACTION").setComponent(componentName).putExtra("com.battlelancer.seriesguide.api.extra.TOKEN", str).putExtra("com.battlelancer.seriesguide.api.extra.ACTION", this.l != null ? this.l.a() : null).putExtra("com.battlelancer.seriesguide.api.extra.ACTION_TYPE", this.m);
        if (this.n == 2) {
            try {
                getPackageManager().getReceiverInfo(componentName, 0);
                sendBroadcast(putExtra);
            } catch (PackageManager.NameNotFoundException unused) {
                g(componentName);
                return;
            }
        } else if (this.n == 1 && Build.VERSION.SDK_INT < 26) {
            try {
                if (startService(putExtra) == null) {
                    g(componentName);
                }
            } catch (SecurityException e) {
                Log.e("SeriesGuideExtension", "Couldn't publish update, id=" + this.i, e);
            }
        }
        return;
    }

    @SuppressLint({"LogNotTimber"})
    private void g(final ComponentName componentName) {
        Log.e("SeriesGuideExtension", "Update not published because subscriber no longer exists, id=" + this.i);
        this.o.post(new Runnable() {
            public void run() {
                SeriesGuideExtension.this.a(componentName, (String) null);
            }
        });
    }

    @SuppressLint({"LogNotTimber"})
    private void h() {
        String string = this.j.getString("action", (String) null);
        if (string != null) {
            try {
                this.l = Action.a((JSONObject) new JSONTokener(string).nextValue());
            } catch (JSONException e) {
                Log.e("SeriesGuideExtension", "Couldn't deserialize current state, id=" + this.i, e);
            }
        } else {
            this.l = null;
        }
    }

    private synchronized void i() {
        this.k = new HashMap();
        Set<String> stringSet = this.j.getStringSet("subscriptions", (Set) null);
        if (stringSet != null) {
            for (String split : stringSet) {
                String[] split2 = split.split("\\|", 2);
                this.k.put(ComponentName.unflattenFromString(split2[0]), split2[1]);
            }
        }
    }

    private synchronized void j() {
        for (ComponentName f : this.k.keySet()) {
            f(f);
        }
    }

    @SuppressLint({"LogNotTimber"})
    private void k() {
        try {
            this.j.edit().putString("action", this.l.b().toString()).apply();
        } catch (JSONException e) {
            Log.e("SeriesGuideExtension", "Couldn't serialize current state, id=" + this.i, e);
        }
    }

    private synchronized void l() {
        HashSet hashSet = new HashSet();
        for (ComponentName next : this.k.keySet()) {
            hashSet.add(next.flattenToShortString() + "|" + this.k.get(next));
        }
        this.j.edit().putStringSet("subscriptions", hashSet).apply();
    }

    /* access modifiers changed from: protected */
    public void a(int i2, Episode episode) {
    }

    /* access modifiers changed from: protected */
    public void a(int i2, Movie movie) {
    }

    /* access modifiers changed from: protected */
    public boolean a(ComponentName componentName) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void b(ComponentName componentName) {
    }

    /* access modifiers changed from: protected */
    public void c(ComponentName componentName) {
    }

    /* access modifiers changed from: protected */
    public final SharedPreferences e() {
        return a((Context) this, this.i);
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    /* access modifiers changed from: protected */
    public void g() {
    }

    public void onCreate() {
        super.onCreate();
        this.j = e();
        i();
        h();
    }

    protected static SharedPreferences a(Context context, String str) {
        return context.getSharedPreferences("seriesguideextension_" + str, 0);
    }

    private void b(int i2, Bundle bundle, int i3) {
        if (i2 > 0 && bundle != null) {
            this.m = 1;
            this.n = i3;
            a(i2, Movie.a(bundle));
        }
    }

    private synchronized void e(ComponentName componentName) {
        c(componentName);
        if (this.k.size() == 0) {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Action action) {
        this.l = action;
        j();
        k();
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        String action = intent.getAction();
        if ("com.battlelancer.seriesguide.api.action.SUBSCRIBE".equals(action)) {
            a((ComponentName) intent.getParcelableExtra("com.battlelancer.seriesguide.api.extra.SUBSCRIBER_COMPONENT"), intent.getStringExtra("com.battlelancer.seriesguide.api.extra.TOKEN"));
        } else if ("com.battlelancer.seriesguide.api.action.UPDATE".equals(action) && intent.hasExtra("com.battlelancer.seriesguide.api.extra.ENTITY_IDENTIFIER")) {
            int intExtra = intent.getIntExtra("com.battlelancer.seriesguide.api.extra.VERSION", 1);
            if (intent.hasExtra("com.battlelancer.seriesguide.api.extra.EPISODE")) {
                a(intent.getIntExtra("com.battlelancer.seriesguide.api.extra.ENTITY_IDENTIFIER", 0), intent.getBundleExtra("com.battlelancer.seriesguide.api.extra.EPISODE"), intExtra);
            } else if (intent.hasExtra("com.battlelancer.seriesguide.api.extra.MOVIE")) {
                b(intent.getIntExtra("com.battlelancer.seriesguide.api.extra.ENTITY_IDENTIFIER", 0), intent.getBundleExtra("com.battlelancer.seriesguide.api.extra.MOVIE"), intExtra);
            }
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"LogNotTimber"})
    public synchronized void a(ComponentName componentName, String str) {
        if (componentName == null) {
            Log.w("SeriesGuideExtension", "No subscriber given.");
            return;
        }
        String str2 = this.k.get(componentName);
        if (!TextUtils.isEmpty(str)) {
            if (!TextUtils.isEmpty(str2)) {
                this.k.remove(componentName);
                e(componentName);
            }
            if (a(componentName)) {
                this.k.put(componentName, str);
                d(componentName);
            } else {
                return;
            }
        } else if (str2 != null) {
            this.k.remove(componentName);
            e(componentName);
        } else {
            return;
        }
        l();
    }

    private void a(int i2, Bundle bundle, int i3) {
        if (i2 > 0 && bundle != null) {
            this.m = 0;
            this.n = i3;
            a(i2, Episode.a(bundle));
        }
    }
}
