package com.vungle.warren.downloader;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.util.Pair;
import com.facebook.common.time.Clock;
import com.vungle.warren.downloader.AssetDownloadListener;
import com.vungle.warren.utility.FileUtility;
import com.vungle.warren.utility.NetworkProvider;
import com.vungle.warren.utility.PriorityRunnable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLException;
import okhttp3.Cache;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.RealResponseBody;
import okio.GzipSource;
import okio.Okio;
import okio.Source;

@SuppressLint({"LogNotTimber"})
public class AssetDownloader implements Downloader {
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String ACCEPT_RANGES = "Accept-Ranges";
    private static final String BYTES = "bytes";
    private static final int CONNECTION_RETRY_TIMEOUT = 300;
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String CONTENT_RANGE = "Content-Range";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final int DOWNLOAD_CHUNK_SIZE = 2048;
    static final String DOWNLOAD_COMPLETE = "DOWNLOAD_COMPLETE";
    static final String DOWNLOAD_URL = "Download_URL";
    static final String ETAG = "ETag";
    private static final String GZIP = "gzip";
    private static final String IDENTITY = "identity";
    private static final String IF_MODIFIED_SINCE = "If-Modified-Since";
    private static final String IF_NONE_MATCH = "If-None-Match";
    private static final String IF_RANGE = "If-Range";
    static final String LAST_CACHE_VERIFICATION = "Last-Cache-Verification";
    static final String LAST_DOWNLOAD = "Last-Download";
    static final String LAST_MODIFIED = "Last-Modified";
    private static final long MAX_PERCENT = 100;
    private static final int MAX_RECONNECT_ATTEMPTS = 10;
    private static final String META_POSTFIX_EXT = ".vng_meta";
    private static final int PROGRESS_STEP = 5;
    private static final String RANGE = "Range";
    private static final int RANGE_NOT_SATISFIABLE = 416;
    private static final int RETRY_COUNT_ON_CONNECTION_LOST = 5;
    /* access modifiers changed from: private */
    public static final String TAG = AssetDownloader.class.getSimpleName();
    private static final int TIMEOUT = 30;
    public static final long VERIFICATION_WINDOW = TimeUnit.HOURS.toMillis(24);
    private final Object addLock;
    /* access modifiers changed from: private */
    public final DownloaderCache cache;
    private final ExecutorService downloadExecutor;
    private boolean isCacheEnabled;
    int maxReconnectAttempts;
    private Map<String, DownloadRequestMediator> mediators;
    private final NetworkProvider.NetworkListener networkListener;
    private final NetworkProvider networkProvider;
    /* access modifiers changed from: private */
    public final OkHttpClient okHttpClient;
    /* access modifiers changed from: private */
    public volatile int progressStep;
    int reconnectTimeout;
    int retryCountOnConnectionLost;
    private final long timeWindow;
    private List<DownloadRequest> transitioning;
    private final ExecutorService uiExecutor;

    public static abstract class DownloadPriorityRunnable extends PriorityRunnable {
        private final int priority;

        public Integer getPriority() {
            return Integer.valueOf(this.priority);
        }

        private DownloadPriorityRunnable(int i) {
            this.priority = i;
        }
    }

    public @interface NetworkType {
        public static final int ANY = 3;
        public static final int CELLULAR = 1;
        public static final int WIFI = 2;
    }

    public AssetDownloader(int i, NetworkProvider networkProvider2, ExecutorService executorService) {
        this((DownloaderCache) null, 0, i, networkProvider2, executorService);
    }

    private void addNetworkListener() {
        Log.d(TAG, "Adding network listner");
        this.networkProvider.addListener(this.networkListener);
    }

    /* access modifiers changed from: private */
    public void appendHeaders(long j, File file, HashMap<String, String> hashMap, Request.Builder builder) {
        builder.addHeader(ACCEPT_ENCODING, "identity");
        if (file.exists() && !hashMap.isEmpty()) {
            String str = hashMap.get(ETAG);
            String str2 = hashMap.get(LAST_MODIFIED);
            if (Boolean.parseBoolean(hashMap.get(DOWNLOAD_COMPLETE))) {
                if (!TextUtils.isEmpty(str)) {
                    builder.addHeader(IF_NONE_MATCH, str);
                }
                if (!TextUtils.isEmpty(str2)) {
                    builder.addHeader(IF_MODIFIED_SINCE, str2);
                }
            } else if (BYTES.equalsIgnoreCase(hashMap.get(ACCEPT_RANGES))) {
                if (hashMap.get(CONTENT_ENCODING) == null || "identity".equalsIgnoreCase(hashMap.get(CONTENT_ENCODING))) {
                    builder.addHeader(RANGE, "bytes=" + j + "-");
                    if (!TextUtils.isEmpty(str)) {
                        builder.addHeader(IF_RANGE, str);
                    } else if (!TextUtils.isEmpty(str2)) {
                        builder.addHeader(IF_RANGE, str2);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkEncoding(File file, File file2, Headers headers) throws IOException {
        String str = headers.get(CONTENT_ENCODING);
        if (str != null && !GZIP.equalsIgnoreCase(str) && !"identity".equalsIgnoreCase(str)) {
            deleteFileAndMeta(file, file2, false);
            throw new IOException("Unknown Content-Encoding");
        }
    }

    private void copyToDestination(File file, File file2, Pair<DownloadRequest, AssetDownloadListener> pair) {
        FileOutputStream fileOutputStream;
        FileInputStream fileInputStream;
        IOException e;
        if (file2.exists()) {
            file2.delete();
        }
        if (file2.getParentFile() != null && !file2.getParentFile().exists()) {
            file2.getParentFile().mkdirs();
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                fileOutputStream = new FileOutputStream(file2);
                try {
                    FileChannel channel = fileInputStream.getChannel();
                    channel.transferTo(0, channel.size(), fileOutputStream.getChannel());
                    Log.d(TAG, "Copying: finished " + ((DownloadRequest) pair.f598a).url + " copying to " + file2.getPath());
                } catch (IOException e2) {
                    e = e2;
                    try {
                        deliverError((DownloadRequest) pair.f598a, (AssetDownloadListener) pair.b, new AssetDownloadListener.DownloadError(-1, e, 2));
                        Log.d(TAG, "Copying: error" + ((DownloadRequest) pair.f598a).url + " copying to " + file2.getPath());
                        FileUtility.closeQuietly(fileInputStream);
                        FileUtility.closeQuietly(fileOutputStream);
                    } catch (Throwable th) {
                        th = th;
                    }
                }
            } catch (IOException e3) {
                e = e3;
                fileOutputStream = null;
                deliverError((DownloadRequest) pair.f598a, (AssetDownloadListener) pair.b, new AssetDownloadListener.DownloadError(-1, e, 2));
                Log.d(TAG, "Copying: error" + ((DownloadRequest) pair.f598a).url + " copying to " + file2.getPath());
                FileUtility.closeQuietly(fileInputStream);
                FileUtility.closeQuietly(fileOutputStream);
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                FileUtility.closeQuietly(fileInputStream);
                FileUtility.closeQuietly(fileOutputStream);
                throw th;
            }
        } catch (IOException e4) {
            fileInputStream = null;
            e = e4;
            fileOutputStream = null;
            deliverError((DownloadRequest) pair.f598a, (AssetDownloadListener) pair.b, new AssetDownloadListener.DownloadError(-1, e, 2));
            Log.d(TAG, "Copying: error" + ((DownloadRequest) pair.f598a).url + " copying to " + file2.getPath());
            FileUtility.closeQuietly(fileInputStream);
            FileUtility.closeQuietly(fileOutputStream);
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream = null;
            fileInputStream = null;
            FileUtility.closeQuietly(fileInputStream);
            FileUtility.closeQuietly(fileOutputStream);
            throw th;
        }
        FileUtility.closeQuietly(fileInputStream);
        FileUtility.closeQuietly(fileOutputStream);
    }

    /* access modifiers changed from: private */
    public String debugString(DownloadRequestMediator downloadRequestMediator) {
        return ", mediator url - " + downloadRequestMediator.url + ", path - " + downloadRequestMediator.filePath + ", th - " + Thread.currentThread().getName() + "id " + downloadRequestMediator;
    }

    /* access modifiers changed from: private */
    public ResponseBody decodeGzipIfNeeded(Response response) {
        if (!GZIP.equalsIgnoreCase(response.header(CONTENT_ENCODING)) || !HttpHeaders.hasBody(response) || response.body() == null) {
            return response.body();
        }
        return new RealResponseBody(response.header("Content-Type"), -1, Okio.a((Source) new GzipSource(response.body().source())));
    }

    /* access modifiers changed from: private */
    public void deleteFileAndMeta(File file, File file2, boolean z) {
        if (file != null) {
            file.delete();
            if (file2 != null) {
                file2.delete();
            }
            if (this.cache != null && isCacheEnabled()) {
                if (z) {
                    this.cache.deleteAndRemove(file);
                } else {
                    this.cache.deleteContents(file);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void deliverError(final DownloadRequest downloadRequest, final AssetDownloadListener assetDownloadListener, final AssetDownloadListener.DownloadError downloadError) {
        if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    assetDownloadListener.onError(downloadError, downloadRequest);
                }
            });
        }
    }

    private void deliverProgress(final AssetDownloadListener.Progress progress, final DownloadRequest downloadRequest, final AssetDownloadListener assetDownloadListener) {
        if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    String access$200 = AssetDownloader.TAG;
                    Log.d(access$200, "On progress " + downloadRequest);
                    assetDownloadListener.onProgress(progress, downloadRequest);
                }
            });
        }
    }

    private void deliverSuccess(Pair<DownloadRequest, AssetDownloadListener> pair, File file) {
        S s = pair.b;
        if (s != null) {
            ((AssetDownloadListener) s).onSuccess(file, (DownloadRequest) pair.f598a);
        }
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> extractMeta(File file) {
        return FileUtility.readMap(file.getPath());
    }

    private DownloadRequestMediator findMediatordForCancelation(DownloadRequest downloadRequest) {
        ArrayList<DownloadRequestMediator> arrayList = new ArrayList<>(2);
        arrayList.add(this.mediators.get(getCacheableKey(downloadRequest)));
        arrayList.add(this.mediators.get(getNonCacheableKey(downloadRequest)));
        for (DownloadRequestMediator downloadRequestMediator : arrayList) {
            if (downloadRequestMediator != null) {
                for (DownloadRequest equals : downloadRequestMediator.requests()) {
                    if (equals.equals(downloadRequest)) {
                        return downloadRequestMediator;
                    }
                }
                continue;
            }
        }
        return null;
    }

    private String getCacheableKey(DownloadRequest downloadRequest) {
        return downloadRequest.url;
    }

    /* access modifiers changed from: private */
    public long getContentLength(Response response) {
        if (response == null) {
            return -1;
        }
        String str = response.headers().get("Content-Length");
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (Throwable unused) {
            return -1;
        }
    }

    private String getNonCacheableKey(DownloadRequest downloadRequest) {
        return downloadRequest.url + " " + downloadRequest.path;
    }

    /* access modifiers changed from: private */
    public boolean isAnyConnected(DownloadRequestMediator downloadRequestMediator) {
        for (DownloadRequest next : downloadRequestMediator.requests()) {
            if (next == null) {
                Log.d(TAG, "Request is null");
            } else if (isConnected(next)) {
                return true;
            }
        }
        return false;
    }

    @TargetApi(21)
    private boolean isConnected(DownloadRequest downloadRequest) {
        int i;
        int currentNetworkType = this.networkProvider.getCurrentNetworkType();
        boolean z = true;
        if (currentNetworkType >= 0 && downloadRequest.networkType == 3) {
            return true;
        }
        if (currentNetworkType != 0) {
            if (currentNetworkType != 1) {
                if (currentNetworkType != 4) {
                    if (currentNetworkType != 9) {
                        if (currentNetworkType != 17) {
                            if (currentNetworkType != 6) {
                                if (currentNetworkType != 7) {
                                    i = -1;
                                    if (i <= 0 || (downloadRequest.networkType & i) != i) {
                                        z = false;
                                    }
                                    String str = TAG;
                                    Log.d(str, "checking pause for type: " + currentNetworkType + " connected " + z + debugString(downloadRequest));
                                    return z;
                                }
                            }
                        }
                    }
                }
            }
            i = 2;
            z = false;
            String str2 = TAG;
            Log.d(str2, "checking pause for type: " + currentNetworkType + " connected " + z + debugString(downloadRequest));
            return z;
        }
        i = 1;
        z = false;
        String str22 = TAG;
        Log.d(str22, "checking pause for type: " + currentNetworkType + " connected " + z + debugString(downloadRequest));
        return z;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r1.lock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0070, code lost:
        monitor-enter(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r7.transitioning.remove(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007b, code lost:
        if (r1.is(6) != false) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0081, code lost:
        if (r1.is(3) == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0087, code lost:
        if (r8.isCancelled() != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008c, code lost:
        if (r1.isCacheable == false) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008e, code lost:
        r1.add(r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0096, code lost:
        if (r1.is(2) == false) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0098, code lost:
        load(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009c, code lost:
        deliverError(r8, r9, new com.vungle.warren.downloader.AssetDownloadListener.DownloadError(-1, new java.lang.IllegalArgumentException("DownloadRequest is already running"), 1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ac, code lost:
        r8 = makeNewMediator(r8, r9);
        r7.mediators.put(r1.key, r8);
        load(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ba, code lost:
        monitor-exit(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r1.unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00bf, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c3, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r1.unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00c7, code lost:
        throw r8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void launchRequest(com.vungle.warren.downloader.DownloadRequest r8, com.vungle.warren.downloader.AssetDownloadListener r9) throws java.io.IOException {
        /*
            r7 = this;
            java.lang.Object r0 = r7.addLock
            monitor-enter(r0)
            monitor-enter(r7)     // Catch:{ all -> 0x00cb }
            boolean r1 = r8.isCancelled()     // Catch:{ all -> 0x00c8 }
            r2 = 1
            r3 = -1
            r4 = 3
            if (r1 == 0) goto L_0x0048
            java.util.List<com.vungle.warren.downloader.DownloadRequest> r1 = r7.transitioning     // Catch:{ all -> 0x00c8 }
            r1.remove(r8)     // Catch:{ all -> 0x00c8 }
            java.lang.String r1 = TAG     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r5.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r6 = "Request "
            r5.append(r6)     // Catch:{ all -> 0x00c8 }
            java.lang.String r6 = r8.url     // Catch:{ all -> 0x00c8 }
            r5.append(r6)     // Catch:{ all -> 0x00c8 }
            java.lang.String r6 = " is cancelled before starting"
            r5.append(r6)     // Catch:{ all -> 0x00c8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00c8 }
            android.util.Log.d(r1, r5)     // Catch:{ all -> 0x00c8 }
            com.vungle.warren.downloader.AssetDownloadListener$Progress r1 = new com.vungle.warren.downloader.AssetDownloadListener$Progress     // Catch:{ all -> 0x00c8 }
            r1.<init>()     // Catch:{ all -> 0x00c8 }
            r1.status = r4     // Catch:{ all -> 0x00c8 }
            com.vungle.warren.downloader.AssetDownloadListener$DownloadError r1 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x00c8 }
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x00c8 }
            java.lang.String r5 = "Cancelled"
            r4.<init>(r5)     // Catch:{ all -> 0x00c8 }
            r1.<init>(r3, r4, r2)     // Catch:{ all -> 0x00c8 }
            r7.deliverError(r8, r9, r1)     // Catch:{ all -> 0x00c8 }
            monitor-exit(r7)     // Catch:{ all -> 0x00c8 }
            monitor-exit(r0)     // Catch:{ all -> 0x00cb }
            return
        L_0x0048:
            java.util.Map<java.lang.String, com.vungle.warren.downloader.DownloadRequestMediator> r1 = r7.mediators     // Catch:{ all -> 0x00c8 }
            java.lang.String r5 = r7.mediatorKeyFromRequest(r8)     // Catch:{ all -> 0x00c8 }
            java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x00c8 }
            com.vungle.warren.downloader.DownloadRequestMediator r1 = (com.vungle.warren.downloader.DownloadRequestMediator) r1     // Catch:{ all -> 0x00c8 }
            if (r1 != 0) goto L_0x006c
            java.util.List<com.vungle.warren.downloader.DownloadRequest> r1 = r7.transitioning     // Catch:{ all -> 0x00c8 }
            r1.remove(r8)     // Catch:{ all -> 0x00c8 }
            com.vungle.warren.downloader.DownloadRequestMediator r8 = r7.makeNewMediator(r8, r9)     // Catch:{ all -> 0x00c8 }
            java.util.Map<java.lang.String, com.vungle.warren.downloader.DownloadRequestMediator> r9 = r7.mediators     // Catch:{ all -> 0x00c8 }
            java.lang.String r1 = r8.key     // Catch:{ all -> 0x00c8 }
            r9.put(r1, r8)     // Catch:{ all -> 0x00c8 }
            r7.load(r8)     // Catch:{ all -> 0x00c8 }
            monitor-exit(r7)     // Catch:{ all -> 0x00c8 }
            monitor-exit(r0)     // Catch:{ all -> 0x00cb }
            return
        L_0x006c:
            monitor-exit(r7)     // Catch:{ all -> 0x00c8 }
            r1.lock()     // Catch:{ all -> 0x00c3 }
            monitor-enter(r7)     // Catch:{ all -> 0x00c3 }
            java.util.List<com.vungle.warren.downloader.DownloadRequest> r5 = r7.transitioning     // Catch:{ all -> 0x00c0 }
            r5.remove(r8)     // Catch:{ all -> 0x00c0 }
            r5 = 6
            boolean r5 = r1.is(r5)     // Catch:{ all -> 0x00c0 }
            if (r5 != 0) goto L_0x00ac
            boolean r4 = r1.is(r4)     // Catch:{ all -> 0x00c0 }
            if (r4 == 0) goto L_0x008a
            boolean r4 = r8.isCancelled()     // Catch:{ all -> 0x00c0 }
            if (r4 != 0) goto L_0x008a
            goto L_0x00ac
        L_0x008a:
            boolean r4 = r1.isCacheable     // Catch:{ all -> 0x00c0 }
            if (r4 == 0) goto L_0x009c
            r1.add(r8, r9)     // Catch:{ all -> 0x00c0 }
            r8 = 2
            boolean r8 = r1.is(r8)     // Catch:{ all -> 0x00c0 }
            if (r8 == 0) goto L_0x00ba
            r7.load(r1)     // Catch:{ all -> 0x00c0 }
            goto L_0x00ba
        L_0x009c:
            com.vungle.warren.downloader.AssetDownloadListener$DownloadError r4 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x00c0 }
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00c0 }
            java.lang.String r6 = "DownloadRequest is already running"
            r5.<init>(r6)     // Catch:{ all -> 0x00c0 }
            r4.<init>(r3, r5, r2)     // Catch:{ all -> 0x00c0 }
            r7.deliverError(r8, r9, r4)     // Catch:{ all -> 0x00c0 }
            goto L_0x00ba
        L_0x00ac:
            com.vungle.warren.downloader.DownloadRequestMediator r8 = r7.makeNewMediator(r8, r9)     // Catch:{ all -> 0x00c0 }
            java.util.Map<java.lang.String, com.vungle.warren.downloader.DownloadRequestMediator> r9 = r7.mediators     // Catch:{ all -> 0x00c0 }
            java.lang.String r2 = r1.key     // Catch:{ all -> 0x00c0 }
            r9.put(r2, r8)     // Catch:{ all -> 0x00c0 }
            r7.load(r8)     // Catch:{ all -> 0x00c0 }
        L_0x00ba:
            monitor-exit(r7)     // Catch:{ all -> 0x00c0 }
            r1.unlock()     // Catch:{ all -> 0x00cb }
            monitor-exit(r0)     // Catch:{ all -> 0x00cb }
            return
        L_0x00c0:
            r8 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00c0 }
            throw r8     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            r8 = move-exception
            r1.unlock()     // Catch:{ all -> 0x00cb }
            throw r8     // Catch:{ all -> 0x00cb }
        L_0x00c8:
            r8 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00c8 }
            throw r8     // Catch:{ all -> 0x00cb }
        L_0x00cb:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00cb }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.launchRequest(com.vungle.warren.downloader.DownloadRequest, com.vungle.warren.downloader.AssetDownloadListener):void");
    }

    private synchronized void load(final DownloadRequestMediator downloadRequestMediator) {
        addNetworkListener();
        downloadRequestMediator.set(1);
        this.downloadExecutor.execute(new DownloadPriorityRunnable(downloadRequestMediator.getPriority()) {
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r23v1, resolved type: okhttp3.Call} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v1, resolved type: java.lang.String} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v4, resolved type: okio.BufferedSource} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r23v4, resolved type: okhttp3.Call} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v6, resolved type: okio.BufferedSource} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r23v5, resolved type: java.io.Closeable} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r20v7, resolved type: okhttp3.Request$Builder} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v8, resolved type: okio.BufferedSource} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r23v6, resolved type: okhttp3.Call} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v16, resolved type: long} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v16, resolved type: okio.BufferedSource} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v18, resolved type: okio.BufferedSource} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v35, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v19, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v20, resolved type: com.vungle.warren.downloader.AssetDownloadListener$DownloadError} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v36, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v20, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r20v17, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v24, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v40, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v26, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v42, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v10, resolved type: okio.BufferedSink} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v28, resolved type: okio.BufferedSource} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v49, resolved type: long} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r23v40, resolved type: okhttp3.Call} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v65, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v31, resolved type: okio.BufferedSource} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v55, resolved type: long} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r23v49, resolved type: int} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r20v24, resolved type: okhttp3.Request$Builder} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r20v26, resolved type: okhttp3.Request$Builder} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v55, resolved type: com.vungle.warren.downloader.AssetDownloadListener$DownloadError} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v57, resolved type: com.vungle.warren.downloader.AssetDownloadListener$DownloadError} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v32, resolved type: okio.BufferedSource} */
            /* JADX WARNING: type inference failed for: r20v14 */
            /* JADX WARNING: type inference failed for: r8v57 */
            /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
                java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
                	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
                	at java.util.ArrayList.get(ArrayList.java:435)
                	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
                	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMaker.processLoop(RegionMaker.java:225)
                	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:106)
                	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
                	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
                */
            /* JADX WARNING: Multi-variable type inference failed */
            public void run() {
                /*
                    r30 = this;
                    r1 = r30
                    com.vungle.warren.downloader.AssetDownloadListener$Progress r2 = new com.vungle.warren.downloader.AssetDownloadListener$Progress
                    r2.<init>()
                    long r3 = java.lang.System.currentTimeMillis()
                    r2.timestampDownloadStart = r3
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    java.lang.String r3 = r0.url
                    java.lang.String r4 = r0.filePath
                    java.lang.String r0 = r0.metaPath
                    java.io.File r11 = new java.io.File
                    r11.<init>(r4)
                    java.io.File r4 = new java.io.File
                    r4.<init>(r0)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    if (r0 == 0) goto L_0x0036
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    boolean r0 = r0.isCacheable
                    if (r0 == 0) goto L_0x0036
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.startTracking(r11)
                L_0x0036:
                    r0 = 0
                    r10 = 0
                    r14 = 0
                    r15 = 0
                L_0x003a:
                    if (r0 != 0) goto L_0x0d2a
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "Start load: url: "
                    r5.append(r6)
                    r5.append(r3)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                    r8 = 2
                    r6 = 5
                    r7 = 3
                    r5 = 4
                    r13 = 1
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x08f0 }
                    boolean r0 = r0.is(r13)     // Catch:{ all -> 0x08f0 }
                    if (r0 != 0) goto L_0x0157
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0144 }
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0144 }
                    r9.<init>()     // Catch:{ all -> 0x0144 }
                    java.lang.String r13 = "Abort download, wrong state "
                    r9.append(r13)     // Catch:{ all -> 0x0144 }
                    com.vungle.warren.downloader.AssetDownloader r13 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0144 }
                    com.vungle.warren.downloader.DownloadRequestMediator r12 = r4     // Catch:{ all -> 0x0144 }
                    java.lang.String r12 = r13.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r12)     // Catch:{ all -> 0x0144 }
                    r9.append(r12)     // Catch:{ all -> 0x0144 }
                    java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0144 }
                    android.util.Log.w(r0, r9)     // Catch:{ all -> 0x0144 }
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "request is done "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    int r0 = r0.getStatus()
                    if (r0 == r8) goto L_0x00d0
                    if (r0 == r7) goto L_0x00c9
                    if (r0 == r5) goto L_0x00c1
                    if (r0 == r6) goto L_0x00b9
                    if (r14 != 0) goto L_0x00d0
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.removeMediator(r2)
                    goto L_0x00d0
                L_0x00b9:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onErrorMediator(r15, r2)
                    goto L_0x00d0
                L_0x00c1:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onSuccessMediator(r11, r2)
                    goto L_0x00d0
                L_0x00c9:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onCancelledMediator(r2)
                L_0x00d0:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "Done with request in state "
                    r2.append(r3)
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    int r3 = r3.getStatus()
                    r2.append(r3)
                    java.lang.String r3 = " "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r9)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0141 }
                    r0.removeNetworkListener()     // Catch:{ all -> 0x0141 }
                    monitor-exit(r9)     // Catch:{ all -> 0x0141 }
                    r2 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    if (r0 == 0) goto L_0x0140
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    boolean r0 = r0.isCacheable
                    if (r0 == 0) goto L_0x0140
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.stopTracking(r11)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    boolean r0 = r0.isCacheEnabled()
                    if (r0 != 0) goto L_0x0137
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.clear()
                    goto L_0x0140
                L_0x0137:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.purge()
                L_0x0140:
                    return
                L_0x0141:
                    r0 = move-exception
                    monitor-exit(r9)     // Catch:{ all -> 0x0141 }
                    throw r0
                L_0x0144:
                    r0 = move-exception
                    r21 = r3
                    r27 = r14
                    r12 = r15
                    r5 = 2
                    r6 = 0
                L_0x014c:
                    r8 = 5
                    r9 = -1
                    r14 = 0
                    r17 = 0
                    r23 = 0
                    r15 = r10
                L_0x0154:
                    r10 = 0
                    goto L_0x0904
                L_0x0157:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08f0 }
                    com.vungle.warren.downloader.DownloadRequestMediator r9 = r4     // Catch:{ all -> 0x08f0 }
                    boolean r0 = r0.isAnyConnected(r9)     // Catch:{ all -> 0x08f0 }
                    if (r0 == 0) goto L_0x08d3
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x08f0 }
                    r9 = 1
                    r0.setConnected(r9)     // Catch:{ all -> 0x08f0 }
                    java.io.File r0 = r11.getParentFile()     // Catch:{ all -> 0x08f0 }
                    if (r0 == 0) goto L_0x017e
                    java.io.File r0 = r11.getParentFile()     // Catch:{ all -> 0x0144 }
                    boolean r0 = r0.exists()     // Catch:{ all -> 0x0144 }
                    if (r0 != 0) goto L_0x017e
                    java.io.File r0 = r11.getParentFile()     // Catch:{ all -> 0x0144 }
                    r0.mkdirs()     // Catch:{ all -> 0x0144 }
                L_0x017e:
                    boolean r0 = r11.exists()     // Catch:{ all -> 0x08f0 }
                    if (r0 == 0) goto L_0x018b
                    long r18 = r11.length()     // Catch:{ all -> 0x0144 }
                    r12 = r18
                    goto L_0x018d
                L_0x018b:
                    r12 = 0
                L_0x018d:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x08f0 }
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x08f0 }
                    r9.<init>()     // Catch:{ all -> 0x08f0 }
                    java.lang.String r6 = "already downloaded : "
                    r9.append(r6)     // Catch:{ all -> 0x08f0 }
                    r9.append(r12)     // Catch:{ all -> 0x08f0 }
                    java.lang.String r6 = ", file exists = "
                    r9.append(r6)     // Catch:{ all -> 0x08f0 }
                    boolean r6 = r11.exists()     // Catch:{ all -> 0x08f0 }
                    r9.append(r6)     // Catch:{ all -> 0x08f0 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08f0 }
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4     // Catch:{ all -> 0x08c8 }
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r7)     // Catch:{ all -> 0x08c8 }
                    r9.append(r6)     // Catch:{ all -> 0x08c8 }
                    java.lang.String r6 = r9.toString()     // Catch:{ all -> 0x08c8 }
                    android.util.Log.d(r0, r6)     // Catch:{ all -> 0x08c8 }
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08c8 }
                    java.util.HashMap r0 = r0.extractMeta(r4)     // Catch:{ all -> 0x08c8 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08c8 }
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4     // Catch:{ all -> 0x08c8 }
                    boolean r6 = r6.useCacheWithoutVerification(r7, r11, r0)     // Catch:{ all -> 0x08c8 }
                    if (r6 == 0) goto L_0x02aa
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x029f }
                    r0.set(r5)     // Catch:{ all -> 0x029f }
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x029f }
                    java.lang.String r6 = "Using cache without verification, dispatch existing file"
                    android.util.Log.d(r0, r6)     // Catch:{ all -> 0x029f }
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "request is done "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    int r0 = r0.getStatus()
                    if (r0 == r8) goto L_0x022b
                    r6 = 3
                    if (r0 == r6) goto L_0x0224
                    if (r0 == r5) goto L_0x021c
                    r7 = 5
                    if (r0 == r7) goto L_0x0214
                    if (r14 != 0) goto L_0x022b
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.removeMediator(r2)
                    goto L_0x022b
                L_0x0214:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onErrorMediator(r15, r2)
                    goto L_0x022b
                L_0x021c:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onSuccessMediator(r11, r2)
                    goto L_0x022b
                L_0x0224:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onCancelledMediator(r2)
                L_0x022b:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "Done with request in state "
                    r2.append(r3)
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    int r3 = r3.getStatus()
                    r2.append(r3)
                    java.lang.String r3 = " "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r9)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x029c }
                    r0.removeNetworkListener()     // Catch:{ all -> 0x029c }
                    monitor-exit(r9)     // Catch:{ all -> 0x029c }
                    r2 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    if (r0 == 0) goto L_0x029b
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    boolean r0 = r0.isCacheable
                    if (r0 == 0) goto L_0x029b
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.stopTracking(r11)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    boolean r0 = r0.isCacheEnabled()
                    if (r0 != 0) goto L_0x0292
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.clear()
                    goto L_0x029b
                L_0x0292:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.purge()
                L_0x029b:
                    return
                L_0x029c:
                    r0 = move-exception
                    monitor-exit(r9)     // Catch:{ all -> 0x029c }
                    throw r0
                L_0x029f:
                    r0 = move-exception
                    r21 = r3
                    r27 = r14
                    r12 = r15
                    r5 = 2
                    r6 = 0
                    r7 = 3
                    goto L_0x014c
                L_0x02aa:
                    r6 = 3
                    r7 = 5
                    okhttp3.Request$Builder r9 = new okhttp3.Request$Builder     // Catch:{ all -> 0x08c8 }
                    r9.<init>()     // Catch:{ all -> 0x08c8 }
                    okhttp3.Request$Builder r20 = r9.url((java.lang.String) r3)     // Catch:{ all -> 0x08c8 }
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08c8 }
                    r26 = r15
                    r15 = 4
                    r5 = r9
                    r9 = 5
                    r6 = r12
                    r8 = r11
                    r9 = r0
                    r15 = r10
                    r10 = r20
                    r5.appendHeaders(r6, r8, r9, r10)     // Catch:{ all -> 0x08b4 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08b4 }
                    okhttp3.OkHttpClient r5 = r5.okHttpClient     // Catch:{ all -> 0x08b4 }
                    okhttp3.Request r6 = r20.build()     // Catch:{ all -> 0x08b4 }
                    okhttp3.Call r5 = r5.newCall(r6)     // Catch:{ all -> 0x08b4 }
                    okhttp3.Response r6 = r5.execute()     // Catch:{ all -> 0x08a0 }
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x088f }
                    long r7 = r7.getContentLength(r6)     // Catch:{ all -> 0x088f }
                    java.lang.String r9 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x088f }
                    java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x088f }
                    r10.<init>()     // Catch:{ all -> 0x088f }
                    r27 = r14
                    java.lang.String r14 = "Response code: "
                    r10.append(r14)     // Catch:{ all -> 0x0889 }
                    int r14 = r6.code()     // Catch:{ all -> 0x0889 }
                    r10.append(r14)     // Catch:{ all -> 0x0889 }
                    java.lang.String r14 = " "
                    r10.append(r14)     // Catch:{ all -> 0x0889 }
                    com.vungle.warren.downloader.DownloadRequestMediator r14 = r4     // Catch:{ all -> 0x0889 }
                    r10.append(r14)     // Catch:{ all -> 0x0889 }
                    java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0889 }
                    android.util.Log.d(r9, r10)     // Catch:{ all -> 0x0889 }
                    int r9 = r6.code()     // Catch:{ all -> 0x0889 }
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0877 }
                    com.vungle.warren.downloader.DownloadRequestMediator r14 = r4     // Catch:{ all -> 0x0877 }
                    boolean r10 = r10.notModified(r11, r6, r14, r0)     // Catch:{ all -> 0x0877 }
                    if (r10 != 0) goto L_0x0745
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0733 }
                    com.vungle.warren.downloader.DownloadRequestMediator r14 = r4     // Catch:{ all -> 0x0733 }
                    boolean r10 = r10.useCacheOnFail(r14, r11, r0, r9)     // Catch:{ all -> 0x0733 }
                    if (r10 == 0) goto L_0x031f
                    goto L_0x0745
                L_0x031f:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0733 }
                    com.vungle.warren.downloader.DownloadRequestMediator r10 = r4     // Catch:{ all -> 0x0733 }
                    r20 = r0
                    r21 = r12
                    r23 = r9
                    r24 = r6
                    r25 = r10
                    boolean r0 = r20.partialMalformed(r21, r23, r24, r25)     // Catch:{ all -> 0x0733 }
                    if (r0 == 0) goto L_0x03ce
                    int r10 = r15 + 1
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x03c6 }
                    int r0 = r0.maxReconnectAttempts     // Catch:{ all -> 0x03c6 }
                    if (r15 >= r0) goto L_0x03af
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x03c6 }
                    r7 = 0
                    r0.deleteFileAndMeta(r11, r4, r7)     // Catch:{ all -> 0x03c6 }
                    if (r6 == 0) goto L_0x0350
                    okhttp3.ResponseBody r0 = r6.body()
                    if (r0 == 0) goto L_0x0350
                    okhttp3.ResponseBody r0 = r6.body()
                    r0.close()
                L_0x0350:
                    if (r5 == 0) goto L_0x0355
                    r5.cancel()
                L_0x0355:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "request is done "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r7)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "Not removing connections and listener "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r7)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r7)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x03ac }
                    r0.removeNetworkListener()     // Catch:{ all -> 0x03ac }
                    monitor-exit(r7)     // Catch:{ all -> 0x03ac }
                    r5 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r5)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r5)
                    r15 = r26
                    r14 = r27
                    r0 = 0
                    goto L_0x003a
                L_0x03ac:
                    r0 = move-exception
                    monitor-exit(r7)     // Catch:{ all -> 0x03ac }
                    throw r0
                L_0x03af:
                    com.vungle.warren.downloader.Downloader$RequestException r0 = new com.vungle.warren.downloader.Downloader$RequestException     // Catch:{ all -> 0x03c6 }
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c6 }
                    r7.<init>()     // Catch:{ all -> 0x03c6 }
                    java.lang.String r8 = "Code: "
                    r7.append(r8)     // Catch:{ all -> 0x03c6 }
                    r7.append(r9)     // Catch:{ all -> 0x03c6 }
                    java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x03c6 }
                    r0.<init>(r7)     // Catch:{ all -> 0x03c6 }
                    throw r0     // Catch:{ all -> 0x03c6 }
                L_0x03c6:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    r15 = r10
                    goto L_0x073a
                L_0x03ce:
                    boolean r0 = r6.isSuccessful()     // Catch:{ all -> 0x0733 }
                    if (r0 == 0) goto L_0x0709
                    r0 = 206(0xce, float:2.89E-43)
                    if (r9 == r0) goto L_0x03e8
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x03e1 }
                    r10 = 0
                    r0.deleteFileAndMeta(r11, r4, r10)     // Catch:{ all -> 0x03e1 }
                    r12 = 0
                    goto L_0x03e8
                L_0x03e1:
                    r0 = move-exception
                L_0x03e2:
                    r21 = r3
                    r23 = r5
                    goto L_0x073a
                L_0x03e8:
                    r4.delete()     // Catch:{ all -> 0x0701 }
                    okhttp3.Headers r0 = r6.headers()     // Catch:{ all -> 0x0701 }
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0701 }
                    r10.checkEncoding(r11, r4, r0)     // Catch:{ all -> 0x0701 }
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0701 }
                    java.util.HashMap r0 = r10.makeMeta(r4, r0, r3)     // Catch:{ all -> 0x0701 }
                    boolean r10 = okhttp3.internal.http.HttpHeaders.hasBody(r6)     // Catch:{ all -> 0x0701 }
                    if (r10 == 0) goto L_0x06e6
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0701 }
                    com.vungle.warren.downloader.DownloaderCache r10 = r10.cache     // Catch:{ all -> 0x0701 }
                    if (r10 == 0) goto L_0x0425
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0421 }
                    com.vungle.warren.downloader.DownloaderCache r10 = r10.cache     // Catch:{ all -> 0x0421 }
                    r20 = r15
                    long r14 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0418 }
                    r10.setCacheLastUpdateTimestamp(r11, r14)     // Catch:{ all -> 0x0418 }
                    goto L_0x0427
                L_0x0418:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    r15 = r20
                    goto L_0x073a
                L_0x0421:
                    r0 = move-exception
                    r20 = r15
                    goto L_0x03e2
                L_0x0425:
                    r20 = r15
                L_0x0427:
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06d8 }
                    okhttp3.ResponseBody r10 = r10.decodeGzipIfNeeded(r6)     // Catch:{ all -> 0x06d8 }
                    okio.BufferedSource r14 = r10.source()     // Catch:{ all -> 0x06d8 }
                    java.lang.String r15 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06c7 }
                    r21 = r3
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x06c5 }
                    r3.<init>()     // Catch:{ all -> 0x06c5 }
                    r22 = r9
                    java.lang.String r9 = "Start download from bytes: "
                    r3.append(r9)     // Catch:{ all -> 0x06b8 }
                    r3.append(r12)     // Catch:{ all -> 0x06b8 }
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06b8 }
                    r23 = r5
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4     // Catch:{ all -> 0x06b6 }
                    java.lang.String r5 = r9.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r5)     // Catch:{ all -> 0x06b6 }
                    r3.append(r5)     // Catch:{ all -> 0x06b6 }
                    java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x06b6 }
                    android.util.Log.d(r15, r3)     // Catch:{ all -> 0x06b6 }
                    long r7 = r7 + r12
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06b6 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x06b6 }
                    r5.<init>()     // Catch:{ all -> 0x06b6 }
                    java.lang.String r9 = "final offset = "
                    r5.append(r9)     // Catch:{ all -> 0x06b6 }
                    r5.append(r12)     // Catch:{ all -> 0x06b6 }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x06b6 }
                    android.util.Log.d(r3, r5)     // Catch:{ all -> 0x06b6 }
                    r18 = 0
                    int r3 = (r12 > r18 ? 1 : (r12 == r18 ? 0 : -1))
                    if (r3 != 0) goto L_0x048a
                    okio.Sink r3 = okio.Okio.b((java.io.File) r11)     // Catch:{ all -> 0x047e }
                    goto L_0x048e
                L_0x047e:
                    r0 = move-exception
                    r15 = r20
                    r9 = r22
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    goto L_0x06d5
                L_0x048a:
                    okio.Sink r3 = okio.Okio.a((java.io.File) r11)     // Catch:{ all -> 0x06b6 }
                L_0x048e:
                    okio.BufferedSink r3 = okio.Okio.a((okio.Sink) r3)     // Catch:{ all -> 0x06b6 }
                    r5 = 0
                    r2.status = r5     // Catch:{ all -> 0x06a8 }
                    long r9 = r10.contentLength()     // Catch:{ all -> 0x06a8 }
                    r2.sizeBytes = r9     // Catch:{ all -> 0x06a8 }
                    r2.startBytes = r12     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.DownloadRequestMediator r9 = r4     // Catch:{ all -> 0x06a8 }
                    r5.onProgressMediator(r9, r2)     // Catch:{ all -> 0x06a8 }
                    r5 = 0
                    r9 = 0
                L_0x04a7:
                    com.vungle.warren.downloader.DownloadRequestMediator r15 = r4     // Catch:{ all -> 0x06a8 }
                    r24 = r5
                    r5 = 1
                    boolean r15 = r15.is(r5)     // Catch:{ all -> 0x06a8 }
                    if (r15 == 0) goto L_0x055b
                    okio.Buffer r5 = r3.j()     // Catch:{ all -> 0x054d }
                    r15 = r0
                    r0 = 2048(0x800, double:1.0118E-320)
                    long r0 = r14.read(r5, r0)     // Catch:{ all -> 0x0549 }
                    r28 = -1
                    int r5 = (r0 > r28 ? 1 : (r0 == r28 ? 0 : -1))
                    if (r5 == 0) goto L_0x0546
                    boolean r5 = r11.exists()     // Catch:{ all -> 0x0549 }
                    if (r5 == 0) goto L_0x053c
                    r3.k()     // Catch:{ all -> 0x0549 }
                    long r9 = r9 + r0
                    long r0 = r12 + r9
                    r28 = 100
                    r18 = 0
                    int r5 = (r7 > r18 ? 1 : (r7 == r18 ? 0 : -1))
                    if (r5 <= 0) goto L_0x04f0
                    long r0 = r0 * r28
                    long r0 = r0 / r7
                    int r1 = (int) r0
                    r5 = r1
                    r1 = r30
                    goto L_0x04f4
                L_0x04df:
                    r0 = move-exception
                    r17 = r3
                    r15 = r20
                    r9 = r22
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r10 = 0
                    r1 = r30
                    goto L_0x0904
                L_0x04f0:
                    r1 = r30
                    r5 = r24
                L_0x04f4:
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x054d }
                    boolean r0 = r0.isConnected()     // Catch:{ all -> 0x054d }
                    if (r0 == 0) goto L_0x0534
                L_0x04fc:
                    int r0 = r2.progressPercent     // Catch:{ all -> 0x054d }
                    r24 = r7
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x054d }
                    int r7 = r7.progressStep     // Catch:{ all -> 0x054d }
                    int r0 = r0 + r7
                    if (r0 > r5) goto L_0x052f
                    int r0 = r2.progressPercent     // Catch:{ all -> 0x054d }
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x054d }
                    int r7 = r7.progressStep     // Catch:{ all -> 0x054d }
                    int r0 = r0 + r7
                    long r7 = (long) r0     // Catch:{ all -> 0x054d }
                    int r0 = (r7 > r28 ? 1 : (r7 == r28 ? 0 : -1))
                    if (r0 > 0) goto L_0x052f
                    r7 = 1
                    r2.status = r7     // Catch:{ all -> 0x054d }
                    int r0 = r2.progressPercent     // Catch:{ all -> 0x054d }
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x054d }
                    int r7 = r7.progressStep     // Catch:{ all -> 0x054d }
                    int r0 = r0 + r7
                    r2.progressPercent = r0     // Catch:{ all -> 0x054d }
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x054d }
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4     // Catch:{ all -> 0x054d }
                    r0.onProgressMediator(r7, r2)     // Catch:{ all -> 0x054d }
                    r7 = r24
                    goto L_0x04fc
                L_0x052f:
                    r0 = r15
                    r7 = r24
                    goto L_0x04a7
                L_0x0534:
                    java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x054d }
                    java.lang.String r5 = "Request is not connected"
                    r0.<init>(r5)     // Catch:{ all -> 0x054d }
                    throw r0     // Catch:{ all -> 0x054d }
                L_0x053c:
                    r1 = r30
                    com.vungle.warren.downloader.Downloader$RequestException r0 = new com.vungle.warren.downloader.Downloader$RequestException     // Catch:{ all -> 0x054d }
                    java.lang.String r5 = "File is not existing"
                    r0.<init>(r5)     // Catch:{ all -> 0x054d }
                    throw r0     // Catch:{ all -> 0x054d }
                L_0x0546:
                    r1 = r30
                    goto L_0x055c
                L_0x0549:
                    r0 = move-exception
                    r1 = r30
                    goto L_0x054e
                L_0x054d:
                    r0 = move-exception
                L_0x054e:
                    r17 = r3
                    r15 = r20
                    r9 = r22
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    goto L_0x0154
                L_0x055b:
                    r15 = r0
                L_0x055c:
                    r3.flush()     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x06a8 }
                    r5 = 1
                    boolean r0 = r0.is(r5)     // Catch:{ all -> 0x06a8 }
                    if (r0 == 0) goto L_0x0596
                    long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x054d }
                    java.lang.String r0 = "DOWNLOAD_COMPLETE"
                    java.lang.Boolean r5 = java.lang.Boolean.TRUE     // Catch:{ all -> 0x054d }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x054d }
                    r9 = r15
                    r9.put(r0, r5)     // Catch:{ all -> 0x054d }
                    java.lang.String r0 = "Last-Cache-Verification"
                    java.lang.String r5 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x054d }
                    r9.put(r0, r5)     // Catch:{ all -> 0x054d }
                    java.lang.String r0 = "Last-Download"
                    java.lang.String r5 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x054d }
                    r9.put(r0, r5)     // Catch:{ all -> 0x054d }
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x054d }
                    r0.saveMeta(r4, r9)     // Catch:{ all -> 0x054d }
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x054d }
                    r5 = 4
                    r0.set(r5)     // Catch:{ all -> 0x054d }
                    goto L_0x05c0
                L_0x0596:
                    r0 = 6
                    r2.status = r0     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4     // Catch:{ all -> 0x06a8 }
                    r0.onProgressMediator(r5, r2)     // Catch:{ all -> 0x06a8 }
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06a8 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x06a8 }
                    r5.<init>()     // Catch:{ all -> 0x06a8 }
                    java.lang.String r7 = "State has changed, cancelling download "
                    r5.append(r7)     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.DownloadRequestMediator r8 = r4     // Catch:{ all -> 0x06a8 }
                    java.lang.String r7 = r7.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r8)     // Catch:{ all -> 0x06a8 }
                    r5.append(r7)     // Catch:{ all -> 0x06a8 }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x06a8 }
                    android.util.Log.d(r0, r5)     // Catch:{ all -> 0x06a8 }
                L_0x05c0:
                    if (r6 == 0) goto L_0x05cf
                    okhttp3.ResponseBody r0 = r6.body()
                    if (r0 == 0) goto L_0x05cf
                    okhttp3.ResponseBody r0 = r6.body()
                    r0.close()
                L_0x05cf:
                    if (r23 == 0) goto L_0x05d4
                    r23.cancel()
                L_0x05d4:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "request is done "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r7)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    int r0 = r0.getStatus()
                    r5 = 2
                    if (r0 == r5) goto L_0x060f
                    r7 = 3
                    if (r0 == r7) goto L_0x0626
                    r5 = 4
                    if (r0 == r5) goto L_0x061c
                    r8 = 5
                    if (r0 == r8) goto L_0x0612
                    if (r27 != 0) goto L_0x060f
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r0.removeMediator(r5)
                L_0x060f:
                    r12 = r26
                    goto L_0x062f
                L_0x0612:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r12 = r26
                    r0.onErrorMediator(r12, r5)
                    goto L_0x062f
                L_0x061c:
                    r12 = r26
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r0.onSuccessMediator(r11, r5)
                    goto L_0x062f
                L_0x0626:
                    r12 = r26
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r0.onCancelledMediator(r5)
                L_0x062f:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "Done with request in state "
                    r5.append(r6)
                    com.vungle.warren.downloader.DownloadRequestMediator r6 = r4
                    int r6 = r6.getStatus()
                    r5.append(r6)
                    java.lang.String r6 = " "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r7)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r9)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a5 }
                    r0.removeNetworkListener()     // Catch:{ all -> 0x06a5 }
                    monitor-exit(r9)     // Catch:{ all -> 0x06a5 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r3)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r14)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    if (r0 == 0) goto L_0x069e
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    boolean r0 = r0.isCacheable
                    if (r0 == 0) goto L_0x069e
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.stopTracking(r11)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    boolean r0 = r0.isCacheEnabled()
                    if (r0 != 0) goto L_0x0695
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.clear()
                    goto L_0x069e
                L_0x0695:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.purge()
                L_0x069e:
                    r15 = r12
                    r14 = r27
                    r0 = 1
                    r10 = 0
                    goto L_0x0c1f
                L_0x06a5:
                    r0 = move-exception
                    monitor-exit(r9)     // Catch:{ all -> 0x06a5 }
                    throw r0
                L_0x06a8:
                    r0 = move-exception
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r17 = r3
                    r15 = r20
                    r9 = r22
                    goto L_0x0154
                L_0x06b6:
                    r0 = move-exception
                    goto L_0x06bb
                L_0x06b8:
                    r0 = move-exception
                    r23 = r5
                L_0x06bb:
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r15 = r20
                    r9 = r22
                    goto L_0x06d5
                L_0x06c5:
                    r0 = move-exception
                    goto L_0x06ca
                L_0x06c7:
                    r0 = move-exception
                    r21 = r3
                L_0x06ca:
                    r23 = r5
                    r22 = r9
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r15 = r20
                L_0x06d5:
                    r10 = 0
                    goto L_0x0741
                L_0x06d8:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    r22 = r9
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    goto L_0x0771
                L_0x06e6:
                    r21 = r3
                    r23 = r5
                    r22 = r9
                    r20 = r15
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x06fb }
                    java.lang.String r3 = "Response body is null"
                    r0.<init>(r3)     // Catch:{ all -> 0x06fb }
                    throw r0     // Catch:{ all -> 0x06fb }
                L_0x06fb:
                    r0 = move-exception
                    r15 = r20
                    r9 = r22
                    goto L_0x073f
                L_0x0701:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    r22 = r9
                    goto L_0x0738
                L_0x0709:
                    r21 = r3
                    r23 = r5
                    r22 = r9
                    r20 = r15
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    com.vungle.warren.downloader.Downloader$RequestException r0 = new com.vungle.warren.downloader.Downloader$RequestException     // Catch:{ all -> 0x072f }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x072f }
                    r3.<init>()     // Catch:{ all -> 0x072f }
                    java.lang.String r9 = "Code: "
                    r3.append(r9)     // Catch:{ all -> 0x072f }
                    r9 = r22
                    r3.append(r9)     // Catch:{ all -> 0x0770 }
                    java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0770 }
                    r0.<init>(r3)     // Catch:{ all -> 0x0770 }
                    throw r0     // Catch:{ all -> 0x0770 }
                L_0x072f:
                    r0 = move-exception
                    r9 = r22
                    goto L_0x0771
                L_0x0733:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                L_0x0738:
                    r20 = r15
                L_0x073a:
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                L_0x073f:
                    r10 = 0
                    r14 = 0
                L_0x0741:
                    r17 = 0
                    goto L_0x0904
                L_0x0745:
                    r21 = r3
                    r23 = r5
                    r20 = r15
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r3 = 304(0x130, float:4.26E-43)
                    if (r9 != r3) goto L_0x0774
                    java.lang.String r3 = "Last-Cache-Verification"
                    long r13 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0770 }
                    java.lang.String r10 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0770 }
                    r0.put(r3, r10)     // Catch:{ all -> 0x0770 }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0770 }
                    r3.saveMeta(r4, r0)     // Catch:{ all -> 0x0770 }
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0770 }
                    java.lang.String r3 = "Verification success, dispatch existing file"
                    android.util.Log.d(r0, r3)     // Catch:{ all -> 0x0770 }
                    goto L_0x0790
                L_0x0770:
                    r0 = move-exception
                L_0x0771:
                    r15 = r20
                    goto L_0x073f
                L_0x0774:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x086e }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x086e }
                    r3.<init>()     // Catch:{ all -> 0x086e }
                    java.lang.String r10 = "Using local cache file despite response code = "
                    r3.append(r10)     // Catch:{ all -> 0x086e }
                    int r10 = r6.code()     // Catch:{ all -> 0x086e }
                    r3.append(r10)     // Catch:{ all -> 0x086e }
                    java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x086e }
                    android.util.Log.d(r0, r3)     // Catch:{ all -> 0x086e }
                L_0x0790:
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x086e }
                    r3 = 4
                    r0.set(r3)     // Catch:{ all -> 0x086e }
                    if (r6 == 0) goto L_0x07a5
                    okhttp3.ResponseBody r0 = r6.body()
                    if (r0 == 0) goto L_0x07a5
                    okhttp3.ResponseBody r0 = r6.body()
                    r0.close()
                L_0x07a5:
                    if (r23 == 0) goto L_0x07aa
                    r23.cancel()
                L_0x07aa:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "request is done "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    int r0 = r0.getStatus()
                    if (r0 == r5) goto L_0x07fa
                    if (r0 == r7) goto L_0x07f3
                    r2 = 4
                    if (r0 == r2) goto L_0x07eb
                    if (r0 == r8) goto L_0x07e3
                    if (r27 != 0) goto L_0x07fa
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.removeMediator(r2)
                    goto L_0x07fa
                L_0x07e3:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onErrorMediator(r12, r2)
                    goto L_0x07fa
                L_0x07eb:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onSuccessMediator(r11, r2)
                    goto L_0x07fa
                L_0x07f3:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onCancelledMediator(r2)
                L_0x07fa:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "Done with request in state "
                    r2.append(r3)
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    int r3 = r3.getStatus()
                    r2.append(r3)
                    java.lang.String r3 = " "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r3)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x086b }
                    r0.removeNetworkListener()     // Catch:{ all -> 0x086b }
                    monitor-exit(r3)     // Catch:{ all -> 0x086b }
                    r10 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r10)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r10)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    if (r0 == 0) goto L_0x086a
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    boolean r0 = r0.isCacheable
                    if (r0 == 0) goto L_0x086a
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.stopTracking(r11)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    boolean r0 = r0.isCacheEnabled()
                    if (r0 != 0) goto L_0x0861
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.clear()
                    goto L_0x086a
                L_0x0861:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.purge()
                L_0x086a:
                    return
                L_0x086b:
                    r0 = move-exception
                    monitor-exit(r3)     // Catch:{ all -> 0x086b }
                    throw r0
                L_0x086e:
                    r0 = move-exception
                    r10 = 0
                    r14 = r10
                    r17 = r14
                    r15 = r20
                    goto L_0x0904
                L_0x0877:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    r20 = r15
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r10 = 0
                    r14 = r10
                    r17 = r14
                    goto L_0x0904
                L_0x0889:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    goto L_0x0896
                L_0x088f:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    r27 = r14
                L_0x0896:
                    r20 = r15
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r10 = 0
                    r14 = r10
                    goto L_0x08b1
                L_0x08a0:
                    r0 = move-exception
                    r21 = r3
                    r23 = r5
                    r27 = r14
                    r20 = r15
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r10 = 0
                    r6 = r10
                    r14 = r6
                L_0x08b1:
                    r17 = r14
                    goto L_0x0903
                L_0x08b4:
                    r0 = move-exception
                    r21 = r3
                    r27 = r14
                    r20 = r15
                    r12 = r26
                    r5 = 2
                    r7 = 3
                    r8 = 5
                    r10 = 0
                    r6 = r10
                    r14 = r6
                    r17 = r14
                    r23 = r17
                    goto L_0x0903
                L_0x08c8:
                    r0 = move-exception
                    r21 = r3
                    r20 = r10
                    r27 = r14
                    r12 = r15
                    r5 = 2
                    r7 = 3
                    goto L_0x08f9
                L_0x08d3:
                    r21 = r3
                    r20 = r10
                    r27 = r14
                    r12 = r15
                    r5 = 2
                    r8 = 5
                    r10 = 0
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x08ee }
                    java.lang.String r3 = "Request is not connected to reuired network"
                    android.util.Log.d(r0, r3)     // Catch:{ all -> 0x08ee }
                    java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x08ee }
                    java.lang.String r3 = "Not connected to correct network"
                    r0.<init>(r3)     // Catch:{ all -> 0x08ee }
                    throw r0     // Catch:{ all -> 0x08ee }
                L_0x08ee:
                    r0 = move-exception
                    goto L_0x08fb
                L_0x08f0:
                    r0 = move-exception
                    r21 = r3
                    r20 = r10
                    r27 = r14
                    r12 = r15
                    r5 = 2
                L_0x08f9:
                    r8 = 5
                    r10 = 0
                L_0x08fb:
                    r6 = r10
                    r14 = r6
                    r17 = r14
                    r23 = r17
                    r15 = r20
                L_0x0903:
                    r9 = -1
                L_0x0904:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0c28 }
                    java.lang.String r13 = "Exception on download"
                    android.util.Log.e(r3, r13, r0)     // Catch:{ all -> 0x0c28 }
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4     // Catch:{ all -> 0x0c28 }
                    boolean r3 = r3.is(r7)     // Catch:{ all -> 0x0c28 }
                    if (r3 != 0) goto L_0x091e
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4     // Catch:{ all -> 0x091b }
                    r3.set(r8)     // Catch:{ all -> 0x091b }
                    goto L_0x091e
                L_0x091b:
                    r0 = move-exception
                    goto L_0x0c2a
                L_0x091e:
                    boolean r3 = r0 instanceof java.io.IOException     // Catch:{ all -> 0x0c28 }
                    if (r3 == 0) goto L_0x0aff
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x091b }
                    com.vungle.warren.downloader.DownloadRequestMediator r13 = r4     // Catch:{ all -> 0x091b }
                    boolean r3 = r3.isAnyConnected(r13)     // Catch:{ all -> 0x091b }
                    if (r3 != 0) goto L_0x0a26
                    if (r6 != 0) goto L_0x0a26
                    com.vungle.warren.downloader.AssetDownloader r13 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x091b }
                    com.vungle.warren.downloader.DownloadRequestMediator r10 = r4     // Catch:{ all -> 0x091b }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x091b }
                    java.util.HashMap r8 = r8.extractMeta(r4)     // Catch:{ all -> 0x091b }
                    r5 = -1
                    boolean r5 = r13.useCacheOnFail(r10, r11, r8, r5)     // Catch:{ all -> 0x091b }
                    if (r5 == 0) goto L_0x0a26
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x091b }
                    boolean r0 = r0.is(r7)     // Catch:{ all -> 0x091b }
                    if (r0 != 0) goto L_0x094d
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x091b }
                    r2 = 4
                    r0.set(r2)     // Catch:{ all -> 0x091b }
                L_0x094d:
                    if (r6 == 0) goto L_0x095c
                    okhttp3.ResponseBody r0 = r6.body()
                    if (r0 == 0) goto L_0x095c
                    okhttp3.ResponseBody r0 = r6.body()
                    r0.close()
                L_0x095c:
                    if (r23 == 0) goto L_0x0961
                    r23.cancel()
                L_0x0961:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "request is done "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    int r0 = r0.getStatus()
                    r2 = 2
                    if (r0 == r2) goto L_0x09b3
                    if (r0 == r7) goto L_0x09ac
                    r2 = 4
                    if (r0 == r2) goto L_0x09a4
                    r2 = 5
                    if (r0 == r2) goto L_0x099c
                    if (r27 != 0) goto L_0x09b3
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.removeMediator(r2)
                    goto L_0x09b3
                L_0x099c:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onErrorMediator(r12, r2)
                    goto L_0x09b3
                L_0x09a4:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onSuccessMediator(r11, r2)
                    goto L_0x09b3
                L_0x09ac:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    r0.onCancelledMediator(r2)
                L_0x09b3:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "Done with request in state "
                    r2.append(r3)
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    int r3 = r3.getStatus()
                    r2.append(r3)
                    java.lang.String r3 = " "
                    r2.append(r3)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    java.lang.String r3 = r3.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r4)
                    r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.util.Log.d(r0, r2)
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r2)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a23 }
                    r0.removeNetworkListener()     // Catch:{ all -> 0x0a23 }
                    monitor-exit(r2)     // Catch:{ all -> 0x0a23 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r17)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r14)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    if (r0 == 0) goto L_0x0a22
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    boolean r0 = r0.isCacheable
                    if (r0 == 0) goto L_0x0a22
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.stopTracking(r11)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    boolean r0 = r0.isCacheEnabled()
                    if (r0 != 0) goto L_0x0a19
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.clear()
                    goto L_0x0a22
                L_0x0a19:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.purge()
                L_0x0a22:
                    return
                L_0x0a23:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch:{ all -> 0x0a23 }
                    throw r0
                L_0x0a26:
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4     // Catch:{ all -> 0x091b }
                    r5.setConnected(r3)     // Catch:{ all -> 0x091b }
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r5 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x091b }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x091b }
                    int r8 = r8.mapExceptionToReason(r0, r3)     // Catch:{ all -> 0x091b }
                    r5.<init>(r9, r0, r8)     // Catch:{ all -> 0x091b }
                    if (r3 != 0) goto L_0x0aad
                    r3 = 5
                    r2.status = r3     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4     // Catch:{ all -> 0x0aa9 }
                    r0.onProgressMediator(r3, r2)     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x0aa9 }
                    boolean r0 = r0.is(r7)     // Catch:{ all -> 0x0aa9 }
                    if (r0 != 0) goto L_0x0aad
                    int r0 = r15 + 1
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0aa9 }
                    int r3 = r3.maxReconnectAttempts     // Catch:{ all -> 0x0aa9 }
                    if (r15 >= r3) goto L_0x0aa6
                    r3 = 0
                L_0x0a53:
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0aa9 }
                    int r8 = r8.retryCountOnConnectionLost     // Catch:{ all -> 0x0aa9 }
                    if (r3 >= r8) goto L_0x0aa6
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0aa9 }
                    int r9 = r9.reconnectTimeout     // Catch:{ all -> 0x0aa9 }
                    long r9 = (long) r9     // Catch:{ all -> 0x0aa9 }
                    r8.sleep(r9)     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.DownloadRequestMediator r8 = r4     // Catch:{ all -> 0x0aa9 }
                    boolean r8 = r8.is(r7)     // Catch:{ all -> 0x0aa9 }
                    if (r8 == 0) goto L_0x0a6c
                    goto L_0x0aa6
                L_0x0a6c:
                    java.lang.String r8 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0aa9 }
                    java.lang.String r9 = "Trying to reconnect"
                    android.util.Log.d(r8, r9)     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.DownloadRequestMediator r9 = r4     // Catch:{ all -> 0x0aa9 }
                    boolean r8 = r8.isAnyConnected(r9)     // Catch:{ all -> 0x0aa9 }
                    if (r8 == 0) goto L_0x0a9d
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0aa9 }
                    java.lang.String r8 = "Reconnected, starting download again"
                    android.util.Log.d(r3, r8)     // Catch:{ all -> 0x0aa9 }
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4     // Catch:{ all -> 0x0a97 }
                    r8 = 1
                    r3.setConnected(r8)     // Catch:{ all -> 0x0a97 }
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4     // Catch:{ all -> 0x0a97 }
                    r3.set(r8)     // Catch:{ all -> 0x0a97 }
                    r15 = r0
                    r10 = 0
                    r13 = 0
                    goto L_0x0aaf
                L_0x0a97:
                    r0 = move-exception
                    r12 = r5
                    r16 = 0
                    goto L_0x0c2c
                L_0x0a9d:
                    com.vungle.warren.downloader.DownloadRequestMediator r8 = r4     // Catch:{ all -> 0x0aa9 }
                    r10 = 0
                    r8.setConnected(r10)     // Catch:{ all -> 0x0aa9 }
                    int r3 = r3 + 1
                    goto L_0x0a53
                L_0x0aa6:
                    r10 = 0
                    r15 = r0
                    goto L_0x0aae
                L_0x0aa9:
                    r0 = move-exception
                    r12 = r5
                    goto L_0x0c2a
                L_0x0aad:
                    r10 = 0
                L_0x0aae:
                    r13 = 1
                L_0x0aaf:
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0af9 }
                    monitor-enter(r3)     // Catch:{ all -> 0x0af9 }
                    if (r13 == 0) goto L_0x0af1
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x0af6 }
                    boolean r0 = r0.isConnected()     // Catch:{ all -> 0x0af6 }
                    if (r0 != 0) goto L_0x0af1
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x0af6 }
                    boolean r0 = r0.isPausable()     // Catch:{ all -> 0x0af6 }
                    if (r0 == 0) goto L_0x0af1
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0af6 }
                    com.vungle.warren.downloader.DownloadRequestMediator r8 = r4     // Catch:{ all -> 0x0af6 }
                    boolean r0 = r0.isAnyConnected(r8)     // Catch:{ all -> 0x0af6 }
                    if (r0 == 0) goto L_0x0ae7
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0af6 }
                    java.lang.String r8 = "Reconnected, starting download again"
                    android.util.Log.d(r0, r8)     // Catch:{ all -> 0x0af6 }
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x0ae4 }
                    r8 = 1
                    r0.setConnected(r8)     // Catch:{ all -> 0x0ae4 }
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4     // Catch:{ all -> 0x0ae4 }
                    r0.set(r8)     // Catch:{ all -> 0x0ae4 }
                    r13 = 0
                    goto L_0x0af1
                L_0x0ae4:
                    r0 = move-exception
                    r13 = 0
                    goto L_0x0af7
                L_0x0ae7:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0af6 }
                    com.vungle.warren.downloader.DownloadRequestMediator r8 = r4     // Catch:{ all -> 0x0af6 }
                    boolean r0 = r0.pause(r8, r2, r5)     // Catch:{ all -> 0x0af6 }
                    r27 = r0
                L_0x0af1:
                    monitor-exit(r3)     // Catch:{ all -> 0x0af6 }
                    r3 = r5
                    r16 = r13
                    goto L_0x0b1e
                L_0x0af6:
                    r0 = move-exception
                L_0x0af7:
                    monitor-exit(r3)     // Catch:{ all -> 0x0af6 }
                    throw r0     // Catch:{ all -> 0x0af9 }
                L_0x0af9:
                    r0 = move-exception
                    r12 = r5
                    r16 = r13
                    goto L_0x0c2c
                L_0x0aff:
                    r10 = 0
                    boolean r3 = r0 instanceof com.vungle.warren.downloader.Downloader.RequestException     // Catch:{ all -> 0x0c28 }
                    if (r3 == 0) goto L_0x0b10
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x091b }
                    r5 = 1
                    r3.deleteFileAndMeta(r11, r4, r5)     // Catch:{ all -> 0x091b }
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r3 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x091b }
                    r3.<init>(r9, r0, r5)     // Catch:{ all -> 0x091b }
                    goto L_0x0b1c
                L_0x0b10:
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0c28 }
                    r5 = 1
                    r3.deleteFileAndMeta(r11, r4, r5)     // Catch:{ all -> 0x091b }
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r3 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x091b }
                    r8 = 4
                    r3.<init>(r9, r0, r8)     // Catch:{ all -> 0x091b }
                L_0x0b1c:
                    r16 = 1
                L_0x0b1e:
                    if (r6 == 0) goto L_0x0b2d
                    okhttp3.ResponseBody r0 = r6.body()
                    if (r0 == 0) goto L_0x0b2d
                    okhttp3.ResponseBody r0 = r6.body()
                    r0.close()
                L_0x0b2d:
                    if (r23 == 0) goto L_0x0b32
                    r23.cancel()
                L_0x0b32:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "request is done "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r8 = r4
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r8)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                    if (r16 == 0) goto L_0x0bb5
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    int r0 = r0.getStatus()
                    r5 = 2
                    if (r0 == r5) goto L_0x0b86
                    if (r0 == r7) goto L_0x0b7f
                    r5 = 4
                    if (r0 == r5) goto L_0x0b77
                    r5 = 5
                    if (r0 == r5) goto L_0x0b6f
                    if (r27 != 0) goto L_0x0b86
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r0.removeMediator(r5)
                    goto L_0x0b86
                L_0x0b6f:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r0.onErrorMediator(r3, r5)
                    goto L_0x0b86
                L_0x0b77:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r0.onSuccessMediator(r11, r5)
                    goto L_0x0b86
                L_0x0b7f:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    r0.onCancelledMediator(r5)
                L_0x0b86:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "Done with request in state "
                    r5.append(r6)
                    com.vungle.warren.downloader.DownloadRequestMediator r6 = r4
                    int r6 = r6.getStatus()
                    r5.append(r6)
                    java.lang.String r6 = " "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r7)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                    goto L_0x0bd5
                L_0x0bb5:
                    java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "Not removing connections and listener "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r7 = r4
                    java.lang.String r6 = r6.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r7)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r0, r5)
                L_0x0bd5:
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r8)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0c25 }
                    r0.removeNetworkListener()     // Catch:{ all -> 0x0c25 }
                    monitor-exit(r8)     // Catch:{ all -> 0x0c25 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r17)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r14)
                    if (r16 == 0) goto L_0x0c18
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    if (r0 == 0) goto L_0x0c18
                    com.vungle.warren.downloader.DownloadRequestMediator r0 = r4
                    boolean r0 = r0.isCacheable
                    if (r0 == 0) goto L_0x0c18
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.stopTracking(r11)
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    boolean r0 = r0.isCacheEnabled()
                    if (r0 != 0) goto L_0x0c0f
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.clear()
                    goto L_0x0c18
                L_0x0c0f:
                    com.vungle.warren.downloader.AssetDownloader r0 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r0 = r0.cache
                    r0.purge()
                L_0x0c18:
                    r20 = r15
                    r0 = r16
                    r14 = r27
                    r15 = r3
                L_0x0c1f:
                    r10 = r20
                    r3 = r21
                    goto L_0x003a
                L_0x0c25:
                    r0 = move-exception
                    monitor-exit(r8)     // Catch:{ all -> 0x0c25 }
                    throw r0
                L_0x0c28:
                    r0 = move-exception
                    r5 = 1
                L_0x0c2a:
                    r16 = 1
                L_0x0c2c:
                    if (r6 == 0) goto L_0x0c3b
                    okhttp3.ResponseBody r2 = r6.body()
                    if (r2 == 0) goto L_0x0c3b
                    okhttp3.ResponseBody r2 = r6.body()
                    r2.close()
                L_0x0c3b:
                    if (r23 == 0) goto L_0x0c40
                    r23.cancel()
                L_0x0c40:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "request is done "
                    r3.append(r4)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    java.lang.String r4 = r4.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r5)
                    r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    android.util.Log.d(r2, r3)
                    if (r16 == 0) goto L_0x0cc3
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    int r2 = r2.getStatus()
                    r3 = 2
                    if (r2 == r3) goto L_0x0c94
                    if (r2 == r7) goto L_0x0c8d
                    r3 = 4
                    if (r2 == r3) goto L_0x0c85
                    r3 = 5
                    if (r2 == r3) goto L_0x0c7d
                    if (r27 != 0) goto L_0x0c94
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    r2.removeMediator(r3)
                    goto L_0x0c94
                L_0x0c7d:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    r2.onErrorMediator(r12, r3)
                    goto L_0x0c94
                L_0x0c85:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    r2.onSuccessMediator(r11, r3)
                    goto L_0x0c94
                L_0x0c8d:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r3 = r4
                    r2.onCancelledMediator(r3)
                L_0x0c94:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "Done with request in state "
                    r3.append(r4)
                    com.vungle.warren.downloader.DownloadRequestMediator r4 = r4
                    int r4 = r4.getStatus()
                    r3.append(r4)
                    java.lang.String r4 = " "
                    r3.append(r4)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    java.lang.String r4 = r4.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r5)
                    r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    android.util.Log.d(r2, r3)
                    goto L_0x0ce3
                L_0x0cc3:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "Not removing connections and listener "
                    r3.append(r4)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequestMediator r5 = r4
                    java.lang.String r4 = r4.debugString((com.vungle.warren.downloader.DownloadRequestMediator) r5)
                    r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    android.util.Log.d(r2, r3)
                L_0x0ce3:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r2)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0d27 }
                    r3.removeNetworkListener()     // Catch:{ all -> 0x0d27 }
                    monitor-exit(r2)     // Catch:{ all -> 0x0d27 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r17)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r14)
                    if (r16 == 0) goto L_0x0d26
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r2 = r2.cache
                    if (r2 == 0) goto L_0x0d26
                    com.vungle.warren.downloader.DownloadRequestMediator r2 = r4
                    boolean r2 = r2.isCacheable
                    if (r2 == 0) goto L_0x0d26
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r2 = r2.cache
                    r2.stopTracking(r11)
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    boolean r2 = r2.isCacheEnabled()
                    if (r2 != 0) goto L_0x0d1d
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r2 = r2.cache
                    r2.clear()
                    goto L_0x0d26
                L_0x0d1d:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloaderCache r2 = r2.cache
                    r2.purge()
                L_0x0d26:
                    throw r0
                L_0x0d27:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch:{ all -> 0x0d27 }
                    throw r0
                L_0x0d2a:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.AnonymousClass2.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> makeMeta(File file, Headers headers, String str) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(DOWNLOAD_URL, str);
        hashMap.put(ETAG, headers.get(ETAG));
        hashMap.put(LAST_MODIFIED, headers.get(LAST_MODIFIED));
        hashMap.put(ACCEPT_RANGES, headers.get(ACCEPT_RANGES));
        hashMap.put(CONTENT_ENCODING, headers.get(CONTENT_ENCODING));
        saveMeta(file, hashMap);
        return hashMap;
    }

    private DownloadRequestMediator makeNewMediator(DownloadRequest downloadRequest, AssetDownloadListener assetDownloadListener) throws IOException {
        String str;
        boolean z;
        File file;
        File file2;
        if (!isCacheEnabled()) {
            file2 = new File(downloadRequest.path);
            file = new File(file2.getPath() + META_POSTFIX_EXT);
            str = downloadRequest.url + " " + downloadRequest.path;
            z = false;
        } else {
            file2 = this.cache.getFile(downloadRequest.url);
            file = this.cache.getMetaFile(file2);
            str = downloadRequest.url;
            z = true;
        }
        Log.d(TAG, "Cache file " + file2.getPath());
        return new DownloadRequestMediator(downloadRequest, assetDownloadListener, file2.getPath(), file.getPath(), z, str);
    }

    /* access modifiers changed from: private */
    public int mapExceptionToReason(Throwable th, boolean z) {
        if (th instanceof RuntimeException) {
            return 4;
        }
        if (!z || (th instanceof SocketException) || (th instanceof SocketTimeoutException)) {
            return 0;
        }
        return ((th instanceof UnknownHostException) || (th instanceof SSLException)) ? 1 : 2;
    }

    private String mediatorKeyFromRequest(DownloadRequest downloadRequest) {
        if (isCacheEnabled()) {
            return getCacheableKey(downloadRequest);
        }
        return getNonCacheableKey(downloadRequest);
    }

    /* access modifiers changed from: private */
    public boolean notModified(File file, Response response, DownloadRequestMediator downloadRequestMediator, HashMap<String, String> hashMap) {
        if (response != null && file.exists() && file.length() > 0 && downloadRequestMediator.isCacheable) {
            int code = response.code();
            if (Boolean.parseBoolean(hashMap.get(DOWNLOAD_COMPLETE)) && code == 304) {
                String str = TAG;
                Log.d(str, "304 code, data size matches file size " + debugString(downloadRequestMediator));
                return true;
            }
        }
        return false;
    }

    private void onCancelled(DownloadRequest downloadRequest) {
        DownloadRequest downloadRequest2;
        if (!downloadRequest.isCancelled()) {
            downloadRequest.cancel();
            DownloadRequestMediator findMediatordForCancelation = findMediatordForCancelation(downloadRequest);
            if (!(findMediatordForCancelation == null || findMediatordForCancelation.getStatus() == 3)) {
                Pair<DownloadRequest, AssetDownloadListener> remove = findMediatordForCancelation.remove(downloadRequest);
                AssetDownloadListener assetDownloadListener = null;
                if (remove == null) {
                    downloadRequest2 = null;
                } else {
                    downloadRequest2 = (DownloadRequest) remove.f598a;
                }
                if (remove != null) {
                    assetDownloadListener = (AssetDownloadListener) remove.b;
                }
                if (findMediatordForCancelation.values().isEmpty()) {
                    findMediatordForCancelation.set(3);
                }
                if (downloadRequest2 != null) {
                    AssetDownloadListener.Progress progress = new AssetDownloadListener.Progress();
                    progress.status = 3;
                    deliverProgress(progress, downloadRequest2, assetDownloadListener);
                } else {
                    return;
                }
            }
            removeNetworkListener();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onCancelledMediator(DownloadRequestMediator downloadRequestMediator) {
        for (DownloadRequest onCancelled : downloadRequestMediator.requests()) {
            onCancelled(onCancelled);
        }
    }

    /* access modifiers changed from: private */
    public void onErrorMediator(AssetDownloadListener.DownloadError downloadError, DownloadRequestMediator downloadRequestMediator) {
        if (downloadError == null) {
            downloadError = new AssetDownloadListener.DownloadError(-1, new RuntimeException(), 4);
        }
        try {
            downloadRequestMediator.lock();
            for (Pair next : downloadRequestMediator.values()) {
                deliverError((DownloadRequest) next.f598a, (AssetDownloadListener) next.b, downloadError);
            }
            removeMediator(downloadRequestMediator);
            downloadRequestMediator.set(6);
        } finally {
            downloadRequestMediator.unlock();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onNetworkChanged(int i) {
        String str = TAG;
        Log.d(str, "Num of connections: " + this.mediators.values().size());
        for (DownloadRequestMediator next : this.mediators.values()) {
            if (next.is(3)) {
                Log.d(TAG, "Result cancelled");
            } else {
                boolean isAnyConnected = isAnyConnected(next);
                String str2 = TAG;
                Log.d(str2, "Connected = " + isAnyConnected + " for " + i);
                next.setConnected(isAnyConnected);
                if (next.isPausable() && isAnyConnected && next.is(2)) {
                    load(next);
                    String str3 = TAG;
                    Log.d(str3, "resumed " + next.key + " " + next);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void onProgressMediator(DownloadRequestMediator downloadRequestMediator, AssetDownloadListener.Progress progress) {
        if (downloadRequestMediator != null) {
            AssetDownloadListener.Progress copy = AssetDownloadListener.Progress.copy(progress);
            String str = TAG;
            Log.d(str, "Progress " + progress.progressPercent + " status " + progress.status + " " + downloadRequestMediator + " " + downloadRequestMediator.filePath);
            for (Pair next : downloadRequestMediator.values()) {
                deliverProgress(copy, (DownloadRequest) next.f598a, (AssetDownloadListener) next.b);
            }
        }
    }

    /* access modifiers changed from: private */
    public void onSuccessMediator(File file, DownloadRequestMediator downloadRequestMediator) {
        String str = TAG;
        Log.d(str, "OnComplete - Removing connections and listener " + downloadRequestMediator);
        try {
            downloadRequestMediator.lock();
            List<Pair<DownloadRequest, AssetDownloadListener>> values = downloadRequestMediator.values();
            if (!file.exists()) {
                onErrorMediator(new AssetDownloadListener.DownloadError(-1, new IOException("File is deleted"), 2), downloadRequestMediator);
                return;
            }
            if (this.cache != null && downloadRequestMediator.isCacheable) {
                this.cache.onCacheHit(file, (long) values.size());
                this.cache.setCacheLastUpdateTimestamp(file, System.currentTimeMillis());
            }
            for (Pair next : values) {
                File file2 = new File(((DownloadRequest) next.f598a).path);
                if (!file2.equals(file)) {
                    copyToDestination(file, file2, next);
                } else {
                    file2 = file;
                }
                String str2 = TAG;
                Log.d(str2, "Deliver success:" + ((DownloadRequest) next.f598a).url + " dest file: " + file2.getPath());
                deliverSuccess(next, file2);
            }
            removeMediator(downloadRequestMediator);
            downloadRequestMediator.set(6);
            String str3 = TAG;
            Log.d(str3, "Finished " + debugString(downloadRequestMediator));
            downloadRequestMediator.unlock();
        } finally {
            downloadRequestMediator.unlock();
        }
    }

    /* access modifiers changed from: private */
    public boolean partialMalformed(long j, int i, Response response, DownloadRequestMediator downloadRequestMediator) {
        return (i == 206 && !satisfiesPartialDownload(response, j, downloadRequestMediator)) || i == RANGE_NOT_SATISFIABLE;
    }

    /* access modifiers changed from: private */
    public boolean pause(DownloadRequestMediator downloadRequestMediator, AssetDownloadListener.Progress progress, AssetDownloadListener.DownloadError downloadError) {
        boolean z = false;
        if (downloadRequestMediator.is(3) || isAnyConnected(downloadRequestMediator)) {
            return false;
        }
        progress.status = 2;
        AssetDownloadListener.Progress copy = AssetDownloadListener.Progress.copy(progress);
        boolean z2 = false;
        for (Pair next : downloadRequestMediator.values()) {
            DownloadRequest downloadRequest = (DownloadRequest) next.f598a;
            if (downloadRequest != null) {
                if (!downloadRequest.pauseOnConnectionLost) {
                    downloadRequestMediator.remove(downloadRequest);
                    deliverError(downloadRequest, (AssetDownloadListener) next.b, downloadError);
                } else {
                    downloadRequestMediator.set(2);
                    String str = TAG;
                    Log.d(str, "Pausing download " + debugString(downloadRequest));
                    deliverProgress(copy, (DownloadRequest) next.f598a, (AssetDownloadListener) next.b);
                    z2 = true;
                }
            }
        }
        if (!z2) {
            downloadRequestMediator.set(5);
        }
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Attempted to pause - ");
        if (downloadRequestMediator.getStatus() == 2) {
            z = true;
        }
        sb.append(z);
        Log.d(str2, sb.toString());
        return z2;
    }

    /* access modifiers changed from: private */
    public synchronized void removeMediator(DownloadRequestMediator downloadRequestMediator) {
        this.mediators.remove(downloadRequestMediator.key);
    }

    /* access modifiers changed from: private */
    public void removeNetworkListener() {
        if (this.mediators.isEmpty()) {
            Log.d(TAG, "Removing listener");
            this.networkProvider.removeListener(this.networkListener);
        }
    }

    private boolean responseVersionMatches(Response response, HashMap<String, String> hashMap) {
        Headers headers = response.headers();
        String str = headers.get(ETAG);
        String str2 = headers.get(LAST_MODIFIED);
        String str3 = TAG;
        Log.d(str3, "server etag: " + str);
        String str4 = TAG;
        Log.d(str4, "server lastModified: " + str2);
        if (str != null && !str.equals(hashMap.get(ETAG))) {
            String str5 = TAG;
            Log.d(str5, "etags miss match current: " + hashMap.get(ETAG));
            return false;
        } else if (str2 == null || str2.equals(hashMap.get(LAST_MODIFIED))) {
            return true;
        } else {
            String str6 = TAG;
            Log.d(str6, "lastModified miss match current: " + hashMap.get(LAST_MODIFIED));
            return false;
        }
    }

    private boolean satisfiesPartialDownload(Response response, long j, DownloadRequestMediator downloadRequestMediator) {
        boolean z;
        RangeResponse rangeResponse = new RangeResponse(response.headers().get(CONTENT_RANGE));
        if (response.code() == 206 && BYTES.equalsIgnoreCase(rangeResponse.dimension)) {
            long j2 = rangeResponse.rangeStart;
            if (j2 >= 0 && j == j2) {
                z = true;
                String str = TAG;
                Log.d(str, "satisfies partial download: " + z + " " + debugString(downloadRequestMediator));
                return z;
            }
        }
        z = false;
        String str2 = TAG;
        Log.d(str2, "satisfies partial download: " + z + " " + debugString(downloadRequestMediator));
        return z;
    }

    /* access modifiers changed from: private */
    public void saveMeta(File file, HashMap<String, String> hashMap) {
        FileUtility.writeMap(file.getPath(), hashMap);
    }

    /* access modifiers changed from: private */
    public void sleep(long j) {
        try {
            Thread.sleep(Math.max(0, j));
        } catch (InterruptedException e) {
            Log.e(TAG, "InterruptedException ", e);
        }
    }

    /* access modifiers changed from: private */
    public boolean useCacheOnFail(DownloadRequestMediator downloadRequestMediator, File file, Map<String, String> map, int i) {
        return this.cache != null && downloadRequestMediator.isCacheable && i != 200 && i != RANGE_NOT_SATISFIABLE && i != 206 && Boolean.parseBoolean(map.get(DOWNLOAD_COMPLETE)) && file.exists() && file.length() > 0;
    }

    /* access modifiers changed from: private */
    public boolean useCacheWithoutVerification(DownloadRequestMediator downloadRequestMediator, File file, Map<String, String> map) {
        String str;
        if (map == null || this.cache == null || !downloadRequestMediator.isCacheable || (str = map.get(LAST_CACHE_VERIFICATION)) == null || !file.exists() || !Boolean.parseBoolean(map.get(DOWNLOAD_COMPLETE))) {
            return false;
        }
        try {
            long parseLong = Long.parseLong(str);
            long j = this.timeWindow;
            if (j >= Clock.MAX_TIME - parseLong || parseLong + j >= System.currentTimeMillis()) {
                return true;
            }
            return false;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    public synchronized void cancel(DownloadRequest downloadRequest) {
        if (downloadRequest != null) {
            onCancelled(downloadRequest);
        }
    }

    public synchronized void cancelAll() {
        Log.d(TAG, "Cancelling all");
        for (DownloadRequest next : this.transitioning) {
            String str = TAG;
            Log.d(str, "Cancel in transtiotion " + next.url);
            cancel(next);
        }
        String str2 = TAG;
        Log.d(str2, "Cancel in mediator " + this.mediators.values().size());
        for (DownloadRequestMediator next2 : this.mediators.values()) {
            String str3 = TAG;
            Log.d(str3, "Cancel in mediator " + next2.key);
            onCancelledMediator(next2);
        }
    }

    public boolean cancelAndAwait(DownloadRequest downloadRequest, long j) {
        if (downloadRequest == null) {
            return false;
        }
        cancel(downloadRequest);
        long currentTimeMillis = System.currentTimeMillis() + Math.max(0, j);
        while (System.currentTimeMillis() < currentTimeMillis) {
            DownloadRequestMediator findMediatordForCancelation = findMediatordForCancelation(downloadRequest);
            if (!this.transitioning.contains(downloadRequest) && (findMediatordForCancelation == null || !findMediatordForCancelation.requests().contains(downloadRequest))) {
                return true;
            }
            sleep(10);
        }
        return false;
    }

    public synchronized void clearCache() {
        if (this.cache != null) {
            this.cache.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void download(final com.vungle.warren.downloader.DownloadRequest r5, final com.vungle.warren.downloader.AssetDownloadListener r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r5 != 0) goto L_0x0019
            if (r6 == 0) goto L_0x0017
            r5 = 0
            com.vungle.warren.downloader.AssetDownloadListener$DownloadError r0 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x002d }
            r1 = -1
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x002d }
            java.lang.String r3 = "DownloadRequest is null"
            r2.<init>(r3)     // Catch:{ all -> 0x002d }
            r3 = 1
            r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x002d }
            r4.deliverError(r5, r6, r0)     // Catch:{ all -> 0x002d }
        L_0x0017:
            monitor-exit(r4)
            return
        L_0x0019:
            java.util.List<com.vungle.warren.downloader.DownloadRequest> r0 = r4.transitioning     // Catch:{ all -> 0x002d }
            r0.add(r5)     // Catch:{ all -> 0x002d }
            java.util.concurrent.ExecutorService r0 = r4.downloadExecutor     // Catch:{ all -> 0x002d }
            com.vungle.warren.downloader.AssetDownloader$1 r1 = new com.vungle.warren.downloader.AssetDownloader$1     // Catch:{ all -> 0x002d }
            r2 = 2147483647(0x7fffffff, float:NaN)
            r1.<init>(r2, r5, r6)     // Catch:{ all -> 0x002d }
            r0.execute(r1)     // Catch:{ all -> 0x002d }
            monitor-exit(r4)
            return
        L_0x002d:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.download(com.vungle.warren.downloader.DownloadRequest, com.vungle.warren.downloader.AssetDownloadListener):void");
    }

    public boolean dropCache(String str) {
        DownloaderCache downloaderCache = this.cache;
        if (downloaderCache == null || str == null) {
            return false;
        }
        try {
            File file = downloaderCache.getFile(str);
            String str2 = TAG;
            Log.d(str2, "Broken asset, deleting " + file.getPath());
            return this.cache.deleteAndRemove(file);
        } catch (IOException e) {
            Log.e(TAG, "There was an error to get file", e);
            return false;
        }
    }

    public synchronized List<DownloadRequest> getAllRequests() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (DownloadRequestMediator requests : new ArrayList(this.mediators.values())) {
            arrayList.addAll(requests.requests());
        }
        arrayList.addAll(this.transitioning);
        return arrayList;
    }

    public synchronized void init() {
        if (this.cache != null) {
            this.cache.init();
        }
    }

    public synchronized boolean isCacheEnabled() {
        return this.cache != null && this.isCacheEnabled;
    }

    public synchronized void setCacheEnabled(boolean z) {
        this.isCacheEnabled = z;
    }

    /* access modifiers changed from: package-private */
    public synchronized void setDownloadedForTests(boolean z, String str, String str2) {
        ArrayList<File> arrayList = new ArrayList<>(2);
        if (this.cache != null) {
            try {
                arrayList.add(this.cache.getMetaFile(this.cache.getFile(str)));
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Failed to get file for request");
            }
        }
        arrayList.add(new File(str2 + META_POSTFIX_EXT));
        for (File file : arrayList) {
            HashMap<String, String> extractMeta = extractMeta(file);
            extractMeta.put(DOWNLOAD_COMPLETE, Boolean.valueOf(z).toString());
            FileUtility.writeSerializable(file, extractMeta);
        }
    }

    public void setProgressStep(int i) {
        if (i != 0) {
            this.progressStep = i;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void shutdown() {
        cancel((DownloadRequest) null);
        this.transitioning.clear();
        this.mediators.clear();
        this.uiExecutor.shutdownNow();
        this.downloadExecutor.shutdownNow();
        try {
            this.downloadExecutor.awaitTermination(2, TimeUnit.SECONDS);
            this.uiExecutor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e(TAG, "InterruptedException ", e);
        }
        return;
    }

    public AssetDownloader(DownloaderCache downloaderCache, long j, int i, NetworkProvider networkProvider2, ExecutorService executorService) {
        this.retryCountOnConnectionLost = 5;
        this.maxReconnectAttempts = 10;
        this.reconnectTimeout = 300;
        this.mediators = new ConcurrentHashMap();
        this.transitioning = new ArrayList();
        this.addLock = new Object();
        this.progressStep = 5;
        this.isCacheEnabled = true;
        this.networkListener = new NetworkProvider.NetworkListener() {
            public void onChanged(int i) {
                String access$200 = AssetDownloader.TAG;
                Log.d(access$200, "Network changed: " + i);
                AssetDownloader.this.onNetworkChanged(i);
            }
        };
        this.cache = downloaderCache;
        int max = Math.max(i, 1);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(max, max, 1, TimeUnit.SECONDS, new PriorityBlockingQueue());
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        this.timeWindow = j;
        this.downloadExecutor = threadPoolExecutor;
        this.networkProvider = networkProvider2;
        this.uiExecutor = executorService;
        this.okHttpClient = new OkHttpClient.Builder().readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS).cache((Cache) null).followRedirects(true).followSslRedirects(true).build();
    }

    private String debugString(DownloadRequest downloadRequest) {
        return ", single request url - " + downloadRequest.url + ", path - " + downloadRequest.path + ", th - " + Thread.currentThread().getName() + "id " + downloadRequest.id;
    }
}
