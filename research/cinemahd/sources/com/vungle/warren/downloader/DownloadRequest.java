package com.vungle.warren.downloader;

import android.text.TextUtils;
import com.vungle.warren.downloader.Downloader;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class DownloadRequest {
    private AtomicBoolean cancelled;
    public final String cookieString;
    final String id;
    public final int networkType;
    public final String path;
    public final boolean pauseOnConnectionLost;
    public final int priority;
    public final String url;

    public @interface Priority {
        public static final int DEFAULT = 1;
        public static final int HIGH = 2;
        public static final int LOW = 0;
    }

    public DownloadRequest(String str, String str2) {
        this(3, 1, str, str2, false, (String) null);
    }

    /* access modifiers changed from: package-private */
    public void cancel() {
        this.cancelled.set(true);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || DownloadRequest.class != obj.getClass()) {
            return false;
        }
        DownloadRequest downloadRequest = (DownloadRequest) obj;
        if (this.networkType != downloadRequest.networkType || this.priority != downloadRequest.priority || this.pauseOnConnectionLost != downloadRequest.pauseOnConnectionLost) {
            return false;
        }
        String str = this.url;
        if (str == null ? downloadRequest.url != null : !str.equals(downloadRequest.url)) {
            return false;
        }
        String str2 = this.path;
        if (str2 == null ? downloadRequest.path != null : !str2.equals(downloadRequest.path)) {
            return false;
        }
        String str3 = this.id;
        if (str3 == null ? downloadRequest.id != null : !str3.equals(downloadRequest.id)) {
            return false;
        }
        String str4 = this.cookieString;
        if (str4 == null ? downloadRequest.cookieString != null : !str4.equals(downloadRequest.cookieString)) {
            return false;
        }
        AtomicBoolean atomicBoolean = this.cancelled;
        AtomicBoolean atomicBoolean2 = downloadRequest.cancelled;
        if (atomicBoolean != null) {
            return atomicBoolean.equals(atomicBoolean2);
        }
        if (atomicBoolean2 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = ((this.networkType * 31) + this.priority) * 31;
        String str = this.url;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.path;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + (this.pauseOnConnectionLost ? 1 : 0)) * 31;
        String str3 = this.id;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.cookieString;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        AtomicBoolean atomicBoolean = this.cancelled;
        if (atomicBoolean != null) {
            i2 = atomicBoolean.hashCode();
        }
        return hashCode4 + i2;
    }

    /* access modifiers changed from: package-private */
    public boolean isCancelled() {
        return this.cancelled.get();
    }

    public DownloadRequest(String str, String str2, String str3) {
        this(3, 1, str, str2, false, str3);
    }

    public DownloadRequest(@Downloader.NetworkType int i, @Priority int i2, String str, String str2, boolean z, String str3) {
        this.cancelled = new AtomicBoolean(false);
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Url or path is empty");
        }
        this.networkType = i;
        this.priority = i2;
        this.url = str;
        this.path = str2;
        this.id = UUID.nameUUIDFromBytes((str2 + "_" + str).getBytes()).toString();
        this.pauseOnConnectionLost = z;
        this.cookieString = str3;
    }
}
