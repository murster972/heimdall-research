package com.vungle.warren.downloader;

import android.util.Base64;
import android.util.Log;
import com.vungle.warren.SizeProvider;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.utility.FileUtility;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CleverCache implements DownloaderCache {
    static final String ASSETS_DIR = "assets";
    static final String CACHE_META = "meta";
    public static final String CACHE_TOUCH_JOURNAL = "cache_touch_timestamp";
    public static final String CC_DIR = "clever_cache";
    private static final String META_POSTFIX_EXT = ".vng_meta";
    private static final String TAG = "CleverCache";
    private final CacheManager cacheManager;
    private final HashMap<File, Long> cacheTouchTime = new HashMap<>();
    private final long expirationAge;
    private final CachePolicy<File> policy;
    private final SizeProvider sizeProvider;
    private Map<File, Integer> trackedFiles = new ConcurrentHashMap();

    public CleverCache(CacheManager cacheManager2, CachePolicy<File> cachePolicy, SizeProvider sizeProvider2, long j) {
        this.cacheManager = cacheManager2;
        this.policy = cachePolicy;
        this.sizeProvider = sizeProvider2;
        this.expirationAge = Math.max(0, j);
    }

    private synchronized void expirationCleanup() {
        long currentTimeMillis = System.currentTimeMillis() - this.expirationAge;
        File[] listFiles = getAssetsDir().listFiles();
        HashSet hashSet = new HashSet(this.cacheTouchTime.keySet());
        if (listFiles != null && listFiles.length > 0) {
            for (File file : listFiles) {
                long cacheUpdateTimestamp = getCacheUpdateTimestamp(file);
                hashSet.remove(file);
                if (!isProtected(file)) {
                    if (cacheUpdateTimestamp == 0 || cacheUpdateTimestamp <= currentTimeMillis) {
                        if (deleteContents(file)) {
                            this.cacheTouchTime.remove(file);
                            this.policy.remove(file);
                        }
                        Log.d(TAG, "Deleted expired file " + file);
                    }
                }
            }
            Iterator it2 = hashSet.iterator();
            while (it2.hasNext()) {
                this.cacheTouchTime.remove((File) it2.next());
            }
            this.policy.save();
            saveTouchTimestamps();
        }
    }

    private File getCacheDir() {
        File file = new File(this.cacheManager.getCache(), CC_DIR);
        if (!file.isDirectory()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    private File getTouchTimestampsFile() {
        return new File(getCacheDir(), CACHE_TOUCH_JOURNAL);
    }

    private void integrityCleanup(List<File> list) {
        File metaDir = getMetaDir();
        File[] listFiles = getAssetsDir().listFiles();
        if (listFiles != null) {
            ArrayList<File> arrayList = new ArrayList<>(Arrays.asList(listFiles));
            arrayList.removeAll(list);
            arrayList.remove(metaDir);
            for (File file : arrayList) {
                deleteContents(file);
                String str = TAG;
                Log.d(str, "Deleted non tracked file " + file);
            }
        }
    }

    private boolean isProtected(File file) {
        Integer num = this.trackedFiles.get(file);
        if (num == null || num.intValue() <= 0) {
            return false;
        }
        String str = TAG;
        Log.d(str, "File is tracked and protected : " + file);
        return true;
    }

    private void loadTouchTimestamps() {
        Serializable serializable = (Serializable) FileUtility.readSerializable(getTouchTimestampsFile());
        if (serializable instanceof HashMap) {
            try {
                this.cacheTouchTime.putAll((HashMap) serializable);
            } catch (ClassCastException unused) {
                getTouchTimestampsFile().delete();
            }
        }
    }

    private void saveTouchTimestamps() {
        FileUtility.writeSerializable(getTouchTimestampsFile(), new HashMap(this.cacheTouchTime));
    }

    public synchronized void clear() {
        List<File> orderedCacheItems = this.policy.getOrderedCacheItems();
        int i = 0;
        integrityCleanup(orderedCacheItems);
        for (File next : orderedCacheItems) {
            if (next != null) {
                if (!isProtected(next)) {
                    if (deleteContents(next)) {
                        i++;
                        this.policy.remove(next);
                        this.cacheTouchTime.remove(next);
                    }
                }
            }
        }
        if (i > 0) {
            this.policy.save();
            saveTouchTimestamps();
        }
    }

    public synchronized boolean deleteAndRemove(File file) {
        if (!deleteContents(file)) {
            return false;
        }
        this.cacheTouchTime.remove(file);
        this.policy.remove(file);
        this.policy.save();
        saveTouchTimestamps();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0013, code lost:
        return false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean deleteContents(java.io.File r1) {
        /*
            r0 = this;
            monitor-enter(r0)
            com.vungle.warren.utility.FileUtility.delete(r1)     // Catch:{ IOException -> 0x0011, all -> 0x000e }
            java.io.File r1 = r0.getMetaFile(r1)     // Catch:{ IOException -> 0x0011, all -> 0x000e }
            com.vungle.warren.utility.FileUtility.delete(r1)     // Catch:{ IOException -> 0x0011, all -> 0x000e }
            r1 = 1
            monitor-exit(r0)
            return r1
        L_0x000e:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0011:
            r1 = 0
            monitor-exit(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.CleverCache.deleteContents(java.io.File):boolean");
    }

    public synchronized File getAssetsDir() {
        File file;
        file = new File(getCacheDir(), ASSETS_DIR);
        if (!file.isDirectory()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public long getCacheUpdateTimestamp(File file) {
        Long l = this.cacheTouchTime.get(file);
        return l == null ? file.lastModified() : l.longValue();
    }

    public synchronized File getFile(String str) throws IOException {
        File file;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes("UTF-8"));
            file = new File(getAssetsDir(), Base64.encodeToString(instance.digest(), 10));
            this.policy.put(file, 0);
        } catch (UnsupportedEncodingException e) {
            throw new IOException(e);
        } catch (NoSuchAlgorithmException e2) {
            throw new IOException(e2);
        }
        return file;
    }

    public synchronized File getMetaDir() {
        File file;
        file = new File(getAssetsDir(), CACHE_META);
        if (!file.isDirectory()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public synchronized File getMetaFile(File file) {
        File metaDir;
        metaDir = getMetaDir();
        return new File(metaDir, file.getName() + META_POSTFIX_EXT);
    }

    public synchronized void init() {
        this.policy.load();
        loadTouchTimestamps();
        expirationCleanup();
    }

    public synchronized void onCacheHit(File file, long j) {
        this.policy.put(file, j);
        this.policy.save();
        String str = TAG;
        Log.d(str, "Cache hit " + file + " cache touch updated");
        purge();
    }

    public synchronized List<File> purge() {
        long targetSize = this.sizeProvider.getTargetSize();
        long size = FileUtility.size(getAssetsDir());
        String str = TAG;
        Log.d(str, "Purge check current cache total: " + size + " target: " + targetSize);
        if (size < targetSize) {
            return Collections.emptyList();
        }
        Log.d(TAG, "Purge start");
        ArrayList arrayList = new ArrayList();
        List<File> orderedCacheItems = this.policy.getOrderedCacheItems();
        integrityCleanup(orderedCacheItems);
        long size2 = FileUtility.size(getAssetsDir());
        if (size2 < targetSize) {
            Log.d(TAG, "Cleaned up not tracked files, size is ok");
            return Collections.emptyList();
        }
        Iterator<File> it2 = orderedCacheItems.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            File next = it2.next();
            if (next != null) {
                if (!isProtected(next)) {
                    long length = next.length();
                    if (deleteContents(next)) {
                        size2 -= length;
                        arrayList.add(next);
                        String str2 = TAG;
                        Log.d(str2, "Deleted file: " + next.getName() + " size: " + length + " total: " + size2 + " target: " + targetSize);
                        this.policy.remove(next);
                        this.cacheTouchTime.remove(next);
                        if (size2 < targetSize) {
                            targetSize = this.sizeProvider.getTargetSize();
                            if (size2 < targetSize) {
                                String str3 = TAG;
                                Log.d(str3, "Cleaned enough total: " + size2 + " target: " + targetSize);
                                break;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
        if (arrayList.size() > 0) {
            this.policy.save();
            saveTouchTimestamps();
        }
        Log.d(TAG, "Purge complete");
        return arrayList;
    }

    public synchronized void setCacheLastUpdateTimestamp(File file, long j) {
        this.cacheTouchTime.put(file, Long.valueOf(j));
        saveTouchTimestamps();
    }

    public synchronized void startTracking(File file) {
        int i;
        Integer num = this.trackedFiles.get(file);
        this.policy.put(file, 0);
        this.policy.save();
        if (num != null) {
            if (num.intValue() > 0) {
                i = Integer.valueOf(num.intValue() + 1);
                this.trackedFiles.put(file, i);
                String str = TAG;
                Log.d(str, "Start tracking file: " + file + " ref count " + i);
            }
        }
        i = 1;
        this.trackedFiles.put(file, i);
        String str2 = TAG;
        Log.d(str2, "Start tracking file: " + file + " ref count " + i);
    }

    public synchronized void stopTracking(File file) {
        Integer num = this.trackedFiles.get(file);
        if (num == null) {
            this.trackedFiles.remove(file);
            return;
        }
        Integer valueOf = Integer.valueOf(num.intValue() - 1);
        if (valueOf.intValue() <= 0) {
            this.trackedFiles.remove(file);
        }
        String str = TAG;
        Log.d(str, "Stop tracking file: " + file + " ref count " + valueOf);
    }
}
