package com.vungle.warren.ui.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.facebook.common.util.ByteConstants;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.utility.ViewUtility;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

public class FullAdWidget extends RelativeLayout {
    protected static final double NINE_BY_SIXTEEN_ASPECT_RATIO = 0.5625d;
    private static final String TAG = FullAdWidget.class.getSimpleName();
    private final ImageView closeButton;
    private final ImageView ctaOverlay;
    /* access modifiers changed from: private */
    public GestureDetector gestureDetector;
    ViewTreeObserver.OnGlobalLayoutListener immersiveModeListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            FullAdWidget.this.removeImmersiveModeListener();
            FullAdWidget.this.window.getDecorView().setSystemUiVisibility(5894);
        }
    };
    private final RelativeLayout.LayoutParams matchParentLayoutParams;
    /* access modifiers changed from: private */
    public final ImageView muteButton;
    /* access modifiers changed from: private */
    public OnItemClickListener onClickProxy;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener onCompletionListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnErrorListener onErrorListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnPreparedListener onPreparedListener;
    private final ImageView privacyOverlay;
    private final ProgressBar progressBar;
    /* access modifiers changed from: private */
    public View.OnClickListener proxyClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            if (FullAdWidget.this.onClickProxy != null) {
                FullAdWidget.this.onClickProxy.onItemClicked(FullAdWidget.this.matchView(view));
            }
        }
    };
    private GestureDetector.SimpleOnGestureListener singleTapOnVideoListener = new GestureDetector.SimpleOnGestureListener() {
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            FullAdWidget.this.proxyClickListener.onClick(FullAdWidget.this.videoViewContainer);
            return true;
        }
    };
    /* access modifiers changed from: private */
    public int startPosition;
    public final VideoView videoView;
    /* access modifiers changed from: private */
    public final RelativeLayout videoViewContainer;
    private Map<View, Integer> viewToId = new HashMap();
    private final WebView webView;
    /* access modifiers changed from: private */
    public final Window window;

    public static class AudioContextWrapper extends ContextWrapper {
        public AudioContextWrapper(Context context) {
            super(context);
        }

        public Object getSystemService(String str) {
            if ("audio".equals(str)) {
                return getApplicationContext().getSystemService(str);
            }
            return super.getSystemService(str);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(int i);
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface ViewEvent {
        public static final int CLOSE_CLICK = 1;
        public static final int CTA_CLICK = 2;
        public static final int MUTE_CLICK = 3;
        public static final int PRIVACY_CLICK = 4;
        public static final int VIDEO_CLICK = 5;
    }

    public FullAdWidget(Context context, Window window2) throws InstantiationException {
        super(context);
        this.window = window2;
        Resources resources = getResources();
        this.matchParentLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
        setLayoutParams(this.matchParentLayoutParams);
        this.videoView = new VideoView(new AudioContextWrapper(context));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        this.videoView.setLayoutParams(layoutParams);
        this.videoViewContainer = new RelativeLayout(context);
        this.videoViewContainer.setTag("videoViewContainer");
        this.videoViewContainer.setLayoutParams(this.matchParentLayoutParams);
        this.videoViewContainer.addView(this.videoView, layoutParams);
        addView(this.videoViewContainer, this.matchParentLayoutParams);
        this.gestureDetector = new GestureDetector(context, this.singleTapOnVideoListener);
        this.webView = ViewUtility.getWebView(context);
        this.webView.setLayoutParams(this.matchParentLayoutParams);
        this.webView.setTag("webView");
        addView(this.webView, this.matchParentLayoutParams);
        this.progressBar = new ProgressBar(context, (AttributeSet) null, 16842872);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, (int) TypedValue.applyDimension(1, 4.0f, resources.getDisplayMetrics()));
        layoutParams2.addRule(12);
        this.progressBar.setLayoutParams(layoutParams2);
        this.progressBar.setMax(100);
        this.progressBar.setIndeterminate(false);
        this.progressBar.setVisibility(4);
        addView(this.progressBar);
        int applyDimension = (int) TypedValue.applyDimension(1, 24.0f, resources.getDisplayMetrics());
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(applyDimension, applyDimension);
        int applyDimension2 = (int) TypedValue.applyDimension(1, 12.0f, resources.getDisplayMetrics());
        layoutParams3.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.muteButton = new ImageView(context);
        this.muteButton.setImageBitmap(ViewUtility.getBitmap(ViewUtility.Asset.unMute, context));
        this.muteButton.setLayoutParams(layoutParams3);
        this.muteButton.setVisibility(8);
        addView(this.muteButton);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(applyDimension, applyDimension);
        layoutParams4.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.closeButton = new ImageView(context);
        this.closeButton.setTag("closeButton");
        this.closeButton.setImageBitmap(ViewUtility.getBitmap(ViewUtility.Asset.close, context));
        layoutParams4.addRule(11);
        this.closeButton.setLayoutParams(layoutParams4);
        this.closeButton.setVisibility(8);
        addView(this.closeButton);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(applyDimension, applyDimension);
        layoutParams5.addRule(12);
        layoutParams5.addRule(11);
        layoutParams5.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.ctaOverlay = new ImageView(context);
        this.ctaOverlay.setTag("ctaOverlay");
        this.ctaOverlay.setLayoutParams(layoutParams5);
        this.ctaOverlay.setImageBitmap(ViewUtility.getBitmap(ViewUtility.Asset.cta, getContext()));
        this.ctaOverlay.setVisibility(8);
        addView(this.ctaOverlay);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(applyDimension, applyDimension);
        layoutParams6.addRule(12);
        layoutParams6.addRule(9);
        layoutParams6.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.privacyOverlay = new ImageView(context);
        this.privacyOverlay.setLayoutParams(layoutParams6);
        this.privacyOverlay.setVisibility(8);
        addView(this.privacyOverlay);
        bindListeners();
        prepare();
    }

    private void bindListener(View view, int i) {
        this.viewToId.put(view, Integer.valueOf(i));
        view.setOnClickListener(this.proxyClickListener);
    }

    private void bindListeners() {
        bindListener(this.closeButton, 1);
        bindListener(this.ctaOverlay, 2);
        bindListener(this.muteButton, 3);
        bindListener(this.privacyOverlay, 4);
        this.viewToId.put(this.videoViewContainer, 5);
        this.videoViewContainer.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                FullAdWidget.this.gestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
        this.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                if (Build.VERSION.SDK_INT >= 26) {
                    mediaPlayer.seekTo((long) FullAdWidget.this.startPosition, 3);
                }
                if (FullAdWidget.this.onPreparedListener != null) {
                    FullAdWidget.this.onPreparedListener.onPrepared(mediaPlayer);
                }
                FullAdWidget.this.muteButton.setVisibility(0);
            }
        });
        this.videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                if (FullAdWidget.this.onErrorListener != null) {
                    return FullAdWidget.this.onErrorListener.onError(mediaPlayer, i, i2);
                }
                return false;
            }
        });
        this.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (FullAdWidget.this.onCompletionListener != null) {
                    FullAdWidget.this.onCompletionListener.onCompletion(mediaPlayer);
                }
                FullAdWidget.this.muteButton.setEnabled(false);
            }
        });
    }

    /* access modifiers changed from: private */
    public int matchView(View view) {
        Integer num = this.viewToId.get(view);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    private void prepare() {
        if (Build.VERSION.SDK_INT >= 17) {
            this.webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        this.videoViewContainer.setVisibility(8);
        this.webView.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void removeImmersiveModeListener() {
        if (Build.VERSION.SDK_INT >= 16) {
            getViewTreeObserver().removeOnGlobalLayoutListener(this.immersiveModeListener);
        } else {
            getViewTreeObserver().removeGlobalOnLayoutListener(this.immersiveModeListener);
        }
    }

    public int getCurrentVideoPosition() {
        return this.videoView.getCurrentPosition();
    }

    public String getUrl() {
        return this.webView.getUrl();
    }

    public int getVideoDuration() {
        return this.videoView.getDuration();
    }

    public boolean isVideoPlaying() {
        return this.videoView.isPlaying();
    }

    public void linkWebView(WebViewClient webViewClient, JavascriptBridge javascriptBridge) {
        WebSettingsUtils.applyDefault(this.webView);
        this.webView.setWebViewClient(webViewClient);
        this.webView.addJavascriptInterface(javascriptBridge, "Android");
    }

    public void pausePlayback() {
        this.videoView.pause();
    }

    public void pauseWeb() {
        this.webView.onPause();
        removeImmersiveModeListener();
    }

    public void playVideo(Uri uri, int i) {
        this.videoViewContainer.setVisibility(0);
        this.videoView.setVideoURI(uri);
        this.privacyOverlay.setImageBitmap(ViewUtility.getBitmap(ViewUtility.Asset.privacy, getContext()));
        this.privacyOverlay.setVisibility(0);
        this.progressBar.setVisibility(0);
        this.progressBar.setMax(this.videoView.getDuration());
        startPlayback(i);
    }

    public void release() {
        this.webView.removeJavascriptInterface("Android");
        this.webView.loadUrl("about:blank");
        removeView(this.webView);
        this.webView.destroy();
        this.videoView.stopPlayback();
        this.videoView.setOnCompletionListener((MediaPlayer.OnCompletionListener) null);
        this.videoView.setOnErrorListener((MediaPlayer.OnErrorListener) null);
        this.videoView.setOnPreparedListener((MediaPlayer.OnPreparedListener) null);
        this.videoView.suspend();
    }

    public void resumeWeb() {
        this.webView.onResume();
    }

    public void setCtaEnabled(boolean z) {
        this.ctaOverlay.setVisibility(z ? 0 : 8);
    }

    public void setImmersiveMode() {
        getViewTreeObserver().addOnGlobalLayoutListener(this.immersiveModeListener);
    }

    public void setMuted(boolean z) {
        Bitmap bitmap = ViewUtility.getBitmap(ViewUtility.Asset.mute, getContext());
        Bitmap bitmap2 = ViewUtility.getBitmap(ViewUtility.Asset.unMute, getContext());
        ImageView imageView = this.muteButton;
        if (!z) {
            bitmap = bitmap2;
        }
        imageView.setImageBitmap(bitmap);
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener2) {
        this.onCompletionListener = onCompletionListener2;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener2) {
        this.onErrorListener = onErrorListener2;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onClickProxy = onItemClickListener;
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener2) {
        this.onPreparedListener = onPreparedListener2;
    }

    public void setProgress(int i, float f) {
        this.progressBar.setMax((int) f);
        this.progressBar.setProgress(i);
    }

    public void showCloseButton(boolean z) {
        this.closeButton.setVisibility(z ? 0 : 8);
    }

    public void showWebsite(String str) {
        String str2 = TAG;
        Log.d(str2, "loadJs: " + str);
        this.webView.loadUrl(str);
        this.webView.setVisibility(0);
        this.videoViewContainer.setVisibility(8);
        this.videoViewContainer.setOnClickListener((View.OnClickListener) null);
        this.progressBar.setVisibility(8);
        this.closeButton.setVisibility(8);
        this.muteButton.setVisibility(8);
        this.ctaOverlay.setVisibility(8);
        this.privacyOverlay.setVisibility(8);
    }

    public boolean startPlayback(int i) {
        if (!this.videoView.isPlaying()) {
            this.videoView.requestFocus();
            this.startPosition = i;
            if (Build.VERSION.SDK_INT < 26) {
                this.videoView.seekTo(this.startPosition);
            }
            this.videoView.start();
        }
        return this.videoView.isPlaying();
    }

    public void stopPlayback() {
        this.videoView.stopPlayback();
    }

    public void updateWindow(boolean z) {
        RelativeLayout.LayoutParams layoutParams;
        if (z) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.window.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int i = displayMetrics.heightPixels;
            int i2 = displayMetrics.widthPixels;
            int i3 = getResources().getConfiguration().orientation;
            if (i3 == 1) {
                this.window.setGravity(83);
                Window window2 = this.window;
                double d = ((double) i2) * NINE_BY_SIXTEEN_ASPECT_RATIO;
                window2.setLayout(i2, (int) Math.round(d));
                layoutParams = new RelativeLayout.LayoutParams(i2, (int) d);
            } else if (i3 == 2) {
                Window window3 = this.window;
                double d2 = ((double) i) * NINE_BY_SIXTEEN_ASPECT_RATIO;
                window3.setLayout((int) Math.round(d2), i);
                this.window.setGravity(85);
                layoutParams = new RelativeLayout.LayoutParams((int) Math.round(d2), i);
                layoutParams.addRule(11, -1);
            } else {
                layoutParams = null;
            }
            this.webView.setLayoutParams(layoutParams);
            this.window.addFlags(288);
            return;
        }
        this.window.setFlags(ByteConstants.KB, ByteConstants.KB);
        this.window.getDecorView().setBackgroundColor(-16777216);
    }
}
