package com.vungle.warren.ui.view;

import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.common.util.UriUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.ui.view.WebViewAPI;

public class VungleWebClient extends WebViewClient implements WebViewAPI {
    public static final String TAG = VungleWebClient.class.getSimpleName();
    private WebViewAPI.MRAIDDelegate MRAIDDelegate;
    private Advertisement advertisement;
    private boolean collectConsent;
    private WebViewAPI.WebClientErrorListener errorListener;
    private String gdprAccept;
    private String gdprBody;
    private String gdprDeny;
    private String gdprTitle;
    private Boolean isViewable;
    private WebView loadedWebView;
    private Placement placement;
    private boolean ready;

    public VungleWebClient(Advertisement advertisement2, Placement placement2) {
        this.advertisement = advertisement2;
        this.placement = placement2;
    }

    public void notifyPropertiesChange(boolean z) {
        if (this.loadedWebView != null) {
            JsonObject jsonObject = new JsonObject();
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.a("width", (Number) Integer.valueOf(this.loadedWebView.getWidth()));
            jsonObject2.a("height", (Number) Integer.valueOf(this.loadedWebView.getHeight()));
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.a("x", (Number) 0);
            jsonObject3.a("y", (Number) 0);
            jsonObject3.a("width", (Number) Integer.valueOf(this.loadedWebView.getWidth()));
            jsonObject3.a("height", (Number) Integer.valueOf(this.loadedWebView.getHeight()));
            JsonObject jsonObject4 = new JsonObject();
            jsonObject4.a("sms", (Boolean) false);
            jsonObject4.a("tel", (Boolean) false);
            jsonObject4.a("calendar", (Boolean) false);
            jsonObject4.a("storePicture", (Boolean) false);
            jsonObject4.a("inlineVideo", (Boolean) false);
            jsonObject.a("maxSize", (JsonElement) jsonObject2);
            jsonObject.a("screenSize", (JsonElement) jsonObject2);
            jsonObject.a("defaultPosition", (JsonElement) jsonObject3);
            jsonObject.a("currentPosition", (JsonElement) jsonObject3);
            jsonObject.a("supports", (JsonElement) jsonObject4);
            jsonObject.a("placementType", this.advertisement.getTemplateType());
            Boolean bool = this.isViewable;
            if (bool != null) {
                jsonObject.a("isViewable", bool);
            }
            jsonObject.a("os", "android");
            jsonObject.a("osVersion", Integer.toString(Build.VERSION.SDK_INT));
            jsonObject.a("incentivized", Boolean.valueOf(this.placement.isIncentivized()));
            jsonObject.a("enableBackImmediately", Boolean.valueOf(this.advertisement.getShowCloseDelay(this.placement.isIncentivized()) == 0));
            jsonObject.a(MediationMetaData.KEY_VERSION, "1.0");
            if (this.collectConsent) {
                jsonObject.a("consentRequired", (Boolean) true);
                jsonObject.a("consentTitleText", this.gdprTitle);
                jsonObject.a("consentBodyText", this.gdprBody);
                jsonObject.a("consentAcceptButtonText", this.gdprAccept);
                jsonObject.a("consentDenyButtonText", this.gdprDeny);
            } else {
                jsonObject.a("consentRequired", (Boolean) false);
            }
            String str = TAG;
            Log.d(str, "loadJsjavascript:window.vungle.mraidBridge.notifyPropertiesChange(" + jsonObject + "," + z + ")");
            WebView webView = this.loadedWebView;
            webView.loadUrl("javascript:window.vungle.mraidBridge.notifyPropertiesChange(" + jsonObject + "," + z + ")");
        }
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        int adType = this.advertisement.getAdType();
        if (adType == 0) {
            webView.loadUrl("javascript:function actionClicked(action){Android.performAction(action);};");
        } else if (adType == 1) {
            this.loadedWebView = webView;
            this.loadedWebView.setVisibility(0);
            notifyPropertiesChange(true);
        } else {
            throw new IllegalArgumentException("Unknown Client Type!");
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        if (Build.VERSION.SDK_INT < 23) {
            Log.e(TAG, "Error desc " + str);
            Log.e(TAG, "Error for URL " + str2);
            String str3 = str2 + " " + str;
            WebViewAPI.WebClientErrorListener webClientErrorListener = this.errorListener;
            if (webClientErrorListener != null) {
                webClientErrorListener.onReceivedError(str3);
            }
        }
    }

    public void setAdVisibility(boolean z) {
        this.isViewable = Boolean.valueOf(z);
        notifyPropertiesChange(false);
    }

    public void setConsentStatus(boolean z, String str, String str2, String str3, String str4) {
        this.collectConsent = z;
        this.gdprTitle = str;
        this.gdprBody = str2;
        this.gdprAccept = str3;
        this.gdprDeny = str4;
    }

    public void setErrorListener(WebViewAPI.WebClientErrorListener webClientErrorListener) {
        this.errorListener = webClientErrorListener;
    }

    public void setMRAIDDelegate(WebViewAPI.MRAIDDelegate mRAIDDelegate) {
        this.MRAIDDelegate = mRAIDDelegate;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String str2 = TAG;
        Log.d(str2, "MRAID Command " + str);
        if (TextUtils.isEmpty(str)) {
            Log.e(TAG, "Invalid URL ");
            return false;
        }
        Uri parse = Uri.parse(str);
        if (parse.getScheme() != null) {
            String scheme = parse.getScheme();
            if (scheme.equals("mraid")) {
                String host = parse.getHost();
                if (host.equals("propertiesChangeCompleted") && !this.ready) {
                    JsonObject createMRAIDArgs = this.advertisement.createMRAIDArgs();
                    webView.loadUrl("javascript:window.vungle.mraidBridge.notifyReadyEvent(" + createMRAIDArgs + ")");
                    this.ready = true;
                } else if (this.MRAIDDelegate != null) {
                    JsonObject jsonObject = new JsonObject();
                    for (String next : parse.getQueryParameterNames()) {
                        jsonObject.a(next, parse.getQueryParameter(next));
                    }
                    if (this.MRAIDDelegate.processCommand(host, jsonObject)) {
                        webView.loadUrl("javascript:window.vungle.mraidBridge.notifyCommandComplete()");
                    }
                }
                return true;
            } else if (UriUtil.HTTP_SCHEME.equalsIgnoreCase(scheme) || UriUtil.HTTPS_SCHEME.equalsIgnoreCase(scheme)) {
                String str3 = TAG;
                Log.d(str3, "Open URL" + str);
                if (this.MRAIDDelegate != null) {
                    JsonObject jsonObject2 = new JsonObject();
                    jsonObject2.a(ReportDBAdapter.ReportColumns.COLUMN_URL, str);
                    this.MRAIDDelegate.processCommand("openNonMraid", jsonObject2);
                }
                return true;
            }
        }
        return false;
    }

    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        if (Build.VERSION.SDK_INT >= 23) {
            Log.e(TAG, "Error desc " + webResourceError.getDescription().toString());
            Log.e(TAG, "Error for URL " + webResourceRequest.getUrl().toString());
            String str = webResourceRequest.getUrl().toString() + " " + webResourceError.getDescription().toString();
            WebViewAPI.WebClientErrorListener webClientErrorListener = this.errorListener;
            if (webClientErrorListener != null) {
                webClientErrorListener.onReceivedError(str);
            }
        }
    }
}
