package com.vungle.warren.ui.view;

import com.google.gson.JsonObject;

public interface WebViewAPI {

    public interface MRAIDDelegate {
        boolean processCommand(String str, JsonObject jsonObject);
    }

    public interface WebClientErrorListener {
        void onReceivedError(String str);
    }

    void notifyPropertiesChange(boolean z);

    void setAdVisibility(boolean z);

    void setConsentStatus(boolean z, String str, String str2, String str3, String str4);

    void setErrorListener(WebClientErrorListener webClientErrorListener);

    void setMRAIDDelegate(MRAIDDelegate mRAIDDelegate);
}
