package com.vungle.warren.ui.presenter;

import android.content.ActivityNotFoundException;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.react.uimanager.ViewProps;
import com.vungle.warren.analytics.AdAnalytics;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.Report;
import com.vungle.warren.model.VisionDataDBAdapter;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.contract.WebAdContract;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.WebViewAPI;
import com.vungle.warren.utility.AsyncFileUtils;
import com.vungle.warren.utility.Scheduler;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class MRAIDAdPresenter implements WebAdContract.WebAdPresenter, WebViewAPI.MRAIDDelegate, WebViewAPI.WebClientErrorListener {
    private static final String ACTION = "action";
    private static final String ACTION_WITH_VALUE = "actionWithValue";
    private static final String CLOSE = "close";
    private static final String CONSENT_ACTION = "consentAction";
    private static final String EXTRA_INCENTIVIZED_SENT = "incentivized_sent";
    private static final String EXTRA_REPORT = "saved_report";
    private static final String FLEXVIEW = "flexview";
    private static final String OPEN = "open";
    private static final String OPEN_NON_MRAID = "openNonMraid";
    private static final String OPEN_PRIVACY = "openPrivacy";
    private static final String SUCCESSFUL_VIEW = "successfulView";
    private static final String TAG = "com.vungle.warren.ui.presenter.MRAIDAdPresenter";
    private static final String TPAT = "tpat";
    private static final String USE_CUSTOM_CLOSE = "useCustomClose";
    private static final String USE_CUSTOM_PRIVACY = "useCustomPrivacy";
    private static final String VIDEO_VIEWED = "videoViewed";
    private long adStartTime;
    /* access modifiers changed from: private */
    public WebAdContract.WebAdView adView;
    private Advertisement advertisement;
    private final AdAnalytics analytics;
    private File assetDir;
    /* access modifiers changed from: private */
    public boolean backEnabled;
    /* access modifiers changed from: private */
    public AdContract.AdvertisementPresenter.EventListener bus;
    private Map<String, Cookie> cookieMap = new HashMap();
    private long duration;
    private final ExecutorService ioExecutor;
    private AtomicBoolean isDestroying = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public Placement placement;
    /* access modifiers changed from: private */
    public Repository.SaveCallback repoCallback = new Repository.SaveCallback() {
        boolean errorHappened = false;

        public void onError(Exception exc) {
            if (!this.errorHappened) {
                this.errorHappened = true;
                if (MRAIDAdPresenter.this.bus != null) {
                    MRAIDAdPresenter.this.bus.onError(new VungleException(26), MRAIDAdPresenter.this.placement.getId());
                }
                MRAIDAdPresenter.this.closeView();
            }
        }

        public void onSaved() {
        }
    };
    /* access modifiers changed from: private */
    public Report report;
    /* access modifiers changed from: private */
    public Repository repository;
    private final Scheduler scheduler;
    private AtomicBoolean sendReportIncentivized = new AtomicBoolean(false);
    private final ExecutorService uiExecutor;
    private WebViewAPI webClient;

    public MRAIDAdPresenter(Advertisement advertisement2, Placement placement2, Repository repository2, Scheduler scheduler2, AdAnalytics adAnalytics, WebViewAPI webViewAPI, OptionsState optionsState, File file, ExecutorService executorService, ExecutorService executorService2) {
        this.advertisement = advertisement2;
        this.repository = repository2;
        this.placement = placement2;
        this.scheduler = scheduler2;
        this.analytics = adAnalytics;
        this.webClient = webViewAPI;
        this.assetDir = file;
        this.ioExecutor = executorService;
        this.uiExecutor = executorService2;
        load(optionsState);
    }

    /* access modifiers changed from: private */
    public void closeView() {
        this.adView.close();
        this.scheduler.cancelAll();
    }

    private void download() {
        reportAction("cta", "");
        try {
            this.analytics.ping(new String[]{this.advertisement.getCTAURL(true)});
            this.adView.open(this.advertisement.getCTAURL(false));
        } catch (ActivityNotFoundException unused) {
        }
    }

    private void load(OptionsState optionsState) {
        Class<Cookie> cls = Cookie.class;
        this.cookieMap.put(Cookie.INCENTIVIZED_TEXT_COOKIE, this.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, cls).get());
        this.cookieMap.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, cls).get());
        this.cookieMap.put(Cookie.CONFIG_COOKIE, this.repository.load(Cookie.CONFIG_COOKIE, cls).get());
        if (optionsState != null) {
            String string = optionsState.getString(EXTRA_REPORT);
            Report report2 = TextUtils.isEmpty(string) ? null : this.repository.load(string, Report.class).get();
            if (report2 != null) {
                this.report = report2;
                this.adStartTime = report2.getAdStartTime();
            }
        }
    }

    private void loadMraid(File file) {
        File file2 = new File(file.getParent());
        final File file3 = new File(file2.getPath() + File.separator + "index.html");
        new AsyncFileUtils(this.ioExecutor, this.uiExecutor).isFileExistAsync(file3, new AsyncFileUtils.FileExist() {
            public void status(boolean z) {
                if (!z) {
                    if (MRAIDAdPresenter.this.bus != null) {
                        MRAIDAdPresenter.this.bus.onError(new VungleException(27), MRAIDAdPresenter.this.placement.getId());
                        MRAIDAdPresenter.this.bus.onError(new VungleException(10), MRAIDAdPresenter.this.placement.getId());
                    }
                    MRAIDAdPresenter.this.adView.close();
                    return;
                }
                WebAdContract.WebAdView access$700 = MRAIDAdPresenter.this.adView;
                access$700.showWebsite("file://" + file3.getPath());
            }
        });
    }

    private void prepare(OptionsState optionsState) {
        String str;
        this.webClient.setMRAIDDelegate(this);
        this.webClient.setErrorListener(this);
        loadMraid(new File(this.assetDir.getPath() + File.separator + Advertisement.KEY_TEMPLATE));
        if (FLEXVIEW.equals(this.advertisement.getTemplateType()) && this.advertisement.getAdConfig().getFlexViewCloseTime() > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    long currentTimeMillis = System.currentTimeMillis();
                    MRAIDAdPresenter.this.report.recordAction("mraidCloseByTimer", "", currentTimeMillis);
                    MRAIDAdPresenter.this.report.recordAction("mraidClose", "", currentTimeMillis);
                    MRAIDAdPresenter.this.repository.save(MRAIDAdPresenter.this.report, MRAIDAdPresenter.this.repoCallback);
                    MRAIDAdPresenter.this.closeView();
                }
            }, (long) (this.advertisement.getAdConfig().getFlexViewCloseTime() * 1000));
        }
        Cookie cookie = this.cookieMap.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        if (cookie == null) {
            str = null;
        } else {
            str = cookie.getString("userID");
        }
        if (this.report == null) {
            this.adStartTime = System.currentTimeMillis();
            this.report = new Report(this.advertisement, this.placement, this.adStartTime, str);
            this.report.setTtDownload(this.advertisement.getTtDownload());
            this.repository.save(this.report, this.repoCallback);
        }
        Cookie cookie2 = this.cookieMap.get(Cookie.CONSENT_COOKIE);
        if (cookie2 != null) {
            boolean z = cookie2.getBoolean("is_country_data_protected").booleanValue() && "unknown".equals(cookie2.getString("consent_status"));
            this.webClient.setConsentStatus(z, cookie2.getString("consent_title"), cookie2.getString("consent_message"), cookie2.getString("button_accept"), cookie2.getString("button_deny"));
            if (z) {
                cookie2.putValue("consent_status", "opted_out_by_timeout");
                cookie2.putValue(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, Long.valueOf(System.currentTimeMillis() / 1000));
                cookie2.putValue("consent_source", "vungle_modal");
                this.repository.save(cookie2, this.repoCallback);
            }
        }
        int showCloseDelay = this.advertisement.getShowCloseDelay(this.placement.isIncentivized());
        if (showCloseDelay > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    boolean unused = MRAIDAdPresenter.this.backEnabled = true;
                }
            }, (long) showCloseDelay);
        } else {
            this.backEnabled = true;
        }
        this.adView.updateWindow(FLEXVIEW.equals(this.advertisement.getTemplateType()));
        AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
        if (eventListener != null) {
            eventListener.onNext(ViewProps.START, (String) null, this.placement.getId());
        }
    }

    public void detach(boolean z) {
        stop(z | true ? 1 : 0);
        this.adView.destroyAdView();
    }

    public void generateSaveState(OptionsState optionsState) {
        if (optionsState != null) {
            this.repository.save(this.report, this.repoCallback);
            optionsState.put(EXTRA_REPORT, this.report.getId());
            optionsState.put(EXTRA_INCENTIVIZED_SENT, this.sendReportIncentivized.get());
        }
    }

    public boolean handleExit(String str) {
        Placement placement2;
        if (str == null) {
            if (this.backEnabled) {
                this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            }
            return false;
        } else if (this.advertisement == null || (placement2 = this.placement) == null) {
            Log.e(TAG, "Unable to close advertisement");
            return false;
        } else if (!placement2.getId().equals(str)) {
            Log.e(TAG, "Cannot close FlexView Ad with invalid placement reference id");
            return false;
        } else if (!FLEXVIEW.equals(this.advertisement.getTemplateType())) {
            Log.e(TAG, "Cannot close a Non FlexView ad");
            return false;
        } else {
            this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            reportAction("mraidCloseByApi", (String) null);
            return true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMraidAction(java.lang.String r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            r1 = -314498168(0xffffffffed412388, float:-3.7358476E27)
            r2 = 2
            r3 = 1
            if (r0 == r1) goto L_0x002a
            r1 = 94756344(0x5a5ddf8, float:1.5598064E-35)
            if (r0 == r1) goto L_0x0020
            r1 = 1427818632(0x551ac888, float:1.06366291E13)
            if (r0 == r1) goto L_0x0016
            goto L_0x0034
        L_0x0016:
            java.lang.String r0 = "download"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 1
            goto L_0x0035
        L_0x0020:
            java.lang.String r0 = "close"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 0
            goto L_0x0035
        L_0x002a:
            java.lang.String r0 = "privacy"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 2
            goto L_0x0035
        L_0x0034:
            r0 = -1
        L_0x0035:
            if (r0 == 0) goto L_0x0057
            if (r0 == r3) goto L_0x0053
            if (r0 != r2) goto L_0x003c
            goto L_0x005a
        L_0x003c:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown action "
            r1.append(r2)
            r1.append(r5)
            java.lang.String r5 = r1.toString()
            r0.<init>(r5)
            throw r0
        L_0x0053:
            r4.download()
            goto L_0x005a
        L_0x0057:
            r4.closeView()
        L_0x005a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.ui.presenter.MRAIDAdPresenter.onMraidAction(java.lang.String):void");
    }

    public void onReceivedError(String str) {
        Report report2 = this.report;
        if (report2 != null) {
            report2.recordError(str);
            this.repository.save(this.report, this.repoCallback);
        }
    }

    public void onViewConfigurationChanged() {
        this.adView.updateWindow(this.advertisement.getTemplateType().equals(FLEXVIEW));
        this.webClient.notifyPropertiesChange(true);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x015a, code lost:
        if (r1.equals("gone") != false) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01a6, code lost:
        if (r1.equals("gone") != false) goto L_0x01b4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0160 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01b6 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean processCommand(java.lang.String r17, com.google.gson.JsonObject r18) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            int r3 = r17.hashCode()
            java.lang.String r4 = "open"
            java.lang.String r5 = "useCustomPrivacy"
            java.lang.String r6 = "openNonMraid"
            java.lang.String r7 = "successfulView"
            r9 = 2
            r10 = 0
            r11 = 1
            switch(r3) {
                case -1912374177: goto L_0x007b;
                case -1422950858: goto L_0x0071;
                case -735200587: goto L_0x0067;
                case -660787472: goto L_0x005d;
                case -511324706: goto L_0x0052;
                case -418575596: goto L_0x004a;
                case -348095344: goto L_0x0041;
                case 3417674: goto L_0x0039;
                case 3566511: goto L_0x002f;
                case 94756344: goto L_0x0025;
                case 1614272768: goto L_0x001a;
                default: goto L_0x0018;
            }
        L_0x0018:
            goto L_0x0084
        L_0x001a:
            java.lang.String r3 = "useCustomClose"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0084
            r3 = 7
            goto L_0x0085
        L_0x0025:
            java.lang.String r3 = "close"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0084
            r3 = 0
            goto L_0x0085
        L_0x002f:
            java.lang.String r3 = "tpat"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0084
            r3 = 3
            goto L_0x0085
        L_0x0039:
            boolean r3 = r1.equals(r4)
            if (r3 == 0) goto L_0x0084
            r3 = 5
            goto L_0x0085
        L_0x0041:
            boolean r3 = r1.equals(r5)
            if (r3 == 0) goto L_0x0084
            r3 = 8
            goto L_0x0085
        L_0x004a:
            boolean r3 = r1.equals(r6)
            if (r3 == 0) goto L_0x0084
            r3 = 6
            goto L_0x0085
        L_0x0052:
            java.lang.String r3 = "openPrivacy"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0084
            r3 = 9
            goto L_0x0085
        L_0x005d:
            java.lang.String r3 = "consentAction"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0084
            r3 = 1
            goto L_0x0085
        L_0x0067:
            java.lang.String r3 = "actionWithValue"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0084
            r3 = 2
            goto L_0x0085
        L_0x0071:
            java.lang.String r3 = "action"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0084
            r3 = 4
            goto L_0x0085
        L_0x007b:
            boolean r3 = r1.equals(r7)
            if (r3 == 0) goto L_0x0084
            r3 = 10
            goto L_0x0085
        L_0x0084:
            r3 = -1
        L_0x0085:
            java.lang.String r12 = "gone"
            java.lang.String r13 = "url"
            r14 = 3178655(0x30809f, float:4.454244E-39)
            java.lang.String r15 = "event"
            r8 = 0
            switch(r3) {
                case 0: goto L_0x0359;
                case 1: goto L_0x031c;
                case 2: goto L_0x0219;
                case 3: goto L_0x0205;
                case 4: goto L_0x0204;
                case 5: goto L_0x01d3;
                case 6: goto L_0x01d3;
                case 7: goto L_0x017d;
                case 8: goto L_0x0129;
                case 9: goto L_0x011b;
                case 10: goto L_0x0093;
                default: goto L_0x0092;
            }
        L_0x0092:
            return r10
        L_0x0093:
            com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter$EventListener r1 = r0.bus
            if (r1 == 0) goto L_0x00a4
            com.vungle.warren.model.Placement r2 = r0.placement
            if (r2 != 0) goto L_0x009d
            r2 = r8
            goto L_0x00a1
        L_0x009d:
            java.lang.String r2 = r2.getId()
        L_0x00a1:
            r1.onNext(r7, r8, r2)
        L_0x00a4:
            java.util.Map<java.lang.String, com.vungle.warren.model.Cookie> r1 = r0.cookieMap
            java.lang.String r2 = "configSettings"
            java.lang.Object r1 = r1.get(r2)
            com.vungle.warren.model.Cookie r1 = (com.vungle.warren.model.Cookie) r1
            com.vungle.warren.model.Placement r2 = r0.placement
            boolean r2 = r2.isIncentivized()
            if (r2 == 0) goto L_0x011a
            if (r1 == 0) goto L_0x011a
            java.lang.String r2 = "isReportIncentivizedEnabled"
            java.lang.Boolean r1 = r1.getBoolean(r2)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x011a
            java.util.concurrent.atomic.AtomicBoolean r1 = r0.sendReportIncentivized
            boolean r1 = r1.getAndSet(r11)
            if (r1 != 0) goto L_0x011a
            com.google.gson.JsonObject r1 = new com.google.gson.JsonObject
            r1.<init>()
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Placement r3 = r0.placement
            java.lang.String r3 = r3.getId()
            r2.<init>((java.lang.String) r3)
            java.lang.String r3 = "placement_reference_id"
            r1.a((java.lang.String) r3, (com.google.gson.JsonElement) r2)
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Advertisement r3 = r0.advertisement
            java.lang.String r3 = r3.getAppID()
            r2.<init>((java.lang.String) r3)
            java.lang.String r3 = "app_id"
            r1.a((java.lang.String) r3, (com.google.gson.JsonElement) r2)
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r3 = r0.report
            long r3 = r3.getAdStartTime()
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r2.<init>((java.lang.Number) r3)
            java.lang.String r3 = "adStartTime"
            r1.a((java.lang.String) r3, (com.google.gson.JsonElement) r2)
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r3 = r0.report
            java.lang.String r3 = r3.getUserID()
            r2.<init>((java.lang.String) r3)
            java.lang.String r3 = "user"
            r1.a((java.lang.String) r3, (com.google.gson.JsonElement) r2)
            com.vungle.warren.analytics.AdAnalytics r2 = r0.analytics
            r2.ri(r1)
        L_0x011a:
            return r11
        L_0x011b:
            com.google.gson.JsonElement r1 = r2.a((java.lang.String) r13)
            java.lang.String r1 = r1.i()
            com.vungle.warren.ui.contract.WebAdContract$WebAdView r2 = r0.adView
            r2.open(r1)
            return r11
        L_0x0129:
            com.google.gson.JsonElement r1 = r2.a((java.lang.String) r5)
            java.lang.String r1 = r1.i()
            int r2 = r1.hashCode()
            if (r2 == r14) goto L_0x0156
            r3 = 3569038(0x36758e, float:5.001287E-39)
            if (r2 == r3) goto L_0x014c
            r3 = 97196323(0x5cb1923, float:1.9099262E-35)
            if (r2 == r3) goto L_0x0142
            goto L_0x015d
        L_0x0142:
            java.lang.String r2 = "false"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x015d
            r10 = 2
            goto L_0x015e
        L_0x014c:
            java.lang.String r2 = "true"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x015d
            r10 = 1
            goto L_0x015e
        L_0x0156:
            boolean r2 = r1.equals(r12)
            if (r2 == 0) goto L_0x015d
            goto L_0x015e
        L_0x015d:
            r10 = -1
        L_0x015e:
            if (r10 == 0) goto L_0x017c
            if (r10 == r11) goto L_0x017c
            if (r10 != r9) goto L_0x0165
            goto L_0x017c
        L_0x0165:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unknown value "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x017c:
            return r11
        L_0x017d:
            java.lang.String r1 = "sdkCloseButton"
            com.google.gson.JsonElement r1 = r2.a((java.lang.String) r1)
            java.lang.String r1 = r1.i()
            int r2 = r1.hashCode()
            r3 = -1901805651(0xffffffff8ea4bfad, float:-4.06137E-30)
            if (r2 == r3) goto L_0x01a9
            if (r2 == r14) goto L_0x01a2
            r3 = 466743410(0x1bd1f072, float:3.4731534E-22)
            if (r2 == r3) goto L_0x0198
            goto L_0x01b3
        L_0x0198:
            java.lang.String r2 = "visible"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x01b3
            r10 = 2
            goto L_0x01b4
        L_0x01a2:
            boolean r2 = r1.equals(r12)
            if (r2 == 0) goto L_0x01b3
            goto L_0x01b4
        L_0x01a9:
            java.lang.String r2 = "invisible"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x01b3
            r10 = 1
            goto L_0x01b4
        L_0x01b3:
            r10 = -1
        L_0x01b4:
            if (r10 == 0) goto L_0x01d2
            if (r10 == r11) goto L_0x01d2
            if (r10 != r9) goto L_0x01bb
            goto L_0x01d2
        L_0x01bb:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unknown value "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x01d2:
            return r11
        L_0x01d3:
            boolean r3 = r4.equalsIgnoreCase(r1)
            if (r3 == 0) goto L_0x01e4
            java.lang.String r1 = "mraidOpen"
            r0.reportAction(r1, r8)
            java.lang.String r1 = "download"
            r0.reportAction(r1, r8)
            goto L_0x01ef
        L_0x01e4:
            boolean r1 = r6.equalsIgnoreCase(r1)
            if (r1 == 0) goto L_0x01ef
            java.lang.String r1 = "nonMraidOpen"
            r0.reportAction(r1, r8)
        L_0x01ef:
            com.google.gson.JsonElement r1 = r2.a((java.lang.String) r13)
            java.lang.String r1 = r1.i()
            com.vungle.warren.download.ApkDownloadManager r2 = com.vungle.warren.download.ApkDownloadManager.getInstance()
            com.vungle.warren.model.Advertisement r3 = r0.advertisement
            boolean r3 = r3.isRequiresNonMarketInstall()
            r2.download(r1, r3)
        L_0x0204:
            return r11
        L_0x0205:
            com.google.gson.JsonElement r1 = r2.a((java.lang.String) r15)
            java.lang.String r1 = r1.i()
            com.vungle.warren.analytics.AdAnalytics r2 = r0.analytics
            com.vungle.warren.model.Advertisement r3 = r0.advertisement
            java.lang.String[] r1 = r3.getTpatUrls(r1)
            r2.ping(r1)
            return r11
        L_0x0219:
            com.google.gson.JsonElement r1 = r2.a((java.lang.String) r15)
            java.lang.String r1 = r1.i()
            java.lang.String r3 = "value"
            com.google.gson.JsonElement r2 = r2.a((java.lang.String) r3)
            java.lang.String r2 = r2.i()
            com.vungle.warren.model.Report r3 = r0.report
            long r4 = java.lang.System.currentTimeMillis()
            r3.recordAction(r1, r2, r4)
            com.vungle.warren.persistence.Repository r3 = r0.repository
            com.vungle.warren.model.Report r4 = r0.report
            com.vungle.warren.persistence.Repository$SaveCallback r5 = r0.repoCallback
            r3.save(r4, r5)
            java.lang.String r3 = "videoViewed"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0300
            long r3 = r0.duration
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x0300
            float r3 = java.lang.Float.parseFloat(r2)     // Catch:{ NumberFormatException -> 0x025b }
            long r4 = r0.duration     // Catch:{ NumberFormatException -> 0x025b }
            float r4 = (float) r4
            float r3 = r3 / r4
            r4 = 1120403456(0x42c80000, float:100.0)
            float r3 = r3 * r4
            int r10 = (int) r3
            goto L_0x0262
        L_0x025b:
            java.lang.String r3 = TAG
            java.lang.String r4 = "value for videoViewed is null !"
            android.util.Log.e(r3, r4)
        L_0x0262:
            if (r10 <= 0) goto L_0x0300
            com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter$EventListener r3 = r0.bus
            if (r3 == 0) goto L_0x0286
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "percentViewed:"
            r4.append(r5)
            r4.append(r10)
            java.lang.String r4 = r4.toString()
            com.vungle.warren.model.Placement r5 = r0.placement
            if (r5 != 0) goto L_0x027f
            r5 = r8
            goto L_0x0283
        L_0x027f:
            java.lang.String r5 = r5.getId()
        L_0x0283:
            r3.onNext(r4, r8, r5)
        L_0x0286:
            java.util.Map<java.lang.String, com.vungle.warren.model.Cookie> r3 = r0.cookieMap
            java.lang.String r4 = "configSettings"
            java.lang.Object r3 = r3.get(r4)
            com.vungle.warren.model.Cookie r3 = (com.vungle.warren.model.Cookie) r3
            com.vungle.warren.model.Placement r4 = r0.placement
            boolean r4 = r4.isIncentivized()
            if (r4 == 0) goto L_0x0300
            r4 = 75
            if (r10 <= r4) goto L_0x0300
            if (r3 == 0) goto L_0x0300
            java.lang.String r4 = "isReportIncentivizedEnabled"
            java.lang.Boolean r3 = r3.getBoolean(r4)
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x0300
            java.util.concurrent.atomic.AtomicBoolean r3 = r0.sendReportIncentivized
            boolean r3 = r3.getAndSet(r11)
            if (r3 != 0) goto L_0x0300
            com.google.gson.JsonObject r3 = new com.google.gson.JsonObject
            r3.<init>()
            com.google.gson.JsonPrimitive r4 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Placement r5 = r0.placement
            java.lang.String r5 = r5.getId()
            r4.<init>((java.lang.String) r5)
            java.lang.String r5 = "placement_reference_id"
            r3.a((java.lang.String) r5, (com.google.gson.JsonElement) r4)
            com.google.gson.JsonPrimitive r4 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Advertisement r5 = r0.advertisement
            java.lang.String r5 = r5.getAppID()
            r4.<init>((java.lang.String) r5)
            java.lang.String r5 = "app_id"
            r3.a((java.lang.String) r5, (com.google.gson.JsonElement) r4)
            com.google.gson.JsonPrimitive r4 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r5 = r0.report
            long r5 = r5.getAdStartTime()
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4.<init>((java.lang.Number) r5)
            java.lang.String r5 = "adStartTime"
            r3.a((java.lang.String) r5, (com.google.gson.JsonElement) r4)
            com.google.gson.JsonPrimitive r4 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r5 = r0.report
            java.lang.String r5 = r5.getUserID()
            r4.<init>((java.lang.String) r5)
            java.lang.String r5 = "user"
            r3.a((java.lang.String) r5, (com.google.gson.JsonElement) r4)
            com.vungle.warren.analytics.AdAnalytics r4 = r0.analytics
            r4.ri(r3)
        L_0x0300:
            java.lang.String r3 = "videoLength"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x0316
            long r4 = java.lang.Long.parseLong(r2)
            r0.duration = r4
            r0.reportAction(r3, r2)
            com.vungle.warren.ui.view.WebViewAPI r1 = r0.webClient
            r1.notifyPropertiesChange(r11)
        L_0x0316:
            com.vungle.warren.ui.contract.WebAdContract$WebAdView r1 = r0.adView
            r1.setVisibility(r11)
            return r11
        L_0x031c:
            java.util.Map<java.lang.String, com.vungle.warren.model.Cookie> r1 = r0.cookieMap
            java.lang.String r3 = "consentIsImportantToVungle"
            java.lang.Object r1 = r1.get(r3)
            com.vungle.warren.model.Cookie r1 = (com.vungle.warren.model.Cookie) r1
            if (r1 != 0) goto L_0x032d
            com.vungle.warren.model.Cookie r1 = new com.vungle.warren.model.Cookie
            r1.<init>(r3)
        L_0x032d:
            com.google.gson.JsonElement r2 = r2.a((java.lang.String) r15)
            java.lang.String r2 = r2.i()
            java.lang.String r3 = "consent_status"
            r1.putValue(r3, r2)
            java.lang.String r2 = "consent_source"
            java.lang.String r3 = "vungle_modal"
            r1.putValue(r2, r3)
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            java.lang.String r3 = "timestamp"
            r1.putValue(r3, r2)
            com.vungle.warren.persistence.Repository r2 = r0.repository
            com.vungle.warren.persistence.Repository$SaveCallback r3 = r0.repoCallback
            r2.save(r1, r3)
            return r11
        L_0x0359:
            java.lang.String r1 = "mraidClose"
            r0.reportAction(r1, r8)
            r16.closeView()
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.ui.presenter.MRAIDAdPresenter.processCommand(java.lang.String, com.google.gson.JsonObject):boolean");
    }

    public void reportAction(String str, String str2) {
        if (str.equals("videoLength")) {
            this.duration = Long.parseLong(str2);
            this.report.setVideoLength(this.duration);
            this.repository.save(this.report, this.repoCallback);
            return;
        }
        this.report.recordAction(str, str2, System.currentTimeMillis());
        this.repository.save(this.report, this.repoCallback);
    }

    public void restoreFromSave(OptionsState optionsState) {
        if (optionsState != null) {
            boolean z = optionsState.getBoolean(EXTRA_INCENTIVIZED_SENT, false);
            if (z) {
                this.sendReportIncentivized.set(z);
            }
            if (this.report == null) {
                this.adView.close();
            }
        }
    }

    public void setAdVisibility(boolean z) {
        this.webClient.setAdVisibility(z);
    }

    public void setEventListener(AdContract.AdvertisementPresenter.EventListener eventListener) {
        this.bus = eventListener;
    }

    public void start() {
        this.adView.setImmersiveMode();
        this.adView.resumeWeb();
        setAdVisibility(true);
    }

    public void stop(@AdContract.AdStopReason int i) {
        boolean z = (i & 1) != 0;
        boolean z2 = (i & 2) != 0;
        boolean z3 = (i & 4) != 0;
        this.adView.pauseWeb();
        setAdVisibility(false);
        if (!z && z2 && !this.isDestroying.getAndSet(true)) {
            WebViewAPI webViewAPI = this.webClient;
            String str = null;
            if (webViewAPI != null) {
                webViewAPI.setMRAIDDelegate((WebViewAPI.MRAIDDelegate) null);
            }
            this.report.setAdDuration(System.currentTimeMillis() - this.adStartTime);
            if (z3) {
                reportAction("mraidCloseByApi", (String) null);
            }
            this.repository.save(this.report, this.repoCallback);
            AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
            if (eventListener != null) {
                if (this.report.isCTAClicked()) {
                    str = "isCTAClicked";
                }
                eventListener.onNext(ViewProps.END, str, this.placement.getId());
            }
        }
    }

    public void attach(WebAdContract.WebAdView webAdView, OptionsState optionsState) {
        boolean z = false;
        this.isDestroying.set(false);
        this.adView = webAdView;
        webAdView.setPresenter(this);
        int settings = this.advertisement.getAdConfig().getSettings();
        if (settings > 0) {
            if ((settings & 2) == 2) {
                z = true;
            }
            this.backEnabled = z;
        }
        int adOrientation = this.advertisement.getAdConfig().getAdOrientation();
        int i = 6;
        if (adOrientation == 3) {
            int orientation = this.advertisement.getOrientation();
            if (orientation != 0) {
                if (orientation != 1) {
                    i = -1;
                }
                String str = TAG;
                Log.d(str, "Requested Orientation " + i);
                webAdView.setOrientation(i);
                prepare(optionsState);
            }
        } else if (adOrientation != 0) {
            if (adOrientation != 1) {
                i = 4;
            }
            String str2 = TAG;
            Log.d(str2, "Requested Orientation " + i);
            webAdView.setOrientation(i);
            prepare(optionsState);
        }
        i = 7;
        String str22 = TAG;
        Log.d(str22, "Requested Orientation " + i);
        webAdView.setOrientation(i);
        prepare(optionsState);
    }
}
