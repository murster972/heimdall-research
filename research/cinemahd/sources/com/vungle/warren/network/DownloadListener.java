package com.vungle.warren.network;

import java.io.File;

public interface DownloadListener {
    void onComplete(long j, File file);

    void onError(long j, Throwable th);

    void onProgress(int i, int i2);
}
