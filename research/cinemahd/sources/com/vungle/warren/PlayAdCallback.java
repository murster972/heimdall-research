package com.vungle.warren;

import com.vungle.warren.error.VungleException;

public interface PlayAdCallback {
    void onAdEnd(String str, boolean z, boolean z2);

    void onAdStart(String str);

    void onError(String str, VungleException vungleException);
}
