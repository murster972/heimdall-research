package com.vungle.warren;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.Keep;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.facebook.react.uimanager.ViewProps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.vungle.warren.AdConfig;
import com.vungle.warren.VungleSettings;
import com.vungle.warren.downloader.DownloadRequest;
import com.vungle.warren.downloader.Downloader;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.VisionDataDBAdapter;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.tasks.SendReportsJob;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.VungleActivity;
import com.vungle.warren.ui.VungleFlexViewActivity;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.view.VungleNativeView;
import com.vungle.warren.utility.Executors;
import com.vungle.warren.utility.SDKExecutors;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Keep
public class Vungle {
    private static final String COM_VUNGLE_SDK = "com.vungle.sdk";
    /* access modifiers changed from: private */
    public static final String TAG = "com.vungle.warren.Vungle";
    static final Vungle _instance = new Vungle();
    /* access modifiers changed from: private */
    public static CacheManager.Listener cacheListener = new CacheManager.Listener() {
        public void onCacheChanged() {
            if (Vungle._instance.context != null) {
                Vungle.stopPlaying();
                ServiceLocator instance = ServiceLocator.getInstance(Vungle._instance.context);
                CacheManager cacheManager = (CacheManager) instance.getService(CacheManager.class);
                Downloader downloader = (Downloader) instance.getService(Downloader.class);
                if (cacheManager.getCache() != null) {
                    List<DownloadRequest> allRequests = downloader.getAllRequests();
                    String path = cacheManager.getCache().getPath();
                    for (DownloadRequest next : allRequests) {
                        if (!next.path.startsWith(path)) {
                            downloader.cancel(next);
                        }
                    }
                }
                downloader.init();
            }
        }
    };
    /* access modifiers changed from: private */
    public static AtomicBoolean isDepInit = new AtomicBoolean(false);
    private static volatile boolean isInitialized;
    private static AtomicBoolean isInitializing = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public volatile String appID;
    /* access modifiers changed from: private */
    public volatile Consent consent;
    /* access modifiers changed from: private */
    public volatile String consentVersion;
    /* access modifiers changed from: private */
    public Context context;
    private Gson gson = new GsonBuilder().a();
    /* access modifiers changed from: private */
    public Map<String, Boolean> playOperations = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public volatile boolean shouldTransmitIMEI;
    /* access modifiers changed from: private */
    public volatile String userIMEI;

    @Keep
    public enum Consent {
        OPTED_IN,
        OPTED_OUT
    }

    private Vungle() {
    }

    static Context appContext() {
        Vungle vungle = _instance;
        if (vungle != null) {
            return vungle.context;
        }
        return null;
    }

    public static boolean canPlayAd(String str) {
        Class cls = Repository.class;
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized");
            return false;
        }
        Advertisement advertisement = ((Repository) ServiceLocator.getInstance(_instance.context).getService(cls)).findValidAdvertisementForPlacement(str).get();
        Placement placement = ((Repository) ServiceLocator.getInstance(_instance.context).getService(cls)).load(str, Placement.class).get();
        if (advertisement == null || placement == null || placement.getPlacementAdType() != 0 || (!AdConfig.AdSize.isDefaultAdSize(placement.getAdSize()) && !placement.getAdSize().equals(advertisement.getAdConfig().getAdSize()))) {
            return false;
        }
        return canPlayAd(advertisement);
    }

    private static void clearCache() {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized");
            return;
        }
        final ServiceLocator instance = ServiceLocator.getInstance(_instance.context);
        ((SDKExecutors) instance.getService(SDKExecutors.class)).getVungleExecutor().execute(new Runnable() {
            public void run() {
                ((Downloader) instance.getService(Downloader.class)).cancelAll();
                ((Repository) instance.getService(Repository.class)).clearAllData();
                ((AdLoader) instance.getService(AdLoader.class)).clear();
                Vungle._instance.playOperations.clear();
                Vungle._instance.configure(((RuntimeValues) instance.getService(RuntimeValues.class)).initCallback);
            }
        });
    }

    public static boolean closeFlexViewAd(String str) {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized, can't close flex view ad");
            return false;
        }
        Intent intent = new Intent(AdContract.AdvertisementBus.ACTION);
        intent.putExtra("placement", str);
        intent.putExtra(AdContract.AdvertisementBus.COMMAND, AdContract.AdvertisementBus.CLOSE_FLEX);
        LocalBroadcastManager.a(_instance.context).a(intent);
        return true;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:130|131) */
    /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
        android.util.Log.e(TAG, "not able to apply vision data config");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:130:0x0329 */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x03a6  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x03b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void configure(com.vungle.warren.InitCallback r24) {
        /*
            r23 = this;
            r1 = r23
            r2 = r24
            java.lang.String r0 = "clever_cache"
            java.lang.String r3 = "button_deny"
            java.lang.String r4 = "button_accept"
            java.lang.String r5 = "consent_message_version"
            java.lang.String r6 = "consent_message"
            java.lang.String r7 = "consent_title"
            java.lang.String r8 = "is_country_data_protected"
            java.lang.String r9 = "apk_direct_download"
            r10 = 2
            android.content.Context r13 = r1.context     // Catch:{ all -> 0x0390 }
            if (r13 == 0) goto L_0x0388
            android.content.Context r13 = r1.context     // Catch:{ all -> 0x0390 }
            com.vungle.warren.ServiceLocator r13 = com.vungle.warren.ServiceLocator.getInstance(r13)     // Catch:{ all -> 0x0390 }
            java.lang.Class<com.vungle.warren.VungleApiClient> r14 = com.vungle.warren.VungleApiClient.class
            java.lang.Object r14 = r13.getService(r14)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.VungleApiClient r14 = (com.vungle.warren.VungleApiClient) r14     // Catch:{ all -> 0x0390 }
            java.lang.Class<com.vungle.warren.persistence.Repository> r15 = com.vungle.warren.persistence.Repository.class
            java.lang.Object r15 = r13.getService(r15)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.persistence.Repository r15 = (com.vungle.warren.persistence.Repository) r15     // Catch:{ all -> 0x0390 }
            java.lang.Class<com.vungle.warren.tasks.JobRunner> r11 = com.vungle.warren.tasks.JobRunner.class
            java.lang.Object r11 = r13.getService(r11)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.tasks.JobRunner r11 = (com.vungle.warren.tasks.JobRunner) r11     // Catch:{ all -> 0x0390 }
            retrofit2.Response r12 = r14.config()     // Catch:{ all -> 0x0390 }
            if (r12 != 0) goto L_0x004c
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0390 }
            r0.<init>(r10)     // Catch:{ all -> 0x0390 }
            r2.onError(r0)     // Catch:{ all -> 0x0390 }
            java.util.concurrent.atomic.AtomicBoolean r0 = isInitializing     // Catch:{ all -> 0x0390 }
            r3 = 0
            r0.set(r3)     // Catch:{ all -> 0x0390 }
            return
        L_0x004c:
            boolean r16 = r12.isSuccessful()     // Catch:{ all -> 0x0390 }
            r17 = r11
            r10 = 0
            if (r16 != 0) goto L_0x0090
            long r3 = r14.getRetryAfterHeaderValue(r12)     // Catch:{ all -> 0x0390 }
            int r0 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x0080
            com.vungle.warren.Vungle r0 = _instance     // Catch:{ all -> 0x0390 }
            java.lang.String r0 = r0.appID     // Catch:{ all -> 0x0390 }
            com.vungle.warren.tasks.JobInfo r0 = com.vungle.warren.tasks.ReconfigJob.makeJobInfo(r0)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.tasks.JobInfo r0 = r0.setDelay(r3)     // Catch:{ all -> 0x0390 }
            r3 = r17
            r3.execute(r0)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0390 }
            r3 = 14
            r0.<init>(r3)     // Catch:{ all -> 0x0390 }
            r2.onError(r0)     // Catch:{ all -> 0x0390 }
            java.util.concurrent.atomic.AtomicBoolean r0 = isInitializing     // Catch:{ all -> 0x0390 }
            r3 = 0
            r0.set(r3)     // Catch:{ all -> 0x0390 }
            return
        L_0x0080:
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0390 }
            r3 = 3
            r0.<init>(r3)     // Catch:{ all -> 0x0390 }
            r2.onError(r0)     // Catch:{ all -> 0x0390 }
            java.util.concurrent.atomic.AtomicBoolean r0 = isInitializing     // Catch:{ all -> 0x0390 }
            r3 = 0
            r0.set(r3)     // Catch:{ all -> 0x0390 }
            return
        L_0x0090:
            r18 = r17
            android.content.Context r10 = r1.context     // Catch:{ all -> 0x0390 }
            java.lang.String r11 = "com.vungle.sdk"
            r16 = r9
            r9 = 0
            android.content.SharedPreferences r10 = r10.getSharedPreferences(r11, r9)     // Catch:{ all -> 0x0390 }
            java.lang.String r11 = "reported"
            boolean r11 = r10.getBoolean(r11, r9)     // Catch:{ all -> 0x0390 }
            if (r11 != 0) goto L_0x00b1
            retrofit2.Call r9 = r14.reportNew()     // Catch:{ all -> 0x0390 }
            com.vungle.warren.Vungle$3 r11 = new com.vungle.warren.Vungle$3     // Catch:{ all -> 0x0390 }
            r11.<init>(r10)     // Catch:{ all -> 0x0390 }
            r9.enqueue(r11)     // Catch:{ all -> 0x0390 }
        L_0x00b1:
            java.lang.Object r9 = r12.body()     // Catch:{ all -> 0x0390 }
            com.google.gson.JsonObject r9 = (com.google.gson.JsonObject) r9     // Catch:{ all -> 0x0390 }
            java.lang.String r11 = "placements"
            com.google.gson.JsonArray r11 = r9.b(r11)     // Catch:{ all -> 0x0390 }
            int r12 = r11.size()     // Catch:{ all -> 0x0390 }
            if (r12 != 0) goto L_0x00d2
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0390 }
            r3 = 0
            r0.<init>(r3)     // Catch:{ all -> 0x0390 }
            r2.onError(r0)     // Catch:{ all -> 0x0390 }
            java.util.concurrent.atomic.AtomicBoolean r0 = isInitializing     // Catch:{ all -> 0x0390 }
            r0.set(r3)     // Catch:{ all -> 0x0390 }
            return
        L_0x00d2:
            com.vungle.warren.CleverCacheSettings r12 = com.vungle.warren.CleverCacheSettings.fromJson(r9)     // Catch:{ all -> 0x0390 }
            java.lang.Class<com.vungle.warren.downloader.Downloader> r14 = com.vungle.warren.downloader.Downloader.class
            java.lang.Object r14 = r13.getService(r14)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.downloader.Downloader r14 = (com.vungle.warren.downloader.Downloader) r14     // Catch:{ all -> 0x0390 }
            if (r12 == 0) goto L_0x011d
            r2 = 0
            java.lang.String r2 = r10.getString(r0, r2)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.CleverCacheSettings r2 = com.vungle.warren.CleverCacheSettings.deserializeFromString(r2)     // Catch:{ all -> 0x0390 }
            if (r2 == 0) goto L_0x00fa
            long r19 = r2.getTimestamp()     // Catch:{ all -> 0x0390 }
            long r21 = r12.getTimestamp()     // Catch:{ all -> 0x0390 }
            int r2 = (r19 > r21 ? 1 : (r19 == r21 ? 0 : -1))
            if (r2 == 0) goto L_0x00f8
            goto L_0x00fa
        L_0x00f8:
            r2 = 0
            goto L_0x00fb
        L_0x00fa:
            r2 = 1
        L_0x00fb:
            boolean r19 = r12.isEnabled()     // Catch:{ all -> 0x0390 }
            if (r19 == 0) goto L_0x0103
            if (r2 == 0) goto L_0x0106
        L_0x0103:
            r14.clearCache()     // Catch:{ all -> 0x0390 }
        L_0x0106:
            boolean r2 = r12.isEnabled()     // Catch:{ all -> 0x0390 }
            r14.setCacheEnabled(r2)     // Catch:{ all -> 0x0390 }
            android.content.SharedPreferences$Editor r2 = r10.edit()     // Catch:{ all -> 0x0390 }
            java.lang.String r10 = r12.serializeToString()     // Catch:{ all -> 0x0390 }
            android.content.SharedPreferences$Editor r0 = r2.putString(r0, r10)     // Catch:{ all -> 0x0390 }
            r0.apply()     // Catch:{ all -> 0x0390 }
            goto L_0x0121
        L_0x011d:
            r0 = 1
            r14.setCacheEnabled(r0)     // Catch:{ all -> 0x0390 }
        L_0x0121:
            java.lang.Class<com.vungle.warren.AdLoader> r0 = com.vungle.warren.AdLoader.class
            java.lang.Object r0 = r13.getService(r0)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.AdLoader r0 = (com.vungle.warren.AdLoader) r0     // Catch:{ all -> 0x0390 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0390 }
            r2.<init>()     // Catch:{ all -> 0x0390 }
            java.util.Iterator r10 = r11.iterator()     // Catch:{ all -> 0x0390 }
        L_0x0132:
            boolean r11 = r10.hasNext()     // Catch:{ all -> 0x0390 }
            if (r11 == 0) goto L_0x014b
            java.lang.Object r11 = r10.next()     // Catch:{ all -> 0x0390 }
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11     // Catch:{ all -> 0x0390 }
            com.vungle.warren.model.Placement r12 = new com.vungle.warren.model.Placement     // Catch:{ all -> 0x0390 }
            com.google.gson.JsonObject r11 = r11.f()     // Catch:{ all -> 0x0390 }
            r12.<init>((com.google.gson.JsonObject) r11)     // Catch:{ all -> 0x0390 }
            r2.add(r12)     // Catch:{ all -> 0x0390 }
            goto L_0x0132
        L_0x014b:
            r15.setValidPlacements(r2)     // Catch:{ all -> 0x0390 }
            java.lang.String r2 = "gdpr"
            boolean r2 = r9.d(r2)     // Catch:{ all -> 0x0390 }
            if (r2 == 0) goto L_0x0248
            java.lang.String r2 = "consentIsImportantToVungle"
            java.lang.Class<com.vungle.warren.model.Cookie> r10 = com.vungle.warren.model.Cookie.class
            com.vungle.warren.persistence.Repository$FutureResult r2 = r15.load(r2, r10)     // Catch:{ all -> 0x0390 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0390 }
            com.vungle.warren.model.Cookie r2 = (com.vungle.warren.model.Cookie) r2     // Catch:{ all -> 0x0390 }
            if (r2 != 0) goto L_0x0186
            com.vungle.warren.model.Cookie r2 = new com.vungle.warren.model.Cookie     // Catch:{ all -> 0x0390 }
            java.lang.String r10 = "consentIsImportantToVungle"
            r2.<init>(r10)     // Catch:{ all -> 0x0390 }
            java.lang.String r10 = "consent_status"
            java.lang.String r11 = "unknown"
            r2.putValue(r10, r11)     // Catch:{ all -> 0x0390 }
            java.lang.String r10 = "consent_source"
            java.lang.String r11 = "no_interaction"
            r2.putValue(r10, r11)     // Catch:{ all -> 0x0390 }
            java.lang.String r10 = "timestamp"
            r11 = 0
            java.lang.Long r14 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0390 }
            r2.putValue(r10, r14)     // Catch:{ all -> 0x0390 }
        L_0x0186:
            java.lang.String r10 = "gdpr"
            com.google.gson.JsonObject r10 = r9.c(r10)     // Catch:{ all -> 0x0390 }
            boolean r11 = com.vungle.warren.model.JsonUtil.hasNonNull(r10, r8)     // Catch:{ all -> 0x0390 }
            if (r11 == 0) goto L_0x019e
            com.google.gson.JsonElement r11 = r10.a((java.lang.String) r8)     // Catch:{ all -> 0x0390 }
            boolean r11 = r11.a()     // Catch:{ all -> 0x0390 }
            if (r11 == 0) goto L_0x019e
            r11 = 1
            goto L_0x019f
        L_0x019e:
            r11 = 0
        L_0x019f:
            boolean r12 = com.vungle.warren.model.JsonUtil.hasNonNull(r10, r7)     // Catch:{ all -> 0x0390 }
            java.lang.String r14 = ""
            if (r12 == 0) goto L_0x01b0
            com.google.gson.JsonElement r12 = r10.a((java.lang.String) r7)     // Catch:{ all -> 0x0390 }
            java.lang.String r12 = r12.i()     // Catch:{ all -> 0x0390 }
            goto L_0x01b1
        L_0x01b0:
            r12 = r14
        L_0x01b1:
            boolean r19 = com.vungle.warren.model.JsonUtil.hasNonNull(r10, r6)     // Catch:{ all -> 0x0390 }
            if (r19 == 0) goto L_0x01c0
            com.google.gson.JsonElement r19 = r10.a((java.lang.String) r6)     // Catch:{ all -> 0x0390 }
            java.lang.String r19 = r19.i()     // Catch:{ all -> 0x0390 }
            goto L_0x01c2
        L_0x01c0:
            r19 = r14
        L_0x01c2:
            boolean r20 = com.vungle.warren.model.JsonUtil.hasNonNull(r10, r5)     // Catch:{ all -> 0x0390 }
            if (r20 == 0) goto L_0x01d1
            com.google.gson.JsonElement r20 = r10.a((java.lang.String) r5)     // Catch:{ all -> 0x0390 }
            java.lang.String r20 = r20.i()     // Catch:{ all -> 0x0390 }
            goto L_0x01d3
        L_0x01d1:
            r20 = r14
        L_0x01d3:
            boolean r21 = com.vungle.warren.model.JsonUtil.hasNonNull(r10, r4)     // Catch:{ all -> 0x0390 }
            if (r21 == 0) goto L_0x01e2
            com.google.gson.JsonElement r21 = r10.a((java.lang.String) r4)     // Catch:{ all -> 0x0390 }
            java.lang.String r21 = r21.i()     // Catch:{ all -> 0x0390 }
            goto L_0x01e4
        L_0x01e2:
            r21 = r14
        L_0x01e4:
            boolean r22 = com.vungle.warren.model.JsonUtil.hasNonNull(r10, r3)     // Catch:{ all -> 0x0390 }
            if (r22 == 0) goto L_0x01f3
            com.google.gson.JsonElement r10 = r10.a((java.lang.String) r3)     // Catch:{ all -> 0x0390 }
            java.lang.String r10 = r10.i()     // Catch:{ all -> 0x0390 }
            goto L_0x01f4
        L_0x01f3:
            r10 = r14
        L_0x01f4:
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ all -> 0x0390 }
            r2.putValue(r8, r11)     // Catch:{ all -> 0x0390 }
            boolean r8 = android.text.TextUtils.isEmpty(r12)     // Catch:{ all -> 0x0390 }
            if (r8 == 0) goto L_0x0203
            java.lang.String r12 = "Targeted Ads"
        L_0x0203:
            r2.putValue(r7, r12)     // Catch:{ all -> 0x0390 }
            boolean r7 = android.text.TextUtils.isEmpty(r19)     // Catch:{ all -> 0x0390 }
            if (r7 == 0) goto L_0x020e
            java.lang.String r19 = "To receive more relevant ad content based on your interactions with our ads, click \"I Consent\" below. Either way, you will see the same amount of ads."
        L_0x020e:
            r7 = r19
            r2.putValue(r6, r7)     // Catch:{ all -> 0x0390 }
            java.lang.String r6 = "consent_source"
            java.lang.String r6 = r2.getString(r6)     // Catch:{ all -> 0x0390 }
            java.lang.String r7 = "publisher"
            boolean r6 = r7.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0390 }
            if (r6 != 0) goto L_0x022d
            boolean r6 = android.text.TextUtils.isEmpty(r20)     // Catch:{ all -> 0x0390 }
            if (r6 == 0) goto L_0x0228
            goto L_0x022a
        L_0x0228:
            r14 = r20
        L_0x022a:
            r2.putValue(r5, r14)     // Catch:{ all -> 0x0390 }
        L_0x022d:
            boolean r5 = android.text.TextUtils.isEmpty(r21)     // Catch:{ all -> 0x0390 }
            if (r5 == 0) goto L_0x0235
            java.lang.String r21 = "I Consent"
        L_0x0235:
            r5 = r21
            r2.putValue(r4, r5)     // Catch:{ all -> 0x0390 }
            boolean r4 = android.text.TextUtils.isEmpty(r10)     // Catch:{ all -> 0x0390 }
            if (r4 == 0) goto L_0x0242
            java.lang.String r10 = "I Do Not Consent"
        L_0x0242:
            r2.putValue(r3, r10)     // Catch:{ all -> 0x0390 }
            r15.save(r2)     // Catch:{ all -> 0x0390 }
        L_0x0248:
            r2 = -1
            r3 = r16
            boolean r4 = r9.d(r3)     // Catch:{ all -> 0x0390 }
            java.lang.String r5 = "enabled"
            if (r4 == 0) goto L_0x0269
            com.google.gson.JsonObject r4 = r9.c(r3)     // Catch:{ all -> 0x0390 }
            boolean r4 = r4.d(r5)     // Catch:{ all -> 0x0390 }
            if (r4 == 0) goto L_0x0269
            com.google.gson.JsonObject r2 = r9.c(r3)     // Catch:{ all -> 0x0390 }
            com.google.gson.JsonElement r2 = r2.a((java.lang.String) r5)     // Catch:{ all -> 0x0390 }
            boolean r2 = r2.a()     // Catch:{ all -> 0x0390 }
        L_0x0269:
            com.vungle.warren.download.ApkDownloadManager r3 = com.vungle.warren.download.ApkDownloadManager.getInstance()     // Catch:{ all -> 0x0390 }
            android.content.Context r4 = r1.context     // Catch:{ all -> 0x0390 }
            r3.init(r4, r2)     // Catch:{ all -> 0x0390 }
            java.lang.String r2 = "ri"
            boolean r2 = r9.d(r2)     // Catch:{ all -> 0x0390 }
            if (r2 == 0) goto L_0x02ab
            java.lang.String r2 = "configSettings"
            java.lang.Class<com.vungle.warren.model.Cookie> r3 = com.vungle.warren.model.Cookie.class
            com.vungle.warren.persistence.Repository$FutureResult r2 = r15.load(r2, r3)     // Catch:{ all -> 0x0390 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0390 }
            com.vungle.warren.model.Cookie r2 = (com.vungle.warren.model.Cookie) r2     // Catch:{ all -> 0x0390 }
            if (r2 != 0) goto L_0x0291
            com.vungle.warren.model.Cookie r2 = new com.vungle.warren.model.Cookie     // Catch:{ all -> 0x0390 }
            java.lang.String r3 = "configSettings"
            r2.<init>(r3)     // Catch:{ all -> 0x0390 }
        L_0x0291:
            java.lang.String r3 = "ri"
            com.google.gson.JsonObject r3 = r9.c(r3)     // Catch:{ all -> 0x0390 }
            com.google.gson.JsonElement r3 = r3.a((java.lang.String) r5)     // Catch:{ all -> 0x0390 }
            boolean r3 = r3.a()     // Catch:{ all -> 0x0390 }
            java.lang.String r4 = "isReportIncentivizedEnabled"
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0390 }
            r2.putValue(r4, r3)     // Catch:{ all -> 0x0390 }
            r15.save(r2)     // Catch:{ all -> 0x0390 }
        L_0x02ab:
            java.lang.String r2 = "attribution_reporting"
            boolean r2 = r9.d(r2)     // Catch:{ all -> 0x0390 }
            if (r2 == 0) goto L_0x02d2
            java.lang.String r2 = "attribution_reporting"
            com.google.gson.JsonObject r2 = r9.c(r2)     // Catch:{ all -> 0x0390 }
            java.lang.String r3 = "should_transmit_imei"
            boolean r3 = r2.d(r3)     // Catch:{ all -> 0x0390 }
            if (r3 == 0) goto L_0x02ce
            java.lang.String r3 = "should_transmit_imei"
            com.google.gson.JsonElement r2 = r2.a((java.lang.String) r3)     // Catch:{ all -> 0x0390 }
            boolean r2 = r2.a()     // Catch:{ all -> 0x0390 }
            r1.shouldTransmitIMEI = r2     // Catch:{ all -> 0x0390 }
            goto L_0x02d5
        L_0x02ce:
            r2 = 0
            r1.shouldTransmitIMEI = r2     // Catch:{ all -> 0x0386 }
            goto L_0x02d5
        L_0x02d2:
            r2 = 0
            r1.shouldTransmitIMEI = r2     // Catch:{ all -> 0x0386 }
        L_0x02d5:
            java.lang.String r2 = "config"
            boolean r2 = r9.d(r2)     // Catch:{ all -> 0x0390 }
            if (r2 == 0) goto L_0x02fd
            java.lang.String r2 = "config"
            com.google.gson.JsonObject r2 = r9.c(r2)     // Catch:{ all -> 0x0390 }
            java.lang.String r3 = "refresh_time"
            com.google.gson.JsonElement r2 = r2.a((java.lang.String) r3)     // Catch:{ all -> 0x0390 }
            long r2 = r2.h()     // Catch:{ all -> 0x0390 }
            java.lang.String r4 = r1.appID     // Catch:{ all -> 0x0390 }
            com.vungle.warren.tasks.JobInfo r4 = com.vungle.warren.tasks.ReconfigJob.makeJobInfo(r4)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.tasks.JobInfo r2 = r4.setDelay(r2)     // Catch:{ all -> 0x0390 }
            r11 = r18
            r11.execute(r2)     // Catch:{ all -> 0x0390 }
            goto L_0x02ff
        L_0x02fd:
            r11 = r18
        L_0x02ff:
            java.lang.Class<com.vungle.warren.VisionController> r2 = com.vungle.warren.VisionController.class
            java.lang.Object r2 = r13.getService(r2)     // Catch:{ DBException -> 0x0329 }
            com.vungle.warren.VisionController r2 = (com.vungle.warren.VisionController) r2     // Catch:{ DBException -> 0x0329 }
            java.lang.String r3 = "vision"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r9, r3)     // Catch:{ DBException -> 0x0329 }
            if (r3 == 0) goto L_0x0320
            com.google.gson.Gson r3 = r1.gson     // Catch:{ DBException -> 0x0329 }
            java.lang.String r4 = "vision"
            com.google.gson.JsonObject r4 = r9.c(r4)     // Catch:{ DBException -> 0x0329 }
            java.lang.Class<com.vungle.warren.vision.VisionConfig> r5 = com.vungle.warren.vision.VisionConfig.class
            java.lang.Object r3 = r3.a((com.google.gson.JsonElement) r4, r5)     // Catch:{ DBException -> 0x0329 }
            com.vungle.warren.vision.VisionConfig r3 = (com.vungle.warren.vision.VisionConfig) r3     // Catch:{ DBException -> 0x0329 }
            goto L_0x0325
        L_0x0320:
            com.vungle.warren.vision.VisionConfig r3 = new com.vungle.warren.vision.VisionConfig     // Catch:{ DBException -> 0x0329 }
            r3.<init>()     // Catch:{ DBException -> 0x0329 }
        L_0x0325:
            r2.setConfig(r3)     // Catch:{ DBException -> 0x0329 }
            goto L_0x0330
        L_0x0329:
            java.lang.String r2 = TAG     // Catch:{ all -> 0x0390 }
            java.lang.String r3 = "not able to apply vision data config"
            android.util.Log.e(r2, r3)     // Catch:{ all -> 0x0390 }
        L_0x0330:
            r2 = 1
            isInitialized = r2     // Catch:{ all -> 0x0390 }
            r24.onSuccess()     // Catch:{ all -> 0x0390 }
            java.util.concurrent.atomic.AtomicBoolean r2 = isInitializing     // Catch:{ all -> 0x0390 }
            r3 = 0
            r2.set(r3)     // Catch:{ all -> 0x0390 }
            com.vungle.warren.persistence.Repository$FutureResult r2 = r15.loadValidPlacements()     // Catch:{ all -> 0x0390 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0390 }
            java.util.Collection r2 = (java.util.Collection) r2     // Catch:{ all -> 0x0390 }
            com.vungle.warren.tasks.JobInfo r3 = com.vungle.warren.tasks.CleanupJob.makeJobInfo()     // Catch:{ all -> 0x0390 }
            r11.execute(r3)     // Catch:{ all -> 0x0390 }
            if (r2 == 0) goto L_0x037d
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0390 }
        L_0x0353:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x0390 }
            if (r3 == 0) goto L_0x037d
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x0390 }
            com.vungle.warren.model.Placement r3 = (com.vungle.warren.model.Placement) r3     // Catch:{ all -> 0x0390 }
            boolean r4 = r3.isAutoCached()     // Catch:{ all -> 0x0390 }
            if (r4 == 0) goto L_0x037a
            java.lang.String r4 = TAG     // Catch:{ all -> 0x0390 }
            java.lang.String r5 = "starting jobs for autocached advs"
            android.util.Log.d(r4, r5)     // Catch:{ all -> 0x0390 }
            java.lang.String r4 = r3.getId()     // Catch:{ all -> 0x0390 }
            com.vungle.warren.AdConfig$AdSize r3 = r3.getAdSize()     // Catch:{ all -> 0x0390 }
            r5 = 0
            r0.loadEndless(r4, r3, r5)     // Catch:{ all -> 0x0390 }
            goto L_0x0353
        L_0x037a:
            r5 = 0
            goto L_0x0353
        L_0x037d:
            r0 = 1
            com.vungle.warren.tasks.JobInfo r0 = com.vungle.warren.tasks.SendReportsJob.makeJobInfo(r0)     // Catch:{ all -> 0x0390 }
            r11.execute(r0)     // Catch:{ all -> 0x0390 }
            goto L_0x03cc
        L_0x0386:
            r0 = move-exception
            goto L_0x0392
        L_0x0388:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0390 }
            java.lang.String r2 = "Context is null"
            r0.<init>(r2)     // Catch:{ all -> 0x0390 }
            throw r0     // Catch:{ all -> 0x0390 }
        L_0x0390:
            r0 = move-exception
            r2 = 0
        L_0x0392:
            isInitialized = r2
            java.util.concurrent.atomic.AtomicBoolean r3 = isInitializing
            r3.set(r2)
            java.lang.String r2 = TAG
            java.lang.String r3 = android.util.Log.getStackTraceString(r0)
            android.util.Log.e(r2, r3)
            boolean r2 = r0 instanceof retrofit2.HttpException
            if (r2 == 0) goto L_0x03b2
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r2 = 3
            r0.<init>(r2)
            r2 = r24
            r2.onError(r0)
            goto L_0x03cc
        L_0x03b2:
            r2 = r24
            boolean r0 = r0 instanceof com.vungle.warren.persistence.DatabaseHelper.DBException
            if (r0 == 0) goto L_0x03c3
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r3 = 26
            r0.<init>(r3)
            r2.onError(r0)
            goto L_0x03cc
        L_0x03c3:
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r3 = 2
            r0.<init>(r3)
            r2.onError(r0)
        L_0x03cc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.Vungle.configure(com.vungle.warren.InitCallback):void");
    }

    protected static void deInit() {
        Context context2 = _instance.context;
        if (context2 != null) {
            ServiceLocator instance = ServiceLocator.getInstance(context2);
            ((CacheManager) instance.getService(CacheManager.class)).removeListener(cacheListener);
            ((Downloader) instance.getService(Downloader.class)).cancelAll();
            ((AdLoader) instance.getService(AdLoader.class)).clear();
            _instance.playOperations.clear();
        }
        ServiceLocator.deInit();
        isInitialized = false;
        isDepInit.set(false);
        isInitializing.set(false);
    }

    /* access modifiers changed from: private */
    public static void dropDownloaderCache(String str) {
        Context context2 = _instance.context;
        if (context2 != null) {
            ((AdLoader) ServiceLocator.getInstance(context2).getService(AdLoader.class)).dropCache(str);
        }
    }

    static Context getAppContext() {
        return _instance.context;
    }

    /* access modifiers changed from: private */
    public static Consent getConsent(Cookie cookie) {
        if (cookie == null) {
            return null;
        }
        return "opted_in".equals(cookie.getString("consent_status")) ? Consent.OPTED_IN : Consent.OPTED_OUT;
    }

    public static String getConsentMessageVersion() {
        if (isInitialized()) {
            return _instance.consentVersion;
        }
        Log.e(TAG, "Vungle is not initialized, please wait initialize or wait until Vungle is intialized to get Consent Message Version");
        return null;
    }

    public static Consent getConsentStatus() {
        if (isInitialized()) {
            return _instance.consent;
        }
        Log.e(TAG, "Vungle is not initialized, consent is null");
        return null;
    }

    public static VungleNativeAd getNativeAd(String str, AdConfig adConfig, PlayAdCallback playAdCallback) {
        if (adConfig == null) {
            adConfig = new AdConfig();
        }
        if (AdConfig.AdSize.isDefaultAdSize(adConfig.getAdSize())) {
            return getNativeAdInternal(str, adConfig, playAdCallback);
        }
        if (playAdCallback == null) {
            return null;
        }
        Log.e(TAG, "Please use Banners.getBanner(... ) to retrieve Banner Ad");
        playAdCallback.onError(str, new VungleException(29));
        return null;
    }

    static VungleNativeView getNativeAdInternal(String str, AdConfig adConfig, PlayAdCallback playAdCallback) {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized, returned VungleNativeAd = null");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(9));
            }
            return null;
        }
        ServiceLocator instance = ServiceLocator.getInstance(_instance.context);
        final Repository repository = (Repository) instance.getService(Repository.class);
        final AdLoader adLoader = (AdLoader) instance.getService(AdLoader.class);
        final JobRunner jobRunner = (JobRunner) instance.getService(JobRunner.class);
        final VisionController visionController = (VisionController) instance.getService(VisionController.class);
        Placement placement = repository.load(str, Placement.class).get();
        if (placement == null) {
            Log.e(TAG, "No Placement for ID");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(13));
            }
            return null;
        }
        final Advertisement advertisement = repository.findValidAdvertisementForPlacement(str).get();
        if (advertisement == null) {
            Log.e(TAG, "No Advertisement for ID");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(10));
            }
            return null;
        } else if (!canPlayAd(advertisement)) {
            if (advertisement.getState() == 1) {
                try {
                    repository.saveAndApplyState(advertisement, str, 4);
                } catch (DatabaseHelper.DBException unused) {
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, new VungleException(26));
                    }
                }
                if (placement.isAutoCached()) {
                    adLoader.loadEndless(placement.getId(), placement.getAdSize(), 0);
                }
            }
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(10));
            }
            return null;
        } else if (Boolean.TRUE.equals(_instance.playOperations.get(str)) || adLoader.isLoading(str)) {
            String str2 = TAG;
            Log.e(str2, "Playing or Loading operation ongoing. Playing " + _instance.playOperations.get(str) + " Loading: " + adLoader.isLoading(str));
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(8));
            }
            return null;
        } else if (advertisement.getAdType() != 1) {
            Log.e(TAG, "Invalid Ad Type for Native Ad.");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(10));
            }
            return null;
        } else if ((!"mrec".equals(advertisement.getTemplateType()) || adConfig.getAdSize() == AdConfig.AdSize.VUNGLE_MREC) && (!"flexfeed".equals(advertisement.getTemplateType()) || adConfig.getAdSize() == AdConfig.AdSize.VUNGLE_DEFAULT)) {
            advertisement.configure(adConfig);
            try {
                repository.save(advertisement);
                _instance.playOperations.put(str, true);
                try {
                    final PlayAdCallback playAdCallback2 = playAdCallback;
                    return new VungleNativeView(_instance.context.getApplicationContext(), str, (PresentationFactory) instance.getService(PresentationFactory.class), new AdContract.AdvertisementPresenter.EventListener() {
                        int percentViewed = -1;
                        boolean successfulView;

                        public void onError(VungleException vungleException, String str) {
                            if (vungleException.getExceptionCode() == 27) {
                                Vungle.dropDownloaderCache(advertisement.getId());
                                return;
                            }
                            Vungle._instance.playOperations.put(str, false);
                            if (vungleException.getExceptionCode() != 25) {
                                try {
                                    repository.saveAndApplyState(advertisement, str, 4);
                                } catch (DatabaseHelper.DBException unused) {
                                    vungleException = new VungleException(26);
                                }
                            }
                            PlayAdCallback playAdCallback = playAdCallback2;
                            if (playAdCallback != null) {
                                playAdCallback.onError(str, vungleException);
                            }
                        }

                        public void onNext(String str, String str2, String str3) {
                            boolean z;
                            try {
                                boolean z2 = false;
                                if (str.equals(ViewProps.START)) {
                                    repository.saveAndApplyState(advertisement, str3, 2);
                                    if (playAdCallback2 != null) {
                                        playAdCallback2.onAdStart(str3);
                                    }
                                    this.percentViewed = 0;
                                    Placement placement = repository.load(str3, Placement.class).get();
                                    if (placement != null && placement.isAutoCached()) {
                                        adLoader.loadEndless(str3, placement.getAdSize(), 0);
                                    }
                                } else if (str.equals(ViewProps.END)) {
                                    Log.d("Vungle", "Cleaning up metadata and assets for placement " + str3 + " and advertisement " + advertisement.getId());
                                    repository.saveAndApplyState(advertisement, str3, 3);
                                    repository.updateAndSaveReportState(str3, advertisement.getAppID(), 0, 1);
                                    jobRunner.execute(SendReportsJob.makeJobInfo(false));
                                    Vungle._instance.playOperations.put(str3, false);
                                    if (playAdCallback2 != null) {
                                        PlayAdCallback playAdCallback = playAdCallback2;
                                        if (!this.successfulView) {
                                            if (this.percentViewed < 80) {
                                                z = false;
                                                if (str2 != null && str2.equals("isCTAClicked")) {
                                                    z2 = true;
                                                }
                                                playAdCallback.onAdEnd(str3, z, z2);
                                            }
                                        }
                                        z = true;
                                        z2 = true;
                                        playAdCallback.onAdEnd(str3, z, z2);
                                    }
                                    if (visionController.isEnabled()) {
                                        visionController.reportData(advertisement.getCreativeId(), advertisement.getCampaignId(), advertisement.getAdvertiserAppId());
                                    }
                                } else if (str.equals("successfulView")) {
                                    this.successfulView = true;
                                } else if (str.startsWith("percentViewed")) {
                                    String[] split = str.split(":");
                                    if (split.length == 2) {
                                        this.percentViewed = Integer.parseInt(split[1]);
                                    }
                                }
                            } catch (DatabaseHelper.DBException unused) {
                                onError(new VungleException(26), str3);
                            }
                        }
                    });
                } catch (Exception unused2) {
                    _instance.playOperations.put(str, false);
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, new VungleException(10));
                    }
                    return null;
                }
            } catch (DatabaseHelper.DBException unused3) {
                if (playAdCallback != null) {
                    playAdCallback.onError(str, new VungleException(26));
                }
                return null;
            }
        } else {
            Log.e(TAG, "Corresponding AdConfig#setAdSize must be passed for the type/size of native ad");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(28));
            }
            return null;
        }
    }

    public static Collection<String> getValidPlacements() {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized return empty placements list");
            return Collections.emptyList();
        }
        Collection<String> collection = ((Repository) ServiceLocator.getInstance(_instance.context).getService(Repository.class)).getValidPlacements().get();
        return collection == null ? Collections.emptyList() : collection;
    }

    @Deprecated
    public static void init(Collection<String> collection, String str, Context context2, InitCallback initCallback) throws IllegalArgumentException {
        init(str, context2, initCallback, new VungleSettings.Builder().build());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r0 = ((com.vungle.warren.persistence.Repository) com.vungle.warren.ServiceLocator.getInstance((r0 = _instance.context)).getService(com.vungle.warren.persistence.Repository.class)).getValidPlacements().get();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isInitialized() {
        /*
            boolean r0 = isInitialized
            if (r0 == 0) goto L_0x002a
            com.vungle.warren.Vungle r0 = _instance
            android.content.Context r0 = r0.context
            if (r0 == 0) goto L_0x002a
            com.vungle.warren.ServiceLocator r0 = com.vungle.warren.ServiceLocator.getInstance(r0)
            java.lang.Class<com.vungle.warren.persistence.Repository> r1 = com.vungle.warren.persistence.Repository.class
            java.lang.Object r0 = r0.getService(r1)
            com.vungle.warren.persistence.Repository r0 = (com.vungle.warren.persistence.Repository) r0
            com.vungle.warren.persistence.Repository$FutureResult r0 = r0.getValidPlacements()
            java.lang.Object r0 = r0.get()
            java.util.Collection r0 = (java.util.Collection) r0
            if (r0 == 0) goto L_0x002a
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x002a
            r0 = 1
            return r0
        L_0x002a:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.Vungle.isInitialized():boolean");
    }

    public static void loadAd(String str, LoadAdCallback loadAdCallback) {
        loadAd(str, new AdConfig(), loadAdCallback);
    }

    static void loadAdInternal(String str, AdConfig adConfig, LoadAdCallback loadAdCallback) {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized");
            if (loadAdCallback != null) {
                loadAdCallback.onError(str, new VungleException(9));
                return;
            }
            return;
        }
        ServiceLocator instance = ServiceLocator.getInstance(_instance.context);
        LoadAdCallbackWrapper loadAdCallbackWrapper = new LoadAdCallbackWrapper(((SDKExecutors) instance.getService(SDKExecutors.class)).getUIExecutor(), loadAdCallback);
        AdLoader adLoader = (AdLoader) instance.getService(AdLoader.class);
        if (adConfig == null) {
            adConfig = new AdConfig();
        }
        adLoader.load(str, adConfig, loadAdCallbackWrapper);
    }

    /* access modifiers changed from: private */
    public static void onError(InitCallback initCallback, VungleException vungleException) {
        if (initCallback != null) {
            initCallback.onError(vungleException);
        }
    }

    public static void playAd(String str, AdConfig adConfig, PlayAdCallback playAdCallback) {
        if (!isInitialized()) {
            Log.e(TAG, "Locator is not initialized");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(9));
                return;
            }
            return;
        }
        ServiceLocator instance = ServiceLocator.getInstance(_instance.context);
        final Executors executors = (Executors) instance.getService(Executors.class);
        final Repository repository = (Repository) instance.getService(Repository.class);
        final AdLoader adLoader = (AdLoader) instance.getService(AdLoader.class);
        final VungleApiClient vungleApiClient = (VungleApiClient) instance.getService(VungleApiClient.class);
        final PlayAdCallbackWrapper playAdCallbackWrapper = new PlayAdCallbackWrapper(executors.getUIExecutor(), playAdCallback);
        final String str2 = str;
        final AdConfig adConfig2 = adConfig;
        executors.getVungleExecutor().execute(new Runnable() {
            public void run() {
                String str;
                if (Boolean.TRUE.equals(Vungle._instance.playOperations.get(str2)) || adLoader.isLoading(str2)) {
                    playAdCallbackWrapper.onError(str2, new VungleException(8));
                    return;
                }
                Placement placement = repository.load(str2, Placement.class).get();
                if (placement == null) {
                    playAdCallbackWrapper.onError(str2, new VungleException(13));
                } else if (AdConfig.AdSize.isBannerAdSize(placement.getAdSize())) {
                    playAdCallbackWrapper.onError(str2, new VungleException(28));
                } else {
                    final boolean z = false;
                    final Advertisement advertisement = repository.findValidAdvertisementForPlacement(str2).get();
                    try {
                        if (!Vungle.canPlayAd(advertisement)) {
                            if (advertisement != null && advertisement.getState() == 1) {
                                repository.saveAndApplyState(advertisement, str2, 4);
                                if (placement.isAutoCached()) {
                                    adLoader.loadEndless(placement.getId(), placement.getAdSize(), 0);
                                }
                            }
                            z = true;
                        } else {
                            advertisement.configure(adConfig2);
                            repository.save(advertisement);
                        }
                        if (Vungle._instance.context == null) {
                            return;
                        }
                        if (vungleApiClient.canCallWillPlayAd()) {
                            VungleApiClient vungleApiClient = vungleApiClient;
                            String id = placement.getId();
                            boolean isAutoCached = placement.isAutoCached();
                            if (z) {
                                str = "";
                            } else {
                                str = advertisement.getAdToken();
                            }
                            vungleApiClient.willPlayAd(id, isAutoCached, str).enqueue(new Callback<JsonObject>() {
                                public void onFailure(Call<JsonObject> call, Throwable th) {
                                    executors.getVungleExecutor().execute(new Runnable() {
                                        public void run() {
                                            AnonymousClass1 r0 = AnonymousClass1.this;
                                            if (z) {
                                                AnonymousClass5 r02 = AnonymousClass5.this;
                                                playAdCallbackWrapper.onError(str2, new VungleException(1));
                                                return;
                                            }
                                            AnonymousClass5 r1 = AnonymousClass5.this;
                                            String str = str2;
                                            Vungle.renderAd(str, playAdCallbackWrapper, str, advertisement);
                                        }
                                    });
                                }

                                public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {
                                    executors.getVungleExecutor().execute(new Runnable() {
                                        /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
                                        /* JADX WARNING: Removed duplicated region for block: B:24:0x007a  */
                                        /* Code decompiled incorrectly, please refer to instructions dump. */
                                        public void run() {
                                            /*
                                                r4 = this;
                                                retrofit2.Response r0 = r3
                                                boolean r0 = r0.isSuccessful()
                                                r1 = 0
                                                if (r0 == 0) goto L_0x0058
                                                retrofit2.Response r0 = r3
                                                java.lang.Object r0 = r0.body()
                                                com.google.gson.JsonObject r0 = (com.google.gson.JsonObject) r0
                                                if (r0 == 0) goto L_0x0058
                                                java.lang.String r2 = "ad"
                                                boolean r3 = r0.d(r2)
                                                if (r3 == 0) goto L_0x0058
                                                com.google.gson.JsonObject r0 = r0.c(r2)     // Catch:{ IllegalArgumentException -> 0x004f, Exception -> 0x0044 }
                                                com.vungle.warren.model.Advertisement r2 = new com.vungle.warren.model.Advertisement     // Catch:{ IllegalArgumentException -> 0x004f, Exception -> 0x0044 }
                                                r2.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x004f, Exception -> 0x0044 }
                                                com.vungle.warren.Vungle$5$1 r0 = com.vungle.warren.Vungle.AnonymousClass5.AnonymousClass1.this     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                com.vungle.warren.Vungle$5 r0 = com.vungle.warren.Vungle.AnonymousClass5.this     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                com.vungle.warren.AdConfig r0 = r7     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                r2.configure(r0)     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                com.vungle.warren.Vungle$5$1 r0 = com.vungle.warren.Vungle.AnonymousClass5.AnonymousClass1.this     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                com.vungle.warren.Vungle$5 r0 = com.vungle.warren.Vungle.AnonymousClass5.this     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                com.vungle.warren.persistence.Repository r0 = r6     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                com.vungle.warren.Vungle$5$1 r1 = com.vungle.warren.Vungle.AnonymousClass5.AnonymousClass1.this     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                com.vungle.warren.Vungle$5 r1 = com.vungle.warren.Vungle.AnonymousClass5.this     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                java.lang.String r1 = r3     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                r3 = 0
                                                r0.saveAndApplyState(r2, r1, r3)     // Catch:{ IllegalArgumentException -> 0x0042, Exception -> 0x003f }
                                                r1 = r2
                                                goto L_0x0058
                                            L_0x003f:
                                                r0 = move-exception
                                                r1 = r2
                                                goto L_0x0045
                                            L_0x0042:
                                                r1 = r2
                                                goto L_0x004f
                                            L_0x0044:
                                                r0 = move-exception
                                            L_0x0045:
                                                java.lang.String r2 = com.vungle.warren.Vungle.TAG
                                                java.lang.String r3 = "Error using will_play_ad!"
                                                android.util.Log.e(r2, r3, r0)
                                                goto L_0x0058
                                            L_0x004f:
                                                java.lang.String r0 = com.vungle.warren.Vungle.TAG
                                                java.lang.String r2 = "Will Play Ad did not respond with a replacement. Move on."
                                                android.util.Log.v(r0, r2)
                                            L_0x0058:
                                                com.vungle.warren.Vungle$5$1 r0 = com.vungle.warren.Vungle.AnonymousClass5.AnonymousClass1.this
                                                boolean r2 = r1
                                                if (r2 == 0) goto L_0x007a
                                                if (r1 != 0) goto L_0x0070
                                                com.vungle.warren.Vungle$5 r0 = com.vungle.warren.Vungle.AnonymousClass5.this
                                                com.vungle.warren.PlayAdCallback r1 = r5
                                                java.lang.String r0 = r3
                                                com.vungle.warren.error.VungleException r2 = new com.vungle.warren.error.VungleException
                                                r3 = 1
                                                r2.<init>(r3)
                                                r1.onError(r0, r2)
                                                goto L_0x0085
                                            L_0x0070:
                                                com.vungle.warren.Vungle$5 r0 = com.vungle.warren.Vungle.AnonymousClass5.this
                                                java.lang.String r2 = r3
                                                com.vungle.warren.PlayAdCallback r0 = r5
                                                com.vungle.warren.Vungle.renderAd(r2, r0, r2, r1)
                                                goto L_0x0085
                                            L_0x007a:
                                                com.vungle.warren.Vungle$5 r1 = com.vungle.warren.Vungle.AnonymousClass5.this
                                                java.lang.String r2 = r3
                                                com.vungle.warren.PlayAdCallback r1 = r5
                                                com.vungle.warren.model.Advertisement r0 = r2
                                                com.vungle.warren.Vungle.renderAd(r2, r1, r2, r0)
                                            L_0x0085:
                                                return
                                            */
                                            throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.Vungle.AnonymousClass5.AnonymousClass1.AnonymousClass1.run():void");
                                        }
                                    });
                                }
                            });
                        } else if (z) {
                            playAdCallbackWrapper.onError(str2, new VungleException(1));
                        } else {
                            String str2 = str2;
                            Vungle.renderAd(str2, playAdCallbackWrapper, str2, advertisement);
                        }
                    } catch (DatabaseHelper.DBException unused) {
                        playAdCallbackWrapper.onError(str2, new VungleException(26));
                    }
                }
            }
        });
    }

    static void reConfigure() {
        Context context2 = _instance.context;
        if (context2 != null) {
            ServiceLocator instance = ServiceLocator.getInstance(context2);
            Executors executors = (Executors) instance.getService(Executors.class);
            final RuntimeValues runtimeValues = (RuntimeValues) instance.getService(RuntimeValues.class);
            if (isInitialized()) {
                executors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        Vungle._instance.configure(runtimeValues.initCallback);
                    }
                });
            } else {
                init(_instance.appID, _instance.context, runtimeValues.initCallback);
            }
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void renderAd(String str, PlayAdCallback playAdCallback, String str2, Advertisement advertisement) {
        synchronized (Vungle.class) {
            if (!isInitialized()) {
                Log.e(TAG, "Sdk is not initilized");
                return;
            }
            ServiceLocator instance = ServiceLocator.getInstance(_instance.context);
            final Repository repository = (Repository) instance.getService(Repository.class);
            final AdLoader adLoader = (AdLoader) instance.getService(AdLoader.class);
            final JobRunner jobRunner = (JobRunner) instance.getService(JobRunner.class);
            final VisionController visionController = (VisionController) instance.getService(VisionController.class);
            boolean z = true;
            _instance.playOperations.put(str, true);
            final Advertisement advertisement2 = advertisement;
            final PlayAdCallback playAdCallback2 = playAdCallback;
            final String str3 = str2;
            AdActivity.setEventListener(new AdContract.AdvertisementPresenter.EventListener() {
                int percentViewed = -1;
                boolean successfulView;

                public void onError(VungleException vungleException, String str) {
                    if (vungleException.getExceptionCode() == 27) {
                        Vungle.dropDownloaderCache(advertisement2.getId());
                        return;
                    }
                    if (!(vungleException.getExceptionCode() == 15 || vungleException.getExceptionCode() == 25)) {
                        try {
                            repository.saveAndApplyState(advertisement2, str, 4);
                        } catch (DatabaseHelper.DBException unused) {
                            vungleException = new VungleException(26);
                        }
                    }
                    AdActivity.setEventListener((AdContract.AdvertisementPresenter.EventListener) null);
                    Vungle._instance.playOperations.put(str, false);
                    PlayAdCallback playAdCallback = playAdCallback2;
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, vungleException);
                    }
                }

                public void onNext(String str, String str2, String str3) {
                    boolean z;
                    try {
                        boolean z2 = false;
                        if (str.equals(ViewProps.START)) {
                            repository.saveAndApplyState(advertisement2, str3, 2);
                            if (playAdCallback2 != null) {
                                playAdCallback2.onAdStart(str3);
                            }
                            this.percentViewed = 0;
                            Placement placement = repository.load(str3, Placement.class).get();
                            if (placement != null && placement.isAutoCached()) {
                                adLoader.loadEndless(str3, placement.getAdSize(), 0);
                            }
                        } else if (str.equals(ViewProps.END)) {
                            Log.d("Vungle", "Cleaning up metadata and assets for placement " + str3 + " and advertisement " + advertisement2.getId());
                            repository.saveAndApplyState(advertisement2, str3, 3);
                            repository.updateAndSaveReportState(str3, advertisement2.getAppID(), 0, 1);
                            AdActivity.setEventListener((AdContract.AdvertisementPresenter.EventListener) null);
                            Vungle._instance.playOperations.put(str3, false);
                            jobRunner.execute(SendReportsJob.makeJobInfo(false));
                            if (playAdCallback2 != null) {
                                PlayAdCallback playAdCallback = playAdCallback2;
                                if (!this.successfulView) {
                                    if (this.percentViewed < 80) {
                                        z = false;
                                        if (str2 != null && str2.equals("isCTAClicked")) {
                                            z2 = true;
                                        }
                                        playAdCallback.onAdEnd(str3, z, z2);
                                    }
                                }
                                z = true;
                                z2 = true;
                                playAdCallback.onAdEnd(str3, z, z2);
                            }
                            if (visionController.isEnabled()) {
                                visionController.reportData(advertisement2.getCreativeId(), advertisement2.getCampaignId(), advertisement2.getAdvertiserAppId());
                            }
                        } else if (str.equals("successfulView")) {
                            this.successfulView = true;
                        } else if (str.startsWith("percentViewed")) {
                            String[] split = str.split(":");
                            if (split.length == 2) {
                                this.percentViewed = Integer.parseInt(split[1]);
                            }
                        }
                    } catch (DatabaseHelper.DBException unused) {
                        onError(new VungleException(26), str3);
                    }
                }
            });
            if (advertisement == null || !"flexview".equals(advertisement.getTemplateType())) {
                z = false;
            }
            Intent intent = new Intent(_instance.context, z ? VungleFlexViewActivity.class : VungleActivity.class);
            intent.addFlags(268435456);
            intent.putExtra("placement", str);
            _instance.context.startActivity(intent);
        }
    }

    public static void setHeaderBiddingCallback(HeaderBiddingCallback headerBiddingCallback) {
        Context context2 = _instance.context;
        if (context2 != null) {
            ServiceLocator instance = ServiceLocator.getInstance(context2);
            ((RuntimeValues) instance.getService(RuntimeValues.class)).headerBiddingCallback = new HeaderBiddingCallbackWrapper(((SDKExecutors) instance.getService(SDKExecutors.class)).getUIExecutor(), headerBiddingCallback);
        }
    }

    public static void setIncentivizedFields(String str, String str2, String str3, String str4, String str5) {
        Context context2 = _instance.context;
        if (context2 == null) {
            Log.e(TAG, "Vungle is not initialized, context is null");
            return;
        }
        final ServiceLocator instance = ServiceLocator.getInstance(context2);
        final String str6 = str2;
        final String str7 = str3;
        final String str8 = str4;
        final String str9 = str5;
        final String str10 = str;
        ((SDKExecutors) instance.getService(SDKExecutors.class)).getVungleExecutor().execute(new Runnable() {
            public void run() {
                if (!Vungle.isInitialized()) {
                    Log.e(Vungle.TAG, "Vungle is not initialized");
                    return;
                }
                Repository repository = (Repository) instance.getService(Repository.class);
                Cookie cookie = repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get();
                if (cookie == null) {
                    cookie = new Cookie(Cookie.INCENTIVIZED_TEXT_COOKIE);
                }
                boolean z = false;
                if (!TextUtils.isEmpty(str6)) {
                    cookie.putValue("title", str6);
                    z = true;
                }
                if (!TextUtils.isEmpty(str7)) {
                    cookie.putValue("body", str7);
                    z = true;
                }
                if (!TextUtils.isEmpty(str8)) {
                    cookie.putValue("continue", str8);
                    z = true;
                }
                if (!TextUtils.isEmpty(str9)) {
                    cookie.putValue(JavascriptBridge.MraidHandler.CLOSE_ACTION, str9);
                    z = true;
                }
                if (!TextUtils.isEmpty(str10)) {
                    cookie.putValue("userID", str10);
                    z = true;
                }
                if (z) {
                    try {
                        repository.save(cookie);
                    } catch (DatabaseHelper.DBException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static void setUserLegacyID(String str) {
        if (isInitialized() || isInitializing.get()) {
            ((VungleApiClient) ServiceLocator.getInstance(_instance.context).getService(VungleApiClient.class)).updateIMEI(str, _instance.shouldTransmitIMEI);
        } else {
            _instance.userIMEI = str;
        }
    }

    /* access modifiers changed from: private */
    public static void stopPlaying() {
        if (_instance.context != null) {
            Intent intent = new Intent(AdContract.AdvertisementBus.ACTION);
            intent.putExtra(AdContract.AdvertisementBus.COMMAND, AdContract.AdvertisementBus.STOP_ALL);
            LocalBroadcastManager.a(_instance.context).a(intent);
        }
    }

    public static void updateConsentStatus(final Consent consent2, final String str) {
        Vungle vungle = _instance;
        vungle.consent = consent2;
        vungle.consentVersion = str;
        if (!isInitialized() || !isDepInit.get()) {
            Log.e(TAG, "Vungle is not initialized");
            return;
        }
        final Repository repository = (Repository) ServiceLocator.getInstance(_instance.context).getService(Repository.class);
        repository.load(Cookie.CONSENT_COOKIE, Cookie.class, new Repository.LoadCallback<Cookie>() {
            public void onLoaded(Cookie cookie) {
                if (cookie == null) {
                    cookie = new Cookie(Cookie.CONSENT_COOKIE);
                }
                cookie.putValue("consent_status", consent2 == Consent.OPTED_IN ? "opted_in" : "opted_out");
                cookie.putValue(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, Long.valueOf(System.currentTimeMillis() / 1000));
                cookie.putValue("consent_source", "publisher");
                String str = str;
                if (str == null) {
                    str = "";
                }
                cookie.putValue("consent_message_version", str);
                repository.save(cookie, (Repository.SaveCallback) null);
            }
        });
    }

    public static void init(String str, Context context2, InitCallback initCallback) throws IllegalArgumentException {
        init(str, context2, initCallback, new VungleSettings.Builder().build());
    }

    public static void loadAd(final String str, final AdConfig adConfig, final LoadAdCallback loadAdCallback) {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized");
            if (loadAdCallback != null) {
                loadAdCallback.onError(str, new VungleException(9));
                return;
            }
            return;
        }
        ((Repository) ServiceLocator.getInstance(_instance.context).getService(Repository.class)).load(str, Placement.class, new Repository.LoadCallback<Placement>() {
            public void onLoaded(Placement placement) {
                if (placement == null) {
                    LoadAdCallback loadAdCallback = loadAdCallback;
                    if (loadAdCallback != null) {
                        loadAdCallback.onError(str, new VungleException(13));
                        return;
                    }
                    return;
                }
                AdConfig adConfig = adConfig;
                if (adConfig == null || AdConfig.AdSize.isDefaultAdSize(adConfig.getAdSize())) {
                    Vungle.loadAdInternal(str, adConfig, loadAdCallback);
                    return;
                }
                LoadAdCallback loadAdCallback2 = loadAdCallback;
                if (loadAdCallback2 != null) {
                    loadAdCallback2.onError(str, new VungleException(29));
                }
            }
        });
    }

    public static void init(final String str, final Context context2, InitCallback initCallback, VungleSettings vungleSettings) throws IllegalArgumentException {
        if (initCallback == null) {
            throw new IllegalArgumentException("A valid InitCallback required to ensure API calls are being made after initialize is successful");
        } else if (context2 == null) {
            initCallback.onError(new VungleException(6));
        } else {
            final RuntimeValues runtimeValues = (RuntimeValues) ServiceLocator.getInstance(context2).getService(RuntimeValues.class);
            runtimeValues.settings = vungleSettings;
            final ServiceLocator instance = ServiceLocator.getInstance(context2);
            Executors executors = (Executors) instance.getService(Executors.class);
            if (!(initCallback instanceof InitCallbackWrapper)) {
                initCallback = new InitCallbackWrapper(executors.getUIExecutor(), initCallback);
            }
            runtimeValues.initCallback = initCallback;
            if (str == null || str.isEmpty()) {
                runtimeValues.initCallback.onError(new VungleException(6));
            } else if (!(context2 instanceof Application)) {
                runtimeValues.initCallback.onError(new VungleException(7));
            } else if (isInitialized()) {
                Log.d(TAG, "init already complete");
                runtimeValues.initCallback.onSuccess();
            } else if (isInitializing.getAndSet(true)) {
                Log.d(TAG, "init ongoing");
                runtimeValues.initCallback.onError(new VungleException(8));
            } else {
                executors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        Class<Cookie> cls = Cookie.class;
                        if (!Vungle.isDepInit.getAndSet(true)) {
                            CacheManager cacheManager = (CacheManager) instance.getService(CacheManager.class);
                            if (runtimeValues.settings == null || cacheManager.getBytesAvailable() >= runtimeValues.settings.getMinimumSpaceForInit()) {
                                cacheManager.addListener(Vungle.cacheListener);
                                Context unused = Vungle._instance.context = context2;
                                String unused2 = Vungle._instance.appID = str;
                                Repository repository = (Repository) instance.getService(Repository.class);
                                try {
                                    repository.init();
                                    VungleApiClient vungleApiClient = (VungleApiClient) instance.getService(VungleApiClient.class);
                                    vungleApiClient.init(str);
                                    if (!TextUtils.isEmpty(Vungle._instance.userIMEI)) {
                                        vungleApiClient.updateIMEI(Vungle._instance.userIMEI, Vungle._instance.shouldTransmitIMEI);
                                    }
                                    if (runtimeValues.settings != null) {
                                        vungleApiClient.setDefaultIdFallbackDisabled(runtimeValues.settings.getAndroidIdOptOut());
                                    }
                                    ((AdLoader) instance.getService(AdLoader.class)).init((JobRunner) instance.getService(JobRunner.class));
                                    if (Vungle._instance.consent == null || TextUtils.isEmpty(Vungle._instance.consentVersion)) {
                                        Cookie cookie = repository.load(Cookie.CONSENT_COOKIE, cls).get();
                                        if (cookie == null) {
                                            Consent unused3 = Vungle._instance.consent = null;
                                            String unused4 = Vungle._instance.consentVersion = null;
                                        } else {
                                            Consent unused5 = Vungle._instance.consent = Vungle.getConsent(cookie);
                                            String unused6 = Vungle._instance.consentVersion = Vungle.getConsentMessageVersion(cookie);
                                        }
                                    } else {
                                        Vungle.updateConsentStatus(Vungle._instance.consent, Vungle._instance.consentVersion);
                                    }
                                    Cookie cookie2 = repository.load("appId", cls).get();
                                    if (cookie2 == null) {
                                        cookie2 = new Cookie("appId");
                                    }
                                    cookie2.putValue("appId", str);
                                    try {
                                        repository.save(cookie2);
                                    } catch (DatabaseHelper.DBException unused7) {
                                        Vungle.onError(runtimeValues.initCallback, new VungleException(16));
                                        Vungle.deInit();
                                        return;
                                    }
                                } catch (DatabaseHelper.DBException unused8) {
                                    Vungle.onError(runtimeValues.initCallback, new VungleException(26));
                                    Vungle.deInit();
                                    return;
                                }
                            } else {
                                if (runtimeValues.initCallback != null) {
                                    runtimeValues.initCallback.onError(new VungleException(16));
                                }
                                Vungle.deInit();
                                return;
                            }
                        }
                        Vungle._instance.configure(runtimeValues.initCallback);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static String getConsentMessageVersion(Cookie cookie) {
        if (cookie == null) {
            return null;
        }
        return cookie.getString("consent_message_version");
    }

    static boolean canPlayAd(Advertisement advertisement) {
        Context context2 = _instance.context;
        if (context2 == null) {
            return false;
        }
        return ((AdLoader) ServiceLocator.getInstance(context2).getService(AdLoader.class)).canPlayAd(advertisement);
    }
}
