package com.vungle.warren;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.security.NetworkSecurityPolicy;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import androidx.annotation.Keep;
import com.facebook.react.uimanager.ViewProps;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moat.analytics.mobile.vng.MoatAnalytics;
import com.moat.analytics.mobile.vng.MoatOptions;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.JsonUtil;
import com.vungle.warren.model.VisionDataDBAdapter;
import com.vungle.warren.network.VungleApi;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.utility.ViewUtility;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.GzipSink;
import okio.Okio;
import okio.Sink;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VungleApiClient {
    private static String BASE_URL = "https://ads.api.vungle.com/";
    static String HEADER_UA = (MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "VungleAmazon/6.5.2" : "VungleDroid/6.5.2");
    static final String MANUFACTURER_AMAZON = "Amazon";
    protected static WrapperFramework WRAPPER_FRAMEWORK_SELECTED;
    private static Set<Interceptor> logInterceptors = new HashSet();
    private static Set<Interceptor> networkInterceptors = new HashSet();
    private final String TAG = "VungleApiClient";
    private VungleApi api;
    private JsonObject appBody;
    private CacheManager cacheManager;
    private OkHttpClient client;
    /* access modifiers changed from: private */
    public Context context;
    private boolean defaultIdFallbackDisabled;
    /* access modifiers changed from: private */
    public JsonObject deviceBody;
    private boolean enableMoat;
    private VungleApi gzipApi;
    private String newEndpoint;
    private String reportAdEndpoint;
    private Repository repository;
    private String requestAdEndpoint;
    /* access modifiers changed from: private */
    public Map<String, Long> retryAfterDataMap = new ConcurrentHashMap();
    private String riEndpoint;
    private boolean shouldTransmitIMEI;
    private VungleApi timeoutApi;
    /* access modifiers changed from: private */
    public String uaString = System.getProperty("http.agent");
    private JsonObject userBody;
    private String userImei;
    private boolean willPlayAdEnabled;
    private String willPlayAdEndpoint;
    private int willPlayAdTimeout;

    public static class ClearTextTrafficException extends IOException {
        ClearTextTrafficException(String str) {
            super(str);
        }
    }

    static class GzipRequestInterceptor implements Interceptor {
        private static final String CONTENT_ENCODING = "Content-Encoding";
        private static final String GZIP = "gzip";

        GzipRequestInterceptor() {
        }

        private RequestBody gzip(final RequestBody requestBody) throws IOException {
            final Buffer buffer = new Buffer();
            BufferedSink a2 = Okio.a((Sink) new GzipSink(buffer));
            requestBody.writeTo(a2);
            a2.close();
            return new RequestBody() {
                public long contentLength() {
                    return buffer.u();
                }

                public MediaType contentType() {
                    return requestBody.contentType();
                }

                public void writeTo(BufferedSink bufferedSink) throws IOException {
                    bufferedSink.a(buffer.v());
                }
            };
        }

        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            if (request.body() == null || request.header(CONTENT_ENCODING) != null) {
                return chain.proceed(request);
            }
            return chain.proceed(request.newBuilder().header(CONTENT_ENCODING, GZIP).method(request.method(), gzip(request.body())).build());
        }
    }

    @Keep
    public enum WrapperFramework {
        admob,
        air,
        cocos2dx,
        corona,
        dfp,
        heyzap,
        marmalade,
        mopub,
        unity,
        fyber,
        ironsource,
        upsight,
        appodeal,
        aerserv,
        adtoapp,
        tapdaq,
        vunglehbs,
        none
    }

    VungleApiClient(Context context2, CacheManager cacheManager2, Repository repository2) {
        Class cls = VungleApi.class;
        this.cacheManager = cacheManager2;
        this.context = context2.getApplicationContext();
        this.repository = repository2;
        OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                int code;
                Request request = chain.request();
                String encodedPath = request.url().encodedPath();
                Long l = (Long) VungleApiClient.this.retryAfterDataMap.get(encodedPath);
                if (l != null) {
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(l.longValue() - System.currentTimeMillis());
                    if (seconds > 0) {
                        return new Response.Builder().request(request).addHeader("Retry-After", String.valueOf(seconds)).code(500).protocol(Protocol.HTTP_1_1).message("Server is busy").body(ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), "{\"Error\":\"Retry-After\"}")).build();
                    }
                    VungleApiClient.this.retryAfterDataMap.remove(encodedPath);
                }
                Response proceed = chain.proceed(request);
                if (proceed != null && ((code = proceed.code()) == 429 || code == 500 || code == 502 || code == 503)) {
                    String str = proceed.headers().get("Retry-After");
                    if (!TextUtils.isEmpty(str)) {
                        try {
                            long parseLong = Long.parseLong(str);
                            if (parseLong > 0) {
                                VungleApiClient.this.retryAfterDataMap.put(encodedPath, Long.valueOf((parseLong * 1000) + System.currentTimeMillis()));
                            }
                        } catch (NumberFormatException unused) {
                            Log.d("VungleApiClient", "Retry-After value is not an valid value");
                        }
                    }
                }
                return proceed;
            }
        });
        this.client = addInterceptor.build();
        OkHttpClient build = addInterceptor.addInterceptor(new GzipRequestInterceptor()).build();
        Retrofit build2 = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(this.client).build();
        this.api = (VungleApi) build2.create(cls);
        this.gzipApi = (VungleApi) build2.newBuilder().client(build).build().create(cls);
    }

    /* access modifiers changed from: private */
    public void addUserAgentInCookie(String str) throws DatabaseHelper.DBException {
        Cookie cookie = new Cookie(Cookie.USER_AGENT_ID_COOKIE);
        cookie.putValue(Cookie.USER_AGENT_ID_COOKIE, str);
        this.repository.save(cookie);
    }

    private String getConnectionTypeDetail(int i) {
        switch (i) {
            case 1:
                return "GPRS";
            case 2:
                return "EDGE";
            case 3:
                return "UMTS";
            case 4:
                return "CDMA";
            case 5:
                return "EVDO_0";
            case 6:
                return "EVDO_A";
            case 7:
                return "1xRTT";
            case 8:
                return "HSDPA";
            case 9:
                return "HSUPA";
            case 10:
                return "HSPA";
            case 11:
                return "IDEN";
            case 12:
                return "EVDO_B";
            case 13:
                return "LTE";
            case 14:
                return "EHPRD";
            case 15:
                return "HSPAP";
            case 16:
                return "GSM";
            case 17:
                return "TD_SCDMA";
            case 18:
                return "IWLAN";
            default:
                return "UNKNOWN";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x01e7  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0252  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x02b1  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x030a A[Catch:{ SettingNotFoundException -> 0x0334 }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0324 A[Catch:{ SettingNotFoundException -> 0x0334 }] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0378  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x037b  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0115  */
    @android.annotation.SuppressLint({"HardwareIds"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.gson.JsonObject getDeviceBody() throws java.lang.IllegalStateException {
        /*
            r15 = this;
            java.lang.String r0 = "ifa"
            java.lang.String r1 = "Amazon"
            java.lang.String r2 = "VungleApiClient"
            com.google.gson.JsonObject r3 = new com.google.gson.JsonObject
            r3.<init>()
            r4 = 0
            r5 = 0
            r6 = 1
            java.lang.String r7 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x008b }
            boolean r7 = r1.equals(r7)     // Catch:{ Exception -> 0x008b }
            java.lang.String r8 = "advertising_id"
            if (r7 == 0) goto L_0x003d
            android.content.Context r7 = r15.context     // Catch:{ SettingNotFoundException -> 0x0031 }
            android.content.ContentResolver r7 = r7.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x0031 }
            java.lang.String r9 = "limit_ad_tracking"
            int r9 = android.provider.Settings.Secure.getInt(r7, r9)     // Catch:{ SettingNotFoundException -> 0x0031 }
            if (r9 != r6) goto L_0x0028
            r9 = 1
            goto L_0x0029
        L_0x0028:
            r9 = 0
        L_0x0029:
            java.lang.String r7 = android.provider.Settings.Secure.getString(r7, r8)     // Catch:{ SettingNotFoundException -> 0x002f }
            goto L_0x0092
        L_0x002f:
            r7 = move-exception
            goto L_0x0033
        L_0x0031:
            r7 = move-exception
            r9 = 1
        L_0x0033:
            java.lang.String r8 = "Error getting Amazon advertising info"
            android.util.Log.w(r2, r8, r7)     // Catch:{ Exception -> 0x003b }
            r7 = r4
            goto L_0x0092
        L_0x003b:
            r7 = r4
            goto L_0x008d
        L_0x003d:
            android.content.Context r7 = r15.context     // Catch:{ NoClassDefFoundError -> 0x005f }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r7 = com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(r7)     // Catch:{ NoClassDefFoundError -> 0x005f }
            if (r7 == 0) goto L_0x0059
            java.lang.String r9 = r7.getId()     // Catch:{ NoClassDefFoundError -> 0x005f }
            boolean r7 = r7.isLimitAdTrackingEnabled()     // Catch:{ NoClassDefFoundError -> 0x0057, Exception -> 0x0055 }
            com.google.gson.JsonObject r10 = r15.deviceBody     // Catch:{ NoClassDefFoundError -> 0x0053 }
            r10.a((java.lang.String) r0, (java.lang.String) r9)     // Catch:{ NoClassDefFoundError -> 0x0053 }
            goto L_0x005b
        L_0x0053:
            r10 = move-exception
            goto L_0x0062
        L_0x0055:
            r7 = r9
            goto L_0x008c
        L_0x0057:
            r10 = move-exception
            goto L_0x0061
        L_0x0059:
            r9 = r4
            r7 = 1
        L_0x005b:
            r14 = r9
            r9 = r7
            r7 = r14
            goto L_0x0092
        L_0x005f:
            r10 = move-exception
            r9 = r4
        L_0x0061:
            r7 = 1
        L_0x0062:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0087 }
            r11.<init>()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r12 = "Play services Not available: "
            r11.append(r12)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r10 = r10.getLocalizedMessage()     // Catch:{ Exception -> 0x0087 }
            r11.append(r10)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r10 = r11.toString()     // Catch:{ Exception -> 0x0087 }
            android.util.Log.e(r2, r10)     // Catch:{ Exception -> 0x0087 }
            android.content.Context r10 = r15.context     // Catch:{ Exception -> 0x0087 }
            android.content.ContentResolver r10 = r10.getContentResolver()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r8 = android.provider.Settings.Secure.getString(r10, r8)     // Catch:{ Exception -> 0x0087 }
            r9 = r7
            r7 = r8
            goto L_0x0092
        L_0x0087:
            r14 = r9
            r9 = r7
            r7 = r14
            goto L_0x008d
        L_0x008b:
            r7 = r4
        L_0x008c:
            r9 = 1
        L_0x008d:
            java.lang.String r8 = "Cannot load Advertising ID"
            android.util.Log.e(r2, r8)
        L_0x0092:
            java.lang.String r8 = ""
            if (r7 == 0) goto L_0x00ac
            java.lang.String r10 = android.os.Build.MANUFACTURER
            boolean r10 = r1.equals(r10)
            if (r10 == 0) goto L_0x00a1
            java.lang.String r10 = "amazon_advertising_id"
            goto L_0x00a3
        L_0x00a1:
            java.lang.String r10 = "gaid"
        L_0x00a3:
            r3.a((java.lang.String) r10, (java.lang.String) r7)
            com.google.gson.JsonObject r10 = r15.deviceBody
            r10.a((java.lang.String) r0, (java.lang.String) r7)
            goto L_0x00d7
        L_0x00ac:
            android.content.Context r7 = r15.context
            android.content.ContentResolver r7 = r7.getContentResolver()
            java.lang.String r10 = "android_id"
            java.lang.String r7 = android.provider.Settings.Secure.getString(r7, r10)
            com.google.gson.JsonObject r11 = r15.deviceBody
            boolean r12 = r15.defaultIdFallbackDisabled
            if (r12 == 0) goto L_0x00c0
        L_0x00be:
            r12 = r8
            goto L_0x00c7
        L_0x00c0:
            boolean r12 = android.text.TextUtils.isEmpty(r7)
            if (r12 != 0) goto L_0x00be
            r12 = r7
        L_0x00c7:
            r11.a((java.lang.String) r0, (java.lang.String) r12)
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            if (r0 != 0) goto L_0x00d7
            boolean r0 = r15.defaultIdFallbackDisabled
            if (r0 != 0) goto L_0x00d7
            r3.a((java.lang.String) r10, (java.lang.String) r7)
        L_0x00d7:
            com.google.gson.JsonObject r0 = r15.deviceBody
            java.lang.Integer r7 = java.lang.Integer.valueOf(r9)
            java.lang.String r9 = "lmt"
            r0.a((java.lang.String) r9, (java.lang.Number) r7)
            android.content.Context r0 = r15.context
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            if (r0 == 0) goto L_0x00f6
            java.lang.String r7 = "com.google.android.gms"
            r0.getPackageInfo(r7, r5)     // Catch:{ NameNotFoundException -> 0x00f1 }
            r0 = 1
            goto L_0x00f7
        L_0x00f1:
            java.lang.String r0 = "Google Play Svc not found"
            android.util.Log.d(r2, r0)
        L_0x00f6:
            r0 = 0
        L_0x00f7:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.String r7 = "is_google_play_services_available"
            r3.a((java.lang.String) r7, (java.lang.Boolean) r0)
            android.content.Context r0 = r15.context
            if (r0 == 0) goto L_0x010f
            android.content.IntentFilter r7 = new android.content.IntentFilter
            java.lang.String r9 = "android.intent.action.BATTERY_CHANGED"
            r7.<init>(r9)
            android.content.Intent r4 = r0.registerReceiver(r4, r7)
        L_0x010f:
            r0 = 4
            r7 = 2
            java.lang.String r9 = "UNKNOWN"
            if (r4 == 0) goto L_0x015c
            r10 = -1
            java.lang.String r11 = "level"
            int r11 = r4.getIntExtra(r11, r10)
            java.lang.String r12 = "scale"
            int r12 = r4.getIntExtra(r12, r10)
            if (r11 <= 0) goto L_0x0132
            if (r12 <= 0) goto L_0x0132
            float r11 = (float) r11
            float r12 = (float) r12
            float r11 = r11 / r12
            java.lang.Float r11 = java.lang.Float.valueOf(r11)
            java.lang.String r12 = "battery_level"
            r3.a((java.lang.String) r12, (java.lang.Number) r11)
        L_0x0132:
            java.lang.String r11 = "status"
            int r11 = r4.getIntExtra(r11, r10)
            if (r11 != r10) goto L_0x013b
            goto L_0x015c
        L_0x013b:
            if (r11 == r7) goto L_0x0144
            r12 = 5
            if (r11 != r12) goto L_0x0141
            goto L_0x0144
        L_0x0141:
            java.lang.String r4 = "NOT_CHARGING"
            goto L_0x015d
        L_0x0144:
            java.lang.String r11 = "plugged"
            int r4 = r4.getIntExtra(r11, r10)
            if (r4 == r6) goto L_0x0159
            if (r4 == r7) goto L_0x0156
            if (r4 == r0) goto L_0x0153
            java.lang.String r4 = "BATTERY_PLUGGED_OTHERS"
            goto L_0x015d
        L_0x0153:
            java.lang.String r4 = "BATTERY_PLUGGED_WIRELESS"
            goto L_0x015d
        L_0x0156:
            java.lang.String r4 = "BATTERY_PLUGGED_USB"
            goto L_0x015d
        L_0x0159:
            java.lang.String r4 = "BATTERY_PLUGGED_AC"
            goto L_0x015d
        L_0x015c:
            r4 = r9
        L_0x015d:
            java.lang.String r10 = "battery_state"
            r3.a((java.lang.String) r10, (java.lang.String) r4)
            int r4 = android.os.Build.VERSION.SDK_INT
            r10 = 21
            if (r4 < r10) goto L_0x0186
            android.content.Context r4 = r15.context
            java.lang.String r10 = "power"
            java.lang.Object r4 = r4.getSystemService(r10)
            android.os.PowerManager r4 = (android.os.PowerManager) r4
            if (r4 == 0) goto L_0x017c
            boolean r4 = r4.isPowerSaveMode()
            if (r4 == 0) goto L_0x017c
            r4 = 1
            goto L_0x017d
        L_0x017c:
            r4 = 0
        L_0x017d:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            java.lang.String r10 = "battery_saver_enabled"
            r3.a((java.lang.String) r10, (java.lang.Number) r4)
        L_0x0186:
            android.content.Context r4 = r15.context
            java.lang.String r10 = "android.permission.ACCESS_NETWORK_STATE"
            int r4 = androidx.core.content.PermissionChecker.a(r4, r10)
            r10 = 3
            if (r4 != 0) goto L_0x021f
            android.content.Context r4 = r15.context
            java.lang.String r11 = "connectivity"
            java.lang.Object r4 = r4.getSystemService(r11)
            android.net.ConnectivityManager r4 = (android.net.ConnectivityManager) r4
            java.lang.String r11 = "NONE"
            if (r4 == 0) goto L_0x01d6
            android.net.NetworkInfo r12 = r4.getActiveNetworkInfo()
            if (r12 == 0) goto L_0x01d6
            int r11 = r12.getType()
            if (r11 == 0) goto L_0x01c8
            if (r11 == r6) goto L_0x01c3
            r12 = 6
            if (r11 == r12) goto L_0x01c3
            r12 = 7
            if (r11 == r12) goto L_0x01be
            r12 = 9
            if (r11 == r12) goto L_0x01b9
            r11 = r9
            goto L_0x01d6
        L_0x01b9:
            java.lang.String r11 = "ETHERNET"
            java.lang.String r12 = "ETHERNET"
            goto L_0x01d7
        L_0x01be:
            java.lang.String r11 = "BLUETOOTH"
            java.lang.String r12 = "BLUETOOTH"
            goto L_0x01d7
        L_0x01c3:
            java.lang.String r11 = "WIFI"
            java.lang.String r12 = "WIFI"
            goto L_0x01d7
        L_0x01c8:
            int r11 = r12.getSubtype()
            java.lang.String r11 = r15.getConnectionTypeDetail(r11)
            java.lang.String r12 = "MOBILE"
            r14 = r12
            r12 = r11
            r11 = r14
            goto L_0x01d7
        L_0x01d6:
            r12 = r11
        L_0x01d7:
            java.lang.String r13 = "connection_type"
            r3.a((java.lang.String) r13, (java.lang.String) r11)
            java.lang.String r11 = "connection_type_detail"
            r3.a((java.lang.String) r11, (java.lang.String) r12)
            int r11 = android.os.Build.VERSION.SDK_INT
            r12 = 24
            if (r11 < r12) goto L_0x021f
            boolean r11 = r4.isActiveNetworkMetered()
            if (r11 == 0) goto L_0x020f
            int r4 = r4.getRestrictBackgroundStatus()
            if (r4 == r6) goto L_0x01fe
            if (r4 == r7) goto L_0x01fb
            if (r4 == r10) goto L_0x01f8
            goto L_0x0200
        L_0x01f8:
            java.lang.String r9 = "ENABLED"
            goto L_0x0200
        L_0x01fb:
            java.lang.String r9 = "WHITELISTED"
            goto L_0x0200
        L_0x01fe:
            java.lang.String r9 = "DISABLED"
        L_0x0200:
            java.lang.String r4 = "data_saver_status"
            r3.a((java.lang.String) r4, (java.lang.String) r9)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r6)
            java.lang.String r7 = "network_metered"
            r3.a((java.lang.String) r7, (java.lang.Number) r4)
            goto L_0x021f
        L_0x020f:
            java.lang.String r4 = "data_saver_status"
            java.lang.String r7 = "NOT_APPLICABLE"
            r3.a((java.lang.String) r4, (java.lang.String) r7)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r5)
            java.lang.String r7 = "network_metered"
            r3.a((java.lang.String) r7, (java.lang.Number) r4)
        L_0x021f:
            java.util.Locale r4 = java.util.Locale.getDefault()
            java.lang.String r4 = r4.toString()
            java.lang.String r7 = "locale"
            r3.a((java.lang.String) r7, (java.lang.String) r4)
            java.util.Locale r4 = java.util.Locale.getDefault()
            java.lang.String r4 = r4.getLanguage()
            java.lang.String r7 = "language"
            r3.a((java.lang.String) r7, (java.lang.String) r4)
            java.util.TimeZone r4 = java.util.TimeZone.getDefault()
            java.lang.String r4 = r4.getID()
            java.lang.String r7 = "time_zone"
            r3.a((java.lang.String) r7, (java.lang.String) r4)
            android.content.Context r4 = r15.context
            java.lang.String r7 = "audio"
            java.lang.Object r4 = r4.getSystemService(r7)
            android.media.AudioManager r4 = (android.media.AudioManager) r4
            if (r4 == 0) goto L_0x0274
            int r7 = r4.getStreamMaxVolume(r10)
            int r4 = r4.getStreamVolume(r10)
            float r9 = (float) r4
            float r7 = (float) r7
            float r9 = r9 / r7
            java.lang.Float r7 = java.lang.Float.valueOf(r9)
            java.lang.String r9 = "volume_level"
            r3.a((java.lang.String) r9, (java.lang.Number) r7)
            if (r4 <= 0) goto L_0x026a
            r4 = 1
            goto L_0x026b
        L_0x026a:
            r4 = 0
        L_0x026b:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            java.lang.String r7 = "sound_enabled"
            r3.a((java.lang.String) r7, (java.lang.Number) r4)
        L_0x0274:
            com.vungle.warren.persistence.CacheManager r4 = r15.cacheManager
            java.io.File r4 = r4.getCache()
            r4.getPath()
            boolean r7 = r4.exists()
            if (r7 == 0) goto L_0x0298
            boolean r4 = r4.isDirectory()
            if (r4 == 0) goto L_0x0298
            com.vungle.warren.persistence.CacheManager r4 = r15.cacheManager
            long r9 = r4.getBytesAvailable()
            java.lang.Long r4 = java.lang.Long.valueOf(r9)
            java.lang.String r7 = "storage_bytes_available"
            r3.a((java.lang.String) r7, (java.lang.Number) r4)
        L_0x0298:
            java.lang.String r4 = android.os.Build.MANUFACTURER
            boolean r4 = r1.equals(r4)
            if (r4 == 0) goto L_0x02b1
            android.content.Context r0 = r15.context
            android.content.Context r0 = r0.getApplicationContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r4 = "amazon.hardware.fire_tv"
            boolean r0 = r0.hasSystemFeature(r4)
            goto L_0x02f0
        L_0x02b1:
            int r4 = android.os.Build.VERSION.SDK_INT
            r7 = 23
            if (r4 < r7) goto L_0x02cb
            android.content.Context r4 = r15.context
            java.lang.String r7 = "uimode"
            java.lang.Object r4 = r4.getSystemService(r7)
            android.app.UiModeManager r4 = (android.app.UiModeManager) r4
            int r4 = r4.getCurrentModeType()
            if (r4 != r0) goto L_0x02c9
        L_0x02c7:
            r0 = 1
            goto L_0x02f0
        L_0x02c9:
            r0 = 0
            goto L_0x02f0
        L_0x02cb:
            android.content.Context r0 = r15.context
            android.content.Context r0 = r0.getApplicationContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r4 = "com.google.android.tv"
            boolean r0 = r0.hasSystemFeature(r4)
            if (r0 != 0) goto L_0x02c7
            android.content.Context r0 = r15.context
            android.content.Context r0 = r0.getApplicationContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r4 = "android.hardware.touchscreen"
            boolean r0 = r0.hasSystemFeature(r4)
            if (r0 != 0) goto L_0x02c9
            goto L_0x02c7
        L_0x02f0:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.String r4 = "is_tv"
            r3.a((java.lang.String) r4, (java.lang.Boolean) r0)
            int r0 = android.os.Build.VERSION.SDK_INT
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r4 = "os_api_level"
            r3.a((java.lang.String) r4, (java.lang.Number) r0)
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ SettingNotFoundException -> 0x0334 }
            r4 = 26
            if (r0 < r4) goto L_0x0324
            android.content.Context r0 = r15.context     // Catch:{ SettingNotFoundException -> 0x0334 }
            java.lang.String r4 = "android.permission.REQUEST_INSTALL_PACKAGES"
            int r0 = r0.checkCallingOrSelfPermission(r4)     // Catch:{ SettingNotFoundException -> 0x0334 }
            if (r0 != 0) goto L_0x033a
            android.content.Context r0 = r15.context     // Catch:{ SettingNotFoundException -> 0x0334 }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ SettingNotFoundException -> 0x0334 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ SettingNotFoundException -> 0x0334 }
            boolean r0 = r0.canRequestPackageInstalls()     // Catch:{ SettingNotFoundException -> 0x0334 }
            r5 = r0
            goto L_0x033a
        L_0x0324:
            android.content.Context r0 = r15.context     // Catch:{ SettingNotFoundException -> 0x0334 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x0334 }
            java.lang.String r4 = "install_non_market_apps"
            int r0 = android.provider.Settings.Secure.getInt(r0, r4)     // Catch:{ SettingNotFoundException -> 0x0334 }
            if (r0 != r6) goto L_0x033a
            r5 = 1
            goto L_0x033a
        L_0x0334:
            r0 = move-exception
            java.lang.String r4 = "isInstallNonMarketAppsEnabled Settings not found"
            android.util.Log.e(r2, r4, r0)
        L_0x033a:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)
            java.lang.String r2 = "is_sideload_enabled"
            r3.a((java.lang.String) r2, (java.lang.Boolean) r0)
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r2 = "mounted"
            boolean r0 = r0.equals(r2)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r2 = "sd_card_available"
            r3.a((java.lang.String) r2, (java.lang.Number) r0)
            java.lang.String r0 = android.os.Build.FINGERPRINT
            java.lang.String r2 = "os_name"
            r3.a((java.lang.String) r2, (java.lang.String) r0)
            java.lang.String r0 = "vduid"
            r3.a((java.lang.String) r0, (java.lang.String) r8)
            com.google.gson.JsonObject r0 = r15.deviceBody
            java.lang.String r2 = "ext"
            com.google.gson.JsonObject r0 = r0.c(r2)
            java.lang.String r2 = "vungle"
            com.google.gson.JsonObject r0 = r0.c(r2)
            java.lang.String r2 = android.os.Build.MANUFACTURER
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x037b
            java.lang.String r1 = "amazon"
            goto L_0x037d
        L_0x037b:
            java.lang.String r1 = "android"
        L_0x037d:
            r0.a((java.lang.String) r1, (com.google.gson.JsonElement) r3)
            com.google.gson.JsonObject r0 = r15.deviceBody
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.VungleApiClient.getDeviceBody():com.google.gson.JsonObject");
    }

    private String getUserAgentFromCookie() {
        Cookie cookie = this.repository.load(Cookie.USER_AGENT_ID_COOKIE, Cookie.class).get();
        if (cookie == null) {
            return System.getProperty("http.agent");
        }
        String string = cookie.getString(Cookie.USER_AGENT_ID_COOKIE);
        return TextUtils.isEmpty(string) ? System.getProperty("http.agent") : string;
    }

    private JsonObject getUserBody() {
        long j;
        String str;
        String str2;
        String str3;
        JsonObject jsonObject = new JsonObject();
        Cookie cookie = this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get();
        if (cookie != null) {
            str2 = cookie.getString("consent_status");
            str = cookie.getString("consent_source");
            j = cookie.getLong(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP).longValue();
            str3 = cookie.getString("consent_message_version");
        } else {
            j = 0;
            str2 = "unknown";
            str = "no_interaction";
            str3 = "";
        }
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.a("consent_status", str2);
        jsonObject2.a("consent_source", str);
        jsonObject2.a("consent_timestamp", (Number) Long.valueOf(j));
        if (TextUtils.isEmpty(str3)) {
            str3 = "";
        }
        jsonObject2.a("consent_message_version", str3);
        jsonObject.a("gdpr", (JsonElement) jsonObject2);
        return jsonObject;
    }

    private void initUserAgentLazy() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    String unused = VungleApiClient.this.uaString = WebSettings.getDefaultUserAgent(VungleApiClient.this.context);
                    VungleApiClient.this.deviceBody.a("ua", VungleApiClient.this.uaString);
                    VungleApiClient.this.addUserAgentInCookie(VungleApiClient.this.uaString);
                } catch (Exception e) {
                    Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
                }
            }
        }).start();
    }

    /* access modifiers changed from: package-private */
    public boolean canCallWillPlayAd() {
        return this.willPlayAdEnabled && !TextUtils.isEmpty(this.willPlayAdEndpoint);
    }

    public retrofit2.Response<JsonObject> config() throws VungleException, IOException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.a("device", (JsonElement) getDeviceBody());
        jsonObject.a("app", (JsonElement) this.appBody);
        jsonObject.a("user", (JsonElement) getUserBody());
        retrofit2.Response<JsonObject> execute = this.api.config(HEADER_UA, jsonObject).execute();
        if (!execute.isSuccessful()) {
            return execute;
        }
        JsonObject body = execute.body();
        Log.d("VungleApiClient", "Config Response: " + body);
        if (JsonUtil.hasNonNull(body, "sleep")) {
            String i = JsonUtil.hasNonNull(body, "info") ? body.a("info").i() : "";
            Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. " + i);
            throw new VungleException(3);
        } else if (JsonUtil.hasNonNull(body, "endpoints")) {
            JsonObject c = body.c("endpoints");
            HttpUrl parse = HttpUrl.parse(c.a("new").i());
            HttpUrl parse2 = HttpUrl.parse(c.a("ads").i());
            HttpUrl parse3 = HttpUrl.parse(c.a("will_play_ad").i());
            HttpUrl parse4 = HttpUrl.parse(c.a("report_ad").i());
            HttpUrl parse5 = HttpUrl.parse(c.a("ri").i());
            if (parse == null || parse2 == null || parse3 == null || parse4 == null || parse5 == null) {
                Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
                throw new VungleException(3);
            }
            this.newEndpoint = parse.toString();
            this.requestAdEndpoint = parse2.toString();
            this.willPlayAdEndpoint = parse3.toString();
            this.reportAdEndpoint = parse4.toString();
            this.riEndpoint = parse5.toString();
            JsonObject c2 = body.c("will_play_ad");
            this.willPlayAdTimeout = c2.a("request_timeout").d();
            this.willPlayAdEnabled = c2.a(ViewProps.ENABLED).a();
            this.enableMoat = body.c("viewability").a("moat").a();
            if (this.willPlayAdEnabled) {
                Log.v("VungleApiClient", "willPlayAd is enabled, generating a timeout client.");
                this.timeoutApi = (VungleApi) new Retrofit.Builder().client(this.client.newBuilder().readTimeout((long) this.willPlayAdTimeout, TimeUnit.MILLISECONDS).build()).addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.vungle.com/").build().create(VungleApi.class);
            }
            if (getMoatEnabled()) {
                MoatOptions moatOptions = new MoatOptions();
                moatOptions.disableAdIdCollection = true;
                moatOptions.disableLocationServices = true;
                moatOptions.loggingEnabled = true;
                MoatAnalytics.getInstance().start(moatOptions, (Application) this.context.getApplicationContext());
            }
            return execute;
        } else {
            Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
            throw new VungleException(3);
        }
    }

    public boolean getMoatEnabled() {
        return this.enableMoat && Build.VERSION.SDK_INT >= 16;
    }

    public long getRetryAfterHeaderValue(retrofit2.Response<JsonObject> response) {
        try {
            return Long.parseLong(response.headers().get("Retry-After")) * 1000;
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public void init(String str) {
        init(this.context, str);
    }

    /* access modifiers changed from: package-private */
    public void overrideApi(VungleApi vungleApi) {
        this.api = vungleApi;
    }

    public boolean pingTPAT(String str) throws ClearTextTrafficException, MalformedURLException {
        boolean z;
        if (TextUtils.isEmpty(str) || HttpUrl.parse(str) == null) {
            throw new MalformedURLException("Invalid URL : " + str);
        }
        try {
            String host = new URL(str).getHost();
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                z = NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(host);
            } else {
                z = i >= 23 ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : true;
            }
            if (z || !URLUtil.isHttpUrl(str)) {
                if (!TextUtils.isEmpty(this.userImei) && this.shouldTransmitIMEI) {
                    str = str.replace("%imei%", this.userImei);
                }
                try {
                    this.api.pingTPAT(this.uaString, str).execute();
                    return true;
                } catch (IOException unused) {
                    return false;
                }
            } else {
                throw new ClearTextTrafficException("Clear Text Traffic is blocked");
            }
        } catch (MalformedURLException unused2) {
            throw new MalformedURLException("Invalid URL : " + str);
        }
    }

    public Call<JsonObject> reportAd(JsonObject jsonObject) {
        if (this.reportAdEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.a("device", (JsonElement) getDeviceBody());
            jsonObject2.a("app", (JsonElement) this.appBody);
            jsonObject2.a("request", (JsonElement) jsonObject);
            jsonObject2.a("user", (JsonElement) getUserBody());
            return this.gzipApi.reportAd(HEADER_UA, this.reportAdEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> reportNew() throws IllegalStateException {
        if (this.newEndpoint != null) {
            HashMap hashMap = new HashMap(2);
            JsonElement a2 = this.appBody.a("id");
            JsonElement a3 = this.deviceBody.a("ifa");
            String str = "";
            hashMap.put(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_APP_ID, a2 != null ? a2.i() : str);
            if (a3 != null) {
                str = a3.i();
            }
            hashMap.put("ifa", str);
            return this.api.reportNew(HEADER_UA, this.newEndpoint, hashMap);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> requestAd(String str, String str2, boolean z, JsonObject jsonObject) throws IllegalStateException {
        if (this.requestAdEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.a("device", (JsonElement) getDeviceBody());
            jsonObject2.a("app", (JsonElement) this.appBody);
            JsonObject userBody2 = getUserBody();
            if (jsonObject != null) {
                userBody2.a("vision", (JsonElement) jsonObject);
            }
            jsonObject2.a("user", (JsonElement) userBody2);
            JsonObject jsonObject3 = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            jsonArray.a(str);
            jsonObject3.a("placements", (JsonElement) jsonArray);
            jsonObject3.a("header_bidding", Boolean.valueOf(z));
            if (!TextUtils.isEmpty(str2)) {
                jsonObject3.a("ad_size", str2);
            }
            jsonObject2.a("request", (JsonElement) jsonObject3);
            return this.gzipApi.ads(HEADER_UA, this.requestAdEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> ri(JsonObject jsonObject) {
        if (this.riEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.a("device", (JsonElement) getDeviceBody());
            jsonObject2.a("app", (JsonElement) this.appBody);
            jsonObject2.a("request", (JsonElement) jsonObject);
            return this.api.ri(HEADER_UA, this.riEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public void setDefaultIdFallbackDisabled(boolean z) {
        this.defaultIdFallbackDisabled = z;
    }

    public void updateIMEI(String str, boolean z) {
        this.userImei = str;
        this.shouldTransmitIMEI = z;
    }

    /* access modifiers changed from: package-private */
    public Call<JsonObject> willPlayAd(String str, boolean z, String str2) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.a("device", (JsonElement) getDeviceBody());
        jsonObject.a("app", (JsonElement) this.appBody);
        jsonObject.a("user", (JsonElement) getUserBody());
        JsonObject jsonObject2 = new JsonObject();
        JsonObject jsonObject3 = new JsonObject();
        jsonObject3.a("reference_id", str);
        jsonObject3.a("is_auto_cached", Boolean.valueOf(z));
        jsonObject2.a("placement", (JsonElement) jsonObject3);
        jsonObject2.a(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_AD_TOKEN, str2);
        jsonObject.a("request", (JsonElement) jsonObject2);
        return this.timeoutApi.willPlayAd(HEADER_UA, this.willPlayAdEndpoint, jsonObject);
    }

    private synchronized void init(final Context context2, String str) {
        this.shouldTransmitIMEI = false;
        JsonObject jsonObject = new JsonObject();
        jsonObject.a("id", str);
        jsonObject.a("bundle", context2.getPackageName());
        String str2 = null;
        try {
            str2 = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException unused) {
        }
        if (str2 == null) {
            str2 = "1.0";
        }
        jsonObject.a("ver", str2);
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.a("make", Build.MANUFACTURER);
        jsonObject2.a("model", Build.MODEL);
        jsonObject2.a("osv", Build.VERSION.RELEASE);
        jsonObject2.a("carrier", ((TelephonyManager) context2.getSystemService("phone")).getNetworkOperatorName());
        jsonObject2.a("os", MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context2.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        jsonObject2.a("w", (Number) Integer.valueOf(displayMetrics.widthPixels));
        jsonObject2.a("h", (Number) Integer.valueOf(displayMetrics.heightPixels));
        JsonObject jsonObject3 = new JsonObject();
        jsonObject3.a("vungle", (JsonElement) new JsonObject());
        jsonObject2.a("ext", (JsonElement) jsonObject3);
        try {
            if (Build.VERSION.SDK_INT >= 17) {
                this.uaString = getUserAgentFromCookie();
                initUserAgentLazy();
            } else if (Looper.getMainLooper() == Looper.myLooper()) {
                this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
            } else {
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        try {
                            String unused = VungleApiClient.this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
                        } catch (InstantiationException e) {
                            Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
                        }
                        countDownLatch.countDown();
                    }
                });
                if (!countDownLatch.await(2, TimeUnit.SECONDS)) {
                    Log.e("VungleApiClient", "Unable to get User Agent String in specified time");
                }
            }
        } catch (Exception e) {
            Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
        }
        jsonObject2.a("ua", this.uaString);
        this.deviceBody = jsonObject2;
        this.appBody = jsonObject;
    }
}
