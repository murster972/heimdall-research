package com.vungle.warren;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.vungle.warren.AdConfig;
import com.vungle.warren.downloader.AssetDownloadListener;
import com.vungle.warren.downloader.DownloadRequest;
import com.vungle.warren.downloader.Downloader;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.AdAsset;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.JsonUtil;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.ui.HackMraid;
import com.vungle.warren.utility.Executors;
import com.vungle.warren.utility.FileUtility;
import com.vungle.warren.utility.UnzipUtility;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdLoader {
    public static final long EXPONENTIAL_RATE = 2;
    public static final int RETRY_COUNT = 5;
    public static final long RETRY_DELAY = 2000;
    /* access modifiers changed from: private */
    public static final String TAG = "com.vungle.warren.AdLoader";
    /* access modifiers changed from: private */
    public final CacheManager cacheManager;
    private final Downloader downloader;
    private JobRunner jobRunner;
    /* access modifiers changed from: private */
    public final Map<String, Operation> loadOperations = new ConcurrentHashMap();
    private final Map<String, Operation> pendingOperations = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public final Repository repository;
    /* access modifiers changed from: private */
    public final RuntimeValues runtimeValues;
    /* access modifiers changed from: private */
    public final Executors sdkExecutors;
    /* access modifiers changed from: private */
    public final VisionController visionController;
    /* access modifiers changed from: private */
    public final VungleStaticApi vungleApi;
    /* access modifiers changed from: private */
    public final VungleApiClient vungleApiClient;

    private class DownloadAdCallback implements DownloadCallback {
        private DownloadAdCallback() {
        }

        public void onDownloadCompleted(String str, String str2) {
            Advertisement advertisement;
            String access$400 = AdLoader.TAG;
            Log.d(access$400, "download completed " + str);
            Placement placement = AdLoader.this.repository.load(str, Placement.class).get();
            if (placement == null) {
                onDownloadFailed(new VungleException(13), str, str2);
                return;
            }
            if (TextUtils.isEmpty(str2)) {
                advertisement = null;
            } else {
                advertisement = AdLoader.this.repository.load(str2, Advertisement.class).get();
            }
            if (advertisement == null) {
                onDownloadFailed(new VungleException(11), str, str2);
                return;
            }
            Operation operation = (Operation) AdLoader.this.loadOperations.get(str);
            if (!(operation == null || operation.downloadStartTimeMillis == 0)) {
                advertisement.setTtDownload(System.currentTimeMillis() - operation.downloadStartTimeMillis);
            }
            try {
                AdLoader.this.repository.saveAndApplyState(advertisement, str, 1);
                onReady(str, placement, advertisement);
            } catch (DatabaseHelper.DBException unused) {
                onDownloadFailed(new VungleException(26), str, str2);
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(4:10|11|12|13) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:71|72|73|74) */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x014e, code lost:
            return;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x0132 */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x00bc A[SYNTHETIC, Splitter:B:44:0x00bc] */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:73:0x0132=Splitter:B:73:0x0132, B:12:0x0049=Splitter:B:12:0x0049} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onDownloadFailed(com.vungle.warren.error.VungleException r13, java.lang.String r14, java.lang.String r15) {
            /*
                r12 = this;
                com.vungle.warren.AdLoader r0 = com.vungle.warren.AdLoader.this
                monitor-enter(r0)
                com.vungle.warren.AdLoader r1 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x014f }
                java.util.Map r1 = r1.loadOperations     // Catch:{ all -> 0x014f }
                java.lang.Object r1 = r1.remove(r14)     // Catch:{ all -> 0x014f }
                com.vungle.warren.AdLoader$Operation r1 = (com.vungle.warren.AdLoader.Operation) r1     // Catch:{ all -> 0x014f }
                com.vungle.warren.AdLoader r2 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x014f }
                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ all -> 0x014f }
                java.lang.Class<com.vungle.warren.model.Placement> r3 = com.vungle.warren.model.Placement.class
                com.vungle.warren.persistence.Repository$FutureResult r2 = r2.load(r14, r3)     // Catch:{ all -> 0x014f }
                java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x014f }
                com.vungle.warren.model.Placement r2 = (com.vungle.warren.model.Placement) r2     // Catch:{ all -> 0x014f }
                if (r15 != 0) goto L_0x0025
                r15 = 0
                goto L_0x0037
            L_0x0025:
                com.vungle.warren.AdLoader r3 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x014f }
                com.vungle.warren.persistence.Repository r3 = r3.repository     // Catch:{ all -> 0x014f }
                java.lang.Class<com.vungle.warren.model.Advertisement> r4 = com.vungle.warren.model.Advertisement.class
                com.vungle.warren.persistence.Repository$FutureResult r15 = r3.load(r15, r4)     // Catch:{ all -> 0x014f }
                java.lang.Object r15 = r15.get()     // Catch:{ all -> 0x014f }
                com.vungle.warren.model.Advertisement r15 = (com.vungle.warren.model.Advertisement) r15     // Catch:{ all -> 0x014f }
            L_0x0037:
                r3 = 26
                r4 = 4
                r5 = 0
                if (r2 != 0) goto L_0x006d
                if (r15 == 0) goto L_0x004e
                com.vungle.warren.AdLoader r2 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0049 }
                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ DBException -> 0x0049 }
                r2.saveAndApplyState(r15, r14, r4)     // Catch:{ DBException -> 0x0049 }
                goto L_0x004e
            L_0x0049:
                com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x014f }
                r13.<init>(r3)     // Catch:{ all -> 0x014f }
            L_0x004e:
                if (r1 == 0) goto L_0x0066
                java.util.Set<com.vungle.warren.LoadAdCallback> r15 = r1.loadAdCallbacks     // Catch:{ all -> 0x014f }
                java.util.Iterator r15 = r15.iterator()     // Catch:{ all -> 0x014f }
            L_0x0056:
                boolean r1 = r15.hasNext()     // Catch:{ all -> 0x014f }
                if (r1 == 0) goto L_0x0066
                java.lang.Object r1 = r15.next()     // Catch:{ all -> 0x014f }
                com.vungle.warren.LoadAdCallback r1 = (com.vungle.warren.LoadAdCallback) r1     // Catch:{ all -> 0x014f }
                r1.onError(r14, r13)     // Catch:{ all -> 0x014f }
                goto L_0x0056
            L_0x0066:
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x014f }
                r13.setLoading(r14, r5)     // Catch:{ all -> 0x014f }
                monitor-exit(r0)     // Catch:{ all -> 0x014f }
                return
            L_0x006d:
                int r2 = r13.getExceptionCode()     // Catch:{ all -> 0x014f }
                r6 = 1
                if (r2 == r6) goto L_0x0096
                r7 = 14
                if (r2 == r7) goto L_0x0096
                r7 = 20
                if (r2 == r7) goto L_0x0093
                r7 = 25
                if (r2 == r7) goto L_0x0096
                r7 = 22
                if (r2 == r7) goto L_0x0093
                r7 = 23
                if (r2 == r7) goto L_0x0089
                goto L_0x008f
            L_0x0089:
                if (r15 == 0) goto L_0x008f
                r2 = 0
                r7 = 1
                r8 = 0
                goto L_0x0098
            L_0x008f:
                r2 = 0
            L_0x0090:
                r7 = 0
            L_0x0091:
                r8 = 4
                goto L_0x0098
            L_0x0093:
                r2 = 0
                r7 = 1
                goto L_0x0091
            L_0x0096:
                r2 = 1
                goto L_0x0090
            L_0x0098:
                java.lang.String r9 = com.vungle.warren.AdLoader.TAG     // Catch:{ all -> 0x014f }
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
                r10.<init>()     // Catch:{ all -> 0x014f }
                java.lang.String r11 = "Failed to load Ad/Assets for "
                r10.append(r11)     // Catch:{ all -> 0x014f }
                r10.append(r14)     // Catch:{ all -> 0x014f }
                java.lang.String r11 = ". Cause : "
                r10.append(r11)     // Catch:{ all -> 0x014f }
                java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x014f }
                android.util.Log.e(r9, r10, r13)     // Catch:{ all -> 0x014f }
                com.vungle.warren.AdLoader r9 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x014f }
                r9.setLoading(r14, r5)     // Catch:{ all -> 0x014f }
                if (r1 == 0) goto L_0x014d
                int r9 = r1.policy     // Catch:{ DBException -> 0x0132 }
                r10 = 2
                if (r9 != 0) goto L_0x00f1
                int r2 = r1.retry     // Catch:{ DBException -> 0x0132 }
                int r5 = r1.retryLimit     // Catch:{ DBException -> 0x0132 }
                if (r2 >= r5) goto L_0x0126
                if (r7 == 0) goto L_0x0126
                if (r15 == 0) goto L_0x00d5
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0132 }
                com.vungle.warren.persistence.Repository r13 = r13.repository     // Catch:{ DBException -> 0x0132 }
                r13.saveAndApplyState(r15, r14, r8)     // Catch:{ DBException -> 0x0132 }
            L_0x00d5:
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0132 }
                long r4 = r1.retryDelay     // Catch:{ DBException -> 0x0132 }
                com.vungle.warren.AdLoader$Operation r15 = r1.delay(r4)     // Catch:{ DBException -> 0x0132 }
                long r4 = r1.retryDelay     // Catch:{ DBException -> 0x0132 }
                long r4 = r4 * r10
                com.vungle.warren.AdLoader$Operation r15 = r15.retryDelay(r4)     // Catch:{ DBException -> 0x0132 }
                int r2 = r1.retry     // Catch:{ DBException -> 0x0132 }
                int r2 = r2 + r6
                com.vungle.warren.AdLoader$Operation r15 = r15.retry(r2)     // Catch:{ DBException -> 0x0132 }
                r13.load(r15)     // Catch:{ DBException -> 0x0132 }
                monitor-exit(r0)     // Catch:{ all -> 0x014f }
                return
            L_0x00f1:
                int r9 = r1.policy     // Catch:{ DBException -> 0x0132 }
                if (r9 != r6) goto L_0x0126
                if (r2 != 0) goto L_0x0126
                int r13 = r1.retry     // Catch:{ DBException -> 0x0132 }
                int r2 = r1.retryLimit     // Catch:{ DBException -> 0x0132 }
                if (r13 >= r2) goto L_0x0102
                if (r7 == 0) goto L_0x0102
                int r5 = r13 + 1
                r4 = r8
            L_0x0102:
                if (r15 == 0) goto L_0x010d
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0132 }
                com.vungle.warren.persistence.Repository r13 = r13.repository     // Catch:{ DBException -> 0x0132 }
                r13.saveAndApplyState(r15, r14, r4)     // Catch:{ DBException -> 0x0132 }
            L_0x010d:
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0132 }
                long r6 = r1.retryDelay     // Catch:{ DBException -> 0x0132 }
                com.vungle.warren.AdLoader$Operation r15 = r1.delay(r6)     // Catch:{ DBException -> 0x0132 }
                long r6 = r1.retryDelay     // Catch:{ DBException -> 0x0132 }
                long r6 = r6 * r10
                com.vungle.warren.AdLoader$Operation r15 = r15.retryDelay(r6)     // Catch:{ DBException -> 0x0132 }
                com.vungle.warren.AdLoader$Operation r15 = r15.retry(r5)     // Catch:{ DBException -> 0x0132 }
                r13.load(r15)     // Catch:{ DBException -> 0x0132 }
                monitor-exit(r0)     // Catch:{ all -> 0x014f }
                return
            L_0x0126:
                if (r15 == 0) goto L_0x0137
                com.vungle.warren.AdLoader r2 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0132 }
                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ DBException -> 0x0132 }
                r2.saveAndApplyState(r15, r14, r4)     // Catch:{ DBException -> 0x0132 }
                goto L_0x0137
            L_0x0132:
                com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x014f }
                r13.<init>(r3)     // Catch:{ all -> 0x014f }
            L_0x0137:
                java.util.Set<com.vungle.warren.LoadAdCallback> r15 = r1.loadAdCallbacks     // Catch:{ all -> 0x014f }
                java.util.Iterator r15 = r15.iterator()     // Catch:{ all -> 0x014f }
            L_0x013d:
                boolean r1 = r15.hasNext()     // Catch:{ all -> 0x014f }
                if (r1 == 0) goto L_0x014d
                java.lang.Object r1 = r15.next()     // Catch:{ all -> 0x014f }
                com.vungle.warren.LoadAdCallback r1 = (com.vungle.warren.LoadAdCallback) r1     // Catch:{ all -> 0x014f }
                r1.onError(r14, r13)     // Catch:{ all -> 0x014f }
                goto L_0x013d
            L_0x014d:
                monitor-exit(r0)     // Catch:{ all -> 0x014f }
                return
            L_0x014f:
                r13 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x014f }
                throw r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.AdLoader.DownloadAdCallback.onDownloadFailed(com.vungle.warren.error.VungleException, java.lang.String, java.lang.String):void");
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(8:12|13|14|15|16|17|(2:20|18)|26) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0067 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onReady(java.lang.String r5, com.vungle.warren.model.Placement r6, com.vungle.warren.model.Advertisement r7) {
            /*
                r4 = this;
                com.vungle.warren.AdLoader r0 = com.vungle.warren.AdLoader.this
                monitor-enter(r0)
                com.vungle.warren.AdLoader r1 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x008d }
                r2 = 0
                r1.setLoading(r5, r2)     // Catch:{ all -> 0x008d }
                com.vungle.warren.AdLoader r1 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x008d }
                com.vungle.warren.RuntimeValues r1 = r1.runtimeValues     // Catch:{ all -> 0x008d }
                com.vungle.warren.HeaderBiddingCallback r1 = r1.headerBiddingCallback     // Catch:{ all -> 0x008d }
                if (r1 == 0) goto L_0x001a
                java.lang.String r2 = r7.getBidToken()     // Catch:{ all -> 0x008d }
                r1.adAvailableForBidToken(r5, r2)     // Catch:{ all -> 0x008d }
            L_0x001a:
                java.lang.String r1 = com.vungle.warren.AdLoader.TAG     // Catch:{ all -> 0x008d }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
                r2.<init>()     // Catch:{ all -> 0x008d }
                java.lang.String r3 = "found already cached valid adv, calling onAdLoad "
                r2.append(r3)     // Catch:{ all -> 0x008d }
                r2.append(r5)     // Catch:{ all -> 0x008d }
                java.lang.String r3 = " callback "
                r2.append(r3)     // Catch:{ all -> 0x008d }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x008d }
                android.util.Log.i(r1, r2)     // Catch:{ all -> 0x008d }
                com.vungle.warren.AdLoader r1 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x008d }
                com.vungle.warren.RuntimeValues r1 = r1.runtimeValues     // Catch:{ all -> 0x008d }
                com.vungle.warren.InitCallback r1 = r1.initCallback     // Catch:{ all -> 0x008d }
                boolean r2 = r6.isAutoCached()     // Catch:{ all -> 0x008d }
                if (r2 == 0) goto L_0x004a
                if (r1 == 0) goto L_0x004a
                r1.onAutoCacheAdAvailable(r5)     // Catch:{ all -> 0x008d }
            L_0x004a:
                com.vungle.warren.AdLoader r1 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x008d }
                java.util.Map r1 = r1.loadOperations     // Catch:{ all -> 0x008d }
                java.lang.Object r1 = r1.remove(r5)     // Catch:{ all -> 0x008d }
                com.vungle.warren.AdLoader$Operation r1 = (com.vungle.warren.AdLoader.Operation) r1     // Catch:{ all -> 0x008d }
                if (r1 == 0) goto L_0x008b
                com.vungle.warren.AdConfig$AdSize r2 = r1.size     // Catch:{ all -> 0x008d }
                r6.setAdSize(r2)     // Catch:{ all -> 0x008d }
                com.vungle.warren.AdLoader r2 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0067 }
                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ DBException -> 0x0067 }
                r2.save(r6)     // Catch:{ DBException -> 0x0067 }
                goto L_0x0075
            L_0x0067:
                com.vungle.warren.error.VungleException r6 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x008d }
                r2 = 26
                r6.<init>(r2)     // Catch:{ all -> 0x008d }
                java.lang.String r7 = r7.getId()     // Catch:{ all -> 0x008d }
                r4.onDownloadFailed(r6, r5, r7)     // Catch:{ all -> 0x008d }
            L_0x0075:
                java.util.Set<com.vungle.warren.LoadAdCallback> r6 = r1.loadAdCallbacks     // Catch:{ all -> 0x008d }
                java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x008d }
            L_0x007b:
                boolean r7 = r6.hasNext()     // Catch:{ all -> 0x008d }
                if (r7 == 0) goto L_0x008b
                java.lang.Object r7 = r6.next()     // Catch:{ all -> 0x008d }
                com.vungle.warren.LoadAdCallback r7 = (com.vungle.warren.LoadAdCallback) r7     // Catch:{ all -> 0x008d }
                r7.onAdLoad(r5)     // Catch:{ all -> 0x008d }
                goto L_0x007b
            L_0x008b:
                monitor-exit(r0)     // Catch:{ all -> 0x008d }
                return
            L_0x008d:
                r5 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x008d }
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.AdLoader.DownloadAdCallback.onReady(java.lang.String, com.vungle.warren.model.Placement, com.vungle.warren.model.Advertisement):void");
        }
    }

    interface DownloadCallback {
        void onDownloadCompleted(String str, String str2);

        void onDownloadFailed(VungleException vungleException, String str, String str2);

        void onReady(String str, Placement placement, Advertisement advertisement);
    }

    static class Operation {
        boolean cancelable;
        final long delay;
        long downloadStartTimeMillis;
        final String id;
        final Set<LoadAdCallback> loadAdCallbacks = new CopyOnWriteArraySet();
        final AtomicBoolean loading;
        final int policy;
        final int retry;
        final long retryDelay;
        final int retryLimit;
        final AdConfig.AdSize size;

        public Operation(String str, AdConfig.AdSize adSize, long j, long j2, int i, int i2, int i3, boolean z, LoadAdCallback... loadAdCallbackArr) {
            this.id = str;
            this.delay = j;
            this.retryDelay = j2;
            this.retryLimit = i;
            this.policy = i2;
            this.retry = i3;
            this.loading = new AtomicBoolean();
            this.cancelable = z;
            this.size = adSize == null ? AdConfig.AdSize.VUNGLE_DEFAULT : adSize;
            if (loadAdCallbackArr != null) {
                this.loadAdCallbacks.addAll(Arrays.asList(loadAdCallbackArr));
            }
        }

        /* access modifiers changed from: package-private */
        public Operation delay(long j) {
            return new Operation(this.id, this.size, j, this.retryDelay, this.retryLimit, this.policy, this.retry, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
        }

        /* access modifiers changed from: package-private */
        public Operation retry(int i) {
            return new Operation(this.id, this.size, this.delay, this.retryDelay, this.retryLimit, this.policy, i, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
        }

        /* access modifiers changed from: package-private */
        public Operation retryDelay(long j) {
            return new Operation(this.id, this.size, this.delay, j, this.retryLimit, this.policy, this.retry, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface ReschedulePolicy {
        public static final int EXPONENTIAL = 0;
        public static final int EXPONENTIAL_ENDLESS_AD = 1;
    }

    public AdLoader(Executors executors, Repository repository2, VungleApiClient vungleApiClient2, CacheManager cacheManager2, Downloader downloader2, RuntimeValues runtimeValues2, VungleStaticApi vungleStaticApi, VisionController visionController2) {
        this.sdkExecutors = executors;
        this.repository = repository2;
        this.vungleApiClient = vungleApiClient2;
        this.cacheManager = cacheManager2;
        this.downloader = downloader2;
        this.runtimeValues = runtimeValues2;
        this.vungleApi = vungleStaticApi;
        this.visionController = visionController2;
    }

    /* access modifiers changed from: private */
    public boolean canReDownload(Advertisement advertisement) {
        List<AdAsset> list;
        if (advertisement == null || ((advertisement.getState() != 0 && advertisement.getState() != 1) || (list = this.repository.loadAllAdAssets(advertisement.getId()).get()) == null || list.size() == 0)) {
            return false;
        }
        for (AdAsset adAsset : list) {
            if (adAsset.fileType == 1) {
                if (!fileIsValid(new File(adAsset.localPath), adAsset)) {
                    return false;
                }
            } else if (TextUtils.isEmpty(adAsset.serverPath)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void downloadAdAssets(com.vungle.warren.model.Advertisement r13, com.vungle.warren.AdLoader.DownloadCallback r14, java.lang.String r15) {
        /*
            r12 = this;
            java.util.Map r0 = r13.getDownloadableUrls()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x000c:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0062
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getKey()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x003c
            java.lang.Object r2 = r1.getValue()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x003c
            java.lang.Object r1 = r1.getValue()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = android.webkit.URLUtil.isValidUrl(r1)
            if (r1 != 0) goto L_0x000c
        L_0x003c:
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r1 = 11
            r0.<init>(r1)
            r1 = 0
            r14.onDownloadFailed(r0, r15, r1)
            java.lang.String r14 = TAG
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r0 = "Aborting, Failed to download Ad assets for: "
            r15.append(r0)
            java.lang.String r13 = r13.getId()
            r15.append(r13)
            java.lang.String r13 = r15.toString()
            android.util.Log.e(r14, r13)
            return
        L_0x0062:
            com.vungle.warren.DownloadCallbackWrapper r0 = new com.vungle.warren.DownloadCallbackWrapper
            com.vungle.warren.utility.Executors r1 = r12.sdkExecutors
            java.util.concurrent.ExecutorService r1 = r1.getUIExecutor()
            r0.<init>(r1, r14)
            java.util.concurrent.atomic.AtomicInteger r14 = new java.util.concurrent.atomic.AtomicInteger
            r14.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.vungle.warren.downloader.AssetDownloadListener r2 = r12.getAssetDownloadListener(r13, r15, r0, r14)
            com.vungle.warren.persistence.Repository r3 = r12.repository
            java.lang.String r4 = r13.getId()
            com.vungle.warren.persistence.Repository$FutureResult r3 = r3.loadAllAdAssets(r4)
            java.lang.Object r3 = r3.get()
            java.util.List r3 = (java.util.List) r3
            r4 = 26
            if (r3 != 0) goto L_0x009c
            com.vungle.warren.error.VungleException r14 = new com.vungle.warren.error.VungleException
            r14.<init>(r4)
            java.lang.String r13 = r13.getId()
            r0.onDownloadFailed(r14, r15, r13)
            return
        L_0x009c:
            java.util.Iterator r3 = r3.iterator()
        L_0x00a0:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x0142
            java.lang.Object r5 = r3.next()
            com.vungle.warren.model.AdAsset r5 = (com.vungle.warren.model.AdAsset) r5
            int r6 = r5.status
            r7 = 3
            r8 = 24
            r9 = 1
            if (r6 != r7) goto L_0x00d3
            java.io.File r6 = new java.io.File
            java.lang.String r7 = r5.localPath
            r6.<init>(r7)
            boolean r6 = r12.fileIsValid(r6, r5)
            if (r6 == 0) goto L_0x00c2
            goto L_0x00a0
        L_0x00c2:
            int r6 = r5.fileType
            if (r6 != r9) goto L_0x00d3
            com.vungle.warren.error.VungleException r14 = new com.vungle.warren.error.VungleException
            r14.<init>(r8)
            java.lang.String r13 = r13.getId()
            r0.onDownloadFailed(r14, r15, r13)
            return
        L_0x00d3:
            int r6 = r5.status
            r7 = 4
            if (r6 != r7) goto L_0x00dd
            int r6 = r5.fileType
            if (r6 != 0) goto L_0x00dd
            goto L_0x00a0
        L_0x00dd:
            java.lang.String r6 = r5.serverPath
            boolean r6 = android.text.TextUtils.isEmpty(r6)
            if (r6 == 0) goto L_0x00f2
            com.vungle.warren.error.VungleException r14 = new com.vungle.warren.error.VungleException
            r14.<init>(r8)
            java.lang.String r13 = r13.getId()
            r0.onDownloadFailed(r14, r15, r13)
            return
        L_0x00f2:
            com.vungle.warren.downloader.DownloadRequest r6 = new com.vungle.warren.downloader.DownloadRequest
            java.lang.String r7 = r5.serverPath
            java.lang.String r8 = r5.localPath
            java.lang.String r10 = r5.identifier
            r6.<init>(r7, r8, r10)
            int r7 = r5.status
            if (r7 != r9) goto L_0x0113
            com.vungle.warren.downloader.Downloader r7 = r12.downloader
            r10 = 1000(0x3e8, double:4.94E-321)
            r7.cancelAndAwait(r6, r10)
            com.vungle.warren.downloader.DownloadRequest r6 = new com.vungle.warren.downloader.DownloadRequest
            java.lang.String r7 = r5.serverPath
            java.lang.String r8 = r5.localPath
            java.lang.String r10 = r5.identifier
            r6.<init>(r7, r8, r10)
        L_0x0113:
            java.lang.String r7 = TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r10 = "Starting download for "
            r8.append(r10)
            r8.append(r5)
            java.lang.String r8 = r8.toString()
            android.util.Log.d(r7, r8)
            r5.status = r9
            com.vungle.warren.persistence.Repository r7 = r12.repository     // Catch:{ DBException -> 0x0135 }
            r7.save(r5)     // Catch:{ DBException -> 0x0135 }
            r1.add(r6)
            goto L_0x00a0
        L_0x0135:
            com.vungle.warren.error.VungleException r14 = new com.vungle.warren.error.VungleException
            r14.<init>(r4)
            java.lang.String r13 = r13.getId()
            r0.onDownloadFailed(r14, r15, r13)
            return
        L_0x0142:
            int r3 = r1.size()
            if (r3 != 0) goto L_0x014e
            java.util.List r14 = java.util.Collections.EMPTY_LIST
            r12.onAssetDownloadFinished(r15, r0, r13, r14)
            return
        L_0x014e:
            int r13 = r1.size()
            r14.set(r13)
            java.util.Iterator r13 = r1.iterator()
        L_0x0159:
            boolean r14 = r13.hasNext()
            if (r14 == 0) goto L_0x016b
            java.lang.Object r14 = r13.next()
            com.vungle.warren.downloader.DownloadRequest r14 = (com.vungle.warren.downloader.DownloadRequest) r14
            com.vungle.warren.downloader.Downloader r15 = r12.downloader
            r15.download(r14, r2)
            goto L_0x0159
        L_0x016b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.AdLoader.downloadAdAssets(com.vungle.warren.model.Advertisement, com.vungle.warren.AdLoader$DownloadCallback, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public void fetchAdMetadata(String str, AdConfig.AdSize adSize, DownloadCallback downloadCallback, HeaderBiddingCallback headerBiddingCallback) {
        boolean z = headerBiddingCallback != null;
        Operation operation = this.loadOperations.get(str);
        if (operation != null) {
            operation.downloadStartTimeMillis = System.currentTimeMillis();
        }
        final String str2 = str;
        final DownloadCallback downloadCallback2 = downloadCallback;
        final AdConfig.AdSize adSize2 = adSize;
        final HeaderBiddingCallback headerBiddingCallback2 = headerBiddingCallback;
        this.vungleApiClient.requestAd(str, AdConfig.AdSize.isBannerAdSize(adSize) ? adSize.getName() : "", z, this.visionController.isEnabled() ? this.visionController.getPayload() : null).enqueue(new Callback<JsonObject>() {
            public void onFailure(Call<JsonObject> call, Throwable th) {
                downloadCallback2.onDownloadFailed(AdLoader.this.retrofitToVungleException(th), str2, (String) null);
            }

            public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        int state;
                        Placement placement = AdLoader.this.repository.load(str2, Placement.class).get();
                        if (placement == null) {
                            Log.e(AdLoader.TAG, "Placement metadata not found for requested advertisement.");
                            downloadCallback2.onDownloadFailed(new VungleException(2), str2, (String) null);
                        } else if (!response.isSuccessful()) {
                            long retryAfterHeaderValue = AdLoader.this.vungleApiClient.getRetryAfterHeaderValue(response);
                            if (retryAfterHeaderValue <= 0 || !placement.isAutoCached()) {
                                Log.e(AdLoader.TAG, "Failed to retrieve advertisement information");
                                AnonymousClass2 r0 = AnonymousClass2.this;
                                downloadCallback2.onDownloadFailed(AdLoader.this.reposeCodeToVungleException(response.code()), str2, (String) null);
                                return;
                            }
                            AnonymousClass2 r2 = AnonymousClass2.this;
                            AdLoader.this.loadEndless(str2, adSize2, retryAfterHeaderValue);
                            downloadCallback2.onDownloadFailed(new VungleException(14), str2, (String) null);
                        } else {
                            JsonObject jsonObject = (JsonObject) response.body();
                            if (jsonObject == null || !jsonObject.d("ads") || jsonObject.a("ads").k()) {
                                downloadCallback2.onDownloadFailed(new VungleException(1), str2, (String) null);
                                return;
                            }
                            JsonArray b = jsonObject.b("ads");
                            if (b == null || b.size() == 0) {
                                downloadCallback2.onDownloadFailed(new VungleException(1), str2, (String) null);
                                return;
                            }
                            JsonObject f = b.get(0).f();
                            try {
                                Advertisement advertisement = new Advertisement(f);
                                if (AdLoader.this.visionController.isEnabled()) {
                                    JsonObject c = f.c("ad_markup");
                                    if (JsonUtil.hasNonNull(c, "data_science_cache")) {
                                        AdLoader.this.visionController.setDataScienceCache(c.a("data_science_cache").i());
                                    } else {
                                        AdLoader.this.visionController.setDataScienceCache((String) null);
                                    }
                                }
                                Advertisement advertisement2 = AdLoader.this.repository.load(advertisement.getId(), Advertisement.class).get();
                                if (advertisement2 == null || !((state = advertisement2.getState()) == 0 || state == 1 || state == 2)) {
                                    if (headerBiddingCallback2 != null) {
                                        headerBiddingCallback2.onBidTokenAvailable(str2, advertisement.getBidToken());
                                    }
                                    AdLoader.this.repository.deleteAdvertisement(advertisement.getId());
                                    Set<Map.Entry<String, String>> entrySet = advertisement.getDownloadableUrls().entrySet();
                                    File destinationDir = AdLoader.this.getDestinationDir(advertisement);
                                    if (destinationDir != null) {
                                        if (destinationDir.isDirectory()) {
                                            for (Map.Entry next : entrySet) {
                                                if (!URLUtil.isHttpsUrl((String) next.getValue())) {
                                                    if (!URLUtil.isHttpUrl((String) next.getValue())) {
                                                        downloadCallback2.onDownloadFailed(new VungleException(11), str2, advertisement.getId());
                                                        return;
                                                    }
                                                }
                                                AdLoader.this.saveAsset(advertisement, destinationDir, (String) next.getKey(), (String) next.getValue());
                                            }
                                            advertisement.getAdConfig().setAdSize(adSize2);
                                            AdLoader.this.repository.saveAndApplyState(advertisement, str2, 0);
                                            AdLoader.this.downloadAdAssets(advertisement, downloadCallback2, str2);
                                            return;
                                        }
                                    }
                                    downloadCallback2.onDownloadFailed(new VungleException(26), str2, advertisement.getId());
                                    return;
                                }
                                Log.d(AdLoader.TAG, "Operation Cancelled");
                                downloadCallback2.onDownloadFailed(new VungleException(25), str2, (String) null);
                            } catch (IllegalArgumentException unused) {
                                JsonObject c2 = f.c("ad_markup");
                                if (c2.d("sleep")) {
                                    int d = c2.a("sleep").d();
                                    placement.snooze((long) d);
                                    try {
                                        AdLoader.this.repository.save(placement);
                                        if (placement.isAutoCached()) {
                                            AnonymousClass2 r1 = AnonymousClass2.this;
                                            AdLoader.this.loadEndless(str2, adSize2, (long) (d * 1000));
                                        }
                                    } catch (DatabaseHelper.DBException unused2) {
                                        downloadCallback2.onDownloadFailed(new VungleException(26), str2, (String) null);
                                        return;
                                    }
                                }
                                downloadCallback2.onDownloadFailed(new VungleException(1), str2, (String) null);
                            } catch (DatabaseHelper.DBException unused3) {
                                downloadCallback2.onDownloadFailed(new VungleException(26), str2, (String) null);
                            }
                        }
                    }
                });
            }
        });
    }

    private boolean fileIsValid(File file, AdAsset adAsset) {
        return file.exists() && file.length() == adAsset.fileSize;
    }

    private AssetDownloadListener getAssetDownloadListener(Advertisement advertisement, String str, DownloadCallback downloadCallback, AtomicInteger atomicInteger) {
        final AtomicInteger atomicInteger2 = atomicInteger;
        final String str2 = str;
        final DownloadCallback downloadCallback2 = downloadCallback;
        final Advertisement advertisement2 = advertisement;
        return new AssetDownloadListener() {
            List<AssetDownloadListener.DownloadError> errors = Collections.synchronizedList(new ArrayList());

            public void onError(final AssetDownloadListener.DownloadError downloadError, final DownloadRequest downloadRequest) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        AdAsset adAsset;
                        Log.e(AdLoader.TAG, "Download Failed");
                        DownloadRequest downloadRequest = downloadRequest;
                        if (downloadRequest != null) {
                            String str = downloadRequest.cookieString;
                            if (TextUtils.isEmpty(str)) {
                                adAsset = null;
                            } else {
                                adAsset = AdLoader.this.repository.load(str, AdAsset.class).get();
                            }
                            if (adAsset != null) {
                                AnonymousClass3.this.errors.add(downloadError);
                                adAsset.status = 2;
                                try {
                                    AdLoader.this.repository.save(adAsset);
                                } catch (DatabaseHelper.DBException unused) {
                                    AnonymousClass3.this.errors.add(new AssetDownloadListener.DownloadError(-1, new VungleException(26), 4));
                                }
                            } else {
                                AnonymousClass3.this.errors.add(new AssetDownloadListener.DownloadError(-1, new IOException("Downloaded file not found!"), 1));
                            }
                        } else {
                            AnonymousClass3.this.errors.add(new AssetDownloadListener.DownloadError(-1, new RuntimeException("error in request"), 4));
                        }
                        if (atomicInteger2.decrementAndGet() <= 0) {
                            AnonymousClass3 r0 = AnonymousClass3.this;
                            AdLoader.this.onAssetDownloadFinished(str2, downloadCallback2, advertisement2, r0.errors);
                        }
                    }
                });
            }

            public void onProgress(AssetDownloadListener.Progress progress, DownloadRequest downloadRequest) {
            }

            public void onSuccess(final File file, final DownloadRequest downloadRequest) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        AdAsset adAsset;
                        if (!file.exists()) {
                            AnonymousClass3.this.onError(new AssetDownloadListener.DownloadError(-1, new IOException("Downloaded file not found!"), 3), downloadRequest);
                            return;
                        }
                        String str = downloadRequest.cookieString;
                        if (str == null) {
                            adAsset = null;
                        } else {
                            adAsset = AdLoader.this.repository.load(str, AdAsset.class).get();
                        }
                        if (adAsset == null) {
                            AnonymousClass3.this.onError(new AssetDownloadListener.DownloadError(-1, new IOException("Downloaded file not found!"), 1), downloadRequest);
                            return;
                        }
                        adAsset.fileType = AdLoader.this.isZip(file) ? 0 : 2;
                        adAsset.fileSize = file.length();
                        adAsset.status = 3;
                        try {
                            AdLoader.this.repository.save(adAsset);
                            if (atomicInteger2.decrementAndGet() <= 0) {
                                AnonymousClass3 r0 = AnonymousClass3.this;
                                AdLoader.this.onAssetDownloadFinished(str2, downloadCallback2, advertisement2, r0.errors);
                            }
                        } catch (DatabaseHelper.DBException unused) {
                            AnonymousClass3.this.onError(new AssetDownloadListener.DownloadError(-1, new VungleException(26), 4), downloadRequest);
                        }
                    }
                });
            }
        };
    }

    /* access modifiers changed from: private */
    public boolean isZip(File file) {
        return file.getName().equals(Advertisement.KEY_POSTROLL) || file.getName().equals(Advertisement.KEY_TEMPLATE);
    }

    private void loadAd(String str, DownloadCallbackWrapper downloadCallbackWrapper) {
        loadAd(str, (AdConfig.AdSize) null, downloadCallbackWrapper);
    }

    /* access modifiers changed from: private */
    public void onAssetDownloadFinished(String str, DownloadCallback downloadCallback, Advertisement advertisement, List<AssetDownloadListener.DownloadError> list) {
        if (list.isEmpty()) {
            List<AdAsset> list2 = this.repository.loadAllAdAssets(advertisement.getId()).get();
            if (list2 == null || list2.size() == 0) {
                downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                return;
            }
            for (AdAsset adAsset : list2) {
                int i = adAsset.status;
                if (i == 3) {
                    File file = new File(adAsset.localPath);
                    if (!fileIsValid(file, adAsset)) {
                        downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                        return;
                    } else if (adAsset.fileType == 0) {
                        try {
                            unzipFile(advertisement, adAsset, file, list2);
                        } catch (IOException unused) {
                            this.downloader.dropCache(adAsset.serverPath);
                            downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                            return;
                        } catch (DatabaseHelper.DBException unused2) {
                            downloadCallback.onDownloadFailed(new VungleException(26), str, advertisement.getId());
                            return;
                        }
                    }
                } else if (adAsset.fileType == 0 && i != 4) {
                    downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                    return;
                }
            }
            if (advertisement.getAdType() == 1) {
                File destinationDir = getDestinationDir(advertisement);
                if (destinationDir == null || !destinationDir.isDirectory()) {
                    downloadCallback.onDownloadFailed(new VungleException(26), str, advertisement.getId());
                    return;
                }
                String str2 = TAG;
                Log.d(str2, "saving MRAID for " + advertisement.getId());
                advertisement.setMraidAssetDir(destinationDir);
                try {
                    this.repository.save(advertisement);
                } catch (DatabaseHelper.DBException unused3) {
                    downloadCallback.onDownloadFailed(new VungleException(26), str, advertisement.getId());
                    return;
                }
            }
            downloadCallback.onDownloadCompleted(str, advertisement.getId());
            return;
        }
        VungleException vungleException = null;
        Iterator<AssetDownloadListener.DownloadError> it2 = list.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            AssetDownloadListener.DownloadError next = it2.next();
            if (VungleException.getExceptionCode(next.cause) != 26) {
                if (recoverableServerCode(next.serverCode) && next.reason == 1) {
                    vungleException = new VungleException(23);
                } else if (next.reason == 0) {
                    vungleException = new VungleException(23);
                } else {
                    vungleException = new VungleException(24);
                }
                if (vungleException.getExceptionCode() == 24) {
                    break;
                }
            } else {
                vungleException = new VungleException(26);
                break;
            }
        }
        downloadCallback.onDownloadFailed(vungleException, str, advertisement.getId());
    }

    private void onError(Operation operation, @VungleException.ExceptionCode int i) {
        if (operation != null) {
            for (LoadAdCallback onError : operation.loadAdCallbacks) {
                onError.onError(operation.id, new VungleException(i));
            }
        }
    }

    private boolean recoverableServerCode(int i) {
        return i == 408 || (500 <= i && i < 600);
    }

    /* access modifiers changed from: private */
    public VungleException reposeCodeToVungleException(int i) {
        if (recoverableServerCode(i)) {
            return new VungleException(22);
        }
        return new VungleException(21);
    }

    /* access modifiers changed from: private */
    public VungleException retrofitToVungleException(Throwable th) {
        if (th instanceof UnknownHostException) {
            return new VungleException(11);
        }
        if (th instanceof IOException) {
            return new VungleException(20);
        }
        return new VungleException(11);
    }

    /* access modifiers changed from: private */
    public void setLoading(String str, boolean z) {
        Operation operation = this.loadOperations.get(str);
        if (operation != null) {
            operation.loading.set(z);
        }
    }

    private void unzipFile(Advertisement advertisement, AdAsset adAsset, final File file, List<AdAsset> list) throws IOException, DatabaseHelper.DBException {
        final ArrayList arrayList = new ArrayList();
        for (AdAsset next : list) {
            if (next.fileType == 2) {
                arrayList.add(next.localPath);
            }
        }
        File destinationDir = getDestinationDir(advertisement);
        if (destinationDir == null || !destinationDir.isDirectory()) {
            throw new IOException("Unable to access Destination Directory");
        }
        List<File> unzip = UnzipUtility.unzip(file.getPath(), destinationDir.getPath(), new UnzipUtility.Filter() {
            public boolean matches(String str) {
                File file = new File(str);
                for (String file2 : arrayList) {
                    File file3 = new File(file2);
                    if (file3.equals(file)) {
                        return false;
                    }
                    String path = file.getPath();
                    if (path.startsWith(file3.getPath() + File.separator)) {
                        return false;
                    }
                }
                return true;
            }
        });
        if (file.getName().equals(Advertisement.KEY_TEMPLATE)) {
            File file2 = new File(destinationDir.getPath() + File.separator + "mraid.js");
            if (file2.exists()) {
                PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file2, true)));
                HackMraid.apply(printWriter);
                printWriter.close();
            }
        }
        for (File next2 : unzip) {
            AdAsset adAsset2 = new AdAsset(advertisement.getId(), (String) null, next2.getPath());
            adAsset2.fileSize = next2.length();
            adAsset2.fileType = 1;
            adAsset2.parentId = adAsset.identifier;
            adAsset2.status = 3;
            this.repository.save(adAsset2);
        }
        String str = TAG;
        Log.d(str, "Uzipped " + destinationDir);
        FileUtility.printDirectoryTree(destinationDir);
        adAsset.status = 4;
        this.repository.save(adAsset, new Repository.SaveCallback() {
            public void onError(Exception exc) {
            }

            public void onSaved() {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        try {
                            FileUtility.delete(file);
                        } catch (IOException e) {
                            Log.e(AdLoader.TAG, "Error on deleting zip assets archive", e);
                        }
                    }
                });
            }
        });
    }

    public boolean canPlayAd(Advertisement advertisement) {
        return advertisement != null && advertisement.getState() == 1 && hasAssetsFor(advertisement.getId());
    }

    public boolean canRenderAd(Advertisement advertisement) {
        if (advertisement == null) {
            return false;
        }
        if ((advertisement.getState() == 1 || advertisement.getState() == 2) && hasAssetsFor(advertisement.getId())) {
            return true;
        }
        return false;
    }

    public synchronized void clear() {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.addAll(this.loadOperations.keySet());
        hashSet.addAll(this.pendingOperations.keySet());
        for (String str : hashSet) {
            onError(this.loadOperations.remove(str), 25);
            onError(this.pendingOperations.remove(str), 25);
        }
    }

    public void dropCache(String str) {
        List<AdAsset> list = this.repository.loadAllAdAssets(str).get();
        if (list == null) {
            Log.w(TAG, "No assets found in ad cache to cleanup");
            return;
        }
        for (AdAsset adAsset : list) {
            this.downloader.dropCache(adAsset.serverPath);
        }
    }

    /* access modifiers changed from: package-private */
    public File getDestinationDir(Advertisement advertisement) {
        return this.repository.getAdvertisementAssetDirectory(advertisement.getId()).get();
    }

    /* access modifiers changed from: package-private */
    public Collection<Operation> getPendingOperations() {
        return this.pendingOperations.values();
    }

    /* access modifiers changed from: package-private */
    public Collection<Operation> getRunningOperations() {
        return this.loadOperations.values();
    }

    public boolean hasAssetsFor(String str) throws IllegalStateException {
        List<AdAsset> list = this.repository.loadAllAdAssets(str).get();
        if (list == null || list.size() == 0) {
            return false;
        }
        for (AdAsset adAsset : list) {
            if (adAsset.fileType == 0) {
                if (adAsset.status != 4) {
                    return false;
                }
            } else if (adAsset.status != 3 || !fileIsValid(new File(adAsset.localPath), adAsset)) {
                return false;
            }
        }
        return true;
    }

    public synchronized void init(JobRunner jobRunner2) {
        this.jobRunner = jobRunner2;
        this.downloader.init();
    }

    public boolean isLoading(String str) {
        Operation operation = this.loadOperations.get(str);
        return operation != null && operation.loading.get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void load(com.vungle.warren.AdLoader.Operation r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            com.vungle.warren.tasks.JobRunner r0 = r6.jobRunner     // Catch:{ all -> 0x0086 }
            if (r0 != 0) goto L_0x000c
            r0 = 9
            r6.onError(r7, r0)     // Catch:{ all -> 0x0086 }
            monitor-exit(r6)
            return
        L_0x000c:
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r0 = r6.pendingOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0086 }
            java.lang.Object r0 = r0.remove(r1)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.AdLoader$Operation r0 = (com.vungle.warren.AdLoader.Operation) r0     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x001f
            java.util.Set<com.vungle.warren.LoadAdCallback> r1 = r7.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            java.util.Set<com.vungle.warren.LoadAdCallback> r0 = r0.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            r1.addAll(r0)     // Catch:{ all -> 0x0086 }
        L_0x001f:
            long r0 = r7.delay     // Catch:{ all -> 0x0086 }
            r2 = 0
            r4 = 1
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 > 0) goto L_0x0068
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r0 = r6.loadOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0086 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.AdLoader$Operation r0 = (com.vungle.warren.AdLoader.Operation) r0     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x0048
            java.util.Set<com.vungle.warren.LoadAdCallback> r1 = r7.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            java.util.Set<com.vungle.warren.LoadAdCallback> r0 = r0.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            r1.addAll(r0)     // Catch:{ all -> 0x0086 }
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r0 = r6.loadOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0086 }
            r0.put(r1, r7)     // Catch:{ all -> 0x0086 }
            java.lang.String r7 = r7.id     // Catch:{ all -> 0x0086 }
            r6.setLoading(r7, r4)     // Catch:{ all -> 0x0086 }
            goto L_0x0084
        L_0x0048:
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r0 = r6.loadOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0086 }
            r0.put(r1, r7)     // Catch:{ all -> 0x0086 }
            java.lang.String r0 = r7.id     // Catch:{ all -> 0x0086 }
            com.vungle.warren.AdConfig$AdSize r7 = r7.size     // Catch:{ all -> 0x0086 }
            com.vungle.warren.DownloadCallbackWrapper r1 = new com.vungle.warren.DownloadCallbackWrapper     // Catch:{ all -> 0x0086 }
            com.vungle.warren.utility.Executors r2 = r6.sdkExecutors     // Catch:{ all -> 0x0086 }
            java.util.concurrent.ExecutorService r2 = r2.getVungleExecutor()     // Catch:{ all -> 0x0086 }
            com.vungle.warren.AdLoader$DownloadAdCallback r3 = new com.vungle.warren.AdLoader$DownloadAdCallback     // Catch:{ all -> 0x0086 }
            r4 = 0
            r3.<init>()     // Catch:{ all -> 0x0086 }
            r1.<init>(r2, r3)     // Catch:{ all -> 0x0086 }
            r6.loadAd(r0, r7, r1)     // Catch:{ all -> 0x0086 }
            goto L_0x0084
        L_0x0068:
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r0 = r6.pendingOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0086 }
            r0.put(r1, r7)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobRunner r0 = r6.jobRunner     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobInfo r1 = com.vungle.warren.tasks.DownloadJob.makeJobInfo(r1)     // Catch:{ all -> 0x0086 }
            long r2 = r7.delay     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobInfo r7 = r1.setDelay(r2)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobInfo r7 = r7.setUpdateCurrent(r4)     // Catch:{ all -> 0x0086 }
            r0.execute(r7)     // Catch:{ all -> 0x0086 }
        L_0x0084:
            monitor-exit(r6)
            return
        L_0x0086:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.AdLoader.load(com.vungle.warren.AdLoader$Operation):void");
    }

    public void loadEndless(String str, AdConfig.AdSize adSize, long j) {
        load(new Operation(str, adSize, j, RETRY_DELAY, 5, 1, 0, true, new LoadAdCallback[0]));
    }

    public void loadEndlessWithCallback(String str, AdConfig.AdSize adSize, long j, LoadAdCallback loadAdCallback) {
        load(new Operation(str, adSize, j, RETRY_DELAY, 5, 1, 0, true, loadAdCallback));
    }

    public synchronized void loadPendingInternal(String str) {
        Operation remove = this.pendingOperations.remove(str);
        if (remove != null) {
            load(remove.delay(0));
        }
    }

    /* access modifiers changed from: package-private */
    public void saveAsset(Advertisement advertisement, File file, String str, String str2) throws DatabaseHelper.DBException {
        String str3 = file.getPath() + File.separator + str;
        int i = (str3.endsWith(Advertisement.KEY_POSTROLL) || str3.endsWith(Advertisement.KEY_TEMPLATE)) ? 0 : 2;
        AdAsset adAsset = new AdAsset(advertisement.getId(), str2, str3);
        adAsset.status = 0;
        adAsset.fileType = i;
        this.repository.save(adAsset);
    }

    private void loadAd(final String str, final AdConfig.AdSize adSize, final DownloadCallbackWrapper downloadCallbackWrapper) {
        this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
            public void run() {
                if (!AdLoader.this.vungleApi.isInitialized()) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(9), str, (String) null);
                    return;
                }
                Placement placement = AdLoader.this.repository.load(str, Placement.class).get();
                if (placement == null) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(13), str, (String) null);
                } else if (placement.getPlacementAdType() == 1 && !AdConfig.AdSize.isBannerAdSize(adSize)) {
                    DownloadCallbackWrapper downloadCallbackWrapper = downloadCallbackWrapper;
                    if (downloadCallbackWrapper != null) {
                        downloadCallbackWrapper.onDownloadFailed(new VungleException(28), str, (String) null);
                    }
                } else if (placement.getPlacementAdType() != 0 || !AdConfig.AdSize.isBannerAdSize(adSize)) {
                    Advertisement advertisement = AdLoader.this.repository.findValidAdvertisementForPlacement(placement.getId()).get();
                    if (!(placement.getPlacementAdType() != 1 || advertisement == null || advertisement.getAdConfig().getAdSize() == adSize)) {
                        try {
                            AdLoader.this.repository.deleteAdvertisement(advertisement.getId());
                        } catch (DatabaseHelper.DBException unused) {
                            downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, (String) null);
                            return;
                        }
                    }
                    if (AdLoader.this.canPlayAd(advertisement)) {
                        downloadCallbackWrapper.onReady(str, placement, advertisement);
                    } else if (AdLoader.this.canReDownload(advertisement)) {
                        Log.d(AdLoader.TAG, "Found valid adv but not ready - downloading content");
                        VungleSettings vungleSettings = AdLoader.this.runtimeValues.settings;
                        if (vungleSettings == null || AdLoader.this.cacheManager.getBytesAvailable() < vungleSettings.getMinimumSpaceForAd()) {
                            if (advertisement.getState() != 4) {
                                try {
                                    AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                                } catch (DatabaseHelper.DBException unused2) {
                                    downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, (String) null);
                                    return;
                                }
                            }
                            downloadCallbackWrapper.onDownloadFailed(new VungleException(19), str, (String) null);
                            return;
                        }
                        AdLoader.this.setLoading(str, true);
                        if (advertisement.getState() != 0) {
                            try {
                                AdLoader.this.repository.saveAndApplyState(advertisement, str, 0);
                            } catch (DatabaseHelper.DBException unused3) {
                                downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, (String) null);
                                return;
                            }
                        }
                        AdLoader.this.downloadAdAssets(advertisement, downloadCallbackWrapper, str);
                    } else if (placement.getWakeupTime() > System.currentTimeMillis()) {
                        downloadCallbackWrapper.onDownloadFailed(new VungleException(1), str, (String) null);
                        String access$400 = AdLoader.TAG;
                        Log.w(access$400, "Placement " + placement.getId() + " is  snoozed");
                        if (placement.isAutoCached()) {
                            String access$4002 = AdLoader.TAG;
                            Log.d(access$4002, "Placement " + placement.getId() + " is sleeping rescheduling it ");
                            AdLoader.this.loadEndless(placement.getId(), adSize, placement.getWakeupTime() - System.currentTimeMillis());
                        }
                    } else {
                        String access$4003 = AdLoader.TAG;
                        Log.i(access$4003, "didn't find cached adv for " + str + " downloading ");
                        if (advertisement != null) {
                            try {
                                AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                            } catch (DatabaseHelper.DBException unused4) {
                                downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, (String) null);
                                return;
                            }
                        }
                        VungleSettings vungleSettings2 = AdLoader.this.runtimeValues.settings;
                        if (vungleSettings2 == null || AdLoader.this.cacheManager.getBytesAvailable() >= vungleSettings2.getMinimumSpaceForAd()) {
                            String access$4004 = AdLoader.TAG;
                            Log.d(access$4004, "No adv for placement " + placement.getId() + " getting new data ");
                            AdLoader.this.setLoading(str, true);
                            AdLoader adLoader = AdLoader.this;
                            adLoader.fetchAdMetadata(str, adSize, downloadCallbackWrapper, adLoader.runtimeValues.headerBiddingCallback);
                            return;
                        }
                        downloadCallbackWrapper.onDownloadFailed(new VungleException(placement.isAutoCached() ? 18 : 17), str, (String) null);
                    }
                } else {
                    DownloadCallbackWrapper downloadCallbackWrapper2 = downloadCallbackWrapper;
                    if (downloadCallbackWrapper2 != null) {
                        downloadCallbackWrapper2.onDownloadFailed(new VungleException(28), str, (String) null);
                    }
                }
            }
        });
    }

    public void load(String str, LoadAdCallback loadAdCallback) {
        load(new Operation(str, AdConfig.AdSize.VUNGLE_DEFAULT, 0, RETRY_DELAY, 5, 0, 0, false, loadAdCallback));
    }

    public void load(String str, AdConfig adConfig, LoadAdCallback loadAdCallback) {
        load(new Operation(str, adConfig.getAdSize(), 0, RETRY_DELAY, 5, 0, 0, false, loadAdCallback));
    }
}
