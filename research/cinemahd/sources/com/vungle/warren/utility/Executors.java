package com.vungle.warren.utility;

import java.util.concurrent.ExecutorService;

public interface Executors {
    ExecutorService getIOExecutor();

    ExecutorService getJobExecutor();

    ExecutorService getUIExecutor();

    ExecutorService getVungleExecutor();
}
