package com.vungle.warren.utility;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

public class ExternalRouter {
    public static final String TAG = "ExternalRouter";

    public static boolean launch(String str, Context context) {
        if (!TextUtils.isEmpty(str) && context != null) {
            try {
                Intent parseUri = Intent.parseUri(str, 0);
                parseUri.setFlags(268435456);
                if (parseUri.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(parseUri);
                    return true;
                }
            } catch (Exception e) {
                String str2 = TAG;
                Log.e(str2, "Error while opening url" + e.getLocalizedMessage());
            }
            String str3 = TAG;
            Log.d(str3, "Cannot open url " + str + str);
        }
        return false;
    }
}
