package com.vungle.warren.utility;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class FileUtility {
    private static final String TAG = "FileUtility";

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    public static void delete(File file) throws IOException {
        if (file != null && file.exists()) {
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    for (File delete : listFiles) {
                        delete(delete);
                    }
                } else {
                    return;
                }
            }
            if (!file.delete()) {
                throw new FileNotFoundException("Failed to delete file: " + file);
            }
        }
    }

    public static byte[] extractBytes(File file) throws IOException {
        if (file == null) {
            return new byte[0];
        }
        int length = (int) file.length();
        if (length > 0) {
            byte[] bArr = new byte[length];
            BufferedInputStream bufferedInputStream = null;
            try {
                BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(file));
                try {
                    if (bufferedInputStream2.read(bArr) >= length) {
                        closeQuietly(bufferedInputStream2);
                        return bArr;
                    }
                    throw new IOException("Failed to read all bytes in the file, object recreation will fail");
                } catch (FileNotFoundException e) {
                    e = e;
                    bufferedInputStream = bufferedInputStream2;
                    try {
                        Log.e(TAG, "FileNotFoundException ", e);
                        closeQuietly(bufferedInputStream);
                        return new byte[0];
                    } catch (Throwable th) {
                        th = th;
                        closeQuietly(bufferedInputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedInputStream = bufferedInputStream2;
                    closeQuietly(bufferedInputStream);
                    throw th;
                }
            } catch (FileNotFoundException e2) {
                e = e2;
                Log.e(TAG, "FileNotFoundException ", e);
                closeQuietly(bufferedInputStream);
                return new byte[0];
            }
        }
        return new byte[0];
    }

    private static String getIndentString(int i) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("|  ");
        }
        return sb.toString();
    }

    public static void printDirectoryTree(File file) {
    }

    private static void printDirectoryTree(File file, int i, StringBuilder sb) {
        if (file != null) {
            if (file.isDirectory()) {
                sb.append(getIndentString(i));
                sb.append("+--");
                sb.append(file.getName());
                sb.append("/\n");
                if (file.listFiles() != null) {
                    for (File file2 : file.listFiles()) {
                        if (file2.isDirectory()) {
                            printDirectoryTree(file2, i + 1, sb);
                        } else {
                            printFile(file2, i + 1, sb);
                        }
                    }
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("folder is not a Directory");
        }
    }

    private static void printFile(File file, int i, StringBuilder sb) {
        sb.append(getIndentString(i));
        sb.append("+--");
        sb.append(file.getName());
        sb.append(10);
    }

    public static ArrayList<String> readAllLines(String str) {
        File file = new File(str);
        if (file.exists()) {
            Object readSerializable = readSerializable(file);
            if (readSerializable instanceof ArrayList) {
                return (ArrayList) readSerializable;
            }
        }
        return new ArrayList<>();
    }

    public static HashMap<String, String> readMap(String str) {
        Object readSerializable = readSerializable(new File(str));
        if (readSerializable instanceof HashMap) {
            return (HashMap) readSerializable;
        }
        return new HashMap<>();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: java.io.ObjectInputStream} */
    /* JADX WARNING: type inference failed for: r0v1, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r0v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> T readSerializable(java.io.File r4) {
        /*
            boolean r0 = r4.exists()
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            java.io.ObjectInputStream r0 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x002b, ClassNotFoundException -> 0x0021, all -> 0x001e }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x002b, ClassNotFoundException -> 0x0021, all -> 0x001e }
            r2.<init>(r4)     // Catch:{ IOException -> 0x002b, ClassNotFoundException -> 0x0021, all -> 0x001e }
            r0.<init>(r2)     // Catch:{ IOException -> 0x002b, ClassNotFoundException -> 0x0021, all -> 0x001e }
            java.lang.Object r4 = r0.readObject()     // Catch:{ IOException -> 0x001c, ClassNotFoundException -> 0x001a }
            closeQuietly(r0)
            return r4
        L_0x001a:
            r4 = move-exception
            goto L_0x0023
        L_0x001c:
            r4 = move-exception
            goto L_0x002d
        L_0x001e:
            r4 = move-exception
            r0 = r1
            goto L_0x0039
        L_0x0021:
            r4 = move-exception
            r0 = r1
        L_0x0023:
            java.lang.String r2 = TAG     // Catch:{ all -> 0x0038 }
            java.lang.String r3 = "ClassNotFoundException"
            android.util.Log.e(r2, r3, r4)     // Catch:{ all -> 0x0038 }
            goto L_0x0034
        L_0x002b:
            r4 = move-exception
            r0 = r1
        L_0x002d:
            java.lang.String r2 = TAG     // Catch:{ all -> 0x0038 }
            java.lang.String r3 = "IOIOException"
            android.util.Log.e(r2, r3, r4)     // Catch:{ all -> 0x0038 }
        L_0x0034:
            closeQuietly(r0)
            return r1
        L_0x0038:
            r4 = move-exception
        L_0x0039:
            closeQuietly(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.utility.FileUtility.readSerializable(java.io.File):java.lang.Object");
    }

    public static long size(File file) {
        long j = 0;
        if (file == null || !file.exists()) {
            return 0;
        }
        if (!file.isDirectory()) {
            return file.length();
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (File size : listFiles) {
                j += size(size);
            }
        }
        return j;
    }

    public static void writeAllLines(String str, ArrayList<String> arrayList) {
        File file = new File(str);
        if (file.exists()) {
            file.delete();
        }
        if (!arrayList.isEmpty()) {
            writeSerializable(file, arrayList);
        }
    }

    public static void writeMap(String str, HashMap<String, String> hashMap) {
        File file = new File(str);
        if (!hashMap.isEmpty()) {
            writeSerializable(file, hashMap);
        }
    }

    public static void writeSerializable(File file, Serializable serializable) {
        if (file.exists()) {
            file.delete();
        }
        if (serializable != null) {
            ObjectOutputStream objectOutputStream = null;
            try {
                ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(new FileOutputStream(file));
                try {
                    objectOutputStream2.writeObject(serializable);
                    closeQuietly(objectOutputStream2);
                } catch (IOException e) {
                    e = e;
                    objectOutputStream = objectOutputStream2;
                    try {
                        Log.e(TAG, "IOIOException", e);
                        closeQuietly(objectOutputStream);
                    } catch (Throwable th) {
                        th = th;
                        closeQuietly(objectOutputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    objectOutputStream = objectOutputStream2;
                    closeQuietly(objectOutputStream);
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                Log.e(TAG, "IOIOException", e);
                closeQuietly(objectOutputStream);
            }
        }
    }
}
