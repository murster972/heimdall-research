package com.vungle.warren;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.vungle.warren.AdConfig;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.Repository;

public final class Banners {
    private static final String TAG = "Banners";

    public static boolean canPlayAd(String str, AdConfig.AdSize adSize) {
        Class cls = Repository.class;
        Context appContext = Vungle.appContext();
        if (appContext == null || !Vungle.isInitialized()) {
            Log.e(TAG, "Vungle is not initialized");
            return false;
        } else if (!AdConfig.AdSize.isBannerAdSize(adSize)) {
            Log.e(TAG, "Invalid Ad Size. Cannot check loaded status of non banner size placements.");
            return false;
        } else {
            Advertisement advertisement = ((Repository) ServiceLocator.getInstance(appContext).getService(cls)).findValidAdvertisementForPlacement(str).get();
            Placement placement = ((Repository) ServiceLocator.getInstance(appContext).getService(cls)).load(str, Placement.class).get();
            if (placement != null && adSize == placement.getAdSize() && advertisement != null && advertisement.getAdConfig().getAdSize().equals(adSize)) {
                return Vungle.canPlayAd(advertisement);
            }
            return false;
        }
    }

    public static VungleBanner getBanner(String str, AdConfig.AdSize adSize, PlayAdCallback playAdCallback) {
        Context appContext = Vungle.appContext();
        if (appContext == null || !Vungle.isInitialized()) {
            Log.e(TAG, "Vungle is not initialized, returned VungleNativeAd = null");
            onPlaybackError(str, playAdCallback, 9);
            return null;
        }
        Repository repository = (Repository) ServiceLocator.getInstance(appContext).getService(Repository.class);
        VungleSettings vungleSettings = ((RuntimeValues) ServiceLocator.getInstance(appContext).getService(RuntimeValues.class)).settings;
        if (TextUtils.isEmpty(str)) {
            onPlaybackError(str, playAdCallback, 13);
            return null;
        }
        Placement placement = repository.load(str, Placement.class).get();
        if (placement == null) {
            onPlaybackError(str, playAdCallback, 13);
            return null;
        } else if (!AdConfig.AdSize.isBannerAdSize(adSize)) {
            onPlaybackError(str, playAdCallback, 30);
            return null;
        } else if (!canPlayAd(str, adSize)) {
            onPlaybackError(str, playAdCallback, 10);
            return null;
        } else if (vungleSettings != null && vungleSettings.getBannerRefreshDisabled()) {
            return new VungleBanner(appContext, str, 0, adSize, playAdCallback);
        } else {
            return new VungleBanner(appContext, str, placement.getAdRefreshDuration(), adSize, playAdCallback);
        }
    }

    public static void loadBanner(final String str, AdConfig.AdSize adSize, final LoadAdCallback loadAdCallback) {
        Context appContext = Vungle.appContext();
        if (appContext == null || !Vungle.isInitialized()) {
            onLoadError(str, loadAdCallback, 9);
        } else if (adSize == null) {
            onLoadError(str, loadAdCallback, 28);
        } else {
            final AdConfig adConfig = new AdConfig();
            adConfig.setAdSize(adSize);
            ((Repository) ServiceLocator.getInstance(appContext).getService(Repository.class)).load(str, Placement.class, new Repository.LoadCallback<Placement>() {
                public void onLoaded(Placement placement) {
                    if (placement == null) {
                        Banners.onLoadError(str, loadAdCallback, 10);
                    } else if (!AdConfig.AdSize.isBannerAdSize(adConfig.getAdSize())) {
                        Banners.onLoadError(str, loadAdCallback, 30);
                    } else {
                        Vungle.loadAdInternal(str, adConfig, loadAdCallback);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void onLoadError(String str, LoadAdCallback loadAdCallback, @VungleException.ExceptionCode int i) {
        if (loadAdCallback != null) {
            loadAdCallback.onError(str, new VungleException(i));
        }
    }

    private static void onPlaybackError(String str, PlayAdCallback playAdCallback, @VungleException.ExceptionCode int i) {
        if (playAdCallback != null) {
            playAdCallback.onError(str, new VungleException(i));
        }
    }
}
