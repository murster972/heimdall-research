package com.vungle.warren.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonUtil {
    public static boolean hasNonNull(JsonElement jsonElement, String str) {
        if (jsonElement == null || jsonElement.k() || !jsonElement.l()) {
            return false;
        }
        JsonObject f = jsonElement.f();
        if (!f.d(str) || f.a(str) == null || f.a(str).k()) {
            return false;
        }
        return true;
    }
}
