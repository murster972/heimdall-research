package com.vungle.warren.model;

import android.text.TextUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.vungle.warren.AdConfig;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.ui.JavascriptBridge;
import java.util.ArrayList;
import java.util.List;

public class Report {
    public static final int FAILED = 3;
    public static final int NEW = 0;
    public static final int READY = 1;
    public static final int SENDING = 2;
    long adDuration;
    String adSize;
    long adStartTime;
    String adToken;
    String adType;
    String advertisementID;
    String appId;
    String campaign;
    final List<String> clickedThrough;
    final List<String> errors;
    boolean incentivized;
    int ordinal;
    String placementId;
    int status;
    String templateId;
    long ttDownload;
    String url;
    final List<UserAction> userActions;
    String userID;
    long videoLength;
    int videoViewed;
    volatile boolean wasCTAClicked;

    public @interface Status {
    }

    public static class UserAction {
        @SerializedName("action")
        private String action;
        @SerializedName("timestamp")
        private long timestamp;
        @SerializedName("value")
        private String value;

        public UserAction(String str, String str2, long j) {
            this.action = str;
            this.value = str2;
            this.timestamp = j;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || UserAction.class != obj.getClass()) {
                return false;
            }
            UserAction userAction = (UserAction) obj;
            return userAction.action.equals(this.action) && userAction.value.equals(this.value) && userAction.timestamp == this.timestamp;
        }

        public int hashCode() {
            long j = this.timestamp;
            return (((this.action.hashCode() * 31) + this.value.hashCode()) * 31) + ((int) (j ^ (j >>> 32)));
        }

        public JsonObject toJson() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.a("action", this.action);
            String str = this.value;
            if (str != null && !str.isEmpty()) {
                jsonObject.a("value", this.value);
            }
            jsonObject.a("timestamp_millis", (Number) Long.valueOf(this.timestamp));
            return jsonObject;
        }
    }

    Report() {
        this.status = 0;
        this.userActions = new ArrayList();
        this.clickedThrough = new ArrayList();
        this.errors = new ArrayList();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Report.class != obj.getClass()) {
            return false;
        }
        Report report = (Report) obj;
        if (!report.placementId.equals(this.placementId) || !report.adToken.equals(this.adToken) || !report.appId.equals(this.appId) || report.incentivized != this.incentivized || report.adStartTime != this.adStartTime || !report.url.equals(this.url) || report.videoLength != this.videoLength || report.adDuration != this.adDuration || report.ttDownload != this.ttDownload || !report.campaign.equals(this.campaign) || !report.adType.equals(this.adType) || !report.templateId.equals(this.templateId) || report.wasCTAClicked != this.wasCTAClicked || !report.userID.equals(this.userID) || report.clickedThrough.size() != this.clickedThrough.size()) {
            return false;
        }
        for (int i = 0; i < this.clickedThrough.size(); i++) {
            if (!report.clickedThrough.get(i).equals(this.clickedThrough.get(i))) {
                return false;
            }
        }
        if (report.errors.size() != this.errors.size()) {
            return false;
        }
        for (int i2 = 0; i2 < this.errors.size(); i2++) {
            if (!report.errors.get(i2).equals(this.errors.get(i2))) {
                return false;
            }
        }
        if (report.userActions.size() != this.userActions.size()) {
            return false;
        }
        for (int i3 = 0; i3 < this.userActions.size(); i3++) {
            if (!report.userActions.get(i3).equals(this.userActions.get(i3))) {
                return false;
            }
        }
        return true;
    }

    public long getAdStartTime() {
        return this.adStartTime;
    }

    public String getAdvertisementID() {
        return this.advertisementID;
    }

    public String getId() {
        return this.placementId + "_" + this.adStartTime;
    }

    public String getPlacementId() {
        return this.placementId;
    }

    @Status
    public int getStatus() {
        return this.status;
    }

    public String getUserID() {
        return this.userID;
    }

    public int hashCode() {
        long j = this.adStartTime;
        long j2 = this.videoLength;
        long j3 = this.adDuration;
        long j4 = this.ttDownload;
        return (((((((((((((((((((((((((((((((this.placementId.hashCode() * 31) + this.adToken.hashCode()) * 31) + this.appId.hashCode()) * 31) + (this.incentivized ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.url.hashCode()) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + this.campaign.hashCode()) * 31) + this.userActions.hashCode()) * 31) + this.clickedThrough.hashCode()) * 31) + this.errors.hashCode()) * 31) + this.adType.hashCode()) * 31) + this.templateId.hashCode()) * 31) + this.userID.hashCode()) * 31) + (this.wasCTAClicked ? 1 : 0);
    }

    public boolean isCTAClicked() {
        return this.wasCTAClicked;
    }

    public synchronized void recordAction(String str, String str2, long j) {
        this.userActions.add(new UserAction(str, str2, j));
        this.clickedThrough.add(str);
        if (str.equals(JavascriptBridge.MraidHandler.DOWNLOAD_ACTION)) {
            this.wasCTAClicked = true;
        }
    }

    public synchronized void recordError(String str) {
        this.errors.add(str);
    }

    public void recordProgress(int i) {
        this.videoViewed = i;
    }

    public void setAdDuration(long j) {
        this.adDuration = j;
    }

    public void setStatus(@Status int i) {
        this.status = i;
    }

    public void setTtDownload(long j) {
        this.ttDownload = j;
    }

    public void setVideoLength(long j) {
        this.videoLength = j;
    }

    public JsonObject toReportBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.a("placement_reference_id", this.placementId);
        jsonObject.a(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_AD_TOKEN, this.adToken);
        jsonObject.a(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_APP_ID, this.appId);
        jsonObject.a("incentivized", (Number) Integer.valueOf(this.incentivized ? 1 : 0));
        jsonObject.a(ReportDBAdapter.ReportColumns.COLUMN_AD_START_TIME, (Number) Long.valueOf(this.adStartTime));
        if (!TextUtils.isEmpty(this.url)) {
            jsonObject.a(ReportDBAdapter.ReportColumns.COLUMN_URL, this.url);
        }
        jsonObject.a("adDuration", (Number) Long.valueOf(this.adDuration));
        jsonObject.a("ttDownload", (Number) Long.valueOf(this.ttDownload));
        jsonObject.a("campaign", this.campaign);
        jsonObject.a("adType", this.adType);
        jsonObject.a("templateId", this.templateId);
        if (!TextUtils.isEmpty(this.adSize)) {
            jsonObject.a("ad_size", this.adSize);
        }
        JsonArray jsonArray = new JsonArray();
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.a("startTime", (Number) Long.valueOf(this.adStartTime));
        int i = this.videoViewed;
        if (i > 0) {
            jsonObject2.a(ReportDBAdapter.ReportColumns.COLUMN_VIDEO_VIEWED, (Number) Integer.valueOf(i));
        }
        long j = this.videoLength;
        if (j > 0) {
            jsonObject2.a("videoLength", (Number) Long.valueOf(j));
        }
        JsonArray jsonArray2 = new JsonArray();
        for (UserAction json : this.userActions) {
            jsonArray2.a((JsonElement) json.toJson());
        }
        jsonObject2.a("userActions", (JsonElement) jsonArray2);
        jsonArray.a((JsonElement) jsonObject2);
        jsonObject.a("plays", (JsonElement) jsonArray);
        JsonArray jsonArray3 = new JsonArray();
        for (String a2 : this.errors) {
            jsonArray3.a(a2);
        }
        jsonObject.a(ReportDBAdapter.ReportColumns.COLUMN_ERRORS, (JsonElement) jsonArray3);
        JsonArray jsonArray4 = new JsonArray();
        for (String a3 : this.clickedThrough) {
            jsonArray4.a(a3);
        }
        jsonObject.a("clickedThrough", (JsonElement) jsonArray4);
        if (this.incentivized && !TextUtils.isEmpty(this.userID)) {
            jsonObject.a("user", this.userID);
        }
        int i2 = this.ordinal;
        if (i2 > 0) {
            jsonObject.a("ordinal_view", (Number) Integer.valueOf(i2));
        }
        return jsonObject;
    }

    public Report(Advertisement advertisement, Placement placement, long j) {
        this(advertisement, placement, j, (String) null);
    }

    public Report(Advertisement advertisement, Placement placement, long j, String str) {
        this.status = 0;
        this.userActions = new ArrayList();
        this.clickedThrough = new ArrayList();
        this.errors = new ArrayList();
        this.placementId = placement.getId();
        this.adToken = advertisement.getAdToken();
        this.advertisementID = advertisement.getId();
        this.appId = advertisement.getAppID();
        this.incentivized = placement.isIncentivized();
        this.adStartTime = j;
        this.url = advertisement.getUrl();
        this.ttDownload = -1;
        this.campaign = advertisement.getCampaign();
        int adType2 = advertisement.getAdType();
        if (adType2 == 0) {
            this.adType = "vungle_local";
        } else if (adType2 == 1) {
            this.adType = "vungle_mraid";
        } else {
            throw new IllegalArgumentException("Unknown ad type, cannot process!");
        }
        this.templateId = advertisement.getTemplateId();
        if (str == null) {
            this.userID = "";
        } else {
            this.userID = str;
        }
        this.ordinal = advertisement.getAdConfig().getOrdinal();
        AdConfig.AdSize adSize2 = advertisement.getAdConfig().getAdSize();
        if (AdConfig.AdSize.isBannerAdSize(adSize2)) {
            this.adSize = adSize2.getName();
        }
    }
}
