package com.xuhao.didi.socket.client.impl.exceptions;

public class UnConnectException extends RuntimeException {
    public UnConnectException() {
    }

    public UnConnectException(String str) {
        super(str);
    }

    public UnConnectException(String str, Throwable th) {
        super(str, th);
    }

    public UnConnectException(Throwable th) {
        super(th);
    }

    protected UnConnectException(String str, Throwable th, boolean z, boolean z2) {
        super(str, th, z, z2);
    }
}
