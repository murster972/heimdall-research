package com.xuhao.didi.socket.client.impl.client.action;

import com.xuhao.didi.core.iocore.interfaces.IStateSender;
import com.xuhao.didi.core.utils.SLog;
import com.xuhao.didi.socket.client.sdk.client.ConnectionInfo;
import com.xuhao.didi.socket.client.sdk.client.OkSocketOptions;
import com.xuhao.didi.socket.client.sdk.client.action.ISocketActionListener;
import com.xuhao.didi.socket.client.sdk.client.connection.IConnectionManager;
import com.xuhao.didi.socket.common.interfaces.basic.AbsLoopThread;
import com.xuhao.didi.socket.common.interfaces.common_interfacies.dispatcher.IRegister;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ActionDispatcher implements IRegister<ISocketActionListener, IConnectionManager>, IStateSender {
    /* access modifiers changed from: private */
    public static final LinkedBlockingQueue<ActionBean> ACTION_QUEUE = new LinkedBlockingQueue<>();
    private static final DispatchThread HANDLE_THREAD = new DispatchThread();
    private volatile ConnectionInfo mConnectionInfo;
    private volatile IConnectionManager mManager;
    /* access modifiers changed from: private */
    public volatile List<ISocketActionListener> mResponseHandlerList = new ArrayList();

    protected static class ActionBean {
        Serializable arg;
        String mAction = "";
        ActionDispatcher mDispatcher;

        public ActionBean(String str, Serializable serializable, ActionDispatcher actionDispatcher) {
            this.mAction = str;
            this.arg = serializable;
            this.mDispatcher = actionDispatcher;
        }
    }

    public static class ActionRunnable implements Runnable {
        private ActionBean mActionBean;

        ActionRunnable(ActionBean actionBean) {
            this.mActionBean = actionBean;
        }

        public void run() {
            ActionDispatcher actionDispatcher;
            ActionBean actionBean = this.mActionBean;
            if (actionBean != null && (actionDispatcher = actionBean.mDispatcher) != null) {
                synchronized (actionDispatcher.mResponseHandlerList) {
                    for (ISocketActionListener access$200 : new ArrayList(actionDispatcher.mResponseHandlerList)) {
                        actionDispatcher.dispatchActionToListener(this.mActionBean.mAction, this.mActionBean.arg, access$200);
                    }
                }
            }
        }
    }

    private static class DispatchThread extends AbsLoopThread {
        public DispatchThread() {
            super("client_action_dispatch_thread");
        }

        /* access modifiers changed from: protected */
        public void loopFinish(Exception exc) {
        }

        /* access modifiers changed from: protected */
        public void runInLoopThread() throws Exception {
            ActionDispatcher actionDispatcher;
            ActionBean actionBean = (ActionBean) ActionDispatcher.ACTION_QUEUE.take();
            if (actionBean != null && (actionDispatcher = actionBean.mDispatcher) != null) {
                synchronized (actionDispatcher.mResponseHandlerList) {
                    for (ISocketActionListener access$200 : new ArrayList(actionDispatcher.mResponseHandlerList)) {
                        actionDispatcher.dispatchActionToListener(actionBean.mAction, actionBean.arg, access$200);
                    }
                }
            }
        }
    }

    static {
        HANDLE_THREAD.start();
    }

    public ActionDispatcher(ConnectionInfo connectionInfo, IConnectionManager iConnectionManager) {
        this.mManager = iConnectionManager;
        this.mConnectionInfo = connectionInfo;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dispatchActionToListener(java.lang.String r2, java.io.Serializable r3, com.xuhao.didi.socket.client.sdk.client.action.ISocketActionListener r4) {
        /*
            r1 = this;
            int r0 = r2.hashCode()
            switch(r0) {
                case -1455248519: goto L_0x0065;
                case -1321574355: goto L_0x005b;
                case -1245920523: goto L_0x0051;
                case -1201839197: goto L_0x0047;
                case -1121297674: goto L_0x003d;
                case -749410229: goto L_0x0033;
                case -542453077: goto L_0x0028;
                case 190576450: goto L_0x001e;
                case 1756120480: goto L_0x0013;
                case 2146005698: goto L_0x0009;
                default: goto L_0x0007;
            }
        L_0x0007:
            goto L_0x006f
        L_0x0009:
            java.lang.String r0 = "action_write_complete"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 6
            goto L_0x0070
        L_0x0013:
            java.lang.String r0 = "action_pulse_request"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 9
            goto L_0x0070
        L_0x001e:
            java.lang.String r0 = "action_write_thread_shutdown"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 7
            goto L_0x0070
        L_0x0028:
            java.lang.String r0 = "action_read_thread_shutdown"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 8
            goto L_0x0070
        L_0x0033:
            java.lang.String r0 = "action_connection_success"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 0
            goto L_0x0070
        L_0x003d:
            java.lang.String r0 = "action_write_thread_start"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 5
            goto L_0x0070
        L_0x0047:
            java.lang.String r0 = "action_disconnection"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 2
            goto L_0x0070
        L_0x0051:
            java.lang.String r0 = "action_connection_failed"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 1
            goto L_0x0070
        L_0x005b:
            java.lang.String r0 = "action_read_thread_start"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 4
            goto L_0x0070
        L_0x0065:
            java.lang.String r0 = "action_read_complete"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x006f
            r0 = 3
            goto L_0x0070
        L_0x006f:
            r0 = -1
        L_0x0070:
            switch(r0) {
                case 0: goto L_0x00ca;
                case 1: goto L_0x00bd;
                case 2: goto L_0x00b0;
                case 3: goto L_0x00a3;
                case 4: goto L_0x009a;
                case 5: goto L_0x009a;
                case 6: goto L_0x008d;
                case 7: goto L_0x0082;
                case 8: goto L_0x0082;
                case 9: goto L_0x0075;
                default: goto L_0x0073;
            }
        L_0x0073:
            goto L_0x00d4
        L_0x0075:
            com.xuhao.didi.core.iocore.interfaces.IPulseSendable r3 = (com.xuhao.didi.core.iocore.interfaces.IPulseSendable) r3     // Catch:{ Exception -> 0x007d }
            com.xuhao.didi.socket.client.sdk.client.ConnectionInfo r2 = r1.mConnectionInfo     // Catch:{ Exception -> 0x007d }
            r4.onPulseSend(r2, r3)     // Catch:{ Exception -> 0x007d }
            goto L_0x00d4
        L_0x007d:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00d4
        L_0x0082:
            java.lang.Exception r3 = (java.lang.Exception) r3     // Catch:{ Exception -> 0x0088 }
            r4.onSocketIOThreadShutdown(r2, r3)     // Catch:{ Exception -> 0x0088 }
            goto L_0x00d4
        L_0x0088:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00d4
        L_0x008d:
            com.xuhao.didi.core.iocore.interfaces.ISendable r3 = (com.xuhao.didi.core.iocore.interfaces.ISendable) r3     // Catch:{ Exception -> 0x0095 }
            com.xuhao.didi.socket.client.sdk.client.ConnectionInfo r0 = r1.mConnectionInfo     // Catch:{ Exception -> 0x0095 }
            r4.onSocketWriteResponse(r0, r2, r3)     // Catch:{ Exception -> 0x0095 }
            goto L_0x00d4
        L_0x0095:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00d4
        L_0x009a:
            r4.onSocketIOThreadStart(r2)     // Catch:{ Exception -> 0x009e }
            goto L_0x00d4
        L_0x009e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00d4
        L_0x00a3:
            com.xuhao.didi.core.pojo.OriginalData r3 = (com.xuhao.didi.core.pojo.OriginalData) r3     // Catch:{ Exception -> 0x00ab }
            com.xuhao.didi.socket.client.sdk.client.ConnectionInfo r0 = r1.mConnectionInfo     // Catch:{ Exception -> 0x00ab }
            r4.onSocketReadResponse(r0, r2, r3)     // Catch:{ Exception -> 0x00ab }
            goto L_0x00d4
        L_0x00ab:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00d4
        L_0x00b0:
            java.lang.Exception r3 = (java.lang.Exception) r3     // Catch:{ Exception -> 0x00b8 }
            com.xuhao.didi.socket.client.sdk.client.ConnectionInfo r0 = r1.mConnectionInfo     // Catch:{ Exception -> 0x00b8 }
            r4.onSocketDisconnection(r0, r2, r3)     // Catch:{ Exception -> 0x00b8 }
            goto L_0x00d4
        L_0x00b8:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00d4
        L_0x00bd:
            java.lang.Exception r3 = (java.lang.Exception) r3     // Catch:{ Exception -> 0x00c5 }
            com.xuhao.didi.socket.client.sdk.client.ConnectionInfo r0 = r1.mConnectionInfo     // Catch:{ Exception -> 0x00c5 }
            r4.onSocketConnectionFailed(r0, r2, r3)     // Catch:{ Exception -> 0x00c5 }
            goto L_0x00d4
        L_0x00c5:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00d4
        L_0x00ca:
            com.xuhao.didi.socket.client.sdk.client.ConnectionInfo r3 = r1.mConnectionInfo     // Catch:{ Exception -> 0x00d0 }
            r4.onSocketConnectionSuccess(r3, r2)     // Catch:{ Exception -> 0x00d0 }
            goto L_0x00d4
        L_0x00d0:
            r2 = move-exception
            r2.printStackTrace()
        L_0x00d4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xuhao.didi.socket.client.impl.client.action.ActionDispatcher.dispatchActionToListener(java.lang.String, java.io.Serializable, com.xuhao.didi.socket.client.sdk.client.action.ISocketActionListener):void");
    }

    public void sendBroadcast(String str, Serializable serializable) {
        OkSocketOptions option = this.mManager.getOption();
        if (option != null) {
            OkSocketOptions.ThreadModeToken callbackThreadModeToken = option.getCallbackThreadModeToken();
            if (callbackThreadModeToken != null) {
                try {
                    callbackThreadModeToken.handleCallbackEvent(new ActionRunnable(new ActionBean(str, serializable, this)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (option.isCallbackInIndependentThread()) {
                ACTION_QUEUE.offer(new ActionBean(str, serializable, this));
            } else if (!option.isCallbackInIndependentThread()) {
                synchronized (this.mResponseHandlerList) {
                    for (ISocketActionListener dispatchActionToListener : new ArrayList(this.mResponseHandlerList)) {
                        dispatchActionToListener(str, serializable, dispatchActionToListener);
                    }
                }
            } else {
                SLog.e("ActionDispatcher error action:" + str + " is not dispatch");
            }
        }
    }

    public void setConnectionInfo(ConnectionInfo connectionInfo) {
        this.mConnectionInfo = connectionInfo;
    }

    public IConnectionManager registerReceiver(ISocketActionListener iSocketActionListener) {
        if (iSocketActionListener != null) {
            synchronized (this.mResponseHandlerList) {
                if (!this.mResponseHandlerList.contains(iSocketActionListener)) {
                    this.mResponseHandlerList.add(iSocketActionListener);
                }
            }
        }
        return this.mManager;
    }

    public IConnectionManager unRegisterReceiver(ISocketActionListener iSocketActionListener) {
        if (iSocketActionListener != null) {
            synchronized (this.mResponseHandlerList) {
                this.mResponseHandlerList.remove(iSocketActionListener);
            }
        }
        return this.mManager;
    }

    public void sendBroadcast(String str) {
        sendBroadcast(str, (Serializable) null);
    }
}
