package com.xuhao.didi.socket.client.impl.client.iothreads;

import com.xuhao.didi.core.iocore.interfaces.IReader;
import com.xuhao.didi.core.iocore.interfaces.IStateSender;
import com.xuhao.didi.core.iocore.interfaces.IWriter;
import com.xuhao.didi.core.utils.SLog;
import com.xuhao.didi.socket.client.impl.exceptions.ManuallyDisconnectException;
import com.xuhao.didi.socket.client.sdk.client.action.IAction;
import com.xuhao.didi.socket.common.interfaces.basic.AbsLoopThread;
import java.io.IOException;

public class SimplexIOThread extends AbsLoopThread {
    private boolean isWrite = false;
    private IReader mReader;
    private IStateSender mStateSender;
    private IWriter mWriter;

    public SimplexIOThread(IReader iReader, IWriter iWriter, IStateSender iStateSender) {
        super("client_simplex_io_thread");
        this.mStateSender = iStateSender;
        this.mReader = iReader;
        this.mWriter = iWriter;
    }

    /* access modifiers changed from: protected */
    public void beforeLoop() throws IOException {
        this.mStateSender.sendBroadcast(IAction.ACTION_WRITE_THREAD_START);
        this.mStateSender.sendBroadcast(IAction.ACTION_READ_THREAD_START);
    }

    /* access modifiers changed from: protected */
    public void loopFinish(Exception exc) {
        if (exc instanceof ManuallyDisconnectException) {
            exc = null;
        }
        if (exc != null) {
            SLog.e("simplex error,thread is dead with exception:" + exc.getMessage());
        }
        this.mStateSender.sendBroadcast(IAction.ACTION_WRITE_THREAD_SHUTDOWN, exc);
        this.mStateSender.sendBroadcast(IAction.ACTION_READ_THREAD_SHUTDOWN, exc);
    }

    /* access modifiers changed from: protected */
    public void runInLoopThread() throws IOException {
        this.isWrite = this.mWriter.write();
        if (this.isWrite) {
            this.isWrite = false;
            this.mReader.read();
        }
    }

    public synchronized void shutdown(Exception exc) {
        this.mReader.close();
        this.mWriter.close();
        super.shutdown(exc);
    }
}
