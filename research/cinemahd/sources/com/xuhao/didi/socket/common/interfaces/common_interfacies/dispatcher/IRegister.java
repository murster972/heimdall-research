package com.xuhao.didi.socket.common.interfaces.common_interfacies.dispatcher;

public interface IRegister<T, E> {
    E registerReceiver(T t);

    E unRegisterReceiver(T t);
}
