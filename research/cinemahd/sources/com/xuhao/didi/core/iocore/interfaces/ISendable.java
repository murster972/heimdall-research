package com.xuhao.didi.core.iocore.interfaces;

import java.io.Serializable;

public interface ISendable extends Serializable {
    byte[] parse();
}
