package com.xuhao.didi.core.iocore.interfaces;

public interface IOAction {
    public static final String ACTION_PULSE_REQUEST = "action_pulse_request";
    public static final String ACTION_READ_COMPLETE = "action_read_complete";
    public static final String ACTION_WRITE_COMPLETE = "action_write_complete";
}
