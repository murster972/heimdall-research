package com.chartboost.sdk.InPlay;

import android.graphics.Bitmap;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.b;
import com.chartboost.sdk.h;
import com.chartboost.sdk.impl.ah;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.ak;
import com.chartboost.sdk.impl.ap;
import com.chartboost.sdk.impl.bh;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.e;
import com.chartboost.sdk.impl.g;
import com.chartboost.sdk.impl.s;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.io.File;
import java.util.concurrent.Executor;

public final class CBInPlay {

    /* renamed from: a  reason: collision with root package name */
    private final e f2389a;
    public final String appName;
    private final ah b;
    private final ap c;
    private final a d;
    private final ak e;
    private final com.chartboost.sdk.Model.a f;
    private Bitmap g;
    public final File largeAppIconFile;
    public final String largeAppIconUrl;
    public final String location;

    private CBInPlay(e eVar, ah ahVar, ap apVar, a aVar, ak akVar, com.chartboost.sdk.Model.a aVar2, String str, File file, String str2) {
        this.f2389a = eVar;
        this.b = ahVar;
        this.c = apVar;
        this.d = aVar;
        this.e = akVar;
        this.f = aVar2;
        this.appName = aVar2.q;
        this.largeAppIconUrl = str;
        this.largeAppIconFile = file;
        this.location = str2;
    }

    public static void cacheInPlay(String str) {
        h a2 = h.a();
        if (a2 != null && b.a()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("CBInPlay", "cacheInPlay location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.e;
                cVar.getClass();
                handler.post(new c.a(4, str, CBError.CBImpressionError.INVALID_LOCATION));
                return;
            }
            com.chartboost.sdk.Model.e eVar = a2.m.get();
            if ((!eVar.y || !eVar.z) && (!eVar.e || !eVar.f)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.e;
                cVar2.getClass();
                handler2.post(new c.a(4, str, CBError.CBImpressionError.END_POINT_DISABLED));
                return;
            }
            e eVar2 = a2.d;
            eVar2.getClass();
            a2.f2426a.execute(new e.a(3, str, (f) null, (CBError.CBImpressionError) null));
        }
    }

    public static CBInPlay getInPlay(String str) {
        h a2 = h.a();
        CBInPlay cBInPlay = null;
        if (a2 != null && b.a()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("CBInPlay", "Inplay location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.e;
                cVar.getClass();
                handler.post(new c.a(4, str, CBError.CBImpressionError.INVALID_LOCATION));
                return null;
            }
            com.chartboost.sdk.Model.e eVar = a2.m.get();
            CBError.CBImpressionError cBImpressionError = CBError.CBImpressionError.NO_AD_FOUND;
            if ((eVar.y && eVar.z) || (eVar.e && eVar.f)) {
                com.chartboost.sdk.Model.a a3 = a2.d.a(str);
                if (a3 != null) {
                    com.chartboost.sdk.Model.b bVar = a3.c.get("lg");
                    if (bVar != null) {
                        File a4 = bVar.a(a2.d.b.d().f2398a);
                        if (!a4.exists()) {
                            cBImpressionError = CBError.CBImpressionError.ASSET_MISSING;
                        } else {
                            cBInPlay = new CBInPlay(a2.d, a2.h, a2.j, a2.o, a2.r, a3, bVar.c, a4, str);
                        }
                    }
                    Executor executor = a2.f2426a;
                    e eVar2 = a2.d;
                    eVar2.getClass();
                    executor.execute(new e.a(8, str, (f) null, (CBError.CBImpressionError) null));
                }
                if (cBInPlay == null) {
                    Executor executor2 = a2.f2426a;
                    e eVar3 = a2.d;
                    eVar3.getClass();
                    executor2.execute(new e.a(3, str, (f) null, (CBError.CBImpressionError) null));
                }
            }
            if (cBInPlay == null) {
                Handler handler2 = a2.p;
                c cVar2 = a2.e;
                cVar2.getClass();
                handler2.post(new c.a(4, str, cBImpressionError));
            }
        }
        return cBInPlay;
    }

    public static boolean hasInPlay(String str) {
        h a2 = h.a();
        if (a2 == null || !b.a()) {
            return false;
        }
        if (s.a().a((CharSequence) str)) {
            CBLogging.b("CBInPlay", "hasInPlay location cannot be empty");
            Handler handler = a2.p;
            c cVar = a2.e;
            cVar.getClass();
            handler.post(new c.a(4, str, CBError.CBImpressionError.INVALID_LOCATION));
            return false;
        } else if (a2.d.a(str) != null) {
            return true;
        } else {
            return false;
        }
    }

    public void click() {
        com.chartboost.sdk.Model.a aVar = this.f;
        String str = aVar.j;
        String str2 = aVar.i;
        String str3 = (str2.isEmpty() || !this.e.a(str2)) ? str : str2;
        aj ajVar = new aj("/api/click", this.c, this.d, 2, (aj.a) null);
        ajVar.a("location", (Object) this.location);
        ajVar.a("to", (Object) this.f.m);
        ajVar.a("cgn", (Object) this.f.g);
        ajVar.a(VisionDataDBAdapter.VisionDataColumns.COLUMN_CREATIVE, (Object) this.f.h);
        ajVar.a("ad_id", (Object) this.f.f);
        ajVar.a("type", (Object) "native");
        if (str3 == null || str3.isEmpty()) {
            this.e.a((com.chartboost.sdk.Model.c) null, false, str3, CBError.CBClickError.URI_INVALID, ajVar);
        } else {
            this.e.a((com.chartboost.sdk.Model.c) null, str3, ajVar);
        }
    }

    public Bitmap getAppIcon() throws Exception {
        if (this.g == null) {
            try {
                byte[] b2 = bh.b(this.largeAppIconFile);
                if (b2 != null) {
                    this.g = s.a().a(b2);
                }
                if (this.g == null) {
                    CBLogging.b("CBInPlay", "Error decoding inplay bitmap " + this.largeAppIconFile.getAbsolutePath());
                    if (!this.largeAppIconFile.delete()) {
                        CBLogging.b("CBInPlay", "Unable to delete corrupt inplay bitmap " + this.largeAppIconFile.getAbsolutePath());
                    }
                    throw new Exception("decodeByteArrayToBitmap returned null");
                }
            } catch (OutOfMemoryError e2) {
                CBLogging.b("CBInPlay", "Out of memory decoding inplay bitmap " + this.largeAppIconFile.getAbsolutePath());
                throw new Exception(e2);
            }
        }
        return this.g;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getLocation() {
        return this.location;
    }

    public void show() {
        aj ajVar = new aj("/inplay/show", this.c, this.d, 2, new g(this.f2389a, this.location));
        ajVar.j = 1;
        ajVar.a("inplay-dictionary", (Object) this.f.f2407a);
        ajVar.a("location", (Object) this.location);
        this.b.a(ajVar);
    }
}
