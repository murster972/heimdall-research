package com.chartboost.sdk.Model;

import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public final JSONObject f2407a;
    public final int b;
    public final Map<String, b> c = new HashMap();
    public final Map<String, String> d = new HashMap();
    public final String e;
    public final String f;
    public final String g;
    public final String h;
    public final String i;
    public final String j;
    public final int k;
    public final String l;
    public final String m;
    public final Map<String, List<String>> n = new HashMap();
    public final int o;
    public final String p;
    public final String q;
    public final b r;
    public final HashSet<String> s = new HashSet<>();

    public a(int i2, JSONObject jSONObject, boolean z) throws JSONException {
        String str;
        this.b = i2;
        this.f2407a = jSONObject;
        this.f = jSONObject.getString("ad_id");
        this.g = jSONObject.getString("cgn");
        this.h = jSONObject.getString(VisionDataDBAdapter.VisionDataColumns.COLUMN_CREATIVE);
        this.i = jSONObject.optString("deep-link");
        this.j = jSONObject.getString("link");
        this.m = jSONObject.getString("to");
        this.o = jSONObject.optInt("animation");
        this.p = jSONObject.optString("media-type");
        this.q = jSONObject.optString(MediationMetaData.KEY_NAME);
        if (i2 == 1) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("webview");
            JSONArray jSONArray = jSONObject2.getJSONArray("elements");
            String str2 = "";
            int i3 = 0;
            int i4 = 0;
            while (true) {
                str = "body";
                if (i3 >= jSONArray.length()) {
                    break;
                }
                JSONObject jSONObject3 = jSONArray.getJSONObject(i3);
                String string = jSONObject3.getString(MediationMetaData.KEY_NAME);
                String optString = jSONObject3.optString("param");
                String string2 = jSONObject3.getString("type");
                String string3 = jSONObject3.getString("value");
                if (string2.equals("param")) {
                    this.d.put(optString, string3);
                    if (string.equals("reward_amount")) {
                        i4 = Integer.valueOf(string3).intValue();
                    } else if (string.equals("reward_currency")) {
                        str2 = string3;
                    }
                } else {
                    this.c.put((!string2.equals("html") || !optString.isEmpty()) ? optString.isEmpty() ? string : optString : str, new b(string2, string, string3));
                }
                i3++;
            }
            this.k = i4;
            this.l = str2;
            this.r = this.c.get(str);
            if (this.r != null) {
                this.e = jSONObject2.getString(Advertisement.KEY_TEMPLATE);
                JSONObject optJSONObject = jSONObject.optJSONObject("events");
                if (optJSONObject != null) {
                    Iterator<String> keys = optJSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        JSONArray jSONArray2 = optJSONObject.getJSONArray(next);
                        ArrayList arrayList = new ArrayList();
                        for (int i5 = 0; i5 < jSONArray2.length(); i5++) {
                            arrayList.add(jSONArray2.getString(i5));
                        }
                        this.n.put(next, arrayList);
                    }
                }
                JSONArray optJSONArray = jSONObject.optJSONArray("certification_providers");
                if (optJSONArray != null) {
                    for (int i6 = 0; i6 < optJSONArray.length(); i6++) {
                        this.s.add(optJSONArray.getString(i6));
                    }
                    return;
                }
                return;
            }
            throw new RuntimeException("WebView AdUnit does not have a template html body asset");
        }
        if (z) {
            String string4 = jSONObject.getJSONObject("icons").getString("lg");
            this.c.put("lg", new b("inPlayIcons", string4.substring(string4.lastIndexOf("/") + 1), string4));
            this.k = 0;
            this.l = "";
        } else {
            JSONObject jSONObject4 = jSONObject.getJSONObject("assets");
            Iterator<String> keys2 = jSONObject4.keys();
            while (keys2.hasNext()) {
                String next2 = keys2.next();
                JSONObject jSONObject5 = jSONObject4.getJSONObject(next2);
                String str3 = (next2.equals("video-portrait") || next2.equals("video-landscape")) ? "videos" : "images";
                String optString2 = jSONObject5.optString("id", (String) null);
                if (optString2 == null) {
                    optString2 = jSONObject5.getString("checksum") + ".png";
                }
                this.c.put(next2, new b(str3, optString2, jSONObject5.getString(ReportDBAdapter.ReportColumns.COLUMN_URL)));
            }
            this.k = jSONObject.optInt("reward");
            this.l = jSONObject.optString("currency-name");
        }
        this.r = null;
        this.e = "";
    }
}
