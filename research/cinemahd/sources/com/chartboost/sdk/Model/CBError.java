package com.chartboost.sdk.Model;

public final class CBError {

    /* renamed from: a  reason: collision with root package name */
    private final a f2402a;
    private final String b;
    private boolean c = true;

    /* renamed from: com.chartboost.sdk.Model.CBError$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2403a = new int[a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|(3:13|14|16)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.chartboost.sdk.Model.CBError$a[] r0 = com.chartboost.sdk.Model.CBError.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2403a = r0
                int[] r0 = f2403a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.MISCELLANEOUS     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2403a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.UNEXPECTED_RESPONSE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2403a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.PUBLIC_KEY_MISSING     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f2403a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.INTERNET_UNAVAILABLE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f2403a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.HTTP_NOT_FOUND     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f2403a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.NETWORK_FAILURE     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = f2403a     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.INVALID_RESPONSE     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Model.CBError.AnonymousClass1.<clinit>():void");
        }
    }

    public enum CBClickError {
        URI_INVALID,
        URI_UNRECOGNIZED,
        AGE_GATE_FAILURE,
        NO_HOST_ACTIVITY,
        INTERNAL
    }

    public enum CBImpressionError {
        INTERNAL,
        INTERNET_UNAVAILABLE,
        TOO_MANY_CONNECTIONS,
        WRONG_ORIENTATION,
        FIRST_SESSION_INTERSTITIALS_DISABLED,
        NETWORK_FAILURE,
        NO_AD_FOUND,
        SESSION_NOT_STARTED,
        IMPRESSION_ALREADY_VISIBLE,
        NO_HOST_ACTIVITY,
        USER_CANCELLATION,
        INVALID_LOCATION,
        VIDEO_UNAVAILABLE,
        VIDEO_ID_MISSING,
        ERROR_PLAYING_VIDEO,
        INVALID_RESPONSE,
        ASSETS_DOWNLOAD_FAILURE,
        ERROR_CREATING_VIEW,
        ERROR_DISPLAYING_VIEW,
        INCOMPATIBLE_API_VERSION,
        ERROR_LOADING_WEB_VIEW,
        ASSET_PREFETCH_IN_PROGRESS,
        ACTIVITY_MISSING_IN_MANIFEST,
        EMPTY_LOCAL_VIDEO_LIST,
        END_POINT_DISABLED,
        HARDWARE_ACCELERATION_DISABLED,
        PENDING_IMPRESSION_ERROR,
        VIDEO_UNAVAILABLE_FOR_CURRENT_ORIENTATION,
        ASSET_MISSING,
        WEB_VIEW_PAGE_LOAD_TIMEOUT,
        WEB_VIEW_CLIENT_RECEIVED_ERROR,
        INTERNET_UNAVAILABLE_AT_SHOW
    }

    public enum a {
        MISCELLANEOUS,
        INTERNET_UNAVAILABLE,
        INVALID_RESPONSE,
        UNEXPECTED_RESPONSE,
        NETWORK_FAILURE,
        PUBLIC_KEY_MISSING,
        HTTP_NOT_FOUND
    }

    public CBError(a aVar, String str) {
        this.f2402a = aVar;
        this.b = str;
    }

    public a a() {
        return this.f2402a;
    }

    public String b() {
        return this.b;
    }

    public CBImpressionError c() {
        int i = AnonymousClass1.f2403a[this.f2402a.ordinal()];
        if (i == 1 || i == 2 || i == 3) {
            return CBImpressionError.INTERNAL;
        }
        if (i == 4) {
            return CBImpressionError.INTERNET_UNAVAILABLE;
        }
        if (i != 5) {
            return CBImpressionError.NETWORK_FAILURE;
        }
        return CBImpressionError.NO_AD_FOUND;
    }
}
