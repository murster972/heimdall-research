package com.chartboost.sdk.Model;

import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.d;
import com.chartboost.sdk.e;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.ah;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.ak;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.ap;
import com.chartboost.sdk.impl.bc;
import com.chartboost.sdk.impl.bf;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.s;
import com.chartboost.sdk.impl.u;
import com.chartboost.sdk.impl.v;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.Locale;
import org.json.JSONObject;

public class c {
    private boolean A;
    private Boolean B = null;
    private e C;
    private Runnable D;

    /* renamed from: a  reason: collision with root package name */
    public final com.chartboost.sdk.impl.c f2409a;
    public final f b;
    public final ah c;
    public final ap d;
    public final a e;
    public final Handler f;
    public final com.chartboost.sdk.c g;
    public final ak h;
    public final d i;
    public final al j;
    public final d k;
    public int l;
    public final String m;
    public int n;
    public final String o;
    public final a p;
    public final SharedPreferences q;
    public boolean r;
    public bc s;
    public boolean t = false;
    public boolean u = false;
    public boolean v = false;
    public aj w;
    public boolean x;
    public boolean y = false;
    public boolean z = false;

    public c(a aVar, d dVar, f fVar, ah ahVar, ap apVar, SharedPreferences sharedPreferences, a aVar2, Handler handler, com.chartboost.sdk.c cVar, ak akVar, d dVar2, al alVar, com.chartboost.sdk.impl.c cVar2, String str, String str2) {
        this.p = aVar;
        this.f2409a = cVar2;
        this.b = fVar;
        this.c = ahVar;
        this.d = apVar;
        this.e = aVar2;
        this.f = handler;
        this.g = cVar;
        this.h = akVar;
        this.i = dVar2;
        this.j = alVar;
        this.k = dVar;
        this.l = 0;
        this.r = false;
        this.x = false;
        this.z = true;
        this.n = 3;
        this.m = str;
        this.o = str2;
        this.A = true;
        this.q = sharedPreferences;
    }

    private boolean x() {
        return this.B != null;
    }

    private boolean y() {
        return this.B.booleanValue();
    }

    public boolean a() {
        this.l = 0;
        a aVar = this.p;
        if (aVar.b == 0) {
            int i2 = this.f2409a.f2477a;
            if (i2 != 0) {
                if (i2 == 1) {
                    this.n = 2;
                    this.C = new v(this, this.b, this.f, this.g);
                    this.A = false;
                }
            } else if (aVar.p.equals(Advertisement.KEY_VIDEO)) {
                this.n = 1;
                this.C = new v(this, this.b, this.f, this.g);
                this.A = false;
            } else {
                this.n = 0;
                this.C = new u(this, this.f, this.g);
            }
        } else {
            int i3 = this.f2409a.f2477a;
            if (i3 != 0) {
                if (i3 == 1) {
                    this.n = 2;
                    this.A = false;
                }
            } else if (aVar.p.equals(Advertisement.KEY_VIDEO)) {
                this.n = 1;
                this.A = false;
            } else {
                this.n = 0;
            }
            this.C = new bf(this, this.b, this.c, this.q, this.e, this.f, this.g, this.i);
        }
        return this.C.a(this.p.f2407a);
    }

    public boolean b() {
        return this.A;
    }

    public void c() {
        this.z = true;
        this.g.b(this);
        this.k.b(this);
    }

    public void d() {
        this.k.a(this);
    }

    public void e() {
        com.chartboost.sdk.a aVar;
        this.u = true;
        this.A = true;
        if (this.f2409a.f2477a == 1 && (aVar = i.c) != null) {
            aVar.didCompleteRewardedVideo(this.m, this.p.k);
        }
        w();
    }

    public void f() {
        this.v = true;
    }

    public boolean g() {
        e eVar = this.C;
        if (eVar != null) {
            eVar.b();
            if (this.C.e() != null) {
                return true;
            }
        } else {
            CBLogging.b("CBImpression", "reinitializing -- no view protocol exists!!");
        }
        CBLogging.e("CBImpression", "reinitializing -- view not yet created");
        return false;
    }

    public void h() {
        i();
        if (this.r) {
            e eVar = this.C;
            if (eVar != null) {
                eVar.d();
            }
            this.C = null;
            CBLogging.e("CBImpression", "Destroying the view and view data");
        }
    }

    public void i() {
        bc bcVar = this.s;
        if (bcVar != null) {
            bcVar.b();
            try {
                if (!(this.C == null || this.C.e() == null || this.C.e().getParent() == null)) {
                    this.s.removeView(this.C.e());
                }
            } catch (Exception e2) {
                CBLogging.a("CBImpression", "Exception raised while cleaning up views", e2);
                a.a(c.class, "cleanUpViews", e2);
            }
            this.s = null;
        }
        e eVar = this.C;
        if (eVar != null) {
            eVar.f();
        }
        CBLogging.e("CBImpression", "Destroying the view");
    }

    public CBError.CBImpressionError j() {
        try {
            if (this.C != null) {
                return this.C.c();
            }
        } catch (Exception e2) {
            a.a(c.class, "tryCreatingView", e2);
        }
        return CBError.CBImpressionError.ERROR_CREATING_VIEW;
    }

    public e.a k() {
        e eVar = this.C;
        if (eVar != null) {
            return eVar.e();
        }
        return null;
    }

    public void l() {
        e eVar = this.C;
        if (eVar != null && eVar.e() != null) {
            this.C.e().setVisibility(8);
        }
    }

    public void m() {
        this.t = true;
    }

    public void n() {
        Runnable runnable = this.D;
        if (runnable != null) {
            runnable.run();
            this.D = null;
        }
        this.t = false;
    }

    public String o() {
        return this.p.f;
    }

    public void p() {
        this.k.c(this);
    }

    public boolean q() {
        e eVar = this.C;
        if (eVar != null) {
            return eVar.l();
        }
        return false;
    }

    public void r() {
        this.x = false;
        e eVar = this.C;
        if (eVar != null && this.y) {
            this.y = false;
            eVar.m();
        }
    }

    public void s() {
        this.x = false;
    }

    public void t() {
        e eVar = this.C;
        if (eVar != null && !this.y) {
            this.y = true;
            eVar.n();
        }
    }

    public e u() {
        return this.C;
    }

    public boolean v() {
        return this.z;
    }

    public void w() {
        aj ajVar = new aj("/api/video-complete", this.d, this.e, 2, (aj.a) null);
        ajVar.a("location", (Object) this.m);
        ajVar.a("reward", (Object) Integer.valueOf(this.p.k));
        ajVar.a("currency-name", (Object) this.p.l);
        ajVar.a("ad_id", (Object) o());
        ajVar.a("force_close", (Object) false);
        if (!this.p.g.isEmpty()) {
            ajVar.a("cgn", (Object) this.p.g);
        }
        e u2 = k() != null ? u() : null;
        if (u2 != null) {
            float k2 = u2.k();
            float j2 = u2.j();
            CBLogging.a(c.class.getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", new Object[]{Float.valueOf(j2), Float.valueOf(k2)}));
            float f2 = j2 / 1000.0f;
            ajVar.a("total_time", (Object) Float.valueOf(f2));
            if (k2 <= 0.0f) {
                ajVar.a("playback_time", (Object) Float.valueOf(f2));
            } else {
                ajVar.a("playback_time", (Object) Float.valueOf(k2 / 1000.0f));
            }
        }
        this.c.a(ajVar);
        this.e.b(this.f2409a.a(this.p.b), o());
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(org.json.JSONObject r7) {
        /*
            r6 = this;
            int r0 = r6.l
            r1 = 0
            r2 = 2
            if (r0 != r2) goto L_0x0045
            boolean r0 = r6.t
            if (r0 == 0) goto L_0x000b
            goto L_0x0045
        L_0x000b:
            com.chartboost.sdk.Model.a r0 = r6.p
            java.lang.String r2 = r0.j
            java.lang.String r0 = r0.i
            boolean r3 = r0.isEmpty()
            if (r3 != 0) goto L_0x0037
            com.chartboost.sdk.impl.ak r3 = r6.h     // Catch:{ Exception -> 0x002f }
            boolean r3 = r3.a(r0)     // Catch:{ Exception -> 0x002f }
            if (r3 == 0) goto L_0x002a
            java.lang.Boolean r2 = java.lang.Boolean.TRUE     // Catch:{ Exception -> 0x0025 }
            r6.B = r2     // Catch:{ Exception -> 0x0025 }
            r2 = r0
            goto L_0x0037
        L_0x0025:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0030
        L_0x002a:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE     // Catch:{ Exception -> 0x002f }
            r6.B = r0     // Catch:{ Exception -> 0x002f }
            goto L_0x0037
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            java.lang.Class<com.chartboost.sdk.Model.c> r3 = com.chartboost.sdk.Model.c.class
            java.lang.String r4 = "onClick"
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r3, (java.lang.String) r4, (java.lang.Exception) r0)
        L_0x0037:
            boolean r0 = r6.x
            if (r0 == 0) goto L_0x003c
            return r1
        L_0x003c:
            r0 = 1
            r6.x = r0
            r6.z = r1
            r6.a(r2, r7)
            return r0
        L_0x0045:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Model.c.a(org.json.JSONObject):boolean");
    }

    public void a(CBError.CBImpressionError cBImpressionError) {
        this.k.a(this, cBImpressionError);
    }

    public void a(Runnable runnable) {
        this.D = runnable;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject) {
        e eVar;
        d c2;
        Handler handler = this.f;
        com.chartboost.sdk.impl.c cVar = this.f2409a;
        cVar.getClass();
        handler.post(new c.a(1, this.m, (CBError.CBImpressionError) null));
        if (b() && this.l == 2 && (c2 = this.g.c()) != null) {
            c2.b(this);
        }
        if (!s.a().a((CharSequence) str)) {
            aj ajVar = new aj("/api/click", this.d, this.e, 2, (aj.a) null);
            if (!this.p.f.isEmpty()) {
                ajVar.a("ad_id", (Object) this.p.f);
            }
            if (!this.p.m.isEmpty()) {
                ajVar.a("to", (Object) this.p.m);
            }
            if (!this.p.g.isEmpty()) {
                ajVar.a("cgn", (Object) this.p.g);
            }
            if (!this.p.h.isEmpty()) {
                ajVar.a(VisionDataDBAdapter.VisionDataColumns.COLUMN_CREATIVE, (Object) this.p.h);
            }
            int i2 = this.n;
            if (i2 == 1 || i2 == 2) {
                if (this.p.b != 0 || k() == null) {
                    eVar = (this.p.b != 1 || k() == null) ? null : (bf) u();
                } else {
                    eVar = (v) u();
                }
                if (eVar != null) {
                    float k2 = eVar.k();
                    float j2 = eVar.j();
                    CBLogging.a(c.class.getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", new Object[]{Float.valueOf(j2), Float.valueOf(k2)}));
                    float f2 = j2 / 1000.0f;
                    ajVar.a("total_time", (Object) Float.valueOf(f2));
                    if (k2 <= 0.0f) {
                        ajVar.a("playback_time", (Object) Float.valueOf(f2));
                    } else {
                        ajVar.a("playback_time", (Object) Float.valueOf(k2 / 1000.0f));
                    }
                }
            }
            if (jSONObject != null) {
                ajVar.a("click_coordinates", (Object) jSONObject);
            }
            ajVar.a("location", (Object) this.m);
            if (x()) {
                ajVar.a("retarget_reinstall", (Object) Boolean.valueOf(y()));
            }
            this.w = ajVar;
            this.h.a(this, str, (aj) null);
        } else {
            this.h.a(this, false, str, CBError.CBClickError.URI_INVALID, (aj) null);
        }
        this.e.c(this.f2409a.a(this.p.b), this.m, o());
    }
}
