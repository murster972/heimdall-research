package com.chartboost.sdk.Model;

import android.os.Build;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.b;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.o;
import com.chartboost.sdk.impl.s;
import com.facebook.react.uimanager.ViewProps;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {
    public final boolean A;
    public final int B;
    public final boolean C;
    public final int D;
    public final boolean E;
    public final String F;
    public final String G;
    public final String H;
    public final String I;
    public final boolean J;
    public final boolean K;
    public final boolean L;

    /* renamed from: a  reason: collision with root package name */
    public final String f2410a;
    public final boolean b;
    public final boolean c;
    public final List<String> d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final int i;
    public final boolean j;
    public final boolean k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public final boolean o;
    public final boolean p;
    public final boolean q;
    public final boolean r;
    public final long s;
    public final int t;
    public final int u;
    public final int v;
    public final int w;
    public final List<String> x;
    public final boolean y;
    public final boolean z;

    public e(JSONObject jSONObject) {
        boolean z2;
        JSONObject optJSONObject;
        JSONObject jSONObject2 = jSONObject;
        this.f2410a = jSONObject2.optString("configVariant");
        this.b = jSONObject2.optBoolean("prefetchDisable");
        this.c = jSONObject2.optBoolean("publisherDisable");
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject2.optJSONArray("invalidateFolderList");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                String optString = optJSONArray.optString(i2);
                if (!optString.isEmpty()) {
                    arrayList.add(optString);
                }
            }
        }
        this.d = Collections.unmodifiableList(arrayList);
        JSONObject optJSONObject2 = jSONObject2.optJSONObject("native");
        optJSONObject2 = optJSONObject2 == null ? new JSONObject() : optJSONObject2;
        boolean z3 = true;
        this.e = optJSONObject2.optBoolean(ViewProps.ENABLED, true);
        this.f = optJSONObject2.optBoolean("inplayEnabled", true);
        this.g = optJSONObject2.optBoolean("interstitialEnabled", true);
        this.h = optJSONObject2.optBoolean("lockOrientation");
        this.i = optJSONObject2.optInt("prefetchSession", 3);
        this.j = optJSONObject2.optBoolean("rewardVideoEnabled", true);
        JSONObject optJSONObject3 = jSONObject2.optJSONObject("trackingLevels");
        optJSONObject3 = optJSONObject3 == null ? new JSONObject() : optJSONObject3;
        this.k = optJSONObject3.optBoolean("critical", true);
        this.r = optJSONObject3.optBoolean("includeStackTrace", true);
        this.l = optJSONObject3.optBoolean("error");
        this.m = optJSONObject3.optBoolean("debug");
        this.n = optJSONObject3.optBoolean("session");
        this.o = optJSONObject3.optBoolean("system");
        this.p = optJSONObject3.optBoolean("timing");
        this.q = optJSONObject3.optBoolean("user");
        this.s = jSONObject2.optLong("getAdRetryBaseMs", b.b);
        this.t = jSONObject2.optInt("getAdRetryMaxBackoffExponent", 5);
        JSONObject optJSONObject4 = jSONObject2.optJSONObject("webview");
        optJSONObject4 = optJSONObject4 == null ? new JSONObject() : optJSONObject4;
        boolean z4 = !"Amazon".equalsIgnoreCase(Build.MANUFACTURER) || Build.VERSION.SDK_INT >= 21;
        this.u = optJSONObject4.optInt("cacheMaxBytes", 104857600);
        int i3 = 10;
        int optInt = optJSONObject4.optInt("cacheMaxUnits", 10);
        this.v = optInt > 0 ? optInt : i3;
        String str = "rewardVideoEnabled";
        this.w = (int) TimeUnit.SECONDS.toDays((long) optJSONObject4.optInt("cacheTTLs", b.f2393a));
        ArrayList arrayList2 = new ArrayList();
        JSONArray optJSONArray2 = optJSONObject4.optJSONArray("directories");
        if (optJSONArray2 != null) {
            int length2 = optJSONArray2.length();
            for (int i4 = 0; i4 < length2; i4++) {
                String optString2 = optJSONArray2.optString(i4);
                if (!optString2.isEmpty()) {
                    arrayList2.add(optString2);
                }
            }
        }
        this.x = Collections.unmodifiableList(arrayList2);
        this.y = z4 && optJSONObject4.optBoolean(ViewProps.ENABLED, a());
        this.z = optJSONObject4.optBoolean("inplayEnabled", true);
        this.A = optJSONObject4.optBoolean("interstitialEnabled", true);
        int optInt2 = optJSONObject4.optInt("invalidatePendingImpression", 3);
        this.B = optInt2 <= 0 ? 3 : optInt2;
        this.C = optJSONObject4.optBoolean("lockOrientation", true);
        this.D = optJSONObject4.optInt("prefetchSession", 3);
        this.E = optJSONObject4.optBoolean(str, true);
        this.F = optJSONObject4.optString(MediationMetaData.KEY_VERSION, "v2");
        this.G = String.format("%s/%s%s", new Object[]{"webview", this.F, "/interstitial/get"});
        this.H = String.format("%s/%s/%s", new Object[]{"webview", this.F, "prefetch"});
        this.I = String.format("%s/%s%s", new Object[]{"webview", this.F, "/reward/get"});
        ArrayList arrayList3 = new ArrayList();
        boolean z5 = i.x != Chartboost.CBPIDataUseConsent.NO_BEHAVIORAL;
        z3 = i.x == Chartboost.CBPIDataUseConsent.NO_BEHAVIORAL ? false : z3;
        JSONObject optJSONObject5 = jSONObject2.optJSONObject("certificationProviders");
        if (optJSONObject5 == null || (optJSONObject = optJSONObject5.optJSONObject("moat")) == null) {
            z2 = false;
        } else {
            arrayList3.add("moat");
            z2 = optJSONObject.optBoolean("loggingEnabled", false);
            z5 = optJSONObject.optBoolean("locationEnabled", z5);
            z3 = optJSONObject.optBoolean("idfaCollectionEnabled", z3);
        }
        this.J = z2;
        this.K = z5;
        this.L = z3;
        o.a((List<String>) arrayList3);
    }

    private static boolean a() {
        int[] iArr = {4, 4, 2};
        String d2 = s.a().d();
        if (d2 != null && d2.length() > 0) {
            String[] split = d2.replaceAll("[^\\d.]", "").split("\\.");
            int i2 = 0;
            while (i2 < split.length && i2 < iArr.length) {
                try {
                    if (Integer.valueOf(split[i2]).intValue() > iArr[i2]) {
                        return true;
                    }
                    if (Integer.valueOf(split[i2]).intValue() < iArr[i2]) {
                        return false;
                    }
                    i2++;
                } catch (NumberFormatException unused) {
                }
            }
        }
        return false;
    }
}
