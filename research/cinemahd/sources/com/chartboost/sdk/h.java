package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.webkit.WebView;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.impl.ah;
import com.chartboost.sdk.impl.ai;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.ak;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.ao;
import com.chartboost.sdk.impl.ap;
import com.chartboost.sdk.impl.aw;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.e;
import com.chartboost.sdk.impl.l;
import com.chartboost.sdk.impl.m;
import com.chartboost.sdk.impl.o;
import com.chartboost.sdk.impl.s;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class h {
    private static h v;

    /* renamed from: a  reason: collision with root package name */
    public final Executor f2426a;
    final l b;
    public final d c;
    public final e d;
    public final c e;
    final e f;
    final c g;
    public final ah h;
    final m i;
    public final ap j;
    final e k;
    final c l;
    public final AtomicReference<com.chartboost.sdk.Model.e> m;
    final SharedPreferences n;
    public final com.chartboost.sdk.Tracking.a o;
    public final Handler p;
    public final c q;
    public final ak r;
    boolean s = true;
    boolean t = false;
    boolean u = true;
    private final s w;

    public class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final int f2428a;
        String b = null;
        boolean c = false;
        boolean d = false;

        a(int i) {
            this.f2428a = i;
        }

        public void run() {
            try {
                int i = this.f2428a;
                if (i == 0) {
                    h.this.d();
                } else if (i == 1) {
                    i.t = this.c;
                } else if (i == 2) {
                    i.v = this.d;
                    if (!this.d || !h.f()) {
                        h.this.i.b();
                    } else {
                        h.this.i.a();
                    }
                } else if (i == 3) {
                    aj ajVar = new aj("api/install", h.this.j, h.this.o, 2, (aj.a) null);
                    ajVar.l = true;
                    h.this.h.a(ajVar);
                    Executor executor = h.this.f2426a;
                    e eVar = h.this.d;
                    eVar.getClass();
                    executor.execute(new e.a(0, (String) null, (f) null, (CBError.CBImpressionError) null));
                    Executor executor2 = h.this.f2426a;
                    e eVar2 = h.this.f;
                    eVar2.getClass();
                    executor2.execute(new e.a(0, (String) null, (f) null, (CBError.CBImpressionError) null));
                    Executor executor3 = h.this.f2426a;
                    e eVar3 = h.this.k;
                    eVar3.getClass();
                    executor3.execute(new e.a(0, (String) null, (f) null, (CBError.CBImpressionError) null));
                    h.this.f2426a.execute(new a(4));
                    h.this.u = false;
                } else if (i == 4) {
                    h.this.i.a();
                } else if (i == 5) {
                    if (i.c != null) {
                        i.c.didFailToLoadMoreApps(this.b, CBError.CBImpressionError.END_POINT_DISABLED);
                    }
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(a.class, "run (" + this.f2428a + ")", e2);
            }
        }
    }

    h(Activity activity, String str, String str2, s sVar, ScheduledExecutorService scheduledExecutorService, Handler handler, Executor executor) {
        JSONObject jSONObject;
        s sVar2 = sVar;
        ScheduledExecutorService scheduledExecutorService2 = scheduledExecutorService;
        Handler handler2 = handler;
        g a2 = g.a();
        Context applicationContext = activity.getApplicationContext();
        this.c = (d) a2.a(new d(applicationContext));
        ai aiVar = (ai) a2.a(new ai());
        i iVar = (i) a2.a(new i());
        ai aiVar2 = aiVar;
        this.h = (ah) a2.a(new ah(scheduledExecutorService, (ao) a2.a(new ao()), aiVar, iVar, handler, executor));
        SharedPreferences a3 = a(applicationContext);
        try {
            jSONObject = new JSONObject(a3.getString("config", "{}"));
        } catch (Exception e2) {
            CBLogging.b("Sdk", "Unable to process config");
            e2.printStackTrace();
            jSONObject = new JSONObject();
        }
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference = new AtomicReference<>((Object) null);
        if (!b.a(atomicReference, jSONObject, a3)) {
            atomicReference.set(new com.chartboost.sdk.Model.e(new JSONObject()));
        }
        this.w = sVar2;
        this.f2426a = scheduledExecutorService2;
        this.m = atomicReference;
        this.n = a3;
        this.p = handler2;
        f fVar = new f(sVar2, applicationContext, atomicReference);
        if (!atomicReference.get().y) {
            i.w = "";
        } else {
            a(applicationContext, (WebView) null, a3);
        }
        ai aiVar3 = aiVar2;
        f fVar2 = fVar;
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference2 = atomicReference;
        SharedPreferences sharedPreferences = a3;
        this.j = (ap) a2.a(new ap(applicationContext, str, this.c, aiVar3, atomicReference2, a3, iVar));
        this.o = (com.chartboost.sdk.Tracking.a) a2.a(new com.chartboost.sdk.Tracking.a(atomicReference));
        this.b = (l) a2.a(new l(scheduledExecutorService, fVar2, this.h, aiVar3, atomicReference2, iVar, this.o));
        d dVar = (d) a2.a(new d((aw) g.a().a(new aw(handler2)), this.b, atomicReference, handler2));
        ai aiVar4 = aiVar2;
        this.r = (ak) a2.a(new ak(scheduledExecutorService2, this.h, aiVar4, handler2));
        Context context = applicationContext;
        g gVar = a2;
        this.q = (c) gVar.a(new c(activity, aiVar4, this, this.o, handler, dVar));
        f fVar3 = fVar2;
        al alVar = (al) gVar.a(new al(fVar3));
        this.e = c.c();
        this.g = c.a();
        this.l = c.b();
        ScheduledExecutorService scheduledExecutorService3 = scheduledExecutorService;
        g gVar2 = gVar;
        f fVar4 = fVar3;
        ai aiVar5 = aiVar4;
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference3 = atomicReference;
        SharedPreferences sharedPreferences2 = sharedPreferences;
        i iVar2 = iVar;
        Handler handler3 = handler;
        this.d = (e) gVar2.a(new e(this.e, scheduledExecutorService3, this.b, fVar4, this.h, aiVar4, this.j, atomicReference3, sharedPreferences2, iVar2, this.o, handler3, this.q, this.r, dVar, alVar));
        this.f = (e) gVar2.a(new e(this.g, scheduledExecutorService, this.b, fVar3, this.h, aiVar5, this.j, atomicReference, sharedPreferences, iVar, this.o, handler, this.q, this.r, dVar, alVar));
        this.k = (e) gVar2.a(new e(this.l, scheduledExecutorService, this.b, fVar3, this.h, aiVar5, this.j, atomicReference, sharedPreferences, iVar, this.o, handler, this.q, this.r, dVar, alVar));
        this.i = (m) gVar2.a(new m(this.b, fVar3, this.h, this.j, this.o, atomicReference));
        i.m = context;
        i.k = str;
        i.l = str2;
        SharedPreferences sharedPreferences3 = sharedPreferences;
        if (!sharedPreferences3.contains("cbLimitTrack") || sharedPreferences3.contains("cbGDPR")) {
            i.x = Chartboost.CBPIDataUseConsent.valueOf(sharedPreferences3.getInt("cbGDPR", i.x.getValue()));
        } else {
            i.x = sharedPreferences3.getBoolean("cbLimitTrack", false) ? Chartboost.CBPIDataUseConsent.NO_BEHAVIORAL : Chartboost.CBPIDataUseConsent.UNKNOWN;
        }
        aiVar5.a(i.m);
        if (s.a().a(19)) {
            o.a(activity.getApplication(), atomicReference.get().J, !atomicReference.get().K, !atomicReference.get().L);
        }
    }

    public static h a() {
        return v;
    }

    static boolean f() {
        h a2 = a();
        if (a2 == null || !a2.m.get().c) {
            return true;
        }
        try {
            throw new Exception("Chartboost Integration Warning: your account has been disabled for this session. This app has no active publishing campaigns, please create a publishing campaign in the Chartboost dashboard and wait at least 30 minutes to re-enable. If you need assistance, please visit http://chartboo.st/publishing .");
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private void g() {
        this.o.a();
        if (!this.u) {
            a((Runnable) new a(3));
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (i.m == null) {
            CBLogging.b("Sdk", "The context must be set through the Chartboost method onCreate() before calling startSession().");
        } else {
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.p.postDelayed(new a(0), 500);
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.o.b();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (!this.t) {
            a aVar = i.c;
            if (aVar != null) {
                aVar.didInitialize();
            }
            this.t = true;
        }
    }

    static void a(h hVar) {
        v = hVar;
    }

    private static SharedPreferences a(Context context) {
        return context.getSharedPreferences("cbPrefs", 0);
    }

    public static void b(Runnable runnable) {
        s a2 = s.a();
        if (!a2.e()) {
            a2.f2494a.post(runnable);
        } else {
            runnable.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final Runnable runnable) {
        this.s = true;
        aj ajVar = new aj("/api/config", this.j, this.o, 1, new aj.a() {
            public void a(aj ajVar, JSONObject jSONObject) {
                h.this.s = false;
                JSONObject a2 = com.chartboost.sdk.Libraries.e.a(jSONObject, "response");
                if (a2 != null) {
                    h hVar = h.this;
                    if (b.a(hVar.m, a2, hVar.n)) {
                        h.this.n.edit().putString("config", a2.toString()).apply();
                    }
                }
                Runnable runnable = runnable;
                if (runnable != null) {
                    runnable.run();
                }
                if (!h.this.t) {
                    a aVar = i.c;
                    if (aVar != null) {
                        aVar.didInitialize();
                    }
                    h.this.t = true;
                }
            }

            public void a(aj ajVar, CBError cBError) {
                h.this.s = false;
                Runnable runnable = runnable;
                if (runnable != null) {
                    runnable.run();
                }
                if (!h.this.t) {
                    a aVar = i.c;
                    if (aVar != null) {
                        aVar.didInitialize();
                    }
                    h.this.t = true;
                }
            }
        });
        ajVar.l = true;
        this.h.a(ajVar);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        if (this.w.a(23)) {
            b.a((Context) activity);
        }
        if (!this.u && !this.q.e()) {
            this.b.c();
        }
    }

    public static void a(Context context, WebView webView, SharedPreferences sharedPreferences) {
        String str;
        String str2 = i.w;
        if (webView == null) {
            try {
                if (!sharedPreferences.contains("user_agent")) {
                    str = new WebView(context.getApplicationContext()).getSettings().getUserAgentString();
                } else {
                    str = sharedPreferences.getString("user_agent", i.w);
                }
            } catch (Exception unused) {
            }
        } else {
            str = webView.getSettings().getUserAgentString();
        }
        str2 = str;
        i.w = str2;
        sharedPreferences.edit().putString("user_agent", str2).apply();
    }

    static void a(Context context, Chartboost.CBPIDataUseConsent cBPIDataUseConsent) {
        i.x = cBPIDataUseConsent;
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            a2.edit().putInt("cbGDPR", cBPIDataUseConsent.getValue()).apply();
        }
    }
}
