package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.impl.s;
import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public final class b {
    static void a(String str) {
        if (i.d == null) {
            CBLogging.b("CBConfig", "Set a valid CBFramework first");
        } else if (TextUtils.isEmpty(str)) {
            CBLogging.b("CBConfig", "Invalid Version String");
        } else {
            i.b = str;
        }
    }

    static boolean b() {
        try {
            if (h.a() == null) {
                throw new Exception("SDK Initialization error. SDK seems to be not initialized properly, check for any integration issues");
            } else if (i.m == null) {
                throw new Exception("SDK Initialization error. Activity context seems to be not initialized properly, host activity or application context is being sent as null");
            } else if (TextUtils.isEmpty(i.k)) {
                throw new Exception("SDK Initialization error. AppId is missing");
            } else if (!TextUtils.isEmpty(i.l)) {
                return true;
            } else {
                throw new Exception("SDK Initialization error. AppSignature is missing");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean c() {
        h a2 = h.a();
        if (a2 == null) {
            return false;
        }
        if (a2.q.d != null) {
            return true;
        }
        try {
            throw new Exception("Chartboost Weak Activity reference is null");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(AtomicReference<e> atomicReference, JSONObject jSONObject, SharedPreferences sharedPreferences) {
        try {
            atomicReference.set(new e(jSONObject));
            return true;
        } catch (Exception e) {
            a.a(b.class, "updateConfig", e);
            return false;
        }
    }

    public static boolean a() {
        return b() && c();
    }

    static boolean a(Activity activity) {
        if (activity != null) {
            return true;
        }
        try {
            throw new Exception("Invalid activity context: Host Activity object is null, Please send a valid activity object");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(Context context) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent(context, CBImpressionActivity.class), 0);
        if (queryIntentActivities.isEmpty()) {
            return false;
        }
        ActivityInfo activityInfo = queryIntentActivities.get(0).activityInfo;
        int i = activityInfo.flags;
        if ((i & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || (i & 32) == 0) {
            return false;
        }
        int i2 = activityInfo.configChanges;
        if ((i2 & 128) == 0 || (i2 & 32) == 0 || (i2 & ByteConstants.KB) == 0) {
            return false;
        }
        return true;
    }

    public static boolean a(Context context) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        if (context != null) {
            try {
                if (s.a().a(23)) {
                    i5 = context.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
                    i2 = context.checkSelfPermission("android.permission.ACCESS_NETWORK_STATE");
                    i3 = context.checkSelfPermission("android.permission.INTERNET");
                    i4 = context.checkSelfPermission("android.permission.READ_PHONE_STATE");
                    i = context.checkSelfPermission("android.permission.ACCESS_WIFI_STATE");
                } else {
                    i5 = context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
                    i2 = context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE");
                    i3 = context.checkCallingOrSelfPermission("android.permission.INTERNET");
                    i4 = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
                    i = context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE");
                }
                i.n = i5 != 0;
                i.o = i3 != 0;
                i.p = i2 != 0;
                i.q = i4 != 0;
                i.r = i != 0;
                if (i.o) {
                    throw new RuntimeException("Please add the permission : android.permission.INTERNET in your android manifest.xml");
                } else if (!i.p) {
                    return true;
                } else {
                    throw new RuntimeException("Please add the permission : android.permission.ACCESS_NETWORK_STATE in your android manifest.xml");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            throw new RuntimeException("Invalid activity context passed during intitalization");
        }
    }

    public static String a(e eVar) {
        return !eVar.y ? "native" : "web";
    }
}
