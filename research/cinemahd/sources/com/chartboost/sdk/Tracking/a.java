package com.chartboost.sdk.Tracking;

import android.text.TextUtils;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.b;
import com.chartboost.sdk.Model.e;
import com.facebook.react.uimanager.ViewProps;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class a implements b {
    private static a d;
    private static final Long g = Long.valueOf(TimeUnit.MINUTES.toMillis(5));
    private final AtomicReference<e> e;
    private boolean f = false;
    private long h = (System.currentTimeMillis() - g.longValue());

    public a(AtomicReference<e> atomicReference) {
        d = this;
        this.e = atomicReference;
    }

    private void c() {
        if (this.e.get().n) {
            a("session", ViewProps.END, (String) null, (String) null, (String) null, (String) null, (JSONObject) null, false);
            a("did-become-active");
        }
    }

    public void a() {
        a(ViewProps.START);
        a("did-become-active");
    }

    public void b() {
        c();
    }

    public void d(String str, String str2, String str3) {
        if (this.e.get().o) {
            a("ad-close", str, str2, str3, (String) null, false);
        }
    }

    public void e(String str, String str2, String str3) {
        if (this.e.get().o) {
            a("ad-dismiss", str, str2, str3, (String) null, false);
        }
    }

    public void b(String str, String str2, String str3) {
        if (this.e.get().o) {
            a("ad-show", str, str2, str3, (String) null, false);
        }
    }

    private void a(String str) {
        if (this.e.get().n) {
            a("session", str, (String) null, (String) null, (String) null, false);
        }
    }

    public void d(String str, String str2) {
        if (this.e.get().o) {
            a("playback-start", str, str2, (String) null, (String) null, false);
        }
    }

    public void e(String str, String str2) {
        if (this.e.get().o) {
            a("playback-stop", str, str2, (String) null, (String) null, false);
        }
    }

    public void b(String str, String str2, String str3, String str4) {
        if (this.e.get().o) {
            if (TextUtils.isEmpty(str3)) {
                str3 = "empty-adid";
            }
            a("ad-warning", str, str2, str3, str4, false);
        }
    }

    public void c(String str, String str2, String str3) {
        if (this.e.get().o) {
            a("ad-click", str, str2, str3, (String) null, false);
        }
    }

    public void a(String str, String str2, String str3, String str4) {
        if (this.e.get().o) {
            a("webview-track", str, str2, str3, str4, (String) null, (JSONObject) null, false);
        }
    }

    public void c(String str, String str2) {
        if (this.e.get().o) {
            a("replay", str, str2, (String) null, (String) null, false);
        }
    }

    public void a(JSONObject jSONObject) {
        e eVar = this.e.get();
        if (eVar.o) {
            a("folder", com.chartboost.sdk.b.a(eVar), (String) null, (String) null, (String) null, (String) null, jSONObject, false);
        }
    }

    public void b(String str, String str2) {
        if (this.e.get().o) {
            a("playback-complete", str, str2, (String) null, (String) null, false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0065, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(java.lang.Class r15, java.lang.String r16, java.lang.Exception r17) {
        /*
            r14 = this;
            r10 = r14
            monitor-enter(r14)
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.Model.e> r0 = r10.e     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0008
            monitor-exit(r14)
            return
        L_0x0008:
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.Model.e> r0 = r10.e     // Catch:{ all -> 0x0066 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0066 }
            com.chartboost.sdk.Model.e r0 = (com.chartboost.sdk.Model.e) r0     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x0064
            boolean r1 = r0.k     // Catch:{ all -> 0x0066 }
            if (r1 == 0) goto L_0x0064
            boolean r1 = r10.f     // Catch:{ all -> 0x0066 }
            if (r1 != 0) goto L_0x0064
            r1 = 1
            r10.f = r1     // Catch:{ all -> 0x0066 }
            r11 = 0
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x005c }
            long r1 = r10.h     // Catch:{ Exception -> 0x005c }
            long r1 = r12 - r1
            java.lang.Long r3 = g     // Catch:{ Exception -> 0x005c }
            long r3 = r3.longValue()     // Catch:{ Exception -> 0x005c }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 < 0) goto L_0x0057
            boolean r0 = r0.r     // Catch:{ Exception -> 0x005c }
            if (r0 == 0) goto L_0x0039
            java.lang.String r0 = android.util.Log.getStackTraceString(r17)     // Catch:{ Exception -> 0x005c }
            goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            r7 = r0
            java.lang.String r2 = "exception"
            java.lang.String r3 = r15.getName()     // Catch:{ Exception -> 0x005c }
            java.lang.Class r0 = r17.getClass()     // Catch:{ Exception -> 0x005c }
            java.lang.String r5 = r0.getName()     // Catch:{ Exception -> 0x005c }
            java.lang.String r6 = r17.getMessage()     // Catch:{ Exception -> 0x005c }
            r8 = 0
            r9 = 1
            r1 = r14
            r4 = r16
            r1.a(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x005c }
            r10.h = r12     // Catch:{ Exception -> 0x005c }
        L_0x0057:
            r10.f = r11     // Catch:{ all -> 0x0066 }
            goto L_0x0064
        L_0x005a:
            r0 = move-exception
            goto L_0x0061
        L_0x005c:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x005a }
            goto L_0x0057
        L_0x0061:
            r10.f = r11     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x0064:
            monitor-exit(r14)
            return
        L_0x0066:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Tracking.a.b(java.lang.Class, java.lang.String, java.lang.Exception):void");
    }

    public void a(String str, String str2, String str3) {
        if (this.e.get().o) {
            a("load", str, str2, str3, (String) null, false);
        }
    }

    public void a(String str, String str2, String str3, String str4, boolean z) {
        if (this.e.get().o) {
            if (TextUtils.isEmpty(str3)) {
                str3 = "empty-adid";
            }
            a("ad-error", str, str2, str3, str4, z);
        }
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        if (this.e.get().l) {
            String str8 = str5;
            a("ad-unit-error", str, str2, str3, str4, (String) null, com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("adId", (Object) str5), com.chartboost.sdk.Libraries.e.a("location", (Object) str6), com.chartboost.sdk.Libraries.e.a((String) AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE, (Object) str7)), true);
        }
    }

    public void a(String str, String str2) {
        e eVar = this.e.get();
        if (eVar.o) {
            a("download-asset-start", com.chartboost.sdk.b.a(eVar), str, str2, (String) null, (String) null, (JSONObject) null, false);
        }
    }

    public void a(String str, String str2, long j, long j2, long j3) {
        if (this.e.get().p) {
            a("download-asset-failure", str, str2, (String) null, (String) null, (String) null, com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("processingMs", (Object) Long.valueOf(j)), com.chartboost.sdk.Libraries.e.a("getResponseCodeMs", (Object) Long.valueOf(j2)), com.chartboost.sdk.Libraries.e.a("readDataMs", (Object) Long.valueOf(j3))), false);
        }
    }

    public void a(String str, long j, long j2, long j3) {
        if (this.e.get().p) {
            a("download-asset-success", str, (String) null, (String) null, (String) null, (String) null, com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("processingMs", (Object) Long.valueOf(j)), com.chartboost.sdk.Libraries.e.a("getResposeCodeMs", (Object) Long.valueOf(j2)), com.chartboost.sdk.Libraries.e.a("readDataMs", (Object) Long.valueOf(j3))), false);
        }
    }

    public static void a(Class cls, String str, Exception exc) {
        exc.printStackTrace();
        a aVar = d;
        if (aVar != null) {
            aVar.b(cls, str, exc);
        }
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, JSONObject jSONObject) {
        if (this.e.get().o) {
            a(str, str2, str3, str4, str5, str6, jSONObject, false);
        }
    }

    private void a(String str, String str2, String str3, String str4, String str5, boolean z) {
        a(str, str2, str3, str4, str5, (String) null, new JSONObject(), z);
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, JSONObject jSONObject, boolean z) {
        if (this.e.get().o) {
            if (str == null) {
                str = "unknown event";
            }
            CBLogging.a("CBTrack", str);
        }
    }
}
