package com.chartboost.sdk.impl;

import android.os.Handler;
import com.chartboost.sdk.Libraries.i;
import java.util.concurrent.Executor;

public class ah {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f2442a;
    private final Executor b;
    private final ao c;
    private final ai d;
    private final i e;
    private final Handler f;

    public ah(Executor executor, ao aoVar, ai aiVar, i iVar, Handler handler, Executor executor2) {
        this.f2442a = executor2;
        this.b = executor;
        this.c = aoVar;
        this.d = aiVar;
        this.e = iVar;
        this.f = handler;
    }

    public <T> void a(ad<T> adVar) {
        this.f2442a.execute(new an(this.b, this.c, this.d, this.e, this.f, adVar));
    }
}
