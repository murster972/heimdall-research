package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.c;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class aj extends ad<JSONObject> {

    /* renamed from: a  reason: collision with root package name */
    public final JSONObject f2445a = new JSONObject();
    public final a k;
    public boolean l = false;
    protected final ap m;
    private final String n;
    private String o;
    private final com.chartboost.sdk.Tracking.a p;

    public interface a {
        void a(aj ajVar, CBError cBError);

        void a(aj ajVar, JSONObject jSONObject);
    }

    public aj(String str, ap apVar, com.chartboost.sdk.Tracking.a aVar, int i, a aVar2) {
        super("POST", a(str), i, (File) null);
        this.n = str;
        this.m = apVar;
        this.p = aVar;
        this.k = aVar2;
    }

    public void b(String str) {
        this.o = str;
    }

    /* access modifiers changed from: protected */
    public void c() {
        a("app", (Object) this.m.s);
        a("model", (Object) this.m.f);
        a("device_type", (Object) this.m.t);
        a("actual_device_type", (Object) this.m.u);
        a("os", (Object) this.m.g);
        a("country", (Object) this.m.h);
        a("language", (Object) this.m.i);
        a("sdk", (Object) this.m.l);
        a("user_agent", (Object) i.w);
        a(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, (Object) String.valueOf(TimeUnit.MILLISECONDS.toSeconds(this.m.e.a())));
        boolean z = false;
        a("session", (Object) Integer.valueOf(this.m.d.getInt("cbPrefSessionCount", 0)));
        a("reachability", (Object) Integer.valueOf(this.m.b.a()));
        a("scale", (Object) this.m.r);
        a("is_portrait", (Object) Boolean.valueOf(CBUtility.a(CBUtility.a())));
        a("bundle", (Object) this.m.j);
        a("bundle_id", (Object) this.m.k);
        a("carrier", (Object) this.m.v);
        a("custom_id", (Object) i.f2429a);
        a("mediation", (Object) i.h);
        if (i.d != null) {
            a("framework_version", (Object) i.f);
            a("wrapper_version", (Object) i.b);
        }
        a("rooted_device", (Object) Boolean.valueOf(this.m.w));
        a("timezone", (Object) this.m.x);
        a("mobile_network", (Object) this.m.y);
        a("dw", (Object) this.m.o);
        a("dh", (Object) this.m.p);
        a("dpi", (Object) this.m.q);
        a("w", (Object) this.m.m);
        a("h", (Object) this.m.n);
        a("commit_hash", (Object) "ea5c9878e5dca6c95016765177cbd146c39a21f7");
        d.a a2 = this.m.f2451a.a();
        a(InterpolationAnimatedNode.EXTRAPOLATE_TYPE_IDENTITY, (Object) a2.b);
        int i = a2.f2395a;
        if (i != -1) {
            if (i == 1) {
                z = true;
            }
            a("limit_ad_tracking", (Object) Boolean.valueOf(z));
        }
        a("pidatauseconsent", (Object) Integer.valueOf(i.x.getValue()));
        String str = this.m.c.get().f2410a;
        if (!s.a().a((CharSequence) str)) {
            a("config_variant", (Object) str);
        }
        a("certification_providers", (Object) o.e());
    }

    public String d() {
        return e();
    }

    public String e() {
        String str = "/";
        if (this.n == null) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        if (this.n.startsWith(str)) {
            str = "";
        }
        sb.append(str);
        sb.append(this.n);
        return sb.toString();
    }

    public static String a(String str) {
        Object[] objArr = new Object[3];
        objArr[0] = "https://live.chartboost.com";
        String str2 = "/";
        if (str != null && str.startsWith(str2)) {
            str2 = "";
        }
        objArr[1] = str2;
        if (str == null) {
            str = "";
        }
        objArr[2] = str;
        return String.format("%s%s%s", objArr);
    }

    public void a(String str, Object obj) {
        e.a(this.f2445a, str, obj);
    }

    private void a(ag agVar, CBError cBError) {
        Object obj;
        String str;
        e.a[] aVarArr = new e.a[5];
        aVarArr[0] = e.a("endpoint", (Object) e());
        String str2 = "None";
        if (agVar == null) {
            obj = str2;
        } else {
            obj = Integer.valueOf(agVar.f2441a);
        }
        aVarArr[1] = e.a("statuscode", obj);
        if (cBError == null) {
            str = str2;
        } else {
            str = cBError.a().toString();
        }
        aVarArr[2] = e.a("error", (Object) str);
        if (cBError != null) {
            str2 = cBError.b();
        }
        aVarArr[3] = e.a("errorDescription", (Object) str2);
        aVarArr[4] = e.a("retryCount", (Object) 0);
        this.p.a("request_manager", "request", cBError == null ? "success" : "failure", (String) null, (String) null, (String) null, e.a(aVarArr));
    }

    public ae a() {
        c();
        String jSONObject = this.f2445a.toString();
        String str = i.k;
        String str2 = i.l;
        String b = c.b(c.a(String.format(Locale.US, "%s %s\n%s\n%s", new Object[]{this.b, d(), str2, jSONObject}).getBytes()));
        HashMap hashMap = new HashMap();
        hashMap.put(TheTvdb.HEADER_ACCEPT, TraktV2.CONTENT_TYPE_JSON);
        hashMap.put("X-Chartboost-Client", CBUtility.b());
        hashMap.put("X-Chartboost-API", "7.3.1");
        hashMap.put("X-Chartboost-App", str);
        hashMap.put("X-Chartboost-Signature", b);
        return new ae(hashMap, jSONObject.getBytes(), TraktV2.CONTENT_TYPE_JSON);
    }

    public af<JSONObject> a(ag agVar) {
        try {
            if (agVar.b == null) {
                return af.a(new CBError(CBError.a.INVALID_RESPONSE, "Response is not a valid json object"));
            }
            JSONObject jSONObject = new JSONObject(new String(agVar.b));
            CBLogging.c("CBRequest", "Request " + e() + " succeeded. Response code: " + agVar.f2441a + ", body: " + jSONObject.toString(4));
            if (this.l) {
                int optInt = jSONObject.optInt(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
                if (optInt == 404) {
                    return af.a(new CBError(CBError.a.HTTP_NOT_FOUND, "404 error from server"));
                }
                if (optInt < 200 || optInt > 299) {
                    String str = "Request failed due to status code " + optInt + " in message";
                    CBLogging.b("CBRequest", str);
                    return af.a(new CBError(CBError.a.UNEXPECTED_RESPONSE, str));
                }
            }
            return af.a(jSONObject);
        } catch (Exception e) {
            com.chartboost.sdk.Tracking.a.a((Class) getClass(), "parseServerResponse", e);
            return af.a(new CBError(CBError.a.MISCELLANEOUS, e.getLocalizedMessage()));
        }
    }

    public void a(JSONObject jSONObject, ag agVar) {
        a aVar = this.k;
        if (!(aVar == null || jSONObject == null)) {
            aVar.a(this, jSONObject);
        }
        if (this.p != null) {
            a(agVar, (CBError) null);
        }
    }

    public void a(CBError cBError, ag agVar) {
        if (cBError != null) {
            a aVar = this.k;
            if (aVar != null) {
                aVar.a(this, cBError);
            }
            if (this.p != null) {
                a(agVar, cBError);
            }
        }
    }
}
