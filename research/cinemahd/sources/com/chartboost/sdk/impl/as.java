package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.GoogleApiAvailability;

public class as {
    public static boolean a(Context context) {
        GoogleApiAvailability a2 = GoogleApiAvailability.a();
        int c = a2.c(context);
        if (c == 0) {
            return true;
        }
        if (!a2.c(c) || !(context instanceof Activity)) {
            return false;
        }
        a2.a((Activity) context, c, 9000).show();
        return true;
    }
}
