package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint({"ViewConstructor"})
public class t extends z {
    private LinearLayout b;
    private LinearLayout c;
    private ay d;
    private az e;
    private TextView f;
    private TextView g;

    public t(Context context, v vVar) {
        super(context, vVar);
    }

    /* access modifiers changed from: protected */
    public View a() {
        Context context = getContext();
        int round = Math.round(getContext().getResources().getDisplayMetrics().density * 6.0f);
        this.b = new LinearLayout(context);
        this.b.setOrientation(0);
        this.b.setGravity(17);
        this.c = new LinearLayout(context);
        this.c.setOrientation(1);
        this.c.setGravity(8388627);
        this.d = new ay(context);
        this.d.setPadding(round, round, round, round);
        if (this.f2506a.J.c()) {
            this.d.a(this.f2506a.J);
        }
        this.e = new az(context) {
            /* access modifiers changed from: protected */
            public void a(MotionEvent motionEvent) {
                t.this.f2506a.e().b(motionEvent.getX(), motionEvent.getY(), (float) super.getWidth(), (float) super.getHeight());
            }
        };
        this.e.setPadding(round, round, round, round);
        if (this.f2506a.K.c()) {
            this.e.a(this.f2506a.K);
        }
        this.f = new TextView(getContext());
        this.f.setTextColor(-15264491);
        this.f.setTypeface((Typeface) null, 1);
        this.f.setGravity(8388611);
        this.f.setPadding(round, round, round, round / 2);
        this.g = new TextView(getContext());
        this.g.setTextColor(-15264491);
        this.g.setTypeface((Typeface) null, 1);
        this.g.setGravity(8388611);
        this.g.setPadding(round, 0, round, round);
        this.f.setTextSize(2, 14.0f);
        this.g.setTextSize(2, 11.0f);
        this.c.addView(this.f);
        this.c.addView(this.g);
        this.b.addView(this.d);
        this.b.addView(this.c, new LinearLayout.LayoutParams(0, -2, 1.0f));
        this.b.addView(this.e);
        return this.b;
    }

    /* access modifiers changed from: protected */
    public int b() {
        return 72;
    }

    public void a(String str, String str2) {
        this.f.setText(str);
        this.g.setText(str2);
    }
}
