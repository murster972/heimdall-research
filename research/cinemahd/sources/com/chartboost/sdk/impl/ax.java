package com.chartboost.sdk.impl;

import android.content.Context;
import android.view.View;
import com.chartboost.sdk.Model.a;

public final class ax extends View {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2458a = false;

    public ax(Context context) {
        super(context);
        setFocusable(false);
        setBackgroundColor(-1442840576);
    }

    public void a(aw awVar, a aVar) {
        if (!this.f2458a) {
            awVar.a(true, (View) this, aVar);
            this.f2458a = true;
        }
    }
}
