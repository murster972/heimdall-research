package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.a;
import com.chartboost.sdk.i;
import com.facebook.common.util.UriUtil;
import com.google.ar.core.ImageMetadata;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Executor;

public class ak {

    /* renamed from: a  reason: collision with root package name */
    final ai f2446a;
    final Handler b;
    private final Executor c;
    private final ah d;

    public ak(Executor executor, ah ahVar, ai aiVar, Handler handler) {
        this.c = executor;
        this.d = ahVar;
        this.f2446a = aiVar;
        this.b = handler;
    }

    public void a(c cVar, boolean z, String str, CBError.CBClickError cBClickError, aj ajVar) {
        aj ajVar2;
        if (cVar != null) {
            cVar.x = false;
            if (cVar.b()) {
                cVar.l = 4;
            }
        }
        if (!z) {
            a aVar = i.c;
            if (aVar != null) {
                aVar.didFailToRecordClick(str, cBClickError);
            }
        } else if (cVar != null && (ajVar2 = cVar.w) != null) {
            this.d.a(ajVar2);
        } else if (ajVar != null) {
            this.d.a(ajVar);
        }
    }

    public void a(c cVar, String str, Activity activity, aj ajVar) {
        try {
            String scheme = new URI(str).getScheme();
            if (scheme == null) {
                a(cVar, false, str, CBError.CBClickError.URI_INVALID, ajVar);
            } else if (scheme.equals(UriUtil.HTTP_SCHEME) || scheme.equals(UriUtil.HTTPS_SCHEME)) {
                final String str2 = str;
                final c cVar2 = cVar;
                final Activity activity2 = activity;
                final aj ajVar2 = ajVar;
                this.c.execute(new Runnable() {
                    private void a(final String str) {
                        AnonymousClass1 r0 = new Runnable() {
                            public void run() {
                                try {
                                    ak.this.a(cVar2, str, (Context) activity2, ajVar2);
                                } catch (Exception e) {
                                    com.chartboost.sdk.Tracking.a.a(ak.class, "open openOnUiThread Runnable.run", e);
                                }
                            }
                        };
                        Activity activity = activity2;
                        if (activity != null) {
                            activity.runOnUiThread(r0);
                        } else {
                            ak.this.b.post(r0);
                        }
                    }

                    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
                        if (r2 != null) goto L_0x0031;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
                        r2.disconnect();
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0045, code lost:
                        if (r2 != null) goto L_0x0031;
                     */
                    /* JADX WARNING: Removed duplicated region for block: B:25:0x004b A[SYNTHETIC, Splitter:B:25:0x004b] */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void run() {
                        /*
                            r6 = this;
                            java.lang.String r0 = r3     // Catch:{ Exception -> 0x0053 }
                            com.chartboost.sdk.impl.ak r1 = com.chartboost.sdk.impl.ak.this     // Catch:{ Exception -> 0x0053 }
                            com.chartboost.sdk.impl.ai r1 = r1.f2446a     // Catch:{ Exception -> 0x0053 }
                            boolean r1 = r1.b()     // Catch:{ Exception -> 0x0053 }
                            if (r1 == 0) goto L_0x004f
                            r1 = 0
                            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            java.lang.String r3 = r3     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            r2.<init>(r3)     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            r1 = 0
                            r2.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x0035 }
                            r1 = 10000(0x2710, float:1.4013E-41)
                            r2.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0035 }
                            r2.setReadTimeout(r1)     // Catch:{ Exception -> 0x0035 }
                            java.lang.String r1 = "Location"
                            java.lang.String r1 = r2.getHeaderField(r1)     // Catch:{ Exception -> 0x0035 }
                            if (r1 == 0) goto L_0x002f
                            r0 = r1
                        L_0x002f:
                            if (r2 == 0) goto L_0x004f
                        L_0x0031:
                            r2.disconnect()     // Catch:{ Exception -> 0x0053 }
                            goto L_0x004f
                        L_0x0035:
                            r1 = move-exception
                            goto L_0x003e
                        L_0x0037:
                            r0 = move-exception
                            r2 = r1
                            goto L_0x0049
                        L_0x003a:
                            r2 = move-exception
                            r5 = r2
                            r2 = r1
                            r1 = r5
                        L_0x003e:
                            java.lang.String r3 = "CBURLOpener"
                            java.lang.String r4 = "Exception raised while opening a HTTP Conection"
                            com.chartboost.sdk.Libraries.CBLogging.a(r3, r4, r1)     // Catch:{ all -> 0x0048 }
                            if (r2 == 0) goto L_0x004f
                            goto L_0x0031
                        L_0x0048:
                            r0 = move-exception
                        L_0x0049:
                            if (r2 == 0) goto L_0x004e
                            r2.disconnect()     // Catch:{ Exception -> 0x0053 }
                        L_0x004e:
                            throw r0     // Catch:{ Exception -> 0x0053 }
                        L_0x004f:
                            r6.a(r0)     // Catch:{ Exception -> 0x0053 }
                            goto L_0x005b
                        L_0x0053:
                            r0 = move-exception
                            java.lang.Class<com.chartboost.sdk.impl.ak> r1 = com.chartboost.sdk.impl.ak.class
                            java.lang.String r2 = "open followTask"
                            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r1, (java.lang.String) r2, (java.lang.Exception) r0)
                        L_0x005b:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ak.AnonymousClass1.run():void");
                    }
                });
            } else {
                a(cVar, str, (Context) activity, ajVar);
            }
        } catch (URISyntaxException unused) {
            a(cVar, false, str, CBError.CBClickError.URI_INVALID, ajVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar, String str, Context context, aj ajVar) {
        if (cVar != null && cVar.b()) {
            cVar.l = 5;
        }
        if (context == null) {
            context = i.m;
        }
        if (context == null) {
            a(cVar, false, str, CBError.CBClickError.NO_HOST_ACTIVITY, ajVar);
            return;
        }
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            intent.setData(Uri.parse(str));
            context.startActivity(intent);
        } catch (Exception unused) {
            if (str.startsWith("market://")) {
                try {
                    str = "http://market.android.com/" + str.substring(9);
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    if (!(context instanceof Activity)) {
                        intent2.addFlags(268435456);
                    }
                    intent2.setData(Uri.parse(str));
                    context.startActivity(intent2);
                } catch (Exception e) {
                    CBLogging.a("CBURLOpener", "Exception raised openeing an inavld playstore URL", e);
                    a(cVar, false, str, CBError.CBClickError.URI_UNRECOGNIZED, ajVar);
                    return;
                }
            } else {
                a(cVar, false, str, CBError.CBClickError.URI_UNRECOGNIZED, ajVar);
            }
        }
        a(cVar, true, str, (CBError.CBClickError) null, ajVar);
    }

    public boolean a(String str) {
        try {
            Context context = i.m;
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            intent.setData(Uri.parse(str));
            if (context.getPackageManager().queryIntentActivities(intent, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE).size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            CBLogging.a("CBURLOpener", "Cannot open URL", e);
            com.chartboost.sdk.Tracking.a.a(ak.class, "canOpenURL", e);
            return false;
        }
    }

    public void a(c cVar, String str, aj ajVar) {
        a(cVar, str, cVar != null ? cVar.g.a() : null, ajVar);
    }
}
