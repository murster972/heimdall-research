package com.chartboost.sdk.impl;

import android.content.Context;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import com.chartboost.sdk.Libraries.CBUtility;

public abstract class z extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected v f2506a;
    private aa b;
    private int c = 1;

    public z(Context context, v vVar) {
        super(context);
        this.f2506a = vVar;
        a(context);
    }

    /* access modifiers changed from: protected */
    public abstract View a();

    public void a(int i) {
        RelativeLayout.LayoutParams layoutParams;
        this.c = i;
        setClickable(false);
        int b2 = b();
        int i2 = this.c;
        if (i2 == 0) {
            layoutParams = new RelativeLayout.LayoutParams(-1, CBUtility.a(b2, getContext()));
            layoutParams.addRule(10);
            this.b.b(1);
        } else if (i2 == 1) {
            layoutParams = new RelativeLayout.LayoutParams(-1, CBUtility.a(b2, getContext()));
            layoutParams.addRule(12);
            this.b.b(4);
        } else if (i2 == 2) {
            layoutParams = new RelativeLayout.LayoutParams(CBUtility.a(b2, getContext()), -1);
            layoutParams.addRule(9);
            this.b.b(8);
        } else if (i2 != 3) {
            layoutParams = null;
        } else {
            layoutParams = new RelativeLayout.LayoutParams(CBUtility.a(b2, getContext()), -1);
            layoutParams.addRule(11);
            this.b.b(2);
        }
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public abstract int b();

    private void a(Context context) {
        Context context2 = getContext();
        setGravity(17);
        this.b = new aa(context2);
        this.b.a(-1);
        this.b.setBackgroundColor(-855638017);
        addView(this.b, new RelativeLayout.LayoutParams(-1, -1));
        addView(a(), new RelativeLayout.LayoutParams(-1, -1));
    }

    public void a(boolean z) {
        a(z, 500);
    }

    private void a(final boolean z, long j) {
        this.f2506a.C = z;
        if (z && getVisibility() == 0) {
            return;
        }
        if (z || getVisibility() != 8) {
            AnonymousClass1 r0 = new Runnable() {
                public void run() {
                    if (!z) {
                        z.this.setVisibility(8);
                        z.this.clearAnimation();
                    }
                    synchronized (z.this.f2506a.g) {
                        z.this.f2506a.g.remove(z.this);
                    }
                }
            };
            if (z) {
                setVisibility(0);
            }
            float a2 = CBUtility.a((float) b(), getContext());
            TranslateAnimation translateAnimation = null;
            int i = this.c;
            if (i == 0) {
                translateAnimation = new TranslateAnimation(0.0f, 0.0f, z ? -a2 : 0.0f, z ? 0.0f : -a2);
            } else if (i == 1) {
                float f = z ? a2 : 0.0f;
                if (z) {
                    a2 = 0.0f;
                }
                translateAnimation = new TranslateAnimation(0.0f, 0.0f, f, a2);
            } else if (i == 2) {
                translateAnimation = new TranslateAnimation(z ? -a2 : 0.0f, z ? 0.0f : -a2, 0.0f, 0.0f);
            } else if (i == 3) {
                float f2 = z ? a2 : 0.0f;
                if (z) {
                    a2 = 0.0f;
                }
                translateAnimation = new TranslateAnimation(f2, a2, 0.0f, 0.0f);
            }
            translateAnimation.setDuration(j);
            translateAnimation.setFillAfter(!z);
            startAnimation(translateAnimation);
            synchronized (this.f2506a.g) {
                this.f2506a.g.put(this, r0);
            }
            this.f2506a.f2420a.postDelayed(r0, j);
        }
    }
}
