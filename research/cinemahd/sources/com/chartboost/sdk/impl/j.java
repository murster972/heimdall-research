package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.i;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

class j implements Comparable<j> {

    /* renamed from: a  reason: collision with root package name */
    final int f2487a;
    final String b;
    final String c;
    final String d;
    final AtomicInteger e;
    final AtomicInteger f;
    private final i g;
    private final AtomicReference<h> h;
    private final long i;

    j(i iVar, int i2, String str, String str2, String str3, AtomicInteger atomicInteger, AtomicReference<h> atomicReference, long j, AtomicInteger atomicInteger2) {
        this.g = iVar;
        this.f2487a = i2;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = atomicInteger;
        this.h = atomicReference;
        this.i = j;
        this.f = atomicInteger2;
        atomicInteger.incrementAndGet();
    }

    /* renamed from: a */
    public int compareTo(j jVar) {
        return this.f2487a - jVar.f2487a;
    }

    /* access modifiers changed from: package-private */
    public void a(Executor executor, boolean z) {
        h andSet;
        if ((this.e.decrementAndGet() == 0 || !z) && (andSet = this.h.getAndSet((Object) null)) != null) {
            executor.execute(new i(andSet, z, (int) TimeUnit.NANOSECONDS.toMillis(this.g.b() - this.i), this.f.get()));
        }
    }
}
