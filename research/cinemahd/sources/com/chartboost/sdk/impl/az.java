package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.h;

public abstract class az extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private final Rect f2460a;
    final a c;
    protected Button d;
    boolean e;

    private class a extends ay {
        private boolean c;

        public a(Context context) {
            super(context);
            this.c = false;
            this.c = false;
        }

        /* access modifiers changed from: protected */
        public void a(boolean z) {
            if (!az.this.e || !z) {
                if (this.c) {
                    if (getDrawable() != null) {
                        getDrawable().clearColorFilter();
                    } else if (getBackground() != null) {
                        getBackground().clearColorFilter();
                    }
                    invalidate();
                    this.c = false;
                }
            } else if (!this.c) {
                if (getDrawable() != null) {
                    getDrawable().setColorFilter(1996488704, PorterDuff.Mode.SRC_ATOP);
                } else if (getBackground() != null) {
                    getBackground().setColorFilter(1996488704, PorterDuff.Mode.SRC_ATOP);
                }
                invalidate();
                this.c = true;
            }
        }

        public boolean performClick() {
            super.performClick();
            return true;
        }
    }

    public az(Context context) {
        this(context, (AttributeSet) null);
    }

    /* access modifiers changed from: protected */
    public abstract void a(MotionEvent motionEvent);

    /* access modifiers changed from: package-private */
    public boolean a(View view, MotionEvent motionEvent) {
        view.getLocalVisibleRect(this.f2460a);
        this.f2460a.left += view.getPaddingLeft();
        this.f2460a.top += view.getPaddingTop();
        this.f2460a.right -= view.getPaddingRight();
        this.f2460a.bottom -= view.getPaddingBottom();
        return this.f2460a.contains(Math.round(motionEvent.getX()), Math.round(motionEvent.getY()));
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public az(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2460a = new Rect();
        this.d = null;
        this.e = true;
        this.c = new a(getContext());
        this.c.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                boolean a2 = az.this.a(view, motionEvent);
                int actionMasked = motionEvent.getActionMasked();
                if (actionMasked != 0) {
                    if (actionMasked == 1) {
                        if (az.this.getVisibility() == 0 && az.this.isEnabled() && a2) {
                            az.this.a(motionEvent);
                        }
                        az.this.c.a(false);
                    } else if (actionMasked == 2) {
                        az.this.c.a(a2);
                    } else if (actionMasked == 3 || actionMasked == 4) {
                        az.this.c.a(false);
                    }
                    return true;
                }
                az.this.c.a(a2);
                return a2;
            }
        });
        addView(this.c, new RelativeLayout.LayoutParams(-1, -1));
    }

    public void a(String str) {
        if (str != null) {
            a().setText(str);
            addView(a(), new RelativeLayout.LayoutParams(-1, -1));
            this.c.setVisibility(8);
            a(false);
            this.d.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    az.this.a((MotionEvent) null);
                }
            });
        } else if (this.d != null) {
            removeView(a());
            this.d = null;
            this.c.setVisibility(0);
            a(true);
        }
    }

    public TextView a() {
        if (this.d == null) {
            this.d = new Button(getContext());
            this.d.setGravity(17);
        }
        this.d.postInvalidate();
        return this.d;
    }

    public void a(h hVar) {
        if (hVar != null && hVar.c()) {
            this.c.a(hVar);
            a((String) null);
        }
    }

    public void a(ImageView.ScaleType scaleType) {
        this.c.setScaleType(scaleType);
    }

    public void a(boolean z) {
        this.e = z;
    }
}
