package com.chartboost.sdk.impl;

import com.facebook.imageutils.JfifUtil;

public class b {
    public static int[] a(char[] cArr, int[] iArr, boolean z) {
        int i = (cArr[0] << 16) + cArr[1];
        int i2 = (cArr[2] << 16) + cArr[3];
        if (!z) {
            a(iArr);
        }
        int i3 = i2;
        int i4 = i;
        int i5 = 0;
        while (i5 < 16) {
            int i6 = i4 ^ iArr[i5];
            a aVar = a.b;
            int i7 = (i6 >>> 16) & JfifUtil.MARKER_FIRST_BYTE;
            int i8 = (i6 >>> 8) & JfifUtil.MARKER_FIRST_BYTE;
            int i9 = i6 & JfifUtil.MARKER_FIRST_BYTE;
            int[][] iArr2 = aVar.f2430a;
            i5++;
            int i10 = i3 ^ (((iArr2[0][i6 >>> 24] + iArr2[1][i7]) ^ iArr2[2][i8]) + iArr2[3][i9]);
            i3 = i6;
            i4 = i10;
        }
        int i11 = iArr[16] ^ i4;
        int i12 = iArr[17] ^ i3;
        int[] iArr3 = {i12, i11};
        cArr[0] = i12 >>> 16;
        cArr[1] = (char) i12;
        cArr[2] = i11 >>> 16;
        cArr[3] = (char) i11;
        if (!z) {
            a(iArr);
        }
        return iArr3;
    }

    private static void a(int[] iArr) {
        for (int i = 0; i < iArr.length / 2; i++) {
            int i2 = iArr[i];
            iArr[i] = iArr[(iArr.length - i) - 1];
            iArr[(iArr.length - i) - 1] = i2;
        }
    }
}
