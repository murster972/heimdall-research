package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.e;

@SuppressLint({"ViewConstructor"})
public class bc extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private e.a f2465a;
    private ax b;
    private ax c;
    private final c d;

    public bc(Context context, c cVar) {
        super(context);
        this.d = cVar;
        if (cVar.p.b == 0) {
            this.b = new ax(context);
            addView(this.b, new RelativeLayout.LayoutParams(-1, -1));
            this.c = new ax(context);
            addView(this.c, new RelativeLayout.LayoutParams(-1, -1));
            this.c.setVisibility(8);
        }
    }

    public void a() {
        if (this.f2465a == null) {
            this.f2465a = this.d.k();
            e.a aVar = this.f2465a;
            if (aVar != null) {
                addView(aVar, new RelativeLayout.LayoutParams(-1, -1));
                this.f2465a.a();
            }
        }
    }

    public void b() {
    }

    public ax c() {
        return this.b;
    }

    public View d() {
        return this.f2465a;
    }

    public c e() {
        return this.d;
    }

    public boolean f() {
        e.a aVar = this.f2465a;
        return aVar != null && aVar.getVisibility() == 0;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        performClick();
        return true;
    }

    public boolean performClick() {
        super.performClick();
        return true;
    }
}
