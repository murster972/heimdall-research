package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Libraries.h;
import com.chartboost.sdk.g;
import com.chartboost.sdk.impl.av;
import com.chartboost.sdk.impl.v;
import java.util.Locale;
import org.json.JSONObject;

@SuppressLint({"ViewConstructor"})
public class ab extends RelativeLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private static final CharSequence k = "00:00";

    /* renamed from: a  reason: collision with root package name */
    final RelativeLayout f2432a;
    final aa b;
    final aa c;
    final az d;
    final TextView e;
    final x f;
    final av g;
    final v h;
    final Handler i;
    final Runnable j = new Runnable() {
        private int b = 0;

        public void run() {
            v.a q = ab.this.h.e();
            if (q != null) {
                if (ab.this.g.a().e()) {
                    int d = ab.this.g.a().d();
                    if (d > 0) {
                        v vVar = ab.this.h;
                        vVar.v = d;
                        if (((float) vVar.v) / 1000.0f > 0.0f && !vVar.t()) {
                            ab.this.h.r();
                            ab.this.h.a(true);
                        }
                    }
                    float c = ((float) d) / ((float) ab.this.g.a().c());
                    ab abVar = ab.this;
                    if (abVar.h.M) {
                        abVar.f.a(c);
                    }
                    int i = d / 1000;
                    if (this.b != i) {
                        this.b = i;
                        ab.this.e.setText(String.format(Locale.US, "%02d:%02d", new Object[]{Integer.valueOf(i / 60), Integer.valueOf(i % 60)}));
                    }
                }
                if (q.f()) {
                    az d2 = q.d(true);
                    if (d2.getVisibility() == 8) {
                        ab.this.h.a(true, d2);
                        d2.setEnabled(true);
                    }
                }
                ab abVar2 = ab.this;
                abVar2.i.removeCallbacks(abVar2.j);
                ab abVar3 = ab.this;
                abVar3.i.postDelayed(abVar3.j, 16);
            }
        }
    };
    private boolean l = false;
    private boolean m = false;
    private final Runnable n = new Runnable() {
        public void run() {
            ab.this.a(false);
        }
    };
    private final Runnable o = new Runnable() {
        public void run() {
            aa aaVar = ab.this.b;
            if (aaVar != null) {
                aaVar.setVisibility(8);
            }
            ab abVar = ab.this;
            if (abVar.h.M) {
                abVar.f.setVisibility(8);
            }
            ab.this.c.setVisibility(8);
            az azVar = ab.this.d;
            if (azVar != null) {
                azVar.setEnabled(false);
            }
        }
    };

    public ab(Context context, v vVar) {
        super(context);
        this.h = vVar;
        this.i = vVar.f2420a;
        JSONObject g2 = vVar.g();
        float f2 = context.getResources().getDisplayMetrics().density;
        float f3 = 10.0f * f2;
        int round = Math.round(f3);
        g a2 = g.a();
        this.g = (av) a2.a(new av(context));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        addView(this.g, layoutParams);
        this.f2432a = (RelativeLayout) a2.a(new RelativeLayout(context));
        if (g2 == null || g2.isNull("video-click-button")) {
            this.b = null;
            this.d = null;
        } else {
            this.b = (aa) a2.a(new aa(context));
            this.b.setVisibility(8);
            this.d = new az(context) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    ab.this.h.b(e.a(e.a("x", (Object) Float.valueOf(motionEvent.getX())), e.a("y", (Object) Float.valueOf(motionEvent.getY())), e.a("w", (Object) Integer.valueOf(ab.this.d.getWidth())), e.a("h", (Object) Integer.valueOf(ab.this.d.getHeight()))));
                }
            };
            this.d.a(ImageView.ScaleType.FIT_CENTER);
            h hVar = vVar.I;
            Point b2 = vVar.b("video-click-button");
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.leftMargin = Math.round(((float) b2.x) / hVar.e());
            layoutParams2.topMargin = Math.round(((float) b2.y) / hVar.e());
            vVar.a(layoutParams2, hVar, 1.0f);
            this.d.a(hVar);
            this.b.addView(this.d, layoutParams2);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, Math.round(((float) layoutParams2.height) + f3));
            layoutParams3.addRule(10);
            this.f2432a.addView(this.b, layoutParams3);
        }
        this.c = (aa) a2.a(new aa(context));
        this.c.setVisibility(8);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, Math.round(f2 * 32.5f));
        layoutParams4.addRule(12);
        this.f2432a.addView(this.c, layoutParams4);
        this.c.setGravity(16);
        this.c.setPadding(round, round, round, round);
        this.e = (TextView) a2.a(new TextView(context));
        this.e.setTextColor(-1);
        this.e.setTextSize(2, 11.0f);
        this.e.setText(k);
        this.e.setPadding(0, 0, round, 0);
        this.e.setSingleLine();
        this.e.measure(0, 0);
        int measuredWidth = this.e.getMeasuredWidth();
        this.e.setGravity(17);
        this.c.addView(this.e, new LinearLayout.LayoutParams(measuredWidth, -1));
        this.f = (x) a2.a(new x(context));
        this.f.setVisibility(8);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, Math.round(f3));
        layoutParams5.setMargins(0, CBUtility.a(1, context), 0, 0);
        this.c.addView(this.f, layoutParams5);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams6.addRule(6, this.g.getId());
        layoutParams6.addRule(8, this.g.getId());
        layoutParams6.addRule(5, this.g.getId());
        layoutParams6.addRule(7, this.g.getId());
        addView(this.f2432a, layoutParams6);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        a(!this.l, z);
    }

    public void b(boolean z) {
        aa aaVar;
        this.i.removeCallbacks(this.n);
        this.i.removeCallbacks(this.o);
        if (z) {
            if (!this.m && (aaVar = this.b) != null) {
                aaVar.setVisibility(0);
            }
            if (this.h.M) {
                this.f.setVisibility(0);
            }
            this.c.setVisibility(0);
            az azVar = this.d;
            if (azVar != null) {
                azVar.setEnabled(true);
            }
        } else {
            aa aaVar2 = this.b;
            if (aaVar2 != null) {
                aaVar2.clearAnimation();
                this.b.setVisibility(8);
            }
            this.c.clearAnimation();
            if (this.h.M) {
                this.f.setVisibility(8);
            }
            this.c.setVisibility(8);
            az azVar2 = this.d;
            if (azVar2 != null) {
                azVar2.setEnabled(false);
            }
        }
        this.l = z;
    }

    public void c(boolean z) {
        setBackgroundColor(z ? -16777216 : 0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        if (!z) {
            layoutParams.addRule(6, this.g.getId());
            layoutParams.addRule(8, this.g.getId());
            layoutParams.addRule(5, this.g.getId());
            layoutParams.addRule(7, this.g.getId());
        }
        this.f2432a.setLayoutParams(layoutParams);
        aa aaVar = this.b;
        if (aaVar != null) {
            aaVar.setGravity(8388627);
            this.b.requestLayout();
        }
    }

    public void d() {
        aa aaVar = this.b;
        if (aaVar != null) {
            aaVar.setVisibility(8);
        }
        this.m = true;
        az azVar = this.d;
        if (azVar != null) {
            azVar.setEnabled(false);
        }
    }

    public void e() {
        this.i.postDelayed(new Runnable() {
            public void run() {
                ab.this.g.setVisibility(0);
            }
        }, 500);
        this.g.a().a();
        this.i.removeCallbacks(this.j);
        this.i.postDelayed(this.j, 16);
    }

    public void f() {
        if (this.g.a().e()) {
            this.h.v = this.g.a().d();
            this.g.a().b();
        }
        if (this.h.e().e.getVisibility() == 0) {
            this.h.e().e.postInvalidate();
        }
        this.i.removeCallbacks(this.j);
    }

    public void g() {
        if (this.g.a().e()) {
            this.h.v = this.g.a().d();
        }
        this.g.a().b();
        this.i.removeCallbacks(this.j);
    }

    public void h() {
        this.g.setVisibility(8);
        invalidate();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.h.v = this.g.a().c();
        if (this.h.e() != null) {
            this.h.e().e();
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.i.removeCallbacks(this.j);
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        this.h.v();
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.h.w = this.g.a().c();
        this.h.e().a(true);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.g.a().e() || motionEvent.getActionMasked() != 0) {
            return false;
        }
        if (this.h != null) {
            a(true);
        }
        return true;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        az azVar = this.d;
        if (azVar != null) {
            azVar.setEnabled(z);
        }
        if (z) {
            b(false);
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, boolean z2) {
        aa aaVar;
        this.i.removeCallbacks(this.n);
        this.i.removeCallbacks(this.o);
        v vVar = this.h;
        if (vVar.y && vVar.p() && z != this.l) {
            this.l = z;
            AlphaAnimation alphaAnimation = this.l ? new AlphaAnimation(0.0f, 1.0f) : new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(z2 ? 100 : 200);
            alphaAnimation.setFillAfter(true);
            if (!this.m && (aaVar = this.b) != null) {
                aaVar.setVisibility(0);
                this.b.startAnimation(alphaAnimation);
                az azVar = this.d;
                if (azVar != null) {
                    azVar.setEnabled(true);
                }
            }
            if (this.h.M) {
                this.f.setVisibility(0);
            }
            this.c.setVisibility(0);
            this.c.startAnimation(alphaAnimation);
            if (this.l) {
                this.i.postDelayed(this.n, 3000);
            } else {
                this.i.postDelayed(this.o, alphaAnimation.getDuration());
            }
        }
    }

    public void d(boolean z) {
        this.e.setVisibility(z ? 0 : 8);
    }

    public x c() {
        return this.f;
    }

    public av.a b() {
        return this.g.a();
    }

    public void a() {
        c(CBUtility.a(CBUtility.a()));
    }

    public void a(int i2) {
        aa aaVar = this.b;
        if (aaVar != null) {
            aaVar.setBackgroundColor(i2);
        }
        this.c.setBackgroundColor(i2);
    }

    public void a(String str) {
        this.g.a().a((MediaPlayer.OnCompletionListener) this);
        this.g.a().a((MediaPlayer.OnErrorListener) this);
        this.g.a().a((MediaPlayer.OnPreparedListener) this);
        this.g.a().a(Uri.parse(str));
    }
}
