package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public final int f2477a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final boolean g;
    public final boolean h;

    public class a implements Runnable {
        private final int b;
        private final String c;
        private final CBError.CBImpressionError d;

        public a(int i, String str, CBError.CBImpressionError cBImpressionError) {
            this.b = i;
            this.c = str;
            this.d = cBImpressionError;
        }

        public void run() {
            int i = this.b;
            if (i == 0) {
                c.this.d(this.c);
            } else if (i == 1) {
                c.this.a(this.c);
            } else if (i == 2) {
                c.this.b(this.c);
            } else if (i == 3) {
                c.this.c(this.c);
            } else if (i == 4) {
                c.this.a(this.c, this.d);
            } else if (i == 5) {
                c.this.e(this.c);
            }
        }
    }

    private c(int i, String str, String str2, String str3, String str4, String str5, boolean z, boolean z2) {
        this.f2477a = i;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = z;
        this.h = z2;
    }

    public static c a() {
        return new c(0, "interstitial", "interstitial", "/interstitial/get", "webview/%s/interstitial/get", "/interstitial/show", false, false);
    }

    public static c b() {
        return new c(1, "rewarded", "rewarded-video", "/reward/get", "webview/%s/reward/get", "/reward/show", true, false);
    }

    public static c c() {
        return new c(2, "inplay", (String) null, "/inplay/get", "no webview endpoint", "/inplay/show", false, true);
    }

    public void d(String str) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar != null) {
            int i = this.f2477a;
            if (i == 0) {
                aVar.didCacheInterstitial(str);
            } else if (i == 1) {
                aVar.didCacheRewardedVideo(str);
            } else if (i == 2) {
                aVar.didCacheInPlay(str);
            }
        }
    }

    public void e(String str) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar != null) {
            int i = this.f2477a;
            if (i == 0) {
                aVar.didDisplayInterstitial(str);
            } else if (i == 1) {
                aVar.didDisplayRewardedVideo(str);
            }
        }
    }

    public boolean f(String str) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar == null) {
            return true;
        }
        int i = this.f2477a;
        if (i == 0) {
            return aVar.shouldDisplayInterstitial(str);
        }
        if (i != 1) {
            return true;
        }
        return aVar.shouldDisplayRewardedVideo(str);
    }

    public boolean g(String str) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar == null || this.f2477a != 0) {
            return true;
        }
        return aVar.shouldRequestInterstitial(str);
    }

    public String a(int i) {
        Object[] objArr = new Object[2];
        objArr[0] = this.c;
        objArr[1] = i == 1 ? "web" : "native";
        return String.format("%s-%s", objArr);
    }

    public void b(String str) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar != null) {
            int i = this.f2477a;
            if (i == 0) {
                aVar.didCloseInterstitial(str);
            } else if (i == 1) {
                aVar.didCloseRewardedVideo(str);
            }
        }
    }

    public void c(String str) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar != null) {
            int i = this.f2477a;
            if (i == 0) {
                aVar.didDismissInterstitial(str);
            } else if (i == 1) {
                aVar.didDismissRewardedVideo(str);
            }
        }
    }

    public void a(String str) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar != null) {
            int i = this.f2477a;
            if (i == 0) {
                aVar.didClickInterstitial(str);
            } else if (i == 1) {
                aVar.didClickRewardedVideo(str);
            }
        }
    }

    public void a(String str, CBError.CBImpressionError cBImpressionError) {
        com.chartboost.sdk.a aVar = i.c;
        if (aVar != null) {
            int i = this.f2477a;
            if (i == 0) {
                aVar.didFailToLoadInterstitial(str, cBImpressionError);
            } else if (i == 1) {
                aVar.didFailToLoadRewardedVideo(str, cBImpressionError);
            } else if (i == 2) {
                aVar.didFailToLoadInPlay(str, cBImpressionError);
            }
        }
    }
}
