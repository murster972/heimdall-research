package com.chartboost.sdk.impl;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.impl.av;
import com.vungle.warren.ui.contract.AdContract;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class at extends SurfaceView implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, SurfaceHolder.Callback, av.a {

    /* renamed from: a  reason: collision with root package name */
    private final String f2453a = "VideoSurfaceView";
    private Uri b;
    private Map<String, String> c;
    private int d;
    private int e = 0;
    private int f = 0;
    private SurfaceHolder g = null;
    private MediaPlayer h = null;
    private int i;
    private int j;
    private int k;
    private int l;
    private MediaPlayer.OnCompletionListener m;
    private MediaPlayer.OnPreparedListener n;
    private int o;
    private MediaPlayer.OnErrorListener p;
    private int q;

    public at(Context context) {
        super(context);
        f();
    }

    private void f() {
        this.i = 0;
        this.j = 0;
        getHolder().addCallback(this);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.e = 0;
        this.f = 0;
    }

    private void g() {
        if (this.b != null && this.g != null) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra(AdContract.AdvertisementBus.COMMAND, "pause");
            getContext().sendBroadcast(intent);
            a(false);
            try {
                this.h = new MediaPlayer();
                this.h.setOnPreparedListener(this);
                this.h.setOnVideoSizeChangedListener(this);
                this.d = -1;
                this.h.setOnCompletionListener(this);
                this.h.setOnErrorListener(this);
                this.h.setOnBufferingUpdateListener(this);
                this.o = 0;
                this.h.setDisplay(this.g);
                this.h.setAudioStreamType(3);
                this.h.setScreenOnWhilePlaying(true);
                FileInputStream fileInputStream = new FileInputStream(new File(this.b.toString()));
                this.h.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
                this.h.prepareAsync();
                this.e = 1;
            } catch (IOException e2) {
                CBLogging.c("VideoSurfaceView", "Unable to open content: " + this.b, e2);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            } catch (IllegalArgumentException e3) {
                CBLogging.c("VideoSurfaceView", "Unable to open content: " + this.b, e3);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        r0 = r3.e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean h() {
        /*
            r3 = this;
            android.media.MediaPlayer r0 = r3.h
            r1 = 1
            if (r0 == 0) goto L_0x000f
            int r0 = r3.e
            r2 = -1
            if (r0 == r2) goto L_0x000f
            if (r0 == 0) goto L_0x000f
            if (r0 == r1) goto L_0x000f
            goto L_0x0010
        L_0x000f:
            r1 = 0
        L_0x0010:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.at.h():boolean");
    }

    public void a(int i2, int i3) {
    }

    public void a(Uri uri) {
        a(uri, (Map<String, String>) null);
    }

    public void b() {
        if (h() && this.h.isPlaying()) {
            this.h.pause();
            this.e = 4;
        }
        this.f = 4;
    }

    public int c() {
        if (h()) {
            int i2 = this.d;
            if (i2 > 0) {
                return i2;
            }
            this.d = this.h.getDuration();
            return this.d;
        }
        this.d = -1;
        return this.d;
    }

    public int d() {
        if (h()) {
            return this.h.getCurrentPosition();
        }
        return 0;
    }

    public boolean e() {
        return h() && this.h.isPlaying();
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
        this.o = i2;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f = 5;
        if (this.e != 5) {
            this.e = 5;
            MediaPlayer.OnCompletionListener onCompletionListener = this.m;
            if (onCompletionListener != null) {
                onCompletionListener.onCompletion(this.h);
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        CBLogging.a("VideoSurfaceView", "Error: " + i2 + "," + i3);
        this.e = -1;
        this.f = -1;
        MediaPlayer.OnErrorListener onErrorListener = this.p;
        if (onErrorListener == null || onErrorListener.onError(this.h, i2, i3)) {
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int defaultSize = SurfaceView.getDefaultSize(0, i2);
        int defaultSize2 = SurfaceView.getDefaultSize(0, i3);
        int i5 = this.i;
        if (i5 > 0 && (i4 = this.j) > 0) {
            int min = Math.min(defaultSize2, Math.round((((float) i4) / ((float) i5)) * ((float) defaultSize)));
            defaultSize = Math.min(defaultSize, Math.round((((float) this.i) / ((float) this.j)) * ((float) defaultSize2)));
            defaultSize2 = min;
        }
        setMeasuredDimension(defaultSize, defaultSize2);
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.e = 2;
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        MediaPlayer.OnPreparedListener onPreparedListener = this.n;
        if (onPreparedListener != null) {
            onPreparedListener.onPrepared(this.h);
        }
        int i2 = this.q;
        if (i2 != 0) {
            a(i2);
        }
        if (this.i != 0 && this.j != 0) {
            getHolder().setFixedSize(this.i, this.j);
            if (this.k == this.i && this.l == this.j && this.f == 3) {
                a();
            }
        } else if (this.f == 3) {
            a();
        }
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        if (this.i != 0 && this.j != 0) {
            getHolder().setFixedSize(this.i, this.j);
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.k = i3;
        this.l = i4;
        boolean z = true;
        boolean z2 = this.f == 3;
        if (!(this.i == i3 && this.j == i4)) {
            z = false;
        }
        if (this.h != null && z2 && z) {
            int i5 = this.q;
            if (i5 != 0) {
                a(i5);
            }
            a();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.g = surfaceHolder;
        g();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.g = null;
        a(true);
    }

    public void a(Uri uri, Map<String, String> map) {
        this.b = uri;
        this.c = map;
        this.q = 0;
        g();
        requestLayout();
        invalidate();
    }

    public void a(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.n = onPreparedListener;
    }

    public void a(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.m = onCompletionListener;
    }

    public void a(MediaPlayer.OnErrorListener onErrorListener) {
        this.p = onErrorListener;
    }

    private void a(boolean z) {
        MediaPlayer mediaPlayer = this.h;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            this.h.release();
            this.h = null;
            this.e = 0;
            if (z) {
                this.f = 0;
            }
        }
    }

    public void a() {
        if (h()) {
            this.h.start();
            this.e = 3;
        }
        this.f = 3;
    }

    public void a(int i2) {
        if (h()) {
            this.h.seekTo(i2);
            this.q = 0;
            return;
        }
        this.q = i2;
    }
}
