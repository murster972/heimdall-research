package com.chartboost.sdk.impl;

import org.json.JSONObject;

public class bg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final bd f2472a;
    private final bf b;
    private final int c;
    private final JSONObject d;
    private final String e;

    bg(bd bdVar, bf bfVar, int i, String str, JSONObject jSONObject) {
        this.f2472a = bdVar;
        this.b = bfVar;
        this.c = i;
        this.e = str;
        this.d = jSONObject;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:28|29) */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:35|36) */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:22|23|101) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:45|46|47|48|88) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:74|75|76|77|113) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:78|79|80|81|93) */
    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        com.chartboost.sdk.Libraries.CBLogging.b("NativeBridgeCommand", "Cannot find video file name");
        r13.b.e("Parsing exception unknown field for video replay");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        com.chartboost.sdk.Libraries.CBLogging.b("NativeBridgeCommand", "Cannot find video file name");
        r13.b.e("Parsing exception unknown field for video play");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        com.chartboost.sdk.Libraries.CBLogging.b("NativeBridgeCommand", "Cannot find video file name");
        r13.b.e("Parsing exception unknown field for video pause");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x007d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x009e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00c5 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x013d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:76:0x022a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:80:0x0256 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x00b0=Splitter:B:31:0x00b0, B:17:0x0062=Splitter:B:17:0x0062, B:80:0x0256=Splitter:B:80:0x0256, B:24:0x0089=Splitter:B:24:0x0089, B:76:0x022a=Splitter:B:76:0x022a, B:47:0x013d=Splitter:B:47:0x013d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r13 = this;
            java.lang.String r0 = "http://"
            java.lang.String r1 = "Exception while opening a browser view with MRAID url"
            java.lang.String r2 = "ActivityNotFoundException occured when opening a url in a browser"
            java.lang.String r3 = "Exception occured while parsing the message for webview debug track event"
            int r4 = r13.c     // Catch:{ Exception -> 0x026e }
            r5 = 1148846080(0x447a0000, float:1000.0)
            java.lang.String r6 = ""
            java.lang.String r7 = "Cannot find duration parameter for the video"
            java.lang.String r8 = "duration"
            java.lang.String r9 = "Cannot find video file name"
            java.lang.String r10 = "name"
            java.lang.String r11 = "message"
            java.lang.String r12 = "NativeBridgeCommand"
            switch(r4) {
                case 0: goto L_0x0267;
                case 1: goto L_0x0261;
                case 2: goto L_0x0233;
                case 3: goto L_0x0204;
                case 4: goto L_0x01d1;
                case 5: goto L_0x0150;
                case 6: goto L_0x0149;
                case 7: goto L_0x0119;
                case 8: goto L_0x00e9;
                case 9: goto L_0x00d7;
                case 10: goto L_0x00b0;
                case 11: goto L_0x0089;
                case 12: goto L_0x0062;
                case 13: goto L_0x002f;
                case 14: goto L_0x001f;
                default: goto L_0x001d;
            }
        L_0x001d:
            goto L_0x028c
        L_0x001f:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x0028 }
            org.json.JSONObject r1 = r13.d     // Catch:{ Exception -> 0x0028 }
            r0.c((org.json.JSONObject) r1)     // Catch:{ Exception -> 0x0028 }
            goto L_0x028c
        L_0x0028:
            java.lang.String r0 = "Invalid set orientation command"
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x002f:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x0056 }
            java.lang.String r0 = r0.getString(r11)     // Catch:{ Exception -> 0x0056 }
            java.lang.Class<com.chartboost.sdk.impl.be> r1 = com.chartboost.sdk.impl.be.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x0056 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0056 }
            r2.<init>()     // Catch:{ Exception -> 0x0056 }
            java.lang.String r3 = "JS->Native Warning message: "
            r2.append(r3)     // Catch:{ Exception -> 0x0056 }
            r2.append(r0)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0056 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0056 }
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x0056 }
            r1.e(r0)     // Catch:{ Exception -> 0x0056 }
            goto L_0x028c
        L_0x0056:
            java.lang.String r0 = "Warning message is empty"
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r0.e(r6)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0062:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x007d }
            java.lang.String r0 = r0.getString(r10)     // Catch:{ Exception -> 0x007d }
            com.chartboost.sdk.impl.s r1 = com.chartboost.sdk.impl.s.a()     // Catch:{ Exception -> 0x007d }
            boolean r1 = r1.a((java.lang.CharSequence) r0)     // Catch:{ Exception -> 0x007d }
            if (r1 != 0) goto L_0x0076
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x007d }
            r1.m = r0     // Catch:{ Exception -> 0x007d }
        L_0x0076:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x007d }
            r0.x()     // Catch:{ Exception -> 0x007d }
            goto L_0x028c
        L_0x007d:
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r9)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for video replay"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0089:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x009e }
            java.lang.String r0 = r0.getString(r10)     // Catch:{ Exception -> 0x009e }
            com.chartboost.sdk.impl.s r1 = com.chartboost.sdk.impl.s.a()     // Catch:{ Exception -> 0x009e }
            boolean r1 = r1.a((java.lang.CharSequence) r0)     // Catch:{ Exception -> 0x009e }
            if (r1 != 0) goto L_0x00a8
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x009e }
            r1.m = r0     // Catch:{ Exception -> 0x009e }
            goto L_0x00a8
        L_0x009e:
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r9)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for video play"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
        L_0x00a8:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r1 = 2
            r0.b((int) r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x00b0:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x00c5 }
            java.lang.String r0 = r0.getString(r10)     // Catch:{ Exception -> 0x00c5 }
            com.chartboost.sdk.impl.s r1 = com.chartboost.sdk.impl.s.a()     // Catch:{ Exception -> 0x00c5 }
            boolean r1 = r1.a((java.lang.CharSequence) r0)     // Catch:{ Exception -> 0x00c5 }
            if (r1 != 0) goto L_0x00cf
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x00c5 }
            r1.m = r0     // Catch:{ Exception -> 0x00c5 }
            goto L_0x00cf
        L_0x00c5:
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r9)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for video pause"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
        L_0x00cf:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r1 = 3
            r0.b((int) r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x00d7:
            com.chartboost.sdk.impl.bd r0 = r13.f2472a     // Catch:{ Exception -> 0x026e }
            r0.onHideCustomView()     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r1 = 1
            r0.b((int) r1)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r0.w()     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x00e9:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x0112 }
            java.lang.String r1 = "event"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x0112 }
            r1.b((java.lang.String) r0)     // Catch:{ Exception -> 0x0112 }
            java.lang.Class<com.chartboost.sdk.impl.be> r1 = com.chartboost.sdk.impl.be.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x0112 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0112 }
            r2.<init>()     // Catch:{ Exception -> 0x0112 }
            java.lang.String r3 = "JS->Native Track VAST event message: "
            r2.append(r3)     // Catch:{ Exception -> 0x0112 }
            r2.append(r0)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0112 }
            android.util.Log.d(r1, r0)     // Catch:{ Exception -> 0x0112 }
            goto L_0x028c
        L_0x0112:
            java.lang.String r0 = "Exception occured while parsing the message for webview tracking VAST events"
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0119:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x013d }
            double r0 = r0.getDouble(r8)     // Catch:{ Exception -> 0x013d }
            float r0 = (float) r0     // Catch:{ Exception -> 0x013d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013d }
            r1.<init>()     // Catch:{ Exception -> 0x013d }
            java.lang.String r2 = "######### JS->Native Video total player duration"
            r1.append(r2)     // Catch:{ Exception -> 0x013d }
            float r0 = r0 * r5
            r1.append(r0)     // Catch:{ Exception -> 0x013d }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x013d }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r1)     // Catch:{ Exception -> 0x013d }
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x013d }
            r1.b((float) r0)     // Catch:{ Exception -> 0x013d }
            goto L_0x028c
        L_0x013d:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for total player duration"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r7)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0149:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r0.z()     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0150:
            org.json.JSONObject r3 = r13.d     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r4 = "url"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            boolean r4 = r3.startsWith(r0)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 != 0) goto L_0x0175
            java.lang.String r4 = "https://"
            boolean r4 = r3.startsWith(r4)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 != 0) goto L_0x0175
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.<init>()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.append(r0)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.append(r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r3 = r4.toString()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
        L_0x0175:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r4 = "android.intent.action.VIEW"
            android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r0.<init>(r4, r5)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            com.chartboost.sdk.impl.bf r4 = r13.b     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            com.chartboost.sdk.impl.bf$b r4 = r4.e()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x028c
            android.content.Context r4 = r4.getContext()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x028c
            android.content.pm.PackageManager r5 = r4.getPackageManager()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            android.content.ComponentName r5 = r0.resolveActivity(r5)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r5 == 0) goto L_0x028c
            r4.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.Class<com.chartboost.sdk.impl.be> r0 = com.chartboost.sdk.impl.be.class
            java.lang.String r0 = r0.getName()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.<init>()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r5 = "JS->Native Track MRAID openUrl: "
            r4.append(r5)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.append(r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r3 = r4.toString()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            goto L_0x028c
        L_0x01b7:
            r0 = move-exception
            java.lang.Class r2 = r13.getClass()     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r2, (java.lang.String) r1, (java.lang.Exception) r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x01c4:
            r0 = move-exception
            java.lang.Class r1 = r13.getClass()     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r1, (java.lang.String) r2, (java.lang.Exception) r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r2)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x01d1:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x01f8 }
            java.lang.String r0 = r0.getString(r11)     // Catch:{ Exception -> 0x01f8 }
            java.lang.Class<com.chartboost.sdk.impl.be> r1 = com.chartboost.sdk.impl.be.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x01f8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01f8 }
            r2.<init>()     // Catch:{ Exception -> 0x01f8 }
            java.lang.String r3 = "JS->Native Error message: "
            r2.append(r3)     // Catch:{ Exception -> 0x01f8 }
            r2.append(r0)     // Catch:{ Exception -> 0x01f8 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01f8 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x01f8 }
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x01f8 }
            r1.d((java.lang.String) r0)     // Catch:{ Exception -> 0x01f8 }
            goto L_0x028c
        L_0x01f8:
            java.lang.String r0 = "Error message is empty"
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r0.d((java.lang.String) r6)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0204:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x022a }
            java.lang.String r0 = r0.getString(r11)     // Catch:{ Exception -> 0x022a }
            java.lang.Class<com.chartboost.sdk.impl.be> r1 = com.chartboost.sdk.impl.be.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x022a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x022a }
            r2.<init>()     // Catch:{ Exception -> 0x022a }
            java.lang.String r4 = "JS->Native Debug message: "
            r2.append(r4)     // Catch:{ Exception -> 0x022a }
            r2.append(r0)     // Catch:{ Exception -> 0x022a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x022a }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x022a }
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x022a }
            r1.c((java.lang.String) r0)     // Catch:{ Exception -> 0x022a }
            goto L_0x028c
        L_0x022a:
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r3)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r0.c((java.lang.String) r3)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0233:
            org.json.JSONObject r0 = r13.d     // Catch:{ Exception -> 0x0256 }
            double r0 = r0.getDouble(r8)     // Catch:{ Exception -> 0x0256 }
            float r0 = (float) r0     // Catch:{ Exception -> 0x0256 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0256 }
            r1.<init>()     // Catch:{ Exception -> 0x0256 }
            java.lang.String r2 = "######### JS->Native Video current player duration"
            r1.append(r2)     // Catch:{ Exception -> 0x0256 }
            float r0 = r0 * r5
            r1.append(r0)     // Catch:{ Exception -> 0x0256 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0256 }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r1)     // Catch:{ Exception -> 0x0256 }
            com.chartboost.sdk.impl.bf r1 = r13.b     // Catch:{ Exception -> 0x0256 }
            r1.a((float) r0)     // Catch:{ Exception -> 0x0256 }
            goto L_0x028c
        L_0x0256:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for current player duration"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.Libraries.CBLogging.b(r12, r7)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0261:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r0.h()     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0267:
            com.chartboost.sdk.impl.bf r0 = r13.b     // Catch:{ Exception -> 0x026e }
            r1 = 0
            r0.b((org.json.JSONObject) r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x026e:
            r0 = move-exception
            java.lang.Class<com.chartboost.sdk.impl.bg> r1 = com.chartboost.sdk.impl.bg.class
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "run("
            r2.append(r3)
            java.lang.String r3 = r13.e
            r2.append(r3)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r1, (java.lang.String) r2, (java.lang.Exception) r0)
        L_0x028c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.bg.run():void");
    }
}
