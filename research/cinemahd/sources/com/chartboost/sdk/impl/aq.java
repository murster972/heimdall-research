package com.chartboost.sdk.impl;

import android.util.Log;
import java.io.File;

public class aq {

    /* renamed from: a  reason: collision with root package name */
    private static String f2452a = "CBTrace";
    private static final boolean b = a();

    private static boolean a() {
        File b2;
        try {
            if (!Log.isLoggable(f2452a, 4) || !s.a().c().equals("mounted") || (b2 = s.a().b()) == null) {
                return false;
            }
            return new File(b2, ".chartboost/log_trace").exists();
        } catch (Throwable unused) {
            return false;
        }
    }

    public static void a(String str, String str2) {
        if (b) {
            String str3 = f2452a;
            Log.i(str3, str + ": " + str2);
        }
    }

    public static void a(String str, boolean z) {
        if (b) {
            String str2 = f2452a;
            Log.i(str2, str + ": " + z);
        }
    }

    public static void a(String str, Object obj) {
        if (!b) {
            return;
        }
        if (obj != null) {
            String str2 = f2452a;
            Log.i(str2, str + ": " + obj.getClass().getName() + " " + obj.hashCode());
            return;
        }
        String str3 = f2452a;
        Log.i(str3, str + ": null");
    }

    public static void a(String str) {
        if (b) {
            Log.i(f2452a, str);
        }
    }
}
