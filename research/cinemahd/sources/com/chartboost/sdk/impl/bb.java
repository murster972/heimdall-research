package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public abstract class bb extends View {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f2464a = null;
    private Canvas b = null;

    public bb(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        try {
            getClass().getMethod("setLayerType", new Class[]{Integer.TYPE, Paint.class}).invoke(this, new Object[]{1, null});
        } catch (Exception unused) {
        }
    }

    private boolean b(Canvas canvas) {
        try {
            return ((Boolean) Canvas.class.getMethod("isHardwareAccelerated", new Class[0]).invoke(canvas, new Object[0])).booleanValue();
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Bitmap bitmap = this.f2464a;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f2464a.recycle();
        }
        this.f2464a = null;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        Canvas canvas2;
        boolean b2 = b(canvas);
        if (b2) {
            Bitmap bitmap = this.f2464a;
            if (!(bitmap != null && bitmap.getWidth() == canvas.getWidth() && this.f2464a.getHeight() == canvas.getHeight())) {
                Bitmap bitmap2 = this.f2464a;
                if (bitmap2 != null && !bitmap2.isRecycled()) {
                    this.f2464a.recycle();
                }
                try {
                    this.f2464a = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
                    this.b = new Canvas(this.f2464a);
                } catch (Throwable unused) {
                    return;
                }
            }
            this.f2464a.eraseColor(0);
            canvas2 = canvas;
            canvas = this.b;
        } else {
            canvas2 = null;
        }
        a(canvas);
        if (b2) {
            canvas2.drawBitmap(this.f2464a, 0.0f, 0.0f, (Paint) null);
        }
    }
}
