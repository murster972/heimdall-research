package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chartboost.sdk.e;

@SuppressLint({"ViewConstructor"})
public class y extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    final v f2504a;
    final az b;
    private LinearLayout c;
    private ay d;
    private TextView e;
    private int f = Integer.MIN_VALUE;

    public y(Context context, v vVar) {
        super(context);
        this.f2504a = vVar;
        int round = Math.round(context.getResources().getDisplayMetrics().density * 8.0f);
        setOrientation(1);
        setGravity(17);
        this.c = new LinearLayout(context);
        this.c.setGravity(17);
        this.c.setOrientation(0);
        this.c.setPadding(round, round, round, round);
        this.d = new ay(context);
        this.d.setScaleType(ImageView.ScaleType.FIT_CENTER);
        this.d.setPadding(0, 0, round, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        vVar.a(layoutParams, vVar.H, 1.0f);
        this.e = new TextView(getContext());
        this.e.setTextColor(-1);
        this.e.setTypeface((Typeface) null, 1);
        this.e.setGravity(17);
        this.e.setTextSize(2, e.a(context) ? 26.0f : 16.0f);
        this.c.addView(this.d, layoutParams);
        this.c.addView(this.e, new LinearLayout.LayoutParams(-2, -2));
        this.b = new az(getContext()) {
            /* access modifiers changed from: protected */
            public void a(MotionEvent motionEvent) {
                y.this.b.setEnabled(false);
                y.this.f2504a.e().g();
            }
        };
        this.b.setContentDescription("CBWatch");
        this.b.setPadding(0, 0, 0, round);
        this.b.a(ImageView.ScaleType.FIT_CENTER);
        this.b.setPadding(round, round, round, round);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        vVar.a(layoutParams2, vVar.G, 1.0f);
        this.d.a(vVar.H);
        this.b.a(vVar.G);
        addView(this.c, new LinearLayout.LayoutParams(-2, -2));
        addView(this.b, layoutParams2);
        a();
    }

    public void a(boolean z) {
        setBackgroundColor(z ? -16777216 : this.f);
    }

    public void a(String str, int i) {
        this.e.setText(str);
        this.f = i;
        a(this.f2504a.s());
    }

    public void a() {
        a(this.f2504a.s());
    }
}
