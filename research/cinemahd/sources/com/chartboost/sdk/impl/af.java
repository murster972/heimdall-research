package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;

public class af<T> {

    /* renamed from: a  reason: collision with root package name */
    public final T f2440a;
    public final CBError b;

    private af(T t, CBError cBError) {
        this.f2440a = t;
        this.b = cBError;
    }

    public static <T> af<T> a(T t) {
        return new af<>(t, (CBError) null);
    }

    public static <T> af<T> a(CBError cBError) {
        return new af<>((Object) null, cBError);
    }
}
