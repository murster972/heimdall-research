package com.chartboost.sdk.impl;

import java.io.Serializable;
import java.io.Writer;

public class bk extends Writer implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final StringBuilder f2476a;

    public bk() {
        this.f2476a = new StringBuilder();
    }

    public void close() {
    }

    public void flush() {
    }

    public String toString() {
        return this.f2476a.toString();
    }

    public void write(String str) {
        if (str != null) {
            this.f2476a.append(str);
        }
    }

    public void write(char[] cArr, int i, int i2) {
        if (cArr != null) {
            this.f2476a.append(cArr, i, i2);
        }
    }

    public bk(int i) {
        this.f2476a = new StringBuilder(i);
    }

    public Writer append(char c) {
        this.f2476a.append(c);
        return this;
    }

    public Writer append(CharSequence charSequence) {
        this.f2476a.append(charSequence);
        return this;
    }

    public Writer append(CharSequence charSequence, int i, int i2) {
        this.f2476a.append(charSequence, i, i2);
        return this;
    }
}
