package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.h;

@SuppressLint({"ViewConstructor"})
public class w extends z {
    private LinearLayout b;
    private ay c;
    private TextView d;

    public w(Context context, v vVar) {
        super(context, vVar);
    }

    /* access modifiers changed from: protected */
    public View a() {
        Context context = getContext();
        int round = Math.round(getContext().getResources().getDisplayMetrics().density * 8.0f);
        this.b = new LinearLayout(context);
        this.b.setOrientation(0);
        this.b.setGravity(17);
        int a2 = CBUtility.a(36, context);
        this.c = new ay(context);
        this.c.setPadding(round, round, round, round);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(a2, a2);
        this.c.setScaleType(ImageView.ScaleType.FIT_CENTER);
        this.d = new TextView(context);
        this.d.setPadding(round / 2, round, round, round);
        this.d.setTextColor(-15264491);
        this.d.setTextSize(2, 16.0f);
        this.d.setTypeface((Typeface) null, 1);
        this.d.setGravity(17);
        this.b.addView(this.c, layoutParams);
        this.b.addView(this.d, new LinearLayout.LayoutParams(-2, -1));
        return this.b;
    }

    /* access modifiers changed from: protected */
    public int b() {
        return 48;
    }

    public void a(h hVar) {
        this.c.a(hVar);
        this.c.setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public void a(String str) {
        this.d.setText(str);
    }
}
