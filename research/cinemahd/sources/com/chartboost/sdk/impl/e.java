package com.chartboost.sdk.impl;

import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.c;
import com.chartboost.sdk.d;
import com.chartboost.sdk.g;
import com.chartboost.sdk.impl.c;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class e {
    private final long A = TimeUnit.SECONDS.toNanos(1);
    private final String[] B = {"ASKED_TO_CACHE", "ASKED_TO_SHOW", "REQUESTING_TO_CACHE", "REQUESTING_TO_SHOW", "DOWNLOADING_TO_CACHE", "DOWNLOADING_TO_SHOW", "READY", "ASKING_UI_TO_SHOW_AD", "DONE"};

    /* renamed from: a  reason: collision with root package name */
    final ScheduledExecutorService f2480a;
    public final f b;
    final i c;
    final Handler d;
    final c e;
    final c f;
    int g = 0;
    final Map<String, f> h;
    final SortedSet<f> i;
    final SortedSet<f> j;
    ScheduledFuture<?> k;
    private final l l;
    private final ah m;
    private final ai n;
    private final ap o;
    private final AtomicReference<com.chartboost.sdk.Model.e> p;
    private final SharedPreferences q;
    private final com.chartboost.sdk.Tracking.a r;
    private final ak s;
    private final d t;
    private final al u;
    private int v;
    private boolean w;
    private final Map<String, Long> x;
    private final Map<String, Integer> y;
    private final long z = TimeUnit.SECONDS.toNanos(5);

    public class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final int f2483a;
        final String b;
        final f c;
        final CBError.CBImpressionError d;

        public a(int i, String str, f fVar, CBError.CBImpressionError cBImpressionError) {
            this.f2483a = i;
            this.b = str;
            this.c = fVar;
            this.d = cBImpressionError;
        }

        public void run() {
            try {
                synchronized (e.this) {
                    int i = this.f2483a;
                    if (i != 0) {
                        switch (i) {
                            case 2:
                                e.this.k = null;
                                e.this.b();
                                break;
                            case 3:
                                e.this.b(this.b);
                                break;
                            case 4:
                                e.this.c(this.b);
                                break;
                            case 5:
                                e.this.b(this.c);
                                break;
                            case 6:
                                e.this.a(this.c, this.d);
                                break;
                            case 7:
                                e.this.a(this.c);
                                break;
                            case 8:
                                e.this.d(this.b);
                                break;
                        }
                    } else {
                        e.this.a();
                    }
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(a.class, "run", e2);
            }
        }
    }

    public e(c cVar, ScheduledExecutorService scheduledExecutorService, l lVar, f fVar, ah ahVar, ai aiVar, ap apVar, AtomicReference<com.chartboost.sdk.Model.e> atomicReference, SharedPreferences sharedPreferences, i iVar, com.chartboost.sdk.Tracking.a aVar, Handler handler, c cVar2, ak akVar, d dVar, al alVar) {
        this.f2480a = scheduledExecutorService;
        this.l = lVar;
        this.b = fVar;
        this.m = ahVar;
        this.n = aiVar;
        this.o = apVar;
        this.p = atomicReference;
        this.q = sharedPreferences;
        this.c = iVar;
        this.r = aVar;
        this.d = handler;
        this.e = cVar2;
        this.s = akVar;
        this.t = dVar;
        this.u = alVar;
        this.f = cVar;
        this.v = 1;
        this.h = new HashMap();
        this.j = new TreeSet();
        this.i = new TreeSet();
        this.x = new HashMap();
        this.y = new HashMap();
        this.w = false;
    }

    private void c() {
        Long l2;
        boolean z2 = true;
        if (this.g == 1) {
            long b2 = this.c.b();
            l2 = null;
            for (Map.Entry next : this.x.entrySet()) {
                if (this.h.get((String) next.getKey()) != null) {
                    long max = Math.max(this.z, ((Long) next.getValue()).longValue() - b2);
                    if (l2 == null || max < l2.longValue()) {
                        l2 = Long.valueOf(max);
                    }
                }
            }
        } else {
            l2 = null;
        }
        if (!(l2 == null || this.k == null)) {
            if (Math.abs(l2.longValue() - this.k.getDelay(TimeUnit.NANOSECONDS)) > TimeUnit.SECONDS.toNanos(5)) {
                z2 = false;
            }
            if (z2) {
                return;
            }
        }
        ScheduledFuture<?> scheduledFuture = this.k;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            this.k = null;
        }
        if (l2 != null) {
            this.k = this.f2480a.schedule(new a(2, (String) null, (f) null, (CBError.CBImpressionError) null), l2.longValue(), TimeUnit.NANOSECONDS);
        }
    }

    private void d() {
        long b2 = this.c.b();
        Iterator<Long> it2 = this.x.values().iterator();
        while (it2.hasNext()) {
            if (b2 - it2.next().longValue() >= 0) {
                it2.remove();
            }
        }
    }

    private boolean e(String str) {
        return this.x.containsKey(str);
    }

    private void f(f fVar) {
        this.h.remove(fVar.b);
        fVar.c = 8;
        fVar.d = null;
    }

    private void g(f fVar) {
        com.chartboost.sdk.Model.e eVar = this.p.get();
        long j2 = eVar.s;
        int i2 = eVar.t;
        Integer num = this.y.get(fVar.b);
        if (num == null) {
            num = 0;
        }
        Integer valueOf = Integer.valueOf(Math.min(num.intValue(), i2));
        this.y.put(fVar.b, Integer.valueOf(valueOf.intValue() + 1));
        this.x.put(fVar.b, Long.valueOf(this.c.b() + TimeUnit.MILLISECONDS.toNanos(j2 << valueOf.intValue())));
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f A[Catch:{ Exception -> 0x00a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008a A[Catch:{ Exception -> 0x00a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void h(com.chartboost.sdk.impl.f r9) {
        /*
            r8 = this;
            com.chartboost.sdk.impl.ai r0 = r8.n
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x001d
            android.os.Handler r0 = r8.d
            com.chartboost.sdk.impl.c$a r1 = new com.chartboost.sdk.impl.c$a
            com.chartboost.sdk.impl.c r2 = r8.f
            r2.getClass()
            r3 = 4
            java.lang.String r9 = r9.b
            com.chartboost.sdk.Model.CBError$CBImpressionError r4 = com.chartboost.sdk.Model.CBError.CBImpressionError.INTERNET_UNAVAILABLE_AT_SHOW
            r1.<init>(r3, r9, r4)
            r0.post(r1)
            return
        L_0x001d:
            r0 = 0
            com.chartboost.sdk.Model.a r1 = r9.d     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.Libraries.f r2 = r8.b     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.Libraries.g r2 = r2.d()     // Catch:{ Exception -> 0x00a1 }
            java.io.File r2 = r2.f2398a     // Catch:{ Exception -> 0x00a1 }
            int r3 = r1.b     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r4 = "AdUnitManager"
            if (r3 != 0) goto L_0x004c
            com.chartboost.sdk.impl.c r3 = r8.f     // Catch:{ Exception -> 0x00a1 }
            boolean r3 = r3.g     // Catch:{ Exception -> 0x00a1 }
            if (r3 != 0) goto L_0x003e
            java.lang.String r3 = r1.p     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r5 = "video"
            boolean r3 = r3.equals(r5)     // Catch:{ Exception -> 0x00a1 }
            if (r3 == 0) goto L_0x004c
        L_0x003e:
            org.json.JSONObject r3 = r1.f2407a     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.Model.CBError$CBImpressionError r3 = r8.a((org.json.JSONObject) r3)     // Catch:{ Exception -> 0x00a1 }
            if (r3 == 0) goto L_0x004d
            java.lang.String r5 = "Video media unavailable for the impression"
            com.chartboost.sdk.Libraries.CBLogging.b(r4, r5)     // Catch:{ Exception -> 0x00a1 }
            goto L_0x004d
        L_0x004c:
            r3 = r0
        L_0x004d:
            if (r3 != 0) goto L_0x0088
            java.util.Map<java.lang.String, com.chartboost.sdk.Model.b> r5 = r1.c     // Catch:{ Exception -> 0x00a1 }
            java.util.Collection r5 = r5.values()     // Catch:{ Exception -> 0x00a1 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x00a1 }
        L_0x0059:
            boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x00a1 }
            if (r6 == 0) goto L_0x0088
            java.lang.Object r6 = r5.next()     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.Model.b r6 = (com.chartboost.sdk.Model.b) r6     // Catch:{ Exception -> 0x00a1 }
            java.io.File r7 = r6.a((java.io.File) r2)     // Catch:{ Exception -> 0x00a1 }
            boolean r7 = r7.exists()     // Catch:{ Exception -> 0x00a1 }
            if (r7 != 0) goto L_0x0059
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a1 }
            r3.<init>()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r7 = "Asset does not exist: "
            r3.append(r7)     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r6 = r6.b     // Catch:{ Exception -> 0x00a1 }
            r3.append(r6)     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.Libraries.CBLogging.b(r4, r3)     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.Model.CBError$CBImpressionError r3 = com.chartboost.sdk.Model.CBError.CBImpressionError.ASSET_MISSING     // Catch:{ Exception -> 0x00a1 }
            goto L_0x0059
        L_0x0088:
            if (r3 != 0) goto L_0x00ab
            int r4 = r1.b     // Catch:{ Exception -> 0x00a1 }
            r5 = 1
            if (r4 != r5) goto L_0x0099
            java.lang.String r1 = r8.a((com.chartboost.sdk.Model.a) r1, (java.io.File) r2)     // Catch:{ Exception -> 0x00a1 }
            if (r1 != 0) goto L_0x009a
            com.chartboost.sdk.Model.CBError$CBImpressionError r2 = com.chartboost.sdk.Model.CBError.CBImpressionError.ERROR_LOADING_WEB_VIEW     // Catch:{ Exception -> 0x00a1 }
            r3 = r2
            goto L_0x009a
        L_0x0099:
            r1 = r0
        L_0x009a:
            if (r3 != 0) goto L_0x00ab
            com.chartboost.sdk.Model.c r0 = r8.a((com.chartboost.sdk.impl.f) r9, (java.lang.String) r1)     // Catch:{ Exception -> 0x00a1 }
            goto L_0x00ab
        L_0x00a1:
            r1 = move-exception
            java.lang.Class<com.chartboost.sdk.impl.e> r2 = com.chartboost.sdk.impl.e.class
            java.lang.String r3 = "showReady"
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r2, (java.lang.String) r3, (java.lang.Exception) r1)
            com.chartboost.sdk.Model.CBError$CBImpressionError r3 = com.chartboost.sdk.Model.CBError.CBImpressionError.INTERNAL
        L_0x00ab:
            if (r3 != 0) goto L_0x00d0
            r1 = 7
            r9.c = r1
            com.chartboost.sdk.c$c r1 = new com.chartboost.sdk.c$c
            com.chartboost.sdk.c r2 = r8.e
            r2.getClass()
            r3 = 10
            r1.<init>(r3)
            r1.d = r0
            com.chartboost.sdk.Libraries.i r0 = r8.c
            long r2 = r0.b()
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            r9.j = r0
            android.os.Handler r9 = r8.d
            r9.post(r1)
            goto L_0x00d6
        L_0x00d0:
            r8.b((com.chartboost.sdk.impl.f) r9, (com.chartboost.sdk.Model.CBError.CBImpressionError) r3)
            r8.f(r9)
        L_0x00d6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.e.h(com.chartboost.sdk.impl.f):void");
    }

    private void i(f fVar) {
        aj ajVar = new aj(this.f.f, this.o, this.r, 2, new g(this, fVar.b));
        ajVar.j = 1;
        ajVar.a("cached", (Object) "0");
        String str = fVar.d.f;
        if (!str.isEmpty()) {
            ajVar.a("ad_id", (Object) str);
        }
        ajVar.a("location", (Object) fVar.b);
        this.m.a(ajVar);
        this.r.b(this.f.a(fVar.d.b), fVar.b, str);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.g == 0) {
            this.g = 1;
            b();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.w) {
            try {
                this.w = true;
                d();
                if (this.g == 1) {
                    if (!a(this.j, 1, 3, 1, "show")) {
                        a(this.i, 0, 2, 2, "cache");
                    }
                }
                c();
                this.w = false;
            } catch (Throwable th) {
                this.w = false;
                throw th;
            }
        }
    }

    private void e(f fVar) {
        b(fVar, CBError.CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
        f(fVar);
        g(fVar);
    }

    private boolean a(SortedSet<f> sortedSet, int i2, int i3, int i4, String str) {
        Iterator it2 = sortedSet.iterator();
        while (it2.hasNext()) {
            f fVar = (f) it2.next();
            if (fVar.c != i2 || fVar.d != null) {
                it2.remove();
            } else if (e(fVar.b)) {
                continue;
            } else if (!this.f.g(fVar.b)) {
                fVar.c = 8;
                this.h.remove(fVar.b);
                it2.remove();
            } else {
                fVar.c = i3;
                it2.remove();
                a(fVar, i4, str);
                return true;
            }
        }
        return false;
    }

    private boolean e() {
        if (this.f.f2477a == 0 && !com.chartboost.sdk.i.u && this.q.getInt("cbPrefSessionCount", 0) == 1) {
            return true;
        }
        return false;
    }

    private void d(f fVar) {
        int i2 = fVar.c;
        long b2 = this.c.b();
        Long l2 = fVar.h;
        if (l2 != null) {
            fVar.k = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - l2.longValue()));
        }
        Long l3 = fVar.i;
        if (l3 != null) {
            fVar.l = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - l3.longValue()));
        }
        b(fVar, "ad-unit-cached");
        fVar.c = 6;
        if (fVar.f) {
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new c.a(0, fVar.b, (CBError.CBImpressionError) null));
        }
        if (i2 == 5) {
            h(fVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (e()) {
            c cVar = this.f;
            cVar.getClass();
            this.d.postDelayed(new c.a(4, str, CBError.CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        f fVar = this.h.get(str);
        if (fVar != null && fVar.c == 6 && !a(fVar.d)) {
            this.h.remove(str);
            fVar = null;
        }
        if (fVar == null) {
            int i2 = this.v;
            this.v = i2 + 1;
            fVar = new f(i2, str, 0);
            this.h.put(str, fVar);
            this.i.add(fVar);
        }
        fVar.f = true;
        if (fVar.h == null) {
            fVar.h = Long.valueOf(this.c.b());
        }
        switch (fVar.c) {
            case 6:
            case 7:
                Handler handler = this.d;
                c cVar2 = this.f;
                cVar2.getClass();
                handler.post(new c.a(0, str, (CBError.CBImpressionError) null));
                break;
        }
        b();
    }

    public synchronized com.chartboost.sdk.Model.a a(String str) {
        f fVar = this.h.get(str);
        if (fVar == null || (fVar.c != 6 && fVar.c != 7)) {
            return null;
        }
        return fVar.d;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        f fVar = this.h.get(str);
        if (fVar != null && fVar.c == 6) {
            f(fVar);
            b();
        }
    }

    private void c(final f fVar) {
        if (fVar.d != null) {
            int i2 = fVar.c;
            if (i2 == 5 || i2 == 4) {
                int i3 = fVar.c == 5 ? 1 : 2;
                if (fVar.g > i3) {
                    AnonymousClass2 r1 = new h() {
                        public void a(boolean z, int i, int i2) {
                            e.this.a(fVar, z, i, i2);
                        }
                    };
                    fVar.g = i3;
                    this.l.a(i3, fVar.d.c, new AtomicInteger(), (h) g.a().a(r1));
                }
            }
        }
    }

    private boolean a(com.chartboost.sdk.Model.a aVar) {
        File file = this.b.d().f2398a;
        for (b next : aVar.c.values()) {
            if (!next.a(file).exists()) {
                CBLogging.b("AdUnitManager", "Asset does not exist: " + next.b);
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: type inference failed for: r0v5, types: [com.chartboost.sdk.impl.ad] */
    /* JADX WARNING: type inference failed for: r15v3, types: [com.chartboost.sdk.impl.aj] */
    /* JADX WARNING: type inference failed for: r15v4, types: [com.chartboost.sdk.impl.am] */
    /* JADX WARNING: type inference failed for: r15v5, types: [com.chartboost.sdk.impl.aj] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.chartboost.sdk.impl.f r22, int r23, java.lang.String r24) {
        /*
            r21 = this;
            r8 = r21
            r9 = r22
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.Model.e> r0 = r8.p     // Catch:{ Exception -> 0x010a }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.Model.e r0 = (com.chartboost.sdk.Model.e) r0     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.c r1 = r8.f     // Catch:{ Exception -> 0x010a }
            int r1 = r1.f2477a     // Catch:{ Exception -> 0x010a }
            r10 = 2
            r11 = 0
            r12 = 1
            if (r1 != r10) goto L_0x0017
            r13 = 1
            goto L_0x0018
        L_0x0017:
            r13 = 0
        L_0x0018:
            boolean r1 = r0.y     // Catch:{ Exception -> 0x010a }
            if (r1 == 0) goto L_0x0020
            if (r13 != 0) goto L_0x0020
            r14 = 1
            goto L_0x0021
        L_0x0020:
            r14 = 0
        L_0x0021:
            com.chartboost.sdk.Libraries.i r1 = r8.c     // Catch:{ Exception -> 0x010a }
            long r4 = r1.b()     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.e$1 r20 = new com.chartboost.sdk.impl.e$1     // Catch:{ Exception -> 0x010a }
            r1 = r20
            r2 = r21
            r3 = r22
            r6 = r13
            r7 = r14
            r1.<init>(r3, r4, r6, r7)     // Catch:{ Exception -> 0x010a }
            int r1 = r9.c     // Catch:{ Exception -> 0x010a }
            if (r1 != r10) goto L_0x003a
            r1 = 1
            goto L_0x003b
        L_0x003a:
            r1 = 0
        L_0x003b:
            java.lang.String r2 = "cache"
            java.lang.String r3 = "location"
            if (r13 == 0) goto L_0x0076
            com.chartboost.sdk.impl.aj r0 = new com.chartboost.sdk.impl.aj     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.c r4 = r8.f     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r4.d     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.ap r5 = r8.o     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.Tracking.a r6 = r8.r     // Catch:{ Exception -> 0x010a }
            r15 = r0
            r16 = r4
            r17 = r5
            r18 = r6
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x010a }
            r0.l = r12     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r9.b     // Catch:{ Exception -> 0x010a }
            r0.a((java.lang.String) r3, (java.lang.Object) r4)     // Catch:{ Exception -> 0x010a }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x010a }
            r0.a((java.lang.String) r2, (java.lang.Object) r1)     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = "raw"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x010a }
            r0.a((java.lang.String) r1, (java.lang.Object) r2)     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x010a }
            r9.e = r1     // Catch:{ Exception -> 0x010a }
            goto L_0x00eb
        L_0x0076:
            if (r14 == 0) goto L_0x00b6
            com.chartboost.sdk.impl.c r4 = r8.f     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r4.e     // Catch:{ Exception -> 0x010a }
            java.lang.Object[] r5 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x010a }
            java.lang.String r0 = r0.F     // Catch:{ Exception -> 0x010a }
            r5[r11] = r0     // Catch:{ Exception -> 0x010a }
            java.lang.String r16 = java.lang.String.format(r4, r5)     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.am r0 = new com.chartboost.sdk.impl.am     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.ap r4 = r8.o     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.Tracking.a r5 = r8.r     // Catch:{ Exception -> 0x010a }
            r15 = r0
            r17 = r4
            r18 = r5
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.Libraries.f r4 = r8.b     // Catch:{ Exception -> 0x010a }
            org.json.JSONObject r4 = r4.c()     // Catch:{ Exception -> 0x010a }
            java.lang.String r5 = "cache_assets"
            r0.a(r5, r4, r11)     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r9.b     // Catch:{ Exception -> 0x010a }
            r0.a(r3, r4, r11)     // Catch:{ Exception -> 0x010a }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x010a }
            r0.a(r2, r1, r11)     // Catch:{ Exception -> 0x010a }
            r0.l = r12     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x010a }
            r9.e = r1     // Catch:{ Exception -> 0x010a }
            goto L_0x00eb
        L_0x00b6:
            com.chartboost.sdk.impl.aj r0 = new com.chartboost.sdk.impl.aj     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.c r4 = r8.f     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r4.d     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.ap r5 = r8.o     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.Tracking.a r6 = r8.r     // Catch:{ Exception -> 0x010a }
            r15 = r0
            r16 = r4
            r17 = r5
            r18 = r6
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = "local-videos"
            com.chartboost.sdk.Libraries.f r5 = r8.b     // Catch:{ Exception -> 0x010a }
            org.json.JSONArray r5 = r5.b()     // Catch:{ Exception -> 0x010a }
            r0.a((java.lang.String) r4, (java.lang.Object) r5)     // Catch:{ Exception -> 0x010a }
            r0.l = r12     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r9.b     // Catch:{ Exception -> 0x010a }
            r0.a((java.lang.String) r3, (java.lang.Object) r4)     // Catch:{ Exception -> 0x010a }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x010a }
            r0.a((java.lang.String) r2, (java.lang.Object) r1)     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x010a }
            r9.e = r1     // Catch:{ Exception -> 0x010a }
        L_0x00eb:
            r0.j = r12     // Catch:{ Exception -> 0x010a }
            r8.g = r10     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.ah r1 = r8.m     // Catch:{ Exception -> 0x010a }
            r1.a(r0)     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.Tracking.a r0 = r8.r     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.impl.c r1 = r8.f     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r2 = r9.e     // Catch:{ Exception -> 0x010a }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = r1.a((int) r2)     // Catch:{ Exception -> 0x010a }
            java.lang.String r2 = r9.b     // Catch:{ Exception -> 0x010a }
            r3 = r24
            r0.a((java.lang.String) r1, (java.lang.String) r3, (java.lang.String) r2)     // Catch:{ Exception -> 0x010a }
            goto L_0x011e
        L_0x010a:
            r0 = move-exception
            java.lang.Class<com.chartboost.sdk.impl.e> r1 = com.chartboost.sdk.impl.e.class
            java.lang.String r2 = "sendAdGetRequest"
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r1, (java.lang.String) r2, (java.lang.Exception) r0)
            com.chartboost.sdk.Model.CBError r0 = new com.chartboost.sdk.Model.CBError
            com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.MISCELLANEOUS
            java.lang.String r2 = "error sending ad-get request"
            r0.<init>(r1, r2)
            r8.a((com.chartboost.sdk.impl.f) r9, (com.chartboost.sdk.Model.CBError) r0)
        L_0x011e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.e.a(com.chartboost.sdk.impl.f, int, java.lang.String):void");
    }

    private void b(f fVar, CBError.CBImpressionError cBImpressionError) {
        String str;
        Handler handler = this.d;
        c cVar = this.f;
        cVar.getClass();
        handler.post(new c.a(4, fVar.b, cBImpressionError));
        if (cBImpressionError != CBError.CBImpressionError.NO_AD_FOUND) {
            com.chartboost.sdk.Model.a aVar = fVar.d;
            String str2 = null;
            String str3 = aVar != null ? aVar.f : null;
            int i2 = fVar.c;
            String str4 = (i2 == 0 || i2 == 2 || i2 == 4) ? "cache" : "show";
            com.chartboost.sdk.Model.a aVar2 = fVar.d;
            Integer valueOf = Integer.valueOf(aVar2 != null ? aVar2.b : fVar.e.intValue());
            if (valueOf != null) {
                str2 = valueOf.intValue() == 0 ? "native" : "web";
            }
            String str5 = str2;
            int i3 = fVar.c;
            if (i3 >= 0) {
                String[] strArr = this.B;
                if (i3 < strArr.length) {
                    str = strArr[i3];
                    this.r.a(this.f.b, str4, str5, cBImpressionError.toString(), str3, fVar.b, str);
                }
            }
            str = "Unknown state: " + fVar.c;
            this.r.a(this.f.b, str4, str5, cBImpressionError.toString(), str3, fVar.b, str);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        if (e()) {
            c cVar = this.f;
            cVar.getClass();
            this.d.postDelayed(new c.a(4, str, CBError.CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        f fVar = this.h.get(str);
        if (fVar == null) {
            int i2 = this.v;
            this.v = i2 + 1;
            fVar = new f(i2, str, 1);
            this.h.put(str, fVar);
            this.j.add(fVar);
        }
        if (fVar.i == null) {
            fVar.i = Long.valueOf(this.c.b());
        }
        switch (fVar.c) {
            case 0:
                this.i.remove(fVar);
                this.j.add(fVar);
                fVar.c = 1;
                break;
            case 2:
                fVar.c = 3;
                break;
            case 4:
                fVar.c = 5;
                c(fVar);
                break;
            case 6:
                h(fVar);
                break;
        }
        b();
    }

    private void b(f fVar, String str) {
        Integer num;
        String str2;
        f fVar2 = fVar;
        if (this.p.get().p) {
            com.chartboost.sdk.Model.a aVar = fVar2.d;
            String str3 = null;
            String str4 = aVar != null ? aVar.f : null;
            int i2 = fVar2.c;
            String str5 = (i2 == 0 || i2 == 2 || i2 == 4) ? "cache" : "show";
            com.chartboost.sdk.Model.a aVar2 = fVar2.d;
            if (aVar2 != null) {
                num = Integer.valueOf(aVar2.b);
            } else {
                num = fVar2.e;
            }
            if (num != null) {
                str3 = num.intValue() == 0 ? "native" : "web";
            }
            String str6 = str3;
            int i3 = fVar2.c;
            if (i3 >= 0) {
                String[] strArr = this.B;
                if (i3 < strArr.length) {
                    str2 = strArr[i3];
                    this.r.a(str, this.f.b, str5, str6, (String) null, (String) null, com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("adGetRequestSubmitToCallbackMs", (Object) fVar2.p), com.chartboost.sdk.Libraries.e.a("downloadRequestToCompletionMs", (Object) fVar2.n), com.chartboost.sdk.Libraries.e.a("downloadAccumulatedProcessingMs", (Object) fVar2.o), com.chartboost.sdk.Libraries.e.a("adGetRequestGetResponseCodeMs", (Object) fVar2.q), com.chartboost.sdk.Libraries.e.a("adGetRequestReadDataMs", (Object) fVar2.r), com.chartboost.sdk.Libraries.e.a("cacheRequestToReadyMs", (Object) fVar2.k), com.chartboost.sdk.Libraries.e.a("showRequestToReadyMs", (Object) fVar2.l), com.chartboost.sdk.Libraries.e.a("showRequestToShownMs", (Object) fVar2.m), com.chartboost.sdk.Libraries.e.a("adId", (Object) str4), com.chartboost.sdk.Libraries.e.a("location", (Object) fVar2.b), com.chartboost.sdk.Libraries.e.a((String) AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE, (Object) str2)), false);
                }
            }
            str2 = "Unknown state: " + fVar2.c;
            this.r.a(str, this.f.b, str5, str6, (String) null, (String) null, com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("adGetRequestSubmitToCallbackMs", (Object) fVar2.p), com.chartboost.sdk.Libraries.e.a("downloadRequestToCompletionMs", (Object) fVar2.n), com.chartboost.sdk.Libraries.e.a("downloadAccumulatedProcessingMs", (Object) fVar2.o), com.chartboost.sdk.Libraries.e.a("adGetRequestGetResponseCodeMs", (Object) fVar2.q), com.chartboost.sdk.Libraries.e.a("adGetRequestReadDataMs", (Object) fVar2.r), com.chartboost.sdk.Libraries.e.a("cacheRequestToReadyMs", (Object) fVar2.k), com.chartboost.sdk.Libraries.e.a("showRequestToReadyMs", (Object) fVar2.l), com.chartboost.sdk.Libraries.e.a("showRequestToShownMs", (Object) fVar2.m), com.chartboost.sdk.Libraries.e.a("adId", (Object) str4), com.chartboost.sdk.Libraries.e.a("location", (Object) fVar2.b), com.chartboost.sdk.Libraries.e.a((String) AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE, (Object) str2)), false);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(f fVar, com.chartboost.sdk.Model.a aVar) {
        this.g = 1;
        fVar.c = fVar.c == 2 ? 4 : 5;
        fVar.d = aVar;
        c(fVar);
        b();
    }

    /* access modifiers changed from: package-private */
    public void b(f fVar) {
        if (fVar.c == 7) {
            if (fVar.i != null && fVar.m == null) {
                fVar.m = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(this.c.b() - fVar.i.longValue()));
            }
            b(fVar, "ad-unit-shown");
            this.y.remove(fVar.b);
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new c.a(5, fVar.b, (CBError.CBImpressionError) null));
            i(fVar);
            f(fVar);
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(f fVar, boolean z2, int i2, int i3) {
        if (fVar.c == 4 || fVar.c == 5) {
            fVar.n = Integer.valueOf(i2);
            fVar.o = Integer.valueOf(i3);
            if (z2) {
                d(fVar);
            } else {
                e(fVar);
            }
        }
        b();
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(f fVar, CBError cBError) {
        if (this.g != 0) {
            this.g = 1;
            b(fVar, cBError.c());
            f(fVar);
            g(fVar);
            b();
        }
    }

    private String a(com.chartboost.sdk.Model.a aVar, File file) {
        b bVar = aVar.r;
        if (bVar == null) {
            CBLogging.b("AdUnitManager", "AdUnit does not have a template body");
            return null;
        }
        File a2 = bVar.a(file);
        HashMap hashMap = new HashMap();
        hashMap.putAll(aVar.d);
        hashMap.put("{% certification_providers %}", o.a(aVar.s));
        for (Map.Entry next : aVar.c.entrySet()) {
            hashMap.put(next.getKey(), ((b) next.getValue()).b);
        }
        try {
            return n.a(a2, hashMap);
        } catch (Exception e2) {
            com.chartboost.sdk.Tracking.a.a(e.class, "loadTemplateHtml", e2);
            return null;
        }
    }

    private com.chartboost.sdk.Model.c a(f fVar, String str) {
        f fVar2 = fVar;
        return new com.chartboost.sdk.Model.c(fVar2.d, new d(this, fVar2), this.b, this.m, this.o, this.q, this.r, this.d, this.e, this.s, this.t, this.u, this.f, fVar2.b, str);
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar, CBError.CBImpressionError cBImpressionError) {
        b(fVar, cBImpressionError);
        if (fVar.c != 7) {
            return;
        }
        if (cBImpressionError == CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE) {
            fVar.c = 6;
            fVar.j = null;
            fVar.i = null;
            fVar.m = null;
            return;
        }
        g(fVar);
        f(fVar);
        b();
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar) {
        if (fVar.c == 7) {
            fVar.c = 6;
            fVar.j = null;
            fVar.i = null;
            fVar.m = null;
        }
    }

    /* access modifiers changed from: package-private */
    public CBError.CBImpressionError a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return CBError.CBImpressionError.INVALID_RESPONSE;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("assets");
        if (optJSONObject == null) {
            return CBError.CBImpressionError.INVALID_RESPONSE;
        }
        JSONObject optJSONObject2 = optJSONObject.optJSONObject(CBUtility.a(CBUtility.a()) ? "video-portrait" : "video-landscape");
        if (optJSONObject2 == null) {
            return CBError.CBImpressionError.VIDEO_UNAVAILABLE_FOR_CURRENT_ORIENTATION;
        }
        String optString = optJSONObject2.optString("id");
        if (optString.isEmpty()) {
            return CBError.CBImpressionError.VIDEO_ID_MISSING;
        }
        if (new File(this.b.d().g, optString).exists()) {
            return null;
        }
        return CBError.CBImpressionError.VIDEO_UNAVAILABLE;
    }
}
