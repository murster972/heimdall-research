package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import java.io.File;
import java.util.HashMap;

class k extends ad<Void> {

    /* renamed from: a  reason: collision with root package name */
    final j f2488a;
    private final l k;
    private final ai l;

    k(l lVar, ai aiVar, j jVar, File file) {
        super("GET", jVar.c, 2, file);
        this.j = 1;
        this.k = lVar;
        this.l = aiVar;
        this.f2488a = jVar;
    }

    public ae a() {
        HashMap hashMap = new HashMap();
        hashMap.put("X-Chartboost-App", i.k);
        hashMap.put("X-Chartboost-Client", CBUtility.b());
        hashMap.put("X-Chartboost-Reachability", Integer.toString(this.l.a()));
        return new ae(hashMap, (byte[]) null, (String) null);
    }

    public void a(Void voidR, ag agVar) {
        this.k.a(this, (CBError) null, (ag) null);
    }

    public void a(CBError cBError, ag agVar) {
        this.k.a(this, cBError, agVar);
    }
}
