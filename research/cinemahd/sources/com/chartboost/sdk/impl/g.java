package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.aj;
import org.json.JSONObject;

public class g implements aj.a {

    /* renamed from: a  reason: collision with root package name */
    private final e f2485a;
    private final String b;

    public g(e eVar, String str) {
        this.f2485a = eVar;
        this.b = str;
    }

    public void a(aj ajVar, JSONObject jSONObject) {
        if (this.f2485a.f.h || i.t) {
            synchronized (this.f2485a) {
                this.f2485a.b(this.b);
            }
        }
    }

    public void a(aj ajVar, CBError cBError) {
        e eVar = this.f2485a;
        if (eVar.f.h) {
            synchronized (eVar) {
                this.f2485a.b(this.b);
            }
        }
    }
}
