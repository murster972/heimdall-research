package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.h;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.e;
import com.chartboost.sdk.g;
import com.chartboost.sdk.impl.u;
import com.facebook.react.uimanager.ViewProps;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.io.File;
import org.json.JSONObject;

public class v extends u {
    protected boolean A;
    protected boolean B;
    protected boolean C = false;
    protected int D = 0;
    protected h E;
    protected h F;
    protected h G;
    protected h H;
    protected h I;
    protected h J;
    protected h K;
    protected h L;
    protected boolean M = false;
    protected boolean N = false;
    protected boolean O = false;
    private boolean P = false;
    private boolean Q = false;
    private boolean R = false;
    final f q;
    protected int r = 0;
    protected int s;
    protected String t;
    protected String u;
    protected int v = 0;
    protected int w = 0;
    JSONObject x;
    protected boolean y;
    protected boolean z;

    public class a extends u.a {
        final ab h;
        y i;
        final t j;
        final w k;
        private final az m;
        private View n;
        private final az o;

        /* access modifiers changed from: protected */
        public void a(int i2, int i3) {
            super.a(i2, i3);
            a(v.this.r, false);
            boolean a2 = CBUtility.a(v.this.a());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams6 = (RelativeLayout.LayoutParams) this.c.getLayoutParams();
            v vVar = v.this;
            vVar.a(layoutParams2, a2 ? vVar.F : vVar.E, 1.0f);
            Point b = v.this.b(a2 ? "replay-portrait" : "replay-landscape");
            int round = Math.round(((((float) layoutParams6.leftMargin) + (((float) layoutParams6.width) / 2.0f)) + ((float) b.x)) - (((float) layoutParams2.width) / 2.0f));
            int round2 = Math.round(((((float) layoutParams6.topMargin) + (((float) layoutParams6.height) / 2.0f)) + ((float) b.y)) - (((float) layoutParams2.height) / 2.0f));
            layoutParams2.leftMargin = Math.min(Math.max(0, round), i2 - layoutParams2.width);
            layoutParams2.topMargin = Math.min(Math.max(0, round2), i3 - layoutParams2.height);
            this.m.bringToFront();
            if (a2) {
                this.m.a(v.this.F);
            } else {
                this.m.a(v.this.E);
            }
            RelativeLayout.LayoutParams layoutParams7 = (RelativeLayout.LayoutParams) this.e.getLayoutParams();
            if (!v.this.s()) {
                layoutParams3.width = layoutParams7.width;
                layoutParams3.height = layoutParams7.height;
                layoutParams3.leftMargin = layoutParams7.leftMargin;
                layoutParams3.topMargin = layoutParams7.topMargin;
                layoutParams4.width = layoutParams7.width;
                layoutParams4.height = layoutParams7.height;
                layoutParams4.leftMargin = layoutParams7.leftMargin;
                layoutParams4.topMargin = layoutParams7.topMargin;
            } else {
                RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-2, -2);
                h hVar = a2 ? v.this.l : v.this.m;
                v.this.a(layoutParams8, hVar, 1.0f);
                layoutParams8.leftMargin = 0;
                layoutParams8.topMargin = 0;
                layoutParams8.addRule(11);
                this.o.setLayoutParams(layoutParams8);
                this.o.a(hVar);
            }
            layoutParams5.width = layoutParams7.width;
            layoutParams5.height = 72;
            layoutParams5.leftMargin = layoutParams7.leftMargin;
            layoutParams5.topMargin = (layoutParams7.topMargin + layoutParams7.height) - 72;
            if (v.this.N) {
                this.n.setLayoutParams(layoutParams);
            }
            if (v.this.e.n == 2) {
                this.i.setLayoutParams(layoutParams3);
            }
            this.h.setLayoutParams(layoutParams4);
            this.j.setLayoutParams(layoutParams5);
            this.m.setLayoutParams(layoutParams2);
            if (v.this.e.n == 2) {
                this.i.a();
            }
            this.h.a();
        }

        /* access modifiers changed from: package-private */
        public void b(boolean z) {
            v vVar = v.this;
            if (vVar.r != 1) {
                if (vVar.z) {
                    a(0, z);
                    return;
                }
                a(1, z);
                JSONObject a2 = e.a(v.this.x, "timer");
                if (v.this.s >= 1 || a2 == null || a2.isNull(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_DELAY)) {
                    this.h.b(!v.this.y);
                } else {
                    Object[] objArr = new Object[1];
                    objArr[0] = v.this.y ? ViewProps.VISIBLE : ViewProps.HIDDEN;
                    CBLogging.c("InterstitialVideoViewProtocol", String.format("controls starting %s, setting timer", objArr));
                    this.h.b(v.this.y);
                    v.this.a((View) this.h, (Runnable) new Runnable() {
                        public void run() {
                            Object[] objArr = new Object[1];
                            objArr[0] = v.this.y ? ViewProps.HIDDEN : "shown";
                            CBLogging.c("InterstitialVideoViewProtocol", String.format("controls %s automatically from timer", objArr));
                            a aVar = a.this;
                            aVar.h.a(!v.this.y, true);
                            synchronized (v.this.g) {
                                v.this.g.remove(a.this.h);
                            }
                        }
                    }, Math.round(a2.optDouble(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_DELAY, 0.0d) * 1000.0d));
                }
                this.h.e();
                v vVar2 = v.this;
                if (vVar2.s <= 1) {
                    vVar2.e.f();
                }
            }
        }

        /* access modifiers changed from: protected */
        public void c() {
            super.c();
            v vVar = v.this;
            if (vVar.r != 0 || (vVar.z && !vVar.o())) {
                a(v.this.r, false);
            } else {
                b(false);
            }
        }

        /* access modifiers changed from: protected */
        public void d() {
            v vVar = v.this;
            if (vVar.r != 1 || vVar.e.f2409a.f2477a != 1) {
                if (v.this.r == 1) {
                    c(false);
                    this.h.h();
                    v vVar2 = v.this;
                    int i2 = vVar2.s;
                    if (i2 < 1) {
                        vVar2.s = i2 + 1;
                        vVar2.e.e();
                    }
                }
                v.this.f2420a.post(new Runnable() {
                    public void run() {
                        try {
                            v.this.h();
                        } catch (Exception e) {
                            com.chartboost.sdk.Tracking.a.a(a.class, "onCloseButton Runnable.run", e);
                        }
                    }
                });
            }
        }

        public void e() {
            c(true);
            this.h.h();
            v vVar = v.this;
            vVar.s++;
            if (vVar.s <= 1 && !vVar.u()) {
                v vVar2 = v.this;
                if (vVar2.v >= 1) {
                    vVar2.e.e();
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean f() {
            v vVar = v.this;
            if (vVar.r == 1 && vVar.s < 1) {
                StringBuilder sb = new StringBuilder();
                sb.append("close-");
                sb.append(CBUtility.a(v.this.a()) ? "portrait" : "landscape");
                JSONObject a2 = e.a(v.this.g(), sb.toString());
                float optDouble = a2 != null ? (float) a2.optDouble(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_DELAY, -1.0d) : -1.0f;
                int round = optDouble >= 0.0f ? Math.round(optDouble * 1000.0f) : -1;
                v.this.D = round;
                if (round < 0 || round > this.h.b().d()) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void g() {
            v.this.z = false;
            b(true);
        }

        private a(Context context) {
            super(context);
            JSONObject optJSONObject;
            JSONObject optJSONObject2;
            g a2 = g.a();
            if (v.this.N) {
                this.n = new View(context);
                this.n.setBackgroundColor(-16777216);
                this.n.setVisibility(8);
                addView(this.n);
            }
            if (v.this.e.n == 2) {
                this.i = (y) a2.a(new y(context, v.this));
                this.i.setVisibility(8);
                addView(this.i);
            }
            this.h = (ab) a2.a(new ab(context, v.this));
            a((View) this.h.g);
            this.h.setVisibility(8);
            addView(this.h);
            this.j = (t) a2.a(new t(context, v.this));
            this.j.setVisibility(8);
            addView(this.j);
            if (v.this.e.n == 2) {
                this.k = (w) a2.a(new w(context, v.this));
                this.k.setVisibility(8);
                addView(this.k);
            } else {
                this.k = null;
            }
            this.m = new az(getContext(), v.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a aVar = a.this;
                    if (v.this.e.n == 2) {
                        aVar.k.a(false);
                    }
                    a aVar2 = a.this;
                    if (v.this.r == 1) {
                        aVar2.c(false);
                    }
                    a.this.b(true);
                }
            };
            this.m.setVisibility(8);
            addView(this.m);
            this.o = new az(getContext(), v.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a.this.d();
                }
            };
            this.o.setVisibility(8);
            this.o.setContentDescription("CBClose");
            addView(this.o);
            JSONObject optJSONObject3 = v.this.x.optJSONObject("progress");
            JSONObject optJSONObject4 = v.this.x.optJSONObject("video-controls-background");
            if (optJSONObject3 != null && !optJSONObject3.isNull("background-color") && !optJSONObject3.isNull("border-color") && !optJSONObject3.isNull("progress-color") && !optJSONObject3.isNull("radius")) {
                v.this.M = true;
                x c = this.h.c();
                c.a(com.chartboost.sdk.e.a(optJSONObject3.optString("background-color")));
                c.b(com.chartboost.sdk.e.a(optJSONObject3.optString("border-color")));
                c.c(com.chartboost.sdk.e.a(optJSONObject3.optString("progress-color")));
                c.b((float) optJSONObject3.optDouble("radius", 0.0d));
            }
            if (optJSONObject4 != null && !optJSONObject4.isNull(ViewProps.COLOR)) {
                this.h.a(com.chartboost.sdk.e.a(optJSONObject4.optString(ViewProps.COLOR)));
            }
            if (v.this.e.n == 2 && v.this.A && (optJSONObject2 = v.this.x.optJSONObject("post-video-toaster")) != null) {
                this.j.a(optJSONObject2.optString("title"), optJSONObject2.optString("tagline"));
            }
            if (v.this.e.n == 2 && v.this.z && (optJSONObject = v.this.x.optJSONObject("confirmation")) != null) {
                this.i.a(optJSONObject.optString("text"), com.chartboost.sdk.e.a(optJSONObject.optString(ViewProps.COLOR)));
            }
            String str = "";
            if (v.this.e.n == 2 && v.this.B) {
                JSONObject a3 = e.a(v.this.x, "post-video-reward-toaster");
                this.k.a((a3 == null || !a3.optString(ViewProps.POSITION).equals("inside-top")) ? 1 : 0);
                this.k.a(a3 != null ? a3.optString("text") : str);
                if (v.this.J.c()) {
                    this.k.a(v.this.L);
                }
            }
            JSONObject g = v.this.g();
            if (g == null || g.isNull("video-click-button")) {
                this.h.d();
            }
            this.h.d(v.this.x.optBoolean("video-progress-timer-enabled"));
            if (v.this.O || v.this.N) {
                this.f.setVisibility(4);
            }
            String[] strArr = new String[1];
            strArr[0] = CBUtility.a(v.this.a()) ? "video-portrait" : "video-landscape";
            JSONObject a4 = e.a(g, strArr);
            v.this.u = a4 != null ? a4.optString("id") : str;
            if (v.this.u.isEmpty()) {
                v.this.a(CBError.CBImpressionError.VIDEO_ID_MISSING);
                return;
            }
            if (v.this.t == null) {
                v.this.t = v.this.q.a(v.this.u);
            }
            String str2 = v.this.t;
            if (str2 == null) {
                v.this.a(CBError.CBImpressionError.VIDEO_UNAVAILABLE);
            } else {
                this.h.a(str2);
            }
        }

        /* access modifiers changed from: package-private */
        public void c(boolean z) {
            JSONObject jSONObject;
            this.h.f();
            v vVar = v.this;
            if (vVar.r == 1 && z) {
                if (vVar.s < 1 && (jSONObject = vVar.x) != null && !jSONObject.isNull("post-video-reward-toaster")) {
                    v vVar2 = v.this;
                    if (vVar2.B && vVar2.J.c() && v.this.K.c()) {
                        e(true);
                    }
                }
                a(2, true);
                if (CBUtility.a(CBUtility.a())) {
                    requestLayout();
                }
            }
        }

        private void e(boolean z) {
            if (z) {
                this.k.a(true);
            } else {
                this.k.setVisibility(0);
            }
            v.this.f2420a.postDelayed(new Runnable() {
                public void run() {
                    a.this.k.a(false);
                }
            }, 2500);
        }

        public az d(boolean z) {
            return ((!v.this.s() || !z) && (v.this.s() || z)) ? this.d : this.o;
        }

        public void b() {
            v.this.n();
            super.b();
        }

        /* access modifiers changed from: protected */
        public void b(float f, float f2, float f3, float f4) {
            if (v.this.r == 1) {
                c(false);
            }
            v.this.b(e.a(e.a("x", (Object) Float.valueOf(f)), e.a("y", (Object) Float.valueOf(f2)), e.a("w", (Object) Float.valueOf(f3)), e.a("h", (Object) Float.valueOf(f4))));
        }

        private void a(int i2, boolean z) {
            v vVar = v.this;
            vVar.r = i2;
            boolean z2 = true;
            if (i2 == 0) {
                vVar.a(!vVar.s(), (View) this.e, z);
                v vVar2 = v.this;
                if (vVar2.e.n == 2) {
                    vVar2.a(true, (View) this.i, z);
                }
                v vVar3 = v.this;
                if (vVar3.N) {
                    vVar3.a(false, this.n, z);
                }
                v.this.a(false, (View) this.h, z);
                v.this.a(false, (View) this.m, z);
                v.this.a(false, (View) this.j, z);
                this.e.setEnabled(false);
                this.m.setEnabled(false);
                this.h.setEnabled(false);
            } else if (i2 == 1) {
                vVar.a(false, (View) this.e, z);
                v vVar4 = v.this;
                if (vVar4.e.n == 2) {
                    vVar4.a(false, (View) this.i, z);
                }
                v vVar5 = v.this;
                if (vVar5.N) {
                    vVar5.a(true, this.n, z);
                }
                v.this.a(true, (View) this.h, z);
                v.this.a(false, (View) this.m, z);
                v.this.a(false, (View) this.j, z);
                this.e.setEnabled(true);
                this.m.setEnabled(false);
                this.h.setEnabled(true);
            } else if (i2 == 2) {
                vVar.a(true, (View) this.e, z);
                v vVar6 = v.this;
                if (vVar6.e.n == 2) {
                    vVar6.a(false, (View) this.i, z);
                }
                v vVar7 = v.this;
                if (vVar7.N) {
                    vVar7.a(false, this.n, z);
                }
                v.this.a(false, (View) this.h, z);
                v.this.a(true, (View) this.m, z);
                v.this.a(v.this.K.c() && v.this.J.c() && v.this.A, (View) this.j, z);
                this.m.setEnabled(true);
                this.e.setEnabled(true);
                this.h.setEnabled(false);
                if (v.this.C) {
                    e(false);
                }
            }
            boolean f = f();
            az d = d(true);
            d.setEnabled(f);
            v.this.a(f, (View) d, z);
            az d2 = d(false);
            d2.setEnabled(false);
            v.this.a(false, (View) d2, z);
            v vVar8 = v.this;
            if (vVar8.O || vVar8.N) {
                v vVar9 = v.this;
                vVar9.a(!vVar9.s(), (View) this.f, z);
            }
            v vVar10 = v.this;
            vVar10.a(!vVar10.s(), (View) this.c, z);
            if (i2 == 0) {
                z2 = false;
            }
            a(z2);
        }

        /* access modifiers changed from: protected */
        public void a(float f, float f2, float f3, float f4) {
            v vVar = v.this;
            if ((!vVar.y || vVar.r != 1) && v.this.r != 0) {
                b(f, f2, f3, f4);
            }
        }
    }

    public v(c cVar, f fVar, Handler handler, com.chartboost.sdk.c cVar2) {
        super(cVar, handler, cVar2);
        this.q = fVar;
        this.r = 0;
        this.E = new h(this);
        this.F = new h(this);
        this.G = new h(this);
        this.H = new h(this);
        this.I = new h(this);
        this.J = new h(this);
        this.K = new h(this);
        this.L = new h(this);
        this.s = 0;
    }

    public boolean a(JSONObject jSONObject) {
        if (!super.a(jSONObject)) {
            return false;
        }
        this.x = jSONObject.optJSONObject("ux");
        if (this.x == null) {
            this.x = e.a(new e.a[0]);
        }
        if (this.d.isNull("video-landscape") || this.d.isNull("replay-landscape")) {
            this.i = false;
        }
        if (!this.E.a("replay-landscape") || !this.F.a("replay-portrait") || !this.I.a("video-click-button") || !this.J.a("post-video-reward-icon") || !this.K.a("post-video-button") || !this.G.a("video-confirmation-button") || !this.H.a("video-confirmation-icon") || !this.L.a("post-video-reward-icon")) {
            CBLogging.b("InterstitialVideoViewProtocol", "Error while downloading the assets");
            a(CBError.CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
            return false;
        }
        this.y = this.x.optBoolean("video-controls-togglable");
        this.N = jSONObject.optBoolean("fullscreen");
        this.O = jSONObject.optBoolean("preroll_popup_fullscreen");
        if (this.e.n == 2) {
            JSONObject optJSONObject = this.x.optJSONObject("confirmation");
            JSONObject optJSONObject2 = this.x.optJSONObject("post-video-toaster");
            if (optJSONObject2 != null && !optJSONObject2.isNull("title") && !optJSONObject2.isNull("tagline")) {
                this.A = true;
            }
            if (optJSONObject != null && !optJSONObject.isNull("text") && !optJSONObject.isNull(ViewProps.COLOR)) {
                this.z = true;
            }
            if (!this.x.isNull("post-video-reward-toaster")) {
                this.B = true;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public e.a b(Context context) {
        return new a(context);
    }

    public void d() {
        super.d();
        this.E = null;
        this.F = null;
        this.I = null;
        this.J = null;
        this.K = null;
        this.G = null;
        this.H = null;
        this.L = null;
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.z && (!this.G.c() || !this.H.c())) {
            this.z = false;
        }
        super.i();
    }

    public float j() {
        return (float) this.w;
    }

    public float k() {
        return (float) this.v;
    }

    public boolean l() {
        e().d();
        return true;
    }

    public void m() {
        super.m();
        if (this.r == 1 && this.P) {
            e().h.b().a(this.v);
            e().h.e();
        }
        this.P = false;
    }

    public void n() {
        super.n();
        if (this.r == 1 && !this.P) {
            this.P = true;
            e().h.g();
        }
    }

    public boolean o() {
        return this.e.n == 1;
    }

    public boolean p() {
        return this.r == 1;
    }

    /* renamed from: q */
    public a e() {
        return (a) super.e();
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.e.p();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x000b A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean s() {
        /*
            r4 = this;
            int r0 = r4.r
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x001c
            if (r0 == r2) goto L_0x000d
            r3 = 2
            if (r0 == r3) goto L_0x002b
        L_0x000b:
            r1 = 1
            goto L_0x002b
        L_0x000d:
            boolean r0 = r4.N
            if (r0 != 0) goto L_0x000b
            int r0 = com.chartboost.sdk.Libraries.CBUtility.a()
            boolean r0 = com.chartboost.sdk.Libraries.CBUtility.a((int) r0)
            if (r0 == 0) goto L_0x002b
            goto L_0x000b
        L_0x001c:
            boolean r0 = r4.O
            if (r0 != 0) goto L_0x000b
            int r0 = com.chartboost.sdk.Libraries.CBUtility.a()
            boolean r0 = com.chartboost.sdk.Libraries.CBUtility.a((int) r0)
            if (r0 == 0) goto L_0x002b
            goto L_0x000b
        L_0x002b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.v.s():boolean");
    }

    public boolean t() {
        return this.Q;
    }

    public boolean u() {
        return this.R;
    }

    public void v() {
        String str = this.t;
        if (str != null) {
            new File(str).delete();
        }
        this.R = true;
        a(CBError.CBImpressionError.ERROR_PLAYING_VIDEO);
    }

    public void a(boolean z2) {
        this.Q = z2;
    }
}
