package com.chartboost.sdk.impl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.Surface;
import android.view.TextureView;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.impl.av;
import com.vungle.warren.ui.contract.AdContract;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class au extends TextureView implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, TextureView.SurfaceTextureListener, av.a {

    /* renamed from: a  reason: collision with root package name */
    private final String f2454a = "VideoTextureView";
    private Uri b;
    private Map<String, String> c;
    private int d;
    private int e = 0;
    private int f = 0;
    private Surface g = null;
    private MediaPlayer h = null;
    private int i;
    private int j;
    private MediaPlayer.OnCompletionListener k;
    private MediaPlayer.OnPreparedListener l;
    private int m;
    private MediaPlayer.OnErrorListener n;
    private int o;

    public au(Context context) {
        super(context);
        f();
    }

    private void f() {
        this.i = 0;
        this.j = 0;
        setSurfaceTextureListener(this);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.e = 0;
        this.f = 0;
    }

    private void g() {
        if (this.b != null && this.g != null) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra(AdContract.AdvertisementBus.COMMAND, "pause");
            getContext().sendBroadcast(intent);
            a(false);
            h();
            try {
                this.h = new MediaPlayer();
                this.h.setOnPreparedListener(this);
                this.h.setOnVideoSizeChangedListener(this);
                this.d = -1;
                this.h.setOnCompletionListener(this);
                this.h.setOnErrorListener(this);
                this.h.setOnBufferingUpdateListener(this);
                this.m = 0;
                FileInputStream fileInputStream = new FileInputStream(new File(this.b.toString()));
                this.h.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
                this.h.setSurface(this.g);
                this.h.setAudioStreamType(3);
                this.h.setScreenOnWhilePlaying(true);
                this.h.prepareAsync();
                this.e = 1;
            } catch (IOException e2) {
                CBLogging.c("VideoTextureView", "Unable to open content: " + this.b, e2);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            } catch (IllegalArgumentException e3) {
                CBLogging.c("VideoTextureView", "Unable to open content: " + this.b, e3);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            }
        }
    }

    private void h() {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            if (s.a().a(16)) {
                mediaMetadataRetriever.setDataSource(this.b.toString());
            } else {
                FileInputStream fileInputStream = new FileInputStream(this.b.toString());
                mediaMetadataRetriever.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(19);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(18);
            this.j = Integer.parseInt(extractMetadata);
            this.i = Integer.parseInt(extractMetadata2);
        } catch (Exception e2) {
            CBLogging.c("play video", "read size error", e2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        r0 = r3.e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean i() {
        /*
            r3 = this;
            android.media.MediaPlayer r0 = r3.h
            r1 = 1
            if (r0 == 0) goto L_0x000f
            int r0 = r3.e
            r2 = -1
            if (r0 == r2) goto L_0x000f
            if (r0 == 0) goto L_0x000f
            if (r0 == r1) goto L_0x000f
            goto L_0x0010
        L_0x000f:
            r1 = 0
        L_0x0010:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.au.i():boolean");
    }

    public void a(Uri uri) {
        a(uri, (Map<String, String>) null);
    }

    public void b() {
        if (i() && this.h.isPlaying()) {
            this.h.pause();
            this.e = 4;
        }
        this.f = 4;
    }

    public int c() {
        if (i()) {
            int i2 = this.d;
            if (i2 > 0) {
                return i2;
            }
            this.d = this.h.getDuration();
            return this.d;
        }
        this.d = -1;
        return this.d;
    }

    public int d() {
        if (i()) {
            return this.h.getCurrentPosition();
        }
        return 0;
    }

    public boolean e() {
        return i() && this.h.isPlaying();
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
        this.m = i2;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f = 5;
        if (this.e != 5) {
            this.e = 5;
            MediaPlayer.OnCompletionListener onCompletionListener = this.k;
            if (onCompletionListener != null) {
                onCompletionListener.onCompletion(this.h);
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        CBLogging.a("VideoTextureView", "Error: " + i2 + "," + i3);
        if (i2 == 100) {
            g();
            return true;
        }
        this.e = -1;
        this.f = -1;
        MediaPlayer.OnErrorListener onErrorListener = this.n;
        if (onErrorListener == null || onErrorListener.onError(this.h, i2, i3)) {
        }
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.e = 2;
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        MediaPlayer.OnPreparedListener onPreparedListener = this.l;
        if (onPreparedListener != null) {
            onPreparedListener.onPrepared(this.h);
        }
        int i2 = this.o;
        if (i2 != 0) {
            a(i2);
        }
        if (this.f == 3) {
            a();
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.g = new Surface(surfaceTexture);
        g();
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.g = null;
        a(true);
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        boolean z = this.f == 3;
        if (this.h != null && z) {
            int i4 = this.o;
            if (i4 != 0) {
                a(i4);
            }
            a();
        }
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        if (this.i != 0 && this.j != 0) {
            a(getWidth(), getHeight());
        }
    }

    public void a(int i2, int i3) {
        int i4;
        int i5 = this.i;
        if (i5 != 0 && (i4 = this.j) != 0 && i2 != 0 && i3 != 0) {
            float f2 = (float) i2;
            float f3 = (float) i3;
            float min = Math.min(f2 / ((float) i5), f3 / ((float) i4));
            float f4 = ((float) this.i) * min;
            float f5 = min * ((float) this.j);
            Matrix matrix = new Matrix();
            matrix.setScale(f4 / f2, f5 / f3, f2 / 2.0f, f3 / 2.0f);
            setTransform(matrix);
        }
    }

    public void a(Uri uri, Map<String, String> map) {
        this.b = uri;
        this.c = map;
        this.o = 0;
        g();
        requestLayout();
        invalidate();
    }

    public void a(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.l = onPreparedListener;
    }

    public void a(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.k = onCompletionListener;
    }

    public void a(MediaPlayer.OnErrorListener onErrorListener) {
        this.n = onErrorListener;
    }

    private void a(boolean z) {
        MediaPlayer mediaPlayer = this.h;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            this.h.release();
            this.h = null;
            this.e = 0;
            if (z) {
                this.f = 0;
            }
        }
    }

    public void a() {
        if (i()) {
            this.h.start();
            this.e = 3;
        }
        this.f = 3;
    }

    public void a(int i2) {
        if (i()) {
            this.h.seekTo(i2);
            this.o = 0;
            return;
        }
        this.o = i2;
    }
}
