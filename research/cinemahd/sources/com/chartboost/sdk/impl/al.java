package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.h;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class al {

    /* renamed from: a  reason: collision with root package name */
    private final f f2449a;
    private final Map<String, h.a> b = new HashMap();

    public al(f fVar) {
        this.f2449a = fVar;
    }

    private boolean b(String str) {
        return this.f2449a.b(String.format("%s%s", new Object[]{str, ".png"}));
    }

    public h.a a(String str) {
        if (b(str)) {
            if (this.b.containsKey(str)) {
                return this.b.get(str);
            }
            h.a aVar = new h.a(str, new File(this.f2449a.d().d, String.format("%s%s", new Object[]{str, ".png"})), this.f2449a);
            this.b.put(str, aVar);
            return aVar;
        } else if (!this.b.containsKey(str)) {
            return null;
        } else {
            this.b.remove(str);
            return null;
        }
    }
}
