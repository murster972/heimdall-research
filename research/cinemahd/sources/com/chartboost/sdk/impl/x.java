package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

public class x extends bb {

    /* renamed from: a  reason: collision with root package name */
    private Paint f2503a;
    private Paint b;
    private Path c;
    private RectF d;
    private RectF e;
    private int f = 0;
    private float g;
    private float h;

    public x(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        float f2 = context.getResources().getDisplayMetrics().density;
        this.g = 4.5f * f2;
        this.f2503a = new Paint();
        this.f2503a.setColor(-1);
        this.f2503a.setStyle(Paint.Style.STROKE);
        this.f2503a.setStrokeWidth(f2 * 1.0f);
        this.f2503a.setAntiAlias(true);
        this.b = new Paint();
        this.b.setColor(-855638017);
        this.b.setStyle(Paint.Style.FILL);
        this.b.setAntiAlias(true);
        this.c = new Path();
        this.e = new RectF();
        this.d = new RectF();
    }

    public void b(int i) {
        this.f2503a.setColor(i);
        invalidate();
    }

    public void c(int i) {
        this.b.setColor(i);
        invalidate();
    }

    public void b(float f2) {
        this.g = f2;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        float f2 = getContext().getResources().getDisplayMetrics().density;
        this.d.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        float min = (float) Math.min(1, Math.round(f2 * 0.5f));
        this.d.inset(min, min);
        this.c.reset();
        Path path = this.c;
        RectF rectF = this.d;
        float f3 = this.g;
        path.addRoundRect(rectF, f3, f3, Path.Direction.CW);
        canvas.save();
        canvas.clipPath(this.c);
        canvas.drawColor(this.f);
        this.e.set(this.d);
        RectF rectF2 = this.e;
        float f4 = rectF2.right;
        float f5 = rectF2.left;
        rectF2.right = ((f4 - f5) * this.h) + f5;
        canvas.drawRect(rectF2, this.b);
        canvas.restore();
        RectF rectF3 = this.d;
        float f6 = this.g;
        canvas.drawRoundRect(rectF3, f6, f6, this.f2503a);
    }

    public void a(int i) {
        this.f = i;
        invalidate();
    }

    public void a(float f2) {
        this.h = f2;
        if (getVisibility() != 8) {
            invalidate();
        }
    }
}
