package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.h;

public class ay extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f2459a = null;
    private h b = null;

    public ay(Context context) {
        super(context);
    }

    public void a(h hVar) {
        if (hVar != null && hVar.c() && this.b != hVar) {
            this.b = hVar;
            setImageDrawable(new BitmapDrawable(hVar.d()));
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        a(canvas);
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.b = null;
        setImageDrawable(new BitmapDrawable(bitmap));
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        TextView textView = this.f2459a;
        if (textView != null) {
            textView.layout(0, 0, canvas.getWidth(), canvas.getHeight());
            this.f2459a.setEnabled(isEnabled());
            this.f2459a.setSelected(isSelected());
            if (isFocused()) {
                this.f2459a.requestFocus();
            } else {
                this.f2459a.clearFocus();
            }
            this.f2459a.setPressed(isPressed());
            this.f2459a.draw(canvas);
        }
    }
}
