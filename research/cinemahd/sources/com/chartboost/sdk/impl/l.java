package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import java.io.File;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class l {

    /* renamed from: a  reason: collision with root package name */
    int f2489a = 1;
    private final Executor b;
    private final ah c;
    private final ai d;
    private final AtomicReference<e> e;
    private final i f;
    private final a g;
    private final f h;
    private k i = null;
    private final PriorityQueue<j> j;

    public l(Executor executor, f fVar, ah ahVar, ai aiVar, AtomicReference<e> atomicReference, i iVar, a aVar) {
        this.b = executor;
        this.h = fVar;
        this.c = ahVar;
        this.d = aiVar;
        this.e = atomicReference;
        this.f = iVar;
        this.g = aVar;
        this.j = new PriorityQueue<>();
    }

    private void d() {
        j poll;
        j peek;
        if (!(this.i == null || (peek = this.j.peek()) == null)) {
            k kVar = this.i;
            if (kVar.f2488a.f2487a > peek.f2487a && kVar.b()) {
                this.j.add(this.i.f2488a);
                this.i = null;
            }
        }
        while (this.i == null && (poll = this.j.poll()) != null) {
            if (poll.e.get() > 0) {
                File file = new File(this.h.d().f2398a, poll.d);
                if (file.exists() || file.mkdirs() || file.isDirectory()) {
                    File file2 = new File(file, poll.b);
                    if (file2.exists()) {
                        this.h.c(file2);
                        poll.a(this.b, true);
                    } else {
                        this.i = new k(this, this.d, poll, file2);
                        this.c.a(this.i);
                        this.g.a(poll.c, poll.b);
                    }
                } else {
                    CBLogging.b("Downloader", "Unable to create directory " + file.getPath());
                    poll.a(this.b, false);
                }
            }
        }
        if (this.i != null) {
            if (this.f2489a != 2) {
                CBLogging.a("Downloader", "Change state to DOWNLOADING");
                this.f2489a = 2;
            }
        } else if (this.f2489a != 1) {
            CBLogging.a("Downloader", "Change state to IDLE");
            this.f2489a = 1;
        }
    }

    public synchronized void a(int i2, Map<String, b> map, AtomicInteger atomicInteger, h hVar) {
        synchronized (this) {
            long b2 = this.f.b();
            AtomicInteger atomicInteger2 = new AtomicInteger();
            AtomicReference atomicReference = new AtomicReference(hVar);
            for (b next : map.values()) {
                long j2 = b2;
                long j3 = b2;
                j jVar = r2;
                j jVar2 = new j(this.f, i2, next.b, next.c, next.f2408a, atomicInteger, atomicReference, j2, atomicInteger2);
                this.j.add(jVar);
                b2 = j3;
            }
            if (this.f2489a == 1 || this.f2489a == 2) {
                d();
            }
        }
    }

    public synchronized void b() {
        int i2 = this.f2489a;
        if (!(i2 == 1 || i2 == 2)) {
            if (i2 == 3) {
                CBLogging.a("Downloader", "Change state to DOWNLOADING");
                this.f2489a = 2;
            } else if (i2 == 4) {
                CBLogging.a("Downloader", "Change state to IDLE");
                this.f2489a = 1;
                d();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x0142 A[Catch:{ Exception -> 0x0198 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0183 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void c() {
        /*
            r24 = this;
            r1 = r24
            monitor-enter(r24)
            int r0 = r1.f2489a     // Catch:{ all -> 0x01a4 }
            r2 = 1
            if (r0 == r2) goto L_0x000a
            monitor-exit(r24)
            return
        L_0x000a:
            java.lang.String r0 = "Downloader"
            java.lang.String r3 = "########### Trimming the disk cache"
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r3)     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.f r0 = r1.h     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.g r0 = r0.d()     // Catch:{ Exception -> 0x0198 }
            java.io.File r0 = r0.f2398a     // Catch:{ Exception -> 0x0198 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String[] r4 = r0.list()     // Catch:{ Exception -> 0x0198 }
            if (r4 == 0) goto L_0x0065
            int r6 = r4.length     // Catch:{ Exception -> 0x0198 }
            if (r6 <= 0) goto L_0x0065
            int r6 = r4.length     // Catch:{ Exception -> 0x0198 }
            r7 = 0
        L_0x0029:
            if (r7 >= r6) goto L_0x0065
            r8 = r4[r7]     // Catch:{ Exception -> 0x0198 }
            java.lang.String r9 = "requests"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "track"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "session"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "videoCompletionEvents"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "."
            boolean r9 = r8.contains(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 == 0) goto L_0x0056
            goto L_0x0062
        L_0x0056:
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x0198 }
            r9.<init>(r0, r8)     // Catch:{ Exception -> 0x0198 }
            java.util.ArrayList r8 = com.chartboost.sdk.Libraries.CBUtility.a((java.io.File) r9, (boolean) r2)     // Catch:{ Exception -> 0x0198 }
            r3.addAll(r8)     // Catch:{ Exception -> 0x0198 }
        L_0x0062:
            int r7 = r7 + 1
            goto L_0x0029
        L_0x0065:
            int r0 = r3.size()     // Catch:{ Exception -> 0x0198 }
            java.io.File[] r0 = new java.io.File[r0]     // Catch:{ Exception -> 0x0198 }
            r3.toArray(r0)     // Catch:{ Exception -> 0x0198 }
            int r3 = r0.length     // Catch:{ Exception -> 0x0198 }
            if (r3 <= r2) goto L_0x0079
            com.chartboost.sdk.impl.l$1 r3 = new com.chartboost.sdk.impl.l$1     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.util.Arrays.sort(r0, r3)     // Catch:{ Exception -> 0x0198 }
        L_0x0079:
            int r3 = r0.length     // Catch:{ Exception -> 0x0198 }
            if (r3 <= 0) goto L_0x018c
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.Model.e> r3 = r1.e     // Catch:{ Exception -> 0x0198 }
            java.lang.Object r3 = r3.get()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Model.e r3 = (com.chartboost.sdk.Model.e) r3     // Catch:{ Exception -> 0x0198 }
            int r4 = r3.u     // Catch:{ Exception -> 0x0198 }
            long r6 = (long) r4     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.f r4 = r1.h     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.f r8 = r1.h     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.g r8 = r8.d()     // Catch:{ Exception -> 0x0198 }
            java.io.File r8 = r8.g     // Catch:{ Exception -> 0x0198 }
            long r8 = r4.b((java.io.File) r8)     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.i r4 = r1.f     // Catch:{ Exception -> 0x0198 }
            long r10 = r4.a()     // Catch:{ Exception -> 0x0198 }
            java.util.List<java.lang.String> r4 = r3.d     // Catch:{ Exception -> 0x0198 }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r13.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r14 = "Total local file count:"
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            int r14 = r0.length     // Catch:{ Exception -> 0x0198 }
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r13)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r13.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r14 = "Video Folder Size in bytes :"
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            r13.append(r8)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r13)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r13.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r14 = "Max Bytes allowed:"
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            r13.append(r6)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r13)     // Catch:{ Exception -> 0x0198 }
            int r12 = r0.length     // Catch:{ Exception -> 0x0198 }
            r13 = r8
            r8 = 0
        L_0x00e3:
            if (r8 >= r12) goto L_0x018c
            r9 = r0[r8]     // Catch:{ Exception -> 0x0198 }
            java.util.concurrent.TimeUnit r15 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ Exception -> 0x0198 }
            long r16 = r9.lastModified()     // Catch:{ Exception -> 0x0198 }
            r18 = r6
            long r5 = r10 - r16
            long r5 = r15.toDays(r5)     // Catch:{ Exception -> 0x0198 }
            int r7 = r3.w     // Catch:{ Exception -> 0x0198 }
            r16 = r3
            long r2 = (long) r7     // Catch:{ Exception -> 0x0198 }
            int r7 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r7 < 0) goto L_0x0100
            r2 = 1
            goto L_0x0101
        L_0x0100:
            r2 = 0
        L_0x0101:
            java.lang.String r3 = r9.getName()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = ".tmp"
            boolean r3 = r3.endsWith(r5)     // Catch:{ Exception -> 0x0198 }
            java.io.File r5 = r9.getParentFile()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r6 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r7 = "/videos"
            boolean r6 = r6.contains(r7)     // Catch:{ Exception -> 0x0198 }
            int r7 = (r13 > r18 ? 1 : (r13 == r18 ? 0 : -1))
            if (r7 <= 0) goto L_0x0121
            if (r6 == 0) goto L_0x0121
            r7 = 1
            goto L_0x0122
        L_0x0121:
            r7 = 0
        L_0x0122:
            long r20 = r9.length()     // Catch:{ Exception -> 0x0198 }
            r22 = 0
            int r17 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
            if (r17 == 0) goto L_0x013f
            if (r3 != 0) goto L_0x013f
            if (r2 != 0) goto L_0x013f
            java.lang.String r2 = r5.getName()     // Catch:{ Exception -> 0x0198 }
            boolean r2 = r4.contains(r2)     // Catch:{ Exception -> 0x0198 }
            if (r2 != 0) goto L_0x013f
            if (r7 == 0) goto L_0x013d
            goto L_0x013f
        L_0x013d:
            r2 = 0
            goto L_0x0140
        L_0x013f:
            r2 = 1
        L_0x0140:
            if (r2 == 0) goto L_0x0183
            if (r6 == 0) goto L_0x0149
            long r2 = r9.length()     // Catch:{ Exception -> 0x0198 }
            long r13 = r13 - r2
        L_0x0149:
            java.lang.String r2 = "Downloader"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = "Deleting file at path:"
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = r9.getPath()     // Catch:{ Exception -> 0x0198 }
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.CBLogging.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
            boolean r2 = r9.delete()     // Catch:{ Exception -> 0x0198 }
            if (r2 != 0) goto L_0x0183
            java.lang.String r2 = "Downloader"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = "Unable to delete "
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = r9.getPath()     // Catch:{ Exception -> 0x0198 }
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Libraries.CBLogging.b(r2, r3)     // Catch:{ Exception -> 0x0198 }
        L_0x0183:
            int r8 = r8 + 1
            r3 = r16
            r6 = r18
            r2 = 1
            goto L_0x00e3
        L_0x018c:
            com.chartboost.sdk.Libraries.f r0 = r1.h     // Catch:{ Exception -> 0x0198 }
            org.json.JSONObject r0 = r0.e()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.Tracking.a r2 = r1.g     // Catch:{ Exception -> 0x0198 }
            r2.a((org.json.JSONObject) r0)     // Catch:{ Exception -> 0x0198 }
            goto L_0x01a2
        L_0x0198:
            r0 = move-exception
            java.lang.Class r2 = r24.getClass()     // Catch:{ all -> 0x01a4 }
            java.lang.String r3 = "reduceCacheSize"
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r2, (java.lang.String) r3, (java.lang.Exception) r0)     // Catch:{ all -> 0x01a4 }
        L_0x01a2:
            monitor-exit(r24)
            return
        L_0x01a4:
            r0 = move-exception
            monitor-exit(r24)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.l.c():void");
    }

    public synchronized void a(AtomicInteger atomicInteger) {
        atomicInteger.set(-10000);
        int i2 = this.f2489a;
        boolean z = true;
        if (i2 != 1) {
            if (i2 == 2) {
                if (this.i.f2488a.e != atomicInteger) {
                    z = false;
                }
                if (z && this.i.b()) {
                    this.i = null;
                    d();
                }
            }
        }
    }

    public synchronized void a() {
        int i2 = this.f2489a;
        if (i2 == 1) {
            CBLogging.a("Downloader", "Change state to PAUSED");
            this.f2489a = 4;
        } else if (i2 == 2) {
            if (this.i.b()) {
                this.j.add(this.i.f2488a);
                this.i = null;
                CBLogging.a("Downloader", "Change state to PAUSED");
                this.f2489a = 4;
            } else {
                CBLogging.a("Downloader", "Change state to PAUSING");
                this.f2489a = 3;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d7, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.chartboost.sdk.impl.k r19, com.chartboost.sdk.Model.CBError r20, com.chartboost.sdk.impl.ag r21) {
        /*
            r18 = this;
            r1 = r18
            r0 = r19
            r2 = r21
            monitor-enter(r18)
            int r3 = r1.f2489a     // Catch:{ all -> 0x00d8 }
            r4 = 1
            if (r3 == r4) goto L_0x00d6
            r5 = 2
            r6 = 3
            if (r3 == r5) goto L_0x0014
            if (r3 == r6) goto L_0x0014
            goto L_0x00d6
        L_0x0014:
            com.chartboost.sdk.impl.k r3 = r1.i     // Catch:{ all -> 0x00d8 }
            if (r0 == r3) goto L_0x001a
            monitor-exit(r18)
            return
        L_0x001a:
            com.chartboost.sdk.impl.j r3 = r0.f2488a     // Catch:{ all -> 0x00d8 }
            r5 = 0
            r1.i = r5     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d8 }
            long r7 = r0.g     // Catch:{ all -> 0x00d8 }
            long r12 = r5.toMillis(r7)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.atomic.AtomicInteger r5 = r3.f     // Catch:{ all -> 0x00d8 }
            int r7 = (int) r12     // Catch:{ all -> 0x00d8 }
            r5.addAndGet(r7)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.Executor r5 = r1.b     // Catch:{ all -> 0x00d8 }
            if (r20 != 0) goto L_0x0032
            goto L_0x0033
        L_0x0032:
            r4 = 0
        L_0x0033:
            r3.a(r5, r4)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d8 }
            long r7 = r0.h     // Catch:{ all -> 0x00d8 }
            long r14 = r4.toMillis(r7)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d8 }
            long r7 = r0.i     // Catch:{ all -> 0x00d8 }
            long r16 = r4.toMillis(r7)     // Catch:{ all -> 0x00d8 }
            if (r20 != 0) goto L_0x006c
            com.chartboost.sdk.Tracking.a r9 = r1.g     // Catch:{ all -> 0x00d8 }
            java.lang.String r10 = r3.c     // Catch:{ all -> 0x00d8 }
            r11 = r12
            r13 = r14
            r15 = r16
            r9.a((java.lang.String) r10, (long) r11, (long) r13, (long) r15)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = "Downloader"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r2.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "Downloaded "
            r2.append(r4)     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = r3.c     // Catch:{ all -> 0x00d8 }
            r2.append(r3)     // Catch:{ all -> 0x00d8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d8 }
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r2)     // Catch:{ all -> 0x00d8 }
            goto L_0x00c4
        L_0x006c:
            java.lang.String r0 = r20.b()     // Catch:{ all -> 0x00d8 }
            com.chartboost.sdk.Tracking.a r9 = r1.g     // Catch:{ all -> 0x00d8 }
            java.lang.String r10 = r3.c     // Catch:{ all -> 0x00d8 }
            r11 = r0
            r9.a((java.lang.String) r10, (java.lang.String) r11, (long) r12, (long) r14, (long) r16)     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "Downloader"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r5.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r7 = "Failed to download "
            r5.append(r7)     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = r3.c     // Catch:{ all -> 0x00d8 }
            r5.append(r3)     // Catch:{ all -> 0x00d8 }
            if (r2 == 0) goto L_0x009f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r3.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r7 = " Status code="
            r3.append(r7)     // Catch:{ all -> 0x00d8 }
            int r2 = r2.f2441a     // Catch:{ all -> 0x00d8 }
            r3.append(r2)     // Catch:{ all -> 0x00d8 }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x00d8 }
            goto L_0x00a1
        L_0x009f:
            java.lang.String r2 = ""
        L_0x00a1:
            r5.append(r2)     // Catch:{ all -> 0x00d8 }
            if (r0 == 0) goto L_0x00b8
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r2.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = " Error message="
            r2.append(r3)     // Catch:{ all -> 0x00d8 }
            r2.append(r0)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00d8 }
            goto L_0x00ba
        L_0x00b8:
            java.lang.String r0 = ""
        L_0x00ba:
            r5.append(r0)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x00d8 }
            com.chartboost.sdk.Libraries.CBLogging.a(r4, r0)     // Catch:{ all -> 0x00d8 }
        L_0x00c4:
            int r0 = r1.f2489a     // Catch:{ all -> 0x00d8 }
            if (r0 != r6) goto L_0x00d3
            java.lang.String r0 = "Downloader"
            java.lang.String r2 = "Change state to PAUSED"
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r2)     // Catch:{ all -> 0x00d8 }
            r0 = 4
            r1.f2489a = r0     // Catch:{ all -> 0x00d8 }
            goto L_0x00d6
        L_0x00d3:
            r18.d()     // Catch:{ all -> 0x00d8 }
        L_0x00d6:
            monitor-exit(r18)
            return
        L_0x00d8:
            r0 = move-exception
            monitor-exit(r18)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.l.a(com.chartboost.sdk.impl.k, com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ag):void");
    }
}
