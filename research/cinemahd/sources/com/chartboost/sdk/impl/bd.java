package com.chartboost.sdk.impl;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.chartboost.sdk.Libraries.CBLogging;
import org.json.JSONException;
import org.json.JSONObject;

public class bd extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private View f2466a;
    private ViewGroup b;
    private boolean c = false;
    private FrameLayout d;
    private WebChromeClient.CustomViewCallback e;
    private a f;
    private final bf g;
    private final Handler h;

    public interface a {
        void a(boolean z);
    }

    public bd(View view, ViewGroup viewGroup, View view2, be beVar, bf bfVar, Handler handler) {
        this.f2466a = view;
        this.b = viewGroup;
        this.g = bfVar;
        this.h = handler;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(org.json.JSONObject r17, java.lang.String r18) {
        /*
            r16 = this;
            r6 = r16
            r4 = r18
            int r0 = r18.hashCode()
            r3 = 4
            r5 = 6
            r7 = 7
            r8 = 2
            r9 = 12
            r10 = 10
            r11 = 11
            r12 = 9
            r13 = 1
            r14 = 0
            switch(r0) {
                case -2012425132: goto L_0x00fa;
                case -1757019252: goto L_0x00ef;
                case -1554056650: goto L_0x00e5;
                case -1263203643: goto L_0x00da;
                case -1086137328: goto L_0x00d0;
                case -715147645: goto L_0x00c5;
                case -640720077: goto L_0x00bb;
                case 3529469: goto L_0x00b0;
                case 94750088: goto L_0x00a6;
                case 94756344: goto L_0x009c;
                case 95458899: goto L_0x0090;
                case 96784904: goto L_0x0084;
                case 133423073: goto L_0x0078;
                case 160987616: goto L_0x006d;
                case 937504109: goto L_0x0061;
                case 939594121: goto L_0x0056;
                case 1000390722: goto L_0x004b;
                case 1082777163: goto L_0x003f;
                case 1124446108: goto L_0x0033;
                case 1270488759: goto L_0x0027;
                case 1880941391: goto L_0x001b;
                default: goto L_0x0019;
            }
        L_0x0019:
            goto L_0x0105
        L_0x001b:
            java.lang.String r0 = "getMaxSize"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 15
            goto L_0x0106
        L_0x0027:
            java.lang.String r0 = "tracking"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 13
            goto L_0x0106
        L_0x0033:
            java.lang.String r0 = "warning"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 11
            goto L_0x0106
        L_0x003f:
            java.lang.String r0 = "totalVideoDuration"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 8
            goto L_0x0106
        L_0x004b:
            java.lang.String r0 = "videoReplay"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 6
            goto L_0x0106
        L_0x0056:
            java.lang.String r0 = "videoPaused"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 5
            goto L_0x0106
        L_0x0061:
            java.lang.String r0 = "getOrientationProperties"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 19
            goto L_0x0106
        L_0x006d:
            java.lang.String r0 = "getParameters"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 0
            goto L_0x0106
        L_0x0078:
            java.lang.String r0 = "setOrientationProperties"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 20
            goto L_0x0106
        L_0x0084:
            java.lang.String r0 = "error"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 10
            goto L_0x0106
        L_0x0090:
            java.lang.String r0 = "debug"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 12
            goto L_0x0106
        L_0x009c:
            java.lang.String r0 = "close"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 2
            goto L_0x0106
        L_0x00a6:
            java.lang.String r0 = "click"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 1
            goto L_0x0106
        L_0x00b0:
            java.lang.String r0 = "show"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 9
            goto L_0x0106
        L_0x00bb:
            java.lang.String r0 = "videoPlaying"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 4
            goto L_0x0106
        L_0x00c5:
            java.lang.String r0 = "getScreenSize"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 16
            goto L_0x0106
        L_0x00d0:
            java.lang.String r0 = "videoCompleted"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 3
            goto L_0x0106
        L_0x00da:
            java.lang.String r0 = "openUrl"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 14
            goto L_0x0106
        L_0x00e5:
            java.lang.String r0 = "currentVideoDuration"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 7
            goto L_0x0106
        L_0x00ef:
            java.lang.String r0 = "getCurrentPosition"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 17
            goto L_0x0106
        L_0x00fa:
            java.lang.String r0 = "getDefaultPosition"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0105
            r0 = 18
            goto L_0x0106
        L_0x0105:
            r0 = -1
        L_0x0106:
            java.lang.String r15 = " callback triggered."
            java.lang.String r1 = "JavaScript to native "
            java.lang.String r2 = "CBWebChromeClient"
            switch(r0) {
                case 0: goto L_0x0234;
                case 1: goto L_0x0208;
                case 2: goto L_0x0206;
                case 3: goto L_0x0203;
                case 4: goto L_0x0200;
                case 5: goto L_0x01fd;
                case 6: goto L_0x01fa;
                case 7: goto L_0x01f8;
                case 8: goto L_0x01f6;
                case 9: goto L_0x01f4;
                case 10: goto L_0x01e8;
                case 11: goto L_0x01da;
                case 12: goto L_0x01d8;
                case 13: goto L_0x01d3;
                case 14: goto L_0x01d0;
                case 15: goto L_0x01b4;
                case 16: goto L_0x0198;
                case 17: goto L_0x017c;
                case 18: goto L_0x0160;
                case 19: goto L_0x0144;
                case 20: goto L_0x0129;
                default: goto L_0x010f;
            }
        L_0x010f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            java.lang.String r1 = " callback not recognized."
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0)
            java.lang.String r0 = "Function name not recognized."
            return r0
        L_0x0129:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            r0 = 14
            r3 = 14
            goto L_0x0209
        L_0x0144:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            com.chartboost.sdk.impl.bf r0 = r6.g
            java.lang.String r0 = r0.p()
            return r0
        L_0x0160:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            com.chartboost.sdk.impl.bf r0 = r6.g
            java.lang.String r0 = r0.u()
            return r0
        L_0x017c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            com.chartboost.sdk.impl.bf r0 = r6.g
            java.lang.String r0 = r0.v()
            return r0
        L_0x0198:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            com.chartboost.sdk.impl.bf r0 = r6.g
            java.lang.String r0 = r0.t()
            return r0
        L_0x01b4:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            com.chartboost.sdk.impl.bf r0 = r6.g
            java.lang.String r0 = r0.s()
            return r0
        L_0x01d0:
            r0 = 5
            r3 = 5
            goto L_0x0209
        L_0x01d3:
            r0 = 8
            r3 = 8
            goto L_0x0209
        L_0x01d8:
            r3 = 3
            goto L_0x0209
        L_0x01da:
            java.lang.Class<com.chartboost.sdk.impl.be> r0 = com.chartboost.sdk.impl.be.class
            java.lang.String r0 = r0.getName()
            java.lang.String r3 = "Javascript warning occurred"
            android.util.Log.d(r0, r3)
            r3 = 13
            goto L_0x0209
        L_0x01e8:
            java.lang.Class<com.chartboost.sdk.impl.be> r0 = com.chartboost.sdk.impl.be.class
            java.lang.String r0 = r0.getName()
            java.lang.String r5 = "Javascript Error occured"
            android.util.Log.d(r0, r5)
            goto L_0x0209
        L_0x01f4:
            r3 = 6
            goto L_0x0209
        L_0x01f6:
            r3 = 7
            goto L_0x0209
        L_0x01f8:
            r3 = 2
            goto L_0x0209
        L_0x01fa:
            r3 = 12
            goto L_0x0209
        L_0x01fd:
            r3 = 10
            goto L_0x0209
        L_0x0200:
            r3 = 11
            goto L_0x0209
        L_0x0203:
            r3 = 9
            goto L_0x0209
        L_0x0206:
            r3 = 1
            goto L_0x0209
        L_0x0208:
            r3 = 0
        L_0x0209:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            com.chartboost.sdk.impl.bg r7 = new com.chartboost.sdk.impl.bg
            com.chartboost.sdk.impl.bf r2 = r6.g
            r0 = r7
            r1 = r16
            r4 = r18
            r5 = r17
            r0.<init>(r1, r2, r3, r4, r5)
            android.os.Handler r0 = r6.h
            r0.post(r7)
            java.lang.String r0 = "Native function successfully called."
            return r0
        L_0x0234:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r4)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r2, r0)
            com.chartboost.sdk.impl.bf r0 = r6.g
            com.chartboost.sdk.Model.c r0 = r0.e
            if (r0 == 0) goto L_0x02c0
            com.chartboost.sdk.Model.a r0 = r0.p
            if (r0 == 0) goto L_0x02c0
            com.chartboost.sdk.Libraries.e$a[] r1 = new com.chartboost.sdk.Libraries.e.a[r14]
            org.json.JSONObject r1 = com.chartboost.sdk.Libraries.e.a(r1)
            java.util.Map<java.lang.String, java.lang.String> r2 = r0.d
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0263:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x027d
            java.lang.Object r3 = r2.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r4 = r3.getKey()
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r3 = r3.getValue()
            com.chartboost.sdk.Libraries.e.a(r1, r4, r3)
            goto L_0x0263
        L_0x027d:
            java.util.Map<java.lang.String, com.chartboost.sdk.Model.b> r0 = r0.c
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x0287:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x02bb
            java.lang.Object r2 = r0.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r3 = r2.getValue()
            com.chartboost.sdk.Model.b r3 = (com.chartboost.sdk.Model.b) r3
            java.lang.Object r2 = r2.getKey()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r3.f2408a
            r4.append(r5)
            java.lang.String r5 = "/"
            r4.append(r5)
            java.lang.String r3 = r3.b
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            com.chartboost.sdk.Libraries.e.a(r1, r2, r3)
            goto L_0x0287
        L_0x02bb:
            java.lang.String r0 = r1.toString()
            return r0
        L_0x02c0:
            java.lang.String r0 = "{}"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.bd.a(org.json.JSONObject, java.lang.String):java.lang.String");
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String simpleName = bd.class.getSimpleName();
        Log.d(simpleName, "Chartboost Webview:" + consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
        return true;
    }

    public void onHideCustomView() {
        if (this.c) {
            this.b.setVisibility(4);
            this.b.removeView(this.d);
            this.f2466a.setVisibility(0);
            WebChromeClient.CustomViewCallback customViewCallback = this.e;
            if (customViewCallback != null && !customViewCallback.getClass().getName().contains(".chromium.")) {
                this.e.onCustomViewHidden();
            }
            this.c = false;
            this.d = null;
            this.e = null;
            a aVar = this.f;
            if (aVar != null) {
                aVar.a(false);
            }
        }
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        try {
            JSONObject jSONObject = new JSONObject(str2);
            jsPromptResult.confirm(a(jSONObject.getJSONObject("eventArgs"), jSONObject.getString("eventType")));
            return true;
        } catch (JSONException unused) {
            CBLogging.b("CBWebChromeClient", "Exception caught parsing the function name from js to native");
            return true;
        }
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        if (view instanceof FrameLayout) {
            this.c = true;
            this.d = (FrameLayout) view;
            this.e = customViewCallback;
            this.f2466a.setVisibility(4);
            this.b.addView(this.d, new ViewGroup.LayoutParams(-1, -1));
            this.b.setVisibility(0);
            a aVar = this.f;
            if (aVar != null) {
                aVar.a(true);
            }
        }
    }

    public void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
        onShowCustomView(view, customViewCallback);
    }
}
