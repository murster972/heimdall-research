package com.chartboost.sdk.impl;

import android.app.Application;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public class o {

    /* renamed from: a  reason: collision with root package name */
    static List<String> f2492a = new ArrayList();
    static HashMap<String, p> b = new HashMap<>();

    public static void a() {
        f2492a.clear();
        if (b()) {
            f2492a.add("moat");
        }
    }

    static boolean b() {
        return true;
    }

    public static void c() {
        for (p next : b.values()) {
            if (next != null) {
                next.a();
            }
        }
    }

    public static void d() {
        for (String str : b.keySet()) {
            p pVar = b.get(str);
            if (pVar != null) {
                pVar.b();
            }
        }
    }

    public static JSONArray e() {
        JSONArray jSONArray = new JSONArray();
        List<String> list = f2492a;
        if (list != null) {
            for (String put : list) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static JSONArray f() {
        JSONArray jSONArray = new JSONArray();
        HashMap<String, p> hashMap = b;
        if (hashMap != null) {
            for (String put : hashMap.keySet()) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static void a(List<String> list) {
        for (String next : list) {
            if (f2492a.contains(next) && !b.containsKey(next)) {
                b.put(next, (Object) null);
            }
        }
    }

    public static void a(Application application, boolean z, boolean z2, boolean z3) {
        for (String next : b.keySet()) {
            if (next.contains("moat")) {
                if (b.get(next) != null) {
                    b.get(next).b();
                }
                r rVar = new r();
                rVar.a(application, z, z2, z3);
                b.put("moat", rVar);
            }
        }
    }

    public static void a(WebView webView, HashSet<String> hashSet) {
        Iterator<String> it2 = hashSet.iterator();
        while (it2.hasNext()) {
            p pVar = b.get(it2.next());
            if (pVar != null) {
                pVar.a(webView);
            }
        }
    }

    public static String a(HashSet<String> hashSet) {
        JSONArray jSONArray = new JSONArray();
        Iterator<String> it2 = hashSet.iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            if (b.get(next) != null) {
                jSONArray.put(next);
            }
        }
        return jSONArray.toString();
    }
}
