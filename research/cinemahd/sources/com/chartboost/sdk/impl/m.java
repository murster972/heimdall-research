package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.aj;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class m implements aj.a {

    /* renamed from: a  reason: collision with root package name */
    private final l f2491a;
    private final f b;
    private final ah c;
    private final ap d;
    private final a e;
    private final AtomicReference<e> f;
    private int g = 1;
    private int h = 0;
    private long i = 0;
    private aj j = null;
    private AtomicInteger k = null;

    public m(l lVar, f fVar, ah ahVar, ap apVar, a aVar, AtomicReference<e> atomicReference) {
        this.f2491a = lVar;
        this.b = fVar;
        this.c = ahVar;
        this.d = apVar;
        this.e = aVar;
        this.f = atomicReference;
    }

    public synchronized void a() {
        try {
            CBLogging.b("Chartboost SDK", "Sdk Version = 7.3.1, Commit: ea5c9878e5dca6c95016765177cbd146c39a21f7");
            e eVar = this.f.get();
            a(eVar);
            if (!eVar.c && !eVar.b) {
                if (i.v) {
                    if (this.g == 3) {
                        if (this.k.get() <= 0) {
                            CBLogging.a("Prefetcher", "Change state to COOLDOWN");
                            this.g = 4;
                            this.k = null;
                        } else {
                            return;
                        }
                    }
                    if (this.g == 4) {
                        if (this.i - System.nanoTime() > 0) {
                            CBLogging.a("Prefetcher", "Prefetch session is still active. Won't be making any new prefetch until the prefetch session expires");
                            return;
                        }
                        CBLogging.a("Prefetcher", "Change state to IDLE");
                        this.g = 1;
                        this.h = 0;
                        this.i = 0;
                    }
                    if (this.g == 1) {
                        if (eVar.y) {
                            am amVar = new am(eVar.H, this.d, this.e, 2, this);
                            amVar.a("cache_assets", this.b.c(), 0);
                            amVar.l = true;
                            CBLogging.a("Prefetcher", "Change state to AWAIT_PREFETCH_RESPONSE");
                            this.g = 2;
                            this.h = 2;
                            this.i = System.nanoTime() + TimeUnit.MINUTES.toNanos((long) eVar.D);
                            this.j = amVar;
                        } else if (eVar.e) {
                            aj ajVar = new aj("/api/video-prefetch", this.d, this.e, 2, this);
                            ajVar.a("local-videos", (Object) this.b.b());
                            ajVar.l = true;
                            CBLogging.a("Prefetcher", "Change state to AWAIT_PREFETCH_RESPONSE");
                            this.g = 2;
                            this.h = 1;
                            this.i = System.nanoTime() + TimeUnit.MINUTES.toNanos((long) eVar.i);
                            this.j = ajVar;
                        } else {
                            CBLogging.b("Prefetcher", "Did not prefetch because neither native nor webview are enabled.");
                            return;
                        }
                        this.c.a(this.j);
                    } else {
                        return;
                    }
                }
            }
            b();
        } catch (Exception e2) {
            if (this.g == 2) {
                CBLogging.a("Prefetcher", "Change state to COOLDOWN");
                this.g = 4;
                this.j = null;
            }
            a.a((Class) getClass(), "prefetch", e2);
        }
    }

    public synchronized void b() {
        if (this.g == 2) {
            CBLogging.a("Prefetcher", "Change state to COOLDOWN");
            this.g = 4;
            this.j = null;
        } else if (this.g == 3) {
            CBLogging.a("Prefetcher", "Change state to COOLDOWN");
            this.g = 4;
            AtomicInteger atomicInteger = this.k;
            this.k = null;
            if (atomicInteger != null) {
                this.f2491a.a(atomicInteger);
            }
        }
    }

    private void a(e eVar) {
        boolean z = eVar.y;
        if ((this.h == 1 && !(!z && eVar.e)) || (this.h == 2 && !z)) {
            CBLogging.a("Prefetcher", "Change state to IDLE");
            this.g = 1;
            this.h = 0;
            this.i = 0;
            this.j = null;
            AtomicInteger atomicInteger = this.k;
            this.k = null;
            if (atomicInteger != null) {
                this.f2491a.a(atomicInteger);
            }
        }
    }

    public synchronized void a(aj ajVar, JSONObject jSONObject) {
        try {
            if (this.g == 2) {
                if (ajVar == this.j) {
                    CBLogging.a("Prefetcher", "Change state to DOWNLOAD_ASSETS");
                    this.g = 3;
                    this.j = null;
                    this.k = new AtomicInteger();
                    if (jSONObject != null) {
                        CBLogging.a("Prefetcher", "Got Asset list for Prefetch from server :)" + jSONObject);
                        if (this.h == 1) {
                            this.f2491a.a(3, b.a(jSONObject), this.k, (h) null);
                        } else if (this.h == 2) {
                            this.f2491a.a(3, b.a(jSONObject, this.f.get().v), this.k, (h) null);
                        }
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } catch (Exception e2) {
            a.a((Class) getClass(), "onSuccess", e2);
        }
        return;
    }

    public synchronized void a(aj ajVar, CBError cBError) {
        if (this.g == 2) {
            if (ajVar == this.j) {
                this.j = null;
                CBLogging.a("Prefetcher", "Change state to COOLDOWN");
                this.g = 4;
            }
        }
    }
}
