package com.chartboost.sdk.Libraries;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public int f2392a;
    public String b;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(android.content.Context r4) {
        /*
            r3 = this;
            java.lang.String r0 = "ContentValues"
            r3.<init>()
            r1 = 0
            com.chartboost.sdk.impl.s r2 = com.chartboost.sdk.impl.s.a()     // Catch:{ IllegalStateException -> 0x001b, IOException -> 0x0015, GooglePlayServicesRepairableException -> 0x000f, GooglePlayServicesNotAvailableException -> 0x0020 }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r4 = r2.a((android.content.Context) r4)     // Catch:{ IllegalStateException -> 0x001b, IOException -> 0x0015, GooglePlayServicesRepairableException -> 0x000f, GooglePlayServicesNotAvailableException -> 0x0020 }
            goto L_0x0021
        L_0x000f:
            java.lang.String r4 = "There was a recoverable error connecting to Google Play Services."
            com.chartboost.sdk.Libraries.CBLogging.b(r0, r4)
            goto L_0x0020
        L_0x0015:
            java.lang.String r4 = "The connection to Google Play Services failed."
            com.chartboost.sdk.Libraries.CBLogging.b(r0, r4)
            goto L_0x0020
        L_0x001b:
            java.lang.String r4 = "This should have been called off the main thread."
            com.chartboost.sdk.Libraries.CBLogging.b(r0, r4)
        L_0x0020:
            r4 = r1
        L_0x0021:
            if (r4 != 0) goto L_0x0029
            r4 = -1
            r3.f2392a = r4
            r3.b = r1
            goto L_0x003e
        L_0x0029:
            boolean r0 = r4.isLimitAdTrackingEnabled()
            if (r0 == 0) goto L_0x0035
            r4 = 1
            r3.f2392a = r4
            r3.b = r1
            goto L_0x003e
        L_0x0035:
            r0 = 0
            r3.f2392a = r0
            java.lang.String r4 = r4.getId()
            r3.b = r4
        L_0x003e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Libraries.a.<init>(android.content.Context):void");
    }
}
