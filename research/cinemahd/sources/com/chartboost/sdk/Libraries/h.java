package com.chartboost.sdk.Libraries;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.chartboost.sdk.e;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.File;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private a f2399a;
    private final e b;
    private String c;
    private float d = 1.0f;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private int f2400a;
        private final String b;
        private final File c;
        private Bitmap d;
        private final f e;
        private int f = -1;
        private int g = -1;

        public a(String str, File file, f fVar) {
            this.c = file;
            this.b = str;
            this.d = null;
            this.f2400a = 1;
            this.e = fVar;
        }

        private void f() {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(this.c.getAbsolutePath(), options);
                this.f = options.outWidth;
                this.g = options.outHeight;
            } catch (Exception e2) {
                CBLogging.a("MemoryBitmap", "Error decoding file size", e2);
                com.chartboost.sdk.Tracking.a.a(a.class, "decodeSize", e2);
            }
        }

        public Bitmap a() {
            if (this.d == null) {
                b();
            }
            return this.d;
        }

        public void b() {
            if (this.d == null) {
                CBLogging.a("MemoryBitmap", "Loading image '" + this.b + "' from cache");
                byte[] a2 = this.e.a(this.c);
                if (a2 == null) {
                    CBLogging.b("MemoryBitmap", "decode() - bitmap not found");
                    return;
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(a2, 0, a2.length, options);
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inJustDecodeBounds = false;
                options2.inDither = false;
                options2.inPurgeable = true;
                options2.inInputShareable = true;
                options2.inTempStorage = new byte[32768];
                options2.inSampleSize = 1;
                while (true) {
                    if (options2.inSampleSize >= 32) {
                        break;
                    }
                    try {
                        this.d = BitmapFactory.decodeByteArray(a2, 0, a2.length, options2);
                        break;
                    } catch (OutOfMemoryError e2) {
                        CBLogging.a("MemoryBitmap", "OutOfMemoryError suppressed - trying larger sample size", e2);
                        options2.inSampleSize *= 2;
                    } catch (Exception e3) {
                        CBLogging.a("MemoryBitmap", "Exception raised decoding bitmap", e3);
                        com.chartboost.sdk.Tracking.a.a(a.class, "decodeByteArray", e3);
                    }
                }
                this.f2400a = options2.inSampleSize;
            }
            return;
            if (this.d == null) {
                this.c.delete();
                throw new RuntimeException("Unable to decode " + this.b);
            }
            this.f2400a = options2.inSampleSize;
        }

        public int c() {
            return this.f2400a;
        }

        public int d() {
            Bitmap bitmap = this.d;
            if (bitmap != null) {
                return bitmap.getWidth();
            }
            int i = this.f;
            if (i >= 0) {
                return i;
            }
            f();
            return this.f;
        }

        public int e() {
            Bitmap bitmap = this.d;
            if (bitmap != null) {
                return bitmap.getHeight();
            }
            int i = this.g;
            if (i >= 0) {
                return i;
            }
            f();
            return this.g;
        }
    }

    public h(e eVar) {
        this.b = eVar;
    }

    public int a() {
        return this.f2399a.d() * this.f2399a.c();
    }

    public int b() {
        return this.f2399a.e() * this.f2399a.c();
    }

    public boolean c() {
        return this.f2399a != null;
    }

    public Bitmap d() {
        a aVar = this.f2399a;
        if (aVar != null) {
            return aVar.a();
        }
        return null;
    }

    public float e() {
        return this.d;
    }

    public boolean a(String str) {
        return a(this.b.g(), str);
    }

    public boolean a(JSONObject jSONObject, String str) {
        JSONObject a2 = e.a(jSONObject, str);
        this.c = str;
        if (a2 == null) {
            return true;
        }
        String optString = a2.optString(ReportDBAdapter.ReportColumns.COLUMN_URL);
        this.d = (float) a2.optDouble("scale", 1.0d);
        if (optString.isEmpty()) {
            return true;
        }
        String optString2 = a2.optString("checksum");
        if (optString2.isEmpty()) {
            return false;
        }
        this.f2399a = this.b.e.j.a(optString2);
        if (this.f2399a != null) {
            return true;
        }
        return false;
    }
}
