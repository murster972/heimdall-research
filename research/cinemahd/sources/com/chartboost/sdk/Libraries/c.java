package com.chartboost.sdk.Libraries;

import com.chartboost.sdk.Tracking.a;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public final class c {
    private c() {
    }

    public static synchronized byte[] a(byte[] bArr) {
        Class<c> cls = c.class;
        synchronized (cls) {
            if (bArr == null) {
                return null;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(bArr);
                byte[] digest = instance.digest();
                return digest;
            } catch (NoSuchAlgorithmException e) {
                a.a((Class) cls, "sha1", (Exception) e);
                return null;
            } catch (Exception e2) {
                a.a((Class) cls, "sha1", e2);
                return null;
            }
        }
    }

    public static String b(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        BigInteger bigInteger = new BigInteger(1, bArr);
        Locale locale = Locale.US;
        return String.format(locale, "%0" + (bArr.length << 1) + "x", new Object[]{bigInteger});
    }
}
