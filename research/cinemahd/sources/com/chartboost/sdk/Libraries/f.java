package com.chartboost.sdk.Libraries;

import android.content.Context;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.bh;
import com.chartboost.sdk.impl.s;
import com.facebook.cache.disk.DefaultDiskStorage;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public final File f2397a;
    public final File b;
    private final AtomicReference<e> c;
    private final g d;
    private final AtomicReference<g> e;
    private s f;

    public f(s sVar, Context context, AtomicReference<e> atomicReference) {
        g[] gVarArr;
        g[] gVarArr2;
        f fVar = this;
        fVar.f = sVar;
        fVar.d = new g(context.getCacheDir());
        fVar.e = new AtomicReference<>();
        fVar.c = atomicReference;
        try {
            File b2 = sVar.b();
            if (b2 != null) {
                fVar.e.set(new g(b2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        fVar.b = new File(fVar.d.f2398a, "track");
        fVar.f2397a = new File(fVar.d.f2398a, "session");
        g[] gVarArr3 = {fVar.d, fVar.e.get()};
        int length = gVarArr3.length;
        int i = 0;
        while (i < length) {
            g gVar = gVarArr3[i];
            try {
                boolean z = gVar == fVar.d;
                if (gVar == null || (!z && !a())) {
                    gVarArr = gVarArr3;
                    i++;
                    fVar = this;
                    AtomicReference<e> atomicReference2 = atomicReference;
                    gVarArr3 = gVarArr;
                } else {
                    long currentTimeMillis = System.currentTimeMillis() - TimeUnit.DAYS.toMillis((long) atomicReference.get().w);
                    File file = new File(gVar.f2398a, "templates");
                    if (file.exists()) {
                        File[] listFiles = file.listFiles();
                        if (listFiles != null) {
                            int length2 = listFiles.length;
                            int i2 = 0;
                            while (i2 < length2) {
                                File file2 = listFiles[i2];
                                if (file2.isDirectory()) {
                                    File[] listFiles2 = file2.listFiles();
                                    if (listFiles2 != null) {
                                        int length3 = listFiles2.length;
                                        int i3 = 0;
                                        while (i3 < length3) {
                                            File file3 = listFiles2[i3];
                                            if ((z || file3.lastModified() < currentTimeMillis) && !file3.delete()) {
                                                StringBuilder sb = new StringBuilder();
                                                sb.append("Unable to delete ");
                                                gVarArr = gVarArr3;
                                                try {
                                                    sb.append(file3.getPath());
                                                    CBLogging.b("FileCache", sb.toString());
                                                } catch (Exception e3) {
                                                    e = e3;
                                                    CBLogging.a("FileCache", "Exception while cleaning up templates directory at " + gVar.f.getPath(), e);
                                                    e.printStackTrace();
                                                    i++;
                                                    fVar = this;
                                                    AtomicReference<e> atomicReference22 = atomicReference;
                                                    gVarArr3 = gVarArr;
                                                }
                                            } else {
                                                gVarArr = gVarArr3;
                                            }
                                            i3++;
                                            AtomicReference<e> atomicReference3 = atomicReference;
                                            gVarArr3 = gVarArr;
                                        }
                                    }
                                    gVarArr2 = gVarArr3;
                                    File[] listFiles3 = file2.listFiles();
                                    if (listFiles3 != null && listFiles3.length == 0 && !file2.delete()) {
                                        CBLogging.b("FileCache", "Unable to delete " + file2.getPath());
                                    }
                                } else {
                                    gVarArr2 = gVarArr3;
                                }
                                i2++;
                                AtomicReference<e> atomicReference4 = atomicReference;
                                gVarArr3 = gVarArr2;
                            }
                        }
                    }
                    gVarArr = gVarArr3;
                    File file4 = new File(gVar.f2398a, ".adId");
                    if (file4.exists() && ((z || file4.lastModified() < currentTimeMillis) && !file4.delete())) {
                        CBLogging.b("FileCache", "Unable to delete " + file4.getPath());
                    }
                    i++;
                    fVar = this;
                    AtomicReference<e> atomicReference222 = atomicReference;
                    gVarArr3 = gVarArr;
                }
            } catch (Exception e4) {
                e = e4;
                gVarArr = gVarArr3;
                CBLogging.a("FileCache", "Exception while cleaning up templates directory at " + gVar.f.getPath(), e);
                e.printStackTrace();
                i++;
                fVar = this;
                AtomicReference<e> atomicReference2222 = atomicReference;
                gVarArr3 = gVarArr;
            }
        }
    }

    public synchronized byte[] a(File file) {
        byte[] bArr;
        bArr = null;
        if (file == null) {
            return null;
        }
        try {
            bArr = bh.b(file);
        } catch (Exception e2) {
            CBLogging.a("FileCache", "Error loading cache from disk", e2);
            a.a((Class) getClass(), "readByteArrayFromDisk", e2);
        }
        return bArr;
    }

    public boolean b(String str) {
        if (d().d == null || str == null) {
            return false;
        }
        return new File(d().d, str).exists();
    }

    public JSONObject c() {
        String[] list;
        JSONObject jSONObject = new JSONObject();
        try {
            File file = d().f2398a;
            for (String next : this.c.get().x) {
                if (!next.equals("templates")) {
                    File file2 = new File(file, next);
                    JSONArray jSONArray = new JSONArray();
                    if (file2.exists() && (list = file2.list()) != null) {
                        for (String str : list) {
                            if (!str.equals(".nomedia") && !str.endsWith(DefaultDiskStorage.FileType.TEMP)) {
                                jSONArray.put(str);
                            }
                        }
                    }
                    e.a(jSONObject, next, jSONArray);
                }
            }
        } catch (Exception e2) {
            a.a(f.class, "getWebViewCacheAssets", e2);
        }
        return jSONObject;
    }

    public g d() {
        if (a()) {
            g gVar = this.e.get();
            if (gVar == null) {
                try {
                    File b2 = this.f.b();
                    if (b2 != null) {
                        this.e.compareAndSet((Object) null, new g(b2));
                        gVar = this.e.get();
                    }
                } catch (Exception e2) {
                    a.a(f.class, "currentLocations", e2);
                }
            }
            if (gVar != null) {
                return gVar;
            }
        }
        return this.d;
    }

    public JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        g gVar = this.e.get();
        if (gVar != null) {
            e.a(jSONObject, ".chartboost-external-folder-size", Long.valueOf(b(gVar.f2398a)));
        }
        e.a(jSONObject, ".chartboost-internal-folder-size", Long.valueOf(b(this.d.f2398a)));
        File file = d().f2398a;
        String[] list = file.list();
        if (list != null && list.length > 0) {
            for (String file2 : list) {
                File file3 = new File(file, file2);
                JSONObject jSONObject2 = new JSONObject();
                e.a(jSONObject2, file3.getName() + "-size", Long.valueOf(b(file3)));
                String[] list2 = file3.list();
                if (list2 != null) {
                    e.a(jSONObject2, "count", Integer.valueOf(list2.length));
                }
                e.a(jSONObject, file3.getName(), jSONObject2);
            }
        }
        return jSONObject;
    }

    public JSONArray b() {
        JSONArray jSONArray = new JSONArray();
        String[] list = d().g.list();
        if (list != null) {
            for (String str : list) {
                if (!str.equals(".nomedia") && !str.endsWith(DefaultDiskStorage.FileType.TEMP)) {
                    jSONArray.put(str);
                }
            }
        }
        return jSONArray;
    }

    public String a(String str) {
        File file = new File(d().g, str);
        if (file.exists()) {
            return file.getPath();
        }
        return null;
    }

    public boolean a() {
        try {
            String c2 = this.f.c();
            if (c2 != null && c2.equals("mounted") && !i.n) {
                return true;
            }
        } catch (Exception e2) {
            a.a(f.class, "isExternalStorageAvailable", e2);
        }
        CBLogging.e("FileCache", "External Storage unavailable");
        return false;
    }

    public long b(File file) {
        long j = 0;
        if (file != null) {
            try {
                if (file.isDirectory()) {
                    File[] listFiles = file.listFiles();
                    if (listFiles == null) {
                        return 0;
                    }
                    for (File b2 : listFiles) {
                        j += b(b2);
                    }
                    return j;
                }
            } catch (Exception e2) {
                a.a(f.class, "getFolderSize", e2);
                return 0;
            }
        }
        if (file != null) {
            return file.length();
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0040 A[SYNTHETIC, Splitter:B:29:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(java.io.File r6) {
        /*
            r5 = this;
            java.lang.String r0 = "FileCache"
            r1 = 0
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0028 }
            java.lang.String r3 = "rw"
            r2.<init>(r6, r3)     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0028 }
            r3 = 0
            r2.seek(r3)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            int r6 = r2.read()     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            r2.seek(r3)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            r2.write(r6)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            r2.close()     // Catch:{ IOException -> 0x003d }
            goto L_0x003d
        L_0x001d:
            r6 = move-exception
            r1 = r2
            goto L_0x003e
        L_0x0020:
            r6 = move-exception
            r1 = r2
            goto L_0x0029
        L_0x0023:
            r6 = move-exception
            r1 = r2
            goto L_0x0035
        L_0x0026:
            r6 = move-exception
            goto L_0x003e
        L_0x0028:
            r6 = move-exception
        L_0x0029:
            java.lang.String r2 = "IOException when attempting to touch file"
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r2, r6)     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x003d
        L_0x0030:
            r1.close()     // Catch:{ IOException -> 0x003d }
            goto L_0x003d
        L_0x0034:
            r6 = move-exception
        L_0x0035:
            java.lang.String r2 = "File not found when attempting to touch"
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r2, r6)     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x003d
            goto L_0x0030
        L_0x003d:
            return
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0043 }
        L_0x0043:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Libraries.f.c(java.io.File):void");
    }
}
