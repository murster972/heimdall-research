package com.chartboost.sdk.Libraries;

import android.app.Activity;
import com.chartboost.sdk.impl.aq;
import java.lang.ref.WeakReference;

public final class j extends WeakReference<Activity> {

    /* renamed from: a  reason: collision with root package name */
    public final int f2401a;

    public j(Activity activity) {
        super(activity);
        aq.a("WeakActivity.WeakActivity", (Object) activity);
        this.f2401a = activity.hashCode();
    }

    public boolean a(Activity activity) {
        return activity != null && activity.hashCode() == this.f2401a;
    }

    public int hashCode() {
        return this.f2401a;
    }
}
