package com.chartboost.sdk.Libraries;

import java.io.File;

public class g {

    /* renamed from: a  reason: collision with root package name */
    public final File f2398a;
    public final File b;
    public final File c;
    public final File d;
    public final File e;
    public final File f;
    public final File g;

    g(File file) {
        this.f2398a = new File(file, ".chartboost");
        if (!this.f2398a.exists()) {
            this.f2398a.mkdirs();
        }
        this.b = a(this.f2398a, "css");
        this.c = a(this.f2398a, "html");
        this.d = a(this.f2398a, "images");
        this.e = a(this.f2398a, "js");
        this.f = a(this.f2398a, "templates");
        this.g = a(this.f2398a, "videos");
    }

    private static File a(File file, String str) {
        File file2 = new File(file, str);
        if (!file2.exists()) {
            file2.mkdir();
        }
        return file2;
    }
}
