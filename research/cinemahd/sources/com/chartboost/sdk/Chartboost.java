package com.chartboost.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.b;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.c;
import com.chartboost.sdk.h;
import com.chartboost.sdk.impl.aq;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.e;
import com.chartboost.sdk.impl.s;
import com.facebook.common.util.ByteConstants;
import java.util.HashMap;
import java.util.Map;

public class Chartboost {

    public enum CBFramework {
        CBFrameworkUnity("Unity"),
        CBFrameworkCorona("Corona"),
        CBFrameworkAir("AIR"),
        CBFrameworkGameSalad("GameSalad"),
        CBFrameworkCordova("Cordova"),
        CBFrameworkCocoonJS("CocoonJS"),
        CBFrameworkCocos2dx("Cocos2dx"),
        CBFrameworkPrime31Unreal("Prime31Unreal"),
        CBFrameworkWeeby("Weeby"),
        CBFrameworkOther("Other");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f2386a;

        private CBFramework(String str) {
            this.f2386a = str;
        }

        public boolean doesWrapperUseCustomBackgroundingBehavior() {
            return this == CBFrameworkAir;
        }

        public boolean doesWrapperUseCustomShouldDisplayBehavior() {
            return this == CBFrameworkAir || this == CBFrameworkCocos2dx;
        }

        public String toString() {
            return this.f2386a;
        }
    }

    public enum CBMediation {
        CBMediationAdMarvel("AdMarvel"),
        CBMediationAdMob("AdMob"),
        CBMediationFuse("Fuse"),
        CBMediationFyber("Fyber"),
        CBMediationHeyZap("HeyZap"),
        CBMediationMoPub("MoPub"),
        CBMediationironSource("ironSource"),
        CBMediationHyprMX("HyprMX"),
        CBMediationAerServ("AerServ"),
        CBMediationOther("Other");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f2387a;

        private CBMediation(String str) {
            this.f2387a = str;
        }

        public String toString() {
            return this.f2387a;
        }
    }

    public enum CBPIDataUseConsent {
        UNKNOWN(-1),
        NO_BEHAVIORAL(0),
        YES_BEHAVIORAL(1);
        
        private static Map<Integer, CBPIDataUseConsent> b;

        /* renamed from: a  reason: collision with root package name */
        private int f2388a;

        static {
            int i;
            b = new HashMap();
            for (CBPIDataUseConsent cBPIDataUseConsent : values()) {
                b.put(Integer.valueOf(cBPIDataUseConsent.f2388a), cBPIDataUseConsent);
            }
        }

        private CBPIDataUseConsent(int i) {
            this.f2388a = i;
        }

        public int getValue() {
            return this.f2388a;
        }

        public static CBPIDataUseConsent valueOf(int i) {
            CBPIDataUseConsent cBPIDataUseConsent = b.get(Integer.valueOf(i));
            return cBPIDataUseConsent == null ? UNKNOWN : cBPIDataUseConsent;
        }
    }

    private Chartboost() {
    }

    public static void cacheInterstitial(String str) {
        aq.a("Chartboost.cacheInterstitial", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "cacheInterstitial location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.g;
                cVar.getClass();
                handler.post(new c.a(4, str, CBError.CBImpressionError.INVALID_LOCATION));
                return;
            }
            e eVar = a2.m.get();
            if ((!eVar.y || !eVar.A) && (!eVar.e || !eVar.g)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.g;
                cVar2.getClass();
                handler2.post(new c.a(4, str, CBError.CBImpressionError.END_POINT_DISABLED));
                return;
            }
            com.chartboost.sdk.impl.e eVar2 = a2.f;
            eVar2.getClass();
            a2.f2426a.execute(new e.a(3, str, (f) null, (CBError.CBImpressionError) null));
        }
    }

    public static void cacheMoreApps(String str) {
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            a2.getClass();
            h.a aVar = new h.a(5);
            aVar.b = str;
            a2.p.postDelayed(aVar, b.c);
        }
    }

    public static void cacheRewardedVideo(String str) {
        aq.a("Chartboost.cacheRewardedVideo", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "cacheRewardedVideo location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.l;
                cVar.getClass();
                handler.post(new c.a(4, str, CBError.CBImpressionError.INVALID_LOCATION));
                return;
            }
            com.chartboost.sdk.Model.e eVar = a2.m.get();
            if ((!eVar.y || !eVar.E) && (!eVar.e || !eVar.j)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.l;
                cVar2.getClass();
                handler2.post(new c.a(4, str, CBError.CBImpressionError.END_POINT_DISABLED));
                return;
            }
            com.chartboost.sdk.impl.e eVar2 = a2.k;
            eVar2.getClass();
            a2.f2426a.execute(new e.a(3, str, (f) null, (CBError.CBImpressionError) null));
        }
    }

    @Deprecated
    public static void closeImpression() {
    }

    private static void forwardTouchEventsAIR(boolean z) {
        h a2 = h.a();
        if (a2 != null) {
            c cVar = a2.q;
            cVar.getClass();
            c.C0017c cVar2 = new c.C0017c(6);
            cVar2.c = z;
            h.b(cVar2);
        }
    }

    public static boolean getAutoCacheAds() {
        return i.t;
    }

    public static String getCustomId() {
        if (!b.b()) {
            return "";
        }
        return i.f2429a;
    }

    public static a getDelegate() {
        return i.c;
    }

    public static CBLogging.Level getLoggingLevel() {
        b.b();
        return CBLogging.f2390a;
    }

    public static CBPIDataUseConsent getPIDataUseConsent() {
        return i.x;
    }

    public static String getSDKVersion() {
        return "7.3.1";
    }

    public static boolean hasInterstitial(String str) {
        aq.a("Chartboost.hasInterstitial", str);
        h a2 = h.a();
        if (a2 == null || !b.a() || a2.f.a(str) == null) {
            return false;
        }
        return true;
    }

    public static boolean hasMoreApps(String str) {
        return false;
    }

    public static boolean hasRewardedVideo(String str) {
        aq.a("Chartboost.hasRewardedVideo", str);
        h a2 = h.a();
        if (a2 == null || !b.a() || a2.k.a(str) == null) {
            return false;
        }
        return true;
    }

    public static boolean isAnyViewVisible() {
        aq.a("Chartboost.isAnyViewVisible");
        h a2 = h.a();
        return a2 != null && a2.q.e();
    }

    public static boolean isWebViewEnabled() {
        h a2 = h.a();
        return a2 == null || a2.m.get().y;
    }

    public static boolean onBackPressed() {
        aq.a("Chartboost.onBackPressed");
        h a2 = h.a();
        if (a2 == null) {
            return false;
        }
        return a2.q.j();
    }

    public static void onCreate(Activity activity) {
        aq.a("Chartboost.onCreate", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.b(activity);
        }
    }

    public static void onDestroy(Activity activity) {
        aq.a("Chartboost.onDestroy", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.j(activity);
        }
    }

    public static void onPause(Activity activity) {
        aq.a("Chartboost.onPause", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.g(activity);
        }
    }

    public static void onResume(Activity activity) {
        aq.a("Chartboost.onResume", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.f(activity);
        }
    }

    public static void onStart(Activity activity) {
        aq.a("Chartboost.onStart", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.d(activity);
        }
    }

    public static void onStop(Activity activity) {
        aq.a("Chartboost.onStop", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.h(activity);
        }
    }

    @Deprecated
    public static void restrictDataCollection(Context context, boolean z) {
        setPIDataUseConsent(context, z ? CBPIDataUseConsent.NO_BEHAVIORAL : CBPIDataUseConsent.UNKNOWN);
    }

    @TargetApi(28)
    public static void setActivityAttrs(Activity activity) {
        if (activity != null && i.g) {
            Window window = activity.getWindow();
            int i = 2;
            if (s.a().a(16)) {
                i = 1798;
                if (Build.VERSION.SDK_INT >= 19) {
                    i = 5894;
                }
                if (Build.VERSION.SDK_INT >= 28) {
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.layoutInDisplayCutoutMode = 1;
                    window.setAttributes(attributes);
                }
            }
            window.getDecorView().setSystemUiVisibility(i);
        } else if ((activity.getWindow().getAttributes().flags & ByteConstants.KB) != 0) {
            CBLogging.d("Chartboost", "Attempting to show Status and Navigation bars on a fullscreen activity. Please change your Chartboost activity theme to: \"@android:style/Theme.Translucent\"` in your Manifest file");
        }
    }

    @TargetApi(14)
    public static void setActivityCallbacks(boolean z) {
        Activity a2;
        Application.ActivityLifecycleCallbacks activityLifecycleCallbacks;
        aq.a("Chartboost.setActivityCallbacks", z);
        h a3 = h.a();
        if (a3 != null && (a2 = a3.q.a()) != null && (activityLifecycleCallbacks = a3.q.h) != null) {
            if (!i.s && z) {
                a2.getApplication().registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
                i.s = true;
            } else if (i.s && !z) {
                a2.getApplication().unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
                i.s = false;
            }
        }
    }

    public static void setAutoCacheAds(boolean z) {
        aq.a("Chartboost.setAutoCacheAds", z);
        h a2 = h.a();
        if (a2 != null) {
            a2.getClass();
            h.a aVar = new h.a(1);
            aVar.c = z;
            h.b(aVar);
        }
    }

    public static void setChartboostWrapperVersion(String str) {
        aq.a("Chartboost.setChartboostWrapperVersion", str);
        f fVar = new f(5);
        fVar.d = str;
        h.b(fVar);
    }

    public static void setCustomId(String str) {
        aq.a("Chartboost.setCustomId", str);
        f fVar = new f(6);
        fVar.e = str;
        h.b(fVar);
    }

    public static void setDelegate(ChartboostDelegate chartboostDelegate) {
        aq.a("Chartboost.setDelegate", (Object) chartboostDelegate);
        f fVar = new f(8);
        fVar.g = chartboostDelegate;
        h.b(fVar);
    }

    public static void setFramework(CBFramework cBFramework, String str) {
        aq.a("Chartboost.setFramework");
        f fVar = new f(4);
        fVar.b = cBFramework;
        fVar.d = str;
        h.b(fVar);
    }

    @Deprecated
    public static void setFrameworkVersion(String str) {
        aq.a("Chartboost.setFrameworkVersion", str);
        f fVar = new f(5);
        fVar.d = str;
        h.b(fVar);
    }

    public static void setLoggingLevel(CBLogging.Level level) {
        aq.a("Chartboost.setLoggingLevel", level.toString());
        f fVar = new f(7);
        fVar.f = level;
        h.b(fVar);
    }

    public static void setMediation(CBMediation cBMediation, String str) {
        aq.a("Chartboost.setMediation");
        f fVar = new f(3);
        fVar.c = cBMediation;
        fVar.d = str;
        h.b(fVar);
    }

    public static void setPIDataUseConsent(Context context, CBPIDataUseConsent cBPIDataUseConsent) {
        h.a(context, cBPIDataUseConsent);
    }

    public static void setShouldDisplayLoadingViewForMoreApps(boolean z) {
    }

    public static void setShouldHideSystemUI(Boolean bool) {
        aq.a("Chartboost.setHideSystemUI", (Object) bool);
        i.g = bool.booleanValue();
    }

    public static void setShouldPrefetchVideoContent(boolean z) {
        aq.a("Chartboost.setShouldPrefetchVideoContent", z);
        h a2 = h.a();
        if (a2 != null && b.a()) {
            a2.getClass();
            h.a aVar = new h.a(2);
            aVar.d = z;
            h.b(aVar);
        }
    }

    public static void setShouldRequestInterstitialsInFirstSession(boolean z) {
        aq.a("Chartboost.setShouldRequestInterstitialsInFirstSession", z);
        if (b.b()) {
            f fVar = new f(1);
            fVar.f2424a = z;
            h.b(fVar);
        }
    }

    public static void showInterstitial(String str) {
        aq.a("Chartboost.showInterstitial", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "showInterstitial location cannot be empty");
                Handler handler = a2.p;
                com.chartboost.sdk.impl.c cVar = a2.g;
                cVar.getClass();
                handler.post(new c.a(4, str, CBError.CBImpressionError.INVALID_LOCATION));
                return;
            }
            com.chartboost.sdk.Model.e eVar = a2.m.get();
            if ((!eVar.y || !eVar.A) && (!eVar.e || !eVar.g)) {
                Handler handler2 = a2.p;
                com.chartboost.sdk.impl.c cVar2 = a2.g;
                cVar2.getClass();
                handler2.post(new c.a(4, str, CBError.CBImpressionError.END_POINT_DISABLED));
                return;
            }
            com.chartboost.sdk.impl.e eVar2 = a2.f;
            eVar2.getClass();
            a2.f2426a.execute(new e.a(4, str, (f) null, (CBError.CBImpressionError) null));
        }
    }

    private static void showInterstitialAIR(String str, boolean z) {
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            com.chartboost.sdk.Model.e eVar = a2.m.get();
            if ((!eVar.y || !eVar.A) && (!eVar.e || !eVar.g)) {
                i.c.didFailToLoadInterstitial(str, CBError.CBImpressionError.END_POINT_DISABLED);
                return;
            }
            Handler handler = a2.p;
            com.chartboost.sdk.impl.c cVar = a2.g;
            cVar.getClass();
            handler.post(new c.a(4, str, CBError.CBImpressionError.INTERNAL));
        }
    }

    public static void showMoreApps(String str) {
        cacheMoreApps(str);
    }

    private static void showMoreAppsAIR(String str, boolean z) {
        cacheMoreApps(str);
    }

    public static void showRewardedVideo(String str) {
        aq.a("Chartboost.showRewardedVideo", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "showRewardedVideo location cannot be empty");
                Handler handler = a2.p;
                com.chartboost.sdk.impl.c cVar = a2.l;
                cVar.getClass();
                handler.post(new c.a(4, str, CBError.CBImpressionError.INVALID_LOCATION));
                return;
            }
            com.chartboost.sdk.Model.e eVar = a2.m.get();
            if ((!eVar.y || !eVar.E) && (!eVar.e || !eVar.j)) {
                Handler handler2 = a2.p;
                com.chartboost.sdk.impl.c cVar2 = a2.l;
                cVar2.getClass();
                handler2.post(new c.a(4, str, CBError.CBImpressionError.END_POINT_DISABLED));
                return;
            }
            com.chartboost.sdk.impl.e eVar2 = a2.k;
            eVar2.getClass();
            a2.f2426a.execute(new e.a(4, str, (f) null, (CBError.CBImpressionError) null));
        }
    }

    private static void showRewardedVideoAIR(String str, boolean z) {
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            com.chartboost.sdk.Model.e eVar = a2.m.get();
            if ((!eVar.y || !eVar.E) && (!eVar.e || !eVar.j)) {
                i.c.didFailToLoadRewardedVideo(str, CBError.CBImpressionError.END_POINT_DISABLED);
                return;
            }
            Handler handler = a2.p;
            com.chartboost.sdk.impl.c cVar = a2.g;
            cVar.getClass();
            handler.post(new c.a(4, str, CBError.CBImpressionError.INTERNAL));
        }
    }

    public static void startWithAppId(Activity activity, String str, String str2) {
        aq.a("Chartboost.startWithAppId", (Object) activity);
        f fVar = new f(0);
        fVar.h = activity;
        fVar.i = str;
        fVar.j = str2;
        h.b(fVar);
    }
}
