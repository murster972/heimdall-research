package com.chartboost.sdk;

import android.app.Activity;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.CBLogging;

class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    boolean f2424a = false;
    Chartboost.CBFramework b = null;
    Chartboost.CBMediation c = null;
    String d = null;
    String e = null;
    CBLogging.Level f = null;
    ChartboostDelegate g = null;
    Activity h = null;
    String i = null;
    String j = null;
    private final int k;

    f(int i2) {
        this.k = i2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v0, resolved type: java.util.concurrent.ScheduledExecutorService} */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x011c A[Catch:{ all -> 0x0116 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            int r0 = r10.k     // Catch:{ Exception -> 0x0136 }
            switch(r0) {
                case 0: goto L_0x0083;
                case 1: goto L_0x007d;
                case 2: goto L_0x0005;
                case 3: goto L_0x0059;
                case 4: goto L_0x002e;
                case 5: goto L_0x0027;
                case 6: goto L_0x0021;
                case 7: goto L_0x0014;
                case 8: goto L_0x0007;
                default: goto L_0x0005;
            }     // Catch:{ Exception -> 0x0136 }
        L_0x0005:
            goto L_0x0154
        L_0x0007:
            com.chartboost.sdk.ChartboostDelegate r0 = r10.g     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.c = r0     // Catch:{ Exception -> 0x0136 }
            java.lang.String r0 = "SdkSettings.assignDelegate"
            com.chartboost.sdk.ChartboostDelegate r1 = r10.g     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.impl.aq.a((java.lang.String) r0, (java.lang.Object) r1)     // Catch:{ Exception -> 0x0136 }
            goto L_0x0154
        L_0x0014:
            boolean r0 = com.chartboost.sdk.b.b()     // Catch:{ Exception -> 0x0136 }
            if (r0 != 0) goto L_0x001b
            return
        L_0x001b:
            com.chartboost.sdk.Libraries.CBLogging$Level r0 = r10.f     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.Libraries.CBLogging.f2390a = r0     // Catch:{ Exception -> 0x0136 }
            goto L_0x0154
        L_0x0021:
            java.lang.String r0 = r10.e     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.f2429a = r0     // Catch:{ Exception -> 0x0136 }
            goto L_0x0154
        L_0x0027:
            java.lang.String r0 = r10.d     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.b.a((java.lang.String) r0)     // Catch:{ Exception -> 0x0136 }
            goto L_0x0154
        L_0x002e:
            com.chartboost.sdk.Chartboost$CBFramework r0 = r10.b     // Catch:{ Exception -> 0x0136 }
            if (r0 != 0) goto L_0x003a
            java.lang.String r0 = "ChartboostCommand"
            java.lang.String r1 = "Pass a valid CBFramework enum value"
            com.chartboost.sdk.Libraries.CBLogging.b(r0, r1)     // Catch:{ Exception -> 0x0136 }
            return
        L_0x003a:
            com.chartboost.sdk.Chartboost$CBFramework r0 = r10.b     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.d = r0     // Catch:{ Exception -> 0x0136 }
            java.lang.String r0 = r10.d     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.e = r0     // Catch:{ Exception -> 0x0136 }
            java.lang.String r0 = "%s %s"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0136 }
            r2 = 0
            com.chartboost.sdk.Chartboost$CBFramework r3 = r10.b     // Catch:{ Exception -> 0x0136 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0136 }
            r2 = 1
            java.lang.String r3 = r10.d     // Catch:{ Exception -> 0x0136 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0136 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.f = r0     // Catch:{ Exception -> 0x0136 }
            goto L_0x0154
        L_0x0059:
            com.chartboost.sdk.Chartboost$CBMediation r0 = r10.c     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.i = r0     // Catch:{ Exception -> 0x0136 }
            java.lang.String r0 = r10.d     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.j = r0     // Catch:{ Exception -> 0x0136 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0136 }
            r0.<init>()     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.Chartboost$CBMediation r1 = com.chartboost.sdk.i.i     // Catch:{ Exception -> 0x0136 }
            r0.append(r1)     // Catch:{ Exception -> 0x0136 }
            java.lang.String r1 = " "
            r0.append(r1)     // Catch:{ Exception -> 0x0136 }
            java.lang.String r1 = com.chartboost.sdk.i.j     // Catch:{ Exception -> 0x0136 }
            r0.append(r1)     // Catch:{ Exception -> 0x0136 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.h = r0     // Catch:{ Exception -> 0x0136 }
            goto L_0x0154
        L_0x007d:
            boolean r0 = r10.f2424a     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.i.u = r0     // Catch:{ Exception -> 0x0136 }
            goto L_0x0154
        L_0x0083:
            com.chartboost.sdk.h r0 = com.chartboost.sdk.h.a()     // Catch:{ Exception -> 0x0136 }
            if (r0 != 0) goto L_0x0154
            java.lang.Class<com.chartboost.sdk.h> r0 = com.chartboost.sdk.h.class
            monitor-enter(r0)     // Catch:{ Exception -> 0x0136 }
            com.chartboost.sdk.h r1 = com.chartboost.sdk.h.a()     // Catch:{ all -> 0x0133 }
            if (r1 != 0) goto L_0x0131
            android.app.Activity r1 = r10.h     // Catch:{ all -> 0x0133 }
            if (r1 != 0) goto L_0x009f
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "Activity object is null. Please pass a valid activity object"
            com.chartboost.sdk.Libraries.CBLogging.b(r1, r2)     // Catch:{ all -> 0x0133 }
            monitor-exit(r0)     // Catch:{ all -> 0x0133 }
            return
        L_0x009f:
            android.app.Activity r1 = r10.h     // Catch:{ all -> 0x0133 }
            boolean r1 = com.chartboost.sdk.b.a((android.content.Context) r1)     // Catch:{ all -> 0x0133 }
            if (r1 != 0) goto L_0x00b0
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "Permissions not set correctly"
            com.chartboost.sdk.Libraries.CBLogging.b(r1, r2)     // Catch:{ all -> 0x0133 }
            monitor-exit(r0)     // Catch:{ all -> 0x0133 }
            return
        L_0x00b0:
            android.app.Activity r1 = r10.h     // Catch:{ all -> 0x0133 }
            boolean r1 = com.chartboost.sdk.b.b(r1)     // Catch:{ all -> 0x0133 }
            if (r1 != 0) goto L_0x00bf
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "Please add CBImpressionActivity in AndroidManifest.xml following README.md instructions."
            com.chartboost.sdk.Libraries.CBLogging.b(r1, r2)     // Catch:{ all -> 0x0133 }
        L_0x00bf:
            java.lang.String r1 = r10.i     // Catch:{ all -> 0x0133 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0133 }
            if (r1 != 0) goto L_0x0128
            java.lang.String r1 = r10.j     // Catch:{ all -> 0x0133 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0133 }
            if (r1 == 0) goto L_0x00d0
            goto L_0x0128
        L_0x00d0:
            com.chartboost.sdk.impl.s r6 = com.chartboost.sdk.impl.s.a()     // Catch:{ all -> 0x0133 }
            com.chartboost.sdk.g r1 = com.chartboost.sdk.g.a()     // Catch:{ all -> 0x0133 }
            android.os.Handler r8 = r6.f2494a     // Catch:{ all -> 0x0133 }
            com.chartboost.sdk.impl.o.a()     // Catch:{ all -> 0x0133 }
            r2 = 0
            java.util.concurrent.ScheduledExecutorService r3 = com.chartboost.sdk.impl.ac.a()     // Catch:{ all -> 0x0119 }
            java.lang.Object r3 = r1.a(r3)     // Catch:{ all -> 0x0119 }
            r7 = r3
            java.util.concurrent.ScheduledExecutorService r7 = (java.util.concurrent.ScheduledExecutorService) r7     // Catch:{ all -> 0x0119 }
            r2 = 4
            java.util.concurrent.ExecutorService r2 = com.chartboost.sdk.impl.ac.a(r2)     // Catch:{ all -> 0x0116 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ all -> 0x0116 }
            r9 = r1
            java.util.concurrent.ExecutorService r9 = (java.util.concurrent.ExecutorService) r9     // Catch:{ all -> 0x0116 }
            com.chartboost.sdk.h r1 = new com.chartboost.sdk.h     // Catch:{ all -> 0x0133 }
            android.app.Activity r3 = r10.h     // Catch:{ all -> 0x0133 }
            java.lang.String r4 = r10.i     // Catch:{ all -> 0x0133 }
            java.lang.String r5 = r10.j     // Catch:{ all -> 0x0133 }
            r2 = r1
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0133 }
            com.chartboost.sdk.h.a((com.chartboost.sdk.h) r1)     // Catch:{ all -> 0x0133 }
            com.chartboost.sdk.impl.l r2 = r1.b     // Catch:{ all -> 0x0133 }
            r2.c()     // Catch:{ all -> 0x0133 }
            com.chartboost.sdk.h$a r2 = new com.chartboost.sdk.h$a     // Catch:{ all -> 0x0133 }
            r1.getClass()     // Catch:{ all -> 0x0133 }
            r3 = 3
            r2.<init>(r3)     // Catch:{ all -> 0x0133 }
            r1.a((java.lang.Runnable) r2)     // Catch:{ all -> 0x0133 }
            goto L_0x0131
        L_0x0116:
            r1 = move-exception
            r2 = r7
            goto L_0x011a
        L_0x0119:
            r1 = move-exception
        L_0x011a:
            if (r2 == 0) goto L_0x011f
            r2.shutdown()     // Catch:{ all -> 0x0133 }
        L_0x011f:
            java.lang.String r2 = "ChartboostCommand"
            java.lang.String r3 = "Unable to start threads"
            com.chartboost.sdk.Libraries.CBLogging.a(r2, r3, r1)     // Catch:{ all -> 0x0133 }
            monitor-exit(r0)     // Catch:{ all -> 0x0133 }
            return
        L_0x0128:
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "AppId or AppSignature is null. Please pass a valid id's"
            com.chartboost.sdk.Libraries.CBLogging.b(r1, r2)     // Catch:{ all -> 0x0133 }
            monitor-exit(r0)     // Catch:{ all -> 0x0133 }
            return
        L_0x0131:
            monitor-exit(r0)     // Catch:{ all -> 0x0133 }
            goto L_0x0154
        L_0x0133:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0133 }
            throw r1     // Catch:{ Exception -> 0x0136 }
        L_0x0136:
            r0 = move-exception
            java.lang.Class<com.chartboost.sdk.f> r1 = com.chartboost.sdk.f.class
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "run ("
            r2.append(r3)
            int r3 = r10.k
            r2.append(r3)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.chartboost.sdk.Tracking.a.a((java.lang.Class) r1, (java.lang.String) r2, (java.lang.Exception) r0)
        L_0x0154:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.f.run():void");
    }
}
