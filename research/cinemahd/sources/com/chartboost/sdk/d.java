package com.chartboost.sdk;

import android.app.Activity;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.c;
import com.chartboost.sdk.impl.aw;
import com.chartboost.sdk.impl.bc;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.l;
import com.chartboost.sdk.impl.s;
import java.util.concurrent.atomic.AtomicReference;

public class d {

    /* renamed from: a  reason: collision with root package name */
    final aw f2418a;
    bc b = null;
    private final l c;
    private final AtomicReference<e> d;
    private final Handler e;
    private int f = -1;

    public d(aw awVar, l lVar, AtomicReference<e> atomicReference, Handler handler) {
        this.f2418a = awVar;
        this.c = lVar;
        this.d = atomicReference;
        this.e = handler;
    }

    private void e(c cVar) {
        int i;
        bc bcVar = this.b;
        if (bcVar == null || bcVar.e() == cVar) {
            int i2 = 1;
            boolean z = cVar.l != 2;
            cVar.l = 2;
            Activity b2 = cVar.g.b();
            CBError.CBImpressionError cBImpressionError = b2 == null ? CBError.CBImpressionError.NO_HOST_ACTIVITY : null;
            if (cBImpressionError == null) {
                cBImpressionError = cVar.j();
            }
            if (cBImpressionError != null) {
                CBLogging.b("CBViewController", "Unable to create the view while trying th display the impression");
                cVar.a(cBImpressionError);
                return;
            }
            if (this.b == null) {
                this.b = (bc) g.a().a(new bc(b2, cVar));
                b2.addContentView(this.b, new FrameLayout.LayoutParams(-1, -1));
            }
            CBUtility.a(b2, cVar.p.b, this.d.get());
            if (s.a().a(11) && this.f == -1 && ((i = cVar.n) == 1 || i == 2)) {
                this.f = b2.getWindow().getDecorView().getSystemUiVisibility();
                Chartboost.setActivityAttrs(b2);
            }
            this.b.a();
            CBLogging.e("CBViewController", "Displaying the impression");
            bc bcVar2 = this.b;
            cVar.s = bcVar2;
            if (z) {
                if (cVar.p.b == 0) {
                    bcVar2.c().a(this.f2418a, cVar.p);
                }
                if (cVar.p.b == 1) {
                    i2 = 6;
                }
                Integer a2 = aw.a(cVar.p.o);
                if (a2 != null) {
                    i2 = a2.intValue();
                }
                cVar.m();
                c cVar2 = cVar.g;
                cVar2.getClass();
                c.C0017c cVar3 = new c.C0017c(12);
                cVar3.d = cVar;
                this.f2418a.a(i2, cVar, (Runnable) cVar3, this);
                this.c.a();
                return;
            }
            return;
        }
        CBLogging.b("CBViewController", "Impression already visible");
        cVar.a(CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE);
    }

    /* access modifiers changed from: package-private */
    public void a(com.chartboost.sdk.Model.c cVar) {
        if (cVar.l != 0) {
            e(cVar);
        }
    }

    public void b(final com.chartboost.sdk.Model.c cVar) {
        CBLogging.e("CBViewController", "Dismissing impression");
        final Activity b2 = cVar.g.b();
        AnonymousClass1 r1 = new Runnable() {
            public void run() {
                com.chartboost.sdk.Model.c cVar = cVar;
                cVar.l = 4;
                int i = 1;
                if (cVar.p.b == 1) {
                    i = 6;
                }
                Integer a2 = aw.a(cVar.p.o);
                if (a2 != null) {
                    i = a2.intValue();
                }
                c cVar2 = cVar.g;
                cVar2.getClass();
                c.C0017c cVar3 = new c.C0017c(13);
                com.chartboost.sdk.Model.c cVar4 = cVar;
                cVar3.d = cVar4;
                cVar3.b = b2;
                d.this.f2418a.a(i, cVar4, (Runnable) cVar3);
            }
        };
        if (cVar.t) {
            cVar.a((Runnable) r1);
        } else {
            r1.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(com.chartboost.sdk.Model.c cVar) {
        CBLogging.e("CBViewController", "Removing impression silently");
        cVar.i();
        try {
            ((ViewGroup) this.b.getParent()).removeView(this.b);
        } catch (Exception e2) {
            CBLogging.a("CBViewController", "Exception removing impression silently", e2);
            a.a(d.class, "removeImpressionSilently", e2);
        }
        this.b = null;
    }

    public void d(com.chartboost.sdk.Model.c cVar) {
        CBLogging.e("CBViewController", "Removing impression");
        cVar.l = 5;
        cVar.h();
        this.b = null;
        this.c.b();
        Handler handler = this.e;
        com.chartboost.sdk.impl.c cVar2 = cVar.f2409a;
        cVar2.getClass();
        handler.post(new c.a(3, cVar.m, (CBError.CBImpressionError) null));
        if (cVar.v()) {
            Handler handler2 = this.e;
            com.chartboost.sdk.impl.c cVar3 = cVar.f2409a;
            cVar3.getClass();
            handler2.post(new c.a(2, cVar.m, (CBError.CBImpressionError) null));
        }
        a(cVar.g);
    }

    /* access modifiers changed from: package-private */
    public void a(com.chartboost.sdk.Model.c cVar, Activity activity) {
        c cVar2 = cVar.g;
        cVar2.getClass();
        c.C0017c cVar3 = new c.C0017c(14);
        cVar3.d = cVar;
        this.e.post(cVar3);
        cVar.l();
        CBUtility.b(activity, cVar.p.b, this.d.get());
        if (this.f != -1) {
            int i = cVar.n;
            if (i == 1 || i == 2) {
                activity.getWindow().getDecorView().setSystemUiVisibility(this.f);
                this.f = -1;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        CBLogging.e("CBViewController", "Attempting to close impression activity");
        Activity b2 = cVar.b();
        if (b2 != null && (b2 instanceof CBImpressionActivity)) {
            CBLogging.e("CBViewController", "Closing impression activity");
            cVar.f();
            b2.finish();
        }
    }

    public bc a() {
        return this.b;
    }
}
