package com.chartboost.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.j;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.impl.ai;
import com.chartboost.sdk.impl.aq;
import com.chartboost.sdk.impl.bc;
import com.chartboost.sdk.impl.s;
import com.facebook.common.util.ByteConstants;
import java.util.HashSet;

public class c {

    /* renamed from: a  reason: collision with root package name */
    final h f2414a;
    final Handler b;
    public final d c;
    j d;
    CBImpressionActivity e = null;
    com.chartboost.sdk.Model.c f = null;
    Runnable g;
    final Application.ActivityLifecycleCallbacks h;
    private final ai i;
    private final com.chartboost.sdk.Tracking.a j;
    private boolean k = false;
    private final HashSet<Integer> l = new HashSet<>();
    private j m;

    @TargetApi(14)
    private class a implements Application.ActivityLifecycleCallbacks {
        private a() {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityCreated", (Object) activity);
            CBLogging.a("CBUIManager", "######## onActivityCreated callback called");
            if (!(activity instanceof CBImpressionActivity)) {
                c.this.b(activity);
            }
        }

        public void onActivityDestroyed(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityDestroyed", (Object) activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityDestroyed callback called from developer side");
                c.this.j(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityDestroyed callback called from CBImpressionactivity");
            c.this.k(activity);
        }

        public void onActivityPaused(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityPaused", (Object) activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityPaused callback called from developer side");
                c.this.g(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityPaused callback called from CBImpressionactivity");
            c.this.a(activity);
            c.this.i();
        }

        public void onActivityResumed(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityResumed", (Object) activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityResumed callback called from developer side");
                c.this.f(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityResumed callback called from CBImpressionactivity");
            c.this.a(activity);
            c.this.h();
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityStarted", (Object) activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityStarted callback called from developer side");
                c.this.d(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityStarted callback called from CBImpressionactivity");
            c.this.e(activity);
        }

        public void onActivityStopped(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityStopped", (Object) activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityStopped callback called from developer side");
                c.this.h(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityStopped callback called from CBImpressionactivity");
            c.this.i(activity);
        }
    }

    class b implements Runnable {
        private final int b;
        private final int c;
        private final int d;

        b() {
            a a2 = a();
            CBImpressionActivity cBImpressionActivity = c.this.e;
            int i = -1;
            this.b = cBImpressionActivity == null ? -1 : cBImpressionActivity.hashCode();
            j jVar = c.this.d;
            this.c = jVar == null ? -1 : jVar.hashCode();
            this.d = a2 != null ? a2.hashCode() : i;
        }

        private a a() {
            return i.c;
        }

        public void run() {
            aq.a("ClearMemoryRunnable.run");
            a a2 = a();
            j jVar = c.this.d;
            if (jVar != null && jVar.hashCode() == this.c) {
                c.this.d = null;
                aq.a("CBUIManager.clearHostActivityRef");
            }
            if (a2 != null && a2.hashCode() == this.d) {
                i.c = null;
                aq.a("SdkSettings.clearDelegate");
            }
        }
    }

    /* renamed from: com.chartboost.sdk.c$c  reason: collision with other inner class name */
    public class C0017c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final int f2417a;
        Activity b = null;
        boolean c = false;
        public com.chartboost.sdk.Model.c d = null;

        public C0017c(int i) {
            this.f2417a = i;
        }

        public void run() {
            try {
                switch (this.f2417a) {
                    case 0:
                        c.this.c(this.b);
                        return;
                    case 1:
                        c.this.b.removeCallbacks(c.this.g);
                        if (c.this.d != null && !c.this.d.a(this.b) && c.this.g()) {
                            c.this.b(c.this.d);
                            c.this.a(c.this.d, false);
                        }
                        c.this.a(this.b, true);
                        c.this.d = c.this.a(this.b);
                        c.this.f2414a.b();
                        c.this.f2414a.a(this.b);
                        c.this.e(this.b);
                        return;
                    case 2:
                        if (c.this.a(c.this.a(this.b))) {
                            c.this.h();
                            return;
                        } else if (CBUtility.a(Chartboost.CBFramework.CBFrameworkUnity)) {
                            c.this.f2414a.b();
                            return;
                        } else {
                            return;
                        }
                    case 3:
                        if (c.this.a(c.this.a(this.b))) {
                            c.this.i();
                            return;
                        }
                        return;
                    case 4:
                        j a2 = c.this.a(this.b);
                        if (c.this.a(a2)) {
                            c.this.b(a2);
                            return;
                        }
                        return;
                    case 5:
                        if (c.this.d == null || c.this.d.a(this.b)) {
                            c.this.g = new b();
                            c.this.g.run();
                        }
                        c.this.k(this.b);
                        return;
                    case 6:
                        if (c.this.e == null) {
                            return;
                        }
                        if (this.c) {
                            c.this.e.forwardTouchEvents(c.this.a());
                            return;
                        } else {
                            c.this.e.forwardTouchEvents((Activity) null);
                            return;
                        }
                    case 7:
                        c.this.l();
                        return;
                    case 9:
                        c.this.a(this.b, this.d);
                        return;
                    case 10:
                        if (this.d.a()) {
                            this.d.u().b();
                            return;
                        }
                        return;
                    case 11:
                        d c2 = c.this.c();
                        if (this.d.l == 2 && c2 != null) {
                            c2.b(this.d);
                            return;
                        }
                        return;
                    case 12:
                        this.d.n();
                        return;
                    case 13:
                        c.this.c.a(this.d, this.b);
                        return;
                    case 14:
                        c.this.c.d(this.d);
                        return;
                    default:
                        return;
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(C0017c.class, "run (" + this.f2417a + ")", e2);
            }
        }
    }

    public c(Activity activity, ai aiVar, h hVar, com.chartboost.sdk.Tracking.a aVar, Handler handler, d dVar) {
        this.i = aiVar;
        this.f2414a = hVar;
        this.j = aVar;
        this.b = handler;
        this.c = dVar;
        this.d = a(activity);
        aq.a("CBUIManager.assignHostActivityRef", (Object) this.d);
        this.g = new b();
        if (s.a().a(14)) {
            this.h = new a();
        } else {
            this.h = null;
        }
    }

    private void b(j jVar, boolean z) {
    }

    private boolean l(Activity activity) {
        return this.e == activity;
    }

    private boolean m() {
        aq.a("CBUIManager.closeImpressionImpl");
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 == null || d2.l != 2) {
            return false;
        }
        if (d2.q()) {
            return true;
        }
        h.b(new C0017c(7));
        return true;
    }

    /* access modifiers changed from: package-private */
    public j a(Activity activity) {
        j jVar = this.m;
        if (jVar == null || jVar.f2401a != activity.hashCode()) {
            this.m = new j(activity);
        }
        return this.m;
    }

    public Activity b() {
        return this.e;
    }

    public d c() {
        if (b() == null) {
            return null;
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public com.chartboost.sdk.Model.c d() {
        bc bcVar;
        d c2 = c();
        if (c2 == null) {
            bcVar = null;
        } else {
            bcVar = c2.a();
        }
        if (bcVar == null || !bcVar.f()) {
            return null;
        }
        return bcVar.e();
    }

    public boolean e() {
        return d() != null;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        aq.a("CBUIManager.clearImpressionActivity");
        this.e = null;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return a(this.d);
    }

    /* access modifiers changed from: package-private */
    public void h() {
        aq.a("CBUIManager.onResumeImpl", (String) null);
        this.i.b(i.m);
        com.chartboost.sdk.Model.c d2 = d();
        if (CBUtility.a(Chartboost.CBFramework.CBFrameworkUnity)) {
            this.f2414a.b();
        }
        if (d2 != null) {
            d2.r();
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        aq.a("CBUIManager.onPauseImpl", (String) null);
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 != null) {
            d2.t();
        }
        this.i.c(i.m);
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        aq.a("CBUIManager.onBackPressedCallback");
        if (!b.b()) {
            return false;
        }
        if (this.d == null) {
            CBLogging.b("CBUIManager", "The Chartboost methods onCreate(), onStart(), onStop(), and onDestroy() must be called in the corresponding methods of your activity in order for Chartboost to function properly.");
            return false;
        } else if (!this.k) {
            return false;
        } else {
            this.k = false;
            k();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        aq.a("CBUIManager.onBackPressedImpl");
        return m();
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        aq.a("CBUIManager.onStop", (Object) jVar);
        if (!(jVar.get() instanceof CBImpressionActivity)) {
            a(jVar, false);
        }
        this.f2414a.c();
    }

    /* access modifiers changed from: package-private */
    public void e(Activity activity) {
        aq.a("CBUIManager.onStartImpl", (Object) activity);
        i.m = activity.getApplicationContext();
        boolean z = activity instanceof CBImpressionActivity;
        if (!z) {
            this.d = a(activity);
            aq.a("CBUIManager.assignHostActivityRef", (Object) this.d);
            a(this.d, true);
        } else {
            a((CBImpressionActivity) activity);
        }
        this.b.removeCallbacks(this.g);
        Chartboost.CBFramework cBFramework = i.d;
        boolean z2 = cBFramework != null && cBFramework.doesWrapperUseCustomBackgroundingBehavior();
        if (activity == null) {
            return;
        }
        if (z2 || l(activity)) {
            b(a(activity), true);
            if (z) {
                this.k = false;
            }
            if (b(activity, this.f)) {
                this.f = null;
            }
            com.chartboost.sdk.Model.c d2 = d();
            if (d2 != null) {
                d2.s();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g(Activity activity) {
        aq.a("CBUIManager.onPauseCallback", (Object) activity);
        if (b.b() && b.a(activity)) {
            C0017c cVar = new C0017c(3);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 == null) {
            return false;
        }
        d2.z = true;
        b(d2);
        return true;
    }

    private boolean c(j jVar) {
        if (jVar == null) {
            return this.e == null;
        }
        return jVar.a(this.e);
    }

    /* access modifiers changed from: package-private */
    public void f(Activity activity) {
        aq.a("CBUIManager.onResumeCallback", (Object) activity);
        if (b.b() && b.a(activity)) {
            this.f2414a.e();
            C0017c cVar = new C0017c(2);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void k(Activity activity) {
        com.chartboost.sdk.Model.c cVar;
        aq.a("CBUIManager.onDestroyImpl", (Object) activity);
        b(a(activity), false);
        com.chartboost.sdk.Model.c d2 = d();
        if (!(d2 == null && activity == this.e && (cVar = this.f) != null)) {
            cVar = d2;
        }
        d c2 = c();
        if (!(c2 == null || cVar == null)) {
            c2.d(cVar);
        }
        this.f = null;
    }

    public Activity a() {
        j jVar = this.d;
        if (jVar != null) {
            return (Activity) jVar.get();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(CBImpressionActivity cBImpressionActivity) {
        aq.a("CBUIManager.setImpressionActivity", (Object) cBImpressionActivity);
        if (this.e == null) {
            i.m = cBImpressionActivity.getApplicationContext();
            this.e = cBImpressionActivity;
        }
        this.b.removeCallbacks(this.g);
    }

    /* access modifiers changed from: package-private */
    public void c(Activity activity) {
        aq.a("CBUIManager.onCreateImpl", (Object) activity);
        j jVar = this.d;
        if (jVar != null && !jVar.a(activity) && g()) {
            b(this.d);
            a(this.d, false);
        }
        this.b.removeCallbacks(this.g);
        this.d = a(activity);
        aq.a("CBUIManager.assignHostActivityRef", (Object) this.d);
    }

    /* access modifiers changed from: package-private */
    public void d(Activity activity) {
        aq.a("CBUIManager.onStartCallback", (Object) activity);
        if (b.b() && b.a(activity)) {
            C0017c cVar = new C0017c(1);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void i(Activity activity) {
        j a2 = a(activity);
        aq.a("CBUIManager.onStopImpl", (Object) a2);
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 != null && d2.p.b == 0) {
            d c2 = c();
            if (c(a2) && c2 != null) {
                c2.c(d2);
                this.f = d2;
                b(a2, false);
            }
            if (!(a2.get() instanceof CBImpressionActivity)) {
                a(a2, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Activity activity) {
        aq.a("CBUIManager.onCreateCallback", (Object) activity);
        if (b.b() && b.a(activity)) {
            C0017c cVar = new C0017c(0);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void h(Activity activity) {
        aq.a("CBUIManager.onStopCallback", (Object) activity);
        if (b.b() && b.a(activity)) {
            C0017c cVar = new C0017c(4);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void j(Activity activity) {
        aq.a("CBUIManager.onDestroyCallback", (Object) activity);
        if (b.b() && b.a(activity)) {
            C0017c cVar = new C0017c(5);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    private void a(int i2, boolean z) {
        if (z) {
            this.l.add(Integer.valueOf(i2));
        } else {
            this.l.remove(Integer.valueOf(i2));
        }
    }

    public void b(com.chartboost.sdk.Model.c cVar) {
        d c2;
        int i2 = cVar.l;
        if (i2 == 2) {
            d c3 = c();
            if (c3 != null) {
                c3.b(cVar);
            }
        } else if (cVar.p.b == 1 && i2 == 1 && (c2 = c()) != null) {
            c2.d(cVar);
        }
        if (cVar.v()) {
            this.j.d(cVar.f2409a.a(cVar.p.b), cVar.m, cVar.o());
        } else {
            this.j.e(cVar.f2409a.a(cVar.p.b), cVar.m, cVar.o());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar, boolean z) {
        if (jVar != null) {
            a(jVar.f2401a, z);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, boolean z) {
        if (activity != null) {
            a(activity.hashCode(), z);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(j jVar) {
        if (jVar == null) {
            return false;
        }
        return this.l.contains(Integer.valueOf(jVar.f2401a));
    }

    public void a(com.chartboost.sdk.Model.c cVar) {
        aq.a("CBUIManager.queueDisplayView", (Object) cVar);
        if (e()) {
            cVar.a(CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE);
        } else if (this.e != null) {
            this.c.a(cVar);
        } else if (!g()) {
            cVar.a(CBError.CBImpressionError.NO_HOST_ACTIVITY);
        } else {
            Activity a2 = a();
            if (a2 == null) {
                CBLogging.b("CBUIManager", "Failed to display impression as the host activity reference has been lost!");
                cVar.a(CBError.CBImpressionError.NO_HOST_ACTIVITY);
                return;
            }
            com.chartboost.sdk.Model.c cVar2 = this.f;
            if (cVar2 == null || cVar2 == cVar) {
                this.f = cVar;
                a aVar = i.c;
                if (aVar != null) {
                    int i2 = cVar.n;
                    if (i2 == 1 || i2 == 2) {
                        i.c.willDisplayVideo(cVar.m);
                    } else if (i2 == 0) {
                        aVar.willDisplayInterstitial(cVar.m);
                    }
                }
                if (i.d != null) {
                    C0017c cVar3 = new C0017c(9);
                    cVar3.b = a2;
                    cVar3.d = cVar;
                    this.b.postDelayed(cVar3, (long) 1);
                    return;
                }
                a(a2, cVar);
                return;
            }
            cVar.a(CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000d, code lost:
        if (r1 != 3) goto L_0x004b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(android.app.Activity r4, com.chartboost.sdk.Model.c r5) {
        /*
            r3 = this;
            r0 = 1
            if (r5 == 0) goto L_0x004b
            int r1 = r5.l
            if (r1 == 0) goto L_0x004b
            if (r1 == r0) goto L_0x0048
            r2 = 2
            if (r1 == r2) goto L_0x0010
            r4 = 3
            if (r1 == r4) goto L_0x0048
            goto L_0x004b
        L_0x0010:
            boolean r1 = r5.g()
            if (r1 != 0) goto L_0x004b
            com.chartboost.sdk.Chartboost$CBFramework r1 = com.chartboost.sdk.i.d
            if (r1 == 0) goto L_0x0026
            boolean r1 = r1.doesWrapperUseCustomBackgroundingBehavior()
            if (r1 == 0) goto L_0x0026
            boolean r4 = r4 instanceof com.chartboost.sdk.CBImpressionActivity
            if (r4 != 0) goto L_0x0026
            r4 = 0
            return r4
        L_0x0026:
            com.chartboost.sdk.d r4 = r3.c()
            if (r4 == 0) goto L_0x004b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error onActivityStart "
            r1.append(r2)
            int r2 = r5.l
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "CBUIManager"
            com.chartboost.sdk.Libraries.CBLogging.b(r2, r1)
            r4.d(r5)
            goto L_0x004b
        L_0x0048:
            r3.a((com.chartboost.sdk.Model.c) r5)
        L_0x004b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.c.b(android.app.Activity, com.chartboost.sdk.Model.c):boolean");
    }

    public void a(Activity activity, com.chartboost.sdk.Model.c cVar) {
        Intent intent = new Intent(activity, CBImpressionActivity.class);
        boolean z = false;
        boolean z2 = (activity.getWindow().getAttributes().flags & ByteConstants.KB) != 0;
        boolean z3 = (activity.getWindow().getAttributes().flags & 2048) != 0;
        if (z2 && !z3) {
            z = true;
        }
        intent.putExtra("paramFullscreen", z);
        intent.putExtra("isChartboost", true);
        try {
            activity.startActivity(intent);
            this.k = true;
        } catch (ActivityNotFoundException unused) {
            CBLogging.b("CBUIManager", "Please add CBImpressionActivity in AndroidManifest.xml following README.md instructions.");
            this.f = null;
            cVar.a(CBError.CBImpressionError.ACTIVITY_MISSING_IN_MANIFEST);
        }
    }
}
