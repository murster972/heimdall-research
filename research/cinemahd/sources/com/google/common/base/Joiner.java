package com.google.common.base;

import java.io.IOException;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;

public class Joiner {

    /* renamed from: a  reason: collision with root package name */
    private final String f4348a;

    private Joiner(String str) {
        Preconditions.a(str);
        this.f4348a = str;
    }

    public static Joiner on(String str) {
        return new Joiner(str);
    }

    public <A extends Appendable> A a(A a2, Iterator<?> it2) throws IOException {
        Preconditions.a(a2);
        if (it2.hasNext()) {
            a2.append(a(it2.next()));
            while (it2.hasNext()) {
                a2.append(this.f4348a);
                a2.append(a(it2.next()));
            }
        }
        return a2;
    }

    public final String join(Iterable<?> iterable) {
        return join(iterable.iterator());
    }

    public final String join(Iterator<?> it2) {
        return a(new StringBuilder(), it2).toString();
    }

    public final String join(Object[] objArr) {
        return join((Iterable<?>) Arrays.asList(objArr));
    }

    public final String join(Object obj, Object obj2, Object... objArr) {
        return join((Iterable<?>) a(obj, obj2, objArr));
    }

    public final StringBuilder a(StringBuilder sb, Iterator<?> it2) {
        try {
            a(sb, it2);
            return sb;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    /* access modifiers changed from: package-private */
    public CharSequence a(Object obj) {
        Preconditions.a(obj);
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }

    private static Iterable<Object> a(final Object obj, final Object obj2, final Object[] objArr) {
        Preconditions.a(objArr);
        return new AbstractList<Object>() {
            public Object get(int i) {
                if (i == 0) {
                    return obj;
                }
                if (i != 1) {
                    return objArr[i - 2];
                }
                return obj2;
            }

            public int size() {
                return objArr.length + 2;
            }
        };
    }
}
