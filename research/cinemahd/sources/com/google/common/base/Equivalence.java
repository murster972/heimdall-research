package com.google.common.base;

import java.io.Serializable;

public abstract class Equivalence<T> {

    static final class Equals extends Equivalence<Object> implements Serializable {

        /* renamed from: a  reason: collision with root package name */
        static final Equals f4346a = new Equals();
        private static final long serialVersionUID = 1;

        Equals() {
        }

        private Object readResolve() {
            return f4346a;
        }

        /* access modifiers changed from: protected */
        public boolean a(Object obj, Object obj2) {
            return obj.equals(obj2);
        }

        /* access modifiers changed from: protected */
        public int a(Object obj) {
            return obj.hashCode();
        }
    }

    static final class Identity extends Equivalence<Object> implements Serializable {

        /* renamed from: a  reason: collision with root package name */
        static final Identity f4347a = new Identity();
        private static final long serialVersionUID = 1;

        Identity() {
        }

        private Object readResolve() {
            return f4347a;
        }

        /* access modifiers changed from: protected */
        public int a(Object obj) {
            return System.identityHashCode(obj);
        }

        /* access modifiers changed from: protected */
        public boolean a(Object obj, Object obj2) {
            return false;
        }
    }

    protected Equivalence() {
    }

    public static Equivalence<Object> a() {
        return Equals.f4346a;
    }

    /* access modifiers changed from: protected */
    public abstract int a(T t);

    /* access modifiers changed from: protected */
    public abstract boolean a(T t, T t2);

    public final boolean b(T t, T t2) {
        if (t == t2) {
            return true;
        }
        if (t == null || t2 == null) {
            return false;
        }
        return a(t, t2);
    }

    public final int b(T t) {
        if (t == null) {
            return 0;
        }
        return a(t);
    }

    public static Equivalence<Object> b() {
        return Identity.f4347a;
    }
}
