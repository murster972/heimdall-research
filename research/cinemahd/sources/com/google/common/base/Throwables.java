package com.google.common.base;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class Throwables {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f4352a = b();

    static {
        if (f4352a != null) {
            a();
        }
        if (f4352a != null) {
            c();
        }
    }

    private Throwables() {
    }

    @Deprecated
    public static <X extends Throwable> void a(Throwable th, Class<X> cls) throws Throwable {
        if (th != null) {
            c(th, cls);
        }
    }

    public static void b(Throwable th) {
        Preconditions.a(th);
        if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else if (th instanceof Error) {
            throw ((Error) th);
        }
    }

    public static <X extends Throwable> void c(Throwable th, Class<X> cls) throws Throwable {
        Preconditions.a(th);
        if (cls.isInstance(th)) {
            throw ((Throwable) cls.cast(th));
        }
    }

    @Deprecated
    public static void a(Throwable th) {
        if (th != null) {
            b(th);
        }
    }

    private static Method a() {
        return a("getStackTraceElement", (Class<?>[]) new Class[]{Throwable.class, Integer.TYPE});
    }

    private static Method a(String str, Class<?>... clsArr) throws ThreadDeath {
        try {
            return Class.forName("sun.misc.JavaLangAccess", false, (ClassLoader) null).getMethod(str, clsArr);
        } catch (ThreadDeath e) {
            throw e;
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Method c() {
        try {
            Method a2 = a("getStackTraceDepth", (Class<?>[]) new Class[]{Throwable.class});
            if (a2 == null) {
                return null;
            }
            a2.invoke(b(), new Object[]{new Throwable()});
            return a2;
        } catch (IllegalAccessException | UnsupportedOperationException | InvocationTargetException unused) {
            return null;
        }
    }

    public static <X extends Throwable> void b(Throwable th, Class<X> cls) throws Throwable {
        a(th, cls);
        a(th);
    }

    private static Object b() {
        try {
            return Class.forName("sun.misc.SharedSecrets", false, (ClassLoader) null).getMethod("getJavaLangAccess", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (ThreadDeath e) {
            throw e;
        } catch (Throwable unused) {
            return null;
        }
    }
}
