package com.google.common.base;

import java.io.Serializable;

public abstract class Optional<T> implements Serializable {
    private static final long serialVersionUID = 0;

    Optional() {
    }

    public static <T> Optional<T> c() {
        return Absent.d();
    }

    public abstract T a();

    public abstract boolean b();
}
