package com.google.common.base;

import java.util.Arrays;

public final class MoreObjects {

    public static final class ToStringHelper {

        /* renamed from: a  reason: collision with root package name */
        private final String f4350a;
        private final ValueHolder b;
        private ValueHolder c;
        private boolean d;

        private static final class ValueHolder {

            /* renamed from: a  reason: collision with root package name */
            String f4351a;
            Object b;
            ValueHolder c;

            private ValueHolder() {
            }
        }

        private ToStringHelper b(Object obj) {
            a().b = obj;
            return this;
        }

        public ToStringHelper a(String str, Object obj) {
            b(str, obj);
            return this;
        }

        public String toString() {
            boolean z = this.d;
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.f4350a);
            sb.append('{');
            String str = "";
            for (ValueHolder valueHolder = this.b.c; valueHolder != null; valueHolder = valueHolder.c) {
                Object obj = valueHolder.b;
                if (!z || obj != null) {
                    sb.append(str);
                    String str2 = valueHolder.f4351a;
                    if (str2 != null) {
                        sb.append(str2);
                        sb.append('=');
                    }
                    if (obj == null || !obj.getClass().isArray()) {
                        sb.append(obj);
                    } else {
                        String deepToString = Arrays.deepToString(new Object[]{obj});
                        sb.append(deepToString, 1, deepToString.length() - 1);
                    }
                    str = ", ";
                }
            }
            sb.append('}');
            return sb.toString();
        }

        private ToStringHelper(String str) {
            this.b = new ValueHolder();
            this.c = this.b;
            this.d = false;
            Preconditions.a(str);
            this.f4350a = str;
        }

        public ToStringHelper a(String str, int i) {
            b(str, String.valueOf(i));
            return this;
        }

        private ToStringHelper b(String str, Object obj) {
            ValueHolder a2 = a();
            a2.b = obj;
            Preconditions.a(str);
            a2.f4351a = str;
            return this;
        }

        public ToStringHelper a(Object obj) {
            b(obj);
            return this;
        }

        private ValueHolder a() {
            ValueHolder valueHolder = new ValueHolder();
            this.c.c = valueHolder;
            this.c = valueHolder;
            return valueHolder;
        }
    }

    private MoreObjects() {
    }

    public static <T> T a(T t, T t2) {
        if (t != null) {
            return t;
        }
        if (t2 != null) {
            return t2;
        }
        throw new NullPointerException("Both parameters are null");
    }

    public static ToStringHelper a(Object obj) {
        return new ToStringHelper(obj.getClass().getSimpleName());
    }
}
