package com.google.common.base;

final class Absent<T> extends Optional<T> {

    /* renamed from: a  reason: collision with root package name */
    static final Absent<Object> f4345a = new Absent<>();
    private static final long serialVersionUID = 0;

    private Absent() {
    }

    static <T> Optional<T> d() {
        return f4345a;
    }

    private Object readResolve() {
        return f4345a;
    }

    public T a() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    public boolean b() {
        return false;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public int hashCode() {
        return 2040732332;
    }

    public String toString() {
        return "Optional.absent()";
    }
}
