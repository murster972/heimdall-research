package com.google.common.base;

public final class Objects extends ExtraObjectsMethodsForWeb {
    private Objects() {
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }
}
