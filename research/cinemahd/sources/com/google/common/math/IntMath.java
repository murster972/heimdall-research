package com.google.common.math;

import com.google.common.primitives.Ints;

public final class IntMath {
    private IntMath() {
    }

    public static int a(int i, int i2) {
        return Ints.a(((long) i) * ((long) i2));
    }
}
