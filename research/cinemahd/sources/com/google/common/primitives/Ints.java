package com.google.common.primitives;

public final class Ints extends IntsMethodsForWeb {
    private Ints() {
    }

    public static int a(long j) {
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (j < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int) j;
    }
}
