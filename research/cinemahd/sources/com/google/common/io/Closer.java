package com.google.common.io;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Closer implements Closeable {
    private static final Suppressor d;

    /* renamed from: a  reason: collision with root package name */
    final Suppressor f4377a;
    private final Deque<Closeable> b = new ArrayDeque(4);
    private Throwable c;

    static final class LoggingSuppressor implements Suppressor {

        /* renamed from: a  reason: collision with root package name */
        static final LoggingSuppressor f4378a = new LoggingSuppressor();

        LoggingSuppressor() {
        }

        public void a(Closeable closeable, Throwable th, Throwable th2) {
            Logger logger = Closeables.f4376a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(closeable);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 42);
            sb.append("Suppressing exception thrown when closing ");
            sb.append(valueOf);
            logger.log(level, sb.toString(), th2);
        }
    }

    static final class SuppressingSuppressor implements Suppressor {

        /* renamed from: a  reason: collision with root package name */
        static final SuppressingSuppressor f4379a = new SuppressingSuppressor();
        static final Method b = a();

        SuppressingSuppressor() {
        }

        private static Method a() {
            try {
                return Throwable.class.getMethod("addSuppressed", new Class[]{Throwable.class});
            } catch (Throwable unused) {
                return null;
            }
        }

        static boolean b() {
            return b != null;
        }

        public void a(Closeable closeable, Throwable th, Throwable th2) {
            if (th != th2) {
                try {
                    b.invoke(th, new Object[]{th2});
                } catch (Throwable unused) {
                    LoggingSuppressor.f4378a.a(closeable, th, th2);
                }
            }
        }
    }

    interface Suppressor {
        void a(Closeable closeable, Throwable th, Throwable th2);
    }

    static {
        Suppressor suppressor;
        if (SuppressingSuppressor.b()) {
            suppressor = SuppressingSuppressor.f4379a;
        } else {
            suppressor = LoggingSuppressor.f4378a;
        }
        d = suppressor;
    }

    Closer(Suppressor suppressor) {
        Preconditions.a(suppressor);
        this.f4377a = suppressor;
    }

    public static Closer s() {
        return new Closer(d);
    }

    public <C extends Closeable> C a(C c2) {
        if (c2 != null) {
            this.b.addFirst(c2);
        }
        return c2;
    }

    public void close() throws IOException {
        Throwable th = this.c;
        while (!this.b.isEmpty()) {
            Closeable removeFirst = this.b.removeFirst();
            try {
                removeFirst.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                } else {
                    this.f4377a.a(removeFirst, th, th2);
                }
            }
        }
        if (this.c == null && th != null) {
            Throwables.b(th, IOException.class);
            throw new AssertionError(th);
        }
    }

    public RuntimeException a(Throwable th) throws IOException {
        Preconditions.a(th);
        this.c = th;
        Throwables.b(th, IOException.class);
        throw new RuntimeException(th);
    }
}
