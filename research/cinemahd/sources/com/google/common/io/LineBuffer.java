package com.google.common.io;

import java.io.IOException;

abstract class LineBuffer {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f4380a = new StringBuilder();
    private boolean b;

    LineBuffer() {
    }

    /* access modifiers changed from: protected */
    public abstract void a(String str, String str2) throws IOException;

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(char[] r7, int r8, int r9) throws java.io.IOException {
        /*
            r6 = this;
            boolean r0 = r6.b
            r1 = 0
            r2 = 10
            r3 = 1
            if (r0 == 0) goto L_0x0019
            if (r9 <= 0) goto L_0x0019
            char r0 = r7[r8]
            if (r0 != r2) goto L_0x0010
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            r6.a(r0)
            if (r0 == 0) goto L_0x0019
            int r0 = r8 + 1
            goto L_0x001a
        L_0x0019:
            r0 = r8
        L_0x001a:
            int r8 = r8 + r9
            r9 = r0
        L_0x001c:
            if (r0 >= r8) goto L_0x0050
            char r4 = r7[r0]
            if (r4 == r2) goto L_0x0042
            r5 = 13
            if (r4 == r5) goto L_0x0027
            goto L_0x004e
        L_0x0027:
            java.lang.StringBuilder r4 = r6.f4380a
            int r5 = r0 - r9
            r4.append(r7, r9, r5)
            r6.b = r3
            int r9 = r0 + 1
            if (r9 >= r8) goto L_0x004c
            char r4 = r7[r9]
            if (r4 != r2) goto L_0x003a
            r4 = 1
            goto L_0x003b
        L_0x003a:
            r4 = 0
        L_0x003b:
            r6.a(r4)
            if (r4 == 0) goto L_0x004c
            r0 = r9
            goto L_0x004c
        L_0x0042:
            java.lang.StringBuilder r4 = r6.f4380a
            int r5 = r0 - r9
            r4.append(r7, r9, r5)
            r6.a(r3)
        L_0x004c:
            int r9 = r0 + 1
        L_0x004e:
            int r0 = r0 + r3
            goto L_0x001c
        L_0x0050:
            java.lang.StringBuilder r0 = r6.f4380a
            int r8 = r8 - r9
            r0.append(r7, r9, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.io.LineBuffer.a(char[], int, int):void");
    }

    private boolean a(boolean z) throws IOException {
        a(this.f4380a.toString(), this.b ? z ? "\r\n" : "\r" : z ? ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE : "");
        this.f4380a = new StringBuilder();
        this.b = false;
        return z;
    }

    /* access modifiers changed from: protected */
    public void a() throws IOException {
        if (this.b || this.f4380a.length() > 0) {
            a(false);
        }
    }
}
