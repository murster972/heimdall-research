package com.google.common.io;

import java.util.logging.Logger;

public final class Closeables {

    /* renamed from: a  reason: collision with root package name */
    static final Logger f4376a = Logger.getLogger(Closeables.class.getName());

    private Closeables() {
    }
}
