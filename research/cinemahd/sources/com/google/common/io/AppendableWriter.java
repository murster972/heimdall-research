package com.google.common.io;

import com.google.common.base.Preconditions;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;

class AppendableWriter extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final Appendable f4374a;
    private boolean b;

    AppendableWriter(Appendable appendable) {
        Preconditions.a(appendable);
        this.f4374a = appendable;
    }

    private void s() throws IOException {
        if (this.b) {
            throw new IOException("Cannot write to a closed writer.");
        }
    }

    public void close() throws IOException {
        this.b = true;
        Appendable appendable = this.f4374a;
        if (appendable instanceof Closeable) {
            ((Closeable) appendable).close();
        }
    }

    public void flush() throws IOException {
        s();
        Appendable appendable = this.f4374a;
        if (appendable instanceof Flushable) {
            ((Flushable) appendable).flush();
        }
    }

    public void write(char[] cArr, int i, int i2) throws IOException {
        s();
        this.f4374a.append(new String(cArr, i, i2));
    }

    public void write(int i) throws IOException {
        s();
        this.f4374a.append((char) i);
    }

    public Writer append(char c) throws IOException {
        s();
        this.f4374a.append(c);
        return this;
    }

    public void write(String str) throws IOException {
        s();
        this.f4374a.append(str);
    }

    public Writer append(CharSequence charSequence) throws IOException {
        s();
        this.f4374a.append(charSequence);
        return this;
    }

    public void write(String str, int i, int i2) throws IOException {
        s();
        this.f4374a.append(str, i, i2 + i);
    }

    public Writer append(CharSequence charSequence, int i, int i2) throws IOException {
        s();
        this.f4374a.append(charSequence, i, i2);
        return this;
    }
}
