package com.google.common.io;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.Charset;

public abstract class ByteSource {

    class AsCharSource extends CharSource {

        /* renamed from: a  reason: collision with root package name */
        final Charset f4375a;

        AsCharSource(Charset charset) {
            Preconditions.a(charset);
            this.f4375a = charset;
        }

        public Reader a() throws IOException {
            return new InputStreamReader(ByteSource.this.a(), this.f4375a);
        }

        public String b() throws IOException {
            return new String(ByteSource.this.b(), this.f4375a);
        }

        public String toString() {
            String obj = ByteSource.this.toString();
            String valueOf = String.valueOf(this.f4375a);
            StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 15 + String.valueOf(valueOf).length());
            sb.append(obj);
            sb.append(".asCharSource(");
            sb.append(valueOf);
            sb.append(")");
            return sb.toString();
        }
    }

    protected ByteSource() {
    }

    public CharSource a(Charset charset) {
        return new AsCharSource(charset);
    }

    public abstract InputStream a() throws IOException;

    public byte[] b() throws IOException {
        byte[] bArr;
        Closer s = Closer.s();
        try {
            InputStream a2 = a();
            s.a(a2);
            InputStream inputStream = a2;
            Optional<Long> c = c();
            if (c.b()) {
                bArr = ByteStreams.a(inputStream, c.a().longValue());
            } else {
                bArr = ByteStreams.a(inputStream);
            }
            s.close();
            return bArr;
        } catch (Throwable th) {
            s.close();
            throw th;
        }
    }

    public Optional<Long> c() {
        return Optional.c();
    }

    public long a(OutputStream outputStream) throws IOException {
        Preconditions.a(outputStream);
        Closer s = Closer.s();
        try {
            InputStream a2 = a();
            s.a(a2);
            long a3 = ByteStreams.a(a2, outputStream);
            s.close();
            return a3;
        } catch (Throwable th) {
            s.close();
            throw th;
        }
    }
}
