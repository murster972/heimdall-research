package com.google.common.io;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;

public final class CharStreams {
    private CharStreams() {
    }

    static CharBuffer a() {
        return CharBuffer.allocate(2048);
    }

    private static StringBuilder b(Readable readable) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (readable instanceof Reader) {
            a((Reader) readable, sb);
        } else {
            a(readable, (Appendable) sb);
        }
        return sb;
    }

    public static long a(Readable readable, Appendable appendable) throws IOException {
        if (!(readable instanceof Reader)) {
            Preconditions.a(readable);
            Preconditions.a(appendable);
            long j = 0;
            CharBuffer a2 = a();
            while (readable.read(a2) != -1) {
                Java8Compatibility.b(a2);
                appendable.append(a2);
                j += (long) a2.remaining();
                Java8Compatibility.a(a2);
            }
            return j;
        } else if (appendable instanceof StringBuilder) {
            return a((Reader) readable, (StringBuilder) appendable);
        } else {
            return a((Reader) readable, a(appendable));
        }
    }

    static long a(Reader reader, StringBuilder sb) throws IOException {
        Preconditions.a(reader);
        Preconditions.a(sb);
        char[] cArr = new char[2048];
        long j = 0;
        while (true) {
            int read = reader.read(cArr);
            if (read == -1) {
                return j;
            }
            sb.append(cArr, 0, read);
            j += (long) read;
        }
    }

    static long a(Reader reader, Writer writer) throws IOException {
        Preconditions.a(reader);
        Preconditions.a(writer);
        char[] cArr = new char[2048];
        long j = 0;
        while (true) {
            int read = reader.read(cArr);
            if (read == -1) {
                return j;
            }
            writer.write(cArr, 0, read);
            j += (long) read;
        }
    }

    public static String a(Readable readable) throws IOException {
        return b(readable).toString();
    }

    public static <T> T a(Readable readable, LineProcessor<T> lineProcessor) throws IOException {
        String a2;
        Preconditions.a(readable);
        Preconditions.a(lineProcessor);
        LineReader lineReader = new LineReader(readable);
        do {
            a2 = lineReader.a();
            if (a2 == null || !lineProcessor.a(a2)) {
            }
            a2 = lineReader.a();
            break;
        } while (!lineProcessor.a(a2));
        return lineProcessor.getResult();
    }

    public static Writer a(Appendable appendable) {
        if (appendable instanceof Writer) {
            return (Writer) appendable;
        }
        return new AppendableWriter(appendable);
    }
}
