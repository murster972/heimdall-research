package com.google.common.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

public final class Resources {

    private static final class UrlByteSource extends ByteSource {

        /* renamed from: a  reason: collision with root package name */
        private final URL f4383a;

        public InputStream a() throws IOException {
            return this.f4383a.openStream();
        }

        public String toString() {
            String valueOf = String.valueOf(this.f4383a);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
            sb.append("Resources.asByteSource(");
            sb.append(valueOf);
            sb.append(")");
            return sb.toString();
        }

        private UrlByteSource(URL url) {
            Preconditions.a(url);
            this.f4383a = url;
        }
    }

    private Resources() {
    }

    public static ByteSource asByteSource(URL url) {
        return new UrlByteSource(url);
    }

    public static CharSource asCharSource(URL url, Charset charset) {
        return asByteSource(url).a(charset);
    }

    public static void copy(URL url, OutputStream outputStream) throws IOException {
        asByteSource(url).a(outputStream);
    }

    public static URL getResource(String str) {
        URL resource = ((ClassLoader) MoreObjects.a(Thread.currentThread().getContextClassLoader(), Resources.class.getClassLoader())).getResource(str);
        Preconditions.a(resource != null, "resource %s not found.", (Object) str);
        return resource;
    }

    public static <T> T readLines(URL url, Charset charset, LineProcessor<T> lineProcessor) throws IOException {
        return asCharSource(url, charset).a(lineProcessor);
    }

    public static byte[] toByteArray(URL url) throws IOException {
        return asByteSource(url).b();
    }

    public static String toString(URL url, Charset charset) throws IOException {
        return asCharSource(url, charset).b();
    }

    public static List<String> readLines(URL url, Charset charset) throws IOException {
        return (List) readLines(url, charset, new LineProcessor<List<String>>() {

            /* renamed from: a  reason: collision with root package name */
            final List<String> f4382a = Lists.a();

            public boolean a(String str) {
                this.f4382a.add(str);
                return true;
            }

            public List<String> getResult() {
                return this.f4382a;
            }
        });
    }

    public static URL getResource(Class<?> cls, String str) {
        URL resource = cls.getResource(str);
        Preconditions.a(resource != null, "resource %s relative to %s not found.", str, cls.getName());
        return resource;
    }
}
