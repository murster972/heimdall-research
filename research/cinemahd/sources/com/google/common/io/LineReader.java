package com.google.common.io;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.LinkedList;
import java.util.Queue;

public final class LineReader {

    /* renamed from: a  reason: collision with root package name */
    private final Readable f4381a;
    private final Reader b;
    private final CharBuffer c = CharStreams.a();
    private final char[] d = this.c.array();
    /* access modifiers changed from: private */
    public final Queue<String> e = new LinkedList();
    private final LineBuffer f = new LineBuffer() {
        /* access modifiers changed from: protected */
        public void a(String str, String str2) {
            LineReader.this.e.add(str);
        }
    };

    public LineReader(Readable readable) {
        Preconditions.a(readable);
        this.f4381a = readable;
        this.b = readable instanceof Reader ? (Reader) readable : null;
    }

    public String a() throws IOException {
        int i;
        while (true) {
            if (this.e.peek() != null) {
                break;
            }
            Java8Compatibility.a(this.c);
            Reader reader = this.b;
            if (reader != null) {
                char[] cArr = this.d;
                i = reader.read(cArr, 0, cArr.length);
            } else {
                i = this.f4381a.read(this.c);
            }
            if (i == -1) {
                this.f.a();
                break;
            }
            this.f.a(this.d, 0, i);
        }
        return this.e.poll();
    }
}
