package com.google.common.io;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.Reader;

public abstract class CharSource {
    protected CharSource() {
    }

    public abstract Reader a() throws IOException;

    public <T> T a(LineProcessor<T> lineProcessor) throws IOException {
        Preconditions.a(lineProcessor);
        Closer s = Closer.s();
        try {
            Reader a2 = a();
            s.a(a2);
            T a3 = CharStreams.a((Readable) a2, lineProcessor);
            s.close();
            return a3;
        } catch (Throwable th) {
            s.close();
            throw th;
        }
    }

    public String b() throws IOException {
        Closer s = Closer.s();
        try {
            Reader a2 = a();
            s.a(a2);
            String a3 = CharStreams.a((Readable) a2);
            s.close();
            return a3;
        } catch (Throwable th) {
            s.close();
            throw th;
        }
    }
}
