package com.google.common.io;

import com.google.common.base.Preconditions;
import com.google.common.math.IntMath;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public final class ByteStreams {
    static {
        new OutputStream() {
            public String toString() {
                return "ByteStreams.nullOutputStream()";
            }

            public void write(int i) {
            }

            public void write(byte[] bArr) {
                Preconditions.a(bArr);
            }

            public void write(byte[] bArr, int i, int i2) {
                Preconditions.a(bArr);
            }
        };
    }

    private ByteStreams() {
    }

    public static long a(InputStream inputStream, OutputStream outputStream) throws IOException {
        Preconditions.a(inputStream);
        Preconditions.a(outputStream);
        byte[] a2 = a();
        long j = 0;
        while (true) {
            int read = inputStream.read(a2);
            if (read == -1) {
                return j;
            }
            outputStream.write(a2, 0, read);
            j += (long) read;
        }
    }

    static byte[] a() {
        return new byte[8192];
    }

    private static byte[] a(InputStream inputStream, Queue<byte[]> queue, int i) throws IOException {
        int i2 = 8192;
        while (i < 2147483639) {
            byte[] bArr = new byte[Math.min(i2, 2147483639 - i)];
            queue.add(bArr);
            int i3 = 0;
            while (i3 < bArr.length) {
                int read = inputStream.read(bArr, i3, bArr.length - i3);
                if (read == -1) {
                    return a(queue, i);
                }
                i3 += read;
                i += read;
            }
            i2 = IntMath.a(i2, 2);
        }
        if (inputStream.read() == -1) {
            return a(queue, 2147483639);
        }
        throw new OutOfMemoryError("input is too large to fit in a byte array");
    }

    private static byte[] a(Queue<byte[]> queue, int i) {
        byte[] bArr = new byte[i];
        int i2 = i;
        while (i2 > 0) {
            byte[] remove = queue.remove();
            int min = Math.min(i2, remove.length);
            System.arraycopy(remove, 0, bArr, i - i2, min);
            i2 -= min;
        }
        return bArr;
    }

    public static byte[] a(InputStream inputStream) throws IOException {
        Preconditions.a(inputStream);
        return a(inputStream, new ArrayDeque(20), 0);
    }

    static byte[] a(InputStream inputStream, long j) throws IOException {
        Preconditions.a(j >= 0, "expectedSize (%s) must be non-negative", j);
        if (j <= 2147483639) {
            int i = (int) j;
            byte[] bArr = new byte[i];
            int i2 = i;
            while (i2 > 0) {
                int i3 = i - i2;
                int read = inputStream.read(bArr, i3, i2);
                if (read == -1) {
                    return Arrays.copyOf(bArr, i3);
                }
                i2 -= read;
            }
            int read2 = inputStream.read();
            if (read2 == -1) {
                return bArr;
            }
            ArrayDeque arrayDeque = new ArrayDeque(22);
            arrayDeque.add(bArr);
            arrayDeque.add(new byte[]{(byte) read2});
            return a(inputStream, arrayDeque, bArr.length + 1);
        }
        StringBuilder sb = new StringBuilder(62);
        sb.append(j);
        sb.append(" bytes is too large to fit in a byte array");
        throw new OutOfMemoryError(sb.toString());
    }
}
