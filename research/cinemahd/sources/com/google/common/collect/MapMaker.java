package com.google.common.collect;

import com.google.common.base.Ascii;
import com.google.common.base.Equivalence;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.MapMakerInternalMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class MapMaker {

    /* renamed from: a  reason: collision with root package name */
    boolean f4358a;
    int b = -1;
    int c = -1;
    MapMakerInternalMap.Strength d;
    MapMakerInternalMap.Strength e;
    Equivalence<Object> f;

    /* access modifiers changed from: package-private */
    public MapMaker a(Equivalence<Object> equivalence) {
        Preconditions.b(this.f == null, "key equivalence was already set to %s", (Object) this.f);
        Preconditions.a(equivalence);
        this.f = equivalence;
        this.f4358a = true;
        return this;
    }

    public MapMaker b(int i) {
        boolean z = true;
        Preconditions.a(this.b == -1, "initial capacity was already set to %s", this.b);
        if (i < 0) {
            z = false;
        }
        Preconditions.a(z);
        this.b = i;
        return this;
    }

    /* access modifiers changed from: package-private */
    public Equivalence<Object> c() {
        return (Equivalence) MoreObjects.a(this.f, d().a());
    }

    /* access modifiers changed from: package-private */
    public MapMakerInternalMap.Strength d() {
        return (MapMakerInternalMap.Strength) MoreObjects.a(this.d, MapMakerInternalMap.Strength.STRONG);
    }

    /* access modifiers changed from: package-private */
    public MapMakerInternalMap.Strength e() {
        return (MapMakerInternalMap.Strength) MoreObjects.a(this.e, MapMakerInternalMap.Strength.STRONG);
    }

    public <K, V> ConcurrentMap<K, V> f() {
        if (!this.f4358a) {
            return new ConcurrentHashMap(b(), 0.75f, a());
        }
        return MapMakerInternalMap.a(this);
    }

    public MapMaker g() {
        return a(MapMakerInternalMap.Strength.WEAK);
    }

    public String toString() {
        MoreObjects.ToStringHelper a2 = MoreObjects.a(this);
        int i = this.b;
        if (i != -1) {
            a2.a("initialCapacity", i);
        }
        int i2 = this.c;
        if (i2 != -1) {
            a2.a("concurrencyLevel", i2);
        }
        MapMakerInternalMap.Strength strength = this.d;
        if (strength != null) {
            a2.a("keyStrength", (Object) Ascii.a(strength.toString()));
        }
        MapMakerInternalMap.Strength strength2 = this.e;
        if (strength2 != null) {
            a2.a("valueStrength", (Object) Ascii.a(strength2.toString()));
        }
        if (this.f != null) {
            a2.a("keyEquivalence");
        }
        return a2.toString();
    }

    public MapMaker a(int i) {
        boolean z = true;
        Preconditions.a(this.c == -1, "concurrency level was already set to %s", this.c);
        if (i <= 0) {
            z = false;
        }
        Preconditions.a(z);
        this.c = i;
        return this;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        int i = this.b;
        if (i == -1) {
            return 16;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public MapMaker b(MapMakerInternalMap.Strength strength) {
        Preconditions.b(this.e == null, "Value strength was already set to %s", (Object) this.e);
        Preconditions.a(strength);
        this.e = strength;
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.f4358a = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i = this.c;
        if (i == -1) {
            return 4;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public MapMaker a(MapMakerInternalMap.Strength strength) {
        Preconditions.b(this.d == null, "Key strength was already set to %s", (Object) this.d);
        Preconditions.a(strength);
        this.d = strength;
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.f4358a = true;
        }
        return this;
    }
}
