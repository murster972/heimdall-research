package com.google.common.collect;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;

public final class Lists {

    private static class RandomAccessReverseList<T> extends ReverseList<T> implements RandomAccess {
        RandomAccessReverseList(List<T> list) {
            super(list);
        }
    }

    private static class ReverseList<T> extends AbstractList<T> {

        /* renamed from: a  reason: collision with root package name */
        private final List<T> f4356a;

        ReverseList(List<T> list) {
            Preconditions.a(list);
            this.f4356a = list;
        }

        /* access modifiers changed from: private */
        public int b(int i) {
            int size = size();
            Preconditions.b(i, size);
            return size - i;
        }

        public void add(int i, T t) {
            this.f4356a.add(b(i), t);
        }

        public void clear() {
            this.f4356a.clear();
        }

        public T get(int i) {
            return this.f4356a.get(a(i));
        }

        public Iterator<T> iterator() {
            return listIterator();
        }

        public ListIterator<T> listIterator(int i) {
            final ListIterator<T> listIterator = this.f4356a.listIterator(b(i));
            return new ListIterator<T>() {

                /* renamed from: a  reason: collision with root package name */
                boolean f4357a;

                public void add(T t) {
                    listIterator.add(t);
                    listIterator.previous();
                    this.f4357a = false;
                }

                public boolean hasNext() {
                    return listIterator.hasPrevious();
                }

                public boolean hasPrevious() {
                    return listIterator.hasNext();
                }

                public T next() {
                    if (hasNext()) {
                        this.f4357a = true;
                        return listIterator.previous();
                    }
                    throw new NoSuchElementException();
                }

                public int nextIndex() {
                    return ReverseList.this.b(listIterator.nextIndex());
                }

                public T previous() {
                    if (hasPrevious()) {
                        this.f4357a = true;
                        return listIterator.next();
                    }
                    throw new NoSuchElementException();
                }

                public int previousIndex() {
                    return nextIndex() - 1;
                }

                public void remove() {
                    CollectPreconditions.a(this.f4357a);
                    listIterator.remove();
                    this.f4357a = false;
                }

                public void set(T t) {
                    Preconditions.b(this.f4357a);
                    listIterator.set(t);
                }
            };
        }

        public T remove(int i) {
            return this.f4356a.remove(a(i));
        }

        /* access modifiers changed from: protected */
        public void removeRange(int i, int i2) {
            subList(i, i2).clear();
        }

        public T set(int i, T t) {
            return this.f4356a.set(a(i), t);
        }

        public int size() {
            return this.f4356a.size();
        }

        public List<T> subList(int i, int i2) {
            Preconditions.b(i, i2, size());
            return Lists.reverse(this.f4356a.subList(b(i2), b(i)));
        }

        /* access modifiers changed from: package-private */
        public List<T> a() {
            return this.f4356a;
        }

        private int a(int i) {
            int size = size();
            Preconditions.a(i, size);
            return (size - 1) - i;
        }
    }

    private Lists() {
    }

    public static <E> ArrayList<E> a() {
        return new ArrayList<>();
    }

    static int b(List<?> list, Object obj) {
        if (list instanceof RandomAccess) {
            return c(list, obj);
        }
        ListIterator<?> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (Objects.a(obj, listIterator.next())) {
                return listIterator.previousIndex();
            }
        }
        return -1;
    }

    private static int c(List<?> list, Object obj) {
        int size = list.size();
        int i = 0;
        if (obj == null) {
            while (i < size) {
                if (list.get(i) == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        while (i < size) {
            if (obj.equals(list.get(i))) {
                return i;
            }
            i++;
        }
        return -1;
    }

    static int d(List<?> list, Object obj) {
        if (list instanceof RandomAccess) {
            return e(list, obj);
        }
        ListIterator<?> listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            if (Objects.a(obj, listIterator.previous())) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    private static int e(List<?> list, Object obj) {
        if (obj == null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                if (list.get(size) == null) {
                    return size;
                }
            }
            return -1;
        }
        for (int size2 = list.size() - 1; size2 >= 0; size2--) {
            if (obj.equals(list.get(size2))) {
                return size2;
            }
        }
        return -1;
    }

    public static <T> List<T> reverse(List<T> list) {
        if (list instanceof ImmutableList) {
            return ((ImmutableList) list).d();
        }
        if (list instanceof ReverseList) {
            return ((ReverseList) list).a();
        }
        if (list instanceof RandomAccess) {
            return new RandomAccessReverseList(list);
        }
        return new ReverseList(list);
    }

    public static <E> ArrayList<E> a(int i) {
        CollectPreconditions.a(i, "initialArraySize");
        return new ArrayList<>(i);
    }

    static boolean a(List<?> list, Object obj) {
        Preconditions.a(list);
        if (obj == list) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        List list2 = (List) obj;
        int size = list.size();
        if (size != list2.size()) {
            return false;
        }
        if (!(list instanceof RandomAccess) || !(list2 instanceof RandomAccess)) {
            return Iterators.a(list.iterator(), (Iterator<?>) list2.iterator());
        }
        for (int i = 0; i < size; i++) {
            if (!Objects.a(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }
}
