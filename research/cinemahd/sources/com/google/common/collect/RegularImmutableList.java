package com.google.common.collect;

import com.google.common.base.Preconditions;

class RegularImmutableList<E> extends ImmutableList<E> {
    static final ImmutableList<Object> e = new RegularImmutableList(new Object[0], 0);
    final transient Object[] c;
    private final transient int d;

    RegularImmutableList(Object[] objArr, int i) {
        this.c = objArr;
        this.d = i;
    }

    /* access modifiers changed from: package-private */
    public Object[] a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return 0;
    }

    public E get(int i) {
        Preconditions.a(i, this.d);
        return this.c[i];
    }

    public int size() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int a(Object[] objArr, int i) {
        System.arraycopy(this.c, 0, objArr, i, this.d);
        return i + this.d;
    }
}
