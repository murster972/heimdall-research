package com.google.common.collect;

import java.util.concurrent.ConcurrentMap;

public abstract class ForwardingConcurrentMap<K, V> extends ForwardingMap<K, V> implements ConcurrentMap<K, V> {
    protected ForwardingConcurrentMap() {
    }

    /* access modifiers changed from: protected */
    public abstract ConcurrentMap<K, V> a();

    public V putIfAbsent(K k, V v) {
        return a().putIfAbsent(k, v);
    }

    public boolean remove(Object obj, Object obj2) {
        return a().remove(obj, obj2);
    }

    public V replace(K k, V v) {
        return a().replace(k, v);
    }

    public boolean replace(K k, V v, V v2) {
        return a().replace(k, v, v2);
    }
}
