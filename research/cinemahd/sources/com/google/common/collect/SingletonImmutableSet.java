package com.google.common.collect;

import com.google.common.base.Preconditions;

final class SingletonImmutableSet<E> extends ImmutableSet<E> {
    final transient E c;
    private transient int d;

    SingletonImmutableSet(E e) {
        Preconditions.a(e);
        this.c = e;
    }

    /* access modifiers changed from: package-private */
    public int a(Object[] objArr, int i) {
        objArr[i] = this.c;
        return i + 1;
    }

    public boolean contains(Object obj) {
        return this.c.equals(obj);
    }

    /* access modifiers changed from: package-private */
    public ImmutableList<E> e() {
        return ImmutableList.a(this.c);
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.d != 0;
    }

    public final int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = this.c.hashCode();
        this.d = hashCode;
        return hashCode;
    }

    public int size() {
        return 1;
    }

    public String toString() {
        String obj = this.c.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 2);
        sb.append('[');
        sb.append(obj);
        sb.append(']');
        return sb.toString();
    }

    public UnmodifiableIterator<E> iterator() {
        return Iterators.a(this.c);
    }

    SingletonImmutableSet(E e, int i) {
        this.c = e;
        this.d = i;
    }
}
