package com.google.common.collect;

import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

public abstract class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E> {
    private transient ImmutableList<E> b;

    private static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        SerializedForm(Object[] objArr) {
            this.elements = objArr;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return ImmutableSet.a((E[]) this.elements);
        }
    }

    ImmutableSet() {
    }

    public static <E> ImmutableSet<E> a(E e) {
        return new SingletonImmutableSet(e);
    }

    private static boolean a(int i, int i2) {
        return i < (i2 >> 1) + (i2 >> 2);
    }

    public static <E> ImmutableSet<E> g() {
        return RegularImmutableSet.h;
    }

    public ImmutableList<E> d() {
        ImmutableList<E> immutableList = this.b;
        if (immutableList != null) {
            return immutableList;
        }
        ImmutableList<E> e = e();
        this.b = e;
        return e;
    }

    /* access modifiers changed from: package-private */
    public ImmutableList<E> e() {
        return ImmutableList.a(toArray());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ImmutableSet) || !f() || !((ImmutableSet) obj).f() || hashCode() == obj.hashCode()) {
            return Sets.a(this, obj);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return false;
    }

    public int hashCode() {
        return Sets.a(this);
    }

    public /* bridge */ /* synthetic */ Iterator iterator() {
        return iterator();
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    public static <E> ImmutableSet<E> a(E e, E e2, E e3) {
        return a(3, e, e2, e3);
    }

    private static <E> ImmutableSet<E> a(int i, Object... objArr) {
        if (i == 0) {
            return g();
        }
        if (i == 1) {
            return a(objArr[0]);
        }
        int a2 = a(i);
        Object[] objArr2 = new Object[a2];
        int i2 = a2 - 1;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i; i5++) {
            Object obj = objArr[i5];
            ObjectArrays.a(obj, i5);
            int hashCode = obj.hashCode();
            int a3 = Hashing.a(hashCode);
            while (true) {
                int i6 = a3 & i2;
                Object obj2 = objArr2[i6];
                if (obj2 == null) {
                    objArr[i4] = obj;
                    objArr2[i6] = obj;
                    i3 += hashCode;
                    i4++;
                    break;
                } else if (obj2.equals(obj)) {
                    break;
                } else {
                    a3++;
                }
            }
        }
        Arrays.fill(objArr, i4, i, (Object) null);
        if (i4 == 1) {
            return new SingletonImmutableSet(objArr[0], i3);
        }
        if (a(i4) < a2 / 2) {
            return a(i4, objArr);
        }
        if (a(i4, objArr.length)) {
            objArr = Arrays.copyOf(objArr, i4);
        }
        return new RegularImmutableSet(objArr, i3, objArr2, i2, i4);
    }

    static int a(int i) {
        int max = Math.max(i, 2);
        boolean z = true;
        if (max < 751619276) {
            int highestOneBit = Integer.highestOneBit(max - 1) << 1;
            while (((double) highestOneBit) * 0.7d < ((double) max)) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        if (max >= 1073741824) {
            z = false;
        }
        Preconditions.a(z, (Object) "collection too large");
        return 1073741824;
    }

    public static <E> ImmutableSet<E> a(E[] eArr) {
        int length = eArr.length;
        if (length == 0) {
            return g();
        }
        if (length != 1) {
            return a(eArr.length, (Object[]) eArr.clone());
        }
        return a(eArr[0]);
    }
}
