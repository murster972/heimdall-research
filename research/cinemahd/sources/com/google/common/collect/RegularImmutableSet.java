package com.google.common.collect;

final class RegularImmutableSet<E> extends ImmutableSet<E> {
    static final RegularImmutableSet<Object> h = new RegularImmutableSet(new Object[0], 0, (Object[]) null, 0, 0);
    final transient Object[] c;
    final transient Object[] d;
    private final transient int e;
    private final transient int f;
    private final transient int g;

    RegularImmutableSet(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.c = objArr;
        this.d = objArr2;
        this.e = i2;
        this.f = i;
        this.g = i3;
    }

    /* access modifiers changed from: package-private */
    public Object[] a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return 0;
    }

    public boolean contains(Object obj) {
        Object[] objArr = this.d;
        if (obj == null || objArr == null) {
            return false;
        }
        int a2 = Hashing.a(obj);
        while (true) {
            int i = a2 & this.e;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            a2 = i + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public ImmutableList<E> e() {
        return ImmutableList.b(this.c, this.g);
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return true;
    }

    public int hashCode() {
        return this.f;
    }

    public int size() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public int a(Object[] objArr, int i) {
        System.arraycopy(this.c, 0, objArr, i, this.g);
        return i + this.g;
    }

    public UnmodifiableIterator<E> iterator() {
        return d().iterator();
    }
}
