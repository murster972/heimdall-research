package com.google.common.collect;

import com.google.common.base.Preconditions;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public abstract class ImmutableList<E> extends ImmutableCollection<E> implements List<E>, RandomAccess {
    private static final UnmodifiableListIterator<Object> b = new Itr(RegularImmutableList.e, 0);

    static class Itr<E> extends AbstractIndexedListIterator<E> {
        private final ImmutableList<E> c;

        Itr(ImmutableList<E> immutableList, int i) {
            super(immutableList.size(), i);
            this.c = immutableList;
        }

        /* access modifiers changed from: protected */
        public E a(int i) {
            return this.c.get(i);
        }
    }

    private static class ReverseImmutableList<E> extends ImmutableList<E> {
        private final transient ImmutableList<E> c;

        ReverseImmutableList(ImmutableList<E> immutableList) {
            this.c = immutableList;
        }

        private int a(int i) {
            return (size() - 1) - i;
        }

        private int b(int i) {
            return size() - i;
        }

        public boolean contains(Object obj) {
            return this.c.contains(obj);
        }

        public ImmutableList<E> d() {
            return this.c;
        }

        public E get(int i) {
            Preconditions.a(i, size());
            return this.c.get(a(i));
        }

        public int indexOf(Object obj) {
            int lastIndexOf = this.c.lastIndexOf(obj);
            if (lastIndexOf >= 0) {
                return a(lastIndexOf);
            }
            return -1;
        }

        public /* bridge */ /* synthetic */ Iterator iterator() {
            return ImmutableList.super.iterator();
        }

        public int lastIndexOf(Object obj) {
            int indexOf = this.c.indexOf(obj);
            if (indexOf >= 0) {
                return a(indexOf);
            }
            return -1;
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return ImmutableList.super.listIterator();
        }

        public int size() {
            return this.c.size();
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return ImmutableList.super.listIterator(i);
        }

        public ImmutableList<E> subList(int i, int i2) {
            Preconditions.b(i, i2, size());
            return this.c.subList(b(i2), b(i)).d();
        }
    }

    static class SerializedForm implements Serializable {
        private static final long serialVersionUID = 0;
        final Object[] elements;

        SerializedForm(Object[] objArr) {
            this.elements = objArr;
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return ImmutableList.c(this.elements);
        }
    }

    class SubList extends ImmutableList<E> {
        final transient int c;
        final transient int d;

        SubList(int i, int i2) {
            this.c = i;
            this.d = i2;
        }

        /* access modifiers changed from: package-private */
        public Object[] a() {
            return ImmutableList.this.a();
        }

        /* access modifiers changed from: package-private */
        public int b() {
            return ImmutableList.this.c() + this.c + this.d;
        }

        /* access modifiers changed from: package-private */
        public int c() {
            return ImmutableList.this.c() + this.c;
        }

        public E get(int i) {
            Preconditions.a(i, this.d);
            return ImmutableList.this.get(i + this.c);
        }

        public /* bridge */ /* synthetic */ Iterator iterator() {
            return ImmutableList.super.iterator();
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return ImmutableList.super.listIterator();
        }

        public int size() {
            return this.d;
        }

        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return ImmutableList.super.listIterator(i);
        }

        public ImmutableList<E> subList(int i, int i2) {
            Preconditions.b(i, i2, this.d);
            ImmutableList immutableList = ImmutableList.this;
            int i3 = this.c;
            return immutableList.subList(i + i3, i2 + i3);
        }
    }

    ImmutableList() {
    }

    public static <E> ImmutableList<E> a(E e) {
        return b(e);
    }

    private static <E> ImmutableList<E> b(Object... objArr) {
        ObjectArrays.a(objArr);
        return a(objArr);
    }

    public static <E> ImmutableList<E> c(E[] eArr) {
        if (eArr.length == 0) {
            return e();
        }
        return b((Object[]) eArr.clone());
    }

    public static <E> ImmutableList<E> e() {
        return RegularImmutableList.e;
    }

    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @Deprecated
    public final void add(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    public ImmutableList<E> d() {
        return size() <= 1 ? this : new ReverseImmutableList(this);
    }

    public boolean equals(Object obj) {
        return Lists.a(this, obj);
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = ~(~((i * 31) + get(i2).hashCode()));
        }
        return i;
    }

    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return Lists.b(this, obj);
    }

    public int lastIndexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return Lists.d(this, obj);
    }

    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(toArray());
    }

    static <E> ImmutableList<E> a(Object[] objArr) {
        return b(objArr, objArr.length);
    }

    static <E> ImmutableList<E> b(Object[] objArr, int i) {
        if (i == 0) {
            return e();
        }
        return new RegularImmutableList(objArr, i);
    }

    public UnmodifiableIterator<E> iterator() {
        return listIterator();
    }

    public ImmutableList<E> subList(int i, int i2) {
        Preconditions.b(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        if (i3 == 0) {
            return e();
        }
        return a(i, i2);
    }

    /* access modifiers changed from: package-private */
    public ImmutableList<E> a(int i, int i2) {
        return new SubList(i, i2 - i);
    }

    public UnmodifiableListIterator<E> listIterator() {
        return listIterator(0);
    }

    /* access modifiers changed from: package-private */
    public int a(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    public UnmodifiableListIterator<E> listIterator(int i) {
        Preconditions.b(i, size());
        if (isEmpty()) {
            return b;
        }
        return new Itr(this, i);
    }
}
