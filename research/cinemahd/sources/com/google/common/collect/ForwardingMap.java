package com.google.common.collect;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public abstract class ForwardingMap<K, V> extends ForwardingObject implements Map<K, V> {
    protected ForwardingMap() {
    }

    /* access modifiers changed from: protected */
    public abstract Map<K, V> a();

    public void clear() {
        a().clear();
    }

    public boolean containsKey(Object obj) {
        return a().containsKey(obj);
    }

    public boolean containsValue(Object obj) {
        return a().containsValue(obj);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return a().entrySet();
    }

    public boolean equals(Object obj) {
        return obj == this || a().equals(obj);
    }

    public V get(Object obj) {
        return a().get(obj);
    }

    public int hashCode() {
        return a().hashCode();
    }

    public boolean isEmpty() {
        return a().isEmpty();
    }

    public Set<K> keySet() {
        return a().keySet();
    }

    public V put(K k, V v) {
        return a().put(k, v);
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        a().putAll(map);
    }

    public V remove(Object obj) {
        return a().remove(obj);
    }

    public int size() {
        return a().size();
    }

    public Collection<V> values() {
        return a().values();
    }
}
