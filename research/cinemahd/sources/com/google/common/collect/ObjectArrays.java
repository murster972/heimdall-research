package com.google.common.collect;

public final class ObjectArrays {
    private ObjectArrays() {
    }

    static Object[] a(Object... objArr) {
        a(objArr, objArr.length);
        return objArr;
    }

    public static <T> T[] b(T[] tArr, int i) {
        return Platform.a(tArr, i);
    }

    static Object[] a(Object[] objArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            a(objArr[i2], i2);
        }
        return objArr;
    }

    static Object a(Object obj, int i) {
        if (obj != null) {
            return obj;
        }
        StringBuilder sb = new StringBuilder(20);
        sb.append("at index ");
        sb.append(i);
        throw new NullPointerException(sb.toString());
    }
}
