package com.google.common.collect;

import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class Iterators {
    private Iterators() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0006  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.util.Iterator<?> r3, java.util.Iterator<?> r4) {
        /*
        L_0x0000:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x001d
            boolean r0 = r4.hasNext()
            r1 = 0
            if (r0 != 0) goto L_0x000e
            return r1
        L_0x000e:
            java.lang.Object r0 = r3.next()
            java.lang.Object r2 = r4.next()
            boolean r0 = com.google.common.base.Objects.a(r0, r2)
            if (r0 != 0) goto L_0x0000
            return r1
        L_0x001d:
            boolean r3 = r4.hasNext()
            r3 = r3 ^ 1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Iterators.a(java.util.Iterator, java.util.Iterator):boolean");
    }

    public static <T> boolean a(Collection<T> collection, Iterator<? extends T> it2) {
        Preconditions.a(collection);
        Preconditions.a(it2);
        boolean z = false;
        while (it2.hasNext()) {
            z |= collection.add(it2.next());
        }
        return z;
    }

    public static <T> UnmodifiableIterator<T> a(final T t) {
        return new UnmodifiableIterator<T>() {

            /* renamed from: a  reason: collision with root package name */
            boolean f4355a;

            public boolean hasNext() {
                return !this.f4355a;
            }

            public T next() {
                if (!this.f4355a) {
                    this.f4355a = true;
                    return t;
                }
                throw new NoSuchElementException();
            }
        };
    }
}
