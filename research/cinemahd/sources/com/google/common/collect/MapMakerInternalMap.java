package com.google.common.collect;

import com.google.ar.core.ImageMetadata;
import com.google.common.base.Equivalence;
import com.google.common.base.Preconditions;
import com.google.common.collect.MapMakerInternalMap.InternalEntry;
import com.google.common.collect.MapMakerInternalMap.Segment;
import com.google.common.primitives.Ints;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;

class MapMakerInternalMap<K, V, E extends InternalEntry<K, V, E>, S extends Segment<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    static final WeakValueReference<Object, Object, DummyInternalEntry> h = new WeakValueReference<Object, Object, DummyInternalEntry>() {
        public DummyInternalEntry a() {
            return null;
        }

        public WeakValueReference<Object, Object, DummyInternalEntry> a(ReferenceQueue<Object> referenceQueue, DummyInternalEntry dummyInternalEntry) {
            return this;
        }

        public void clear() {
        }

        public Object get() {
            return null;
        }
    };
    private static final long serialVersionUID = 5;

    /* renamed from: a  reason: collision with root package name */
    final transient int f4359a;
    final transient int b;
    final transient Segment<K, V, E, S>[] c;
    final int concurrencyLevel;
    final transient InternalEntryHelper<K, V, E, S> d;
    transient Set<K> e;
    transient Collection<V> f;
    transient Set<Map.Entry<K, V>> g;
    final Equivalence<Object> keyEquivalence;

    static abstract class AbstractStrongKeyEntry<K, V, E extends InternalEntry<K, V, E>> implements InternalEntry<K, V, E> {

        /* renamed from: a  reason: collision with root package name */
        final K f4361a;
        final int b;
        final E c;

        AbstractStrongKeyEntry(K k, int i, E e) {
            this.f4361a = k;
            this.b = i;
            this.c = e;
        }

        public int a() {
            return this.b;
        }

        public E b() {
            return this.c;
        }

        public K getKey() {
            return this.f4361a;
        }
    }

    static abstract class AbstractWeakKeyEntry<K, V, E extends InternalEntry<K, V, E>> extends WeakReference<K> implements InternalEntry<K, V, E> {

        /* renamed from: a  reason: collision with root package name */
        final int f4362a;
        final E b;

        AbstractWeakKeyEntry(ReferenceQueue<K> referenceQueue, K k, int i, E e) {
            super(k, referenceQueue);
            this.f4362a = i;
            this.b = e;
        }

        public int a() {
            return this.f4362a;
        }

        public E b() {
            return this.b;
        }

        public K getKey() {
            return get();
        }
    }

    static final class DummyInternalEntry implements InternalEntry<Object, Object, DummyInternalEntry> {
        private DummyInternalEntry() {
            throw new AssertionError();
        }

        public int a() {
            throw new AssertionError();
        }

        public Object getKey() {
            throw new AssertionError();
        }

        public Object getValue() {
            throw new AssertionError();
        }

        public DummyInternalEntry b() {
            throw new AssertionError();
        }
    }

    final class EntryIterator extends MapMakerInternalMap<K, V, E, S>.HashIterator<Map.Entry<K, V>> {
        EntryIterator(MapMakerInternalMap mapMakerInternalMap) {
            super();
        }

        public Map.Entry<K, V> next() {
            return b();
        }
    }

    final class EntrySet extends SafeToArraySet<Map.Entry<K, V>> {
        EntrySet() {
            super();
        }

        public void clear() {
            MapMakerInternalMap.this.clear();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
            r4 = (java.util.Map.Entry) r4;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean contains(java.lang.Object r4) {
            /*
                r3 = this;
                boolean r0 = r4 instanceof java.util.Map.Entry
                r1 = 0
                if (r0 != 0) goto L_0x0006
                return r1
            L_0x0006:
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                java.lang.Object r0 = r4.getKey()
                if (r0 != 0) goto L_0x000f
                return r1
            L_0x000f:
                com.google.common.collect.MapMakerInternalMap r2 = com.google.common.collect.MapMakerInternalMap.this
                java.lang.Object r0 = r2.get(r0)
                if (r0 == 0) goto L_0x0028
                com.google.common.collect.MapMakerInternalMap r2 = com.google.common.collect.MapMakerInternalMap.this
                com.google.common.base.Equivalence r2 = r2.a()
                java.lang.Object r4 = r4.getValue()
                boolean r4 = r2.b(r4, r0)
                if (r4 == 0) goto L_0x0028
                r1 = 1
            L_0x0028:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.MapMakerInternalMap.EntrySet.contains(java.lang.Object):boolean");
        }

        public boolean isEmpty() {
            return MapMakerInternalMap.this.isEmpty();
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new EntryIterator(MapMakerInternalMap.this);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
            r4 = (java.util.Map.Entry) r4;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean remove(java.lang.Object r4) {
            /*
                r3 = this;
                boolean r0 = r4 instanceof java.util.Map.Entry
                r1 = 0
                if (r0 != 0) goto L_0x0006
                return r1
            L_0x0006:
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                java.lang.Object r0 = r4.getKey()
                if (r0 == 0) goto L_0x001b
                com.google.common.collect.MapMakerInternalMap r2 = com.google.common.collect.MapMakerInternalMap.this
                java.lang.Object r4 = r4.getValue()
                boolean r4 = r2.remove(r0, r4)
                if (r4 == 0) goto L_0x001b
                r1 = 1
            L_0x001b:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.MapMakerInternalMap.EntrySet.remove(java.lang.Object):boolean");
        }

        public int size() {
            return MapMakerInternalMap.this.size();
        }
    }

    interface InternalEntry<K, V, E extends InternalEntry<K, V, E>> {
        int a();

        E b();

        K getKey();

        V getValue();
    }

    interface InternalEntryHelper<K, V, E extends InternalEntry<K, V, E>, S extends Segment<K, V, E, S>> {
        E a(S s, E e, E e2);

        E a(S s, K k, int i, E e);

        S a(MapMakerInternalMap<K, V, E, S> mapMakerInternalMap, int i, int i2);

        Strength a();

        void a(S s, E e, V v);

        Strength b();
    }

    final class KeyIterator extends MapMakerInternalMap<K, V, E, S>.HashIterator<K> {
        KeyIterator(MapMakerInternalMap mapMakerInternalMap) {
            super();
        }

        public K next() {
            return b().getKey();
        }
    }

    final class KeySet extends SafeToArraySet<K> {
        KeySet() {
            super();
        }

        public void clear() {
            MapMakerInternalMap.this.clear();
        }

        public boolean contains(Object obj) {
            return MapMakerInternalMap.this.containsKey(obj);
        }

        public boolean isEmpty() {
            return MapMakerInternalMap.this.isEmpty();
        }

        public Iterator<K> iterator() {
            return new KeyIterator(MapMakerInternalMap.this);
        }

        public boolean remove(Object obj) {
            return MapMakerInternalMap.this.remove(obj) != null;
        }

        public int size() {
            return MapMakerInternalMap.this.size();
        }
    }

    private static abstract class SafeToArraySet<E> extends AbstractSet<E> {
        private SafeToArraySet() {
        }

        public Object[] toArray() {
            return MapMakerInternalMap.b(this).toArray();
        }

        public <T> T[] toArray(T[] tArr) {
            return MapMakerInternalMap.b(this).toArray(tArr);
        }
    }

    static abstract class Segment<K, V, E extends InternalEntry<K, V, E>, S extends Segment<K, V, E, S>> extends ReentrantLock {
        volatile int count;
        final MapMakerInternalMap<K, V, E, S> map;
        final int maxSegmentSize;
        int modCount;
        final AtomicInteger readCount = new AtomicInteger();
        volatile AtomicReferenceArray<E> table;
        int threshold;

        Segment(MapMakerInternalMap<K, V, E, S> mapMakerInternalMap, int i, int i2) {
            this.map = mapMakerInternalMap;
            this.maxSegmentSize = i2;
            a(b(i));
        }

        /* access modifiers changed from: package-private */
        public void a(E e, V v) {
            this.map.d.a(i(), e, v);
        }

        /* access modifiers changed from: package-private */
        public AtomicReferenceArray<E> b(int i) {
            return new AtomicReferenceArray<>(i);
        }

        /* access modifiers changed from: package-private */
        public void c() {
        }

        /* access modifiers changed from: package-private */
        public void c(ReferenceQueue<V> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends V> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.a((WeakValueReference) poll);
                    i++;
                } else {
                    return;
                }
            } while (i != 16);
        }

        /* access modifiers changed from: package-private */
        public E d(Object obj, int i) {
            return c(obj, i);
        }

        /* access modifiers changed from: package-private */
        public void d() {
        }

        /* access modifiers changed from: package-private */
        public V e(Object obj, int i) {
            lock();
            try {
                f();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(length);
                InternalEntry internalEntry2 = internalEntry;
                while (internalEntry2 != null) {
                    Object key = internalEntry2.getKey();
                    if (internalEntry2.a() != i || key == null || !this.map.keyEquivalence.b(obj, key)) {
                        internalEntry2 = internalEntry2.b();
                    } else {
                        V value = internalEntry2.getValue();
                        if (value == null) {
                            if (!b(internalEntry2)) {
                                unlock();
                                return null;
                            }
                        }
                        this.modCount++;
                        atomicReferenceArray.set(length, b(internalEntry, internalEntry2));
                        this.count--;
                        return value;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            h();
        }

        /* access modifiers changed from: package-private */
        public void g() {
            h();
        }

        /* access modifiers changed from: package-private */
        public void h() {
            if (tryLock()) {
                try {
                    d();
                    this.readCount.set(0);
                } finally {
                    unlock();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public abstract S i();

        /* access modifiers changed from: package-private */
        public void j() {
            if (tryLock()) {
                try {
                    d();
                } finally {
                    unlock();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public E a(E e, E e2) {
            return this.map.d.a(i(), e, e2);
        }

        /* access modifiers changed from: package-private */
        public void b(ReferenceQueue<K> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends K> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.b((InternalEntry) poll);
                    i++;
                } else {
                    return;
                }
            } while (i != 16);
        }

        /* access modifiers changed from: package-private */
        public void a(AtomicReferenceArray<E> atomicReferenceArray) {
            this.threshold = (atomicReferenceArray.length() * 3) / 4;
            int i = this.threshold;
            if (i == this.maxSegmentSize) {
                this.threshold = i + 1;
            }
            this.table = atomicReferenceArray;
        }

        /* access modifiers changed from: package-private */
        public E c(Object obj, int i) {
            if (this.count == 0) {
                return null;
            }
            for (E a2 = a(i); a2 != null; a2 = a2.b()) {
                if (a2.a() == i) {
                    Object key = a2.getKey();
                    if (key == null) {
                        j();
                    } else if (this.map.keyEquivalence.b(obj, key)) {
                        return a2;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public V b(Object obj, int i) {
            try {
                InternalEntry d = d(obj, i);
                if (d == null) {
                    return null;
                }
                V value = d.getValue();
                if (value == null) {
                    j();
                }
                e();
                return value;
            } finally {
                e();
            }
        }

        /* access modifiers changed from: package-private */
        public <T> void a(ReferenceQueue<T> referenceQueue) {
            do {
            } while (referenceQueue.poll() != null);
        }

        /* access modifiers changed from: package-private */
        public E a(int i) {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            return (InternalEntry) atomicReferenceArray.get(i & (atomicReferenceArray.length() - 1));
        }

        /* access modifiers changed from: package-private */
        public boolean a(Object obj, int i) {
            try {
                boolean z = false;
                if (this.count != 0) {
                    InternalEntry d = d(obj, i);
                    if (!(d == null || d.getValue() == null)) {
                        z = true;
                    }
                    return z;
                }
                e();
                return false;
            } finally {
                e();
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = atomicReferenceArray.length();
            if (length < 1073741824) {
                int i = this.count;
                AtomicReferenceArray<E> b = b(length << 1);
                this.threshold = (b.length() * 3) / 4;
                int length2 = b.length() - 1;
                for (int i2 = 0; i2 < length; i2++) {
                    InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(i2);
                    if (internalEntry != null) {
                        InternalEntry b2 = internalEntry.b();
                        int a2 = internalEntry.a() & length2;
                        if (b2 == null) {
                            b.set(a2, internalEntry);
                        } else {
                            InternalEntry internalEntry2 = internalEntry;
                            while (b2 != null) {
                                int a3 = b2.a() & length2;
                                if (a3 != a2) {
                                    internalEntry2 = b2;
                                    a2 = a3;
                                }
                                b2 = b2.b();
                            }
                            b.set(a2, internalEntry2);
                            while (internalEntry != internalEntry2) {
                                int a4 = internalEntry.a() & length2;
                                InternalEntry a5 = a(internalEntry, (InternalEntry) b.get(a4));
                                if (a5 != null) {
                                    b.set(a4, a5);
                                } else {
                                    i--;
                                }
                                internalEntry = internalEntry.b();
                            }
                        }
                    }
                }
                this.table = b;
                this.count = i;
            }
        }

        /* access modifiers changed from: package-private */
        public V a(K k, int i, V v, boolean z) {
            lock();
            try {
                f();
                int i2 = this.count + 1;
                if (i2 > this.threshold) {
                    b();
                    i2 = this.count + 1;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(length);
                InternalEntry internalEntry2 = internalEntry;
                while (internalEntry2 != null) {
                    Object key = internalEntry2.getKey();
                    if (internalEntry2.a() != i || key == null || !this.map.keyEquivalence.b(k, key)) {
                        internalEntry2 = internalEntry2.b();
                    } else {
                        V value = internalEntry2.getValue();
                        if (value == null) {
                            this.modCount++;
                            a(internalEntry2, v);
                            this.count = this.count;
                            return null;
                        } else if (z) {
                            unlock();
                            return value;
                        } else {
                            this.modCount++;
                            a(internalEntry2, v);
                            unlock();
                            return value;
                        }
                    }
                }
                this.modCount++;
                E a2 = this.map.d.a(i(), k, i, internalEntry);
                a(a2, v);
                atomicReferenceArray.set(length, a2);
                this.count = i2;
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public void e() {
            if ((this.readCount.incrementAndGet() & 63) == 0) {
                g();
            }
        }

        /* access modifiers changed from: package-private */
        public V b(K k, int i, V v) {
            lock();
            try {
                f();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(length);
                InternalEntry internalEntry2 = internalEntry;
                while (internalEntry2 != null) {
                    Object key = internalEntry2.getKey();
                    if (internalEntry2.a() != i || key == null || !this.map.keyEquivalence.b(k, key)) {
                        internalEntry2 = internalEntry2.b();
                    } else {
                        V value = internalEntry2.getValue();
                        if (value == null) {
                            if (b(internalEntry2)) {
                                this.modCount++;
                                atomicReferenceArray.set(length, b(internalEntry, internalEntry2));
                                this.count--;
                            }
                            return null;
                        }
                        this.modCount++;
                        a(internalEntry2, v);
                        unlock();
                        return value;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(K k, int i, V v, V v2) {
            lock();
            try {
                f();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(length);
                InternalEntry internalEntry2 = internalEntry;
                while (internalEntry2 != null) {
                    Object key = internalEntry2.getKey();
                    if (internalEntry2.a() != i || key == null || !this.map.keyEquivalence.b(k, key)) {
                        internalEntry2 = internalEntry2.b();
                    } else {
                        Object value = internalEntry2.getValue();
                        if (value == null) {
                            if (b(internalEntry2)) {
                                this.modCount++;
                                atomicReferenceArray.set(length, b(internalEntry, internalEntry2));
                                this.count--;
                            }
                            return false;
                        } else if (this.map.a().b(v, value)) {
                            this.modCount++;
                            a(internalEntry2, v2);
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public E b(E e, E e2) {
            int i = this.count;
            E b = e2.b();
            while (e != e2) {
                E a2 = a(e, b);
                if (a2 != null) {
                    b = a2;
                } else {
                    i--;
                }
                e = e.b();
            }
            this.count = i;
            return b;
        }

        static <K, V, E extends InternalEntry<K, V, E>> boolean b(E e) {
            return e.getValue() == null;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Object obj, int i, Object obj2) {
            lock();
            try {
                f();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(length);
                InternalEntry internalEntry2 = internalEntry;
                while (true) {
                    boolean z = false;
                    if (internalEntry2 != null) {
                        Object key = internalEntry2.getKey();
                        if (internalEntry2.a() != i || key == null || !this.map.keyEquivalence.b(obj, key)) {
                            internalEntry2 = internalEntry2.b();
                        } else {
                            if (this.map.a().b(obj2, internalEntry2.getValue())) {
                                z = true;
                            } else if (!b(internalEntry2)) {
                                unlock();
                                return false;
                            }
                            this.modCount++;
                            atomicReferenceArray.set(length, b(internalEntry, internalEntry2));
                            this.count--;
                            return z;
                        }
                    } else {
                        unlock();
                        return false;
                    }
                }
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.count != 0) {
                lock();
                try {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    for (int i = 0; i < atomicReferenceArray.length(); i++) {
                        atomicReferenceArray.set(i, (Object) null);
                    }
                    c();
                    this.readCount.set(0);
                    this.modCount++;
                    this.count = 0;
                } finally {
                    unlock();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(E e, int i) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = i & (atomicReferenceArray.length() - 1);
                E e2 = (InternalEntry) atomicReferenceArray.get(length);
                for (E e3 = e2; e3 != null; e3 = e3.b()) {
                    if (e3 == e) {
                        this.modCount++;
                        atomicReferenceArray.set(length, b(e2, e3));
                        this.count--;
                        return true;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(K k, int i, WeakValueReference<K, V, E> weakValueReference) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(length);
                InternalEntry internalEntry2 = internalEntry;
                while (internalEntry2 != null) {
                    Object key = internalEntry2.getKey();
                    if (internalEntry2.a() != i || key == null || !this.map.keyEquivalence.b(k, key)) {
                        internalEntry2 = internalEntry2.b();
                    } else if (((WeakValueEntry) internalEntry2).c() == weakValueReference) {
                        this.modCount++;
                        atomicReferenceArray.set(length, b(internalEntry, internalEntry2));
                        this.count--;
                        return true;
                    } else {
                        unlock();
                        return false;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public V a(E e) {
            if (e.getKey() == null) {
                j();
                return null;
            }
            V value = e.getValue();
            if (value != null) {
                return value;
            }
            j();
            return null;
        }
    }

    private static final class SerializationProxy<K, V> extends AbstractSerializationProxy<K, V> {
        private static final long serialVersionUID = 3;

        SerializationProxy(Strength strength, Strength strength2, Equivalence<Object> equivalence, Equivalence<Object> equivalence2, int i, ConcurrentMap<K, V> concurrentMap) {
            super(strength, strength2, equivalence, equivalence2, i, concurrentMap);
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.f4360a = b(objectInputStream).f();
            a(objectInputStream);
        }

        private Object readResolve() {
            return this.f4360a;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            a(objectOutputStream);
        }
    }

    enum Strength {
        STRONG {
            /* access modifiers changed from: package-private */
            public Equivalence<Object> a() {
                return Equivalence.a();
            }
        },
        WEAK {
            /* access modifiers changed from: package-private */
            public Equivalence<Object> a() {
                return Equivalence.b();
            }
        };

        /* access modifiers changed from: package-private */
        public abstract Equivalence<Object> a();
    }

    static final class StrongKeyStrongValueEntry<K, V> extends AbstractStrongKeyEntry<K, V, StrongKeyStrongValueEntry<K, V>> implements StrongValueEntry<K, V, StrongKeyStrongValueEntry<K, V>> {
        private volatile V d = null;

        StrongKeyStrongValueEntry(K k, int i, StrongKeyStrongValueEntry<K, V> strongKeyStrongValueEntry) {
            super(k, i, strongKeyStrongValueEntry);
        }

        /* access modifiers changed from: package-private */
        public void a(V v) {
            this.d = v;
        }

        public V getValue() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public StrongKeyStrongValueEntry<K, V> a(StrongKeyStrongValueEntry<K, V> strongKeyStrongValueEntry) {
            StrongKeyStrongValueEntry<K, V> strongKeyStrongValueEntry2 = new StrongKeyStrongValueEntry<>(this.f4361a, this.b, strongKeyStrongValueEntry);
            strongKeyStrongValueEntry2.d = this.d;
            return strongKeyStrongValueEntry2;
        }

        static final class Helper<K, V> implements InternalEntryHelper<K, V, StrongKeyStrongValueEntry<K, V>, StrongKeyStrongValueSegment<K, V>> {

            /* renamed from: a  reason: collision with root package name */
            private static final Helper<?, ?> f4367a = new Helper<>();

            Helper() {
            }

            static <K, V> Helper<K, V> c() {
                return f4367a;
            }

            public Strength b() {
                return Strength.STRONG;
            }

            public Strength a() {
                return Strength.STRONG;
            }

            public StrongKeyStrongValueSegment<K, V> a(MapMakerInternalMap<K, V, StrongKeyStrongValueEntry<K, V>, StrongKeyStrongValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
                return new StrongKeyStrongValueSegment<>(mapMakerInternalMap, i, i2);
            }

            public StrongKeyStrongValueEntry<K, V> a(StrongKeyStrongValueSegment<K, V> strongKeyStrongValueSegment, StrongKeyStrongValueEntry<K, V> strongKeyStrongValueEntry, StrongKeyStrongValueEntry<K, V> strongKeyStrongValueEntry2) {
                return strongKeyStrongValueEntry.a(strongKeyStrongValueEntry2);
            }

            public void a(StrongKeyStrongValueSegment<K, V> strongKeyStrongValueSegment, StrongKeyStrongValueEntry<K, V> strongKeyStrongValueEntry, V v) {
                strongKeyStrongValueEntry.a(v);
            }

            public StrongKeyStrongValueEntry<K, V> a(StrongKeyStrongValueSegment<K, V> strongKeyStrongValueSegment, K k, int i, StrongKeyStrongValueEntry<K, V> strongKeyStrongValueEntry) {
                return new StrongKeyStrongValueEntry<>(k, i, strongKeyStrongValueEntry);
            }
        }
    }

    static final class StrongKeyStrongValueSegment<K, V> extends Segment<K, V, StrongKeyStrongValueEntry<K, V>, StrongKeyStrongValueSegment<K, V>> {
        StrongKeyStrongValueSegment(MapMakerInternalMap<K, V, StrongKeyStrongValueEntry<K, V>, StrongKeyStrongValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }

        /* access modifiers changed from: package-private */
        public StrongKeyStrongValueSegment<K, V> i() {
            return this;
        }
    }

    static final class StrongKeyWeakValueSegment<K, V> extends Segment<K, V, StrongKeyWeakValueEntry<K, V>, StrongKeyWeakValueSegment<K, V>> {
        /* access modifiers changed from: private */
        public final ReferenceQueue<V> queueForValues = new ReferenceQueue<>();

        StrongKeyWeakValueSegment(MapMakerInternalMap<K, V, StrongKeyWeakValueEntry<K, V>, StrongKeyWeakValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }

        /* access modifiers changed from: package-private */
        public void c() {
            a(this.queueForValues);
        }

        /* access modifiers changed from: package-private */
        public void d() {
            c(this.queueForValues);
        }

        /* access modifiers changed from: package-private */
        public StrongKeyWeakValueSegment<K, V> i() {
            return this;
        }
    }

    interface StrongValueEntry<K, V, E extends InternalEntry<K, V, E>> extends InternalEntry<K, V, E> {
    }

    final class ValueIterator extends MapMakerInternalMap<K, V, E, S>.HashIterator<V> {
        ValueIterator(MapMakerInternalMap mapMakerInternalMap) {
            super();
        }

        public V next() {
            return b().getValue();
        }
    }

    final class Values extends AbstractCollection<V> {
        Values() {
        }

        public void clear() {
            MapMakerInternalMap.this.clear();
        }

        public boolean contains(Object obj) {
            return MapMakerInternalMap.this.containsValue(obj);
        }

        public boolean isEmpty() {
            return MapMakerInternalMap.this.isEmpty();
        }

        public Iterator<V> iterator() {
            return new ValueIterator(MapMakerInternalMap.this);
        }

        public int size() {
            return MapMakerInternalMap.this.size();
        }

        public Object[] toArray() {
            return MapMakerInternalMap.b(this).toArray();
        }

        public <T> T[] toArray(T[] tArr) {
            return MapMakerInternalMap.b(this).toArray(tArr);
        }
    }

    static final class WeakKeyStrongValueEntry<K, V> extends AbstractWeakKeyEntry<K, V, WeakKeyStrongValueEntry<K, V>> implements StrongValueEntry<K, V, WeakKeyStrongValueEntry<K, V>> {
        private volatile V c = null;

        WeakKeyStrongValueEntry(ReferenceQueue<K> referenceQueue, K k, int i, WeakKeyStrongValueEntry<K, V> weakKeyStrongValueEntry) {
            super(referenceQueue, k, i, weakKeyStrongValueEntry);
        }

        /* access modifiers changed from: package-private */
        public void a(V v) {
            this.c = v;
        }

        public V getValue() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public WeakKeyStrongValueEntry<K, V> a(ReferenceQueue<K> referenceQueue, WeakKeyStrongValueEntry<K, V> weakKeyStrongValueEntry) {
            WeakKeyStrongValueEntry<K, V> weakKeyStrongValueEntry2 = new WeakKeyStrongValueEntry<>(referenceQueue, getKey(), this.f4362a, weakKeyStrongValueEntry);
            weakKeyStrongValueEntry2.a(this.c);
            return weakKeyStrongValueEntry2;
        }

        static final class Helper<K, V> implements InternalEntryHelper<K, V, WeakKeyStrongValueEntry<K, V>, WeakKeyStrongValueSegment<K, V>> {

            /* renamed from: a  reason: collision with root package name */
            private static final Helper<?, ?> f4370a = new Helper<>();

            Helper() {
            }

            static <K, V> Helper<K, V> c() {
                return f4370a;
            }

            public Strength b() {
                return Strength.WEAK;
            }

            public Strength a() {
                return Strength.STRONG;
            }

            public WeakKeyStrongValueSegment<K, V> a(MapMakerInternalMap<K, V, WeakKeyStrongValueEntry<K, V>, WeakKeyStrongValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
                return new WeakKeyStrongValueSegment<>(mapMakerInternalMap, i, i2);
            }

            public WeakKeyStrongValueEntry<K, V> a(WeakKeyStrongValueSegment<K, V> weakKeyStrongValueSegment, WeakKeyStrongValueEntry<K, V> weakKeyStrongValueEntry, WeakKeyStrongValueEntry<K, V> weakKeyStrongValueEntry2) {
                if (weakKeyStrongValueEntry.getKey() == null) {
                    return null;
                }
                return weakKeyStrongValueEntry.a(weakKeyStrongValueSegment.queueForKeys, weakKeyStrongValueEntry2);
            }

            public void a(WeakKeyStrongValueSegment<K, V> weakKeyStrongValueSegment, WeakKeyStrongValueEntry<K, V> weakKeyStrongValueEntry, V v) {
                weakKeyStrongValueEntry.a(v);
            }

            public WeakKeyStrongValueEntry<K, V> a(WeakKeyStrongValueSegment<K, V> weakKeyStrongValueSegment, K k, int i, WeakKeyStrongValueEntry<K, V> weakKeyStrongValueEntry) {
                return new WeakKeyStrongValueEntry<>(weakKeyStrongValueSegment.queueForKeys, k, i, weakKeyStrongValueEntry);
            }
        }
    }

    static final class WeakKeyStrongValueSegment<K, V> extends Segment<K, V, WeakKeyStrongValueEntry<K, V>, WeakKeyStrongValueSegment<K, V>> {
        /* access modifiers changed from: private */
        public final ReferenceQueue<K> queueForKeys = new ReferenceQueue<>();

        WeakKeyStrongValueSegment(MapMakerInternalMap<K, V, WeakKeyStrongValueEntry<K, V>, WeakKeyStrongValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }

        /* access modifiers changed from: package-private */
        public void c() {
            a(this.queueForKeys);
        }

        /* access modifiers changed from: package-private */
        public void d() {
            b(this.queueForKeys);
        }

        /* access modifiers changed from: package-private */
        public WeakKeyStrongValueSegment<K, V> i() {
            return this;
        }
    }

    static final class WeakKeyWeakValueSegment<K, V> extends Segment<K, V, WeakKeyWeakValueEntry<K, V>, WeakKeyWeakValueSegment<K, V>> {
        /* access modifiers changed from: private */
        public final ReferenceQueue<K> queueForKeys = new ReferenceQueue<>();
        /* access modifiers changed from: private */
        public final ReferenceQueue<V> queueForValues = new ReferenceQueue<>();

        WeakKeyWeakValueSegment(MapMakerInternalMap<K, V, WeakKeyWeakValueEntry<K, V>, WeakKeyWeakValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
            super(mapMakerInternalMap, i, i2);
        }

        /* access modifiers changed from: package-private */
        public void c() {
            a(this.queueForKeys);
        }

        /* access modifiers changed from: package-private */
        public void d() {
            b(this.queueForKeys);
            c(this.queueForValues);
        }

        /* access modifiers changed from: package-private */
        public WeakKeyWeakValueSegment<K, V> i() {
            return this;
        }
    }

    interface WeakValueEntry<K, V, E extends InternalEntry<K, V, E>> extends InternalEntry<K, V, E> {
        WeakValueReference<K, V, E> c();
    }

    interface WeakValueReference<K, V, E extends InternalEntry<K, V, E>> {
        E a();

        WeakValueReference<K, V, E> a(ReferenceQueue<V> referenceQueue, E e);

        void clear();

        V get();
    }

    static final class WeakValueReferenceImpl<K, V, E extends InternalEntry<K, V, E>> extends WeakReference<V> implements WeakValueReference<K, V, E> {

        /* renamed from: a  reason: collision with root package name */
        final E f4372a;

        WeakValueReferenceImpl(ReferenceQueue<V> referenceQueue, V v, E e) {
            super(v, referenceQueue);
            this.f4372a = e;
        }

        public E a() {
            return this.f4372a;
        }

        public WeakValueReference<K, V, E> a(ReferenceQueue<V> referenceQueue, E e) {
            return new WeakValueReferenceImpl(referenceQueue, get(), e);
        }
    }

    final class WriteThroughEntry extends AbstractMapEntry<K, V> {

        /* renamed from: a  reason: collision with root package name */
        final K f4373a;
        V b;

        WriteThroughEntry(K k, V v) {
            this.f4373a = k;
            this.b = v;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            if (!this.f4373a.equals(entry.getKey()) || !this.b.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        public K getKey() {
            return this.f4373a;
        }

        public V getValue() {
            return this.b;
        }

        public int hashCode() {
            return this.f4373a.hashCode() ^ this.b.hashCode();
        }

        public V setValue(V v) {
            V put = MapMakerInternalMap.this.put(this.f4373a, v);
            this.b = v;
            return put;
        }
    }

    private MapMakerInternalMap(MapMaker mapMaker, InternalEntryHelper<K, V, E, S> internalEntryHelper) {
        this.concurrencyLevel = Math.min(mapMaker.a(), ImageMetadata.CONTROL_AE_ANTIBANDING_MODE);
        this.keyEquivalence = mapMaker.c();
        this.d = internalEntryHelper;
        int min = Math.min(mapMaker.b(), 1073741824);
        int i = 0;
        int i2 = 1;
        int i3 = 1;
        int i4 = 0;
        while (i3 < this.concurrencyLevel) {
            i4++;
            i3 <<= 1;
        }
        this.b = 32 - i4;
        this.f4359a = i3 - 1;
        this.c = a(i3);
        int i5 = min / i3;
        while (i2 < (i3 * i5 < min ? i5 + 1 : i5)) {
            i2 <<= 1;
        }
        while (true) {
            Segment<K, V, E, S>[] segmentArr = this.c;
            if (i < segmentArr.length) {
                segmentArr[i] = a(i2, -1);
                i++;
            } else {
                return;
            }
        }
    }

    static <K, V, E extends InternalEntry<K, V, E>> WeakValueReference<K, V, E> b() {
        return h;
    }

    static int c(int i) {
        int i2 = i + ((i << 15) ^ -12931);
        int i3 = i2 ^ (i2 >>> 10);
        int i4 = i3 + (i3 << 3);
        int i5 = i4 ^ (i4 >>> 6);
        int i6 = i5 + (i5 << 2) + (i5 << 14);
        return i6 ^ (i6 >>> 16);
    }

    public void clear() {
        for (Segment<K, V, E, S> a2 : this.c) {
            a2.a();
        }
    }

    public boolean containsKey(Object obj) {
        if (obj == null) {
            return false;
        }
        int a2 = a(obj);
        return b(a2).a(obj, a2);
    }

    public boolean containsValue(Object obj) {
        Object obj2 = obj;
        if (obj2 == null) {
            return false;
        }
        Segment<K, V, E, S>[] segmentArr = this.c;
        long j = -1;
        int i = 0;
        while (i < 3) {
            long j2 = 0;
            for (Segment<K, V, E, S> segment : segmentArr) {
                int i2 = segment.count;
                AtomicReferenceArray<E> atomicReferenceArray = segment.table;
                for (int i3 = 0; i3 < atomicReferenceArray.length(); i3++) {
                    for (InternalEntry internalEntry = (InternalEntry) atomicReferenceArray.get(i3); internalEntry != null; internalEntry = internalEntry.b()) {
                        V a2 = segment.a(internalEntry);
                        if (a2 != null && a().b(obj2, a2)) {
                            return true;
                        }
                    }
                }
                j2 += (long) segment.modCount;
            }
            if (j2 == j) {
                return false;
            }
            i++;
            j = j2;
        }
        return false;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.g;
        if (set != null) {
            return set;
        }
        EntrySet entrySet = new EntrySet();
        this.g = entrySet;
        return entrySet;
    }

    public V get(Object obj) {
        if (obj == null) {
            return null;
        }
        int a2 = a(obj);
        return b(a2).b(obj, a2);
    }

    public boolean isEmpty() {
        Segment<K, V, E, S>[] segmentArr = this.c;
        long j = 0;
        for (int i = 0; i < segmentArr.length; i++) {
            if (segmentArr[i].count != 0) {
                return false;
            }
            j += (long) segmentArr[i].modCount;
        }
        if (j == 0) {
            return true;
        }
        long j2 = j;
        for (int i2 = 0; i2 < segmentArr.length; i2++) {
            if (segmentArr[i2].count != 0) {
                return false;
            }
            j2 -= (long) segmentArr[i2].modCount;
        }
        if (j2 == 0) {
            return true;
        }
        return false;
    }

    public Set<K> keySet() {
        Set<K> set = this.e;
        if (set != null) {
            return set;
        }
        KeySet keySet = new KeySet();
        this.e = keySet;
        return keySet;
    }

    public V put(K k, V v) {
        Preconditions.a(k);
        Preconditions.a(v);
        int a2 = a((Object) k);
        return b(a2).a(k, a2, v, false);
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    public V putIfAbsent(K k, V v) {
        Preconditions.a(k);
        Preconditions.a(v);
        int a2 = a((Object) k);
        return b(a2).a(k, a2, v, true);
    }

    public V remove(Object obj) {
        if (obj == null) {
            return null;
        }
        int a2 = a(obj);
        return b(a2).e(obj, a2);
    }

    public boolean replace(K k, V v, V v2) {
        Preconditions.a(k);
        Preconditions.a(v2);
        if (v == null) {
            return false;
        }
        int a2 = a((Object) k);
        return b(a2).a(k, a2, v, v2);
    }

    public int size() {
        Segment<K, V, E, S>[] segmentArr = this.c;
        long j = 0;
        for (Segment<K, V, E, S> segment : segmentArr) {
            j += (long) segment.count;
        }
        return Ints.a(j);
    }

    public Collection<V> values() {
        Collection<V> collection = this.f;
        if (collection != null) {
            return collection;
        }
        Values values = new Values();
        this.f = values;
        return values;
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializationProxy(this.d.b(), this.d.a(), this.keyEquivalence, this.d.a().a(), this.concurrencyLevel, this);
    }

    static abstract class AbstractSerializationProxy<K, V> extends ForwardingConcurrentMap<K, V> implements Serializable {
        private static final long serialVersionUID = 3;

        /* renamed from: a  reason: collision with root package name */
        transient ConcurrentMap<K, V> f4360a;
        final int concurrencyLevel;
        final Equivalence<Object> keyEquivalence;
        final Strength keyStrength;
        final Equivalence<Object> valueEquivalence;
        final Strength valueStrength;

        AbstractSerializationProxy(Strength strength, Strength strength2, Equivalence<Object> equivalence, Equivalence<Object> equivalence2, int i, ConcurrentMap<K, V> concurrentMap) {
            this.keyStrength = strength;
            this.valueStrength = strength2;
            this.keyEquivalence = equivalence;
            this.valueEquivalence = equivalence2;
            this.concurrencyLevel = i;
            this.f4360a = concurrentMap;
        }

        /* access modifiers changed from: package-private */
        public MapMaker b(ObjectInputStream objectInputStream) throws IOException {
            return new MapMaker().b(objectInputStream.readInt()).a(this.keyStrength).b(this.valueStrength).a(this.keyEquivalence).a(this.concurrencyLevel);
        }

        /* access modifiers changed from: protected */
        public ConcurrentMap<K, V> a() {
            return this.f4360a;
        }

        /* access modifiers changed from: package-private */
        public void a(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeInt(this.f4360a.size());
            for (Map.Entry entry : this.f4360a.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject((Object) null);
        }

        /* access modifiers changed from: package-private */
        public void a(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            while (true) {
                Object readObject = objectInputStream.readObject();
                if (readObject != null) {
                    this.f4360a.put(readObject, objectInputStream.readObject());
                } else {
                    return;
                }
            }
        }
    }

    static <K, V> MapMakerInternalMap<K, V, ? extends InternalEntry<K, V, ?>, ?> a(MapMaker mapMaker) {
        if (mapMaker.d() == Strength.STRONG && mapMaker.e() == Strength.STRONG) {
            return new MapMakerInternalMap<>(mapMaker, StrongKeyStrongValueEntry.Helper.c());
        }
        if (mapMaker.d() == Strength.STRONG && mapMaker.e() == Strength.WEAK) {
            return new MapMakerInternalMap<>(mapMaker, StrongKeyWeakValueEntry.Helper.c());
        }
        if (mapMaker.d() == Strength.WEAK && mapMaker.e() == Strength.STRONG) {
            return new MapMakerInternalMap<>(mapMaker, WeakKeyStrongValueEntry.Helper.c());
        }
        if (mapMaker.d() == Strength.WEAK && mapMaker.e() == Strength.WEAK) {
            return new MapMakerInternalMap<>(mapMaker, WeakKeyWeakValueEntry.Helper.c());
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void b(E e2) {
        int a2 = e2.a();
        b(a2).a(e2, a2);
    }

    static final class StrongKeyWeakValueEntry<K, V> extends AbstractStrongKeyEntry<K, V, StrongKeyWeakValueEntry<K, V>> implements WeakValueEntry<K, V, StrongKeyWeakValueEntry<K, V>> {
        private volatile WeakValueReference<K, V, StrongKeyWeakValueEntry<K, V>> d = MapMakerInternalMap.b();

        StrongKeyWeakValueEntry(K k, int i, StrongKeyWeakValueEntry<K, V> strongKeyWeakValueEntry) {
            super(k, i, strongKeyWeakValueEntry);
        }

        /* access modifiers changed from: package-private */
        public void a(V v, ReferenceQueue<V> referenceQueue) {
            WeakValueReference<K, V, StrongKeyWeakValueEntry<K, V>> weakValueReference = this.d;
            this.d = new WeakValueReferenceImpl(referenceQueue, v, this);
            weakValueReference.clear();
        }

        public WeakValueReference<K, V, StrongKeyWeakValueEntry<K, V>> c() {
            return this.d;
        }

        public V getValue() {
            return this.d.get();
        }

        static final class Helper<K, V> implements InternalEntryHelper<K, V, StrongKeyWeakValueEntry<K, V>, StrongKeyWeakValueSegment<K, V>> {

            /* renamed from: a  reason: collision with root package name */
            private static final Helper<?, ?> f4368a = new Helper<>();

            Helper() {
            }

            static <K, V> Helper<K, V> c() {
                return f4368a;
            }

            public Strength b() {
                return Strength.STRONG;
            }

            public Strength a() {
                return Strength.WEAK;
            }

            public StrongKeyWeakValueSegment<K, V> a(MapMakerInternalMap<K, V, StrongKeyWeakValueEntry<K, V>, StrongKeyWeakValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
                return new StrongKeyWeakValueSegment<>(mapMakerInternalMap, i, i2);
            }

            public StrongKeyWeakValueEntry<K, V> a(StrongKeyWeakValueSegment<K, V> strongKeyWeakValueSegment, StrongKeyWeakValueEntry<K, V> strongKeyWeakValueEntry, StrongKeyWeakValueEntry<K, V> strongKeyWeakValueEntry2) {
                if (Segment.b(strongKeyWeakValueEntry)) {
                    return null;
                }
                return strongKeyWeakValueEntry.a((ReferenceQueue<V>) strongKeyWeakValueSegment.queueForValues, strongKeyWeakValueEntry2);
            }

            public void a(StrongKeyWeakValueSegment<K, V> strongKeyWeakValueSegment, StrongKeyWeakValueEntry<K, V> strongKeyWeakValueEntry, V v) {
                strongKeyWeakValueEntry.a(v, (ReferenceQueue<V>) strongKeyWeakValueSegment.queueForValues);
            }

            public StrongKeyWeakValueEntry<K, V> a(StrongKeyWeakValueSegment<K, V> strongKeyWeakValueSegment, K k, int i, StrongKeyWeakValueEntry<K, V> strongKeyWeakValueEntry) {
                return new StrongKeyWeakValueEntry<>(k, i, strongKeyWeakValueEntry);
            }
        }

        /* access modifiers changed from: package-private */
        public StrongKeyWeakValueEntry<K, V> a(ReferenceQueue<V> referenceQueue, StrongKeyWeakValueEntry<K, V> strongKeyWeakValueEntry) {
            StrongKeyWeakValueEntry<K, V> strongKeyWeakValueEntry2 = new StrongKeyWeakValueEntry<>(this.f4361a, this.b, strongKeyWeakValueEntry);
            strongKeyWeakValueEntry2.d = this.d.a(referenceQueue, strongKeyWeakValueEntry2);
            return strongKeyWeakValueEntry2;
        }
    }

    static final class WeakKeyWeakValueEntry<K, V> extends AbstractWeakKeyEntry<K, V, WeakKeyWeakValueEntry<K, V>> implements WeakValueEntry<K, V, WeakKeyWeakValueEntry<K, V>> {
        private volatile WeakValueReference<K, V, WeakKeyWeakValueEntry<K, V>> c = MapMakerInternalMap.b();

        WeakKeyWeakValueEntry(ReferenceQueue<K> referenceQueue, K k, int i, WeakKeyWeakValueEntry<K, V> weakKeyWeakValueEntry) {
            super(referenceQueue, k, i, weakKeyWeakValueEntry);
        }

        /* access modifiers changed from: package-private */
        public WeakKeyWeakValueEntry<K, V> a(ReferenceQueue<K> referenceQueue, ReferenceQueue<V> referenceQueue2, WeakKeyWeakValueEntry<K, V> weakKeyWeakValueEntry) {
            WeakKeyWeakValueEntry<K, V> weakKeyWeakValueEntry2 = new WeakKeyWeakValueEntry<>(referenceQueue, getKey(), this.f4362a, weakKeyWeakValueEntry);
            weakKeyWeakValueEntry2.c = this.c.a(referenceQueue2, weakKeyWeakValueEntry2);
            return weakKeyWeakValueEntry2;
        }

        public WeakValueReference<K, V, WeakKeyWeakValueEntry<K, V>> c() {
            return this.c;
        }

        public V getValue() {
            return this.c.get();
        }

        static final class Helper<K, V> implements InternalEntryHelper<K, V, WeakKeyWeakValueEntry<K, V>, WeakKeyWeakValueSegment<K, V>> {

            /* renamed from: a  reason: collision with root package name */
            private static final Helper<?, ?> f4371a = new Helper<>();

            Helper() {
            }

            static <K, V> Helper<K, V> c() {
                return f4371a;
            }

            public Strength b() {
                return Strength.WEAK;
            }

            public Strength a() {
                return Strength.WEAK;
            }

            public WeakKeyWeakValueSegment<K, V> a(MapMakerInternalMap<K, V, WeakKeyWeakValueEntry<K, V>, WeakKeyWeakValueSegment<K, V>> mapMakerInternalMap, int i, int i2) {
                return new WeakKeyWeakValueSegment<>(mapMakerInternalMap, i, i2);
            }

            public WeakKeyWeakValueEntry<K, V> a(WeakKeyWeakValueSegment<K, V> weakKeyWeakValueSegment, WeakKeyWeakValueEntry<K, V> weakKeyWeakValueEntry, WeakKeyWeakValueEntry<K, V> weakKeyWeakValueEntry2) {
                if (weakKeyWeakValueEntry.getKey() != null && !Segment.b(weakKeyWeakValueEntry)) {
                    return weakKeyWeakValueEntry.a(weakKeyWeakValueSegment.queueForKeys, weakKeyWeakValueSegment.queueForValues, weakKeyWeakValueEntry2);
                }
                return null;
            }

            public void a(WeakKeyWeakValueSegment<K, V> weakKeyWeakValueSegment, WeakKeyWeakValueEntry<K, V> weakKeyWeakValueEntry, V v) {
                weakKeyWeakValueEntry.a(v, weakKeyWeakValueSegment.queueForValues);
            }

            public WeakKeyWeakValueEntry<K, V> a(WeakKeyWeakValueSegment<K, V> weakKeyWeakValueSegment, K k, int i, WeakKeyWeakValueEntry<K, V> weakKeyWeakValueEntry) {
                return new WeakKeyWeakValueEntry<>(weakKeyWeakValueSegment.queueForKeys, k, i, weakKeyWeakValueEntry);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(V v, ReferenceQueue<V> referenceQueue) {
            WeakValueReference<K, V, WeakKeyWeakValueEntry<K, V>> weakValueReference = this.c;
            this.c = new WeakValueReferenceImpl(referenceQueue, v, this);
            weakValueReference.clear();
        }
    }

    public boolean remove(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            return false;
        }
        int a2 = a(obj);
        return b(a2).a(obj, a2, obj2);
    }

    /* access modifiers changed from: package-private */
    public Segment<K, V, E, S> b(int i) {
        return this.c[(i >>> this.b) & this.f4359a];
    }

    /* access modifiers changed from: private */
    public static <E> ArrayList<E> b(Collection<E> collection) {
        ArrayList<E> arrayList = new ArrayList<>(collection.size());
        Iterators.a(arrayList, collection.iterator());
        return arrayList;
    }

    public V replace(K k, V v) {
        Preconditions.a(k);
        Preconditions.a(v);
        int a2 = a((Object) k);
        return b(a2).b(k, a2, v);
    }

    abstract class HashIterator<T> implements Iterator<T> {

        /* renamed from: a  reason: collision with root package name */
        int f4364a;
        int b = -1;
        Segment<K, V, E, S> c;
        AtomicReferenceArray<E> d;
        E e;
        MapMakerInternalMap<K, V, E, S>.WriteThroughEntry f;
        MapMakerInternalMap<K, V, E, S>.WriteThroughEntry g;

        HashIterator() {
            this.f4364a = MapMakerInternalMap.this.c.length - 1;
            a();
        }

        /* access modifiers changed from: package-private */
        public final void a() {
            this.f = null;
            if (!c() && !d()) {
                while (true) {
                    int i = this.f4364a;
                    if (i >= 0) {
                        Segment<K, V, E, S>[] segmentArr = MapMakerInternalMap.this.c;
                        this.f4364a = i - 1;
                        this.c = segmentArr[i];
                        if (this.c.count != 0) {
                            this.d = this.c.table;
                            this.b = this.d.length() - 1;
                            if (d()) {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public MapMakerInternalMap<K, V, E, S>.WriteThroughEntry b() {
            MapMakerInternalMap<K, V, E, S>.WriteThroughEntry writeThroughEntry = this.f;
            if (writeThroughEntry != null) {
                this.g = writeThroughEntry;
                a();
                return this.g;
            }
            throw new NoSuchElementException();
        }

        /* access modifiers changed from: package-private */
        public boolean c() {
            E e2 = this.e;
            if (e2 == null) {
                return false;
            }
            while (true) {
                this.e = e2.b();
                E e3 = this.e;
                if (e3 == null) {
                    return false;
                }
                if (a(e3)) {
                    return true;
                }
                e2 = this.e;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean d() {
            while (true) {
                int i = this.b;
                if (i < 0) {
                    return false;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.d;
                this.b = i - 1;
                E e2 = (InternalEntry) atomicReferenceArray.get(i);
                this.e = e2;
                if (e2 != null && (a(this.e) || c())) {
                    return true;
                }
            }
        }

        public boolean hasNext() {
            return this.f != null;
        }

        public void remove() {
            CollectPreconditions.a(this.g != null);
            MapMakerInternalMap.this.remove(this.g.getKey());
            this.g = null;
        }

        /* access modifiers changed from: package-private */
        public boolean a(E e2) {
            boolean z;
            try {
                Object key = e2.getKey();
                Object a2 = MapMakerInternalMap.this.a(e2);
                if (a2 != null) {
                    this.f = new WriteThroughEntry(key, a2);
                    z = true;
                } else {
                    z = false;
                }
                return z;
            } finally {
                this.c.e();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a(Object obj) {
        return c(this.keyEquivalence.b(obj));
    }

    /* access modifiers changed from: package-private */
    public void a(WeakValueReference<K, V, E> weakValueReference) {
        E a2 = weakValueReference.a();
        int a3 = a2.a();
        b(a3).a(a2.getKey(), a3, weakValueReference);
    }

    /* access modifiers changed from: package-private */
    public Segment<K, V, E, S> a(int i, int i2) {
        return this.d.a(this, i, i2);
    }

    /* access modifiers changed from: package-private */
    public V a(E e2) {
        if (e2.getKey() == null) {
            return null;
        }
        return e2.getValue();
    }

    /* access modifiers changed from: package-private */
    public final Segment<K, V, E, S>[] a(int i) {
        return new Segment[i];
    }

    /* access modifiers changed from: package-private */
    public Equivalence<Object> a() {
        return this.d.a().a();
    }
}
