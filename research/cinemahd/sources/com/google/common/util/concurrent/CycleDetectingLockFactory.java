package com.google.common.util.concurrent;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.MapMaker;
import java.util.ArrayList;

public class CycleDetectingLockFactory {

    private static class ExampleStackTrace extends IllegalStateException {
        static {
            ImmutableSet.a(CycleDetectingLockFactory.class.getName(), ExampleStackTrace.class.getName(), LockGraphNode.class.getName());
        }
    }

    private static class LockGraphNode {
    }

    public static final class PotentialDeadlockException extends ExampleStackTrace {
        private final ExampleStackTrace conflictingStackTrace;

        public String getMessage() {
            StringBuilder sb = new StringBuilder(super.getMessage());
            for (Throwable th = this.conflictingStackTrace; th != null; th = th.getCause()) {
                sb.append(", ");
                sb.append(th.getMessage());
            }
            return sb.toString();
        }
    }

    static {
        new MapMaker().g().f();
        new ThreadLocal<ArrayList<LockGraphNode>>() {
            /* access modifiers changed from: protected */
            public ArrayList<LockGraphNode> initialValue() {
                return Lists.a(3);
            }
        };
    }
}
