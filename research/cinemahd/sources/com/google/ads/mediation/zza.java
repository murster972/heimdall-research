package com.google.ads.mediation;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

final class zza implements RewardedVideoAdListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AbstractAdViewAdapter f3147a;

    zza(AbstractAdViewAdapter abstractAdViewAdapter) {
        this.f3147a = abstractAdViewAdapter;
    }

    public final void onRewarded(RewardItem rewardItem) {
        this.f3147a.zzlv.onRewarded(this.f3147a, rewardItem);
    }

    public final void onRewardedVideoAdClosed() {
        this.f3147a.zzlv.onAdClosed(this.f3147a);
        InterstitialAd unused = this.f3147a.zzlu = null;
    }

    public final void onRewardedVideoAdFailedToLoad(int i) {
        this.f3147a.zzlv.onAdFailedToLoad(this.f3147a, i);
    }

    public final void onRewardedVideoAdLeftApplication() {
        this.f3147a.zzlv.onAdLeftApplication(this.f3147a);
    }

    public final void onRewardedVideoAdLoaded() {
        this.f3147a.zzlv.onAdLoaded(this.f3147a);
    }

    public final void onRewardedVideoAdOpened() {
        this.f3147a.zzlv.onAdOpened(this.f3147a);
    }

    public final void onRewardedVideoCompleted() {
        this.f3147a.zzlv.onVideoCompleted(this.f3147a);
    }

    public final void onRewardedVideoStarted() {
        this.f3147a.zzlv.onVideoStarted(this.f3147a);
    }
}
