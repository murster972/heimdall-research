package com.google.ads.mediation;

import com.google.android.gms.ads.reward.AdMetadataListener;

final class zzb extends AdMetadataListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AbstractAdViewAdapter f3148a;

    zzb(AbstractAdViewAdapter abstractAdViewAdapter) {
        this.f3148a = abstractAdViewAdapter;
    }

    public final void onAdMetadataChanged() {
        if (this.f3148a.zzlu != null && this.f3148a.zzlv != null) {
            this.f3148a.zzlv.zzb(this.f3148a.zzlu.getAdMetadata());
        }
    }
}
