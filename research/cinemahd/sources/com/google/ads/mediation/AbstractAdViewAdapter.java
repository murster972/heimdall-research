package com.google.ads.mediation;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdView;
import com.google.android.gms.ads.formats.NativeAdViewHolder;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.OnImmersiveModeUpdatedListener;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzbfy;
import com.google.android.gms.internal.ads.zzty;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzxb;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public abstract class AbstractAdViewAdapter implements MediationBannerAdapter, MediationNativeAdapter, OnImmersiveModeUpdatedListener, com.google.android.gms.ads.mediation.zza, MediationRewardedVideoAdAdapter, zzbfy {
    public static final String AD_UNIT_ID_PARAMETER = "pubid";
    private AdView zzlq;
    private InterstitialAd zzlr;
    private AdLoader zzls;
    private Context zzlt;
    /* access modifiers changed from: private */
    public InterstitialAd zzlu;
    /* access modifiers changed from: private */
    public MediationRewardedVideoAdListener zzlv;
    private final RewardedVideoAdListener zzlw = new zza(this);

    static class zza extends NativeAppInstallAdMapper {
        private final NativeAppInstallAd p;

        public zza(NativeAppInstallAd nativeAppInstallAd) {
            this.p = nativeAppInstallAd;
            setHeadline(nativeAppInstallAd.getHeadline().toString());
            setImages(nativeAppInstallAd.getImages());
            setBody(nativeAppInstallAd.getBody().toString());
            setIcon(nativeAppInstallAd.getIcon());
            setCallToAction(nativeAppInstallAd.getCallToAction().toString());
            if (nativeAppInstallAd.getStarRating() != null) {
                setStarRating(nativeAppInstallAd.getStarRating().doubleValue());
            }
            if (nativeAppInstallAd.getStore() != null) {
                setStore(nativeAppInstallAd.getStore().toString());
            }
            if (nativeAppInstallAd.getPrice() != null) {
                setPrice(nativeAppInstallAd.getPrice().toString());
            }
            setOverrideImpressionRecording(true);
            setOverrideClickHandling(true);
            zza(nativeAppInstallAd.getVideoController());
        }

        public final void trackView(View view) {
            if (view instanceof NativeAdView) {
                ((NativeAdView) view).setNativeAd(this.p);
            }
            NativeAdViewHolder nativeAdViewHolder = NativeAdViewHolder.zzbkd.get(view);
            if (nativeAdViewHolder != null) {
                nativeAdViewHolder.setNativeAd((NativeAd) this.p);
            }
        }
    }

    static class zzb extends UnifiedNativeAdMapper {
        private final UnifiedNativeAd s;

        public zzb(UnifiedNativeAd unifiedNativeAd) {
            this.s = unifiedNativeAd;
            setHeadline(unifiedNativeAd.getHeadline());
            setImages(unifiedNativeAd.getImages());
            setBody(unifiedNativeAd.getBody());
            setIcon(unifiedNativeAd.getIcon());
            setCallToAction(unifiedNativeAd.getCallToAction());
            setAdvertiser(unifiedNativeAd.getAdvertiser());
            setStarRating(unifiedNativeAd.getStarRating());
            setStore(unifiedNativeAd.getStore());
            setPrice(unifiedNativeAd.getPrice());
            zzn(unifiedNativeAd.zzjo());
            setOverrideImpressionRecording(true);
            setOverrideClickHandling(true);
            zza(unifiedNativeAd.getVideoController());
        }

        public final void trackViews(View view, Map<String, View> map, Map<String, View> map2) {
            if (view instanceof UnifiedNativeAdView) {
                ((UnifiedNativeAdView) view).setNativeAd(this.s);
                return;
            }
            NativeAdViewHolder nativeAdViewHolder = NativeAdViewHolder.zzbkd.get(view);
            if (nativeAdViewHolder != null) {
                nativeAdViewHolder.setNativeAd(this.s);
            }
        }
    }

    static class zzc extends NativeContentAdMapper {
        private final NativeContentAd n;

        public zzc(NativeContentAd nativeContentAd) {
            this.n = nativeContentAd;
            setHeadline(nativeContentAd.getHeadline().toString());
            setImages(nativeContentAd.getImages());
            setBody(nativeContentAd.getBody().toString());
            if (nativeContentAd.getLogo() != null) {
                setLogo(nativeContentAd.getLogo());
            }
            setCallToAction(nativeContentAd.getCallToAction().toString());
            setAdvertiser(nativeContentAd.getAdvertiser().toString());
            setOverrideImpressionRecording(true);
            setOverrideClickHandling(true);
            zza(nativeContentAd.getVideoController());
        }

        public final void trackView(View view) {
            if (view instanceof NativeAdView) {
                ((NativeAdView) view).setNativeAd(this.n);
            }
            NativeAdViewHolder nativeAdViewHolder = NativeAdViewHolder.zzbkd.get(view);
            if (nativeAdViewHolder != null) {
                nativeAdViewHolder.setNativeAd((NativeAd) this.n);
            }
        }
    }

    static final class zzd extends AdListener implements zzty {

        /* renamed from: a  reason: collision with root package name */
        private final AbstractAdViewAdapter f3140a;
        private final MediationInterstitialListener b;

        public zzd(AbstractAdViewAdapter abstractAdViewAdapter, MediationInterstitialListener mediationInterstitialListener) {
            this.f3140a = abstractAdViewAdapter;
            this.b = mediationInterstitialListener;
        }

        public final void onAdClicked() {
            this.b.onAdClicked(this.f3140a);
        }

        public final void onAdClosed() {
            this.b.onAdClosed(this.f3140a);
        }

        public final void onAdFailedToLoad(int i) {
            this.b.onAdFailedToLoad(this.f3140a, i);
        }

        public final void onAdLeftApplication() {
            this.b.onAdLeftApplication(this.f3140a);
        }

        public final void onAdLoaded() {
            this.b.onAdLoaded(this.f3140a);
        }

        public final void onAdOpened() {
            this.b.onAdOpened(this.f3140a);
        }
    }

    static final class zze extends AdListener implements AppEventListener, zzty {

        /* renamed from: a  reason: collision with root package name */
        private final AbstractAdViewAdapter f3141a;
        private final MediationBannerListener b;

        public zze(AbstractAdViewAdapter abstractAdViewAdapter, MediationBannerListener mediationBannerListener) {
            this.f3141a = abstractAdViewAdapter;
            this.b = mediationBannerListener;
        }

        public final void onAdClicked() {
            this.b.onAdClicked(this.f3141a);
        }

        public final void onAdClosed() {
            this.b.onAdClosed(this.f3141a);
        }

        public final void onAdFailedToLoad(int i) {
            this.b.onAdFailedToLoad(this.f3141a, i);
        }

        public final void onAdLeftApplication() {
            this.b.onAdLeftApplication(this.f3141a);
        }

        public final void onAdLoaded() {
            this.b.onAdLoaded(this.f3141a);
        }

        public final void onAdOpened() {
            this.b.onAdOpened(this.f3141a);
        }

        public final void onAppEvent(String str, String str2) {
            this.b.zza(this.f3141a, str, str2);
        }
    }

    static final class zzf extends AdListener implements NativeAppInstallAd.OnAppInstallAdLoadedListener, NativeContentAd.OnContentAdLoadedListener, NativeCustomTemplateAd.OnCustomClickListener, NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener, UnifiedNativeAd.OnUnifiedNativeAdLoadedListener {

        /* renamed from: a  reason: collision with root package name */
        private final AbstractAdViewAdapter f3142a;
        private final MediationNativeListener b;

        public zzf(AbstractAdViewAdapter abstractAdViewAdapter, MediationNativeListener mediationNativeListener) {
            this.f3142a = abstractAdViewAdapter;
            this.b = mediationNativeListener;
        }

        public final void onAdClicked() {
            this.b.onAdClicked(this.f3142a);
        }

        public final void onAdClosed() {
            this.b.onAdClosed(this.f3142a);
        }

        public final void onAdFailedToLoad(int i) {
            this.b.onAdFailedToLoad(this.f3142a, i);
        }

        public final void onAdImpression() {
            this.b.onAdImpression(this.f3142a);
        }

        public final void onAdLeftApplication() {
            this.b.onAdLeftApplication(this.f3142a);
        }

        public final void onAdLoaded() {
        }

        public final void onAdOpened() {
            this.b.onAdOpened(this.f3142a);
        }

        public final void onAppInstallAdLoaded(NativeAppInstallAd nativeAppInstallAd) {
            this.b.onAdLoaded((MediationNativeAdapter) this.f3142a, (NativeAdMapper) new zza(nativeAppInstallAd));
        }

        public final void onContentAdLoaded(NativeContentAd nativeContentAd) {
            this.b.onAdLoaded((MediationNativeAdapter) this.f3142a, (NativeAdMapper) new zzc(nativeContentAd));
        }

        public final void onCustomClick(NativeCustomTemplateAd nativeCustomTemplateAd, String str) {
            this.b.zza(this.f3142a, nativeCustomTemplateAd, str);
        }

        public final void onCustomTemplateAdLoaded(NativeCustomTemplateAd nativeCustomTemplateAd) {
            this.b.zza(this.f3142a, nativeCustomTemplateAd);
        }

        public final void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
            this.b.onAdLoaded((MediationNativeAdapter) this.f3142a, (UnifiedNativeAdMapper) new zzb(unifiedNativeAd));
        }
    }

    private final AdRequest zza(Context context, MediationAdRequest mediationAdRequest, Bundle bundle, Bundle bundle2) {
        AdRequest.Builder builder = new AdRequest.Builder();
        Date birthday = mediationAdRequest.getBirthday();
        if (birthday != null) {
            builder.setBirthday(birthday);
        }
        int gender = mediationAdRequest.getGender();
        if (gender != 0) {
            builder.setGender(gender);
        }
        Set<String> keywords = mediationAdRequest.getKeywords();
        if (keywords != null) {
            for (String addKeyword : keywords) {
                builder.addKeyword(addKeyword);
            }
        }
        Location location = mediationAdRequest.getLocation();
        if (location != null) {
            builder.setLocation(location);
        }
        if (mediationAdRequest.isTesting()) {
            zzve.zzou();
            builder.addTestDevice(zzayk.zzbi(context));
        }
        if (mediationAdRequest.taggedForChildDirectedTreatment() != -1) {
            boolean z = true;
            if (mediationAdRequest.taggedForChildDirectedTreatment() != 1) {
                z = false;
            }
            builder.tagForChildDirectedTreatment(z);
        }
        builder.setIsDesignedForFamilies(mediationAdRequest.isDesignedForFamilies());
        builder.addNetworkExtrasBundle(AdMobAdapter.class, zza(bundle, bundle2));
        return builder.build();
    }

    public String getAdUnitId(Bundle bundle) {
        return bundle.getString(AD_UNIT_ID_PARAMETER);
    }

    public View getBannerView() {
        return this.zzlq;
    }

    public Bundle getInterstitialAdapterInfo() {
        return new MediationAdapter.zza().zzdf(1).zzaby();
    }

    public zzxb getVideoController() {
        VideoController videoController;
        AdView adView = this.zzlq;
        if (adView == null || (videoController = adView.getVideoController()) == null) {
            return null;
        }
        return videoController.zzdl();
    }

    public void initialize(Context context, MediationAdRequest mediationAdRequest, String str, MediationRewardedVideoAdListener mediationRewardedVideoAdListener, Bundle bundle, Bundle bundle2) {
        this.zzlt = context.getApplicationContext();
        this.zzlv = mediationRewardedVideoAdListener;
        this.zzlv.onInitializationSucceeded(this);
    }

    public boolean isInitialized() {
        return this.zzlv != null;
    }

    public void loadAd(MediationAdRequest mediationAdRequest, Bundle bundle, Bundle bundle2) {
        Context context = this.zzlt;
        if (context == null || this.zzlv == null) {
            zzayu.zzex("AdMobAdapter.loadAd called before initialize.");
            return;
        }
        this.zzlu = new InterstitialAd(context);
        this.zzlu.zzd(true);
        this.zzlu.setAdUnitId(getAdUnitId(bundle));
        this.zzlu.setRewardedVideoAdListener(this.zzlw);
        this.zzlu.setAdMetadataListener(new zzb(this));
        this.zzlu.loadAd(zza(this.zzlt, mediationAdRequest, bundle2, bundle));
    }

    public void onDestroy() {
        AdView adView = this.zzlq;
        if (adView != null) {
            adView.destroy();
            this.zzlq = null;
        }
        if (this.zzlr != null) {
            this.zzlr = null;
        }
        if (this.zzls != null) {
            this.zzls = null;
        }
        if (this.zzlu != null) {
            this.zzlu = null;
        }
    }

    public void onImmersiveModeUpdated(boolean z) {
        InterstitialAd interstitialAd = this.zzlr;
        if (interstitialAd != null) {
            interstitialAd.setImmersiveMode(z);
        }
        InterstitialAd interstitialAd2 = this.zzlu;
        if (interstitialAd2 != null) {
            interstitialAd2.setImmersiveMode(z);
        }
    }

    public void onPause() {
        AdView adView = this.zzlq;
        if (adView != null) {
            adView.pause();
        }
    }

    public void onResume() {
        AdView adView = this.zzlq;
        if (adView != null) {
            adView.resume();
        }
    }

    public void requestBannerAd(Context context, MediationBannerListener mediationBannerListener, Bundle bundle, AdSize adSize, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.zzlq = new AdView(context);
        this.zzlq.setAdSize(new AdSize(adSize.getWidth(), adSize.getHeight()));
        this.zzlq.setAdUnitId(getAdUnitId(bundle));
        this.zzlq.setAdListener(new zze(this, mediationBannerListener));
        this.zzlq.loadAd(zza(context, mediationAdRequest, bundle2, bundle));
    }

    public void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.zzlr = new InterstitialAd(context);
        this.zzlr.setAdUnitId(getAdUnitId(bundle));
        this.zzlr.setAdListener(new zzd(this, mediationInterstitialListener));
        this.zzlr.loadAd(zza(context, mediationAdRequest, bundle2, bundle));
    }

    public void requestNativeAd(Context context, MediationNativeListener mediationNativeListener, Bundle bundle, NativeMediationAdRequest nativeMediationAdRequest, Bundle bundle2) {
        zzf zzf2 = new zzf(this, mediationNativeListener);
        AdLoader.Builder withAdListener = new AdLoader.Builder(context, bundle.getString(AD_UNIT_ID_PARAMETER)).withAdListener(zzf2);
        NativeAdOptions nativeAdOptions = nativeMediationAdRequest.getNativeAdOptions();
        if (nativeAdOptions != null) {
            withAdListener.withNativeAdOptions(nativeAdOptions);
        }
        if (nativeMediationAdRequest.isUnifiedNativeAdRequested()) {
            withAdListener.forUnifiedNativeAd(zzf2);
        }
        if (nativeMediationAdRequest.isAppInstallAdRequested()) {
            withAdListener.forAppInstallAd(zzf2);
        }
        if (nativeMediationAdRequest.isContentAdRequested()) {
            withAdListener.forContentAd(zzf2);
        }
        if (nativeMediationAdRequest.zzsz()) {
            for (String next : nativeMediationAdRequest.zzta().keySet()) {
                withAdListener.forCustomTemplateAd(next, zzf2, nativeMediationAdRequest.zzta().get(next).booleanValue() ? zzf2 : null);
            }
        }
        this.zzls = withAdListener.build();
        this.zzls.loadAd(zza(context, nativeMediationAdRequest, bundle2, bundle));
    }

    public void showInterstitial() {
        this.zzlr.show();
    }

    public void showVideo() {
        this.zzlu.show();
    }

    /* access modifiers changed from: protected */
    public abstract Bundle zza(Bundle bundle, Bundle bundle2);
}
