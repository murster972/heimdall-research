package com.google.ads.mediation.customevent;

import android.app.Activity;
import android.view.View;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.ads.zzayu;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter<CustomEventExtras, CustomEventServerParameters>, MediationInterstitialAdapter<CustomEventExtras, CustomEventServerParameters> {

    /* renamed from: a  reason: collision with root package name */
    private View f3144a;
    private CustomEventBanner b;
    private CustomEventInterstitial c;

    class zza implements CustomEventInterstitialListener {

        /* renamed from: a  reason: collision with root package name */
        private final CustomEventAdapter f3145a;
        private final MediationInterstitialListener b;

        public zza(CustomEventAdapter customEventAdapter, MediationInterstitialListener mediationInterstitialListener) {
            this.f3145a = customEventAdapter;
            this.b = mediationInterstitialListener;
        }

        public final void onDismissScreen() {
            zzayu.zzea("Custom event adapter called onDismissScreen.");
            this.b.onDismissScreen(this.f3145a);
        }

        public final void onFailedToReceiveAd() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.b.onFailedToReceiveAd(this.f3145a, AdRequest.ErrorCode.NO_FILL);
        }

        public final void onLeaveApplication() {
            zzayu.zzea("Custom event adapter called onLeaveApplication.");
            this.b.onLeaveApplication(this.f3145a);
        }

        public final void onPresentScreen() {
            zzayu.zzea("Custom event adapter called onPresentScreen.");
            this.b.onPresentScreen(this.f3145a);
        }

        public final void onReceivedAd() {
            zzayu.zzea("Custom event adapter called onReceivedAd.");
            this.b.onReceivedAd(CustomEventAdapter.this);
        }
    }

    static final class zzb implements CustomEventBannerListener {

        /* renamed from: a  reason: collision with root package name */
        private final CustomEventAdapter f3146a;
        private final MediationBannerListener b;

        public zzb(CustomEventAdapter customEventAdapter, MediationBannerListener mediationBannerListener) {
            this.f3146a = customEventAdapter;
            this.b = mediationBannerListener;
        }

        public final void onClick() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.b.onClick(this.f3146a);
        }

        public final void onDismissScreen() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.b.onDismissScreen(this.f3146a);
        }

        public final void onFailedToReceiveAd() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.b.onFailedToReceiveAd(this.f3146a, AdRequest.ErrorCode.NO_FILL);
        }

        public final void onLeaveApplication() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.b.onLeaveApplication(this.f3146a);
        }

        public final void onPresentScreen() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.b.onPresentScreen(this.f3146a);
        }

        public final void onReceivedAd(View view) {
            zzayu.zzea("Custom event adapter called onReceivedAd.");
            this.f3146a.a(view);
            this.b.onReceivedAd(this.f3146a);
        }
    }

    private static <T> T a(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            String message = th.getMessage();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 46 + String.valueOf(message).length());
            sb.append("Could not instantiate custom event adapter: ");
            sb.append(str);
            sb.append(". ");
            sb.append(message);
            zzayu.zzez(sb.toString());
            return null;
        }
    }

    public final void destroy() {
        CustomEventBanner customEventBanner = this.b;
        if (customEventBanner != null) {
            customEventBanner.destroy();
        }
        CustomEventInterstitial customEventInterstitial = this.c;
        if (customEventInterstitial != null) {
            customEventInterstitial.destroy();
        }
    }

    public final Class<CustomEventExtras> getAdditionalParametersType() {
        return CustomEventExtras.class;
    }

    public final View getBannerView() {
        return this.f3144a;
    }

    public final Class<CustomEventServerParameters> getServerParametersType() {
        return CustomEventServerParameters.class;
    }

    public final void requestBannerAd(MediationBannerListener mediationBannerListener, Activity activity, CustomEventServerParameters customEventServerParameters, AdSize adSize, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        Object obj;
        this.b = (CustomEventBanner) a(customEventServerParameters.className);
        if (this.b == null) {
            mediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        if (customEventExtras == null) {
            obj = null;
        } else {
            obj = customEventExtras.getExtra(customEventServerParameters.label);
        }
        Activity activity2 = activity;
        this.b.requestBannerAd(new zzb(this, mediationBannerListener), activity2, customEventServerParameters.label, customEventServerParameters.parameter, adSize, mediationAdRequest, obj);
    }

    public final void requestInterstitialAd(MediationInterstitialListener mediationInterstitialListener, Activity activity, CustomEventServerParameters customEventServerParameters, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        Object obj;
        this.c = (CustomEventInterstitial) a(customEventServerParameters.className);
        if (this.c == null) {
            mediationInterstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        if (customEventExtras == null) {
            obj = null;
        } else {
            obj = customEventExtras.getExtra(customEventServerParameters.label);
        }
        Activity activity2 = activity;
        this.c.requestInterstitialAd(new zza(this, mediationInterstitialListener), activity2, customEventServerParameters.label, customEventServerParameters.parameter, mediationAdRequest, obj);
    }

    public final void showInterstitial() {
        this.c.showInterstitial();
    }

    /* access modifiers changed from: private */
    public final void a(View view) {
        this.f3144a = view;
    }
}
