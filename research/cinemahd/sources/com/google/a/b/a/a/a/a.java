package com.google.a.b.a.a.a;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface a extends IInterface {
    void a(String str, Bundle bundle, d dVar) throws RemoteException;

    void a(String str, List<Bundle> list, Bundle bundle, d dVar) throws RemoteException;
}
