package com.google.a.b.a.a.a;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.a.a.b;
import com.google.a.a.c;

public abstract class e extends b implements d {
    public e() {
        super("com.google.android.play.core.install.protocol.IInstallServiceCallback");
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, Parcel parcel) throws RemoteException {
        if (i == 1) {
            a((Bundle) c.a(parcel, Bundle.CREATOR));
        } else if (i == 2) {
            d((Bundle) c.a(parcel, Bundle.CREATOR));
        } else if (i != 3) {
            return false;
        } else {
            c.a(parcel, Bundle.CREATOR);
            a();
        }
        return true;
    }
}
