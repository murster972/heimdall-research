package com.google.zxing;

public abstract class ReaderException extends Exception {
    static {
        String property = System.getProperty("surefire.test.class.path");
    }

    ReaderException() {
    }

    public final Throwable fillInStackTrace() {
        return null;
    }
}
