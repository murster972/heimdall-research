package com.google.zxing.qrcode.encoder;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.decoder.Mode;
import com.google.zxing.qrcode.decoder.Version;
import com.original.tase.model.socket.UserResponces;

public final class QRCode {

    /* renamed from: a  reason: collision with root package name */
    private Mode f4470a;
    private ErrorCorrectionLevel b;
    private Version c;
    private int d = -1;
    private ByteMatrix e;

    public static boolean b(int i) {
        return i >= 0 && i < 8;
    }

    public ByteMatrix a() {
        return this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(UserResponces.USER_RESPONCE_SUCCSES);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.f4470a);
        sb.append("\n ecLevel: ");
        sb.append(this.b);
        sb.append("\n version: ");
        sb.append(this.c);
        sb.append("\n maskPattern: ");
        sb.append(this.d);
        if (this.e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.e);
        }
        sb.append(">>\n");
        return sb.toString();
    }

    public void a(Mode mode) {
        this.f4470a = mode;
    }

    public void a(ErrorCorrectionLevel errorCorrectionLevel) {
        this.b = errorCorrectionLevel;
    }

    public void a(Version version) {
        this.c = version;
    }

    public void a(int i) {
        this.d = i;
    }

    public void a(ByteMatrix byteMatrix) {
        this.e = byteMatrix;
    }
}
