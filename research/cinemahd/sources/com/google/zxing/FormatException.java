package com.google.zxing;

public final class FormatException extends ReaderException {
    static {
        new FormatException();
    }

    private FormatException() {
    }
}
