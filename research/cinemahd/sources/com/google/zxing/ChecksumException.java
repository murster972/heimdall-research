package com.google.zxing;

public final class ChecksumException extends ReaderException {
    static {
        new ChecksumException();
    }

    private ChecksumException() {
    }
}
