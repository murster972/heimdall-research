package com.google.zxing.common;

import java.util.Arrays;

public final class BitMatrix implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final int f4455a;
    private final int b;
    private final int c;
    private final int[] d;

    public BitMatrix(int i, int i2) {
        if (i < 1 || i2 < 1) {
            throw new IllegalArgumentException("Both dimensions must be greater than 0");
        }
        this.f4455a = i;
        this.b = i2;
        this.c = (i + 31) / 32;
        this.d = new int[(this.c * i2)];
    }

    public boolean a(int i, int i2) {
        return ((this.d[(i2 * this.c) + (i / 32)] >>> (i & 31)) & 1) != 0;
    }

    public int b() {
        return this.f4455a;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BitMatrix)) {
            return false;
        }
        BitMatrix bitMatrix = (BitMatrix) obj;
        if (this.f4455a == bitMatrix.f4455a && this.b == bitMatrix.b && this.c == bitMatrix.c && Arrays.equals(this.d, bitMatrix.d)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = this.f4455a;
        return (((((((i * 31) + i) * 31) + this.b) * 31) + this.c) * 31) + Arrays.hashCode(this.d);
    }

    public String toString() {
        return a("X ", "  ");
    }

    public BitMatrix clone() {
        return new BitMatrix(this.f4455a, this.b, this.c, (int[]) this.d.clone());
    }

    public void a(int i, int i2, int i3, int i4) {
        if (i2 < 0 || i < 0) {
            throw new IllegalArgumentException("Left and top must be nonnegative");
        } else if (i4 < 1 || i3 < 1) {
            throw new IllegalArgumentException("Height and width must be at least 1");
        } else {
            int i5 = i3 + i;
            int i6 = i4 + i2;
            if (i6 > this.b || i5 > this.f4455a) {
                throw new IllegalArgumentException("The region must fit inside the matrix");
            }
            while (i2 < i6) {
                int i7 = this.c * i2;
                for (int i8 = i; i8 < i5; i8++) {
                    int[] iArr = this.d;
                    int i9 = (i8 / 32) + i7;
                    iArr[i9] = iArr[i9] | (1 << (i8 & 31));
                }
                i2++;
            }
        }
    }

    private BitMatrix(int i, int i2, int i3, int[] iArr) {
        this.f4455a = i;
        this.b = i2;
        this.c = i3;
        this.d = iArr;
    }

    public int a() {
        return this.b;
    }

    public String a(String str, String str2) {
        return a(str, str2, System.lineSeparator());
    }

    public String a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(this.b * (this.f4455a + 1));
        for (int i = 0; i < this.b; i++) {
            for (int i2 = 0; i2 < this.f4455a; i2++) {
                sb.append(a(i2, i) ? str : str2);
            }
            sb.append(str3);
        }
        return sb.toString();
    }
}
