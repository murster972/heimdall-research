package com.google.zxing.common;

import java.util.Arrays;

public final class BitArray implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int[] f4454a;
    private int b;

    public BitArray() {
        this.b = 0;
        this.f4454a = new int[1];
    }

    private static int[] c(int i) {
        return new int[((i + 31) / 32)];
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return (this.b + 7) / 8;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BitArray)) {
            return false;
        }
        BitArray bitArray = (BitArray) obj;
        if (this.b != bitArray.b || !Arrays.equals(this.f4454a, bitArray.f4454a)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.b * 31) + Arrays.hashCode(this.f4454a);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.b);
        for (int i = 0; i < this.b; i++) {
            if ((i & 7) == 0) {
                sb.append(' ');
            }
            sb.append(a(i) ? 'X' : '.');
        }
        return sb.toString();
    }

    private void b(int i) {
        if (i > this.f4454a.length * 32) {
            int[] c = c(i);
            int[] iArr = this.f4454a;
            System.arraycopy(iArr, 0, c, 0, iArr.length);
            this.f4454a = c;
        }
    }

    public boolean a(int i) {
        return ((1 << (i & 31)) & this.f4454a[i / 32]) != 0;
    }

    public BitArray clone() {
        return new BitArray((int[]) this.f4454a.clone(), this.b);
    }

    public void a(boolean z) {
        b(this.b + 1);
        if (z) {
            int[] iArr = this.f4454a;
            int i = this.b;
            int i2 = i / 32;
            iArr[i2] = (1 << (i & 31)) | iArr[i2];
        }
        this.b++;
    }

    BitArray(int[] iArr, int i) {
        this.f4454a = iArr;
        this.b = i;
    }

    public void a(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        b(this.b + i2);
        while (i2 > 0) {
            boolean z = true;
            if (((i >> (i2 - 1)) & 1) != 1) {
                z = false;
            }
            a(z);
            i2--;
        }
    }

    public void b(BitArray bitArray) {
        if (this.f4454a.length == bitArray.f4454a.length) {
            int i = 0;
            while (true) {
                int[] iArr = this.f4454a;
                if (i < iArr.length) {
                    iArr[i] = iArr[i] ^ bitArray.f4454a[i];
                    i++;
                } else {
                    return;
                }
            }
        } else {
            throw new IllegalArgumentException("Sizes don't match");
        }
    }

    public void a(BitArray bitArray) {
        int i = bitArray.b;
        b(this.b + i);
        for (int i2 = 0; i2 < i; i2++) {
            a(bitArray.a(i2));
        }
    }

    public void a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        int i5 = 0;
        while (i5 < i3) {
            int i6 = i4;
            int i7 = 0;
            for (int i8 = 0; i8 < 8; i8++) {
                if (a(i6)) {
                    i7 |= 1 << (7 - i8);
                }
                i6++;
            }
            bArr[i2 + i5] = (byte) i7;
            i5++;
            i4 = i6;
        }
    }
}
