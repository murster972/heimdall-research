package com.google.zxing.common.reedsolomon;

final class GenericGFPoly {

    /* renamed from: a  reason: collision with root package name */
    private final GenericGF f4458a;
    private final int[] b;

    GenericGFPoly(GenericGF genericGF, int[] iArr) {
        if (iArr.length != 0) {
            this.f4458a = genericGF;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.b = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.b = new int[]{0};
                return;
            }
            this.b = new int[(length - i)];
            int[] iArr2 = this.b;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    public int[] a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b.length - 1;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.b[0] == 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(b() * 8);
        for (int b2 = b(); b2 >= 0; b2--) {
            int a2 = a(b2);
            if (a2 != 0) {
                if (a2 < 0) {
                    sb.append(" - ");
                    a2 = -a2;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (b2 == 0 || a2 != 1) {
                    int c = this.f4458a.c(a2);
                    if (c == 0) {
                        sb.append('1');
                    } else if (c == 1) {
                        sb.append('a');
                    } else {
                        sb.append("a^");
                        sb.append(c);
                    }
                }
                if (b2 != 0) {
                    if (b2 == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(b2);
                    }
                }
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public int a(int i) {
        int[] iArr = this.b;
        return iArr[(iArr.length - 1) - i];
    }

    /* access modifiers changed from: package-private */
    public GenericGFPoly[] b(GenericGFPoly genericGFPoly) {
        if (!this.f4458a.equals(genericGFPoly.f4458a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (!genericGFPoly.c()) {
            GenericGFPoly b2 = this.f4458a.b();
            int b3 = this.f4458a.b(genericGFPoly.a(genericGFPoly.b()));
            GenericGFPoly genericGFPoly2 = b2;
            GenericGFPoly genericGFPoly3 = this;
            while (genericGFPoly3.b() >= genericGFPoly.b() && !genericGFPoly3.c()) {
                int b4 = genericGFPoly3.b() - genericGFPoly.b();
                int b5 = this.f4458a.b(genericGFPoly3.a(genericGFPoly3.b()), b3);
                GenericGFPoly a2 = genericGFPoly.a(b4, b5);
                genericGFPoly2 = genericGFPoly2.a(this.f4458a.a(b4, b5));
                genericGFPoly3 = genericGFPoly3.a(a2);
            }
            return new GenericGFPoly[]{genericGFPoly2, genericGFPoly3};
        } else {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    /* access modifiers changed from: package-private */
    public GenericGFPoly c(GenericGFPoly genericGFPoly) {
        if (!this.f4458a.equals(genericGFPoly.f4458a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c() || genericGFPoly.c()) {
            return this.f4458a.b();
        } else {
            int[] iArr = this.b;
            int length = iArr.length;
            int[] iArr2 = genericGFPoly.b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    int i4 = i + i3;
                    iArr3[i4] = GenericGF.c(iArr3[i4], this.f4458a.b(i2, iArr2[i3]));
                }
            }
            return new GenericGFPoly(this.f4458a, iArr3);
        }
    }

    /* access modifiers changed from: package-private */
    public GenericGFPoly a(GenericGFPoly genericGFPoly) {
        if (!this.f4458a.equals(genericGFPoly.f4458a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c()) {
            return genericGFPoly;
        } else {
            if (genericGFPoly.c()) {
                return this;
            }
            int[] iArr = this.b;
            int[] iArr2 = genericGFPoly.b;
            if (iArr.length > iArr2.length) {
                int[] iArr3 = iArr;
                iArr = iArr2;
                iArr2 = iArr3;
            }
            int[] iArr4 = new int[iArr2.length];
            int length = iArr2.length - iArr.length;
            System.arraycopy(iArr2, 0, iArr4, 0, length);
            for (int i = length; i < iArr2.length; i++) {
                iArr4[i] = GenericGF.c(iArr[i - length], iArr2[i]);
            }
            return new GenericGFPoly(this.f4458a, iArr4);
        }
    }

    /* access modifiers changed from: package-private */
    public GenericGFPoly a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.f4458a.b();
        } else {
            int length = this.b.length;
            int[] iArr = new int[(i + length)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.f4458a.b(this.b[i3], i2);
            }
            return new GenericGFPoly(this.f4458a, iArr);
        }
    }
}
