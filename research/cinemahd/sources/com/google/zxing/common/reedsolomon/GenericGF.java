package com.google.zxing.common.reedsolomon;

import com.facebook.common.util.ByteConstants;

public final class GenericGF {
    public static final GenericGF g = new GenericGF(67, 64, 1);
    public static final GenericGF h = new GenericGF(285, 256, 0);
    public static final GenericGF i = new GenericGF(301, 256, 1);

    /* renamed from: a  reason: collision with root package name */
    private final int[] f4457a;
    private final int[] b;
    private final GenericGFPoly c;
    private final int d;
    private final int e;
    private final int f;

    static {
        new GenericGF(4201, 4096, 1);
        new GenericGF(1033, ByteConstants.KB, 1);
        new GenericGF(19, 16, 1);
    }

    public GenericGF(int i2, int i3, int i4) {
        this.e = i2;
        this.d = i3;
        this.f = i4;
        this.f4457a = new int[i3];
        this.b = new int[i3];
        int i5 = 1;
        for (int i6 = 0; i6 < i3; i6++) {
            this.f4457a[i6] = i5;
            i5 *= 2;
            if (i5 >= i3) {
                i5 = (i5 ^ i2) & (i3 - 1);
            }
        }
        for (int i7 = 0; i7 < i3 - 1; i7++) {
            this.b[this.f4457a[i7]] = i7;
        }
        this.c = new GenericGFPoly(this, new int[]{0});
        new GenericGFPoly(this, new int[]{1});
    }

    static int c(int i2, int i3) {
        return i2 ^ i3;
    }

    /* access modifiers changed from: package-private */
    public GenericGFPoly a(int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException();
        } else if (i3 == 0) {
            return this.c;
        } else {
            int[] iArr = new int[(i2 + 1)];
            iArr[0] = i3;
            return new GenericGFPoly(this, iArr);
        }
    }

    /* access modifiers changed from: package-private */
    public GenericGFPoly b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int c(int i2) {
        if (i2 != 0) {
            return this.b[i2];
        }
        throw new IllegalArgumentException();
    }

    public String toString() {
        return "GF(0x" + Integer.toHexString(this.e) + ',' + this.d + ')';
    }

    /* access modifiers changed from: package-private */
    public int b(int i2) {
        if (i2 != 0) {
            return this.f4457a[(this.d - this.b[i2]) - 1];
        }
        throw new ArithmeticException();
    }

    /* access modifiers changed from: package-private */
    public int b(int i2, int i3) {
        if (i2 == 0 || i3 == 0) {
            return 0;
        }
        int[] iArr = this.f4457a;
        int[] iArr2 = this.b;
        return iArr[(iArr2[i2] + iArr2[i3]) % (this.d - 1)];
    }

    /* access modifiers changed from: package-private */
    public int a(int i2) {
        return this.f4457a[i2];
    }

    public int a() {
        return this.f;
    }
}
