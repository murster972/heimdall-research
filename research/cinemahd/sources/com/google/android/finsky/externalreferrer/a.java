package com.google.android.finsky.externalreferrer;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface a extends IInterface {

    /* renamed from: com.google.android.finsky.externalreferrer.a$a  reason: collision with other inner class name */
    public static abstract class C0031a extends Binder implements a {

        /* renamed from: com.google.android.finsky.externalreferrer.a$a$a  reason: collision with other inner class name */
        static class C0032a implements a {

            /* renamed from: a  reason: collision with root package name */
            private IBinder f3674a;

            C0032a(IBinder iBinder) {
                this.f3674a = iBinder;
            }

            public final Bundle a(Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3674a.transact(1, obtain, obtain2, 0)) {
                        C0031a.a();
                    }
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public final IBinder asBinder() {
                return this.f3674a;
            }
        }

        public static a a() {
            return null;
        }

        public static a a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
                return new C0032a(iBinder);
            }
            return (a) queryLocalInterface;
        }
    }

    Bundle a(Bundle bundle) throws RemoteException;
}
