package com.google.android.exoplayer2.scheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.PowerManager;
import com.google.android.exoplayer2.util.Util;
import com.vungle.warren.model.ReportDBAdapter;

public final class Requirements {

    /* renamed from: a  reason: collision with root package name */
    private final int f3397a;

    public Requirements(int i) {
        this.f3397a = i;
    }

    private static void a(String str) {
    }

    private boolean d(Context context) {
        int a2 = a();
        if (a2 == 0) {
            return true;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            a("No network info or no connection.");
            return false;
        } else if (!a(connectivityManager)) {
            return false;
        } else {
            if (a2 == 1) {
                return true;
            }
            if (a2 == 3) {
                boolean isRoaming = activeNetworkInfo.isRoaming();
                a("Roaming: " + isRoaming);
                return !isRoaming;
            }
            boolean a3 = a(connectivityManager, activeNetworkInfo);
            a("Metered network: " + a3);
            if (a2 == 2) {
                return !a3;
            }
            if (a2 == 4) {
                return a3;
            }
            throw new IllegalStateException();
        }
    }

    public int a() {
        return this.f3397a & 7;
    }

    public boolean b() {
        return (this.f3397a & 16) != 0;
    }

    public boolean c() {
        return (this.f3397a & 8) != 0;
    }

    public String toString() {
        return super.toString();
    }

    private boolean b(Context context) {
        if (!b()) {
            return true;
        }
        Intent registerReceiver = context.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return false;
        }
        int intExtra = registerReceiver.getIntExtra(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, -1);
        if (intExtra == 2 || intExtra == 5) {
            return true;
        }
        return false;
    }

    private boolean c(Context context) {
        if (!c()) {
            return true;
        }
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        int i = Util.f3651a;
        if (i >= 23) {
            return powerManager.isDeviceIdleMode();
        }
        if (i >= 20) {
            if (!powerManager.isInteractive()) {
                return true;
            }
        } else if (!powerManager.isScreenOn()) {
            return true;
        }
        return false;
    }

    public boolean a(Context context) {
        return d(context) && b(context) && c(context);
    }

    private static boolean a(ConnectivityManager connectivityManager) {
        if (Util.f3651a < 23) {
            return true;
        }
        Network activeNetwork = connectivityManager.getActiveNetwork();
        boolean z = false;
        if (activeNetwork == null) {
            a("No active network.");
            return false;
        }
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
        if (networkCapabilities == null || !networkCapabilities.hasCapability(16)) {
            z = true;
        }
        a("Network capability validated: " + z);
        return !z;
    }

    private static boolean a(ConnectivityManager connectivityManager, NetworkInfo networkInfo) {
        if (Util.f3651a >= 16) {
            return connectivityManager.isActiveNetworkMetered();
        }
        int type = networkInfo.getType();
        return (type == 1 || type == 7 || type == 9) ? false : true;
    }
}
