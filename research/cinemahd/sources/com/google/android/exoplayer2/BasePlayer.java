package com.google.android.exoplayer2;

import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.util.Util;

public abstract class BasePlayer implements Player {

    /* renamed from: a  reason: collision with root package name */
    protected final Timeline.Window f3149a = new Timeline.Window();

    private int v() {
        int repeatMode = getRepeatMode();
        if (repeatMode == 1) {
            return 0;
        }
        return repeatMode;
    }

    public final void a(long j) {
        a(f(), j);
    }

    public final void b(int i) {
        a(i, -9223372036854775807L);
    }

    public final int p() {
        Timeline i = i();
        if (i.c()) {
            return -1;
        }
        return i.b(f(), v(), r());
    }

    public final int q() {
        Timeline i = i();
        if (i.c()) {
            return -1;
        }
        return i.a(f(), v(), r());
    }

    public final long s() {
        Timeline i = i();
        if (i.c()) {
            return -9223372036854775807L;
        }
        return i.a(f(), this.f3149a).c();
    }

    public final void t() {
        b(f());
    }

    public final void u() {
        c(false);
    }

    public final int a() {
        long bufferedPosition = getBufferedPosition();
        long duration = getDuration();
        if (bufferedPosition == -9223372036854775807L || duration == -9223372036854775807L) {
            return 0;
        }
        if (duration == 0) {
            return 100;
        }
        return Util.a((int) ((bufferedPosition * 100) / duration), 0, 100);
    }
}
