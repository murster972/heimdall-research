package com.google.android.exoplayer2.ext.ffmpeg;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.decoder.SimpleDecoder;
import com.google.android.exoplayer2.decoder.SimpleOutputBuffer;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.ar.core.ImageMetadata;
import java.nio.ByteBuffer;

final class FfmpegDecoder extends SimpleDecoder<DecoderInputBuffer, SimpleOutputBuffer, FfmpegDecoderException> {
    private final String n;
    private final byte[] o;
    private final int p;
    private final int q;
    private long r;
    private boolean s;
    private volatile int t;
    private volatile int u;

    public FfmpegDecoder(int i, int i2, int i3, Format format, boolean z) throws FfmpegDecoderException {
        super(new DecoderInputBuffer[i], new SimpleOutputBuffer[i2]);
        if (FfmpegLibrary.b()) {
            Assertions.a(format.g);
            String a2 = FfmpegLibrary.a(format.g, format.v);
            Assertions.a(a2);
            this.n = a2;
            this.o = a(format.g, format.i);
            this.p = z ? 4 : 2;
            this.q = z ? 131072 : ImageMetadata.CONTROL_AE_ANTIBANDING_MODE;
            this.r = ffmpegInitialize(this.n, this.o, z, format.u, format.t);
            if (this.r != 0) {
                a(i3);
                return;
            }
            throw new FfmpegDecoderException("Initialization failed.");
        }
        throw new FfmpegDecoderException("Failed to load decoder native libraries.");
    }

    private native int ffmpegDecode(long j, ByteBuffer byteBuffer, int i, ByteBuffer byteBuffer2, int i2);

    private native int ffmpegGetChannelCount(long j);

    private native int ffmpegGetSampleRate(long j);

    private native long ffmpegInitialize(String str, byte[] bArr, boolean z, int i, int i2);

    private native void ffmpegRelease(long j);

    private native long ffmpegReset(long j, byte[] bArr);

    /* access modifiers changed from: protected */
    public DecoderInputBuffer c() {
        return new DecoderInputBuffer(2);
    }

    public int e() {
        return this.t;
    }

    public int f() {
        return this.p;
    }

    public int g() {
        return this.u;
    }

    public String getName() {
        return "ffmpeg" + FfmpegLibrary.a() + "-" + this.n;
    }

    public void release() {
        super.release();
        ffmpegRelease(this.r);
        this.r = 0;
    }

    /* access modifiers changed from: protected */
    public SimpleOutputBuffer d() {
        return new SimpleOutputBuffer(this);
    }

    /* access modifiers changed from: protected */
    public FfmpegDecoderException a(Throwable th) {
        return new FfmpegDecoderException("Unexpected decode error", th);
    }

    /* access modifiers changed from: protected */
    public FfmpegDecoderException a(DecoderInputBuffer decoderInputBuffer, SimpleOutputBuffer simpleOutputBuffer, boolean z) {
        if (z) {
            this.r = ffmpegReset(this.r, this.o);
            if (this.r == 0) {
                return new FfmpegDecoderException("Error resetting (see logcat).");
            }
        }
        ByteBuffer byteBuffer = decoderInputBuffer.b;
        int ffmpegDecode = ffmpegDecode(this.r, byteBuffer, byteBuffer.limit(), simpleOutputBuffer.init(decoderInputBuffer.c, this.q), this.q);
        if (ffmpegDecode < 0) {
            return new FfmpegDecoderException("Error decoding (see logcat). Code: " + ffmpegDecode);
        }
        if (!this.s) {
            this.t = ffmpegGetChannelCount(this.r);
            this.u = ffmpegGetSampleRate(this.r);
            if (this.u == 0 && "alac".equals(this.n)) {
                Assertions.a(this.o);
                ParsableByteArray parsableByteArray = new ParsableByteArray(this.o);
                parsableByteArray.e(this.o.length - 4);
                this.u = parsableByteArray.x();
            }
            this.s = true;
        }
        simpleOutputBuffer.b.position(0);
        simpleOutputBuffer.b.limit(ffmpegDecode);
        return null;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.lang.String r6, java.util.List<byte[]> r7) {
        /*
            int r0 = r6.hashCode()
            r1 = 3
            r2 = 2
            r3 = 1
            r4 = 0
            switch(r0) {
                case -1003765268: goto L_0x002a;
                case -53558318: goto L_0x0020;
                case 1504470054: goto L_0x0016;
                case 1504891608: goto L_0x000c;
                default: goto L_0x000b;
            }
        L_0x000b:
            goto L_0x0034
        L_0x000c:
            java.lang.String r0 = "audio/opus"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0034
            r6 = 2
            goto L_0x0035
        L_0x0016:
            java.lang.String r0 = "audio/alac"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0034
            r6 = 1
            goto L_0x0035
        L_0x0020:
            java.lang.String r0 = "audio/mp4a-latm"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0034
            r6 = 0
            goto L_0x0035
        L_0x002a:
            java.lang.String r0 = "audio/vorbis"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0034
            r6 = 3
            goto L_0x0035
        L_0x0034:
            r6 = -1
        L_0x0035:
            if (r6 == 0) goto L_0x0084
            if (r6 == r3) goto L_0x0084
            if (r6 == r2) goto L_0x0084
            if (r6 == r1) goto L_0x003f
            r6 = 0
            return r6
        L_0x003f:
            java.lang.Object r6 = r7.get(r4)
            byte[] r6 = (byte[]) r6
            java.lang.Object r7 = r7.get(r3)
            byte[] r7 = (byte[]) r7
            int r0 = r6.length
            int r5 = r7.length
            int r0 = r0 + r5
            int r0 = r0 + 6
            byte[] r0 = new byte[r0]
            int r5 = r6.length
            int r5 = r5 >> 8
            byte r5 = (byte) r5
            r0[r4] = r5
            int r5 = r6.length
            r5 = r5 & 255(0xff, float:3.57E-43)
            byte r5 = (byte) r5
            r0[r3] = r5
            int r3 = r6.length
            java.lang.System.arraycopy(r6, r4, r0, r2, r3)
            int r3 = r6.length
            int r3 = r3 + r2
            r0[r3] = r4
            int r2 = r6.length
            int r2 = r2 + r1
            r0[r2] = r4
            int r1 = r6.length
            int r1 = r1 + 4
            int r2 = r7.length
            int r2 = r2 >> 8
            byte r2 = (byte) r2
            r0[r1] = r2
            int r1 = r6.length
            int r1 = r1 + 5
            int r2 = r7.length
            r2 = r2 & 255(0xff, float:3.57E-43)
            byte r2 = (byte) r2
            r0[r1] = r2
            int r6 = r6.length
            int r6 = r6 + 6
            int r1 = r7.length
            java.lang.System.arraycopy(r7, r4, r0, r6, r1)
            return r0
        L_0x0084:
            java.lang.Object r6 = r7.get(r4)
            byte[] r6 = (byte[]) r6
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ext.ffmpeg.FfmpegDecoder.a(java.lang.String, java.util.List):byte[]");
    }
}
