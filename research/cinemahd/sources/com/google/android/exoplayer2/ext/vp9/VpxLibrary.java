package com.google.android.exoplayer2.ext.vp9;

import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.util.LibraryLoader;

public final class VpxLibrary {

    /* renamed from: a  reason: collision with root package name */
    private static final LibraryLoader f3242a = new LibraryLoader("vpx", "vpxJNI");

    static {
        ExoPlayerLibraryInfo.a("goog.exo.vpx");
    }

    private VpxLibrary() {
    }

    public static String a() {
        if (b()) {
            return vpxGetVersion();
        }
        return null;
    }

    public static boolean b() {
        return f3242a.a();
    }

    private static native String vpxGetBuildConfig();

    private static native String vpxGetVersion();

    public static native boolean vpxIsSecureDecodeSupported();
}
