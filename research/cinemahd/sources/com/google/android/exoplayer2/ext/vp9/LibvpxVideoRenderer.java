package com.google.android.exoplayer2.ext.vp9;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.Surface;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.TimedValueQueue;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoFrameMetadataListener;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

public class LibvpxVideoRenderer extends BaseRenderer {
    private DrmSession<ExoMediaCrypto> A;
    private DrmSession<ExoMediaCrypto> B;
    private int C;
    private boolean D;
    private Bitmap E;
    private boolean F;
    private long G;
    private long H;
    private Surface I;
    private VpxOutputBufferRenderer J;
    private int K;
    private boolean L;
    private boolean M;
    private boolean N;
    private int O;
    private int P;
    private long Q;
    private int R;
    private int S;
    private int T;
    private long U;
    private long V;
    private VideoFrameMetadataListener W;
    protected DecoderCounters X;
    private final boolean j;
    private final boolean k;
    private final long l;
    private final int m;
    private final boolean n;
    private final VideoRendererEventListener.EventDispatcher o;
    private final FormatHolder p;
    private final TimedValueQueue<Format> q;
    private final DecoderInputBuffer r;
    private final DrmSessionManager<ExoMediaCrypto> s;
    private final boolean t;
    private Format u;
    private Format v;
    private Format w;
    private VpxDecoder x;
    private VpxInputBuffer y;
    private VpxOutputBuffer z;

    public LibvpxVideoRenderer(boolean z2, long j2, Handler handler, VideoRendererEventListener videoRendererEventListener, int i) {
        this(z2, j2, handler, videoRendererEventListener, i, (DrmSessionManager<ExoMediaCrypto>) null, false, false, false);
    }

    private void A() {
        if (this.F) {
            this.o.b(this.I);
        }
    }

    private void B() {
        if (this.O != -1 || this.P != -1) {
            this.o.b(this.O, this.P, 0, 1.0f);
        }
    }

    private void C() {
        this.H = this.l > 0 ? SystemClock.elapsedRealtime() + this.l : -9223372036854775807L;
    }

    private static boolean e(long j2) {
        return j2 < -30000;
    }

    private boolean e(long j2, long j3) throws ExoPlaybackException, VpxDecoderException {
        if (this.z == null) {
            this.z = (VpxOutputBuffer) this.x.a();
            VpxOutputBuffer vpxOutputBuffer = this.z;
            if (vpxOutputBuffer == null) {
                return false;
            }
            DecoderCounters decoderCounters = this.X;
            int i = decoderCounters.f;
            int i2 = vpxOutputBuffer.skippedOutputBufferCount;
            decoderCounters.f = i + i2;
            this.T -= i2;
        }
        if (this.z.isEndOfStream()) {
            if (this.C == 2) {
                t();
                x();
            } else {
                this.z.release();
                this.z = null;
                this.N = true;
            }
            return false;
        }
        boolean f = f(j2, j3);
        if (f) {
            d(this.z.timeUs);
            this.z = null;
        }
        return f;
    }

    private static boolean f(long j2) {
        return j2 < -500000;
    }

    private boolean f(long j2, long j3) throws ExoPlaybackException, VpxDecoderException {
        long j4 = j2;
        long j5 = j3;
        if (this.G == -9223372036854775807L) {
            this.G = j4;
        }
        long j6 = this.z.timeUs;
        long j7 = j6 - j4;
        if (this.K != -1) {
            long j8 = j6 - this.V;
            Format a2 = this.q.a(j8);
            if (a2 != null) {
                this.w = a2;
            }
            long elapsedRealtime = SystemClock.elapsedRealtime() * 1000;
            boolean z2 = getState() == 2;
            if (!this.F || (z2 && d(j7, elapsedRealtime - this.U))) {
                VideoFrameMetadataListener videoFrameMetadataListener = this.W;
                if (videoFrameMetadataListener != null) {
                    videoFrameMetadataListener.a(j8, System.nanoTime(), this.w);
                }
                b(this.z);
                return true;
            } else if (!z2 || j4 == this.G) {
                return false;
            } else {
                if (b(j7, j5) && c(j2)) {
                    return false;
                }
                if (c(j7, j5)) {
                    a(this.z);
                    return true;
                } else if (j7 >= 30000) {
                    return false;
                } else {
                    VideoFrameMetadataListener videoFrameMetadataListener2 = this.W;
                    if (videoFrameMetadataListener2 != null) {
                        videoFrameMetadataListener2.a(j8, System.nanoTime(), this.w);
                    }
                    b(this.z);
                    return true;
                }
            }
        } else if (!e(j7)) {
            return false;
        } else {
            c(this.z);
            return true;
        }
    }

    private void u() {
        this.F = false;
    }

    private void v() {
        this.O = -1;
        this.P = -1;
    }

    private boolean w() throws VpxDecoderException, ExoPlaybackException {
        int i;
        VpxDecoder vpxDecoder = this.x;
        if (vpxDecoder == null || this.C == 2 || this.M) {
            return false;
        }
        if (this.y == null) {
            this.y = (VpxInputBuffer) vpxDecoder.b();
            if (this.y == null) {
                return false;
            }
        }
        if (this.C == 1) {
            this.y.setFlags(4);
            this.x.a(this.y);
            this.y = null;
            this.C = 2;
            return false;
        }
        if (this.L) {
            i = -4;
        } else {
            i = a(this.p, (DecoderInputBuffer) this.y, false);
        }
        if (i == -3) {
            return false;
        }
        if (i == -5) {
            b(this.p.f3165a);
            return true;
        } else if (this.y.isEndOfStream()) {
            this.M = true;
            this.x.a(this.y);
            this.y = null;
            return false;
        } else {
            this.L = b(this.y.c());
            if (this.L) {
                return false;
            }
            Format format = this.v;
            if (format != null) {
                this.q.a(this.y.c, format);
                this.v = null;
            }
            this.y.b();
            VpxInputBuffer vpxInputBuffer = this.y;
            vpxInputBuffer.e = this.p.f3165a.s;
            a(vpxInputBuffer);
            this.x.a(this.y);
            this.T++;
            this.D = true;
            this.X.c++;
            this.y = null;
            return true;
        }
    }

    private void x() throws ExoPlaybackException {
        if (this.x == null) {
            this.A = this.B;
            ExoMediaCrypto exoMediaCrypto = null;
            DrmSession<ExoMediaCrypto> drmSession = this.A;
            if (drmSession == null || (exoMediaCrypto = drmSession.a()) != null || this.A.b() != null) {
                ExoMediaCrypto exoMediaCrypto2 = exoMediaCrypto;
                try {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    TraceUtil.a("createVpxDecoder");
                    this.x = new VpxDecoder(8, 8, this.u.h != -1 ? this.u.h : 786432, exoMediaCrypto2, this.k, this.t);
                    this.x.b(this.K);
                    TraceUtil.a();
                    long elapsedRealtime2 = SystemClock.elapsedRealtime();
                    a(this.x.getName(), elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
                    this.X.f3215a++;
                } catch (VpxDecoderException e) {
                    throw ExoPlaybackException.a(e, m());
                }
            }
        }
    }

    private void y() {
        if (this.R > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.o.a(this.R, elapsedRealtime - this.Q);
            this.R = 0;
            this.Q = elapsedRealtime;
        }
    }

    private void z() {
        if (!this.F) {
            this.F = true;
            this.o.b(this.I);
        }
    }

    public int a(Format format) {
        if (!VpxLibrary.b() || !"video/x-vnd.on2.vp9".equalsIgnoreCase(format.g)) {
            return 0;
        }
        return !BaseRenderer.a((DrmSessionManager<?>) this.s, format.j) ? 2 : 20;
    }

    /* access modifiers changed from: protected */
    public void a(VpxInputBuffer vpxInputBuffer) {
    }

    /* access modifiers changed from: protected */
    public void b(Format format) throws ExoPlaybackException {
        Format format2 = this.u;
        this.u = format;
        this.v = format;
        if (!Util.a((Object) this.u.j, (Object) format2 == null ? null : format2.j)) {
            if (this.u.j != null) {
                DrmSessionManager<ExoMediaCrypto> drmSessionManager = this.s;
                if (drmSessionManager != null) {
                    this.B = drmSessionManager.a(Looper.myLooper(), this.u.j);
                    DrmSession<ExoMediaCrypto> drmSession = this.B;
                    if (drmSession == this.A) {
                        this.s.a(drmSession);
                    }
                } else {
                    throw ExoPlaybackException.a(new IllegalStateException("Media requires a DrmSessionManager"), m());
                }
            } else {
                this.B = null;
            }
        }
        if (this.B != this.A) {
            if (this.D) {
                this.C = 1;
            } else {
                t();
                x();
            }
        }
        this.o.a(this.u);
    }

    /* access modifiers changed from: protected */
    public boolean c(long j2, long j3) {
        return e(j2);
    }

    /* access modifiers changed from: protected */
    public void d(long j2) {
        this.T--;
    }

    public boolean isReady() {
        if (this.L) {
            return false;
        }
        if (this.u != null && ((o() || this.z != null) && (this.F || this.K == -1))) {
            this.H = -9223372036854775807L;
            return true;
        } else if (this.H == -9223372036854775807L) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.H) {
                return true;
            }
            this.H = -9223372036854775807L;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void p() {
        this.u = null;
        this.L = false;
        v();
        u();
        try {
            t();
            try {
                if (this.A != null) {
                    this.s.a(this.A);
                }
                try {
                    if (!(this.B == null || this.B == this.A)) {
                        this.s.a(this.B);
                    }
                } finally {
                    this.A = null;
                    this.B = null;
                    this.X.a();
                    this.o.a(this.X);
                }
            } catch (Throwable th) {
                if (!(this.B == null || this.B == this.A)) {
                    this.s.a(this.B);
                }
                throw th;
            } finally {
                this.A = null;
                this.B = null;
                this.X.a();
                this.o.a(this.X);
            }
        } catch (Throwable th2) {
            try {
                if (!(this.B == null || this.B == this.A)) {
                    this.s.a(this.B);
                }
                throw th2;
            } finally {
                this.A = null;
                this.B = null;
                this.X.a();
                this.o.a(this.X);
            }
        } finally {
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        this.R = 0;
        this.Q = SystemClock.elapsedRealtime();
        this.U = SystemClock.elapsedRealtime() * 1000;
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.H = -9223372036854775807L;
        y();
    }

    /* access modifiers changed from: protected */
    public void s() throws ExoPlaybackException {
        this.L = false;
        this.T = 0;
        if (this.C != 0) {
            t();
            x();
            return;
        }
        this.y = null;
        VpxOutputBuffer vpxOutputBuffer = this.z;
        if (vpxOutputBuffer != null) {
            vpxOutputBuffer.release();
            this.z = null;
        }
        this.x.flush();
        this.D = false;
    }

    /* access modifiers changed from: protected */
    public void t() {
        VpxDecoder vpxDecoder = this.x;
        if (vpxDecoder != null) {
            this.y = null;
            this.z = null;
            vpxDecoder.release();
            this.x = null;
            this.X.b++;
            this.C = 0;
            this.D = false;
            this.T = 0;
        }
    }

    public LibvpxVideoRenderer(boolean z2, long j2, Handler handler, VideoRendererEventListener videoRendererEventListener, int i, DrmSessionManager<ExoMediaCrypto> drmSessionManager, boolean z3, boolean z4, boolean z5) {
        super(2);
        this.j = z2;
        this.k = z4;
        this.l = j2;
        this.m = i;
        this.s = drmSessionManager;
        this.n = z3;
        this.t = z5;
        this.H = -9223372036854775807L;
        v();
        this.p = new FormatHolder();
        this.q = new TimedValueQueue<>();
        this.r = DecoderInputBuffer.e();
        this.o = new VideoRendererEventListener.EventDispatcher(handler, videoRendererEventListener);
        this.K = -1;
        this.C = 0;
    }

    /* access modifiers changed from: protected */
    public void c(VpxOutputBuffer vpxOutputBuffer) {
        this.X.f++;
        vpxOutputBuffer.release();
    }

    /* access modifiers changed from: protected */
    public boolean d(long j2, long j3) {
        return e(j2) && j3 > 100000;
    }

    public void a(long j2, long j3) throws ExoPlaybackException {
        if (!this.N) {
            if (this.u == null) {
                this.r.clear();
                int a2 = a(this.p, this.r, true);
                if (a2 == -5) {
                    b(this.p.f3165a);
                } else if (a2 == -4) {
                    Assertions.b(this.r.isEndOfStream());
                    this.M = true;
                    this.N = true;
                    return;
                } else {
                    return;
                }
            }
            x();
            if (this.x != null) {
                try {
                    TraceUtil.a("drainAndFeed");
                    while (e(j2, j3)) {
                    }
                    while (w()) {
                    }
                    TraceUtil.a();
                    this.X.a();
                } catch (VpxDecoderException e) {
                    throw ExoPlaybackException.a(e, m());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean c(long j2) throws ExoPlaybackException {
        int b = b(j2);
        if (b == 0) {
            return false;
        }
        this.X.i++;
        a(this.T + b);
        s();
        return true;
    }

    public boolean a() {
        return this.N;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) throws ExoPlaybackException {
        this.X = new DecoderCounters();
        this.o.b(this.X);
    }

    /* access modifiers changed from: protected */
    public boolean b(long j2, long j3) {
        return f(j2);
    }

    /* access modifiers changed from: protected */
    public void b(VpxOutputBuffer vpxOutputBuffer) throws VpxDecoderException {
        int i = vpxOutputBuffer.mode;
        boolean z2 = i == 1 && this.I != null;
        boolean z3 = i == 2 && this.I != null;
        boolean z4 = i == 0 && this.J != null;
        this.U = SystemClock.elapsedRealtime() * 1000;
        if (z2 || z4 || z3) {
            a(vpxOutputBuffer.width, vpxOutputBuffer.height);
            if (z2) {
                a(vpxOutputBuffer, this.j);
                vpxOutputBuffer.release();
            } else if (z4) {
                this.J.a(vpxOutputBuffer);
            } else {
                this.x.a(vpxOutputBuffer, this.I);
                vpxOutputBuffer.release();
            }
            this.S = 0;
            this.X.e++;
            z();
            return;
        }
        a(vpxOutputBuffer);
    }

    /* access modifiers changed from: protected */
    public void a(long j2, boolean z2) throws ExoPlaybackException {
        this.M = false;
        this.N = false;
        u();
        this.G = -9223372036854775807L;
        this.S = 0;
        if (this.x != null) {
            s();
        }
        if (z2) {
            C();
        } else {
            this.H = -9223372036854775807L;
        }
        this.q.a();
    }

    /* access modifiers changed from: protected */
    public void a(Format[] formatArr, long j2) throws ExoPlaybackException {
        this.V = j2;
        super.a(formatArr, j2);
    }

    /* access modifiers changed from: protected */
    public void a(String str, long j2, long j3) {
        this.o.a(str, j2, j3);
    }

    /* access modifiers changed from: protected */
    public void a(VpxOutputBuffer vpxOutputBuffer) {
        a(1);
        vpxOutputBuffer.release();
    }

    private boolean b(boolean z2) throws ExoPlaybackException {
        if (this.A == null || (!z2 && this.n)) {
            return false;
        }
        int state = this.A.getState();
        if (state != 1) {
            return state != 4;
        }
        throw ExoPlaybackException.a(this.A.b(), m());
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        DecoderCounters decoderCounters = this.X;
        decoderCounters.g += i;
        this.R += i;
        this.S += i;
        decoderCounters.h = Math.max(this.S, decoderCounters.h);
        int i2 = this.m;
        if (i2 > 0 && this.R >= i2) {
            y();
        }
    }

    public void a(int i, Object obj) throws ExoPlaybackException {
        if (i == 1) {
            a((Surface) obj, (VpxOutputBufferRenderer) null);
        } else if (i == 10000) {
            a((Surface) null, (VpxOutputBufferRenderer) obj);
        } else if (i == 6) {
            this.W = (VideoFrameMetadataListener) obj;
        } else {
            super.a(i, obj);
        }
    }

    private void a(Surface surface, VpxOutputBufferRenderer vpxOutputBufferRenderer) {
        int i = 0;
        int i2 = 1;
        Assertions.b(surface == null || vpxOutputBufferRenderer == null);
        if (this.I != surface || this.J != vpxOutputBufferRenderer) {
            this.I = surface;
            this.J = vpxOutputBufferRenderer;
            if (surface != null) {
                if (this.t) {
                    i2 = 2;
                }
                this.K = i2;
            } else {
                if (vpxOutputBufferRenderer == null) {
                    i = -1;
                }
                this.K = i;
            }
            int i3 = this.K;
            if (i3 != -1) {
                VpxDecoder vpxDecoder = this.x;
                if (vpxDecoder != null) {
                    vpxDecoder.b(i3);
                }
                B();
                u();
                if (getState() == 2) {
                    C();
                    return;
                }
                return;
            }
            v();
            u();
        } else if (this.K != -1) {
            B();
            A();
        }
    }

    private void a(VpxOutputBuffer vpxOutputBuffer, boolean z2) {
        Bitmap bitmap = this.E;
        if (!(bitmap != null && bitmap.getWidth() == vpxOutputBuffer.width && this.E.getHeight() == vpxOutputBuffer.height)) {
            this.E = Bitmap.createBitmap(vpxOutputBuffer.width, vpxOutputBuffer.height, Bitmap.Config.RGB_565);
        }
        this.E.copyPixelsFromBuffer(vpxOutputBuffer.data);
        Canvas lockCanvas = this.I.lockCanvas((Rect) null);
        if (z2) {
            lockCanvas.scale(((float) lockCanvas.getWidth()) / ((float) vpxOutputBuffer.width), ((float) lockCanvas.getHeight()) / ((float) vpxOutputBuffer.height));
        }
        lockCanvas.drawBitmap(this.E, 0.0f, 0.0f, (Paint) null);
        this.I.unlockCanvasAndPost(lockCanvas);
    }

    private void a(int i, int i2) {
        if (this.O != i || this.P != i2) {
            this.O = i;
            this.P = i2;
            this.o.b(i, i2, 0, 1.0f);
        }
    }
}
