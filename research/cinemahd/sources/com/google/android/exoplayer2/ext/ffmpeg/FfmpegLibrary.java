package com.google.android.exoplayer2.ext.ffmpeg;

import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.util.LibraryLoader;

public final class FfmpegLibrary {

    /* renamed from: a  reason: collision with root package name */
    private static final LibraryLoader f3241a = new LibraryLoader("avutil", "avresample", "avcodec", "ffmpeg");

    static {
        ExoPlayerLibraryInfo.a("goog.exo.ffmpeg");
    }

    private FfmpegLibrary() {
    }

    public static String a() {
        if (b()) {
            return ffmpegGetVersion();
        }
        return null;
    }

    public static boolean b() {
        return f3241a.a();
    }

    private static native String ffmpegGetVersion();

    private static native boolean ffmpegHasDecoder(String str);

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String a(java.lang.String r1, int r2) {
        /*
            int r0 = r1.hashCode()
            switch(r0) {
                case -2123537834: goto L_0x00b9;
                case -1606874997: goto L_0x00ae;
                case -1095064472: goto L_0x00a3;
                case -1003765268: goto L_0x0098;
                case -432837260: goto L_0x008e;
                case -432837259: goto L_0x0084;
                case -53558318: goto L_0x007a;
                case 187078296: goto L_0x0070;
                case 187094639: goto L_0x0065;
                case 1503095341: goto L_0x005a;
                case 1504470054: goto L_0x004e;
                case 1504578661: goto L_0x0043;
                case 1504619009: goto L_0x0037;
                case 1504831518: goto L_0x002c;
                case 1504891608: goto L_0x0020;
                case 1505942594: goto L_0x0014;
                case 1556697186: goto L_0x0009;
                default: goto L_0x0007;
            }
        L_0x0007:
            goto L_0x00c3
        L_0x0009:
            java.lang.String r0 = "audio/true-hd"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 7
            goto L_0x00c4
        L_0x0014:
            java.lang.String r0 = "audio/vnd.dts.hd"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 9
            goto L_0x00c4
        L_0x0020:
            java.lang.String r0 = "audio/opus"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 11
            goto L_0x00c4
        L_0x002c:
            java.lang.String r0 = "audio/mpeg"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 1
            goto L_0x00c4
        L_0x0037:
            java.lang.String r0 = "audio/flac"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 14
            goto L_0x00c4
        L_0x0043:
            java.lang.String r0 = "audio/eac3"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 5
            goto L_0x00c4
        L_0x004e:
            java.lang.String r0 = "audio/alac"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 15
            goto L_0x00c4
        L_0x005a:
            java.lang.String r0 = "audio/3gpp"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 12
            goto L_0x00c4
        L_0x0065:
            java.lang.String r0 = "audio/raw"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 16
            goto L_0x00c4
        L_0x0070:
            java.lang.String r0 = "audio/ac3"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 4
            goto L_0x00c4
        L_0x007a:
            java.lang.String r0 = "audio/mp4a-latm"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 0
            goto L_0x00c4
        L_0x0084:
            java.lang.String r0 = "audio/mpeg-L2"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 3
            goto L_0x00c4
        L_0x008e:
            java.lang.String r0 = "audio/mpeg-L1"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 2
            goto L_0x00c4
        L_0x0098:
            java.lang.String r0 = "audio/vorbis"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 10
            goto L_0x00c4
        L_0x00a3:
            java.lang.String r0 = "audio/vnd.dts"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 8
            goto L_0x00c4
        L_0x00ae:
            java.lang.String r0 = "audio/amr-wb"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 13
            goto L_0x00c4
        L_0x00b9:
            java.lang.String r0 = "audio/eac3-joc"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x00c3
            r1 = 6
            goto L_0x00c4
        L_0x00c3:
            r1 = -1
        L_0x00c4:
            r0 = 0
            switch(r1) {
                case 0: goto L_0x00f9;
                case 1: goto L_0x00f6;
                case 2: goto L_0x00f6;
                case 3: goto L_0x00f6;
                case 4: goto L_0x00f3;
                case 5: goto L_0x00f0;
                case 6: goto L_0x00f0;
                case 7: goto L_0x00ed;
                case 8: goto L_0x00ea;
                case 9: goto L_0x00ea;
                case 10: goto L_0x00e7;
                case 11: goto L_0x00e4;
                case 12: goto L_0x00e1;
                case 13: goto L_0x00de;
                case 14: goto L_0x00db;
                case 15: goto L_0x00d8;
                case 16: goto L_0x00c9;
                default: goto L_0x00c8;
            }
        L_0x00c8:
            return r0
        L_0x00c9:
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            if (r2 != r1) goto L_0x00d0
            java.lang.String r1 = "pcm_mulaw"
            return r1
        L_0x00d0:
            r1 = 536870912(0x20000000, float:1.0842022E-19)
            if (r2 != r1) goto L_0x00d7
            java.lang.String r1 = "pcm_alaw"
            return r1
        L_0x00d7:
            return r0
        L_0x00d8:
            java.lang.String r1 = "alac"
            return r1
        L_0x00db:
            java.lang.String r1 = "flac"
            return r1
        L_0x00de:
            java.lang.String r1 = "amrwb"
            return r1
        L_0x00e1:
            java.lang.String r1 = "amrnb"
            return r1
        L_0x00e4:
            java.lang.String r1 = "opus"
            return r1
        L_0x00e7:
            java.lang.String r1 = "vorbis"
            return r1
        L_0x00ea:
            java.lang.String r1 = "dca"
            return r1
        L_0x00ed:
            java.lang.String r1 = "truehd"
            return r1
        L_0x00f0:
            java.lang.String r1 = "eac3"
            return r1
        L_0x00f3:
            java.lang.String r1 = "ac3"
            return r1
        L_0x00f6:
            java.lang.String r1 = "mp3"
            return r1
        L_0x00f9:
            java.lang.String r1 = "aac"
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ext.ffmpeg.FfmpegLibrary.a(java.lang.String, int):java.lang.String");
    }

    public static boolean b(String str, int i) {
        String a2;
        if (b() && (a2 = a(str, i)) != null && ffmpegHasDecoder(a2)) {
            return true;
        }
        return false;
    }
}
