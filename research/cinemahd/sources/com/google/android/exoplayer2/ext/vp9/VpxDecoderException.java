package com.google.android.exoplayer2.ext.vp9;

public final class VpxDecoderException extends Exception {
    VpxDecoderException(String str) {
        super(str);
    }

    VpxDecoderException(String str, Throwable th) {
        super(str, th);
    }
}
