package com.google.android.exoplayer2.ext.okhttp;

import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Predicate;
import okhttp3.CacheControl;
import okhttp3.Call;

public final class OkHttpDataSourceFactory extends HttpDataSource.BaseFactory {
    private final Call.Factory b;
    private final String c;
    private final TransferListener d;
    private final CacheControl e;

    public OkHttpDataSourceFactory(Call.Factory factory, String str, TransferListener transferListener, CacheControl cacheControl) {
        this.b = factory;
        this.c = str;
        this.d = transferListener;
        this.e = cacheControl;
    }

    /* access modifiers changed from: protected */
    public OkHttpDataSource a(HttpDataSource.RequestProperties requestProperties) {
        OkHttpDataSource okHttpDataSource = new OkHttpDataSource(this.b, this.c, (Predicate<String>) null, this.e, requestProperties);
        TransferListener transferListener = this.d;
        if (transferListener != null) {
            okHttpDataSource.a(transferListener);
        }
        return okHttpDataSource;
    }
}
