package com.google.android.exoplayer2.ext.ffmpeg;

import android.os.Handler;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.audio.AudioCapabilities;
import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.audio.AudioSink;
import com.google.android.exoplayer2.audio.DefaultAudioSink;
import com.google.android.exoplayer2.audio.SimpleDecoderAudioRenderer;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Collections;
import java.util.List;

public final class FfmpegAudioRenderer extends SimpleDecoderAudioRenderer {
    private final boolean H;
    private FfmpegDecoder I;

    public FfmpegAudioRenderer() {
        this((Handler) null, (AudioRendererEventListener) null, new AudioProcessor[0]);
    }

    private boolean b(Format format) {
        return c(format) || a(format.t, 2);
    }

    private boolean c(Format format) {
        Assertions.a(format.g);
        if (!this.H || !a(format.t, 4)) {
            return false;
        }
        String str = format.g;
        char c = 65535;
        int hashCode = str.hashCode();
        if (hashCode != 187078296) {
            if (hashCode == 187094639 && str.equals("audio/raw")) {
                c = 0;
            }
        } else if (str.equals("audio/ac3")) {
            c = 1;
        }
        if (c == 0) {
            int i = format.v;
            if (i == Integer.MIN_VALUE || i == 1073741824 || i == 4) {
                return true;
            }
            return false;
        } else if (c != 1) {
            return true;
        } else {
            return false;
        }
    }

    public final int k() throws ExoPlaybackException {
        return 8;
    }

    public Format s() {
        Assertions.a(this.I);
        return Format.a((String) null, "audio/raw", (String) null, -1, -1, this.I.e(), this.I.g(), this.I.f(), (List<byte[]>) Collections.emptyList(), (DrmInitData) null, 0, (String) null);
    }

    public FfmpegAudioRenderer(Handler handler, AudioRendererEventListener audioRendererEventListener, AudioProcessor... audioProcessorArr) {
        this(handler, audioRendererEventListener, new DefaultAudioSink((AudioCapabilities) null, audioProcessorArr), false);
    }

    /* access modifiers changed from: protected */
    public int a(DrmSessionManager<ExoMediaCrypto> drmSessionManager, Format format) {
        Assertions.a(format.g);
        if (!FfmpegLibrary.b()) {
            return 0;
        }
        if (!FfmpegLibrary.b(format.g, format.v) || !b(format)) {
            return 1;
        }
        return !BaseRenderer.a((DrmSessionManager<?>) drmSessionManager, format.j) ? 2 : 4;
    }

    public FfmpegAudioRenderer(Handler handler, AudioRendererEventListener audioRendererEventListener, AudioSink audioSink, boolean z) {
        super(handler, audioRendererEventListener, (DrmSessionManager<ExoMediaCrypto>) null, false, audioSink);
        this.H = z;
    }

    /* access modifiers changed from: protected */
    public FfmpegDecoder a(Format format, ExoMediaCrypto exoMediaCrypto) throws FfmpegDecoderException {
        int i = format.h;
        this.I = new FfmpegDecoder(16, 16, i != -1 ? i : 5760, format, c(format));
        return this.I;
    }
}
