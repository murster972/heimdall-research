package com.google.android.exoplayer2.ext.vp9;

import android.view.Surface;
import com.google.android.exoplayer2.decoder.CryptoInfo;
import com.google.android.exoplayer2.decoder.SimpleDecoder;
import com.google.android.exoplayer2.drm.DecryptionException;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import java.nio.ByteBuffer;

final class VpxDecoder extends SimpleDecoder<VpxInputBuffer, VpxOutputBuffer, VpxDecoderException> {
    private final ExoMediaCrypto n;
    private final long o;
    private volatile int p;

    public VpxDecoder(int i, int i2, int i3, ExoMediaCrypto exoMediaCrypto, boolean z, boolean z2) throws VpxDecoderException {
        super(new VpxInputBuffer[i], new VpxOutputBuffer[i2]);
        if (VpxLibrary.b()) {
            this.n = exoMediaCrypto;
            if (exoMediaCrypto == null || VpxLibrary.vpxIsSecureDecodeSupported()) {
                this.o = vpxInit(z, z2);
                if (this.o != 0) {
                    a(i3);
                    return;
                }
                throw new VpxDecoderException("Failed to initialize decoder");
            }
            throw new VpxDecoderException("Vpx decoder does not support secure decode.");
        }
        throw new VpxDecoderException("Failed to load decoder native libraries.");
    }

    private native long vpxClose(long j);

    private native long vpxDecode(long j, ByteBuffer byteBuffer, int i);

    private native int vpxGetErrorCode(long j);

    private native String vpxGetErrorMessage(long j);

    private native int vpxGetFrame(long j, VpxOutputBuffer vpxOutputBuffer);

    private native long vpxInit(boolean z, boolean z2);

    private native int vpxReleaseFrame(long j, VpxOutputBuffer vpxOutputBuffer);

    private native int vpxRenderFrame(long j, Surface surface, VpxOutputBuffer vpxOutputBuffer);

    private native long vpxSecureDecode(long j, ByteBuffer byteBuffer, int i, ExoMediaCrypto exoMediaCrypto, int i2, byte[] bArr, byte[] bArr2, int i3, int[] iArr, int[] iArr2);

    public void b(int i) {
        this.p = i;
    }

    public String getName() {
        return "libvpx" + VpxLibrary.a();
    }

    public void release() {
        super.release();
        vpxClose(this.o);
    }

    /* access modifiers changed from: protected */
    public VpxInputBuffer c() {
        return new VpxInputBuffer();
    }

    /* access modifiers changed from: protected */
    public VpxOutputBuffer d() {
        return new VpxOutputBuffer(this);
    }

    /* access modifiers changed from: protected */
    public void a(VpxOutputBuffer vpxOutputBuffer) {
        if (this.p == 2 && !vpxOutputBuffer.isDecodeOnly()) {
            vpxReleaseFrame(this.o, vpxOutputBuffer);
        }
        super.a(vpxOutputBuffer);
    }

    /* access modifiers changed from: protected */
    public VpxDecoderException a(Throwable th) {
        return new VpxDecoderException("Unexpected decode error", th);
    }

    /* access modifiers changed from: protected */
    public VpxDecoderException a(VpxInputBuffer vpxInputBuffer, VpxOutputBuffer vpxOutputBuffer, boolean z) {
        long j;
        ByteBuffer byteBuffer = vpxInputBuffer.b;
        int limit = byteBuffer.limit();
        CryptoInfo cryptoInfo = vpxInputBuffer.f3216a;
        if (vpxInputBuffer.c()) {
            j = vpxSecureDecode(this.o, byteBuffer, limit, this.n, cryptoInfo.c, cryptoInfo.b, cryptoInfo.f3213a, cryptoInfo.f, cryptoInfo.d, cryptoInfo.e);
        } else {
            j = vpxDecode(this.o, byteBuffer, limit);
        }
        if (j != 0) {
            if (j == 2) {
                String str = "Drm error: " + vpxGetErrorMessage(this.o);
                return new VpxDecoderException(str, new DecryptionException(vpxGetErrorCode(this.o), str));
            }
            return new VpxDecoderException("Decode error: " + vpxGetErrorMessage(this.o));
        } else if (vpxInputBuffer.isDecodeOnly()) {
            return null;
        } else {
            vpxOutputBuffer.init(vpxInputBuffer.c, this.p);
            int vpxGetFrame = vpxGetFrame(this.o, vpxOutputBuffer);
            if (vpxGetFrame == 1) {
                vpxOutputBuffer.addFlag(Integer.MIN_VALUE);
            } else if (vpxGetFrame == -1) {
                return new VpxDecoderException("Buffer initialization failed.");
            }
            vpxOutputBuffer.colorInfo = vpxInputBuffer.e;
            return null;
        }
    }

    public void a(VpxOutputBuffer vpxOutputBuffer, Surface surface) throws VpxDecoderException {
        if (vpxRenderFrame(this.o, surface, vpxOutputBuffer) == -1) {
            throw new VpxDecoderException("Buffer render failed.");
        }
    }
}
