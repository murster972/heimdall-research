package com.google.android.exoplayer2.ext.okhttp;

import android.net.Uri;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.upstream.BaseDataSource;
import com.google.android.exoplayer2.upstream.DataSourceException;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Predicate;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OkHttpDataSource extends BaseDataSource implements HttpDataSource {
    private static final byte[] s = new byte[4096];
    private final Call.Factory e;
    private final HttpDataSource.RequestProperties f = new HttpDataSource.RequestProperties();
    private final String g;
    private final Predicate<String> h;
    private final CacheControl i;
    private final HttpDataSource.RequestProperties j;
    private DataSpec k;
    private Response l;
    private InputStream m;
    private boolean n;
    private long o;
    private long p;
    private long q;
    private long r;

    static {
        ExoPlayerLibraryInfo.a("goog.exo.okhttp");
    }

    public OkHttpDataSource(Call.Factory factory, String str, Predicate<String> predicate, CacheControl cacheControl, HttpDataSource.RequestProperties requestProperties) {
        super(true);
        Assertions.a(factory);
        this.e = factory;
        this.g = str;
        this.h = predicate;
        this.i = cacheControl;
        this.j = requestProperties;
    }

    private void c() {
        Response response = this.l;
        if (response != null) {
            ResponseBody body = response.body();
            Assertions.a(body);
            body.close();
            this.l = null;
        }
        this.m = null;
    }

    private Request d(DataSpec dataSpec) throws HttpDataSource.HttpDataSourceException {
        long j2 = dataSpec.e;
        long j3 = dataSpec.f;
        boolean a2 = dataSpec.a(1);
        HttpUrl parse = HttpUrl.parse(dataSpec.f3586a.toString());
        if (parse != null) {
            Request.Builder url = new Request.Builder().url(parse);
            CacheControl cacheControl = this.i;
            if (cacheControl != null) {
                url.cacheControl(cacheControl);
            }
            HttpDataSource.RequestProperties requestProperties = this.j;
            if (requestProperties != null) {
                for (Map.Entry next : requestProperties.a().entrySet()) {
                    url.header((String) next.getKey(), (String) next.getValue());
                }
            }
            for (Map.Entry next2 : this.f.a().entrySet()) {
                url.header((String) next2.getKey(), (String) next2.getValue());
            }
            if (!(j2 == 0 && j3 == -1)) {
                String str = "bytes=" + j2 + "-";
                if (j3 != -1) {
                    str = str + ((j2 + j3) - 1);
                }
                url.addHeader("Range", str);
            }
            String str2 = this.g;
            if (str2 != null) {
                url.addHeader("User-Agent", str2);
            }
            if (!a2) {
                url.addHeader("Accept-Encoding", InterpolationAnimatedNode.EXTRAPOLATE_TYPE_IDENTITY);
            }
            byte[] bArr = dataSpec.c;
            RequestBody requestBody = null;
            if (bArr != null) {
                requestBody = RequestBody.create((MediaType) null, bArr);
            } else if (dataSpec.b == 2) {
                requestBody = RequestBody.create((MediaType) null, Util.f);
            }
            url.method(dataSpec.a(), requestBody);
            return url.build();
        }
        throw new HttpDataSource.HttpDataSourceException("Malformed URL", dataSpec, 1);
    }

    public Map<String, List<String>> a() {
        Response response = this.l;
        return response == null ? Collections.emptyMap() : response.headers().toMultimap();
    }

    public void close() throws HttpDataSource.HttpDataSourceException {
        if (this.n) {
            this.n = false;
            b();
            c();
        }
    }

    public Uri getUri() {
        Response response = this.l;
        if (response == null) {
            return null;
        }
        return Uri.parse(response.request().url().toString());
    }

    public int read(byte[] bArr, int i2, int i3) throws HttpDataSource.HttpDataSourceException {
        try {
            d();
            return a(bArr, i2, i3);
        } catch (IOException e2) {
            DataSpec dataSpec = this.k;
            Assertions.a(dataSpec);
            throw new HttpDataSource.HttpDataSourceException(e2, dataSpec, 2);
        }
    }

    public void a(String str, String str2) {
        Assertions.a(str);
        Assertions.a(str2);
        this.f.a(str, str2);
    }

    public long a(DataSpec dataSpec) throws HttpDataSource.HttpDataSourceException {
        this.k = dataSpec;
        long j2 = 0;
        this.r = 0;
        this.q = 0;
        b(dataSpec);
        try {
            this.l = this.e.newCall(d(dataSpec)).execute();
            Response response = this.l;
            ResponseBody body = response.body();
            Assertions.a(body);
            ResponseBody responseBody = body;
            this.m = responseBody.byteStream();
            int code = response.code();
            if (!response.isSuccessful()) {
                Map<String, List<String>> multimap = response.headers().toMultimap();
                c();
                HttpDataSource.InvalidResponseCodeException invalidResponseCodeException = new HttpDataSource.InvalidResponseCodeException(code, response.message(), multimap, dataSpec);
                if (code == 416) {
                    invalidResponseCodeException.initCause(new DataSourceException(0));
                }
                throw invalidResponseCodeException;
            }
            MediaType contentType = responseBody.contentType();
            String mediaType = contentType != null ? contentType.toString() : "";
            Predicate<String> predicate = this.h;
            if (predicate == null || predicate.a(mediaType)) {
                if (code == 200) {
                    long j3 = dataSpec.e;
                    if (j3 != 0) {
                        j2 = j3;
                    }
                }
                this.o = j2;
                long j4 = dataSpec.f;
                long j5 = -1;
                if (j4 != -1) {
                    this.p = j4;
                } else {
                    long contentLength = responseBody.contentLength();
                    if (contentLength != -1) {
                        j5 = contentLength - this.o;
                    }
                    this.p = j5;
                }
                this.n = true;
                c(dataSpec);
                return this.p;
            }
            c();
            throw new HttpDataSource.InvalidContentTypeException(mediaType, dataSpec);
        } catch (IOException e2) {
            throw new HttpDataSource.HttpDataSourceException("Unable to connect to " + dataSpec.f3586a, e2, dataSpec, 1);
        }
    }

    private void d() throws IOException {
        if (this.q != this.o) {
            while (true) {
                long j2 = this.q;
                long j3 = this.o;
                if (j2 != j3) {
                    int min = (int) Math.min(j3 - j2, (long) s.length);
                    InputStream inputStream = this.m;
                    Util.a(inputStream);
                    int read = inputStream.read(s, 0, min);
                    if (Thread.currentThread().isInterrupted()) {
                        throw new InterruptedIOException();
                    } else if (read != -1) {
                        this.q += (long) read;
                        a(read);
                    } else {
                        throw new EOFException();
                    }
                } else {
                    return;
                }
            }
        }
    }

    private int a(byte[] bArr, int i2, int i3) throws IOException {
        if (i3 == 0) {
            return 0;
        }
        long j2 = this.p;
        if (j2 != -1) {
            long j3 = j2 - this.r;
            if (j3 == 0) {
                return -1;
            }
            i3 = (int) Math.min((long) i3, j3);
        }
        InputStream inputStream = this.m;
        Util.a(inputStream);
        int read = inputStream.read(bArr, i2, i3);
        if (read != -1) {
            this.r += (long) read;
            a(read);
            return read;
        } else if (this.p == -1) {
            return -1;
        } else {
            throw new EOFException();
        }
    }
}
