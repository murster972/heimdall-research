package com.google.android.exoplayer2;

import java.util.HashSet;

public final class ExoPlayerLibraryInfo {

    /* renamed from: a  reason: collision with root package name */
    private static final HashSet<String> f3163a = new HashSet<>();
    private static String b = "goog.exo.core";

    private ExoPlayerLibraryInfo() {
    }

    public static synchronized String a() {
        String str;
        synchronized (ExoPlayerLibraryInfo.class) {
            str = b;
        }
        return str;
    }

    public static synchronized void a(String str) {
        synchronized (ExoPlayerLibraryInfo.class) {
            if (f3163a.add(str)) {
                b += ", " + str;
            }
        }
    }
}
