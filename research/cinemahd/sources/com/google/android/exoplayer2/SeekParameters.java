package com.google.android.exoplayer2;

import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.util.Assertions;

public final class SeekParameters {
    public static final SeekParameters c = new SeekParameters(0, 0);
    public static final SeekParameters d = c;

    /* renamed from: a  reason: collision with root package name */
    public final long f3173a;
    public final long b;

    static {
        new SeekParameters(Clock.MAX_TIME, Clock.MAX_TIME);
        new SeekParameters(Clock.MAX_TIME, 0);
        new SeekParameters(0, Clock.MAX_TIME);
    }

    public SeekParameters(long j, long j2) {
        boolean z = true;
        Assertions.a(j >= 0);
        Assertions.a(j2 < 0 ? false : z);
        this.f3173a = j;
        this.b = j2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SeekParameters.class != obj.getClass()) {
            return false;
        }
        SeekParameters seekParameters = (SeekParameters) obj;
        if (this.f3173a == seekParameters.f3173a && this.b == seekParameters.b) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((int) this.f3173a) * 31) + ((int) this.b);
    }
}
