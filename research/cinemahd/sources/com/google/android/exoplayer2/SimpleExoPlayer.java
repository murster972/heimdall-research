package com.google.android.exoplayer2;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.Looper;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.PlayerMessage;
import com.google.android.exoplayer2.analytics.AnalyticsCollector;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.audio.AudioFocusManager;
import com.google.android.exoplayer2.audio.AudioListener;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.drm.DefaultDrmSessionEventListener;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataOutput;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.Clock;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

@TargetApi(16)
public class SimpleExoPlayer extends BasePlayer implements ExoPlayer, Player.AudioComponent, Player.VideoComponent, Player.TextComponent {
    private MediaSource A;
    /* access modifiers changed from: private */
    public List<Cue> B;
    private boolean C;
    protected final Renderer[] b;
    private final ExoPlayerImpl c;
    private final Handler d;
    private final ComponentListener e;
    /* access modifiers changed from: private */
    public final CopyOnWriteArraySet<com.google.android.exoplayer2.video.VideoListener> f;
    /* access modifiers changed from: private */
    public final CopyOnWriteArraySet<AudioListener> g;
    /* access modifiers changed from: private */
    public final CopyOnWriteArraySet<TextOutput> h;
    /* access modifiers changed from: private */
    public final CopyOnWriteArraySet<MetadataOutput> i;
    /* access modifiers changed from: private */
    public final CopyOnWriteArraySet<VideoRendererEventListener> j;
    /* access modifiers changed from: private */
    public final CopyOnWriteArraySet<AudioRendererEventListener> k;
    private final BandwidthMeter l;
    private final AnalyticsCollector m;
    private final AudioFocusManager n;
    /* access modifiers changed from: private */
    public Format o;
    /* access modifiers changed from: private */
    public Format p;
    /* access modifiers changed from: private */
    public Surface q;
    private boolean r;
    private SurfaceHolder s;
    private TextureView t;
    private int u;
    private int v;
    /* access modifiers changed from: private */
    public DecoderCounters w;
    /* access modifiers changed from: private */
    public DecoderCounters x;
    /* access modifiers changed from: private */
    public int y;
    private float z;

    @Deprecated
    public interface VideoListener extends com.google.android.exoplayer2.video.VideoListener {
    }

    protected SimpleExoPlayer(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, LoadControl loadControl, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, BandwidthMeter bandwidthMeter, AnalyticsCollector.Factory factory, Looper looper) {
        this(context, renderersFactory, trackSelector, loadControl, drmSessionManager, bandwidthMeter, factory, Clock.f3623a, looper);
    }

    private void A() {
        TextureView textureView = this.t;
        if (textureView != null) {
            if (textureView.getSurfaceTextureListener() != this.e) {
                Log.d("SimpleExoPlayer", "SurfaceTextureListener already unset or replaced.");
            } else {
                this.t.setSurfaceTextureListener((TextureView.SurfaceTextureListener) null);
            }
            this.t = null;
        }
        SurfaceHolder surfaceHolder = this.s;
        if (surfaceHolder != null) {
            surfaceHolder.removeCallback(this.e);
            this.s = null;
        }
    }

    /* access modifiers changed from: private */
    public void B() {
        float a2 = this.z * this.n.a();
        for (Renderer renderer : this.b) {
            if (renderer.getTrackType() == 1) {
                PlayerMessage a3 = this.c.a((PlayerMessage.Target) renderer);
                a3.a(2);
                a3.a((Object) Float.valueOf(a2));
                a3.k();
            }
        }
    }

    private void C() {
        if (Looper.myLooper() != j()) {
            Log.b("SimpleExoPlayer", "Player is accessed on the wrong thread. See https://google.github.io/ExoPlayer/faqs.html#what-do-player-is-accessed-on-the-wrong-thread-warnings-mean", this.C ? null : new IllegalStateException());
            this.C = true;
        }
    }

    public Player.VideoComponent g() {
        return this;
    }

    public long getBufferedPosition() {
        C();
        return this.c.getBufferedPosition();
    }

    public long getCurrentPosition() {
        C();
        return this.c.getCurrentPosition();
    }

    public long getDuration() {
        C();
        return this.c.getDuration();
    }

    public int getPlaybackState() {
        C();
        return this.c.getPlaybackState();
    }

    public int getRepeatMode() {
        C();
        return this.c.getRepeatMode();
    }

    public Looper j() {
        return this.c.j();
    }

    public TrackSelectionArray k() {
        C();
        return this.c.k();
    }

    public Player.TextComponent l() {
        return this;
    }

    public boolean m() {
        C();
        return this.c.m();
    }

    public int n() {
        C();
        return this.c.n();
    }

    public long o() {
        C();
        return this.c.o();
    }

    public boolean r() {
        C();
        return this.c.r();
    }

    public void setRepeatMode(int i2) {
        C();
        this.c.setRepeatMode(i2);
    }

    public Format v() {
        return this.p;
    }

    public int w() {
        return this.y;
    }

    public boolean x() {
        C();
        return this.c.x();
    }

    public void y() {
        this.n.b();
        this.c.y();
        A();
        Surface surface = this.q;
        if (surface != null) {
            if (this.r) {
                surface.release();
            }
            this.q = null;
        }
        MediaSource mediaSource = this.A;
        if (mediaSource != null) {
            mediaSource.a((MediaSourceEventListener) this.m);
            this.A = null;
        }
        this.l.a(this.m);
        this.B = Collections.emptyList();
    }

    public void z() {
        C();
        if (this.A == null) {
            return;
        }
        if (e() != null || getPlaybackState() == 1) {
            a(this.A, false, false);
        }
    }

    private final class ComponentListener implements VideoRendererEventListener, AudioRendererEventListener, TextOutput, MetadataOutput, SurfaceHolder.Callback, TextureView.SurfaceTextureListener, AudioFocusManager.PlayerControl {
        private ComponentListener() {
        }

        public void a(String str, long j, long j2) {
            Iterator it2 = SimpleExoPlayer.this.j.iterator();
            while (it2.hasNext()) {
                ((VideoRendererEventListener) it2.next()).a(str, j, j2);
            }
        }

        public void b(DecoderCounters decoderCounters) {
            Iterator it2 = SimpleExoPlayer.this.j.iterator();
            while (it2.hasNext()) {
                ((VideoRendererEventListener) it2.next()).b(decoderCounters);
            }
            Format unused = SimpleExoPlayer.this.o = null;
            DecoderCounters unused2 = SimpleExoPlayer.this.w = null;
        }

        public void c(DecoderCounters decoderCounters) {
            Iterator it2 = SimpleExoPlayer.this.k.iterator();
            while (it2.hasNext()) {
                ((AudioRendererEventListener) it2.next()).c(decoderCounters);
            }
            Format unused = SimpleExoPlayer.this.p = null;
            DecoderCounters unused2 = SimpleExoPlayer.this.x = null;
            int unused3 = SimpleExoPlayer.this.y = 0;
        }

        public void d(DecoderCounters decoderCounters) {
            DecoderCounters unused = SimpleExoPlayer.this.w = decoderCounters;
            Iterator it2 = SimpleExoPlayer.this.j.iterator();
            while (it2.hasNext()) {
                ((VideoRendererEventListener) it2.next()).d(decoderCounters);
            }
        }

        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
            SimpleExoPlayer.this.a(new Surface(surfaceTexture), true);
            SimpleExoPlayer.this.a(i, i2);
        }

        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            SimpleExoPlayer.this.a((Surface) null, true);
            SimpleExoPlayer.this.a(0, 0);
            return true;
        }

        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
            SimpleExoPlayer.this.a(i, i2);
        }

        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

        public void onVideoSizeChanged(int i, int i2, int i3, float f) {
            Iterator it2 = SimpleExoPlayer.this.f.iterator();
            while (it2.hasNext()) {
                com.google.android.exoplayer2.video.VideoListener videoListener = (com.google.android.exoplayer2.video.VideoListener) it2.next();
                if (!SimpleExoPlayer.this.j.contains(videoListener)) {
                    videoListener.onVideoSizeChanged(i, i2, i3, f);
                }
            }
            Iterator it3 = SimpleExoPlayer.this.j.iterator();
            while (it3.hasNext()) {
                ((VideoRendererEventListener) it3.next()).onVideoSizeChanged(i, i2, i3, f);
            }
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            SimpleExoPlayer.this.a(i2, i3);
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            SimpleExoPlayer.this.a(surfaceHolder.getSurface(), false);
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            SimpleExoPlayer.this.a((Surface) null, false);
            SimpleExoPlayer.this.a(0, 0);
        }

        public void a(Format format) {
            Format unused = SimpleExoPlayer.this.o = format;
            Iterator it2 = SimpleExoPlayer.this.j.iterator();
            while (it2.hasNext()) {
                ((VideoRendererEventListener) it2.next()).a(format);
            }
        }

        public void b(String str, long j, long j2) {
            Iterator it2 = SimpleExoPlayer.this.k.iterator();
            while (it2.hasNext()) {
                ((AudioRendererEventListener) it2.next()).b(str, j, j2);
            }
        }

        public void a(int i, long j) {
            Iterator it2 = SimpleExoPlayer.this.j.iterator();
            while (it2.hasNext()) {
                ((VideoRendererEventListener) it2.next()).a(i, j);
            }
        }

        public void b(Format format) {
            Format unused = SimpleExoPlayer.this.p = format;
            Iterator it2 = SimpleExoPlayer.this.k.iterator();
            while (it2.hasNext()) {
                ((AudioRendererEventListener) it2.next()).b(format);
            }
        }

        public void a(Surface surface) {
            if (SimpleExoPlayer.this.q == surface) {
                Iterator it2 = SimpleExoPlayer.this.f.iterator();
                while (it2.hasNext()) {
                    ((com.google.android.exoplayer2.video.VideoListener) it2.next()).onRenderedFirstFrame();
                }
            }
            Iterator it3 = SimpleExoPlayer.this.j.iterator();
            while (it3.hasNext()) {
                ((VideoRendererEventListener) it3.next()).a(surface);
            }
        }

        public void b(int i) {
            SimpleExoPlayer simpleExoPlayer = SimpleExoPlayer.this;
            simpleExoPlayer.a(simpleExoPlayer.m(), i);
        }

        public void a(DecoderCounters decoderCounters) {
            DecoderCounters unused = SimpleExoPlayer.this.x = decoderCounters;
            Iterator it2 = SimpleExoPlayer.this.k.iterator();
            while (it2.hasNext()) {
                ((AudioRendererEventListener) it2.next()).a(decoderCounters);
            }
        }

        public void a(int i) {
            if (SimpleExoPlayer.this.y != i) {
                int unused = SimpleExoPlayer.this.y = i;
                Iterator it2 = SimpleExoPlayer.this.g.iterator();
                while (it2.hasNext()) {
                    AudioListener audioListener = (AudioListener) it2.next();
                    if (!SimpleExoPlayer.this.k.contains(audioListener)) {
                        audioListener.a(i);
                    }
                }
                Iterator it3 = SimpleExoPlayer.this.k.iterator();
                while (it3.hasNext()) {
                    ((AudioRendererEventListener) it3.next()).a(i);
                }
            }
        }

        public void a(int i, long j, long j2) {
            Iterator it2 = SimpleExoPlayer.this.k.iterator();
            while (it2.hasNext()) {
                ((AudioRendererEventListener) it2.next()).a(i, j, j2);
            }
        }

        public void a(List<Cue> list) {
            List unused = SimpleExoPlayer.this.B = list;
            Iterator it2 = SimpleExoPlayer.this.h.iterator();
            while (it2.hasNext()) {
                ((TextOutput) it2.next()).a(list);
            }
        }

        public void a(Metadata metadata) {
            Iterator it2 = SimpleExoPlayer.this.i.iterator();
            while (it2.hasNext()) {
                ((MetadataOutput) it2.next()).a(metadata);
            }
        }

        public void a(float f) {
            SimpleExoPlayer.this.B();
        }
    }

    protected SimpleExoPlayer(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, LoadControl loadControl, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, BandwidthMeter bandwidthMeter, AnalyticsCollector.Factory factory, Clock clock, Looper looper) {
        DrmSessionManager<FrameworkMediaCrypto> drmSessionManager2 = drmSessionManager;
        BandwidthMeter bandwidthMeter2 = bandwidthMeter;
        this.l = bandwidthMeter2;
        this.e = new ComponentListener();
        this.f = new CopyOnWriteArraySet<>();
        this.g = new CopyOnWriteArraySet<>();
        this.h = new CopyOnWriteArraySet<>();
        this.i = new CopyOnWriteArraySet<>();
        this.j = new CopyOnWriteArraySet<>();
        this.k = new CopyOnWriteArraySet<>();
        this.d = new Handler(looper);
        Handler handler = this.d;
        ComponentListener componentListener = this.e;
        this.b = renderersFactory.a(handler, componentListener, componentListener, componentListener, componentListener, drmSessionManager);
        this.z = 1.0f;
        this.y = 0;
        AudioAttributes audioAttributes = AudioAttributes.e;
        this.B = Collections.emptyList();
        this.c = new ExoPlayerImpl(this.b, trackSelector, loadControl, bandwidthMeter, clock, looper);
        this.m = factory.a(this.c, clock);
        b((Player.EventListener) this.m);
        this.j.add(this.m);
        this.f.add(this.m);
        this.k.add(this.m);
        this.g.add(this.m);
        a((MetadataOutput) this.m);
        bandwidthMeter2.a(this.d, this.m);
        if (drmSessionManager2 instanceof DefaultDrmSessionManager) {
            ((DefaultDrmSessionManager) drmSessionManager2).a(this.d, (DefaultDrmSessionEventListener) this.m);
        }
        Context context2 = context;
        this.n = new AudioFocusManager(context, this.e);
    }

    public void c(boolean z2) {
        C();
        this.c.c(z2);
        MediaSource mediaSource = this.A;
        if (mediaSource != null) {
            mediaSource.a((MediaSourceEventListener) this.m);
            this.m.f();
            if (z2) {
                this.A = null;
            }
        }
        this.n.b();
        this.B = Collections.emptyList();
    }

    public long d() {
        C();
        return this.c.d();
    }

    public ExoPlaybackException e() {
        C();
        return this.c.e();
    }

    public int f() {
        C();
        return this.c.f();
    }

    public int h() {
        C();
        return this.c.h();
    }

    public Timeline i() {
        C();
        return this.c.i();
    }

    public void b(SurfaceHolder surfaceHolder) {
        C();
        A();
        this.s = surfaceHolder;
        if (surfaceHolder == null) {
            a((Surface) null, false);
            a(0, 0);
            return;
        }
        surfaceHolder.addCallback(this.e);
        Surface surface = surfaceHolder.getSurface();
        if (surface == null || !surface.isValid()) {
            a((Surface) null, false);
            a(0, 0);
            return;
        }
        a(surface, false);
        Rect surfaceFrame = surfaceHolder.getSurfaceFrame();
        a(surfaceFrame.width(), surfaceFrame.height());
    }

    public void a(Surface surface) {
        C();
        A();
        int i2 = 0;
        a(surface, false);
        if (surface != null) {
            i2 = -1;
        }
        a(i2, i2);
    }

    public boolean c() {
        C();
        return this.c.c();
    }

    public void a(SurfaceHolder surfaceHolder) {
        C();
        if (surfaceHolder != null && surfaceHolder == this.s) {
            b((SurfaceHolder) null);
        }
    }

    public void a(SurfaceView surfaceView) {
        b(surfaceView == null ? null : surfaceView.getHolder());
    }

    public void a(TextureView textureView) {
        C();
        A();
        this.t = textureView;
        if (textureView == null) {
            a((Surface) null, true);
            a(0, 0);
            return;
        }
        if (textureView.getSurfaceTextureListener() != null) {
            Log.d("SimpleExoPlayer", "Replacing existing SurfaceTextureListener.");
        }
        textureView.setSurfaceTextureListener(this.e);
        SurfaceTexture surfaceTexture = textureView.isAvailable() ? textureView.getSurfaceTexture() : null;
        if (surfaceTexture == null) {
            a((Surface) null, true);
            a(0, 0);
            return;
        }
        a(new Surface(surfaceTexture), true);
        a(textureView.getWidth(), textureView.getHeight());
    }

    public void b(SurfaceView surfaceView) {
        a(surfaceView == null ? null : surfaceView.getHolder());
    }

    public void b(TextureView textureView) {
        C();
        if (textureView != null && textureView == this.t) {
            a((TextureView) null);
        }
    }

    public void b(com.google.android.exoplayer2.video.VideoListener videoListener) {
        this.f.add(videoListener);
    }

    public void b(TextOutput textOutput) {
        if (!this.B.isEmpty()) {
            textOutput.a(this.B);
        }
        this.h.add(textOutput);
    }

    public void b(Player.EventListener eventListener) {
        C();
        this.c.b(eventListener);
    }

    public void b(boolean z2) {
        C();
        this.c.b(z2);
    }

    public PlaybackParameters b() {
        C();
        return this.c.b();
    }

    public void a(float f2) {
        C();
        float a2 = Util.a(f2, 0.0f, 1.0f);
        if (this.z != a2) {
            this.z = a2;
            B();
            Iterator<AudioListener> it2 = this.g.iterator();
            while (it2.hasNext()) {
                it2.next().a(a2);
            }
        }
    }

    public void a(AnalyticsListener analyticsListener) {
        C();
        this.m.a(analyticsListener);
    }

    public void a(com.google.android.exoplayer2.video.VideoListener videoListener) {
        this.f.remove(videoListener);
    }

    @Deprecated
    public void a(VideoListener videoListener) {
        this.f.clear();
        if (videoListener != null) {
            b((com.google.android.exoplayer2.video.VideoListener) videoListener);
        }
    }

    public void a(TextOutput textOutput) {
        this.h.remove(textOutput);
    }

    public void a(MetadataOutput metadataOutput) {
        this.i.add(metadataOutput);
    }

    public void a(Player.EventListener eventListener) {
        C();
        this.c.a(eventListener);
    }

    public void a(MediaSource mediaSource) {
        a(mediaSource, true, true);
    }

    public void a(MediaSource mediaSource, boolean z2, boolean z3) {
        C();
        MediaSource mediaSource2 = this.A;
        if (mediaSource2 != null) {
            mediaSource2.a((MediaSourceEventListener) this.m);
            this.m.f();
        }
        this.A = mediaSource;
        mediaSource.a(this.d, (MediaSourceEventListener) this.m);
        a(m(), this.n.a(m()));
        this.c.a(mediaSource, z2, z3);
    }

    public void a(boolean z2) {
        C();
        a(z2, this.n.a(z2, getPlaybackState()));
    }

    public void a(int i2, long j2) {
        C();
        this.m.e();
        this.c.a(i2, j2);
    }

    public PlayerMessage a(PlayerMessage.Target target) {
        C();
        return this.c.a(target);
    }

    public int a(int i2) {
        C();
        return this.c.a(i2);
    }

    /* access modifiers changed from: private */
    public void a(Surface surface, boolean z2) {
        ArrayList<PlayerMessage> arrayList = new ArrayList<>();
        for (Renderer renderer : this.b) {
            if (renderer.getTrackType() == 2) {
                PlayerMessage a2 = this.c.a((PlayerMessage.Target) renderer);
                a2.a(1);
                a2.a((Object) surface);
                a2.k();
                arrayList.add(a2);
            }
        }
        Surface surface2 = this.q;
        if (!(surface2 == null || surface2 == surface)) {
            try {
                for (PlayerMessage a3 : arrayList) {
                    a3.a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
            if (this.r) {
                this.q.release();
            }
        }
        this.q = surface;
        this.r = z2;
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        if (i2 != this.u || i3 != this.v) {
            this.u = i2;
            this.v = i3;
            Iterator<com.google.android.exoplayer2.video.VideoListener> it2 = this.f.iterator();
            while (it2.hasNext()) {
                it2.next().a(i2, i3);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, int i2) {
        ExoPlayerImpl exoPlayerImpl = this.c;
        boolean z3 = false;
        boolean z4 = z2 && i2 != -1;
        if (i2 != 1) {
            z3 = true;
        }
        exoPlayerImpl.a(z4, z3);
    }
}
