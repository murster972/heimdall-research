package com.google.android.exoplayer2;

import com.google.android.exoplayer2.util.Clock;
import com.google.android.exoplayer2.util.MediaClock;
import com.google.android.exoplayer2.util.StandaloneMediaClock;

final class DefaultMediaClock implements MediaClock {

    /* renamed from: a  reason: collision with root package name */
    private final StandaloneMediaClock f3153a;
    private final PlaybackParameterListener b;
    private Renderer c;
    private MediaClock d;

    public interface PlaybackParameterListener {
        void onPlaybackParametersChanged(PlaybackParameters playbackParameters);
    }

    public DefaultMediaClock(PlaybackParameterListener playbackParameterListener, Clock clock) {
        this.b = playbackParameterListener;
        this.f3153a = new StandaloneMediaClock(clock);
    }

    private void e() {
        this.f3153a.a(this.d.h());
        PlaybackParameters b2 = this.d.b();
        if (!b2.equals(this.f3153a.b())) {
            this.f3153a.a(b2);
            this.b.onPlaybackParametersChanged(b2);
        }
    }

    private boolean f() {
        Renderer renderer = this.c;
        return renderer != null && !renderer.a() && (this.c.isReady() || !this.c.c());
    }

    public void a() {
        this.f3153a.a();
    }

    public void b(Renderer renderer) throws ExoPlaybackException {
        MediaClock mediaClock;
        MediaClock j = renderer.j();
        if (j != null && j != (mediaClock = this.d)) {
            if (mediaClock == null) {
                this.d = j;
                this.c = renderer;
                this.d.a(this.f3153a.b());
                e();
                return;
            }
            throw ExoPlaybackException.a((RuntimeException) new IllegalStateException("Multiple renderer media clocks enabled."));
        }
    }

    public void c() {
        this.f3153a.c();
    }

    public long d() {
        if (!f()) {
            return this.f3153a.h();
        }
        e();
        return this.d.h();
    }

    public long h() {
        if (f()) {
            return this.d.h();
        }
        return this.f3153a.h();
    }

    public void a(long j) {
        this.f3153a.a(j);
    }

    public void a(Renderer renderer) {
        if (renderer == this.c) {
            this.d = null;
            this.c = null;
        }
    }

    public PlaybackParameters a(PlaybackParameters playbackParameters) {
        MediaClock mediaClock = this.d;
        if (mediaClock != null) {
            playbackParameters = mediaClock.a(playbackParameters);
        }
        this.f3153a.a(playbackParameters);
        this.b.onPlaybackParametersChanged(playbackParameters);
        return playbackParameters;
    }

    public PlaybackParameters b() {
        MediaClock mediaClock = this.d;
        if (mediaClock != null) {
            return mediaClock.b();
        }
        return this.f3153a.b();
    }
}
