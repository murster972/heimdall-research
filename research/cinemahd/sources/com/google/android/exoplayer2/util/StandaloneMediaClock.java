package com.google.android.exoplayer2.util;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.PlaybackParameters;

public final class StandaloneMediaClock implements MediaClock {

    /* renamed from: a  reason: collision with root package name */
    private final Clock f3647a;
    private boolean b;
    private long c;
    private long d;
    private PlaybackParameters e = PlaybackParameters.e;

    public StandaloneMediaClock(Clock clock) {
        this.f3647a = clock;
    }

    public void a() {
        if (!this.b) {
            this.d = this.f3647a.a();
            this.b = true;
        }
    }

    public PlaybackParameters b() {
        return this.e;
    }

    public void c() {
        if (this.b) {
            a(h());
            this.b = false;
        }
    }

    public long h() {
        long j;
        long j2 = this.c;
        if (!this.b) {
            return j2;
        }
        long a2 = this.f3647a.a() - this.d;
        PlaybackParameters playbackParameters = this.e;
        if (playbackParameters.f3170a == 1.0f) {
            j = C.a(a2);
        } else {
            j = playbackParameters.a(a2);
        }
        return j2 + j;
    }

    public void a(long j) {
        this.c = j;
        if (this.b) {
            this.d = this.f3647a.a();
        }
    }

    public PlaybackParameters a(PlaybackParameters playbackParameters) {
        if (this.b) {
            a(h());
        }
        this.e = playbackParameters;
        return playbackParameters;
    }
}
