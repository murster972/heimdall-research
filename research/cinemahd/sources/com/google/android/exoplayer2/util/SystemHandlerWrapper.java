package com.google.android.exoplayer2.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class SystemHandlerWrapper implements HandlerWrapper {

    /* renamed from: a  reason: collision with root package name */
    private final Handler f3648a;

    public SystemHandlerWrapper(Handler handler) {
        this.f3648a = handler;
    }

    public Looper a() {
        return this.f3648a.getLooper();
    }

    public void b(int i) {
        this.f3648a.removeMessages(i);
    }

    public Message a(int i, Object obj) {
        return this.f3648a.obtainMessage(i, obj);
    }

    public Message a(int i, int i2, int i3) {
        return this.f3648a.obtainMessage(i, i2, i3);
    }

    public Message a(int i, int i2, int i3, Object obj) {
        return this.f3648a.obtainMessage(i, i2, i3, obj);
    }

    public boolean a(int i) {
        return this.f3648a.sendEmptyMessage(i);
    }

    public boolean a(int i, long j) {
        return this.f3648a.sendEmptyMessageAtTime(i, j);
    }
}
