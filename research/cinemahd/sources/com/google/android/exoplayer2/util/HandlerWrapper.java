package com.google.android.exoplayer2.util;

import android.os.Looper;
import android.os.Message;

public interface HandlerWrapper {
    Looper a();

    Message a(int i, int i2, int i3);

    Message a(int i, int i2, int i3, Object obj);

    Message a(int i, Object obj);

    boolean a(int i);

    boolean a(int i, long j);

    void b(int i);
}
