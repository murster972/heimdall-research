package com.google.android.exoplayer2.util;

import java.util.Arrays;

public final class LongArray {

    /* renamed from: a  reason: collision with root package name */
    private int f3634a;
    private long[] b;

    public LongArray() {
        this(32);
    }

    public void a(long j) {
        int i = this.f3634a;
        long[] jArr = this.b;
        if (i == jArr.length) {
            this.b = Arrays.copyOf(jArr, i * 2);
        }
        long[] jArr2 = this.b;
        int i2 = this.f3634a;
        this.f3634a = i2 + 1;
        jArr2[i2] = j;
    }

    public long[] b() {
        return Arrays.copyOf(this.b, this.f3634a);
    }

    public LongArray(int i) {
        this.b = new long[i];
    }

    public long a(int i) {
        if (i >= 0 && i < this.f3634a) {
            return this.b[i];
        }
        throw new IndexOutOfBoundsException("Invalid index " + i + ", size is " + this.f3634a);
    }

    public int a() {
        return this.f3634a;
    }
}
