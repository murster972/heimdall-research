package com.google.android.exoplayer2.util;

import android.annotation.TargetApi;
import android.os.Trace;

public final class TraceUtil {
    private TraceUtil() {
    }

    public static void a(String str) {
        if (Util.f3651a >= 18) {
            b(str);
        }
    }

    @TargetApi(18)
    private static void b(String str) {
        Trace.beginSection(str);
    }

    @TargetApi(18)
    private static void b() {
        Trace.endSection();
    }

    public static void a() {
        if (Util.f3651a >= 18) {
            b();
        }
    }
}
