package com.google.android.exoplayer2.util;

import android.os.Handler;
import android.os.Looper;

final class SystemClock implements Clock {
    SystemClock() {
    }

    public long a() {
        return android.os.SystemClock.elapsedRealtime();
    }

    public long b() {
        return android.os.SystemClock.uptimeMillis();
    }

    public HandlerWrapper a(Looper looper, Handler.Callback callback) {
        return new SystemHandlerWrapper(new Handler(looper, callback));
    }
}
