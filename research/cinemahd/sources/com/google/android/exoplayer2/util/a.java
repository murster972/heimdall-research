package com.google.android.exoplayer2.util;

import com.google.android.exoplayer2.util.EventDispatcher;

/* compiled from: lambda */
public final /* synthetic */ class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ EventDispatcher.HandlerAndListener f3652a;
    private final /* synthetic */ EventDispatcher.Event b;

    public /* synthetic */ a(EventDispatcher.HandlerAndListener handlerAndListener, EventDispatcher.Event event) {
        this.f3652a = handlerAndListener;
        this.b = event;
    }

    public final void run() {
        this.f3652a.b(this.b);
    }
}
