package com.google.android.exoplayer2.util;

import com.facebook.imageutils.JfifUtil;

public final class ParsableBitArray {

    /* renamed from: a  reason: collision with root package name */
    public byte[] f3640a;
    private int b;
    private int c;
    private int d;

    public ParsableBitArray() {
        this.f3640a = Util.f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r2.d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void g() {
        /*
            r2 = this;
            int r0 = r2.b
            if (r0 < 0) goto L_0x0010
            int r1 = r2.d
            if (r0 < r1) goto L_0x000e
            if (r0 != r1) goto L_0x0010
            int r0 = r2.c
            if (r0 != 0) goto L_0x0010
        L_0x000e:
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            com.google.android.exoplayer2.util.Assertions.b(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.util.ParsableBitArray.g():void");
    }

    public void a(byte[] bArr) {
        a(bArr, bArr.length);
    }

    public void b(int i) {
        this.b = i / 8;
        this.c = i - (this.b * 8);
        g();
    }

    public int c() {
        Assertions.b(this.c == 0);
        return this.b;
    }

    public int d() {
        return (this.b * 8) + this.c;
    }

    public boolean e() {
        boolean z = (this.f3640a[this.b] & (128 >> this.c)) != 0;
        f();
        return z;
    }

    public void f() {
        int i = this.c + 1;
        this.c = i;
        if (i == 8) {
            this.c = 0;
            this.b++;
        }
        g();
    }

    public void a(ParsableByteArray parsableByteArray) {
        a(parsableByteArray.f3641a, parsableByteArray.d());
        b(parsableByteArray.c() * 8);
    }

    public void d(int i) {
        Assertions.b(this.c == 0);
        this.b += i;
        g();
    }

    public ParsableBitArray(byte[] bArr) {
        this(bArr, bArr.length);
    }

    public void c(int i) {
        int i2 = i / 8;
        this.b += i2;
        this.c += i - (i2 * 8);
        int i3 = this.c;
        if (i3 > 7) {
            this.b++;
            this.c = i3 - 8;
        }
        g();
    }

    public ParsableBitArray(byte[] bArr, int i) {
        this.f3640a = bArr;
        this.d = i;
    }

    public void a(byte[] bArr, int i) {
        this.f3640a = bArr;
        this.b = 0;
        this.c = 0;
        this.d = i;
    }

    public void b() {
        if (this.c != 0) {
            this.c = 0;
            this.b++;
            g();
        }
    }

    public int a() {
        return ((this.d - this.b) * 8) - this.c;
    }

    public void b(byte[] bArr, int i, int i2) {
        Assertions.b(this.c == 0);
        System.arraycopy(this.f3640a, this.b, bArr, i, i2);
        this.b += i2;
        g();
    }

    public int a(int i) {
        int i2;
        if (i == 0) {
            return 0;
        }
        this.c += i;
        int i3 = 0;
        while (true) {
            i2 = this.c;
            if (i2 <= 8) {
                break;
            }
            this.c = i2 - 8;
            byte[] bArr = this.f3640a;
            int i4 = this.b;
            this.b = i4 + 1;
            i3 |= (bArr[i4] & 255) << this.c;
        }
        byte[] bArr2 = this.f3640a;
        int i5 = this.b;
        int i6 = (-1 >>> (32 - i)) & (i3 | ((bArr2[i5] & 255) >> (8 - i2)));
        if (i2 == 8) {
            this.c = 0;
            this.b = i5 + 1;
        }
        g();
        return i6;
    }

    public void a(byte[] bArr, int i, int i2) {
        int i3 = (i2 >> 3) + i;
        while (i < i3) {
            byte[] bArr2 = this.f3640a;
            int i4 = this.b;
            this.b = i4 + 1;
            byte b2 = bArr2[i4];
            int i5 = this.c;
            bArr[i] = (byte) (b2 << i5);
            bArr[i] = (byte) (((255 & bArr2[this.b]) >> (8 - i5)) | bArr[i]);
            i++;
        }
        int i6 = i2 & 7;
        if (i6 != 0) {
            bArr[i3] = (byte) (bArr[i3] & (JfifUtil.MARKER_FIRST_BYTE >> i6));
            int i7 = this.c;
            if (i7 + i6 > 8) {
                byte b3 = bArr[i3];
                byte[] bArr3 = this.f3640a;
                int i8 = this.b;
                this.b = i8 + 1;
                bArr[i3] = (byte) (b3 | ((bArr3[i8] & 255) << i7));
                this.c = i7 - 8;
            }
            this.c += i6;
            byte[] bArr4 = this.f3640a;
            int i9 = this.b;
            int i10 = this.c;
            bArr[i3] = (byte) (((byte) (((bArr4[i9] & 255) >> (8 - i10)) << (8 - i6))) | bArr[i3]);
            if (i10 == 8) {
                this.c = 0;
                this.b = i9 + 1;
            }
            g();
        }
    }

    public void a(int i, int i2) {
        if (i2 < 32) {
            i &= (1 << i2) - 1;
        }
        int min = Math.min(8 - this.c, i2);
        int i3 = this.c;
        int i4 = (8 - i3) - min;
        byte[] bArr = this.f3640a;
        int i5 = this.b;
        bArr[i5] = (byte) (((65280 >> i3) | ((1 << i4) - 1)) & bArr[i5]);
        int i6 = i2 - min;
        bArr[i5] = (byte) (((i >>> i6) << i4) | bArr[i5]);
        int i7 = i5 + 1;
        while (i6 > 8) {
            this.f3640a[i7] = (byte) (i >>> (i6 - 8));
            i6 -= 8;
            i7++;
        }
        int i8 = 8 - i6;
        byte[] bArr2 = this.f3640a;
        bArr2[i7] = (byte) (bArr2[i7] & ((1 << i8) - 1));
        bArr2[i7] = (byte) (((i & ((1 << i6) - 1)) << i8) | bArr2[i7]);
        c(i2);
        g();
    }
}
