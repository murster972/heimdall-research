package com.google.android.exoplayer2.util;

import android.text.TextUtils;

public final class Log {

    /* renamed from: a  reason: collision with root package name */
    private static int f3633a = 0;
    private static boolean b = true;

    private Log() {
    }

    public static void a(String str, String str2) {
        if (f3633a == 0) {
            android.util.Log.d(str, str2);
        }
    }

    public static void b(String str, String str2, Throwable th) {
        if (!b) {
            d(str, a(str2, th));
        }
        if (f3633a <= 2) {
            android.util.Log.w(str, str2, th);
        }
    }

    public static void c(String str, String str2) {
        if (f3633a <= 1) {
            android.util.Log.i(str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (f3633a <= 2) {
            android.util.Log.w(str, str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (!b) {
            b(str, a(str2, th));
        }
        if (f3633a <= 3) {
            android.util.Log.e(str, str2, th);
        }
    }

    public static void b(String str, String str2) {
        if (f3633a <= 3) {
            android.util.Log.e(str, str2);
        }
    }

    private static String a(String str, Throwable th) {
        if (th == null) {
            return str;
        }
        String message = th.getMessage();
        if (TextUtils.isEmpty(message)) {
            return str;
        }
        return str + " - " + message;
    }
}
