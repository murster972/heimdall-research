package com.google.android.exoplayer2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class AtomicFile {

    /* renamed from: a  reason: collision with root package name */
    private final File f3621a;
    private final File b;

    private static final class AtomicFileOutputStream extends OutputStream {

        /* renamed from: a  reason: collision with root package name */
        private final FileOutputStream f3622a;
        private boolean b = false;

        public AtomicFileOutputStream(File file) throws FileNotFoundException {
            this.f3622a = new FileOutputStream(file);
        }

        public void close() throws IOException {
            if (!this.b) {
                this.b = true;
                flush();
                try {
                    this.f3622a.getFD().sync();
                } catch (IOException e) {
                    Log.b("AtomicFile", "Failed to sync file descriptor:", e);
                }
                this.f3622a.close();
            }
        }

        public void flush() throws IOException {
            this.f3622a.flush();
        }

        public void write(int i) throws IOException {
            this.f3622a.write(i);
        }

        public void write(byte[] bArr) throws IOException {
            this.f3622a.write(bArr);
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            this.f3622a.write(bArr, i, i2);
        }
    }

    public AtomicFile(File file) {
        this.f3621a = file;
        this.b = new File(file.getPath() + ".bak");
    }

    private void d() {
        if (this.b.exists()) {
            this.f3621a.delete();
            this.b.renameTo(this.f3621a);
        }
    }

    public void a() {
        this.f3621a.delete();
        this.b.delete();
    }

    public InputStream b() throws FileNotFoundException {
        d();
        return new FileInputStream(this.f3621a);
    }

    public OutputStream c() throws IOException {
        if (this.f3621a.exists()) {
            if (this.b.exists()) {
                this.f3621a.delete();
            } else if (!this.f3621a.renameTo(this.b)) {
                Log.d("AtomicFile", "Couldn't rename file " + this.f3621a + " to backup file " + this.b);
            }
        }
        try {
            return new AtomicFileOutputStream(this.f3621a);
        } catch (FileNotFoundException e) {
            File parentFile = this.f3621a.getParentFile();
            if (parentFile == null || !parentFile.mkdirs()) {
                throw new IOException("Couldn't create directory " + this.f3621a, e);
            }
            try {
                return new AtomicFileOutputStream(this.f3621a);
            } catch (FileNotFoundException e2) {
                throw new IOException("Couldn't create " + this.f3621a, e2);
            }
        }
    }

    public void a(OutputStream outputStream) throws IOException {
        outputStream.close();
        this.b.delete();
    }
}
