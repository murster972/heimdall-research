package com.google.android.exoplayer2.util;

import com.google.android.exoplayer2.PlaybackParameters;

public interface MediaClock {
    PlaybackParameters a(PlaybackParameters playbackParameters);

    PlaybackParameters b();

    long h();
}
