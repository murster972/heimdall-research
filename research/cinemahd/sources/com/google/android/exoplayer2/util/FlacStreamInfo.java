package com.google.android.exoplayer2.util;

public final class FlacStreamInfo {

    /* renamed from: a  reason: collision with root package name */
    public final int f3631a;
    public final int b;
    public final int c;
    public final long d;

    public FlacStreamInfo(byte[] bArr, int i) {
        ParsableBitArray parsableBitArray = new ParsableBitArray(bArr);
        parsableBitArray.b(i * 8);
        parsableBitArray.a(16);
        parsableBitArray.a(16);
        parsableBitArray.a(24);
        parsableBitArray.a(24);
        this.f3631a = parsableBitArray.a(20);
        this.b = parsableBitArray.a(3) + 1;
        this.c = parsableBitArray.a(5) + 1;
        this.d = ((((long) parsableBitArray.a(4)) & 15) << 32) | (((long) parsableBitArray.a(32)) & 4294967295L);
    }

    public int a() {
        return this.c * this.f3631a;
    }

    public long b() {
        return (this.d * 1000000) / ((long) this.f3631a);
    }
}
