package com.google.android.exoplayer2.util;

import android.os.Handler;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public final class EventDispatcher<T> {

    /* renamed from: a  reason: collision with root package name */
    private final CopyOnWriteArrayList<HandlerAndListener<T>> f3628a = new CopyOnWriteArrayList<>();

    public interface Event<T> {
        void a(T t);
    }

    private static final class HandlerAndListener<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f3629a;
        /* access modifiers changed from: private */
        public final T b;
        private boolean c;

        public HandlerAndListener(Handler handler, T t) {
            this.f3629a = handler;
            this.b = t;
        }

        public /* synthetic */ void b(Event event) {
            if (!this.c) {
                event.a(this.b);
            }
        }

        public void a() {
            this.c = true;
        }

        public void a(Event<T> event) {
            this.f3629a.post(new a(this, event));
        }
    }

    public void a(Handler handler, T t) {
        Assertions.a((handler == null || t == null) ? false : true);
        a(t);
        this.f3628a.add(new HandlerAndListener(handler, t));
    }

    public void a(T t) {
        Iterator<HandlerAndListener<T>> it2 = this.f3628a.iterator();
        while (it2.hasNext()) {
            HandlerAndListener next = it2.next();
            if (next.b == t) {
                next.a();
                this.f3628a.remove(next);
            }
        }
    }

    public void a(Event<T> event) {
        Iterator<HandlerAndListener<T>> it2 = this.f3628a.iterator();
        while (it2.hasNext()) {
            it2.next().a(event);
        }
    }
}
