package com.google.android.exoplayer2.util;

import android.os.SystemClock;
import android.view.Surface;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.analytics.a;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import okhttp3.HttpUrl;

public class EventLogger implements AnalyticsListener {
    private static final NumberFormat f = NumberFormat.getInstance(Locale.US);

    /* renamed from: a  reason: collision with root package name */
    private final MappingTrackSelector f3630a;
    private final String b;
    private final Timeline.Window c;
    private final Timeline.Period d;
    private final long e;

    static {
        f.setMinimumFractionDigits(2);
        f.setMaximumFractionDigits(2);
        f.setGroupingUsed(false);
    }

    public EventLogger(MappingTrackSelector mappingTrackSelector) {
        this(mappingTrackSelector, "EventLogger");
    }

    private static String a(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? i != 4 ? "?" : "INTERNAL" : "AD_INSERTION" : "SEEK_ADJUSTMENT" : "SEEK" : "PERIOD_TRANSITION";
    }

    private static String a(int i, int i2) {
        return i < 2 ? "N/A" : i2 != 0 ? i2 != 8 ? i2 != 16 ? "?" : "YES" : "YES_NOT_SEAMLESS" : "NO";
    }

    private static String a(boolean z) {
        return z ? "[X]" : "[ ]";
    }

    private static String b(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? i != 4 ? "?" : "YES" : "NO_EXCEEDS_CAPABILITIES" : "NO_UNSUPPORTED_DRM" : "NO_UNSUPPORTED_TYPE" : "NO";
    }

    private static String c(int i) {
        return i != 0 ? i != 1 ? i != 2 ? "?" : "ALL" : "ONE" : "OFF";
    }

    private static String d(int i) {
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? "?" : "ENDED" : "READY" : "BUFFERING" : "IDLE";
    }

    private static String e(int i) {
        return i != 0 ? i != 1 ? i != 2 ? "?" : "DYNAMIC" : "RESET" : "PREPARED";
    }

    private String j(AnalyticsListener.EventTime eventTime) {
        String str = "window=" + eventTime.c;
        if (eventTime.d != null) {
            str = str + ", period=" + eventTime.b.a(eventTime.d.f3414a);
            if (eventTime.d.a()) {
                str = (str + ", adGroup=" + eventTime.d.b) + ", ad=" + eventTime.d.c;
            }
        }
        return a(eventTime.f3182a - this.e) + ", " + a(eventTime.e) + ", " + str;
    }

    public /* synthetic */ void a(AnalyticsListener.EventTime eventTime, float f2) {
        a.a(this, eventTime, f2);
    }

    public void a(AnalyticsListener.EventTime eventTime, int i, long j, long j2) {
    }

    public void a(AnalyticsListener.EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
    }

    public void a(AnalyticsListener.EventTime eventTime, boolean z) {
        b(eventTime, "loading", Boolean.toString(z));
    }

    public void b(AnalyticsListener.EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
    }

    public void b(AnalyticsListener.EventTime eventTime, boolean z) {
        b(eventTime, "shuffleModeEnabled", Boolean.toString(z));
    }

    public void c(AnalyticsListener.EventTime eventTime, int i) {
        b(eventTime, "positionDiscontinuity", a(i));
    }

    public void c(AnalyticsListener.EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
    }

    public void d(AnalyticsListener.EventTime eventTime, int i) {
        b(eventTime, "repeatMode", c(i));
    }

    public void e(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "drmSessionAcquired");
    }

    public void f(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "seekProcessed");
    }

    public void g(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "drmSessionReleased");
    }

    public void h(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "drmKeysLoaded");
    }

    public void i(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "mediaPeriodReadingStarted");
    }

    public EventLogger(MappingTrackSelector mappingTrackSelector, String str) {
        this.f3630a = mappingTrackSelector;
        this.b = str;
        this.c = new Timeline.Window();
        this.d = new Timeline.Period();
        this.e = SystemClock.elapsedRealtime();
    }

    private static String f(int i) {
        switch (i) {
            case 0:
                return "default";
            case 1:
                return "audio";
            case 2:
                return Advertisement.KEY_VIDEO;
            case 3:
                return "text";
            case 4:
                return "metadata";
            case 5:
                return "camera motion";
            case 6:
                return ViewProps.NONE;
            default:
                if (i < 10000) {
                    return "?";
                }
                return "custom (" + i + ")";
        }
    }

    public void a(AnalyticsListener.EventTime eventTime, boolean z, int i) {
        b(eventTime, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE, z + ", " + d(i));
    }

    public void b(AnalyticsListener.EventTime eventTime, int i) {
        b(eventTime, "audioSessionId", Integer.toString(i));
    }

    public void c(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "mediaPeriodCreated");
    }

    public void d(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "seekStarted");
    }

    public void a(AnalyticsListener.EventTime eventTime, PlaybackParameters playbackParameters) {
        b(eventTime, "playbackParameters", Util.a("speed=%.2f, pitch=%.2f, skipSilence=%s", Float.valueOf(playbackParameters.f3170a), Float.valueOf(playbackParameters.b), Boolean.valueOf(playbackParameters.c)));
    }

    public void b(AnalyticsListener.EventTime eventTime, int i, DecoderCounters decoderCounters) {
        b(eventTime, "decoderDisabled", f(i));
    }

    public void b(AnalyticsListener.EventTime eventTime, int i, long j, long j2) {
        a(eventTime, "audioTrackUnderrun", i + ", " + j + ", " + j2 + "]", (Throwable) null);
    }

    public void b(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "mediaPeriodReleased");
    }

    public void b(AnalyticsListener.EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        b(eventTime, "downstreamFormatChanged", Format.c(mediaLoadData.c));
    }

    private void b(AnalyticsListener.EventTime eventTime, String str) {
        a(a(eventTime, str));
    }

    public void a(AnalyticsListener.EventTime eventTime, int i) {
        int a2 = eventTime.b.a();
        int b2 = eventTime.b.b();
        a("timelineChanged [" + j(eventTime) + ", periodCount=" + a2 + ", windowCount=" + b2 + ", reason=" + e(i));
        for (int i2 = 0; i2 < Math.min(a2, 3); i2++) {
            eventTime.b.a(i2, this.d);
            a("  period [" + a(this.d.c()) + "]");
        }
        if (a2 > 3) {
            a("  ...");
        }
        for (int i3 = 0; i3 < Math.min(b2, 3); i3++) {
            eventTime.b.a(i3, this.c);
            a("  window [" + a(this.c.c()) + ", " + this.c.f3177a + ", " + this.c.b + "]");
        }
        if (b2 > 3) {
            a("  ...");
        }
        a("]");
    }

    private void b(AnalyticsListener.EventTime eventTime, String str, String str2) {
        a(a(eventTime, str, str2));
    }

    public void a(AnalyticsListener.EventTime eventTime, ExoPlaybackException exoPlaybackException) {
        a(eventTime, "playerFailed", (Throwable) exoPlaybackException);
    }

    public void a(AnalyticsListener.EventTime eventTime, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        String str;
        String str2;
        int i;
        MappingTrackSelector mappingTrackSelector = this.f3630a;
        MappingTrackSelector.MappedTrackInfo c2 = mappingTrackSelector != null ? mappingTrackSelector.c() : null;
        if (c2 == null) {
            b(eventTime, "tracksChanged", HttpUrl.PATH_SEGMENT_ENCODE_SET_URI);
            return;
        }
        AnalyticsListener.EventTime eventTime2 = eventTime;
        a("tracksChanged [" + j(eventTime) + ", ");
        int a2 = c2.a();
        int i2 = 0;
        while (true) {
            str = "  ]";
            str2 = " [";
            if (i2 >= a2) {
                break;
            }
            TrackGroupArray c3 = c2.c(i2);
            TrackSelection a3 = trackSelectionArray.a(i2);
            if (c3.f3424a > 0) {
                StringBuilder sb = new StringBuilder();
                i = a2;
                sb.append("  Renderer:");
                sb.append(i2);
                sb.append(str2);
                a(sb.toString());
                int i3 = 0;
                while (i3 < c3.f3424a) {
                    TrackGroup a4 = c3.a(i3);
                    TrackGroupArray trackGroupArray2 = c3;
                    String str3 = str;
                    String a5 = a(a4.f3423a, c2.a(i2, i3, false));
                    a("    Group:" + i3 + ", adaptive_supported=" + a5 + str2);
                    int i4 = 0;
                    while (i4 < a4.f3423a) {
                        String a6 = a(a3, a4, i4);
                        String b2 = b(c2.a(i2, i3, i4));
                        String str4 = str2;
                        a("      " + a6 + " Track:" + i4 + ", " + Format.c(a4.a(i4)) + ", supported=" + b2);
                        i4++;
                        str2 = str4;
                    }
                    String str5 = str2;
                    a("    ]");
                    i3++;
                    TrackSelectionArray trackSelectionArray2 = trackSelectionArray;
                    c3 = trackGroupArray2;
                    str = str3;
                }
                String str6 = str;
                if (a3 != null) {
                    int i5 = 0;
                    while (true) {
                        if (i5 >= a3.length()) {
                            break;
                        }
                        Metadata metadata = a3.a(i5).e;
                        if (metadata != null) {
                            a("    Metadata [");
                            a(metadata, "      ");
                            a("    ]");
                            break;
                        }
                        i5++;
                    }
                }
                a(str6);
            } else {
                i = a2;
            }
            i2++;
            a2 = i;
        }
        String str7 = str;
        String str8 = str2;
        TrackGroupArray b3 = c2.b();
        if (b3.f3424a > 0) {
            a("  Renderer:None [");
            int i6 = 0;
            while (i6 < b3.f3424a) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("    Group:");
                sb2.append(i6);
                String str9 = str8;
                sb2.append(str9);
                a(sb2.toString());
                TrackGroup a7 = b3.a(i6);
                for (int i7 = 0; i7 < a7.f3423a; i7++) {
                    String a8 = a(false);
                    String b4 = b(0);
                    a("      " + a8 + " Track:" + i7 + ", " + Format.c(a7.a(i7)) + ", supported=" + b4);
                }
                a("    ]");
                i6++;
                str8 = str9;
            }
            a(str7);
        }
        a("]");
    }

    public void a(AnalyticsListener.EventTime eventTime, Metadata metadata) {
        a("metadata [" + j(eventTime) + ", ");
        a(metadata, "  ");
        a("]");
    }

    public void a(AnalyticsListener.EventTime eventTime, int i, DecoderCounters decoderCounters) {
        b(eventTime, "decoderEnabled", f(i));
    }

    public void a(AnalyticsListener.EventTime eventTime, int i, String str, long j) {
        b(eventTime, "decoderInitialized", f(i) + ", " + str);
    }

    public void a(AnalyticsListener.EventTime eventTime, int i, Format format) {
        b(eventTime, "decoderInputFormatChanged", f(i) + ", " + Format.c(format));
    }

    public void a(AnalyticsListener.EventTime eventTime, int i, long j) {
        b(eventTime, "droppedFrames", Integer.toString(i));
    }

    public void a(AnalyticsListener.EventTime eventTime, int i, int i2, int i3, float f2) {
        b(eventTime, "videoSizeChanged", i + ", " + i2);
    }

    public void a(AnalyticsListener.EventTime eventTime, Surface surface) {
        b(eventTime, "renderedFirstFrame", String.valueOf(surface));
    }

    public void a(AnalyticsListener.EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException iOException, boolean z) {
        a(eventTime, "loadError", (Exception) iOException);
    }

    public void a(AnalyticsListener.EventTime eventTime, int i, int i2) {
        b(eventTime, "surfaceSizeChanged", i + ", " + i2);
    }

    public void a(AnalyticsListener.EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        b(eventTime, "upstreamDiscarded", Format.c(mediaLoadData.c));
    }

    public void a(AnalyticsListener.EventTime eventTime, Exception exc) {
        a(eventTime, "drmSessionManagerError", exc);
    }

    public void a(AnalyticsListener.EventTime eventTime) {
        b(eventTime, "drmKeysRestored");
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        Log.a(this.b, str);
    }

    /* access modifiers changed from: protected */
    public void a(String str, Throwable th) {
        Log.a(this.b, str, th);
    }

    private void a(AnalyticsListener.EventTime eventTime, String str, Throwable th) {
        a(a(eventTime, str), th);
    }

    private void a(AnalyticsListener.EventTime eventTime, String str, String str2, Throwable th) {
        a(a(eventTime, str, str2), th);
    }

    private void a(AnalyticsListener.EventTime eventTime, String str, Exception exc) {
        a(eventTime, "internalError", str, (Throwable) exc);
    }

    private void a(Metadata metadata, String str) {
        for (int i = 0; i < metadata.a(); i++) {
            a(str + metadata.a(i));
        }
    }

    private String a(AnalyticsListener.EventTime eventTime, String str) {
        return str + " [" + j(eventTime) + "]";
    }

    private String a(AnalyticsListener.EventTime eventTime, String str, String str2) {
        return str + " [" + j(eventTime) + ", " + str2 + "]";
    }

    private static String a(long j) {
        return j == -9223372036854775807L ? "?" : f.format((double) (((float) j) / 1000.0f));
    }

    private static String a(TrackSelection trackSelection, TrackGroup trackGroup, int i) {
        return a((trackSelection == null || trackSelection.c() != trackGroup || trackSelection.c(i) == -1) ? false : true);
    }
}
