package com.google.android.exoplayer2.util;

import com.facebook.common.time.Clock;
import java.util.Arrays;

public final class TimedValueQueue<V> {

    /* renamed from: a  reason: collision with root package name */
    private long[] f3649a;
    private V[] b;
    private int c;
    private int d;

    public TimedValueQueue() {
        this(10);
    }

    private void b(long j) {
        int i = this.d;
        if (i > 0) {
            if (j <= this.f3649a[((this.c + i) - 1) % this.b.length]) {
                a();
            }
        }
    }

    public synchronized void a(long j, V v) {
        b(j);
        b();
        b(j, v);
    }

    public TimedValueQueue(int i) {
        this.f3649a = new long[i];
        this.b = a(i);
    }

    private void b() {
        int length = this.b.length;
        if (this.d >= length) {
            int i = length * 2;
            long[] jArr = new long[i];
            V[] a2 = a(i);
            int i2 = this.c;
            int i3 = length - i2;
            System.arraycopy(this.f3649a, i2, jArr, 0, i3);
            System.arraycopy(this.b, this.c, a2, 0, i3);
            int i4 = this.c;
            if (i4 > 0) {
                System.arraycopy(this.f3649a, 0, jArr, i3, i4);
                System.arraycopy(this.b, 0, a2, i3, this.c);
            }
            this.f3649a = jArr;
            this.b = a2;
            this.c = 0;
        }
    }

    public synchronized void a() {
        this.c = 0;
        this.d = 0;
        Arrays.fill(this.b, (Object) null);
    }

    public synchronized V a(long j) {
        return a(j, true);
    }

    private V a(long j, boolean z) {
        long j2 = Clock.MAX_TIME;
        V v = null;
        while (this.d > 0) {
            long j3 = j - this.f3649a[this.c];
            if (j3 < 0 && (z || (-j3) >= j2)) {
                break;
            }
            V[] vArr = this.b;
            int i = this.c;
            v = vArr[i];
            vArr[i] = null;
            this.c = (i + 1) % vArr.length;
            this.d--;
            j2 = j3;
        }
        return v;
    }

    private static <V> V[] a(int i) {
        return new Object[i];
    }

    private void b(long j, V v) {
        int i = this.c;
        int i2 = this.d;
        V[] vArr = this.b;
        int length = (i + i2) % vArr.length;
        this.f3649a[length] = j;
        vArr[length] = v;
        this.d = i2 + 1;
    }
}
