package com.google.android.exoplayer2.util;

import com.google.android.exoplayer2.util.SlidingPercentile;
import java.util.Comparator;

/* compiled from: lambda */
public final /* synthetic */ class b implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ b f3653a = new b();

    private /* synthetic */ b() {
    }

    public final int compare(Object obj, Object obj2) {
        return SlidingPercentile.a((SlidingPercentile.Sample) obj, (SlidingPercentile.Sample) obj2);
    }
}
