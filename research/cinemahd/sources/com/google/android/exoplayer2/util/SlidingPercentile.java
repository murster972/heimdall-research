package com.google.android.exoplayer2.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SlidingPercentile {
    private static final Comparator<Sample> h = b.f3653a;
    private static final Comparator<Sample> i = c.f3654a;

    /* renamed from: a  reason: collision with root package name */
    private final int f3645a;
    private final ArrayList<Sample> b = new ArrayList<>();
    private final Sample[] c = new Sample[5];
    private int d = -1;
    private int e;
    private int f;
    private int g;

    private static class Sample {

        /* renamed from: a  reason: collision with root package name */
        public int f3646a;
        public int b;
        public float c;

        private Sample() {
        }
    }

    public SlidingPercentile(int i2) {
        this.f3645a = i2;
    }

    static /* synthetic */ int a(Sample sample, Sample sample2) {
        return sample.f3646a - sample2.f3646a;
    }

    private void b() {
        if (this.d != 0) {
            Collections.sort(this.b, i);
            this.d = 0;
        }
    }

    public void a(int i2, float f2) {
        Sample sample;
        a();
        int i3 = this.g;
        if (i3 > 0) {
            Sample[] sampleArr = this.c;
            int i4 = i3 - 1;
            this.g = i4;
            sample = sampleArr[i4];
        } else {
            sample = new Sample();
        }
        int i5 = this.e;
        this.e = i5 + 1;
        sample.f3646a = i5;
        sample.b = i2;
        sample.c = f2;
        this.b.add(sample);
        this.f += i2;
        while (true) {
            int i6 = this.f;
            int i7 = this.f3645a;
            if (i6 > i7) {
                int i8 = i6 - i7;
                Sample sample2 = this.b.get(0);
                int i9 = sample2.b;
                if (i9 <= i8) {
                    this.f -= i9;
                    this.b.remove(0);
                    int i10 = this.g;
                    if (i10 < 5) {
                        Sample[] sampleArr2 = this.c;
                        this.g = i10 + 1;
                        sampleArr2[i10] = sample2;
                    }
                } else {
                    sample2.b = i9 - i8;
                    this.f -= i8;
                }
            } else {
                return;
            }
        }
    }

    public float a(float f2) {
        b();
        float f3 = f2 * ((float) this.f);
        int i2 = 0;
        for (int i3 = 0; i3 < this.b.size(); i3++) {
            Sample sample = this.b.get(i3);
            i2 += sample.b;
            if (((float) i2) >= f3) {
                return sample.c;
            }
        }
        if (this.b.isEmpty()) {
            return Float.NaN;
        }
        ArrayList<Sample> arrayList = this.b;
        return arrayList.get(arrayList.size() - 1).c;
    }

    private void a() {
        if (this.d != 1) {
            Collections.sort(this.b, h);
            this.d = 1;
        }
    }
}
