package com.google.android.exoplayer2.util;

import com.facebook.common.time.Clock;

public final class TimestampAdjuster {

    /* renamed from: a  reason: collision with root package name */
    private long f3650a;
    private long b;
    private volatile long c = -9223372036854775807L;

    public TimestampAdjuster(long j) {
        c(j);
    }

    public long a() {
        return this.f3650a;
    }

    public long b() {
        if (this.c != -9223372036854775807L) {
            return this.b + this.c;
        }
        long j = this.f3650a;
        if (j != Clock.MAX_TIME) {
            return j;
        }
        return -9223372036854775807L;
    }

    public synchronized void c(long j) {
        Assertions.b(this.c == -9223372036854775807L);
        this.f3650a = j;
    }

    public void d() {
        this.c = -9223372036854775807L;
    }

    public synchronized void e() throws InterruptedException {
        while (this.c == -9223372036854775807L) {
            wait();
        }
    }

    public static long d(long j) {
        return (j * 1000000) / 90000;
    }

    public long a(long j) {
        if (j == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        if (this.c != -9223372036854775807L) {
            this.c = j;
        } else {
            long j2 = this.f3650a;
            if (j2 != Clock.MAX_TIME) {
                this.b = j2 - j;
            }
            synchronized (this) {
                this.c = j;
                notifyAll();
            }
        }
        return j + this.b;
    }

    public long b(long j) {
        if (j == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        if (this.c != -9223372036854775807L) {
            long e = e(this.c);
            long j2 = (4294967296L + e) / 8589934592L;
            long j3 = ((j2 - 1) * 8589934592L) + j;
            j += j2 * 8589934592L;
            if (Math.abs(j3 - e) < Math.abs(j - e)) {
                j = j3;
            }
        }
        return a(d(j));
    }

    public static long e(long j) {
        return (j * 90000) / 1000000;
    }

    public long c() {
        if (this.f3650a == Clock.MAX_TIME) {
            return 0;
        }
        if (this.c == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        return this.b;
    }
}
