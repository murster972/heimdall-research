package com.google.android.exoplayer2.util;

import java.util.concurrent.ThreadFactory;

/* compiled from: lambda */
public final /* synthetic */ class d implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f3655a;

    public /* synthetic */ d(String str) {
        this.f3655a = str;
    }

    public final Thread newThread(Runnable runnable) {
        return Util.a(this.f3655a, runnable);
    }
}
