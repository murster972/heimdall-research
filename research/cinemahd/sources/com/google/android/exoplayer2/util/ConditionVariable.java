package com.google.android.exoplayer2.util;

public final class ConditionVariable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3626a;

    public synchronized void a() throws InterruptedException {
        while (!this.f3626a) {
            wait();
        }
    }

    public synchronized boolean b() {
        boolean z;
        z = this.f3626a;
        this.f3626a = false;
        return z;
    }

    public synchronized boolean c() {
        if (this.f3626a) {
            return false;
        }
        this.f3626a = true;
        notifyAll();
        return true;
    }
}
