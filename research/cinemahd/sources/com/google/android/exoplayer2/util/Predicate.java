package com.google.android.exoplayer2.util;

public interface Predicate<T> {
    boolean a(T t);
}
