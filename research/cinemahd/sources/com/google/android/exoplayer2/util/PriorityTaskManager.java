package com.google.android.exoplayer2.util;

import java.io.IOException;
import java.util.Collections;
import java.util.PriorityQueue;

public final class PriorityTaskManager {

    /* renamed from: a  reason: collision with root package name */
    private final Object f3643a = new Object();
    private final PriorityQueue<Integer> b = new PriorityQueue<>(10, Collections.reverseOrder());
    private int c = Integer.MIN_VALUE;

    public static class PriorityTooLowException extends IOException {
        public PriorityTooLowException(int i, int i2) {
            super("Priority too low [priority=" + i + ", highest=" + i2 + "]");
        }
    }

    public void a(int i) {
        synchronized (this.f3643a) {
            this.b.add(Integer.valueOf(i));
            this.c = Math.max(this.c, i);
        }
    }

    public void b(int i) throws InterruptedException {
        synchronized (this.f3643a) {
            while (this.c != i) {
                this.f3643a.wait();
            }
        }
    }

    public void c(int i) throws PriorityTooLowException {
        synchronized (this.f3643a) {
            if (this.c != i) {
                throw new PriorityTooLowException(i, this.c);
            }
        }
    }

    public void d(int i) {
        int i2;
        synchronized (this.f3643a) {
            this.b.remove(Integer.valueOf(i));
            if (this.b.isEmpty()) {
                i2 = Integer.MIN_VALUE;
            } else {
                Integer peek = this.b.peek();
                Util.a(peek);
                i2 = peek.intValue();
            }
            this.c = i2;
            this.f3643a.notifyAll();
        }
    }
}
