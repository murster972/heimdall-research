package com.google.android.exoplayer2;

public interface ControlDispatcher {
    boolean a(Player player, int i);

    boolean a(Player player, int i, long j);

    boolean a(Player player, boolean z);

    boolean b(Player player, boolean z);
}
