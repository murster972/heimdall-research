package com.google.android.exoplayer2;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.metadata.MetadataOutput;
import com.google.android.exoplayer2.metadata.MetadataRenderer;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.text.TextRenderer;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.video.MediaCodecVideoRenderer;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import com.google.android.exoplayer2.video.spherical.CameraMotionRenderer;
import java.util.ArrayList;

public class DefaultRenderersFactory implements RenderersFactory {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3154a;
    private final DrmSessionManager<FrameworkMediaCrypto> b;
    private final int c;
    private final long d;

    public DefaultRenderersFactory(Context context) {
        this(context, 0);
    }

    /* access modifiers changed from: protected */
    public void a(Context context, Handler handler, int i, ArrayList<Renderer> arrayList) {
    }

    public Renderer[] a(Handler handler, VideoRendererEventListener videoRendererEventListener, AudioRendererEventListener audioRendererEventListener, TextOutput textOutput, MetadataOutput metadataOutput, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager) {
        DrmSessionManager<FrameworkMediaCrypto> drmSessionManager2 = drmSessionManager == null ? this.b : drmSessionManager;
        ArrayList arrayList = new ArrayList();
        DrmSessionManager<FrameworkMediaCrypto> drmSessionManager3 = drmSessionManager2;
        a(this.f3154a, drmSessionManager3, this.d, handler, videoRendererEventListener, this.c, (ArrayList<Renderer>) arrayList);
        a(this.f3154a, drmSessionManager3, a(), handler, audioRendererEventListener, this.c, (ArrayList<Renderer>) arrayList);
        ArrayList arrayList2 = arrayList;
        a(this.f3154a, textOutput, handler.getLooper(), this.c, (ArrayList<Renderer>) arrayList2);
        a(this.f3154a, metadataOutput, handler.getLooper(), this.c, (ArrayList<Renderer>) arrayList2);
        a(this.f3154a, this.c, arrayList);
        Handler handler2 = handler;
        a(this.f3154a, handler, this.c, arrayList);
        return (Renderer[]) arrayList.toArray(new Renderer[arrayList.size()]);
    }

    /* access modifiers changed from: protected */
    public AudioProcessor[] a() {
        return new AudioProcessor[0];
    }

    public DefaultRenderersFactory(Context context, int i) {
        this(context, i, 5000);
    }

    public DefaultRenderersFactory(Context context, int i, long j) {
        this.f3154a = context;
        this.c = i;
        this.d = j;
        this.b = null;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, long j, Handler handler, VideoRendererEventListener videoRendererEventListener, int i, ArrayList<Renderer> arrayList) {
        int i2 = i;
        ArrayList<Renderer> arrayList2 = arrayList;
        arrayList2.add(new MediaCodecVideoRenderer(context, MediaCodecSelector.f3360a, j, drmSessionManager, false, handler, videoRendererEventListener, 50));
        if (i2 != 0) {
            int size = arrayList.size();
            if (i2 == 2) {
                size--;
            }
            try {
                arrayList2.add(size, (Renderer) Class.forName("com.google.android.exoplayer2.ext.vp9.LibvpxVideoRenderer").getConstructor(new Class[]{Boolean.TYPE, Long.TYPE, Handler.class, VideoRendererEventListener.class, Integer.TYPE}).newInstance(new Object[]{true, Long.valueOf(j), handler, videoRendererEventListener, 50}));
                Log.c("DefaultRenderersFactory", "Loaded LibvpxVideoRenderer.");
            } catch (ClassNotFoundException unused) {
            } catch (Exception e) {
                throw new RuntimeException("Error instantiating VP9 extension", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0060, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0068, code lost:
        throw new java.lang.RuntimeException("Error instantiating Opus extension", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0097, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009f, code lost:
        throw new java.lang.RuntimeException("Error instantiating FLAC extension", r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0060 A[ExcHandler: Exception (r0v16 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:7:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0097 A[ExcHandler: Exception (r0v10 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:19:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.content.Context r16, com.google.android.exoplayer2.drm.DrmSessionManager<com.google.android.exoplayer2.drm.FrameworkMediaCrypto> r17, com.google.android.exoplayer2.audio.AudioProcessor[] r18, android.os.Handler r19, com.google.android.exoplayer2.audio.AudioRendererEventListener r20, int r21, java.util.ArrayList<com.google.android.exoplayer2.Renderer> r22) {
        /*
            r15 = this;
            r0 = r21
            r10 = r22
            java.lang.String r11 = "DefaultRenderersFactory"
            java.lang.Class<com.google.android.exoplayer2.audio.AudioProcessor[]> r12 = com.google.android.exoplayer2.audio.AudioProcessor[].class
            java.lang.Class<com.google.android.exoplayer2.audio.AudioRendererEventListener> r13 = com.google.android.exoplayer2.audio.AudioRendererEventListener.class
            com.google.android.exoplayer2.audio.MediaCodecAudioRenderer r14 = new com.google.android.exoplayer2.audio.MediaCodecAudioRenderer
            com.google.android.exoplayer2.mediacodec.MediaCodecSelector r3 = com.google.android.exoplayer2.mediacodec.MediaCodecSelector.f3360a
            com.google.android.exoplayer2.audio.AudioCapabilities r8 = com.google.android.exoplayer2.audio.AudioCapabilities.a((android.content.Context) r16)
            r5 = 0
            r1 = r14
            r2 = r16
            r4 = r17
            r6 = r19
            r7 = r20
            r9 = r18
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            r10.add(r14)
            if (r0 != 0) goto L_0x0027
            return
        L_0x0027:
            int r1 = r22.size()
            r2 = 2
            if (r0 != r2) goto L_0x0030
            int r1 = r1 + -1
        L_0x0030:
            java.lang.String r0 = "com.google.android.exoplayer2.ext.opus.LibopusAudioRenderer"
            r3 = 0
            r4 = 3
            r5 = 1
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            java.lang.Class[] r6 = new java.lang.Class[r4]     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            java.lang.Class<android.os.Handler> r7 = android.os.Handler.class
            r6[r3] = r7     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            r6[r5] = r13     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            r6[r2] = r12     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            java.lang.reflect.Constructor r0 = r0.getConstructor(r6)     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            java.lang.Object[] r6 = new java.lang.Object[r4]     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            r6[r3] = r19     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            r6[r5] = r20     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            r6[r2] = r18     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            java.lang.Object r0 = r0.newInstance(r6)     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            com.google.android.exoplayer2.Renderer r0 = (com.google.android.exoplayer2.Renderer) r0     // Catch:{ ClassNotFoundException -> 0x0069, Exception -> 0x0060 }
            int r6 = r1 + 1
            r10.add(r1, r0)     // Catch:{ ClassNotFoundException -> 0x006a, Exception -> 0x0060 }
            java.lang.String r0 = "Loaded LibopusAudioRenderer."
            com.google.android.exoplayer2.util.Log.c(r11, r0)     // Catch:{ ClassNotFoundException -> 0x006a, Exception -> 0x0060 }
            goto L_0x006a
        L_0x0060:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "Error instantiating Opus extension"
            r1.<init>(r2, r0)
            throw r1
        L_0x0069:
            r6 = r1
        L_0x006a:
            java.lang.String r0 = "com.google.android.exoplayer2.ext.flac.LibflacAudioRenderer"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            java.lang.Class[] r1 = new java.lang.Class[r4]     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            java.lang.Class<android.os.Handler> r7 = android.os.Handler.class
            r1[r3] = r7     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            r1[r5] = r13     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            r1[r2] = r12     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            java.lang.reflect.Constructor r0 = r0.getConstructor(r1)     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            java.lang.Object[] r1 = new java.lang.Object[r4]     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            r1[r3] = r19     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            r1[r5] = r20     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            r1[r2] = r18     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            java.lang.Object r0 = r0.newInstance(r1)     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            com.google.android.exoplayer2.Renderer r0 = (com.google.android.exoplayer2.Renderer) r0     // Catch:{ ClassNotFoundException -> 0x00a0, Exception -> 0x0097 }
            int r1 = r6 + 1
            r10.add(r6, r0)     // Catch:{ ClassNotFoundException -> 0x00a1, Exception -> 0x0097 }
            java.lang.String r0 = "Loaded LibflacAudioRenderer."
            com.google.android.exoplayer2.util.Log.c(r11, r0)     // Catch:{ ClassNotFoundException -> 0x00a1, Exception -> 0x0097 }
            goto L_0x00a1
        L_0x0097:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "Error instantiating FLAC extension"
            r1.<init>(r2, r0)
            throw r1
        L_0x00a0:
            r1 = r6
        L_0x00a1:
            java.lang.String r0 = "com.google.android.exoplayer2.ext.ffmpeg.FfmpegAudioRenderer"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            java.lang.Class[] r6 = new java.lang.Class[r4]     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            java.lang.Class<android.os.Handler> r7 = android.os.Handler.class
            r6[r3] = r7     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            r6[r5] = r13     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            r6[r2] = r12     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            java.lang.reflect.Constructor r0 = r0.getConstructor(r6)     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            r4[r3] = r19     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            r4[r5] = r20     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            r4[r2] = r18     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            java.lang.Object r0 = r0.newInstance(r4)     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            com.google.android.exoplayer2.Renderer r0 = (com.google.android.exoplayer2.Renderer) r0     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            r10.add(r1, r0)     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            java.lang.String r0 = "Loaded FfmpegAudioRenderer."
            com.google.android.exoplayer2.util.Log.c(r11, r0)     // Catch:{ ClassNotFoundException -> 0x00d5, Exception -> 0x00cc }
            goto L_0x00d5
        L_0x00cc:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r2 = "Error instantiating FFmpeg extension"
            r1.<init>(r2, r0)
            throw r1
        L_0x00d5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.DefaultRenderersFactory.a(android.content.Context, com.google.android.exoplayer2.drm.DrmSessionManager, com.google.android.exoplayer2.audio.AudioProcessor[], android.os.Handler, com.google.android.exoplayer2.audio.AudioRendererEventListener, int, java.util.ArrayList):void");
    }

    /* access modifiers changed from: protected */
    public void a(Context context, TextOutput textOutput, Looper looper, int i, ArrayList<Renderer> arrayList) {
        arrayList.add(new TextRenderer(textOutput, looper));
    }

    /* access modifiers changed from: protected */
    public void a(Context context, MetadataOutput metadataOutput, Looper looper, int i, ArrayList<Renderer> arrayList) {
        arrayList.add(new MetadataRenderer(metadataOutput, looper));
    }

    /* access modifiers changed from: protected */
    public void a(Context context, int i, ArrayList<Renderer> arrayList) {
        arrayList.add(new CameraMotionRenderer());
    }
}
