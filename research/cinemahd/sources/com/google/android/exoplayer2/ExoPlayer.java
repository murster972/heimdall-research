package com.google.android.exoplayer2;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.PlayerMessage;

public interface ExoPlayer extends Player {

    @Deprecated
    public interface EventListener extends Player.EventListener {
    }

    PlayerMessage a(PlayerMessage.Target target);
}
