package com.google.android.exoplayer2.analytics;

import android.view.Surface;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import java.io.IOException;

public interface AnalyticsListener {

    public static final class EventTime {

        /* renamed from: a  reason: collision with root package name */
        public final long f3182a;
        public final Timeline b;
        public final int c;
        public final MediaSource.MediaPeriodId d;
        public final long e;

        public EventTime(long j, Timeline timeline, int i, MediaSource.MediaPeriodId mediaPeriodId, long j2, long j3, long j4) {
            this.f3182a = j;
            this.b = timeline;
            this.c = i;
            this.d = mediaPeriodId;
            this.e = j3;
        }
    }

    void a(EventTime eventTime);

    void a(EventTime eventTime, float f);

    void a(EventTime eventTime, int i);

    void a(EventTime eventTime, int i, int i2);

    void a(EventTime eventTime, int i, int i2, int i3, float f);

    void a(EventTime eventTime, int i, long j);

    void a(EventTime eventTime, int i, long j, long j2);

    void a(EventTime eventTime, int i, Format format);

    void a(EventTime eventTime, int i, DecoderCounters decoderCounters);

    void a(EventTime eventTime, int i, String str, long j);

    void a(EventTime eventTime, Surface surface);

    void a(EventTime eventTime, ExoPlaybackException exoPlaybackException);

    void a(EventTime eventTime, PlaybackParameters playbackParameters);

    void a(EventTime eventTime, Metadata metadata);

    void a(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData);

    void a(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException iOException, boolean z);

    void a(EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData);

    void a(EventTime eventTime, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray);

    void a(EventTime eventTime, Exception exc);

    void a(EventTime eventTime, boolean z);

    void a(EventTime eventTime, boolean z, int i);

    void b(EventTime eventTime);

    void b(EventTime eventTime, int i);

    void b(EventTime eventTime, int i, long j, long j2);

    void b(EventTime eventTime, int i, DecoderCounters decoderCounters);

    void b(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData);

    void b(EventTime eventTime, MediaSourceEventListener.MediaLoadData mediaLoadData);

    void b(EventTime eventTime, boolean z);

    void c(EventTime eventTime);

    void c(EventTime eventTime, int i);

    void c(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData);

    void d(EventTime eventTime);

    void d(EventTime eventTime, int i);

    void e(EventTime eventTime);

    void f(EventTime eventTime);

    void g(EventTime eventTime);

    void h(EventTime eventTime);

    void i(EventTime eventTime);
}
