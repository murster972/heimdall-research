package com.google.android.exoplayer2.analytics;

import android.view.Surface;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.audio.AudioListener;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.drm.DefaultDrmSessionEventListener;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataOutput;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Clock;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

public class AnalyticsCollector implements Player.EventListener, MetadataOutput, AudioRendererEventListener, VideoRendererEventListener, MediaSourceEventListener, BandwidthMeter.EventListener, DefaultDrmSessionEventListener, VideoListener, AudioListener {

    /* renamed from: a  reason: collision with root package name */
    private final CopyOnWriteArraySet<AnalyticsListener> f3179a;
    private final Clock b;
    private final Timeline.Window c;
    private final MediaPeriodQueueTracker d;
    private Player e;

    public static class Factory {
        public AnalyticsCollector a(Player player, Clock clock) {
            return new AnalyticsCollector(player, clock);
        }
    }

    private static final class MediaPeriodInfo {

        /* renamed from: a  reason: collision with root package name */
        public final MediaSource.MediaPeriodId f3180a;
        public final Timeline b;
        public final int c;

        public MediaPeriodInfo(MediaSource.MediaPeriodId mediaPeriodId, Timeline timeline, int i) {
            this.f3180a = mediaPeriodId;
            this.b = timeline;
            this.c = i;
        }
    }

    private static final class MediaPeriodQueueTracker {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final ArrayList<MediaPeriodInfo> f3181a = new ArrayList<>();
        private final HashMap<MediaSource.MediaPeriodId, MediaPeriodInfo> b = new HashMap<>();
        private final Timeline.Period c = new Timeline.Period();
        private MediaPeriodInfo d;
        private MediaPeriodInfo e;
        private Timeline f = Timeline.f3175a;
        private boolean g;

        private void h() {
            if (!this.f3181a.isEmpty()) {
                this.d = this.f3181a.get(0);
            }
        }

        public MediaPeriodInfo b() {
            if (this.f3181a.isEmpty()) {
                return null;
            }
            ArrayList<MediaPeriodInfo> arrayList = this.f3181a;
            return arrayList.get(arrayList.size() - 1);
        }

        public MediaPeriodInfo c() {
            if (this.f3181a.isEmpty() || this.f.c() || this.g) {
                return null;
            }
            return this.f3181a.get(0);
        }

        public MediaPeriodInfo d() {
            return this.e;
        }

        public boolean e() {
            return this.g;
        }

        public void f() {
            this.g = false;
            h();
        }

        public void g() {
            this.g = true;
        }

        public MediaPeriodInfo a() {
            return this.d;
        }

        public MediaPeriodInfo a(MediaSource.MediaPeriodId mediaPeriodId) {
            return this.b.get(mediaPeriodId);
        }

        public MediaPeriodInfo b(int i) {
            MediaPeriodInfo mediaPeriodInfo = null;
            for (int i2 = 0; i2 < this.f3181a.size(); i2++) {
                MediaPeriodInfo mediaPeriodInfo2 = this.f3181a.get(i2);
                int a2 = this.f.a(mediaPeriodInfo2.f3180a.f3414a);
                if (a2 != -1 && this.f.a(a2, this.c).b == i) {
                    if (mediaPeriodInfo != null) {
                        return null;
                    }
                    mediaPeriodInfo = mediaPeriodInfo2;
                }
            }
            return mediaPeriodInfo;
        }

        public void c(MediaSource.MediaPeriodId mediaPeriodId) {
            this.e = this.b.get(mediaPeriodId);
        }

        public void a(int i) {
            h();
        }

        public void a(Timeline timeline) {
            for (int i = 0; i < this.f3181a.size(); i++) {
                MediaPeriodInfo a2 = a(this.f3181a.get(i), timeline);
                this.f3181a.set(i, a2);
                this.b.put(a2.f3180a, a2);
            }
            MediaPeriodInfo mediaPeriodInfo = this.e;
            if (mediaPeriodInfo != null) {
                this.e = a(mediaPeriodInfo, timeline);
            }
            this.f = timeline;
            h();
        }

        public boolean b(MediaSource.MediaPeriodId mediaPeriodId) {
            MediaPeriodInfo remove = this.b.remove(mediaPeriodId);
            if (remove == null) {
                return false;
            }
            this.f3181a.remove(remove);
            MediaPeriodInfo mediaPeriodInfo = this.e;
            if (mediaPeriodInfo == null || !mediaPeriodId.equals(mediaPeriodInfo.f3180a)) {
                return true;
            }
            this.e = this.f3181a.isEmpty() ? null : this.f3181a.get(0);
            return true;
        }

        public void a(int i, MediaSource.MediaPeriodId mediaPeriodId) {
            MediaPeriodInfo mediaPeriodInfo = new MediaPeriodInfo(mediaPeriodId, this.f.a(mediaPeriodId.f3414a) != -1 ? this.f : Timeline.f3175a, i);
            this.f3181a.add(mediaPeriodInfo);
            this.b.put(mediaPeriodId, mediaPeriodInfo);
            if (this.f3181a.size() == 1 && !this.f.c()) {
                h();
            }
        }

        private MediaPeriodInfo a(MediaPeriodInfo mediaPeriodInfo, Timeline timeline) {
            int a2 = timeline.a(mediaPeriodInfo.f3180a.f3414a);
            if (a2 == -1) {
                return mediaPeriodInfo;
            }
            return new MediaPeriodInfo(mediaPeriodInfo.f3180a, timeline, timeline.a(a2, this.c).b);
        }
    }

    protected AnalyticsCollector(Player player, Clock clock) {
        if (player != null) {
            this.e = player;
        }
        Assertions.a(clock);
        this.b = clock;
        this.f3179a = new CopyOnWriteArraySet<>();
        this.d = new MediaPeriodQueueTracker();
        this.c = new Timeline.Window();
    }

    private AnalyticsListener.EventTime g() {
        return a(this.d.a());
    }

    private AnalyticsListener.EventTime i() {
        return a(this.d.b());
    }

    private AnalyticsListener.EventTime j() {
        return a(this.d.c());
    }

    private AnalyticsListener.EventTime k() {
        return a(this.d.d());
    }

    public void a(AnalyticsListener analyticsListener) {
        this.f3179a.add(analyticsListener);
    }

    public final void b(String str, long j, long j2) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, 1, str, j2);
        }
    }

    public final void c(DecoderCounters decoderCounters) {
        AnalyticsListener.EventTime g = g();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().b(g, 1, decoderCounters);
        }
    }

    public final void d(DecoderCounters decoderCounters) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, 2, decoderCounters);
        }
    }

    public final void e() {
        if (!this.d.e()) {
            AnalyticsListener.EventTime j = j();
            this.d.g();
            Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
            while (it2.hasNext()) {
                it2.next().d(j);
            }
        }
    }

    public final void f() {
        for (MediaPeriodInfo mediaPeriodInfo : new ArrayList(this.d.f3181a)) {
            c(mediaPeriodInfo.c, mediaPeriodInfo.f3180a);
        }
    }

    public final void h() {
        if (this.d.e()) {
            this.d.f();
            AnalyticsListener.EventTime j = j();
            Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
            while (it2.hasNext()) {
                it2.next().f(j);
            }
        }
    }

    public final void onLoadingChanged(boolean z) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, z);
        }
    }

    public final void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, playbackParameters);
        }
    }

    public final void onPlayerError(ExoPlaybackException exoPlaybackException) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, exoPlaybackException);
        }
    }

    public final void onPlayerStateChanged(boolean z, int i) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, z, i);
        }
    }

    public final void onRenderedFirstFrame() {
    }

    public final void onRepeatModeChanged(int i) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().d(j, i);
        }
    }

    public final void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, trackGroupArray, trackSelectionArray);
        }
    }

    public final void onVideoSizeChanged(int i, int i2, int i3, float f) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, i, i2, i3, f);
        }
    }

    public final void a(Metadata metadata) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, metadata);
        }
    }

    public final void b(Format format) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, 1, format);
        }
    }

    public final void c(int i, MediaSource.MediaPeriodId mediaPeriodId) {
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        if (this.d.b(mediaPeriodId)) {
            Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
            while (it2.hasNext()) {
                it2.next().b(d2);
            }
        }
    }

    public final void d() {
        AnalyticsListener.EventTime g = g();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().g(g);
        }
    }

    public final void a(DecoderCounters decoderCounters) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, 1, decoderCounters);
        }
    }

    private AnalyticsListener.EventTime d(int i, MediaSource.MediaPeriodId mediaPeriodId) {
        Assertions.a(this.e);
        if (mediaPeriodId != null) {
            MediaPeriodInfo a2 = this.d.a(mediaPeriodId);
            if (a2 != null) {
                return a(a2);
            }
            return a(Timeline.f3175a, i, mediaPeriodId);
        }
        Timeline i2 = this.e.i();
        if (!(i < i2.b())) {
            i2 = Timeline.f3175a;
        }
        return a(i2, i, (MediaSource.MediaPeriodId) null);
    }

    public final void b(DecoderCounters decoderCounters) {
        AnalyticsListener.EventTime g = g();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().b(g, 2, decoderCounters);
        }
    }

    public final void a(int i, long j, long j2) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().b(k, i, j, j2);
        }
    }

    public final void c(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().b(d2, loadEventInfo, mediaLoadData);
        }
    }

    public final void b(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(d2, loadEventInfo, mediaLoadData);
        }
    }

    public final void a(int i) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().b(k, i);
        }
    }

    public final void c() {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().h(k);
        }
    }

    public final void b(int i, MediaSource.MediaPeriodId mediaPeriodId) {
        this.d.c(mediaPeriodId);
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().i(d2);
        }
    }

    public void a(float f) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, f);
        }
    }

    public final void a(String str, long j, long j2) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, 2, str, j2);
        }
    }

    public final void b(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().b(d2, mediaLoadData);
        }
    }

    public final void a(Format format) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, 2, format);
        }
    }

    public final void b(boolean z) {
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().b(j, z);
        }
    }

    public final void a(int i, long j) {
        AnalyticsListener.EventTime g = g();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(g, i, j);
        }
    }

    public final void b(int i) {
        this.d.a(i);
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().c(j, i);
        }
    }

    public final void a(Surface surface) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, surface);
        }
    }

    public final void b(int i, long j, long j2) {
        AnalyticsListener.EventTime i2 = i();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(i2, i, j, j2);
        }
    }

    public void a(int i, int i2) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, i, i2);
        }
    }

    public final void b() {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().e(k);
        }
    }

    public final void a(int i, MediaSource.MediaPeriodId mediaPeriodId) {
        this.d.a(i, mediaPeriodId);
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().c(d2);
        }
    }

    public final void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().c(d2, loadEventInfo, mediaLoadData);
        }
    }

    public final void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException iOException, boolean z) {
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(d2, loadEventInfo, mediaLoadData, iOException, z);
        }
    }

    public final void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        AnalyticsListener.EventTime d2 = d(i, mediaPeriodId);
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(d2, mediaLoadData);
        }
    }

    public final void a(Timeline timeline, Object obj, int i) {
        this.d.a(timeline);
        AnalyticsListener.EventTime j = j();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(j, i);
        }
    }

    public final void a(Exception exc) {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k, exc);
        }
    }

    public final void a() {
        AnalyticsListener.EventTime k = k();
        Iterator<AnalyticsListener> it2 = this.f3179a.iterator();
        while (it2.hasNext()) {
            it2.next().a(k);
        }
    }

    /* access modifiers changed from: protected */
    public AnalyticsListener.EventTime a(Timeline timeline, int i, MediaSource.MediaPeriodId mediaPeriodId) {
        if (timeline.c()) {
            mediaPeriodId = null;
        }
        MediaSource.MediaPeriodId mediaPeriodId2 = mediaPeriodId;
        long a2 = this.b.a();
        boolean z = true;
        boolean z2 = timeline == this.e.i() && i == this.e.f();
        long j = 0;
        if (mediaPeriodId2 != null && mediaPeriodId2.a()) {
            if (!(z2 && this.e.h() == mediaPeriodId2.b && this.e.n() == mediaPeriodId2.c)) {
                z = false;
            }
            if (z) {
                j = this.e.getCurrentPosition();
            }
        } else if (z2) {
            j = this.e.o();
        } else if (!timeline.c()) {
            j = timeline.a(i, this.c).a();
        }
        return new AnalyticsListener.EventTime(a2, timeline, i, mediaPeriodId2, j, this.e.getCurrentPosition(), this.e.d());
    }

    private AnalyticsListener.EventTime a(MediaPeriodInfo mediaPeriodInfo) {
        Assertions.a(this.e);
        if (mediaPeriodInfo == null) {
            int f = this.e.f();
            MediaPeriodInfo b2 = this.d.b(f);
            if (b2 == null) {
                Timeline i = this.e.i();
                if (!(f < i.b())) {
                    i = Timeline.f3175a;
                }
                return a(i, f, (MediaSource.MediaPeriodId) null);
            }
            mediaPeriodInfo = b2;
        }
        return a(mediaPeriodInfo.b, mediaPeriodInfo.c, mediaPeriodInfo.f3180a);
    }
}
