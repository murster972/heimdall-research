package com.google.android.exoplayer2;

import com.google.android.exoplayer2.source.ClippingMediaPeriod;
import com.google.android.exoplayer2.source.EmptySampleStream;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectorResult;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;

final class MediaPeriodHolder {

    /* renamed from: a  reason: collision with root package name */
    public final MediaPeriod f3166a;
    public final Object b;
    public final SampleStream[] c;
    public final boolean[] d;
    public boolean e;
    public boolean f;
    public MediaPeriodInfo g;
    public MediaPeriodHolder h;
    public TrackGroupArray i;
    public TrackSelectorResult j;
    private final RendererCapabilities[] k;
    private final TrackSelector l;
    private final MediaSource m;
    private long n;
    private TrackSelectorResult o;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: com.google.android.exoplayer2.source.MediaPeriod} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: com.google.android.exoplayer2.source.ClippingMediaPeriod} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: com.google.android.exoplayer2.source.ClippingMediaPeriod} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.google.android.exoplayer2.source.ClippingMediaPeriod} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MediaPeriodHolder(com.google.android.exoplayer2.RendererCapabilities[] r8, long r9, com.google.android.exoplayer2.trackselection.TrackSelector r11, com.google.android.exoplayer2.upstream.Allocator r12, com.google.android.exoplayer2.source.MediaSource r13, com.google.android.exoplayer2.MediaPeriodInfo r14) {
        /*
            r7 = this;
            r7.<init>()
            r7.k = r8
            long r0 = r14.b
            long r9 = r9 - r0
            r7.n = r9
            r7.l = r11
            r7.m = r13
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r9 = r14.f3167a
            java.lang.Object r9 = r9.f3414a
            com.google.android.exoplayer2.util.Assertions.a(r9)
            r7.b = r9
            r7.g = r14
            int r9 = r8.length
            com.google.android.exoplayer2.source.SampleStream[] r9 = new com.google.android.exoplayer2.source.SampleStream[r9]
            r7.c = r9
            int r8 = r8.length
            boolean[] r8 = new boolean[r8]
            r7.d = r8
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r8 = r14.f3167a
            com.google.android.exoplayer2.source.MediaPeriod r1 = r13.a((com.google.android.exoplayer2.source.MediaSource.MediaPeriodId) r8, (com.google.android.exoplayer2.upstream.Allocator) r12)
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r8 = r14.f3167a
            long r5 = r8.e
            r8 = -9223372036854775808
            int r10 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x003d
            com.google.android.exoplayer2.source.ClippingMediaPeriod r8 = new com.google.android.exoplayer2.source.ClippingMediaPeriod
            r2 = 1
            r3 = 0
            r0 = r8
            r0.<init>(r1, r2, r3, r5)
            goto L_0x003e
        L_0x003d:
            r8 = r1
        L_0x003e:
            r7.f3166a = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.MediaPeriodHolder.<init>(com.google.android.exoplayer2.RendererCapabilities[], long, com.google.android.exoplayer2.trackselection.TrackSelector, com.google.android.exoplayer2.upstream.Allocator, com.google.android.exoplayer2.source.MediaSource, com.google.android.exoplayer2.MediaPeriodInfo):void");
    }

    public long a() {
        if (!this.e) {
            return this.g.b;
        }
        long f2 = this.f ? this.f3166a.f() : Long.MIN_VALUE;
        return f2 == Long.MIN_VALUE ? this.g.d : f2;
    }

    public long b() {
        if (!this.e) {
            return 0;
        }
        return this.f3166a.b();
    }

    public long c(long j2) {
        return j2 - c();
    }

    public long d(long j2) {
        return j2 + c();
    }

    public boolean e() {
        return this.e && (!this.f || this.f3166a.f() == Long.MIN_VALUE);
    }

    public void f() {
        c((TrackSelectorResult) null);
        try {
            if (this.g.f3167a.e != Long.MIN_VALUE) {
                this.m.a(((ClippingMediaPeriod) this.f3166a).f3399a);
            } else {
                this.m.a(this.f3166a);
            }
        } catch (RuntimeException e2) {
            Log.a("MediaPeriodHolder", "Period release failed.", e2);
        }
    }

    public void b(long j2) {
        if (this.e) {
            this.f3166a.c(c(j2));
        }
    }

    public long c() {
        return this.n;
    }

    public long d() {
        return this.g.b + this.n;
    }

    private void c(TrackSelectorResult trackSelectorResult) {
        TrackSelectorResult trackSelectorResult2 = this.o;
        if (trackSelectorResult2 != null) {
            a(trackSelectorResult2);
        }
        this.o = trackSelectorResult;
        TrackSelectorResult trackSelectorResult3 = this.o;
        if (trackSelectorResult3 != null) {
            b(trackSelectorResult3);
        }
    }

    public boolean b(float f2) throws ExoPlaybackException {
        TrackSelectorResult a2 = this.l.a(this.k, this.i);
        if (a2.a(this.o)) {
            return false;
        }
        this.j = a2;
        for (TrackSelection trackSelection : this.j.c.a()) {
            if (trackSelection != null) {
                trackSelection.a(f2);
            }
        }
        return true;
    }

    public void a(float f2) throws ExoPlaybackException {
        this.e = true;
        this.i = this.f3166a.e();
        b(f2);
        long a2 = a(this.g.b, false);
        long j2 = this.n;
        MediaPeriodInfo mediaPeriodInfo = this.g;
        this.n = j2 + (mediaPeriodInfo.b - a2);
        this.g = mediaPeriodInfo.a(a2);
    }

    private void b(TrackSelectorResult trackSelectorResult) {
        for (int i2 = 0; i2 < trackSelectorResult.f3566a; i2++) {
            boolean a2 = trackSelectorResult.a(i2);
            TrackSelection a3 = trackSelectorResult.c.a(i2);
            if (a2 && a3 != null) {
                a3.enable();
            }
        }
    }

    public void a(long j2) {
        this.f3166a.b(c(j2));
    }

    private void b(SampleStream[] sampleStreamArr) {
        int i2 = 0;
        while (true) {
            RendererCapabilities[] rendererCapabilitiesArr = this.k;
            if (i2 < rendererCapabilitiesArr.length) {
                if (rendererCapabilitiesArr[i2].getTrackType() == 6) {
                    sampleStreamArr[i2] = null;
                }
                i2++;
            } else {
                return;
            }
        }
    }

    public long a(long j2, boolean z) {
        return a(j2, z, new boolean[this.k.length]);
    }

    public long a(long j2, boolean z, boolean[] zArr) {
        int i2 = 0;
        while (true) {
            TrackSelectorResult trackSelectorResult = this.j;
            boolean z2 = true;
            if (i2 >= trackSelectorResult.f3566a) {
                break;
            }
            boolean[] zArr2 = this.d;
            if (z || !trackSelectorResult.a(this.o, i2)) {
                z2 = false;
            }
            zArr2[i2] = z2;
            i2++;
        }
        b(this.c);
        c(this.j);
        TrackSelectionArray trackSelectionArray = this.j.c;
        long a2 = this.f3166a.a(trackSelectionArray.a(), this.d, this.c, zArr, j2);
        a(this.c);
        this.f = false;
        int i3 = 0;
        while (true) {
            SampleStream[] sampleStreamArr = this.c;
            if (i3 >= sampleStreamArr.length) {
                return a2;
            }
            if (sampleStreamArr[i3] != null) {
                Assertions.b(this.j.a(i3));
                if (this.k[i3].getTrackType() != 6) {
                    this.f = true;
                }
            } else {
                Assertions.b(trackSelectionArray.a(i3) == null);
            }
            i3++;
        }
    }

    private void a(TrackSelectorResult trackSelectorResult) {
        for (int i2 = 0; i2 < trackSelectorResult.f3566a; i2++) {
            boolean a2 = trackSelectorResult.a(i2);
            TrackSelection a3 = trackSelectorResult.c.a(i2);
            if (a2 && a3 != null) {
                a3.disable();
            }
        }
    }

    private void a(SampleStream[] sampleStreamArr) {
        int i2 = 0;
        while (true) {
            RendererCapabilities[] rendererCapabilitiesArr = this.k;
            if (i2 < rendererCapabilitiesArr.length) {
                if (rendererCapabilitiesArr[i2].getTrackType() == 6 && this.j.a(i2)) {
                    sampleStreamArr[i2] = new EmptySampleStream();
                }
                i2++;
            } else {
                return;
            }
        }
    }
}
