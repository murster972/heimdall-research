package com.google.android.exoplayer2.audio;

public final class AuxEffectInfo {

    /* renamed from: a  reason: collision with root package name */
    public final int f3195a;
    public final float b;

    public AuxEffectInfo(int i, float f) {
        this.f3195a = i;
        this.b = f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AuxEffectInfo.class != obj.getClass()) {
            return false;
        }
        AuxEffectInfo auxEffectInfo = (AuxEffectInfo) obj;
        if (this.f3195a == auxEffectInfo.f3195a && Float.compare(auxEffectInfo.b, this.b) == 0) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((527 + this.f3195a) * 31) + Float.floatToIntBits(this.b);
    }
}
