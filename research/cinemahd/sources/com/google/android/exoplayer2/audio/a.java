package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f3207a;
    private final /* synthetic */ Format b;

    public /* synthetic */ a(AudioRendererEventListener.EventDispatcher eventDispatcher, Format format) {
        this.f3207a = eventDispatcher;
        this.b = format;
    }

    public final void run() {
        this.f3207a.b(this.b);
    }
}
