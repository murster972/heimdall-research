package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f3210a;
    private final /* synthetic */ int b;

    public /* synthetic */ d(AudioRendererEventListener.EventDispatcher eventDispatcher, int i) {
        this.f3210a = eventDispatcher;
        this.b = i;
    }

    public final void run() {
        this.f3210a.b(this.b);
    }
}
