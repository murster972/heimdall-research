package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class SilenceSkippingAudioProcessor implements AudioProcessor {
    private int b = -1;
    private int c = -1;
    private int d;
    private boolean e;
    private ByteBuffer f;
    private ByteBuffer g;
    private boolean h;
    private byte[] i;
    private byte[] j;
    private int k;
    private int l;
    private int m;
    private boolean n;
    private long o;

    public SilenceSkippingAudioProcessor() {
        ByteBuffer byteBuffer = AudioProcessor.f3190a;
        this.f = byteBuffer;
        this.g = byteBuffer;
        byte[] bArr = Util.f;
        this.i = bArr;
        this.j = bArr;
    }

    private void e(ByteBuffer byteBuffer) {
        int limit = byteBuffer.limit();
        int c2 = c(byteBuffer);
        int position = c2 - byteBuffer.position();
        byte[] bArr = this.i;
        int length = bArr.length;
        int i2 = this.l;
        int i3 = length - i2;
        if (c2 >= limit || position >= i3) {
            int min = Math.min(position, i3);
            byteBuffer.limit(byteBuffer.position() + min);
            byteBuffer.get(this.i, this.l, min);
            this.l += min;
            int i4 = this.l;
            byte[] bArr2 = this.i;
            if (i4 == bArr2.length) {
                if (this.n) {
                    a(bArr2, this.m);
                    this.o += (long) ((this.l - (this.m * 2)) / this.d);
                } else {
                    this.o += (long) ((i4 - this.m) / this.d);
                }
                a(byteBuffer, this.i, this.l);
                this.l = 0;
                this.k = 2;
            }
            byteBuffer.limit(limit);
            return;
        }
        a(bArr, i2);
        this.l = 0;
        this.k = 0;
    }

    public void a(boolean z) {
        this.e = z;
        flush();
    }

    public ByteBuffer b() {
        ByteBuffer byteBuffer = this.g;
        this.g = AudioProcessor.f3190a;
        return byteBuffer;
    }

    public int c() {
        return this.b;
    }

    public int d() {
        return this.c;
    }

    public int e() {
        return 2;
    }

    public void f() {
        this.h = true;
        int i2 = this.l;
        if (i2 > 0) {
            a(this.i, i2);
        }
        if (!this.n) {
            this.o += (long) (this.m / this.d);
        }
    }

    public void flush() {
        if (isActive()) {
            int a2 = a(150000) * this.d;
            if (this.i.length != a2) {
                this.i = new byte[a2];
            }
            this.m = a(20000) * this.d;
            int length = this.j.length;
            int i2 = this.m;
            if (length != i2) {
                this.j = new byte[i2];
            }
        }
        this.k = 0;
        this.g = AudioProcessor.f3190a;
        this.h = false;
        this.o = 0;
        this.l = 0;
        this.n = false;
    }

    public long g() {
        return this.o;
    }

    public boolean isActive() {
        return this.c != -1 && this.e;
    }

    public void reset() {
        this.e = false;
        flush();
        this.f = AudioProcessor.f3190a;
        this.b = -1;
        this.c = -1;
        this.m = 0;
        byte[] bArr = Util.f;
        this.i = bArr;
        this.j = bArr;
    }

    private int c(ByteBuffer byteBuffer) {
        for (int position = byteBuffer.position() + 1; position < byteBuffer.limit(); position += 2) {
            if (Math.abs(byteBuffer.get(position)) > 4) {
                int i2 = this.d;
                return i2 * (position / i2);
            }
        }
        return byteBuffer.limit();
    }

    private void d(ByteBuffer byteBuffer) {
        a(byteBuffer.remaining());
        this.f.put(byteBuffer);
        this.f.flip();
        this.g = this.f;
    }

    private void g(ByteBuffer byteBuffer) {
        int limit = byteBuffer.limit();
        int c2 = c(byteBuffer);
        byteBuffer.limit(c2);
        this.o += (long) (byteBuffer.remaining() / this.d);
        a(byteBuffer, this.j, this.m);
        if (c2 < limit) {
            a(this.j, this.m);
            this.k = 0;
            byteBuffer.limit(limit);
        }
    }

    private int b(ByteBuffer byteBuffer) {
        for (int limit = byteBuffer.limit() - 1; limit >= byteBuffer.position(); limit -= 2) {
            if (Math.abs(byteBuffer.get(limit)) > 4) {
                int i2 = this.d;
                return ((limit / i2) * i2) + i2;
            }
        }
        return byteBuffer.position();
    }

    public boolean a(int i2, int i3, int i4) throws AudioProcessor.UnhandledFormatException {
        if (i4 != 2) {
            throw new AudioProcessor.UnhandledFormatException(i2, i3, i4);
        } else if (this.c == i2 && this.b == i3) {
            return false;
        } else {
            this.c = i2;
            this.b = i3;
            this.d = i3 * 2;
            return true;
        }
    }

    private void f(ByteBuffer byteBuffer) {
        int limit = byteBuffer.limit();
        byteBuffer.limit(Math.min(limit, byteBuffer.position() + this.i.length));
        int b2 = b(byteBuffer);
        if (b2 == byteBuffer.position()) {
            this.k = 1;
        } else {
            byteBuffer.limit(b2);
            d(byteBuffer);
        }
        byteBuffer.limit(limit);
    }

    public void a(ByteBuffer byteBuffer) {
        while (byteBuffer.hasRemaining() && !this.g.hasRemaining()) {
            int i2 = this.k;
            if (i2 == 0) {
                f(byteBuffer);
            } else if (i2 == 1) {
                e(byteBuffer);
            } else if (i2 == 2) {
                g(byteBuffer);
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public boolean a() {
        return this.h && this.g == AudioProcessor.f3190a;
    }

    private void a(byte[] bArr, int i2) {
        a(i2);
        this.f.put(bArr, 0, i2);
        this.f.flip();
        this.g = this.f;
    }

    private void a(int i2) {
        if (this.f.capacity() < i2) {
            this.f = ByteBuffer.allocateDirect(i2).order(ByteOrder.nativeOrder());
        } else {
            this.f.clear();
        }
        if (i2 > 0) {
            this.n = true;
        }
    }

    private void a(ByteBuffer byteBuffer, byte[] bArr, int i2) {
        int min = Math.min(byteBuffer.remaining(), this.m);
        int i3 = this.m - min;
        System.arraycopy(bArr, i2 - i3, this.j, 0, i3);
        byteBuffer.position(byteBuffer.limit() - min);
        byteBuffer.get(this.j, i3, min);
    }

    private int a(long j2) {
        return (int) ((j2 * ((long) this.c)) / 1000000);
    }
}
