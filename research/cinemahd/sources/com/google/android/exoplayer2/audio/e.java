package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;

/* compiled from: lambda */
public final /* synthetic */ class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f3211a;
    private final /* synthetic */ DecoderCounters b;

    public /* synthetic */ e(AudioRendererEventListener.EventDispatcher eventDispatcher, DecoderCounters decoderCounters) {
        this.f3211a = eventDispatcher;
        this.b = decoderCounters;
    }

    public final void run() {
        this.f3211a.c(this.b);
    }
}
