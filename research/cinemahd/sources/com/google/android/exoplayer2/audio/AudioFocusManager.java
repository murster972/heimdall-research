package com.google.android.exoplayer2.audio;

import android.content.Context;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;

public final class AudioFocusManager {

    /* renamed from: a  reason: collision with root package name */
    private final AudioManager f3188a;
    private final AudioFocusListener b;
    /* access modifiers changed from: private */
    public final PlayerControl c;
    private AudioAttributes d;
    /* access modifiers changed from: private */
    public int e;
    private int f;
    /* access modifiers changed from: private */
    public float g = 1.0f;
    private AudioFocusRequest h;
    private boolean i;

    private class AudioFocusListener implements AudioManager.OnAudioFocusChangeListener {
        private AudioFocusListener() {
        }

        public void onAudioFocusChange(int i) {
            if (i != -3) {
                if (i == -2) {
                    int unused = AudioFocusManager.this.e = 2;
                } else if (i == -1) {
                    int unused2 = AudioFocusManager.this.e = -1;
                } else if (i != 1) {
                    Log.d("AudioFocusManager", "Unknown focus change type: " + i);
                    return;
                } else {
                    int unused3 = AudioFocusManager.this.e = 1;
                }
            } else if (AudioFocusManager.this.i()) {
                int unused4 = AudioFocusManager.this.e = 2;
            } else {
                int unused5 = AudioFocusManager.this.e = 3;
            }
            int a2 = AudioFocusManager.this.e;
            if (a2 == -1) {
                AudioFocusManager.this.c.b(-1);
                AudioFocusManager.this.b(true);
            } else if (a2 != 0) {
                if (a2 == 1) {
                    AudioFocusManager.this.c.b(1);
                } else if (a2 == 2) {
                    AudioFocusManager.this.c.b(0);
                } else if (a2 != 3) {
                    throw new IllegalStateException("Unknown audio focus state: " + AudioFocusManager.this.e);
                }
            }
            float f = AudioFocusManager.this.e == 3 ? 0.2f : 1.0f;
            if (AudioFocusManager.this.g != f) {
                float unused6 = AudioFocusManager.this.g = f;
                AudioFocusManager.this.c.a(f);
            }
        }
    }

    public interface PlayerControl {
        void a(float f);

        void b(int i);
    }

    public AudioFocusManager(Context context, PlayerControl playerControl) {
        AudioManager audioManager;
        if (context == null) {
            audioManager = null;
        } else {
            audioManager = (AudioManager) context.getApplicationContext().getSystemService("audio");
        }
        this.f3188a = audioManager;
        this.c = playerControl;
        this.b = new AudioFocusListener();
        this.e = 0;
    }

    private int c(boolean z) {
        return z ? 1 : -1;
    }

    private void e() {
        if (this.h != null) {
            AudioManager audioManager = this.f3188a;
            Assertions.a(audioManager);
            audioManager.abandonAudioFocusRequest(this.h);
        }
    }

    private int f() {
        int i2;
        if (this.f == 0) {
            if (this.e != 0) {
                b(true);
            }
            return 1;
        }
        if (this.e == 0) {
            if (Util.f3651a >= 26) {
                i2 = h();
            } else {
                i2 = g();
            }
            this.e = i2 == 1 ? 1 : 0;
        }
        int i3 = this.e;
        if (i3 == 0) {
            return -1;
        }
        if (i3 == 2) {
            return 0;
        }
        return 1;
    }

    private int g() {
        AudioManager audioManager = this.f3188a;
        Assertions.a(audioManager);
        AudioFocusListener audioFocusListener = this.b;
        AudioAttributes audioAttributes = this.d;
        Assertions.a(audioAttributes);
        return audioManager.requestAudioFocus(audioFocusListener, Util.d(audioAttributes.c), this.f);
    }

    private int h() {
        if (this.h == null || this.i) {
            AudioFocusRequest audioFocusRequest = this.h;
            AudioFocusRequest.Builder builder = audioFocusRequest == null ? new AudioFocusRequest.Builder(this.f) : new AudioFocusRequest.Builder(audioFocusRequest);
            boolean i2 = i();
            AudioAttributes audioAttributes = this.d;
            Assertions.a(audioAttributes);
            this.h = builder.setAudioAttributes(audioAttributes.a()).setWillPauseWhenDucked(i2).setOnAudioFocusChangeListener(this.b).build();
            this.i = false;
        }
        AudioManager audioManager = this.f3188a;
        Assertions.a(audioManager);
        return audioManager.requestAudioFocus(this.h);
    }

    /* access modifiers changed from: private */
    public boolean i() {
        AudioAttributes audioAttributes = this.d;
        return audioAttributes != null && audioAttributes.f3185a == 1;
    }

    private void c() {
        b(false);
    }

    private void d() {
        AudioManager audioManager = this.f3188a;
        Assertions.a(audioManager);
        audioManager.abandonAudioFocus(this.b);
    }

    public void b() {
        if (this.f3188a != null) {
            b(true);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (this.f != 0 || this.e != 0) {
            if (this.f != 1 || this.e == -1 || z) {
                if (Util.f3651a >= 26) {
                    e();
                } else {
                    d();
                }
                this.e = 0;
            }
        }
    }

    public float a() {
        return this.g;
    }

    public int a(boolean z) {
        if (this.f3188a == null) {
            return 1;
        }
        if (z) {
            return f();
        }
        return -1;
    }

    public int a(boolean z, int i2) {
        if (this.f3188a == null) {
            return 1;
        }
        if (z) {
            return i2 == 1 ? c(z) : f();
        }
        c();
        return -1;
    }
}
