package com.google.android.exoplayer2.audio;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Handler;
import android.view.Surface;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.audio.AudioSink;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.mediacodec.MediaCodecInfo;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.mediacodec.MediaFormatUtil;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.MediaClock;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.util.Collections;
import java.util.List;

@TargetApi(16)
public class MediaCodecAudioRenderer extends MediaCodecRenderer implements MediaClock {
    private long A0;
    private int B0;
    private final Context k0;
    /* access modifiers changed from: private */
    public final AudioRendererEventListener.EventDispatcher l0;
    private final AudioSink m0;
    private final long[] n0;
    private int o0;
    private boolean p0;
    private boolean q0;
    private boolean r0;
    private MediaFormat s0;
    private int t0;
    private int u0;
    private int v0;
    private int w0;
    private long x0;
    private boolean y0;
    /* access modifiers changed from: private */
    public boolean z0;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public MediaCodecAudioRenderer(Context context, MediaCodecSelector mediaCodecSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, boolean z, Handler handler, AudioRendererEventListener audioRendererEventListener, AudioCapabilities audioCapabilities, AudioProcessor... audioProcessorArr) {
        this(context, mediaCodecSelector, drmSessionManager, z, handler, audioRendererEventListener, new DefaultAudioSink(audioCapabilities, audioProcessorArr));
        AudioCapabilities audioCapabilities2 = audioCapabilities;
    }

    private void B() {
        long a2 = this.m0.a(a());
        if (a2 != Long.MIN_VALUE) {
            if (!this.z0) {
                a2 = Math.max(this.x0, a2);
            }
            this.x0 = a2;
            this.z0 = false;
        }
    }

    /* access modifiers changed from: protected */
    public void A() {
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
    }

    /* access modifiers changed from: protected */
    public void a(int i, long j, long j2) {
    }

    /* access modifiers changed from: protected */
    public void b(Format format) throws ExoPlaybackException {
        super.b(format);
        this.l0.a(format);
        this.t0 = "audio/raw".equals(format.g) ? format.v : 2;
        this.u0 = format.t;
        this.v0 = format.w;
        this.w0 = format.x;
    }

    /* access modifiers changed from: protected */
    public void c(long j) {
        while (this.B0 != 0 && j >= this.n0[0]) {
            this.m0.f();
            this.B0--;
            long[] jArr = this.n0;
            System.arraycopy(jArr, 1, jArr, 0, this.B0);
        }
    }

    public long h() {
        if (getState() == 2) {
            B();
        }
        return this.x0;
    }

    public boolean isReady() {
        return this.m0.c() || super.isReady();
    }

    public MediaClock j() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void p() {
        try {
            this.A0 = -9223372036854775807L;
            this.B0 = 0;
            this.m0.release();
            try {
                super.p();
            } finally {
                this.i0.a();
                this.l0.a(this.i0);
            }
        } catch (Throwable th) {
            super.p();
            throw th;
        } finally {
            this.i0.a();
            this.l0.a(this.i0);
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        super.q();
        this.m0.play();
    }

    /* access modifiers changed from: protected */
    public void r() {
        B();
        this.m0.pause();
        super.r();
    }

    /* access modifiers changed from: protected */
    public void z() throws ExoPlaybackException {
        try {
            this.m0.e();
        } catch (AudioSink.WriteException e) {
            throw ExoPlaybackException.a(e, m());
        }
    }

    private final class AudioSinkListener implements AudioSink.Listener {
        private AudioSinkListener() {
        }

        public void a(int i) {
            MediaCodecAudioRenderer.this.l0.a(i);
            MediaCodecAudioRenderer.this.a(i);
        }

        public void a() {
            MediaCodecAudioRenderer.this.A();
            boolean unused = MediaCodecAudioRenderer.this.z0 = true;
        }

        public void a(int i, long j, long j2) {
            MediaCodecAudioRenderer.this.l0.a(i, j, j2);
            MediaCodecAudioRenderer.this.a(i, j, j2);
        }
    }

    public MediaCodecAudioRenderer(Context context, MediaCodecSelector mediaCodecSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, boolean z, Handler handler, AudioRendererEventListener audioRendererEventListener, AudioSink audioSink) {
        super(1, mediaCodecSelector, drmSessionManager, z, 44100.0f);
        this.k0 = context.getApplicationContext();
        this.m0 = audioSink;
        this.A0 = -9223372036854775807L;
        this.n0 = new long[10];
        this.l0 = new AudioRendererEventListener.EventDispatcher(handler, audioRendererEventListener);
        audioSink.a((AudioSink.Listener) new AudioSinkListener());
    }

    /* access modifiers changed from: protected */
    public int a(MediaCodecSelector mediaCodecSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, Format format) throws MediaCodecUtil.DecoderQueryException {
        boolean z;
        String str = format.g;
        if (!MimeTypes.j(str)) {
            return 0;
        }
        int i = Util.f3651a >= 21 ? 32 : 0;
        boolean a2 = BaseRenderer.a((DrmSessionManager<?>) drmSessionManager, format.j);
        int i2 = 4;
        int i3 = 8;
        if (a2 && a(format.t, str) && mediaCodecSelector.a() != null) {
            return i | 8 | 4;
        }
        if (("audio/raw".equals(str) && !this.m0.a(format.t, format.v)) || !this.m0.a(format.t, 2)) {
            return 1;
        }
        DrmInitData drmInitData = format.j;
        if (drmInitData != null) {
            z = false;
            for (int i4 = 0; i4 < drmInitData.d; i4++) {
                z |= drmInitData.a(i4).f;
            }
        } else {
            z = false;
        }
        List<MediaCodecInfo> a3 = mediaCodecSelector.a(format.g, z);
        if (a3.isEmpty()) {
            if (!z || mediaCodecSelector.a(format.g, false).isEmpty()) {
                return 1;
            }
            return 2;
        } else if (!a2) {
            return 2;
        } else {
            MediaCodecInfo mediaCodecInfo = a3.get(0);
            boolean a4 = mediaCodecInfo.a(format);
            if (a4 && mediaCodecInfo.b(format)) {
                i3 = 16;
            }
            if (!a4) {
                i2 = 3;
            }
            return i3 | i | i2;
        }
    }

    public PlaybackParameters b() {
        return this.m0.b();
    }

    private static boolean b(String str) {
        return Util.f3651a < 21 && "OMX.SEC.mp3.dec".equals(str) && "samsung".equals(Util.c) && (Util.b.startsWith("baffin") || Util.b.startsWith("grand") || Util.b.startsWith("fortuna") || Util.b.startsWith("gprimelte") || Util.b.startsWith("j2y18lte") || Util.b.startsWith("ms01"));
    }

    /* access modifiers changed from: protected */
    public List<MediaCodecInfo> a(MediaCodecSelector mediaCodecSelector, Format format, boolean z) throws MediaCodecUtil.DecoderQueryException {
        MediaCodecInfo a2;
        if (!a(format.t, format.g) || (a2 = mediaCodecSelector.a()) == null) {
            return super.a(mediaCodecSelector, format, z);
        }
        return Collections.singletonList(a2);
    }

    /* access modifiers changed from: protected */
    public boolean a(int i, String str) {
        return this.m0.a(i, MimeTypes.c(str));
    }

    /* access modifiers changed from: protected */
    public void a(MediaCodecInfo mediaCodecInfo, MediaCodec mediaCodec, Format format, MediaCrypto mediaCrypto, float f) {
        this.o0 = a(mediaCodecInfo, format, n());
        this.q0 = a(mediaCodecInfo.f3359a);
        this.r0 = b(mediaCodecInfo.f3359a);
        this.p0 = mediaCodecInfo.g;
        String str = mediaCodecInfo.b;
        if (str == null) {
            str = "audio/raw";
        }
        MediaFormat a2 = a(format, str, this.o0, f);
        mediaCodec.configure(a2, (Surface) null, mediaCrypto, 0);
        if (this.p0) {
            this.s0 = a2;
            this.s0.setString("mime", format.g);
            return;
        }
        this.s0 = null;
    }

    /* access modifiers changed from: protected */
    public int a(MediaCodec mediaCodec, MediaCodecInfo mediaCodecInfo, Format format, Format format2) {
        if (a(mediaCodecInfo, format2) <= this.o0 && mediaCodecInfo.a(format, format2, true) && format.w == 0 && format.x == 0 && format2.w == 0 && format2.x == 0) {
            return 1;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public float a(float f, Format format, Format[] formatArr) {
        int i = -1;
        for (Format format2 : formatArr) {
            int i2 = format2.u;
            if (i2 != -1) {
                i = Math.max(i, i2);
            }
        }
        if (i == -1) {
            return -1.0f;
        }
        return f * ((float) i);
    }

    /* access modifiers changed from: protected */
    public void a(String str, long j, long j2) {
        this.l0.a(str, j, j2);
    }

    /* access modifiers changed from: protected */
    public void a(MediaCodec mediaCodec, MediaFormat mediaFormat) throws ExoPlaybackException {
        int i;
        int[] iArr;
        int i2;
        MediaFormat mediaFormat2 = this.s0;
        if (mediaFormat2 != null) {
            i = MimeTypes.c(mediaFormat2.getString("mime"));
            mediaFormat = this.s0;
        } else {
            i = this.t0;
        }
        int i3 = i;
        int integer = mediaFormat.getInteger("channel-count");
        int integer2 = mediaFormat.getInteger("sample-rate");
        if (!this.q0 || integer != 6 || (i2 = this.u0) >= 6) {
            iArr = null;
        } else {
            iArr = new int[i2];
            for (int i4 = 0; i4 < this.u0; i4++) {
                iArr[i4] = i4;
            }
        }
        try {
            this.m0.a(i3, integer, integer2, 0, iArr, this.v0, this.w0);
        } catch (AudioSink.ConfigurationException e) {
            throw ExoPlaybackException.a(e, m());
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) throws ExoPlaybackException {
        super.a(z);
        this.l0.b(this.i0);
        int i = l().f3172a;
        if (i != 0) {
            this.m0.a(i);
        } else {
            this.m0.d();
        }
    }

    /* access modifiers changed from: protected */
    public void a(Format[] formatArr, long j) throws ExoPlaybackException {
        super.a(formatArr, j);
        if (this.A0 != -9223372036854775807L) {
            int i = this.B0;
            if (i == this.n0.length) {
                Log.d("MediaCodecAudioRenderer", "Too many stream changes, so dropping change at " + this.n0[this.B0 - 1]);
            } else {
                this.B0 = i + 1;
            }
            this.n0[this.B0 - 1] = this.A0;
        }
    }

    /* access modifiers changed from: protected */
    public void a(long j, boolean z) throws ExoPlaybackException {
        super.a(j, z);
        this.m0.reset();
        this.x0 = j;
        this.y0 = true;
        this.z0 = true;
        this.A0 = -9223372036854775807L;
        this.B0 = 0;
    }

    public boolean a() {
        return super.a() && this.m0.a();
    }

    public PlaybackParameters a(PlaybackParameters playbackParameters) {
        return this.m0.a(playbackParameters);
    }

    /* access modifiers changed from: protected */
    public void a(DecoderInputBuffer decoderInputBuffer) {
        if (this.y0 && !decoderInputBuffer.isDecodeOnly()) {
            if (Math.abs(decoderInputBuffer.c - this.x0) > 500000) {
                this.x0 = decoderInputBuffer.c;
            }
            this.y0 = false;
        }
        this.A0 = Math.max(decoderInputBuffer.c, this.A0);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r1 != -9223372036854775807L) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(long r1, long r3, android.media.MediaCodec r5, java.nio.ByteBuffer r6, int r7, int r8, long r9, boolean r11, com.google.android.exoplayer2.Format r12) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            r0 = this;
            boolean r1 = r0.r0
            if (r1 == 0) goto L_0x001a
            r1 = 0
            int r3 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r3 != 0) goto L_0x001a
            r1 = r8 & 4
            if (r1 == 0) goto L_0x001a
            long r1 = r0.A0
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r12 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r12 == 0) goto L_0x001a
            goto L_0x001b
        L_0x001a:
            r1 = r9
        L_0x001b:
            boolean r3 = r0.p0
            r4 = 0
            r9 = 1
            if (r3 == 0) goto L_0x0029
            r3 = r8 & 2
            if (r3 == 0) goto L_0x0029
            r5.releaseOutputBuffer(r7, r4)
            return r9
        L_0x0029:
            if (r11 == 0) goto L_0x003b
            r5.releaseOutputBuffer(r7, r4)
            com.google.android.exoplayer2.decoder.DecoderCounters r1 = r0.i0
            int r2 = r1.f
            int r2 = r2 + r9
            r1.f = r2
            com.google.android.exoplayer2.audio.AudioSink r1 = r0.m0
            r1.f()
            return r9
        L_0x003b:
            com.google.android.exoplayer2.audio.AudioSink r3 = r0.m0     // Catch:{ InitializationException -> 0x0051, WriteException -> 0x004f }
            boolean r1 = r3.a((java.nio.ByteBuffer) r6, (long) r1)     // Catch:{ InitializationException -> 0x0051, WriteException -> 0x004f }
            if (r1 == 0) goto L_0x004e
            r5.releaseOutputBuffer(r7, r4)     // Catch:{ InitializationException -> 0x0051, WriteException -> 0x004f }
            com.google.android.exoplayer2.decoder.DecoderCounters r1 = r0.i0     // Catch:{ InitializationException -> 0x0051, WriteException -> 0x004f }
            int r2 = r1.e     // Catch:{ InitializationException -> 0x0051, WriteException -> 0x004f }
            int r2 = r2 + r9
            r1.e = r2     // Catch:{ InitializationException -> 0x0051, WriteException -> 0x004f }
            return r9
        L_0x004e:
            return r4
        L_0x004f:
            r1 = move-exception
            goto L_0x0052
        L_0x0051:
            r1 = move-exception
        L_0x0052:
            int r2 = r0.m()
            com.google.android.exoplayer2.ExoPlaybackException r1 = com.google.android.exoplayer2.ExoPlaybackException.a(r1, r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.MediaCodecAudioRenderer.a(long, long, android.media.MediaCodec, java.nio.ByteBuffer, int, int, long, boolean, com.google.android.exoplayer2.Format):boolean");
    }

    public void a(int i, Object obj) throws ExoPlaybackException {
        if (i == 2) {
            this.m0.a(((Float) obj).floatValue());
        } else if (i == 3) {
            this.m0.a((AudioAttributes) obj);
        } else if (i != 5) {
            super.a(i, obj);
        } else {
            this.m0.a((AuxEffectInfo) obj);
        }
    }

    /* access modifiers changed from: protected */
    public int a(MediaCodecInfo mediaCodecInfo, Format format, Format[] formatArr) {
        int a2 = a(mediaCodecInfo, format);
        if (formatArr.length == 1) {
            return a2;
        }
        int i = a2;
        for (Format format2 : formatArr) {
            if (mediaCodecInfo.a(format, format2, false)) {
                i = Math.max(i, a(mediaCodecInfo, format2));
            }
        }
        return i;
    }

    private int a(MediaCodecInfo mediaCodecInfo, Format format) {
        PackageManager packageManager;
        if (Util.f3651a < 24 && "OMX.google.raw.decoder".equals(mediaCodecInfo.f3359a)) {
            boolean z = true;
            if (Util.f3651a == 23 && (packageManager = this.k0.getPackageManager()) != null && packageManager.hasSystemFeature("android.software.leanback")) {
                z = false;
            }
            if (z) {
                return -1;
            }
        }
        return format.h;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"InlinedApi"})
    public MediaFormat a(Format format, String str, int i, float f) {
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", str);
        mediaFormat.setInteger("channel-count", format.t);
        mediaFormat.setInteger("sample-rate", format.u);
        MediaFormatUtil.a(mediaFormat, format.i);
        MediaFormatUtil.a(mediaFormat, "max-input-size", i);
        if (Util.f3651a >= 23) {
            mediaFormat.setInteger("priority", 0);
            if (f != -1.0f) {
                mediaFormat.setFloat("operating-rate", f);
            }
        }
        return mediaFormat;
    }

    private static boolean a(String str) {
        return Util.f3651a < 24 && "OMX.SEC.aac.dec".equals(str) && "samsung".equals(Util.c) && (Util.b.startsWith("zeroflte") || Util.b.startsWith("herolte") || Util.b.startsWith("heroqlte"));
    }
}
