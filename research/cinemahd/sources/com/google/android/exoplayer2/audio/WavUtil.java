package com.google.android.exoplayer2.audio;

import com.facebook.common.util.UriUtil;
import com.google.android.exoplayer2.util.Util;

public final class WavUtil {

    /* renamed from: a  reason: collision with root package name */
    public static final int f3206a = Util.c("RIFF");
    public static final int b = Util.c("WAVE");
    public static final int c = Util.c("fmt ");

    static {
        Util.c(UriUtil.DATA_SCHEME);
    }

    private WavUtil() {
    }

    public static int a(int i, int i2) {
        if (i != 1) {
            if (i == 3) {
                return i2 == 32 ? 4 : 0;
            }
            if (i != 65534) {
                if (i != 6) {
                    return i != 7 ? 0 : 268435456;
                }
                return 536870912;
            }
        }
        return Util.c(i2);
    }
}
