package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f3212a;
    private final /* synthetic */ int b;
    private final /* synthetic */ long c;
    private final /* synthetic */ long d;

    public /* synthetic */ f(AudioRendererEventListener.EventDispatcher eventDispatcher, int i, long j, long j2) {
        this.f3212a = eventDispatcher;
        this.b = i;
        this.c = j;
        this.d = j2;
    }

    public final void run() {
        this.f3212a.b(this.b, this.c, this.d);
    }
}
