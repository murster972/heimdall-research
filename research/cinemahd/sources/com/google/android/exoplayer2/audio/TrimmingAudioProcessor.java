package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

final class TrimmingAudioProcessor implements AudioProcessor {
    private boolean b;
    private int c;
    private int d;
    private int e = -1;
    private int f = -1;
    private int g;
    private boolean h;
    private int i;
    private ByteBuffer j;
    private ByteBuffer k;
    private byte[] l = Util.f;
    private int m;
    private boolean n;
    private long o;

    public TrimmingAudioProcessor() {
        ByteBuffer byteBuffer = AudioProcessor.f3190a;
        this.j = byteBuffer;
        this.k = byteBuffer;
    }

    public void a(int i2, int i3) {
        this.c = i2;
        this.d = i3;
    }

    public ByteBuffer b() {
        ByteBuffer byteBuffer = this.k;
        if (this.n && this.m > 0 && byteBuffer == AudioProcessor.f3190a) {
            int capacity = this.j.capacity();
            int i2 = this.m;
            if (capacity < i2) {
                this.j = ByteBuffer.allocateDirect(i2).order(ByteOrder.nativeOrder());
            } else {
                this.j.clear();
            }
            this.j.put(this.l, 0, this.m);
            this.m = 0;
            this.j.flip();
            byteBuffer = this.j;
        }
        this.k = AudioProcessor.f3190a;
        return byteBuffer;
    }

    public int c() {
        return this.e;
    }

    public int d() {
        return this.f;
    }

    public int e() {
        return 2;
    }

    public void f() {
        this.n = true;
    }

    public void flush() {
        this.k = AudioProcessor.f3190a;
        this.n = false;
        if (this.h) {
            this.i = 0;
        }
        this.m = 0;
    }

    public long g() {
        return this.o;
    }

    public void h() {
        this.o = 0;
    }

    public boolean isActive() {
        return this.b;
    }

    public void reset() {
        flush();
        this.j = AudioProcessor.f3190a;
        this.e = -1;
        this.f = -1;
        this.l = Util.f;
    }

    public boolean a(int i2, int i3, int i4) throws AudioProcessor.UnhandledFormatException {
        if (i4 == 2) {
            int i5 = this.m;
            if (i5 > 0) {
                this.o += (long) (i5 / this.g);
            }
            this.e = i3;
            this.f = i2;
            this.g = Util.b(2, i3);
            int i6 = this.d;
            int i7 = this.g;
            this.l = new byte[(i6 * i7)];
            this.m = 0;
            int i8 = this.c;
            this.i = i7 * i8;
            boolean z = this.b;
            this.b = (i8 == 0 && i6 == 0) ? false : true;
            this.h = false;
            if (z != this.b) {
                return true;
            }
            return false;
        }
        throw new AudioProcessor.UnhandledFormatException(i2, i3, i4);
    }

    public void a(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int i2 = limit - position;
        if (i2 != 0) {
            this.h = true;
            int min = Math.min(i2, this.i);
            this.o += (long) (min / this.g);
            this.i -= min;
            byteBuffer.position(position + min);
            if (this.i <= 0) {
                int i3 = i2 - min;
                int length = (this.m + i3) - this.l.length;
                if (this.j.capacity() < length) {
                    this.j = ByteBuffer.allocateDirect(length).order(ByteOrder.nativeOrder());
                } else {
                    this.j.clear();
                }
                int a2 = Util.a(length, 0, this.m);
                this.j.put(this.l, 0, a2);
                int a3 = Util.a(length - a2, 0, i3);
                byteBuffer.limit(byteBuffer.position() + a3);
                this.j.put(byteBuffer);
                byteBuffer.limit(limit);
                int i4 = i3 - a3;
                this.m -= a2;
                byte[] bArr = this.l;
                System.arraycopy(bArr, a2, bArr, 0, this.m);
                byteBuffer.get(this.l, this.m, i4);
                this.m += i4;
                this.j.flip();
                this.k = this.j;
            }
        }
    }

    public boolean a() {
        return this.n && this.m == 0 && this.k == AudioProcessor.f3190a;
    }
}
