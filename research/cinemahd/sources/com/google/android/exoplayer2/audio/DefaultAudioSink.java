package com.google.android.exoplayer2.audio;

import android.annotation.TargetApi;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.os.ConditionVariable;
import android.os.SystemClock;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.audio.AudioSink;
import com.google.android.exoplayer2.audio.AudioTrackPositionTracker;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public final class DefaultAudioSink implements AudioSink {
    public static boolean a0 = false;
    public static boolean b0 = false;
    private long A;
    private ByteBuffer B;
    private int C;
    private int D;
    private long E;
    private long F;
    private int G;
    private long H;
    private long I;
    private int J;
    private int K;
    private long L;
    private float M;
    private AudioProcessor[] N;
    private ByteBuffer[] O;
    private ByteBuffer P;
    private ByteBuffer Q;
    private byte[] R;
    private int S;
    private int T;
    private boolean U;
    private boolean V;
    private int W;
    private AuxEffectInfo X;
    private boolean Y;
    /* access modifiers changed from: private */
    public long Z;

    /* renamed from: a  reason: collision with root package name */
    private final AudioCapabilities f3196a;
    private final AudioProcessorChain b;
    private final boolean c;
    private final ChannelMappingAudioProcessor d;
    private final TrimmingAudioProcessor e;
    private final AudioProcessor[] f;
    private final AudioProcessor[] g;
    /* access modifiers changed from: private */
    public final ConditionVariable h;
    private final AudioTrackPositionTracker i;
    private final ArrayDeque<PlaybackParametersCheckpoint> j;
    /* access modifiers changed from: private */
    public AudioSink.Listener k;
    private AudioTrack l;
    private AudioTrack m;
    private boolean n;
    private boolean o;
    private int p;
    private int q;
    private int r;
    private int s;
    private AudioAttributes t;
    private boolean u;
    private boolean v;
    private int w;
    private PlaybackParameters x;
    private PlaybackParameters y;
    private long z;

    public interface AudioProcessorChain {
        long a(long j);

        PlaybackParameters a(PlaybackParameters playbackParameters);

        AudioProcessor[] a();

        long b();
    }

    public static class DefaultAudioProcessorChain implements AudioProcessorChain {

        /* renamed from: a  reason: collision with root package name */
        private final AudioProcessor[] f3199a;
        private final SilenceSkippingAudioProcessor b = new SilenceSkippingAudioProcessor();
        private final SonicAudioProcessor c = new SonicAudioProcessor();

        public DefaultAudioProcessorChain(AudioProcessor... audioProcessorArr) {
            this.f3199a = (AudioProcessor[]) Arrays.copyOf(audioProcessorArr, audioProcessorArr.length + 2);
            AudioProcessor[] audioProcessorArr2 = this.f3199a;
            audioProcessorArr2[audioProcessorArr.length] = this.b;
            audioProcessorArr2[audioProcessorArr.length + 1] = this.c;
        }

        public AudioProcessor[] a() {
            return this.f3199a;
        }

        public long b() {
            return this.b.g();
        }

        public PlaybackParameters a(PlaybackParameters playbackParameters) {
            this.b.a(playbackParameters.c);
            return new PlaybackParameters(this.c.b(playbackParameters.f3170a), this.c.a(playbackParameters.b), playbackParameters.c);
        }

        public long a(long j) {
            return this.c.a(j);
        }
    }

    public static final class InvalidAudioTrackTimestampException extends RuntimeException {
        private InvalidAudioTrackTimestampException(String str) {
            super(str);
        }
    }

    private static final class PlaybackParametersCheckpoint {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final PlaybackParameters f3200a;
        /* access modifiers changed from: private */
        public final long b;
        /* access modifiers changed from: private */
        public final long c;

        private PlaybackParametersCheckpoint(PlaybackParameters playbackParameters, long j, long j2) {
            this.f3200a = playbackParameters;
            this.b = j;
            this.c = j2;
        }
    }

    public DefaultAudioSink(AudioCapabilities audioCapabilities, AudioProcessor[] audioProcessorArr) {
        this(audioCapabilities, audioProcessorArr, false);
    }

    @TargetApi(21)
    private AudioTrack g() {
        AudioAttributes audioAttributes;
        if (this.Y) {
            audioAttributes = new AudioAttributes.Builder().setContentType(3).setFlags(16).setUsage(1).build();
        } else {
            audioAttributes = this.t.a();
        }
        AudioAttributes audioAttributes2 = audioAttributes;
        AudioFormat build = new AudioFormat.Builder().setChannelMask(this.r).setEncoding(this.s).setSampleRate(this.q).build();
        int i2 = this.W;
        return new AudioTrack(audioAttributes2, build, this.w, 1, i2 != 0 ? i2 : 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean h() throws com.google.android.exoplayer2.audio.AudioSink.WriteException {
        /*
            r9 = this;
            int r0 = r9.T
            r1 = -1
            r2 = 1
            r3 = 0
            if (r0 != r1) goto L_0x0014
            boolean r0 = r9.u
            if (r0 == 0) goto L_0x000d
            r0 = 0
            goto L_0x0010
        L_0x000d:
            com.google.android.exoplayer2.audio.AudioProcessor[] r0 = r9.N
            int r0 = r0.length
        L_0x0010:
            r9.T = r0
        L_0x0012:
            r0 = 1
            goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            int r4 = r9.T
            com.google.android.exoplayer2.audio.AudioProcessor[] r5 = r9.N
            int r6 = r5.length
            r7 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r4 >= r6) goto L_0x0038
            r4 = r5[r4]
            if (r0 == 0) goto L_0x0028
            r4.f()
        L_0x0028:
            r9.f(r7)
            boolean r0 = r4.a()
            if (r0 != 0) goto L_0x0032
            return r3
        L_0x0032:
            int r0 = r9.T
            int r0 = r0 + r2
            r9.T = r0
            goto L_0x0012
        L_0x0038:
            java.nio.ByteBuffer r0 = r9.Q
            if (r0 == 0) goto L_0x0044
            r9.b((java.nio.ByteBuffer) r0, (long) r7)
            java.nio.ByteBuffer r0 = r9.Q
            if (r0 == 0) goto L_0x0044
            return r3
        L_0x0044:
            r9.T = r1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.DefaultAudioSink.h():boolean");
    }

    private void i() {
        int i2 = 0;
        while (true) {
            AudioProcessor[] audioProcessorArr = this.N;
            if (i2 < audioProcessorArr.length) {
                AudioProcessor audioProcessor = audioProcessorArr[i2];
                audioProcessor.flush();
                this.O[i2] = audioProcessor.b();
                i2++;
            } else {
                return;
            }
        }
    }

    private AudioProcessor[] j() {
        return this.o ? this.g : this.f;
    }

    private int k() {
        if (this.n) {
            int minBufferSize = AudioTrack.getMinBufferSize(this.q, this.r, this.s);
            Assertions.b(minBufferSize != -2);
            return Util.a(minBufferSize * 4, ((int) c(250000)) * this.G, (int) Math.max((long) minBufferSize, c(750000) * ((long) this.G)));
        }
        int b2 = b(this.s);
        if (this.s == 5) {
            b2 *= 2;
        }
        return (int) ((((long) b2) * 250000) / 1000000);
    }

    /* access modifiers changed from: private */
    public long l() {
        return this.n ? this.E / ((long) this.D) : this.F;
    }

    /* access modifiers changed from: private */
    public long m() {
        return this.n ? this.H / ((long) this.G) : this.I;
    }

    private void n() throws AudioSink.InitializationException {
        this.h.block();
        this.m = o();
        int audioSessionId = this.m.getAudioSessionId();
        if (a0 && Util.f3651a < 21) {
            AudioTrack audioTrack = this.l;
            if (!(audioTrack == null || audioSessionId == audioTrack.getAudioSessionId())) {
                q();
            }
            if (this.l == null) {
                this.l = c(audioSessionId);
            }
        }
        if (this.W != audioSessionId) {
            this.W = audioSessionId;
            AudioSink.Listener listener = this.k;
            if (listener != null) {
                listener.a(audioSessionId);
            }
        }
        this.y = this.v ? this.b.a(this.y) : PlaybackParameters.e;
        s();
        this.i.a(this.m, this.s, this.G, this.w);
        r();
        int i2 = this.X.f3195a;
        if (i2 != 0) {
            this.m.attachAuxEffect(i2);
            this.m.setAuxEffectSendLevel(this.X.b);
        }
    }

    private AudioTrack o() throws AudioSink.InitializationException {
        AudioTrack audioTrack;
        if (Util.f3651a >= 21) {
            audioTrack = g();
        } else {
            int d2 = Util.d(this.t.c);
            int i2 = this.W;
            if (i2 == 0) {
                audioTrack = new AudioTrack(d2, this.q, this.r, this.s, this.w, 1);
            } else {
                audioTrack = new AudioTrack(d2, this.q, this.r, this.s, this.w, 1, i2);
            }
        }
        int state = audioTrack.getState();
        if (state == 1) {
            return audioTrack;
        }
        try {
            audioTrack.release();
        } catch (Exception unused) {
        }
        throw new AudioSink.InitializationException(state, this.q, this.r, this.w);
    }

    private boolean p() {
        return this.m != null;
    }

    private void q() {
        final AudioTrack audioTrack = this.l;
        if (audioTrack != null) {
            this.l = null;
            new Thread(this) {
                public void run() {
                    audioTrack.release();
                }
            }.start();
        }
    }

    private void r() {
        if (p()) {
            if (Util.f3651a >= 21) {
                a(this.m, this.M);
            } else {
                b(this.m, this.M);
            }
        }
    }

    private void s() {
        ArrayList arrayList = new ArrayList();
        for (AudioProcessor audioProcessor : j()) {
            if (audioProcessor.isActive()) {
                arrayList.add(audioProcessor);
            } else {
                audioProcessor.flush();
            }
        }
        int size = arrayList.size();
        this.N = (AudioProcessor[]) arrayList.toArray(new AudioProcessor[size]);
        this.O = new ByteBuffer[size];
        i();
    }

    public void f() {
        if (this.K == 1) {
            this.K = 2;
        }
    }

    public void pause() {
        this.V = false;
        if (p() && this.i.b()) {
            this.m.pause();
        }
    }

    public void play() {
        this.V = true;
        if (p()) {
            this.i.d();
            this.m.play();
        }
    }

    public void release() {
        reset();
        q();
        for (AudioProcessor reset : this.f) {
            reset.reset();
        }
        for (AudioProcessor reset2 : this.g) {
            reset2.reset();
        }
        this.W = 0;
        this.V = false;
    }

    public void reset() {
        if (p()) {
            this.E = 0;
            this.F = 0;
            this.H = 0;
            this.I = 0;
            this.J = 0;
            PlaybackParameters playbackParameters = this.x;
            if (playbackParameters != null) {
                this.y = playbackParameters;
                this.x = null;
            } else if (!this.j.isEmpty()) {
                this.y = this.j.getLast().f3200a;
            }
            this.j.clear();
            this.z = 0;
            this.A = 0;
            this.e.h();
            this.P = null;
            this.Q = null;
            i();
            this.U = false;
            this.T = -1;
            this.B = null;
            this.C = 0;
            this.K = 0;
            if (this.i.a()) {
                this.m.pause();
            }
            final AudioTrack audioTrack = this.m;
            this.m = null;
            this.i.c();
            this.h.close();
            new Thread() {
                public void run() {
                    try {
                        audioTrack.flush();
                        audioTrack.release();
                    } finally {
                        DefaultAudioSink.this.h.open();
                    }
                }
            }.start();
        }
    }

    public DefaultAudioSink(AudioCapabilities audioCapabilities, AudioProcessor[] audioProcessorArr, boolean z2) {
        this(audioCapabilities, (AudioProcessorChain) new DefaultAudioProcessorChain(audioProcessorArr), z2);
    }

    private void b(ByteBuffer byteBuffer, long j2) throws AudioSink.WriteException {
        if (byteBuffer.hasRemaining()) {
            ByteBuffer byteBuffer2 = this.Q;
            boolean z2 = true;
            int i2 = 0;
            if (byteBuffer2 != null) {
                Assertions.a(byteBuffer2 == byteBuffer);
            } else {
                this.Q = byteBuffer;
                if (Util.f3651a < 21) {
                    int remaining = byteBuffer.remaining();
                    byte[] bArr = this.R;
                    if (bArr == null || bArr.length < remaining) {
                        this.R = new byte[remaining];
                    }
                    int position = byteBuffer.position();
                    byteBuffer.get(this.R, 0, remaining);
                    byteBuffer.position(position);
                    this.S = 0;
                }
            }
            int remaining2 = byteBuffer.remaining();
            if (Util.f3651a < 21) {
                int a2 = this.i.a(this.H);
                if (a2 > 0 && (i2 = this.m.write(this.R, this.S, Math.min(remaining2, a2))) > 0) {
                    this.S += i2;
                    byteBuffer.position(byteBuffer.position() + i2);
                }
            } else if (this.Y) {
                if (j2 == -9223372036854775807L) {
                    z2 = false;
                }
                Assertions.b(z2);
                i2 = a(this.m, byteBuffer, remaining2, j2);
            } else {
                i2 = a(this.m, byteBuffer, remaining2);
            }
            this.Z = SystemClock.elapsedRealtime();
            if (i2 >= 0) {
                if (this.n) {
                    this.H += (long) i2;
                }
                if (i2 == remaining2) {
                    if (!this.n) {
                        this.I += (long) this.J;
                    }
                    this.Q = null;
                    return;
                }
                return;
            }
            throw new AudioSink.WriteException(i2);
        }
    }

    public void a(AudioSink.Listener listener) {
        this.k = listener;
    }

    public boolean c() {
        return p() && this.i.c(m());
    }

    public void d() {
        if (this.Y) {
            this.Y = false;
            this.W = 0;
            reset();
        }
    }

    public void e() throws AudioSink.WriteException {
        if (!this.U && p() && h()) {
            this.i.b(m());
            this.m.stop();
            this.C = 0;
            this.U = true;
        }
    }

    public DefaultAudioSink(AudioCapabilities audioCapabilities, AudioProcessorChain audioProcessorChain, boolean z2) {
        this.f3196a = audioCapabilities;
        Assertions.a(audioProcessorChain);
        this.b = audioProcessorChain;
        this.c = z2;
        this.h = new ConditionVariable(true);
        this.i = new AudioTrackPositionTracker(new PositionTrackerListener());
        this.d = new ChannelMappingAudioProcessor();
        this.e = new TrimmingAudioProcessor();
        ArrayList arrayList = new ArrayList();
        Collections.addAll(arrayList, new AudioProcessor[]{new ResamplingAudioProcessor(), this.d, this.e});
        Collections.addAll(arrayList, audioProcessorChain.a());
        this.f = (AudioProcessor[]) arrayList.toArray(new AudioProcessor[arrayList.size()]);
        this.g = new AudioProcessor[]{new FloatResamplingAudioProcessor()};
        this.M = 1.0f;
        this.K = 0;
        this.t = AudioAttributes.e;
        this.W = 0;
        this.X = new AuxEffectInfo(0, 0.0f);
        this.y = PlaybackParameters.e;
        this.T = -1;
        this.N = new AudioProcessor[0];
        this.O = new ByteBuffer[0];
        this.j = new ArrayDeque<>();
    }

    private long c(long j2) {
        return (j2 * ((long) this.q)) / 1000000;
    }

    private void f(long j2) throws AudioSink.WriteException {
        ByteBuffer byteBuffer;
        int length = this.N.length;
        int i2 = length;
        while (i2 >= 0) {
            if (i2 > 0) {
                byteBuffer = this.O[i2 - 1];
            } else {
                byteBuffer = this.P;
                if (byteBuffer == null) {
                    byteBuffer = AudioProcessor.f3190a;
                }
            }
            if (i2 == length) {
                b(byteBuffer, j2);
            } else {
                AudioProcessor audioProcessor = this.N[i2];
                audioProcessor.a(byteBuffer);
                ByteBuffer b2 = audioProcessor.b();
                this.O[i2] = b2;
                if (b2.hasRemaining()) {
                    i2++;
                }
            }
            if (!byteBuffer.hasRemaining()) {
                i2--;
            } else {
                return;
            }
        }
    }

    public boolean a(int i2, int i3) {
        if (!Util.f(i3)) {
            AudioCapabilities audioCapabilities = this.f3196a;
            if (audioCapabilities == null || !audioCapabilities.a(i3) || (i2 != -1 && i2 > this.f3196a.a())) {
                return false;
            }
            return true;
        } else if (i3 != 4 || Util.f3651a >= 21) {
            return true;
        } else {
            return false;
        }
    }

    private AudioTrack c(int i2) {
        return new AudioTrack(3, 4000, 4, 2, 2, 0, i2);
    }

    private final class PositionTrackerListener implements AudioTrackPositionTracker.Listener {
        private PositionTrackerListener() {
        }

        public void a(long j, long j2, long j3, long j4) {
            String str = "Spurious audio timestamp (frame position mismatch): " + j + ", " + j2 + ", " + j3 + ", " + j4 + ", " + DefaultAudioSink.this.l() + ", " + DefaultAudioSink.this.m();
            if (!DefaultAudioSink.b0) {
                Log.d("AudioTrack", str);
                return;
            }
            throw new InvalidAudioTrackTimestampException(str);
        }

        public void b(long j, long j2, long j3, long j4) {
            String str = "Spurious audio timestamp (system clock mismatch): " + j + ", " + j2 + ", " + j3 + ", " + j4 + ", " + DefaultAudioSink.this.l() + ", " + DefaultAudioSink.this.m();
            if (!DefaultAudioSink.b0) {
                Log.d("AudioTrack", str);
                return;
            }
            throw new InvalidAudioTrackTimestampException(str);
        }

        public void a(long j) {
            Log.d("AudioTrack", "Ignoring impossibly large audio latency: " + j);
        }

        public void a(int i, long j) {
            if (DefaultAudioSink.this.k != null) {
                DefaultAudioSink.this.k.a(i, j, SystemClock.elapsedRealtime() - DefaultAudioSink.this.Z);
            }
        }
    }

    private long d(long j2) {
        return (j2 * 1000000) / ((long) this.q);
    }

    private long e(long j2) {
        return (j2 * 1000000) / ((long) this.p);
    }

    public long a(boolean z2) {
        if (!p() || this.K == 0) {
            return Long.MIN_VALUE;
        }
        return this.L + a(b(Math.min(this.i.a(z2), d(m()))));
    }

    public void a(int i2, int i3, int i4, int i5, int[] iArr, int i6, int i7) throws AudioSink.ConfigurationException {
        int i8;
        int i9;
        boolean z2;
        this.p = i4;
        this.n = Util.f(i2);
        boolean z3 = true;
        int i10 = 0;
        this.o = this.c && a(i3, 1073741824) && Util.e(i2);
        if (this.n) {
            this.D = Util.b(i2, i3);
        }
        boolean z4 = this.n && i2 != 4;
        if (!z4 || this.o) {
            z3 = false;
        }
        this.v = z3;
        if (Util.f3651a < 21 && i3 == 8 && iArr == null) {
            iArr = new int[6];
            for (int i11 = 0; i11 < iArr.length; i11++) {
                iArr[i11] = i11;
            }
        }
        if (z4) {
            this.e.a(i6, i7);
            this.d.a(iArr);
            AudioProcessor[] j2 = j();
            int length = j2.length;
            i8 = i4;
            i9 = i2;
            z2 = false;
            while (i10 < length) {
                AudioProcessor audioProcessor = j2[i10];
                try {
                    z2 |= audioProcessor.a(i8, i3, i9);
                    if (audioProcessor.isActive()) {
                        i3 = audioProcessor.c();
                        i8 = audioProcessor.d();
                        i9 = audioProcessor.e();
                    }
                    i10++;
                } catch (AudioProcessor.UnhandledFormatException e2) {
                    throw new AudioSink.ConfigurationException((Throwable) e2);
                }
            }
        } else {
            i8 = i4;
            i9 = i2;
            z2 = false;
        }
        int a2 = a(i3, this.n);
        if (a2 == 0) {
            throw new AudioSink.ConfigurationException("Unsupported channel count: " + i3);
        } else if (z2 || !p() || this.s != i9 || this.q != i8 || this.r != a2) {
            reset();
            this.u = z4;
            this.q = i8;
            this.r = a2;
            this.s = i9;
            this.G = this.n ? Util.b(this.s, i3) : -1;
            if (i5 == 0) {
                i5 = k();
            }
            this.w = i5;
        }
    }

    public PlaybackParameters b() {
        return this.y;
    }

    private long b(long j2) {
        long j3;
        long a2;
        PlaybackParametersCheckpoint playbackParametersCheckpoint = null;
        while (!this.j.isEmpty() && j2 >= this.j.getFirst().c) {
            playbackParametersCheckpoint = this.j.remove();
        }
        if (playbackParametersCheckpoint != null) {
            this.y = playbackParametersCheckpoint.f3200a;
            this.A = playbackParametersCheckpoint.c;
            this.z = playbackParametersCheckpoint.b - this.L;
        }
        if (this.y.f3170a == 1.0f) {
            return (j2 + this.z) - this.A;
        }
        if (this.j.isEmpty()) {
            j3 = this.z;
            a2 = this.b.a(j2 - this.A);
        } else {
            j3 = this.z;
            a2 = Util.a(j2 - this.A, this.y.f3170a);
        }
        return j3 + a2;
    }

    public boolean a(ByteBuffer byteBuffer, long j2) throws AudioSink.InitializationException, AudioSink.WriteException {
        ByteBuffer byteBuffer2 = byteBuffer;
        long j3 = j2;
        ByteBuffer byteBuffer3 = this.P;
        Assertions.a(byteBuffer3 == null || byteBuffer2 == byteBuffer3);
        if (!p()) {
            n();
            if (this.V) {
                play();
            }
        }
        if (!this.i.e(m())) {
            return false;
        }
        if (this.P == null) {
            if (!byteBuffer.hasRemaining()) {
                return true;
            }
            if (!this.n && this.J == 0) {
                this.J = a(this.s, byteBuffer2);
                if (this.J == 0) {
                    return true;
                }
            }
            if (this.x != null) {
                if (!h()) {
                    return false;
                }
                PlaybackParameters playbackParameters = this.x;
                this.x = null;
                this.j.add(new PlaybackParametersCheckpoint(this.b.a(playbackParameters), Math.max(0, j3), d(m())));
                s();
            }
            if (this.K == 0) {
                this.L = Math.max(0, j3);
                this.K = 1;
            } else {
                long e2 = this.L + e(l() - this.e.g());
                if (this.K == 1 && Math.abs(e2 - j3) > 200000) {
                    Log.b("AudioTrack", "Discontinuity detected [expected " + e2 + ", got " + j3 + "]");
                    this.K = 2;
                }
                if (this.K == 2) {
                    long j4 = j3 - e2;
                    this.L += j4;
                    this.K = 1;
                    AudioSink.Listener listener = this.k;
                    if (!(listener == null || j4 == 0)) {
                        listener.a();
                    }
                }
            }
            if (this.n) {
                this.E += (long) byteBuffer.remaining();
            } else {
                this.F += (long) this.J;
            }
            this.P = byteBuffer2;
        }
        if (this.u) {
            f(j3);
        } else {
            b(this.P, j3);
        }
        if (!this.P.hasRemaining()) {
            this.P = null;
            return true;
        } else if (!this.i.d(m())) {
            return false;
        } else {
            Log.d("AudioTrack", "Resetting stalled audio track");
            reset();
            return true;
        }
    }

    private static int b(int i2) {
        if (i2 == 5) {
            return 80000;
        }
        if (i2 == 6) {
            return 768000;
        }
        if (i2 == 7) {
            return 192000;
        }
        if (i2 == 8) {
            return 2250000;
        }
        if (i2 == 14) {
            return 3062500;
        }
        throw new IllegalArgumentException();
    }

    private static void b(AudioTrack audioTrack, float f2) {
        audioTrack.setStereoVolume(f2, f2);
    }

    public boolean a() {
        return !p() || (this.U && !c());
    }

    public PlaybackParameters a(PlaybackParameters playbackParameters) {
        if (!p() || this.v) {
            PlaybackParameters playbackParameters2 = this.x;
            if (playbackParameters2 == null) {
                playbackParameters2 = !this.j.isEmpty() ? this.j.getLast().f3200a : this.y;
            }
            if (!playbackParameters.equals(playbackParameters2)) {
                if (p()) {
                    this.x = playbackParameters;
                } else {
                    this.y = this.b.a(playbackParameters);
                }
            }
            return this.y;
        }
        this.y = PlaybackParameters.e;
        return this.y;
    }

    public void a(AudioAttributes audioAttributes) {
        if (!this.t.equals(audioAttributes)) {
            this.t = audioAttributes;
            if (!this.Y) {
                reset();
                this.W = 0;
            }
        }
    }

    public void a(AuxEffectInfo auxEffectInfo) {
        if (!this.X.equals(auxEffectInfo)) {
            int i2 = auxEffectInfo.f3195a;
            float f2 = auxEffectInfo.b;
            AudioTrack audioTrack = this.m;
            if (audioTrack != null) {
                if (this.X.f3195a != i2) {
                    audioTrack.attachAuxEffect(i2);
                }
                if (i2 != 0) {
                    this.m.setAuxEffectSendLevel(f2);
                }
            }
            this.X = auxEffectInfo;
        }
    }

    public void a(int i2) {
        Assertions.b(Util.f3651a >= 21);
        if (!this.Y || this.W != i2) {
            this.Y = true;
            this.W = i2;
            reset();
        }
    }

    public void a(float f2) {
        if (this.M != f2) {
            this.M = f2;
            r();
        }
    }

    private long a(long j2) {
        return j2 + d(this.b.b());
    }

    private static int a(int i2, boolean z2) {
        if (Util.f3651a <= 28 && !z2) {
            if (i2 == 7) {
                i2 = 8;
            } else if (i2 == 3 || i2 == 4 || i2 == 5) {
                i2 = 6;
            }
        }
        if (Util.f3651a <= 26 && "fugu".equals(Util.b) && !z2 && i2 == 1) {
            i2 = 2;
        }
        return Util.a(i2);
    }

    private static int a(int i2, ByteBuffer byteBuffer) {
        if (i2 == 7 || i2 == 8) {
            return DtsUtil.a(byteBuffer);
        }
        if (i2 == 5) {
            return Ac3Util.a();
        }
        if (i2 == 6) {
            return Ac3Util.b(byteBuffer);
        }
        if (i2 == 14) {
            int a2 = Ac3Util.a(byteBuffer);
            if (a2 == -1) {
                return 0;
            }
            return Ac3Util.a(byteBuffer, a2) * 16;
        }
        throw new IllegalStateException("Unexpected audio encoding: " + i2);
    }

    @TargetApi(21)
    private static int a(AudioTrack audioTrack, ByteBuffer byteBuffer, int i2) {
        return audioTrack.write(byteBuffer, i2, 1);
    }

    @TargetApi(21)
    private int a(AudioTrack audioTrack, ByteBuffer byteBuffer, int i2, long j2) {
        if (this.B == null) {
            this.B = ByteBuffer.allocate(16);
            this.B.order(ByteOrder.BIG_ENDIAN);
            this.B.putInt(1431633921);
        }
        if (this.C == 0) {
            this.B.putInt(4, i2);
            this.B.putLong(8, j2 * 1000);
            this.B.position(0);
            this.C = i2;
        }
        int remaining = this.B.remaining();
        if (remaining > 0) {
            int write = audioTrack.write(this.B, remaining, 1);
            if (write < 0) {
                this.C = 0;
                return write;
            } else if (write < remaining) {
                return 0;
            }
        }
        int a2 = a(audioTrack, byteBuffer, i2);
        if (a2 < 0) {
            this.C = 0;
            return a2;
        }
        this.C -= a2;
        return a2;
    }

    @TargetApi(21)
    private static void a(AudioTrack audioTrack, float f2) {
        audioTrack.setVolume(f2);
    }
}
