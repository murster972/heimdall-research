package com.google.android.exoplayer2.audio;

import android.os.Handler;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.util.Assertions;

public interface AudioRendererEventListener {
    void a(int i);

    void a(int i, long j, long j2);

    void a(DecoderCounters decoderCounters);

    void b(Format format);

    void b(String str, long j, long j2);

    void c(DecoderCounters decoderCounters);

    public static final class EventDispatcher {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f3191a;
        private final AudioRendererEventListener b;

        public EventDispatcher(Handler handler, AudioRendererEventListener audioRendererEventListener) {
            Handler handler2;
            if (audioRendererEventListener != null) {
                Assertions.a(handler);
                handler2 = handler;
            } else {
                handler2 = null;
            }
            this.f3191a = handler2;
            this.b = audioRendererEventListener;
        }

        public void a(String str, long j, long j2) {
            if (this.b != null) {
                this.f3191a.post(new b(this, str, j, j2));
            }
        }

        public void b(DecoderCounters decoderCounters) {
            if (this.b != null) {
                this.f3191a.post(new c(this, decoderCounters));
            }
        }

        public /* synthetic */ void c(DecoderCounters decoderCounters) {
            decoderCounters.a();
            this.b.c(decoderCounters);
        }

        public /* synthetic */ void d(DecoderCounters decoderCounters) {
            this.b.a(decoderCounters);
        }

        public void a(Format format) {
            if (this.b != null) {
                this.f3191a.post(new a(this, format));
            }
        }

        public /* synthetic */ void b(String str, long j, long j2) {
            this.b.b(str, j, j2);
        }

        public /* synthetic */ void b(Format format) {
            this.b.b(format);
        }

        public void a(int i, long j, long j2) {
            if (this.b != null) {
                this.f3191a.post(new f(this, i, j, j2));
            }
        }

        public /* synthetic */ void b(int i, long j, long j2) {
            this.b.a(i, j, j2);
        }

        public /* synthetic */ void b(int i) {
            this.b.a(i);
        }

        public void a(DecoderCounters decoderCounters) {
            if (this.b != null) {
                this.f3191a.post(new e(this, decoderCounters));
            }
        }

        public void a(int i) {
            if (this.b != null) {
                this.f3191a.post(new d(this, i));
            }
        }
    }
}
