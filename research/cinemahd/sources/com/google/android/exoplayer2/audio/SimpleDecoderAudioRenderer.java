package com.google.android.exoplayer2.audio;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.audio.AudioSink;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.decoder.SimpleDecoder;
import com.google.android.exoplayer2.decoder.SimpleOutputBuffer;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MediaClock;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import java.util.List;

public abstract class SimpleDecoderAudioRenderer extends BaseRenderer implements MediaClock {
    private boolean A;
    private long B;
    private boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    private final DrmSessionManager<ExoMediaCrypto> j;
    private final boolean k;
    /* access modifiers changed from: private */
    public final AudioRendererEventListener.EventDispatcher l;
    private final AudioSink m;
    private final FormatHolder n;
    private final DecoderInputBuffer o;
    private DecoderCounters p;
    private Format q;
    private int r;
    private int s;
    private SimpleDecoder<DecoderInputBuffer, ? extends SimpleOutputBuffer, ? extends AudioDecoderException> t;
    private DecoderInputBuffer u;
    private SimpleOutputBuffer v;
    private DrmSession<ExoMediaCrypto> w;
    private DrmSession<ExoMediaCrypto> x;
    private int y;
    private boolean z;

    public SimpleDecoderAudioRenderer() {
        this((Handler) null, (AudioRendererEventListener) null, new AudioProcessor[0]);
    }

    private void A() {
        long a2 = this.m.a(a());
        if (a2 != Long.MIN_VALUE) {
            if (!this.D) {
                a2 = Math.max(this.B, a2);
            }
            this.B = a2;
            this.D = false;
        }
    }

    private boolean b(boolean z2) throws ExoPlaybackException {
        if (this.w == null || (!z2 && this.k)) {
            return false;
        }
        int state = this.w.getState();
        if (state != 1) {
            return state != 4;
        }
        throw ExoPlaybackException.a(this.w.b(), m());
    }

    private boolean u() throws ExoPlaybackException, AudioDecoderException, AudioSink.ConfigurationException, AudioSink.InitializationException, AudioSink.WriteException {
        if (this.v == null) {
            this.v = (SimpleOutputBuffer) this.t.a();
            SimpleOutputBuffer simpleOutputBuffer = this.v;
            if (simpleOutputBuffer == null) {
                return false;
            }
            this.p.f += simpleOutputBuffer.skippedOutputBufferCount;
        }
        if (this.v.isEndOfStream()) {
            if (this.y == 2) {
                z();
                x();
                this.A = true;
            } else {
                this.v.release();
                this.v = null;
                y();
            }
            return false;
        }
        if (this.A) {
            Format s2 = s();
            this.m.a(s2.v, s2.t, s2.u, 0, (int[]) null, this.r, this.s);
            this.A = false;
        }
        AudioSink audioSink = this.m;
        SimpleOutputBuffer simpleOutputBuffer2 = this.v;
        if (!audioSink.a(simpleOutputBuffer2.b, simpleOutputBuffer2.timeUs)) {
            return false;
        }
        this.p.e++;
        this.v.release();
        this.v = null;
        return true;
    }

    private boolean v() throws AudioDecoderException, ExoPlaybackException {
        int i;
        SimpleDecoder<DecoderInputBuffer, ? extends SimpleOutputBuffer, ? extends AudioDecoderException> simpleDecoder = this.t;
        if (simpleDecoder == null || this.y == 2 || this.E) {
            return false;
        }
        if (this.u == null) {
            this.u = simpleDecoder.b();
            if (this.u == null) {
                return false;
            }
        }
        if (this.y == 1) {
            this.u.setFlags(4);
            this.t.a(this.u);
            this.u = null;
            this.y = 2;
            return false;
        }
        if (this.G) {
            i = -4;
        } else {
            i = a(this.n, this.u, false);
        }
        if (i == -3) {
            return false;
        }
        if (i == -5) {
            b(this.n.f3165a);
            return true;
        } else if (this.u.isEndOfStream()) {
            this.E = true;
            this.t.a(this.u);
            this.u = null;
            return false;
        } else {
            this.G = b(this.u.c());
            if (this.G) {
                return false;
            }
            this.u.b();
            a(this.u);
            this.t.a(this.u);
            this.z = true;
            this.p.c++;
            this.u = null;
            return true;
        }
    }

    private void w() throws ExoPlaybackException {
        this.G = false;
        if (this.y != 0) {
            z();
            x();
            return;
        }
        this.u = null;
        SimpleOutputBuffer simpleOutputBuffer = this.v;
        if (simpleOutputBuffer != null) {
            simpleOutputBuffer.release();
            this.v = null;
        }
        this.t.flush();
        this.z = false;
    }

    private void x() throws ExoPlaybackException {
        if (this.t == null) {
            this.w = this.x;
            ExoMediaCrypto exoMediaCrypto = null;
            DrmSession<ExoMediaCrypto> drmSession = this.w;
            if (drmSession == null || (exoMediaCrypto = drmSession.a()) != null || this.w.b() != null) {
                try {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    TraceUtil.a("createAudioDecoder");
                    this.t = a(this.q, exoMediaCrypto);
                    TraceUtil.a();
                    long elapsedRealtime2 = SystemClock.elapsedRealtime();
                    this.l.a(this.t.getName(), elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
                    this.p.f3215a++;
                } catch (AudioDecoderException e) {
                    throw ExoPlaybackException.a(e, m());
                }
            }
        }
    }

    private void y() throws ExoPlaybackException {
        this.F = true;
        try {
            this.m.e();
        } catch (AudioSink.WriteException e) {
            throw ExoPlaybackException.a(e, m());
        }
    }

    private void z() {
        SimpleDecoder<DecoderInputBuffer, ? extends SimpleOutputBuffer, ? extends AudioDecoderException> simpleDecoder = this.t;
        if (simpleDecoder != null) {
            this.u = null;
            this.v = null;
            simpleDecoder.release();
            this.t = null;
            this.p.b++;
            this.y = 0;
            this.z = false;
        }
    }

    /* access modifiers changed from: protected */
    public abstract int a(DrmSessionManager<ExoMediaCrypto> drmSessionManager, Format format);

    /* access modifiers changed from: protected */
    public abstract SimpleDecoder<DecoderInputBuffer, ? extends SimpleOutputBuffer, ? extends AudioDecoderException> a(Format format, ExoMediaCrypto exoMediaCrypto) throws AudioDecoderException;

    /* access modifiers changed from: protected */
    public void a(int i) {
    }

    /* access modifiers changed from: protected */
    public void a(int i, long j2, long j3) {
    }

    public long h() {
        if (getState() == 2) {
            A();
        }
        return this.B;
    }

    public boolean isReady() {
        return this.m.c() || (this.q != null && !this.G && (o() || this.v != null));
    }

    public MediaClock j() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void p() {
        this.q = null;
        this.A = true;
        this.G = false;
        try {
            z();
            this.m.release();
            try {
                if (this.w != null) {
                    this.j.a(this.w);
                }
                try {
                    if (!(this.x == null || this.x == this.w)) {
                        this.j.a(this.x);
                    }
                } finally {
                    this.w = null;
                    this.x = null;
                    this.p.a();
                    this.l.a(this.p);
                }
            } catch (Throwable th) {
                if (!(this.x == null || this.x == this.w)) {
                    this.j.a(this.x);
                }
                throw th;
            } finally {
                this.w = null;
                this.x = null;
                this.p.a();
                this.l.a(this.p);
            }
        } catch (Throwable th2) {
            try {
                if (!(this.x == null || this.x == this.w)) {
                    this.j.a(this.x);
                }
                throw th2;
            } finally {
                this.w = null;
                this.x = null;
                this.p.a();
                this.l.a(this.p);
            }
        } finally {
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        this.m.play();
    }

    /* access modifiers changed from: protected */
    public void r() {
        A();
        this.m.pause();
    }

    /* access modifiers changed from: protected */
    public Format s() {
        Format format = this.q;
        return Format.a((String) null, "audio/raw", (String) null, -1, -1, format.t, format.u, 2, (List<byte[]>) null, (DrmInitData) null, 0, (String) null);
    }

    /* access modifiers changed from: protected */
    public void t() {
    }

    private final class AudioSinkListener implements AudioSink.Listener {
        private AudioSinkListener() {
        }

        public void a(int i) {
            SimpleDecoderAudioRenderer.this.l.a(i);
            SimpleDecoderAudioRenderer.this.a(i);
        }

        public void a() {
            SimpleDecoderAudioRenderer.this.t();
            boolean unused = SimpleDecoderAudioRenderer.this.D = true;
        }

        public void a(int i, long j, long j2) {
            SimpleDecoderAudioRenderer.this.l.a(i, j, j2);
            SimpleDecoderAudioRenderer.this.a(i, j, j2);
        }
    }

    public SimpleDecoderAudioRenderer(Handler handler, AudioRendererEventListener audioRendererEventListener, AudioProcessor... audioProcessorArr) {
        this(handler, audioRendererEventListener, (AudioCapabilities) null, (DrmSessionManager<ExoMediaCrypto>) null, false, audioProcessorArr);
    }

    public SimpleDecoderAudioRenderer(Handler handler, AudioRendererEventListener audioRendererEventListener, AudioCapabilities audioCapabilities, DrmSessionManager<ExoMediaCrypto> drmSessionManager, boolean z2, AudioProcessor... audioProcessorArr) {
        this(handler, audioRendererEventListener, drmSessionManager, z2, new DefaultAudioSink(audioCapabilities, audioProcessorArr));
    }

    public final int a(Format format) {
        int i = 0;
        if (!MimeTypes.j(format.g)) {
            return 0;
        }
        int a2 = a(this.j, format);
        if (a2 <= 2) {
            return a2;
        }
        if (Util.f3651a >= 21) {
            i = 32;
        }
        return a2 | i | 8;
    }

    public SimpleDecoderAudioRenderer(Handler handler, AudioRendererEventListener audioRendererEventListener, DrmSessionManager<ExoMediaCrypto> drmSessionManager, boolean z2, AudioSink audioSink) {
        super(1);
        this.j = drmSessionManager;
        this.k = z2;
        this.l = new AudioRendererEventListener.EventDispatcher(handler, audioRendererEventListener);
        this.m = audioSink;
        audioSink.a((AudioSink.Listener) new AudioSinkListener());
        this.n = new FormatHolder();
        this.o = DecoderInputBuffer.e();
        this.y = 0;
        this.A = true;
    }

    public PlaybackParameters b() {
        return this.m.b();
    }

    private void b(Format format) throws ExoPlaybackException {
        Format format2 = this.q;
        this.q = format;
        if (!Util.a((Object) this.q.j, (Object) format2 == null ? null : format2.j)) {
            if (this.q.j != null) {
                DrmSessionManager<ExoMediaCrypto> drmSessionManager = this.j;
                if (drmSessionManager != null) {
                    this.x = drmSessionManager.a(Looper.myLooper(), this.q.j);
                    DrmSession<ExoMediaCrypto> drmSession = this.x;
                    if (drmSession == this.w) {
                        this.j.a(drmSession);
                    }
                } else {
                    throw ExoPlaybackException.a(new IllegalStateException("Media requires a DrmSessionManager"), m());
                }
            } else {
                this.x = null;
            }
        }
        if (this.z) {
            this.y = 1;
        } else {
            z();
            x();
            this.A = true;
        }
        this.r = format.w;
        this.s = format.x;
        this.l.a(format);
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, int i2) {
        return this.m.a(i, i2);
    }

    public void a(long j2, long j3) throws ExoPlaybackException {
        if (this.F) {
            try {
                this.m.e();
            } catch (AudioSink.WriteException e) {
                throw ExoPlaybackException.a(e, m());
            }
        } else {
            if (this.q == null) {
                this.o.clear();
                int a2 = a(this.n, this.o, true);
                if (a2 == -5) {
                    b(this.n.f3165a);
                } else if (a2 == -4) {
                    Assertions.b(this.o.isEndOfStream());
                    this.E = true;
                    y();
                    return;
                } else {
                    return;
                }
            }
            x();
            if (this.t != null) {
                try {
                    TraceUtil.a("drainAndFeed");
                    while (u()) {
                    }
                    while (v()) {
                    }
                    TraceUtil.a();
                    this.p.a();
                } catch (AudioDecoderException | AudioSink.ConfigurationException | AudioSink.InitializationException | AudioSink.WriteException e2) {
                    throw ExoPlaybackException.a(e2, m());
                }
            }
        }
    }

    public boolean a() {
        return this.F && this.m.a();
    }

    public PlaybackParameters a(PlaybackParameters playbackParameters) {
        return this.m.a(playbackParameters);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) throws ExoPlaybackException {
        this.p = new DecoderCounters();
        this.l.b(this.p);
        int i = l().f3172a;
        if (i != 0) {
            this.m.a(i);
        } else {
            this.m.d();
        }
    }

    /* access modifiers changed from: protected */
    public void a(long j2, boolean z2) throws ExoPlaybackException {
        this.m.reset();
        this.B = j2;
        this.C = true;
        this.D = true;
        this.E = false;
        this.F = false;
        if (this.t != null) {
            w();
        }
    }

    public void a(int i, Object obj) throws ExoPlaybackException {
        if (i == 2) {
            this.m.a(((Float) obj).floatValue());
        } else if (i == 3) {
            this.m.a((AudioAttributes) obj);
        } else if (i != 5) {
            super.a(i, obj);
        } else {
            this.m.a((AuxEffectInfo) obj);
        }
    }

    private void a(DecoderInputBuffer decoderInputBuffer) {
        if (this.C && !decoderInputBuffer.isDecodeOnly()) {
            if (Math.abs(decoderInputBuffer.c - this.B) > 500000) {
                this.B = decoderInputBuffer.c;
            }
            this.C = false;
        }
    }
}
