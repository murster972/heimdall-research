package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public final class SonicAudioProcessor implements AudioProcessor {
    private int b = -1;
    private int c = -1;
    private float d = 1.0f;
    private float e = 1.0f;
    private int f = -1;
    private int g = -1;
    private Sonic h;
    private ByteBuffer i = AudioProcessor.f3190a;
    private ShortBuffer j = this.i.asShortBuffer();
    private ByteBuffer k = AudioProcessor.f3190a;
    private long l;
    private long m;
    private boolean n;

    public float a(float f2) {
        float a2 = Util.a(f2, 0.1f, 8.0f);
        if (this.e != a2) {
            this.e = a2;
            this.h = null;
        }
        flush();
        return a2;
    }

    public float b(float f2) {
        float a2 = Util.a(f2, 0.1f, 8.0f);
        if (this.d != a2) {
            this.d = a2;
            this.h = null;
        }
        flush();
        return a2;
    }

    public int c() {
        return this.b;
    }

    public int d() {
        return this.f;
    }

    public int e() {
        return 2;
    }

    public void f() {
        Assertions.b(this.h != null);
        this.h.c();
        this.n = true;
    }

    public void flush() {
        if (isActive()) {
            Sonic sonic = this.h;
            if (sonic == null) {
                this.h = new Sonic(this.c, this.b, this.d, this.e, this.f);
            } else {
                sonic.a();
            }
        }
        this.k = AudioProcessor.f3190a;
        this.l = 0;
        this.m = 0;
        this.n = false;
    }

    public boolean isActive() {
        return this.c != -1 && (Math.abs(this.d - 1.0f) >= 0.01f || Math.abs(this.e - 1.0f) >= 0.01f || this.f != this.c);
    }

    public void reset() {
        this.d = 1.0f;
        this.e = 1.0f;
        this.b = -1;
        this.c = -1;
        this.f = -1;
        this.i = AudioProcessor.f3190a;
        this.j = this.i.asShortBuffer();
        this.k = AudioProcessor.f3190a;
        this.g = -1;
        this.h = null;
        this.l = 0;
        this.m = 0;
        this.n = false;
    }

    public long a(long j2) {
        long j3 = this.m;
        if (j3 < 1024) {
            return (long) (((double) this.d) * ((double) j2));
        }
        int i2 = this.f;
        int i3 = this.c;
        if (i2 == i3) {
            return Util.c(j2, this.l, j3);
        }
        return Util.c(j2, this.l * ((long) i2), j3 * ((long) i3));
    }

    public ByteBuffer b() {
        ByteBuffer byteBuffer = this.k;
        this.k = AudioProcessor.f3190a;
        return byteBuffer;
    }

    public boolean a(int i2, int i3, int i4) throws AudioProcessor.UnhandledFormatException {
        if (i4 == 2) {
            int i5 = this.g;
            if (i5 == -1) {
                i5 = i2;
            }
            if (this.c == i2 && this.b == i3 && this.f == i5) {
                return false;
            }
            this.c = i2;
            this.b = i3;
            this.f = i5;
            this.h = null;
            return true;
        }
        throw new AudioProcessor.UnhandledFormatException(i2, i3, i4);
    }

    public void a(ByteBuffer byteBuffer) {
        Assertions.b(this.h != null);
        if (byteBuffer.hasRemaining()) {
            ShortBuffer asShortBuffer = byteBuffer.asShortBuffer();
            int remaining = byteBuffer.remaining();
            this.l += (long) remaining;
            this.h.b(asShortBuffer);
            byteBuffer.position(byteBuffer.position() + remaining);
        }
        int b2 = this.h.b() * this.b * 2;
        if (b2 > 0) {
            if (this.i.capacity() < b2) {
                this.i = ByteBuffer.allocateDirect(b2).order(ByteOrder.nativeOrder());
                this.j = this.i.asShortBuffer();
            } else {
                this.i.clear();
                this.j.clear();
            }
            this.h.a(this.j);
            this.m += (long) b2;
            this.i.limit(b2);
            this.k = this.i;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r1.h;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a() {
        /*
            r1 = this;
            boolean r0 = r1.n
            if (r0 == 0) goto L_0x0010
            com.google.android.exoplayer2.audio.Sonic r0 = r1.h
            if (r0 == 0) goto L_0x000e
            int r0 = r0.b()
            if (r0 != 0) goto L_0x0010
        L_0x000e:
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.SonicAudioProcessor.a():boolean");
    }
}
