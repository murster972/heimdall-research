package com.google.android.exoplayer2.audio;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.Arrays;

@TargetApi(21)
public final class AudioCapabilities {
    public static final AudioCapabilities c = new AudioCapabilities(new int[]{2}, 8);

    /* renamed from: a  reason: collision with root package name */
    private final int[] f3187a;
    private final int b;

    public AudioCapabilities(int[] iArr, int i) {
        if (iArr != null) {
            this.f3187a = Arrays.copyOf(iArr, iArr.length);
            Arrays.sort(this.f3187a);
        } else {
            this.f3187a = new int[0];
        }
        this.b = i;
    }

    public static AudioCapabilities a(Context context) {
        return a(context.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.media.action.HDMI_AUDIO_PLUG")));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AudioCapabilities)) {
            return false;
        }
        AudioCapabilities audioCapabilities = (AudioCapabilities) obj;
        if (!Arrays.equals(this.f3187a, audioCapabilities.f3187a) || this.b != audioCapabilities.b) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.b + (Arrays.hashCode(this.f3187a) * 31);
    }

    public String toString() {
        return "AudioCapabilities[maxChannelCount=" + this.b + ", supportedEncodings=" + Arrays.toString(this.f3187a) + "]";
    }

    @SuppressLint({"InlinedApi"})
    static AudioCapabilities a(Intent intent) {
        if (intent == null || intent.getIntExtra("android.media.extra.AUDIO_PLUG_STATE", 0) == 0) {
            return c;
        }
        return new AudioCapabilities(intent.getIntArrayExtra("android.media.extra.ENCODINGS"), intent.getIntExtra("android.media.extra.MAX_CHANNEL_COUNT", 8));
    }

    public boolean a(int i) {
        return Arrays.binarySearch(this.f3187a, i) >= 0;
    }

    public int a() {
        return this.b;
    }
}
