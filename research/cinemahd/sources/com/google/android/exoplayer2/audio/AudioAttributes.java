package com.google.android.exoplayer2.audio;

import android.annotation.TargetApi;
import android.media.AudioAttributes;

public final class AudioAttributes {
    public static final AudioAttributes e = new Builder().a();

    /* renamed from: a  reason: collision with root package name */
    public final int f3185a;
    public final int b;
    public final int c;
    private android.media.AudioAttributes d;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private int f3186a = 0;
        private int b = 0;
        private int c = 1;

        public AudioAttributes a() {
            return new AudioAttributes(this.f3186a, this.b, this.c);
        }
    }

    @TargetApi(21)
    public android.media.AudioAttributes a() {
        if (this.d == null) {
            this.d = new AudioAttributes.Builder().setContentType(this.f3185a).setFlags(this.b).setUsage(this.c).build();
        }
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AudioAttributes.class != obj.getClass()) {
            return false;
        }
        AudioAttributes audioAttributes = (AudioAttributes) obj;
        if (this.f3185a == audioAttributes.f3185a && this.b == audioAttributes.b && this.c == audioAttributes.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((527 + this.f3185a) * 31) + this.b) * 31) + this.c;
    }

    private AudioAttributes(int i, int i2, int i3) {
        this.f3185a = i;
        this.b = i2;
        this.c = i3;
    }
}
