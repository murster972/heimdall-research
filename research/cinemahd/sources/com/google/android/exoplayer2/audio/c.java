package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;

/* compiled from: lambda */
public final /* synthetic */ class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f3209a;
    private final /* synthetic */ DecoderCounters b;

    public /* synthetic */ c(AudioRendererEventListener.EventDispatcher eventDispatcher, DecoderCounters decoderCounters) {
        this.f3209a = eventDispatcher;
        this.b = decoderCounters;
    }

    public final void run() {
        this.f3209a.d(this.b);
    }
}
