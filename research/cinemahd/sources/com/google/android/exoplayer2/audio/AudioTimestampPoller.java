package com.google.android.exoplayer2.audio;

import android.annotation.TargetApi;
import android.media.AudioTimestamp;
import android.media.AudioTrack;
import com.google.android.exoplayer2.util.Util;

final class AudioTimestampPoller {

    /* renamed from: a  reason: collision with root package name */
    private final AudioTimestampV19 f3192a;
    private int b;
    private long c;
    private long d;
    private long e;
    private long f;

    @TargetApi(19)
    private static final class AudioTimestampV19 {

        /* renamed from: a  reason: collision with root package name */
        private final AudioTrack f3193a;
        private final AudioTimestamp b = new AudioTimestamp();
        private long c;
        private long d;
        private long e;

        public AudioTimestampV19(AudioTrack audioTrack) {
            this.f3193a = audioTrack;
        }

        public long a() {
            return this.e;
        }

        public long b() {
            return this.b.nanoTime / 1000;
        }

        public boolean c() {
            boolean timestamp = this.f3193a.getTimestamp(this.b);
            if (timestamp) {
                long j = this.b.framePosition;
                if (this.d > j) {
                    this.c++;
                }
                this.d = j;
                this.e = j + (this.c << 32);
            }
            return timestamp;
        }
    }

    public AudioTimestampPoller(AudioTrack audioTrack) {
        if (Util.f3651a >= 19) {
            this.f3192a = new AudioTimestampV19(audioTrack);
            g();
            return;
        }
        this.f3192a = null;
        a(3);
    }

    public boolean a(long j) {
        AudioTimestampV19 audioTimestampV19 = this.f3192a;
        if (audioTimestampV19 == null || j - this.e < this.d) {
            return false;
        }
        this.e = j;
        boolean c2 = audioTimestampV19.c();
        int i = this.b;
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i == 4) {
                            return c2;
                        }
                        throw new IllegalStateException();
                    } else if (!c2) {
                        return c2;
                    } else {
                        g();
                        return c2;
                    }
                } else if (c2) {
                    return c2;
                } else {
                    g();
                    return c2;
                }
            } else if (!c2) {
                g();
                return c2;
            } else if (this.f3192a.a() <= this.f) {
                return c2;
            } else {
                a(2);
                return c2;
            }
        } else if (c2) {
            if (this.f3192a.b() < this.c) {
                return false;
            }
            this.f = this.f3192a.a();
            a(1);
            return c2;
        } else if (j - this.c <= 500000) {
            return c2;
        } else {
            a(3);
            return c2;
        }
    }

    public long b() {
        AudioTimestampV19 audioTimestampV19 = this.f3192a;
        if (audioTimestampV19 != null) {
            return audioTimestampV19.a();
        }
        return -1;
    }

    public long c() {
        AudioTimestampV19 audioTimestampV19 = this.f3192a;
        if (audioTimestampV19 != null) {
            return audioTimestampV19.b();
        }
        return -9223372036854775807L;
    }

    public boolean d() {
        int i = this.b;
        return i == 1 || i == 2;
    }

    public boolean e() {
        return this.b == 2;
    }

    public void f() {
        a(4);
    }

    public void g() {
        if (this.f3192a != null) {
            a(0);
        }
    }

    public void a() {
        if (this.b == 4) {
            g();
        }
    }

    private void a(int i) {
        this.b = i;
        if (i == 0) {
            this.e = 0;
            this.f = -1;
            this.c = System.nanoTime() / 1000;
            this.d = 5000;
        } else if (i == 1) {
            this.d = 5000;
        } else if (i == 2 || i == 3) {
            this.d = 10000000;
        } else if (i == 4) {
            this.d = 500000;
        } else {
            throw new IllegalStateException();
        }
    }
}
