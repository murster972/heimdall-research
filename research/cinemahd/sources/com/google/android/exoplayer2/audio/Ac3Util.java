package com.google.android.exoplayer2.audio;

import com.facebook.imagepipeline.memory.BitmapCounterConfig;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.gms.ads.AdRequest;
import java.nio.ByteBuffer;
import java.util.List;

public final class Ac3Util {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f3183a = {1, 2, 3, 6};
    private static final int[] b = {48000, 44100, 32000};
    private static final int[] c = {24000, 22050, 16000};
    private static final int[] d = {2, 1, 2, 3, 3, 4, 4, 5};
    private static final int[] e = {32, 40, 48, 56, 64, 80, 96, 112, 128, 160, JfifUtil.MARKER_SOFn, 224, 256, 320, BitmapCounterConfig.DEFAULT_MAX_BITMAP_COUNT, 448, AdRequest.MAX_CONTENT_URL_LENGTH, 576, 640};
    private static final int[] f = {69, 87, 104, 121, 139, 174, JfifUtil.MARKER_RST0, 243, 278, 348, 417, 487, 557, 696, 835, 975, 1114, 1253, 1393};

    public static final class SyncFrameInfo {

        /* renamed from: a  reason: collision with root package name */
        public final String f3184a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;

        private SyncFrameInfo(String str, int i, int i2, int i3, int i4, int i5) {
            this.f3184a = str;
            this.c = i2;
            this.b = i3;
            this.d = i4;
            this.e = i5;
        }
    }

    private Ac3Util() {
    }

    public static int a() {
        return 1536;
    }

    public static Format a(ParsableByteArray parsableByteArray, String str, String str2, DrmInitData drmInitData) {
        int i = b[(parsableByteArray.t() & JfifUtil.MARKER_SOFn) >> 6];
        int t = parsableByteArray.t();
        int i2 = d[(t & 56) >> 3];
        if ((t & 4) != 0) {
            i2++;
        }
        return Format.a(str, "audio/ac3", (String) null, -1, -1, i2, i, (List<byte[]>) null, drmInitData, 0, str2);
    }

    public static Format b(ParsableByteArray parsableByteArray, String str, String str2, DrmInitData drmInitData) {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        parsableByteArray.f(2);
        int i = b[(parsableByteArray.t() & JfifUtil.MARKER_SOFn) >> 6];
        int t = parsableByteArray.t();
        int i2 = d[(t & 14) >> 1];
        if ((t & 1) != 0) {
            i2++;
        }
        if (((parsableByteArray.t() & 30) >> 1) > 0 && (2 & parsableByteArray.t()) != 0) {
            i2 += 2;
        }
        return Format.a(str, (parsableByteArray.a() <= 0 || (parsableByteArray.t() & 1) == 0) ? "audio/eac3" : "audio/eac3-joc", (String) null, -1, -1, i2, i, (List<byte[]>) null, drmInitData, 0, str2);
    }

    public static SyncFrameInfo a(ParsableBitArray parsableBitArray) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        String str;
        int i6;
        int i7;
        int i8;
        int i9;
        ParsableBitArray parsableBitArray2 = parsableBitArray;
        int d2 = parsableBitArray.d();
        parsableBitArray2.c(40);
        boolean z = parsableBitArray2.a(5) == 16;
        parsableBitArray2.b(d2);
        int i10 = -1;
        if (z) {
            parsableBitArray2.c(16);
            int a2 = parsableBitArray2.a(2);
            if (a2 == 0) {
                i10 = 0;
            } else if (a2 == 1) {
                i10 = 1;
            } else if (a2 == 2) {
                i10 = 2;
            }
            parsableBitArray2.c(3);
            int a3 = (parsableBitArray2.a(11) + 1) * 2;
            int a4 = parsableBitArray2.a(2);
            if (a4 == 3) {
                i6 = c[parsableBitArray2.a(2)];
                i8 = 3;
                i7 = 6;
            } else {
                i8 = parsableBitArray2.a(2);
                i7 = f3183a[i8];
                i6 = b[a4];
            }
            int i11 = i7 * 256;
            int a5 = parsableBitArray2.a(3);
            boolean e2 = parsableBitArray.e();
            int i12 = d[a5] + (e2 ? 1 : 0);
            parsableBitArray2.c(10);
            if (parsableBitArray.e()) {
                parsableBitArray2.c(8);
            }
            if (a5 == 0) {
                parsableBitArray2.c(5);
                if (parsableBitArray.e()) {
                    parsableBitArray2.c(8);
                }
            }
            if (i10 == 1 && parsableBitArray.e()) {
                parsableBitArray2.c(16);
            }
            if (parsableBitArray.e()) {
                if (a5 > 2) {
                    parsableBitArray2.c(2);
                }
                if ((a5 & 1) != 0 && a5 > 2) {
                    parsableBitArray2.c(6);
                }
                if ((a5 & 4) != 0) {
                    parsableBitArray2.c(6);
                }
                if (e2 && parsableBitArray.e()) {
                    parsableBitArray2.c(5);
                }
                if (i10 == 0) {
                    if (parsableBitArray.e()) {
                        parsableBitArray2.c(6);
                    }
                    if (a5 == 0 && parsableBitArray.e()) {
                        parsableBitArray2.c(6);
                    }
                    if (parsableBitArray.e()) {
                        parsableBitArray2.c(6);
                    }
                    int a6 = parsableBitArray2.a(2);
                    if (a6 == 1) {
                        parsableBitArray2.c(5);
                    } else if (a6 == 2) {
                        parsableBitArray2.c(12);
                    } else if (a6 == 3) {
                        int a7 = parsableBitArray2.a(5);
                        if (parsableBitArray.e()) {
                            parsableBitArray2.c(5);
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(4);
                            }
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(4);
                            }
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(4);
                            }
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(4);
                            }
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(4);
                            }
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(4);
                            }
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(4);
                            }
                            if (parsableBitArray.e()) {
                                if (parsableBitArray.e()) {
                                    parsableBitArray2.c(4);
                                }
                                if (parsableBitArray.e()) {
                                    parsableBitArray2.c(4);
                                }
                            }
                        }
                        if (parsableBitArray.e()) {
                            parsableBitArray2.c(5);
                            if (parsableBitArray.e()) {
                                parsableBitArray2.c(7);
                                if (parsableBitArray.e()) {
                                    parsableBitArray2.c(8);
                                }
                            }
                        }
                        parsableBitArray2.c((a7 + 2) * 8);
                        parsableBitArray.b();
                    }
                    if (a5 < 2) {
                        if (parsableBitArray.e()) {
                            parsableBitArray2.c(14);
                        }
                        if (a5 == 0 && parsableBitArray.e()) {
                            parsableBitArray2.c(14);
                        }
                    }
                    if (parsableBitArray.e()) {
                        if (i8 == 0) {
                            parsableBitArray2.c(5);
                        } else {
                            for (int i13 = 0; i13 < i7; i13++) {
                                if (parsableBitArray.e()) {
                                    parsableBitArray2.c(5);
                                }
                            }
                        }
                    }
                }
            }
            if (parsableBitArray.e()) {
                parsableBitArray2.c(5);
                if (a5 == 2) {
                    parsableBitArray2.c(4);
                }
                if (a5 >= 6) {
                    parsableBitArray2.c(2);
                }
                if (parsableBitArray.e()) {
                    parsableBitArray2.c(8);
                }
                if (a5 == 0 && parsableBitArray.e()) {
                    parsableBitArray2.c(8);
                }
                i9 = 3;
                if (a4 < 3) {
                    parsableBitArray.f();
                }
            } else {
                i9 = 3;
            }
            if (i10 == 0 && i8 != i9) {
                parsableBitArray.f();
            }
            if (i10 == 2 && (i8 == i9 || parsableBitArray.e())) {
                parsableBitArray2.c(6);
            }
            str = (parsableBitArray.e() && parsableBitArray2.a(6) == 1 && parsableBitArray2.a(8) == 1) ? "audio/eac3-joc" : "audio/eac3";
            i5 = i10;
            i2 = a3;
            i3 = i6;
            i = i11;
            i4 = i12;
        } else {
            parsableBitArray2.c(32);
            int a8 = parsableBitArray2.a(2);
            int a9 = a(a8, parsableBitArray2.a(6));
            parsableBitArray2.c(8);
            int a10 = parsableBitArray2.a(3);
            if (!((a10 & 1) == 0 || a10 == 1)) {
                parsableBitArray2.c(2);
            }
            if ((a10 & 4) != 0) {
                parsableBitArray2.c(2);
            }
            if (a10 == 2) {
                parsableBitArray2.c(2);
            }
            str = "audio/ac3";
            i2 = a9;
            i3 = b[a8];
            i4 = d[a10] + (parsableBitArray.e() ? 1 : 0);
            i5 = -1;
            i = 1536;
        }
        return new SyncFrameInfo(str, i5, i4, i3, i2, i);
    }

    public static int b(ByteBuffer byteBuffer) {
        int i = 6;
        if (((byteBuffer.get(byteBuffer.position() + 4) & 192) >> 6) != 3) {
            i = f3183a[(byteBuffer.get(byteBuffer.position() + 4) & 48) >> 4];
        }
        return i * 256;
    }

    public static int b(byte[] bArr) {
        boolean z = false;
        if (bArr[4] != -8 || bArr[5] != 114 || bArr[6] != 111 || (bArr[7] & 254) != 186) {
            return 0;
        }
        if ((bArr[7] & 255) == 187) {
            z = true;
        }
        return 40 << ((bArr[z ? (char) 9 : 8] >> 4) & 7);
    }

    public static int a(byte[] bArr) {
        if (bArr.length < 6) {
            return -1;
        }
        if (!(((bArr[5] & 255) >> 3) == 16)) {
            return a((bArr[4] & 192) >> 6, (int) bArr[4] & 63);
        }
        return (((bArr[3] & 255) | ((bArr[2] & 7) << 8)) + 1) * 2;
    }

    public static int a(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int limit = byteBuffer.limit() - 10;
        for (int i = position; i <= limit; i++) {
            if ((byteBuffer.getInt(i + 4) & -16777217) == -1167101192) {
                return i - position;
            }
        }
        return -1;
    }

    public static int a(ByteBuffer byteBuffer, int i) {
        return 40 << ((byteBuffer.get((byteBuffer.position() + i) + ((byteBuffer.get((byteBuffer.position() + i) + 7) & 255) == 187 ? 9 : 8)) >> 4) & 7);
    }

    private static int a(int i, int i2) {
        int i3 = i2 / 2;
        if (i < 0) {
            return -1;
        }
        int[] iArr = b;
        if (i >= iArr.length || i2 < 0) {
            return -1;
        }
        int[] iArr2 = f;
        if (i3 >= iArr2.length) {
            return -1;
        }
        int i4 = iArr[i];
        if (i4 == 44100) {
            return (iArr2[i3] + (i2 % 2)) * 2;
        }
        int i5 = e[i3];
        return i4 == 32000 ? i5 * 6 : i5 * 4;
    }
}
