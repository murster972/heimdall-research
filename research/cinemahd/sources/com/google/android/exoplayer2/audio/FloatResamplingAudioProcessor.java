package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

final class FloatResamplingAudioProcessor implements AudioProcessor {
    private static final int h = Float.floatToIntBits(Float.NaN);
    private int b = -1;
    private int c = -1;
    private int d = 0;
    private ByteBuffer e;
    private ByteBuffer f;
    private boolean g;

    public FloatResamplingAudioProcessor() {
        ByteBuffer byteBuffer = AudioProcessor.f3190a;
        this.e = byteBuffer;
        this.f = byteBuffer;
    }

    public boolean a(int i, int i2, int i3) throws AudioProcessor.UnhandledFormatException {
        if (!Util.e(i3)) {
            throw new AudioProcessor.UnhandledFormatException(i, i2, i3);
        } else if (this.b == i && this.c == i2 && this.d == i3) {
            return false;
        } else {
            this.b = i;
            this.c = i2;
            this.d = i3;
            return true;
        }
    }

    public ByteBuffer b() {
        ByteBuffer byteBuffer = this.f;
        this.f = AudioProcessor.f3190a;
        return byteBuffer;
    }

    public int c() {
        return this.c;
    }

    public int d() {
        return this.b;
    }

    public int e() {
        return 4;
    }

    public void f() {
        this.g = true;
    }

    public void flush() {
        this.f = AudioProcessor.f3190a;
        this.g = false;
    }

    public boolean isActive() {
        return Util.e(this.d);
    }

    public void reset() {
        flush();
        this.b = -1;
        this.c = -1;
        this.d = 0;
        this.e = AudioProcessor.f3190a;
    }

    public void a(ByteBuffer byteBuffer) {
        boolean z = this.d == 1073741824;
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int i = limit - position;
        if (!z) {
            i = (i / 3) * 4;
        }
        if (this.e.capacity() < i) {
            this.e = ByteBuffer.allocateDirect(i).order(ByteOrder.nativeOrder());
        } else {
            this.e.clear();
        }
        if (z) {
            while (position < limit) {
                a((byteBuffer.get(position) & 255) | ((byteBuffer.get(position + 1) & 255) << 8) | ((byteBuffer.get(position + 2) & 255) << 16) | ((byteBuffer.get(position + 3) & 255) << 24), this.e);
                position += 4;
            }
        } else {
            while (position < limit) {
                a(((byteBuffer.get(position) & 255) << 8) | ((byteBuffer.get(position + 1) & 255) << 16) | ((byteBuffer.get(position + 2) & 255) << 24), this.e);
                position += 3;
            }
        }
        byteBuffer.position(byteBuffer.limit());
        this.e.flip();
        this.f = this.e;
    }

    public boolean a() {
        return this.g && this.f == AudioProcessor.f3190a;
    }

    private static void a(int i, ByteBuffer byteBuffer) {
        int floatToIntBits = Float.floatToIntBits((float) (((double) i) * 4.656612875245797E-10d));
        if (floatToIntBits == h) {
            floatToIntBits = Float.floatToIntBits(0.0f);
        }
        byteBuffer.putInt(floatToIntBits);
    }
}
