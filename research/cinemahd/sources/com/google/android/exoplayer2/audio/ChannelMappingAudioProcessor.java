package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.util.Assertions;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

final class ChannelMappingAudioProcessor implements AudioProcessor {
    private int b = -1;
    private int c = -1;
    private int[] d;
    private boolean e;
    private int[] f;
    private ByteBuffer g;
    private ByteBuffer h;
    private boolean i;

    public ChannelMappingAudioProcessor() {
        ByteBuffer byteBuffer = AudioProcessor.f3190a;
        this.g = byteBuffer;
        this.h = byteBuffer;
    }

    public void a(int[] iArr) {
        this.d = iArr;
    }

    public ByteBuffer b() {
        ByteBuffer byteBuffer = this.h;
        this.h = AudioProcessor.f3190a;
        return byteBuffer;
    }

    public int c() {
        int[] iArr = this.f;
        return iArr == null ? this.b : iArr.length;
    }

    public int d() {
        return this.c;
    }

    public int e() {
        return 2;
    }

    public void f() {
        this.i = true;
    }

    public void flush() {
        this.h = AudioProcessor.f3190a;
        this.i = false;
    }

    public boolean isActive() {
        return this.e;
    }

    public void reset() {
        flush();
        this.g = AudioProcessor.f3190a;
        this.b = -1;
        this.c = -1;
        this.f = null;
        this.d = null;
        this.e = false;
    }

    public boolean a(int i2, int i3, int i4) throws AudioProcessor.UnhandledFormatException {
        boolean z = !Arrays.equals(this.d, this.f);
        this.f = this.d;
        if (this.f == null) {
            this.e = false;
            return z;
        } else if (i4 != 2) {
            throw new AudioProcessor.UnhandledFormatException(i2, i3, i4);
        } else if (!z && this.c == i2 && this.b == i3) {
            return false;
        } else {
            this.c = i2;
            this.b = i3;
            this.e = i3 != this.f.length;
            int i5 = 0;
            while (true) {
                int[] iArr = this.f;
                if (i5 >= iArr.length) {
                    return true;
                }
                int i6 = iArr[i5];
                if (i6 < i3) {
                    this.e = (i6 != i5) | this.e;
                    i5++;
                } else {
                    throw new AudioProcessor.UnhandledFormatException(i2, i3, i4);
                }
            }
        }
    }

    public void a(ByteBuffer byteBuffer) {
        Assertions.b(this.f != null);
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int length = ((limit - position) / (this.b * 2)) * this.f.length * 2;
        if (this.g.capacity() < length) {
            this.g = ByteBuffer.allocateDirect(length).order(ByteOrder.nativeOrder());
        } else {
            this.g.clear();
        }
        while (position < limit) {
            for (int i2 : this.f) {
                this.g.putShort(byteBuffer.getShort((i2 * 2) + position));
            }
            position += this.b * 2;
        }
        byteBuffer.position(limit);
        this.g.flip();
        this.h = this.g;
    }

    public boolean a() {
        return this.i && this.h == AudioProcessor.f3190a;
    }
}
