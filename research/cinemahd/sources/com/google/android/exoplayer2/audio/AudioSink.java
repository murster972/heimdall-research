package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.PlaybackParameters;
import java.nio.ByteBuffer;

public interface AudioSink {

    public static final class ConfigurationException extends Exception {
        public ConfigurationException(Throwable th) {
            super(th);
        }

        public ConfigurationException(String str) {
            super(str);
        }
    }

    public static final class InitializationException extends Exception {
        public final int audioTrackState;

        public InitializationException(int i, int i2, int i3, int i4) {
            super("AudioTrack init failed: " + i + ", Config(" + i2 + ", " + i3 + ", " + i4 + ")");
            this.audioTrackState = i;
        }
    }

    public interface Listener {
        void a();

        void a(int i);

        void a(int i, long j, long j2);
    }

    public static final class WriteException extends Exception {
        public final int errorCode;

        public WriteException(int i) {
            super("AudioTrack write failed: " + i);
            this.errorCode = i;
        }
    }

    long a(boolean z);

    PlaybackParameters a(PlaybackParameters playbackParameters);

    void a(float f);

    void a(int i);

    void a(int i, int i2, int i3, int i4, int[] iArr, int i5, int i6) throws ConfigurationException;

    void a(AudioAttributes audioAttributes);

    void a(Listener listener);

    void a(AuxEffectInfo auxEffectInfo);

    boolean a();

    boolean a(int i, int i2);

    boolean a(ByteBuffer byteBuffer, long j) throws InitializationException, WriteException;

    PlaybackParameters b();

    boolean c();

    void d();

    void e() throws WriteException;

    void f();

    void pause();

    void play();

    void release();

    void reset();
}
