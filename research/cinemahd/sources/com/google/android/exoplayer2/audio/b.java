package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f3208a;
    private final /* synthetic */ String b;
    private final /* synthetic */ long c;
    private final /* synthetic */ long d;

    public /* synthetic */ b(AudioRendererEventListener.EventDispatcher eventDispatcher, String str, long j, long j2) {
        this.f3208a = eventDispatcher;
        this.b = str;
        this.c = j;
        this.d = j2;
    }

    public final void run() {
        this.f3208a.b(this.b, this.c, this.d);
    }
}
