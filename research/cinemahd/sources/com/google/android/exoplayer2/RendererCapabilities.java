package com.google.android.exoplayer2;

public interface RendererCapabilities {
    int a(Format format) throws ExoPlaybackException;

    int getTrackType();

    int k() throws ExoPlaybackException;
}
