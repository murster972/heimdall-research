package com.google.android.exoplayer2;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

/* compiled from: Player */
public final /* synthetic */ class b {
    public static void a(Player.EventListener eventListener) {
    }

    public static void a(Player.EventListener eventListener, int i) {
    }

    public static void a(Player.EventListener eventListener, ExoPlaybackException exoPlaybackException) {
    }

    public static void a(Player.EventListener eventListener, PlaybackParameters playbackParameters) {
    }

    public static void a(Player.EventListener eventListener, Timeline timeline, Object obj, int i) {
    }

    public static void a(Player.EventListener eventListener, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    public static void a(Player.EventListener eventListener, boolean z) {
    }

    public static void a(Player.EventListener eventListener, boolean z, int i) {
    }

    public static void b(Player.EventListener eventListener, int i) {
    }

    public static void b(Player.EventListener eventListener, boolean z) {
    }
}
