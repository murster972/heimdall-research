package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f3673a;
    private final /* synthetic */ int b;
    private final /* synthetic */ long c;

    public /* synthetic */ g(VideoRendererEventListener.EventDispatcher eventDispatcher, int i, long j) {
        this.f3673a = eventDispatcher;
        this.b = i;
        this.c = j;
    }

    public final void run() {
        this.f3673a.b(this.b, this.c);
    }
}
