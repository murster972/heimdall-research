package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f3669a;
    private final /* synthetic */ int b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;
    private final /* synthetic */ float e;

    public /* synthetic */ c(VideoRendererEventListener.EventDispatcher eventDispatcher, int i, int i2, int i3, float f) {
        this.f3669a = eventDispatcher;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = f;
    }

    public final void run() {
        this.f3669a.a(this.b, this.c, this.d, this.e);
    }
}
