package com.google.android.exoplayer2.video;

import android.os.Handler;
import android.view.Surface;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.util.Assertions;

public interface VideoRendererEventListener {
    void a(int i, long j);

    void a(Surface surface);

    void a(Format format);

    void a(String str, long j, long j2);

    void b(DecoderCounters decoderCounters);

    void d(DecoderCounters decoderCounters);

    void onVideoSizeChanged(int i, int i2, int i3, float f);

    public static final class EventDispatcher {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f3666a;
        private final VideoRendererEventListener b;

        public EventDispatcher(Handler handler, VideoRendererEventListener videoRendererEventListener) {
            Handler handler2;
            if (videoRendererEventListener != null) {
                Assertions.a(handler);
                handler2 = handler;
            } else {
                handler2 = null;
            }
            this.f3666a = handler2;
            this.b = videoRendererEventListener;
        }

        public void a(String str, long j, long j2) {
            if (this.b != null) {
                this.f3666a.post(new d(this, str, j, j2));
            }
        }

        public void b(DecoderCounters decoderCounters) {
            if (this.b != null) {
                this.f3666a.post(new e(this, decoderCounters));
            }
        }

        public /* synthetic */ void c(DecoderCounters decoderCounters) {
            decoderCounters.a();
            this.b.b(decoderCounters);
        }

        public /* synthetic */ void d(DecoderCounters decoderCounters) {
            this.b.d(decoderCounters);
        }

        public void a(Format format) {
            if (this.b != null) {
                this.f3666a.post(new a(this, format));
            }
        }

        public /* synthetic */ void b(String str, long j, long j2) {
            this.b.a(str, j, j2);
        }

        public /* synthetic */ void b(Format format) {
            this.b.a(format);
        }

        public void a(int i, long j) {
            if (this.b != null) {
                this.f3666a.post(new g(this, i, j));
            }
        }

        public /* synthetic */ void b(int i, long j) {
            this.b.a(i, j);
        }

        public void b(int i, int i2, int i3, float f) {
            if (this.b != null) {
                this.f3666a.post(new c(this, i, i2, i3, f));
            }
        }

        public /* synthetic */ void a(int i, int i2, int i3, float f) {
            this.b.onVideoSizeChanged(i, i2, i3, f);
        }

        public /* synthetic */ void a(Surface surface) {
            this.b.a(surface);
        }

        public void b(Surface surface) {
            if (this.b != null) {
                this.f3666a.post(new b(this, surface));
            }
        }

        public void a(DecoderCounters decoderCounters) {
            if (this.b != null) {
                this.f3666a.post(new f(this, decoderCounters));
            }
        }
    }
}
