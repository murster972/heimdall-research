package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f3670a;
    private final /* synthetic */ String b;
    private final /* synthetic */ long c;
    private final /* synthetic */ long d;

    public /* synthetic */ d(VideoRendererEventListener.EventDispatcher eventDispatcher, String str, long j, long j2) {
        this.f3670a = eventDispatcher;
        this.b = str;
        this.c = j;
        this.d = j2;
    }

    public final void run() {
        this.f3670a.b(this.b, this.c, this.d);
    }
}
