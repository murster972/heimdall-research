package com.google.android.exoplayer2.video.spherical;

import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;

public class CameraMotionRenderer extends BaseRenderer {
    private final FormatHolder j = new FormatHolder();
    private final DecoderInputBuffer k = new DecoderInputBuffer(1);
    private final ParsableByteArray l = new ParsableByteArray();
    private long m;
    private CameraMotionListener n;
    private long o;

    public CameraMotionRenderer() {
        super(5);
    }

    private void s() {
        this.o = 0;
        CameraMotionListener cameraMotionListener = this.n;
        if (cameraMotionListener != null) {
            cameraMotionListener.a();
        }
    }

    public int a(Format format) {
        return "application/x-camera-motion".equals(format.g) ? 4 : 0;
    }

    public boolean isReady() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void p() {
        s();
    }

    public void a(int i, Object obj) throws ExoPlaybackException {
        if (i == 7) {
            this.n = (CameraMotionListener) obj;
        } else {
            super.a(i, obj);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Format[] formatArr, long j2) throws ExoPlaybackException {
        this.m = j2;
    }

    /* access modifiers changed from: protected */
    public void a(long j2, boolean z) throws ExoPlaybackException {
        s();
    }

    public void a(long j2, long j3) throws ExoPlaybackException {
        float[] a2;
        while (!c() && this.o < 100000 + j2) {
            this.k.clear();
            if (a(this.j, this.k, false) == -4 && !this.k.isEndOfStream()) {
                this.k.b();
                DecoderInputBuffer decoderInputBuffer = this.k;
                this.o = decoderInputBuffer.c;
                if (!(this.n == null || (a2 = a(decoderInputBuffer.b)) == null)) {
                    CameraMotionListener cameraMotionListener = this.n;
                    Util.a(cameraMotionListener);
                    cameraMotionListener.a(this.o - this.m, a2);
                }
            } else {
                return;
            }
        }
    }

    public boolean a() {
        return c();
    }

    private float[] a(ByteBuffer byteBuffer) {
        if (byteBuffer.remaining() != 16) {
            return null;
        }
        this.l.a(byteBuffer.array(), byteBuffer.limit());
        this.l.e(byteBuffer.arrayOffset() + 4);
        float[] fArr = new float[3];
        for (int i = 0; i < 3; i++) {
            fArr[i] = Float.intBitsToFloat(this.l.k());
        }
        return fArr;
    }
}
