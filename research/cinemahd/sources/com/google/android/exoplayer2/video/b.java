package com.google.android.exoplayer2.video;

import android.view.Surface;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f3668a;
    private final /* synthetic */ Surface b;

    public /* synthetic */ b(VideoRendererEventListener.EventDispatcher eventDispatcher, Surface surface) {
        this.f3668a = eventDispatcher;
        this.b = surface;
    }

    public final void run() {
        this.f3668a.a(this.b);
    }
}
