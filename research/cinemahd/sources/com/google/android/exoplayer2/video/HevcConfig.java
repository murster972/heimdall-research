package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.Collections;
import java.util.List;

public final class HevcConfig {

    /* renamed from: a  reason: collision with root package name */
    public final List<byte[]> f3660a;
    public final int b;

    private HevcConfig(List<byte[]> list, int i) {
        this.f3660a = list;
        this.b = i;
    }

    public static HevcConfig a(ParsableByteArray parsableByteArray) throws ParserException {
        List list;
        try {
            parsableByteArray.f(21);
            int t = parsableByteArray.t() & 3;
            int t2 = parsableByteArray.t();
            int c = parsableByteArray.c();
            int i = 0;
            int i2 = 0;
            while (i < t2) {
                parsableByteArray.f(1);
                int z = parsableByteArray.z();
                int i3 = i2;
                for (int i4 = 0; i4 < z; i4++) {
                    int z2 = parsableByteArray.z();
                    i3 += z2 + 4;
                    parsableByteArray.f(z2);
                }
                i++;
                i2 = i3;
            }
            parsableByteArray.e(c);
            byte[] bArr = new byte[i2];
            int i5 = 0;
            int i6 = 0;
            while (i5 < t2) {
                parsableByteArray.f(1);
                int z3 = parsableByteArray.z();
                int i7 = i6;
                for (int i8 = 0; i8 < z3; i8++) {
                    int z4 = parsableByteArray.z();
                    System.arraycopy(NalUnitUtil.f3637a, 0, bArr, i7, NalUnitUtil.f3637a.length);
                    int length = i7 + NalUnitUtil.f3637a.length;
                    System.arraycopy(parsableByteArray.f3641a, parsableByteArray.c(), bArr, length, z4);
                    i7 = length + z4;
                    parsableByteArray.f(z4);
                }
                i5++;
                i6 = i7;
            }
            if (i2 == 0) {
                list = null;
            } else {
                list = Collections.singletonList(bArr);
            }
            return new HevcConfig(list, t + 1);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ParserException("Error parsing HEVC config", e);
        }
    }
}
