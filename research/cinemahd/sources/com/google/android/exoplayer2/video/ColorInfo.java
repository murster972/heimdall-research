package com.google.android.exoplayer2.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;

public final class ColorInfo implements Parcelable {
    public static final Parcelable.Creator<ColorInfo> CREATOR = new Parcelable.Creator<ColorInfo>() {
        public ColorInfo[] newArray(int i) {
            return new ColorInfo[0];
        }

        public ColorInfo createFromParcel(Parcel parcel) {
            return new ColorInfo(parcel);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final int f3657a;
    public final int b;
    public final int c;
    public final byte[] d;
    private int e;

    public ColorInfo(int i, int i2, int i3, byte[] bArr) {
        this.f3657a = i;
        this.b = i2;
        this.c = i3;
        this.d = bArr;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ColorInfo.class != obj.getClass()) {
            return false;
        }
        ColorInfo colorInfo = (ColorInfo) obj;
        if (this.f3657a == colorInfo.f3657a && this.b == colorInfo.b && this.c == colorInfo.c && Arrays.equals(this.d, colorInfo.d)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.e == 0) {
            this.e = ((((((527 + this.f3657a) * 31) + this.b) * 31) + this.c) * 31) + Arrays.hashCode(this.d);
        }
        return this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ColorInfo(");
        sb.append(this.f3657a);
        sb.append(", ");
        sb.append(this.b);
        sb.append(", ");
        sb.append(this.c);
        sb.append(", ");
        sb.append(this.d != null);
        sb.append(")");
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f3657a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        Util.a(parcel, this.d != null);
        byte[] bArr = this.d;
        if (bArr != null) {
            parcel.writeByteArray(bArr);
        }
    }

    ColorInfo(Parcel parcel) {
        this.f3657a = parcel.readInt();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = Util.a(parcel) ? parcel.createByteArray() : null;
    }
}
