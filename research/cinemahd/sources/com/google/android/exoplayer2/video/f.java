package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f3672a;
    private final /* synthetic */ DecoderCounters b;

    public /* synthetic */ f(VideoRendererEventListener.EventDispatcher eventDispatcher, DecoderCounters decoderCounters) {
        this.f3672a = eventDispatcher;
        this.b = decoderCounters;
    }

    public final void run() {
        this.f3672a.c(this.b);
    }
}
