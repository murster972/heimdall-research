package com.google.android.exoplayer2.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.Choreographer;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.exoplayer2.util.Util;

@TargetApi(16)
public final class VideoFrameReleaseTimeHelper {

    /* renamed from: a  reason: collision with root package name */
    private final WindowManager f3663a;
    private final VSyncSampler b;
    private final DefaultDisplayListener c;
    private long d;
    private long e;
    private long f;
    private long g;
    private long h;
    private boolean i;
    private long j;
    private long k;
    private long l;

    @TargetApi(17)
    private final class DefaultDisplayListener implements DisplayManager.DisplayListener {

        /* renamed from: a  reason: collision with root package name */
        private final DisplayManager f3664a;

        public DefaultDisplayListener(DisplayManager displayManager) {
            this.f3664a = displayManager;
        }

        public void a() {
            this.f3664a.registerDisplayListener(this, (Handler) null);
        }

        public void b() {
            this.f3664a.unregisterDisplayListener(this);
        }

        public void onDisplayAdded(int i) {
        }

        public void onDisplayChanged(int i) {
            if (i == 0) {
                VideoFrameReleaseTimeHelper.this.c();
            }
        }

        public void onDisplayRemoved(int i) {
        }
    }

    private static final class VSyncSampler implements Choreographer.FrameCallback, Handler.Callback {
        private static final VSyncSampler f = new VSyncSampler();

        /* renamed from: a  reason: collision with root package name */
        public volatile long f3665a = -9223372036854775807L;
        private final Handler b;
        private final HandlerThread c = new HandlerThread("ChoreographerOwner:Handler");
        private Choreographer d;
        private int e;

        private VSyncSampler() {
            this.c.start();
            this.b = Util.a(this.c.getLooper(), (Handler.Callback) this);
            this.b.sendEmptyMessage(0);
        }

        private void c() {
            this.e++;
            if (this.e == 1) {
                this.d.postFrameCallback(this);
            }
        }

        private void d() {
            this.d = Choreographer.getInstance();
        }

        public static VSyncSampler e() {
            return f;
        }

        private void f() {
            this.e--;
            if (this.e == 0) {
                this.d.removeFrameCallback(this);
                this.f3665a = -9223372036854775807L;
            }
        }

        public void a() {
            this.b.sendEmptyMessage(1);
        }

        public void b() {
            this.b.sendEmptyMessage(2);
        }

        public void doFrame(long j) {
            this.f3665a = j;
            this.d.postFrameCallbackDelayed(this, 500);
        }

        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                d();
                return true;
            } else if (i == 1) {
                c();
                return true;
            } else if (i != 2) {
                return false;
            } else {
                f();
                return true;
            }
        }
    }

    public VideoFrameReleaseTimeHelper() {
        this((Context) null);
    }

    /* access modifiers changed from: private */
    public void c() {
        Display defaultDisplay = this.f3663a.getDefaultDisplay();
        if (defaultDisplay != null) {
            this.d = (long) (1.0E9d / ((double) defaultDisplay.getRefreshRate()));
            this.e = (this.d * 80) / 100;
        }
    }

    public void b() {
        this.i = false;
        if (this.f3663a != null) {
            this.b.a();
            DefaultDisplayListener defaultDisplayListener = this.c;
            if (defaultDisplayListener != null) {
                defaultDisplayListener.a();
            }
            c();
        }
    }

    public VideoFrameReleaseTimeHelper(Context context) {
        DefaultDisplayListener defaultDisplayListener = null;
        if (context != null) {
            context = context.getApplicationContext();
            this.f3663a = (WindowManager) context.getSystemService("window");
        } else {
            this.f3663a = null;
        }
        if (this.f3663a != null) {
            this.c = Util.f3651a >= 17 ? a(context) : defaultDisplayListener;
            this.b = VSyncSampler.e();
        } else {
            this.c = null;
            this.b = null;
        }
        this.d = -9223372036854775807L;
        this.e = -9223372036854775807L;
    }

    public void a() {
        if (this.f3663a != null) {
            DefaultDisplayListener defaultDisplayListener = this.c;
            if (defaultDisplayListener != null) {
                defaultDisplayListener.b();
            }
            this.b.b();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0078 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(long r11, long r13) {
        /*
            r10 = this;
            r0 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 * r11
            boolean r2 = r10.i
            if (r2 == 0) goto L_0x0042
            long r2 = r10.f
            int r4 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x0019
            long r2 = r10.l
            r4 = 1
            long r2 = r2 + r4
            r10.l = r2
            long r2 = r10.h
            r10.g = r2
        L_0x0019:
            long r2 = r10.l
            r4 = 6
            r6 = 0
            int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r7 < 0) goto L_0x003a
            long r4 = r10.k
            long r4 = r0 - r4
            long r4 = r4 / r2
            long r2 = r10.g
            long r2 = r2 + r4
            boolean r4 = r10.b(r2, r13)
            if (r4 == 0) goto L_0x0033
            r10.i = r6
            goto L_0x0042
        L_0x0033:
            long r4 = r10.j
            long r4 = r4 + r2
            long r6 = r10.k
            long r4 = r4 - r6
            goto L_0x0044
        L_0x003a:
            boolean r2 = r10.b(r0, r13)
            if (r2 == 0) goto L_0x0042
            r10.i = r6
        L_0x0042:
            r4 = r13
            r2 = r0
        L_0x0044:
            boolean r6 = r10.i
            if (r6 != 0) goto L_0x0053
            r10.k = r0
            r10.j = r13
            r13 = 0
            r10.l = r13
            r13 = 1
            r10.i = r13
        L_0x0053:
            r10.f = r11
            r10.h = r2
            com.google.android.exoplayer2.video.VideoFrameReleaseTimeHelper$VSyncSampler r11 = r10.b
            if (r11 == 0) goto L_0x0078
            long r12 = r10.d
            r0 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r14 = (r12 > r0 ? 1 : (r12 == r0 ? 0 : -1))
            if (r14 != 0) goto L_0x0067
            goto L_0x0078
        L_0x0067:
            long r6 = r11.f3665a
            int r11 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r11 != 0) goto L_0x006e
            return r4
        L_0x006e:
            long r8 = r10.d
            long r11 = a(r4, r6, r8)
            long r13 = r10.e
            long r11 = r11 - r13
            return r11
        L_0x0078:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.video.VideoFrameReleaseTimeHelper.a(long, long):long");
    }

    private boolean b(long j2, long j3) {
        return Math.abs((j3 - this.j) - (j2 - this.k)) > 20000000;
    }

    @TargetApi(17)
    private DefaultDisplayListener a(Context context) {
        DisplayManager displayManager = (DisplayManager) context.getSystemService(ViewProps.DISPLAY);
        if (displayManager == null) {
            return null;
        }
        return new DefaultDisplayListener(displayManager);
    }

    private static long a(long j2, long j3, long j4) {
        long j5;
        long j6 = j3 + (((j2 - j3) / j4) * j4);
        if (j2 <= j6) {
            j5 = j6 - j4;
        } else {
            long j7 = j6;
            j6 = j4 + j6;
            j5 = j7;
        }
        return j6 - j2 < j2 - j5 ? j6 : j5;
    }
}
