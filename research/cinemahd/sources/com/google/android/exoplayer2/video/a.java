package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* compiled from: lambda */
public final /* synthetic */ class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f3667a;
    private final /* synthetic */ Format b;

    public /* synthetic */ a(VideoRendererEventListener.EventDispatcher eventDispatcher, Format format) {
        this.f3667a = eventDispatcher;
        this.b = format;
    }

    public final void run() {
        this.f3667a.b(this.b);
    }
}
