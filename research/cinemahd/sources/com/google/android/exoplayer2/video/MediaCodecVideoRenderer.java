package com.google.android.exoplayer2.video;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Surface;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.mediacodec.MediaCodecInfo;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.mediacodec.MediaFormatUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import java.nio.ByteBuffer;
import java.util.List;
import org.joda.time.DateTimeConstants;

@TargetApi(16)
public class MediaCodecVideoRenderer extends MediaCodecRenderer {
    private static final int[] W0 = {1920, 1600, DateTimeConstants.MINUTES_PER_DAY, 1280, 960, 854, 640, 540, 480};
    private static boolean X0;
    private static boolean Y0;
    private long A0;
    private int B0;
    private int C0;
    private int D0;
    private long E0;
    private int F0;
    private float G0;
    private int H0;
    private int I0;
    private int J0;
    private float K0;
    private int L0;
    private int M0;
    private int N0;
    private float O0;
    private boolean P0;
    private int Q0;
    OnFrameRenderedListenerV23 R0;
    private long S0;
    private long T0;
    private int U0;
    private VideoFrameMetadataListener V0;
    private final Context k0;
    private final VideoFrameReleaseTimeHelper l0 = new VideoFrameReleaseTimeHelper(this.k0);
    private final VideoRendererEventListener.EventDispatcher m0;
    private final long n0;
    private final int o0;
    private final boolean p0;
    private final long[] q0;
    private final long[] r0;
    private CodecMaxValues s0;
    private boolean t0;
    private Surface u0;
    private Surface v0;
    private int w0;
    private boolean x0;
    private long y0;
    private long z0;

    protected static final class CodecMaxValues {

        /* renamed from: a  reason: collision with root package name */
        public final int f3661a;
        public final int b;
        public final int c;

        public CodecMaxValues(int i, int i2, int i3) {
            this.f3661a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    @TargetApi(23)
    private final class OnFrameRenderedListenerV23 implements MediaCodec.OnFrameRenderedListener {
        public void onFrameRendered(MediaCodec mediaCodec, long j, long j2) {
            MediaCodecVideoRenderer mediaCodecVideoRenderer = MediaCodecVideoRenderer.this;
            if (this == mediaCodecVideoRenderer.R0) {
                mediaCodecVideoRenderer.e(j);
            }
        }

        private OnFrameRenderedListenerV23(MediaCodec mediaCodec) {
            mediaCodec.setOnFrameRenderedListener(this, new Handler());
        }
    }

    public MediaCodecVideoRenderer(Context context, MediaCodecSelector mediaCodecSelector, long j, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, boolean z, Handler handler, VideoRendererEventListener videoRendererEventListener, int i) {
        super(2, mediaCodecSelector, drmSessionManager, z, 30.0f);
        this.n0 = j;
        this.o0 = i;
        this.k0 = context.getApplicationContext();
        this.m0 = new VideoRendererEventListener.EventDispatcher(handler, videoRendererEventListener);
        this.p0 = D();
        this.q0 = new long[10];
        this.r0 = new long[10];
        this.T0 = -9223372036854775807L;
        this.S0 = -9223372036854775807L;
        this.z0 = -9223372036854775807L;
        this.H0 = -1;
        this.I0 = -1;
        this.K0 = -1.0f;
        this.G0 = -1.0f;
        this.w0 = 1;
        C();
    }

    private void B() {
        MediaCodec t;
        this.x0 = false;
        if (Util.f3651a >= 23 && this.P0 && (t = t()) != null) {
            this.R0 = new OnFrameRenderedListenerV23(t);
        }
    }

    private void C() {
        this.L0 = -1;
        this.M0 = -1;
        this.O0 = -1.0f;
        this.N0 = -1;
    }

    private static boolean D() {
        return Util.f3651a <= 22 && "foster".equals(Util.b) && "NVIDIA".equals(Util.c);
    }

    private void E() {
        if (this.B0 > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.m0.a(this.B0, elapsedRealtime - this.A0);
            this.B0 = 0;
            this.A0 = elapsedRealtime;
        }
    }

    private void F() {
        if (this.H0 != -1 || this.I0 != -1) {
            if (this.L0 != this.H0 || this.M0 != this.I0 || this.N0 != this.J0 || this.O0 != this.K0) {
                this.m0.b(this.H0, this.I0, this.J0, this.K0);
                this.L0 = this.H0;
                this.M0 = this.I0;
                this.N0 = this.J0;
                this.O0 = this.K0;
            }
        }
    }

    private void G() {
        if (this.x0) {
            this.m0.b(this.u0);
        }
    }

    private void H() {
        if (this.L0 != -1 || this.M0 != -1) {
            this.m0.b(this.L0, this.M0, this.N0, this.O0);
        }
    }

    private void I() {
        this.z0 = this.n0 > 0 ? SystemClock.elapsedRealtime() + this.n0 : -9223372036854775807L;
    }

    private static boolean f(long j) {
        return j < -30000;
    }

    private static boolean g(long j) {
        return j < -500000;
    }

    /* access modifiers changed from: package-private */
    public void A() {
        if (!this.x0) {
            this.x0 = true;
            this.m0.b(this.u0);
        }
    }

    /* access modifiers changed from: protected */
    public int a(MediaCodecSelector mediaCodecSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, Format format) throws MediaCodecUtil.DecoderQueryException {
        boolean z;
        int i = 0;
        if (!MimeTypes.l(format.g)) {
            return 0;
        }
        DrmInitData drmInitData = format.j;
        if (drmInitData != null) {
            z = false;
            for (int i2 = 0; i2 < drmInitData.d; i2++) {
                z |= drmInitData.a(i2).f;
            }
        } else {
            z = false;
        }
        List<MediaCodecInfo> a2 = mediaCodecSelector.a(format.g, z);
        if (a2.isEmpty()) {
            if (!z || mediaCodecSelector.a(format.g, false).isEmpty()) {
                return 1;
            }
            return 2;
        } else if (!BaseRenderer.a((DrmSessionManager<?>) drmSessionManager, drmInitData)) {
            return 2;
        } else {
            MediaCodecInfo mediaCodecInfo = a2.get(0);
            boolean a3 = mediaCodecInfo.a(format);
            int i3 = mediaCodecInfo.b(format) ? 16 : 8;
            if (mediaCodecInfo.e) {
                i = 32;
            }
            return (a3 ? 4 : 3) | i3 | i;
        }
    }

    /* access modifiers changed from: protected */
    public void b(Format format) throws ExoPlaybackException {
        super.b(format);
        this.m0.a(format);
        this.G0 = format.p;
        this.F0 = format.o;
    }

    /* access modifiers changed from: protected */
    public void c(long j) {
        this.D0--;
        while (true) {
            int i = this.U0;
            if (i != 0 && j >= this.r0[0]) {
                long[] jArr = this.q0;
                this.T0 = jArr[0];
                this.U0 = i - 1;
                System.arraycopy(jArr, 1, jArr, 0, this.U0);
                long[] jArr2 = this.r0;
                System.arraycopy(jArr2, 1, jArr2, 0, this.U0);
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean d(long j, long j2) {
        return f(j) && j2 > 100000;
    }

    /* access modifiers changed from: protected */
    public void e(long j) {
        Format d = d(j);
        if (d != null) {
            a(t(), d.l, d.m);
        }
        F();
        A();
        c(j);
    }

    public boolean isReady() {
        Surface surface;
        if (super.isReady() && (this.x0 || (((surface = this.v0) != null && this.u0 == surface) || t() == null || this.P0))) {
            this.z0 = -9223372036854775807L;
            return true;
        } else if (this.z0 == -9223372036854775807L) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.z0) {
                return true;
            }
            this.z0 = -9223372036854775807L;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void p() {
        this.H0 = -1;
        this.I0 = -1;
        this.K0 = -1.0f;
        this.G0 = -1.0f;
        this.T0 = -9223372036854775807L;
        this.S0 = -9223372036854775807L;
        this.U0 = 0;
        C();
        B();
        this.l0.a();
        this.R0 = null;
        this.P0 = false;
        try {
            super.p();
        } finally {
            this.i0.a();
            this.m0.a(this.i0);
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        super.q();
        this.B0 = 0;
        this.A0 = SystemClock.elapsedRealtime();
        this.E0 = SystemClock.elapsedRealtime() * 1000;
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.z0 = -9223372036854775807L;
        E();
        super.r();
    }

    /* access modifiers changed from: protected */
    public void s() throws ExoPlaybackException {
        super.s();
        this.D0 = 0;
    }

    /* access modifiers changed from: protected */
    public boolean v() {
        return this.P0;
    }

    /* access modifiers changed from: protected */
    public void y() {
        try {
            super.y();
        } finally {
            this.D0 = 0;
            Surface surface = this.v0;
            if (surface != null) {
                if (this.u0 == surface) {
                    this.u0 = null;
                }
                this.v0.release();
                this.v0 = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean b(long j, long j2) {
        return g(j);
    }

    /* access modifiers changed from: protected */
    public void b(MediaCodec mediaCodec, int i, long j) {
        F();
        TraceUtil.a("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, true);
        TraceUtil.a();
        this.E0 = SystemClock.elapsedRealtime() * 1000;
        this.i0.e++;
        this.C0 = 0;
        A();
    }

    /* access modifiers changed from: protected */
    public boolean c(long j, long j2) {
        return f(j);
    }

    /* access modifiers changed from: protected */
    public void c(MediaCodec mediaCodec, int i, long j) {
        TraceUtil.a("skipVideoBuffer");
        mediaCodec.releaseOutputBuffer(i, false);
        TraceUtil.a();
        this.i0.f++;
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public void b(MediaCodec mediaCodec, int i, long j, long j2) {
        F();
        TraceUtil.a("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, j2);
        TraceUtil.a();
        this.E0 = SystemClock.elapsedRealtime() * 1000;
        this.i0.e++;
        this.C0 = 0;
        A();
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) throws ExoPlaybackException {
        super.a(z);
        this.Q0 = l().f3172a;
        this.P0 = this.Q0 != 0;
        this.m0.b(this.i0);
        this.l0.b();
    }

    private boolean b(MediaCodecInfo mediaCodecInfo) {
        return Util.f3651a >= 23 && !this.P0 && !a(mediaCodecInfo.f3359a) && (!mediaCodecInfo.f || DummySurface.b(this.k0));
    }

    /* access modifiers changed from: protected */
    public void a(Format[] formatArr, long j) throws ExoPlaybackException {
        if (this.T0 == -9223372036854775807L) {
            this.T0 = j;
        } else {
            int i = this.U0;
            if (i == this.q0.length) {
                Log.d("MediaCodecVideoRenderer", "Too many stream changes, so dropping offset: " + this.q0[this.U0 - 1]);
            } else {
                this.U0 = i + 1;
            }
            long[] jArr = this.q0;
            int i2 = this.U0;
            jArr[i2 - 1] = j;
            this.r0[i2 - 1] = this.S0;
        }
        super.a(formatArr, j);
    }

    private static int b(MediaCodecInfo mediaCodecInfo, Format format) {
        if (format.h == -1) {
            return a(mediaCodecInfo, format.g, format.l, format.m);
        }
        int size = format.i.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            i += format.i.get(i2).length;
        }
        return format.h + i;
    }

    /* access modifiers changed from: protected */
    public void a(long j, boolean z) throws ExoPlaybackException {
        super.a(j, z);
        B();
        this.y0 = -9223372036854775807L;
        this.C0 = 0;
        this.S0 = -9223372036854775807L;
        int i = this.U0;
        if (i != 0) {
            this.T0 = this.q0[i - 1];
            this.U0 = 0;
        }
        if (z) {
            I();
        } else {
            this.z0 = -9223372036854775807L;
        }
    }

    public void a(int i, Object obj) throws ExoPlaybackException {
        if (i == 1) {
            a((Surface) obj);
        } else if (i == 4) {
            this.w0 = ((Integer) obj).intValue();
            MediaCodec t = t();
            if (t != null) {
                t.setVideoScalingMode(this.w0);
            }
        } else if (i == 6) {
            this.V0 = (VideoFrameMetadataListener) obj;
        } else {
            super.a(i, obj);
        }
    }

    private void a(Surface surface) throws ExoPlaybackException {
        if (surface == null) {
            Surface surface2 = this.v0;
            if (surface2 != null) {
                surface = surface2;
            } else {
                MediaCodecInfo u = u();
                if (u != null && b(u)) {
                    this.v0 = DummySurface.a(this.k0, u.f);
                    surface = this.v0;
                }
            }
        }
        if (this.u0 != surface) {
            this.u0 = surface;
            int state = getState();
            if (state == 1 || state == 2) {
                MediaCodec t = t();
                if (Util.f3651a < 23 || t == null || surface == null || this.t0) {
                    y();
                    x();
                } else {
                    a(t, surface);
                }
            }
            if (surface == null || surface == this.v0) {
                C();
                B();
                return;
            }
            H();
            B();
            if (state == 2) {
                I();
            }
        } else if (surface != null && surface != this.v0) {
            H();
            G();
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(MediaCodecInfo mediaCodecInfo) {
        return this.u0 != null || b(mediaCodecInfo);
    }

    /* access modifiers changed from: protected */
    public void a(MediaCodecInfo mediaCodecInfo, MediaCodec mediaCodec, Format format, MediaCrypto mediaCrypto, float f) throws MediaCodecUtil.DecoderQueryException {
        this.s0 = a(mediaCodecInfo, format, n());
        MediaFormat a2 = a(format, this.s0, f, this.p0, this.Q0);
        if (this.u0 == null) {
            Assertions.b(b(mediaCodecInfo));
            if (this.v0 == null) {
                this.v0 = DummySurface.a(this.k0, mediaCodecInfo.f);
            }
            this.u0 = this.v0;
        }
        mediaCodec.configure(a2, this.u0, mediaCrypto, 0);
        if (Util.f3651a >= 23 && this.P0) {
            this.R0 = new OnFrameRenderedListenerV23(mediaCodec);
        }
    }

    /* access modifiers changed from: protected */
    public int a(MediaCodec mediaCodec, MediaCodecInfo mediaCodecInfo, Format format, Format format2) {
        if (!mediaCodecInfo.a(format, format2, true)) {
            return 0;
        }
        int i = format2.l;
        CodecMaxValues codecMaxValues = this.s0;
        if (i > codecMaxValues.f3661a || format2.m > codecMaxValues.b || b(mediaCodecInfo, format2) > this.s0.c) {
            return 0;
        }
        if (format.b(format2)) {
            return 1;
        }
        return 3;
    }

    /* access modifiers changed from: protected */
    public float a(float f, Format format, Format[] formatArr) {
        float f2 = -1.0f;
        for (Format format2 : formatArr) {
            float f3 = format2.n;
            if (f3 != -1.0f) {
                f2 = Math.max(f2, f3);
            }
        }
        if (f2 == -1.0f) {
            return -1.0f;
        }
        return f2 * f;
    }

    /* access modifiers changed from: protected */
    public void a(String str, long j, long j2) {
        this.m0.a(str, j, j2);
        this.t0 = a(str);
    }

    /* access modifiers changed from: protected */
    public void a(DecoderInputBuffer decoderInputBuffer) {
        this.D0++;
        this.S0 = Math.max(decoderInputBuffer.c, this.S0);
        if (Util.f3651a < 23 && this.P0) {
            e(decoderInputBuffer.c);
        }
    }

    /* access modifiers changed from: protected */
    public void a(MediaCodec mediaCodec, MediaFormat mediaFormat) {
        int i;
        int i2;
        boolean z = mediaFormat.containsKey("crop-right") && mediaFormat.containsKey("crop-left") && mediaFormat.containsKey("crop-bottom") && mediaFormat.containsKey("crop-top");
        if (z) {
            i = (mediaFormat.getInteger("crop-right") - mediaFormat.getInteger("crop-left")) + 1;
        } else {
            i = mediaFormat.getInteger("width");
        }
        if (z) {
            i2 = (mediaFormat.getInteger("crop-bottom") - mediaFormat.getInteger("crop-top")) + 1;
        } else {
            i2 = mediaFormat.getInteger("height");
        }
        a(mediaCodec, i, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j3, boolean z, Format format) throws ExoPlaybackException {
        long j4 = j;
        long j5 = j2;
        MediaCodec mediaCodec2 = mediaCodec;
        int i3 = i;
        long j6 = j3;
        if (this.y0 == -9223372036854775807L) {
            this.y0 = j4;
        }
        long j7 = j6 - this.T0;
        if (z) {
            c(mediaCodec2, i3, j7);
            return true;
        }
        long j8 = j6 - j4;
        if (this.u0 != this.v0) {
            long elapsedRealtime = SystemClock.elapsedRealtime() * 1000;
            boolean z2 = getState() == 2;
            if (!this.x0 || (z2 && d(j8, elapsedRealtime - this.E0))) {
                long nanoTime = System.nanoTime();
                a(j7, nanoTime, format);
                if (Util.f3651a >= 21) {
                    b(mediaCodec, i, j7, nanoTime);
                    return true;
                }
                b(mediaCodec2, i3, j7);
                return true;
            }
            if (z2 && j4 != this.y0) {
                long nanoTime2 = System.nanoTime();
                long a2 = this.l0.a(j6, ((j8 - (elapsedRealtime - j5)) * 1000) + nanoTime2);
                long j9 = (a2 - nanoTime2) / 1000;
                if (b(j9, j5) && a(mediaCodec, i, j7, j)) {
                    return false;
                }
                if (c(j9, j5)) {
                    a(mediaCodec2, i3, j7);
                    return true;
                } else if (Util.f3651a >= 21) {
                    if (j9 < 50000) {
                        a(j7, a2, format);
                        b(mediaCodec, i, j7, a2);
                        return true;
                    }
                } else if (j9 < 30000) {
                    if (j9 > 11000) {
                        try {
                            Thread.sleep((j9 - 10000) / 1000);
                        } catch (InterruptedException unused) {
                            Thread.currentThread().interrupt();
                            return false;
                        }
                    }
                    a(j7, a2, format);
                    b(mediaCodec2, i3, j7);
                    return true;
                }
            }
            return false;
        } else if (!f(j8)) {
            return false;
        } else {
            c(mediaCodec2, i3, j7);
            return true;
        }
    }

    private void a(MediaCodec mediaCodec, int i, int i2) {
        this.H0 = i;
        this.I0 = i2;
        this.K0 = this.G0;
        if (Util.f3651a >= 21) {
            int i3 = this.F0;
            if (i3 == 90 || i3 == 270) {
                int i4 = this.H0;
                this.H0 = this.I0;
                this.I0 = i4;
                this.K0 = 1.0f / this.K0;
            }
        } else {
            this.J0 = this.F0;
        }
        mediaCodec.setVideoScalingMode(this.w0);
    }

    private void a(long j, long j2, Format format) {
        VideoFrameMetadataListener videoFrameMetadataListener = this.V0;
        if (videoFrameMetadataListener != null) {
            videoFrameMetadataListener.a(j, j2, format);
        }
    }

    /* access modifiers changed from: protected */
    public void a(MediaCodec mediaCodec, int i, long j) {
        TraceUtil.a("dropVideoBuffer");
        mediaCodec.releaseOutputBuffer(i, false);
        TraceUtil.a();
        a(1);
    }

    /* access modifiers changed from: protected */
    public boolean a(MediaCodec mediaCodec, int i, long j, long j2) throws ExoPlaybackException {
        int b = b(j2);
        if (b == 0) {
            return false;
        }
        this.i0.i++;
        a(this.D0 + b);
        s();
        return true;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        DecoderCounters decoderCounters = this.i0;
        decoderCounters.g += i;
        this.B0 += i;
        this.C0 += i;
        decoderCounters.h = Math.max(this.C0, decoderCounters.h);
        int i2 = this.o0;
        if (i2 > 0 && this.B0 >= i2) {
            E();
        }
    }

    @TargetApi(23)
    private static void a(MediaCodec mediaCodec, Surface surface) {
        mediaCodec.setOutputSurface(surface);
    }

    @TargetApi(21)
    private static void a(MediaFormat mediaFormat, int i) {
        mediaFormat.setFeatureEnabled("tunneled-playback", true);
        mediaFormat.setInteger("audio-session-id", i);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"InlinedApi"})
    public MediaFormat a(Format format, CodecMaxValues codecMaxValues, float f, boolean z, int i) {
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", format.g);
        mediaFormat.setInteger("width", format.l);
        mediaFormat.setInteger("height", format.m);
        MediaFormatUtil.a(mediaFormat, format.i);
        MediaFormatUtil.a(mediaFormat, "frame-rate", format.n);
        MediaFormatUtil.a(mediaFormat, "rotation-degrees", format.o);
        MediaFormatUtil.a(mediaFormat, format.s);
        mediaFormat.setInteger("max-width", codecMaxValues.f3661a);
        mediaFormat.setInteger("max-height", codecMaxValues.b);
        MediaFormatUtil.a(mediaFormat, "max-input-size", codecMaxValues.c);
        if (Util.f3651a >= 23) {
            mediaFormat.setInteger("priority", 0);
            if (f != -1.0f) {
                mediaFormat.setFloat("operating-rate", f);
            }
        }
        if (z) {
            mediaFormat.setInteger("auto-frc", 0);
        }
        if (i != 0) {
            a(mediaFormat, i);
        }
        return mediaFormat;
    }

    /* access modifiers changed from: protected */
    public CodecMaxValues a(MediaCodecInfo mediaCodecInfo, Format format, Format[] formatArr) throws MediaCodecUtil.DecoderQueryException {
        int a2;
        int i = format.l;
        int i2 = format.m;
        int b = b(mediaCodecInfo, format);
        if (formatArr.length == 1) {
            if (!(b == -1 || (a2 = a(mediaCodecInfo, format.g, format.l, format.m)) == -1)) {
                b = Math.min((int) (((float) b) * 1.5f), a2);
            }
            return new CodecMaxValues(i, i2, b);
        }
        int i3 = i2;
        int i4 = b;
        boolean z = false;
        int i5 = i;
        for (Format format2 : formatArr) {
            if (mediaCodecInfo.a(format, format2, false)) {
                z |= format2.l == -1 || format2.m == -1;
                i5 = Math.max(i5, format2.l);
                i3 = Math.max(i3, format2.m);
                i4 = Math.max(i4, b(mediaCodecInfo, format2));
            }
        }
        if (z) {
            Log.d("MediaCodecVideoRenderer", "Resolutions unknown. Codec max resolution: " + i5 + "x" + i3);
            Point a3 = a(mediaCodecInfo, format);
            if (a3 != null) {
                i5 = Math.max(i5, a3.x);
                i3 = Math.max(i3, a3.y);
                i4 = Math.max(i4, a(mediaCodecInfo, format.g, i5, i3));
                Log.d("MediaCodecVideoRenderer", "Codec max resolution adjusted to: " + i5 + "x" + i3);
            }
        }
        return new CodecMaxValues(i5, i3, i4);
    }

    private static Point a(MediaCodecInfo mediaCodecInfo, Format format) throws MediaCodecUtil.DecoderQueryException {
        boolean z = format.m > format.l;
        int i = z ? format.m : format.l;
        int i2 = z ? format.l : format.m;
        float f = ((float) i2) / ((float) i);
        for (int i3 : W0) {
            int i4 = (int) (((float) i3) * f);
            if (i3 <= i || i4 <= i2) {
                break;
            }
            if (Util.f3651a >= 21) {
                int i5 = z ? i4 : i3;
                if (!z) {
                    i3 = i4;
                }
                Point a2 = mediaCodecInfo.a(i5, i3);
                if (mediaCodecInfo.a(a2.x, a2.y, (double) format.n)) {
                    return a2;
                }
            } else {
                int a3 = Util.a(i3, 16) * 16;
                int a4 = Util.a(i4, 16) * 16;
                if (a3 * a4 <= MediaCodecUtil.b()) {
                    int i6 = z ? a4 : a3;
                    if (z) {
                        a4 = a3;
                    }
                    return new Point(i6, a4);
                }
            }
        }
        return null;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(com.google.android.exoplayer2.mediacodec.MediaCodecInfo r7, java.lang.String r8, int r9, int r10) {
        /*
            r0 = -1
            if (r9 == r0) goto L_0x00a9
            if (r10 != r0) goto L_0x0007
            goto L_0x00a9
        L_0x0007:
            int r1 = r8.hashCode()
            r2 = 5
            r3 = 1
            r4 = 4
            r5 = 3
            r6 = 2
            switch(r1) {
                case -1664118616: goto L_0x0046;
                case -1662541442: goto L_0x003c;
                case 1187890754: goto L_0x0032;
                case 1331836730: goto L_0x0028;
                case 1599127256: goto L_0x001e;
                case 1599127257: goto L_0x0014;
                default: goto L_0x0013;
            }
        L_0x0013:
            goto L_0x0050
        L_0x0014:
            java.lang.String r1 = "video/x-vnd.on2.vp9"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x0050
            r8 = 5
            goto L_0x0051
        L_0x001e:
            java.lang.String r1 = "video/x-vnd.on2.vp8"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x0050
            r8 = 3
            goto L_0x0051
        L_0x0028:
            java.lang.String r1 = "video/avc"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x0050
            r8 = 2
            goto L_0x0051
        L_0x0032:
            java.lang.String r1 = "video/mp4v-es"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x0050
            r8 = 1
            goto L_0x0051
        L_0x003c:
            java.lang.String r1 = "video/hevc"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x0050
            r8 = 4
            goto L_0x0051
        L_0x0046:
            java.lang.String r1 = "video/3gpp"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x0050
            r8 = 0
            goto L_0x0051
        L_0x0050:
            r8 = -1
        L_0x0051:
            if (r8 == 0) goto L_0x00a0
            if (r8 == r3) goto L_0x00a0
            if (r8 == r6) goto L_0x0061
            if (r8 == r5) goto L_0x00a0
            if (r8 == r4) goto L_0x005e
            if (r8 == r2) goto L_0x005e
            return r0
        L_0x005e:
            int r9 = r9 * r10
            goto L_0x00a3
        L_0x0061:
            java.lang.String r8 = com.google.android.exoplayer2.util.Util.d
            java.lang.String r1 = "BRAVIA 4K 2015"
            boolean r8 = r1.equals(r8)
            if (r8 != 0) goto L_0x009f
            java.lang.String r8 = com.google.android.exoplayer2.util.Util.c
            java.lang.String r1 = "Amazon"
            boolean r8 = r1.equals(r8)
            if (r8 == 0) goto L_0x008e
            java.lang.String r8 = com.google.android.exoplayer2.util.Util.d
            java.lang.String r1 = "KFSOWI"
            boolean r8 = r1.equals(r8)
            if (r8 != 0) goto L_0x009f
            java.lang.String r8 = com.google.android.exoplayer2.util.Util.d
            java.lang.String r1 = "AFTS"
            boolean r8 = r1.equals(r8)
            if (r8 == 0) goto L_0x008e
            boolean r7 = r7.f
            if (r7 == 0) goto L_0x008e
            goto L_0x009f
        L_0x008e:
            r7 = 16
            int r8 = com.google.android.exoplayer2.util.Util.a((int) r9, (int) r7)
            int r9 = com.google.android.exoplayer2.util.Util.a((int) r10, (int) r7)
            int r8 = r8 * r9
            int r8 = r8 * 16
            int r9 = r8 * 16
            goto L_0x00a2
        L_0x009f:
            return r0
        L_0x00a0:
            int r9 = r9 * r10
        L_0x00a2:
            r4 = 2
        L_0x00a3:
            int r9 = r9 * 3
            int r4 = r4 * 2
            int r9 = r9 / r4
            return r9
        L_0x00a9:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.video.MediaCodecVideoRenderer.a(com.google.android.exoplayer2.mediacodec.MediaCodecInfo, java.lang.String, int, int):int");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:375:0x05be, code lost:
        r1 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:376:0x05bf, code lost:
        switch(r1) {
            case 0: goto L_0x05c3;
            case 1: goto L_0x05c3;
            case 2: goto L_0x05c3;
            case 3: goto L_0x05c3;
            case 4: goto L_0x05c3;
            case 5: goto L_0x05c3;
            case 6: goto L_0x05c3;
            case 7: goto L_0x05c3;
            case 8: goto L_0x05c3;
            case 9: goto L_0x05c3;
            case 10: goto L_0x05c3;
            case 11: goto L_0x05c3;
            case 12: goto L_0x05c3;
            case 13: goto L_0x05c3;
            case 14: goto L_0x05c3;
            case 15: goto L_0x05c3;
            case 16: goto L_0x05c3;
            case 17: goto L_0x05c3;
            case 18: goto L_0x05c3;
            case 19: goto L_0x05c3;
            case 20: goto L_0x05c3;
            case 21: goto L_0x05c3;
            case 22: goto L_0x05c3;
            case 23: goto L_0x05c3;
            case 24: goto L_0x05c3;
            case 25: goto L_0x05c3;
            case 26: goto L_0x05c3;
            case 27: goto L_0x05c3;
            case 28: goto L_0x05c3;
            case 29: goto L_0x05c3;
            case 30: goto L_0x05c3;
            case 31: goto L_0x05c3;
            case 32: goto L_0x05c3;
            case 33: goto L_0x05c3;
            case 34: goto L_0x05c3;
            case 35: goto L_0x05c3;
            case 36: goto L_0x05c3;
            case 37: goto L_0x05c3;
            case 38: goto L_0x05c3;
            case 39: goto L_0x05c3;
            case 40: goto L_0x05c3;
            case 41: goto L_0x05c3;
            case 42: goto L_0x05c3;
            case 43: goto L_0x05c3;
            case 44: goto L_0x05c3;
            case 45: goto L_0x05c3;
            case 46: goto L_0x05c3;
            case 47: goto L_0x05c3;
            case 48: goto L_0x05c3;
            case 49: goto L_0x05c3;
            case 50: goto L_0x05c3;
            case 51: goto L_0x05c3;
            case 52: goto L_0x05c3;
            case 53: goto L_0x05c3;
            case 54: goto L_0x05c3;
            case 55: goto L_0x05c3;
            case 56: goto L_0x05c3;
            case 57: goto L_0x05c3;
            case 58: goto L_0x05c3;
            case 59: goto L_0x05c3;
            case 60: goto L_0x05c3;
            case 61: goto L_0x05c3;
            case 62: goto L_0x05c3;
            case 63: goto L_0x05c3;
            case 64: goto L_0x05c3;
            case 65: goto L_0x05c3;
            case 66: goto L_0x05c3;
            case 67: goto L_0x05c3;
            case 68: goto L_0x05c3;
            case 69: goto L_0x05c3;
            case 70: goto L_0x05c3;
            case 71: goto L_0x05c3;
            case 72: goto L_0x05c3;
            case 73: goto L_0x05c3;
            case 74: goto L_0x05c3;
            case 75: goto L_0x05c3;
            case 76: goto L_0x05c3;
            case 77: goto L_0x05c3;
            case 78: goto L_0x05c3;
            case 79: goto L_0x05c3;
            case 80: goto L_0x05c3;
            case 81: goto L_0x05c3;
            case 82: goto L_0x05c3;
            case 83: goto L_0x05c3;
            case 84: goto L_0x05c3;
            case 85: goto L_0x05c3;
            case 86: goto L_0x05c3;
            case 87: goto L_0x05c3;
            case 88: goto L_0x05c3;
            case 89: goto L_0x05c3;
            case 90: goto L_0x05c3;
            case 91: goto L_0x05c3;
            case 92: goto L_0x05c3;
            case 93: goto L_0x05c3;
            case 94: goto L_0x05c3;
            case 95: goto L_0x05c3;
            case 96: goto L_0x05c3;
            case 97: goto L_0x05c3;
            case 98: goto L_0x05c3;
            case 99: goto L_0x05c3;
            case 100: goto L_0x05c3;
            case 101: goto L_0x05c3;
            case 102: goto L_0x05c3;
            case 103: goto L_0x05c3;
            case 104: goto L_0x05c3;
            case 105: goto L_0x05c3;
            case 106: goto L_0x05c3;
            case 107: goto L_0x05c3;
            case 108: goto L_0x05c3;
            case 109: goto L_0x05c3;
            case 110: goto L_0x05c3;
            case 111: goto L_0x05c3;
            case 112: goto L_0x05c3;
            case 113: goto L_0x05c3;
            case 114: goto L_0x05c3;
            case 115: goto L_0x05c3;
            case 116: goto L_0x05c3;
            case 117: goto L_0x05c3;
            case 118: goto L_0x05c3;
            case 119: goto L_0x05c3;
            case 120: goto L_0x05c3;
            default: goto L_0x05c2;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:378:0x05c3, code lost:
        Y0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:379:0x05c5, code lost:
        r0 = com.google.android.exoplayer2.util.Util.d;
        r1 = r0.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:380:0x05ce, code lost:
        if (r1 == 2006354) goto L_0x05e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:382:0x05d3, code lost:
        if (r1 == 2006367) goto L_0x05d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:385:0x05dc, code lost:
        if (r0.equals("AFTN") == false) goto L_0x05e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:386:0x05de, code lost:
        r2 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:388:0x05e6, code lost:
        if (r0.equals("AFTA") == false) goto L_0x05e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:390:0x05e9, code lost:
        r2 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:391:0x05ea, code lost:
        if (r2 == 0) goto L_0x05ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:392:0x05ec, code lost:
        if (r2 == 1) goto L_0x05ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:394:0x05ef, code lost:
        Y0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:395:0x05f1, code lost:
        X0 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r7) {
        /*
            r6 = this;
            int r0 = com.google.android.exoplayer2.util.Util.f3651a
            r1 = 27
            r2 = 0
            if (r0 >= r1) goto L_0x05fa
            java.lang.String r0 = "OMX.google"
            boolean r7 = r7.startsWith(r0)
            if (r7 == 0) goto L_0x0011
            goto L_0x05fa
        L_0x0011:
            java.lang.Class<com.google.android.exoplayer2.video.MediaCodecVideoRenderer> r7 = com.google.android.exoplayer2.video.MediaCodecVideoRenderer.class
            monitor-enter(r7)
            boolean r0 = X0     // Catch:{ all -> 0x05f7 }
            if (r0 != 0) goto L_0x05f3
            java.lang.String r0 = com.google.android.exoplayer2.util.Util.b     // Catch:{ all -> 0x05f7 }
            int r3 = r0.hashCode()     // Catch:{ all -> 0x05f7 }
            r4 = -1
            r5 = 1
            switch(r3) {
                case -2144781245: goto L_0x05b3;
                case -2144781185: goto L_0x05a8;
                case -2144781160: goto L_0x059d;
                case -2097309513: goto L_0x0592;
                case -2022874474: goto L_0x0587;
                case -1978993182: goto L_0x057c;
                case -1978990237: goto L_0x0571;
                case -1936688988: goto L_0x0566;
                case -1936688066: goto L_0x055b;
                case -1936688065: goto L_0x054f;
                case -1931988508: goto L_0x0543;
                case -1696512866: goto L_0x0537;
                case -1680025915: goto L_0x052b;
                case -1615810839: goto L_0x051f;
                case -1554255044: goto L_0x0513;
                case -1481772737: goto L_0x0507;
                case -1481772730: goto L_0x04fb;
                case -1481772729: goto L_0x04ef;
                case -1320080169: goto L_0x04e3;
                case -1217592143: goto L_0x04d7;
                case -1180384755: goto L_0x04cb;
                case -1139198265: goto L_0x04bf;
                case -1052835013: goto L_0x04b3;
                case -993250464: goto L_0x04a8;
                case -965403638: goto L_0x049c;
                case -958336948: goto L_0x0492;
                case -879245230: goto L_0x0486;
                case -842500323: goto L_0x047a;
                case -821392978: goto L_0x046f;
                case -797483286: goto L_0x0463;
                case -794946968: goto L_0x0457;
                case -788334647: goto L_0x044b;
                case -782144577: goto L_0x043f;
                case -575125681: goto L_0x0433;
                case -521118391: goto L_0x0427;
                case -430914369: goto L_0x041b;
                case -290434366: goto L_0x040f;
                case -282781963: goto L_0x0403;
                case -277133239: goto L_0x03f7;
                case -173639913: goto L_0x03eb;
                case -56598463: goto L_0x03df;
                case 2126: goto L_0x03d3;
                case 2564: goto L_0x03c7;
                case 2715: goto L_0x03bb;
                case 2719: goto L_0x03af;
                case 3483: goto L_0x03a3;
                case 73405: goto L_0x0397;
                case 75739: goto L_0x038b;
                case 76779: goto L_0x037f;
                case 78669: goto L_0x0373;
                case 79305: goto L_0x0367;
                case 80618: goto L_0x035b;
                case 88274: goto L_0x034f;
                case 98846: goto L_0x0343;
                case 98848: goto L_0x0337;
                case 99329: goto L_0x032b;
                case 101481: goto L_0x031f;
                case 1513190: goto L_0x0314;
                case 1514184: goto L_0x0309;
                case 1514185: goto L_0x02fe;
                case 2436959: goto L_0x02f2;
                case 2463773: goto L_0x02e6;
                case 2464648: goto L_0x02da;
                case 2689555: goto L_0x02ce;
                case 3284551: goto L_0x02c2;
                case 3351335: goto L_0x02b6;
                case 3386211: goto L_0x02aa;
                case 41325051: goto L_0x029e;
                case 55178625: goto L_0x0292;
                case 61542055: goto L_0x0287;
                case 65355429: goto L_0x027b;
                case 66214468: goto L_0x026f;
                case 66214470: goto L_0x0263;
                case 66214473: goto L_0x0257;
                case 66215429: goto L_0x024b;
                case 66215431: goto L_0x023f;
                case 66215433: goto L_0x0233;
                case 66216390: goto L_0x0227;
                case 76402249: goto L_0x021b;
                case 76404105: goto L_0x020f;
                case 76404911: goto L_0x0203;
                case 80963634: goto L_0x01f7;
                case 82882791: goto L_0x01eb;
                case 98715550: goto L_0x01df;
                case 102844228: goto L_0x01d3;
                case 165221241: goto L_0x01c8;
                case 182191441: goto L_0x01bc;
                case 245388979: goto L_0x01b0;
                case 287431619: goto L_0x01a4;
                case 307593612: goto L_0x0198;
                case 308517133: goto L_0x018c;
                case 316215098: goto L_0x0180;
                case 316215116: goto L_0x0174;
                case 316246811: goto L_0x0168;
                case 316246818: goto L_0x015c;
                case 407160593: goto L_0x0150;
                case 507412548: goto L_0x0144;
                case 793982701: goto L_0x0138;
                case 794038622: goto L_0x012c;
                case 794040393: goto L_0x0120;
                case 835649806: goto L_0x0114;
                case 917340916: goto L_0x0109;
                case 958008161: goto L_0x00fd;
                case 1060579533: goto L_0x00f1;
                case 1150207623: goto L_0x00e5;
                case 1176899427: goto L_0x00d9;
                case 1280332038: goto L_0x00cd;
                case 1306947716: goto L_0x00c1;
                case 1349174697: goto L_0x00b5;
                case 1522194893: goto L_0x00a9;
                case 1691543273: goto L_0x009d;
                case 1709443163: goto L_0x0091;
                case 1865889110: goto L_0x0085;
                case 1906253259: goto L_0x0079;
                case 1977196784: goto L_0x006d;
                case 2029784656: goto L_0x0061;
                case 2030379515: goto L_0x0055;
                case 2033393791: goto L_0x0049;
                case 2047190025: goto L_0x003d;
                case 2047252157: goto L_0x0031;
                case 2048319463: goto L_0x0025;
                default: goto L_0x0023;
            }     // Catch:{ all -> 0x05f7 }
        L_0x0023:
            goto L_0x05be
        L_0x0025:
            java.lang.String r1 = "HWVNS-H"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 51
            goto L_0x05bf
        L_0x0031:
            java.lang.String r1 = "ELUGA_Prim"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 26
            goto L_0x05bf
        L_0x003d:
            java.lang.String r1 = "ELUGA_Note"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 25
            goto L_0x05bf
        L_0x0049:
            java.lang.String r1 = "ASUS_X00AD_2"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 11
            goto L_0x05bf
        L_0x0055:
            java.lang.String r1 = "HWCAM-H"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 50
            goto L_0x05bf
        L_0x0061:
            java.lang.String r1 = "HWBLN-H"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 49
            goto L_0x05bf
        L_0x006d:
            java.lang.String r1 = "Infinix-X572"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 54
            goto L_0x05bf
        L_0x0079:
            java.lang.String r1 = "PB2-670M"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 82
            goto L_0x05bf
        L_0x0085:
            java.lang.String r1 = "santoni"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 98
            goto L_0x05bf
        L_0x0091:
            java.lang.String r1 = "iball8735_9806"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 53
            goto L_0x05bf
        L_0x009d:
            java.lang.String r1 = "CPH1609"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 18
            goto L_0x05bf
        L_0x00a9:
            java.lang.String r1 = "woods_f"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 114(0x72, float:1.6E-43)
            goto L_0x05bf
        L_0x00b5:
            java.lang.String r1 = "htc_e56ml_dtul"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 47
            goto L_0x05bf
        L_0x00c1:
            java.lang.String r1 = "EverStar_S"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 28
            goto L_0x05bf
        L_0x00cd:
            java.lang.String r1 = "hwALE-H"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 48
            goto L_0x05bf
        L_0x00d9:
            java.lang.String r1 = "itel_S41"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 56
            goto L_0x05bf
        L_0x00e5:
            java.lang.String r1 = "LS-5017"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 62
            goto L_0x05bf
        L_0x00f1:
            java.lang.String r1 = "panell_d"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 78
            goto L_0x05bf
        L_0x00fd:
            java.lang.String r1 = "j2xlteins"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 57
            goto L_0x05bf
        L_0x0109:
            java.lang.String r1 = "A7000plus"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 7
            goto L_0x05bf
        L_0x0114:
            java.lang.String r1 = "manning"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 64
            goto L_0x05bf
        L_0x0120:
            java.lang.String r1 = "GIONEE_WBL7519"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 45
            goto L_0x05bf
        L_0x012c:
            java.lang.String r1 = "GIONEE_WBL7365"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 44
            goto L_0x05bf
        L_0x0138:
            java.lang.String r1 = "GIONEE_WBL5708"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 43
            goto L_0x05bf
        L_0x0144:
            java.lang.String r1 = "QM16XE_U"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 96
            goto L_0x05bf
        L_0x0150:
            java.lang.String r1 = "Pixi5-10_4G"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 88
            goto L_0x05bf
        L_0x015c:
            java.lang.String r1 = "TB3-850M"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 106(0x6a, float:1.49E-43)
            goto L_0x05bf
        L_0x0168:
            java.lang.String r1 = "TB3-850F"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 105(0x69, float:1.47E-43)
            goto L_0x05bf
        L_0x0174:
            java.lang.String r1 = "TB3-730X"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 104(0x68, float:1.46E-43)
            goto L_0x05bf
        L_0x0180:
            java.lang.String r1 = "TB3-730F"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 103(0x67, float:1.44E-43)
            goto L_0x05bf
        L_0x018c:
            java.lang.String r1 = "A7020a48"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 9
            goto L_0x05bf
        L_0x0198:
            java.lang.String r1 = "A7010a48"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 8
            goto L_0x05bf
        L_0x01a4:
            java.lang.String r1 = "griffin"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 46
            goto L_0x05bf
        L_0x01b0:
            java.lang.String r1 = "marino_f"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 65
            goto L_0x05bf
        L_0x01bc:
            java.lang.String r1 = "CPY83_I00"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 19
            goto L_0x05bf
        L_0x01c8:
            java.lang.String r1 = "A2016a40"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 5
            goto L_0x05bf
        L_0x01d3:
            java.lang.String r1 = "le_x6"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 61
            goto L_0x05bf
        L_0x01df:
            java.lang.String r1 = "i9031"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 52
            goto L_0x05bf
        L_0x01eb:
            java.lang.String r1 = "X3_HK"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 116(0x74, float:1.63E-43)
            goto L_0x05bf
        L_0x01f7:
            java.lang.String r1 = "V23GB"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 109(0x6d, float:1.53E-43)
            goto L_0x05bf
        L_0x0203:
            java.lang.String r1 = "Q4310"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 94
            goto L_0x05bf
        L_0x020f:
            java.lang.String r1 = "Q4260"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 92
            goto L_0x05bf
        L_0x021b:
            java.lang.String r1 = "PRO7S"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 90
            goto L_0x05bf
        L_0x0227:
            java.lang.String r1 = "F3311"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 35
            goto L_0x05bf
        L_0x0233:
            java.lang.String r1 = "F3215"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 34
            goto L_0x05bf
        L_0x023f:
            java.lang.String r1 = "F3213"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 33
            goto L_0x05bf
        L_0x024b:
            java.lang.String r1 = "F3211"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 32
            goto L_0x05bf
        L_0x0257:
            java.lang.String r1 = "F3116"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 31
            goto L_0x05bf
        L_0x0263:
            java.lang.String r1 = "F3113"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 30
            goto L_0x05bf
        L_0x026f:
            java.lang.String r1 = "F3111"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 29
            goto L_0x05bf
        L_0x027b:
            java.lang.String r1 = "E5643"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 23
            goto L_0x05bf
        L_0x0287:
            java.lang.String r1 = "A1601"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 4
            goto L_0x05bf
        L_0x0292:
            java.lang.String r1 = "Aura_Note_2"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 12
            goto L_0x05bf
        L_0x029e:
            java.lang.String r1 = "MEIZU_M5"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 66
            goto L_0x05bf
        L_0x02aa:
            java.lang.String r1 = "p212"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 75
            goto L_0x05bf
        L_0x02b6:
            java.lang.String r1 = "mido"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 68
            goto L_0x05bf
        L_0x02c2:
            java.lang.String r1 = "kate"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 60
            goto L_0x05bf
        L_0x02ce:
            java.lang.String r1 = "XE2X"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 117(0x75, float:1.64E-43)
            goto L_0x05bf
        L_0x02da:
            java.lang.String r1 = "Q427"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 93
            goto L_0x05bf
        L_0x02e6:
            java.lang.String r1 = "Q350"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 91
            goto L_0x05bf
        L_0x02f2:
            java.lang.String r1 = "P681"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 76
            goto L_0x05bf
        L_0x02fe:
            java.lang.String r1 = "1714"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 2
            goto L_0x05bf
        L_0x0309:
            java.lang.String r1 = "1713"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 1
            goto L_0x05bf
        L_0x0314:
            java.lang.String r1 = "1601"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 0
            goto L_0x05bf
        L_0x031f:
            java.lang.String r1 = "flo"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 36
            goto L_0x05bf
        L_0x032b:
            java.lang.String r1 = "deb"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 22
            goto L_0x05bf
        L_0x0337:
            java.lang.String r1 = "cv3"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 21
            goto L_0x05bf
        L_0x0343:
            java.lang.String r1 = "cv1"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 20
            goto L_0x05bf
        L_0x034f:
            java.lang.String r1 = "Z80"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 120(0x78, float:1.68E-43)
            goto L_0x05bf
        L_0x035b:
            java.lang.String r1 = "QX1"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 97
            goto L_0x05bf
        L_0x0367:
            java.lang.String r1 = "PLE"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 89
            goto L_0x05bf
        L_0x0373:
            java.lang.String r1 = "P85"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 77
            goto L_0x05bf
        L_0x037f:
            java.lang.String r1 = "MX6"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 69
            goto L_0x05bf
        L_0x038b:
            java.lang.String r1 = "M5c"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 63
            goto L_0x05bf
        L_0x0397:
            java.lang.String r1 = "JGZ"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 58
            goto L_0x05bf
        L_0x03a3:
            java.lang.String r1 = "mh"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 67
            goto L_0x05bf
        L_0x03af:
            java.lang.String r1 = "V5"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 110(0x6e, float:1.54E-43)
            goto L_0x05bf
        L_0x03bb:
            java.lang.String r1 = "V1"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 108(0x6c, float:1.51E-43)
            goto L_0x05bf
        L_0x03c7:
            java.lang.String r1 = "Q5"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 95
            goto L_0x05bf
        L_0x03d3:
            java.lang.String r1 = "C1"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 15
            goto L_0x05bf
        L_0x03df:
            java.lang.String r1 = "woods_fn"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 115(0x73, float:1.61E-43)
            goto L_0x05bf
        L_0x03eb:
            java.lang.String r1 = "ELUGA_A3_Pro"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 24
            goto L_0x05bf
        L_0x03f7:
            java.lang.String r1 = "Z12_PRO"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 119(0x77, float:1.67E-43)
            goto L_0x05bf
        L_0x0403:
            java.lang.String r1 = "BLACK-1X"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 13
            goto L_0x05bf
        L_0x040f:
            java.lang.String r1 = "taido_row"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 102(0x66, float:1.43E-43)
            goto L_0x05bf
        L_0x041b:
            java.lang.String r1 = "Pixi4-7_3G"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 87
            goto L_0x05bf
        L_0x0427:
            java.lang.String r1 = "GIONEE_GBL7360"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 39
            goto L_0x05bf
        L_0x0433:
            java.lang.String r1 = "GiONEE_CBL7513"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 37
            goto L_0x05bf
        L_0x043f:
            java.lang.String r1 = "OnePlus5T"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 74
            goto L_0x05bf
        L_0x044b:
            java.lang.String r1 = "whyred"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 113(0x71, float:1.58E-43)
            goto L_0x05bf
        L_0x0457:
            java.lang.String r1 = "watson"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 112(0x70, float:1.57E-43)
            goto L_0x05bf
        L_0x0463:
            java.lang.String r1 = "SVP-DTV15"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 100
            goto L_0x05bf
        L_0x046f:
            java.lang.String r1 = "A7000-a"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 6
            goto L_0x05bf
        L_0x047a:
            java.lang.String r1 = "nicklaus_f"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 71
            goto L_0x05bf
        L_0x0486:
            java.lang.String r1 = "tcl_eu"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 107(0x6b, float:1.5E-43)
            goto L_0x05bf
        L_0x0492:
            java.lang.String r3 = "ELUGA_Ray_X"
            boolean r0 = r0.equals(r3)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            goto L_0x05bf
        L_0x049c:
            java.lang.String r1 = "s905x018"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 101(0x65, float:1.42E-43)
            goto L_0x05bf
        L_0x04a8:
            java.lang.String r1 = "A10-70F"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 3
            goto L_0x05bf
        L_0x04b3:
            java.lang.String r1 = "namath"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 70
            goto L_0x05bf
        L_0x04bf:
            java.lang.String r1 = "Slate_Pro"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 99
            goto L_0x05bf
        L_0x04cb:
            java.lang.String r1 = "iris60"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 55
            goto L_0x05bf
        L_0x04d7:
            java.lang.String r1 = "BRAVIA_ATV2"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 14
            goto L_0x05bf
        L_0x04e3:
            java.lang.String r1 = "GiONEE_GBL7319"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 38
            goto L_0x05bf
        L_0x04ef:
            java.lang.String r1 = "panell_dt"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 81
            goto L_0x05bf
        L_0x04fb:
            java.lang.String r1 = "panell_ds"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 80
            goto L_0x05bf
        L_0x0507:
            java.lang.String r1 = "panell_dl"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 79
            goto L_0x05bf
        L_0x0513:
            java.lang.String r1 = "vernee_M5"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 111(0x6f, float:1.56E-43)
            goto L_0x05bf
        L_0x051f:
            java.lang.String r1 = "Phantom6"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 86
            goto L_0x05bf
        L_0x052b:
            java.lang.String r1 = "ComioS1"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 16
            goto L_0x05bf
        L_0x0537:
            java.lang.String r1 = "XT1663"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 118(0x76, float:1.65E-43)
            goto L_0x05bf
        L_0x0543:
            java.lang.String r1 = "AquaPowerM"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 10
            goto L_0x05bf
        L_0x054f:
            java.lang.String r1 = "PGN611"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 85
            goto L_0x05bf
        L_0x055b:
            java.lang.String r1 = "PGN610"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 84
            goto L_0x05bf
        L_0x0566:
            java.lang.String r1 = "PGN528"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 83
            goto L_0x05bf
        L_0x0571:
            java.lang.String r1 = "NX573J"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 73
            goto L_0x05bf
        L_0x057c:
            java.lang.String r1 = "NX541J"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 72
            goto L_0x05bf
        L_0x0587:
            java.lang.String r1 = "CP8676_I02"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 17
            goto L_0x05bf
        L_0x0592:
            java.lang.String r1 = "K50a40"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 59
            goto L_0x05bf
        L_0x059d:
            java.lang.String r1 = "GIONEE_SWW1631"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 42
            goto L_0x05bf
        L_0x05a8:
            java.lang.String r1 = "GIONEE_SWW1627"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 41
            goto L_0x05bf
        L_0x05b3:
            java.lang.String r1 = "GIONEE_SWW1609"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05be
            r1 = 40
            goto L_0x05bf
        L_0x05be:
            r1 = -1
        L_0x05bf:
            switch(r1) {
                case 0: goto L_0x05c3;
                case 1: goto L_0x05c3;
                case 2: goto L_0x05c3;
                case 3: goto L_0x05c3;
                case 4: goto L_0x05c3;
                case 5: goto L_0x05c3;
                case 6: goto L_0x05c3;
                case 7: goto L_0x05c3;
                case 8: goto L_0x05c3;
                case 9: goto L_0x05c3;
                case 10: goto L_0x05c3;
                case 11: goto L_0x05c3;
                case 12: goto L_0x05c3;
                case 13: goto L_0x05c3;
                case 14: goto L_0x05c3;
                case 15: goto L_0x05c3;
                case 16: goto L_0x05c3;
                case 17: goto L_0x05c3;
                case 18: goto L_0x05c3;
                case 19: goto L_0x05c3;
                case 20: goto L_0x05c3;
                case 21: goto L_0x05c3;
                case 22: goto L_0x05c3;
                case 23: goto L_0x05c3;
                case 24: goto L_0x05c3;
                case 25: goto L_0x05c3;
                case 26: goto L_0x05c3;
                case 27: goto L_0x05c3;
                case 28: goto L_0x05c3;
                case 29: goto L_0x05c3;
                case 30: goto L_0x05c3;
                case 31: goto L_0x05c3;
                case 32: goto L_0x05c3;
                case 33: goto L_0x05c3;
                case 34: goto L_0x05c3;
                case 35: goto L_0x05c3;
                case 36: goto L_0x05c3;
                case 37: goto L_0x05c3;
                case 38: goto L_0x05c3;
                case 39: goto L_0x05c3;
                case 40: goto L_0x05c3;
                case 41: goto L_0x05c3;
                case 42: goto L_0x05c3;
                case 43: goto L_0x05c3;
                case 44: goto L_0x05c3;
                case 45: goto L_0x05c3;
                case 46: goto L_0x05c3;
                case 47: goto L_0x05c3;
                case 48: goto L_0x05c3;
                case 49: goto L_0x05c3;
                case 50: goto L_0x05c3;
                case 51: goto L_0x05c3;
                case 52: goto L_0x05c3;
                case 53: goto L_0x05c3;
                case 54: goto L_0x05c3;
                case 55: goto L_0x05c3;
                case 56: goto L_0x05c3;
                case 57: goto L_0x05c3;
                case 58: goto L_0x05c3;
                case 59: goto L_0x05c3;
                case 60: goto L_0x05c3;
                case 61: goto L_0x05c3;
                case 62: goto L_0x05c3;
                case 63: goto L_0x05c3;
                case 64: goto L_0x05c3;
                case 65: goto L_0x05c3;
                case 66: goto L_0x05c3;
                case 67: goto L_0x05c3;
                case 68: goto L_0x05c3;
                case 69: goto L_0x05c3;
                case 70: goto L_0x05c3;
                case 71: goto L_0x05c3;
                case 72: goto L_0x05c3;
                case 73: goto L_0x05c3;
                case 74: goto L_0x05c3;
                case 75: goto L_0x05c3;
                case 76: goto L_0x05c3;
                case 77: goto L_0x05c3;
                case 78: goto L_0x05c3;
                case 79: goto L_0x05c3;
                case 80: goto L_0x05c3;
                case 81: goto L_0x05c3;
                case 82: goto L_0x05c3;
                case 83: goto L_0x05c3;
                case 84: goto L_0x05c3;
                case 85: goto L_0x05c3;
                case 86: goto L_0x05c3;
                case 87: goto L_0x05c3;
                case 88: goto L_0x05c3;
                case 89: goto L_0x05c3;
                case 90: goto L_0x05c3;
                case 91: goto L_0x05c3;
                case 92: goto L_0x05c3;
                case 93: goto L_0x05c3;
                case 94: goto L_0x05c3;
                case 95: goto L_0x05c3;
                case 96: goto L_0x05c3;
                case 97: goto L_0x05c3;
                case 98: goto L_0x05c3;
                case 99: goto L_0x05c3;
                case 100: goto L_0x05c3;
                case 101: goto L_0x05c3;
                case 102: goto L_0x05c3;
                case 103: goto L_0x05c3;
                case 104: goto L_0x05c3;
                case 105: goto L_0x05c3;
                case 106: goto L_0x05c3;
                case 107: goto L_0x05c3;
                case 108: goto L_0x05c3;
                case 109: goto L_0x05c3;
                case 110: goto L_0x05c3;
                case 111: goto L_0x05c3;
                case 112: goto L_0x05c3;
                case 113: goto L_0x05c3;
                case 114: goto L_0x05c3;
                case 115: goto L_0x05c3;
                case 116: goto L_0x05c3;
                case 117: goto L_0x05c3;
                case 118: goto L_0x05c3;
                case 119: goto L_0x05c3;
                case 120: goto L_0x05c3;
                default: goto L_0x05c2;
            }     // Catch:{ all -> 0x05f7 }
        L_0x05c2:
            goto L_0x05c5
        L_0x05c3:
            Y0 = r5     // Catch:{ all -> 0x05f7 }
        L_0x05c5:
            java.lang.String r0 = com.google.android.exoplayer2.util.Util.d     // Catch:{ all -> 0x05f7 }
            int r1 = r0.hashCode()     // Catch:{ all -> 0x05f7 }
            r3 = 2006354(0x1e9d52, float:2.811501E-39)
            if (r1 == r3) goto L_0x05e0
            r2 = 2006367(0x1e9d5f, float:2.811519E-39)
            if (r1 == r2) goto L_0x05d6
            goto L_0x05e9
        L_0x05d6:
            java.lang.String r1 = "AFTN"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05e9
            r2 = 1
            goto L_0x05ea
        L_0x05e0:
            java.lang.String r1 = "AFTA"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x05f7 }
            if (r0 == 0) goto L_0x05e9
            goto L_0x05ea
        L_0x05e9:
            r2 = -1
        L_0x05ea:
            if (r2 == 0) goto L_0x05ef
            if (r2 == r5) goto L_0x05ef
            goto L_0x05f1
        L_0x05ef:
            Y0 = r5     // Catch:{ all -> 0x05f7 }
        L_0x05f1:
            X0 = r5     // Catch:{ all -> 0x05f7 }
        L_0x05f3:
            monitor-exit(r7)     // Catch:{ all -> 0x05f7 }
            boolean r7 = Y0
            return r7
        L_0x05f7:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x05f7 }
            throw r0
        L_0x05fa:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.video.MediaCodecVideoRenderer.a(java.lang.String):boolean");
    }
}
