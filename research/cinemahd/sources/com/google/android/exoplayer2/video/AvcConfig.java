package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.ArrayList;
import java.util.List;

public final class AvcConfig {

    /* renamed from: a  reason: collision with root package name */
    public final List<byte[]> f3656a;
    public final int b;
    public final int c;
    public final int d;
    public final float e;

    private AvcConfig(List<byte[]> list, int i, int i2, int i3, float f) {
        this.f3656a = list;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = f;
    }

    private static byte[] a(ParsableByteArray parsableByteArray) {
        int z = parsableByteArray.z();
        int c2 = parsableByteArray.c();
        parsableByteArray.f(z);
        return CodecSpecificDataUtil.a(parsableByteArray.f3641a, c2, z);
    }

    public static AvcConfig b(ParsableByteArray parsableByteArray) throws ParserException {
        float f;
        int i;
        int i2;
        try {
            parsableByteArray.f(4);
            int t = (parsableByteArray.t() & 3) + 1;
            if (t != 3) {
                ArrayList arrayList = new ArrayList();
                int t2 = parsableByteArray.t() & 31;
                for (int i3 = 0; i3 < t2; i3++) {
                    arrayList.add(a(parsableByteArray));
                }
                int t3 = parsableByteArray.t();
                for (int i4 = 0; i4 < t3; i4++) {
                    arrayList.add(a(parsableByteArray));
                }
                if (t2 > 0) {
                    NalUnitUtil.SpsData c2 = NalUnitUtil.c((byte[]) arrayList.get(0), t, ((byte[]) arrayList.get(0)).length);
                    int i5 = c2.e;
                    int i6 = c2.f;
                    f = c2.g;
                    i2 = i5;
                    i = i6;
                } else {
                    i2 = -1;
                    i = -1;
                    f = 1.0f;
                }
                return new AvcConfig(arrayList, t, i2, i, f);
            }
            throw new IllegalStateException();
        } catch (ArrayIndexOutOfBoundsException e2) {
            throw new ParserException("Error parsing AVC config", e2);
        }
    }
}
