package com.google.android.exoplayer2.video;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.Surface;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.EGLSurfaceTexture;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;

@TargetApi(17)
public final class DummySurface extends Surface {
    private static int c;
    private static boolean d;

    /* renamed from: a  reason: collision with root package name */
    private final DummySurfaceThread f3658a;
    private boolean b;

    public static DummySurface a(Context context, boolean z) {
        a();
        int i = 0;
        Assertions.b(!z || b(context));
        DummySurfaceThread dummySurfaceThread = new DummySurfaceThread();
        if (z) {
            i = c;
        }
        return dummySurfaceThread.a(i);
    }

    public static synchronized boolean b(Context context) {
        boolean z;
        synchronized (DummySurface.class) {
            z = true;
            if (!d) {
                c = Util.f3651a < 24 ? 0 : a(context);
                d = true;
            }
            if (c == 0) {
                z = false;
            }
        }
        return z;
    }

    public void release() {
        super.release();
        synchronized (this.f3658a) {
            if (!this.b) {
                this.f3658a.a();
                this.b = true;
            }
        }
    }

    private DummySurface(DummySurfaceThread dummySurfaceThread, SurfaceTexture surfaceTexture, boolean z) {
        super(surfaceTexture);
        this.f3658a = dummySurfaceThread;
    }

    private static class DummySurfaceThread extends HandlerThread implements Handler.Callback {

        /* renamed from: a  reason: collision with root package name */
        private EGLSurfaceTexture f3659a;
        private Handler b;
        private Error c;
        private RuntimeException d;
        private DummySurface e;

        public DummySurfaceThread() {
            super("dummySurface");
        }

        private void b(int i) {
            Assertions.a(this.f3659a);
            this.f3659a.a(i);
            this.e = new DummySurface(this, this.f3659a.a(), i != 0);
        }

        public DummySurface a(int i) {
            boolean z;
            start();
            this.b = new Handler(getLooper(), this);
            this.f3659a = new EGLSurfaceTexture(this.b);
            synchronized (this) {
                z = false;
                this.b.obtainMessage(1, i, 0).sendToTarget();
                while (this.e == null && this.d == null && this.c == null) {
                    try {
                        wait();
                    } catch (InterruptedException unused) {
                        z = true;
                    }
                }
            }
            if (z) {
                Thread.currentThread().interrupt();
            }
            RuntimeException runtimeException = this.d;
            if (runtimeException == null) {
                Error error = this.c;
                if (error == null) {
                    DummySurface dummySurface = this.e;
                    Assertions.a(dummySurface);
                    return dummySurface;
                }
                throw error;
            }
            throw runtimeException;
        }

        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                try {
                    b(message.arg1);
                    synchronized (this) {
                        notify();
                    }
                } catch (RuntimeException e2) {
                    Log.a("DummySurface", "Failed to initialize dummy surface", e2);
                    this.d = e2;
                    synchronized (this) {
                        notify();
                    }
                } catch (Error e3) {
                    try {
                        Log.a("DummySurface", "Failed to initialize dummy surface", e3);
                        this.c = e3;
                        synchronized (this) {
                            notify();
                        }
                    } catch (Throwable th) {
                        synchronized (this) {
                            notify();
                            throw th;
                        }
                    }
                }
                return true;
            } else if (i != 2) {
                return true;
            } else {
                try {
                    b();
                } catch (Throwable th2) {
                    quit();
                    throw th2;
                }
                quit();
                return true;
            }
        }

        private void b() {
            Assertions.a(this.f3659a);
            this.f3659a.b();
        }

        public void a() {
            Assertions.a(this.b);
            this.b.sendEmptyMessage(2);
        }
    }

    private static void a() {
        if (Util.f3651a < 17) {
            throw new UnsupportedOperationException("Unsupported prior to API level 17");
        }
    }

    @TargetApi(24)
    private static int a(Context context) {
        String eglQueryString;
        if (Util.f3651a < 26 && ("samsung".equals(Util.c) || "XT1650".equals(Util.d))) {
            return 0;
        }
        if ((Util.f3651a >= 26 || context.getPackageManager().hasSystemFeature("android.hardware.vr.high_performance")) && (eglQueryString = EGL14.eglQueryString(EGL14.eglGetDisplay(0), 12373)) != null && eglQueryString.contains("EGL_EXT_protected_content")) {
            return eglQueryString.contains("EGL_KHR_surfaceless_context") ? 1 : 2;
        }
        return 0;
    }
}
