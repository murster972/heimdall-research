package com.google.android.exoplayer2.text.webvtt;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class WebvttParserUtil {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f3553a = Pattern.compile("^NOTE(( |\t).*)?$");

    private WebvttParserUtil() {
    }

    public static float a(String str) throws NumberFormatException {
        if (str.endsWith("%")) {
            return Float.parseFloat(str.substring(0, str.length() - 1)) / 100.0f;
        }
        throw new NumberFormatException("Percentages must end with %");
    }

    public static boolean b(ParsableByteArray parsableByteArray) {
        String j = parsableByteArray.j();
        return j != null && j.startsWith("WEBVTT");
    }

    public static void c(ParsableByteArray parsableByteArray) throws ParserException {
        int c = parsableByteArray.c();
        if (!b(parsableByteArray)) {
            parsableByteArray.e(c);
            throw new ParserException("Expected WEBVTT. Got " + parsableByteArray.j());
        }
    }

    public static long b(String str) throws NumberFormatException {
        String[] b = Util.b(str, "\\.");
        long j = 0;
        for (String parseLong : Util.a(b[0], ":")) {
            j = (j * 60) + Long.parseLong(parseLong);
        }
        long j2 = j * 1000;
        if (b.length == 2) {
            j2 += Long.parseLong(b[1]);
        }
        return j2 * 1000;
    }

    public static Matcher a(ParsableByteArray parsableByteArray) {
        String j;
        while (true) {
            String j2 = parsableByteArray.j();
            if (j2 == null) {
                return null;
            }
            if (f3553a.matcher(j2).matches()) {
                do {
                    j = parsableByteArray.j();
                    if (j == null) {
                        break;
                    }
                } while (j.isEmpty());
            } else {
                Matcher matcher = WebvttCueParser.b.matcher(j2);
                if (matcher.matches()) {
                    return matcher;
                }
            }
        }
    }
}
