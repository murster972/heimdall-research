package com.google.android.exoplayer2.text.cea;

import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.text.SubtitleInputBuffer;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.original.tase.model.socket.UserResponces;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import okhttp3.internal.ws.WebSocketProtocol;

public final class Cea608Decoder extends CeaDecoder {
    private static final int[] s = {11, 1, 3, 12, 14, 5, 7, 9};
    private static final int[] t = {0, 4, 8, 12, 16, 20, 24, 28};
    /* access modifiers changed from: private */
    public static final int[] u = {-1, -16711936, -16776961, -16711681, -65536, -256, -65281};
    private static final int[] v = {32, 33, 34, 35, 36, 37, 38, 39, 40, 41, JfifUtil.MARKER_APP1, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 233, 93, 237, 243, 250, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 231, 247, 209, 241, 9632};
    private static final int[] w = {174, 176, 189, 191, 8482, 162, 163, 9834, 224, 32, 232, 226, 234, 238, 244, 251};
    private static final int[] x = {193, 201, 211, JfifUtil.MARKER_SOS, 220, 252, 8216, 161, 42, 39, 8212, 169, 8480, 8226, 8220, 8221, JfifUtil.MARKER_SOFn, 194, 199, UserResponces.USER_RESPONCE_SUCCSES, 202, 203, 235, 206, 207, 239, 212, JfifUtil.MARKER_EOI, 249, 219, 171, 187};
    private static final int[] y = {195, 227, 205, 204, 236, 210, 242, 213, 245, 123, 125, 92, 94, 95, 124, WebSocketProtocol.PAYLOAD_SHORT, 196, 228, 214, 246, 223, 165, 164, 9474, 197, 229, JfifUtil.MARKER_SOI, 248, 9484, 9488, 9492, 9496};
    private final ParsableByteArray g = new ParsableByteArray();
    private final int h;
    private final int i;
    private final ArrayList<CueBuilder> j = new ArrayList<>();
    private CueBuilder k = new CueBuilder(0, 4);
    private List<Cue> l;
    private List<Cue> m;
    private int n;
    private int o;
    private boolean p;
    private byte q;
    private byte r;

    private static class CueBuilder {

        /* renamed from: a  reason: collision with root package name */
        private final List<CueStyle> f3517a = new ArrayList();
        private final List<SpannableString> b = new ArrayList();
        private final StringBuilder c = new StringBuilder();
        private int d;
        private int e;
        private int f;
        private int g;
        private int h;

        private static class CueStyle {

            /* renamed from: a  reason: collision with root package name */
            public final int f3518a;
            public final boolean b;
            public int c;

            public CueStyle(int i, boolean z, int i2) {
                this.f3518a = i;
                this.b = z;
                this.c = i2;
            }
        }

        public CueBuilder(int i, int i2) {
            a(i);
            b(i2);
        }

        public void a(int i) {
            this.g = i;
            this.f3517a.clear();
            this.b.clear();
            this.c.setLength(0);
            this.d = 15;
            this.e = 0;
            this.f = 0;
        }

        public void b(int i) {
            this.h = i;
        }

        public void c(int i) {
            this.e = i;
        }

        public int d() {
            return this.d;
        }

        public boolean e() {
            return this.f3517a.isEmpty() && this.b.isEmpty() && this.c.length() == 0;
        }

        public void f() {
            this.b.add(c());
            this.c.setLength(0);
            this.f3517a.clear();
            int min = Math.min(this.h, this.d);
            while (this.b.size() >= min) {
                this.b.remove(0);
            }
        }

        public String toString() {
            return this.c.toString();
        }

        public Cue b() {
            int i;
            float f2;
            int i2;
            int i3;
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            for (int i4 = 0; i4 < this.b.size(); i4++) {
                spannableStringBuilder.append(this.b.get(i4));
                spannableStringBuilder.append(10);
            }
            spannableStringBuilder.append(c());
            if (spannableStringBuilder.length() == 0) {
                return null;
            }
            int i5 = this.e + this.f;
            int length = (32 - i5) - spannableStringBuilder.length();
            int i6 = i5 - length;
            if (this.g == 2 && (Math.abs(i6) < 3 || length < 0)) {
                f2 = 0.5f;
                i = 1;
            } else if (this.g != 2 || i6 <= 0) {
                f2 = ((((float) i5) / 32.0f) * 0.8f) + 0.1f;
                i = 0;
            } else {
                f2 = ((((float) (32 - length)) / 32.0f) * 0.8f) + 0.1f;
                i = 2;
            }
            if (this.g == 1 || (i3 = this.d) > 7) {
                i3 = (this.d - 15) - 2;
                i2 = 2;
            } else {
                i2 = 0;
            }
            return new Cue(spannableStringBuilder, Layout.Alignment.ALIGN_NORMAL, (float) i3, 1, i2, f2, i, Float.MIN_VALUE);
        }

        public SpannableString c() {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.c);
            int length = spannableStringBuilder.length();
            int i = 0;
            int i2 = -1;
            int i3 = -1;
            int i4 = 0;
            int i5 = -1;
            int i6 = -1;
            boolean z = false;
            while (i < this.f3517a.size()) {
                CueStyle cueStyle = this.f3517a.get(i);
                boolean z2 = cueStyle.b;
                int i7 = cueStyle.f3518a;
                if (i7 != 8) {
                    boolean z3 = i7 == 7;
                    if (i7 != 7) {
                        i6 = Cea608Decoder.u[i7];
                    }
                    z = z3;
                }
                int i8 = cueStyle.c;
                i++;
                if (i8 != (i < this.f3517a.size() ? this.f3517a.get(i).c : length)) {
                    if (i2 != -1 && !z2) {
                        b(spannableStringBuilder, i2, i8);
                        i2 = -1;
                    } else if (i2 == -1 && z2) {
                        i2 = i8;
                    }
                    if (i3 != -1 && !z) {
                        a(spannableStringBuilder, i3, i8);
                        i3 = -1;
                    } else if (i3 == -1 && z) {
                        i3 = i8;
                    }
                    if (i6 != i5) {
                        a(spannableStringBuilder, i4, i8, i5);
                        i5 = i6;
                        i4 = i8;
                    }
                }
            }
            if (!(i2 == -1 || i2 == length)) {
                b(spannableStringBuilder, i2, length);
            }
            if (!(i3 == -1 || i3 == length)) {
                a(spannableStringBuilder, i3, length);
            }
            if (i4 != length) {
                a(spannableStringBuilder, i4, length, i5);
            }
            return new SpannableString(spannableStringBuilder);
        }

        public void d(int i) {
            this.d = i;
        }

        public void e(int i) {
            this.f = i;
        }

        public void a() {
            int length = this.c.length();
            if (length > 0) {
                this.c.delete(length - 1, length);
                int size = this.f3517a.size() - 1;
                while (size >= 0) {
                    CueStyle cueStyle = this.f3517a.get(size);
                    int i = cueStyle.c;
                    if (i == length) {
                        cueStyle.c = i - 1;
                        size--;
                    } else {
                        return;
                    }
                }
            }
        }

        public void a(int i, boolean z) {
            this.f3517a.add(new CueStyle(i, z, this.c.length()));
        }

        private static void b(SpannableStringBuilder spannableStringBuilder, int i, int i2) {
            spannableStringBuilder.setSpan(new UnderlineSpan(), i, i2, 33);
        }

        public void a(char c2) {
            this.c.append(c2);
        }

        private static void a(SpannableStringBuilder spannableStringBuilder, int i, int i2) {
            spannableStringBuilder.setSpan(new StyleSpan(2), i, i2, 33);
        }

        private static void a(SpannableStringBuilder spannableStringBuilder, int i, int i2, int i3) {
            if (i3 != -1) {
                spannableStringBuilder.setSpan(new ForegroundColorSpan(i3), i, i2, 33);
            }
        }
    }

    public Cea608Decoder(String str, int i2) {
        this.h = "application/x-mp4-cea-608".equals(str) ? 2 : 3;
        if (i2 == 3 || i2 == 4) {
            this.i = 2;
        } else {
            this.i = 1;
        }
        a(0);
        g();
    }

    private static boolean c(byte b, byte b2) {
        return (b & 247) == 17 && (b2 & 240) == 32;
    }

    private static boolean d(byte b, byte b2) {
        return (b & 247) == 20 && (b2 & 240) == 32;
    }

    private static boolean e(byte b, byte b2) {
        return (b & 240) == 16 && (b2 & 192) == 64;
    }

    private void f(byte b) {
        if (b == 32) {
            a(2);
        } else if (b != 41) {
            switch (b) {
                case 37:
                    a(1);
                    b(2);
                    return;
                case 38:
                    a(1);
                    b(3);
                    return;
                case 39:
                    a(1);
                    b(4);
                    return;
                default:
                    int i2 = this.n;
                    if (i2 != 0) {
                        if (b == 33) {
                            this.k.a();
                            return;
                        } else if (b != 36) {
                            switch (b) {
                                case 44:
                                    this.l = Collections.emptyList();
                                    int i3 = this.n;
                                    if (i3 == 1 || i3 == 3) {
                                        g();
                                        return;
                                    }
                                    return;
                                case 45:
                                    if (i2 == 1 && !this.k.e()) {
                                        this.k.f();
                                        return;
                                    }
                                    return;
                                case 46:
                                    g();
                                    return;
                                case 47:
                                    this.l = f();
                                    g();
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
            }
        } else {
            a(3);
        }
    }

    private static boolean f(byte b, byte b2) {
        return (b & 247) == 23 && b2 >= 33 && b2 <= 35;
    }

    private void g() {
        this.k.a(this.n);
        this.j.clear();
        this.j.add(this.k);
    }

    private static boolean g(byte b) {
        return (b & 240) == 16;
    }

    /* access modifiers changed from: protected */
    public Subtitle c() {
        List<Cue> list = this.l;
        this.m = list;
        return new CeaSubtitle(list);
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return this.l != this.m;
    }

    public void flush() {
        super.flush();
        this.l = null;
        this.m = null;
        a(0);
        b(4);
        g();
        this.p = false;
        this.q = 0;
        this.r = 0;
    }

    public void release() {
    }

    private static char d(byte b) {
        return (char) w[b & 15];
    }

    private void e(byte b) {
        this.k.a(' ');
        this.k.a((b >> 1) & 7, (b & 1) == 1);
    }

    private void b(byte b, byte b2) {
        int i2 = s[b & 7];
        boolean z = false;
        if ((b2 & 32) != 0) {
            i2++;
        }
        if (i2 != this.k.d()) {
            if (this.n != 1 && !this.k.e()) {
                this.k = new CueBuilder(this.n, this.o);
                this.j.add(this.k);
            }
            this.k.d(i2);
        }
        boolean z2 = (b2 & 16) == 16;
        if ((b2 & 1) == 1) {
            z = true;
        }
        int i3 = (b2 >> 1) & 7;
        this.k.a(z2 ? 8 : i3, z);
        if (z2) {
            this.k.c(t[i3]);
        }
    }

    private static char c(byte b) {
        return (char) y[b & 31];
    }

    /* access modifiers changed from: protected */
    public void a(SubtitleInputBuffer subtitleInputBuffer) {
        byte b;
        this.g.a(subtitleInputBuffer.b.array(), subtitleInputBuffer.b.limit());
        boolean z = false;
        boolean z2 = false;
        while (true) {
            int a2 = this.g.a();
            int i2 = this.h;
            if (a2 < i2) {
                break;
            }
            if (i2 == 2) {
                b = -4;
            } else {
                b = (byte) this.g.t();
            }
            byte t2 = (byte) (this.g.t() & 127);
            byte t3 = (byte) (this.g.t() & 127);
            if ((b & 6) == 4 && ((this.i != 1 || (b & 1) == 0) && ((this.i != 2 || (b & 1) == 1) && !(t2 == 0 && t3 == 0)))) {
                if ((t2 & 247) == 17 && (t3 & 240) == 48) {
                    this.k.a(d(t3));
                } else if ((t2 & 246) == 18 && (t3 & 224) == 32) {
                    this.k.a();
                    if ((t2 & 1) == 0) {
                        this.k.a(b(t3));
                    } else {
                        this.k.a(c(t3));
                    }
                } else if ((t2 & 224) == 0) {
                    z2 = a(t2, t3);
                } else {
                    this.k.a(a(t2));
                    if ((t3 & 224) != 0) {
                        this.k.a(a(t3));
                    }
                }
                z = true;
            }
        }
        if (z) {
            if (!z2) {
                this.p = false;
            }
            int i3 = this.n;
            if (i3 == 1 || i3 == 3) {
                this.l = f();
            }
        }
    }

    private void b(int i2) {
        this.o = i2;
        this.k.b(i2);
    }

    private static char b(byte b) {
        return (char) x[b & 31];
    }

    private List<Cue> f() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < this.j.size(); i2++) {
            Cue b = this.j.get(i2).b();
            if (b != null) {
                arrayList.add(b);
            }
        }
        return arrayList;
    }

    private boolean a(byte b, byte b2) {
        boolean g2 = g(b);
        if (g2) {
            if (this.p && this.q == b && this.r == b2) {
                this.p = false;
                return true;
            }
            this.p = true;
            this.q = b;
            this.r = b2;
        }
        if (c(b, b2)) {
            e(b2);
        } else if (e(b, b2)) {
            b(b, b2);
        } else if (f(b, b2)) {
            this.k.e(b2 - 32);
        } else if (d(b, b2)) {
            f(b2);
        }
        return g2;
    }

    private void a(int i2) {
        int i3 = this.n;
        if (i3 != i2) {
            this.n = i2;
            g();
            if (i3 == 3 || i2 == 1 || i2 == 0) {
                this.l = Collections.emptyList();
            }
        }
    }

    private static char a(byte b) {
        return (char) v[(b & Byte.MAX_VALUE) - 32];
    }
}
