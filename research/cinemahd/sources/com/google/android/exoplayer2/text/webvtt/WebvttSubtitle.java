package com.google.android.exoplayer2.text.webvtt;

import android.text.SpannableStringBuilder;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class WebvttSubtitle implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private final List<WebvttCue> f3554a;
    private final int b;
    private final long[] c = new long[(this.b * 2)];
    private final long[] d;

    public WebvttSubtitle(List<WebvttCue> list) {
        this.f3554a = list;
        this.b = list.size();
        for (int i = 0; i < this.b; i++) {
            WebvttCue webvttCue = list.get(i);
            int i2 = i * 2;
            long[] jArr = this.c;
            jArr[i2] = webvttCue.m;
            jArr[i2 + 1] = webvttCue.n;
        }
        long[] jArr2 = this.c;
        this.d = Arrays.copyOf(jArr2, jArr2.length);
        Arrays.sort(this.d);
    }

    public int a(long j) {
        int a2 = Util.a(this.d, j, false, false);
        if (a2 < this.d.length) {
            return a2;
        }
        return -1;
    }

    public List<Cue> b(long j) {
        SpannableStringBuilder spannableStringBuilder = null;
        WebvttCue webvttCue = null;
        ArrayList arrayList = null;
        for (int i = 0; i < this.b; i++) {
            long[] jArr = this.c;
            int i2 = i * 2;
            if (jArr[i2] <= j && j < jArr[i2 + 1]) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                WebvttCue webvttCue2 = this.f3554a.get(i);
                if (!webvttCue2.a()) {
                    arrayList.add(webvttCue2);
                } else if (webvttCue == null) {
                    webvttCue = webvttCue2;
                } else if (spannableStringBuilder == null) {
                    spannableStringBuilder = new SpannableStringBuilder();
                    spannableStringBuilder.append(webvttCue.f3514a).append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE).append(webvttCue2.f3514a);
                } else {
                    spannableStringBuilder.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE).append(webvttCue2.f3514a);
                }
            }
        }
        if (spannableStringBuilder != null) {
            arrayList.add(new WebvttCue(spannableStringBuilder));
        } else if (webvttCue != null) {
            arrayList.add(webvttCue);
        }
        if (arrayList != null) {
            return arrayList;
        }
        return Collections.emptyList();
    }

    public int a() {
        return this.d.length;
    }

    public long a(int i) {
        boolean z = true;
        Assertions.a(i >= 0);
        if (i >= this.d.length) {
            z = false;
        }
        Assertions.a(z);
        return this.d[i];
    }
}
