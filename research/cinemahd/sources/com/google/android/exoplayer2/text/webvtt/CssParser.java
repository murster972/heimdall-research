package com.google.android.exoplayer2.text.webvtt;

import android.text.TextUtils;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.exoplayer2.util.ColorParser;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class CssParser {
    private static final Pattern c = Pattern.compile("\\[voice=\"([^\"]*)\"\\]");

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3545a = new ParsableByteArray();
    private final StringBuilder b = new StringBuilder();

    static String b(ParsableByteArray parsableByteArray, StringBuilder sb) {
        f(parsableByteArray);
        if (parsableByteArray.a() == 0) {
            return null;
        }
        String a2 = a(parsableByteArray, sb);
        if (!"".equals(a2)) {
            return a2;
        }
        return "" + ((char) parsableByteArray.t());
    }

    private static boolean c(ParsableByteArray parsableByteArray) {
        char a2 = a(parsableByteArray, parsableByteArray.c());
        if (a2 != 9 && a2 != 10 && a2 != 12 && a2 != 13 && a2 != ' ') {
            return false;
        }
        parsableByteArray.f(1);
        return true;
    }

    private static String d(ParsableByteArray parsableByteArray, StringBuilder sb) {
        f(parsableByteArray);
        if (parsableByteArray.a() < 5 || !"::cue".equals(parsableByteArray.b(5))) {
            return null;
        }
        int c2 = parsableByteArray.c();
        String b2 = b(parsableByteArray, sb);
        if (b2 == null) {
            return null;
        }
        if ("{".equals(b2)) {
            parsableByteArray.e(c2);
            return "";
        }
        String d = "(".equals(b2) ? d(parsableByteArray) : null;
        String b3 = b(parsableByteArray, sb);
        if (!")".equals(b3) || b3 == null) {
            return null;
        }
        return d;
    }

    static void e(ParsableByteArray parsableByteArray) {
        do {
        } while (!TextUtils.isEmpty(parsableByteArray.j()));
    }

    static void f(ParsableByteArray parsableByteArray) {
        while (true) {
            boolean z = true;
            while (parsableByteArray.a() > 0 && z) {
                if (!c(parsableByteArray) && !b(parsableByteArray)) {
                    z = false;
                }
            }
            return;
        }
    }

    public WebvttCssStyle a(ParsableByteArray parsableByteArray) {
        this.b.setLength(0);
        int c2 = parsableByteArray.c();
        e(parsableByteArray);
        this.f3545a.a(parsableByteArray.f3641a, parsableByteArray.c());
        this.f3545a.e(c2);
        String d = d(this.f3545a, this.b);
        if (d == null || !"{".equals(b(this.f3545a, this.b))) {
            return null;
        }
        WebvttCssStyle webvttCssStyle = new WebvttCssStyle();
        a(webvttCssStyle, d);
        String str = null;
        boolean z = false;
        while (!z) {
            int c3 = this.f3545a.c();
            str = b(this.f3545a, this.b);
            boolean z2 = str == null || "}".equals(str);
            if (!z2) {
                this.f3545a.e(c3);
                a(this.f3545a, webvttCssStyle, this.b);
            }
            z = z2;
        }
        if ("}".equals(str)) {
            return webvttCssStyle;
        }
        return null;
    }

    private static String c(ParsableByteArray parsableByteArray, StringBuilder sb) {
        StringBuilder sb2 = new StringBuilder();
        boolean z = false;
        while (!z) {
            int c2 = parsableByteArray.c();
            String b2 = b(parsableByteArray, sb);
            if (b2 == null) {
                return null;
            }
            if ("}".equals(b2) || ";".equals(b2)) {
                parsableByteArray.e(c2);
                z = true;
            } else {
                sb2.append(b2);
            }
        }
        return sb2.toString();
    }

    private static boolean b(ParsableByteArray parsableByteArray) {
        int c2 = parsableByteArray.c();
        int d = parsableByteArray.d();
        byte[] bArr = parsableByteArray.f3641a;
        if (c2 + 2 > d) {
            return false;
        }
        int i = c2 + 1;
        if (bArr[c2] != 47) {
            return false;
        }
        int i2 = i + 1;
        if (bArr[i] != 42) {
            return false;
        }
        while (true) {
            int i3 = i2 + 1;
            if (i3 >= d) {
                parsableByteArray.f(d - parsableByteArray.c());
                return true;
            } else if (((char) bArr[i2]) == '*' && ((char) bArr[i3]) == '/') {
                i2 = i3 + 1;
                d = i2;
            } else {
                i2 = i3;
            }
        }
    }

    private static String d(ParsableByteArray parsableByteArray) {
        int c2 = parsableByteArray.c();
        int d = parsableByteArray.d();
        boolean z = false;
        while (c2 < d && !z) {
            int i = c2 + 1;
            z = ((char) parsableByteArray.f3641a[c2]) == ')';
            c2 = i;
        }
        return parsableByteArray.b((c2 - 1) - parsableByteArray.c()).trim();
    }

    private static void a(ParsableByteArray parsableByteArray, WebvttCssStyle webvttCssStyle, StringBuilder sb) {
        f(parsableByteArray);
        String a2 = a(parsableByteArray, sb);
        if (!"".equals(a2) && ":".equals(b(parsableByteArray, sb))) {
            f(parsableByteArray);
            String c2 = c(parsableByteArray, sb);
            if (c2 != null && !"".equals(c2)) {
                int c3 = parsableByteArray.c();
                String b2 = b(parsableByteArray, sb);
                if (!";".equals(b2)) {
                    if ("}".equals(b2)) {
                        parsableByteArray.e(c3);
                    } else {
                        return;
                    }
                }
                if (ViewProps.COLOR.equals(a2)) {
                    webvttCssStyle.b(ColorParser.a(c2));
                } else if ("background-color".equals(a2)) {
                    webvttCssStyle.a(ColorParser.a(c2));
                } else if ("text-decoration".equals(a2)) {
                    if ("underline".equals(c2)) {
                        webvttCssStyle.c(true);
                    }
                } else if ("font-family".equals(a2)) {
                    webvttCssStyle.a(c2);
                } else if ("font-weight".equals(a2)) {
                    if ("bold".equals(c2)) {
                        webvttCssStyle.a(true);
                    }
                } else if ("font-style".equals(a2) && "italic".equals(c2)) {
                    webvttCssStyle.b(true);
                }
            }
        }
    }

    private static char a(ParsableByteArray parsableByteArray, int i) {
        return (char) parsableByteArray.f3641a[i];
    }

    private static String a(ParsableByteArray parsableByteArray, StringBuilder sb) {
        boolean z = false;
        sb.setLength(0);
        int c2 = parsableByteArray.c();
        int d = parsableByteArray.d();
        while (c2 < d && !z) {
            char c3 = (char) parsableByteArray.f3641a[c2];
            if ((c3 < 'A' || c3 > 'Z') && ((c3 < 'a' || c3 > 'z') && !((c3 >= '0' && c3 <= '9') || c3 == '#' || c3 == '-' || c3 == '.' || c3 == '_'))) {
                z = true;
            } else {
                c2++;
                sb.append(c3);
            }
        }
        parsableByteArray.f(c2 - parsableByteArray.c());
        return sb.toString();
    }

    private void a(WebvttCssStyle webvttCssStyle, String str) {
        if (!"".equals(str)) {
            int indexOf = str.indexOf(91);
            if (indexOf != -1) {
                Matcher matcher = c.matcher(str.substring(indexOf));
                if (matcher.matches()) {
                    webvttCssStyle.d(matcher.group(1));
                }
                str = str.substring(0, indexOf);
            }
            String[] a2 = Util.a(str, "\\.");
            String str2 = a2[0];
            int indexOf2 = str2.indexOf(35);
            if (indexOf2 != -1) {
                webvttCssStyle.c(str2.substring(0, indexOf2));
                webvttCssStyle.b(str2.substring(indexOf2 + 1));
            } else {
                webvttCssStyle.c(str2);
            }
            if (a2.length > 1) {
                webvttCssStyle.a((String[]) Arrays.copyOfRange(a2, 1, a2.length));
            }
        }
    }
}
