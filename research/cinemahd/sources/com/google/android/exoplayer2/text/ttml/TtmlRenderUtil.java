package com.google.android.exoplayer2.text.ttml;

import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import java.util.Map;

final class TtmlRenderUtil {
    private TtmlRenderUtil() {
    }

    public static TtmlStyle a(TtmlStyle ttmlStyle, String[] strArr, Map<String, TtmlStyle> map) {
        if (ttmlStyle == null && strArr == null) {
            return null;
        }
        int i = 0;
        if (ttmlStyle == null && strArr.length == 1) {
            return map.get(strArr[0]);
        }
        if (ttmlStyle == null && strArr.length > 1) {
            TtmlStyle ttmlStyle2 = new TtmlStyle();
            int length = strArr.length;
            while (i < length) {
                ttmlStyle2.a(map.get(strArr[i]));
                i++;
            }
            return ttmlStyle2;
        } else if (ttmlStyle != null && strArr != null && strArr.length == 1) {
            return ttmlStyle.a(map.get(strArr[0]));
        } else {
            if (!(ttmlStyle == null || strArr == null || strArr.length <= 1)) {
                int length2 = strArr.length;
                while (i < length2) {
                    ttmlStyle.a(map.get(strArr[i]));
                    i++;
                }
            }
            return ttmlStyle;
        }
    }

    public static void a(SpannableStringBuilder spannableStringBuilder, int i, int i2, TtmlStyle ttmlStyle) {
        if (ttmlStyle.g() != -1) {
            spannableStringBuilder.setSpan(new StyleSpan(ttmlStyle.g()), i, i2, 33);
        }
        if (ttmlStyle.k()) {
            spannableStringBuilder.setSpan(new StrikethroughSpan(), i, i2, 33);
        }
        if (ttmlStyle.l()) {
            spannableStringBuilder.setSpan(new UnderlineSpan(), i, i2, 33);
        }
        if (ttmlStyle.j()) {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(ttmlStyle.b()), i, i2, 33);
        }
        if (ttmlStyle.i()) {
            spannableStringBuilder.setSpan(new BackgroundColorSpan(ttmlStyle.a()), i, i2, 33);
        }
        if (ttmlStyle.c() != null) {
            spannableStringBuilder.setSpan(new TypefaceSpan(ttmlStyle.c()), i, i2, 33);
        }
        if (ttmlStyle.h() != null) {
            spannableStringBuilder.setSpan(new AlignmentSpan.Standard(ttmlStyle.h()), i, i2, 33);
        }
        int e = ttmlStyle.e();
        if (e == 1) {
            spannableStringBuilder.setSpan(new AbsoluteSizeSpan((int) ttmlStyle.d(), true), i, i2, 33);
        } else if (e == 2) {
            spannableStringBuilder.setSpan(new RelativeSizeSpan(ttmlStyle.d()), i, i2, 33);
        } else if (e == 3) {
            spannableStringBuilder.setSpan(new RelativeSizeSpan(ttmlStyle.d() / 100.0f), i, i2, 33);
        }
    }

    static void a(SpannableStringBuilder spannableStringBuilder) {
        int length = spannableStringBuilder.length() - 1;
        while (length >= 0 && spannableStringBuilder.charAt(length) == ' ') {
            length--;
        }
        if (length >= 0 && spannableStringBuilder.charAt(length) != 10) {
            spannableStringBuilder.append(10);
        }
    }

    static String a(String str) {
        return str.replaceAll("\r\n", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE).replaceAll(" *\n *", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE).replaceAll(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, " ").replaceAll("[ \t\\x0B\f\r]+", " ");
    }
}
