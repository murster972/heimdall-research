package com.google.android.exoplayer2.text;

final class SimpleSubtitleOutputBuffer extends SubtitleOutputBuffer {
    private final SimpleSubtitleDecoder c;

    public SimpleSubtitleOutputBuffer(SimpleSubtitleDecoder simpleSubtitleDecoder) {
        this.c = simpleSubtitleDecoder;
    }

    public final void release() {
        this.c.a((SubtitleOutputBuffer) this);
    }
}
