package com.google.android.exoplayer2.text.dvb;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Region;
import android.util.SparseArray;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class DvbParser {
    private static final byte[] h = {0, 7, 8, 15};
    private static final byte[] i = {0, 119, -120, -1};
    private static final byte[] j = {0, 17, 34, 51, 68, 85, 102, 119, -120, -103, -86, -69, -52, -35, -18, -1};

    /* renamed from: a  reason: collision with root package name */
    private final Paint f3524a = new Paint();
    private final Paint b;
    private final Canvas c;
    private final DisplayDefinition d;
    private final ClutDefinition e;
    private final SubtitleService f;
    private Bitmap g;

    private static final class ClutDefinition {

        /* renamed from: a  reason: collision with root package name */
        public final int f3525a;
        public final int[] b;
        public final int[] c;
        public final int[] d;

        public ClutDefinition(int i, int[] iArr, int[] iArr2, int[] iArr3) {
            this.f3525a = i;
            this.b = iArr;
            this.c = iArr2;
            this.d = iArr3;
        }
    }

    private static final class DisplayDefinition {

        /* renamed from: a  reason: collision with root package name */
        public final int f3526a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;

        public DisplayDefinition(int i, int i2, int i3, int i4, int i5, int i6) {
            this.f3526a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = i6;
        }
    }

    private static final class ObjectData {

        /* renamed from: a  reason: collision with root package name */
        public final int f3527a;
        public final boolean b;
        public final byte[] c;
        public final byte[] d;

        public ObjectData(int i, boolean z, byte[] bArr, byte[] bArr2) {
            this.f3527a = i;
            this.b = z;
            this.c = bArr;
            this.d = bArr2;
        }
    }

    private static final class PageComposition {

        /* renamed from: a  reason: collision with root package name */
        public final int f3528a;
        public final int b;
        public final SparseArray<PageRegion> c;

        public PageComposition(int i, int i2, int i3, SparseArray<PageRegion> sparseArray) {
            this.f3528a = i2;
            this.b = i3;
            this.c = sparseArray;
        }
    }

    private static final class PageRegion {

        /* renamed from: a  reason: collision with root package name */
        public final int f3529a;
        public final int b;

        public PageRegion(int i, int i2) {
            this.f3529a = i;
            this.b = i2;
        }
    }

    private static final class RegionComposition {

        /* renamed from: a  reason: collision with root package name */
        public final int f3530a;
        public final boolean b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final int i;
        public final SparseArray<RegionObject> j;

        public RegionComposition(int i2, boolean z, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, SparseArray<RegionObject> sparseArray) {
            this.f3530a = i2;
            this.b = z;
            this.c = i3;
            this.d = i4;
            this.e = i6;
            this.f = i7;
            this.g = i8;
            this.h = i9;
            this.i = i10;
            this.j = sparseArray;
        }

        public void a(RegionComposition regionComposition) {
            if (regionComposition != null) {
                SparseArray<RegionObject> sparseArray = regionComposition.j;
                for (int i2 = 0; i2 < sparseArray.size(); i2++) {
                    this.j.put(sparseArray.keyAt(i2), sparseArray.valueAt(i2));
                }
            }
        }
    }

    private static final class RegionObject {

        /* renamed from: a  reason: collision with root package name */
        public final int f3531a;
        public final int b;

        public RegionObject(int i, int i2, int i3, int i4, int i5, int i6) {
            this.f3531a = i3;
            this.b = i4;
        }
    }

    private static final class SubtitleService {

        /* renamed from: a  reason: collision with root package name */
        public final int f3532a;
        public final int b;
        public final SparseArray<RegionComposition> c = new SparseArray<>();
        public final SparseArray<ClutDefinition> d = new SparseArray<>();
        public final SparseArray<ObjectData> e = new SparseArray<>();
        public final SparseArray<ClutDefinition> f = new SparseArray<>();
        public final SparseArray<ObjectData> g = new SparseArray<>();
        public DisplayDefinition h;
        public PageComposition i;

        public SubtitleService(int i2, int i3) {
            this.f3532a = i2;
            this.b = i3;
        }

        public void a() {
            this.c.clear();
            this.d.clear();
            this.e.clear();
            this.f.clear();
            this.g.clear();
            this.h = null;
            this.i = null;
        }
    }

    public DvbParser(int i2, int i3) {
        this.f3524a.setStyle(Paint.Style.FILL_AND_STROKE);
        this.f3524a.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        this.f3524a.setPathEffect((PathEffect) null);
        this.b = new Paint();
        this.b.setStyle(Paint.Style.FILL);
        this.b.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
        this.b.setPathEffect((PathEffect) null);
        this.c = new Canvas();
        this.d = new DisplayDefinition(719, 575, 0, 719, 0, 575);
        this.e = new ClutDefinition(0, b(), c(), d());
        this.f = new SubtitleService(i2, i3);
    }

    private static int a(int i2, int i3, int i4, int i5) {
        return (i2 << 24) | (i3 << 16) | (i4 << 8) | i5;
    }

    private static PageComposition b(ParsableBitArray parsableBitArray, int i2) {
        int a2 = parsableBitArray.a(8);
        int a3 = parsableBitArray.a(4);
        int a4 = parsableBitArray.a(2);
        parsableBitArray.c(2);
        int i3 = i2 - 2;
        SparseArray sparseArray = new SparseArray();
        while (i3 > 0) {
            int a5 = parsableBitArray.a(8);
            parsableBitArray.c(8);
            i3 -= 6;
            sparseArray.put(a5, new PageRegion(parsableBitArray.a(16), parsableBitArray.a(16)));
        }
        return new PageComposition(a2, a3, a4, sparseArray);
    }

    private static RegionComposition c(ParsableBitArray parsableBitArray, int i2) {
        int i3;
        int i4;
        ParsableBitArray parsableBitArray2 = parsableBitArray;
        int a2 = parsableBitArray2.a(8);
        parsableBitArray2.c(4);
        boolean e2 = parsableBitArray.e();
        parsableBitArray2.c(3);
        int i5 = 16;
        int a3 = parsableBitArray2.a(16);
        int a4 = parsableBitArray2.a(16);
        int a5 = parsableBitArray2.a(3);
        int a6 = parsableBitArray2.a(3);
        int i6 = 2;
        parsableBitArray2.c(2);
        int a7 = parsableBitArray2.a(8);
        int a8 = parsableBitArray2.a(8);
        int a9 = parsableBitArray2.a(4);
        int a10 = parsableBitArray2.a(2);
        parsableBitArray2.c(2);
        int i7 = i2 - 10;
        SparseArray sparseArray = new SparseArray();
        while (i7 > 0) {
            int a11 = parsableBitArray2.a(i5);
            int a12 = parsableBitArray2.a(i6);
            int a13 = parsableBitArray2.a(i6);
            int a14 = parsableBitArray2.a(12);
            int i8 = a10;
            parsableBitArray2.c(4);
            int a15 = parsableBitArray2.a(12);
            i7 -= 6;
            if (a12 == 1 || a12 == 2) {
                i7 -= 2;
                i4 = parsableBitArray2.a(8);
                i3 = parsableBitArray2.a(8);
            } else {
                i4 = 0;
                i3 = 0;
            }
            sparseArray.put(a11, new RegionObject(a12, a13, a14, a15, i4, i3));
            a10 = i8;
            i6 = 2;
            i5 = 16;
        }
        return new RegionComposition(a2, e2, a3, a4, a5, a6, a7, a8, a9, a10, sparseArray);
    }

    private static int[] d() {
        int[] iArr = new int[256];
        iArr[0] = 0;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            int i3 = JfifUtil.MARKER_FIRST_BYTE;
            if (i2 < 8) {
                int i4 = (i2 & 1) != 0 ? JfifUtil.MARKER_FIRST_BYTE : 0;
                int i5 = (i2 & 2) != 0 ? JfifUtil.MARKER_FIRST_BYTE : 0;
                if ((i2 & 4) == 0) {
                    i3 = 0;
                }
                iArr[i2] = a(63, i4, i5, i3);
            } else {
                int i6 = i2 & 136;
                int i7 = 170;
                int i8 = 85;
                if (i6 == 0) {
                    int i9 = ((i2 & 1) != 0 ? 85 : 0) + ((i2 & 16) != 0 ? 170 : 0);
                    int i10 = ((i2 & 2) != 0 ? 85 : 0) + ((i2 & 32) != 0 ? 170 : 0);
                    if ((i2 & 4) == 0) {
                        i8 = 0;
                    }
                    if ((i2 & 64) == 0) {
                        i7 = 0;
                    }
                    iArr[i2] = a(JfifUtil.MARKER_FIRST_BYTE, i9, i10, i8 + i7);
                } else if (i6 != 8) {
                    int i11 = 43;
                    if (i6 == 128) {
                        int i12 = ((i2 & 1) != 0 ? 43 : 0) + 127 + ((i2 & 16) != 0 ? 85 : 0);
                        int i13 = ((i2 & 2) != 0 ? 43 : 0) + 127 + ((i2 & 32) != 0 ? 85 : 0);
                        if ((i2 & 4) == 0) {
                            i11 = 0;
                        }
                        int i14 = i11 + 127;
                        if ((i2 & 64) == 0) {
                            i8 = 0;
                        }
                        iArr[i2] = a(JfifUtil.MARKER_FIRST_BYTE, i12, i13, i14 + i8);
                    } else if (i6 == 136) {
                        int i15 = ((i2 & 1) != 0 ? 43 : 0) + ((i2 & 16) != 0 ? 85 : 0);
                        int i16 = ((i2 & 2) != 0 ? 43 : 0) + ((i2 & 32) != 0 ? 85 : 0);
                        if ((i2 & 4) == 0) {
                            i11 = 0;
                        }
                        if ((i2 & 64) == 0) {
                            i8 = 0;
                        }
                        iArr[i2] = a(JfifUtil.MARKER_FIRST_BYTE, i15, i16, i11 + i8);
                    }
                } else {
                    int i17 = ((i2 & 1) != 0 ? 85 : 0) + ((i2 & 16) != 0 ? 170 : 0);
                    int i18 = ((i2 & 2) != 0 ? 85 : 0) + ((i2 & 32) != 0 ? 170 : 0);
                    if ((i2 & 4) == 0) {
                        i8 = 0;
                    }
                    if ((i2 & 64) == 0) {
                        i7 = 0;
                    }
                    iArr[i2] = a(127, i17, i18, i8 + i7);
                }
            }
        }
        return iArr;
    }

    public void a() {
        this.f.a();
    }

    public List<Cue> a(byte[] bArr, int i2) {
        int i3;
        int i4;
        SparseArray<RegionObject> sparseArray;
        ParsableBitArray parsableBitArray = new ParsableBitArray(bArr, i2);
        while (parsableBitArray.a() >= 48 && parsableBitArray.a(8) == 15) {
            a(parsableBitArray, this.f);
        }
        SubtitleService subtitleService = this.f;
        if (subtitleService.i == null) {
            return Collections.emptyList();
        }
        DisplayDefinition displayDefinition = subtitleService.h;
        if (displayDefinition == null) {
            displayDefinition = this.d;
        }
        Bitmap bitmap = this.g;
        if (!(bitmap != null && displayDefinition.f3526a + 1 == bitmap.getWidth() && displayDefinition.b + 1 == this.g.getHeight())) {
            this.g = Bitmap.createBitmap(displayDefinition.f3526a + 1, displayDefinition.b + 1, Bitmap.Config.ARGB_8888);
            this.c.setBitmap(this.g);
        }
        ArrayList arrayList = new ArrayList();
        SparseArray<PageRegion> sparseArray2 = this.f.i.c;
        for (int i5 = 0; i5 < sparseArray2.size(); i5++) {
            PageRegion valueAt = sparseArray2.valueAt(i5);
            RegionComposition regionComposition = this.f.c.get(sparseArray2.keyAt(i5));
            int i6 = valueAt.f3529a + displayDefinition.c;
            int i7 = valueAt.b + displayDefinition.e;
            float f2 = (float) i6;
            float f3 = (float) i7;
            float f4 = f3;
            float f5 = f2;
            this.c.clipRect(f2, f3, (float) Math.min(regionComposition.c + i6, displayDefinition.d), (float) Math.min(regionComposition.d + i7, displayDefinition.f), Region.Op.REPLACE);
            ClutDefinition clutDefinition = this.f.d.get(regionComposition.f);
            if (clutDefinition == null && (clutDefinition = this.f.f.get(regionComposition.f)) == null) {
                clutDefinition = this.e;
            }
            SparseArray<RegionObject> sparseArray3 = regionComposition.j;
            int i8 = 0;
            while (i8 < sparseArray3.size()) {
                int keyAt = sparseArray3.keyAt(i8);
                RegionObject valueAt2 = sparseArray3.valueAt(i8);
                ObjectData objectData = this.f.e.get(keyAt);
                ObjectData objectData2 = objectData == null ? this.f.g.get(keyAt) : objectData;
                if (objectData2 != null) {
                    i4 = i8;
                    sparseArray = sparseArray3;
                    a(objectData2, clutDefinition, regionComposition.e, valueAt2.f3531a + i6, i7 + valueAt2.b, objectData2.b ? null : this.f3524a, this.c);
                } else {
                    i4 = i8;
                    sparseArray = sparseArray3;
                }
                i8 = i4 + 1;
                sparseArray3 = sparseArray;
            }
            if (regionComposition.b) {
                int i9 = regionComposition.e;
                if (i9 == 3) {
                    i3 = clutDefinition.d[regionComposition.g];
                } else if (i9 == 2) {
                    i3 = clutDefinition.c[regionComposition.h];
                } else {
                    i3 = clutDefinition.b[regionComposition.i];
                }
                this.b.setColor(i3);
                this.c.drawRect(f5, f4, (float) (regionComposition.c + i6), (float) (regionComposition.d + i7), this.b);
            }
            Bitmap createBitmap = Bitmap.createBitmap(this.g, i6, i7, regionComposition.c, regionComposition.d);
            int i10 = displayDefinition.f3526a;
            int i11 = displayDefinition.b;
            arrayList.add(new Cue(createBitmap, f5 / ((float) i10), 0, f4 / ((float) i11), 0, ((float) regionComposition.c) / ((float) i10), ((float) regionComposition.d) / ((float) i11)));
            this.c.drawColor(0, PorterDuff.Mode.CLEAR);
        }
        return arrayList;
    }

    private static ObjectData b(ParsableBitArray parsableBitArray) {
        byte[] bArr;
        int a2 = parsableBitArray.a(16);
        parsableBitArray.c(4);
        int a3 = parsableBitArray.a(2);
        boolean e2 = parsableBitArray.e();
        parsableBitArray.c(1);
        byte[] bArr2 = null;
        if (a3 == 1) {
            parsableBitArray.c(parsableBitArray.a(8) * 16);
        } else if (a3 == 0) {
            int a4 = parsableBitArray.a(16);
            int a5 = parsableBitArray.a(16);
            if (a4 > 0) {
                bArr2 = new byte[a4];
                parsableBitArray.b(bArr2, 0, a4);
            }
            if (a5 > 0) {
                bArr = new byte[a5];
                parsableBitArray.b(bArr, 0, a5);
                return new ObjectData(a2, e2, bArr2, bArr);
            }
        }
        bArr = bArr2;
        return new ObjectData(a2, e2, bArr2, bArr);
    }

    private static int[] b() {
        return new int[]{0, -1, -16777216, -8421505};
    }

    private static int[] c() {
        int[] iArr = new int[16];
        iArr[0] = 0;
        for (int i2 = 1; i2 < iArr.length; i2++) {
            if (i2 < 8) {
                iArr[i2] = a(JfifUtil.MARKER_FIRST_BYTE, (i2 & 1) != 0 ? JfifUtil.MARKER_FIRST_BYTE : 0, (i2 & 2) != 0 ? JfifUtil.MARKER_FIRST_BYTE : 0, (i2 & 4) != 0 ? JfifUtil.MARKER_FIRST_BYTE : 0);
            } else {
                int i3 = 127;
                int i4 = (i2 & 1) != 0 ? 127 : 0;
                int i5 = (i2 & 2) != 0 ? 127 : 0;
                if ((i2 & 4) == 0) {
                    i3 = 0;
                }
                iArr[i2] = a(JfifUtil.MARKER_FIRST_BYTE, i4, i5, i3);
            }
        }
        return iArr;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v18, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008f A[LOOP:0: B:1:0x0009->B:33:0x008f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int b(com.google.android.exoplayer2.util.ParsableBitArray r13, int[] r14, byte[] r15, int r16, int r17, android.graphics.Paint r18, android.graphics.Canvas r19) {
        /*
            r0 = r13
            r1 = r17
            r8 = r18
            r9 = 0
            r10 = r16
            r2 = 0
        L_0x0009:
            r3 = 4
            int r4 = r13.a((int) r3)
            r5 = 2
            r6 = 1
            if (r4 == 0) goto L_0x0017
            r12 = r2
            r3 = r4
        L_0x0014:
            r11 = 1
            goto L_0x006f
        L_0x0017:
            boolean r4 = r13.e()
            r7 = 3
            if (r4 != 0) goto L_0x002e
            int r3 = r13.a((int) r7)
            if (r3 == 0) goto L_0x002a
            int r3 = r3 + 2
            r12 = r2
            r11 = r3
            r3 = 0
            goto L_0x006f
        L_0x002a:
            r3 = 0
            r11 = 0
            r12 = 1
            goto L_0x006f
        L_0x002e:
            boolean r4 = r13.e()
            if (r4 != 0) goto L_0x0040
            int r4 = r13.a((int) r5)
            int r4 = r4 + r3
            int r3 = r13.a((int) r3)
        L_0x003d:
            r12 = r2
            r11 = r4
            goto L_0x006f
        L_0x0040:
            int r4 = r13.a((int) r5)
            if (r4 == 0) goto L_0x006c
            if (r4 == r6) goto L_0x0068
            if (r4 == r5) goto L_0x005d
            if (r4 == r7) goto L_0x0050
            r12 = r2
            r3 = 0
            r11 = 0
            goto L_0x006f
        L_0x0050:
            r4 = 8
            int r4 = r13.a((int) r4)
            int r4 = r4 + 25
            int r3 = r13.a((int) r3)
            goto L_0x003d
        L_0x005d:
            int r4 = r13.a((int) r3)
            int r4 = r4 + 9
            int r3 = r13.a((int) r3)
            goto L_0x003d
        L_0x0068:
            r12 = r2
            r3 = 0
            r11 = 2
            goto L_0x006f
        L_0x006c:
            r12 = r2
            r3 = 0
            goto L_0x0014
        L_0x006f:
            if (r11 == 0) goto L_0x008b
            if (r8 == 0) goto L_0x008b
            if (r15 == 0) goto L_0x0077
            byte r3 = r15[r3]
        L_0x0077:
            r2 = r14[r3]
            r8.setColor(r2)
            float r3 = (float) r10
            float r4 = (float) r1
            int r2 = r10 + r11
            float r5 = (float) r2
            int r2 = r1 + 1
            float r6 = (float) r2
            r2 = r19
            r7 = r18
            r2.drawRect(r3, r4, r5, r6, r7)
        L_0x008b:
            int r10 = r10 + r11
            if (r12 == 0) goto L_0x008f
            return r10
        L_0x008f:
            r2 = r12
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.dvb.DvbParser.b(com.google.android.exoplayer2.util.ParsableBitArray, int[], byte[], int, int, android.graphics.Paint, android.graphics.Canvas):int");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int c(com.google.android.exoplayer2.util.ParsableBitArray r13, int[] r14, byte[] r15, int r16, int r17, android.graphics.Paint r18, android.graphics.Canvas r19) {
        /*
            r0 = r13
            r1 = r17
            r8 = r18
            r9 = 0
            r10 = r16
            r2 = 0
        L_0x0009:
            r3 = 8
            int r4 = r13.a((int) r3)
            r5 = 1
            if (r4 == 0) goto L_0x0016
            r12 = r2
            r3 = r4
            r11 = 1
            goto L_0x0035
        L_0x0016:
            boolean r4 = r13.e()
            r6 = 7
            if (r4 != 0) goto L_0x002b
            int r3 = r13.a((int) r6)
            if (r3 == 0) goto L_0x0027
            r12 = r2
            r11 = r3
            r3 = 0
            goto L_0x0035
        L_0x0027:
            r3 = 0
            r11 = 0
            r12 = 1
            goto L_0x0035
        L_0x002b:
            int r4 = r13.a((int) r6)
            int r3 = r13.a((int) r3)
            r12 = r2
            r11 = r4
        L_0x0035:
            if (r11 == 0) goto L_0x0053
            if (r8 == 0) goto L_0x0053
            if (r15 == 0) goto L_0x003d
            byte r3 = r15[r3]
        L_0x003d:
            r2 = r14[r3]
            r8.setColor(r2)
            float r3 = (float) r10
            float r4 = (float) r1
            int r2 = r10 + r11
            float r6 = (float) r2
            int r2 = r1 + 1
            float r7 = (float) r2
            r2 = r19
            r5 = r6
            r6 = r7
            r7 = r18
            r2.drawRect(r3, r4, r5, r6, r7)
        L_0x0053:
            int r10 = r10 + r11
            if (r12 == 0) goto L_0x0057
            return r10
        L_0x0057:
            r2 = r12
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.dvb.DvbParser.c(com.google.android.exoplayer2.util.ParsableBitArray, int[], byte[], int, int, android.graphics.Paint, android.graphics.Canvas):int");
    }

    private static void a(ParsableBitArray parsableBitArray, SubtitleService subtitleService) {
        int a2 = parsableBitArray.a(8);
        int a3 = parsableBitArray.a(16);
        int a4 = parsableBitArray.a(16);
        int c2 = parsableBitArray.c() + a4;
        if (a4 * 8 > parsableBitArray.a()) {
            Log.d("DvbParser", "Data field length exceeds limit");
            parsableBitArray.c(parsableBitArray.a());
            return;
        }
        switch (a2) {
            case 16:
                if (a3 == subtitleService.f3532a) {
                    PageComposition pageComposition = subtitleService.i;
                    PageComposition b2 = b(parsableBitArray, a4);
                    if (b2.b == 0) {
                        if (!(pageComposition == null || pageComposition.f3528a == b2.f3528a)) {
                            subtitleService.i = b2;
                            break;
                        }
                    } else {
                        subtitleService.i = b2;
                        subtitleService.c.clear();
                        subtitleService.d.clear();
                        subtitleService.e.clear();
                        break;
                    }
                }
                break;
            case 17:
                PageComposition pageComposition2 = subtitleService.i;
                if (a3 == subtitleService.f3532a && pageComposition2 != null) {
                    RegionComposition c3 = c(parsableBitArray, a4);
                    if (pageComposition2.b == 0) {
                        c3.a(subtitleService.c.get(c3.f3530a));
                    }
                    subtitleService.c.put(c3.f3530a, c3);
                    break;
                }
            case 18:
                if (a3 != subtitleService.f3532a) {
                    if (a3 == subtitleService.b) {
                        ClutDefinition a5 = a(parsableBitArray, a4);
                        subtitleService.f.put(a5.f3525a, a5);
                        break;
                    }
                } else {
                    ClutDefinition a6 = a(parsableBitArray, a4);
                    subtitleService.d.put(a6.f3525a, a6);
                    break;
                }
                break;
            case 19:
                if (a3 != subtitleService.f3532a) {
                    if (a3 == subtitleService.b) {
                        ObjectData b3 = b(parsableBitArray);
                        subtitleService.g.put(b3.f3527a, b3);
                        break;
                    }
                } else {
                    ObjectData b4 = b(parsableBitArray);
                    subtitleService.e.put(b4.f3527a, b4);
                    break;
                }
                break;
            case 20:
                if (a3 == subtitleService.f3532a) {
                    subtitleService.h = a(parsableBitArray);
                    break;
                }
                break;
        }
        parsableBitArray.d(c2 - parsableBitArray.c());
    }

    private static DisplayDefinition a(ParsableBitArray parsableBitArray) {
        int i2;
        int i3;
        int i4;
        int i5;
        parsableBitArray.c(4);
        boolean e2 = parsableBitArray.e();
        parsableBitArray.c(3);
        int a2 = parsableBitArray.a(16);
        int a3 = parsableBitArray.a(16);
        if (e2) {
            int a4 = parsableBitArray.a(16);
            int a5 = parsableBitArray.a(16);
            int a6 = parsableBitArray.a(16);
            i2 = parsableBitArray.a(16);
            i4 = a5;
            i3 = a6;
            i5 = a4;
        } else {
            i4 = a2;
            i2 = a3;
            i5 = 0;
            i3 = 0;
        }
        return new DisplayDefinition(a2, a3, i5, i4, i3, i2);
    }

    private static ClutDefinition a(ParsableBitArray parsableBitArray, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        ParsableBitArray parsableBitArray2 = parsableBitArray;
        int i8 = 8;
        int a2 = parsableBitArray2.a(8);
        parsableBitArray2.c(8);
        int i9 = 2;
        int i10 = i2 - 2;
        int[] b2 = b();
        int[] c2 = c();
        int[] d2 = d();
        while (i10 > 0) {
            int a3 = parsableBitArray2.a(i8);
            int a4 = parsableBitArray2.a(i8);
            int i11 = i10 - 2;
            int[] iArr = (a4 & 128) != 0 ? b2 : (a4 & 64) != 0 ? c2 : d2;
            if ((a4 & 1) != 0) {
                i6 = parsableBitArray2.a(i8);
                i5 = parsableBitArray2.a(i8);
                i4 = parsableBitArray2.a(i8);
                i3 = parsableBitArray2.a(i8);
                i7 = i11 - 4;
            } else {
                i4 = parsableBitArray2.a(4) << 4;
                i7 = i11 - 2;
                int a5 = parsableBitArray2.a(4) << 4;
                i3 = parsableBitArray2.a(i9) << 6;
                i6 = parsableBitArray2.a(6) << i9;
                i5 = a5;
            }
            if (i6 == 0) {
                i5 = 0;
                i4 = 0;
                i3 = JfifUtil.MARKER_FIRST_BYTE;
            }
            double d3 = (double) i6;
            double d4 = (double) (i5 - 128);
            double d5 = (double) (i4 - 128);
            iArr[a3] = a((byte) (255 - (i3 & JfifUtil.MARKER_FIRST_BYTE)), Util.a((int) (d3 + (1.402d * d4)), 0, (int) JfifUtil.MARKER_FIRST_BYTE), Util.a((int) ((d3 - (0.34414d * d5)) - (d4 * 0.71414d)), 0, (int) JfifUtil.MARKER_FIRST_BYTE), Util.a((int) (d3 + (d5 * 1.772d)), 0, (int) JfifUtil.MARKER_FIRST_BYTE));
            i10 = i7;
            a2 = a2;
            i8 = 8;
            i9 = 2;
        }
        return new ClutDefinition(a2, b2, c2, d2);
    }

    private static void a(ObjectData objectData, ClutDefinition clutDefinition, int i2, int i3, int i4, Paint paint, Canvas canvas) {
        int[] iArr;
        if (i2 == 3) {
            iArr = clutDefinition.d;
        } else if (i2 == 2) {
            iArr = clutDefinition.c;
        } else {
            iArr = clutDefinition.b;
        }
        int[] iArr2 = iArr;
        int i5 = i2;
        int i6 = i3;
        Paint paint2 = paint;
        Canvas canvas2 = canvas;
        a(objectData.c, iArr2, i5, i6, i4, paint2, canvas2);
        a(objectData.d, iArr2, i5, i6, i4 + 1, paint2, canvas2);
    }

    private static void a(byte[] bArr, int[] iArr, int i2, int i3, int i4, Paint paint, Canvas canvas) {
        byte[] bArr2;
        int a2;
        byte[] bArr3;
        byte[] bArr4;
        int i5 = i2;
        byte[] bArr5 = bArr;
        ParsableBitArray parsableBitArray = new ParsableBitArray(bArr);
        int i6 = i3;
        int i7 = i4;
        byte[] bArr6 = null;
        byte[] bArr7 = null;
        while (parsableBitArray.a() != 0) {
            int a3 = parsableBitArray.a(8);
            if (a3 != 240) {
                switch (a3) {
                    case 16:
                        if (i5 != 3) {
                            if (i5 != 2) {
                                bArr2 = null;
                                a2 = a(parsableBitArray, iArr, bArr2, i6, i7, paint, canvas);
                                parsableBitArray.b();
                                break;
                            } else {
                                bArr3 = bArr7 == null ? h : bArr7;
                            }
                        } else {
                            bArr3 = bArr6 == null ? i : bArr6;
                        }
                        bArr2 = bArr3;
                        a2 = a(parsableBitArray, iArr, bArr2, i6, i7, paint, canvas);
                        parsableBitArray.b();
                    case 17:
                        a2 = b(parsableBitArray, iArr, i5 == 3 ? j : null, i6, i7, paint, canvas);
                        parsableBitArray.b();
                        break;
                    case 18:
                        a2 = c(parsableBitArray, iArr, (byte[]) null, i6, i7, paint, canvas);
                        break;
                    default:
                        switch (a3) {
                            case 32:
                                bArr7 = a(4, 4, parsableBitArray);
                                continue;
                            case 33:
                                bArr4 = a(4, 8, parsableBitArray);
                                break;
                            case 34:
                                bArr4 = a(16, 8, parsableBitArray);
                                break;
                            default:
                                continue;
                        }
                        bArr6 = bArr4;
                        break;
                }
                i6 = a2;
            } else {
                i7 += 2;
                i6 = i3;
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v15, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0083 A[LOOP:0: B:1:0x0009->B:30:0x0083, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0082 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(com.google.android.exoplayer2.util.ParsableBitArray r13, int[] r14, byte[] r15, int r16, int r17, android.graphics.Paint r18, android.graphics.Canvas r19) {
        /*
            r0 = r13
            r1 = r17
            r8 = r18
            r9 = 0
            r10 = r16
            r2 = 0
        L_0x0009:
            r3 = 2
            int r4 = r13.a((int) r3)
            r5 = 1
            if (r4 == 0) goto L_0x0015
            r12 = r2
            r3 = r4
        L_0x0013:
            r11 = 1
            goto L_0x0061
        L_0x0015:
            boolean r4 = r13.e()
            r6 = 3
            if (r4 == 0) goto L_0x0028
            int r4 = r13.a((int) r6)
            int r4 = r4 + r6
            int r3 = r13.a((int) r3)
        L_0x0025:
            r12 = r2
            r11 = r4
            goto L_0x0061
        L_0x0028:
            boolean r4 = r13.e()
            if (r4 == 0) goto L_0x0031
            r12 = r2
            r3 = 0
            goto L_0x0013
        L_0x0031:
            int r4 = r13.a((int) r3)
            if (r4 == 0) goto L_0x005e
            if (r4 == r5) goto L_0x005a
            if (r4 == r3) goto L_0x004e
            if (r4 == r6) goto L_0x0041
            r12 = r2
            r3 = 0
            r11 = 0
            goto L_0x0061
        L_0x0041:
            r4 = 8
            int r4 = r13.a((int) r4)
            int r4 = r4 + 29
            int r3 = r13.a((int) r3)
            goto L_0x0025
        L_0x004e:
            r4 = 4
            int r4 = r13.a((int) r4)
            int r4 = r4 + 12
            int r3 = r13.a((int) r3)
            goto L_0x0025
        L_0x005a:
            r12 = r2
            r3 = 0
            r11 = 2
            goto L_0x0061
        L_0x005e:
            r3 = 0
            r11 = 0
            r12 = 1
        L_0x0061:
            if (r11 == 0) goto L_0x007f
            if (r8 == 0) goto L_0x007f
            if (r15 == 0) goto L_0x0069
            byte r3 = r15[r3]
        L_0x0069:
            r2 = r14[r3]
            r8.setColor(r2)
            float r3 = (float) r10
            float r4 = (float) r1
            int r2 = r10 + r11
            float r6 = (float) r2
            int r2 = r1 + 1
            float r7 = (float) r2
            r2 = r19
            r5 = r6
            r6 = r7
            r7 = r18
            r2.drawRect(r3, r4, r5, r6, r7)
        L_0x007f:
            int r10 = r10 + r11
            if (r12 == 0) goto L_0x0083
            return r10
        L_0x0083:
            r2 = r12
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.dvb.DvbParser.a(com.google.android.exoplayer2.util.ParsableBitArray, int[], byte[], int, int, android.graphics.Paint, android.graphics.Canvas):int");
    }

    private static byte[] a(int i2, int i3, ParsableBitArray parsableBitArray) {
        byte[] bArr = new byte[i2];
        for (int i4 = 0; i4 < i2; i4++) {
            bArr[i4] = (byte) parsableBitArray.a(i3);
        }
        return bArr;
    }
}
