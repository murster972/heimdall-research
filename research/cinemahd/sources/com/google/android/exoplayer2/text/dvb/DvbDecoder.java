package com.google.android.exoplayer2.text.dvb;

import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.List;

public final class DvbDecoder extends SimpleSubtitleDecoder {
    private final DvbParser o;

    public DvbDecoder(List<byte[]> list) {
        super("DvbDecoder");
        ParsableByteArray parsableByteArray = new ParsableByteArray(list.get(0));
        this.o = new DvbParser(parsableByteArray.z(), parsableByteArray.z());
    }

    /* access modifiers changed from: protected */
    public DvbSubtitle a(byte[] bArr, int i, boolean z) {
        if (z) {
            this.o.a();
        }
        return new DvbSubtitle(this.o.a(bArr, i));
    }
}
