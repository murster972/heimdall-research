package com.google.android.exoplayer2.text.pgs;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import java.util.List;

final class PgsSubtitle implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private final List<Cue> f3535a;

    public PgsSubtitle(List<Cue> list) {
        this.f3535a = list;
    }

    public int a() {
        return 1;
    }

    public int a(long j) {
        return -1;
    }

    public long a(int i) {
        return 0;
    }

    public List<Cue> b(long j) {
        return this.f3535a;
    }
}
