package com.google.android.exoplayer2.text.webvtt;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.exoplayer2.text.webvtt.WebvttCue;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Mp4WebvttDecoder extends SimpleSubtitleDecoder {
    private static final int q = Util.c("payl");
    private static final int r = Util.c("sttg");
    private static final int s = Util.c("vttc");
    private final ParsableByteArray o = new ParsableByteArray();
    private final WebvttCue.Builder p = new WebvttCue.Builder();

    public Mp4WebvttDecoder() {
        super("Mp4WebvttDecoder");
    }

    /* access modifiers changed from: protected */
    public Mp4WebvttSubtitle a(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        this.o.a(bArr, i);
        ArrayList arrayList = new ArrayList();
        while (this.o.a() > 0) {
            if (this.o.a() >= 8) {
                int h = this.o.h();
                if (this.o.h() == s) {
                    arrayList.add(a(this.o, this.p, h - 8));
                } else {
                    this.o.f(h - 8);
                }
            } else {
                throw new SubtitleDecoderException("Incomplete Mp4Webvtt Top Level box header found.");
            }
        }
        return new Mp4WebvttSubtitle(arrayList);
    }

    private static Cue a(ParsableByteArray parsableByteArray, WebvttCue.Builder builder, int i) throws SubtitleDecoderException {
        builder.b();
        while (i > 0) {
            if (i >= 8) {
                int h = parsableByteArray.h();
                int h2 = parsableByteArray.h();
                int i2 = h - 8;
                String a2 = Util.a(parsableByteArray.f3641a, parsableByteArray.c(), i2);
                parsableByteArray.f(i2);
                i = (i - 8) - i2;
                if (h2 == r) {
                    WebvttCueParser.a(a2, builder);
                } else if (h2 == q) {
                    WebvttCueParser.a((String) null, a2.trim(), builder, (List<WebvttCssStyle>) Collections.emptyList());
                }
            } else {
                throw new SubtitleDecoderException("Incomplete vtt cue box header found.");
            }
        }
        return builder.a();
    }
}
