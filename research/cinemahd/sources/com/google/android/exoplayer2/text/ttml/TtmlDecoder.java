package com.google.android.exoplayer2.text.ttml;

import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.util.XmlPullParserUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class TtmlDecoder extends SimpleSubtitleDecoder {
    private static final Pattern p = Pattern.compile("^([0-9][0-9]+):([0-9][0-9]):([0-9][0-9])(?:(\\.[0-9]+)|:([0-9][0-9])(?:\\.([0-9]+))?)?$");
    private static final Pattern q = Pattern.compile("^([0-9]+(?:\\.[0-9]+)?)(h|m|s|ms|f|t)$");
    private static final Pattern r = Pattern.compile("^(([0-9]*.)?[0-9]+)(px|em|%)$");
    private static final Pattern s = Pattern.compile("^(\\d+\\.?\\d*?)% (\\d+\\.?\\d*?)%$");
    private static final Pattern t = Pattern.compile("^(\\d+) (\\d+)$");
    private static final FrameAndTickRate u = new FrameAndTickRate(30.0f, 1, 1);
    private static final CellResolution v = new CellResolution(32, 15);
    private final XmlPullParserFactory o;

    private static final class CellResolution {

        /* renamed from: a  reason: collision with root package name */
        final int f3538a;

        CellResolution(int i, int i2) {
            this.f3538a = i2;
        }
    }

    private static final class FrameAndTickRate {

        /* renamed from: a  reason: collision with root package name */
        final float f3539a;
        final int b;
        final int c;

        FrameAndTickRate(float f, int i, int i2) {
            this.f3539a = f;
            this.b = i;
            this.c = i2;
        }
    }

    public TtmlDecoder() {
        super("TtmlDecoder");
        try {
            this.o = XmlPullParserFactory.newInstance();
            this.o.setNamespaceAware(true);
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    private TtmlRegion b(XmlPullParser xmlPullParser, CellResolution cellResolution) {
        String a2 = XmlPullParserUtil.a(xmlPullParser, "id");
        if (a2 == null) {
            return null;
        }
        String a3 = XmlPullParserUtil.a(xmlPullParser, "origin");
        if (a3 != null) {
            Matcher matcher = s.matcher(a3);
            if (matcher.matches()) {
                try {
                    float parseFloat = Float.parseFloat(matcher.group(1)) / 100.0f;
                    int i = 2;
                    float parseFloat2 = Float.parseFloat(matcher.group(2)) / 100.0f;
                    String a4 = XmlPullParserUtil.a(xmlPullParser, "extent");
                    if (a4 != null) {
                        Matcher matcher2 = s.matcher(a4);
                        if (matcher2.matches()) {
                            try {
                                float parseFloat3 = Float.parseFloat(matcher2.group(1)) / 100.0f;
                                float parseFloat4 = Float.parseFloat(matcher2.group(2)) / 100.0f;
                                String a5 = XmlPullParserUtil.a(xmlPullParser, "displayAlign");
                                if (a5 != null) {
                                    String k = Util.k(a5);
                                    char c = 65535;
                                    int hashCode = k.hashCode();
                                    if (hashCode != -1364013995) {
                                        if (hashCode == 92734940 && k.equals("after")) {
                                            c = 1;
                                        }
                                    } else if (k.equals("center")) {
                                        c = 0;
                                    }
                                    if (c == 0) {
                                        parseFloat2 += parseFloat4 / 2.0f;
                                        i = 1;
                                    } else if (c == 1) {
                                        parseFloat2 += parseFloat4;
                                    }
                                    return new TtmlRegion(a2, parseFloat, parseFloat2, 0, i, parseFloat3, 1, 1.0f / ((float) cellResolution.f3538a));
                                }
                                i = 0;
                                return new TtmlRegion(a2, parseFloat, parseFloat2, 0, i, parseFloat3, 1, 1.0f / ((float) cellResolution.f3538a));
                            } catch (NumberFormatException unused) {
                                Log.d("TtmlDecoder", "Ignoring region with malformed extent: " + a3);
                                return null;
                            }
                        } else {
                            Log.d("TtmlDecoder", "Ignoring region with unsupported extent: " + a3);
                            return null;
                        }
                    } else {
                        Log.d("TtmlDecoder", "Ignoring region without an extent");
                        return null;
                    }
                } catch (NumberFormatException unused2) {
                    Log.d("TtmlDecoder", "Ignoring region with malformed origin: " + a3);
                    return null;
                }
            } else {
                Log.d("TtmlDecoder", "Ignoring region with unsupported origin: " + a3);
                return null;
            }
        } else {
            Log.d("TtmlDecoder", "Ignoring region without an origin");
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public TtmlSubtitle a(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        try {
            XmlPullParser newPullParser = this.o.newPullParser();
            HashMap hashMap = new HashMap();
            HashMap hashMap2 = new HashMap();
            TtmlSubtitle ttmlSubtitle = null;
            hashMap2.put("", new TtmlRegion((String) null));
            int i2 = 0;
            newPullParser.setInput(new ByteArrayInputStream(bArr, 0, i), (String) null);
            ArrayDeque arrayDeque = new ArrayDeque();
            FrameAndTickRate frameAndTickRate = u;
            CellResolution cellResolution = v;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.getEventType()) {
                TtmlNode ttmlNode = (TtmlNode) arrayDeque.peek();
                if (i2 == 0) {
                    String name = newPullParser.getName();
                    if (eventType == 2) {
                        if ("tt".equals(name)) {
                            frameAndTickRate = a(newPullParser);
                            cellResolution = a(newPullParser, v);
                        }
                        if (!a(name)) {
                            Log.c("TtmlDecoder", "Ignoring unsupported tag: " + newPullParser.getName());
                        } else if ("head".equals(name)) {
                            a(newPullParser, (Map<String, TtmlStyle>) hashMap, (Map<String, TtmlRegion>) hashMap2, cellResolution);
                        } else {
                            try {
                                TtmlNode a2 = a(newPullParser, ttmlNode, (Map<String, TtmlRegion>) hashMap2, frameAndTickRate);
                                arrayDeque.push(a2);
                                if (ttmlNode != null) {
                                    ttmlNode.a(a2);
                                }
                            } catch (SubtitleDecoderException e) {
                                Log.b("TtmlDecoder", "Suppressing parser error", e);
                            }
                        }
                    } else if (eventType == 4) {
                        ttmlNode.a(TtmlNode.a(newPullParser.getText()));
                    } else if (eventType == 3) {
                        if (newPullParser.getName().equals("tt")) {
                            ttmlSubtitle = new TtmlSubtitle((TtmlNode) arrayDeque.peek(), hashMap, hashMap2);
                        }
                        arrayDeque.pop();
                    }
                    newPullParser.next();
                } else if (eventType != 2) {
                    if (eventType == 3) {
                        i2--;
                    }
                    newPullParser.next();
                }
                i2++;
                newPullParser.next();
            }
            return ttmlSubtitle;
        } catch (XmlPullParserException e2) {
            throw new SubtitleDecoderException("Unable to decode source", e2);
        } catch (IOException e3) {
            throw new IllegalStateException("Unexpected error when reading input.", e3);
        }
    }

    private String[] b(String str) {
        String trim = str.trim();
        return trim.isEmpty() ? new String[0] : Util.a(trim, "\\s+");
    }

    private FrameAndTickRate a(XmlPullParser xmlPullParser) throws SubtitleDecoderException {
        String attributeValue = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "frameRate");
        int parseInt = attributeValue != null ? Integer.parseInt(attributeValue) : 30;
        float f = 1.0f;
        String attributeValue2 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "frameRateMultiplier");
        if (attributeValue2 != null) {
            String[] a2 = Util.a(attributeValue2, " ");
            if (a2.length == 2) {
                f = ((float) Integer.parseInt(a2[0])) / ((float) Integer.parseInt(a2[1]));
            } else {
                throw new SubtitleDecoderException("frameRateMultiplier doesn't have 2 parts");
            }
        }
        int i = u.b;
        String attributeValue3 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "subFrameRate");
        if (attributeValue3 != null) {
            i = Integer.parseInt(attributeValue3);
        }
        int i2 = u.c;
        String attributeValue4 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "tickRate");
        if (attributeValue4 != null) {
            i2 = Integer.parseInt(attributeValue4);
        }
        return new FrameAndTickRate(((float) parseInt) * f, i, i2);
    }

    private CellResolution a(XmlPullParser xmlPullParser, CellResolution cellResolution) throws SubtitleDecoderException {
        String attributeValue = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "cellResolution");
        if (attributeValue == null) {
            return cellResolution;
        }
        Matcher matcher = t.matcher(attributeValue);
        if (!matcher.matches()) {
            Log.d("TtmlDecoder", "Ignoring malformed cell resolution: " + attributeValue);
            return cellResolution;
        }
        try {
            int parseInt = Integer.parseInt(matcher.group(1));
            int parseInt2 = Integer.parseInt(matcher.group(2));
            if (parseInt != 0 && parseInt2 != 0) {
                return new CellResolution(parseInt, parseInt2);
            }
            throw new SubtitleDecoderException("Invalid cell resolution " + parseInt + " " + parseInt2);
        } catch (NumberFormatException unused) {
            Log.d("TtmlDecoder", "Ignoring malformed cell resolution: " + attributeValue);
            return cellResolution;
        }
    }

    private Map<String, TtmlStyle> a(XmlPullParser xmlPullParser, Map<String, TtmlStyle> map, Map<String, TtmlRegion> map2, CellResolution cellResolution) throws IOException, XmlPullParserException {
        TtmlRegion b;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser, "style")) {
                String a2 = XmlPullParserUtil.a(xmlPullParser, "style");
                TtmlStyle a3 = a(xmlPullParser, new TtmlStyle());
                if (a2 != null) {
                    for (String str : b(a2)) {
                        a3.a(map.get(str));
                    }
                }
                if (a3.f() != null) {
                    map.put(a3.f(), a3);
                }
            } else if (XmlPullParserUtil.d(xmlPullParser, "region") && (b = b(xmlPullParser, cellResolution)) != null) {
                map2.put(b.f3541a, b);
            }
        } while (!XmlPullParserUtil.c(xmlPullParser, "head"));
        return map;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.android.exoplayer2.text.ttml.TtmlStyle a(org.xmlpull.v1.XmlPullParser r12, com.google.android.exoplayer2.text.ttml.TtmlStyle r13) {
        /*
            r11 = this;
            int r0 = r12.getAttributeCount()
            r1 = 0
            r2 = r13
            r13 = 0
        L_0x0007:
            if (r13 >= r0) goto L_0x020c
            java.lang.String r3 = r12.getAttributeValue(r13)
            java.lang.String r4 = r12.getAttributeName(r13)
            int r5 = r4.hashCode()
            r6 = 4
            r7 = -1
            r8 = 2
            r9 = 3
            r10 = 1
            switch(r5) {
                case -1550943582: goto L_0x006f;
                case -1224696685: goto L_0x0065;
                case -1065511464: goto L_0x005b;
                case -879295043: goto L_0x0050;
                case -734428249: goto L_0x0046;
                case 3355: goto L_0x003c;
                case 94842723: goto L_0x0032;
                case 365601008: goto L_0x0028;
                case 1287124693: goto L_0x001e;
                default: goto L_0x001d;
            }
        L_0x001d:
            goto L_0x0079
        L_0x001e:
            java.lang.String r5 = "backgroundColor"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 1
            goto L_0x007a
        L_0x0028:
            java.lang.String r5 = "fontSize"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 4
            goto L_0x007a
        L_0x0032:
            java.lang.String r5 = "color"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 2
            goto L_0x007a
        L_0x003c:
            java.lang.String r5 = "id"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 0
            goto L_0x007a
        L_0x0046:
            java.lang.String r5 = "fontWeight"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 5
            goto L_0x007a
        L_0x0050:
            java.lang.String r5 = "textDecoration"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 8
            goto L_0x007a
        L_0x005b:
            java.lang.String r5 = "textAlign"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 7
            goto L_0x007a
        L_0x0065:
            java.lang.String r5 = "fontFamily"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 3
            goto L_0x007a
        L_0x006f:
            java.lang.String r5 = "fontStyle"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x0079
            r4 = 6
            goto L_0x007a
        L_0x0079:
            r4 = -1
        L_0x007a:
            java.lang.String r5 = "TtmlDecoder"
            switch(r4) {
                case 0: goto L_0x01f4;
                case 1: goto L_0x01d3;
                case 2: goto L_0x01b2;
                case 3: goto L_0x01a9;
                case 4: goto L_0x018b;
                case 5: goto L_0x017b;
                case 6: goto L_0x016b;
                case 7: goto L_0x00e6;
                case 8: goto L_0x0081;
                default: goto L_0x007f;
            }
        L_0x007f:
            goto L_0x0208
        L_0x0081:
            java.lang.String r3 = com.google.android.exoplayer2.util.Util.k(r3)
            int r4 = r3.hashCode()
            switch(r4) {
                case -1461280213: goto L_0x00ab;
                case -1026963764: goto L_0x00a1;
                case 913457136: goto L_0x0097;
                case 1679736913: goto L_0x008d;
                default: goto L_0x008c;
            }
        L_0x008c:
            goto L_0x00b4
        L_0x008d:
            java.lang.String r4 = "linethrough"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00b4
            r7 = 0
            goto L_0x00b4
        L_0x0097:
            java.lang.String r4 = "nolinethrough"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00b4
            r7 = 1
            goto L_0x00b4
        L_0x00a1:
            java.lang.String r4 = "underline"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00b4
            r7 = 2
            goto L_0x00b4
        L_0x00ab:
            java.lang.String r4 = "nounderline"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00b4
            r7 = 3
        L_0x00b4:
            if (r7 == 0) goto L_0x00dc
            if (r7 == r10) goto L_0x00d2
            if (r7 == r8) goto L_0x00c8
            if (r7 == r9) goto L_0x00be
            goto L_0x0208
        L_0x00be:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.d(r1)
            goto L_0x0208
        L_0x00c8:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.d(r10)
            goto L_0x0208
        L_0x00d2:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.c((boolean) r1)
            goto L_0x0208
        L_0x00dc:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.c((boolean) r10)
            goto L_0x0208
        L_0x00e6:
            java.lang.String r3 = com.google.android.exoplayer2.util.Util.k(r3)
            int r4 = r3.hashCode()
            switch(r4) {
                case -1364013995: goto L_0x011a;
                case 100571: goto L_0x0110;
                case 3317767: goto L_0x0106;
                case 108511772: goto L_0x00fc;
                case 109757538: goto L_0x00f2;
                default: goto L_0x00f1;
            }
        L_0x00f1:
            goto L_0x0123
        L_0x00f2:
            java.lang.String r4 = "start"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0123
            r7 = 1
            goto L_0x0123
        L_0x00fc:
            java.lang.String r4 = "right"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0123
            r7 = 2
            goto L_0x0123
        L_0x0106:
            java.lang.String r4 = "left"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0123
            r7 = 0
            goto L_0x0123
        L_0x0110:
            java.lang.String r4 = "end"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0123
            r7 = 3
            goto L_0x0123
        L_0x011a:
            java.lang.String r4 = "center"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0123
            r7 = 4
        L_0x0123:
            if (r7 == 0) goto L_0x015f
            if (r7 == r10) goto L_0x0153
            if (r7 == r8) goto L_0x0147
            if (r7 == r9) goto L_0x013b
            if (r7 == r6) goto L_0x012f
            goto L_0x0208
        L_0x012f:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            android.text.Layout$Alignment r3 = android.text.Layout.Alignment.ALIGN_CENTER
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.a((android.text.Layout.Alignment) r3)
            goto L_0x0208
        L_0x013b:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            android.text.Layout$Alignment r3 = android.text.Layout.Alignment.ALIGN_OPPOSITE
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.a((android.text.Layout.Alignment) r3)
            goto L_0x0208
        L_0x0147:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            android.text.Layout$Alignment r3 = android.text.Layout.Alignment.ALIGN_OPPOSITE
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.a((android.text.Layout.Alignment) r3)
            goto L_0x0208
        L_0x0153:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            android.text.Layout$Alignment r3 = android.text.Layout.Alignment.ALIGN_NORMAL
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.a((android.text.Layout.Alignment) r3)
            goto L_0x0208
        L_0x015f:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            android.text.Layout$Alignment r3 = android.text.Layout.Alignment.ALIGN_NORMAL
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.a((android.text.Layout.Alignment) r3)
            goto L_0x0208
        L_0x016b:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            java.lang.String r4 = "italic"
            boolean r3 = r4.equalsIgnoreCase(r3)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.b((boolean) r3)
            goto L_0x0208
        L_0x017b:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            java.lang.String r4 = "bold"
            boolean r3 = r4.equalsIgnoreCase(r3)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.a((boolean) r3)
            goto L_0x0208
        L_0x018b:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)     // Catch:{ SubtitleDecoderException -> 0x0194 }
            a((java.lang.String) r3, (com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)     // Catch:{ SubtitleDecoderException -> 0x0194 }
            goto L_0x0208
        L_0x0194:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "Failed parsing fontSize value: "
            r4.append(r6)
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            com.google.android.exoplayer2.util.Log.d(r5, r3)
            goto L_0x0208
        L_0x01a9:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.a((java.lang.String) r3)
            goto L_0x0208
        L_0x01b2:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            int r4 = com.google.android.exoplayer2.util.ColorParser.b(r3)     // Catch:{ IllegalArgumentException -> 0x01be }
            r2.b((int) r4)     // Catch:{ IllegalArgumentException -> 0x01be }
            goto L_0x0208
        L_0x01be:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "Failed parsing color value: "
            r4.append(r6)
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            com.google.android.exoplayer2.util.Log.d(r5, r3)
            goto L_0x0208
        L_0x01d3:
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            int r4 = com.google.android.exoplayer2.util.ColorParser.b(r3)     // Catch:{ IllegalArgumentException -> 0x01df }
            r2.a((int) r4)     // Catch:{ IllegalArgumentException -> 0x01df }
            goto L_0x0208
        L_0x01df:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "Failed parsing background value: "
            r4.append(r6)
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            com.google.android.exoplayer2.util.Log.d(r5, r3)
            goto L_0x0208
        L_0x01f4:
            java.lang.String r4 = r12.getName()
            java.lang.String r5 = "style"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x0208
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r11.a((com.google.android.exoplayer2.text.ttml.TtmlStyle) r2)
            com.google.android.exoplayer2.text.ttml.TtmlStyle r2 = r2.b((java.lang.String) r3)
        L_0x0208:
            int r13 = r13 + 1
            goto L_0x0007
        L_0x020c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ttml.TtmlDecoder.a(org.xmlpull.v1.XmlPullParser, com.google.android.exoplayer2.text.ttml.TtmlStyle):com.google.android.exoplayer2.text.ttml.TtmlStyle");
    }

    private TtmlStyle a(TtmlStyle ttmlStyle) {
        return ttmlStyle == null ? new TtmlStyle() : ttmlStyle;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.android.exoplayer2.text.ttml.TtmlNode a(org.xmlpull.v1.XmlPullParser r20, com.google.android.exoplayer2.text.ttml.TtmlNode r21, java.util.Map<java.lang.String, com.google.android.exoplayer2.text.ttml.TtmlRegion> r22, com.google.android.exoplayer2.text.ttml.TtmlDecoder.FrameAndTickRate r23) throws com.google.android.exoplayer2.text.SubtitleDecoderException {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r23
            int r4 = r20.getAttributeCount()
            r5 = 0
            com.google.android.exoplayer2.text.ttml.TtmlStyle r11 = r0.a((org.xmlpull.v1.XmlPullParser) r1, (com.google.android.exoplayer2.text.ttml.TtmlStyle) r5)
            java.lang.String r9 = ""
            r17 = r5
            r16 = r9
            r5 = 0
            r9 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r12 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r14 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L_0x0027:
            if (r5 >= r4) goto L_0x00af
            java.lang.String r6 = r1.getAttributeName(r5)
            java.lang.String r7 = r1.getAttributeValue(r5)
            int r18 = r6.hashCode()
            switch(r18) {
                case -934795532: goto L_0x0061;
                case 99841: goto L_0x0057;
                case 100571: goto L_0x004d;
                case 93616297: goto L_0x0043;
                case 109780401: goto L_0x0039;
                default: goto L_0x0038;
            }
        L_0x0038:
            goto L_0x006b
        L_0x0039:
            java.lang.String r8 = "style"
            boolean r6 = r6.equals(r8)
            if (r6 == 0) goto L_0x006b
            r6 = 3
            goto L_0x006c
        L_0x0043:
            java.lang.String r8 = "begin"
            boolean r6 = r6.equals(r8)
            if (r6 == 0) goto L_0x006b
            r6 = 0
            goto L_0x006c
        L_0x004d:
            java.lang.String r8 = "end"
            boolean r6 = r6.equals(r8)
            if (r6 == 0) goto L_0x006b
            r6 = 1
            goto L_0x006c
        L_0x0057:
            java.lang.String r8 = "dur"
            boolean r6 = r6.equals(r8)
            if (r6 == 0) goto L_0x006b
            r6 = 2
            goto L_0x006c
        L_0x0061:
            java.lang.String r8 = "region"
            boolean r6 = r6.equals(r8)
            if (r6 == 0) goto L_0x006b
            r6 = 4
            goto L_0x006c
        L_0x006b:
            r6 = -1
        L_0x006c:
            if (r6 == 0) goto L_0x00a4
            r8 = 1
            if (r6 == r8) goto L_0x009c
            r8 = 2
            if (r6 == r8) goto L_0x0094
            r8 = 3
            if (r6 == r8) goto L_0x0088
            r8 = 4
            if (r6 == r8) goto L_0x007d
            r6 = r22
            goto L_0x00ab
        L_0x007d:
            r6 = r22
            boolean r8 = r6.containsKey(r7)
            if (r8 == 0) goto L_0x00ab
            r16 = r7
            goto L_0x00ab
        L_0x0088:
            r6 = r22
            java.lang.String[] r7 = r0.b(r7)
            int r8 = r7.length
            if (r8 <= 0) goto L_0x00ab
            r17 = r7
            goto L_0x00ab
        L_0x0094:
            r6 = r22
            long r7 = a((java.lang.String) r7, (com.google.android.exoplayer2.text.ttml.TtmlDecoder.FrameAndTickRate) r3)
            r14 = r7
            goto L_0x00ab
        L_0x009c:
            r6 = r22
            long r7 = a((java.lang.String) r7, (com.google.android.exoplayer2.text.ttml.TtmlDecoder.FrameAndTickRate) r3)
            r12 = r7
            goto L_0x00ab
        L_0x00a4:
            r6 = r22
            long r7 = a((java.lang.String) r7, (com.google.android.exoplayer2.text.ttml.TtmlDecoder.FrameAndTickRate) r3)
            r9 = r7
        L_0x00ab:
            int r5 = r5 + 1
            goto L_0x0027
        L_0x00af:
            if (r2 == 0) goto L_0x00c9
            long r3 = r2.d
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00ce
            int r7 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00c1
            long r9 = r9 + r3
        L_0x00c1:
            int r3 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x00ce
            long r3 = r2.d
            long r12 = r12 + r3
            goto L_0x00ce
        L_0x00c9:
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L_0x00ce:
            r7 = r9
            int r3 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r3 != 0) goto L_0x00e4
            int r3 = (r14 > r5 ? 1 : (r14 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x00da
            long r14 = r14 + r7
            r9 = r14
            goto L_0x00e5
        L_0x00da:
            if (r2 == 0) goto L_0x00e4
            long r2 = r2.e
            int r4 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r4 == 0) goto L_0x00e4
            r9 = r2
            goto L_0x00e5
        L_0x00e4:
            r9 = r12
        L_0x00e5:
            java.lang.String r6 = r20.getName()
            r12 = r17
            r13 = r16
            com.google.android.exoplayer2.text.ttml.TtmlNode r1 = com.google.android.exoplayer2.text.ttml.TtmlNode.a(r6, r7, r9, r11, r12, r13)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ttml.TtmlDecoder.a(org.xmlpull.v1.XmlPullParser, com.google.android.exoplayer2.text.ttml.TtmlNode, java.util.Map, com.google.android.exoplayer2.text.ttml.TtmlDecoder$FrameAndTickRate):com.google.android.exoplayer2.text.ttml.TtmlNode");
    }

    private static boolean a(String str) {
        return str.equals("tt") || str.equals("head") || str.equals("body") || str.equals("div") || str.equals("p") || str.equals("span") || str.equals("br") || str.equals("style") || str.equals("styling") || str.equals("layout") || str.equals("region") || str.equals("metadata") || str.equals("smpte:image") || str.equals("smpte:data") || str.equals("smpte:information");
    }

    private static void a(String str, TtmlStyle ttmlStyle) throws SubtitleDecoderException {
        Matcher matcher;
        String[] a2 = Util.a(str, "\\s+");
        if (a2.length == 1) {
            matcher = r.matcher(str);
        } else if (a2.length == 2) {
            matcher = r.matcher(a2[1]);
            Log.d("TtmlDecoder", "Multiple values in fontSize attribute. Picking the second value for vertical font size and ignoring the first.");
        } else {
            throw new SubtitleDecoderException("Invalid number of entries for fontSize: " + a2.length + ".");
        }
        if (matcher.matches()) {
            String group = matcher.group(3);
            char c = 65535;
            int hashCode = group.hashCode();
            if (hashCode != 37) {
                if (hashCode != 3240) {
                    if (hashCode == 3592 && group.equals("px")) {
                        c = 0;
                    }
                } else if (group.equals("em")) {
                    c = 1;
                }
            } else if (group.equals("%")) {
                c = 2;
            }
            if (c == 0) {
                ttmlStyle.c(1);
            } else if (c == 1) {
                ttmlStyle.c(2);
            } else if (c == 2) {
                ttmlStyle.c(3);
            } else {
                throw new SubtitleDecoderException("Invalid unit for fontSize: '" + group + "'.");
            }
            ttmlStyle.a(Float.valueOf(matcher.group(1)).floatValue());
            return;
        }
        throw new SubtitleDecoderException("Invalid expression for fontSize: '" + str + "'.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0108  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long a(java.lang.String r14, com.google.android.exoplayer2.text.ttml.TtmlDecoder.FrameAndTickRate r15) throws com.google.android.exoplayer2.text.SubtitleDecoderException {
        /*
            java.util.regex.Pattern r0 = p
            java.util.regex.Matcher r0 = r0.matcher(r14)
            boolean r1 = r0.matches()
            r2 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            r4 = 5
            r5 = 4
            r6 = 3
            r7 = 2
            r8 = 1
            if (r1 == 0) goto L_0x0076
            java.lang.String r14 = r0.group(r8)
            long r8 = java.lang.Long.parseLong(r14)
            r10 = 3600(0xe10, double:1.7786E-320)
            long r8 = r8 * r10
            double r8 = (double) r8
            java.lang.String r14 = r0.group(r7)
            long r10 = java.lang.Long.parseLong(r14)
            r12 = 60
            long r10 = r10 * r12
            double r10 = (double) r10
            double r8 = r8 + r10
            java.lang.String r14 = r0.group(r6)
            long r6 = java.lang.Long.parseLong(r14)
            double r6 = (double) r6
            double r8 = r8 + r6
            java.lang.String r14 = r0.group(r5)
            r5 = 0
            if (r14 == 0) goto L_0x0048
            double r10 = java.lang.Double.parseDouble(r14)
            goto L_0x0049
        L_0x0048:
            r10 = r5
        L_0x0049:
            double r8 = r8 + r10
            java.lang.String r14 = r0.group(r4)
            if (r14 == 0) goto L_0x005a
            long r10 = java.lang.Long.parseLong(r14)
            float r14 = (float) r10
            float r1 = r15.f3539a
            float r14 = r14 / r1
            double r10 = (double) r14
            goto L_0x005b
        L_0x005a:
            r10 = r5
        L_0x005b:
            double r8 = r8 + r10
            r14 = 6
            java.lang.String r14 = r0.group(r14)
            if (r14 == 0) goto L_0x0071
            long r0 = java.lang.Long.parseLong(r14)
            double r0 = (double) r0
            int r14 = r15.b
            double r4 = (double) r14
            double r0 = r0 / r4
            float r14 = r15.f3539a
            double r14 = (double) r14
            double r5 = r0 / r14
        L_0x0071:
            double r8 = r8 + r5
            double r8 = r8 * r2
            long r14 = (long) r8
            return r14
        L_0x0076:
            java.util.regex.Pattern r0 = q
            java.util.regex.Matcher r0 = r0.matcher(r14)
            boolean r1 = r0.matches()
            if (r1 == 0) goto L_0x0113
            java.lang.String r14 = r0.group(r8)
            double r9 = java.lang.Double.parseDouble(r14)
            java.lang.String r14 = r0.group(r7)
            r0 = -1
            int r1 = r14.hashCode()
            r11 = 102(0x66, float:1.43E-43)
            if (r1 == r11) goto L_0x00de
            r11 = 104(0x68, float:1.46E-43)
            if (r1 == r11) goto L_0x00d4
            r11 = 109(0x6d, float:1.53E-43)
            if (r1 == r11) goto L_0x00ca
            r11 = 3494(0xda6, float:4.896E-42)
            if (r1 == r11) goto L_0x00c0
            r11 = 115(0x73, float:1.61E-43)
            if (r1 == r11) goto L_0x00b6
            r11 = 116(0x74, float:1.63E-43)
            if (r1 == r11) goto L_0x00ac
            goto L_0x00e8
        L_0x00ac:
            java.lang.String r1 = "t"
            boolean r14 = r14.equals(r1)
            if (r14 == 0) goto L_0x00e8
            r14 = 5
            goto L_0x00e9
        L_0x00b6:
            java.lang.String r1 = "s"
            boolean r14 = r14.equals(r1)
            if (r14 == 0) goto L_0x00e8
            r14 = 2
            goto L_0x00e9
        L_0x00c0:
            java.lang.String r1 = "ms"
            boolean r14 = r14.equals(r1)
            if (r14 == 0) goto L_0x00e8
            r14 = 3
            goto L_0x00e9
        L_0x00ca:
            java.lang.String r1 = "m"
            boolean r14 = r14.equals(r1)
            if (r14 == 0) goto L_0x00e8
            r14 = 1
            goto L_0x00e9
        L_0x00d4:
            java.lang.String r1 = "h"
            boolean r14 = r14.equals(r1)
            if (r14 == 0) goto L_0x00e8
            r14 = 0
            goto L_0x00e9
        L_0x00de:
            java.lang.String r1 = "f"
            boolean r14 = r14.equals(r1)
            if (r14 == 0) goto L_0x00e8
            r14 = 4
            goto L_0x00e9
        L_0x00e8:
            r14 = -1
        L_0x00e9:
            if (r14 == 0) goto L_0x0108
            if (r14 == r8) goto L_0x0105
            if (r14 == r7) goto L_0x010f
            if (r14 == r6) goto L_0x00fe
            if (r14 == r5) goto L_0x00fa
            if (r14 == r4) goto L_0x00f6
            goto L_0x010f
        L_0x00f6:
            int r14 = r15.c
            double r14 = (double) r14
            goto L_0x0103
        L_0x00fa:
            float r14 = r15.f3539a
            double r14 = (double) r14
            goto L_0x0103
        L_0x00fe:
            r14 = 4652007308841189376(0x408f400000000000, double:1000.0)
        L_0x0103:
            double r9 = r9 / r14
            goto L_0x010f
        L_0x0105:
            r14 = 4633641066610819072(0x404e000000000000, double:60.0)
            goto L_0x010d
        L_0x0108:
            r14 = 4660134898793709568(0x40ac200000000000, double:3600.0)
        L_0x010d:
            double r9 = r9 * r14
        L_0x010f:
            double r9 = r9 * r2
            long r14 = (long) r9
            return r14
        L_0x0113:
            com.google.android.exoplayer2.text.SubtitleDecoderException r15 = new com.google.android.exoplayer2.text.SubtitleDecoderException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Malformed time expression: "
            r0.append(r1)
            r0.append(r14)
            java.lang.String r14 = r0.toString()
            r15.<init>((java.lang.String) r14)
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ttml.TtmlDecoder.a(java.lang.String, com.google.android.exoplayer2.text.ttml.TtmlDecoder$FrameAndTickRate):long");
    }
}
