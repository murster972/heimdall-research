package com.google.android.exoplayer2.text.ttml;

import android.text.Layout;
import android.text.SpannableStringBuilder;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.util.Assertions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

final class TtmlNode {

    /* renamed from: a  reason: collision with root package name */
    public final String f3540a;
    public final String b;
    public final boolean c;
    public final long d;
    public final long e;
    public final TtmlStyle f;
    public final String g;
    private final String[] h;
    private final HashMap<String, Integer> i;
    private final HashMap<String, Integer> j;
    private List<TtmlNode> k;

    private TtmlNode(String str, String str2, long j2, long j3, TtmlStyle ttmlStyle, String[] strArr, String str3) {
        this.f3540a = str;
        this.b = str2;
        this.f = ttmlStyle;
        this.h = strArr;
        this.c = str2 != null;
        this.d = j2;
        this.e = j3;
        Assertions.a(str3);
        this.g = str3;
        this.i = new HashMap<>();
        this.j = new HashMap<>();
    }

    public static TtmlNode a(String str) {
        return new TtmlNode((String) null, TtmlRenderUtil.a(str), -9223372036854775807L, -9223372036854775807L, (TtmlStyle) null, (String[]) null, "");
    }

    public long[] b() {
        TreeSet treeSet = new TreeSet();
        int i2 = 0;
        a((TreeSet<Long>) treeSet, false);
        long[] jArr = new long[treeSet.size()];
        Iterator it2 = treeSet.iterator();
        while (it2.hasNext()) {
            jArr[i2] = ((Long) it2.next()).longValue();
            i2++;
        }
        return jArr;
    }

    public static TtmlNode a(String str, long j2, long j3, TtmlStyle ttmlStyle, String[] strArr, String str2) {
        return new TtmlNode(str, (String) null, j2, j3, ttmlStyle, strArr, str2);
    }

    public boolean a(long j2) {
        return (this.d == -9223372036854775807L && this.e == -9223372036854775807L) || (this.d <= j2 && this.e == -9223372036854775807L) || ((this.d == -9223372036854775807L && j2 < this.e) || (this.d <= j2 && j2 < this.e));
    }

    public void a(TtmlNode ttmlNode) {
        if (this.k == null) {
            this.k = new ArrayList();
        }
        this.k.add(ttmlNode);
    }

    private void b(long j2, Map<String, TtmlStyle> map, Map<String, SpannableStringBuilder> map2) {
        int i2;
        if (a(j2)) {
            Iterator<Map.Entry<String, Integer>> it2 = this.j.entrySet().iterator();
            while (true) {
                i2 = 0;
                if (!it2.hasNext()) {
                    break;
                }
                Map.Entry next = it2.next();
                String str = (String) next.getKey();
                if (this.i.containsKey(str)) {
                    i2 = this.i.get(str).intValue();
                }
                int intValue = ((Integer) next.getValue()).intValue();
                if (i2 != intValue) {
                    a(map, map2.get(str), i2, intValue);
                }
            }
            while (i2 < a()) {
                a(i2).b(j2, map, map2);
                i2++;
            }
        }
    }

    public TtmlNode a(int i2) {
        List<TtmlNode> list = this.k;
        if (list != null) {
            return list.get(i2);
        }
        throw new IndexOutOfBoundsException();
    }

    public int a() {
        List<TtmlNode> list = this.k;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    private void a(TreeSet<Long> treeSet, boolean z) {
        boolean equals = "p".equals(this.f3540a);
        if (z || equals) {
            long j2 = this.d;
            if (j2 != -9223372036854775807L) {
                treeSet.add(Long.valueOf(j2));
            }
            long j3 = this.e;
            if (j3 != -9223372036854775807L) {
                treeSet.add(Long.valueOf(j3));
            }
        }
        if (this.k != null) {
            for (int i2 = 0; i2 < this.k.size(); i2++) {
                this.k.get(i2).a(treeSet, z || equals);
            }
        }
    }

    public List<Cue> a(long j2, Map<String, TtmlStyle> map, Map<String, TtmlRegion> map2) {
        TreeMap treeMap = new TreeMap();
        a(j2, false, this.g, (Map<String, SpannableStringBuilder>) treeMap);
        b(j2, map, treeMap);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry : treeMap.entrySet()) {
            TtmlRegion ttmlRegion = map2.get(entry.getKey());
            SpannableStringBuilder spannableStringBuilder = (SpannableStringBuilder) entry.getValue();
            a(spannableStringBuilder);
            arrayList.add(new Cue((CharSequence) spannableStringBuilder, (Layout.Alignment) null, ttmlRegion.c, ttmlRegion.d, ttmlRegion.e, ttmlRegion.b, Integer.MIN_VALUE, ttmlRegion.f, ttmlRegion.g, ttmlRegion.h));
        }
        return arrayList;
    }

    private void a(long j2, boolean z, String str, Map<String, SpannableStringBuilder> map) {
        this.i.clear();
        this.j.clear();
        if (!"metadata".equals(this.f3540a)) {
            if (!"".equals(this.g)) {
                str = this.g;
            }
            if (this.c && z) {
                a(str, map).append(this.b);
            } else if ("br".equals(this.f3540a) && z) {
                a(str, map).append(10);
            } else if (a(j2)) {
                for (Map.Entry next : map.entrySet()) {
                    this.i.put(next.getKey(), Integer.valueOf(((SpannableStringBuilder) next.getValue()).length()));
                }
                boolean equals = "p".equals(this.f3540a);
                for (int i2 = 0; i2 < a(); i2++) {
                    a(i2).a(j2, z || equals, str, map);
                }
                if (equals) {
                    TtmlRenderUtil.a(a(str, map));
                }
                for (Map.Entry next2 : map.entrySet()) {
                    this.j.put(next2.getKey(), Integer.valueOf(((SpannableStringBuilder) next2.getValue()).length()));
                }
            }
        }
    }

    private static SpannableStringBuilder a(String str, Map<String, SpannableStringBuilder> map) {
        if (!map.containsKey(str)) {
            map.put(str, new SpannableStringBuilder());
        }
        return map.get(str);
    }

    private void a(Map<String, TtmlStyle> map, SpannableStringBuilder spannableStringBuilder, int i2, int i3) {
        TtmlStyle a2 = TtmlRenderUtil.a(this.f, this.h, map);
        if (a2 != null) {
            TtmlRenderUtil.a(spannableStringBuilder, i2, i3, a2);
        }
    }

    private SpannableStringBuilder a(SpannableStringBuilder spannableStringBuilder) {
        int i2;
        int i3;
        int i4 = 0;
        int length = spannableStringBuilder.length();
        for (int i5 = 0; i5 < length; i5++) {
            if (spannableStringBuilder.charAt(i5) == ' ') {
                int i6 = i5 + 1;
                int i7 = i6;
                while (i7 < spannableStringBuilder.length() && spannableStringBuilder.charAt(i7) == ' ') {
                    i7++;
                }
                int i8 = i7 - i6;
                if (i8 > 0) {
                    spannableStringBuilder.delete(i5, i5 + i8);
                    length -= i8;
                }
            }
        }
        if (length > 0 && spannableStringBuilder.charAt(0) == ' ') {
            spannableStringBuilder.delete(0, 1);
            length--;
        }
        int i9 = 0;
        while (true) {
            i2 = length - 1;
            if (i9 >= i2) {
                break;
            }
            if (spannableStringBuilder.charAt(i9) == 10) {
                int i10 = i9 + 1;
                if (spannableStringBuilder.charAt(i10) == ' ') {
                    spannableStringBuilder.delete(i10, i9 + 2);
                    length--;
                }
            }
            i9++;
        }
        if (length > 0 && spannableStringBuilder.charAt(i2) == ' ') {
            spannableStringBuilder.delete(i2, length);
            length--;
        }
        while (true) {
            i3 = length - 1;
            if (i4 >= i3) {
                break;
            }
            if (spannableStringBuilder.charAt(i4) == ' ') {
                int i11 = i4 + 1;
                if (spannableStringBuilder.charAt(i11) == 10) {
                    spannableStringBuilder.delete(i4, i11);
                    length--;
                }
            }
            i4++;
        }
        if (length > 0 && spannableStringBuilder.charAt(i3) == 10) {
            spannableStringBuilder.delete(i3, length);
        }
        return spannableStringBuilder;
    }
}
