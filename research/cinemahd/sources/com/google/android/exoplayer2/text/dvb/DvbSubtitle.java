package com.google.android.exoplayer2.text.dvb;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import java.util.List;

final class DvbSubtitle implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private final List<Cue> f3533a;

    public DvbSubtitle(List<Cue> list) {
        this.f3533a = list;
    }

    public int a() {
        return 1;
    }

    public int a(long j) {
        return -1;
    }

    public long a(int i) {
        return 0;
    }

    public List<Cue> b(long j) {
        return this.f3533a;
    }
}
