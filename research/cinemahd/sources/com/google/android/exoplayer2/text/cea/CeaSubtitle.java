package com.google.android.exoplayer2.text.cea;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Collections;
import java.util.List;

final class CeaSubtitle implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private final List<Cue> f3522a;

    public CeaSubtitle(List<Cue> list) {
        this.f3522a = list;
    }

    public int a() {
        return 1;
    }

    public int a(long j) {
        return j < 0 ? 0 : -1;
    }

    public long a(int i) {
        Assertions.a(i == 0);
        return 0;
    }

    public List<Cue> b(long j) {
        return j >= 0 ? this.f3522a : Collections.emptyList();
    }
}
