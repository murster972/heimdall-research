package com.google.android.exoplayer2.text.webvtt;

import android.text.TextUtils;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.exoplayer2.text.webvtt.WebvttCue;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.ArrayList;
import java.util.List;

public final class WebvttDecoder extends SimpleSubtitleDecoder {
    private final WebvttCueParser o = new WebvttCueParser();
    private final ParsableByteArray p = new ParsableByteArray();
    private final WebvttCue.Builder q = new WebvttCue.Builder();
    private final CssParser r = new CssParser();
    private final List<WebvttCssStyle> s = new ArrayList();

    public WebvttDecoder() {
        super("WebvttDecoder");
    }

    private static void b(ParsableByteArray parsableByteArray) {
        do {
        } while (!TextUtils.isEmpty(parsableByteArray.j()));
    }

    /* access modifiers changed from: protected */
    public WebvttSubtitle a(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        this.p.a(bArr, i);
        this.q.b();
        this.s.clear();
        try {
            WebvttParserUtil.c(this.p);
            do {
            } while (!TextUtils.isEmpty(this.p.j()));
            ArrayList arrayList = new ArrayList();
            while (true) {
                int a2 = a(this.p);
                if (a2 == 0) {
                    return new WebvttSubtitle(arrayList);
                }
                if (a2 == 1) {
                    b(this.p);
                } else if (a2 == 2) {
                    if (arrayList.isEmpty()) {
                        this.p.j();
                        WebvttCssStyle a3 = this.r.a(this.p);
                        if (a3 != null) {
                            this.s.add(a3);
                        }
                    } else {
                        throw new SubtitleDecoderException("A style block was found after the first cue.");
                    }
                } else if (a2 == 3 && this.o.a(this.p, this.q, this.s)) {
                    arrayList.add(this.q.a());
                    this.q.b();
                }
            }
        } catch (ParserException e) {
            throw new SubtitleDecoderException((Exception) e);
        }
    }

    private static int a(ParsableByteArray parsableByteArray) {
        int i = -1;
        int i2 = 0;
        while (i == -1) {
            i2 = parsableByteArray.c();
            String j = parsableByteArray.j();
            if (j == null) {
                i = 0;
            } else if ("STYLE".equals(j)) {
                i = 2;
            } else {
                i = j.startsWith("NOTE") ? 1 : 3;
            }
        }
        parsableByteArray.e(i2);
        return i;
    }
}
