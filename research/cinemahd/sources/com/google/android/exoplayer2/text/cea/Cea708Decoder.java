package com.google.android.exoplayer2.text.cea;

import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.text.SubtitleInputBuffer;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import okhttp3.internal.ws.WebSocketProtocol;

public final class Cea708Decoder extends CeaDecoder {
    private final ParsableByteArray g = new ParsableByteArray();
    private final ParsableBitArray h = new ParsableBitArray();
    private final int i;
    private final CueBuilder[] j;
    private CueBuilder k;
    private List<Cue> l;
    private List<Cue> m;
    private DtvCcPacket n;
    private int o;

    private static final class CueBuilder {
        private static final int[] A = {0, 0, 0, 0, 0, 0, 2};
        private static final int[] B = {3, 3, 3, 3, 3, 3, 1};
        private static final boolean[] C = {false, false, false, true, true, true, false};
        private static final int[] D;
        private static final int[] E = {0, 1, 2, 3, 4, 3, 4};
        private static final int[] F = {0, 0, 0, 0, 0, 3, 3};
        private static final int[] G;
        public static final int w = a(2, 2, 2, 0);
        public static final int x = a(0, 0, 0, 0);
        public static final int y = a(0, 0, 0, 3);
        private static final int[] z = {0, 0, 0, 0, 0, 2, 0};

        /* renamed from: a  reason: collision with root package name */
        private final List<SpannableString> f3519a = new ArrayList();
        private final SpannableStringBuilder b = new SpannableStringBuilder();
        private boolean c;
        private boolean d;
        private int e;
        private boolean f;
        private int g;
        private int h;
        private int i;
        private int j;
        private boolean k;
        private int l;
        private int m;
        private int n;
        private int o;
        private int p;
        private int q;
        private int r;
        private int s;
        private int t;
        private int u;
        private int v;

        static {
            int i2 = x;
            int i3 = y;
            D = new int[]{i2, i3, i2, i2, i3, i2, i2};
            G = new int[]{i2, i2, i2, i2, i2, i3, i3};
        }

        public CueBuilder() {
            h();
        }

        public void a(boolean z2) {
            this.d = z2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:19:0x0065  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0070  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0091  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0093  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x009f  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00a1  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00ad  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.google.android.exoplayer2.text.cea.Cea708Cue b() {
            /*
                r15 = this;
                boolean r0 = r15.f()
                if (r0 == 0) goto L_0x0008
                r0 = 0
                return r0
            L_0x0008:
                android.text.SpannableStringBuilder r2 = new android.text.SpannableStringBuilder
                r2.<init>()
                r0 = 0
                r1 = 0
            L_0x000f:
                java.util.List<android.text.SpannableString> r3 = r15.f3519a
                int r3 = r3.size()
                if (r1 >= r3) goto L_0x002a
                java.util.List<android.text.SpannableString> r3 = r15.f3519a
                java.lang.Object r3 = r3.get(r1)
                java.lang.CharSequence r3 = (java.lang.CharSequence) r3
                r2.append(r3)
                r3 = 10
                r2.append(r3)
                int r1 = r1 + 1
                goto L_0x000f
            L_0x002a:
                android.text.SpannableString r1 = r15.c()
                r2.append(r1)
                int r1 = r15.l
                r3 = 2
                r4 = 3
                r5 = 1
                if (r1 == 0) goto L_0x005e
                if (r1 == r5) goto L_0x005b
                if (r1 == r3) goto L_0x0058
                if (r1 != r4) goto L_0x003f
                goto L_0x005e
            L_0x003f:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Unexpected justification value: "
                r1.append(r2)
                int r2 = r15.l
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x0058:
                android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
                goto L_0x0060
            L_0x005b:
                android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_OPPOSITE
                goto L_0x0060
            L_0x005e:
                android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_NORMAL
            L_0x0060:
                r6 = r1
                boolean r1 = r15.f
                if (r1 == 0) goto L_0x0070
                int r1 = r15.h
                float r1 = (float) r1
                r7 = 1120272384(0x42c60000, float:99.0)
                float r1 = r1 / r7
                int r8 = r15.g
                float r8 = (float) r8
                float r8 = r8 / r7
                goto L_0x007d
            L_0x0070:
                int r1 = r15.h
                float r1 = (float) r1
                r7 = 1129381888(0x43510000, float:209.0)
                float r1 = r1 / r7
                int r7 = r15.g
                float r7 = (float) r7
                r8 = 1116995584(0x42940000, float:74.0)
                float r8 = r7 / r8
            L_0x007d:
                r7 = 1063675494(0x3f666666, float:0.9)
                float r1 = r1 * r7
                r9 = 1028443341(0x3d4ccccd, float:0.05)
                float r10 = r1 + r9
                float r8 = r8 * r7
                float r7 = r8 + r9
                int r1 = r15.i
                int r8 = r1 % 3
                if (r8 != 0) goto L_0x0093
                r8 = 0
                goto L_0x0099
            L_0x0093:
                int r1 = r1 % r4
                if (r1 != r5) goto L_0x0098
                r8 = 1
                goto L_0x0099
            L_0x0098:
                r8 = 2
            L_0x0099:
                int r1 = r15.i
                int r9 = r1 / 3
                if (r9 != 0) goto L_0x00a1
                r9 = 0
                goto L_0x00a7
            L_0x00a1:
                int r1 = r1 / r4
                if (r1 != r5) goto L_0x00a6
                r9 = 1
                goto L_0x00a7
            L_0x00a6:
                r9 = 2
            L_0x00a7:
                int r1 = r15.o
                int r3 = x
                if (r1 == r3) goto L_0x00ae
                r0 = 1
            L_0x00ae:
                com.google.android.exoplayer2.text.cea.Cea708Cue r13 = new com.google.android.exoplayer2.text.cea.Cea708Cue
                r5 = 0
                r11 = 1
                int r12 = r15.o
                int r14 = r15.e
                r1 = r13
                r3 = r6
                r4 = r7
                r6 = r8
                r7 = r10
                r8 = r9
                r9 = r11
                r10 = r0
                r11 = r12
                r12 = r14
                r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.cea.Cea708Decoder.CueBuilder.b():com.google.android.exoplayer2.text.cea.Cea708Cue");
        }

        public SpannableString c() {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.b);
            int length = spannableStringBuilder.length();
            if (length > 0) {
                if (this.p != -1) {
                    spannableStringBuilder.setSpan(new StyleSpan(2), this.p, length, 33);
                }
                if (this.q != -1) {
                    spannableStringBuilder.setSpan(new UnderlineSpan(), this.q, length, 33);
                }
                if (this.r != -1) {
                    spannableStringBuilder.setSpan(new ForegroundColorSpan(this.s), this.r, length, 33);
                }
                if (this.t != -1) {
                    spannableStringBuilder.setSpan(new BackgroundColorSpan(this.u), this.t, length, 33);
                }
            }
            return new SpannableString(spannableStringBuilder);
        }

        public void d() {
            this.f3519a.clear();
            this.b.clear();
            this.p = -1;
            this.q = -1;
            this.r = -1;
            this.t = -1;
            this.v = 0;
        }

        public boolean e() {
            return this.c;
        }

        public boolean f() {
            return !e() || (this.f3519a.isEmpty() && this.b.length() == 0);
        }

        public boolean g() {
            return this.d;
        }

        public void h() {
            d();
            this.c = false;
            this.d = false;
            this.e = 4;
            this.f = false;
            this.g = 0;
            this.h = 0;
            this.i = 0;
            this.j = 15;
            this.k = true;
            this.l = 0;
            this.m = 0;
            this.n = 0;
            int i2 = x;
            this.o = i2;
            this.s = w;
            this.u = i2;
        }

        public void a(boolean z2, boolean z3, boolean z4, int i2, boolean z5, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
            boolean z6 = z3;
            int i10 = i8;
            int i11 = i9;
            this.c = true;
            this.d = z2;
            this.k = z6;
            this.e = i2;
            this.f = z5;
            this.g = i3;
            this.h = i4;
            this.i = i7;
            int i12 = i5 + 1;
            if (this.j != i12) {
                this.j = i12;
                while (true) {
                    if ((!z6 || this.f3519a.size() < this.j) && this.f3519a.size() < 15) {
                        break;
                    }
                    this.f3519a.remove(0);
                }
            }
            if (!(i10 == 0 || this.m == i10)) {
                this.m = i10;
                int i13 = i10 - 1;
                a(D[i13], y, C[i13], 0, A[i13], B[i13], z[i13]);
            }
            if (i11 != 0 && this.n != i11) {
                this.n = i11;
                int i14 = i11 - 1;
                a(0, 1, 1, false, false, F[i14], E[i14]);
                a(w, G[i14], x);
            }
        }

        public void a(int i2, int i3, boolean z2, int i4, int i5, int i6, int i7) {
            this.o = i2;
            this.l = i7;
        }

        public static int b(int i2, int i3, int i4) {
            return a(i2, i3, i4, 0);
        }

        public void a(int i2, int i3, int i4, boolean z2, boolean z3, int i5, int i6) {
            if (this.p != -1) {
                if (!z2) {
                    this.b.setSpan(new StyleSpan(2), this.p, this.b.length(), 33);
                    this.p = -1;
                }
            } else if (z2) {
                this.p = this.b.length();
            }
            if (this.q != -1) {
                if (!z3) {
                    this.b.setSpan(new UnderlineSpan(), this.q, this.b.length(), 33);
                    this.q = -1;
                }
            } else if (z3) {
                this.q = this.b.length();
            }
        }

        public void a(int i2, int i3, int i4) {
            int i5;
            int i6;
            if (!(this.r == -1 || (i6 = this.s) == i2)) {
                this.b.setSpan(new ForegroundColorSpan(i6), this.r, this.b.length(), 33);
            }
            if (i2 != w) {
                this.r = this.b.length();
                this.s = i2;
            }
            if (!(this.t == -1 || (i5 = this.u) == i3)) {
                this.b.setSpan(new BackgroundColorSpan(i5), this.t, this.b.length(), 33);
            }
            if (i3 != x) {
                this.t = this.b.length();
                this.u = i3;
            }
        }

        public void a(int i2, int i3) {
            if (this.v != i2) {
                a(10);
            }
            this.v = i2;
        }

        public void a() {
            int length = this.b.length();
            if (length > 0) {
                this.b.delete(length - 1, length);
            }
        }

        public void a(char c2) {
            if (c2 == 10) {
                this.f3519a.add(c());
                this.b.clear();
                if (this.p != -1) {
                    this.p = 0;
                }
                if (this.q != -1) {
                    this.q = 0;
                }
                if (this.r != -1) {
                    this.r = 0;
                }
                if (this.t != -1) {
                    this.t = 0;
                }
                while (true) {
                    if ((this.k && this.f3519a.size() >= this.j) || this.f3519a.size() >= 15) {
                        this.f3519a.remove(0);
                    } else {
                        return;
                    }
                }
            } else {
                this.b.append(c2);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:11:0x0024  */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x002a  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x002d  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0030  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static int a(int r4, int r5, int r6, int r7) {
            /*
                r0 = 4
                r1 = 0
                com.google.android.exoplayer2.util.Assertions.a(r4, r1, r0)
                com.google.android.exoplayer2.util.Assertions.a(r5, r1, r0)
                com.google.android.exoplayer2.util.Assertions.a(r6, r1, r0)
                com.google.android.exoplayer2.util.Assertions.a(r7, r1, r0)
                r0 = 1
                r2 = 255(0xff, float:3.57E-43)
                if (r7 == 0) goto L_0x001b
                if (r7 == r0) goto L_0x001b
                r3 = 2
                if (r7 == r3) goto L_0x0020
                r3 = 3
                if (r7 == r3) goto L_0x001e
            L_0x001b:
                r7 = 255(0xff, float:3.57E-43)
                goto L_0x0022
            L_0x001e:
                r7 = 0
                goto L_0x0022
            L_0x0020:
                r7 = 127(0x7f, float:1.78E-43)
            L_0x0022:
                if (r4 <= r0) goto L_0x0027
                r4 = 255(0xff, float:3.57E-43)
                goto L_0x0028
            L_0x0027:
                r4 = 0
            L_0x0028:
                if (r5 <= r0) goto L_0x002d
                r5 = 255(0xff, float:3.57E-43)
                goto L_0x002e
            L_0x002d:
                r5 = 0
            L_0x002e:
                if (r6 <= r0) goto L_0x0032
                r1 = 255(0xff, float:3.57E-43)
            L_0x0032:
                int r4 = android.graphics.Color.argb(r7, r4, r5, r1)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.cea.Cea708Decoder.CueBuilder.a(int, int, int, int):int");
        }
    }

    private static final class DtvCcPacket {

        /* renamed from: a  reason: collision with root package name */
        public final int f3520a;
        public final int b;
        public final byte[] c;
        int d = 0;

        public DtvCcPacket(int i, int i2) {
            this.f3520a = i;
            this.b = i2;
            this.c = new byte[((i2 * 2) - 1)];
        }
    }

    public Cea708Decoder(int i2, List<byte[]> list) {
        this.i = i2 == -1 ? 1 : i2;
        this.j = new CueBuilder[8];
        for (int i3 = 0; i3 < 8; i3++) {
            this.j[i3] = new CueBuilder();
        }
        this.k = this.j[0];
        l();
    }

    private void e() {
        if (this.n != null) {
            k();
            this.n = null;
        }
    }

    private void f(int i2) {
        if (i2 == 127) {
            this.k.a(9835);
        } else {
            this.k.a((char) (i2 & JfifUtil.MARKER_FIRST_BYTE));
        }
    }

    private void g(int i2) {
        this.k.a((char) (i2 & JfifUtil.MARKER_FIRST_BYTE));
    }

    private void h(int i2) {
        if (i2 == 32) {
            this.k.a(' ');
        } else if (i2 == 33) {
            this.k.a(160);
        } else if (i2 == 37) {
            this.k.a(8230);
        } else if (i2 == 42) {
            this.k.a(352);
        } else if (i2 == 44) {
            this.k.a(338);
        } else if (i2 == 63) {
            this.k.a(376);
        } else if (i2 == 57) {
            this.k.a(8482);
        } else if (i2 == 58) {
            this.k.a(353);
        } else if (i2 == 60) {
            this.k.a(339);
        } else if (i2 != 61) {
            switch (i2) {
                case 48:
                    this.k.a(9608);
                    return;
                case 49:
                    this.k.a(8216);
                    return;
                case 50:
                    this.k.a(8217);
                    return;
                case 51:
                    this.k.a(8220);
                    return;
                case 52:
                    this.k.a(8221);
                    return;
                case 53:
                    this.k.a(8226);
                    return;
                default:
                    switch (i2) {
                        case 118:
                            this.k.a(8539);
                            return;
                        case 119:
                            this.k.a(8540);
                            return;
                        case 120:
                            this.k.a(8541);
                            return;
                        case 121:
                            this.k.a(8542);
                            return;
                        case 122:
                            this.k.a(9474);
                            return;
                        case 123:
                            this.k.a(9488);
                            return;
                        case 124:
                            this.k.a(9492);
                            return;
                        case 125:
                            this.k.a(9472);
                            return;
                        case WebSocketProtocol.PAYLOAD_SHORT /*126*/:
                            this.k.a(9496);
                            return;
                        case 127:
                            this.k.a(9484);
                            return;
                        default:
                            Log.d("Cea708Decoder", "Invalid G2 character: " + i2);
                            return;
                    }
            }
        } else {
            this.k.a(8480);
        }
    }

    private void i(int i2) {
        if (i2 == 160) {
            this.k.a(13252);
            return;
        }
        Log.d("Cea708Decoder", "Invalid G3 character: " + i2);
        this.k.a('_');
    }

    private void j() {
        int a2 = CueBuilder.a(this.h.a(2), this.h.a(2), this.h.a(2), this.h.a(2));
        int a3 = this.h.a(2);
        int b = CueBuilder.b(this.h.a(2), this.h.a(2), this.h.a(2));
        if (this.h.e()) {
            a3 |= 4;
        }
        boolean e = this.h.e();
        int a4 = this.h.a(2);
        int a5 = this.h.a(2);
        int a6 = this.h.a(2);
        this.h.c(8);
        this.k.a(a2, b, e, a3, a4, a5, a6);
    }

    private void k() {
        DtvCcPacket dtvCcPacket = this.n;
        int i2 = dtvCcPacket.d;
        if (i2 != (dtvCcPacket.b * 2) - 1) {
            Log.d("Cea708Decoder", "DtvCcPacket ended prematurely; size is " + ((this.n.b * 2) - 1) + ", but current index is " + this.n.d + " (sequence number " + this.n.f3520a + "); ignoring packet");
            return;
        }
        this.h.a(dtvCcPacket.c, i2);
        int a2 = this.h.a(3);
        int a3 = this.h.a(5);
        if (a2 == 7) {
            this.h.c(2);
            a2 = this.h.a(6);
            if (a2 < 7) {
                Log.d("Cea708Decoder", "Invalid extended service number: " + a2);
            }
        }
        if (a3 == 0) {
            if (a2 != 0) {
                Log.d("Cea708Decoder", "serviceNumber is non-zero (" + a2 + ") when blockSize is 0");
            }
        } else if (a2 == this.i) {
            boolean z = false;
            while (this.h.a() > 0) {
                int a4 = this.h.a(8);
                if (a4 == 16) {
                    int a5 = this.h.a(8);
                    if (a5 <= 31) {
                        c(a5);
                    } else if (a5 <= 127) {
                        h(a5);
                    } else if (a5 <= 159) {
                        d(a5);
                    } else if (a5 <= 255) {
                        i(a5);
                    } else {
                        Log.d("Cea708Decoder", "Invalid extended command: " + a5);
                    }
                } else if (a4 <= 31) {
                    a(a4);
                } else if (a4 <= 127) {
                    f(a4);
                } else if (a4 <= 159) {
                    b(a4);
                } else if (a4 <= 255) {
                    g(a4);
                } else {
                    Log.d("Cea708Decoder", "Invalid base command: " + a4);
                }
                z = true;
            }
            if (z) {
                this.l = f();
            }
        }
    }

    private void l() {
        for (int i2 = 0; i2 < 8; i2++) {
            this.j[i2].h();
        }
    }

    /* access modifiers changed from: protected */
    public Subtitle c() {
        List<Cue> list = this.l;
        this.m = list;
        return new CeaSubtitle(list);
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return this.l != this.m;
    }

    public void flush() {
        super.flush();
        this.l = null;
        this.m = null;
        this.o = 0;
        this.k = this.j[this.o];
        l();
        this.n = null;
    }

    private void d(int i2) {
        if (i2 <= 135) {
            this.h.c(32);
        } else if (i2 <= 143) {
            this.h.c(40);
        } else if (i2 <= 159) {
            this.h.c(2);
            this.h.c(this.h.a(6) * 8);
        }
    }

    private void g() {
        this.k.a(this.h.a(4), this.h.a(2), this.h.a(2), this.h.e(), this.h.e(), this.h.a(3), this.h.a(3));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0093, code lost:
        if (r2 > 8) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009b, code lost:
        if (r4.h.e() == false) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x009d, code lost:
        r4.j[8 - r2].h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a6, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c5, code lost:
        if (r2 > 8) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cd, code lost:
        if (r4.h.e() == false) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00cf, code lost:
        r4.j[8 - r2].a(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00d9, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f3, code lost:
        if (r2 > 8) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fb, code lost:
        if (r4.h.e() == false) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00fd, code lost:
        r4.j[8 - r2].d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0106, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r5) {
        /*
            r4 = this;
            r0 = 16
            r1 = 8
            r2 = 1
            switch(r5) {
                case 128: goto L_0x0109;
                case 129: goto L_0x0109;
                case 130: goto L_0x0109;
                case 131: goto L_0x0109;
                case 132: goto L_0x0109;
                case 133: goto L_0x0109;
                case 134: goto L_0x0109;
                case 135: goto L_0x0109;
                case 136: goto L_0x00f3;
                case 137: goto L_0x00dc;
                case 138: goto L_0x00c5;
                case 139: goto L_0x00a9;
                case 140: goto L_0x0093;
                case 141: goto L_0x008c;
                case 142: goto L_0x0117;
                case 143: goto L_0x0087;
                case 144: goto L_0x0073;
                case 145: goto L_0x005d;
                case 146: goto L_0x0049;
                case 147: goto L_0x0008;
                case 148: goto L_0x0008;
                case 149: goto L_0x0008;
                case 150: goto L_0x0008;
                case 151: goto L_0x0033;
                case 152: goto L_0x0020;
                case 153: goto L_0x0020;
                case 154: goto L_0x0020;
                case 155: goto L_0x0020;
                case 156: goto L_0x0020;
                case 157: goto L_0x0020;
                case 158: goto L_0x0020;
                case 159: goto L_0x0020;
                default: goto L_0x0008;
            }
        L_0x0008:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Invalid C1 command: "
            r0.append(r1)
            r0.append(r5)
            java.lang.String r5 = r0.toString()
            java.lang.String r0 = "Cea708Decoder"
            com.google.android.exoplayer2.util.Log.d(r0, r5)
            goto L_0x0117
        L_0x0020:
            int r5 = r5 + -152
            r4.e(r5)
            int r0 = r4.o
            if (r0 == r5) goto L_0x0117
            r4.o = r5
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder[] r0 = r4.j
            r5 = r0[r5]
            r4.k = r5
            goto L_0x0117
        L_0x0033:
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder r5 = r4.k
            boolean r5 = r5.e()
            if (r5 != 0) goto L_0x0044
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            r0 = 32
            r5.c(r0)
            goto L_0x0117
        L_0x0044:
            r4.j()
            goto L_0x0117
        L_0x0049:
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder r5 = r4.k
            boolean r5 = r5.e()
            if (r5 != 0) goto L_0x0058
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            r5.c(r0)
            goto L_0x0117
        L_0x0058:
            r4.i()
            goto L_0x0117
        L_0x005d:
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder r5 = r4.k
            boolean r5 = r5.e()
            if (r5 != 0) goto L_0x006e
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            r0 = 24
            r5.c(r0)
            goto L_0x0117
        L_0x006e:
            r4.h()
            goto L_0x0117
        L_0x0073:
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder r5 = r4.k
            boolean r5 = r5.e()
            if (r5 != 0) goto L_0x0082
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            r5.c(r0)
            goto L_0x0117
        L_0x0082:
            r4.g()
            goto L_0x0117
        L_0x0087:
            r4.l()
            goto L_0x0117
        L_0x008c:
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            r5.c(r1)
            goto L_0x0117
        L_0x0093:
            if (r2 > r1) goto L_0x0117
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            boolean r5 = r5.e()
            if (r5 == 0) goto L_0x00a6
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder[] r5 = r4.j
            int r0 = 8 - r2
            r5 = r5[r0]
            r5.h()
        L_0x00a6:
            int r2 = r2 + 1
            goto L_0x0093
        L_0x00a9:
            r5 = 1
        L_0x00aa:
            if (r5 > r1) goto L_0x0117
            com.google.android.exoplayer2.util.ParsableBitArray r0 = r4.h
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x00c2
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder[] r0 = r4.j
            int r3 = 8 - r5
            r0 = r0[r3]
            boolean r3 = r0.g()
            r3 = r3 ^ r2
            r0.a((boolean) r3)
        L_0x00c2:
            int r5 = r5 + 1
            goto L_0x00aa
        L_0x00c5:
            if (r2 > r1) goto L_0x0117
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            boolean r5 = r5.e()
            if (r5 == 0) goto L_0x00d9
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder[] r5 = r4.j
            int r0 = 8 - r2
            r5 = r5[r0]
            r0 = 0
            r5.a((boolean) r0)
        L_0x00d9:
            int r2 = r2 + 1
            goto L_0x00c5
        L_0x00dc:
            r5 = 1
        L_0x00dd:
            if (r5 > r1) goto L_0x0117
            com.google.android.exoplayer2.util.ParsableBitArray r0 = r4.h
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x00f0
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder[] r0 = r4.j
            int r3 = 8 - r5
            r0 = r0[r3]
            r0.a((boolean) r2)
        L_0x00f0:
            int r5 = r5 + 1
            goto L_0x00dd
        L_0x00f3:
            if (r2 > r1) goto L_0x0117
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r4.h
            boolean r5 = r5.e()
            if (r5 == 0) goto L_0x0106
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder[] r5 = r4.j
            int r0 = 8 - r2
            r5 = r5[r0]
            r5.d()
        L_0x0106:
            int r2 = r2 + 1
            goto L_0x00f3
        L_0x0109:
            int r5 = r5 + -128
            int r0 = r4.o
            if (r0 == r5) goto L_0x0117
            r4.o = r5
            com.google.android.exoplayer2.text.cea.Cea708Decoder$CueBuilder[] r0 = r4.j
            r5 = r0[r5]
            r4.k = r5
        L_0x0117:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.cea.Cea708Decoder.b(int):void");
    }

    private void c(int i2) {
        if (i2 > 7) {
            if (i2 <= 15) {
                this.h.c(8);
            } else if (i2 <= 23) {
                this.h.c(16);
            } else if (i2 <= 31) {
                this.h.c(24);
            }
        }
    }

    private List<Cue> f() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < 8; i2++) {
            if (!this.j[i2].f() && this.j[i2].g()) {
                arrayList.add(this.j[i2].b());
            }
        }
        Collections.sort(arrayList);
        return Collections.unmodifiableList(arrayList);
    }

    /* access modifiers changed from: protected */
    public void a(SubtitleInputBuffer subtitleInputBuffer) {
        this.g.a(subtitleInputBuffer.b.array(), subtitleInputBuffer.b.limit());
        while (this.g.a() >= 3) {
            int t = this.g.t() & 7;
            int i2 = t & 3;
            boolean z = false;
            boolean z2 = (t & 4) == 4;
            byte t2 = (byte) this.g.t();
            byte t3 = (byte) this.g.t();
            if ((i2 == 2 || i2 == 3) && z2) {
                if (i2 == 3) {
                    e();
                    int i3 = (t2 & 192) >> 6;
                    byte b = t2 & 63;
                    if (b == 0) {
                        b = 64;
                    }
                    this.n = new DtvCcPacket(i3, b);
                    DtvCcPacket dtvCcPacket = this.n;
                    byte[] bArr = dtvCcPacket.c;
                    int i4 = dtvCcPacket.d;
                    dtvCcPacket.d = i4 + 1;
                    bArr[i4] = t3;
                } else {
                    if (i2 == 2) {
                        z = true;
                    }
                    Assertions.a(z);
                    DtvCcPacket dtvCcPacket2 = this.n;
                    if (dtvCcPacket2 == null) {
                        Log.b("Cea708Decoder", "Encountered DTVCC_PACKET_DATA before DTVCC_PACKET_START");
                    } else {
                        byte[] bArr2 = dtvCcPacket2.c;
                        int i5 = dtvCcPacket2.d;
                        dtvCcPacket2.d = i5 + 1;
                        bArr2[i5] = t2;
                        int i6 = dtvCcPacket2.d;
                        dtvCcPacket2.d = i6 + 1;
                        bArr2[i6] = t3;
                    }
                }
                DtvCcPacket dtvCcPacket3 = this.n;
                if (dtvCcPacket3.d == (dtvCcPacket3.b * 2) - 1) {
                    e();
                }
            }
        }
    }

    private void e(int i2) {
        CueBuilder cueBuilder = this.j[i2];
        this.h.c(2);
        boolean e = this.h.e();
        boolean e2 = this.h.e();
        boolean e3 = this.h.e();
        int a2 = this.h.a(3);
        boolean e4 = this.h.e();
        int a3 = this.h.a(7);
        int a4 = this.h.a(8);
        int a5 = this.h.a(4);
        int a6 = this.h.a(4);
        this.h.c(2);
        int a7 = this.h.a(6);
        this.h.c(2);
        cueBuilder.a(e, e2, e3, a2, e4, a3, a4, a6, a7, a5, this.h.a(3), this.h.a(3));
    }

    private void i() {
        this.h.c(4);
        int a2 = this.h.a(4);
        this.h.c(2);
        this.k.a(a2, this.h.a(6));
    }

    private void a(int i2) {
        if (i2 == 0) {
            return;
        }
        if (i2 == 3) {
            this.l = f();
        } else if (i2 != 8) {
            switch (i2) {
                case 12:
                    l();
                    return;
                case 13:
                    this.k.a(10);
                    return;
                case 14:
                    return;
                default:
                    if (i2 >= 17 && i2 <= 23) {
                        Log.d("Cea708Decoder", "Currently unsupported COMMAND_EXT1 Command: " + i2);
                        this.h.c(8);
                        return;
                    } else if (i2 < 24 || i2 > 31) {
                        Log.d("Cea708Decoder", "Invalid C0 command: " + i2);
                        return;
                    } else {
                        Log.d("Cea708Decoder", "Currently unsupported COMMAND_P16 Command: " + i2);
                        this.h.c(16);
                        return;
                    }
            }
        } else {
            this.k.a();
        }
    }

    private void h() {
        int a2 = CueBuilder.a(this.h.a(2), this.h.a(2), this.h.a(2), this.h.a(2));
        int a3 = CueBuilder.a(this.h.a(2), this.h.a(2), this.h.a(2), this.h.a(2));
        this.h.c(2);
        this.k.a(a2, a3, CueBuilder.b(this.h.a(2), this.h.a(2), this.h.a(2)));
    }
}
