package com.google.android.exoplayer2.text.tx3g;

import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.nio.charset.Charset;
import java.util.List;

public final class Tx3gDecoder extends SimpleSubtitleDecoder {
    private static final int v = Util.c("styl");
    private static final int w = Util.c("tbox");
    private final ParsableByteArray o = new ParsableByteArray();
    private boolean p;
    private int q;
    private int r;
    private String s;
    private float t;
    private int u;

    public Tx3gDecoder(List<byte[]> list) {
        super("Tx3gDecoder");
        a(list);
    }

    private void a(List<byte[]> list) {
        String str = "sans-serif";
        boolean z = false;
        if (list != null && list.size() == 1 && (list.get(0).length == 48 || list.get(0).length == 53)) {
            byte[] bArr = list.get(0);
            this.q = bArr[24];
            this.r = ((bArr[26] & 255) << 24) | ((bArr[27] & 255) << 16) | ((bArr[28] & 255) << 8) | (bArr[29] & 255);
            if ("Serif".equals(Util.a(bArr, 43, bArr.length - 43))) {
                str = "serif";
            }
            this.s = str;
            this.u = bArr[25] * 20;
            if ((bArr[0] & 32) != 0) {
                z = true;
            }
            this.p = z;
            if (this.p) {
                this.t = ((float) ((bArr[11] & 255) | ((bArr[10] & 255) << 8))) / ((float) this.u);
                this.t = Util.a(this.t, 0.0f, 0.95f);
                return;
            }
            this.t = 0.85f;
            return;
        }
        this.q = 0;
        this.r = -1;
        this.s = str;
        this.p = false;
        this.t = 0.85f;
    }

    private static void b(SpannableStringBuilder spannableStringBuilder, int i, int i2, int i3, int i4, int i5) {
        if (i != i2) {
            int i6 = i5 | 33;
            boolean z = true;
            boolean z2 = (i & 1) != 0;
            boolean z3 = (i & 2) != 0;
            if (z2) {
                if (z3) {
                    spannableStringBuilder.setSpan(new StyleSpan(3), i3, i4, i6);
                } else {
                    spannableStringBuilder.setSpan(new StyleSpan(1), i3, i4, i6);
                }
            } else if (z3) {
                spannableStringBuilder.setSpan(new StyleSpan(2), i3, i4, i6);
            }
            if ((i & 4) == 0) {
                z = false;
            }
            if (z) {
                spannableStringBuilder.setSpan(new UnderlineSpan(), i3, i4, i6);
            }
            if (!z && !z2 && !z3) {
                spannableStringBuilder.setSpan(new StyleSpan(0), i3, i4, i6);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Subtitle a(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        this.o.a(bArr, i);
        String a2 = a(this.o);
        if (a2.isEmpty()) {
            return Tx3gSubtitle.b;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(a2);
        SpannableStringBuilder spannableStringBuilder2 = spannableStringBuilder;
        b(spannableStringBuilder2, this.q, 0, 0, spannableStringBuilder.length(), 16711680);
        a(spannableStringBuilder2, this.r, -1, 0, spannableStringBuilder.length(), 16711680);
        a(spannableStringBuilder2, this.s, "sans-serif", 0, spannableStringBuilder.length(), 16711680);
        float f = this.t;
        while (this.o.a() >= 8) {
            int c = this.o.c();
            int h = this.o.h();
            int h2 = this.o.h();
            boolean z2 = true;
            if (h2 == v) {
                if (this.o.a() < 2) {
                    z2 = false;
                }
                a(z2);
                int z3 = this.o.z();
                for (int i2 = 0; i2 < z3; i2++) {
                    a(this.o, spannableStringBuilder);
                }
            } else if (h2 == w && this.p) {
                if (this.o.a() < 2) {
                    z2 = false;
                }
                a(z2);
                f = Util.a(((float) this.o.z()) / ((float) this.u), 0.0f, 0.95f);
            }
            this.o.e(c + h);
        }
        return new Tx3gSubtitle(new Cue(spannableStringBuilder, (Layout.Alignment) null, f, 0, 0, Float.MIN_VALUE, Integer.MIN_VALUE, Float.MIN_VALUE));
    }

    private static String a(ParsableByteArray parsableByteArray) throws SubtitleDecoderException {
        char e;
        a(parsableByteArray.a() >= 2);
        int z = parsableByteArray.z();
        if (z == 0) {
            return "";
        }
        if (parsableByteArray.a() < 2 || ((e = parsableByteArray.e()) != 65279 && e != 65534)) {
            return parsableByteArray.a(z, Charset.forName("UTF-8"));
        }
        return parsableByteArray.a(z, Charset.forName("UTF-16"));
    }

    private void a(ParsableByteArray parsableByteArray, SpannableStringBuilder spannableStringBuilder) throws SubtitleDecoderException {
        a(parsableByteArray.a() >= 12);
        int z = parsableByteArray.z();
        int z2 = parsableByteArray.z();
        parsableByteArray.f(2);
        int t2 = parsableByteArray.t();
        parsableByteArray.f(1);
        int h = parsableByteArray.h();
        SpannableStringBuilder spannableStringBuilder2 = spannableStringBuilder;
        int i = z;
        int i2 = z2;
        b(spannableStringBuilder2, t2, this.q, i, i2, 0);
        a(spannableStringBuilder2, h, this.r, i, i2, 0);
    }

    private static void a(SpannableStringBuilder spannableStringBuilder, int i, int i2, int i3, int i4, int i5) {
        if (i != i2) {
            spannableStringBuilder.setSpan(new ForegroundColorSpan((i >>> 8) | ((i & JfifUtil.MARKER_FIRST_BYTE) << 24)), i3, i4, i5 | 33);
        }
    }

    private static void a(SpannableStringBuilder spannableStringBuilder, String str, String str2, int i, int i2, int i3) {
        if (str != str2) {
            spannableStringBuilder.setSpan(new TypefaceSpan(str), i, i2, i3 | 33);
        }
    }

    private static void a(boolean z) throws SubtitleDecoderException {
        if (!z) {
            throw new SubtitleDecoderException("Unexpected subtitle format.");
        }
    }
}
