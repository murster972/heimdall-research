package com.google.android.exoplayer2.text.subrip;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.Collections;
import java.util.List;

final class SubripSubtitle implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private final Cue[] f3537a;
    private final long[] b;

    public SubripSubtitle(Cue[] cueArr, long[] jArr) {
        this.f3537a = cueArr;
        this.b = jArr;
    }

    public int a(long j) {
        int a2 = Util.a(this.b, j, false, false);
        if (a2 < this.b.length) {
            return a2;
        }
        return -1;
    }

    public List<Cue> b(long j) {
        int b2 = Util.b(this.b, j, true, false);
        if (b2 != -1) {
            Cue[] cueArr = this.f3537a;
            if (cueArr[b2] != null) {
                return Collections.singletonList(cueArr[b2]);
            }
        }
        return Collections.emptyList();
    }

    public int a() {
        return this.b.length;
    }

    public long a(int i) {
        boolean z = true;
        Assertions.a(i >= 0);
        if (i >= this.b.length) {
            z = false;
        }
        Assertions.a(z);
        return this.b[i];
    }
}
