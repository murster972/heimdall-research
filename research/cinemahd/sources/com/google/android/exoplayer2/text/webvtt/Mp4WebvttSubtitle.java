package com.google.android.exoplayer2.text.webvtt;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Collections;
import java.util.List;

final class Mp4WebvttSubtitle implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private final List<Cue> f3546a;

    public Mp4WebvttSubtitle(List<Cue> list) {
        this.f3546a = Collections.unmodifiableList(list);
    }

    public int a() {
        return 1;
    }

    public int a(long j) {
        return j < 0 ? 0 : -1;
    }

    public long a(int i) {
        Assertions.a(i == 0);
        return 0;
    }

    public List<Cue> b(long j) {
        return j >= 0 ? this.f3546a : Collections.emptyList();
    }
}
