package com.google.android.exoplayer2.text.cea;

import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.text.SubtitleDecoder;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.exoplayer2.text.SubtitleInputBuffer;
import com.google.android.exoplayer2.text.SubtitleOutputBuffer;
import com.google.android.exoplayer2.util.Assertions;
import java.util.ArrayDeque;
import java.util.PriorityQueue;

abstract class CeaDecoder implements SubtitleDecoder {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayDeque<CeaInputBuffer> f3521a = new ArrayDeque<>();
    private final ArrayDeque<SubtitleOutputBuffer> b;
    private final PriorityQueue<CeaInputBuffer> c;
    private CeaInputBuffer d;
    private long e;
    private long f;

    private static final class CeaInputBuffer extends SubtitleInputBuffer implements Comparable<CeaInputBuffer> {
        /* access modifiers changed from: private */
        public long f;

        private CeaInputBuffer() {
        }

        /* renamed from: a */
        public int compareTo(CeaInputBuffer ceaInputBuffer) {
            if (isEndOfStream() == ceaInputBuffer.isEndOfStream()) {
                long j = this.c - ceaInputBuffer.c;
                if (j == 0) {
                    j = this.f - ceaInputBuffer.f;
                    if (j == 0) {
                        return 0;
                    }
                }
                if (j > 0) {
                    return 1;
                }
                return -1;
            } else if (isEndOfStream()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    private final class CeaOutputBuffer extends SubtitleOutputBuffer {
        private CeaOutputBuffer() {
        }

        public final void release() {
            CeaDecoder.this.a((SubtitleOutputBuffer) this);
        }
    }

    public CeaDecoder() {
        for (int i = 0; i < 10; i++) {
            this.f3521a.add(new CeaInputBuffer());
        }
        this.b = new ArrayDeque<>();
        for (int i2 = 0; i2 < 2; i2++) {
            this.b.add(new CeaOutputBuffer());
        }
        this.c = new PriorityQueue<>();
    }

    /* access modifiers changed from: protected */
    public abstract void a(SubtitleInputBuffer subtitleInputBuffer);

    /* access modifiers changed from: protected */
    public abstract Subtitle c();

    /* access modifiers changed from: protected */
    public abstract boolean d();

    public void flush() {
        this.f = 0;
        this.e = 0;
        while (!this.c.isEmpty()) {
            a(this.c.poll());
        }
        CeaInputBuffer ceaInputBuffer = this.d;
        if (ceaInputBuffer != null) {
            a(ceaInputBuffer);
            this.d = null;
        }
    }

    public void release() {
    }

    public SubtitleInputBuffer b() throws SubtitleDecoderException {
        Assertions.b(this.d == null);
        if (this.f3521a.isEmpty()) {
            return null;
        }
        this.d = this.f3521a.pollFirst();
        return this.d;
    }

    public void a(long j) {
        this.e = j;
    }

    public SubtitleOutputBuffer a() throws SubtitleDecoderException {
        if (this.b.isEmpty()) {
            return null;
        }
        while (!this.c.isEmpty() && this.c.peek().c <= this.e) {
            CeaInputBuffer poll = this.c.poll();
            if (poll.isEndOfStream()) {
                SubtitleOutputBuffer pollFirst = this.b.pollFirst();
                pollFirst.addFlag(4);
                a(poll);
                return pollFirst;
            }
            a((SubtitleInputBuffer) poll);
            if (d()) {
                Subtitle c2 = c();
                if (!poll.isDecodeOnly()) {
                    SubtitleOutputBuffer pollFirst2 = this.b.pollFirst();
                    pollFirst2.a(poll.c, c2, Clock.MAX_TIME);
                    a(poll);
                    return pollFirst2;
                }
            }
            a(poll);
        }
        return null;
    }

    /* renamed from: b */
    public void a(SubtitleInputBuffer subtitleInputBuffer) throws SubtitleDecoderException {
        Assertions.a(subtitleInputBuffer == this.d);
        if (subtitleInputBuffer.isDecodeOnly()) {
            a(this.d);
        } else {
            CeaInputBuffer ceaInputBuffer = this.d;
            long j = this.f;
            this.f = 1 + j;
            long unused = ceaInputBuffer.f = j;
            this.c.add(this.d);
        }
        this.d = null;
    }

    private void a(CeaInputBuffer ceaInputBuffer) {
        ceaInputBuffer.clear();
        this.f3521a.add(ceaInputBuffer);
    }

    /* access modifiers changed from: protected */
    public void a(SubtitleOutputBuffer subtitleOutputBuffer) {
        subtitleOutputBuffer.clear();
        this.b.add(subtitleOutputBuffer);
    }
}
