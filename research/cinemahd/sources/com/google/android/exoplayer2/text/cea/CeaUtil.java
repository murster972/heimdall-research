package com.google.android.exoplayer2.text.cea;

import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;

public final class CeaUtil {

    /* renamed from: a  reason: collision with root package name */
    public static final int f3523a = Util.c("GA94");

    private CeaUtil() {
    }

    public static void a(long j, ParsableByteArray parsableByteArray, TrackOutput[] trackOutputArr) {
        while (true) {
            boolean z = true;
            if (parsableByteArray.a() > 1) {
                int a2 = a(parsableByteArray);
                int a3 = a(parsableByteArray);
                int c = parsableByteArray.c() + a3;
                if (a3 == -1 || a3 > parsableByteArray.a()) {
                    Log.d("CeaUtil", "Skipping remainder of malformed SEI NAL unit.");
                    c = parsableByteArray.d();
                } else if (a2 == 4 && a3 >= 8) {
                    int t = parsableByteArray.t();
                    int z2 = parsableByteArray.z();
                    int h = z2 == 49 ? parsableByteArray.h() : 0;
                    int t2 = parsableByteArray.t();
                    if (z2 == 47) {
                        parsableByteArray.f(1);
                    }
                    boolean z3 = t == 181 && (z2 == 49 || z2 == 47) && t2 == 3;
                    if (z2 == 49) {
                        if (h != f3523a) {
                            z = false;
                        }
                        z3 &= z;
                    }
                    if (z3) {
                        b(j, parsableByteArray, trackOutputArr);
                    }
                }
                parsableByteArray.e(c);
            } else {
                return;
            }
        }
    }

    public static void b(long j, ParsableByteArray parsableByteArray, TrackOutput[] trackOutputArr) {
        int t = parsableByteArray.t();
        if ((t & 64) != 0) {
            parsableByteArray.f(1);
            int i = (t & 31) * 3;
            int c = parsableByteArray.c();
            for (TrackOutput trackOutput : trackOutputArr) {
                parsableByteArray.e(c);
                trackOutput.a(parsableByteArray, i);
                trackOutput.a(j, 1, i, 0, (TrackOutput.CryptoData) null);
            }
        }
    }

    private static int a(ParsableByteArray parsableByteArray) {
        int i = 0;
        while (parsableByteArray.a() != 0) {
            int t = parsableByteArray.t();
            i += t;
            if (t != 255) {
                return i;
            }
        }
        return -1;
    }
}
