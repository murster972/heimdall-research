package com.google.android.exoplayer2.text.ttml;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.util.Util;
import java.util.Collections;
import java.util.List;
import java.util.Map;

final class TtmlSubtitle implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private final TtmlNode f3543a;
    private final long[] b;
    private final Map<String, TtmlStyle> c;
    private final Map<String, TtmlRegion> d;

    public TtmlSubtitle(TtmlNode ttmlNode, Map<String, TtmlStyle> map, Map<String, TtmlRegion> map2) {
        this.f3543a = ttmlNode;
        this.d = map2;
        this.c = map != null ? Collections.unmodifiableMap(map) : Collections.emptyMap();
        this.b = ttmlNode.b();
    }

    public int a(long j) {
        int a2 = Util.a(this.b, j, false, false);
        if (a2 < this.b.length) {
            return a2;
        }
        return -1;
    }

    public List<Cue> b(long j) {
        return this.f3543a.a(j, this.c, this.d);
    }

    public int a() {
        return this.b.length;
    }

    public long a(int i) {
        return this.b[i];
    }
}
