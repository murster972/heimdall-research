package com.google.android.exoplayer2.text.tx3g;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Collections;
import java.util.List;

final class Tx3gSubtitle implements Subtitle {
    public static final Tx3gSubtitle b = new Tx3gSubtitle();

    /* renamed from: a  reason: collision with root package name */
    private final List<Cue> f3544a;

    public Tx3gSubtitle(Cue cue) {
        this.f3544a = Collections.singletonList(cue);
    }

    public int a() {
        return 1;
    }

    public int a(long j) {
        return j < 0 ? 0 : -1;
    }

    public long a(int i) {
        Assertions.a(i == 0);
        return 0;
    }

    public List<Cue> b(long j) {
        return j >= 0 ? this.f3544a : Collections.emptyList();
    }

    private Tx3gSubtitle() {
        this.f3544a = Collections.emptyList();
    }
}
