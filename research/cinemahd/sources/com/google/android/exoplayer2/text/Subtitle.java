package com.google.android.exoplayer2.text;

import java.util.List;

public interface Subtitle {
    int a();

    int a(long j);

    long a(int i);

    List<Cue> b(long j);
}
