package com.google.android.exoplayer2.text;

import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.decoder.OutputBuffer;
import java.util.List;

public abstract class SubtitleOutputBuffer extends OutputBuffer implements Subtitle {

    /* renamed from: a  reason: collision with root package name */
    private Subtitle f3516a;
    private long b;

    public void a(long j, Subtitle subtitle, long j2) {
        this.timeUs = j;
        this.f3516a = subtitle;
        if (j2 == Clock.MAX_TIME) {
            j2 = this.timeUs;
        }
        this.b = j2;
    }

    public List<Cue> b(long j) {
        return this.f3516a.b(j - this.b);
    }

    public void clear() {
        super.clear();
        this.f3516a = null;
    }

    public int a() {
        return this.f3516a.a();
    }

    public long a(int i) {
        return this.f3516a.a(i) + this.b;
    }

    public int a(long j) {
        return this.f3516a.a(j - this.b);
    }
}
