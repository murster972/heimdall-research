package com.google.android.exoplayer2.text.pgs;

import android.graphics.Bitmap;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.text.Subtitle;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.zip.Inflater;

public final class PgsDecoder extends SimpleSubtitleDecoder {
    private final ParsableByteArray o = new ParsableByteArray();
    private final ParsableByteArray p = new ParsableByteArray();
    private final CueBuilder q = new CueBuilder();
    private Inflater r;

    private static final class CueBuilder {

        /* renamed from: a  reason: collision with root package name */
        private final ParsableByteArray f3534a = new ParsableByteArray();
        private final int[] b = new int[256];
        private boolean c;
        private int d;
        private int e;
        private int f;
        private int g;
        private int h;
        private int i;

        /* access modifiers changed from: private */
        public void a(ParsableByteArray parsableByteArray, int i2) {
            int w;
            if (i2 >= 4) {
                parsableByteArray.f(3);
                int i3 = i2 - 4;
                if ((parsableByteArray.t() & 128) != 0) {
                    if (i3 >= 7 && (w = parsableByteArray.w()) >= 4) {
                        this.h = parsableByteArray.z();
                        this.i = parsableByteArray.z();
                        this.f3534a.c(w - 4);
                        i3 -= 7;
                    } else {
                        return;
                    }
                }
                int c2 = this.f3534a.c();
                int d2 = this.f3534a.d();
                if (c2 < d2 && i3 > 0) {
                    int min = Math.min(i3, d2 - c2);
                    parsableByteArray.a(this.f3534a.f3641a, c2, min);
                    this.f3534a.e(c2 + min);
                }
            }
        }

        /* access modifiers changed from: private */
        public void b(ParsableByteArray parsableByteArray, int i2) {
            if (i2 >= 19) {
                this.d = parsableByteArray.z();
                this.e = parsableByteArray.z();
                parsableByteArray.f(11);
                this.f = parsableByteArray.z();
                this.g = parsableByteArray.z();
            }
        }

        /* access modifiers changed from: private */
        public void c(ParsableByteArray parsableByteArray, int i2) {
            if (i2 % 5 == 2) {
                parsableByteArray.f(2);
                Arrays.fill(this.b, 0);
                int i3 = i2 / 5;
                int i4 = 0;
                while (i4 < i3) {
                    int t = parsableByteArray.t();
                    int t2 = parsableByteArray.t();
                    int t3 = parsableByteArray.t();
                    int t4 = parsableByteArray.t();
                    int t5 = parsableByteArray.t();
                    double d2 = (double) t2;
                    double d3 = (double) (t3 - 128);
                    int i5 = i4;
                    double d4 = (double) (t4 - 128);
                    int[] iArr = this.b;
                    int a2 = Util.a((int) ((d2 - (0.34414d * d4)) - (d3 * 0.71414d)), 0, (int) JfifUtil.MARKER_FIRST_BYTE) << 8;
                    iArr[t] = Util.a((int) (d2 + (d4 * 1.772d)), 0, (int) JfifUtil.MARKER_FIRST_BYTE) | a2 | (t5 << 24) | (Util.a((int) ((1.402d * d3) + d2), 0, (int) JfifUtil.MARKER_FIRST_BYTE) << 16);
                    i4 = i5 + 1;
                }
                this.c = true;
            }
        }

        public void b() {
            this.d = 0;
            this.e = 0;
            this.f = 0;
            this.g = 0;
            this.h = 0;
            this.i = 0;
            this.f3534a.c(0);
            this.c = false;
        }

        public Cue a() {
            int i2;
            int i3;
            int i4;
            if (this.d == 0 || this.e == 0 || this.h == 0 || this.i == 0 || this.f3534a.d() == 0 || this.f3534a.c() != this.f3534a.d() || !this.c) {
                return null;
            }
            this.f3534a.e(0);
            int[] iArr = new int[(this.h * this.i)];
            int i5 = 0;
            while (i5 < iArr.length) {
                int t = this.f3534a.t();
                if (t != 0) {
                    i4 = i5 + 1;
                    iArr[i5] = this.b[t];
                } else {
                    int t2 = this.f3534a.t();
                    if (t2 != 0) {
                        if ((t2 & 64) == 0) {
                            i2 = t2 & 63;
                        } else {
                            i2 = ((t2 & 63) << 8) | this.f3534a.t();
                        }
                        if ((t2 & 128) == 0) {
                            i3 = 0;
                        } else {
                            i3 = this.b[this.f3534a.t()];
                        }
                        i4 = i2 + i5;
                        Arrays.fill(iArr, i5, i4, i3);
                    }
                }
                i5 = i4;
            }
            Bitmap createBitmap = Bitmap.createBitmap(iArr, this.h, this.i, Bitmap.Config.ARGB_8888);
            int i6 = this.d;
            float f2 = ((float) this.f) / ((float) i6);
            int i7 = this.e;
            return new Cue(createBitmap, f2, 0, ((float) this.g) / ((float) i7), 0, ((float) this.h) / ((float) i6), ((float) this.i) / ((float) i7));
        }
    }

    public PgsDecoder() {
        super("PgsDecoder");
    }

    /* access modifiers changed from: protected */
    public Subtitle a(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        this.o.a(bArr, i);
        a(this.o);
        this.q.b();
        ArrayList arrayList = new ArrayList();
        while (this.o.a() >= 3) {
            Cue a2 = a(this.o, this.q);
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return new PgsSubtitle(Collections.unmodifiableList(arrayList));
    }

    private void a(ParsableByteArray parsableByteArray) {
        if (parsableByteArray.a() > 0 && parsableByteArray.f() == 120) {
            if (this.r == null) {
                this.r = new Inflater();
            }
            if (Util.a(parsableByteArray, this.p, this.r)) {
                ParsableByteArray parsableByteArray2 = this.p;
                parsableByteArray.a(parsableByteArray2.f3641a, parsableByteArray2.d());
            }
        }
    }

    private static Cue a(ParsableByteArray parsableByteArray, CueBuilder cueBuilder) {
        int d = parsableByteArray.d();
        int t = parsableByteArray.t();
        int z = parsableByteArray.z();
        int c = parsableByteArray.c() + z;
        Cue cue = null;
        if (c > d) {
            parsableByteArray.e(d);
            return null;
        }
        if (t != 128) {
            switch (t) {
                case 20:
                    cueBuilder.c(parsableByteArray, z);
                    break;
                case 21:
                    cueBuilder.a(parsableByteArray, z);
                    break;
                case 22:
                    cueBuilder.b(parsableByteArray, z);
                    break;
            }
        } else {
            cue = cueBuilder.a();
            cueBuilder.b();
        }
        parsableByteArray.e(c);
        return cue;
    }
}
