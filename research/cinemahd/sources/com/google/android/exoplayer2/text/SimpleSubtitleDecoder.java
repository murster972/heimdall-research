package com.google.android.exoplayer2.text;

import com.facebook.common.util.ByteConstants;
import com.google.android.exoplayer2.decoder.SimpleDecoder;
import java.nio.ByteBuffer;

public abstract class SimpleSubtitleDecoder extends SimpleDecoder<SubtitleInputBuffer, SubtitleOutputBuffer, SubtitleDecoderException> implements SubtitleDecoder {
    private final String n;

    protected SimpleSubtitleDecoder(String str) {
        super(new SubtitleInputBuffer[2], new SubtitleOutputBuffer[2]);
        this.n = str;
        a((int) ByteConstants.KB);
    }

    /* access modifiers changed from: protected */
    public abstract Subtitle a(byte[] bArr, int i, boolean z) throws SubtitleDecoderException;

    public void a(long j) {
    }

    public final String getName() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final SubtitleInputBuffer c() {
        return new SubtitleInputBuffer();
    }

    /* access modifiers changed from: protected */
    public final SubtitleOutputBuffer d() {
        return new SimpleSubtitleOutputBuffer(this);
    }

    /* access modifiers changed from: protected */
    public final SubtitleDecoderException a(Throwable th) {
        return new SubtitleDecoderException("Unexpected decode error", th);
    }

    /* access modifiers changed from: protected */
    public final void a(SubtitleOutputBuffer subtitleOutputBuffer) {
        super.a(subtitleOutputBuffer);
    }

    /* access modifiers changed from: protected */
    public final SubtitleDecoderException a(SubtitleInputBuffer subtitleInputBuffer, SubtitleOutputBuffer subtitleOutputBuffer, boolean z) {
        try {
            ByteBuffer byteBuffer = subtitleInputBuffer.b;
            SubtitleOutputBuffer subtitleOutputBuffer2 = subtitleOutputBuffer;
            subtitleOutputBuffer2.a(subtitleInputBuffer.c, a(byteBuffer.array(), byteBuffer.limit(), z), subtitleInputBuffer.e);
            subtitleOutputBuffer.clearFlag(Integer.MIN_VALUE);
            return null;
        } catch (SubtitleDecoderException e) {
            return e;
        }
    }
}
