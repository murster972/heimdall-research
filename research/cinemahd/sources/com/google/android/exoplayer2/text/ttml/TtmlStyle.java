package com.google.android.exoplayer2.text.ttml;

import android.text.Layout;
import com.google.android.exoplayer2.util.Assertions;

final class TtmlStyle {

    /* renamed from: a  reason: collision with root package name */
    private String f3542a;
    private int b;
    private boolean c;
    private int d;
    private boolean e;
    private int f = -1;
    private int g = -1;
    private int h = -1;
    private int i = -1;
    private int j = -1;
    private float k;
    private String l;
    private TtmlStyle m;
    private Layout.Alignment n;

    public TtmlStyle a(boolean z) {
        Assertions.b(this.m == null);
        this.h = z ? 1 : 0;
        return this;
    }

    public TtmlStyle b(boolean z) {
        Assertions.b(this.m == null);
        this.i = z ? 1 : 0;
        return this;
    }

    public TtmlStyle c(boolean z) {
        Assertions.b(this.m == null);
        this.f = z ? 1 : 0;
        return this;
    }

    public TtmlStyle d(boolean z) {
        Assertions.b(this.m == null);
        this.g = z ? 1 : 0;
        return this;
    }

    public int e() {
        return this.j;
    }

    public String f() {
        return this.l;
    }

    public int g() {
        if (this.h == -1 && this.i == -1) {
            return -1;
        }
        int i2 = 0;
        int i3 = this.h == 1 ? 1 : 0;
        if (this.i == 1) {
            i2 = 2;
        }
        return i3 | i2;
    }

    public Layout.Alignment h() {
        return this.n;
    }

    public boolean i() {
        return this.e;
    }

    public boolean j() {
        return this.c;
    }

    public boolean k() {
        return this.f == 1;
    }

    public boolean l() {
        return this.g == 1;
    }

    public TtmlStyle a(String str) {
        Assertions.b(this.m == null);
        this.f3542a = str;
        return this;
    }

    public int b() {
        if (this.c) {
            return this.b;
        }
        throw new IllegalStateException("Font color has not been defined.");
    }

    public String c() {
        return this.f3542a;
    }

    public float d() {
        return this.k;
    }

    public TtmlStyle c(int i2) {
        this.j = i2;
        return this;
    }

    public int a() {
        if (this.e) {
            return this.d;
        }
        throw new IllegalStateException("Background color has not been defined.");
    }

    public TtmlStyle b(int i2) {
        Assertions.b(this.m == null);
        this.b = i2;
        this.c = true;
        return this;
    }

    public TtmlStyle a(int i2) {
        this.d = i2;
        this.e = true;
        return this;
    }

    public TtmlStyle b(String str) {
        this.l = str;
        return this;
    }

    public TtmlStyle a(TtmlStyle ttmlStyle) {
        a(ttmlStyle, true);
        return this;
    }

    private TtmlStyle a(TtmlStyle ttmlStyle, boolean z) {
        if (ttmlStyle != null) {
            if (!this.c && ttmlStyle.c) {
                b(ttmlStyle.b);
            }
            if (this.h == -1) {
                this.h = ttmlStyle.h;
            }
            if (this.i == -1) {
                this.i = ttmlStyle.i;
            }
            if (this.f3542a == null) {
                this.f3542a = ttmlStyle.f3542a;
            }
            if (this.f == -1) {
                this.f = ttmlStyle.f;
            }
            if (this.g == -1) {
                this.g = ttmlStyle.g;
            }
            if (this.n == null) {
                this.n = ttmlStyle.n;
            }
            if (this.j == -1) {
                this.j = ttmlStyle.j;
                this.k = ttmlStyle.k;
            }
            if (z && !this.e && ttmlStyle.e) {
                a(ttmlStyle.d);
            }
        }
        return this;
    }

    public TtmlStyle a(Layout.Alignment alignment) {
        this.n = alignment;
        return this;
    }

    public TtmlStyle a(float f2) {
        this.k = f2;
        return this;
    }
}
