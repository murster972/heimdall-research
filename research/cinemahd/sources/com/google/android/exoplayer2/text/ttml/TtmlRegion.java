package com.google.android.exoplayer2.text.ttml;

final class TtmlRegion {

    /* renamed from: a  reason: collision with root package name */
    public final String f3541a;
    public final float b;
    public final float c;
    public final int d;
    public final int e;
    public final float f;
    public final int g;
    public final float h;

    public TtmlRegion(String str) {
        this(str, Float.MIN_VALUE, Float.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, Float.MIN_VALUE, Integer.MIN_VALUE, Float.MIN_VALUE);
    }

    public TtmlRegion(String str, float f2, float f3, int i, int i2, float f4, int i3, float f5) {
        this.f3541a = str;
        this.b = f2;
        this.c = f3;
        this.d = i;
        this.e = i2;
        this.f = f4;
        this.g = i3;
        this.h = f5;
    }
}
