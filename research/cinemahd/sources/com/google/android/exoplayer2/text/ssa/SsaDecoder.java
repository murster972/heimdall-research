package com.google.android.exoplayer2.text.ssa;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.SimpleSubtitleDecoder;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.LongArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SsaDecoder extends SimpleSubtitleDecoder {
    private static final Pattern t = Pattern.compile("(?:(\\d+):)?(\\d+):(\\d+)(?::|\\.)(\\d+)");
    private final boolean o;
    private int p;
    private int q;
    private int r;
    private int s;

    public SsaDecoder() {
        this((List<byte[]>) null);
    }

    public static long b(String str) {
        Matcher matcher = t.matcher(str);
        if (!matcher.matches()) {
            return -9223372036854775807L;
        }
        return (Long.parseLong(matcher.group(1)) * 60 * 60 * 1000000) + (Long.parseLong(matcher.group(2)) * 60 * 1000000) + (Long.parseLong(matcher.group(3)) * 1000000) + (Long.parseLong(matcher.group(4)) * 10000);
    }

    public SsaDecoder(List<byte[]> list) {
        super("SsaDecoder");
        if (list == null || list.isEmpty()) {
            this.o = false;
            return;
        }
        this.o = true;
        String a2 = Util.a(list.get(0));
        Assertions.a(a2.startsWith("Format: "));
        a(a2);
        a(new ParsableByteArray(list.get(1)));
    }

    /* access modifiers changed from: protected */
    public SsaSubtitle a(byte[] bArr, int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        LongArray longArray = new LongArray();
        ParsableByteArray parsableByteArray = new ParsableByteArray(bArr, i);
        if (!this.o) {
            a(parsableByteArray);
        }
        a(parsableByteArray, (List<Cue>) arrayList, longArray);
        Cue[] cueArr = new Cue[arrayList.size()];
        arrayList.toArray(cueArr);
        return new SsaSubtitle(cueArr, longArray.b());
    }

    /*  JADX ERROR: StackOverflow in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    private void a(com.google.android.exoplayer2.util.ParsableByteArray r3) {
        /*
            r2 = this;
        L_0x0000:
            java.lang.String r0 = r3.j()
            if (r0 == 0) goto L_0x000e
            java.lang.String r1 = "[Events]"
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x0000
        L_0x000e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ssa.SsaDecoder.a(com.google.android.exoplayer2.util.ParsableByteArray):void");
    }

    private void a(ParsableByteArray parsableByteArray, List<Cue> list, LongArray longArray) {
        while (true) {
            String j = parsableByteArray.j();
            if (j == null) {
                return;
            }
            if (!this.o && j.startsWith("Format: ")) {
                a(j);
            } else if (j.startsWith("Dialogue: ")) {
                a(j, list, longArray);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0068  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r9) {
        /*
            r8 = this;
            r0 = 8
            java.lang.String r9 = r9.substring(r0)
            java.lang.String r0 = ","
            java.lang.String[] r9 = android.text.TextUtils.split(r9, r0)
            int r0 = r9.length
            r8.p = r0
            r0 = -1
            r8.q = r0
            r8.r = r0
            r8.s = r0
            r1 = 0
            r2 = 0
        L_0x0018:
            int r3 = r8.p
            if (r2 >= r3) goto L_0x006d
            r3 = r9[r2]
            java.lang.String r3 = r3.trim()
            java.lang.String r3 = com.google.android.exoplayer2.util.Util.k(r3)
            int r4 = r3.hashCode()
            r5 = 100571(0x188db, float:1.4093E-40)
            r6 = 2
            r7 = 1
            if (r4 == r5) goto L_0x0050
            r5 = 3556653(0x36452d, float:4.983932E-39)
            if (r4 == r5) goto L_0x0046
            r5 = 109757538(0x68ac462, float:5.219839E-35)
            if (r4 == r5) goto L_0x003c
            goto L_0x005a
        L_0x003c:
            java.lang.String r4 = "start"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x005a
            r3 = 0
            goto L_0x005b
        L_0x0046:
            java.lang.String r4 = "text"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x005a
            r3 = 2
            goto L_0x005b
        L_0x0050:
            java.lang.String r4 = "end"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x005a
            r3 = 1
            goto L_0x005b
        L_0x005a:
            r3 = -1
        L_0x005b:
            if (r3 == 0) goto L_0x0068
            if (r3 == r7) goto L_0x0065
            if (r3 == r6) goto L_0x0062
            goto L_0x006a
        L_0x0062:
            r8.s = r2
            goto L_0x006a
        L_0x0065:
            r8.r = r2
            goto L_0x006a
        L_0x0068:
            r8.q = r2
        L_0x006a:
            int r2 = r2 + 1
            goto L_0x0018
        L_0x006d:
            int r9 = r8.q
            if (r9 == r0) goto L_0x0079
            int r9 = r8.r
            if (r9 == r0) goto L_0x0079
            int r9 = r8.s
            if (r9 != r0) goto L_0x007b
        L_0x0079:
            r8.p = r1
        L_0x007b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.text.ssa.SsaDecoder.a(java.lang.String):void");
    }

    private void a(String str, List<Cue> list, LongArray longArray) {
        long j;
        if (this.p == 0) {
            Log.d("SsaDecoder", "Skipping dialogue line before complete format: " + str);
            return;
        }
        String[] split = str.substring(10).split(",", this.p);
        if (split.length != this.p) {
            Log.d("SsaDecoder", "Skipping dialogue line with fewer columns than format: " + str);
            return;
        }
        long b = b(split[this.q]);
        if (b == -9223372036854775807L) {
            Log.d("SsaDecoder", "Skipping invalid timing: " + str);
            return;
        }
        String str2 = split[this.r];
        if (!str2.trim().isEmpty()) {
            j = b(str2);
            if (j == -9223372036854775807L) {
                Log.d("SsaDecoder", "Skipping invalid timing: " + str);
                return;
            }
        } else {
            j = -9223372036854775807L;
        }
        list.add(new Cue(split[this.s].replaceAll("\\{.*?\\}", "").replaceAll("\\\\N", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE).replaceAll("\\\\n", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE)));
        longArray.a(b);
        if (j != -9223372036854775807L) {
            list.add((Object) null);
            longArray.a(j);
        }
    }
}
