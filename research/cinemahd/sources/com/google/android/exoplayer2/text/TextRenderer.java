package com.google.android.exoplayer2.text;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.util.Collections;
import java.util.List;

public final class TextRenderer extends BaseRenderer implements Handler.Callback {
    private final Handler j;
    private final TextOutput k;
    private final SubtitleDecoderFactory l;
    private final FormatHolder m;
    private boolean n;
    private boolean o;
    private int p;
    private Format q;
    private SubtitleDecoder r;
    private SubtitleInputBuffer s;
    private SubtitleOutputBuffer t;
    private SubtitleOutputBuffer u;
    private int v;

    public TextRenderer(TextOutput textOutput, Looper looper) {
        this(textOutput, looper, SubtitleDecoderFactory.f3515a);
    }

    private void b(List<Cue> list) {
        Handler handler = this.j;
        if (handler != null) {
            handler.obtainMessage(0, list).sendToTarget();
        } else {
            a(list);
        }
    }

    private void s() {
        b(Collections.emptyList());
    }

    private long t() {
        int i = this.v;
        return (i == -1 || i >= this.t.a()) ? Clock.MAX_TIME : this.t.a(this.v);
    }

    private void u() {
        this.s = null;
        this.v = -1;
        SubtitleOutputBuffer subtitleOutputBuffer = this.t;
        if (subtitleOutputBuffer != null) {
            subtitleOutputBuffer.release();
            this.t = null;
        }
        SubtitleOutputBuffer subtitleOutputBuffer2 = this.u;
        if (subtitleOutputBuffer2 != null) {
            subtitleOutputBuffer2.release();
            this.u = null;
        }
    }

    private void v() {
        u();
        this.r.release();
        this.r = null;
        this.p = 0;
    }

    private void w() {
        v();
        this.r = this.l.b(this.q);
    }

    public int a(Format format) {
        return this.l.a(format) ? BaseRenderer.a((DrmSessionManager<?>) null, format.j) ? 4 : 2 : MimeTypes.k(format.g) ? 1 : 0;
    }

    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            a((List<Cue>) (List) message.obj);
            return true;
        }
        throw new IllegalStateException();
    }

    public boolean isReady() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void p() {
        this.q = null;
        s();
        v();
    }

    public TextRenderer(TextOutput textOutput, Looper looper, SubtitleDecoderFactory subtitleDecoderFactory) {
        super(3);
        Handler handler;
        Assertions.a(textOutput);
        this.k = textOutput;
        if (looper == null) {
            handler = null;
        } else {
            handler = Util.a(looper, (Handler.Callback) this);
        }
        this.j = handler;
        this.l = subtitleDecoderFactory;
        this.m = new FormatHolder();
    }

    /* access modifiers changed from: protected */
    public void a(Format[] formatArr, long j2) throws ExoPlaybackException {
        this.q = formatArr[0];
        if (this.r != null) {
            this.p = 1;
        } else {
            this.r = this.l.b(this.q);
        }
    }

    /* access modifiers changed from: protected */
    public void a(long j2, boolean z) {
        s();
        this.n = false;
        this.o = false;
        if (this.p != 0) {
            w();
            return;
        }
        u();
        this.r.flush();
    }

    public void a(long j2, long j3) throws ExoPlaybackException {
        boolean z;
        if (!this.o) {
            if (this.u == null) {
                this.r.a(j2);
                try {
                    this.u = (SubtitleOutputBuffer) this.r.a();
                } catch (SubtitleDecoderException e) {
                    throw ExoPlaybackException.a(e, m());
                }
            }
            if (getState() == 2) {
                if (this.t != null) {
                    long t2 = t();
                    z = false;
                    while (t2 <= j2) {
                        this.v++;
                        t2 = t();
                        z = true;
                    }
                } else {
                    z = false;
                }
                SubtitleOutputBuffer subtitleOutputBuffer = this.u;
                if (subtitleOutputBuffer != null) {
                    if (subtitleOutputBuffer.isEndOfStream()) {
                        if (!z && t() == Clock.MAX_TIME) {
                            if (this.p == 2) {
                                w();
                            } else {
                                u();
                                this.o = true;
                            }
                        }
                    } else if (this.u.timeUs <= j2) {
                        SubtitleOutputBuffer subtitleOutputBuffer2 = this.t;
                        if (subtitleOutputBuffer2 != null) {
                            subtitleOutputBuffer2.release();
                        }
                        this.t = this.u;
                        this.u = null;
                        this.v = this.t.a(j2);
                        z = true;
                    }
                }
                if (z) {
                    b(this.t.b(j2));
                }
                if (this.p != 2) {
                    while (!this.n) {
                        try {
                            if (this.s == null) {
                                this.s = (SubtitleInputBuffer) this.r.b();
                                if (this.s == null) {
                                    return;
                                }
                            }
                            if (this.p == 1) {
                                this.s.setFlags(4);
                                this.r.a(this.s);
                                this.s = null;
                                this.p = 2;
                                return;
                            }
                            int a2 = a(this.m, (DecoderInputBuffer) this.s, false);
                            if (a2 == -4) {
                                if (this.s.isEndOfStream()) {
                                    this.n = true;
                                } else {
                                    this.s.e = this.m.f3165a.k;
                                    this.s.b();
                                }
                                this.r.a(this.s);
                                this.s = null;
                            } else if (a2 == -3) {
                                return;
                            }
                        } catch (SubtitleDecoderException e2) {
                            throw ExoPlaybackException.a(e2, m());
                        }
                    }
                }
            }
        }
    }

    public boolean a() {
        return this.o;
    }

    private void a(List<Cue> list) {
        this.k.a(list);
    }
}
