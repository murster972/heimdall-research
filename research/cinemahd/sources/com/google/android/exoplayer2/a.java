package com.google.android.exoplayer2;

/* compiled from: lambda */
public final /* synthetic */ class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ExoPlayerImplInternal f3178a;
    private final /* synthetic */ PlayerMessage b;

    public /* synthetic */ a(ExoPlayerImplInternal exoPlayerImplInternal, PlayerMessage playerMessage) {
        this.f3178a = exoPlayerImplInternal;
        this.b = playerMessage;
    }

    public final void run() {
        this.f3178a.b(this.b);
    }
}
