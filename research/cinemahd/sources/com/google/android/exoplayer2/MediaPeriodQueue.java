package com.google.android.exoplayer2;

import android.util.Pair;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.util.Assertions;

final class MediaPeriodQueue {

    /* renamed from: a  reason: collision with root package name */
    private final Timeline.Period f3168a = new Timeline.Period();
    private final Timeline.Window b = new Timeline.Window();
    private long c;
    private Timeline d = Timeline.f3175a;
    private int e;
    private boolean f;
    private MediaPeriodHolder g;
    private MediaPeriodHolder h;
    private MediaPeriodHolder i;
    private int j;
    private Object k;
    private long l;

    private boolean i() {
        MediaPeriodHolder mediaPeriodHolder;
        MediaPeriodHolder c2 = c();
        if (c2 == null) {
            return true;
        }
        int a2 = this.d.a(c2.b);
        while (true) {
            a2 = this.d.a(a2, this.f3168a, this.b, this.e, this.f);
            while (true) {
                MediaPeriodHolder mediaPeriodHolder2 = c2.h;
                if (mediaPeriodHolder2 != null && !c2.g.e) {
                    c2 = mediaPeriodHolder2;
                }
            }
            if (a2 == -1 || (mediaPeriodHolder = c2.h) == null || this.d.a(mediaPeriodHolder.b) != a2) {
                boolean a3 = a(c2);
                c2.g = a(c2.g);
            } else {
                c2 = c2.h;
            }
        }
        boolean a32 = a(c2);
        c2.g = a(c2.g);
        if (!a32 || !g()) {
            return true;
        }
        return false;
    }

    public void a(Timeline timeline) {
        this.d = timeline;
    }

    public boolean b(boolean z) {
        this.f = z;
        return i();
    }

    public MediaPeriodHolder c() {
        return g() ? this.g : this.i;
    }

    public MediaPeriodHolder d() {
        return this.i;
    }

    public MediaPeriodHolder e() {
        return this.g;
    }

    public MediaPeriodHolder f() {
        return this.h;
    }

    public boolean g() {
        return this.g != null;
    }

    public boolean h() {
        MediaPeriodHolder mediaPeriodHolder = this.i;
        return mediaPeriodHolder == null || (!mediaPeriodHolder.g.f && mediaPeriodHolder.e() && this.i.g.d != -9223372036854775807L && this.j < 100);
    }

    public boolean a(int i2) {
        this.e = i2;
        return i();
    }

    public MediaPeriodHolder b() {
        MediaPeriodHolder mediaPeriodHolder = this.h;
        Assertions.b((mediaPeriodHolder == null || mediaPeriodHolder.h == null) ? false : true);
        this.h = this.h.h;
        return this.h;
    }

    public boolean a(MediaPeriod mediaPeriod) {
        MediaPeriodHolder mediaPeriodHolder = this.i;
        return mediaPeriodHolder != null && mediaPeriodHolder.f3166a == mediaPeriod;
    }

    public void a(long j2) {
        MediaPeriodHolder mediaPeriodHolder = this.i;
        if (mediaPeriodHolder != null) {
            mediaPeriodHolder.b(j2);
        }
    }

    private MediaSource.MediaPeriodId b(Object obj, long j2, long j3) {
        long j4;
        this.d.a(obj, this.f3168a);
        int b2 = this.f3168a.b(j2);
        if (b2 == -1) {
            int a2 = this.f3168a.a(j2);
            if (a2 == -1) {
                j4 = Long.MIN_VALUE;
            } else {
                j4 = this.f3168a.b(a2);
            }
            return new MediaSource.MediaPeriodId(obj, j3, j4);
        }
        return new MediaSource.MediaPeriodId(obj, b2, this.f3168a.c(b2), j3);
    }

    public MediaPeriodInfo a(long j2, PlaybackInfo playbackInfo) {
        MediaPeriodHolder mediaPeriodHolder = this.i;
        if (mediaPeriodHolder == null) {
            return a(playbackInfo);
        }
        return a(mediaPeriodHolder, j2);
    }

    public MediaPeriod a(RendererCapabilities[] rendererCapabilitiesArr, TrackSelector trackSelector, Allocator allocator, MediaSource mediaSource, MediaPeriodInfo mediaPeriodInfo) {
        long j2;
        MediaPeriodHolder mediaPeriodHolder = this.i;
        if (mediaPeriodHolder == null) {
            j2 = mediaPeriodInfo.b;
        } else {
            j2 = mediaPeriodHolder.c() + this.i.g.d;
        }
        MediaPeriodHolder mediaPeriodHolder2 = new MediaPeriodHolder(rendererCapabilitiesArr, j2, trackSelector, allocator, mediaSource, mediaPeriodInfo);
        if (this.i != null) {
            Assertions.b(g());
            this.i.h = mediaPeriodHolder2;
        }
        this.k = null;
        this.i = mediaPeriodHolder2;
        this.j++;
        return mediaPeriodHolder2.f3166a;
    }

    public MediaPeriodHolder a() {
        MediaPeriodHolder mediaPeriodHolder = this.g;
        if (mediaPeriodHolder != null) {
            if (mediaPeriodHolder == this.h) {
                this.h = mediaPeriodHolder.h;
            }
            this.g.f();
            this.j--;
            if (this.j == 0) {
                this.i = null;
                MediaPeriodHolder mediaPeriodHolder2 = this.g;
                this.k = mediaPeriodHolder2.b;
                this.l = mediaPeriodHolder2.g.f3167a.d;
            }
            this.g = this.g.h;
        } else {
            MediaPeriodHolder mediaPeriodHolder3 = this.i;
            this.g = mediaPeriodHolder3;
            this.h = mediaPeriodHolder3;
        }
        return this.g;
    }

    public boolean a(MediaPeriodHolder mediaPeriodHolder) {
        boolean z = false;
        Assertions.b(mediaPeriodHolder != null);
        this.i = mediaPeriodHolder;
        while (true) {
            mediaPeriodHolder = mediaPeriodHolder.h;
            if (mediaPeriodHolder != null) {
                if (mediaPeriodHolder == this.h) {
                    this.h = this.g;
                    z = true;
                }
                mediaPeriodHolder.f();
                this.j--;
            } else {
                this.i.h = null;
                return z;
            }
        }
    }

    public void a(boolean z) {
        MediaPeriodHolder c2 = c();
        if (c2 != null) {
            this.k = z ? c2.b : null;
            this.l = c2.g.f3167a.d;
            c2.f();
            a(c2);
        } else if (!z) {
            this.k = null;
        }
        this.g = null;
        this.i = null;
        this.h = null;
        this.j = 0;
    }

    public boolean a(MediaSource.MediaPeriodId mediaPeriodId, long j2) {
        int a2 = this.d.a(mediaPeriodId.f3414a);
        MediaPeriodHolder mediaPeriodHolder = null;
        MediaPeriodHolder c2 = c();
        while (c2 != null) {
            if (mediaPeriodHolder == null) {
                c2.g = a(c2.g);
            } else if (a2 == -1 || !c2.b.equals(this.d.a(a2))) {
                return !a(mediaPeriodHolder);
            } else {
                MediaPeriodInfo a3 = a(mediaPeriodHolder, j2);
                if (a3 == null) {
                    return !a(mediaPeriodHolder);
                }
                c2.g = a(c2.g);
                if (!a(c2, a3)) {
                    return !a(mediaPeriodHolder);
                }
            }
            if (c2.g.e) {
                a2 = this.d.a(a2, this.f3168a, this.b, this.e, this.f);
            }
            MediaPeriodHolder mediaPeriodHolder2 = c2;
            c2 = c2.h;
            mediaPeriodHolder = mediaPeriodHolder2;
        }
        return true;
    }

    public MediaPeriodInfo a(MediaPeriodInfo mediaPeriodInfo) {
        long j2;
        boolean a2 = a(mediaPeriodInfo.f3167a);
        boolean a3 = a(mediaPeriodInfo.f3167a, a2);
        this.d.a(mediaPeriodInfo.f3167a.f3414a, this.f3168a);
        if (mediaPeriodInfo.f3167a.a()) {
            Timeline.Period period = this.f3168a;
            MediaSource.MediaPeriodId mediaPeriodId = mediaPeriodInfo.f3167a;
            j2 = period.a(mediaPeriodId.b, mediaPeriodId.c);
        } else {
            j2 = mediaPeriodInfo.f3167a.e;
            if (j2 == Long.MIN_VALUE) {
                j2 = this.f3168a.d();
            }
        }
        return new MediaPeriodInfo(mediaPeriodInfo.f3167a, mediaPeriodInfo.b, mediaPeriodInfo.c, j2, a2, a3);
    }

    public MediaSource.MediaPeriodId a(Object obj, long j2) {
        return b(obj, j2, a(obj));
    }

    private long a(Object obj) {
        int a2;
        int i2 = this.d.a(obj, this.f3168a).b;
        Object obj2 = this.k;
        if (obj2 != null && (a2 = this.d.a(obj2)) != -1 && this.d.a(a2, this.f3168a).b == i2) {
            return this.l;
        }
        for (MediaPeriodHolder c2 = c(); c2 != null; c2 = c2.h) {
            if (c2.b.equals(obj)) {
                return c2.g.f3167a.d;
            }
        }
        for (MediaPeriodHolder c3 = c(); c3 != null; c3 = c3.h) {
            int a3 = this.d.a(c3.b);
            if (a3 != -1 && this.d.a(a3, this.f3168a).b == i2) {
                return c3.g.f3167a.d;
            }
        }
        long j2 = this.c;
        this.c = 1 + j2;
        return j2;
    }

    private boolean a(MediaPeriodHolder mediaPeriodHolder, MediaPeriodInfo mediaPeriodInfo) {
        MediaPeriodInfo mediaPeriodInfo2 = mediaPeriodHolder.g;
        return mediaPeriodInfo2.b == mediaPeriodInfo.b && mediaPeriodInfo2.f3167a.equals(mediaPeriodInfo.f3167a);
    }

    private MediaPeriodInfo a(PlaybackInfo playbackInfo) {
        return a(playbackInfo.c, playbackInfo.e, playbackInfo.d);
    }

    private MediaPeriodInfo a(MediaPeriodHolder mediaPeriodHolder, long j2) {
        long j3;
        long j4;
        Object obj;
        long j5;
        MediaPeriodHolder mediaPeriodHolder2 = mediaPeriodHolder;
        MediaPeriodInfo mediaPeriodInfo = mediaPeriodHolder2.g;
        long c2 = (mediaPeriodHolder.c() + mediaPeriodInfo.d) - j2;
        long j6 = 0;
        if (mediaPeriodInfo.e) {
            int a2 = this.d.a(this.d.a(mediaPeriodInfo.f3167a.f3414a), this.f3168a, this.b, this.e, this.f);
            if (a2 == -1) {
                return null;
            }
            int i2 = this.d.a(a2, this.f3168a, true).b;
            Object obj2 = this.f3168a.f3176a;
            long j7 = mediaPeriodInfo.f3167a.d;
            if (this.d.a(i2, this.b).c == a2) {
                Pair<Object, Long> a3 = this.d.a(this.b, this.f3168a, i2, -9223372036854775807L, Math.max(0, c2));
                if (a3 == null) {
                    return null;
                }
                Object obj3 = a3.first;
                long longValue = ((Long) a3.second).longValue();
                MediaPeriodHolder mediaPeriodHolder3 = mediaPeriodHolder2.h;
                if (mediaPeriodHolder3 == null || !mediaPeriodHolder3.b.equals(obj3)) {
                    j5 = this.c;
                    this.c = 1 + j5;
                } else {
                    j5 = mediaPeriodHolder2.h.g.f3167a.d;
                }
                j6 = longValue;
                j4 = j5;
                obj = obj3;
            } else {
                obj = obj2;
                j4 = j7;
            }
            long j8 = j6;
            return a(b(obj, j8, j4), j8, j6);
        }
        MediaSource.MediaPeriodId mediaPeriodId = mediaPeriodInfo.f3167a;
        this.d.a(mediaPeriodId.f3414a, this.f3168a);
        if (mediaPeriodId.a()) {
            int i3 = mediaPeriodId.b;
            int a4 = this.f3168a.a(i3);
            if (a4 == -1) {
                return null;
            }
            int b2 = this.f3168a.b(i3, mediaPeriodId.c);
            if (b2 >= a4) {
                long j9 = mediaPeriodInfo.c;
                if (this.f3168a.a() == 1 && this.f3168a.b(0) == 0) {
                    Timeline timeline = this.d;
                    Timeline.Window window = this.b;
                    Timeline.Period period = this.f3168a;
                    Pair<Object, Long> a5 = timeline.a(window, period, period.b, -9223372036854775807L, Math.max(0, c2));
                    if (a5 == null) {
                        return null;
                    }
                    j3 = ((Long) a5.second).longValue();
                } else {
                    j3 = j9;
                }
                return a(mediaPeriodId.f3414a, j3, mediaPeriodId.d);
            } else if (!this.f3168a.c(i3, b2)) {
                return null;
            } else {
                return a(mediaPeriodId.f3414a, i3, b2, mediaPeriodInfo.c, mediaPeriodId.d);
            }
        } else {
            long j10 = mediaPeriodInfo.f3167a.e;
            if (j10 != Long.MIN_VALUE) {
                int b3 = this.f3168a.b(j10);
                if (b3 == -1) {
                    return a(mediaPeriodId.f3414a, mediaPeriodInfo.f3167a.e, mediaPeriodId.d);
                }
                int c3 = this.f3168a.c(b3);
                if (!this.f3168a.c(b3, c3)) {
                    return null;
                }
                return a(mediaPeriodId.f3414a, b3, c3, mediaPeriodInfo.f3167a.e, mediaPeriodId.d);
            }
            int a6 = this.f3168a.a();
            if (a6 == 0) {
                return null;
            }
            int i4 = a6 - 1;
            if (this.f3168a.b(i4) != Long.MIN_VALUE || this.f3168a.d(i4)) {
                return null;
            }
            int c4 = this.f3168a.c(i4);
            if (!this.f3168a.c(i4, c4)) {
                return null;
            }
            return a(mediaPeriodId.f3414a, i4, c4, this.f3168a.d(), mediaPeriodId.d);
        }
    }

    private MediaPeriodInfo a(MediaSource.MediaPeriodId mediaPeriodId, long j2, long j3) {
        this.d.a(mediaPeriodId.f3414a, this.f3168a);
        if (!mediaPeriodId.a()) {
            return a(mediaPeriodId.f3414a, j3, mediaPeriodId.d);
        } else if (!this.f3168a.c(mediaPeriodId.b, mediaPeriodId.c)) {
            return null;
        } else {
            return a(mediaPeriodId.f3414a, mediaPeriodId.b, mediaPeriodId.c, j2, mediaPeriodId.d);
        }
    }

    private MediaPeriodInfo a(Object obj, int i2, int i3, long j2, long j3) {
        MediaSource.MediaPeriodId mediaPeriodId = new MediaSource.MediaPeriodId(obj, i2, i3, j3);
        boolean a2 = a(mediaPeriodId);
        boolean a3 = a(mediaPeriodId, a2);
        return new MediaPeriodInfo(mediaPeriodId, i3 == this.f3168a.c(i2) ? this.f3168a.b() : 0, j2, this.d.a(mediaPeriodId.f3414a, this.f3168a).a(mediaPeriodId.b, mediaPeriodId.c), a2, a3);
    }

    private MediaPeriodInfo a(Object obj, long j2, long j3) {
        long j4;
        int a2 = this.f3168a.a(j2);
        if (a2 == -1) {
            j4 = Long.MIN_VALUE;
        } else {
            j4 = this.f3168a.b(a2);
        }
        MediaSource.MediaPeriodId mediaPeriodId = new MediaSource.MediaPeriodId(obj, j3, j4);
        this.d.a(mediaPeriodId.f3414a, this.f3168a);
        boolean a3 = a(mediaPeriodId);
        return new MediaPeriodInfo(mediaPeriodId, j2, -9223372036854775807L, j4 == Long.MIN_VALUE ? this.f3168a.d() : j4, a3, a(mediaPeriodId, a3));
    }

    private boolean a(MediaSource.MediaPeriodId mediaPeriodId) {
        int a2 = this.d.a(mediaPeriodId.f3414a, this.f3168a).a();
        if (a2 == 0) {
            return true;
        }
        int i2 = a2 - 1;
        boolean a3 = mediaPeriodId.a();
        if (this.f3168a.b(i2) == Long.MIN_VALUE) {
            int a4 = this.f3168a.a(i2);
            if (a4 == -1) {
                return false;
            }
            if (a3 && mediaPeriodId.b == i2 && mediaPeriodId.c == a4 + -1) {
                return true;
            }
            if (a3 || this.f3168a.c(i2) != a4) {
                return false;
            }
            return true;
        } else if (a3 || mediaPeriodId.e != Long.MIN_VALUE) {
            return false;
        } else {
            return true;
        }
    }

    private boolean a(MediaSource.MediaPeriodId mediaPeriodId, boolean z) {
        int a2 = this.d.a(mediaPeriodId.f3414a);
        return !this.d.a(this.d.a(a2, this.f3168a).b, this.b).b && this.d.b(a2, this.f3168a, this.b, this.e, this.f) && z;
    }
}
