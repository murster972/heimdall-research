package com.google.android.exoplayer2;

import android.os.Looper;
import android.view.SurfaceView;
import android.view.TextureView;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.video.VideoListener;

public interface Player {

    public interface AudioComponent {
    }

    @Deprecated
    public static abstract class DefaultEventListener implements EventListener {
        @Deprecated
        public void a(Timeline timeline, Object obj) {
        }

        public void a(Timeline timeline, Object obj, int i) {
            a(timeline, obj);
        }

        public /* synthetic */ void b(int i) {
            b.a((EventListener) this, i);
        }

        public /* synthetic */ void b(boolean z) {
            b.b((EventListener) this, z);
        }

        public /* synthetic */ void h() {
            b.a(this);
        }

        public /* synthetic */ void onLoadingChanged(boolean z) {
            b.a((EventListener) this, z);
        }

        public /* synthetic */ void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            b.a((EventListener) this, playbackParameters);
        }

        public /* synthetic */ void onPlayerError(ExoPlaybackException exoPlaybackException) {
            b.a((EventListener) this, exoPlaybackException);
        }

        public /* synthetic */ void onPlayerStateChanged(boolean z, int i) {
            b.a((EventListener) this, z, i);
        }

        public /* synthetic */ void onRepeatModeChanged(int i) {
            b.b((EventListener) this, i);
        }

        public /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            b.a((EventListener) this, trackGroupArray, trackSelectionArray);
        }
    }

    public interface EventListener {
        void a(Timeline timeline, Object obj, int i);

        void b(int i);

        void b(boolean z);

        void h();

        void onLoadingChanged(boolean z);

        void onPlaybackParametersChanged(PlaybackParameters playbackParameters);

        void onPlayerError(ExoPlaybackException exoPlaybackException);

        void onPlayerStateChanged(boolean z, int i);

        void onRepeatModeChanged(int i);

        void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray);
    }

    public interface TextComponent {
        void a(TextOutput textOutput);

        void b(TextOutput textOutput);
    }

    public interface VideoComponent {
        void a(SurfaceView surfaceView);

        void a(TextureView textureView);

        void a(VideoListener videoListener);

        void b(SurfaceView surfaceView);

        void b(TextureView textureView);

        void b(VideoListener videoListener);
    }

    int a(int i);

    void a(int i, long j);

    void a(EventListener eventListener);

    void a(boolean z);

    PlaybackParameters b();

    void b(EventListener eventListener);

    void b(boolean z);

    void c(boolean z);

    boolean c();

    long d();

    ExoPlaybackException e();

    int f();

    VideoComponent g();

    long getBufferedPosition();

    long getCurrentPosition();

    long getDuration();

    int getPlaybackState();

    int getRepeatMode();

    int h();

    Timeline i();

    Looper j();

    TrackSelectionArray k();

    TextComponent l();

    boolean m();

    int n();

    long o();

    int p();

    int q();

    boolean r();

    void setRepeatMode(int i);
}
