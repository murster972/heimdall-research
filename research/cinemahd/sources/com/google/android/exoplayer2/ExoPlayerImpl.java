package com.google.android.exoplayer2;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.PlayerMessage;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectorResult;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Clock;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

final class ExoPlayerImpl extends BasePlayer implements ExoPlayer {
    final TrackSelectorResult b;
    private final Renderer[] c;
    private final TrackSelector d;
    private final Handler e;
    private final ExoPlayerImplInternal f;
    private final Handler g;
    private final CopyOnWriteArraySet<Player.EventListener> h;
    private final Timeline.Period i;
    private final ArrayDeque<PlaybackInfoUpdate> j;
    private boolean k;
    private boolean l;
    private int m;
    private boolean n;
    private int o;
    private boolean p;
    private boolean q;
    private PlaybackParameters r;
    private ExoPlaybackException s;
    private PlaybackInfo t;
    private int u;
    private int v;
    private long w;

    private static final class PlaybackInfoUpdate {

        /* renamed from: a  reason: collision with root package name */
        private final PlaybackInfo f3157a;
        private final Set<Player.EventListener> b;
        private final TrackSelector c;
        private final boolean d;
        private final int e;
        private final int f;
        private final boolean g;
        private final boolean h;
        private final boolean i;
        private final boolean j;
        private final boolean k;
        private final boolean l;

        public PlaybackInfoUpdate(PlaybackInfo playbackInfo, PlaybackInfo playbackInfo2, Set<Player.EventListener> set, TrackSelector trackSelector, boolean z, int i2, int i3, boolean z2, boolean z3, boolean z4) {
            this.f3157a = playbackInfo;
            this.b = set;
            this.c = trackSelector;
            this.d = z;
            this.e = i2;
            this.f = i3;
            this.g = z2;
            this.h = z3;
            boolean z5 = false;
            this.i = z4 || playbackInfo2.f != playbackInfo.f;
            this.j = (playbackInfo2.f3169a == playbackInfo.f3169a && playbackInfo2.b == playbackInfo.b) ? false : true;
            this.k = playbackInfo2.g != playbackInfo.g;
            this.l = playbackInfo2.i != playbackInfo.i ? true : z5;
        }

        public void a() {
            if (this.j || this.f == 0) {
                for (Player.EventListener a2 : this.b) {
                    PlaybackInfo playbackInfo = this.f3157a;
                    a2.a(playbackInfo.f3169a, playbackInfo.b, this.f);
                }
            }
            if (this.d) {
                for (Player.EventListener b2 : this.b) {
                    b2.b(this.e);
                }
            }
            if (this.l) {
                this.c.a(this.f3157a.i.d);
                for (Player.EventListener onTracksChanged : this.b) {
                    PlaybackInfo playbackInfo2 = this.f3157a;
                    onTracksChanged.onTracksChanged(playbackInfo2.h, playbackInfo2.i.c);
                }
            }
            if (this.k) {
                for (Player.EventListener onLoadingChanged : this.b) {
                    onLoadingChanged.onLoadingChanged(this.f3157a.g);
                }
            }
            if (this.i) {
                for (Player.EventListener onPlayerStateChanged : this.b) {
                    onPlayerStateChanged.onPlayerStateChanged(this.h, this.f3157a.f);
                }
            }
            if (this.g) {
                for (Player.EventListener h2 : this.b) {
                    h2.h();
                }
            }
        }
    }

    @SuppressLint({"HandlerLeak"})
    public ExoPlayerImpl(Renderer[] rendererArr, TrackSelector trackSelector, LoadControl loadControl, BandwidthMeter bandwidthMeter, Clock clock, Looper looper) {
        Renderer[] rendererArr2 = rendererArr;
        Log.c("ExoPlayerImpl", "Init " + Integer.toHexString(System.identityHashCode(this)) + " [" + "ExoPlayerLib/2.9.2" + "] [" + Util.e + "]");
        Assertions.b(rendererArr2.length > 0);
        Assertions.a(rendererArr);
        this.c = rendererArr2;
        Assertions.a(trackSelector);
        this.d = trackSelector;
        this.k = false;
        this.m = 0;
        this.n = false;
        this.h = new CopyOnWriteArraySet<>();
        this.b = new TrackSelectorResult(new RendererConfiguration[rendererArr2.length], new TrackSelection[rendererArr2.length], (Object) null);
        this.i = new Timeline.Period();
        this.r = PlaybackParameters.e;
        SeekParameters seekParameters = SeekParameters.d;
        this.e = new Handler(looper) {
            public void handleMessage(Message message) {
                ExoPlayerImpl.this.a(message);
            }
        };
        this.t = PlaybackInfo.a(0, this.b);
        this.j = new ArrayDeque<>();
        this.f = new ExoPlayerImplInternal(rendererArr, trackSelector, this.b, loadControl, bandwidthMeter, this.k, this.m, this.n, this.e, this, clock);
        this.g = new Handler(this.f.b());
    }

    private boolean z() {
        return this.t.f3169a.c() || this.o > 0;
    }

    public void a(Player.EventListener eventListener) {
        this.h.remove(eventListener);
    }

    public void b(Player.EventListener eventListener) {
        this.h.add(eventListener);
    }

    public void c(boolean z) {
        if (z) {
            this.s = null;
        }
        PlaybackInfo a2 = a(z, z, 1);
        this.o++;
        this.f.c(z);
        a(a2, false, 4, 1, false, false);
    }

    public long d() {
        return Math.max(0, C.b(this.t.l));
    }

    public ExoPlaybackException e() {
        return this.s;
    }

    public int f() {
        if (z()) {
            return this.u;
        }
        PlaybackInfo playbackInfo = this.t;
        return playbackInfo.f3169a.a(playbackInfo.c.f3414a, this.i).b;
    }

    public Player.VideoComponent g() {
        return null;
    }

    public long getBufferedPosition() {
        if (!c()) {
            return v();
        }
        PlaybackInfo playbackInfo = this.t;
        if (playbackInfo.j.equals(playbackInfo.c)) {
            return C.b(this.t.k);
        }
        return getDuration();
    }

    public long getCurrentPosition() {
        if (z()) {
            return this.w;
        }
        if (this.t.c.a()) {
            return C.b(this.t.m);
        }
        PlaybackInfo playbackInfo = this.t;
        return a(playbackInfo.c, playbackInfo.m);
    }

    public long getDuration() {
        if (!c()) {
            return s();
        }
        PlaybackInfo playbackInfo = this.t;
        MediaSource.MediaPeriodId mediaPeriodId = playbackInfo.c;
        playbackInfo.f3169a.a(mediaPeriodId.f3414a, this.i);
        return C.b(this.i.a(mediaPeriodId.b, mediaPeriodId.c));
    }

    public int getPlaybackState() {
        return this.t.f;
    }

    public int getRepeatMode() {
        return this.m;
    }

    public int h() {
        if (c()) {
            return this.t.c.b;
        }
        return -1;
    }

    public Timeline i() {
        return this.t.f3169a;
    }

    public Looper j() {
        return this.e.getLooper();
    }

    public TrackSelectionArray k() {
        return this.t.i.c;
    }

    public Player.TextComponent l() {
        return null;
    }

    public boolean m() {
        return this.k;
    }

    public int n() {
        if (c()) {
            return this.t.c.c;
        }
        return -1;
    }

    public long o() {
        if (!c()) {
            return getCurrentPosition();
        }
        PlaybackInfo playbackInfo = this.t;
        playbackInfo.f3169a.a(playbackInfo.c.f3414a, this.i);
        return this.i.e() + C.b(this.t.e);
    }

    public boolean r() {
        return this.n;
    }

    public void setRepeatMode(int i2) {
        if (this.m != i2) {
            this.m = i2;
            this.f.a(i2);
            Iterator<Player.EventListener> it2 = this.h.iterator();
            while (it2.hasNext()) {
                it2.next().onRepeatModeChanged(i2);
            }
        }
    }

    public long v() {
        if (z()) {
            return this.w;
        }
        PlaybackInfo playbackInfo = this.t;
        if (playbackInfo.j.d != playbackInfo.c.d) {
            return playbackInfo.f3169a.a(f(), this.f3149a).c();
        }
        long j2 = playbackInfo.k;
        if (this.t.j.a()) {
            PlaybackInfo playbackInfo2 = this.t;
            Timeline.Period a2 = playbackInfo2.f3169a.a(playbackInfo2.j.f3414a, this.i);
            long b2 = a2.b(this.t.j.b);
            j2 = b2 == Long.MIN_VALUE ? a2.c : b2;
        }
        return a(this.t.j, j2);
    }

    public int w() {
        if (z()) {
            return this.v;
        }
        PlaybackInfo playbackInfo = this.t;
        return playbackInfo.f3169a.a(playbackInfo.c.f3414a);
    }

    public boolean x() {
        return this.t.g;
    }

    public void y() {
        Log.c("ExoPlayerImpl", "Release " + Integer.toHexString(System.identityHashCode(this)) + " [" + "ExoPlayerLib/2.9.2" + "] [" + Util.e + "] [" + ExoPlayerLibraryInfo.a() + "]");
        this.f.c();
        this.e.removeCallbacksAndMessages((Object) null);
    }

    public void a(MediaSource mediaSource, boolean z, boolean z2) {
        this.s = null;
        PlaybackInfo a2 = a(z, z2, 2);
        this.p = true;
        this.o++;
        this.f.a(mediaSource, z, z2);
        a(a2, false, 4, 1, false, false);
    }

    public void b(boolean z) {
        if (this.n != z) {
            this.n = z;
            this.f.b(z);
            Iterator<Player.EventListener> it2 = this.h.iterator();
            while (it2.hasNext()) {
                it2.next().b(z);
            }
        }
    }

    public boolean c() {
        return !z() && this.t.c.a();
    }

    public PlaybackParameters b() {
        return this.r;
    }

    public void a(boolean z) {
        a(z, false);
    }

    public void a(boolean z, boolean z2) {
        boolean z3 = z && !z2;
        if (this.l != z3) {
            this.l = z3;
            this.f.a(z3);
        }
        if (this.k != z) {
            this.k = z;
            a(this.t, false, 4, 1, false, true);
        }
    }

    public void a(int i2, long j2) {
        Timeline timeline = this.t.f3169a;
        if (i2 < 0 || (!timeline.c() && i2 >= timeline.b())) {
            throw new IllegalSeekPositionException(timeline, i2, j2);
        }
        this.q = true;
        this.o++;
        if (c()) {
            Log.d("ExoPlayerImpl", "seekTo ignored because an ad is playing");
            this.e.obtainMessage(0, 1, -1, this.t).sendToTarget();
            return;
        }
        this.u = i2;
        if (timeline.c()) {
            this.w = j2 == -9223372036854775807L ? 0 : j2;
            this.v = 0;
        } else {
            long b2 = j2 == -9223372036854775807L ? timeline.a(i2, this.f3149a).b() : C.a(j2);
            Pair<Object, Long> a2 = timeline.a(this.f3149a, this.i, i2, b2);
            this.w = C.b(b2);
            this.v = timeline.a(a2.first);
        }
        this.f.a(timeline, i2, C.a(j2));
        Iterator<Player.EventListener> it2 = this.h.iterator();
        while (it2.hasNext()) {
            it2.next().b(1);
        }
    }

    public PlayerMessage a(PlayerMessage.Target target) {
        return new PlayerMessage(this.f, target, this.t.f3169a, f(), this.g);
    }

    public int a(int i2) {
        return this.c[i2].getTrackType();
    }

    /* access modifiers changed from: package-private */
    public void a(Message message) {
        int i2 = message.what;
        boolean z = true;
        if (i2 == 0) {
            PlaybackInfo playbackInfo = (PlaybackInfo) message.obj;
            int i3 = message.arg1;
            if (message.arg2 == -1) {
                z = false;
            }
            a(playbackInfo, i3, z, message.arg2);
        } else if (i2 == 1) {
            PlaybackParameters playbackParameters = (PlaybackParameters) message.obj;
            if (!this.r.equals(playbackParameters)) {
                this.r = playbackParameters;
                Iterator<Player.EventListener> it2 = this.h.iterator();
                while (it2.hasNext()) {
                    it2.next().onPlaybackParametersChanged(playbackParameters);
                }
            }
        } else if (i2 == 2) {
            ExoPlaybackException exoPlaybackException = (ExoPlaybackException) message.obj;
            this.s = exoPlaybackException;
            Iterator<Player.EventListener> it3 = this.h.iterator();
            while (it3.hasNext()) {
                it3.next().onPlayerError(exoPlaybackException);
            }
        } else {
            throw new IllegalStateException();
        }
    }

    private void a(PlaybackInfo playbackInfo, int i2, boolean z, int i3) {
        this.o -= i2;
        if (this.o == 0) {
            if (playbackInfo.d == -9223372036854775807L) {
                playbackInfo = playbackInfo.a(playbackInfo.c, 0, playbackInfo.e);
            }
            PlaybackInfo playbackInfo2 = playbackInfo;
            if ((!this.t.f3169a.c() || this.p) && playbackInfo2.f3169a.c()) {
                this.v = 0;
                this.u = 0;
                this.w = 0;
            }
            int i4 = this.p ? 0 : 2;
            boolean z2 = this.q;
            this.p = false;
            this.q = false;
            a(playbackInfo2, z, i3, i4, z2, false);
        }
    }

    private PlaybackInfo a(boolean z, boolean z2, int i2) {
        long j2;
        long j3 = 0;
        if (z) {
            this.u = 0;
            this.v = 0;
            this.w = 0;
        } else {
            this.u = f();
            this.v = w();
            this.w = getCurrentPosition();
        }
        MediaSource.MediaPeriodId a2 = z ? this.t.a(this.n, this.f3149a) : this.t.c;
        if (!z) {
            j3 = this.t.m;
        }
        long j4 = j3;
        if (z) {
            j2 = -9223372036854775807L;
        } else {
            j2 = this.t.e;
        }
        return new PlaybackInfo(z2 ? Timeline.f3175a : this.t.f3169a, z2 ? null : this.t.b, a2, j4, j2, i2, false, z2 ? TrackGroupArray.d : this.t.h, z2 ? this.b : this.t.i, a2, j4, 0, j4);
    }

    private void a(PlaybackInfo playbackInfo, boolean z, int i2, int i3, boolean z2, boolean z3) {
        boolean z4 = !this.j.isEmpty();
        this.j.addLast(new PlaybackInfoUpdate(playbackInfo, this.t, this.h, this.d, z, i2, i3, z2, this.k, z3));
        this.t = playbackInfo;
        if (!z4) {
            while (!this.j.isEmpty()) {
                this.j.peekFirst().a();
                this.j.removeFirst();
            }
        }
    }

    private long a(MediaSource.MediaPeriodId mediaPeriodId, long j2) {
        long b2 = C.b(j2);
        this.t.f3169a.a(mediaPeriodId.f3414a, this.i);
        return b2 + this.i.e();
    }
}
