package com.google.android.exoplayer2;

import com.google.android.exoplayer2.PlayerMessage;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.util.MediaClock;
import java.io.IOException;

public interface Renderer extends PlayerMessage.Target {
    void a(float f) throws ExoPlaybackException;

    void a(long j) throws ExoPlaybackException;

    void a(long j, long j2) throws ExoPlaybackException;

    void a(RendererConfiguration rendererConfiguration, Format[] formatArr, SampleStream sampleStream, long j, boolean z, long j2) throws ExoPlaybackException;

    void a(Format[] formatArr, SampleStream sampleStream, long j) throws ExoPlaybackException;

    boolean a();

    boolean c();

    void d();

    void disable();

    void e() throws IOException;

    boolean f();

    RendererCapabilities g();

    int getState();

    int getTrackType();

    SampleStream i();

    boolean isReady();

    MediaClock j();

    void setIndex(int i);

    void start() throws ExoPlaybackException;

    void stop() throws ExoPlaybackException;
}
