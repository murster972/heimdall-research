package com.google.android.exoplayer2;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.ColorInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class Format implements Parcelable {
    public static final Parcelable.Creator<Format> CREATOR = new Parcelable.Creator<Format>() {
        public Format createFromParcel(Parcel parcel) {
            return new Format(parcel);
        }

        public Format[] newArray(int i) {
            return new Format[i];
        }
    };
    public final int A;
    private int B;

    /* renamed from: a  reason: collision with root package name */
    public final String f3164a;
    public final String b;
    public final int c;
    public final String d;
    public final Metadata e;
    public final String f;
    public final String g;
    public final int h;
    public final List<byte[]> i;
    public final DrmInitData j;
    public final long k;
    public final int l;
    public final int m;
    public final float n;
    public final int o;
    public final float p;
    public final int q;
    public final byte[] r;
    public final ColorInfo s;
    public final int t;
    public final int u;
    public final int v;
    public final int w;
    public final int x;
    public final int y;
    public final String z;

    Format(String str, String str2, String str3, String str4, String str5, int i2, int i3, int i4, int i5, float f2, int i6, float f3, byte[] bArr, int i7, ColorInfo colorInfo, int i8, int i9, int i10, int i11, int i12, int i13, String str6, int i14, long j2, List<byte[]> list, DrmInitData drmInitData, Metadata metadata) {
        this.f3164a = str;
        this.b = str2;
        this.f = str3;
        this.g = str4;
        this.d = str5;
        this.c = i2;
        this.h = i3;
        this.l = i4;
        this.m = i5;
        this.n = f2;
        int i15 = i6;
        this.o = i15 == -1 ? 0 : i15;
        this.p = f3 == -1.0f ? 1.0f : f3;
        this.r = bArr;
        this.q = i7;
        this.s = colorInfo;
        this.t = i8;
        this.u = i9;
        this.v = i10;
        int i16 = i11;
        this.w = i16 == -1 ? 0 : i16;
        int i17 = i12;
        this.x = i17 == -1 ? 0 : i17;
        this.y = i13;
        this.z = str6;
        this.A = i14;
        this.k = j2;
        this.i = list == null ? Collections.emptyList() : list;
        this.j = drmInitData;
        this.e = metadata;
    }

    public static Format a(String str, String str2, String str3, String str4, String str5, int i2, int i3, int i4, float f2, List<byte[]> list, int i5) {
        return new Format(str, str2, str3, str4, str5, i2, -1, i3, i4, f2, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, -1, -1, -1, -1, -1, i5, (String) null, -1, Clock.MAX_TIME, list, (DrmInitData) null, (Metadata) null);
    }

    public static Format b(String str, String str2, String str3, String str4, String str5, int i2, int i3, String str6) {
        return a(str, str2, str3, str4, str5, i2, i3, str6, -1);
    }

    public static String c(Format format) {
        if (format == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("id=");
        sb.append(format.f3164a);
        sb.append(", mimeType=");
        sb.append(format.g);
        if (format.c != -1) {
            sb.append(", bitrate=");
            sb.append(format.c);
        }
        if (format.d != null) {
            sb.append(", codecs=");
            sb.append(format.d);
        }
        if (!(format.l == -1 || format.m == -1)) {
            sb.append(", res=");
            sb.append(format.l);
            sb.append("x");
            sb.append(format.m);
        }
        if (format.n != -1.0f) {
            sb.append(", fps=");
            sb.append(format.n);
        }
        if (format.t != -1) {
            sb.append(", channels=");
            sb.append(format.t);
        }
        if (format.u != -1) {
            sb.append(", sample_rate=");
            sb.append(format.u);
        }
        if (format.z != null) {
            sb.append(", language=");
            sb.append(format.z);
        }
        if (format.b != null) {
            sb.append(", label=");
            sb.append(format.b);
        }
        return sb.toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        int i2;
        if (this == obj) {
            return true;
        }
        if (obj == null || Format.class != obj.getClass()) {
            return false;
        }
        Format format = (Format) obj;
        int i3 = this.B;
        if ((i3 == 0 || (i2 = format.B) == 0 || i3 == i2) && this.c == format.c && this.h == format.h && this.l == format.l && this.m == format.m && Float.compare(this.n, format.n) == 0 && this.o == format.o && Float.compare(this.p, format.p) == 0 && this.q == format.q && this.t == format.t && this.u == format.u && this.v == format.v && this.w == format.w && this.x == format.x && this.k == format.k && this.y == format.y && Util.a((Object) this.f3164a, (Object) format.f3164a) && Util.a((Object) this.b, (Object) format.b) && Util.a((Object) this.z, (Object) format.z) && this.A == format.A && Util.a((Object) this.f, (Object) format.f) && Util.a((Object) this.g, (Object) format.g) && Util.a((Object) this.d, (Object) format.d) && Util.a((Object) this.j, (Object) format.j) && Util.a((Object) this.e, (Object) format.e) && Util.a((Object) this.s, (Object) format.s) && Arrays.equals(this.r, format.r) && b(format)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.B == 0) {
            String str = this.f3164a;
            int i2 = 0;
            int hashCode = (527 + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.f;
            int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.g;
            int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.d;
            int hashCode4 = (((((((((((hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31) + this.c) * 31) + this.l) * 31) + this.m) * 31) + this.t) * 31) + this.u) * 31;
            String str5 = this.z;
            int hashCode5 = (((hashCode4 + (str5 == null ? 0 : str5.hashCode())) * 31) + this.A) * 31;
            DrmInitData drmInitData = this.j;
            int hashCode6 = (hashCode5 + (drmInitData == null ? 0 : drmInitData.hashCode())) * 31;
            Metadata metadata = this.e;
            int hashCode7 = (hashCode6 + (metadata == null ? 0 : metadata.hashCode())) * 31;
            String str6 = this.b;
            if (str6 != null) {
                i2 = str6.hashCode();
            }
            this.B = ((((((((((((((((((((hashCode7 + i2) * 31) + this.h) * 31) + ((int) this.k)) * 31) + Float.floatToIntBits(this.n)) * 31) + Float.floatToIntBits(this.p)) * 31) + this.o) * 31) + this.q) * 31) + this.v) * 31) + this.w) * 31) + this.x) * 31) + this.y;
        }
        return this.B;
    }

    public String toString() {
        return "Format(" + this.f3164a + ", " + this.b + ", " + this.f + ", " + this.g + ", " + this.d + ", " + this.c + ", " + this.z + ", [" + this.l + ", " + this.m + ", " + this.n + "], [" + this.t + ", " + this.u + "])";
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f3164a);
        parcel.writeString(this.b);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.d);
        parcel.writeInt(this.c);
        parcel.writeInt(this.h);
        parcel.writeInt(this.l);
        parcel.writeInt(this.m);
        parcel.writeFloat(this.n);
        parcel.writeInt(this.o);
        parcel.writeFloat(this.p);
        Util.a(parcel, this.r != null);
        byte[] bArr = this.r;
        if (bArr != null) {
            parcel.writeByteArray(bArr);
        }
        parcel.writeInt(this.q);
        parcel.writeParcelable(this.s, i2);
        parcel.writeInt(this.t);
        parcel.writeInt(this.u);
        parcel.writeInt(this.v);
        parcel.writeInt(this.w);
        parcel.writeInt(this.x);
        parcel.writeInt(this.y);
        parcel.writeString(this.z);
        parcel.writeInt(this.A);
        parcel.writeLong(this.k);
        int size = this.i.size();
        parcel.writeInt(size);
        for (int i3 = 0; i3 < size; i3++) {
            parcel.writeByteArray(this.i.get(i3));
        }
        parcel.writeParcelable(this.j, 0);
        parcel.writeParcelable(this.e, 0);
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, int i4, int i5, float f2, List<byte[]> list, int i6, float f3, DrmInitData drmInitData) {
        return a(str, str2, str3, i2, i3, i4, i5, f2, list, i6, f3, (byte[]) null, -1, (ColorInfo) null, drmInitData);
    }

    public boolean b(Format format) {
        if (this.i.size() != format.i.size()) {
            return false;
        }
        for (int i2 = 0; i2 < this.i.size(); i2++) {
            if (!Arrays.equals(this.i.get(i2), format.i.get(i2))) {
                return false;
            }
        }
        return true;
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, int i4, int i5, float f2, List<byte[]> list, int i6, float f3, byte[] bArr, int i7, ColorInfo colorInfo, DrmInitData drmInitData) {
        return new Format(str, (String) null, (String) null, str2, str3, i2, i3, i4, i5, f2, i6, f3, bArr, i7, colorInfo, -1, -1, -1, -1, -1, 0, (String) null, -1, Clock.MAX_TIME, list, drmInitData, (Metadata) null);
    }

    public static Format a(String str, String str2, String str3, String str4, String str5, int i2, int i3, int i4, List<byte[]> list, int i5, String str6) {
        return new Format(str, str2, str3, str4, str5, i2, -1, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, i3, i4, -1, -1, -1, i5, str6, -1, Clock.MAX_TIME, list, (DrmInitData) null, (Metadata) null);
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, int i4, int i5, List<byte[]> list, DrmInitData drmInitData, int i6, String str4) {
        return a(str, str2, str3, i2, i3, i4, i5, -1, list, drmInitData, i6, str4);
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, int i4, int i5, int i6, List<byte[]> list, DrmInitData drmInitData, int i7, String str4) {
        return a(str, str2, str3, i2, i3, i4, i5, i6, -1, -1, list, drmInitData, i7, str4, (Metadata) null);
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, int i4, int i5, int i6, int i7, int i8, List<byte[]> list, DrmInitData drmInitData, int i9, String str4, Metadata metadata) {
        return new Format(str, (String) null, (String) null, str2, str3, i2, i3, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, i4, i5, i6, i7, i8, i9, str4, -1, Clock.MAX_TIME, list, drmInitData, metadata);
    }

    public static Format a(String str, String str2, String str3, String str4, String str5, int i2, int i3, String str6, int i4) {
        return new Format(str, str2, str3, str4, str5, i2, -1, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, -1, -1, -1, -1, -1, i3, str6, i4, Clock.MAX_TIME, (List<byte[]>) null, (DrmInitData) null, (Metadata) null);
    }

    public static Format a(String str, String str2, int i2, String str3) {
        return a(str, str2, i2, str3, (DrmInitData) null);
    }

    public static Format a(String str, String str2, int i2, String str3, DrmInitData drmInitData) {
        return a(str, str2, (String) null, -1, i2, str3, -1, drmInitData, Clock.MAX_TIME, Collections.emptyList());
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, String str4, DrmInitData drmInitData, long j2) {
        return a(str, str2, str3, i2, i3, str4, -1, drmInitData, j2, Collections.emptyList());
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, String str4, int i4, DrmInitData drmInitData, long j2, List<byte[]> list) {
        return new Format(str, (String) null, (String) null, str2, str3, i2, -1, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, -1, -1, -1, -1, -1, i3, str4, i4, j2, list, drmInitData, (Metadata) null);
    }

    public static Format a(String str, String str2, String str3, int i2, int i3, List<byte[]> list, String str4, DrmInitData drmInitData) {
        return new Format(str, (String) null, (String) null, str2, str3, i2, -1, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, -1, -1, -1, -1, -1, i3, str4, -1, Clock.MAX_TIME, list, drmInitData, (Metadata) null);
    }

    public static Format a(String str, String str2, String str3, String str4, String str5, int i2, int i3, String str6) {
        return new Format(str, str2, str3, str4, str5, i2, -1, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, -1, -1, -1, -1, -1, i3, str6, -1, Clock.MAX_TIME, (List<byte[]>) null, (DrmInitData) null, (Metadata) null);
    }

    public static Format a(String str, String str2, long j2) {
        return new Format(str, (String) null, (String) null, str2, (String) null, -1, -1, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, -1, -1, -1, -1, -1, 0, (String) null, -1, j2, (List<byte[]>) null, (DrmInitData) null, (Metadata) null);
    }

    public static Format a(String str, String str2, String str3, int i2, DrmInitData drmInitData) {
        return new Format(str, (String) null, (String) null, str2, str3, i2, -1, -1, -1, -1.0f, -1, -1.0f, (byte[]) null, -1, (ColorInfo) null, -1, -1, -1, -1, -1, 0, (String) null, -1, Clock.MAX_TIME, (List<byte[]>) null, drmInitData, (Metadata) null);
    }

    public Format a(int i2) {
        String str = this.f3164a;
        return new Format(str, this.b, this.f, this.g, this.d, this.c, i2, this.l, this.m, this.n, this.o, this.p, this.r, this.q, this.s, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, this.k, this.i, this.j, this.e);
    }

    public Format a(long j2) {
        return new Format(this.f3164a, this.b, this.f, this.g, this.d, this.c, this.h, this.l, this.m, this.n, this.o, this.p, this.r, this.q, this.s, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, j2, this.i, this.j, this.e);
    }

    public Format a(String str, String str2, String str3, String str4, int i2, int i3, int i4, int i5, String str5) {
        return new Format(str, str2, this.f, str3, str4, i2, this.h, i3, i4, this.n, this.o, this.p, this.r, this.q, this.s, this.t, this.u, this.v, this.w, this.x, i5, str5, this.A, this.k, this.i, this.j, this.e);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        r6 = r1.z;
     */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.exoplayer2.Format a(com.google.android.exoplayer2.Format r33) {
        /*
            r32 = this;
            r0 = r32
            r1 = r33
            if (r0 != r1) goto L_0x0007
            return r0
        L_0x0007:
            java.lang.String r2 = r0.g
            int r2 = com.google.android.exoplayer2.util.MimeTypes.f(r2)
            java.lang.String r4 = r1.f3164a
            java.lang.String r3 = r1.b
            if (r3 == 0) goto L_0x0014
            goto L_0x0016
        L_0x0014:
            java.lang.String r3 = r0.b
        L_0x0016:
            r5 = r3
            java.lang.String r3 = r0.z
            r6 = 3
            r7 = 1
            if (r2 == r6) goto L_0x001f
            if (r2 != r7) goto L_0x0026
        L_0x001f:
            java.lang.String r6 = r1.z
            if (r6 == 0) goto L_0x0026
            r25 = r6
            goto L_0x0028
        L_0x0026:
            r25 = r3
        L_0x0028:
            int r3 = r0.c
            r6 = -1
            if (r3 != r6) goto L_0x002f
            int r3 = r1.c
        L_0x002f:
            r9 = r3
            java.lang.String r3 = r0.d
            if (r3 != 0) goto L_0x0043
            java.lang.String r6 = r1.d
            java.lang.String r6 = com.google.android.exoplayer2.util.Util.a((java.lang.String) r6, (int) r2)
            java.lang.String[] r8 = com.google.android.exoplayer2.util.Util.j(r6)
            int r8 = r8.length
            if (r8 != r7) goto L_0x0043
            r8 = r6
            goto L_0x0044
        L_0x0043:
            r8 = r3
        L_0x0044:
            float r3 = r0.n
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r6 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x0053
            r6 = 2
            if (r2 != r6) goto L_0x0053
            float r2 = r1.n
            r13 = r2
            goto L_0x0054
        L_0x0053:
            r13 = r3
        L_0x0054:
            int r2 = r0.y
            int r3 = r1.y
            r24 = r2 | r3
            com.google.android.exoplayer2.drm.DrmInitData r1 = r1.j
            com.google.android.exoplayer2.drm.DrmInitData r2 = r0.j
            com.google.android.exoplayer2.drm.DrmInitData r30 = com.google.android.exoplayer2.drm.DrmInitData.a((com.google.android.exoplayer2.drm.DrmInitData) r1, (com.google.android.exoplayer2.drm.DrmInitData) r2)
            com.google.android.exoplayer2.Format r1 = new com.google.android.exoplayer2.Format
            r3 = r1
            java.lang.String r6 = r0.f
            java.lang.String r7 = r0.g
            int r10 = r0.h
            int r11 = r0.l
            int r12 = r0.m
            int r14 = r0.o
            float r15 = r0.p
            byte[] r2 = r0.r
            r16 = r2
            int r2 = r0.q
            r17 = r2
            com.google.android.exoplayer2.video.ColorInfo r2 = r0.s
            r18 = r2
            int r2 = r0.t
            r19 = r2
            int r2 = r0.u
            r20 = r2
            int r2 = r0.v
            r21 = r2
            int r2 = r0.w
            r22 = r2
            int r2 = r0.x
            r23 = r2
            int r2 = r0.A
            r26 = r2
            r33 = r1
            long r1 = r0.k
            r27 = r1
            java.util.List<byte[]> r1 = r0.i
            r29 = r1
            com.google.android.exoplayer2.metadata.Metadata r1 = r0.e
            r31 = r1
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r29, r30, r31)
            return r33
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.Format.a(com.google.android.exoplayer2.Format):com.google.android.exoplayer2.Format");
    }

    Format(Parcel parcel) {
        this.f3164a = parcel.readString();
        this.b = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.d = parcel.readString();
        this.c = parcel.readInt();
        this.h = parcel.readInt();
        this.l = parcel.readInt();
        this.m = parcel.readInt();
        this.n = parcel.readFloat();
        this.o = parcel.readInt();
        this.p = parcel.readFloat();
        this.r = Util.a(parcel) ? parcel.createByteArray() : null;
        this.q = parcel.readInt();
        this.s = (ColorInfo) parcel.readParcelable(ColorInfo.class.getClassLoader());
        this.t = parcel.readInt();
        this.u = parcel.readInt();
        this.v = parcel.readInt();
        this.w = parcel.readInt();
        this.x = parcel.readInt();
        this.y = parcel.readInt();
        this.z = parcel.readString();
        this.A = parcel.readInt();
        this.k = parcel.readLong();
        int readInt = parcel.readInt();
        this.i = new ArrayList(readInt);
        for (int i2 = 0; i2 < readInt; i2++) {
            this.i.add(parcel.createByteArray());
        }
        this.j = (DrmInitData) parcel.readParcelable(DrmInitData.class.getClassLoader());
        this.e = (Metadata) parcel.readParcelable(Metadata.class.getClassLoader());
    }

    public Format a(int i2, int i3) {
        String str = this.f3164a;
        return new Format(str, this.b, this.f, this.g, this.d, this.c, this.h, this.l, this.m, this.n, this.o, this.p, this.r, this.q, this.s, this.t, this.u, this.v, i2, i3, this.y, this.z, this.A, this.k, this.i, this.j, this.e);
    }

    public Format a(DrmInitData drmInitData) {
        String str = this.f3164a;
        return new Format(str, this.b, this.f, this.g, this.d, this.c, this.h, this.l, this.m, this.n, this.o, this.p, this.r, this.q, this.s, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, this.k, this.i, drmInitData, this.e);
    }

    public Format a(Metadata metadata) {
        String str = this.f3164a;
        return new Format(str, this.b, this.f, this.g, this.d, this.c, this.h, this.l, this.m, this.n, this.o, this.p, this.r, this.q, this.s, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, this.k, this.i, this.j, metadata);
    }

    public int a() {
        int i2;
        int i3 = this.l;
        if (i3 == -1 || (i2 = this.m) == -1) {
            return -1;
        }
        return i3 * i2;
    }
}
