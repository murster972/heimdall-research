package com.google.android.exoplayer2;

import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectorResult;

final class PlaybackInfo {
    private static final MediaSource.MediaPeriodId n = new MediaSource.MediaPeriodId(new Object());

    /* renamed from: a  reason: collision with root package name */
    public final Timeline f3169a;
    public final Object b;
    public final MediaSource.MediaPeriodId c;
    public final long d;
    public final long e;
    public final int f;
    public final boolean g;
    public final TrackGroupArray h;
    public final TrackSelectorResult i;
    public final MediaSource.MediaPeriodId j;
    public volatile long k;
    public volatile long l;
    public volatile long m;

    public PlaybackInfo(Timeline timeline, Object obj, MediaSource.MediaPeriodId mediaPeriodId, long j2, long j3, int i2, boolean z, TrackGroupArray trackGroupArray, TrackSelectorResult trackSelectorResult, MediaSource.MediaPeriodId mediaPeriodId2, long j4, long j5, long j6) {
        this.f3169a = timeline;
        this.b = obj;
        this.c = mediaPeriodId;
        this.d = j2;
        this.e = j3;
        this.f = i2;
        this.g = z;
        this.h = trackGroupArray;
        this.i = trackSelectorResult;
        this.j = mediaPeriodId2;
        this.k = j4;
        this.l = j5;
        this.m = j6;
    }

    public static PlaybackInfo a(long j2, TrackSelectorResult trackSelectorResult) {
        TrackSelectorResult trackSelectorResult2 = trackSelectorResult;
        return new PlaybackInfo(Timeline.f3175a, (Object) null, n, j2, -9223372036854775807L, 1, false, TrackGroupArray.d, trackSelectorResult2, n, j2, 0, j2);
    }

    public MediaSource.MediaPeriodId a(boolean z, Timeline.Window window) {
        if (this.f3169a.c()) {
            return n;
        }
        Timeline timeline = this.f3169a;
        return new MediaSource.MediaPeriodId(this.f3169a.a(timeline.a(timeline.a(z), window).c));
    }

    public PlaybackInfo a(MediaSource.MediaPeriodId mediaPeriodId, long j2, long j3) {
        return new PlaybackInfo(this.f3169a, this.b, mediaPeriodId, j2, mediaPeriodId.a() ? j3 : -9223372036854775807L, this.f, this.g, this.h, this.i, mediaPeriodId, j2, 0, j2);
    }

    public PlaybackInfo a(MediaSource.MediaPeriodId mediaPeriodId, long j2, long j3, long j4) {
        return new PlaybackInfo(this.f3169a, this.b, mediaPeriodId, j2, mediaPeriodId.a() ? j3 : -9223372036854775807L, this.f, this.g, this.h, this.i, this.j, this.k, j4, j2);
    }

    public PlaybackInfo a(Timeline timeline, Object obj) {
        Timeline timeline2 = timeline;
        return new PlaybackInfo(timeline, obj, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m);
    }

    public PlaybackInfo a(int i2) {
        Timeline timeline = this.f3169a;
        return new PlaybackInfo(timeline, this.b, this.c, this.d, this.e, i2, this.g, this.h, this.i, this.j, this.k, this.l, this.m);
    }

    public PlaybackInfo a(boolean z) {
        Timeline timeline = this.f3169a;
        return new PlaybackInfo(timeline, this.b, this.c, this.d, this.e, this.f, z, this.h, this.i, this.j, this.k, this.l, this.m);
    }

    public PlaybackInfo a(TrackGroupArray trackGroupArray, TrackSelectorResult trackSelectorResult) {
        Timeline timeline = this.f3169a;
        return new PlaybackInfo(timeline, this.b, this.c, this.d, this.e, this.f, this.g, trackGroupArray, trackSelectorResult, this.j, this.k, this.l, this.m);
    }

    public PlaybackInfo a(MediaSource.MediaPeriodId mediaPeriodId) {
        Timeline timeline = this.f3169a;
        return new PlaybackInfo(timeline, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, mediaPeriodId, this.k, this.l, this.m);
    }
}
