package com.google.android.exoplayer2.mediacodec;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseIntArray;
import com.facebook.common.util.ByteConstants;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.ar.core.ImageMetadata;
import com.uwetrottmann.trakt5.TraktV2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.internal.cache.DiskLruCache;
import okhttp3.internal.http2.Http2;
import okhttp3.internal.http2.Http2Connection;

@SuppressLint({"InlinedApi"})
@TargetApi(16)
public final class MediaCodecUtil {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f3361a = Pattern.compile("^\\D?(\\d+)$");
    private static final RawAudioCodecComparator b = new RawAudioCodecComparator();
    private static final HashMap<CodecKey, List<MediaCodecInfo>> c = new HashMap<>();
    private static final SparseIntArray d = new SparseIntArray();
    private static final SparseIntArray e = new SparseIntArray();
    private static final Map<String, Integer> f = new HashMap();
    private static final SparseIntArray g = new SparseIntArray();
    private static int h = -1;

    private static final class CodecKey {

        /* renamed from: a  reason: collision with root package name */
        public final String f3362a;
        public final boolean b;

        public CodecKey(String str, boolean z) {
            this.f3362a = str;
            this.b = z;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || obj.getClass() != CodecKey.class) {
                return false;
            }
            CodecKey codecKey = (CodecKey) obj;
            if (!TextUtils.equals(this.f3362a, codecKey.f3362a) || this.b != codecKey.b) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            String str = this.f3362a;
            return (((str == null ? 0 : str.hashCode()) + 31) * 31) + (this.b ? 1231 : 1237);
        }
    }

    public static class DecoderQueryException extends Exception {
        private DecoderQueryException(Throwable th) {
            super("Failed to query underlying media codecs", th);
        }
    }

    private interface MediaCodecListCompat {
        boolean a();

        boolean a(String str, MediaCodecInfo.CodecCapabilities codecCapabilities);

        int getCodecCount();

        MediaCodecInfo getCodecInfoAt(int i);
    }

    private static final class MediaCodecListCompatV16 implements MediaCodecListCompat {
        private MediaCodecListCompatV16() {
        }

        public boolean a() {
            return false;
        }

        public boolean a(String str, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return "video/avc".equals(str);
        }

        public int getCodecCount() {
            return MediaCodecList.getCodecCount();
        }

        public MediaCodecInfo getCodecInfoAt(int i) {
            return MediaCodecList.getCodecInfoAt(i);
        }
    }

    @TargetApi(21)
    private static final class MediaCodecListCompatV21 implements MediaCodecListCompat {

        /* renamed from: a  reason: collision with root package name */
        private final int f3363a;
        private MediaCodecInfo[] b;

        public MediaCodecListCompatV21(boolean z) {
            this.f3363a = z ? 1 : 0;
        }

        private void b() {
            if (this.b == null) {
                this.b = new MediaCodecList(this.f3363a).getCodecInfos();
            }
        }

        public boolean a() {
            return true;
        }

        public boolean a(String str, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return codecCapabilities.isFeatureSupported("secure-playback");
        }

        public int getCodecCount() {
            b();
            return this.b.length;
        }

        public MediaCodecInfo getCodecInfoAt(int i) {
            b();
            return this.b[i];
        }
    }

    private static final class RawAudioCodecComparator implements Comparator<MediaCodecInfo> {
        private RawAudioCodecComparator() {
        }

        /* renamed from: a */
        public int compare(MediaCodecInfo mediaCodecInfo, MediaCodecInfo mediaCodecInfo2) {
            return a(mediaCodecInfo) - a(mediaCodecInfo2);
        }

        private static int a(MediaCodecInfo mediaCodecInfo) {
            String str = mediaCodecInfo.f3359a;
            if (str.startsWith("OMX.google") || str.startsWith("c2.android")) {
                return -1;
            }
            return (Util.f3651a >= 26 || !str.equals("OMX.MTK.AUDIO.DECODER.RAW")) ? 0 : 1;
        }
    }

    static {
        d.put(66, 1);
        d.put(77, 2);
        d.put(88, 4);
        d.put(100, 8);
        d.put(110, 16);
        d.put(122, 32);
        d.put(244, 64);
        e.put(10, 1);
        e.put(11, 4);
        e.put(12, 8);
        e.put(13, 16);
        e.put(20, 32);
        e.put(21, 64);
        e.put(22, 128);
        e.put(30, 256);
        e.put(31, AdRequest.MAX_CONTENT_URL_LENGTH);
        e.put(32, ByteConstants.KB);
        e.put(40, 2048);
        e.put(41, 4096);
        e.put(42, 8192);
        e.put(50, Http2.INITIAL_MAX_FRAME_SIZE);
        e.put(51, 32768);
        e.put(52, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE);
        f.put("L30", 1);
        f.put("L60", 4);
        f.put("L63", 16);
        f.put("L90", 64);
        f.put("L93", 256);
        f.put("L120", Integer.valueOf(ByteConstants.KB));
        f.put("L123", 4096);
        f.put("L150", Integer.valueOf(Http2.INITIAL_MAX_FRAME_SIZE));
        f.put("L153", Integer.valueOf(ImageMetadata.CONTROL_AE_ANTIBANDING_MODE));
        f.put("L156", 262144);
        f.put("L180", 1048576);
        f.put("L183", 4194304);
        f.put("L186", Integer.valueOf(Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE));
        f.put("H30", 2);
        f.put("H60", 8);
        f.put("H63", 32);
        f.put("H90", 128);
        f.put("H93", Integer.valueOf(AdRequest.MAX_CONTENT_URL_LENGTH));
        f.put("H120", 2048);
        f.put("H123", 8192);
        f.put("H150", 32768);
        f.put("H153", 131072);
        f.put("H156", Integer.valueOf(ImageMetadata.LENS_APERTURE));
        f.put("H180", 2097152);
        f.put("H183", 8388608);
        f.put("H186", 33554432);
        g.put(1, 1);
        g.put(2, 2);
        g.put(3, 3);
        g.put(4, 4);
        g.put(5, 5);
        g.put(6, 6);
        g.put(17, 17);
        g.put(20, 20);
        g.put(23, 23);
        g.put(29, 29);
        g.put(39, 39);
        g.put(42, 42);
    }

    private MediaCodecUtil() {
    }

    private static int a(int i) {
        if (i == 1 || i == 2) {
            return 25344;
        }
        switch (i) {
            case 8:
            case 16:
            case 32:
                return 101376;
            case 64:
                return 202752;
            case 128:
            case 256:
                return 414720;
            case AdRequest.MAX_CONTENT_URL_LENGTH /*512*/:
                return 921600;
            case ByteConstants.KB:
                return 1310720;
            case 2048:
            case 4096:
                return 2097152;
            case 8192:
                return 2228224;
            case Http2.INITIAL_MAX_FRAME_SIZE /*16384*/:
                return 5652480;
            case 32768:
            case ImageMetadata.CONTROL_AE_ANTIBANDING_MODE /*65536*/:
                return 9437184;
            default:
                return -1;
        }
    }

    public static MediaCodecInfo a() throws DecoderQueryException {
        MediaCodecInfo a2 = a("audio/raw", false);
        if (a2 == null) {
            return null;
        }
        return MediaCodecInfo.d(a2.f3359a);
    }

    public static synchronized List<MediaCodecInfo> b(String str, boolean z) throws DecoderQueryException {
        synchronized (MediaCodecUtil.class) {
            CodecKey codecKey = new CodecKey(str, z);
            List<MediaCodecInfo> list = c.get(codecKey);
            if (list != null) {
                return list;
            }
            MediaCodecListCompat mediaCodecListCompatV21 = Util.f3651a >= 21 ? new MediaCodecListCompatV21(z) : new MediaCodecListCompatV16();
            ArrayList<MediaCodecInfo> a2 = a(codecKey, mediaCodecListCompatV21, str);
            if (z && a2.isEmpty() && 21 <= Util.f3651a && Util.f3651a <= 23) {
                mediaCodecListCompatV21 = new MediaCodecListCompatV16();
                a2 = a(codecKey, mediaCodecListCompatV21, str);
                if (!a2.isEmpty()) {
                    Log.d("MediaCodecUtil", "MediaCodecList API didn't list secure decoder for: " + str + ". Assuming: " + a2.get(0).f3359a);
                }
            }
            if ("audio/eac3-joc".equals(str)) {
                a2.addAll(a(new CodecKey("audio/eac3", codecKey.b), mediaCodecListCompatV21, str));
            }
            a(str, (List<MediaCodecInfo>) a2);
            List<MediaCodecInfo> unmodifiableList = Collections.unmodifiableList(a2);
            c.put(codecKey, unmodifiableList);
            return unmodifiableList;
        }
    }

    private static Pair<Integer, Integer> c(String str, String[] strArr) {
        int i;
        if (strArr.length < 4) {
            Log.d("MediaCodecUtil", "Ignoring malformed HEVC codec string: " + str);
            return null;
        }
        Matcher matcher = f3361a.matcher(strArr[1]);
        if (!matcher.matches()) {
            Log.d("MediaCodecUtil", "Ignoring malformed HEVC codec string: " + str);
            return null;
        }
        String group = matcher.group(1);
        if (DiskLruCache.VERSION_1.equals(group)) {
            i = 1;
        } else if (TraktV2.API_VERSION.equals(group)) {
            i = 2;
        } else {
            Log.d("MediaCodecUtil", "Unknown HEVC profile string: " + group);
            return null;
        }
        Integer num = f.get(strArr[3]);
        if (num != null) {
            return new Pair<>(Integer.valueOf(i), num);
        }
        Log.d("MediaCodecUtil", "Unknown HEVC level string: " + matcher.group(1));
        return null;
    }

    public static MediaCodecInfo a(String str, boolean z) throws DecoderQueryException {
        List<MediaCodecInfo> b2 = b(str, z);
        if (b2.isEmpty()) {
            return null;
        }
        return b2.get(0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0057 A[Catch:{ Exception -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0060 A[Catch:{ Exception -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008a A[SYNTHETIC, Splitter:B:38:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00aa A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.ArrayList<com.google.android.exoplayer2.mediacodec.MediaCodecInfo> a(com.google.android.exoplayer2.mediacodec.MediaCodecUtil.CodecKey r17, com.google.android.exoplayer2.mediacodec.MediaCodecUtil.MediaCodecListCompat r18, java.lang.String r19) throws com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException {
        /*
            r1 = r17
            r2 = r18
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00e1 }
            r3.<init>()     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r4 = r1.f3362a     // Catch:{ Exception -> 0x00e1 }
            int r5 = r18.getCodecCount()     // Catch:{ Exception -> 0x00e1 }
            boolean r6 = r18.a()     // Catch:{ Exception -> 0x00e1 }
            r8 = 0
        L_0x0014:
            if (r8 >= r5) goto L_0x00e0
            android.media.MediaCodecInfo r9 = r2.getCodecInfoAt(r8)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r10 = r9.getName()     // Catch:{ Exception -> 0x00e1 }
            r11 = r19
            boolean r0 = a(r9, r10, r6, r11)     // Catch:{ Exception -> 0x00e1 }
            if (r0 == 0) goto L_0x00d6
            java.lang.String[] r12 = r9.getSupportedTypes()     // Catch:{ Exception -> 0x00e1 }
            int r13 = r12.length     // Catch:{ Exception -> 0x00e1 }
            r14 = 0
        L_0x002c:
            if (r14 >= r13) goto L_0x00d6
            r15 = r12[r14]     // Catch:{ Exception -> 0x00e1 }
            boolean r0 = r15.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x00e1 }
            if (r0 == 0) goto L_0x00cc
            android.media.MediaCodecInfo$CodecCapabilities r0 = r9.getCapabilitiesForType(r15)     // Catch:{ Exception -> 0x007f }
            boolean r7 = r2.a(r4, r0)     // Catch:{ Exception -> 0x007f }
            boolean r2 = a((java.lang.String) r10)     // Catch:{ Exception -> 0x007f }
            if (r6 == 0) goto L_0x004f
            r16 = r5
            boolean r5 = r1.b     // Catch:{ Exception -> 0x004d }
            if (r5 == r7) goto L_0x004b
            goto L_0x0051
        L_0x004b:
            r5 = 0
            goto L_0x0058
        L_0x004d:
            r0 = move-exception
            goto L_0x0082
        L_0x004f:
            r16 = r5
        L_0x0051:
            if (r6 != 0) goto L_0x0060
            boolean r5 = r1.b     // Catch:{ Exception -> 0x004d }
            if (r5 != 0) goto L_0x0060
            goto L_0x004b
        L_0x0058:
            com.google.android.exoplayer2.mediacodec.MediaCodecInfo r0 = com.google.android.exoplayer2.mediacodec.MediaCodecInfo.a(r10, r4, r0, r2, r5)     // Catch:{ Exception -> 0x004d }
            r3.add(r0)     // Catch:{ Exception -> 0x004d }
            goto L_0x00ce
        L_0x0060:
            r5 = 0
            if (r6 != 0) goto L_0x00ce
            if (r7 == 0) goto L_0x00ce
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004d }
            r7.<init>()     // Catch:{ Exception -> 0x004d }
            r7.append(r10)     // Catch:{ Exception -> 0x004d }
            java.lang.String r5 = ".secure"
            r7.append(r5)     // Catch:{ Exception -> 0x004d }
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x004d }
            r7 = 1
            com.google.android.exoplayer2.mediacodec.MediaCodecInfo r0 = com.google.android.exoplayer2.mediacodec.MediaCodecInfo.a(r5, r4, r0, r2, r7)     // Catch:{ Exception -> 0x004d }
            r3.add(r0)     // Catch:{ Exception -> 0x004d }
            return r3
        L_0x007f:
            r0 = move-exception
            r16 = r5
        L_0x0082:
            int r2 = com.google.android.exoplayer2.util.Util.f3651a     // Catch:{ Exception -> 0x00e1 }
            r5 = 23
            java.lang.String r7 = "MediaCodecUtil"
            if (r2 > r5) goto L_0x00aa
            boolean r2 = r3.isEmpty()     // Catch:{ Exception -> 0x00e1 }
            if (r2 != 0) goto L_0x00aa
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e1 }
            r0.<init>()     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r2 = "Skipping codec "
            r0.append(r2)     // Catch:{ Exception -> 0x00e1 }
            r0.append(r10)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r2 = " (failed to query capabilities)"
            r0.append(r2)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00e1 }
            com.google.android.exoplayer2.util.Log.b(r7, r0)     // Catch:{ Exception -> 0x00e1 }
            goto L_0x00ce
        L_0x00aa:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e1 }
            r1.<init>()     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r2 = "Failed to query codec "
            r1.append(r2)     // Catch:{ Exception -> 0x00e1 }
            r1.append(r10)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r2 = " ("
            r1.append(r2)     // Catch:{ Exception -> 0x00e1 }
            r1.append(r15)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r2 = ")"
            r1.append(r2)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00e1 }
            com.google.android.exoplayer2.util.Log.b(r7, r1)     // Catch:{ Exception -> 0x00e1 }
            throw r0     // Catch:{ Exception -> 0x00e1 }
        L_0x00cc:
            r16 = r5
        L_0x00ce:
            int r14 = r14 + 1
            r2 = r18
            r5 = r16
            goto L_0x002c
        L_0x00d6:
            r16 = r5
            int r8 = r8 + 1
            r2 = r18
            r5 = r16
            goto L_0x0014
        L_0x00e0:
            return r3
        L_0x00e1:
            r0 = move-exception
            com.google.android.exoplayer2.mediacodec.MediaCodecUtil$DecoderQueryException r1 = new com.google.android.exoplayer2.mediacodec.MediaCodecUtil$DecoderQueryException
            r2 = 0
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecUtil.a(com.google.android.exoplayer2.mediacodec.MediaCodecUtil$CodecKey, com.google.android.exoplayer2.mediacodec.MediaCodecUtil$MediaCodecListCompat, java.lang.String):java.util.ArrayList");
    }

    public static int b() throws DecoderQueryException {
        if (h == -1) {
            int i = 0;
            MediaCodecInfo a2 = a("video/avc", false);
            if (a2 != null) {
                MediaCodecInfo.CodecProfileLevel[] a3 = a2.a();
                int length = a3.length;
                int i2 = 0;
                while (i < length) {
                    i2 = Math.max(a(a3[i].level), i2);
                    i++;
                }
                i = Math.max(i2, Util.f3651a >= 21 ? 345600 : 172800);
            }
            h = i;
        }
        return h;
    }

    private static boolean a(MediaCodecInfo mediaCodecInfo, String str, boolean z, String str2) {
        if (mediaCodecInfo.isEncoder() || (!z && str.endsWith(".secure"))) {
            return false;
        }
        if (Util.f3651a < 21 && ("CIPAACDecoder".equals(str) || "CIPMP3Decoder".equals(str) || "CIPVorbisDecoder".equals(str) || "CIPAMRNBDecoder".equals(str) || "AACDecoder".equals(str) || "MP3Decoder".equals(str))) {
            return false;
        }
        if (Util.f3651a < 18 && "OMX.SEC.MP3.Decoder".equals(str)) {
            return false;
        }
        if ("OMX.SEC.mp3.dec".equals(str) && "SM-T530".equals(Util.d)) {
            return false;
        }
        if (Util.f3651a < 18 && "OMX.MTK.AUDIO.DECODER.AAC".equals(str) && ("a70".equals(Util.b) || ("Xiaomi".equals(Util.c) && Util.b.startsWith("HM")))) {
            return false;
        }
        if (Util.f3651a == 16 && "OMX.qcom.audio.decoder.mp3".equals(str) && ("dlxu".equals(Util.b) || "protou".equals(Util.b) || "ville".equals(Util.b) || "villeplus".equals(Util.b) || "villec2".equals(Util.b) || Util.b.startsWith("gee") || "C6602".equals(Util.b) || "C6603".equals(Util.b) || "C6606".equals(Util.b) || "C6616".equals(Util.b) || "L36h".equals(Util.b) || "SO-02E".equals(Util.b))) {
            return false;
        }
        if (Util.f3651a == 16 && "OMX.qcom.audio.decoder.aac".equals(str) && ("C1504".equals(Util.b) || "C1505".equals(Util.b) || "C1604".equals(Util.b) || "C1605".equals(Util.b))) {
            return false;
        }
        if (Util.f3651a < 24 && (("OMX.SEC.aac.dec".equals(str) || "OMX.Exynos.AAC.Decoder".equals(str)) && "samsung".equals(Util.c) && (Util.b.startsWith("zeroflte") || Util.b.startsWith("zerolte") || Util.b.startsWith("zenlte") || "SC-05G".equals(Util.b) || "marinelteatt".equals(Util.b) || "404SC".equals(Util.b) || "SC-04G".equals(Util.b) || "SCV31".equals(Util.b)))) {
            return false;
        }
        if (Util.f3651a <= 19 && "OMX.SEC.vp8.dec".equals(str) && "samsung".equals(Util.c) && (Util.b.startsWith("d2") || Util.b.startsWith("serrano") || Util.b.startsWith("jflte") || Util.b.startsWith("santos") || Util.b.startsWith("t0"))) {
            return false;
        }
        if (Util.f3651a <= 19 && Util.b.startsWith("jflte") && "OMX.qcom.video.decoder.vp8".equals(str)) {
            return false;
        }
        if (!"audio/eac3-joc".equals(str2) || !"OMX.MTK.AUDIO.DECODER.DSPAC3".equals(str)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        if (r3.equals("hev1") != false) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.util.Pair<java.lang.Integer, java.lang.Integer> b(java.lang.String r10) {
        /*
            r0 = 0
            if (r10 != 0) goto L_0x0004
            return r0
        L_0x0004:
            java.lang.String r1 = "\\."
            java.lang.String[] r1 = r10.split(r1)
            r2 = 0
            r3 = r1[r2]
            r4 = -1
            int r5 = r3.hashCode()
            r6 = 4
            r7 = 3
            r8 = 2
            r9 = 1
            switch(r5) {
                case 3006243: goto L_0x0041;
                case 3006244: goto L_0x0037;
                case 3199032: goto L_0x002e;
                case 3214780: goto L_0x0024;
                case 3356560: goto L_0x001a;
                default: goto L_0x0019;
            }
        L_0x0019:
            goto L_0x004b
        L_0x001a:
            java.lang.String r2 = "mp4a"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x004b
            r2 = 4
            goto L_0x004c
        L_0x0024:
            java.lang.String r2 = "hvc1"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x004b
            r2 = 1
            goto L_0x004c
        L_0x002e:
            java.lang.String r5 = "hev1"
            boolean r3 = r3.equals(r5)
            if (r3 == 0) goto L_0x004b
            goto L_0x004c
        L_0x0037:
            java.lang.String r2 = "avc2"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x004b
            r2 = 3
            goto L_0x004c
        L_0x0041:
            java.lang.String r2 = "avc1"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x004b
            r2 = 2
            goto L_0x004c
        L_0x004b:
            r2 = -1
        L_0x004c:
            if (r2 == 0) goto L_0x0061
            if (r2 == r9) goto L_0x0061
            if (r2 == r8) goto L_0x005c
            if (r2 == r7) goto L_0x005c
            if (r2 == r6) goto L_0x0057
            return r0
        L_0x0057:
            android.util.Pair r10 = a((java.lang.String) r10, (java.lang.String[]) r1)
            return r10
        L_0x005c:
            android.util.Pair r10 = b((java.lang.String) r10, (java.lang.String[]) r1)
            return r10
        L_0x0061:
            android.util.Pair r10 = c(r10, r1)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecUtil.b(java.lang.String):android.util.Pair");
    }

    private static Pair<Integer, Integer> b(String str, String[] strArr) {
        Integer num;
        Integer num2;
        if (strArr.length < 2) {
            Log.d("MediaCodecUtil", "Ignoring malformed AVC codec string: " + str);
            return null;
        }
        try {
            if (strArr[1].length() == 6) {
                Integer valueOf = Integer.valueOf(Integer.parseInt(strArr[1].substring(0, 2), 16));
                num = Integer.valueOf(Integer.parseInt(strArr[1].substring(4), 16));
                num2 = valueOf;
            } else if (strArr.length >= 3) {
                num2 = Integer.valueOf(Integer.parseInt(strArr[1]));
                num = Integer.valueOf(Integer.parseInt(strArr[2]));
            } else {
                Log.d("MediaCodecUtil", "Ignoring malformed AVC codec string: " + str);
                return null;
            }
            int i = d.get(num2.intValue(), -1);
            if (i == -1) {
                Log.d("MediaCodecUtil", "Unknown AVC profile: " + num2);
                return null;
            }
            int i2 = e.get(num.intValue(), -1);
            if (i2 != -1) {
                return new Pair<>(Integer.valueOf(i), Integer.valueOf(i2));
            }
            Log.d("MediaCodecUtil", "Unknown AVC level: " + num);
            return null;
        } catch (NumberFormatException unused) {
            Log.d("MediaCodecUtil", "Ignoring malformed AVC codec string: " + str);
            return null;
        }
    }

    private static void a(String str, List<MediaCodecInfo> list) {
        if ("audio/raw".equals(str)) {
            Collections.sort(list, b);
        }
    }

    private static boolean a(String str) {
        return Util.f3651a <= 22 && ("ODROID-XU3".equals(Util.d) || "Nexus 10".equals(Util.d)) && ("OMX.Exynos.AVC.Decoder".equals(str) || "OMX.Exynos.AVC.Decoder.secure".equals(str));
    }

    private static Pair<Integer, Integer> a(String str, String[] strArr) {
        int i;
        if (strArr.length != 3) {
            Log.d("MediaCodecUtil", "Ignoring malformed MP4A codec string: " + str);
            return null;
        }
        try {
            if ("audio/mp4a-latm".equals(MimeTypes.a(Integer.parseInt(strArr[1], 16))) && (i = g.get(Integer.parseInt(strArr[2]), -1)) != -1) {
                return new Pair<>(Integer.valueOf(i), 0);
            }
        } catch (NumberFormatException unused) {
            Log.d("MediaCodecUtil", "Ignoring malformed MP4A codec string: " + str);
        }
        return null;
    }
}
