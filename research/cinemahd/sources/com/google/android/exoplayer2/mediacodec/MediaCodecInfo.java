package com.google.android.exoplayer2.mediacodec;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.media.MediaCodecInfo;
import android.util.Pair;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

@TargetApi(16)
public final class MediaCodecInfo {

    /* renamed from: a  reason: collision with root package name */
    public final String f3359a;
    public final String b;
    public final MediaCodecInfo.CodecCapabilities c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    private final boolean h;

    private MediaCodecInfo(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities, boolean z, boolean z2, boolean z3) {
        Assertions.a(str);
        this.f3359a = str;
        this.b = str2;
        this.c = codecCapabilities;
        this.g = z;
        boolean z4 = true;
        this.d = !z2 && codecCapabilities != null && a(codecCapabilities);
        this.e = codecCapabilities != null && e(codecCapabilities);
        if (!z3 && (codecCapabilities == null || !c(codecCapabilities))) {
            z4 = false;
        }
        this.f = z4;
        this.h = MimeTypes.l(str2);
    }

    public static MediaCodecInfo a(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities, boolean z, boolean z2) {
        return new MediaCodecInfo(str, str2, codecCapabilities, false, z, z2);
    }

    private void c(String str) {
        Log.a("MediaCodecInfo", "NoSupport [" + str + "] [" + this.f3359a + ", " + this.b + "] [" + Util.e + "]");
    }

    public static MediaCodecInfo d(String str) {
        return new MediaCodecInfo(str, (String) null, (MediaCodecInfo.CodecCapabilities) null, true, false, false);
    }

    private static boolean e(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return Util.f3651a >= 21 && f(codecCapabilities);
    }

    @TargetApi(21)
    private static boolean f(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("tunneled-playback");
    }

    public boolean b(Format format) {
        if (this.h) {
            return this.d;
        }
        Pair<Integer, Integer> b2 = MediaCodecUtil.b(format.d);
        return b2 != null && ((Integer) b2.first).intValue() == 42;
    }

    public String toString() {
        return this.f3359a;
    }

    private static boolean c(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return Util.f3651a >= 21 && d(codecCapabilities);
    }

    @TargetApi(21)
    private static boolean d(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("secure-playback");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r0.profileLevels;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.media.MediaCodecInfo.CodecProfileLevel[] a() {
        /*
            r1 = this;
            android.media.MediaCodecInfo$CodecCapabilities r0 = r1.c
            if (r0 == 0) goto L_0x0008
            android.media.MediaCodecInfo$CodecProfileLevel[] r0 = r0.profileLevels
            if (r0 != 0) goto L_0x000b
        L_0x0008:
            r0 = 0
            android.media.MediaCodecInfo$CodecProfileLevel[] r0 = new android.media.MediaCodecInfo.CodecProfileLevel[r0]
        L_0x000b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecInfo.a():android.media.MediaCodecInfo$CodecProfileLevel[]");
    }

    public boolean a(Format format) throws MediaCodecUtil.DecoderQueryException {
        int i;
        boolean z = false;
        if (!a(format.d)) {
            return false;
        }
        if (this.h) {
            int i2 = format.l;
            if (i2 <= 0 || (i = format.m) <= 0) {
                return true;
            }
            if (Util.f3651a >= 21) {
                return a(i2, i, (double) format.n);
            }
            if (i2 * i <= MediaCodecUtil.b()) {
                z = true;
            }
            if (!z) {
                c("legacyFrameSize, " + format.l + "x" + format.m);
            }
            return z;
        }
        if (Util.f3651a >= 21) {
            int i3 = format.u;
            if (i3 != -1 && !b(i3)) {
                return false;
            }
            int i4 = format.t;
            if (i4 == -1 || a(i4)) {
                return true;
            }
            return false;
        }
        return true;
    }

    @TargetApi(21)
    public boolean b(int i) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.c;
        if (codecCapabilities == null) {
            c("sampleRate.caps");
            return false;
        }
        MediaCodecInfo.AudioCapabilities audioCapabilities = codecCapabilities.getAudioCapabilities();
        if (audioCapabilities == null) {
            c("sampleRate.aCaps");
            return false;
        } else if (audioCapabilities.isSampleRateSupported(i)) {
            return true;
        } else {
            c("sampleRate.support, " + i);
            return false;
        }
    }

    private void b(String str) {
        Log.a("MediaCodecInfo", "AssumedSupport [" + str + "] [" + this.f3359a + ", " + this.b + "] [" + Util.e + "]");
    }

    @TargetApi(19)
    private static boolean b(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("adaptive-playback");
    }

    public boolean a(String str) {
        String d2;
        if (str == null || this.b == null || (d2 = MimeTypes.d(str)) == null) {
            return true;
        }
        if (!this.b.equals(d2)) {
            c("codec.mime " + str + ", " + d2);
            return false;
        }
        Pair<Integer, Integer> b2 = MediaCodecUtil.b(str);
        if (b2 == null) {
            return true;
        }
        for (MediaCodecInfo.CodecProfileLevel codecProfileLevel : a()) {
            if (codecProfileLevel.profile == ((Integer) b2.first).intValue() && codecProfileLevel.level >= ((Integer) b2.second).intValue()) {
                return true;
            }
        }
        c("codec.profileLevel, " + str + ", " + d2);
        return false;
    }

    public boolean a(Format format, Format format2, boolean z) {
        if (!this.h) {
            if ("audio/mp4a-latm".equals(this.b) && format.g.equals(format2.g) && format.t == format2.t && format.u == format2.u) {
                Pair<Integer, Integer> b2 = MediaCodecUtil.b(format.d);
                Pair<Integer, Integer> b3 = MediaCodecUtil.b(format2.d);
                if (!(b2 == null || b3 == null)) {
                    int intValue = ((Integer) b2.first).intValue();
                    int intValue2 = ((Integer) b3.first).intValue();
                    if (intValue == 42 && intValue2 == 42) {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        } else if (!format.g.equals(format2.g) || format.o != format2.o || ((!this.d && (format.l != format2.l || format.m != format2.m)) || ((z || format2.s != null) && !Util.a((Object) format.s, (Object) format2.s)))) {
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(21)
    public boolean a(int i, int i2, double d2) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.c;
        if (codecCapabilities == null) {
            c("sizeAndRate.caps");
            return false;
        }
        MediaCodecInfo.VideoCapabilities videoCapabilities = codecCapabilities.getVideoCapabilities();
        if (videoCapabilities == null) {
            c("sizeAndRate.vCaps");
            return false;
        } else if (a(videoCapabilities, i, i2, d2)) {
            return true;
        } else {
            if (i >= i2 || !a(videoCapabilities, i2, i, d2)) {
                c("sizeAndRate.support, " + i + "x" + i2 + "x" + d2);
                return false;
            }
            b("sizeAndRate.rotated, " + i + "x" + i2 + "x" + d2);
            return true;
        }
    }

    @TargetApi(21)
    public Point a(int i, int i2) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.c;
        if (codecCapabilities == null) {
            c("align.caps");
            return null;
        }
        MediaCodecInfo.VideoCapabilities videoCapabilities = codecCapabilities.getVideoCapabilities();
        if (videoCapabilities == null) {
            c("align.vCaps");
            return null;
        }
        int widthAlignment = videoCapabilities.getWidthAlignment();
        int heightAlignment = videoCapabilities.getHeightAlignment();
        return new Point(Util.a(i, widthAlignment) * widthAlignment, Util.a(i2, heightAlignment) * heightAlignment);
    }

    @TargetApi(21)
    public boolean a(int i) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.c;
        if (codecCapabilities == null) {
            c("channelCount.caps");
            return false;
        }
        MediaCodecInfo.AudioCapabilities audioCapabilities = codecCapabilities.getAudioCapabilities();
        if (audioCapabilities == null) {
            c("channelCount.aCaps");
            return false;
        } else if (a(this.f3359a, this.b, audioCapabilities.getMaxInputChannelCount()) >= i) {
            return true;
        } else {
            c("channelCount.support, " + i);
            return false;
        }
    }

    private static int a(String str, String str2, int i) {
        int i2;
        if (i > 1 || ((Util.f3651a >= 26 && i > 0) || "audio/mpeg".equals(str2) || "audio/3gpp".equals(str2) || "audio/amr-wb".equals(str2) || "audio/mp4a-latm".equals(str2) || "audio/vorbis".equals(str2) || "audio/opus".equals(str2) || "audio/raw".equals(str2) || "audio/flac".equals(str2) || "audio/g711-alaw".equals(str2) || "audio/g711-mlaw".equals(str2) || "audio/gsm".equals(str2))) {
            return i;
        }
        if ("audio/ac3".equals(str2)) {
            i2 = 6;
        } else {
            i2 = "audio/eac3".equals(str2) ? 16 : 30;
        }
        Log.d("MediaCodecInfo", "AssumedMaxChannelAdjustment: " + str + ", [" + i + " to " + i2 + "]");
        return i2;
    }

    private static boolean a(MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return Util.f3651a >= 19 && b(codecCapabilities);
    }

    @TargetApi(21)
    private static boolean a(MediaCodecInfo.VideoCapabilities videoCapabilities, int i, int i2, double d2) {
        if (d2 == -1.0d || d2 <= 0.0d) {
            return videoCapabilities.isSizeSupported(i, i2);
        }
        return videoCapabilities.areSizeAndRateSupported(i, i2, d2);
    }
}
