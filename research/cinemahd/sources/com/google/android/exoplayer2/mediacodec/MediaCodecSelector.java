package com.google.android.exoplayer2.mediacodec;

import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import java.util.Collections;
import java.util.List;

public interface MediaCodecSelector {

    /* renamed from: a  reason: collision with root package name */
    public static final MediaCodecSelector f3360a = new MediaCodecSelector() {
        public List<MediaCodecInfo> a(String str, boolean z) throws MediaCodecUtil.DecoderQueryException {
            List<MediaCodecInfo> b = MediaCodecUtil.b(str, z);
            if (b.isEmpty()) {
                return Collections.emptyList();
            }
            return Collections.singletonList(b.get(0));
        }

        public MediaCodecInfo a() throws MediaCodecUtil.DecoderQueryException {
            return MediaCodecUtil.a();
        }
    };

    static {
        new MediaCodecSelector() {
            public List<MediaCodecInfo> a(String str, boolean z) throws MediaCodecUtil.DecoderQueryException {
                return MediaCodecUtil.b(str, z);
            }

            public MediaCodecInfo a() throws MediaCodecUtil.DecoderQueryException {
                return MediaCodecUtil.a();
            }
        };
    }

    MediaCodecInfo a() throws MediaCodecUtil.DecoderQueryException;

    List<MediaCodecInfo> a(String str, boolean z) throws MediaCodecUtil.DecoderQueryException;
}
