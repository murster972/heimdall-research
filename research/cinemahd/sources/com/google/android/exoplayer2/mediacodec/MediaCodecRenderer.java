package com.google.android.exoplayer2.mediacodec;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.SystemClock;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.TimedValueQueue;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

@TargetApi(16)
public abstract class MediaCodecRenderer extends BaseRenderer {
    private static final byte[] j0 = Util.a("0000016742C00BDA259000000168CE0F13200000016588840DCE7118A0002FBF1C31C3275D78");
    private float A;
    private boolean B;
    private ArrayDeque<MediaCodecInfo> C;
    private DecoderInitializationException D;
    private MediaCodecInfo E;
    private int F;
    private boolean G;
    private boolean H;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    private boolean M;
    private boolean N;
    private boolean O;
    private ByteBuffer[] P;
    private ByteBuffer[] Q;
    private long R;
    private int S;
    private int T;
    private ByteBuffer U;
    private boolean V;
    private boolean W;
    private int X;
    private int Y;
    private boolean c0;
    private boolean d0;
    private boolean e0;
    private boolean f0;
    private boolean g0;
    private boolean h0;
    protected DecoderCounters i0;
    private final MediaCodecSelector j;
    private final DrmSessionManager<FrameworkMediaCrypto> k;
    private final boolean l;
    private final float m;
    private final DecoderInputBuffer n;
    private final DecoderInputBuffer o;
    private final FormatHolder p;
    private final TimedValueQueue<Format> q;
    private final List<Long> r;
    private final MediaCodec.BufferInfo s;
    private Format t;
    private Format u;
    private Format v;
    private DrmSession<FrameworkMediaCrypto> w;
    private DrmSession<FrameworkMediaCrypto> x;
    private MediaCodec y;
    private float z;

    public static class DecoderInitializationException extends Exception {
        public final String decoderName;
        public final String diagnosticInfo;
        public final DecoderInitializationException fallbackDecoderInitializationException;
        public final String mimeType;
        public final boolean secureDecoderRequired;

        public DecoderInitializationException(Format format, Throwable th, boolean z, int i) {
            this("Decoder init failed: [" + i + "], " + format, th, format.g, z, (String) null, a(i), (DecoderInitializationException) null);
        }

        /* access modifiers changed from: private */
        public DecoderInitializationException a(DecoderInitializationException decoderInitializationException) {
            return new DecoderInitializationException(getMessage(), getCause(), this.mimeType, this.secureDecoderRequired, this.decoderName, this.diagnosticInfo, decoderInitializationException);
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public DecoderInitializationException(Format format, Throwable th, boolean z, String str) {
            this("Decoder init failed: " + str + ", " + format, th, format.g, z, str, Util.f3651a >= 21 ? a(th) : null, (DecoderInitializationException) null);
        }

        @TargetApi(21)
        private static String a(Throwable th) {
            if (th instanceof MediaCodec.CodecException) {
                return ((MediaCodec.CodecException) th).getDiagnosticInfo();
            }
            return null;
        }

        private DecoderInitializationException(String str, Throwable th, String str2, boolean z, String str3, String str4, DecoderInitializationException decoderInitializationException) {
            super(str, th);
            this.mimeType = str2;
            this.secureDecoderRequired = z;
            this.decoderName = str3;
            this.diagnosticInfo = str4;
            this.fallbackDecoderInitializationException = decoderInitializationException;
        }

        private static String a(int i) {
            String str = i < 0 ? "neg_" : "";
            return "com.google.android.exoplayer.MediaCodecTrackRenderer_" + str + Math.abs(i);
        }
    }

    public MediaCodecRenderer(int i, MediaCodecSelector mediaCodecSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, boolean z2, float f) {
        super(i);
        Assertions.b(Util.f3651a >= 16);
        Assertions.a(mediaCodecSelector);
        this.j = mediaCodecSelector;
        this.k = drmSessionManager;
        this.l = z2;
        this.m = f;
        this.n = new DecoderInputBuffer(0);
        this.o = DecoderInputBuffer.e();
        this.p = new FormatHolder();
        this.q = new TimedValueQueue<>();
        this.r = new ArrayList();
        this.s = new MediaCodec.BufferInfo();
        this.X = 0;
        this.Y = 0;
        this.A = -1.0f;
        this.z = 1.0f;
    }

    private boolean A() {
        return "Amazon".equals(Util.c) && ("AFTM".equals(Util.d) || "AFTB".equals(Util.d));
    }

    private boolean B() throws ExoPlaybackException {
        int i;
        int i2;
        MediaCodec mediaCodec = this.y;
        if (mediaCodec == null || this.Y == 2 || this.e0) {
            return false;
        }
        if (this.S < 0) {
            this.S = mediaCodec.dequeueInputBuffer(0);
            int i3 = this.S;
            if (i3 < 0) {
                return false;
            }
            this.n.b = a(i3);
            this.n.clear();
        }
        if (this.Y == 1) {
            if (!this.O) {
                this.d0 = true;
                this.y.queueInputBuffer(this.S, 0, 0, 0, 4);
                I();
            }
            this.Y = 2;
            return false;
        } else if (this.M) {
            this.M = false;
            this.n.b.put(j0);
            this.y.queueInputBuffer(this.S, 0, j0.length, 0, 0);
            I();
            this.c0 = true;
            return true;
        } else {
            if (this.g0) {
                i2 = -4;
                i = 0;
            } else {
                if (this.X == 1) {
                    for (int i4 = 0; i4 < this.t.i.size(); i4++) {
                        this.n.b.put(this.t.i.get(i4));
                    }
                    this.X = 2;
                }
                i = this.n.b.position();
                i2 = a(this.p, this.n, false);
            }
            if (i2 == -3) {
                return false;
            }
            if (i2 == -5) {
                if (this.X == 2) {
                    this.n.clear();
                    this.X = 1;
                }
                b(this.p.f3165a);
                return true;
            } else if (this.n.isEndOfStream()) {
                if (this.X == 2) {
                    this.n.clear();
                    this.X = 1;
                }
                this.e0 = true;
                if (!this.c0) {
                    D();
                    return false;
                }
                try {
                    if (!this.O) {
                        this.d0 = true;
                        this.y.queueInputBuffer(this.S, 0, 0, 0, 4);
                        I();
                    }
                    return false;
                } catch (MediaCodec.CryptoException e) {
                    throw ExoPlaybackException.a(e, m());
                }
            } else if (!this.h0 || this.n.isKeyFrame()) {
                this.h0 = false;
                boolean c = this.n.c();
                this.g0 = c(c);
                if (this.g0) {
                    return false;
                }
                if (this.H && !c) {
                    NalUnitUtil.a(this.n.b);
                    if (this.n.b.position() == 0) {
                        return true;
                    }
                    this.H = false;
                }
                try {
                    long j2 = this.n.c;
                    if (this.n.isDecodeOnly()) {
                        this.r.add(Long.valueOf(j2));
                    }
                    if (this.u != null) {
                        this.q.a(j2, this.u);
                        this.u = null;
                    }
                    this.n.b();
                    a(this.n);
                    if (c) {
                        this.y.queueSecureInputBuffer(this.S, 0, a(this.n, i), j2, 0);
                    } else {
                        this.y.queueInputBuffer(this.S, 0, this.n.b.limit(), j2, 0);
                    }
                    I();
                    this.c0 = true;
                    this.X = 0;
                    this.i0.c++;
                    return true;
                } catch (MediaCodec.CryptoException e2) {
                    throw ExoPlaybackException.a(e2, m());
                }
            } else {
                this.n.clear();
                if (this.X == 2) {
                    this.X = 1;
                }
                return true;
            }
        }
    }

    private boolean C() {
        return this.T >= 0;
    }

    private void D() throws ExoPlaybackException {
        if (this.Y == 2) {
            y();
            x();
            return;
        }
        this.f0 = true;
        z();
    }

    private void E() {
        if (Util.f3651a < 21) {
            this.Q = this.y.getOutputBuffers();
        }
    }

    private void F() throws ExoPlaybackException {
        MediaFormat outputFormat = this.y.getOutputFormat();
        if (this.F != 0 && outputFormat.getInteger("width") == 32 && outputFormat.getInteger("height") == 32) {
            this.N = true;
            return;
        }
        if (this.L) {
            outputFormat.setInteger("channel-count", 1);
        }
        a(this.y, outputFormat);
    }

    private void G() throws ExoPlaybackException {
        this.C = null;
        if (this.c0) {
            this.Y = 1;
            return;
        }
        y();
        x();
    }

    private void H() {
        if (Util.f3651a < 21) {
            this.P = null;
            this.Q = null;
        }
    }

    private void I() {
        this.S = -1;
        this.n.b = null;
    }

    private void J() {
        this.T = -1;
        this.U = null;
    }

    private void K() throws ExoPlaybackException {
        Format format = this.t;
        if (format != null && Util.f3651a >= 23) {
            float a2 = a(this.z, format, n());
            if (this.A != a2) {
                this.A = a2;
                if (this.y != null && this.Y == 0) {
                    int i = (a2 > -1.0f ? 1 : (a2 == -1.0f ? 0 : -1));
                    if (i == 0 && this.B) {
                        G();
                    } else if (i == 0) {
                    } else {
                        if (this.B || a2 > this.m) {
                            Bundle bundle = new Bundle();
                            bundle.putFloat("operating-rate", a2);
                            this.y.setParameters(bundle);
                            this.B = true;
                        }
                    }
                }
            }
        }
    }

    private List<MediaCodecInfo> b(boolean z2) throws MediaCodecUtil.DecoderQueryException {
        List<MediaCodecInfo> a2 = a(this.j, this.t, z2);
        if (a2.isEmpty() && z2) {
            a2 = a(this.j, this.t, false);
            if (!a2.isEmpty()) {
                Log.d("MediaCodecRenderer", "Drm session requires secure decoder for " + this.t.g + ", but no secure decoder available. Trying to proceed with " + a2 + ".");
            }
        }
        return a2;
    }

    private boolean c(boolean z2) throws ExoPlaybackException {
        if (this.w == null || (!z2 && this.l)) {
            return false;
        }
        int state = this.w.getState();
        if (state != 1) {
            return state != 4;
        }
        throw ExoPlaybackException.a(this.w.b(), m());
    }

    private boolean e(long j2) {
        int size = this.r.size();
        for (int i = 0; i < size; i++) {
            if (this.r.get(i).longValue() == j2) {
                this.r.remove(i);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract float a(float f, Format format, Format[] formatArr);

    /* access modifiers changed from: protected */
    public abstract int a(MediaCodec mediaCodec, MediaCodecInfo mediaCodecInfo, Format format, Format format2);

    public final int a(Format format) throws ExoPlaybackException {
        try {
            return a(this.j, this.k, format);
        } catch (MediaCodecUtil.DecoderQueryException e) {
            throw ExoPlaybackException.a(e, m());
        }
    }

    /* access modifiers changed from: protected */
    public abstract int a(MediaCodecSelector mediaCodecSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, Format format) throws MediaCodecUtil.DecoderQueryException;

    /* access modifiers changed from: protected */
    public abstract void a(MediaCodec mediaCodec, MediaFormat mediaFormat) throws ExoPlaybackException;

    /* access modifiers changed from: protected */
    public abstract void a(DecoderInputBuffer decoderInputBuffer);

    /* access modifiers changed from: protected */
    public abstract void a(MediaCodecInfo mediaCodecInfo, MediaCodec mediaCodec, Format format, MediaCrypto mediaCrypto, float f) throws MediaCodecUtil.DecoderQueryException;

    /* access modifiers changed from: protected */
    public abstract void a(String str, long j2, long j3);

    /* access modifiers changed from: protected */
    public abstract boolean a(long j2, long j3, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j4, boolean z2, Format format) throws ExoPlaybackException;

    /* access modifiers changed from: protected */
    public boolean a(MediaCodecInfo mediaCodecInfo) {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract void c(long j2);

    /* access modifiers changed from: protected */
    public final Format d(long j2) {
        Format a2 = this.q.a(j2);
        if (a2 != null) {
            this.v = a2;
        }
        return a2;
    }

    public boolean isReady() {
        return this.t != null && !this.g0 && (o() || C() || (this.R != -9223372036854775807L && SystemClock.elapsedRealtime() < this.R));
    }

    public final int k() {
        return 8;
    }

    /* access modifiers changed from: protected */
    public void p() {
        this.t = null;
        this.C = null;
        try {
            y();
            try {
                if (this.w != null) {
                    this.k.a(this.w);
                }
                try {
                    if (!(this.x == null || this.x == this.w)) {
                        this.k.a(this.x);
                    }
                } finally {
                    this.w = null;
                    this.x = null;
                }
            } catch (Throwable th) {
                if (!(this.x == null || this.x == this.w)) {
                    this.k.a(this.x);
                }
                throw th;
            } finally {
                this.w = null;
                this.x = null;
            }
        } catch (Throwable th2) {
            try {
                if (!(this.x == null || this.x == this.w)) {
                    this.k.a(this.x);
                }
                throw th2;
            } finally {
                this.w = null;
                this.x = null;
            }
        } finally {
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
    }

    /* access modifiers changed from: protected */
    public void r() {
    }

    /* access modifiers changed from: protected */
    public void s() throws ExoPlaybackException {
        this.R = -9223372036854775807L;
        I();
        J();
        this.h0 = true;
        this.g0 = false;
        this.V = false;
        this.r.clear();
        this.M = false;
        this.N = false;
        if (this.I || (this.J && this.d0)) {
            y();
            x();
        } else if (this.Y != 0) {
            y();
            x();
        } else {
            this.y.flush();
            this.c0 = false;
        }
        if (this.W && this.t != null) {
            this.X = 1;
        }
    }

    /* access modifiers changed from: protected */
    public final MediaCodec t() {
        return this.y;
    }

    /* access modifiers changed from: protected */
    public final MediaCodecInfo u() {
        return this.E;
    }

    /* access modifiers changed from: protected */
    public boolean v() {
        return false;
    }

    /* access modifiers changed from: protected */
    public long w() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void x() throws ExoPlaybackException {
        Format format;
        boolean z2;
        if (this.y == null && (format = this.t) != null) {
            this.w = this.x;
            String str = format.g;
            MediaCrypto mediaCrypto = null;
            DrmSession<FrameworkMediaCrypto> drmSession = this.w;
            boolean z3 = false;
            if (drmSession != null) {
                FrameworkMediaCrypto a2 = drmSession.a();
                if (a2 != null) {
                    mediaCrypto = a2.a();
                    z2 = a2.a(str);
                } else if (this.w.b() != null) {
                    z2 = false;
                } else {
                    return;
                }
                if (A()) {
                    int state = this.w.getState();
                    if (state == 1) {
                        throw ExoPlaybackException.a(this.w.b(), m());
                    } else if (state != 4) {
                        return;
                    }
                }
            } else {
                z2 = false;
            }
            try {
                if (a(mediaCrypto, z2)) {
                    String str2 = this.E.f3359a;
                    this.F = a(str2);
                    this.G = e(str2);
                    this.H = a(str2, this.t);
                    this.I = d(str2);
                    this.J = b(str2);
                    this.K = c(str2);
                    this.L = b(str2, this.t);
                    if (b(this.E) || v()) {
                        z3 = true;
                    }
                    this.O = z3;
                    this.R = getState() == 2 ? SystemClock.elapsedRealtime() + 1000 : -9223372036854775807L;
                    I();
                    J();
                    this.h0 = true;
                    this.i0.f3215a++;
                }
            } catch (DecoderInitializationException e) {
                throw ExoPlaybackException.a(e, m());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void y() {
        this.R = -9223372036854775807L;
        I();
        J();
        this.g0 = false;
        this.V = false;
        this.r.clear();
        H();
        this.E = null;
        this.W = false;
        this.c0 = false;
        this.H = false;
        this.I = false;
        this.F = 0;
        this.G = false;
        this.J = false;
        this.L = false;
        this.M = false;
        this.N = false;
        this.O = false;
        this.d0 = false;
        this.X = 0;
        this.Y = 0;
        this.B = false;
        MediaCodec mediaCodec = this.y;
        if (mediaCodec != null) {
            this.i0.b++;
            try {
                mediaCodec.stop();
                try {
                    this.y.release();
                    this.y = null;
                    DrmSession<FrameworkMediaCrypto> drmSession = this.w;
                    if (drmSession != null && this.x != drmSession) {
                        try {
                            this.k.a(drmSession);
                        } finally {
                            this.w = null;
                        }
                    }
                } catch (Throwable th) {
                    this.y = null;
                    DrmSession<FrameworkMediaCrypto> drmSession2 = this.w;
                    if (!(drmSession2 == null || this.x == drmSession2)) {
                        this.k.a(drmSession2);
                    }
                    throw th;
                } finally {
                    this.w = null;
                }
            } catch (Throwable th2) {
                this.y = null;
                DrmSession<FrameworkMediaCrypto> drmSession3 = this.w;
                if (!(drmSession3 == null || this.x == drmSession3)) {
                    try {
                        this.k.a(drmSession3);
                    } finally {
                        this.w = null;
                    }
                }
                throw th2;
            } finally {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void z() throws ExoPlaybackException {
    }

    private static boolean d(String str) {
        int i = Util.f3651a;
        return i < 18 || (i == 18 && ("OMX.SEC.avc.dec".equals(str) || "OMX.SEC.avc.dec.secure".equals(str))) || (Util.f3651a == 19 && Util.d.startsWith("SM-G800") && ("OMX.Exynos.avc.dec".equals(str) || "OMX.Exynos.avc.dec.secure".equals(str)));
    }

    /* access modifiers changed from: protected */
    public List<MediaCodecInfo> a(MediaCodecSelector mediaCodecSelector, Format format, boolean z2) throws MediaCodecUtil.DecoderQueryException {
        return mediaCodecSelector.a(format.g, z2);
    }

    private static boolean c(String str) {
        return Util.f3651a == 21 && "OMX.google.aac.decoder".equals(str);
    }

    private static boolean e(String str) {
        return Util.d.startsWith("SM-T230") && "OMX.MARVELL.VIDEO.HW.CODA7542DECODER".equals(str);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) throws ExoPlaybackException {
        this.i0 = new DecoderCounters();
    }

    /* access modifiers changed from: protected */
    public void a(long j2, boolean z2) throws ExoPlaybackException {
        this.e0 = false;
        this.f0 = false;
        if (this.y != null) {
            s();
        }
        this.q.a();
    }

    private ByteBuffer b(int i) {
        if (Util.f3651a >= 21) {
            return this.y.getOutputBuffer(i);
        }
        return this.Q[i];
    }

    public final void a(float f) throws ExoPlaybackException {
        this.z = f;
        K();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0084, code lost:
        if (r6.m == r0.m) goto L_0x0086;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0097  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.google.android.exoplayer2.Format r6) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            r5 = this;
            com.google.android.exoplayer2.Format r0 = r5.t
            r5.t = r6
            r5.u = r6
            com.google.android.exoplayer2.Format r6 = r5.t
            com.google.android.exoplayer2.drm.DrmInitData r6 = r6.j
            r1 = 0
            if (r0 != 0) goto L_0x000f
            r2 = r1
            goto L_0x0011
        L_0x000f:
            com.google.android.exoplayer2.drm.DrmInitData r2 = r0.j
        L_0x0011:
            boolean r6 = com.google.android.exoplayer2.util.Util.a((java.lang.Object) r6, (java.lang.Object) r2)
            r2 = 1
            r6 = r6 ^ r2
            if (r6 == 0) goto L_0x004f
            com.google.android.exoplayer2.Format r6 = r5.t
            com.google.android.exoplayer2.drm.DrmInitData r6 = r6.j
            if (r6 == 0) goto L_0x004d
            com.google.android.exoplayer2.drm.DrmSessionManager<com.google.android.exoplayer2.drm.FrameworkMediaCrypto> r6 = r5.k
            if (r6 == 0) goto L_0x003d
            android.os.Looper r1 = android.os.Looper.myLooper()
            com.google.android.exoplayer2.Format r3 = r5.t
            com.google.android.exoplayer2.drm.DrmInitData r3 = r3.j
            com.google.android.exoplayer2.drm.DrmSession r6 = r6.a(r1, r3)
            r5.x = r6
            com.google.android.exoplayer2.drm.DrmSession<com.google.android.exoplayer2.drm.FrameworkMediaCrypto> r6 = r5.x
            com.google.android.exoplayer2.drm.DrmSession<com.google.android.exoplayer2.drm.FrameworkMediaCrypto> r1 = r5.w
            if (r6 != r1) goto L_0x004f
            com.google.android.exoplayer2.drm.DrmSessionManager<com.google.android.exoplayer2.drm.FrameworkMediaCrypto> r1 = r5.k
            r1.a(r6)
            goto L_0x004f
        L_0x003d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "Media requires a DrmSessionManager"
            r6.<init>(r0)
            int r0 = r5.m()
            com.google.android.exoplayer2.ExoPlaybackException r6 = com.google.android.exoplayer2.ExoPlaybackException.a(r6, r0)
            throw r6
        L_0x004d:
            r5.x = r1
        L_0x004f:
            com.google.android.exoplayer2.drm.DrmSession<com.google.android.exoplayer2.drm.FrameworkMediaCrypto> r6 = r5.x
            com.google.android.exoplayer2.drm.DrmSession<com.google.android.exoplayer2.drm.FrameworkMediaCrypto> r1 = r5.w
            r3 = 0
            if (r6 != r1) goto L_0x0090
            android.media.MediaCodec r6 = r5.y
            if (r6 == 0) goto L_0x0090
            com.google.android.exoplayer2.mediacodec.MediaCodecInfo r1 = r5.E
            com.google.android.exoplayer2.Format r4 = r5.t
            int r6 = r5.a(r6, r1, r0, r4)
            if (r6 == 0) goto L_0x0090
            if (r6 == r2) goto L_0x0091
            r1 = 3
            if (r6 != r1) goto L_0x008a
            boolean r6 = r5.G
            if (r6 != 0) goto L_0x0090
            r5.W = r2
            r5.X = r2
            int r6 = r5.F
            r1 = 2
            if (r6 == r1) goto L_0x0086
            if (r6 != r2) goto L_0x0087
            com.google.android.exoplayer2.Format r6 = r5.t
            int r1 = r6.l
            int r4 = r0.l
            if (r1 != r4) goto L_0x0087
            int r6 = r6.m
            int r0 = r0.m
            if (r6 != r0) goto L_0x0087
        L_0x0086:
            r3 = 1
        L_0x0087:
            r5.M = r3
            goto L_0x0091
        L_0x008a:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            r6.<init>()
            throw r6
        L_0x0090:
            r2 = 0
        L_0x0091:
            if (r2 != 0) goto L_0x0097
            r5.G()
            goto L_0x009a
        L_0x0097:
            r5.K()
        L_0x009a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.b(com.google.android.exoplayer2.Format):void");
    }

    public void a(long j2, long j3) throws ExoPlaybackException {
        if (this.f0) {
            z();
            return;
        }
        if (this.t == null) {
            this.o.clear();
            int a2 = a(this.p, this.o, true);
            if (a2 == -5) {
                b(this.p.f3165a);
            } else if (a2 == -4) {
                Assertions.b(this.o.isEndOfStream());
                this.e0 = true;
                D();
                return;
            } else {
                return;
            }
        }
        x();
        if (this.y != null) {
            TraceUtil.a("drainAndFeed");
            do {
            } while (b(j2, j3));
            do {
            } while (B());
            TraceUtil.a();
        } else {
            this.i0.d += b(j2);
            this.o.clear();
            int a3 = a(this.p, this.o, false);
            if (a3 == -5) {
                b(this.p.f3165a);
            } else if (a3 == -4) {
                Assertions.b(this.o.isEndOfStream());
                this.e0 = true;
                D();
            }
        }
        this.i0.a();
    }

    private boolean b(long j2, long j3) throws ExoPlaybackException {
        boolean z2;
        int i;
        if (!C()) {
            if (!this.K || !this.d0) {
                i = this.y.dequeueOutputBuffer(this.s, w());
            } else {
                try {
                    i = this.y.dequeueOutputBuffer(this.s, w());
                } catch (IllegalStateException unused) {
                    D();
                    if (this.f0) {
                        y();
                    }
                    return false;
                }
            }
            if (i < 0) {
                if (i == -2) {
                    F();
                    return true;
                } else if (i == -3) {
                    E();
                    return true;
                } else {
                    if (this.O && (this.e0 || this.Y == 2)) {
                        D();
                    }
                    return false;
                }
            } else if (this.N) {
                this.N = false;
                this.y.releaseOutputBuffer(i, false);
                return true;
            } else {
                MediaCodec.BufferInfo bufferInfo = this.s;
                if (bufferInfo.size != 0 || (bufferInfo.flags & 4) == 0) {
                    this.T = i;
                    this.U = b(i);
                    ByteBuffer byteBuffer = this.U;
                    if (byteBuffer != null) {
                        byteBuffer.position(this.s.offset);
                        ByteBuffer byteBuffer2 = this.U;
                        MediaCodec.BufferInfo bufferInfo2 = this.s;
                        byteBuffer2.limit(bufferInfo2.offset + bufferInfo2.size);
                    }
                    this.V = e(this.s.presentationTimeUs);
                    d(this.s.presentationTimeUs);
                } else {
                    D();
                    return false;
                }
            }
        }
        if (!this.K || !this.d0) {
            MediaCodec mediaCodec = this.y;
            ByteBuffer byteBuffer3 = this.U;
            int i2 = this.T;
            MediaCodec.BufferInfo bufferInfo3 = this.s;
            z2 = a(j2, j3, mediaCodec, byteBuffer3, i2, bufferInfo3.flags, bufferInfo3.presentationTimeUs, this.V, this.v);
        } else {
            try {
                z2 = a(j2, j3, this.y, this.U, this.T, this.s.flags, this.s.presentationTimeUs, this.V, this.v);
            } catch (IllegalStateException unused2) {
                D();
                if (this.f0) {
                    y();
                }
                return false;
            }
        }
        if (z2) {
            c(this.s.presentationTimeUs);
            boolean z3 = (this.s.flags & 4) != 0;
            J();
            if (!z3) {
                return true;
            }
            D();
        }
        return false;
    }

    private boolean a(MediaCrypto mediaCrypto, boolean z2) throws DecoderInitializationException {
        if (this.C == null) {
            try {
                this.C = new ArrayDeque<>(b(z2));
                this.D = null;
            } catch (MediaCodecUtil.DecoderQueryException e) {
                throw new DecoderInitializationException(this.t, (Throwable) e, z2, -49998);
            }
        }
        if (!this.C.isEmpty()) {
            do {
                MediaCodecInfo peekFirst = this.C.peekFirst();
                if (!a(peekFirst)) {
                    return false;
                }
                try {
                    a(peekFirst, mediaCrypto);
                    return true;
                } catch (Exception e2) {
                    Log.b("MediaCodecRenderer", "Failed to initialize decoder: " + peekFirst, e2);
                    this.C.removeFirst();
                    DecoderInitializationException decoderInitializationException = new DecoderInitializationException(this.t, (Throwable) e2, z2, peekFirst.f3359a);
                    DecoderInitializationException decoderInitializationException2 = this.D;
                    if (decoderInitializationException2 == null) {
                        this.D = decoderInitializationException;
                    } else {
                        this.D = decoderInitializationException2.a(decoderInitializationException);
                    }
                    if (this.C.isEmpty()) {
                        throw this.D;
                    }
                }
            } while (this.C.isEmpty());
            throw this.D;
        }
        throw new DecoderInitializationException(this.t, (Throwable) null, z2, -49999);
    }

    private void a(MediaCodecInfo mediaCodecInfo, MediaCrypto mediaCrypto) throws Exception {
        String str = mediaCodecInfo.f3359a;
        K();
        boolean z2 = this.A > this.m;
        MediaCodec mediaCodec = null;
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            TraceUtil.a("createCodec:" + str);
            MediaCodec createByCodecName = MediaCodec.createByCodecName(str);
            TraceUtil.a();
            TraceUtil.a("configureCodec");
            a(mediaCodecInfo, createByCodecName, this.t, mediaCrypto, z2 ? this.A : -1.0f);
            this.B = z2;
            TraceUtil.a();
            TraceUtil.a("startCodec");
            createByCodecName.start();
            TraceUtil.a();
            long elapsedRealtime2 = SystemClock.elapsedRealtime();
            a(createByCodecName);
            this.y = createByCodecName;
            this.E = mediaCodecInfo;
            a(str, elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
        } catch (Exception e) {
            if (mediaCodec != null) {
                H();
                mediaCodec.release();
            }
            throw e;
        }
    }

    private static boolean b(MediaCodecInfo mediaCodecInfo) {
        String str = mediaCodecInfo.f3359a;
        return (Util.f3651a <= 17 && ("OMX.rk.video_decoder.avc".equals(str) || "OMX.allwinner.video.decoder.avc".equals(str))) || ("Amazon".equals(Util.c) && "AFTS".equals(Util.d) && mediaCodecInfo.f);
    }

    private void a(MediaCodec mediaCodec) {
        if (Util.f3651a < 21) {
            this.P = mediaCodec.getInputBuffers();
            this.Q = mediaCodec.getOutputBuffers();
        }
    }

    private static boolean b(String str) {
        return (Util.f3651a <= 23 && "OMX.google.vorbis.decoder".equals(str)) || (Util.f3651a <= 19 && "hb2000".equals(Util.b) && ("OMX.amlogic.avc.decoder.awesome".equals(str) || "OMX.amlogic.avc.decoder.awesome.secure".equals(str)));
    }

    private ByteBuffer a(int i) {
        if (Util.f3651a >= 21) {
            return this.y.getInputBuffer(i);
        }
        return this.P[i];
    }

    private static boolean b(String str, Format format) {
        if (Util.f3651a > 18 || format.t != 1 || !"OMX.MTK.AUDIO.DECODER.MP3".equals(str)) {
            return false;
        }
        return true;
    }

    public boolean a() {
        return this.f0;
    }

    private static MediaCodec.CryptoInfo a(DecoderInputBuffer decoderInputBuffer, int i) {
        MediaCodec.CryptoInfo a2 = decoderInputBuffer.f3216a.a();
        if (i == 0) {
            return a2;
        }
        if (a2.numBytesOfClearData == null) {
            a2.numBytesOfClearData = new int[1];
        }
        int[] iArr = a2.numBytesOfClearData;
        iArr[0] = iArr[0] + i;
        return a2;
    }

    private int a(String str) {
        if (Util.f3651a <= 25 && "OMX.Exynos.avc.dec.secure".equals(str) && (Util.d.startsWith("SM-T585") || Util.d.startsWith("SM-A510") || Util.d.startsWith("SM-A520") || Util.d.startsWith("SM-J700"))) {
            return 2;
        }
        if (Util.f3651a >= 24) {
            return 0;
        }
        if ("OMX.Nvidia.h264.decode".equals(str) || "OMX.Nvidia.h264.decode.secure".equals(str)) {
            return ("flounder".equals(Util.b) || "flounder_lte".equals(Util.b) || "grouper".equals(Util.b) || "tilapia".equals(Util.b)) ? 1 : 0;
        }
        return 0;
    }

    private static boolean a(String str, Format format) {
        return Util.f3651a < 21 && format.i.isEmpty() && "OMX.MTK.VIDEO.DECODER.AVC".equals(str);
    }
}
