package com.google.android.exoplayer2;

import com.google.android.exoplayer2.util.Assertions;

public final class PlaybackParameters {
    public static final PlaybackParameters e = new PlaybackParameters(1.0f);

    /* renamed from: a  reason: collision with root package name */
    public final float f3170a;
    public final float b;
    public final boolean c;
    private final int d;

    public PlaybackParameters(float f) {
        this(f, 1.0f, false);
    }

    public long a(long j) {
        return j * ((long) this.d);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || PlaybackParameters.class != obj.getClass()) {
            return false;
        }
        PlaybackParameters playbackParameters = (PlaybackParameters) obj;
        if (this.f3170a == playbackParameters.f3170a && this.b == playbackParameters.b && this.c == playbackParameters.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((527 + Float.floatToRawIntBits(this.f3170a)) * 31) + Float.floatToRawIntBits(this.b)) * 31) + (this.c ? 1 : 0);
    }

    public PlaybackParameters(float f, float f2, boolean z) {
        boolean z2 = true;
        Assertions.a(f > 0.0f);
        Assertions.a(f2 <= 0.0f ? false : z2);
        this.f3170a = f;
        this.b = f2;
        this.c = z;
        this.d = Math.round(f * 1000.0f);
    }
}
