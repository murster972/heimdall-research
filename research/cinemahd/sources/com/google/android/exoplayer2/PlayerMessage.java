package com.google.android.exoplayer2;

import android.os.Handler;
import com.google.android.exoplayer2.util.Assertions;

public final class PlayerMessage {

    /* renamed from: a  reason: collision with root package name */
    private final Target f3171a;
    private final Sender b;
    private final Timeline c;
    private int d;
    private Object e;
    private Handler f;
    private int g;
    private long h = -9223372036854775807L;
    private boolean i = true;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;

    public interface Sender {
        void a(PlayerMessage playerMessage);
    }

    public interface Target {
        void a(int i, Object obj) throws ExoPlaybackException;
    }

    public PlayerMessage(Sender sender, Target target, Timeline timeline, int i2, Handler handler) {
        this.b = sender;
        this.f3171a = target;
        this.c = timeline;
        this.f = handler;
        this.g = i2;
    }

    public PlayerMessage a(int i2) {
        Assertions.b(!this.j);
        this.d = i2;
        return this;
    }

    public boolean b() {
        return this.i;
    }

    public Handler c() {
        return this.f;
    }

    public Object d() {
        return this.e;
    }

    public long e() {
        return this.h;
    }

    public Target f() {
        return this.f3171a;
    }

    public Timeline g() {
        return this.c;
    }

    public int h() {
        return this.d;
    }

    public int i() {
        return this.g;
    }

    public synchronized boolean j() {
        return this.m;
    }

    public PlayerMessage k() {
        Assertions.b(!this.j);
        if (this.h == -9223372036854775807L) {
            Assertions.a(this.i);
        }
        this.j = true;
        this.b.a(this);
        return this;
    }

    public PlayerMessage a(Object obj) {
        Assertions.b(!this.j);
        this.e = obj;
        return this;
    }

    public synchronized boolean a() throws InterruptedException {
        Assertions.b(this.j);
        Assertions.b(this.f.getLooper().getThread() != Thread.currentThread());
        while (!this.l) {
            wait();
        }
        return this.k;
    }

    public synchronized void a(boolean z) {
        this.k = z | this.k;
        this.l = true;
        notifyAll();
    }
}
