package com.google.android.exoplayer2;

import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MediaClock;
import java.io.IOException;

public abstract class BaseRenderer implements Renderer, RendererCapabilities {

    /* renamed from: a  reason: collision with root package name */
    private final int f3150a;
    private RendererConfiguration b;
    private int c;
    private int d;
    private SampleStream e;
    private Format[] f;
    private long g;
    private boolean h = true;
    private boolean i;

    public BaseRenderer(int i2) {
        this.f3150a = i2;
    }

    public /* synthetic */ void a(float f2) throws ExoPlaybackException {
        C0081c.a(this, f2);
    }

    public void a(int i2, Object obj) throws ExoPlaybackException {
    }

    /* access modifiers changed from: protected */
    public abstract void a(long j, boolean z) throws ExoPlaybackException;

    public final void a(RendererConfiguration rendererConfiguration, Format[] formatArr, SampleStream sampleStream, long j, boolean z, long j2) throws ExoPlaybackException {
        Assertions.b(this.d == 0);
        this.b = rendererConfiguration;
        this.d = 1;
        a(z);
        a(formatArr, sampleStream, j2);
        a(j, z);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) throws ExoPlaybackException {
    }

    /* access modifiers changed from: protected */
    public void a(Format[] formatArr, long j) throws ExoPlaybackException {
    }

    /* access modifiers changed from: protected */
    public int b(long j) {
        return this.e.d(j - this.g);
    }

    public final boolean c() {
        return this.h;
    }

    public final void d() {
        this.i = true;
    }

    public final void disable() {
        boolean z = true;
        if (this.d != 1) {
            z = false;
        }
        Assertions.b(z);
        this.d = 0;
        this.e = null;
        this.f = null;
        this.i = false;
        p();
    }

    public final void e() throws IOException {
        this.e.a();
    }

    public final boolean f() {
        return this.i;
    }

    public final RendererCapabilities g() {
        return this;
    }

    public final int getState() {
        return this.d;
    }

    public final int getTrackType() {
        return this.f3150a;
    }

    public final SampleStream i() {
        return this.e;
    }

    public MediaClock j() {
        return null;
    }

    public int k() throws ExoPlaybackException {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final RendererConfiguration l() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final int m() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final Format[] n() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final boolean o() {
        return this.h ? this.i : this.e.isReady();
    }

    /* access modifiers changed from: protected */
    public abstract void p();

    /* access modifiers changed from: protected */
    public void q() throws ExoPlaybackException {
    }

    /* access modifiers changed from: protected */
    public void r() throws ExoPlaybackException {
    }

    public final void setIndex(int i2) {
        this.c = i2;
    }

    public final void start() throws ExoPlaybackException {
        boolean z = true;
        if (this.d != 1) {
            z = false;
        }
        Assertions.b(z);
        this.d = 2;
        q();
    }

    public final void stop() throws ExoPlaybackException {
        Assertions.b(this.d == 2);
        this.d = 1;
        r();
    }

    public final void a(Format[] formatArr, SampleStream sampleStream, long j) throws ExoPlaybackException {
        Assertions.b(!this.i);
        this.e = sampleStream;
        this.h = false;
        this.f = formatArr;
        this.g = j;
        a(formatArr, j);
    }

    public final void a(long j) throws ExoPlaybackException {
        this.i = false;
        this.h = false;
        a(j, false);
    }

    /* access modifiers changed from: protected */
    public final int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
        int a2 = this.e.a(formatHolder, decoderInputBuffer, z);
        if (a2 == -4) {
            if (decoderInputBuffer.isEndOfStream()) {
                this.h = true;
                if (this.i) {
                    return -4;
                }
                return -3;
            }
            decoderInputBuffer.c += this.g;
        } else if (a2 == -5) {
            Format format = formatHolder.f3165a;
            long j = format.k;
            if (j != Clock.MAX_TIME) {
                formatHolder.f3165a = format.a(j + this.g);
            }
        }
        return a2;
    }

    protected static boolean a(DrmSessionManager<?> drmSessionManager, DrmInitData drmInitData) {
        if (drmInitData == null) {
            return true;
        }
        if (drmSessionManager == null) {
            return false;
        }
        return drmSessionManager.a(drmInitData);
    }
}
