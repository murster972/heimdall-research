package com.google.android.exoplayer2.trackselection;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.RendererConfiguration;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class DefaultTrackSelector extends MappingTrackSelector {
    private static final int[] f = new int[0];
    private final TrackSelection.Factory d;
    private final AtomicReference<Parameters> e;

    private static final class AudioConfigurationTuple {

        /* renamed from: a  reason: collision with root package name */
        public final int f3557a;
        public final int b;
        public final String c;

        public AudioConfigurationTuple(int i, int i2, String str) {
            this.f3557a = i;
            this.b = i2;
            this.c = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || AudioConfigurationTuple.class != obj.getClass()) {
                return false;
            }
            AudioConfigurationTuple audioConfigurationTuple = (AudioConfigurationTuple) obj;
            if (this.f3557a == audioConfigurationTuple.f3557a && this.b == audioConfigurationTuple.b && TextUtils.equals(this.c, audioConfigurationTuple.c)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i = ((this.f3557a * 31) + this.b) * 31;
            String str = this.c;
            return i + (str != null ? str.hashCode() : 0);
        }
    }

    protected static final class AudioTrackScore implements Comparable<AudioTrackScore> {

        /* renamed from: a  reason: collision with root package name */
        private final Parameters f3558a;
        private final int b;
        private final int c;
        private final int d;
        private final int e;
        private final int f;
        private final int g;

        public AudioTrackScore(Format format, Parameters parameters, int i) {
            this.f3558a = parameters;
            this.b = DefaultTrackSelector.a(i, false) ? 1 : 0;
            this.c = DefaultTrackSelector.a(format, parameters.c) ? 1 : 0;
            this.d = (format.y & 1) == 0 ? 0 : 1;
            this.e = format.t;
            this.f = format.u;
            this.g = format.c;
        }

        /* renamed from: a */
        public int compareTo(AudioTrackScore audioTrackScore) {
            int a2;
            int i = this.b;
            int i2 = audioTrackScore.b;
            if (i != i2) {
                return DefaultTrackSelector.c(i, i2);
            }
            int i3 = this.c;
            int i4 = audioTrackScore.c;
            if (i3 != i4) {
                return DefaultTrackSelector.c(i3, i4);
            }
            int i5 = this.d;
            int i6 = audioTrackScore.d;
            if (i5 != i6) {
                return DefaultTrackSelector.c(i5, i6);
            }
            if (this.f3558a.o) {
                return DefaultTrackSelector.c(audioTrackScore.g, this.g);
            }
            int i7 = 1;
            if (i != 1) {
                i7 = -1;
            }
            int i8 = this.e;
            int i9 = audioTrackScore.e;
            if (i8 != i9) {
                a2 = DefaultTrackSelector.c(i8, i9);
            } else {
                int i10 = this.f;
                int i11 = audioTrackScore.f;
                if (i10 != i11) {
                    a2 = DefaultTrackSelector.c(i10, i11);
                } else {
                    a2 = DefaultTrackSelector.c(this.g, audioTrackScore.g);
                }
            }
            return i7 * a2;
        }
    }

    public static final class Parameters implements Parcelable {
        public static final Parcelable.Creator<Parameters> CREATOR = new Parcelable.Creator<Parameters>() {
            public Parameters createFromParcel(Parcel parcel) {
                return new Parameters(parcel);
            }

            public Parameters[] newArray(int i) {
                return new Parameters[i];
            }
        };
        public static final Parameters u = new Parameters();
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final SparseArray<Map<TrackGroupArray, SelectionOverride>> f3559a;
        /* access modifiers changed from: private */
        public final SparseBooleanArray b;
        public final String c;
        public final String d;
        public final boolean e;
        public final int f;
        public final int g;
        public final int h;
        public final int i;
        public final int j;
        public final boolean k;
        public final int l;
        public final int m;
        public final boolean n;
        public final boolean o;
        public final boolean p;
        public final boolean q;
        public final boolean r;
        public final boolean s;
        public final int t;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private Parameters() {
            /*
                r21 = this;
                r0 = r21
                android.util.SparseArray r2 = new android.util.SparseArray
                r1 = r2
                r2.<init>()
                android.util.SparseBooleanArray r3 = new android.util.SparseBooleanArray
                r2 = r3
                r3.<init>()
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r10 = 1
                r11 = 2147483647(0x7fffffff, float:NaN)
                r12 = 2147483647(0x7fffffff, float:NaN)
                r13 = 2147483647(0x7fffffff, float:NaN)
                r14 = 2147483647(0x7fffffff, float:NaN)
                r15 = 1
                r16 = 1
                r17 = 2147483647(0x7fffffff, float:NaN)
                r18 = 2147483647(0x7fffffff, float:NaN)
                r19 = 1
                r20 = 0
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.trackselection.DefaultTrackSelector.Parameters.<init>():void");
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Parameters.class != obj.getClass()) {
                return false;
            }
            Parameters parameters = (Parameters) obj;
            if (this.e == parameters.e && this.f == parameters.f && this.o == parameters.o && this.p == parameters.p && this.q == parameters.q && this.r == parameters.r && this.g == parameters.g && this.h == parameters.h && this.i == parameters.i && this.k == parameters.k && this.s == parameters.s && this.n == parameters.n && this.l == parameters.l && this.m == parameters.m && this.j == parameters.j && this.t == parameters.t && TextUtils.equals(this.c, parameters.c) && TextUtils.equals(this.d, parameters.d) && a(this.b, parameters.b) && a(this.f3559a, parameters.f3559a)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i2;
            int i3 = (((((((((((((((((((((((((((((((this.e ? 1 : 0) * true) + this.f) * 31) + (this.o ? 1 : 0)) * 31) + (this.p ? 1 : 0)) * 31) + (this.q ? 1 : 0)) * 31) + (this.r ? 1 : 0)) * 31) + this.g) * 31) + this.h) * 31) + this.i) * 31) + (this.k ? 1 : 0)) * 31) + (this.s ? 1 : 0)) * 31) + (this.n ? 1 : 0)) * 31) + this.l) * 31) + this.m) * 31) + this.j) * 31) + this.t) * 31;
            String str = this.c;
            int i4 = 0;
            if (str == null) {
                i2 = 0;
            } else {
                i2 = str.hashCode();
            }
            int i5 = (i3 + i2) * 31;
            String str2 = this.d;
            if (str2 != null) {
                i4 = str2.hashCode();
            }
            return i5 + i4;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            a(parcel, this.f3559a);
            parcel.writeSparseBooleanArray(this.b);
            parcel.writeString(this.c);
            parcel.writeString(this.d);
            Util.a(parcel, this.e);
            parcel.writeInt(this.f);
            Util.a(parcel, this.o);
            Util.a(parcel, this.p);
            Util.a(parcel, this.q);
            Util.a(parcel, this.r);
            parcel.writeInt(this.g);
            parcel.writeInt(this.h);
            parcel.writeInt(this.i);
            parcel.writeInt(this.j);
            Util.a(parcel, this.k);
            Util.a(parcel, this.s);
            parcel.writeInt(this.l);
            parcel.writeInt(this.m);
            Util.a(parcel, this.n);
            parcel.writeInt(this.t);
        }

        Parameters(SparseArray<Map<TrackGroupArray, SelectionOverride>> sparseArray, SparseBooleanArray sparseBooleanArray, String str, String str2, boolean z, int i2, boolean z2, boolean z3, boolean z4, boolean z5, int i3, int i4, int i5, int i6, boolean z6, boolean z7, int i7, int i8, boolean z8, int i9) {
            this.f3559a = sparseArray;
            this.b = sparseBooleanArray;
            this.c = Util.g(str);
            this.d = Util.g(str2);
            this.e = z;
            this.f = i2;
            this.o = z2;
            this.p = z3;
            this.q = z4;
            this.r = z5;
            this.g = i3;
            this.h = i4;
            this.i = i5;
            this.j = i6;
            this.k = z6;
            this.s = z7;
            this.l = i7;
            this.m = i8;
            this.n = z8;
            this.t = i9;
        }

        public final boolean a(int i2) {
            return this.b.get(i2);
        }

        public final boolean b(int i2, TrackGroupArray trackGroupArray) {
            Map map = this.f3559a.get(i2);
            return map != null && map.containsKey(trackGroupArray);
        }

        public final SelectionOverride a(int i2, TrackGroupArray trackGroupArray) {
            Map map = this.f3559a.get(i2);
            if (map != null) {
                return (SelectionOverride) map.get(trackGroupArray);
            }
            return null;
        }

        private static SparseArray<Map<TrackGroupArray, SelectionOverride>> a(Parcel parcel) {
            int readInt = parcel.readInt();
            SparseArray<Map<TrackGroupArray, SelectionOverride>> sparseArray = new SparseArray<>(readInt);
            for (int i2 = 0; i2 < readInt; i2++) {
                int readInt2 = parcel.readInt();
                int readInt3 = parcel.readInt();
                HashMap hashMap = new HashMap(readInt3);
                for (int i3 = 0; i3 < readInt3; i3++) {
                    hashMap.put((TrackGroupArray) parcel.readParcelable(TrackGroupArray.class.getClassLoader()), (SelectionOverride) parcel.readParcelable(SelectionOverride.class.getClassLoader()));
                }
                sparseArray.put(readInt2, hashMap);
            }
            return sparseArray;
        }

        private static void a(Parcel parcel, SparseArray<Map<TrackGroupArray, SelectionOverride>> sparseArray) {
            int size = sparseArray.size();
            parcel.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                int keyAt = sparseArray.keyAt(i2);
                Map valueAt = sparseArray.valueAt(i2);
                int size2 = valueAt.size();
                parcel.writeInt(keyAt);
                parcel.writeInt(size2);
                for (Map.Entry entry : valueAt.entrySet()) {
                    parcel.writeParcelable((Parcelable) entry.getKey(), 0);
                    parcel.writeParcelable((Parcelable) entry.getValue(), 0);
                }
            }
        }

        Parameters(Parcel parcel) {
            this.f3559a = a(parcel);
            this.b = parcel.readSparseBooleanArray();
            this.c = parcel.readString();
            this.d = parcel.readString();
            this.e = Util.a(parcel);
            this.f = parcel.readInt();
            this.o = Util.a(parcel);
            this.p = Util.a(parcel);
            this.q = Util.a(parcel);
            this.r = Util.a(parcel);
            this.g = parcel.readInt();
            this.h = parcel.readInt();
            this.i = parcel.readInt();
            this.j = parcel.readInt();
            this.k = Util.a(parcel);
            this.s = Util.a(parcel);
            this.l = parcel.readInt();
            this.m = parcel.readInt();
            this.n = Util.a(parcel);
            this.t = parcel.readInt();
        }

        private static boolean a(SparseBooleanArray sparseBooleanArray, SparseBooleanArray sparseBooleanArray2) {
            int size = sparseBooleanArray.size();
            if (sparseBooleanArray2.size() != size) {
                return false;
            }
            for (int i2 = 0; i2 < size; i2++) {
                if (sparseBooleanArray2.indexOfKey(sparseBooleanArray.keyAt(i2)) < 0) {
                    return false;
                }
            }
            return true;
        }

        private static boolean a(SparseArray<Map<TrackGroupArray, SelectionOverride>> sparseArray, SparseArray<Map<TrackGroupArray, SelectionOverride>> sparseArray2) {
            int size = sparseArray.size();
            if (sparseArray2.size() != size) {
                return false;
            }
            for (int i2 = 0; i2 < size; i2++) {
                int indexOfKey = sparseArray2.indexOfKey(sparseArray.keyAt(i2));
                if (indexOfKey < 0 || !a(sparseArray.valueAt(i2), sparseArray2.valueAt(indexOfKey))) {
                    return false;
                }
            }
            return true;
        }

        /* JADX WARNING: Removed duplicated region for block: B:6:0x001a  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static boolean a(java.util.Map<com.google.android.exoplayer2.source.TrackGroupArray, com.google.android.exoplayer2.trackselection.DefaultTrackSelector.SelectionOverride> r4, java.util.Map<com.google.android.exoplayer2.source.TrackGroupArray, com.google.android.exoplayer2.trackselection.DefaultTrackSelector.SelectionOverride> r5) {
            /*
                int r0 = r4.size()
                int r1 = r5.size()
                r2 = 0
                if (r1 == r0) goto L_0x000c
                return r2
            L_0x000c:
                java.util.Set r4 = r4.entrySet()
                java.util.Iterator r4 = r4.iterator()
            L_0x0014:
                boolean r0 = r4.hasNext()
                if (r0 == 0) goto L_0x003b
                java.lang.Object r0 = r4.next()
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                java.lang.Object r1 = r0.getKey()
                com.google.android.exoplayer2.source.TrackGroupArray r1 = (com.google.android.exoplayer2.source.TrackGroupArray) r1
                boolean r3 = r5.containsKey(r1)
                if (r3 == 0) goto L_0x003a
                java.lang.Object r0 = r0.getValue()
                java.lang.Object r1 = r5.get(r1)
                boolean r0 = com.google.android.exoplayer2.util.Util.a((java.lang.Object) r0, (java.lang.Object) r1)
                if (r0 != 0) goto L_0x0014
            L_0x003a:
                return r2
            L_0x003b:
                r4 = 1
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.trackselection.DefaultTrackSelector.Parameters.a(java.util.Map, java.util.Map):boolean");
        }
    }

    public static final class ParametersBuilder {

        /* renamed from: a  reason: collision with root package name */
        private final SparseArray<Map<TrackGroupArray, SelectionOverride>> f3560a;
        private final SparseBooleanArray b;
        private String c;
        private String d;
        private boolean e;
        private int f;
        private boolean g;
        private boolean h;
        private boolean i;
        private boolean j;
        private int k;
        private int l;
        private int m;
        private int n;
        private boolean o;
        private boolean p;
        private int q;
        private int r;
        private boolean s;
        private int t;

        public ParametersBuilder() {
            this(Parameters.u);
        }

        public Parameters a() {
            return new Parameters(this.f3560a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t);
        }

        private ParametersBuilder(Parameters parameters) {
            this.f3560a = a(parameters.f3559a);
            this.b = parameters.b.clone();
            this.c = parameters.c;
            this.d = parameters.d;
            this.e = parameters.e;
            this.f = parameters.f;
            this.g = parameters.o;
            this.h = parameters.p;
            this.i = parameters.q;
            this.j = parameters.r;
            this.k = parameters.g;
            this.l = parameters.h;
            this.m = parameters.i;
            this.n = parameters.j;
            this.o = parameters.k;
            this.p = parameters.s;
            this.q = parameters.l;
            this.r = parameters.m;
            this.s = parameters.n;
            this.t = parameters.t;
        }

        private static SparseArray<Map<TrackGroupArray, SelectionOverride>> a(SparseArray<Map<TrackGroupArray, SelectionOverride>> sparseArray) {
            SparseArray<Map<TrackGroupArray, SelectionOverride>> sparseArray2 = new SparseArray<>();
            for (int i2 = 0; i2 < sparseArray.size(); i2++) {
                sparseArray2.put(sparseArray.keyAt(i2), new HashMap(sparseArray.valueAt(i2)));
            }
            return sparseArray2;
        }
    }

    public DefaultTrackSelector() {
        this(new AdaptiveTrackSelection.Factory());
    }

    protected static boolean a(int i, boolean z) {
        int i2 = i & 7;
        return i2 == 4 || (z && i2 == 3);
    }

    private static int b(int i, int i2) {
        if (i == -1) {
            return i2 == -1 ? 0 : -1;
        }
        if (i2 == -1) {
            return 1;
        }
        return i - i2;
    }

    /* access modifiers changed from: private */
    public static int c(int i, int i2) {
        if (i > i2) {
            return 1;
        }
        return i2 > i ? -1 : 0;
    }

    /* access modifiers changed from: protected */
    public TrackSelection b(TrackGroupArray trackGroupArray, int[][] iArr, int i, Parameters parameters, TrackSelection.Factory factory) throws ExoPlaybackException {
        TrackSelection a2 = (parameters.p || parameters.o || factory == null) ? null : a(trackGroupArray, iArr, i, parameters, factory, a());
        return a2 == null ? b(trackGroupArray, iArr, parameters) : a2;
    }

    public Parameters d() {
        return this.e.get();
    }

    public DefaultTrackSelector(TrackSelection.Factory factory) {
        this.d = factory;
        this.e = new AtomicReference<>(Parameters.u);
    }

    public void a(Parameters parameters) {
        Assertions.a(parameters);
        if (!this.e.getAndSet(parameters).equals(parameters)) {
            b();
        }
    }

    public static final class SelectionOverride implements Parcelable {
        public static final Parcelable.Creator<SelectionOverride> CREATOR = new Parcelable.Creator<SelectionOverride>() {
            public SelectionOverride createFromParcel(Parcel parcel) {
                return new SelectionOverride(parcel);
            }

            public SelectionOverride[] newArray(int i) {
                return new SelectionOverride[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        public final int f3561a;
        public final int[] b;
        public final int c;

        public SelectionOverride(int i, int... iArr) {
            this.f3561a = i;
            this.b = Arrays.copyOf(iArr, iArr.length);
            this.c = iArr.length;
            Arrays.sort(this.b);
        }

        public boolean a(int i) {
            for (int i2 : this.b) {
                if (i2 == i) {
                    return true;
                }
            }
            return false;
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || SelectionOverride.class != obj.getClass()) {
                return false;
            }
            SelectionOverride selectionOverride = (SelectionOverride) obj;
            if (this.f3561a != selectionOverride.f3561a || !Arrays.equals(this.b, selectionOverride.b)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (this.f3561a * 31) + Arrays.hashCode(this.b);
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f3561a);
            parcel.writeInt(this.b.length);
            parcel.writeIntArray(this.b);
        }

        SelectionOverride(Parcel parcel) {
            this.f3561a = parcel.readInt();
            this.c = parcel.readByte();
            this.b = new int[this.c];
            parcel.readIntArray(this.b);
        }
    }

    private static int b(TrackGroup trackGroup, int[] iArr, int i, String str, int i2, int i3, int i4, int i5, List<Integer> list) {
        int i6 = 0;
        for (int i7 = 0; i7 < list.size(); i7++) {
            int intValue = list.get(i7).intValue();
            TrackGroup trackGroup2 = trackGroup;
            if (a(trackGroup.a(intValue), str, iArr[intValue], i, i2, i3, i4, i5)) {
                i6++;
            }
        }
        return i6;
    }

    /* access modifiers changed from: protected */
    public final Pair<RendererConfiguration[], TrackSelection[]> a(MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int[][][] iArr, int[] iArr2) throws ExoPlaybackException {
        Parameters parameters = this.e.get();
        int a2 = mappedTrackInfo.a();
        TrackSelection[] a3 = a(mappedTrackInfo, iArr, iArr2, parameters);
        for (int i = 0; i < a2; i++) {
            if (parameters.a(i)) {
                a3[i] = null;
            } else {
                TrackGroupArray c = mappedTrackInfo.c(i);
                if (parameters.b(i, c)) {
                    SelectionOverride a4 = parameters.a(i, c);
                    if (a4 == null) {
                        a3[i] = null;
                    } else if (a4.c == 1) {
                        a3[i] = new FixedTrackSelection(c.a(a4.f3561a), a4.b[0]);
                    } else {
                        TrackSelection.Factory factory = this.d;
                        Assertions.a(factory);
                        a3[i] = factory.a(c.a(a4.f3561a), a(), a4.b);
                    }
                }
            }
        }
        RendererConfiguration[] rendererConfigurationArr = new RendererConfiguration[a2];
        for (int i2 = 0; i2 < a2; i2++) {
            rendererConfigurationArr[i2] = !parameters.a(i2) && (mappedTrackInfo.b(i2) == 6 || a3[i2] != null) ? RendererConfiguration.b : null;
        }
        a(mappedTrackInfo, iArr, rendererConfigurationArr, a3, parameters.t);
        return Pair.create(rendererConfigurationArr, a3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0097, code lost:
        if (b(r2.c, r14) < 0) goto L_0x0099;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00c5 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.exoplayer2.trackselection.TrackSelection b(com.google.android.exoplayer2.source.TrackGroupArray r19, int[][] r20, com.google.android.exoplayer2.trackselection.DefaultTrackSelector.Parameters r21) {
        /*
            r0 = r19
            r1 = r21
            r3 = -1
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = -1
            r10 = -1
        L_0x000b:
            int r11 = r0.f3424a
            if (r5 >= r11) goto L_0x00d8
            com.google.android.exoplayer2.source.TrackGroup r11 = r0.a((int) r5)
            int r12 = r1.l
            int r13 = r1.m
            boolean r14 = r1.n
            java.util.List r12 = a((com.google.android.exoplayer2.source.TrackGroup) r11, (int) r12, (int) r13, (boolean) r14)
            r13 = r20[r5]
            r14 = r10
            r10 = r9
            r9 = r8
            r8 = r7
            r7 = r6
            r6 = 0
        L_0x0025:
            int r15 = r11.f3423a
            if (r6 >= r15) goto L_0x00cc
            r15 = r13[r6]
            boolean r2 = r1.s
            boolean r2 = a((int) r15, (boolean) r2)
            if (r2 == 0) goto L_0x00c5
            com.google.android.exoplayer2.Format r2 = r11.a((int) r6)
            java.lang.Integer r15 = java.lang.Integer.valueOf(r6)
            boolean r15 = r12.contains(r15)
            r17 = 1
            if (r15 == 0) goto L_0x006c
            int r15 = r2.l
            if (r15 == r3) goto L_0x004b
            int r4 = r1.g
            if (r15 > r4) goto L_0x006c
        L_0x004b:
            int r4 = r2.m
            if (r4 == r3) goto L_0x0053
            int r15 = r1.h
            if (r4 > r15) goto L_0x006c
        L_0x0053:
            float r4 = r2.n
            r15 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r15 = (r4 > r15 ? 1 : (r4 == r15 ? 0 : -1))
            if (r15 == 0) goto L_0x0062
            int r15 = r1.i
            float r15 = (float) r15
            int r4 = (r4 > r15 ? 1 : (r4 == r15 ? 0 : -1))
            if (r4 > 0) goto L_0x006c
        L_0x0062:
            int r4 = r2.c
            if (r4 == r3) goto L_0x006a
            int r15 = r1.j
            if (r4 > r15) goto L_0x006c
        L_0x006a:
            r4 = 1
            goto L_0x006d
        L_0x006c:
            r4 = 0
        L_0x006d:
            if (r4 != 0) goto L_0x0074
            boolean r15 = r1.k
            if (r15 != 0) goto L_0x0074
            goto L_0x00c5
        L_0x0074:
            if (r4 == 0) goto L_0x0078
            r15 = 2
            goto L_0x0079
        L_0x0078:
            r15 = 1
        L_0x0079:
            r3 = r13[r6]
            r0 = 0
            boolean r3 = a((int) r3, (boolean) r0)
            if (r3 == 0) goto L_0x0084
            int r15 = r15 + 1000
        L_0x0084:
            if (r15 <= r9) goto L_0x0089
            r18 = 1
            goto L_0x008b
        L_0x0089:
            r18 = 0
        L_0x008b:
            if (r15 != r9) goto L_0x00ba
            boolean r0 = r1.o
            if (r0 == 0) goto L_0x009f
            int r0 = r2.c
            int r0 = b(r0, r14)
            if (r0 >= 0) goto L_0x009c
        L_0x0099:
            r18 = 1
            goto L_0x00ba
        L_0x009c:
            r18 = 0
            goto L_0x00ba
        L_0x009f:
            int r0 = r2.a()
            if (r0 == r10) goto L_0x00aa
            int r0 = b(r0, r10)
            goto L_0x00b0
        L_0x00aa:
            int r0 = r2.c
            int r0 = b(r0, r14)
        L_0x00b0:
            if (r3 == 0) goto L_0x00b7
            if (r4 == 0) goto L_0x00b7
            if (r0 <= 0) goto L_0x009c
            goto L_0x0099
        L_0x00b7:
            if (r0 >= 0) goto L_0x009c
            goto L_0x0099
        L_0x00ba:
            if (r18 == 0) goto L_0x00c5
            int r14 = r2.c
            int r10 = r2.a()
            r8 = r6
            r7 = r11
            r9 = r15
        L_0x00c5:
            int r6 = r6 + 1
            r3 = -1
            r0 = r19
            goto L_0x0025
        L_0x00cc:
            int r5 = r5 + 1
            r3 = -1
            r0 = r19
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r14
            goto L_0x000b
        L_0x00d8:
            if (r6 != 0) goto L_0x00dd
            r16 = 0
            goto L_0x00e4
        L_0x00dd:
            com.google.android.exoplayer2.trackselection.FixedTrackSelection r2 = new com.google.android.exoplayer2.trackselection.FixedTrackSelection
            r2.<init>(r6, r7)
            r16 = r2
        L_0x00e4:
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.trackselection.DefaultTrackSelector.b(com.google.android.exoplayer2.source.TrackGroupArray, int[][], com.google.android.exoplayer2.trackselection.DefaultTrackSelector$Parameters):com.google.android.exoplayer2.trackselection.TrackSelection");
    }

    /* access modifiers changed from: protected */
    public TrackSelection[] a(MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int[][][] iArr, int[] iArr2, Parameters parameters) throws ExoPlaybackException {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        AudioTrackScore audioTrackScore;
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo2 = mappedTrackInfo;
        Parameters parameters2 = parameters;
        int a2 = mappedTrackInfo.a();
        TrackSelection[] trackSelectionArr = new TrackSelection[a2];
        int i6 = 0;
        boolean z = false;
        int i7 = 0;
        int i8 = 0;
        while (true) {
            i = 2;
            i2 = 1;
            if (i7 >= a2) {
                break;
            }
            if (2 == mappedTrackInfo2.b(i7)) {
                if (!z) {
                    trackSelectionArr[i7] = b(mappedTrackInfo2.c(i7), iArr[i7], iArr2[i7], parameters, this.d);
                    z = trackSelectionArr[i7] != null;
                }
                if (mappedTrackInfo2.c(i7).f3424a <= 0) {
                    i2 = 0;
                }
                i8 |= i2;
            }
            i7++;
        }
        AudioTrackScore audioTrackScore2 = null;
        int i9 = -1;
        int i10 = -1;
        int i11 = Integer.MIN_VALUE;
        while (i6 < a2) {
            int b = mappedTrackInfo2.b(i6);
            if (b != i2) {
                if (b != i) {
                    if (b != 3) {
                        trackSelectionArr[i6] = a(b, mappedTrackInfo2.c(i6), iArr[i6], parameters2);
                    } else {
                        Pair<TrackSelection, Integer> a3 = a(mappedTrackInfo2.c(i6), iArr[i6], parameters2);
                        if (a3 != null && ((Integer) a3.second).intValue() > i11) {
                            if (i10 != -1) {
                                trackSelectionArr[i10] = null;
                            }
                            trackSelectionArr[i6] = (TrackSelection) a3.first;
                            i11 = ((Integer) a3.second).intValue();
                            i10 = i6;
                            i6++;
                            i = 2;
                            i2 = 1;
                        }
                    }
                }
                i5 = i9;
                audioTrackScore = audioTrackScore2;
                i4 = i10;
                i3 = i11;
            } else {
                TrackGroupArray c = mappedTrackInfo2.c(i6);
                i5 = i9;
                int[][] iArr3 = iArr[i6];
                audioTrackScore = audioTrackScore2;
                i4 = i10;
                i3 = i11;
                Pair<TrackSelection, AudioTrackScore> a4 = a(c, iArr3, iArr2[i6], parameters, i8 != 0 ? null : this.d);
                if (a4 != null && (audioTrackScore == null || ((AudioTrackScore) a4.second).compareTo(audioTrackScore) > 0)) {
                    if (i5 != -1) {
                        trackSelectionArr[i5] = null;
                    }
                    trackSelectionArr[i6] = (TrackSelection) a4.first;
                    audioTrackScore2 = (AudioTrackScore) a4.second;
                    i9 = i6;
                    i10 = i4;
                    i11 = i3;
                    i6++;
                    i = 2;
                    i2 = 1;
                }
            }
            audioTrackScore2 = audioTrackScore;
            i9 = i5;
            i10 = i4;
            i11 = i3;
            i6++;
            i = 2;
            i2 = 1;
        }
        return trackSelectionArr;
    }

    private static TrackSelection a(TrackGroupArray trackGroupArray, int[][] iArr, int i, Parameters parameters, TrackSelection.Factory factory, BandwidthMeter bandwidthMeter) throws ExoPlaybackException {
        TrackGroupArray trackGroupArray2 = trackGroupArray;
        Parameters parameters2 = parameters;
        int i2 = parameters2.r ? 24 : 16;
        boolean z = parameters2.q && (i & i2) != 0;
        int i3 = 0;
        while (i3 < trackGroupArray2.f3424a) {
            TrackGroup a2 = trackGroupArray2.a(i3);
            TrackGroup trackGroup = a2;
            int[] a3 = a(a2, iArr[i3], z, i2, parameters2.g, parameters2.h, parameters2.i, parameters2.j, parameters2.l, parameters2.m, parameters2.n);
            if (a3.length > 0) {
                Assertions.a(factory);
                return factory.a(trackGroup, bandwidthMeter, a3);
            }
            BandwidthMeter bandwidthMeter2 = bandwidthMeter;
            i3++;
            trackGroupArray2 = trackGroupArray;
        }
        return null;
    }

    private static int[] a(TrackGroup trackGroup, int[] iArr, boolean z, int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z2) {
        String str;
        TrackGroup trackGroup2 = trackGroup;
        if (trackGroup2.f3423a < 2) {
            return f;
        }
        List<Integer> a2 = a(trackGroup2, i6, i7, z2);
        if (a2.size() < 2) {
            return f;
        }
        if (!z) {
            HashSet hashSet = new HashSet();
            String str2 = null;
            int i8 = 0;
            for (int i9 = 0; i9 < a2.size(); i9++) {
                String str3 = trackGroup2.a(a2.get(i9).intValue()).g;
                if (hashSet.add(str3)) {
                    String str4 = str3;
                    int b = b(trackGroup, iArr, i, str3, i2, i3, i4, i5, a2);
                    if (b > i8) {
                        i8 = b;
                        str2 = str4;
                    }
                }
            }
            str = str2;
        } else {
            str = null;
        }
        a(trackGroup, iArr, i, str, i2, i3, i4, i5, a2);
        return a2.size() < 2 ? f : Util.a(a2);
    }

    private static void a(TrackGroup trackGroup, int[] iArr, int i, String str, int i2, int i3, int i4, int i5, List<Integer> list) {
        List<Integer> list2 = list;
        for (int size = list.size() - 1; size >= 0; size--) {
            int intValue = list2.get(size).intValue();
            TrackGroup trackGroup2 = trackGroup;
            if (!a(trackGroup.a(intValue), str, iArr[intValue], i, i2, i3, i4, i5)) {
                list2.remove(size);
            }
        }
    }

    private static boolean a(Format format, String str, int i, int i2, int i3, int i4, int i5, int i6) {
        if (!a(i, false) || (i & i2) == 0) {
            return false;
        }
        if (str != null && !Util.a((Object) format.g, (Object) str)) {
            return false;
        }
        int i7 = format.l;
        if (i7 != -1 && i7 > i3) {
            return false;
        }
        int i8 = format.m;
        if (i8 != -1 && i8 > i4) {
            return false;
        }
        float f2 = format.n;
        if (f2 != -1.0f && f2 > ((float) i5)) {
            return false;
        }
        int i9 = format.c;
        if (i9 == -1 || i9 <= i6) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Pair<TrackSelection, AudioTrackScore> a(TrackGroupArray trackGroupArray, int[][] iArr, int i, Parameters parameters, TrackSelection.Factory factory) throws ExoPlaybackException {
        TrackGroupArray trackGroupArray2 = trackGroupArray;
        Parameters parameters2 = parameters;
        TrackSelection.Factory factory2 = factory;
        TrackSelection trackSelection = null;
        AudioTrackScore audioTrackScore = null;
        int i2 = 0;
        int i3 = -1;
        int i4 = -1;
        while (i2 < trackGroupArray2.f3424a) {
            TrackGroup a2 = trackGroupArray2.a(i2);
            int[] iArr2 = iArr[i2];
            int i5 = i4;
            AudioTrackScore audioTrackScore2 = audioTrackScore;
            int i6 = i3;
            for (int i7 = 0; i7 < a2.f3423a; i7++) {
                if (a(iArr2[i7], parameters2.s)) {
                    AudioTrackScore audioTrackScore3 = new AudioTrackScore(a2.a(i7), parameters2, iArr2[i7]);
                    if (audioTrackScore2 == null || audioTrackScore3.compareTo(audioTrackScore2) > 0) {
                        i6 = i2;
                        i5 = i7;
                        audioTrackScore2 = audioTrackScore3;
                    }
                }
            }
            i2++;
            i3 = i6;
            audioTrackScore = audioTrackScore2;
            i4 = i5;
        }
        if (i3 == -1) {
            return null;
        }
        TrackGroup a3 = trackGroupArray2.a(i3);
        if (!parameters2.p && !parameters2.o && factory2 != null) {
            int[] a4 = a(a3, iArr[i3], parameters2.q);
            if (a4.length > 0) {
                trackSelection = factory2.a(a3, a(), a4);
            }
        }
        if (trackSelection == null) {
            trackSelection = new FixedTrackSelection(a3, i4);
        }
        Assertions.a(audioTrackScore);
        return Pair.create(trackSelection, audioTrackScore);
    }

    private static int[] a(TrackGroup trackGroup, int[] iArr, boolean z) {
        int a2;
        HashSet hashSet = new HashSet();
        AudioConfigurationTuple audioConfigurationTuple = null;
        int i = 0;
        for (int i2 = 0; i2 < trackGroup.f3423a; i2++) {
            Format a3 = trackGroup.a(i2);
            AudioConfigurationTuple audioConfigurationTuple2 = new AudioConfigurationTuple(a3.t, a3.u, z ? null : a3.g);
            if (hashSet.add(audioConfigurationTuple2) && (a2 = a(trackGroup, iArr, audioConfigurationTuple2)) > i) {
                i = a2;
                audioConfigurationTuple = audioConfigurationTuple2;
            }
        }
        if (i <= 1) {
            return f;
        }
        int[] iArr2 = new int[i];
        int i3 = 0;
        for (int i4 = 0; i4 < trackGroup.f3423a; i4++) {
            Format a4 = trackGroup.a(i4);
            int i5 = iArr[i4];
            Assertions.a(audioConfigurationTuple);
            if (a(a4, i5, audioConfigurationTuple)) {
                iArr2[i3] = i4;
                i3++;
            }
        }
        return iArr2;
    }

    private static int a(TrackGroup trackGroup, int[] iArr, AudioConfigurationTuple audioConfigurationTuple) {
        int i = 0;
        for (int i2 = 0; i2 < trackGroup.f3423a; i2++) {
            if (a(trackGroup.a(i2), iArr[i2], audioConfigurationTuple)) {
                i++;
            }
        }
        return i;
    }

    private static boolean a(Format format, int i, AudioConfigurationTuple audioConfigurationTuple) {
        if (!a(i, false) || format.t != audioConfigurationTuple.f3557a || format.u != audioConfigurationTuple.b) {
            return false;
        }
        String str = audioConfigurationTuple.c;
        if (str == null || TextUtils.equals(str, format.g)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Pair<TrackSelection, Integer> a(TrackGroupArray trackGroupArray, int[][] iArr, Parameters parameters) throws ExoPlaybackException {
        TrackGroupArray trackGroupArray2 = trackGroupArray;
        Parameters parameters2 = parameters;
        int i = 0;
        TrackGroup trackGroup = null;
        int i2 = 0;
        int i3 = 0;
        while (i < trackGroupArray2.f3424a) {
            TrackGroup a2 = trackGroupArray2.a(i);
            int[] iArr2 = iArr[i];
            int i4 = i3;
            int i5 = i2;
            TrackGroup trackGroup2 = trackGroup;
            for (int i6 = 0; i6 < a2.f3423a; i6++) {
                if (a(iArr2[i6], parameters2.s)) {
                    Format a3 = a2.a(i6);
                    int i7 = a3.y & (~parameters2.f);
                    int i8 = 1;
                    boolean z = (i7 & 1) != 0;
                    boolean z2 = (i7 & 2) != 0;
                    boolean a4 = a(a3, parameters2.d);
                    if (a4 || (parameters2.e && a(a3))) {
                        i8 = (z ? 8 : !z2 ? 6 : 4) + (a4 ? 1 : 0);
                    } else if (z) {
                        i8 = 3;
                    } else if (z2) {
                        if (a(a3, parameters2.c)) {
                            i8 = 2;
                        }
                    }
                    if (a(iArr2[i6], false)) {
                        i8 += 1000;
                    }
                    if (i8 > i4) {
                        i5 = i6;
                        trackGroup2 = a2;
                        i4 = i8;
                    }
                }
            }
            i++;
            trackGroup = trackGroup2;
            i2 = i5;
            i3 = i4;
        }
        if (trackGroup == null) {
            return null;
        }
        return Pair.create(new FixedTrackSelection(trackGroup, i2), Integer.valueOf(i3));
    }

    /* access modifiers changed from: protected */
    public TrackSelection a(int i, TrackGroupArray trackGroupArray, int[][] iArr, Parameters parameters) throws ExoPlaybackException {
        TrackGroup trackGroup = null;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < trackGroupArray.f3424a) {
            TrackGroup a2 = trackGroupArray.a(i2);
            int[] iArr2 = iArr[i2];
            int i5 = i4;
            int i6 = i3;
            TrackGroup trackGroup2 = trackGroup;
            for (int i7 = 0; i7 < a2.f3423a; i7++) {
                if (a(iArr2[i7], parameters.s)) {
                    int i8 = 1;
                    if ((a2.a(i7).y & 1) != 0) {
                        i8 = 2;
                    }
                    if (a(iArr2[i7], false)) {
                        i8 += 1000;
                    }
                    if (i8 > i5) {
                        i6 = i7;
                        trackGroup2 = a2;
                        i5 = i8;
                    }
                }
            }
            i2++;
            trackGroup = trackGroup2;
            i3 = i6;
            i4 = i5;
        }
        if (trackGroup == null) {
            return null;
        }
        return new FixedTrackSelection(trackGroup, i3);
    }

    private static void a(MappingTrackSelector.MappedTrackInfo mappedTrackInfo, int[][][] iArr, RendererConfiguration[] rendererConfigurationArr, TrackSelection[] trackSelectionArr, int i) {
        boolean z;
        if (i != 0) {
            boolean z2 = false;
            int i2 = 0;
            int i3 = -1;
            int i4 = -1;
            while (true) {
                if (i2 >= mappedTrackInfo.a()) {
                    z = true;
                    break;
                }
                int b = mappedTrackInfo.b(i2);
                TrackSelection trackSelection = trackSelectionArr[i2];
                if ((b == 1 || b == 2) && trackSelection != null && a(iArr[i2], mappedTrackInfo.c(i2), trackSelection)) {
                    if (b == 1) {
                        if (i4 != -1) {
                            break;
                        }
                        i4 = i2;
                    } else if (i3 != -1) {
                        break;
                    } else {
                        i3 = i2;
                    }
                }
                i2++;
            }
            z = false;
            if (!(i4 == -1 || i3 == -1)) {
                z2 = true;
            }
            if (z && z2) {
                RendererConfiguration rendererConfiguration = new RendererConfiguration(i);
                rendererConfigurationArr[i4] = rendererConfiguration;
                rendererConfigurationArr[i3] = rendererConfiguration;
            }
        }
    }

    private static boolean a(int[][] iArr, TrackGroupArray trackGroupArray, TrackSelection trackSelection) {
        if (trackSelection == null) {
            return false;
        }
        int a2 = trackGroupArray.a(trackSelection.c());
        for (int i = 0; i < trackSelection.length(); i++) {
            if ((iArr[a2][trackSelection.b(i)] & 32) != 32) {
                return false;
            }
        }
        return true;
    }

    protected static boolean a(Format format) {
        return TextUtils.isEmpty(format.z) || a(format, "und");
    }

    protected static boolean a(Format format, String str) {
        return str != null && TextUtils.equals(str, Util.g(format.z));
    }

    private static List<Integer> a(TrackGroup trackGroup, int i, int i2, boolean z) {
        int i3;
        ArrayList arrayList = new ArrayList(trackGroup.f3423a);
        for (int i4 = 0; i4 < trackGroup.f3423a; i4++) {
            arrayList.add(Integer.valueOf(i4));
        }
        if (!(i == Integer.MAX_VALUE || i2 == Integer.MAX_VALUE)) {
            int i5 = Integer.MAX_VALUE;
            for (int i6 = 0; i6 < trackGroup.f3423a; i6++) {
                Format a2 = trackGroup.a(i6);
                int i7 = a2.l;
                if (i7 > 0 && (i3 = a2.m) > 0) {
                    Point a3 = a(z, i, i2, i7, i3);
                    int i8 = a2.l;
                    int i9 = a2.m;
                    int i10 = i8 * i9;
                    if (i8 >= ((int) (((float) a3.x) * 0.98f)) && i9 >= ((int) (((float) a3.y) * 0.98f)) && i10 < i5) {
                        i5 = i10;
                    }
                }
            }
            if (i5 != Integer.MAX_VALUE) {
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    int a4 = trackGroup.a(((Integer) arrayList.get(size)).intValue()).a();
                    if (a4 == -1 || a4 > i5) {
                        arrayList.remove(size);
                    }
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000d, code lost:
        if (r1 != r3) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.Point a(boolean r3, int r4, int r5, int r6, int r7) {
        /*
            if (r3 == 0) goto L_0x0010
            r3 = 1
            r0 = 0
            if (r6 <= r7) goto L_0x0008
            r1 = 1
            goto L_0x0009
        L_0x0008:
            r1 = 0
        L_0x0009:
            if (r4 <= r5) goto L_0x000c
            goto L_0x000d
        L_0x000c:
            r3 = 0
        L_0x000d:
            if (r1 == r3) goto L_0x0010
            goto L_0x0013
        L_0x0010:
            r2 = r5
            r5 = r4
            r4 = r2
        L_0x0013:
            int r3 = r6 * r4
            int r0 = r7 * r5
            if (r3 < r0) goto L_0x0023
            android.graphics.Point r3 = new android.graphics.Point
            int r4 = com.google.android.exoplayer2.util.Util.a((int) r0, (int) r6)
            r3.<init>(r5, r4)
            return r3
        L_0x0023:
            android.graphics.Point r5 = new android.graphics.Point
            int r3 = com.google.android.exoplayer2.util.Util.a((int) r3, (int) r7)
            r5.<init>(r3, r4)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.trackselection.DefaultTrackSelector.a(boolean, int, int, int, int):android.graphics.Point");
    }
}
