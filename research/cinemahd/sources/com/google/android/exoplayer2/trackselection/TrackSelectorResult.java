package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.RendererConfiguration;
import com.google.android.exoplayer2.util.Util;

public final class TrackSelectorResult {

    /* renamed from: a  reason: collision with root package name */
    public final int f3566a;
    public final RendererConfiguration[] b;
    public final TrackSelectionArray c;
    public final Object d;

    public TrackSelectorResult(RendererConfiguration[] rendererConfigurationArr, TrackSelection[] trackSelectionArr, Object obj) {
        this.b = rendererConfigurationArr;
        this.c = new TrackSelectionArray(trackSelectionArr);
        this.d = obj;
        this.f3566a = rendererConfigurationArr.length;
    }

    public boolean a(int i) {
        return this.b[i] != null;
    }

    public boolean a(TrackSelectorResult trackSelectorResult) {
        if (trackSelectorResult == null || trackSelectorResult.c.f3564a != this.c.f3564a) {
            return false;
        }
        for (int i = 0; i < this.c.f3564a; i++) {
            if (!a(trackSelectorResult, i)) {
                return false;
            }
        }
        return true;
    }

    public boolean a(TrackSelectorResult trackSelectorResult, int i) {
        if (trackSelectorResult != null && Util.a((Object) this.b[i], (Object) trackSelectorResult.b[i]) && Util.a((Object) this.c.a(i), (Object) trackSelectorResult.c.a(i))) {
            return true;
        }
        return false;
    }
}
