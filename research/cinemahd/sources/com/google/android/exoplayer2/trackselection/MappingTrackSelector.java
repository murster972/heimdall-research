package com.google.android.exoplayer2.trackselection;

import android.util.Pair;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.RendererCapabilities;
import com.google.android.exoplayer2.RendererConfiguration;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;

public abstract class MappingTrackSelector extends TrackSelector {
    private MappedTrackInfo c;

    public static final class MappedTrackInfo {

        /* renamed from: a  reason: collision with root package name */
        private final int f3562a;
        private final int[] b;
        private final TrackGroupArray[] c;
        private final int[] d;
        private final int[][][] e;
        private final TrackGroupArray f;

        MappedTrackInfo(int[] iArr, TrackGroupArray[] trackGroupArrayArr, int[] iArr2, int[][][] iArr3, TrackGroupArray trackGroupArray) {
            this.b = iArr;
            this.c = trackGroupArrayArr;
            this.e = iArr3;
            this.d = iArr2;
            this.f = trackGroupArray;
            this.f3562a = iArr.length;
        }

        public int a() {
            return this.f3562a;
        }

        public int b(int i) {
            return this.b[i];
        }

        public TrackGroupArray c(int i) {
            return this.c[i];
        }

        public int d(int i) {
            int i2 = 0;
            for (int i3 = 0; i3 < this.f3562a; i3++) {
                if (this.b[i3] == i) {
                    i2 = Math.max(i2, a(i3));
                }
            }
            return i2;
        }

        public int a(int i) {
            int i2;
            int[][] iArr = this.e[i];
            int i3 = 0;
            int i4 = 0;
            while (i3 < iArr.length) {
                int i5 = i4;
                for (int i6 : iArr[i3]) {
                    int i7 = i6 & 7;
                    if (i7 == 3) {
                        i2 = 2;
                    } else if (i7 == 4) {
                        return 3;
                    } else {
                        i2 = 1;
                    }
                    i5 = Math.max(i5, i2);
                }
                i3++;
                i4 = i5;
            }
            return i4;
        }

        public TrackGroupArray b() {
            return this.f;
        }

        public int a(int i, int i2, int i3) {
            return this.e[i][i2][i3] & 7;
        }

        public int a(int i, int i2, boolean z) {
            int i3 = this.c[i].a(i2).f3423a;
            int[] iArr = new int[i3];
            int i4 = 0;
            for (int i5 = 0; i5 < i3; i5++) {
                int a2 = a(i, i2, i5);
                if (a2 == 4 || (z && a2 == 3)) {
                    iArr[i4] = i5;
                    i4++;
                }
            }
            return a(i, i2, Arrays.copyOf(iArr, i4));
        }

        public int a(int i, int i2, int[] iArr) {
            int i3 = 0;
            String str = null;
            boolean z = false;
            int i4 = 0;
            int i5 = 16;
            while (i3 < iArr.length) {
                String str2 = this.c[i].a(i2).a(iArr[i3]).g;
                int i6 = i4 + 1;
                if (i4 == 0) {
                    str = str2;
                } else {
                    z |= !Util.a((Object) str, (Object) str2);
                }
                i5 = Math.min(i5, this.e[i][i2][i3] & 24);
                i3++;
                i4 = i6;
            }
            return z ? Math.min(i5, this.d[i]) : i5;
        }
    }

    /* access modifiers changed from: protected */
    public abstract Pair<RendererConfiguration[], TrackSelection[]> a(MappedTrackInfo mappedTrackInfo, int[][][] iArr, int[] iArr2) throws ExoPlaybackException;

    public final void a(Object obj) {
        this.c = (MappedTrackInfo) obj;
    }

    public final MappedTrackInfo c() {
        return this.c;
    }

    public final TrackSelectorResult a(RendererCapabilities[] rendererCapabilitiesArr, TrackGroupArray trackGroupArray) throws ExoPlaybackException {
        int[] iArr;
        int[] iArr2 = new int[(rendererCapabilitiesArr.length + 1)];
        TrackGroup[][] trackGroupArr = new TrackGroup[(rendererCapabilitiesArr.length + 1)][];
        int[][][] iArr3 = new int[(rendererCapabilitiesArr.length + 1)][][];
        for (int i = 0; i < trackGroupArr.length; i++) {
            int i2 = trackGroupArray.f3424a;
            trackGroupArr[i] = new TrackGroup[i2];
            iArr3[i] = new int[i2][];
        }
        int[] a2 = a(rendererCapabilitiesArr);
        for (int i3 = 0; i3 < trackGroupArray.f3424a; i3++) {
            TrackGroup a3 = trackGroupArray.a(i3);
            int a4 = a(rendererCapabilitiesArr, a3);
            if (a4 == rendererCapabilitiesArr.length) {
                iArr = new int[a3.f3423a];
            } else {
                iArr = a(rendererCapabilitiesArr[a4], a3);
            }
            int i4 = iArr2[a4];
            trackGroupArr[a4][i4] = a3;
            iArr3[a4][i4] = iArr;
            iArr2[a4] = iArr2[a4] + 1;
        }
        TrackGroupArray[] trackGroupArrayArr = new TrackGroupArray[rendererCapabilitiesArr.length];
        int[] iArr4 = new int[rendererCapabilitiesArr.length];
        for (int i5 = 0; i5 < rendererCapabilitiesArr.length; i5++) {
            int i6 = iArr2[i5];
            trackGroupArrayArr[i5] = new TrackGroupArray((TrackGroup[]) Util.a((T[]) trackGroupArr[i5], i6));
            iArr3[i5] = (int[][]) Util.a((T[]) iArr3[i5], i6);
            iArr4[i5] = rendererCapabilitiesArr[i5].getTrackType();
        }
        int[] iArr5 = a2;
        int[][][] iArr6 = iArr3;
        MappedTrackInfo mappedTrackInfo = new MappedTrackInfo(iArr4, trackGroupArrayArr, iArr5, iArr6, new TrackGroupArray((TrackGroup[]) Util.a((T[]) trackGroupArr[rendererCapabilitiesArr.length], iArr2[rendererCapabilitiesArr.length])));
        Pair<RendererConfiguration[], TrackSelection[]> a5 = a(mappedTrackInfo, iArr3, a2);
        return new TrackSelectorResult((RendererConfiguration[]) a5.first, (TrackSelection[]) a5.second, mappedTrackInfo);
    }

    private static int a(RendererCapabilities[] rendererCapabilitiesArr, TrackGroup trackGroup) throws ExoPlaybackException {
        int length = rendererCapabilitiesArr.length;
        int i = 0;
        int i2 = 0;
        while (i < rendererCapabilitiesArr.length) {
            RendererCapabilities rendererCapabilities = rendererCapabilitiesArr[i];
            int i3 = i2;
            int i4 = length;
            for (int i5 = 0; i5 < trackGroup.f3423a; i5++) {
                int a2 = rendererCapabilities.a(trackGroup.a(i5)) & 7;
                if (a2 > i3) {
                    if (a2 == 4) {
                        return i;
                    }
                    i4 = i;
                    i3 = a2;
                }
            }
            i++;
            length = i4;
            i2 = i3;
        }
        return length;
    }

    private static int[] a(RendererCapabilities rendererCapabilities, TrackGroup trackGroup) throws ExoPlaybackException {
        int[] iArr = new int[trackGroup.f3423a];
        for (int i = 0; i < trackGroup.f3423a; i++) {
            iArr[i] = rendererCapabilities.a(trackGroup.a(i));
        }
        return iArr;
    }

    private static int[] a(RendererCapabilities[] rendererCapabilitiesArr) throws ExoPlaybackException {
        int[] iArr = new int[rendererCapabilitiesArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = rendererCapabilitiesArr[i].k();
        }
        return iArr;
    }
}
