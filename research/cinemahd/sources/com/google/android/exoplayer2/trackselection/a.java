package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import java.util.List;

/* compiled from: TrackSelection */
public final /* synthetic */ class a {
    @Deprecated
    public static void a(TrackSelection _this, long j, long j2, long j3) {
        throw new UnsupportedOperationException();
    }

    public static void a(TrackSelection _this, long j, long j2, long j3, List list, MediaChunkIterator[] mediaChunkIteratorArr) {
        _this.a(j, j2, j3);
        throw null;
    }
}
