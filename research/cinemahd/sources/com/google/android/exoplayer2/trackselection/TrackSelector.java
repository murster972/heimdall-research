package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.RendererCapabilities;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.Assertions;

public abstract class TrackSelector {

    /* renamed from: a  reason: collision with root package name */
    private InvalidationListener f3565a;
    private BandwidthMeter b;

    public interface InvalidationListener {
        void a();
    }

    public abstract TrackSelectorResult a(RendererCapabilities[] rendererCapabilitiesArr, TrackGroupArray trackGroupArray) throws ExoPlaybackException;

    public final void a(InvalidationListener invalidationListener, BandwidthMeter bandwidthMeter) {
        this.f3565a = invalidationListener;
        this.b = bandwidthMeter;
    }

    public abstract void a(Object obj);

    /* access modifiers changed from: protected */
    public final void b() {
        InvalidationListener invalidationListener = this.f3565a;
        if (invalidationListener != null) {
            invalidationListener.a();
        }
    }

    /* access modifiers changed from: protected */
    public final BandwidthMeter a() {
        BandwidthMeter bandwidthMeter = this.b;
        Assertions.a(bandwidthMeter);
        return bandwidthMeter;
    }
}
