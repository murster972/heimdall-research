package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import java.util.List;

public final class FixedTrackSelection extends BaseTrackSelection {
    private final int g;
    private final Object h;

    public FixedTrackSelection(TrackGroup trackGroup, int i) {
        this(trackGroup, i, 0, (Object) null);
    }

    public int a() {
        return 0;
    }

    public void a(long j, long j2, long j3, List<? extends MediaChunk> list, MediaChunkIterator[] mediaChunkIteratorArr) {
    }

    public Object b() {
        return this.h;
    }

    public int f() {
        return this.g;
    }

    public FixedTrackSelection(TrackGroup trackGroup, int i, int i2, Object obj) {
        super(trackGroup, i);
        this.g = i2;
        this.h = obj;
    }
}
