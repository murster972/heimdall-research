package com.google.android.exoplayer2.trackselection;

import java.util.Arrays;

public final class TrackSelectionArray {

    /* renamed from: a  reason: collision with root package name */
    public final int f3564a;
    private final TrackSelection[] b;
    private int c;

    public TrackSelectionArray(TrackSelection... trackSelectionArr) {
        this.b = trackSelectionArr;
        this.f3564a = trackSelectionArr.length;
    }

    public TrackSelection a(int i) {
        return this.b[i];
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || TrackSelectionArray.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.b, ((TrackSelectionArray) obj).b);
    }

    public int hashCode() {
        if (this.c == 0) {
            this.c = 527 + Arrays.hashCode(this.b);
        }
        return this.c;
    }

    public TrackSelection[] a() {
        return (TrackSelection[]) this.b.clone();
    }
}
