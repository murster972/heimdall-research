package com.google.android.exoplayer2.trackselection;

import android.os.SystemClock;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public abstract class BaseTrackSelection implements TrackSelection {

    /* renamed from: a  reason: collision with root package name */
    protected final TrackGroup f3556a;
    protected final int b;
    protected final int[] c;
    private final Format[] d;
    private final long[] e;
    private int f;

    private static final class DecreasingBandwidthComparator implements Comparator<Format> {
        private DecreasingBandwidthComparator() {
        }

        /* renamed from: a */
        public int compare(Format format, Format format2) {
            return format2.c - format.c;
        }
    }

    public BaseTrackSelection(TrackGroup trackGroup, int... iArr) {
        int i = 0;
        Assertions.b(iArr.length > 0);
        Assertions.a(trackGroup);
        this.f3556a = trackGroup;
        this.b = iArr.length;
        this.d = new Format[this.b];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            this.d[i2] = trackGroup.a(iArr[i2]);
        }
        Arrays.sort(this.d, new DecreasingBandwidthComparator());
        this.c = new int[this.b];
        while (true) {
            int i3 = this.b;
            if (i < i3) {
                this.c[i] = trackGroup.a(this.d[i]);
                i++;
            } else {
                this.e = new long[i3];
                return;
            }
        }
    }

    public final Format a(int i) {
        return this.d[i];
    }

    public void a(float f2) {
    }

    @Deprecated
    public /* synthetic */ void a(long j, long j2, long j3) {
        a.a(this, j, j2, j3);
    }

    public /* synthetic */ void a(long j, long j2, long j3, List<? extends MediaChunk> list, MediaChunkIterator[] mediaChunkIteratorArr) {
        a.a(this, j, j2, j3, list, mediaChunkIteratorArr);
    }

    public final int b(int i) {
        return this.c[i];
    }

    public final TrackGroup c() {
        return this.f3556a;
    }

    public final int d() {
        return this.c[a()];
    }

    public void disable() {
    }

    public final Format e() {
        return this.d[a()];
    }

    public void enable() {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BaseTrackSelection baseTrackSelection = (BaseTrackSelection) obj;
        if (this.f3556a != baseTrackSelection.f3556a || !Arrays.equals(this.c, baseTrackSelection.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.f == 0) {
            this.f = (System.identityHashCode(this.f3556a) * 31) + Arrays.hashCode(this.c);
        }
        return this.f;
    }

    public final int length() {
        return this.c.length;
    }

    public final int a(Format format) {
        for (int i = 0; i < this.b; i++) {
            if (this.d[i] == format) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public final boolean b(int i, long j) {
        return this.e[i] > j;
    }

    public final int c(int i) {
        for (int i2 = 0; i2 < this.b; i2++) {
            if (this.c[i2] == i) {
                return i2;
            }
        }
        return -1;
    }

    public int a(long j, List<? extends MediaChunk> list) {
        return list.size();
    }

    public final boolean a(int i, long j) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        boolean b2 = b(i, elapsedRealtime);
        int i2 = 0;
        while (i2 < this.b && !b2) {
            b2 = i2 != i && !b(i2, elapsedRealtime);
            i2++;
        }
        if (!b2) {
            return false;
        }
        long[] jArr = this.e;
        jArr[i] = Math.max(jArr[i], Util.a(elapsedRealtime, j, (long) Clock.MAX_TIME));
        return true;
    }
}
