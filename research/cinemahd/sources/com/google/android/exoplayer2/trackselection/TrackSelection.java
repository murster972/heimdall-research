package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import java.util.List;

public interface TrackSelection {

    public interface Factory {
        TrackSelection a(TrackGroup trackGroup, BandwidthMeter bandwidthMeter, int... iArr);
    }

    int a();

    int a(long j, List<? extends MediaChunk> list);

    int a(Format format);

    Format a(int i);

    void a(float f);

    @Deprecated
    void a(long j, long j2, long j3);

    void a(long j, long j2, long j3, List<? extends MediaChunk> list, MediaChunkIterator[] mediaChunkIteratorArr);

    boolean a(int i, long j);

    int b(int i);

    Object b();

    int c(int i);

    TrackGroup c();

    int d();

    void disable();

    Format e();

    void enable();

    int f();

    int length();
}
