package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.Clock;
import com.google.android.exoplayer2.util.Util;
import com.vungle.warren.AdLoader;
import java.util.List;

public class AdaptiveTrackSelection extends BaseTrackSelection {
    private final BandwidthMeter g;
    private final long h;
    private final long i;
    private final long j;
    private final float k;
    private final float l;
    private final long m;
    private final Clock n;
    private float o = 1.0f;
    private int p = a(Long.MIN_VALUE);
    private int q = 1;
    private long r = -9223372036854775807L;

    public static final class Factory implements TrackSelection.Factory {

        /* renamed from: a  reason: collision with root package name */
        private final BandwidthMeter f3555a;
        private final int b;
        private final int c;
        private final int d;
        private final float e;
        private final float f;
        private final long g;
        private final Clock h;

        public Factory() {
            this(10000, 25000, 25000, 0.75f, 0.75f, AdLoader.RETRY_DELAY, Clock.f3623a);
        }

        @Deprecated
        public Factory(BandwidthMeter bandwidthMeter) {
            this(bandwidthMeter, 10000, 25000, 25000, 0.75f, 0.75f, AdLoader.RETRY_DELAY, Clock.f3623a);
        }

        public AdaptiveTrackSelection a(TrackGroup trackGroup, BandwidthMeter bandwidthMeter, int... iArr) {
            BandwidthMeter bandwidthMeter2 = this.f3555a;
            return new AdaptiveTrackSelection(trackGroup, iArr, bandwidthMeter2 != null ? bandwidthMeter2 : bandwidthMeter, (long) this.b, (long) this.c, (long) this.d, this.e, this.f, this.g, this.h);
        }

        public Factory(int i, int i2, int i3, float f2, float f3, long j, Clock clock) {
            this((BandwidthMeter) null, i, i2, i3, f2, f3, j, clock);
        }

        @Deprecated
        public Factory(BandwidthMeter bandwidthMeter, int i, int i2, int i3, float f2, float f3, long j, Clock clock) {
            this.f3555a = bandwidthMeter;
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = f2;
            this.f = f3;
            this.g = j;
            this.h = clock;
        }
    }

    public AdaptiveTrackSelection(TrackGroup trackGroup, int[] iArr, BandwidthMeter bandwidthMeter, long j2, long j3, long j4, float f, float f2, long j5, Clock clock) {
        super(trackGroup, iArr);
        this.g = bandwidthMeter;
        this.h = j2 * 1000;
        this.i = j3 * 1000;
        this.j = j4 * 1000;
        this.k = f;
        this.l = f2;
        this.m = j5;
        this.n = clock;
    }

    private long b(long j2) {
        return (j2 > -9223372036854775807L ? 1 : (j2 == -9223372036854775807L ? 0 : -1)) != 0 && (j2 > this.h ? 1 : (j2 == this.h ? 0 : -1)) <= 0 ? (long) (((float) j2) * this.l) : this.h;
    }

    public void a(float f) {
        this.o = f;
    }

    public Object b() {
        return null;
    }

    public void enable() {
        this.r = -9223372036854775807L;
    }

    public int f() {
        return this.q;
    }

    public void a(long j2, long j3, long j4, List<? extends MediaChunk> list, MediaChunkIterator[] mediaChunkIteratorArr) {
        long a2 = this.n.a();
        int i2 = this.p;
        this.p = a(a2);
        if (this.p != i2) {
            if (!b(i2, a2)) {
                Format a3 = a(i2);
                Format a4 = a(this.p);
                if (a4.c > a3.c && j3 < b(j4)) {
                    this.p = i2;
                } else if (a4.c < a3.c && j3 >= this.i) {
                    this.p = i2;
                }
            }
            if (this.p != i2) {
                this.q = 3;
            }
        }
    }

    public int a() {
        return this.p;
    }

    public int a(long j2, List<? extends MediaChunk> list) {
        int i2;
        int i3;
        long a2 = this.n.a();
        long j3 = this.r;
        if (j3 != -9223372036854775807L && a2 - j3 < this.m) {
            return list.size();
        }
        this.r = a2;
        if (list.isEmpty()) {
            return 0;
        }
        int size = list.size();
        if (Util.b(((MediaChunk) list.get(size - 1)).f - j2, this.o) < this.j) {
            return size;
        }
        Format a3 = a(a(a2));
        for (int i4 = 0; i4 < size; i4++) {
            MediaChunk mediaChunk = (MediaChunk) list.get(i4);
            Format format = mediaChunk.c;
            if (Util.b(mediaChunk.f - j2, this.o) >= this.j && format.c < a3.c && (i2 = format.m) != -1 && i2 < 720 && (i3 = format.l) != -1 && i3 < 1280 && i2 < a3.m) {
                return i4;
            }
        }
        return size;
    }

    private int a(long j2) {
        long b = (long) (((float) this.g.b()) * this.k);
        int i2 = 0;
        for (int i3 = 0; i3 < this.b; i3++) {
            if (j2 == Long.MIN_VALUE || !b(i3, j2)) {
                if (((long) Math.round(((float) a(i3).c) * this.o)) <= b) {
                    return i3;
                }
                i2 = i3;
            }
        }
        return i2;
    }
}
