package com.google.android.exoplayer2.trackselection;

import android.os.SystemClock;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import java.util.List;
import java.util.Random;

public final class RandomTrackSelection extends BaseTrackSelection {
    private final Random g;
    private int h;

    public static final class Factory implements TrackSelection.Factory {

        /* renamed from: a  reason: collision with root package name */
        private final Random f3563a = new Random();

        public RandomTrackSelection a(TrackGroup trackGroup, BandwidthMeter bandwidthMeter, int... iArr) {
            return new RandomTrackSelection(trackGroup, iArr, this.f3563a);
        }
    }

    public RandomTrackSelection(TrackGroup trackGroup, int[] iArr, Random random) {
        super(trackGroup, iArr);
        this.g = random;
        this.h = random.nextInt(this.b);
    }

    public void a(long j, long j2, long j3, List<? extends MediaChunk> list, MediaChunkIterator[] mediaChunkIteratorArr) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        int i = 0;
        for (int i2 = 0; i2 < this.b; i2++) {
            if (!b(i2, elapsedRealtime)) {
                i++;
            }
        }
        this.h = this.g.nextInt(i);
        if (i != this.b) {
            int i3 = 0;
            for (int i4 = 0; i4 < this.b; i4++) {
                if (!b(i4, elapsedRealtime)) {
                    int i5 = i3 + 1;
                    if (this.h == i3) {
                        this.h = i4;
                        return;
                    }
                    i3 = i5;
                }
            }
        }
    }

    public Object b() {
        return null;
    }

    public int f() {
        return 3;
    }

    public int a() {
        return this.h;
    }
}
