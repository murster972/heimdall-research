package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* compiled from: lambda */
public final /* synthetic */ class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f3476a;
    private final /* synthetic */ MediaSourceEventListener b;
    private final /* synthetic */ MediaSourceEventListener.LoadEventInfo c;
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData d;

    public /* synthetic */ h(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f3476a = eventDispatcher;
        this.b = mediaSourceEventListener;
        this.c = loadEventInfo;
        this.d = mediaLoadData;
    }

    public final void run() {
        this.f3476a.b(this.b, this.c, this.d);
    }
}
