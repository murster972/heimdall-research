package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.Arrays;
import okhttp3.internal.http2.Http2;

public abstract class DataChunk extends Chunk {
    private byte[] i;
    private volatile boolean j;

    public DataChunk(DataSource dataSource, DataSpec dataSpec, int i2, Format format, int i3, Object obj, byte[] bArr) {
        super(dataSource, dataSpec, i2, format, i3, obj, -9223372036854775807L, -9223372036854775807L);
        this.i = bArr;
    }

    private void a(int i2) {
        byte[] bArr = this.i;
        if (bArr == null) {
            this.i = new byte[Http2.INITIAL_MAX_FRAME_SIZE];
        } else if (bArr.length < i2 + Http2.INITIAL_MAX_FRAME_SIZE) {
            this.i = Arrays.copyOf(bArr, bArr.length + Http2.INITIAL_MAX_FRAME_SIZE);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr, int i2) throws IOException;

    public final void cancelLoad() {
        this.j = true;
    }

    public byte[] e() {
        return this.i;
    }

    public final void load() throws IOException, InterruptedException {
        try {
            this.h.a(this.f3431a);
            int i2 = 0;
            int i3 = 0;
            while (i2 != -1 && !this.j) {
                a(i3);
                i2 = this.h.read(this.i, i3, Http2.INITIAL_MAX_FRAME_SIZE);
                if (i2 != -1) {
                    i3 += i2;
                }
            }
            if (!this.j) {
                a(this.i, i3);
            }
        } finally {
            Util.a((DataSource) this.h);
        }
    }
}
