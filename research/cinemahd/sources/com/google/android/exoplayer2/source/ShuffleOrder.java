package com.google.android.exoplayer2.source;

import java.util.Arrays;
import java.util.Random;

public interface ShuffleOrder {

    public static class DefaultShuffleOrder implements ShuffleOrder {

        /* renamed from: a  reason: collision with root package name */
        private final Random f3422a;
        private final int[] b;
        private final int[] c;

        public DefaultShuffleOrder(int i) {
            this(i, new Random());
        }

        public int a(int i) {
            int i2 = this.c[i] - 1;
            if (i2 >= 0) {
                return this.b[i2];
            }
            return -1;
        }

        public int b(int i) {
            int i2 = this.c[i] + 1;
            int[] iArr = this.b;
            if (i2 < iArr.length) {
                return iArr[i2];
            }
            return -1;
        }

        public int c() {
            int[] iArr = this.b;
            if (iArr.length > 0) {
                return iArr[0];
            }
            return -1;
        }

        public int getLength() {
            return this.b.length;
        }

        private DefaultShuffleOrder(int i, Random random) {
            this(a(i, random), random);
        }

        private DefaultShuffleOrder(int[] iArr, Random random) {
            this.b = iArr;
            this.f3422a = random;
            this.c = new int[iArr.length];
            for (int i = 0; i < iArr.length; i++) {
                this.c[iArr[i]] = i;
            }
        }

        public int a() {
            int[] iArr = this.b;
            if (iArr.length > 0) {
                return iArr[iArr.length - 1];
            }
            return -1;
        }

        public ShuffleOrder b(int i, int i2) {
            int[] iArr = new int[i2];
            int[] iArr2 = new int[i2];
            int i3 = 0;
            while (i3 < i2) {
                iArr[i3] = this.f3422a.nextInt(this.b.length + 1);
                int i4 = i3 + 1;
                int nextInt = this.f3422a.nextInt(i4);
                iArr2[i3] = iArr2[nextInt];
                iArr2[nextInt] = i3 + i;
                i3 = i4;
            }
            Arrays.sort(iArr);
            int[] iArr3 = new int[(this.b.length + i2)];
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < this.b.length + i2; i7++) {
                if (i5 >= i2 || i6 != iArr[i5]) {
                    int i8 = i6 + 1;
                    iArr3[i7] = this.b[i6];
                    if (iArr3[i7] >= i) {
                        iArr3[i7] = iArr3[i7] + i2;
                    }
                    i6 = i8;
                } else {
                    iArr3[i7] = iArr2[i5];
                    i5++;
                }
            }
            return new DefaultShuffleOrder(iArr3, new Random(this.f3422a.nextLong()));
        }

        public ShuffleOrder a(int i, int i2) {
            int i3 = i2 - i;
            int[] iArr = new int[(this.b.length - i3)];
            int i4 = 0;
            int i5 = 0;
            while (true) {
                int[] iArr2 = this.b;
                if (i4 >= iArr2.length) {
                    return new DefaultShuffleOrder(iArr, new Random(this.f3422a.nextLong()));
                }
                if (iArr2[i4] < i || iArr2[i4] >= i2) {
                    int i6 = i4 - i5;
                    int[] iArr3 = this.b;
                    iArr[i6] = iArr3[i4] >= i ? iArr3[i4] - i3 : iArr3[i4];
                } else {
                    i5++;
                }
                i4++;
            }
        }

        private static int[] a(int i, Random random) {
            int[] iArr = new int[i];
            int i2 = 0;
            while (i2 < i) {
                int i3 = i2 + 1;
                int nextInt = random.nextInt(i3);
                iArr[i2] = iArr[nextInt];
                iArr[nextInt] = i2;
                i2 = i3;
            }
            return iArr;
        }

        public ShuffleOrder b() {
            return new DefaultShuffleOrder(0, new Random(this.f3422a.nextLong()));
        }
    }

    int a();

    int a(int i);

    ShuffleOrder a(int i, int i2);

    int b(int i);

    ShuffleOrder b();

    ShuffleOrder b(int i, int i2);

    int c();

    int getLength();
}
