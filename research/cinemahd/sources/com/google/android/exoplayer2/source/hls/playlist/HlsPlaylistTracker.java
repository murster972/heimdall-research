package com.google.android.exoplayer2.source.hls.playlist;

import android.net.Uri;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.hls.HlsDataSourceFactory;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import java.io.IOException;

public interface HlsPlaylistTracker {

    public interface Factory {
        HlsPlaylistTracker a(HlsDataSourceFactory hlsDataSourceFactory, LoadErrorHandlingPolicy loadErrorHandlingPolicy, HlsPlaylistParserFactory hlsPlaylistParserFactory);
    }

    public interface PlaylistEventListener {
        boolean a(HlsMasterPlaylist.HlsUrl hlsUrl, long j);

        void g();
    }

    public static final class PlaylistResetException extends IOException {
        public final String url;

        public PlaylistResetException(String str) {
            this.url = str;
        }
    }

    public static final class PlaylistStuckException extends IOException {
        public final String url;

        public PlaylistStuckException(String str) {
            this.url = str;
        }
    }

    public interface PrimaryPlaylistListener {
        void a(HlsMediaPlaylist hlsMediaPlaylist);
    }

    long a();

    HlsMediaPlaylist a(HlsMasterPlaylist.HlsUrl hlsUrl, boolean z);

    void a(Uri uri, MediaSourceEventListener.EventDispatcher eventDispatcher, PrimaryPlaylistListener primaryPlaylistListener);

    void a(HlsMasterPlaylist.HlsUrl hlsUrl);

    void a(PlaylistEventListener playlistEventListener);

    HlsMasterPlaylist b();

    void b(PlaylistEventListener playlistEventListener);

    boolean b(HlsMasterPlaylist.HlsUrl hlsUrl);

    void c() throws IOException;

    void c(HlsMasterPlaylist.HlsUrl hlsUrl) throws IOException;

    boolean isLive();

    void stop();
}
