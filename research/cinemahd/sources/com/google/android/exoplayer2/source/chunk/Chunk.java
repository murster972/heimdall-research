package com.google.android.exoplayer2.source.chunk;

import android.net.Uri;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.upstream.StatsDataSource;
import com.google.android.exoplayer2.util.Assertions;
import java.util.List;
import java.util.Map;

public abstract class Chunk implements Loader.Loadable {

    /* renamed from: a  reason: collision with root package name */
    public final DataSpec f3431a;
    public final int b;
    public final Format c;
    public final int d;
    public final Object e;
    public final long f;
    public final long g;
    protected final StatsDataSource h;

    public Chunk(DataSource dataSource, DataSpec dataSpec, int i, Format format, int i2, Object obj, long j, long j2) {
        this.h = new StatsDataSource(dataSource);
        Assertions.a(dataSpec);
        this.f3431a = dataSpec;
        this.b = i;
        this.c = format;
        this.d = i2;
        this.e = obj;
        this.f = j;
        this.g = j2;
    }

    public final long a() {
        return this.h.b();
    }

    public final long b() {
        return this.g - this.f;
    }

    public final Map<String, List<String>> c() {
        return this.h.d();
    }

    public final Uri d() {
        return this.h.c();
    }
}
