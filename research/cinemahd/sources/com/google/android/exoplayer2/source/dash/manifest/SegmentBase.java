package com.google.android.exoplayer2.source.dash.manifest;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.util.Util;
import java.util.List;

public abstract class SegmentBase {

    /* renamed from: a  reason: collision with root package name */
    final RangedUri f3468a;
    final long b;
    final long c;

    public static class SegmentList extends MultiSegmentBase {
        final List<RangedUri> g;

        public SegmentList(RangedUri rangedUri, long j, long j2, long j3, long j4, List<SegmentTimelineElement> list, List<RangedUri> list2) {
            super(rangedUri, j, j2, j3, j4, list);
            this.g = list2;
        }

        public RangedUri a(Representation representation, long j) {
            return this.g.get((int) (j - this.d));
        }

        public boolean c() {
            return true;
        }

        public int a(long j) {
            return this.g.size();
        }
    }

    public static class SegmentTimelineElement {

        /* renamed from: a  reason: collision with root package name */
        final long f3469a;
        final long b;

        public SegmentTimelineElement(long j, long j2) {
            this.f3469a = j;
            this.b = j2;
        }
    }

    public SegmentBase(RangedUri rangedUri, long j, long j2) {
        this.f3468a = rangedUri;
        this.b = j;
        this.c = j2;
    }

    public RangedUri a(Representation representation) {
        return this.f3468a;
    }

    public long a() {
        return Util.c(this.c, 1000000, this.b);
    }

    public static class SingleSegmentBase extends SegmentBase {
        final long d;
        final long e;

        public SingleSegmentBase(RangedUri rangedUri, long j, long j2, long j3, long j4) {
            super(rangedUri, j, j2);
            this.d = j3;
            this.e = j4;
        }

        public RangedUri b() {
            long j = this.e;
            if (j <= 0) {
                return null;
            }
            return new RangedUri((String) null, this.d, j);
        }

        public SingleSegmentBase() {
            this((RangedUri) null, 1, 0, 0, 0);
        }
    }

    public static class SegmentTemplate extends MultiSegmentBase {
        final UrlTemplate g;
        final UrlTemplate h;

        public SegmentTemplate(RangedUri rangedUri, long j, long j2, long j3, long j4, List<SegmentTimelineElement> list, UrlTemplate urlTemplate, UrlTemplate urlTemplate2) {
            super(rangedUri, j, j2, j3, j4, list);
            this.g = urlTemplate;
            this.h = urlTemplate2;
        }

        public RangedUri a(Representation representation) {
            UrlTemplate urlTemplate = this.g;
            if (urlTemplate == null) {
                return SegmentBase.super.a(representation);
            }
            Format format = representation.f3467a;
            return new RangedUri(urlTemplate.a(format.f3164a, 0, format.c, 0), 0, -1);
        }

        public RangedUri a(Representation representation, long j) {
            long j2;
            List<SegmentTimelineElement> list = this.f;
            if (list != null) {
                j2 = list.get((int) (j - this.d)).f3469a;
            } else {
                j2 = (j - this.d) * this.e;
            }
            long j3 = j2;
            UrlTemplate urlTemplate = this.h;
            Format format = representation.f3467a;
            return new RangedUri(urlTemplate.a(format.f3164a, j, format.c, j3), 0, -1);
        }

        public int a(long j) {
            List<SegmentTimelineElement> list = this.f;
            if (list != null) {
                return list.size();
            }
            if (j != -9223372036854775807L) {
                return (int) Util.a(j, (this.e * 1000000) / this.b);
            }
            return -1;
        }
    }

    public static abstract class MultiSegmentBase extends SegmentBase {
        final long d;
        final long e;
        final List<SegmentTimelineElement> f;

        public MultiSegmentBase(RangedUri rangedUri, long j, long j2, long j3, long j4, List<SegmentTimelineElement> list) {
            super(rangedUri, j, j2);
            this.d = j3;
            this.e = j4;
            this.f = list;
        }

        public abstract int a(long j);

        public final long a(long j, long j2) {
            List<SegmentTimelineElement> list = this.f;
            if (list != null) {
                return (list.get((int) (j - this.d)).b * 1000000) / this.b;
            }
            int a2 = a(j2);
            return (a2 == -1 || j != (b() + ((long) a2)) - 1) ? (this.e * 1000000) / this.b : j2 - b(j);
        }

        public abstract RangedUri a(Representation representation, long j);

        public long b(long j, long j2) {
            long b = b();
            long a2 = (long) a(j2);
            if (a2 == 0) {
                return b;
            }
            if (this.f == null) {
                long j3 = (j / ((this.e * 1000000) / this.b)) + this.d;
                if (j3 < b) {
                    return b;
                }
                if (a2 == -1) {
                    return j3;
                }
                return Math.min(j3, (b + a2) - 1);
            }
            long j4 = (a2 + b) - 1;
            long j5 = b;
            while (j5 <= j4) {
                long j6 = ((j4 - j5) / 2) + j5;
                int i = (b(j6) > j ? 1 : (b(j6) == j ? 0 : -1));
                if (i < 0) {
                    j5 = j6 + 1;
                } else if (i <= 0) {
                    return j6;
                } else {
                    j4 = j6 - 1;
                }
            }
            return j5 == b ? j5 : j4;
        }

        public boolean c() {
            return this.f != null;
        }

        public final long b(long j) {
            long j2;
            List<SegmentTimelineElement> list = this.f;
            if (list != null) {
                j2 = list.get((int) (j - this.d)).f3469a - this.c;
            } else {
                j2 = (j - this.d) * this.e;
            }
            return Util.c(j2, 1000000, this.b);
        }

        public long b() {
            return this.d;
        }
    }
}
