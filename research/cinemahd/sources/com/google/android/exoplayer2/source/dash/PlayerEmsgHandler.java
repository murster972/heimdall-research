package com.google.android.exoplayer2.source.dash;

import android.os.Handler;
import android.os.Message;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.metadata.MetadataInputBuffer;
import com.google.android.exoplayer2.metadata.emsg.EventMessage;
import com.google.android.exoplayer2.metadata.emsg.EventMessageDecoder;
import com.google.android.exoplayer2.source.SampleQueue;
import com.google.android.exoplayer2.source.chunk.Chunk;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import okhttp3.internal.cache.DiskLruCache;

public final class PlayerEmsgHandler implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final Allocator f3453a;
    private final PlayerEmsgCallback b;
    /* access modifiers changed from: private */
    public final EventMessageDecoder c = new EventMessageDecoder();
    /* access modifiers changed from: private */
    public final Handler d = Util.a((Handler.Callback) this);
    private final TreeMap<Long, Long> e = new TreeMap<>();
    private DashManifest f;
    private long g;
    private long h = -9223372036854775807L;
    private long i = -9223372036854775807L;
    private boolean j;
    private boolean k;

    private static final class ManifestExpiryEventInfo {

        /* renamed from: a  reason: collision with root package name */
        public final long f3454a;
        public final long b;

        public ManifestExpiryEventInfo(long j, long j2) {
            this.f3454a = j;
            this.b = j2;
        }
    }

    public interface PlayerEmsgCallback {
        void a();

        void a(long j);
    }

    public final class PlayerTrackEmsgHandler implements TrackOutput {

        /* renamed from: a  reason: collision with root package name */
        private final SampleQueue f3455a;
        private final FormatHolder b = new FormatHolder();
        private final MetadataInputBuffer c = new MetadataInputBuffer();

        PlayerTrackEmsgHandler(SampleQueue sampleQueue) {
            this.f3455a = sampleQueue;
        }

        private void c() {
            while (this.f3455a.j()) {
                MetadataInputBuffer b2 = b();
                if (b2 != null) {
                    long j = b2.c;
                    EventMessage eventMessage = (EventMessage) PlayerEmsgHandler.this.c.a(b2).a(0);
                    if (PlayerEmsgHandler.a(eventMessage.f3366a, eventMessage.b)) {
                        a(j, eventMessage);
                    }
                }
            }
            this.f3455a.c();
        }

        public void a(Format format) {
            this.f3455a.a(format);
        }

        public void b(Chunk chunk) {
            PlayerEmsgHandler.this.b(chunk);
        }

        private MetadataInputBuffer b() {
            this.c.clear();
            if (this.f3455a.a(this.b, (DecoderInputBuffer) this.c, false, false, 0) != -4) {
                return null;
            }
            this.c.b();
            return this.c;
        }

        public int a(ExtractorInput extractorInput, int i, boolean z) throws IOException, InterruptedException {
            return this.f3455a.a(extractorInput, i, z);
        }

        public void a(ParsableByteArray parsableByteArray, int i) {
            this.f3455a.a(parsableByteArray, i);
        }

        public void a(long j, int i, int i2, int i3, TrackOutput.CryptoData cryptoData) {
            this.f3455a.a(j, i, i2, i3, cryptoData);
            c();
        }

        public boolean a(long j) {
            return PlayerEmsgHandler.this.a(j);
        }

        public boolean a(Chunk chunk) {
            return PlayerEmsgHandler.this.a(chunk);
        }

        public void a() {
            this.f3455a.l();
        }

        private void a(long j, EventMessage eventMessage) {
            long a2 = PlayerEmsgHandler.b(eventMessage);
            if (a2 != -9223372036854775807L) {
                a(j, a2);
            }
        }

        private void a(long j, long j2) {
            PlayerEmsgHandler.this.d.sendMessage(PlayerEmsgHandler.this.d.obtainMessage(1, new ManifestExpiryEventInfo(j, j2)));
        }
    }

    public PlayerEmsgHandler(DashManifest dashManifest, PlayerEmsgCallback playerEmsgCallback, Allocator allocator) {
        this.f = dashManifest;
        this.b = playerEmsgCallback;
        this.f3453a = allocator;
    }

    private void c() {
        long j2 = this.i;
        if (j2 == -9223372036854775807L || j2 != this.h) {
            this.j = true;
            this.i = this.h;
            this.b.a();
        }
    }

    private void d() {
        this.b.a(this.g);
    }

    private void e() {
        Iterator<Map.Entry<Long, Long>> it2 = this.e.entrySet().iterator();
        while (it2.hasNext()) {
            if (((Long) it2.next().getKey()).longValue() < this.f.h) {
                it2.remove();
            }
        }
    }

    public boolean handleMessage(Message message) {
        if (this.k) {
            return true;
        }
        if (message.what != 1) {
            return false;
        }
        ManifestExpiryEventInfo manifestExpiryEventInfo = (ManifestExpiryEventInfo) message.obj;
        a(manifestExpiryEventInfo.f3454a, manifestExpiryEventInfo.b);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(Chunk chunk) {
        long j2 = this.h;
        if (j2 != -9223372036854775807L || chunk.g > j2) {
            this.h = chunk.g;
        }
    }

    public void a(DashManifest dashManifest) {
        this.j = false;
        this.g = -9223372036854775807L;
        this.f = dashManifest;
        e();
    }

    public void b() {
        this.k = true;
        this.d.removeCallbacksAndMessages((Object) null);
    }

    private Map.Entry<Long, Long> b(long j2) {
        return this.e.ceilingEntry(Long.valueOf(j2));
    }

    /* access modifiers changed from: private */
    public static long b(EventMessage eventMessage) {
        try {
            return Util.h(Util.a(eventMessage.f));
        } catch (ParserException unused) {
            return -9223372036854775807L;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(long j2) {
        DashManifest dashManifest = this.f;
        boolean z = false;
        if (!dashManifest.d) {
            return false;
        }
        if (this.j) {
            return true;
        }
        Map.Entry<Long, Long> b2 = b(dashManifest.h);
        if (b2 != null && b2.getValue().longValue() < j2) {
            this.g = b2.getKey().longValue();
            d();
            z = true;
        }
        if (z) {
            c();
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Chunk chunk) {
        if (!this.f.d) {
            return false;
        }
        if (this.j) {
            return true;
        }
        long j2 = this.h;
        if (!(j2 != -9223372036854775807L && j2 < chunk.f)) {
            return false;
        }
        c();
        return true;
    }

    public static boolean a(String str, String str2) {
        return "urn:mpeg:dash:event:2012".equals(str) && (DiskLruCache.VERSION_1.equals(str2) || TraktV2.API_VERSION.equals(str2) || "3".equals(str2));
    }

    public PlayerTrackEmsgHandler a() {
        return new PlayerTrackEmsgHandler(new SampleQueue(this.f3453a));
    }

    private void a(long j2, long j3) {
        Long l = this.e.get(Long.valueOf(j3));
        if (l == null) {
            this.e.put(Long.valueOf(j3), Long.valueOf(j2));
        } else if (l.longValue() > j2) {
            this.e.put(Long.valueOf(j3), Long.valueOf(j2));
        }
    }
}
