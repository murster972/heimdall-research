package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.extractor.DummyTrackOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.source.SampleQueue;
import com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper;
import com.google.android.exoplayer2.util.Log;

public final class BaseMediaChunkOutput implements ChunkExtractorWrapper.TrackOutputProvider {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f3430a;
    private final SampleQueue[] b;

    public BaseMediaChunkOutput(int[] iArr, SampleQueue[] sampleQueueArr) {
        this.f3430a = iArr;
        this.b = sampleQueueArr;
    }

    public TrackOutput a(int i, int i2) {
        int i3 = 0;
        while (true) {
            int[] iArr = this.f3430a;
            if (i3 >= iArr.length) {
                Log.b("BaseMediaChunkOutput", "Unmatched track of type: " + i2);
                return new DummyTrackOutput();
            } else if (i2 == iArr[i3]) {
                return this.b[i3];
            } else {
                i3++;
            }
        }
    }

    public int[] a() {
        int[] iArr = new int[this.b.length];
        int i = 0;
        while (true) {
            SampleQueue[] sampleQueueArr = this.b;
            if (i >= sampleQueueArr.length) {
                return iArr;
            }
            if (sampleQueueArr[i] != null) {
                iArr[i] = sampleQueueArr[i].i();
            }
            i++;
        }
    }

    public void a(long j) {
        for (SampleQueue sampleQueue : this.b) {
            if (sampleQueue != null) {
                sampleQueue.a(j);
            }
        }
    }
}
