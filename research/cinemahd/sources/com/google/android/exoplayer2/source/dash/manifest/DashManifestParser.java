package com.google.android.exoplayer2.source.dash.manifest;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import android.util.Xml;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.metadata.emsg.EventMessage;
import com.google.android.exoplayer2.source.dash.manifest.SegmentBase;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.UriUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.util.XmlPullParserUtil;
import com.google.android.gms.ads.AdRequest;
import com.vungle.warren.model.Advertisement;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

public class DashManifestParser extends DefaultHandler implements ParsingLoadable.Parser<DashManifest> {
    private static final Pattern c = Pattern.compile("(\\d+)(?:/(\\d+))?");
    private static final Pattern d = Pattern.compile("CC([1-4])=.*");
    private static final Pattern e = Pattern.compile("([1-9]|[1-5][0-9]|6[0-3])=.*");

    /* renamed from: a  reason: collision with root package name */
    private final String f3460a;
    private final XmlPullParserFactory b;

    protected static final class RepresentationInfo {

        /* renamed from: a  reason: collision with root package name */
        public final Format f3461a;
        public final String b;
        public final SegmentBase c;
        public final String d;
        public final ArrayList<DrmInitData.SchemeData> e;
        public final ArrayList<Descriptor> f;
        public final long g;

        public RepresentationInfo(Format format, String str, SegmentBase segmentBase, String str2, ArrayList<DrmInitData.SchemeData> arrayList, ArrayList<Descriptor> arrayList2, long j) {
            this.f3461a = format;
            this.b = str;
            this.c = segmentBase;
            this.d = str2;
            this.e = arrayList;
            this.f = arrayList2;
            this.g = j;
        }
    }

    public DashManifestParser() {
        this((String) null);
    }

    public static void l(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        if (XmlPullParserUtil.b(xmlPullParser)) {
            int i = 1;
            while (i != 0) {
                xmlPullParser.next();
                if (XmlPullParserUtil.b(xmlPullParser)) {
                    i++;
                } else if (XmlPullParserUtil.a(xmlPullParser)) {
                    i--;
                }
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static int m(org.xmlpull.v1.XmlPullParser r5) {
        /*
            r0 = 0
            java.lang.String r1 = "value"
            java.lang.String r5 = r5.getAttributeValue(r0, r1)
            java.lang.String r5 = com.google.android.exoplayer2.util.Util.k(r5)
            r0 = -1
            if (r5 != 0) goto L_0x000f
            return r0
        L_0x000f:
            int r1 = r5.hashCode()
            r2 = 3
            r3 = 2
            r4 = 1
            switch(r1) {
                case 1596796: goto L_0x0038;
                case 2937391: goto L_0x002e;
                case 3094035: goto L_0x0024;
                case 3133436: goto L_0x001a;
                default: goto L_0x0019;
            }
        L_0x0019:
            goto L_0x0042
        L_0x001a:
            java.lang.String r1 = "fa01"
            boolean r5 = r5.equals(r1)
            if (r5 == 0) goto L_0x0042
            r5 = 3
            goto L_0x0043
        L_0x0024:
            java.lang.String r1 = "f801"
            boolean r5 = r5.equals(r1)
            if (r5 == 0) goto L_0x0042
            r5 = 2
            goto L_0x0043
        L_0x002e:
            java.lang.String r1 = "a000"
            boolean r5 = r5.equals(r1)
            if (r5 == 0) goto L_0x0042
            r5 = 1
            goto L_0x0043
        L_0x0038:
            java.lang.String r1 = "4000"
            boolean r5 = r5.equals(r1)
            if (r5 == 0) goto L_0x0042
            r5 = 0
            goto L_0x0043
        L_0x0042:
            r5 = -1
        L_0x0043:
            if (r5 == 0) goto L_0x0052
            if (r5 == r4) goto L_0x0051
            if (r5 == r3) goto L_0x004f
            if (r5 == r2) goto L_0x004c
            return r0
        L_0x004c:
            r5 = 8
            return r5
        L_0x004f:
            r5 = 6
            return r5
        L_0x0051:
            return r3
        L_0x0052:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.m(org.xmlpull.v1.XmlPullParser):int");
    }

    /* access modifiers changed from: protected */
    public int b(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String b2 = b(xmlPullParser, "schemeIdUri", (String) null);
        int i = -1;
        if ("urn:mpeg:dash:23003:3:audio_channel_configuration:2011".equals(b2)) {
            i = a(xmlPullParser, "value", -1);
        } else if ("tag:dolby.com,2014:dash:audio_channel_configuration:2011".equals(b2)) {
            i = m(xmlPullParser);
        }
        do {
            xmlPullParser.next();
        } while (!XmlPullParserUtil.c(xmlPullParser, "AudioChannelConfiguration"));
        return i;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: byte[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v12, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v11, resolved type: java.lang.String} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x013f A[LOOP:1: B:35:0x009d->B:75:0x013f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x012e A[EDGE_INSN: B:77:0x012e->B:70:0x012e ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.util.Pair<java.lang.String, com.google.android.exoplayer2.drm.DrmInitData.SchemeData> c(org.xmlpull.v1.XmlPullParser r17) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r16 = this;
            r0 = r17
            r1 = 0
            java.lang.String r2 = "schemeIdUri"
            java.lang.String r2 = r0.getAttributeValue(r1, r2)
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0098
            java.lang.String r2 = com.google.android.exoplayer2.util.Util.k(r2)
            r5 = -1
            int r6 = r2.hashCode()
            r7 = 489446379(0x1d2c5beb, float:2.281153E-21)
            r8 = 2
            if (r6 == r7) goto L_0x003b
            r7 = 755418770(0x2d06c692, float:7.66111E-12)
            if (r6 == r7) goto L_0x0031
            r7 = 1812765994(0x6c0c9d2a, float:6.799672E26)
            if (r6 == r7) goto L_0x0027
            goto L_0x0044
        L_0x0027:
            java.lang.String r6 = "urn:mpeg:dash:mp4protection:2011"
            boolean r2 = r2.equals(r6)
            if (r2 == 0) goto L_0x0044
            r5 = 0
            goto L_0x0044
        L_0x0031:
            java.lang.String r6 = "urn:uuid:edef8ba9-79d6-4ace-a3c8-27dcd51d21ed"
            boolean r2 = r2.equals(r6)
            if (r2 == 0) goto L_0x0044
            r5 = 2
            goto L_0x0044
        L_0x003b:
            java.lang.String r6 = "urn:uuid:9a04f079-9840-4286-ab92-e65be0885f95"
            boolean r2 = r2.equals(r6)
            if (r2 == 0) goto L_0x0044
            r5 = 1
        L_0x0044:
            if (r5 == 0) goto L_0x0052
            if (r5 == r3) goto L_0x004e
            if (r5 == r8) goto L_0x004b
            goto L_0x0098
        L_0x004b:
            java.util.UUID r2 = com.google.android.exoplayer2.C.d
            goto L_0x0050
        L_0x004e:
            java.util.UUID r2 = com.google.android.exoplayer2.C.e
        L_0x0050:
            r5 = r1
            goto L_0x009a
        L_0x0052:
            java.lang.String r2 = "value"
            java.lang.String r2 = r0.getAttributeValue(r1, r2)
            java.lang.String r5 = "default_KID"
            java.lang.String r5 = com.google.android.exoplayer2.util.XmlPullParserUtil.b(r0, r5)
            boolean r6 = android.text.TextUtils.isEmpty(r5)
            if (r6 != 0) goto L_0x0092
            java.lang.String r6 = "00000000-0000-0000-0000-000000000000"
            boolean r6 = r6.equals(r5)
            if (r6 != 0) goto L_0x0092
            java.lang.String r6 = "\\s+"
            java.lang.String[] r5 = r5.split(r6)
            int r6 = r5.length
            java.util.UUID[] r6 = new java.util.UUID[r6]
            r7 = 0
        L_0x0076:
            int r8 = r5.length
            if (r7 >= r8) goto L_0x0084
            r8 = r5[r7]
            java.util.UUID r8 = java.util.UUID.fromString(r8)
            r6[r7] = r8
            int r7 = r7 + 1
            goto L_0x0076
        L_0x0084:
            java.util.UUID r5 = com.google.android.exoplayer2.C.b
            byte[] r5 = com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil.a(r5, r6, r1)
            java.util.UUID r6 = com.google.android.exoplayer2.C.b
            r7 = r1
            r8 = 0
            r15 = r6
            r6 = r2
            r2 = r15
            goto L_0x009d
        L_0x0092:
            r5 = r1
            r7 = r5
            r6 = r2
            r8 = 0
            r2 = r7
            goto L_0x009d
        L_0x0098:
            r2 = r1
            r5 = r2
        L_0x009a:
            r6 = r5
            r7 = r6
            r8 = 0
        L_0x009d:
            r17.next()
            java.lang.String r9 = "ms:laurl"
            boolean r9 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r9)
            if (r9 == 0) goto L_0x00b4
            java.lang.String r7 = "licenseUrl"
            java.lang.String r7 = r0.getAttributeValue(r1, r7)
        L_0x00ae:
            r10 = r2
            r13 = r5
        L_0x00b0:
            r11 = r7
            r14 = r8
            goto L_0x0126
        L_0x00b4:
            java.lang.String r9 = "widevine:license"
            boolean r9 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r9)
            if (r9 == 0) goto L_0x00d0
            java.lang.String r8 = "robustness_level"
            java.lang.String r8 = r0.getAttributeValue(r1, r8)
            if (r8 == 0) goto L_0x00ce
            java.lang.String r9 = "HW"
            boolean r8 = r8.startsWith(r9)
            if (r8 == 0) goto L_0x00ce
            r8 = 1
            goto L_0x00ae
        L_0x00ce:
            r8 = 0
            goto L_0x00ae
        L_0x00d0:
            r9 = 4
            if (r5 != 0) goto L_0x00fb
            java.lang.String r10 = "pssh"
            boolean r10 = com.google.android.exoplayer2.util.XmlPullParserUtil.e(r0, r10)
            if (r10 == 0) goto L_0x00fb
            int r10 = r17.next()
            if (r10 != r9) goto L_0x00fb
            java.lang.String r2 = r17.getText()
            byte[] r2 = android.util.Base64.decode(r2, r4)
            java.util.UUID r5 = com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil.c(r2)
            if (r5 != 0) goto L_0x00f8
            java.lang.String r2 = "MpdParser"
            java.lang.String r9 = "Skipping malformed cenc:pssh data"
            com.google.android.exoplayer2.util.Log.d(r2, r9)
            r13 = r1
            goto L_0x00f9
        L_0x00f8:
            r13 = r2
        L_0x00f9:
            r10 = r5
            goto L_0x00b0
        L_0x00fb:
            if (r5 != 0) goto L_0x0122
            java.util.UUID r10 = com.google.android.exoplayer2.C.e
            boolean r10 = r10.equals(r2)
            if (r10 == 0) goto L_0x0122
            java.lang.String r10 = "mspr:pro"
            boolean r10 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r10)
            if (r10 == 0) goto L_0x0122
            int r10 = r17.next()
            if (r10 != r9) goto L_0x0122
            java.util.UUID r5 = com.google.android.exoplayer2.C.e
            java.lang.String r9 = r17.getText()
            byte[] r9 = android.util.Base64.decode(r9, r4)
            byte[] r5 = com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil.a((java.util.UUID) r5, (byte[]) r9)
            goto L_0x00ae
        L_0x0122:
            l(r17)
            goto L_0x00ae
        L_0x0126:
            java.lang.String r2 = "ContentProtection"
            boolean r2 = com.google.android.exoplayer2.util.XmlPullParserUtil.c(r0, r2)
            if (r2 == 0) goto L_0x013f
            if (r10 == 0) goto L_0x0139
            com.google.android.exoplayer2.drm.DrmInitData$SchemeData r0 = new com.google.android.exoplayer2.drm.DrmInitData$SchemeData
            java.lang.String r12 = "video/mp4"
            r9 = r0
            r9.<init>(r10, r11, r12, r13, r14)
            goto L_0x013a
        L_0x0139:
            r0 = r1
        L_0x013a:
            android.util.Pair r0 = android.util.Pair.create(r6, r0)
            return r0
        L_0x013f:
            r2 = r10
            r7 = r11
            r5 = r13
            r8 = r14
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.c(org.xmlpull.v1.XmlPullParser):android.util.Pair");
    }

    /* access modifiers changed from: protected */
    public int d(XmlPullParser xmlPullParser) {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, "contentType");
        if (TextUtils.isEmpty(attributeValue)) {
            return -1;
        }
        if ("audio".equals(attributeValue)) {
            return 1;
        }
        if (Advertisement.KEY_VIDEO.equals(attributeValue)) {
            return 2;
        }
        if ("text".equals(attributeValue)) {
            return 3;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public EventStream e(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String b2 = b(xmlPullParser, "schemeIdUri", "");
        String b3 = b(xmlPullParser, "value", "");
        long d2 = d(xmlPullParser, "timescale", 1);
        ArrayList arrayList = new ArrayList();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(AdRequest.MAX_CONTENT_URL_LENGTH);
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser, "Event")) {
                arrayList.add(a(xmlPullParser, b2, b3, d2, byteArrayOutputStream));
            } else {
                l(xmlPullParser);
            }
        } while (!XmlPullParserUtil.c(xmlPullParser, "EventStream"));
        long[] jArr = new long[arrayList.size()];
        EventMessage[] eventMessageArr = new EventMessage[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            EventMessage eventMessage = (EventMessage) arrayList.get(i);
            jArr[i] = eventMessage.d;
            eventMessageArr[i] = eventMessage;
        }
        return a(b2, b3, d2, jArr, eventMessageArr);
    }

    /* access modifiers changed from: protected */
    public RangedUri f(XmlPullParser xmlPullParser) {
        return a(xmlPullParser, "sourceURL", "range");
    }

    /* access modifiers changed from: protected */
    public ProgramInformation g(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        String str = null;
        String b2 = b(xmlPullParser, "moreInformationURL", (String) null);
        String b3 = b(xmlPullParser, "lang", (String) null);
        String str2 = null;
        String str3 = null;
        while (true) {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser, "Title")) {
                str = xmlPullParser.nextText();
            } else if (XmlPullParserUtil.d(xmlPullParser, "Source")) {
                str2 = xmlPullParser.nextText();
            } else if (XmlPullParserUtil.d(xmlPullParser, "Copyright")) {
                str3 = xmlPullParser.nextText();
            } else {
                l(xmlPullParser);
            }
            String str4 = str3;
            if (XmlPullParserUtil.c(xmlPullParser, "ProgramInformation")) {
                return new ProgramInformation(str, str2, str4, b2, b3);
            }
            str3 = str4;
        }
    }

    /* access modifiers changed from: protected */
    public int h(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String b2 = b(xmlPullParser, "schemeIdUri", (String) null);
        String b3 = b(xmlPullParser, "value", (String) null);
        do {
            xmlPullParser.next();
        } while (!XmlPullParserUtil.c(xmlPullParser, "Role"));
        return (!"urn:mpeg:dash:role:2011".equals(b2) || !"main".equals(b3)) ? 0 : 1;
    }

    /* access modifiers changed from: protected */
    public List<SegmentBase.SegmentTimelineElement> i(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        long j = 0;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser, "S")) {
                j = d(xmlPullParser, "t", j);
                long d2 = d(xmlPullParser, "d", -9223372036854775807L);
                int a2 = a(xmlPullParser, "r", 0) + 1;
                for (int i = 0; i < a2; i++) {
                    arrayList.add(a(j, d2));
                    j += d2;
                }
            } else {
                l(xmlPullParser);
            }
        } while (!XmlPullParserUtil.c(xmlPullParser, "SegmentTimeline"));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public RangedUri j(XmlPullParser xmlPullParser) {
        return a(xmlPullParser, "media", "mediaRange");
    }

    /* access modifiers changed from: protected */
    public UtcTimingElement k(XmlPullParser xmlPullParser) {
        return a(xmlPullParser.getAttributeValue((String) null, "schemeIdUri"), xmlPullParser.getAttributeValue((String) null, "value"));
    }

    public DashManifestParser(String str) {
        this.f3460a = str;
        try {
            this.b = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e2) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e2);
        }
    }

    public DashManifest a(Uri uri, InputStream inputStream) throws IOException {
        try {
            XmlPullParser newPullParser = this.b.newPullParser();
            newPullParser.setInput(inputStream, (String) null);
            if (newPullParser.next() == 2 && "MPD".equals(newPullParser.getName())) {
                return a(newPullParser, uri.toString());
            }
            throw new ParserException("inputStream does not contain a valid media presentation description");
        } catch (XmlPullParserException e2) {
            throw new ParserException((Throwable) e2);
        }
    }

    protected static long d(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
        return attributeValue == null ? j : Long.parseLong(attributeValue);
    }

    private static String b(String str, String str2) {
        if (str == null) {
            return str2;
        }
        if (str2 == null) {
            return str;
        }
        Assertions.b(str.equals(str2));
        return str;
    }

    protected static int b(List<Descriptor> list) {
        String str;
        for (int i = 0; i < list.size(); i++) {
            Descriptor descriptor = list.get(i);
            if ("urn:scte:dash:cc:cea-708:2015".equals(descriptor.f3462a) && (str = descriptor.b) != null) {
                Matcher matcher = e.matcher(str);
                if (matcher.matches()) {
                    return Integer.parseInt(matcher.group(1));
                }
                Log.d("MpdParser", "Unable to parse CEA-708 service block number from: " + descriptor.b);
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0179 A[LOOP:0: B:20:0x006c->B:67:0x0179, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x013d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.exoplayer2.source.dash.manifest.DashManifest a(org.xmlpull.v1.XmlPullParser r33, java.lang.String r34) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r32 = this;
            r0 = r33
            r1 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            java.lang.String r3 = "availabilityStartTime"
            long r5 = b((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r3, (long) r1)
            java.lang.String r3 = "mediaPresentationDuration"
            long r3 = c(r0, r3, r1)
            java.lang.String r7 = "minBufferTime"
            long r9 = c(r0, r7, r1)
            r7 = 0
            java.lang.String r8 = "type"
            java.lang.String r8 = r0.getAttributeValue(r7, r8)
            r12 = 0
            if (r8 == 0) goto L_0x002d
            java.lang.String r13 = "dynamic"
            boolean r8 = r13.equals(r8)
            if (r8 == 0) goto L_0x002d
            r13 = 1
            goto L_0x002e
        L_0x002d:
            r13 = 0
        L_0x002e:
            if (r13 == 0) goto L_0x0037
            java.lang.String r8 = "minimumUpdatePeriod"
            long r14 = c(r0, r8, r1)
            goto L_0x0038
        L_0x0037:
            r14 = r1
        L_0x0038:
            if (r13 == 0) goto L_0x0041
            java.lang.String r8 = "timeShiftBufferDepth"
            long r16 = c(r0, r8, r1)
            goto L_0x0043
        L_0x0041:
            r16 = r1
        L_0x0043:
            if (r13 == 0) goto L_0x004c
            java.lang.String r8 = "suggestedPresentationDelay"
            long r18 = c(r0, r8, r1)
            goto L_0x004e
        L_0x004c:
            r18 = r1
        L_0x004e:
            java.lang.String r8 = "publishTime"
            long r20 = b((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r8, (long) r1)
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            if (r13 == 0) goto L_0x005e
            r22 = r1
            goto L_0x0060
        L_0x005e:
            r22 = 0
        L_0x0060:
            r25 = r7
            r26 = r25
            r27 = r26
            r1 = r22
            r22 = 0
            r7 = r34
        L_0x006c:
            r33.next()
            java.lang.String r11 = "BaseURL"
            boolean r11 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r11)
            if (r11 == 0) goto L_0x008a
            if (r12 != 0) goto L_0x0082
            java.lang.String r7 = b((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r7)
            r30 = r14
            r12 = 1
            goto L_0x0130
        L_0x0082:
            r28 = r1
            r34 = r12
            r30 = r14
            goto L_0x012c
        L_0x008a:
            java.lang.String r11 = "ProgramInformation"
            boolean r11 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r11)
            if (r11 == 0) goto L_0x009c
            com.google.android.exoplayer2.source.dash.manifest.ProgramInformation r11 = r32.g(r33)
            r25 = r11
        L_0x0098:
            r30 = r14
            goto L_0x0130
        L_0x009c:
            java.lang.String r11 = "UTCTiming"
            boolean r11 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r11)
            if (r11 == 0) goto L_0x00ab
            com.google.android.exoplayer2.source.dash.manifest.UtcTimingElement r11 = r32.k(r33)
            r26 = r11
            goto L_0x0098
        L_0x00ab:
            java.lang.String r11 = "Location"
            boolean r11 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r11)
            if (r11 == 0) goto L_0x00be
            java.lang.String r11 = r33.nextText()
            android.net.Uri r11 = android.net.Uri.parse(r11)
            r27 = r11
            goto L_0x0098
        L_0x00be:
            java.lang.String r11 = "Period"
            boolean r11 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r11)
            if (r11 == 0) goto L_0x0123
            if (r22 != 0) goto L_0x0123
            r11 = r32
            r34 = r12
            android.util.Pair r12 = r11.a((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r7, (long) r1)
            r28 = r1
            java.lang.Object r1 = r12.first
            com.google.android.exoplayer2.source.dash.manifest.Period r1 = (com.google.android.exoplayer2.source.dash.manifest.Period) r1
            r30 = r14
            long r14 = r1.b
            r23 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r2 = (r14 > r23 ? 1 : (r14 == r23 ? 0 : -1))
            if (r2 != 0) goto L_0x0103
            if (r13 == 0) goto L_0x00e8
            r22 = 1
            goto L_0x012c
        L_0x00e8:
            com.google.android.exoplayer2.ParserException r0 = new com.google.android.exoplayer2.ParserException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unable to determine start of period "
            r1.append(r2)
            int r2 = r8.size()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>((java.lang.String) r1)
            throw r0
        L_0x0103:
            java.lang.Object r2 = r12.second
            java.lang.Long r2 = (java.lang.Long) r2
            long r14 = r2.longValue()
            r23 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r2 = (r14 > r23 ? 1 : (r14 == r23 ? 0 : -1))
            if (r2 != 0) goto L_0x011a
            r11 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            goto L_0x011d
        L_0x011a:
            long r11 = r1.b
            long r11 = r11 + r14
        L_0x011d:
            r8.add(r1)
            r28 = r11
            goto L_0x012c
        L_0x0123:
            r28 = r1
            r34 = r12
            r30 = r14
            l(r33)
        L_0x012c:
            r12 = r34
            r1 = r28
        L_0x0130:
            java.lang.String r11 = "MPD"
            boolean r11 = com.google.android.exoplayer2.util.XmlPullParserUtil.c(r0, r11)
            r14 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r11 == 0) goto L_0x0179
            int r0 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
            if (r0 != 0) goto L_0x0151
            int r0 = (r1 > r14 ? 1 : (r1 == r14 ? 0 : -1))
            if (r0 == 0) goto L_0x0146
            goto L_0x0152
        L_0x0146:
            if (r13 == 0) goto L_0x0149
            goto L_0x0151
        L_0x0149:
            com.google.android.exoplayer2.ParserException r0 = new com.google.android.exoplayer2.ParserException
            java.lang.String r1 = "Unable to determine duration of static manifest."
            r0.<init>((java.lang.String) r1)
            throw r0
        L_0x0151:
            r1 = r3
        L_0x0152:
            boolean r0 = r8.isEmpty()
            if (r0 != 0) goto L_0x0171
            r4 = r32
            r23 = r8
            r7 = r1
            r11 = r13
            r12 = r30
            r14 = r16
            r16 = r18
            r18 = r20
            r20 = r25
            r21 = r26
            r22 = r27
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r0 = r4.a(r5, r7, r9, r11, r12, r14, r16, r18, r20, r21, r22, r23)
            return r0
        L_0x0171:
            com.google.android.exoplayer2.ParserException r0 = new com.google.android.exoplayer2.ParserException
            java.lang.String r1 = "No periods found."
            r0.<init>((java.lang.String) r1)
            throw r0
        L_0x0179:
            r14 = r30
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.a(org.xmlpull.v1.XmlPullParser, java.lang.String):com.google.android.exoplayer2.source.dash.manifest.DashManifest");
    }

    protected static long b(XmlPullParser xmlPullParser, String str, long j) throws ParserException {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
        if (attributeValue == null) {
            return j;
        }
        return Util.h(attributeValue);
    }

    protected static String b(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        xmlPullParser.next();
        return UriUtil.a(str, xmlPullParser.getText());
    }

    protected static String b(XmlPullParser xmlPullParser, String str, String str2) {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
        return attributeValue == null ? str2 : attributeValue;
    }

    private static String c(String str, String str2) {
        if (MimeTypes.j(str)) {
            return MimeTypes.a(str2);
        }
        if (MimeTypes.l(str)) {
            return MimeTypes.i(str2);
        }
        if (a(str)) {
            return str;
        }
        if ("application/mp4".equals(str)) {
            if (str2 != null) {
                if (str2.startsWith("stpp")) {
                    return "application/ttml+xml";
                }
                if (str2.startsWith("wvtt")) {
                    return "application/x-mp4-vtt";
                }
            }
        } else if ("application/x-rawcc".equals(str) && str2 != null) {
            if (str2.contains("cea708")) {
                return "application/cea-708";
            }
            if (str2.contains("eia608") || str2.contains("cea608")) {
                return "application/cea-608";
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public DashManifest a(long j, long j2, long j3, boolean z, long j4, long j5, long j6, long j7, ProgramInformation programInformation, UtcTimingElement utcTimingElement, Uri uri, List<Period> list) {
        return new DashManifest(j, j2, j3, z, j4, j5, j6, j7, programInformation, utcTimingElement, uri, list);
    }

    /* access modifiers changed from: protected */
    public UtcTimingElement a(String str, String str2) {
        return new UtcTimingElement(str, str2);
    }

    /* access modifiers changed from: protected */
    public Pair<Period, Long> a(XmlPullParser xmlPullParser, String str, long j) throws XmlPullParserException, IOException {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, "id");
        long c2 = c(xmlPullParser, ViewProps.START, j);
        long c3 = c(xmlPullParser, "duration", -9223372036854775807L);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        boolean z = false;
        SegmentBase segmentBase = null;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser, "BaseURL")) {
                if (!z) {
                    str = b(xmlPullParser, str);
                    z = true;
                }
            } else if (XmlPullParserUtil.d(xmlPullParser, "AdaptationSet")) {
                arrayList.add(a(xmlPullParser, str, segmentBase));
            } else if (XmlPullParserUtil.d(xmlPullParser, "EventStream")) {
                arrayList2.add(e(xmlPullParser));
            } else if (XmlPullParserUtil.d(xmlPullParser, "SegmentBase")) {
                segmentBase = a(xmlPullParser, (SegmentBase.SingleSegmentBase) null);
            } else if (XmlPullParserUtil.d(xmlPullParser, "SegmentList")) {
                segmentBase = a(xmlPullParser, (SegmentBase.SegmentList) null);
            } else if (XmlPullParserUtil.d(xmlPullParser, "SegmentTemplate")) {
                segmentBase = a(xmlPullParser, (SegmentBase.SegmentTemplate) null);
            } else {
                l(xmlPullParser);
            }
        } while (!XmlPullParserUtil.c(xmlPullParser, "Period"));
        return Pair.create(a(attributeValue, c2, arrayList, arrayList2), Long.valueOf(c3));
    }

    protected static Descriptor c(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        String b2 = b(xmlPullParser, "schemeIdUri", "");
        String b3 = b(xmlPullParser, "value", (String) null);
        String b4 = b(xmlPullParser, "id", (String) null);
        do {
            xmlPullParser.next();
        } while (!XmlPullParserUtil.c(xmlPullParser, str));
        return new Descriptor(b2, b3, b4);
    }

    protected static String c(List<Descriptor> list) {
        for (int i = 0; i < list.size(); i++) {
            Descriptor descriptor = list.get(i);
            if ("tag:dolby.com,2014:dash:DolbyDigitalPlusExtensionType:2014".equals(descriptor.f3462a) && "ec+3".equals(descriptor.b)) {
                return "audio/eac3-joc";
            }
        }
        return "audio/eac3";
    }

    protected static long c(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
        if (attributeValue == null) {
            return j;
        }
        return Util.i(attributeValue);
    }

    /* access modifiers changed from: protected */
    public Period a(String str, long j, List<AdaptationSet> list, List<EventStream> list2) {
        return new Period(str, j, list, list2);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x023c A[LOOP:0: B:1:0x0069->B:59:0x023c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0203 A[EDGE_INSN: B:60:0x0203->B:53:0x0203 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.exoplayer2.source.dash.manifest.AdaptationSet a(org.xmlpull.v1.XmlPullParser r41, java.lang.String r42, com.google.android.exoplayer2.source.dash.manifest.SegmentBase r43) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r40 = this;
            r15 = r40
            r14 = r41
            r0 = -1
            java.lang.String r1 = "id"
            int r16 = a((org.xmlpull.v1.XmlPullParser) r14, (java.lang.String) r1, (int) r0)
            int r1 = r40.d(r41)
            r13 = 0
            java.lang.String r2 = "mimeType"
            java.lang.String r17 = r14.getAttributeValue(r13, r2)
            java.lang.String r2 = "codecs"
            java.lang.String r18 = r14.getAttributeValue(r13, r2)
            java.lang.String r2 = "width"
            int r19 = a((org.xmlpull.v1.XmlPullParser) r14, (java.lang.String) r2, (int) r0)
            java.lang.String r2 = "height"
            int r20 = a((org.xmlpull.v1.XmlPullParser) r14, (java.lang.String) r2, (int) r0)
            r2 = -1082130432(0xffffffffbf800000, float:-1.0)
            float r21 = a((org.xmlpull.v1.XmlPullParser) r14, (float) r2)
            java.lang.String r2 = "audioSamplingRate"
            int r22 = a((org.xmlpull.v1.XmlPullParser) r14, (java.lang.String) r2, (int) r0)
            java.lang.String r12 = "lang"
            java.lang.String r2 = r14.getAttributeValue(r13, r12)
            java.lang.String r3 = "label"
            java.lang.String r23 = r14.getAttributeValue(r13, r3)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            r24 = 0
            r6 = r42
            r28 = r43
            r4 = r1
            r5 = r2
            r29 = r13
            r25 = 0
            r26 = 0
            r27 = -1
        L_0x0069:
            r41.next()
            java.lang.String r0 = "BaseURL"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r0 == 0) goto L_0x00a3
            if (r25 != 0) goto L_0x008f
            java.lang.String r0 = b((org.xmlpull.v1.XmlPullParser) r14, (java.lang.String) r6)
            r1 = 1
            r32 = r0
            r6 = r7
            r34 = r8
            r35 = r9
            r7 = r10
            r37 = r11
            r38 = r12
            r39 = r13
            r1 = r14
            r25 = 1
        L_0x008c:
            r8 = r4
            goto L_0x01fb
        L_0x008f:
            r2 = r4
            r31 = r5
            r32 = r6
            r6 = r7
            r34 = r8
            r35 = r9
            r7 = r10
            r37 = r11
            r38 = r12
            r39 = r13
            r1 = r14
            goto L_0x01f8
        L_0x00a3:
            java.lang.String r0 = "ContentProtection"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r0 == 0) goto L_0x00bf
            android.util.Pair r0 = r40.c((org.xmlpull.v1.XmlPullParser) r41)
            java.lang.Object r1 = r0.first
            if (r1 == 0) goto L_0x00b7
            r29 = r1
            java.lang.String r29 = (java.lang.String) r29
        L_0x00b7:
            java.lang.Object r0 = r0.second
            if (r0 == 0) goto L_0x00f7
            r11.add(r0)
            goto L_0x00f7
        L_0x00bf:
            java.lang.String r0 = "ContentComponent"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r0 == 0) goto L_0x00e9
            java.lang.String r0 = r14.getAttributeValue(r13, r12)
            java.lang.String r5 = b((java.lang.String) r5, (java.lang.String) r0)
            int r0 = r40.d(r41)
            int r0 = a((int) r4, (int) r0)
            r32 = r6
            r6 = r7
            r34 = r8
            r35 = r9
            r7 = r10
            r37 = r11
            r38 = r12
            r39 = r13
            r1 = r14
            r8 = r0
            goto L_0x01fb
        L_0x00e9:
            java.lang.String r0 = "Role"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r0 == 0) goto L_0x0107
            int r0 = r40.h(r41)
            r26 = r26 | r0
        L_0x00f7:
            r32 = r6
            r6 = r7
            r34 = r8
            r35 = r9
            r7 = r10
            r37 = r11
            r38 = r12
            r39 = r13
            r1 = r14
            goto L_0x008c
        L_0x0107:
            java.lang.String r0 = "AudioChannelConfiguration"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r0 == 0) goto L_0x0114
            int r27 = r40.b((org.xmlpull.v1.XmlPullParser) r41)
            goto L_0x00f7
        L_0x0114:
            java.lang.String r0 = "Accessibility"
            boolean r1 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r1 == 0) goto L_0x0125
            com.google.android.exoplayer2.source.dash.manifest.Descriptor r0 = c((org.xmlpull.v1.XmlPullParser) r14, (java.lang.String) r0)
            r9.add(r0)
            goto L_0x008f
        L_0x0125:
            java.lang.String r0 = "SupplementalProperty"
            boolean r1 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r1 == 0) goto L_0x0136
            com.google.android.exoplayer2.source.dash.manifest.Descriptor r0 = c((org.xmlpull.v1.XmlPullParser) r14, (java.lang.String) r0)
            r8.add(r0)
            goto L_0x008f
        L_0x0136:
            java.lang.String r0 = "Representation"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r14, r0)
            if (r0 == 0) goto L_0x018d
            r0 = r40
            r1 = r41
            r2 = r6
            r3 = r23
            r30 = r4
            r4 = r17
            r31 = r5
            r5 = r18
            r32 = r6
            r6 = r19
            r33 = r7
            r7 = r20
            r34 = r8
            r8 = r21
            r35 = r9
            r9 = r27
            r36 = r10
            r10 = r22
            r37 = r11
            r11 = r31
            r38 = r12
            r12 = r26
            r39 = r13
            r13 = r35
            r14 = r28
            com.google.android.exoplayer2.source.dash.manifest.DashManifestParser$RepresentationInfo r0 = r0.a((org.xmlpull.v1.XmlPullParser) r1, (java.lang.String) r2, (java.lang.String) r3, (java.lang.String) r4, (java.lang.String) r5, (int) r6, (int) r7, (float) r8, (int) r9, (int) r10, (java.lang.String) r11, (int) r12, (java.util.List<com.google.android.exoplayer2.source.dash.manifest.Descriptor>) r13, (com.google.android.exoplayer2.source.dash.manifest.SegmentBase) r14)
            com.google.android.exoplayer2.Format r1 = r0.f3461a
            int r1 = r15.a((com.google.android.exoplayer2.Format) r1)
            r2 = r30
            int r1 = a((int) r2, (int) r1)
            r6 = r33
            r6.add(r0)
            r8 = r1
            r5 = r31
            r7 = r36
            r1 = r41
            goto L_0x01fb
        L_0x018d:
            r2 = r4
            r31 = r5
            r32 = r6
            r6 = r7
            r34 = r8
            r35 = r9
            r36 = r10
            r37 = r11
            r38 = r12
            r39 = r13
            java.lang.String r0 = "SegmentBase"
            r1 = r41
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r1, r0)
            if (r0 == 0) goto L_0x01b9
            r0 = r28
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SingleSegmentBase r0 = (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SingleSegmentBase) r0
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SingleSegmentBase r0 = r15.a((org.xmlpull.v1.XmlPullParser) r1, (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SingleSegmentBase) r0)
        L_0x01b1:
            r28 = r0
            r8 = r2
            r5 = r31
            r7 = r36
            goto L_0x01fb
        L_0x01b9:
            java.lang.String r0 = "SegmentList"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r1, r0)
            if (r0 == 0) goto L_0x01ca
            r0 = r28
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentList r0 = (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentList) r0
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentList r0 = r15.a((org.xmlpull.v1.XmlPullParser) r1, (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentList) r0)
            goto L_0x01b1
        L_0x01ca:
            java.lang.String r0 = "SegmentTemplate"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r1, r0)
            if (r0 == 0) goto L_0x01db
            r0 = r28
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentTemplate r0 = (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentTemplate) r0
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentTemplate r0 = r15.a((org.xmlpull.v1.XmlPullParser) r1, (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentTemplate) r0)
            goto L_0x01b1
        L_0x01db:
            java.lang.String r0 = "InbandEventStream"
            boolean r3 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r1, r0)
            if (r3 == 0) goto L_0x01ed
            com.google.android.exoplayer2.source.dash.manifest.Descriptor r0 = c((org.xmlpull.v1.XmlPullParser) r1, (java.lang.String) r0)
            r7 = r36
            r7.add(r0)
            goto L_0x01f8
        L_0x01ed:
            r7 = r36
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.b(r41)
            if (r0 == 0) goto L_0x01f8
            r40.a((org.xmlpull.v1.XmlPullParser) r41)
        L_0x01f8:
            r8 = r2
            r5 = r31
        L_0x01fb:
            java.lang.String r0 = "AdaptationSet"
            boolean r0 = com.google.android.exoplayer2.util.XmlPullParserUtil.c(r1, r0)
            if (r0 == 0) goto L_0x023c
            java.util.ArrayList r9 = new java.util.ArrayList
            int r0 = r6.size()
            r9.<init>(r0)
            r10 = 0
        L_0x020d:
            int r0 = r6.size()
            if (r10 >= r0) goto L_0x022d
            java.lang.Object r0 = r6.get(r10)
            r1 = r0
            com.google.android.exoplayer2.source.dash.manifest.DashManifestParser$RepresentationInfo r1 = (com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.RepresentationInfo) r1
            java.lang.String r2 = r15.f3460a
            r0 = r40
            r3 = r29
            r4 = r37
            r5 = r7
            com.google.android.exoplayer2.source.dash.manifest.Representation r0 = r0.a((com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.RepresentationInfo) r1, (java.lang.String) r2, (java.lang.String) r3, (java.util.ArrayList<com.google.android.exoplayer2.drm.DrmInitData.SchemeData>) r4, (java.util.ArrayList<com.google.android.exoplayer2.source.dash.manifest.Descriptor>) r5)
            r9.add(r0)
            int r10 = r10 + 1
            goto L_0x020d
        L_0x022d:
            r0 = r40
            r1 = r16
            r2 = r8
            r3 = r9
            r4 = r35
            r5 = r34
            com.google.android.exoplayer2.source.dash.manifest.AdaptationSet r0 = r0.a((int) r1, (int) r2, (java.util.List<com.google.android.exoplayer2.source.dash.manifest.Representation>) r3, (java.util.List<com.google.android.exoplayer2.source.dash.manifest.Descriptor>) r4, (java.util.List<com.google.android.exoplayer2.source.dash.manifest.Descriptor>) r5)
            return r0
        L_0x023c:
            r14 = r1
            r10 = r7
            r4 = r8
            r8 = r34
            r9 = r35
            r11 = r37
            r12 = r38
            r13 = r39
            r7 = r6
            r6 = r32
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.a(org.xmlpull.v1.XmlPullParser, java.lang.String, com.google.android.exoplayer2.source.dash.manifest.SegmentBase):com.google.android.exoplayer2.source.dash.manifest.AdaptationSet");
    }

    /* access modifiers changed from: protected */
    public AdaptationSet a(int i, int i2, List<Representation> list, List<Descriptor> list2, List<Descriptor> list3) {
        return new AdaptationSet(i, i2, list, list2, list3);
    }

    /* access modifiers changed from: protected */
    public int a(Format format) {
        String str = format.g;
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        if (MimeTypes.l(str)) {
            return 2;
        }
        if (MimeTypes.j(str)) {
            return 1;
        }
        if (a(str)) {
            return 3;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void a(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        l(xmlPullParser);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0146 A[LOOP:0: B:1:0x0058->B:43:0x0146, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x010a A[EDGE_INSN: B:44:0x010a->B:37:0x010a ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.RepresentationInfo a(org.xmlpull.v1.XmlPullParser r23, java.lang.String r24, java.lang.String r25, java.lang.String r26, java.lang.String r27, int r28, int r29, float r30, int r31, int r32, java.lang.String r33, int r34, java.util.List<com.google.android.exoplayer2.source.dash.manifest.Descriptor> r35, com.google.android.exoplayer2.source.dash.manifest.SegmentBase r36) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r22 = this;
            r15 = r22
            r0 = r23
            r1 = 0
            java.lang.String r2 = "id"
            java.lang.String r2 = r0.getAttributeValue(r1, r2)
            java.lang.String r3 = "bandwidth"
            r4 = -1
            int r9 = a((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r3, (int) r4)
            java.lang.String r3 = "mimeType"
            r4 = r26
            java.lang.String r3 = b((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r3, (java.lang.String) r4)
            java.lang.String r4 = "codecs"
            r5 = r27
            java.lang.String r13 = b((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r4, (java.lang.String) r5)
            java.lang.String r4 = "width"
            r5 = r28
            int r4 = a((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r4, (int) r5)
            java.lang.String r5 = "height"
            r6 = r29
            int r5 = a((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r5, (int) r6)
            r6 = r30
            float r6 = a((org.xmlpull.v1.XmlPullParser) r0, (float) r6)
            java.lang.String r7 = "audioSamplingRate"
            r8 = r32
            int r8 = a((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r7, (int) r8)
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r7 = 0
            r16 = r31
            r10 = r36
            r17 = r1
            r1 = r24
        L_0x0058:
            r23.next()
            r26 = r13
            java.lang.String r13 = "BaseURL"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r13)
            if (r13 == 0) goto L_0x0078
            if (r7 != 0) goto L_0x0074
            java.lang.String r1 = b((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r1)
            r7 = 1
        L_0x006c:
            r13 = r16
            r18 = r17
            r16 = r1
            goto L_0x0100
        L_0x0074:
            r24 = r1
            goto L_0x00fa
        L_0x0078:
            java.lang.String r13 = "AudioChannelConfiguration"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r13)
            if (r13 == 0) goto L_0x008a
            int r13 = r22.b((org.xmlpull.v1.XmlPullParser) r23)
            r16 = r1
            r18 = r17
            goto L_0x0100
        L_0x008a:
            java.lang.String r13 = "SegmentBase"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r13)
            if (r13 == 0) goto L_0x0099
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SingleSegmentBase r10 = (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SingleSegmentBase) r10
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SingleSegmentBase r10 = r15.a((org.xmlpull.v1.XmlPullParser) r0, (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SingleSegmentBase) r10)
            goto L_0x006c
        L_0x0099:
            java.lang.String r13 = "SegmentList"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r13)
            if (r13 == 0) goto L_0x00a8
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentList r10 = (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentList) r10
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentList r10 = r15.a((org.xmlpull.v1.XmlPullParser) r0, (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentList) r10)
            goto L_0x006c
        L_0x00a8:
            java.lang.String r13 = "SegmentTemplate"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r13)
            if (r13 == 0) goto L_0x00b7
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentTemplate r10 = (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentTemplate) r10
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SegmentTemplate r10 = r15.a((org.xmlpull.v1.XmlPullParser) r0, (com.google.android.exoplayer2.source.dash.manifest.SegmentBase.SegmentTemplate) r10)
            goto L_0x006c
        L_0x00b7:
            java.lang.String r13 = "ContentProtection"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r13)
            if (r13 == 0) goto L_0x00d5
            android.util.Pair r13 = r22.c((org.xmlpull.v1.XmlPullParser) r23)
            r24 = r1
            java.lang.Object r1 = r13.first
            if (r1 == 0) goto L_0x00cd
            r17 = r1
            java.lang.String r17 = (java.lang.String) r17
        L_0x00cd:
            java.lang.Object r1 = r13.second
            if (r1 == 0) goto L_0x00fa
            r14.add(r1)
            goto L_0x00fa
        L_0x00d5:
            r24 = r1
            java.lang.String r1 = "InbandEventStream"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r1)
            if (r13 == 0) goto L_0x00e7
            com.google.android.exoplayer2.source.dash.manifest.Descriptor r1 = c((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r1)
            r12.add(r1)
            goto L_0x00fa
        L_0x00e7:
            java.lang.String r1 = "SupplementalProperty"
            boolean r13 = com.google.android.exoplayer2.util.XmlPullParserUtil.d(r0, r1)
            if (r13 == 0) goto L_0x00f7
            com.google.android.exoplayer2.source.dash.manifest.Descriptor r1 = c((org.xmlpull.v1.XmlPullParser) r0, (java.lang.String) r1)
            r11.add(r1)
            goto L_0x00fa
        L_0x00f7:
            l(r23)
        L_0x00fa:
            r13 = r16
            r18 = r17
            r16 = r24
        L_0x0100:
            r17 = r10
            java.lang.String r1 = "Representation"
            boolean r1 = com.google.android.exoplayer2.util.XmlPullParserUtil.c(r0, r1)
            if (r1 == 0) goto L_0x0146
            r0 = r22
            r1 = r2
            r2 = r25
            r7 = r13
            r10 = r33
            r19 = r11
            r11 = r34
            r20 = r12
            r12 = r35
            r13 = r26
            r21 = r14
            r14 = r19
            com.google.android.exoplayer2.Format r0 = r0.a((java.lang.String) r1, (java.lang.String) r2, (java.lang.String) r3, (int) r4, (int) r5, (float) r6, (int) r7, (int) r8, (int) r9, (java.lang.String) r10, (int) r11, (java.util.List<com.google.android.exoplayer2.source.dash.manifest.Descriptor>) r12, (java.lang.String) r13, (java.util.List<com.google.android.exoplayer2.source.dash.manifest.Descriptor>) r14)
            if (r17 == 0) goto L_0x0129
            r1 = r17
            goto L_0x012e
        L_0x0129:
            com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SingleSegmentBase r1 = new com.google.android.exoplayer2.source.dash.manifest.SegmentBase$SingleSegmentBase
            r1.<init>()
        L_0x012e:
            com.google.android.exoplayer2.source.dash.manifest.DashManifestParser$RepresentationInfo r2 = new com.google.android.exoplayer2.source.dash.manifest.DashManifestParser$RepresentationInfo
            r3 = -1
            r23 = r2
            r24 = r0
            r25 = r16
            r26 = r1
            r27 = r18
            r28 = r21
            r29 = r20
            r30 = r3
            r23.<init>(r24, r25, r26, r27, r28, r29, r30)
            return r2
        L_0x0146:
            r1 = r16
            r10 = r17
            r17 = r18
            r16 = r13
            r13 = r26
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.manifest.DashManifestParser.a(org.xmlpull.v1.XmlPullParser, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int, float, int, int, java.lang.String, int, java.util.List, com.google.android.exoplayer2.source.dash.manifest.SegmentBase):com.google.android.exoplayer2.source.dash.manifest.DashManifestParser$RepresentationInfo");
    }

    /* access modifiers changed from: protected */
    public Format a(String str, String str2, String str3, int i, int i2, float f, int i3, int i4, int i5, String str4, int i6, List<Descriptor> list, String str5, List<Descriptor> list2) {
        String str6;
        int i7;
        int b2;
        String str7 = str3;
        String c2 = c(str3, str5);
        if (c2 != null) {
            if ("audio/eac3".equals(c2)) {
                c2 = c(list2);
            }
            str6 = c2;
            if (MimeTypes.l(str6)) {
                return Format.a(str, str2, str3, str6, str5, i5, i, i2, f, (List<byte[]>) null, i6);
            }
            if (MimeTypes.j(str6)) {
                return Format.a(str, str2, str3, str6, str5, i5, i3, i4, (List<byte[]>) null, i6, str4);
            }
            if (a(str6)) {
                if ("application/cea-608".equals(str6)) {
                    b2 = a(list);
                } else if ("application/cea-708".equals(str6)) {
                    b2 = b(list);
                } else {
                    i7 = -1;
                    return Format.a(str, str2, str3, str6, str5, i5, i6, str4, i7);
                }
                i7 = b2;
                return Format.a(str, str2, str3, str6, str5, i5, i6, str4, i7);
            }
        } else {
            str6 = c2;
        }
        return Format.a(str, str2, str3, str6, str5, i5, i6, str4);
    }

    /* access modifiers changed from: protected */
    public Representation a(RepresentationInfo representationInfo, String str, String str2, ArrayList<DrmInitData.SchemeData> arrayList, ArrayList<Descriptor> arrayList2) {
        Format format = representationInfo.f3461a;
        String str3 = representationInfo.d;
        if (str3 != null) {
            str2 = str3;
        }
        ArrayList<DrmInitData.SchemeData> arrayList3 = representationInfo.e;
        arrayList3.addAll(arrayList);
        if (!arrayList3.isEmpty()) {
            a(arrayList3);
            format = format.a(new DrmInitData(str2, (List<DrmInitData.SchemeData>) arrayList3));
        }
        ArrayList<Descriptor> arrayList4 = representationInfo.f;
        arrayList4.addAll(arrayList2);
        return Representation.a(str, representationInfo.g, format, representationInfo.b, representationInfo.c, arrayList4);
    }

    /* access modifiers changed from: protected */
    public SegmentBase.SingleSegmentBase a(XmlPullParser xmlPullParser, SegmentBase.SingleSegmentBase singleSegmentBase) throws XmlPullParserException, IOException {
        long j;
        long j2;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        SegmentBase.SingleSegmentBase singleSegmentBase2 = singleSegmentBase;
        long d2 = d(xmlPullParser2, "timescale", singleSegmentBase2 != null ? singleSegmentBase2.b : 1);
        long j3 = 0;
        long d3 = d(xmlPullParser2, "presentationTimeOffset", singleSegmentBase2 != null ? singleSegmentBase2.c : 0);
        long j4 = singleSegmentBase2 != null ? singleSegmentBase2.d : 0;
        if (singleSegmentBase2 != null) {
            j3 = singleSegmentBase2.e;
        }
        RangedUri rangedUri = null;
        String attributeValue = xmlPullParser2.getAttributeValue((String) null, "indexRange");
        if (attributeValue != null) {
            String[] split = attributeValue.split("-");
            long parseLong = Long.parseLong(split[0]);
            j = (Long.parseLong(split[1]) - parseLong) + 1;
            j2 = parseLong;
        } else {
            j = j3;
            j2 = j4;
        }
        if (singleSegmentBase2 != null) {
            rangedUri = singleSegmentBase2.f3468a;
        }
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser2, "Initialization")) {
                rangedUri = f(xmlPullParser);
            } else {
                l(xmlPullParser);
            }
        } while (!XmlPullParserUtil.c(xmlPullParser2, "SegmentBase"));
        return a(rangedUri, d2, d3, j2, j);
    }

    /* access modifiers changed from: protected */
    public SegmentBase.SingleSegmentBase a(RangedUri rangedUri, long j, long j2, long j3, long j4) {
        return new SegmentBase.SingleSegmentBase(rangedUri, j, j2, j3, j4);
    }

    /* access modifiers changed from: protected */
    public SegmentBase.SegmentList a(XmlPullParser xmlPullParser, SegmentBase.SegmentList segmentList) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        SegmentBase.SegmentList segmentList2 = segmentList;
        long j = 1;
        long d2 = d(xmlPullParser2, "timescale", segmentList2 != null ? segmentList2.b : 1);
        long d3 = d(xmlPullParser2, "presentationTimeOffset", segmentList2 != null ? segmentList2.c : 0);
        long d4 = d(xmlPullParser2, "duration", segmentList2 != null ? segmentList2.e : -9223372036854775807L);
        if (segmentList2 != null) {
            j = segmentList2.d;
        }
        long d5 = d(xmlPullParser2, "startNumber", j);
        List list = null;
        RangedUri rangedUri = null;
        List<SegmentBase.SegmentTimelineElement> list2 = null;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser2, "Initialization")) {
                rangedUri = f(xmlPullParser);
            } else if (XmlPullParserUtil.d(xmlPullParser2, "SegmentTimeline")) {
                list2 = i(xmlPullParser);
            } else if (XmlPullParserUtil.d(xmlPullParser2, "SegmentURL")) {
                if (list == null) {
                    list = new ArrayList();
                }
                list.add(j(xmlPullParser));
            } else {
                l(xmlPullParser);
            }
        } while (!XmlPullParserUtil.c(xmlPullParser2, "SegmentList"));
        if (segmentList2 != null) {
            if (rangedUri == null) {
                rangedUri = segmentList2.f3468a;
            }
            if (list2 == null) {
                list2 = segmentList2.f;
            }
            if (list == null) {
                list = segmentList2.g;
            }
        }
        return a(rangedUri, d2, d3, d5, d4, list2, list);
    }

    /* access modifiers changed from: protected */
    public SegmentBase.SegmentList a(RangedUri rangedUri, long j, long j2, long j3, long j4, List<SegmentBase.SegmentTimelineElement> list, List<RangedUri> list2) {
        return new SegmentBase.SegmentList(rangedUri, j, j2, j3, j4, list, list2);
    }

    /* access modifiers changed from: protected */
    public SegmentBase.SegmentTemplate a(XmlPullParser xmlPullParser, SegmentBase.SegmentTemplate segmentTemplate) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        SegmentBase.SegmentTemplate segmentTemplate2 = segmentTemplate;
        long j = 1;
        long d2 = d(xmlPullParser2, "timescale", segmentTemplate2 != null ? segmentTemplate2.b : 1);
        long d3 = d(xmlPullParser2, "presentationTimeOffset", segmentTemplate2 != null ? segmentTemplate2.c : 0);
        long d4 = d(xmlPullParser2, "duration", segmentTemplate2 != null ? segmentTemplate2.e : -9223372036854775807L);
        if (segmentTemplate2 != null) {
            j = segmentTemplate2.d;
        }
        long d5 = d(xmlPullParser2, "startNumber", j);
        RangedUri rangedUri = null;
        UrlTemplate a2 = a(xmlPullParser2, "media", segmentTemplate2 != null ? segmentTemplate2.h : null);
        UrlTemplate a3 = a(xmlPullParser2, "initialization", segmentTemplate2 != null ? segmentTemplate2.g : null);
        List<SegmentBase.SegmentTimelineElement> list = null;
        do {
            xmlPullParser.next();
            if (XmlPullParserUtil.d(xmlPullParser2, "Initialization")) {
                rangedUri = f(xmlPullParser);
            } else if (XmlPullParserUtil.d(xmlPullParser2, "SegmentTimeline")) {
                list = i(xmlPullParser);
            } else {
                l(xmlPullParser);
            }
        } while (!XmlPullParserUtil.c(xmlPullParser2, "SegmentTemplate"));
        if (segmentTemplate2 != null) {
            if (rangedUri == null) {
                rangedUri = segmentTemplate2.f3468a;
            }
            if (list == null) {
                list = segmentTemplate2.f;
            }
        }
        return a(rangedUri, d2, d3, d5, d4, list, a3, a2);
    }

    /* access modifiers changed from: protected */
    public SegmentBase.SegmentTemplate a(RangedUri rangedUri, long j, long j2, long j3, long j4, List<SegmentBase.SegmentTimelineElement> list, UrlTemplate urlTemplate, UrlTemplate urlTemplate2) {
        return new SegmentBase.SegmentTemplate(rangedUri, j, j2, j3, j4, list, urlTemplate, urlTemplate2);
    }

    /* access modifiers changed from: protected */
    public EventStream a(String str, String str2, long j, long[] jArr, EventMessage[] eventMessageArr) {
        return new EventStream(str, str2, j, jArr, eventMessageArr);
    }

    /* access modifiers changed from: protected */
    public EventMessage a(XmlPullParser xmlPullParser, String str, String str2, long j, ByteArrayOutputStream byteArrayOutputStream) throws IOException, XmlPullParserException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        long d2 = d(xmlPullParser2, "id", 0);
        long d3 = d(xmlPullParser2, "duration", -9223372036854775807L);
        long d4 = d(xmlPullParser2, "presentationTime", 0);
        long c2 = Util.c(d3, 1000, j);
        long c3 = Util.c(d4, 1000000, j);
        String b2 = b(xmlPullParser2, "messageData", (String) null);
        byte[] a2 = a(xmlPullParser2, byteArrayOutputStream);
        if (b2 != null) {
            a2 = Util.d(b2);
        }
        return a(str, str2, d2, c2, a2, c3);
    }

    /* access modifiers changed from: protected */
    public byte[] a(XmlPullParser xmlPullParser, ByteArrayOutputStream byteArrayOutputStream) throws XmlPullParserException, IOException {
        byteArrayOutputStream.reset();
        XmlSerializer newSerializer = Xml.newSerializer();
        newSerializer.setOutput(byteArrayOutputStream, "UTF-8");
        xmlPullParser.nextToken();
        while (!XmlPullParserUtil.c(xmlPullParser, "Event")) {
            switch (xmlPullParser.getEventType()) {
                case 0:
                    newSerializer.startDocument((String) null, false);
                    break;
                case 1:
                    newSerializer.endDocument();
                    break;
                case 2:
                    newSerializer.startTag(xmlPullParser.getNamespace(), xmlPullParser.getName());
                    for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
                        newSerializer.attribute(xmlPullParser.getAttributeNamespace(i), xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
                    }
                    break;
                case 3:
                    newSerializer.endTag(xmlPullParser.getNamespace(), xmlPullParser.getName());
                    break;
                case 4:
                    newSerializer.text(xmlPullParser.getText());
                    break;
                case 5:
                    newSerializer.cdsect(xmlPullParser.getText());
                    break;
                case 6:
                    newSerializer.entityRef(xmlPullParser.getText());
                    break;
                case 7:
                    newSerializer.ignorableWhitespace(xmlPullParser.getText());
                    break;
                case 8:
                    newSerializer.processingInstruction(xmlPullParser.getText());
                    break;
                case 9:
                    newSerializer.comment(xmlPullParser.getText());
                    break;
                case 10:
                    newSerializer.docdecl(xmlPullParser.getText());
                    break;
            }
            xmlPullParser.nextToken();
        }
        newSerializer.flush();
        return byteArrayOutputStream.toByteArray();
    }

    /* access modifiers changed from: protected */
    public EventMessage a(String str, String str2, long j, long j2, byte[] bArr, long j3) {
        return new EventMessage(str, str2, j2, j, bArr, j3);
    }

    /* access modifiers changed from: protected */
    public SegmentBase.SegmentTimelineElement a(long j, long j2) {
        return new SegmentBase.SegmentTimelineElement(j, j2);
    }

    /* access modifiers changed from: protected */
    public UrlTemplate a(XmlPullParser xmlPullParser, String str, UrlTemplate urlTemplate) {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
        return attributeValue != null ? UrlTemplate.a(attributeValue) : urlTemplate;
    }

    /* access modifiers changed from: protected */
    public RangedUri a(XmlPullParser xmlPullParser, String str, String str2) {
        long j;
        long j2;
        String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
        String attributeValue2 = xmlPullParser.getAttributeValue((String) null, str2);
        if (attributeValue2 != null) {
            String[] split = attributeValue2.split("-");
            j2 = Long.parseLong(split[0]);
            if (split.length == 2) {
                j = (Long.parseLong(split[1]) - j2) + 1;
                return a(attributeValue, j2, j);
            }
        } else {
            j2 = 0;
        }
        j = -1;
        return a(attributeValue, j2, j);
    }

    /* access modifiers changed from: protected */
    public RangedUri a(String str, long j, long j2) {
        return new RangedUri(str, j, j2);
    }

    private static void a(ArrayList<DrmInitData.SchemeData> arrayList) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            DrmInitData.SchemeData schemeData = arrayList.get(size);
            if (!schemeData.a()) {
                int i = 0;
                while (true) {
                    if (i >= arrayList.size()) {
                        break;
                    } else if (arrayList.get(i).a(schemeData)) {
                        arrayList.remove(size);
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
    }

    private static boolean a(String str) {
        return MimeTypes.k(str) || "application/ttml+xml".equals(str) || "application/x-mp4-vtt".equals(str) || "application/cea-708".equals(str) || "application/cea-608".equals(str);
    }

    private static int a(int i, int i2) {
        if (i == -1) {
            return i2;
        }
        if (i2 == -1) {
            return i;
        }
        Assertions.b(i == i2);
        return i;
    }

    protected static int a(List<Descriptor> list) {
        String str;
        for (int i = 0; i < list.size(); i++) {
            Descriptor descriptor = list.get(i);
            if ("urn:scte:dash:cc:cea-608:2015".equals(descriptor.f3462a) && (str = descriptor.b) != null) {
                Matcher matcher = d.matcher(str);
                if (matcher.matches()) {
                    return Integer.parseInt(matcher.group(1));
                }
                Log.d("MpdParser", "Unable to parse CEA-608 channel number from: " + descriptor.b);
            }
        }
        return -1;
    }

    protected static float a(XmlPullParser xmlPullParser, float f) {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, "frameRate");
        if (attributeValue == null) {
            return f;
        }
        Matcher matcher = c.matcher(attributeValue);
        if (!matcher.matches()) {
            return f;
        }
        int parseInt = Integer.parseInt(matcher.group(1));
        String group = matcher.group(2);
        return !TextUtils.isEmpty(group) ? ((float) parseInt) / ((float) Integer.parseInt(group)) : (float) parseInt;
    }

    protected static int a(XmlPullParser xmlPullParser, String str, int i) {
        String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
        return attributeValue == null ? i : Integer.parseInt(attributeValue);
    }
}
