package com.google.android.exoplayer2.source.smoothstreaming;

import android.net.Uri;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor;
import com.google.android.exoplayer2.extractor.mp4.Track;
import com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.chunk.BaseMediaChunkIterator;
import com.google.android.exoplayer2.source.chunk.Chunk;
import com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper;
import com.google.android.exoplayer2.source.chunk.ChunkHolder;
import com.google.android.exoplayer2.source.chunk.ContainerMediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import com.google.android.exoplayer2.source.smoothstreaming.SsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifest;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.LoaderErrorThrower;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.List;

public class DefaultSsChunkSource implements SsChunkSource {

    /* renamed from: a  reason: collision with root package name */
    private final LoaderErrorThrower f3503a;
    private final int b;
    private final TrackSelection c;
    private final ChunkExtractorWrapper[] d;
    private final DataSource e;
    private SsManifest f;
    private int g;
    private IOException h;

    public static final class Factory implements SsChunkSource.Factory {

        /* renamed from: a  reason: collision with root package name */
        private final DataSource.Factory f3504a;

        public Factory(DataSource.Factory factory) {
            this.f3504a = factory;
        }

        public SsChunkSource a(LoaderErrorThrower loaderErrorThrower, SsManifest ssManifest, int i, TrackSelection trackSelection, TrackEncryptionBox[] trackEncryptionBoxArr, TransferListener transferListener) {
            DataSource a2 = this.f3504a.a();
            if (transferListener != null) {
                a2.a(transferListener);
            }
            return new DefaultSsChunkSource(loaderErrorThrower, ssManifest, i, trackSelection, a2, trackEncryptionBoxArr);
        }
    }

    private static final class StreamElementIterator extends BaseMediaChunkIterator {
        public StreamElementIterator(SsManifest.StreamElement streamElement, int i, int i2) {
            super((long) i2, (long) (streamElement.k - 1));
        }
    }

    public DefaultSsChunkSource(LoaderErrorThrower loaderErrorThrower, SsManifest ssManifest, int i, TrackSelection trackSelection, DataSource dataSource, TrackEncryptionBox[] trackEncryptionBoxArr) {
        SsManifest ssManifest2 = ssManifest;
        int i2 = i;
        TrackSelection trackSelection2 = trackSelection;
        this.f3503a = loaderErrorThrower;
        this.f = ssManifest2;
        this.b = i2;
        this.c = trackSelection2;
        this.e = dataSource;
        SsManifest.StreamElement streamElement = ssManifest2.f[i2];
        this.d = new ChunkExtractorWrapper[trackSelection.length()];
        int i3 = 0;
        while (i3 < this.d.length) {
            int b2 = trackSelection2.b(i3);
            Format format = streamElement.j[b2];
            int i4 = i3;
            Track track = r7;
            Track track2 = new Track(b2, streamElement.f3510a, streamElement.c, -9223372036854775807L, ssManifest2.g, format, 0, trackEncryptionBoxArr, streamElement.f3510a == 2 ? 4 : 0, (long[]) null, (long[]) null);
            this.d[i4] = new ChunkExtractorWrapper(new FragmentedMp4Extractor(3, (TimestampAdjuster) null, track, (DrmInitData) null), streamElement.f3510a, format);
            i3 = i4 + 1;
        }
    }

    public long a(long j, SeekParameters seekParameters) {
        SsManifest.StreamElement streamElement = this.f.f[this.b];
        int a2 = streamElement.a(j);
        long b2 = streamElement.b(a2);
        return Util.a(j, seekParameters, b2, (b2 >= j || a2 >= streamElement.k + -1) ? b2 : streamElement.b(a2 + 1));
    }

    public void a(Chunk chunk) {
    }

    public void a(SsManifest ssManifest) {
        SsManifest.StreamElement[] streamElementArr = this.f.f;
        int i = this.b;
        SsManifest.StreamElement streamElement = streamElementArr[i];
        int i2 = streamElement.k;
        SsManifest.StreamElement streamElement2 = ssManifest.f[i];
        if (i2 == 0 || streamElement2.k == 0) {
            this.g += i2;
        } else {
            int i3 = i2 - 1;
            long b2 = streamElement.b(i3) + streamElement.a(i3);
            long b3 = streamElement2.b(0);
            if (b2 <= b3) {
                this.g += i2;
            } else {
                this.g += streamElement.a(b3);
            }
        }
        this.f = ssManifest;
    }

    public void a() throws IOException {
        IOException iOException = this.h;
        if (iOException == null) {
            this.f3503a.a();
            return;
        }
        throw iOException;
    }

    public int a(long j, List<? extends MediaChunk> list) {
        if (this.h != null || this.c.length() < 2) {
            return list.size();
        }
        return this.c.a(j, list);
    }

    public final void a(long j, long j2, List<? extends MediaChunk> list, ChunkHolder chunkHolder) {
        int i;
        long j3 = j2;
        ChunkHolder chunkHolder2 = chunkHolder;
        if (this.h == null) {
            SsManifest ssManifest = this.f;
            SsManifest.StreamElement streamElement = ssManifest.f[this.b];
            if (streamElement.k == 0) {
                chunkHolder2.b = !ssManifest.d;
                return;
            }
            if (list.isEmpty()) {
                i = streamElement.a(j3);
                List<? extends MediaChunk> list2 = list;
            } else {
                i = (int) (((MediaChunk) list.get(list.size() - 1)).e() - ((long) this.g));
                if (i < 0) {
                    this.h = new BehindLiveWindowException();
                    return;
                }
            }
            if (i >= streamElement.k) {
                chunkHolder2.b = !this.f.d;
                return;
            }
            long j4 = j3 - j;
            long a2 = a(j);
            MediaChunkIterator[] mediaChunkIteratorArr = new MediaChunkIterator[this.c.length()];
            for (int i2 = 0; i2 < mediaChunkIteratorArr.length; i2++) {
                mediaChunkIteratorArr[i2] = new StreamElementIterator(streamElement, this.c.b(i2), i);
            }
            this.c.a(j, j4, a2, list, mediaChunkIteratorArr);
            long b2 = streamElement.b(i);
            long a3 = b2 + streamElement.a(i);
            if (!list.isEmpty()) {
                j3 = -9223372036854775807L;
            }
            long j5 = j3;
            int i3 = i + this.g;
            int a4 = this.c.a();
            ChunkExtractorWrapper chunkExtractorWrapper = this.d[a4];
            Uri a5 = streamElement.a(this.c.b(a4), i);
            chunkHolder2.f3434a = a(this.c.e(), this.e, a5, (String) null, i3, b2, a3, j5, this.c.f(), this.c.b(), chunkExtractorWrapper);
        }
    }

    public boolean a(Chunk chunk, boolean z, Exception exc, long j) {
        if (z && j != -9223372036854775807L) {
            TrackSelection trackSelection = this.c;
            if (trackSelection.a(trackSelection.a(chunk.c), j)) {
                return true;
            }
        }
        return false;
    }

    private static MediaChunk a(Format format, DataSource dataSource, Uri uri, String str, int i, long j, long j2, long j3, int i2, Object obj, ChunkExtractorWrapper chunkExtractorWrapper) {
        DataSource dataSource2 = dataSource;
        long j4 = j2;
        long j5 = j3;
        int i3 = i2;
        Object obj2 = obj;
        DataSpec dataSpec = r26;
        DataSpec dataSpec2 = new DataSpec(uri, 0, -1, str);
        return new ContainerMediaChunk(dataSource2, dataSpec, format, i3, obj2, j, j4, j5, -9223372036854775807L, (long) i, 1, j, chunkExtractorWrapper);
    }

    private long a(long j) {
        SsManifest ssManifest = this.f;
        if (!ssManifest.d) {
            return -9223372036854775807L;
        }
        SsManifest.StreamElement streamElement = ssManifest.f[this.b];
        int i = streamElement.k - 1;
        return (streamElement.b(i) + streamElement.a(i)) - j;
    }
}
