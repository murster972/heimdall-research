package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.source.SequenceableLoader;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import java.io.IOException;

public interface MediaPeriod extends SequenceableLoader {

    public interface Callback extends SequenceableLoader.Callback<MediaPeriod> {
        void a(MediaPeriod mediaPeriod);
    }

    long a(long j);

    long a(long j, SeekParameters seekParameters);

    long a(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr, boolean[] zArr2, long j);

    void a(long j, boolean z);

    void a(Callback callback, long j);

    long b();

    boolean b(long j);

    long c();

    void c(long j);

    void d() throws IOException;

    TrackGroupArray e();

    long f();
}
