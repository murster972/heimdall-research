package com.google.android.exoplayer2.source;

import android.os.Handler;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.PlayerMessage;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ShuffleOrder;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

public class ConcatenatingMediaSource extends CompositeMediaSource<MediaSourceHolder> implements PlayerMessage.Target {
    private final List<MediaSourceHolder> j;
    private final List<MediaSourceHolder> k;
    private final Map<MediaPeriod, MediaSourceHolder> l;
    private final Map<Object, MediaSourceHolder> m;
    private final List<Runnable> n;
    private final boolean o;
    private final boolean p;
    private final Timeline.Window q;
    private final Timeline.Period r;
    private ExoPlayer s;
    private Handler t;
    private boolean u;
    private ShuffleOrder v;
    private int w;
    private int x;

    private static final class ConcatenatedTimeline extends AbstractConcatenatedTimeline {
        private final int e;
        private final int f;
        private final int[] g;
        private final int[] h;
        private final Timeline[] i;
        private final Object[] j;
        private final HashMap<Object, Integer> k = new HashMap<>();

        public ConcatenatedTimeline(Collection<MediaSourceHolder> collection, int i2, int i3, ShuffleOrder shuffleOrder, boolean z) {
            super(z, shuffleOrder);
            this.e = i2;
            this.f = i3;
            int size = collection.size();
            this.g = new int[size];
            this.h = new int[size];
            this.i = new Timeline[size];
            this.j = new Object[size];
            int i4 = 0;
            for (MediaSourceHolder next : collection) {
                this.i[i4] = next.c;
                this.g[i4] = next.f;
                this.h[i4] = next.e;
                Object[] objArr = this.j;
                objArr[i4] = next.b;
                this.k.put(objArr[i4], Integer.valueOf(i4));
                i4++;
            }
        }

        public int a() {
            return this.f;
        }

        /* access modifiers changed from: protected */
        public int b(int i2) {
            return Util.a(this.g, i2 + 1, false, false);
        }

        /* access modifiers changed from: protected */
        public int c(int i2) {
            return Util.a(this.h, i2 + 1, false, false);
        }

        /* access modifiers changed from: protected */
        public Object d(int i2) {
            return this.j[i2];
        }

        /* access modifiers changed from: protected */
        public int e(int i2) {
            return this.g[i2];
        }

        /* access modifiers changed from: protected */
        public int f(int i2) {
            return this.h[i2];
        }

        /* access modifiers changed from: protected */
        public Timeline g(int i2) {
            return this.i[i2];
        }

        /* access modifiers changed from: protected */
        public int b(Object obj) {
            Integer num = this.k.get(obj);
            if (num == null) {
                return -1;
            }
            return num.intValue();
        }

        public int b() {
            return this.e;
        }
    }

    private static final class DeferredTimeline extends ForwardingTimeline {
        /* access modifiers changed from: private */
        public static final Object d = new Object();
        private static final DummyTimeline e = new DummyTimeline();
        /* access modifiers changed from: private */
        public final Object c;

        public DeferredTimeline() {
            this(e, d);
        }

        public Timeline d() {
            return this.b;
        }

        private DeferredTimeline(Timeline timeline, Object obj) {
            super(timeline);
            this.c = obj;
        }

        public static DeferredTimeline a(Timeline timeline, Object obj) {
            return new DeferredTimeline(timeline, obj);
        }

        public DeferredTimeline a(Timeline timeline) {
            return new DeferredTimeline(timeline, this.c);
        }

        public Timeline.Period a(int i, Timeline.Period period, boolean z) {
            this.b.a(i, period, z);
            if (Util.a(period.f3176a, this.c)) {
                period.f3176a = d;
            }
            return period;
        }

        public int a(Object obj) {
            Timeline timeline = this.b;
            if (d.equals(obj)) {
                obj = this.c;
            }
            return timeline.a(obj);
        }

        public Object a(int i) {
            Object a2 = this.b.a(i);
            return Util.a(a2, this.c) ? d : a2;
        }
    }

    private static final class DummyMediaSource extends BaseMediaSource {
        private DummyMediaSource() {
        }

        public MediaPeriod a(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
            throw new UnsupportedOperationException();
        }

        /* access modifiers changed from: protected */
        public void a(ExoPlayer exoPlayer, boolean z, TransferListener transferListener) {
        }

        public void a(MediaPeriod mediaPeriod) {
        }

        public void b() throws IOException {
        }

        /* access modifiers changed from: protected */
        public void h() {
        }
    }

    private static final class DummyTimeline extends Timeline {
        private DummyTimeline() {
        }

        public int a() {
            return 1;
        }

        public Timeline.Window a(int i, Timeline.Window window, boolean z, long j) {
            return window.a((Object) null, -9223372036854775807L, -9223372036854775807L, false, true, 0, -9223372036854775807L, 0, 0, 0);
        }

        public int b() {
            return 1;
        }

        public Timeline.Period a(int i, Timeline.Period period, boolean z) {
            return period.a(0, DeferredTimeline.d, 0, -9223372036854775807L, 0);
        }

        public int a(Object obj) {
            return obj == DeferredTimeline.d ? 0 : -1;
        }

        public Object a(int i) {
            return DeferredTimeline.d;
        }
    }

    private static final class MessageData<T> {

        /* renamed from: a  reason: collision with root package name */
        public final int f3405a;
        public final T b;
        public final Runnable c;

        public MessageData(int i, T t, Runnable runnable) {
            this.f3405a = i;
            this.c = runnable;
            this.b = t;
        }
    }

    public ConcatenatingMediaSource(MediaSource... mediaSourceArr) {
        this(false, mediaSourceArr);
    }

    private void k() {
        this.u = false;
        List emptyList = this.n.isEmpty() ? Collections.emptyList() : new ArrayList(this.n);
        this.n.clear();
        a((Timeline) new ConcatenatedTimeline(this.k, this.w, this.x, this.v, this.o), (Object) null);
        if (!emptyList.isEmpty()) {
            ExoPlayer exoPlayer = this.s;
            Assertions.a(exoPlayer);
            PlayerMessage a2 = exoPlayer.a(this);
            a2.a(5);
            a2.a((Object) emptyList);
            a2.k();
        }
    }

    public void b() throws IOException {
    }

    public final void h() {
        super.h();
        this.k.clear();
        this.m.clear();
        this.s = null;
        this.t = null;
        this.v = this.v.b();
        this.w = 0;
        this.x = 0;
    }

    public ConcatenatingMediaSource(boolean z, MediaSource... mediaSourceArr) {
        this(z, new ShuffleOrder.DefaultShuffleOrder(0), mediaSourceArr);
    }

    private static Object b(Object obj) {
        return AbstractConcatenatedTimeline.d(obj);
    }

    public ConcatenatingMediaSource(boolean z, ShuffleOrder shuffleOrder, MediaSource... mediaSourceArr) {
        this(z, false, shuffleOrder, mediaSourceArr);
    }

    private static Object b(MediaSourceHolder mediaSourceHolder, Object obj) {
        if (mediaSourceHolder.c.c.equals(obj)) {
            obj = DeferredTimeline.d;
        }
        return AbstractConcatenatedTimeline.a(mediaSourceHolder.b, obj);
    }

    public final synchronized void a(Collection<MediaSource> collection) {
        a(this.j.size(), collection, (Runnable) null);
    }

    public ConcatenatingMediaSource(boolean z, boolean z2, ShuffleOrder shuffleOrder, MediaSource... mediaSourceArr) {
        for (MediaSource a2 : mediaSourceArr) {
            Assertions.a(a2);
        }
        this.v = shuffleOrder.getLength() > 0 ? shuffleOrder.b() : shuffleOrder;
        this.l = new IdentityHashMap();
        this.m = new HashMap();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.n = new ArrayList();
        this.o = z;
        this.p = z2;
        this.q = new Timeline.Window();
        this.r = new Timeline.Period();
        a((Collection<MediaSource>) Arrays.asList(mediaSourceArr));
    }

    public final synchronized void a(int i, Collection<MediaSource> collection, Runnable runnable) {
        for (MediaSource a2 : collection) {
            Assertions.a(a2);
        }
        ArrayList arrayList = new ArrayList(collection.size());
        for (MediaSource mediaSourceHolder : collection) {
            arrayList.add(new MediaSourceHolder(mediaSourceHolder));
        }
        this.j.addAll(i, arrayList);
        if (this.s != null && !collection.isEmpty()) {
            PlayerMessage a3 = this.s.a(this);
            a3.a(0);
            a3.a((Object) new MessageData(i, arrayList, runnable));
            a3.k();
        } else if (runnable != null) {
            runnable.run();
        }
    }

    static final class MediaSourceHolder implements Comparable<MediaSourceHolder> {

        /* renamed from: a  reason: collision with root package name */
        public final MediaSource f3404a;
        public final Object b = new Object();
        public DeferredTimeline c = new DeferredTimeline();
        public int d;
        public int e;
        public int f;
        public boolean g;
        public boolean h;
        public boolean i;
        public List<DeferredMediaPeriod> j = new ArrayList();

        public MediaSourceHolder(MediaSource mediaSource) {
            this.f3404a = mediaSource;
        }

        public void a(int i2, int i3, int i4) {
            this.d = i2;
            this.e = i3;
            this.f = i4;
            this.g = false;
            this.h = false;
            this.i = false;
            this.j.clear();
        }

        /* renamed from: a */
        public int compareTo(MediaSourceHolder mediaSourceHolder) {
            return this.f - mediaSourceHolder.f;
        }
    }

    public final synchronized void a(ExoPlayer exoPlayer, boolean z, TransferListener transferListener) {
        super.a(exoPlayer, z, transferListener);
        this.s = exoPlayer;
        this.t = new Handler(exoPlayer.j());
        if (this.j.isEmpty()) {
            k();
        } else {
            this.v = this.v.b(0, this.j.size());
            a(0, (Collection<MediaSourceHolder>) this.j);
            a((Runnable) null);
        }
    }

    public final MediaPeriod a(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        MediaSourceHolder mediaSourceHolder = this.m.get(b(mediaPeriodId.f3414a));
        if (mediaSourceHolder == null) {
            mediaSourceHolder = new MediaSourceHolder(new DummyMediaSource());
            mediaSourceHolder.g = true;
        }
        DeferredMediaPeriod deferredMediaPeriod = new DeferredMediaPeriod(mediaSourceHolder.f3404a, mediaPeriodId, allocator);
        this.l.put(deferredMediaPeriod, mediaSourceHolder);
        mediaSourceHolder.j.add(deferredMediaPeriod);
        if (!mediaSourceHolder.g) {
            mediaSourceHolder.g = true;
            a(mediaSourceHolder, mediaSourceHolder.f3404a);
        } else if (mediaSourceHolder.h) {
            deferredMediaPeriod.a(mediaPeriodId.a(a(mediaSourceHolder, mediaPeriodId.f3414a)));
        }
        return deferredMediaPeriod;
    }

    public final void a(MediaPeriod mediaPeriod) {
        MediaSourceHolder remove = this.l.remove(mediaPeriod);
        Assertions.a(remove);
        MediaSourceHolder mediaSourceHolder = remove;
        ((DeferredMediaPeriod) mediaPeriod).g();
        mediaSourceHolder.j.remove(mediaPeriod);
        a(mediaSourceHolder);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void b(MediaSourceHolder mediaSourceHolder, MediaSource mediaSource, Timeline timeline, Object obj) {
        a(mediaSourceHolder, timeline);
    }

    /* access modifiers changed from: protected */
    public MediaSource.MediaPeriodId a(MediaSourceHolder mediaSourceHolder, MediaSource.MediaPeriodId mediaPeriodId) {
        for (int i = 0; i < mediaSourceHolder.j.size(); i++) {
            if (mediaSourceHolder.j.get(i).b.d == mediaPeriodId.d) {
                return mediaPeriodId.a(b(mediaSourceHolder, mediaPeriodId.f3414a));
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public int a(MediaSourceHolder mediaSourceHolder, int i) {
        return i + mediaSourceHolder.e;
    }

    public final void a(int i, Object obj) throws ExoPlaybackException {
        if (this.s != null) {
            if (i == 0) {
                Util.a(obj);
                MessageData messageData = (MessageData) obj;
                this.v = this.v.b(messageData.f3405a, ((Collection) messageData.b).size());
                a(messageData.f3405a, (Collection<MediaSourceHolder>) (Collection) messageData.b);
                a(messageData.c);
            } else if (i == 1) {
                Util.a(obj);
                MessageData messageData2 = (MessageData) obj;
                int i2 = messageData2.f3405a;
                int intValue = ((Integer) messageData2.b).intValue();
                if (i2 == 0 && intValue == this.v.getLength()) {
                    this.v = this.v.b();
                } else {
                    this.v = this.v.a(i2, intValue);
                }
                for (int i3 = intValue - 1; i3 >= i2; i3--) {
                    a(i3);
                }
                a(messageData2.c);
            } else if (i == 2) {
                Util.a(obj);
                MessageData messageData3 = (MessageData) obj;
                ShuffleOrder shuffleOrder = this.v;
                int i4 = messageData3.f3405a;
                this.v = shuffleOrder.a(i4, i4 + 1);
                this.v = this.v.b(((Integer) messageData3.b).intValue(), 1);
                a(messageData3.f3405a, ((Integer) messageData3.b).intValue());
                a(messageData3.c);
            } else if (i == 3) {
                Util.a(obj);
                MessageData messageData4 = (MessageData) obj;
                this.v = (ShuffleOrder) messageData4.b;
                a(messageData4.c);
            } else if (i == 4) {
                k();
            } else if (i == 5) {
                Util.a(obj);
                List list = (List) obj;
                Handler handler = this.t;
                Assertions.a(handler);
                Handler handler2 = handler;
                for (int i5 = 0; i5 < list.size(); i5++) {
                    handler2.post((Runnable) list.get(i5));
                }
            } else {
                throw new IllegalStateException();
            }
        }
    }

    private void a(Runnable runnable) {
        if (!this.u) {
            ExoPlayer exoPlayer = this.s;
            Assertions.a(exoPlayer);
            PlayerMessage a2 = exoPlayer.a(this);
            a2.a(4);
            a2.k();
            this.u = true;
        }
        if (runnable != null) {
            this.n.add(runnable);
        }
    }

    private void a(int i, Collection<MediaSourceHolder> collection) {
        for (MediaSourceHolder a2 : collection) {
            a(i, a2);
            i++;
        }
    }

    private void a(int i, MediaSourceHolder mediaSourceHolder) {
        if (i > 0) {
            MediaSourceHolder mediaSourceHolder2 = this.k.get(i - 1);
            mediaSourceHolder.a(i, mediaSourceHolder2.e + mediaSourceHolder2.c.b(), mediaSourceHolder2.f + mediaSourceHolder2.c.a());
        } else {
            mediaSourceHolder.a(i, 0, 0);
        }
        a(i, 1, mediaSourceHolder.c.b(), mediaSourceHolder.c.a());
        this.k.add(i, mediaSourceHolder);
        this.m.put(mediaSourceHolder.b, mediaSourceHolder);
        if (!this.p) {
            mediaSourceHolder.g = true;
            a(mediaSourceHolder, mediaSourceHolder.f3404a);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.google.android.exoplayer2.source.ConcatenatingMediaSource.MediaSourceHolder r12, com.google.android.exoplayer2.Timeline r13) {
        /*
            r11 = this;
            if (r12 == 0) goto L_0x00b1
            com.google.android.exoplayer2.source.ConcatenatingMediaSource$DeferredTimeline r1 = r12.c
            com.google.android.exoplayer2.Timeline r2 = r1.d()
            if (r2 != r13) goto L_0x000b
            return
        L_0x000b:
            int r2 = r13.b()
            int r3 = r1.b()
            int r2 = r2 - r3
            int r3 = r13.a()
            int r4 = r1.a()
            int r3 = r3 - r4
            r4 = 0
            r7 = 1
            if (r2 != 0) goto L_0x0023
            if (r3 == 0) goto L_0x0029
        L_0x0023:
            int r5 = r12.d
            int r5 = r5 + r7
            r11.a((int) r5, (int) r4, (int) r2, (int) r3)
        L_0x0029:
            boolean r2 = r12.h
            r8 = 0
            if (r2 == 0) goto L_0x0036
            com.google.android.exoplayer2.source.ConcatenatingMediaSource$DeferredTimeline r1 = r1.a((com.google.android.exoplayer2.Timeline) r13)
            r12.c = r1
            goto L_0x00ab
        L_0x0036:
            boolean r1 = r13.c()
            if (r1 == 0) goto L_0x0047
            java.lang.Object r1 = com.google.android.exoplayer2.source.ConcatenatingMediaSource.DeferredTimeline.d
            com.google.android.exoplayer2.source.ConcatenatingMediaSource$DeferredTimeline r1 = com.google.android.exoplayer2.source.ConcatenatingMediaSource.DeferredTimeline.a(r13, r1)
            r12.c = r1
            goto L_0x00ab
        L_0x0047:
            java.util.List<com.google.android.exoplayer2.source.DeferredMediaPeriod> r1 = r12.j
            int r1 = r1.size()
            if (r1 > r7) goto L_0x0051
            r1 = 1
            goto L_0x0052
        L_0x0051:
            r1 = 0
        L_0x0052:
            com.google.android.exoplayer2.util.Assertions.b(r1)
            java.util.List<com.google.android.exoplayer2.source.DeferredMediaPeriod> r1 = r12.j
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x005f
            r9 = r8
            goto L_0x0068
        L_0x005f:
            java.util.List<com.google.android.exoplayer2.source.DeferredMediaPeriod> r1 = r12.j
            java.lang.Object r1 = r1.get(r4)
            com.google.android.exoplayer2.source.DeferredMediaPeriod r1 = (com.google.android.exoplayer2.source.DeferredMediaPeriod) r1
            r9 = r1
        L_0x0068:
            com.google.android.exoplayer2.Timeline$Window r1 = r11.q
            long r1 = r1.b()
            if (r9 == 0) goto L_0x007c
            long r3 = r9.a()
            r5 = 0
            int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r10 == 0) goto L_0x007c
            r5 = r3
            goto L_0x007d
        L_0x007c:
            r5 = r1
        L_0x007d:
            com.google.android.exoplayer2.Timeline$Window r2 = r11.q
            com.google.android.exoplayer2.Timeline$Period r3 = r11.r
            r4 = 0
            r1 = r13
            android.util.Pair r1 = r1.a((com.google.android.exoplayer2.Timeline.Window) r2, (com.google.android.exoplayer2.Timeline.Period) r3, (int) r4, (long) r5)
            java.lang.Object r2 = r1.first
            java.lang.Object r1 = r1.second
            java.lang.Long r1 = (java.lang.Long) r1
            long r3 = r1.longValue()
            com.google.android.exoplayer2.source.ConcatenatingMediaSource$DeferredTimeline r1 = com.google.android.exoplayer2.source.ConcatenatingMediaSource.DeferredTimeline.a(r13, r2)
            r12.c = r1
            if (r9 == 0) goto L_0x00ab
            r9.d(r3)
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r1 = r9.b
            java.lang.Object r2 = r1.f3414a
            java.lang.Object r2 = a((com.google.android.exoplayer2.source.ConcatenatingMediaSource.MediaSourceHolder) r12, (java.lang.Object) r2)
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r1 = r1.a(r2)
            r9.a((com.google.android.exoplayer2.source.MediaSource.MediaPeriodId) r1)
        L_0x00ab:
            r12.h = r7
            r11.a((java.lang.Runnable) r8)
            return
        L_0x00b1:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.ConcatenatingMediaSource.a(com.google.android.exoplayer2.source.ConcatenatingMediaSource$MediaSourceHolder, com.google.android.exoplayer2.Timeline):void");
    }

    private void a(int i) {
        MediaSourceHolder remove = this.k.remove(i);
        this.m.remove(remove.b);
        DeferredTimeline deferredTimeline = remove.c;
        a(i, -1, -deferredTimeline.b(), -deferredTimeline.a());
        remove.i = true;
        a(remove);
    }

    private void a(int i, int i2) {
        int min = Math.min(i, i2);
        int max = Math.max(i, i2);
        int i3 = this.k.get(min).e;
        int i4 = this.k.get(min).f;
        List<MediaSourceHolder> list = this.k;
        list.add(i2, list.remove(i));
        while (min <= max) {
            MediaSourceHolder mediaSourceHolder = this.k.get(min);
            mediaSourceHolder.e = i3;
            mediaSourceHolder.f = i4;
            i3 += mediaSourceHolder.c.b();
            i4 += mediaSourceHolder.c.a();
            min++;
        }
    }

    private void a(int i, int i2, int i3, int i4) {
        this.w += i3;
        this.x += i4;
        while (i < this.k.size()) {
            this.k.get(i).d += i2;
            this.k.get(i).e += i3;
            this.k.get(i).f += i4;
            i++;
        }
    }

    private void a(MediaSourceHolder mediaSourceHolder) {
        if (mediaSourceHolder.i && mediaSourceHolder.g && mediaSourceHolder.j.isEmpty()) {
            a(mediaSourceHolder);
        }
    }

    private static Object a(MediaSourceHolder mediaSourceHolder, Object obj) {
        Object c = AbstractConcatenatedTimeline.c(obj);
        return c.equals(DeferredTimeline.d) ? mediaSourceHolder.c.c : c;
    }
}
