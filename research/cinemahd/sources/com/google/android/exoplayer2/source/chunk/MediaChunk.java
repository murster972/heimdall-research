package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Assertions;

public abstract class MediaChunk extends Chunk {
    public final long i;

    public MediaChunk(DataSource dataSource, DataSpec dataSpec, Format format, int i2, Object obj, long j, long j2, long j3) {
        super(dataSource, dataSpec, 1, format, i2, obj, j, j2);
        Assertions.a(format);
        this.i = j3;
    }

    public long e() {
        long j = this.i;
        if (j != -1) {
            return 1 + j;
        }
        return -1;
    }

    public abstract boolean f();
}
