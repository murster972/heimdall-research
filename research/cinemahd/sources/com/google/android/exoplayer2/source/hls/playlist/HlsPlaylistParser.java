package com.google.android.exoplayer2.source.hls.playlist;

import android.net.Uri;
import android.util.Base64;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil;
import com.google.android.exoplayer2.source.UnrecognizedInputFormatException;
import com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.util.Util;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.internal.cache.DiskLruCache;

public final class HlsPlaylistParser implements ParsingLoadable.Parser<HlsPlaylist> {
    private static final Pattern A = a("AUTOSELECT");
    private static final Pattern B = a("DEFAULT");
    private static final Pattern C = a("FORCED");
    private static final Pattern D = Pattern.compile("VALUE=\"(.+?)\"");
    private static final Pattern E = Pattern.compile("IMPORT=\"(.+?)\"");
    private static final Pattern F = Pattern.compile("\\{\\$([a-zA-Z0-9\\-_]+)\\}");
    private static final Pattern b = Pattern.compile("AVERAGE-BANDWIDTH=(\\d+)\\b");
    private static final Pattern c = Pattern.compile("AUDIO=\"(.+?)\"");
    private static final Pattern d = Pattern.compile("[^-]BANDWIDTH=(\\d+)\\b");
    private static final Pattern e = Pattern.compile("CODECS=\"(.+?)\"");
    private static final Pattern f = Pattern.compile("RESOLUTION=(\\d+x\\d+)");
    private static final Pattern g = Pattern.compile("FRAME-RATE=([\\d\\.]+)\\b");
    private static final Pattern h = Pattern.compile("#EXT-X-TARGETDURATION:(\\d+)\\b");
    private static final Pattern i = Pattern.compile("#EXT-X-VERSION:(\\d+)\\b");
    private static final Pattern j = Pattern.compile("#EXT-X-PLAYLIST-TYPE:(.+)\\b");
    private static final Pattern k = Pattern.compile("#EXT-X-MEDIA-SEQUENCE:(\\d+)\\b");
    private static final Pattern l = Pattern.compile("#EXTINF:([\\d\\.]+)\\b");
    private static final Pattern m = Pattern.compile("#EXTINF:[\\d\\.]+\\b,(.+)");
    private static final Pattern n = Pattern.compile("TIME-OFFSET=(-?[\\d\\.]+)\\b");
    private static final Pattern o = Pattern.compile("#EXT-X-BYTERANGE:(\\d+(?:@\\d+)?)\\b");
    private static final Pattern p = Pattern.compile("BYTERANGE=\"(\\d+(?:@\\d+)?)\\b\"");
    private static final Pattern q = Pattern.compile("METHOD=(NONE|AES-128|SAMPLE-AES|SAMPLE-AES-CENC|SAMPLE-AES-CTR)\\s*(?:,|$)");
    private static final Pattern r = Pattern.compile("KEYFORMAT=\"(.+?)\"");
    private static final Pattern s = Pattern.compile("KEYFORMATVERSIONS=\"(.+?)\"");
    private static final Pattern t = Pattern.compile("URI=\"(.+?)\"");
    private static final Pattern u = Pattern.compile("IV=([^,.*]+)");
    private static final Pattern v = Pattern.compile("TYPE=(AUDIO|VIDEO|SUBTITLES|CLOSED-CAPTIONS)");
    private static final Pattern w = Pattern.compile("LANGUAGE=\"(.+?)\"");
    private static final Pattern x = Pattern.compile("NAME=\"(.+?)\"");
    private static final Pattern y = Pattern.compile("GROUP-ID=\"(.+?)\"");
    private static final Pattern z = Pattern.compile("INSTREAM-ID=\"((?:CC|SERVICE)\\d+)\"");

    /* renamed from: a  reason: collision with root package name */
    private final HlsMasterPlaylist f3496a;

    private static class LineIterator {

        /* renamed from: a  reason: collision with root package name */
        private final BufferedReader f3497a;
        private final Queue<String> b;
        private String c;

        public LineIterator(Queue<String> queue, BufferedReader bufferedReader) {
            this.b = queue;
            this.f3497a = bufferedReader;
        }

        public boolean a() throws IOException {
            if (this.c != null) {
                return true;
            }
            if (!this.b.isEmpty()) {
                this.c = this.b.poll();
                return true;
            }
            do {
                String readLine = this.f3497a.readLine();
                this.c = readLine;
                if (readLine == null) {
                    return false;
                }
                this.c = this.c.trim();
            } while (this.c.isEmpty());
            return true;
        }

        public String b() throws IOException {
            if (!a()) {
                return null;
            }
            String str = this.c;
            this.c = null;
            return str;
        }
    }

    public HlsPlaylistParser() {
        this(HlsMasterPlaylist.j);
    }

    private static int b(String str) {
        int i2 = a(str, B, false) ? 1 : 0;
        if (a(str, C, false)) {
            i2 |= 2;
        }
        return a(str, A, false) ? i2 | 4 : i2;
    }

    private static long c(String str, Pattern pattern) throws ParserException {
        return Long.parseLong(b(str, pattern, Collections.emptyMap()));
    }

    public HlsPlaylistParser(HlsMasterPlaylist hlsMasterPlaylist) {
        this.f3496a = hlsMasterPlaylist;
    }

    public HlsPlaylist a(Uri uri, InputStream inputStream) throws IOException {
        String trim;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayDeque arrayDeque = new ArrayDeque();
        try {
            if (a(bufferedReader)) {
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        trim = readLine.trim();
                        if (!trim.isEmpty()) {
                            if (!trim.startsWith("#EXT-X-STREAM-INF")) {
                                if (trim.startsWith("#EXT-X-TARGETDURATION") || trim.startsWith("#EXT-X-MEDIA-SEQUENCE") || trim.startsWith("#EXTINF") || trim.startsWith("#EXT-X-KEY") || trim.startsWith("#EXT-X-BYTERANGE") || trim.equals("#EXT-X-DISCONTINUITY") || trim.equals("#EXT-X-DISCONTINUITY-SEQUENCE")) {
                                    break;
                                } else if (trim.equals("#EXT-X-ENDLIST")) {
                                    break;
                                } else {
                                    arrayDeque.add(trim);
                                }
                            } else {
                                arrayDeque.add(trim);
                                HlsMasterPlaylist a2 = a(new LineIterator(arrayDeque, bufferedReader), uri.toString());
                                Util.a((Closeable) bufferedReader);
                                return a2;
                            }
                        }
                    } else {
                        Util.a((Closeable) bufferedReader);
                        throw new ParserException("Failed to parse the playlist, could not identify any tags.");
                    }
                }
                arrayDeque.add(trim);
                return a(this.f3496a, new LineIterator(arrayDeque, bufferedReader), uri.toString());
            }
            throw new UnrecognizedInputFormatException("Input does not start with the #EXTM3U header.", uri);
        } finally {
            Util.a((Closeable) bufferedReader);
        }
    }

    private static int b(String str, Pattern pattern) throws ParserException {
        return Integer.parseInt(b(str, pattern, Collections.emptyMap()));
    }

    private static String b(String str, Pattern pattern, Map<String, String> map) throws ParserException {
        String a2 = a(str, pattern, map);
        if (a2 != null) {
            return a2;
        }
        throw new ParserException("Couldn't match " + pattern.pattern() + " in " + str);
    }

    private static String b(String str, Map<String, String> map) {
        Matcher matcher = F.matcher(str);
        StringBuffer stringBuffer = new StringBuffer();
        while (matcher.find()) {
            String group = matcher.group(1);
            if (map.containsKey(group)) {
                matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(map.get(group)));
            }
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

    private static boolean a(BufferedReader bufferedReader) throws IOException {
        int read = bufferedReader.read();
        if (read == 239) {
            if (bufferedReader.read() != 187 || bufferedReader.read() != 191) {
                return false;
            }
            read = bufferedReader.read();
        }
        int a2 = a(bufferedReader, true, read);
        for (int i2 = 0; i2 < 7; i2++) {
            if (a2 != "#EXTM3U".charAt(i2)) {
                return false;
            }
            a2 = bufferedReader.read();
        }
        return Util.g(a(bufferedReader, false, a2));
    }

    private static int a(BufferedReader bufferedReader, boolean z2, int i2) throws IOException {
        while (i2 != -1 && Character.isWhitespace(i2) && (z2 || !Util.g(i2))) {
            i2 = bufferedReader.read();
        }
        return i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0203  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist a(com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistParser.LineIterator r32, java.lang.String r33) throws java.io.IOException {
        /*
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r11 = new java.util.HashMap
            r11.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r3 = 0
            r8 = 1
            r9 = 0
            r10 = 0
        L_0x002c:
            boolean r12 = r32.a()
            if (r12 == 0) goto L_0x011b
            java.lang.String r12 = r32.b()
            java.lang.String r14 = "#EXT"
            boolean r14 = r12.startsWith(r14)
            if (r14 == 0) goto L_0x0041
            r4.add(r12)
        L_0x0041:
            java.lang.String r14 = "#EXT-X-DEFINE"
            boolean r14 = r12.startsWith(r14)
            if (r14 == 0) goto L_0x0059
            java.util.regex.Pattern r13 = x
            java.lang.String r13 = b(r12, r13, r11)
            java.util.regex.Pattern r14 = D
            java.lang.String r12 = b(r12, r14, r11)
            r11.put(r13, r12)
            goto L_0x002c
        L_0x0059:
            java.lang.String r14 = "#EXT-X-INDEPENDENT-SEGMENTS"
            boolean r14 = r12.equals(r14)
            if (r14 == 0) goto L_0x0063
            r10 = 1
            goto L_0x002c
        L_0x0063:
            java.lang.String r14 = "#EXT-X-MEDIA"
            boolean r14 = r12.startsWith(r14)
            if (r14 == 0) goto L_0x006f
            r2.add(r12)
            goto L_0x002c
        L_0x006f:
            java.lang.String r14 = "#EXT-X-STREAM-INF"
            boolean r14 = r12.startsWith(r14)
            if (r14 == 0) goto L_0x002c
            java.lang.String r14 = "CLOSED-CAPTIONS=NONE"
            boolean r14 = r12.contains(r14)
            r9 = r9 | r14
            java.util.regex.Pattern r14 = d
            int r14 = b((java.lang.String) r12, (java.util.regex.Pattern) r14)
            java.util.regex.Pattern r15 = b
            java.lang.String r15 = a((java.lang.String) r12, (java.util.regex.Pattern) r15, (java.util.Map<java.lang.String, java.lang.String>) r11)
            if (r15 == 0) goto L_0x0090
            int r14 = java.lang.Integer.parseInt(r15)
        L_0x0090:
            r20 = r14
            java.util.regex.Pattern r14 = e
            java.lang.String r14 = a((java.lang.String) r12, (java.util.regex.Pattern) r14, (java.util.Map<java.lang.String, java.lang.String>) r11)
            java.util.regex.Pattern r15 = f
            java.lang.String r15 = a((java.lang.String) r12, (java.util.regex.Pattern) r15, (java.util.Map<java.lang.String, java.lang.String>) r11)
            if (r15 == 0) goto L_0x00c3
            java.lang.String r13 = "x"
            java.lang.String[] r13 = r15.split(r13)
            r15 = r13[r3]
            int r15 = java.lang.Integer.parseInt(r15)
            r13 = r13[r8]
            int r13 = java.lang.Integer.parseInt(r13)
            if (r15 <= 0) goto L_0x00bb
            if (r13 > 0) goto L_0x00b7
            goto L_0x00bb
        L_0x00b7:
            r16 = r13
            r13 = r15
            goto L_0x00be
        L_0x00bb:
            r13 = -1
            r16 = -1
        L_0x00be:
            r21 = r13
            r22 = r16
            goto L_0x00c7
        L_0x00c3:
            r21 = -1
            r22 = -1
        L_0x00c7:
            r13 = -1082130432(0xffffffffbf800000, float:-1.0)
            java.util.regex.Pattern r15 = g
            java.lang.String r15 = a((java.lang.String) r12, (java.util.regex.Pattern) r15, (java.util.Map<java.lang.String, java.lang.String>) r11)
            if (r15 == 0) goto L_0x00d8
            float r13 = java.lang.Float.parseFloat(r15)
            r23 = r13
            goto L_0x00da
        L_0x00d8:
            r23 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x00da:
            java.util.regex.Pattern r13 = c
            java.lang.String r12 = a((java.lang.String) r12, (java.util.regex.Pattern) r13, (java.util.Map<java.lang.String, java.lang.String>) r11)
            if (r12 == 0) goto L_0x00eb
            if (r14 == 0) goto L_0x00eb
            java.lang.String r13 = com.google.android.exoplayer2.util.Util.a((java.lang.String) r14, (int) r8)
            r1.put(r12, r13)
        L_0x00eb:
            java.lang.String r12 = r32.b()
            java.lang.String r12 = b((java.lang.String) r12, (java.util.Map<java.lang.String, java.lang.String>) r11)
            boolean r13 = r0.add(r12)
            if (r13 == 0) goto L_0x002c
            int r13 = r5.size()
            java.lang.String r15 = java.lang.Integer.toString(r13)
            r16 = 0
            r18 = 0
            r24 = 0
            r25 = 0
            java.lang.String r17 = "application/x-mpegURL"
            r19 = r14
            com.google.android.exoplayer2.Format r13 = com.google.android.exoplayer2.Format.a((java.lang.String) r15, (java.lang.String) r16, (java.lang.String) r17, (java.lang.String) r18, (java.lang.String) r19, (int) r20, (int) r21, (int) r22, (float) r23, (java.util.List<byte[]>) r24, (int) r25)
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl r14 = new com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl
            r14.<init>(r12, r13)
            r5.add(r14)
            goto L_0x002c
        L_0x011b:
            r12 = 0
            r13 = 0
            r14 = 0
        L_0x011e:
            int r15 = r2.size()
            if (r12 >= r15) goto L_0x0240
            java.lang.Object r15 = r2.get(r12)
            java.lang.String r15 = (java.lang.String) r15
            int r26 = b(r15)
            java.util.regex.Pattern r0 = t
            java.lang.String r0 = a((java.lang.String) r15, (java.util.regex.Pattern) r0, (java.util.Map<java.lang.String, java.lang.String>) r11)
            java.util.regex.Pattern r3 = x
            java.lang.String r3 = b(r15, r3, r11)
            java.util.regex.Pattern r8 = w
            java.lang.String r27 = a((java.lang.String) r15, (java.util.regex.Pattern) r8, (java.util.Map<java.lang.String, java.lang.String>) r11)
            java.util.regex.Pattern r8 = y
            java.lang.String r8 = a((java.lang.String) r15, (java.util.regex.Pattern) r8, (java.util.Map<java.lang.String, java.lang.String>) r11)
            r28 = r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r8)
            r29 = r10
            java.lang.String r10 = ":"
            r2.append(r10)
            r2.append(r3)
            java.lang.String r17 = r2.toString()
            java.util.regex.Pattern r2 = v
            java.lang.String r2 = b(r15, r2, r11)
            int r10 = r2.hashCode()
            r30 = r13
            r13 = -959297733(0xffffffffc6d2473b, float:-26915.615)
            r31 = r5
            r5 = 2
            if (r10 == r13) goto L_0x0191
            r13 = -333210994(0xffffffffec239a8e, float:-7.911391E26)
            if (r10 == r13) goto L_0x0187
            r13 = 62628790(0x3bba3b6, float:1.1028458E-36)
            if (r10 == r13) goto L_0x017d
            goto L_0x019b
        L_0x017d:
            java.lang.String r10 = "AUDIO"
            boolean r2 = r2.equals(r10)
            if (r2 == 0) goto L_0x019b
            r2 = 0
            goto L_0x019c
        L_0x0187:
            java.lang.String r10 = "CLOSED-CAPTIONS"
            boolean r2 = r2.equals(r10)
            if (r2 == 0) goto L_0x019b
            r2 = 2
            goto L_0x019c
        L_0x0191:
            java.lang.String r10 = "SUBTITLES"
            boolean r2 = r2.equals(r10)
            if (r2 == 0) goto L_0x019b
            r2 = 1
            goto L_0x019c
        L_0x019b:
            r2 = -1
        L_0x019c:
            if (r2 == 0) goto L_0x0203
            r10 = 1
            if (r2 == r10) goto L_0x01e8
            if (r2 == r5) goto L_0x01a5
            goto L_0x0232
        L_0x01a5:
            java.util.regex.Pattern r0 = z
            java.lang.String r0 = b(r15, r0, r11)
            java.lang.String r2 = "CC"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x01be
            java.lang.String r0 = r0.substring(r5)
            int r0 = java.lang.Integer.parseInt(r0)
            java.lang.String r2 = "application/cea-608"
            goto L_0x01c9
        L_0x01be:
            r2 = 7
            java.lang.String r0 = r0.substring(r2)
            int r0 = java.lang.Integer.parseInt(r0)
            java.lang.String r2 = "application/cea-708"
        L_0x01c9:
            r25 = r0
            r20 = r2
            if (r14 != 0) goto L_0x01d4
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
        L_0x01d4:
            r19 = 0
            r21 = 0
            r22 = -1
            r18 = r3
            r23 = r26
            r24 = r27
            com.google.android.exoplayer2.Format r0 = com.google.android.exoplayer2.Format.a((java.lang.String) r17, (java.lang.String) r18, (java.lang.String) r19, (java.lang.String) r20, (java.lang.String) r21, (int) r22, (int) r23, (java.lang.String) r24, (int) r25)
            r14.add(r0)
            goto L_0x0232
        L_0x01e8:
            r21 = 0
            r22 = -1
            java.lang.String r19 = "application/x-mpegURL"
            java.lang.String r20 = "text/vtt"
            r18 = r3
            r23 = r26
            r24 = r27
            com.google.android.exoplayer2.Format r2 = com.google.android.exoplayer2.Format.b(r17, r18, r19, r20, r21, r22, r23, r24)
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl r3 = new com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl
            r3.<init>(r0, r2)
            r7.add(r3)
            goto L_0x0232
        L_0x0203:
            r10 = 1
            java.lang.Object r2 = r1.get(r8)
            r21 = r2
            java.lang.String r21 = (java.lang.String) r21
            if (r21 == 0) goto L_0x0215
            java.lang.String r2 = com.google.android.exoplayer2.util.MimeTypes.d(r21)
            r20 = r2
            goto L_0x0217
        L_0x0215:
            r20 = 0
        L_0x0217:
            r22 = -1
            r23 = -1
            r24 = -1
            r25 = 0
            java.lang.String r19 = "application/x-mpegURL"
            r18 = r3
            com.google.android.exoplayer2.Format r13 = com.google.android.exoplayer2.Format.a((java.lang.String) r17, (java.lang.String) r18, (java.lang.String) r19, (java.lang.String) r20, (java.lang.String) r21, (int) r22, (int) r23, (int) r24, (java.util.List<byte[]>) r25, (int) r26, (java.lang.String) r27)
            if (r0 != 0) goto L_0x022a
            goto L_0x0234
        L_0x022a:
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl r2 = new com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl
            r2.<init>(r0, r13)
            r6.add(r2)
        L_0x0232:
            r13 = r30
        L_0x0234:
            int r12 = r12 + 1
            r2 = r28
            r10 = r29
            r5 = r31
            r3 = 0
            r8 = 1
            goto L_0x011e
        L_0x0240:
            r31 = r5
            r29 = r10
            r30 = r13
            if (r9 == 0) goto L_0x024e
            java.util.List r0 = java.util.Collections.emptyList()
            r9 = r0
            goto L_0x024f
        L_0x024e:
            r9 = r14
        L_0x024f:
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist r0 = new com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist
            r2 = r0
            r3 = r33
            r5 = r31
            r8 = r30
            r10 = r29
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistParser.a(com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistParser$LineIterator, java.lang.String):com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist");
    }

    private static HlsMediaPlaylist a(HlsMasterPlaylist hlsMasterPlaylist, LineIterator lineIterator, String str) throws IOException {
        TreeMap treeMap;
        DrmInitData drmInitData;
        DrmInitData.SchemeData schemeData;
        HlsMasterPlaylist hlsMasterPlaylist2 = hlsMasterPlaylist;
        boolean z2 = hlsMasterPlaylist2.c;
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        TreeMap treeMap2 = new TreeMap();
        int i2 = 0;
        int i3 = 1;
        boolean z3 = z2;
        long j2 = -9223372036854775807L;
        long j3 = -9223372036854775807L;
        String str2 = "";
        boolean z4 = false;
        int i4 = 0;
        String str3 = null;
        long j4 = 0;
        int i5 = 0;
        long j5 = 0;
        int i6 = 1;
        boolean z5 = false;
        DrmInitData drmInitData2 = null;
        long j6 = 0;
        long j7 = 0;
        DrmInitData drmInitData3 = null;
        boolean z6 = false;
        long j8 = -1;
        int i7 = 0;
        long j9 = 0;
        String str4 = null;
        String str5 = null;
        HlsMediaPlaylist.Segment segment = null;
        long j10 = 0;
        while (lineIterator.a()) {
            String b2 = lineIterator.b();
            if (b2.startsWith("#EXT")) {
                arrayList2.add(b2);
            }
            if (b2.startsWith("#EXT-X-PLAYLIST-TYPE")) {
                String b3 = b(b2, j, hashMap);
                if ("VOD".equals(b3)) {
                    i4 = 1;
                } else if ("EVENT".equals(b3)) {
                    i4 = 2;
                }
            } else if (b2.startsWith("#EXT-X-START")) {
                j2 = (long) (a(b2, n) * 1000000.0d);
            } else if (b2.startsWith("#EXT-X-MAP")) {
                String b4 = b(b2, t, hashMap);
                String a2 = a(b2, p, (Map<String, String>) hashMap);
                if (a2 != null) {
                    String[] split = a2.split("@");
                    j8 = Long.parseLong(split[i2]);
                    if (split.length > i3) {
                        j6 = Long.parseLong(split[i3]);
                    }
                }
                segment = new HlsMediaPlaylist.Segment(b4, j6, j8);
                j6 = 0;
                j8 = -1;
            } else if (b2.startsWith("#EXT-X-TARGETDURATION")) {
                j3 = 1000000 * ((long) b(b2, h));
            } else if (b2.startsWith("#EXT-X-MEDIA-SEQUENCE")) {
                j7 = c(b2, k);
                j5 = j7;
            } else if (b2.startsWith("#EXT-X-VERSION")) {
                i6 = b(b2, i);
            } else {
                if (b2.startsWith("#EXT-X-DEFINE")) {
                    String a3 = a(b2, E, (Map<String, String>) hashMap);
                    if (a3 != null) {
                        String str6 = hlsMasterPlaylist2.i.get(a3);
                        if (str6 != null) {
                            hashMap.put(a3, str6);
                        }
                    } else {
                        hashMap.put(b(b2, x, hashMap), b(b2, D, hashMap));
                    }
                } else if (b2.startsWith("#EXTINF")) {
                    str2 = a(b2, m, "", hashMap);
                    j10 = (long) (a(b2, l) * 1000000.0d);
                } else if (b2.startsWith("#EXT-X-KEY")) {
                    String b5 = b(b2, q, hashMap);
                    String a4 = a(b2, r, InterpolationAnimatedNode.EXTRAPOLATE_TYPE_IDENTITY, hashMap);
                    if ("NONE".equals(b5)) {
                        treeMap2.clear();
                        drmInitData3 = null;
                        str4 = null;
                        str5 = null;
                    } else {
                        String a5 = a(b2, u, (Map<String, String>) hashMap);
                        if (!InterpolationAnimatedNode.EXTRAPOLATE_TYPE_IDENTITY.equals(a4)) {
                            if (str3 == null) {
                                str3 = ("SAMPLE-AES-CENC".equals(b5) || "SAMPLE-AES-CTR".equals(b5)) ? "cenc" : "cbcs";
                            }
                            if ("com.microsoft.playready".equals(a4)) {
                                schemeData = a(b2, (Map<String, String>) hashMap);
                            } else {
                                schemeData = a(b2, a4, (Map<String, String>) hashMap);
                            }
                            if (schemeData != null) {
                                treeMap2.put(a4, schemeData);
                                str5 = a5;
                                drmInitData3 = null;
                                str4 = null;
                            }
                        } else if ("AES-128".equals(b5)) {
                            str4 = b(b2, t, hashMap);
                            str5 = a5;
                        }
                        str5 = a5;
                        str4 = null;
                    }
                } else if (b2.startsWith("#EXT-X-BYTERANGE")) {
                    String[] split2 = b(b2, o, hashMap).split("@");
                    j8 = Long.parseLong(split2[i2]);
                    if (split2.length > i3) {
                        j6 = Long.parseLong(split2[i3]);
                    }
                } else if (b2.startsWith("#EXT-X-DISCONTINUITY-SEQUENCE")) {
                    i5 = Integer.parseInt(b2.substring(b2.indexOf(58) + i3));
                    z4 = true;
                } else if (b2.equals("#EXT-X-DISCONTINUITY")) {
                    i7++;
                } else if (b2.startsWith("#EXT-X-PROGRAM-DATE-TIME")) {
                    if (j4 == 0) {
                        j4 = C.a(Util.h(b2.substring(b2.indexOf(58) + i3))) - j9;
                    }
                } else if (b2.equals("#EXT-X-GAP")) {
                    z6 = true;
                } else if (b2.equals("#EXT-X-INDEPENDENT-SEGMENTS")) {
                    z3 = true;
                } else if (b2.equals("#EXT-X-ENDLIST")) {
                    z5 = true;
                } else if (!b2.startsWith("#")) {
                    String hexString = str4 == null ? null : str5 != null ? str5 : Long.toHexString(j7);
                    long j11 = j7 + 1;
                    int i8 = (j8 > -1 ? 1 : (j8 == -1 ? 0 : -1));
                    if (i8 == 0) {
                        j6 = 0;
                    }
                    if (drmInitData3 != null || treeMap2.isEmpty()) {
                        treeMap = treeMap2;
                        drmInitData = drmInitData3;
                    } else {
                        DrmInitData.SchemeData[] schemeDataArr = (DrmInitData.SchemeData[]) treeMap2.values().toArray(new DrmInitData.SchemeData[i2]);
                        drmInitData = new DrmInitData(str3, schemeDataArr);
                        if (drmInitData2 == null) {
                            DrmInitData.SchemeData[] schemeDataArr2 = new DrmInitData.SchemeData[schemeDataArr.length];
                            int i9 = 0;
                            while (i9 < schemeDataArr.length) {
                                schemeDataArr2[i9] = schemeDataArr[i9].a((byte[]) null);
                                i9++;
                                treeMap2 = treeMap2;
                            }
                            treeMap = treeMap2;
                            drmInitData2 = new DrmInitData(str3, schemeDataArr2);
                        } else {
                            treeMap = treeMap2;
                        }
                    }
                    arrayList.add(new HlsMediaPlaylist.Segment(b(b2, (Map<String, String>) hashMap), segment, str2, j10, i7, j9, drmInitData, str4, hexString, j6, j8, z6));
                    j9 += j10;
                    if (i8 != 0) {
                        j6 += j8;
                    }
                    i2 = 0;
                    i3 = 1;
                    z6 = false;
                    j8 = -1;
                    j10 = 0;
                    hlsMasterPlaylist2 = hlsMasterPlaylist;
                    str2 = "";
                    j7 = j11;
                    drmInitData3 = drmInitData;
                    treeMap2 = treeMap;
                }
                treeMap = treeMap2;
                i2 = 0;
                i3 = 1;
                hlsMasterPlaylist2 = hlsMasterPlaylist;
                treeMap2 = treeMap;
            }
        }
        return new HlsMediaPlaylist(i4, str, arrayList2, j2, j4, z4, i5, j5, i6, j3, z3, z5, j4 != 0, drmInitData2, arrayList);
    }

    private static DrmInitData.SchemeData a(String str, Map<String, String> map) throws ParserException {
        if (!DiskLruCache.VERSION_1.equals(a(str, s, DiskLruCache.VERSION_1, map))) {
            return null;
        }
        String b2 = b(str, t, map);
        return new DrmInitData.SchemeData(C.e, "video/mp4", PsshAtomUtil.a(C.e, Base64.decode(b2.substring(b2.indexOf(44)), 0)));
    }

    private static DrmInitData.SchemeData a(String str, String str2, Map<String, String> map) throws ParserException {
        if ("urn:uuid:edef8ba9-79d6-4ace-a3c8-27dcd51d21ed".equals(str2)) {
            String b2 = b(str, t, map);
            return new DrmInitData.SchemeData(C.d, "video/mp4", Base64.decode(b2.substring(b2.indexOf(44)), 0));
        } else if (!"com.widevine".equals(str2)) {
            return null;
        } else {
            try {
                return new DrmInitData.SchemeData(C.d, "hls", str.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e2) {
                throw new ParserException((Throwable) e2);
            }
        }
    }

    private static double a(String str, Pattern pattern) throws ParserException {
        return Double.parseDouble(b(str, pattern, Collections.emptyMap()));
    }

    private static String a(String str, Pattern pattern, Map<String, String> map) {
        return a(str, pattern, (String) null, map);
    }

    private static String a(String str, Pattern pattern, String str2, Map<String, String> map) {
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            str2 = matcher.group(1);
        }
        return (map.isEmpty() || str2 == null) ? str2 : b(str2, map);
    }

    private static boolean a(String str, Pattern pattern, boolean z2) {
        Matcher matcher = pattern.matcher(str);
        return matcher.find() ? matcher.group(1).equals("YES") : z2;
    }

    private static Pattern a(String str) {
        return Pattern.compile(str + "=(" + "NO" + "|" + "YES" + ")");
    }
}
