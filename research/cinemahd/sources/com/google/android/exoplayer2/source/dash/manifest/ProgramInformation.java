package com.google.android.exoplayer2.source.dash.manifest;

import com.google.android.exoplayer2.util.Util;

public class ProgramInformation {

    /* renamed from: a  reason: collision with root package name */
    public final String f3465a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;

    public ProgramInformation(String str, String str2, String str3, String str4, String str5) {
        this.f3465a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ProgramInformation.class != obj.getClass()) {
            return false;
        }
        ProgramInformation programInformation = (ProgramInformation) obj;
        if (!Util.a((Object) this.f3465a, (Object) programInformation.f3465a) || !Util.a((Object) this.b, (Object) programInformation.b) || !Util.a((Object) this.c, (Object) programInformation.c) || !Util.a((Object) this.d, (Object) programInformation.d) || !Util.a((Object) this.e, (Object) programInformation.e)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.f3465a;
        int i = 0;
        int hashCode = (527 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }
}
