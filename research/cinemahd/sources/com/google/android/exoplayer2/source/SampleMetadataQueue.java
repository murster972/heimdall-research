package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;

final class SampleMetadataQueue {

    /* renamed from: a  reason: collision with root package name */
    private int f3418a = 1000;
    private int[] b;
    private long[] c;
    private int[] d;
    private int[] e;
    private long[] f;
    private TrackOutput.CryptoData[] g;
    private Format[] h;
    private int i;
    private int j;
    private int k;
    private int l;
    private long m;
    private long n;
    private boolean o;
    private boolean p;
    private Format q;
    private int r;

    public static final class SampleExtrasHolder {

        /* renamed from: a  reason: collision with root package name */
        public int f3419a;
        public long b;
        public TrackOutput.CryptoData c;
    }

    public SampleMetadataQueue() {
        int i2 = this.f3418a;
        this.b = new int[i2];
        this.c = new long[i2];
        this.f = new long[i2];
        this.e = new int[i2];
        this.d = new int[i2];
        this.g = new TrackOutput.CryptoData[i2];
        this.h = new Format[i2];
        this.m = Long.MIN_VALUE;
        this.n = Long.MIN_VALUE;
        this.p = true;
        this.o = true;
    }

    public void a(boolean z) {
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.o = true;
        this.m = Long.MIN_VALUE;
        this.n = Long.MIN_VALUE;
        if (z) {
            this.q = null;
            this.p = true;
        }
    }

    public synchronized boolean b(int i2) {
        if (this.j > i2 || i2 > this.j + this.i) {
            return false;
        }
        this.l = i2 - this.j;
        return true;
    }

    public void c(int i2) {
        this.r = i2;
    }

    public int d() {
        return this.j;
    }

    public synchronized long e() {
        return this.i == 0 ? Long.MIN_VALUE : this.f[this.k];
    }

    public synchronized long f() {
        return this.n;
    }

    public int g() {
        return this.j + this.l;
    }

    public synchronized Format h() {
        return this.p ? null : this.q;
    }

    public int i() {
        return this.j + this.i;
    }

    public synchronized boolean j() {
        return this.l != this.i;
    }

    public int k() {
        return j() ? this.b[f(this.l)] : this.r;
    }

    public synchronized void l() {
        this.l = 0;
    }

    private long d(int i2) {
        this.m = Math.max(this.m, e(i2));
        this.i -= i2;
        this.j += i2;
        this.k += i2;
        int i3 = this.k;
        int i4 = this.f3418a;
        if (i3 >= i4) {
            this.k = i3 - i4;
        }
        this.l -= i2;
        if (this.l < 0) {
            this.l = 0;
        }
        if (this.i != 0) {
            return this.c[this.k];
        }
        int i5 = this.k;
        if (i5 == 0) {
            i5 = this.f3418a;
        }
        int i6 = i5 - 1;
        return this.c[i6] + ((long) this.d[i6]);
    }

    private long e(int i2) {
        long j2 = Long.MIN_VALUE;
        if (i2 == 0) {
            return Long.MIN_VALUE;
        }
        int f2 = f(i2 - 1);
        for (int i3 = 0; i3 < i2; i3++) {
            j2 = Math.max(j2, this.f[f2]);
            if ((this.e[f2] & 1) != 0) {
                break;
            }
            f2--;
            if (f2 == -1) {
                f2 = this.f3418a - 1;
            }
        }
        return j2;
    }

    private int f(int i2) {
        int i3 = this.k + i2;
        int i4 = this.f3418a;
        return i3 < i4 ? i3 : i3 - i4;
    }

    public synchronized long c() {
        if (this.l == 0) {
            return -1;
        }
        return d(this.l);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0037, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long b(long r10, boolean r12, boolean r13) {
        /*
            r9 = this;
            monitor-enter(r9)
            int r0 = r9.i     // Catch:{ all -> 0x0038 }
            r1 = -1
            if (r0 == 0) goto L_0x0036
            long[] r0 = r9.f     // Catch:{ all -> 0x0038 }
            int r3 = r9.k     // Catch:{ all -> 0x0038 }
            r3 = r0[r3]     // Catch:{ all -> 0x0038 }
            int r0 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x0012
            goto L_0x0036
        L_0x0012:
            if (r13 == 0) goto L_0x001f
            int r13 = r9.l     // Catch:{ all -> 0x0038 }
            int r0 = r9.i     // Catch:{ all -> 0x0038 }
            if (r13 == r0) goto L_0x001f
            int r13 = r9.l     // Catch:{ all -> 0x0038 }
            int r13 = r13 + 1
            goto L_0x0021
        L_0x001f:
            int r13 = r9.i     // Catch:{ all -> 0x0038 }
        L_0x0021:
            r5 = r13
            int r4 = r9.k     // Catch:{ all -> 0x0038 }
            r3 = r9
            r6 = r10
            r8 = r12
            int r10 = r3.a(r4, r5, r6, r8)     // Catch:{ all -> 0x0038 }
            r11 = -1
            if (r10 != r11) goto L_0x0030
            monitor-exit(r9)
            return r1
        L_0x0030:
            long r10 = r9.d(r10)     // Catch:{ all -> 0x0038 }
            monitor-exit(r9)
            return r10
        L_0x0036:
            monitor-exit(r9)
            return r1
        L_0x0038:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.SampleMetadataQueue.b(long, boolean, boolean):long");
    }

    public long a(int i2) {
        int i3 = i() - i2;
        Assertions.a(i3 >= 0 && i3 <= this.i - this.l);
        this.i -= i3;
        this.n = Math.max(this.m, e(this.i));
        int i4 = this.i;
        if (i4 == 0) {
            return 0;
        }
        int f2 = f(i4 - 1);
        return this.c[f2] + ((long) this.d[f2]);
    }

    public synchronized long b() {
        if (this.i == 0) {
            return -1;
        }
        return d(this.i);
    }

    public synchronized void b(long j2) {
        this.n = Math.max(this.n, j2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0023, code lost:
        return -3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int a(com.google.android.exoplayer2.FormatHolder r5, com.google.android.exoplayer2.decoder.DecoderInputBuffer r6, boolean r7, boolean r8, com.google.android.exoplayer2.Format r9, com.google.android.exoplayer2.source.SampleMetadataQueue.SampleExtrasHolder r10) {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.j()     // Catch:{ all -> 0x006a }
            r1 = -5
            r2 = -3
            r3 = -4
            if (r0 != 0) goto L_0x0024
            if (r8 == 0) goto L_0x0012
            r5 = 4
            r6.setFlags(r5)     // Catch:{ all -> 0x006a }
            monitor-exit(r4)
            return r3
        L_0x0012:
            com.google.android.exoplayer2.Format r6 = r4.q     // Catch:{ all -> 0x006a }
            if (r6 == 0) goto L_0x0022
            if (r7 != 0) goto L_0x001c
            com.google.android.exoplayer2.Format r6 = r4.q     // Catch:{ all -> 0x006a }
            if (r6 == r9) goto L_0x0022
        L_0x001c:
            com.google.android.exoplayer2.Format r6 = r4.q     // Catch:{ all -> 0x006a }
            r5.f3165a = r6     // Catch:{ all -> 0x006a }
            monitor-exit(r4)
            return r1
        L_0x0022:
            monitor-exit(r4)
            return r2
        L_0x0024:
            int r8 = r4.l     // Catch:{ all -> 0x006a }
            int r8 = r4.f(r8)     // Catch:{ all -> 0x006a }
            if (r7 != 0) goto L_0x0062
            com.google.android.exoplayer2.Format[] r7 = r4.h     // Catch:{ all -> 0x006a }
            r7 = r7[r8]     // Catch:{ all -> 0x006a }
            if (r7 == r9) goto L_0x0033
            goto L_0x0062
        L_0x0033:
            boolean r5 = r6.d()     // Catch:{ all -> 0x006a }
            if (r5 == 0) goto L_0x003b
            monitor-exit(r4)
            return r2
        L_0x003b:
            long[] r5 = r4.f     // Catch:{ all -> 0x006a }
            r0 = r5[r8]     // Catch:{ all -> 0x006a }
            r6.c = r0     // Catch:{ all -> 0x006a }
            int[] r5 = r4.e     // Catch:{ all -> 0x006a }
            r5 = r5[r8]     // Catch:{ all -> 0x006a }
            r6.setFlags(r5)     // Catch:{ all -> 0x006a }
            int[] r5 = r4.d     // Catch:{ all -> 0x006a }
            r5 = r5[r8]     // Catch:{ all -> 0x006a }
            r10.f3419a = r5     // Catch:{ all -> 0x006a }
            long[] r5 = r4.c     // Catch:{ all -> 0x006a }
            r6 = r5[r8]     // Catch:{ all -> 0x006a }
            r10.b = r6     // Catch:{ all -> 0x006a }
            com.google.android.exoplayer2.extractor.TrackOutput$CryptoData[] r5 = r4.g     // Catch:{ all -> 0x006a }
            r5 = r5[r8]     // Catch:{ all -> 0x006a }
            r10.c = r5     // Catch:{ all -> 0x006a }
            int r5 = r4.l     // Catch:{ all -> 0x006a }
            int r5 = r5 + 1
            r4.l = r5     // Catch:{ all -> 0x006a }
            monitor-exit(r4)
            return r3
        L_0x0062:
            com.google.android.exoplayer2.Format[] r6 = r4.h     // Catch:{ all -> 0x006a }
            r6 = r6[r8]     // Catch:{ all -> 0x006a }
            r5.f3165a = r6     // Catch:{ all -> 0x006a }
            monitor-exit(r4)
            return r1
        L_0x006a:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.SampleMetadataQueue.a(com.google.android.exoplayer2.FormatHolder, com.google.android.exoplayer2.decoder.DecoderInputBuffer, boolean, boolean, com.google.android.exoplayer2.Format, com.google.android.exoplayer2.source.SampleMetadataQueue$SampleExtrasHolder):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0038, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int a(long r9, boolean r11, boolean r12) {
        /*
            r8 = this;
            monitor-enter(r8)
            int r0 = r8.l     // Catch:{ all -> 0x0039 }
            int r2 = r8.f(r0)     // Catch:{ all -> 0x0039 }
            boolean r0 = r8.j()     // Catch:{ all -> 0x0039 }
            r7 = -1
            if (r0 == 0) goto L_0x0037
            long[] r0 = r8.f     // Catch:{ all -> 0x0039 }
            r3 = r0[r2]     // Catch:{ all -> 0x0039 }
            int r0 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r0 < 0) goto L_0x0037
            long r0 = r8.n     // Catch:{ all -> 0x0039 }
            int r3 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x001f
            if (r12 != 0) goto L_0x001f
            goto L_0x0037
        L_0x001f:
            int r12 = r8.i     // Catch:{ all -> 0x0039 }
            int r0 = r8.l     // Catch:{ all -> 0x0039 }
            int r3 = r12 - r0
            r1 = r8
            r4 = r9
            r6 = r11
            int r9 = r1.a(r2, r3, r4, r6)     // Catch:{ all -> 0x0039 }
            if (r9 != r7) goto L_0x0030
            monitor-exit(r8)
            return r7
        L_0x0030:
            int r10 = r8.l     // Catch:{ all -> 0x0039 }
            int r10 = r10 + r9
            r8.l = r10     // Catch:{ all -> 0x0039 }
            monitor-exit(r8)
            return r9
        L_0x0037:
            monitor-exit(r8)
            return r7
        L_0x0039:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.SampleMetadataQueue.a(long, boolean, boolean):int");
    }

    public synchronized int a() {
        int i2;
        i2 = this.i - this.l;
        this.l = this.i;
        return i2;
    }

    public synchronized boolean a(Format format) {
        if (format == null) {
            this.p = true;
            return false;
        }
        this.p = false;
        if (Util.a((Object) format, (Object) this.q)) {
            return false;
        }
        this.q = format;
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00d1, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(long r6, int r8, long r9, int r11, com.google.android.exoplayer2.extractor.TrackOutput.CryptoData r12) {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = r5.o     // Catch:{ all -> 0x00d2 }
            r1 = 0
            if (r0 == 0) goto L_0x000e
            r0 = r8 & 1
            if (r0 != 0) goto L_0x000c
            monitor-exit(r5)
            return
        L_0x000c:
            r5.o = r1     // Catch:{ all -> 0x00d2 }
        L_0x000e:
            boolean r0 = r5.p     // Catch:{ all -> 0x00d2 }
            r2 = 1
            if (r0 != 0) goto L_0x0015
            r0 = 1
            goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            com.google.android.exoplayer2.util.Assertions.b(r0)     // Catch:{ all -> 0x00d2 }
            r5.b((long) r6)     // Catch:{ all -> 0x00d2 }
            int r0 = r5.i     // Catch:{ all -> 0x00d2 }
            int r0 = r5.f(r0)     // Catch:{ all -> 0x00d2 }
            long[] r3 = r5.f     // Catch:{ all -> 0x00d2 }
            r3[r0] = r6     // Catch:{ all -> 0x00d2 }
            long[] r6 = r5.c     // Catch:{ all -> 0x00d2 }
            r6[r0] = r9     // Catch:{ all -> 0x00d2 }
            int[] r6 = r5.d     // Catch:{ all -> 0x00d2 }
            r6[r0] = r11     // Catch:{ all -> 0x00d2 }
            int[] r6 = r5.e     // Catch:{ all -> 0x00d2 }
            r6[r0] = r8     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.extractor.TrackOutput$CryptoData[] r6 = r5.g     // Catch:{ all -> 0x00d2 }
            r6[r0] = r12     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.Format[] r6 = r5.h     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.Format r7 = r5.q     // Catch:{ all -> 0x00d2 }
            r6[r0] = r7     // Catch:{ all -> 0x00d2 }
            int[] r6 = r5.b     // Catch:{ all -> 0x00d2 }
            int r7 = r5.r     // Catch:{ all -> 0x00d2 }
            r6[r0] = r7     // Catch:{ all -> 0x00d2 }
            int r6 = r5.i     // Catch:{ all -> 0x00d2 }
            int r6 = r6 + r2
            r5.i = r6     // Catch:{ all -> 0x00d2 }
            int r6 = r5.i     // Catch:{ all -> 0x00d2 }
            int r7 = r5.f3418a     // Catch:{ all -> 0x00d2 }
            if (r6 != r7) goto L_0x00d0
            int r6 = r5.f3418a     // Catch:{ all -> 0x00d2 }
            int r6 = r6 + 1000
            int[] r7 = new int[r6]     // Catch:{ all -> 0x00d2 }
            long[] r8 = new long[r6]     // Catch:{ all -> 0x00d2 }
            long[] r9 = new long[r6]     // Catch:{ all -> 0x00d2 }
            int[] r10 = new int[r6]     // Catch:{ all -> 0x00d2 }
            int[] r11 = new int[r6]     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.extractor.TrackOutput$CryptoData[] r12 = new com.google.android.exoplayer2.extractor.TrackOutput.CryptoData[r6]     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.Format[] r0 = new com.google.android.exoplayer2.Format[r6]     // Catch:{ all -> 0x00d2 }
            int r2 = r5.f3418a     // Catch:{ all -> 0x00d2 }
            int r3 = r5.k     // Catch:{ all -> 0x00d2 }
            int r2 = r2 - r3
            long[] r3 = r5.c     // Catch:{ all -> 0x00d2 }
            int r4 = r5.k     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r3, r4, r8, r1, r2)     // Catch:{ all -> 0x00d2 }
            long[] r3 = r5.f     // Catch:{ all -> 0x00d2 }
            int r4 = r5.k     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r3, r4, r9, r1, r2)     // Catch:{ all -> 0x00d2 }
            int[] r3 = r5.e     // Catch:{ all -> 0x00d2 }
            int r4 = r5.k     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r3, r4, r10, r1, r2)     // Catch:{ all -> 0x00d2 }
            int[] r3 = r5.d     // Catch:{ all -> 0x00d2 }
            int r4 = r5.k     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r3, r4, r11, r1, r2)     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.extractor.TrackOutput$CryptoData[] r3 = r5.g     // Catch:{ all -> 0x00d2 }
            int r4 = r5.k     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r3, r4, r12, r1, r2)     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.Format[] r3 = r5.h     // Catch:{ all -> 0x00d2 }
            int r4 = r5.k     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r3, r4, r0, r1, r2)     // Catch:{ all -> 0x00d2 }
            int[] r3 = r5.b     // Catch:{ all -> 0x00d2 }
            int r4 = r5.k     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r3, r4, r7, r1, r2)     // Catch:{ all -> 0x00d2 }
            int r3 = r5.k     // Catch:{ all -> 0x00d2 }
            long[] r4 = r5.c     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r4, r1, r8, r2, r3)     // Catch:{ all -> 0x00d2 }
            long[] r4 = r5.f     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r4, r1, r9, r2, r3)     // Catch:{ all -> 0x00d2 }
            int[] r4 = r5.e     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r4, r1, r10, r2, r3)     // Catch:{ all -> 0x00d2 }
            int[] r4 = r5.d     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r4, r1, r11, r2, r3)     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.extractor.TrackOutput$CryptoData[] r4 = r5.g     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r4, r1, r12, r2, r3)     // Catch:{ all -> 0x00d2 }
            com.google.android.exoplayer2.Format[] r4 = r5.h     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r4, r1, r0, r2, r3)     // Catch:{ all -> 0x00d2 }
            int[] r4 = r5.b     // Catch:{ all -> 0x00d2 }
            java.lang.System.arraycopy(r4, r1, r7, r2, r3)     // Catch:{ all -> 0x00d2 }
            r5.c = r8     // Catch:{ all -> 0x00d2 }
            r5.f = r9     // Catch:{ all -> 0x00d2 }
            r5.e = r10     // Catch:{ all -> 0x00d2 }
            r5.d = r11     // Catch:{ all -> 0x00d2 }
            r5.g = r12     // Catch:{ all -> 0x00d2 }
            r5.h = r0     // Catch:{ all -> 0x00d2 }
            r5.b = r7     // Catch:{ all -> 0x00d2 }
            r5.k = r1     // Catch:{ all -> 0x00d2 }
            int r7 = r5.f3418a     // Catch:{ all -> 0x00d2 }
            r5.i = r7     // Catch:{ all -> 0x00d2 }
            r5.f3418a = r6     // Catch:{ all -> 0x00d2 }
        L_0x00d0:
            monitor-exit(r5)
            return
        L_0x00d2:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.SampleMetadataQueue.a(long, int, long, int, com.google.android.exoplayer2.extractor.TrackOutput$CryptoData):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000f, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean a(long r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            int r0 = r7.i     // Catch:{ all -> 0x004a }
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0010
            long r3 = r7.m     // Catch:{ all -> 0x004a }
            int r0 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x000e
            r1 = 1
        L_0x000e:
            monitor-exit(r7)
            return r1
        L_0x0010:
            long r3 = r7.m     // Catch:{ all -> 0x004a }
            int r0 = r7.l     // Catch:{ all -> 0x004a }
            long r5 = r7.e(r0)     // Catch:{ all -> 0x004a }
            long r3 = java.lang.Math.max(r3, r5)     // Catch:{ all -> 0x004a }
            int r0 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r0 < 0) goto L_0x0022
            monitor-exit(r7)
            return r1
        L_0x0022:
            int r0 = r7.i     // Catch:{ all -> 0x004a }
            int r1 = r7.i     // Catch:{ all -> 0x004a }
            int r1 = r1 - r2
            int r1 = r7.f(r1)     // Catch:{ all -> 0x004a }
        L_0x002b:
            int r3 = r7.l     // Catch:{ all -> 0x004a }
            if (r0 <= r3) goto L_0x0042
            long[] r3 = r7.f     // Catch:{ all -> 0x004a }
            r4 = r3[r1]     // Catch:{ all -> 0x004a }
            int r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r3 < 0) goto L_0x0042
            int r0 = r0 + -1
            int r1 = r1 + -1
            r3 = -1
            if (r1 != r3) goto L_0x002b
            int r1 = r7.f3418a     // Catch:{ all -> 0x004a }
            int r1 = r1 - r2
            goto L_0x002b
        L_0x0042:
            int r8 = r7.j     // Catch:{ all -> 0x004a }
            int r8 = r8 + r0
            r7.a((int) r8)     // Catch:{ all -> 0x004a }
            monitor-exit(r7)
            return r2
        L_0x004a:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.SampleMetadataQueue.a(long):boolean");
    }

    private int a(int i2, int i3, long j2, boolean z) {
        int i4 = i2;
        int i5 = -1;
        for (int i6 = 0; i6 < i3 && this.f[i4] <= j2; i6++) {
            if (!z || (this.e[i4] & 1) != 0) {
                i5 = i6;
            }
            i4++;
            if (i4 == this.f3418a) {
                i4 = 0;
            }
        }
        return i5;
    }
}
