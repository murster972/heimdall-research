package com.google.android.exoplayer2.source.hls;

import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSourceInputStream;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class Aes128DataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    private final DataSource f3477a;
    private final byte[] b;
    private final byte[] c;
    private CipherInputStream d;

    public Aes128DataSource(DataSource dataSource, byte[] bArr, byte[] bArr2) {
        this.f3477a = dataSource;
        this.b = bArr;
        this.c = bArr2;
    }

    public final void a(TransferListener transferListener) {
        this.f3477a.a(transferListener);
    }

    /* access modifiers changed from: protected */
    public Cipher b() throws NoSuchPaddingException, NoSuchAlgorithmException {
        return Cipher.getInstance("AES/CBC/PKCS7Padding");
    }

    public void close() throws IOException {
        if (this.d != null) {
            this.d = null;
            this.f3477a.close();
        }
    }

    public final Uri getUri() {
        return this.f3477a.getUri();
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        Assertions.a(this.d);
        int read = this.d.read(bArr, i, i2);
        if (read < 0) {
            return -1;
        }
        return read;
    }

    public final long a(DataSpec dataSpec) throws IOException {
        try {
            Cipher b2 = b();
            try {
                b2.init(2, new SecretKeySpec(this.b, "AES"), new IvParameterSpec(this.c));
                DataSourceInputStream dataSourceInputStream = new DataSourceInputStream(this.f3477a, dataSpec);
                this.d = new CipherInputStream(dataSourceInputStream, b2);
                dataSourceInputStream.s();
                return -1;
            } catch (InvalidAlgorithmParameterException | InvalidKeyException e) {
                throw new RuntimeException(e);
            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e2) {
            throw new RuntimeException(e2);
        }
    }

    public final Map<String, List<String>> a() {
        return this.f3477a.a();
    }
}
