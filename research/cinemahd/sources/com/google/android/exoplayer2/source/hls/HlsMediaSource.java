package com.google.android.exoplayer2.source.hls;

import android.net.Uri;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.BaseMediaSource;
import com.google.android.exoplayer2.source.CompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.DefaultCompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.SinglePeriodTimeline;
import com.google.android.exoplayer2.source.ads.AdsMediaSource$MediaSourceFactory;
import com.google.android.exoplayer2.source.hls.playlist.DefaultHlsPlaylistParserFactory;
import com.google.android.exoplayer2.source.hls.playlist.DefaultHlsPlaylistTracker;
import com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistParserFactory;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.List;

public final class HlsMediaSource extends BaseMediaSource implements HlsPlaylistTracker.PrimaryPlaylistListener {
    private final HlsExtractorFactory f;
    private final Uri g;
    private final HlsDataSourceFactory h;
    private final CompositeSequenceableLoaderFactory i;
    private final LoadErrorHandlingPolicy j;
    private final boolean k;
    private final HlsPlaylistTracker l;
    private final Object m;
    private TransferListener n;

    public static final class Factory implements AdsMediaSource$MediaSourceFactory {

        /* renamed from: a  reason: collision with root package name */
        private final HlsDataSourceFactory f3483a;
        private HlsExtractorFactory b;
        private HlsPlaylistParserFactory c;
        private HlsPlaylistTracker.Factory d;
        private CompositeSequenceableLoaderFactory e;
        private LoadErrorHandlingPolicy f;
        private boolean g;
        private boolean h;
        private Object i;

        public Factory(DataSource.Factory factory) {
            this((HlsDataSourceFactory) new DefaultHlsDataSourceFactory(factory));
        }

        public Factory a(HlsPlaylistParserFactory hlsPlaylistParserFactory) {
            Assertions.b(!this.h);
            Assertions.a(hlsPlaylistParserFactory);
            this.c = hlsPlaylistParserFactory;
            return this;
        }

        public Factory(HlsDataSourceFactory hlsDataSourceFactory) {
            Assertions.a(hlsDataSourceFactory);
            this.f3483a = hlsDataSourceFactory;
            this.c = new DefaultHlsPlaylistParserFactory();
            this.d = DefaultHlsPlaylistTracker.p;
            this.b = HlsExtractorFactory.f3481a;
            this.f = new DefaultLoadErrorHandlingPolicy();
            this.e = new DefaultCompositeSequenceableLoaderFactory();
        }

        public HlsMediaSource a(Uri uri) {
            this.h = true;
            HlsDataSourceFactory hlsDataSourceFactory = this.f3483a;
            HlsExtractorFactory hlsExtractorFactory = this.b;
            CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory = this.e;
            LoadErrorHandlingPolicy loadErrorHandlingPolicy = this.f;
            return new HlsMediaSource(uri, hlsDataSourceFactory, hlsExtractorFactory, compositeSequenceableLoaderFactory, loadErrorHandlingPolicy, this.d.a(hlsDataSourceFactory, loadErrorHandlingPolicy, this.c), this.g, this.i);
        }
    }

    static {
        ExoPlayerLibraryInfo.a("goog.exo.hls");
    }

    public void a(ExoPlayer exoPlayer, boolean z, TransferListener transferListener) {
        this.n = transferListener;
        this.l.a(this.g, a((MediaSource.MediaPeriodId) null), this);
    }

    public void b() throws IOException {
        this.l.c();
    }

    public void h() {
        this.l.stop();
    }

    private HlsMediaSource(Uri uri, HlsDataSourceFactory hlsDataSourceFactory, HlsExtractorFactory hlsExtractorFactory, CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory, LoadErrorHandlingPolicy loadErrorHandlingPolicy, HlsPlaylistTracker hlsPlaylistTracker, boolean z, Object obj) {
        this.g = uri;
        this.h = hlsDataSourceFactory;
        this.f = hlsExtractorFactory;
        this.i = compositeSequenceableLoaderFactory;
        this.j = loadErrorHandlingPolicy;
        this.l = hlsPlaylistTracker;
        this.k = z;
        this.m = obj;
    }

    public MediaPeriod a(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        return new HlsMediaPeriod(this.f, this.l, this.h, this.n, this.j, a(mediaPeriodId), allocator, this.i, this.k);
    }

    public void a(MediaPeriod mediaPeriod) {
        ((HlsMediaPeriod) mediaPeriod).h();
    }

    public void a(HlsMediaPlaylist hlsMediaPlaylist) {
        SinglePeriodTimeline singlePeriodTimeline;
        long j2;
        long j3;
        HlsMediaPlaylist hlsMediaPlaylist2 = hlsMediaPlaylist;
        long b = hlsMediaPlaylist2.m ? C.b(hlsMediaPlaylist2.f) : -9223372036854775807L;
        int i2 = hlsMediaPlaylist2.d;
        long j4 = (i2 == 2 || i2 == 1) ? b : -9223372036854775807L;
        long j5 = hlsMediaPlaylist2.e;
        if (this.l.isLive()) {
            long a2 = hlsMediaPlaylist2.f - this.l.a();
            long j6 = hlsMediaPlaylist2.l ? a2 + hlsMediaPlaylist2.p : -9223372036854775807L;
            List<HlsMediaPlaylist.Segment> list = hlsMediaPlaylist2.o;
            if (j5 == -9223372036854775807L) {
                if (list.isEmpty()) {
                    j3 = 0;
                } else {
                    j3 = list.get(Math.max(0, list.size() - 3)).e;
                }
                j2 = j3;
            } else {
                j2 = j5;
            }
            singlePeriodTimeline = new SinglePeriodTimeline(j4, b, j6, hlsMediaPlaylist2.p, a2, j2, true, !hlsMediaPlaylist2.l, this.m);
        } else {
            long j7 = j5 == -9223372036854775807L ? 0 : j5;
            long j8 = hlsMediaPlaylist2.p;
            singlePeriodTimeline = new SinglePeriodTimeline(j4, b, j8, j8, 0, j7, true, false, this.m);
        }
        a((Timeline) singlePeriodTimeline, (Object) new HlsManifest(this.l.b(), hlsMediaPlaylist2));
    }
}
