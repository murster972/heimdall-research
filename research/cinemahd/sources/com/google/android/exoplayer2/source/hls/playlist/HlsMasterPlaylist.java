package com.google.android.exoplayer2.source.hls.playlist;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.offline.StreamKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class HlsMasterPlaylist extends HlsPlaylist {
    public static final HlsMasterPlaylist j = new HlsMasterPlaylist("", Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), (Format) null, Collections.emptyList(), false, Collections.emptyMap());
    public final List<HlsUrl> d;
    public final List<HlsUrl> e;
    public final List<HlsUrl> f;
    public final Format g;
    public final List<Format> h;
    public final Map<String, String> i;

    public static final class HlsUrl {

        /* renamed from: a  reason: collision with root package name */
        public final String f3493a;
        public final Format b;

        public HlsUrl(String str, Format format) {
            this.f3493a = str;
            this.b = format;
        }

        public static HlsUrl a(String str) {
            return new HlsUrl(str, Format.a("0", (String) null, "application/x-mpegURL", (String) null, (String) null, -1, 0, (String) null));
        }
    }

    public HlsMasterPlaylist(String str, List<String> list, List<HlsUrl> list2, List<HlsUrl> list3, List<HlsUrl> list4, Format format, List<Format> list5, boolean z, Map<String, String> map) {
        super(str, list, z);
        this.d = Collections.unmodifiableList(list2);
        this.e = Collections.unmodifiableList(list3);
        this.f = Collections.unmodifiableList(list4);
        this.g = format;
        this.h = list5 != null ? Collections.unmodifiableList(list5) : null;
        this.i = Collections.unmodifiableMap(map);
    }

    public HlsMasterPlaylist a(List<StreamKey> list) {
        return new HlsMasterPlaylist(this.f3495a, this.b, a(this.d, 0, list), a(this.e, 1, list), a(this.f, 2, list), this.g, this.h, this.c, this.i);
    }

    public static HlsMasterPlaylist a(String str) {
        List singletonList = Collections.singletonList(HlsUrl.a(str));
        List emptyList = Collections.emptyList();
        return new HlsMasterPlaylist((String) null, Collections.emptyList(), singletonList, emptyList, emptyList, (Format) null, (List<Format>) null, false, Collections.emptyMap());
    }

    private static List<HlsUrl> a(List<HlsUrl> list, int i2, List<StreamKey> list2) {
        ArrayList arrayList = new ArrayList(list2.size());
        for (int i3 = 0; i3 < list.size(); i3++) {
            HlsUrl hlsUrl = list.get(i3);
            int i4 = 0;
            while (true) {
                if (i4 >= list2.size()) {
                    break;
                }
                StreamKey streamKey = list2.get(i4);
                if (streamKey.b == i2 && streamKey.c == i3) {
                    arrayList.add(hlsUrl);
                    break;
                }
                i4++;
            }
        }
        return arrayList;
    }
}
