package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* compiled from: lambda */
public final /* synthetic */ class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f3502a;
    private final /* synthetic */ MediaSourceEventListener b;
    private final /* synthetic */ MediaSource.MediaPeriodId c;

    public /* synthetic */ l(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
        this.f3502a = eventDispatcher;
        this.b = mediaSourceEventListener;
        this.c = mediaPeriodId;
    }

    public final void run() {
        this.f3502a.b(this.b, this.c);
    }
}
