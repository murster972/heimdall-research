package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.Timeline;

public abstract class ForwardingTimeline extends Timeline {
    protected final Timeline b;

    public ForwardingTimeline(Timeline timeline) {
        this.b = timeline;
    }

    public int a(int i, int i2, boolean z) {
        return this.b.a(i, i2, z);
    }

    public int b() {
        return this.b.b();
    }

    public int a(boolean z) {
        return this.b.a(z);
    }

    public int b(int i, int i2, boolean z) {
        return this.b.b(i, i2, z);
    }

    public Timeline.Window a(int i, Timeline.Window window, boolean z, long j) {
        return this.b.a(i, window, z, j);
    }

    public int b(boolean z) {
        return this.b.b(z);
    }

    public int a() {
        return this.b.a();
    }

    public Timeline.Period a(int i, Timeline.Period period, boolean z) {
        return this.b.a(i, period, z);
    }

    public int a(Object obj) {
        return this.b.a(obj);
    }

    public Object a(int i) {
        return this.b.a(i);
    }
}
