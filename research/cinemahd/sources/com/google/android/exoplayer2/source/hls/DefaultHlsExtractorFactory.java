package com.google.android.exoplayer2.source.hls;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor;
import com.google.android.exoplayer2.extractor.mp4.Track;
import com.google.android.exoplayer2.extractor.ts.Ac3Extractor;
import com.google.android.exoplayer2.extractor.ts.AdtsExtractor;
import com.google.android.exoplayer2.extractor.ts.DefaultTsPayloadReaderFactory;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import java.io.EOFException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class DefaultHlsExtractorFactory implements HlsExtractorFactory {
    private final int b;

    public DefaultHlsExtractorFactory() {
        this(0);
    }

    public Pair<Extractor, Boolean> a(Extractor extractor, Uri uri, Format format, List<Format> list, DrmInitData drmInitData, TimestampAdjuster timestampAdjuster, Map<String, List<String>> map, ExtractorInput extractorInput) throws InterruptedException, IOException {
        List<Format> list2;
        if (extractor == null) {
            Extractor a2 = a(uri, format, list, drmInitData, timestampAdjuster);
            extractorInput.a();
            if (a(a2, extractorInput)) {
                return a(a2);
            }
            if (!(a2 instanceof WebvttExtractor)) {
                WebvttExtractor webvttExtractor = new WebvttExtractor(format.z, timestampAdjuster);
                if (a(webvttExtractor, extractorInput)) {
                    return a(webvttExtractor);
                }
            }
            if (!(a2 instanceof AdtsExtractor)) {
                AdtsExtractor adtsExtractor = new AdtsExtractor();
                if (a(adtsExtractor, extractorInput)) {
                    return a(adtsExtractor);
                }
            }
            if (!(a2 instanceof Ac3Extractor)) {
                Ac3Extractor ac3Extractor = new Ac3Extractor();
                if (a(ac3Extractor, extractorInput)) {
                    return a(ac3Extractor);
                }
            }
            if (!(a2 instanceof Mp3Extractor)) {
                Mp3Extractor mp3Extractor = new Mp3Extractor(0, 0);
                if (a(mp3Extractor, extractorInput)) {
                    return a(mp3Extractor);
                }
            }
            if (!(a2 instanceof FragmentedMp4Extractor)) {
                if (list != null) {
                    list2 = list;
                } else {
                    list2 = Collections.emptyList();
                }
                FragmentedMp4Extractor fragmentedMp4Extractor = new FragmentedMp4Extractor(0, timestampAdjuster, (Track) null, drmInitData, list2);
                if (a(fragmentedMp4Extractor, extractorInput)) {
                    return a(fragmentedMp4Extractor);
                }
            }
            if (!(a2 instanceof TsExtractor)) {
                TsExtractor a3 = a(this.b, format, list, timestampAdjuster);
                if (a(a3, extractorInput)) {
                    return a(a3);
                }
            }
            return a(a2);
        } else if ((extractor instanceof TsExtractor) || (extractor instanceof FragmentedMp4Extractor)) {
            return a(extractor);
        } else {
            if (extractor instanceof WebvttExtractor) {
                return a(new WebvttExtractor(format.z, timestampAdjuster));
            }
            if (extractor instanceof AdtsExtractor) {
                return a(new AdtsExtractor());
            }
            if (extractor instanceof Ac3Extractor) {
                return a(new Ac3Extractor());
            }
            if (extractor instanceof Mp3Extractor) {
                return a(new Mp3Extractor());
            }
            throw new IllegalArgumentException("Unexpected previousExtractor type: " + extractor.getClass().getSimpleName());
        }
    }

    public DefaultHlsExtractorFactory(int i) {
        this.b = i;
    }

    private Extractor a(Uri uri, Format format, List<Format> list, DrmInitData drmInitData, TimestampAdjuster timestampAdjuster) {
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment == null) {
            lastPathSegment = "";
        }
        if ("text/vtt".equals(format.g) || lastPathSegment.endsWith(".webvtt") || lastPathSegment.endsWith(".vtt")) {
            return new WebvttExtractor(format.z, timestampAdjuster);
        }
        if (lastPathSegment.endsWith(".aac")) {
            return new AdtsExtractor();
        }
        if (lastPathSegment.endsWith(".ac3") || lastPathSegment.endsWith(".ec3")) {
            return new Ac3Extractor();
        }
        if (lastPathSegment.endsWith(".mp3")) {
            return new Mp3Extractor(0, 0);
        }
        if (!lastPathSegment.endsWith(".mp4") && !lastPathSegment.startsWith(".m4", lastPathSegment.length() - 4) && !lastPathSegment.startsWith(".mp4", lastPathSegment.length() - 5) && !lastPathSegment.startsWith(".cmf", lastPathSegment.length() - 5)) {
            return a(this.b, format, list, timestampAdjuster);
        }
        if (list == null) {
            list = Collections.emptyList();
        }
        return new FragmentedMp4Extractor(0, timestampAdjuster, (Track) null, drmInitData, list);
    }

    private static TsExtractor a(int i, Format format, List<Format> list, TimestampAdjuster timestampAdjuster) {
        int i2 = i | 16;
        if (list != null) {
            i2 |= 32;
        } else {
            list = Collections.singletonList(Format.a((String) null, "application/cea-608", 0, (String) null));
        }
        String str = format.d;
        if (!TextUtils.isEmpty(str)) {
            if (!"audio/mp4a-latm".equals(MimeTypes.a(str))) {
                i2 |= 2;
            }
            if (!"video/avc".equals(MimeTypes.i(str))) {
                i2 |= 4;
            }
        }
        return new TsExtractor(2, timestampAdjuster, new DefaultTsPayloadReaderFactory(i2, list));
    }

    private static Pair<Extractor, Boolean> a(Extractor extractor) {
        return new Pair<>(extractor, Boolean.valueOf((extractor instanceof AdtsExtractor) || (extractor instanceof Ac3Extractor) || (extractor instanceof Mp3Extractor)));
    }

    /* JADX INFO: finally extract failed */
    private static boolean a(Extractor extractor, ExtractorInput extractorInput) throws InterruptedException, IOException {
        try {
            boolean a2 = extractor.a(extractorInput);
            extractorInput.a();
            return a2;
        } catch (EOFException unused) {
            extractorInput.a();
            return false;
        } catch (Throwable th) {
            extractorInput.a();
            throw th;
        }
    }
}
