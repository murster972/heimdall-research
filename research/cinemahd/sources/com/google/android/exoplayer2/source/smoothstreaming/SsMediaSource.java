package com.google.android.exoplayer2.source.smoothstreaming;

import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.BaseMediaSource;
import com.google.android.exoplayer2.source.CompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.DefaultCompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.SinglePeriodTimeline;
import com.google.android.exoplayer2.source.ads.AdsMediaSource$MediaSourceFactory;
import com.google.android.exoplayer2.source.smoothstreaming.SsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifest;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifestParser;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsUtil;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.upstream.LoaderErrorThrower;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.ArrayList;

public final class SsMediaSource extends BaseMediaSource implements Loader.Callback<ParsingLoadable<SsManifest>> {
    private final boolean f;
    private final Uri g;
    private final DataSource.Factory h;
    private final SsChunkSource.Factory i;
    private final CompositeSequenceableLoaderFactory j;
    private final LoadErrorHandlingPolicy k;
    private final long l;
    private final MediaSourceEventListener.EventDispatcher m;
    private final ParsingLoadable.Parser<? extends SsManifest> n;
    private final ArrayList<SsMediaPeriod> o;
    private final Object p;
    private DataSource q;
    private Loader r;
    private LoaderErrorThrower s;
    private TransferListener t;
    private long u;
    private SsManifest v;
    private Handler w;

    static {
        ExoPlayerLibraryInfo.a("goog.exo.smoothstreaming");
    }

    private void k() {
        SinglePeriodTimeline singlePeriodTimeline;
        for (int i2 = 0; i2 < this.o.size(); i2++) {
            this.o.get(i2).a(this.v);
        }
        long j2 = Long.MIN_VALUE;
        long j3 = Long.MAX_VALUE;
        for (SsManifest.StreamElement streamElement : this.v.f) {
            if (streamElement.k > 0) {
                long min = Math.min(j3, streamElement.b(0));
                j2 = Math.max(j2, streamElement.b(streamElement.k - 1) + streamElement.a(streamElement.k - 1));
                j3 = min;
            }
        }
        if (j3 == Clock.MAX_TIME) {
            singlePeriodTimeline = new SinglePeriodTimeline(this.v.d ? -9223372036854775807L : 0, 0, 0, 0, true, this.v.d, this.p);
        } else {
            SsManifest ssManifest = this.v;
            if (ssManifest.d) {
                long j4 = ssManifest.h;
                if (j4 != -9223372036854775807L && j4 > 0) {
                    j3 = Math.max(j3, j2 - j4);
                }
                long j5 = j3;
                long j6 = j2 - j5;
                long a2 = j6 - C.a(this.l);
                if (a2 < 5000000) {
                    a2 = Math.min(5000000, j6 / 2);
                }
                singlePeriodTimeline = new SinglePeriodTimeline(-9223372036854775807L, j6, j5, a2, true, true, this.p);
            } else {
                long j7 = ssManifest.g;
                long j8 = j7 != -9223372036854775807L ? j7 : j2 - j3;
                singlePeriodTimeline = new SinglePeriodTimeline(j3 + j8, j8, j3, 0, true, false, this.p);
            }
        }
        a((Timeline) singlePeriodTimeline, (Object) this.v);
    }

    private void l() {
        if (this.v.d) {
            this.w.postDelayed(new a(this), Math.max(0, (this.u + 5000) - SystemClock.elapsedRealtime()));
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        ParsingLoadable parsingLoadable = new ParsingLoadable(this.q, this.g, 4, this.n);
        this.m.a(parsingLoadable.f3601a, parsingLoadable.b, this.r.a(parsingLoadable, this, this.k.a(parsingLoadable.b)));
    }

    public void b() throws IOException {
        this.s.a();
    }

    public void h() {
        this.v = this.f ? this.v : null;
        this.q = null;
        this.u = 0;
        Loader loader = this.r;
        if (loader != null) {
            loader.d();
            this.r = null;
        }
        Handler handler = this.w;
        if (handler != null) {
            handler.removeCallbacksAndMessages((Object) null);
            this.w = null;
        }
    }

    public static final class Factory implements AdsMediaSource$MediaSourceFactory {

        /* renamed from: a  reason: collision with root package name */
        private final SsChunkSource.Factory f3506a;
        private final DataSource.Factory b;
        private ParsingLoadable.Parser<? extends SsManifest> c;
        private CompositeSequenceableLoaderFactory d = new DefaultCompositeSequenceableLoaderFactory();
        private LoadErrorHandlingPolicy e = new DefaultLoadErrorHandlingPolicy();
        private long f = 30000;
        private boolean g;
        private Object h;

        public Factory(SsChunkSource.Factory factory, DataSource.Factory factory2) {
            Assertions.a(factory);
            this.f3506a = factory;
            this.b = factory2;
        }

        public Factory a(ParsingLoadable.Parser<? extends SsManifest> parser) {
            Assertions.b(!this.g);
            Assertions.a(parser);
            this.c = parser;
            return this;
        }

        public SsMediaSource a(Uri uri) {
            this.g = true;
            if (this.c == null) {
                this.c = new SsManifestParser();
            }
            Assertions.a(uri);
            return new SsMediaSource((SsManifest) null, uri, this.b, this.c, this.f3506a, this.d, this.e, this.f, this.h);
        }
    }

    private SsMediaSource(SsManifest ssManifest, Uri uri, DataSource.Factory factory, ParsingLoadable.Parser<? extends SsManifest> parser, SsChunkSource.Factory factory2, CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory, LoadErrorHandlingPolicy loadErrorHandlingPolicy, long j2, Object obj) {
        Uri uri2;
        boolean z = false;
        Assertions.b(ssManifest == null || !ssManifest.d);
        this.v = ssManifest;
        if (uri == null) {
            uri2 = null;
        } else {
            uri2 = SsUtil.a(uri);
        }
        this.g = uri2;
        this.h = factory;
        this.n = parser;
        this.i = factory2;
        this.j = compositeSequenceableLoaderFactory;
        this.k = loadErrorHandlingPolicy;
        this.l = j2;
        this.m = a((MediaSource.MediaPeriodId) null);
        this.p = obj;
        this.f = ssManifest != null ? true : z;
        this.o = new ArrayList<>();
    }

    public void a(ExoPlayer exoPlayer, boolean z, TransferListener transferListener) {
        this.t = transferListener;
        if (this.f) {
            this.s = new LoaderErrorThrower.Dummy();
            k();
            return;
        }
        this.q = this.h.a();
        this.r = new Loader("Loader:Manifest");
        this.s = this.r;
        this.w = new Handler();
        m();
    }

    public MediaPeriod a(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        SsMediaPeriod ssMediaPeriod = new SsMediaPeriod(this.v, this.i, this.t, this.j, this.k, a(mediaPeriodId), this.s, allocator);
        this.o.add(ssMediaPeriod);
        return ssMediaPeriod;
    }

    public void a(MediaPeriod mediaPeriod) {
        ((SsMediaPeriod) mediaPeriod).g();
        this.o.remove(mediaPeriod);
    }

    public void a(ParsingLoadable<SsManifest> parsingLoadable, long j2, long j3) {
        ParsingLoadable<SsManifest> parsingLoadable2 = parsingLoadable;
        this.m.b(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), parsingLoadable2.b, j2, j3, parsingLoadable.a());
        this.v = parsingLoadable.c();
        this.u = j2 - j3;
        k();
        l();
    }

    public void a(ParsingLoadable<SsManifest> parsingLoadable, long j2, long j3, boolean z) {
        ParsingLoadable<SsManifest> parsingLoadable2 = parsingLoadable;
        this.m.a(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), parsingLoadable2.b, j2, j3, parsingLoadable.a());
    }

    public Loader.LoadErrorAction a(ParsingLoadable<SsManifest> parsingLoadable, long j2, long j3, IOException iOException, int i2) {
        ParsingLoadable<SsManifest> parsingLoadable2 = parsingLoadable;
        IOException iOException2 = iOException;
        boolean z = iOException2 instanceof ParserException;
        this.m.a(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), parsingLoadable2.b, j2, j3, parsingLoadable.a(), iOException2, z);
        return z ? Loader.f : Loader.d;
    }
}
