package com.google.android.exoplayer2.source.hls;

import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;

final class HlsSampleStream implements SampleStream {

    /* renamed from: a  reason: collision with root package name */
    private final int f3484a;
    private final HlsSampleStreamWrapper b;
    private int c = -1;

    public HlsSampleStream(HlsSampleStreamWrapper hlsSampleStreamWrapper, int i) {
        this.b = hlsSampleStreamWrapper;
        this.f3484a = i;
    }

    public void a() throws IOException {
        if (this.c != -2) {
            this.b.i();
            return;
        }
        throw new SampleQueueMappingException(this.b.e().a(this.f3484a).a(0).g);
    }

    public void b() {
        Assertions.a(this.c == -1);
        this.c = this.b.a(this.f3484a);
    }

    public void c() {
        if (this.c != -1) {
            this.b.c(this.f3484a);
            this.c = -1;
        }
    }

    public int d(long j) {
        if (d()) {
            return this.b.a(this.c, j);
        }
        return 0;
    }

    public boolean isReady() {
        return this.c == -3 || (d() && this.b.b(this.c));
    }

    private boolean d() {
        int i = this.c;
        return (i == -1 || i == -3 || i == -2) ? false : true;
    }

    public int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
        if (d()) {
            return this.b.a(this.c, formatHolder, decoderInputBuffer, z);
        }
        return -3;
    }
}
