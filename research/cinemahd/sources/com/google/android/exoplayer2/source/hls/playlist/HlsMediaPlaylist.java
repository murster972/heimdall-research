package com.google.android.exoplayer2.source.hls.playlist;

import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.offline.StreamKey;
import java.util.Collections;
import java.util.List;

public final class HlsMediaPlaylist extends HlsPlaylist {
    public final int d;
    public final long e;
    public final long f;
    public final boolean g;
    public final int h;
    public final long i;
    public final int j;
    public final long k;
    public final boolean l;
    public final boolean m;
    public final DrmInitData n;
    public final List<Segment> o;
    public final long p;

    public static final class Segment implements Comparable<Long> {

        /* renamed from: a  reason: collision with root package name */
        public final String f3494a;
        public final Segment b;
        public final long c;
        public final int d;
        public final long e;
        public final DrmInitData f;
        public final String g;
        public final String h;
        public final long i;
        public final long j;
        public final boolean k;

        public Segment(String str, long j2, long j3) {
            this(str, (Segment) null, "", 0, -1, -9223372036854775807L, (DrmInitData) null, (String) null, (String) null, j2, j3, false);
        }

        /* renamed from: a */
        public int compareTo(Long l) {
            if (this.e > l.longValue()) {
                return 1;
            }
            return this.e < l.longValue() ? -1 : 0;
        }

        public Segment(String str, Segment segment, String str2, long j2, int i2, long j3, DrmInitData drmInitData, String str3, String str4, long j4, long j5, boolean z) {
            this.f3494a = str;
            this.b = segment;
            this.c = j2;
            this.d = i2;
            this.e = j3;
            this.f = drmInitData;
            this.g = str3;
            this.h = str4;
            this.i = j4;
            this.j = j5;
            this.k = z;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HlsMediaPlaylist(int i2, String str, List<String> list, long j2, long j3, boolean z, int i3, long j4, int i4, long j5, boolean z2, boolean z3, boolean z4, DrmInitData drmInitData, List<Segment> list2) {
        super(str, list, z2);
        String str2 = str;
        List<String> list3 = list;
        this.d = i2;
        this.f = j3;
        this.g = z;
        this.h = i3;
        this.i = j4;
        this.j = i4;
        this.k = j5;
        this.l = z3;
        this.m = z4;
        this.n = drmInitData;
        this.o = Collections.unmodifiableList(list2);
        if (!list2.isEmpty()) {
            Segment segment = list2.get(list2.size() - 1);
            this.p = segment.e + segment.c;
        } else {
            this.p = 0;
        }
        this.e = j2 == -9223372036854775807L ? -9223372036854775807L : j2 >= 0 ? j2 : this.p + j2;
    }

    public HlsMediaPlaylist a(List<StreamKey> list) {
        return this;
    }

    public long b() {
        return this.f + this.p;
    }

    public boolean a(HlsMediaPlaylist hlsMediaPlaylist) {
        if (hlsMediaPlaylist == null) {
            return true;
        }
        long j2 = this.i;
        long j3 = hlsMediaPlaylist.i;
        if (j2 > j3) {
            return true;
        }
        if (j2 < j3) {
            return false;
        }
        int size = this.o.size();
        int size2 = hlsMediaPlaylist.o.size();
        if (size > size2) {
            return true;
        }
        if (size != size2 || !this.l || hlsMediaPlaylist.l) {
            return false;
        }
        return true;
    }

    public HlsMediaPlaylist a(long j2, int i2) {
        return new HlsMediaPlaylist(this.d, this.f3495a, this.b, this.e, j2, true, i2, this.i, this.j, this.k, this.c, this.l, this.m, this.n, this.o);
    }

    public HlsMediaPlaylist a() {
        if (this.l) {
            return this;
        }
        HlsMediaPlaylist hlsMediaPlaylist = r2;
        HlsMediaPlaylist hlsMediaPlaylist2 = new HlsMediaPlaylist(this.d, this.f3495a, this.b, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.c, true, this.m, this.n, this.o);
        return hlsMediaPlaylist;
    }
}
