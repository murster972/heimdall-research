package com.google.android.exoplayer2.source;

import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.CryptoInfo;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.source.SampleMetadataQueue;
import com.google.android.exoplayer2.upstream.Allocation;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;

public class SampleQueue implements TrackOutput {

    /* renamed from: a  reason: collision with root package name */
    private final Allocator f3420a;
    private final int b;
    private final SampleMetadataQueue c = new SampleMetadataQueue();
    private final SampleMetadataQueue.SampleExtrasHolder d = new SampleMetadataQueue.SampleExtrasHolder();
    private final ParsableByteArray e = new ParsableByteArray(32);
    private AllocationNode f = new AllocationNode(0, this.b);
    private AllocationNode g;
    private AllocationNode h;
    private Format i;
    private boolean j;
    private Format k;
    private long l;
    private long m;
    private boolean n;
    private UpstreamFormatChangedListener o;

    public interface UpstreamFormatChangedListener {
        void a(Format format);
    }

    public SampleQueue(Allocator allocator) {
        this.f3420a = allocator;
        this.b = allocator.c();
        AllocationNode allocationNode = this.f;
        this.g = allocationNode;
        this.h = allocationNode;
    }

    public void a(boolean z) {
        this.c.a(z);
        a(this.f);
        this.f = new AllocationNode(0, this.b);
        AllocationNode allocationNode = this.f;
        this.g = allocationNode;
        this.h = allocationNode;
        this.m = 0;
        this.f3420a.b();
    }

    public void b(long j2, boolean z, boolean z2) {
        c(this.c.b(j2, z, z2));
    }

    public void c(int i2) {
        this.c.c(i2);
    }

    public int d() {
        return this.c.d();
    }

    public long e() {
        return this.c.e();
    }

    public long f() {
        return this.c.f();
    }

    public int g() {
        return this.c.g();
    }

    public Format h() {
        return this.c.h();
    }

    public int i() {
        return this.c.i();
    }

    public boolean j() {
        return this.c.j();
    }

    public int k() {
        return this.c.k();
    }

    public void l() {
        a(false);
    }

    public void m() {
        this.c.l();
        this.g = this.f;
    }

    public void n() {
        this.n = true;
    }

    private void d(int i2) {
        this.m += (long) i2;
        long j2 = this.m;
        AllocationNode allocationNode = this.h;
        if (j2 == allocationNode.b) {
            this.h = allocationNode.e;
        }
    }

    private int e(int i2) {
        AllocationNode allocationNode = this.h;
        if (!allocationNode.c) {
            allocationNode.a(this.f3420a.a(), new AllocationNode(this.h.b, this.b));
        }
        return Math.min(i2, (int) (this.h.b - this.m));
    }

    public void b() {
        c(this.c.b());
    }

    public void c() {
        c(this.c.c());
    }

    private static final class AllocationNode {

        /* renamed from: a  reason: collision with root package name */
        public final long f3421a;
        public final long b;
        public boolean c;
        public Allocation d;
        public AllocationNode e;

        public AllocationNode(long j, int i) {
            this.f3421a = j;
            this.b = j + ((long) i);
        }

        public void a(Allocation allocation, AllocationNode allocationNode) {
            this.d = allocation;
            this.e = allocationNode;
            this.c = true;
        }

        public int a(long j) {
            return ((int) (j - this.f3421a)) + this.d.b;
        }

        public AllocationNode a() {
            this.d = null;
            AllocationNode allocationNode = this.e;
            this.e = null;
            return allocationNode;
        }
    }

    private void c(long j2) {
        AllocationNode allocationNode;
        if (j2 != -1) {
            while (true) {
                allocationNode = this.f;
                if (j2 < allocationNode.b) {
                    break;
                }
                this.f3420a.a(allocationNode.d);
                this.f = this.f.a();
            }
            if (this.g.f3421a < allocationNode.f3421a) {
                this.g = allocationNode;
            }
        }
    }

    public boolean b(int i2) {
        return this.c.b(i2);
    }

    private void b(long j2) {
        while (true) {
            AllocationNode allocationNode = this.g;
            if (j2 >= allocationNode.b) {
                this.g = allocationNode.e;
            } else {
                return;
            }
        }
    }

    public void a(int i2) {
        this.m = this.c.a(i2);
        long j2 = this.m;
        if (j2 != 0) {
            AllocationNode allocationNode = this.f;
            if (j2 != allocationNode.f3421a) {
                while (this.m > allocationNode.b) {
                    allocationNode = allocationNode.e;
                }
                AllocationNode allocationNode2 = allocationNode.e;
                a(allocationNode2);
                allocationNode.e = new AllocationNode(allocationNode.b, this.b);
                this.h = this.m == allocationNode.b ? allocationNode.e : allocationNode;
                if (this.g == allocationNode2) {
                    this.g = allocationNode.e;
                    return;
                }
                return;
            }
        }
        a(this.f);
        this.f = new AllocationNode(this.m, this.b);
        AllocationNode allocationNode3 = this.f;
        this.g = allocationNode3;
        this.h = allocationNode3;
    }

    public int a() {
        return this.c.a();
    }

    public int a(long j2, boolean z, boolean z2) {
        return this.c.a(j2, z, z2);
    }

    public int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z, boolean z2, long j2) {
        int a2 = this.c.a(formatHolder, decoderInputBuffer, z, z2, this.i, this.d);
        if (a2 == -5) {
            this.i = formatHolder.f3165a;
            return -5;
        } else if (a2 == -4) {
            if (!decoderInputBuffer.isEndOfStream()) {
                if (decoderInputBuffer.c < j2) {
                    decoderInputBuffer.addFlag(Integer.MIN_VALUE);
                }
                if (decoderInputBuffer.c()) {
                    a(decoderInputBuffer, this.d);
                }
                decoderInputBuffer.b(this.d.f3419a);
                SampleMetadataQueue.SampleExtrasHolder sampleExtrasHolder = this.d;
                a(sampleExtrasHolder.b, decoderInputBuffer.b, sampleExtrasHolder.f3419a);
            }
            return -4;
        } else if (a2 == -3) {
            return -3;
        } else {
            throw new IllegalStateException();
        }
    }

    private void a(DecoderInputBuffer decoderInputBuffer, SampleMetadataQueue.SampleExtrasHolder sampleExtrasHolder) {
        int i2;
        DecoderInputBuffer decoderInputBuffer2 = decoderInputBuffer;
        SampleMetadataQueue.SampleExtrasHolder sampleExtrasHolder2 = sampleExtrasHolder;
        long j2 = sampleExtrasHolder2.b;
        this.e.c(1);
        a(j2, this.e.f3641a, 1);
        long j3 = j2 + 1;
        byte b2 = this.e.f3641a[0];
        boolean z = (b2 & 128) != 0;
        byte b3 = b2 & Byte.MAX_VALUE;
        CryptoInfo cryptoInfo = decoderInputBuffer2.f3216a;
        if (cryptoInfo.f3213a == null) {
            cryptoInfo.f3213a = new byte[16];
        }
        a(j3, decoderInputBuffer2.f3216a.f3213a, (int) b3);
        long j4 = j3 + ((long) b3);
        if (z) {
            this.e.c(2);
            a(j4, this.e.f3641a, 2);
            j4 += 2;
            i2 = this.e.z();
        } else {
            i2 = 1;
        }
        int[] iArr = decoderInputBuffer2.f3216a.d;
        if (iArr == null || iArr.length < i2) {
            iArr = new int[i2];
        }
        int[] iArr2 = iArr;
        int[] iArr3 = decoderInputBuffer2.f3216a.e;
        if (iArr3 == null || iArr3.length < i2) {
            iArr3 = new int[i2];
        }
        int[] iArr4 = iArr3;
        if (z) {
            int i3 = i2 * 6;
            this.e.c(i3);
            a(j4, this.e.f3641a, i3);
            j4 += (long) i3;
            this.e.e(0);
            for (int i4 = 0; i4 < i2; i4++) {
                iArr2[i4] = this.e.z();
                iArr4[i4] = this.e.x();
            }
        } else {
            iArr2[0] = 0;
            iArr4[0] = sampleExtrasHolder2.f3419a - ((int) (j4 - sampleExtrasHolder2.b));
        }
        TrackOutput.CryptoData cryptoData = sampleExtrasHolder2.c;
        CryptoInfo cryptoInfo2 = decoderInputBuffer2.f3216a;
        cryptoInfo2.a(i2, iArr2, iArr4, cryptoData.b, cryptoInfo2.f3213a, cryptoData.f3258a, cryptoData.c, cryptoData.d);
        long j5 = sampleExtrasHolder2.b;
        int i5 = (int) (j4 - j5);
        sampleExtrasHolder2.b = j5 + ((long) i5);
        sampleExtrasHolder2.f3419a -= i5;
    }

    private void a(long j2, ByteBuffer byteBuffer, int i2) {
        b(j2);
        while (i2 > 0) {
            int min = Math.min(i2, (int) (this.g.b - j2));
            AllocationNode allocationNode = this.g;
            byteBuffer.put(allocationNode.d.f3583a, allocationNode.a(j2), min);
            i2 -= min;
            j2 += (long) min;
            AllocationNode allocationNode2 = this.g;
            if (j2 == allocationNode2.b) {
                this.g = allocationNode2.e;
            }
        }
    }

    private void a(long j2, byte[] bArr, int i2) {
        b(j2);
        long j3 = j2;
        int i3 = i2;
        while (i3 > 0) {
            int min = Math.min(i3, (int) (this.g.b - j3));
            AllocationNode allocationNode = this.g;
            System.arraycopy(allocationNode.d.f3583a, allocationNode.a(j3), bArr, i2 - i3, min);
            i3 -= min;
            j3 += (long) min;
            AllocationNode allocationNode2 = this.g;
            if (j3 == allocationNode2.b) {
                this.g = allocationNode2.e;
            }
        }
    }

    public void a(UpstreamFormatChangedListener upstreamFormatChangedListener) {
        this.o = upstreamFormatChangedListener;
    }

    public void a(long j2) {
        if (this.l != j2) {
            this.l = j2;
            this.j = true;
        }
    }

    public void a(Format format) {
        Format a2 = a(format, this.l);
        boolean a3 = this.c.a(a2);
        this.k = format;
        this.j = false;
        UpstreamFormatChangedListener upstreamFormatChangedListener = this.o;
        if (upstreamFormatChangedListener != null && a3) {
            upstreamFormatChangedListener.a(a2);
        }
    }

    public int a(ExtractorInput extractorInput, int i2, boolean z) throws IOException, InterruptedException {
        int e2 = e(i2);
        AllocationNode allocationNode = this.h;
        int read = extractorInput.read(allocationNode.d.f3583a, allocationNode.a(this.m), e2);
        if (read != -1) {
            d(read);
            return read;
        } else if (z) {
            return -1;
        } else {
            throw new EOFException();
        }
    }

    public void a(ParsableByteArray parsableByteArray, int i2) {
        while (i2 > 0) {
            int e2 = e(i2);
            AllocationNode allocationNode = this.h;
            parsableByteArray.a(allocationNode.d.f3583a, allocationNode.a(this.m), e2);
            i2 -= e2;
            d(e2);
        }
    }

    public void a(long j2, int i2, int i3, int i4, TrackOutput.CryptoData cryptoData) {
        if (this.j) {
            a(this.k);
        }
        long j3 = j2 + this.l;
        if (this.n) {
            if ((i2 & 1) != 0 && this.c.a(j3)) {
                this.n = false;
            } else {
                return;
            }
        }
        int i5 = i3;
        this.c.a(j3, i2, (this.m - ((long) i5)) - ((long) i4), i5, cryptoData);
    }

    private void a(AllocationNode allocationNode) {
        if (allocationNode.c) {
            AllocationNode allocationNode2 = this.h;
            Allocation[] allocationArr = new Allocation[((allocationNode2.c ? 1 : 0) + (((int) (allocationNode2.f3421a - allocationNode.f3421a)) / this.b))];
            for (int i2 = 0; i2 < allocationArr.length; i2++) {
                allocationArr[i2] = allocationNode.d;
                allocationNode = allocationNode.a();
            }
            this.f3420a.a(allocationArr);
        }
    }

    private static Format a(Format format, long j2) {
        if (format == null) {
            return null;
        }
        if (j2 == 0) {
            return format;
        }
        long j3 = format.k;
        return j3 != Clock.MAX_TIME ? format.a(j3 + j2) : format;
    }
}
