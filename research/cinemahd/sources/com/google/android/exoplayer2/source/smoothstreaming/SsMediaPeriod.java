package com.google.android.exoplayer2.source.smoothstreaming;

import android.util.Base64;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox;
import com.google.android.exoplayer2.source.CompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.SequenceableLoader;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.chunk.ChunkSampleStream;
import com.google.android.exoplayer2.source.smoothstreaming.SsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifest;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.LoaderErrorThrower;
import com.google.android.exoplayer2.upstream.TransferListener;
import java.io.IOException;
import java.util.ArrayList;

final class SsMediaPeriod implements MediaPeriod, SequenceableLoader.Callback<ChunkSampleStream<SsChunkSource>> {

    /* renamed from: a  reason: collision with root package name */
    private final SsChunkSource.Factory f3505a;
    private final TransferListener b;
    private final LoaderErrorThrower c;
    private final LoadErrorHandlingPolicy d;
    private final MediaSourceEventListener.EventDispatcher e;
    private final Allocator f;
    private final TrackGroupArray g;
    private final TrackEncryptionBox[] h;
    private final CompositeSequenceableLoaderFactory i;
    private MediaPeriod.Callback j;
    private SsManifest k;
    private ChunkSampleStream<SsChunkSource>[] l;
    private SequenceableLoader m;
    private boolean n;

    public SsMediaPeriod(SsManifest ssManifest, SsChunkSource.Factory factory, TransferListener transferListener, CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory, LoadErrorHandlingPolicy loadErrorHandlingPolicy, MediaSourceEventListener.EventDispatcher eventDispatcher, LoaderErrorThrower loaderErrorThrower, Allocator allocator) {
        SsManifest ssManifest2 = ssManifest;
        CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory2 = compositeSequenceableLoaderFactory;
        this.f3505a = factory;
        this.b = transferListener;
        this.c = loaderErrorThrower;
        this.d = loadErrorHandlingPolicy;
        this.e = eventDispatcher;
        this.f = allocator;
        this.i = compositeSequenceableLoaderFactory2;
        this.g = b(ssManifest);
        SsManifest.ProtectionElement protectionElement = ssManifest2.e;
        if (protectionElement != null) {
            this.h = new TrackEncryptionBox[]{new TrackEncryptionBox(true, (String) null, 8, a(protectionElement.b), 0, 0, (byte[]) null)};
        } else {
            this.h = null;
        }
        this.k = ssManifest2;
        this.l = a(0);
        this.m = compositeSequenceableLoaderFactory2.a(this.l);
        eventDispatcher.a();
    }

    public boolean b(long j2) {
        return this.m.b(j2);
    }

    public void c(long j2) {
        this.m.c(j2);
    }

    public void d() throws IOException {
        this.c.a();
    }

    public TrackGroupArray e() {
        return this.g;
    }

    public long f() {
        return this.m.f();
    }

    public void g() {
        for (ChunkSampleStream<SsChunkSource> j2 : this.l) {
            j2.j();
        }
        this.j = null;
        this.e.b();
    }

    public void a(SsManifest ssManifest) {
        this.k = ssManifest;
        for (ChunkSampleStream<SsChunkSource> h2 : this.l) {
            h2.h().a(ssManifest);
        }
        this.j.a(this);
    }

    public long b() {
        return this.m.b();
    }

    public long c() {
        if (this.n) {
            return -9223372036854775807L;
        }
        this.e.c();
        this.n = true;
        return -9223372036854775807L;
    }

    private static TrackGroupArray b(SsManifest ssManifest) {
        TrackGroup[] trackGroupArr = new TrackGroup[ssManifest.f.length];
        int i2 = 0;
        while (true) {
            SsManifest.StreamElement[] streamElementArr = ssManifest.f;
            if (i2 >= streamElementArr.length) {
                return new TrackGroupArray(trackGroupArr);
            }
            trackGroupArr[i2] = new TrackGroup(streamElementArr[i2].j);
            i2++;
        }
    }

    public void a(MediaPeriod.Callback callback, long j2) {
        this.j = callback;
        callback.a(this);
    }

    public long a(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr, boolean[] zArr2, long j2) {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < trackSelectionArr.length; i2++) {
            if (sampleStreamArr[i2] != null) {
                ChunkSampleStream chunkSampleStream = sampleStreamArr[i2];
                if (trackSelectionArr[i2] == null || !zArr[i2]) {
                    chunkSampleStream.j();
                    sampleStreamArr[i2] = null;
                } else {
                    arrayList.add(chunkSampleStream);
                }
            }
            if (sampleStreamArr[i2] == null && trackSelectionArr[i2] != null) {
                ChunkSampleStream<SsChunkSource> a2 = a(trackSelectionArr[i2], j2);
                arrayList.add(a2);
                sampleStreamArr[i2] = a2;
                zArr2[i2] = true;
            }
        }
        this.l = a(arrayList.size());
        arrayList.toArray(this.l);
        this.m = this.i.a(this.l);
        return j2;
    }

    public void a(long j2, boolean z) {
        for (ChunkSampleStream<SsChunkSource> a2 : this.l) {
            a2.a(j2, z);
        }
    }

    public long a(long j2) {
        for (ChunkSampleStream<SsChunkSource> a2 : this.l) {
            a2.a(j2);
        }
        return j2;
    }

    public long a(long j2, SeekParameters seekParameters) {
        for (ChunkSampleStream<SsChunkSource> chunkSampleStream : this.l) {
            if (chunkSampleStream.f3435a == 2) {
                return chunkSampleStream.a(j2, seekParameters);
            }
        }
        return j2;
    }

    public void a(ChunkSampleStream<SsChunkSource> chunkSampleStream) {
        this.j.a(this);
    }

    private ChunkSampleStream<SsChunkSource> a(TrackSelection trackSelection, long j2) {
        int a2 = this.g.a(trackSelection.c());
        return new ChunkSampleStream(this.k.f[a2].f3510a, (int[]) null, (Format[]) null, this.f3505a.a(this.c, this.k, a2, trackSelection, this.h, this.b), this, this.f, j2, this.d, this.e);
    }

    private static ChunkSampleStream<SsChunkSource>[] a(int i2) {
        return new ChunkSampleStream[i2];
    }

    private static byte[] a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < bArr.length; i2 += 2) {
            sb.append((char) bArr[i2]);
        }
        String sb2 = sb.toString();
        byte[] decode = Base64.decode(sb2.substring(sb2.indexOf("<KID>") + 5, sb2.indexOf("</KID>")), 0);
        a(decode, 0, 3);
        a(decode, 1, 2);
        a(decode, 4, 5);
        a(decode, 6, 7);
        return decode;
    }

    private static void a(byte[] bArr, int i2, int i3) {
        byte b2 = bArr[i2];
        bArr[i2] = bArr[i3];
        bArr[i3] = b2;
    }
}
