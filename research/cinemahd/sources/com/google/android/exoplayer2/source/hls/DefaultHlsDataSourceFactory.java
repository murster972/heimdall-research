package com.google.android.exoplayer2.source.hls;

import com.google.android.exoplayer2.upstream.DataSource;

public final class DefaultHlsDataSourceFactory implements HlsDataSourceFactory {

    /* renamed from: a  reason: collision with root package name */
    private final DataSource.Factory f3478a;

    public DefaultHlsDataSourceFactory(DataSource.Factory factory) {
        this.f3478a = factory;
    }

    public DataSource a(int i) {
        return this.f3478a.a();
    }
}
