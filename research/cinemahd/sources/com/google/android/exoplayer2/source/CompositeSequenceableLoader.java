package com.google.android.exoplayer2.source;

import com.facebook.common.time.Clock;

public class CompositeSequenceableLoader implements SequenceableLoader {

    /* renamed from: a  reason: collision with root package name */
    protected final SequenceableLoader[] f3403a;

    public CompositeSequenceableLoader(SequenceableLoader[] sequenceableLoaderArr) {
        this.f3403a = sequenceableLoaderArr;
    }

    public final long b() {
        long j = Long.MAX_VALUE;
        for (SequenceableLoader b : this.f3403a) {
            long b2 = b.b();
            if (b2 != Long.MIN_VALUE) {
                j = Math.min(j, b2);
            }
        }
        if (j == Clock.MAX_TIME) {
            return Long.MIN_VALUE;
        }
        return j;
    }

    public final void c(long j) {
        for (SequenceableLoader c : this.f3403a) {
            c.c(j);
        }
    }

    public final long f() {
        long j = Long.MAX_VALUE;
        for (SequenceableLoader f : this.f3403a) {
            long f2 = f.f();
            if (f2 != Long.MIN_VALUE) {
                j = Math.min(j, f2);
            }
        }
        if (j == Clock.MAX_TIME) {
            return Long.MIN_VALUE;
        }
        return j;
    }

    public boolean b(long j) {
        long j2 = j;
        boolean z = false;
        while (true) {
            long b = b();
            if (b != Long.MIN_VALUE) {
                boolean z2 = false;
                for (SequenceableLoader sequenceableLoader : this.f3403a) {
                    long b2 = sequenceableLoader.b();
                    boolean z3 = b2 != Long.MIN_VALUE && b2 <= j2;
                    if (b2 == b || z3) {
                        z2 |= sequenceableLoader.b(j2);
                    }
                }
                z |= z2;
                if (!z2) {
                    break;
                }
            } else {
                break;
            }
        }
        return z;
    }
}
