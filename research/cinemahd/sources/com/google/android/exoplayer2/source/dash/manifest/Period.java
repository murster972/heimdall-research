package com.google.android.exoplayer2.source.dash.manifest;

import java.util.Collections;
import java.util.List;

public class Period {

    /* renamed from: a  reason: collision with root package name */
    public final String f3464a;
    public final long b;
    public final List<AdaptationSet> c;
    public final List<EventStream> d;

    public Period(String str, long j, List<AdaptationSet> list, List<EventStream> list2) {
        this.f3464a = str;
        this.b = j;
        this.c = Collections.unmodifiableList(list);
        this.d = Collections.unmodifiableList(list2);
    }

    public int a(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.c.get(i2).b == i) {
                return i2;
            }
        }
        return -1;
    }
}
