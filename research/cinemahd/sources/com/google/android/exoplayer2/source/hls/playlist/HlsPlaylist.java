package com.google.android.exoplayer2.source.hls.playlist;

import com.google.android.exoplayer2.offline.FilterableManifest;
import java.util.Collections;
import java.util.List;

public abstract class HlsPlaylist implements FilterableManifest<HlsPlaylist> {

    /* renamed from: a  reason: collision with root package name */
    public final String f3495a;
    public final List<String> b;
    public final boolean c;

    protected HlsPlaylist(String str, List<String> list, boolean z) {
        this.f3495a = str;
        this.b = Collections.unmodifiableList(list);
        this.c = z;
    }
}
