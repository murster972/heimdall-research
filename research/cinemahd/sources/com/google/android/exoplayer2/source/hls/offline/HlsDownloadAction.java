package com.google.android.exoplayer2.source.hls.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.offline.SegmentDownloadAction;
import com.google.android.exoplayer2.offline.StreamKey;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;

public final class HlsDownloadAction extends SegmentDownloadAction {
    public static final DownloadAction.Deserializer DESERIALIZER = new SegmentDownloadAction.SegmentDownloadActionDeserializer("hls", 1) {
        /* access modifiers changed from: protected */
        public DownloadAction a(Uri uri, boolean z, byte[] bArr, List<StreamKey> list) {
            return new HlsDownloadAction(uri, z, bArr, list);
        }

        /* access modifiers changed from: protected */
        public StreamKey b(int i, DataInputStream dataInputStream) throws IOException {
            if (i > 0) {
                return super.b(i, dataInputStream);
            }
            return new StreamKey(dataInputStream.readInt(), dataInputStream.readInt());
        }
    };

    @Deprecated
    public HlsDownloadAction(Uri uri, boolean z, byte[] bArr, List<StreamKey> list) {
        super("hls", 1, uri, z, bArr, list);
    }

    public HlsDownloader a(DownloaderConstructorHelper downloaderConstructorHelper) {
        return new HlsDownloader(this.c, this.g, downloaderConstructorHelper);
    }
}
