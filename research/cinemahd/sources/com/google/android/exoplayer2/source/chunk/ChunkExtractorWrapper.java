package com.google.android.exoplayer2.source.chunk;

import android.util.SparseArray;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.DummyTrackOutput;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;

public final class ChunkExtractorWrapper implements ExtractorOutput {

    /* renamed from: a  reason: collision with root package name */
    public final Extractor f3432a;
    private final int b;
    private final Format c;
    private final SparseArray<BindingTrackOutput> d = new SparseArray<>();
    private boolean e;
    private TrackOutputProvider f;
    private long g;
    private SeekMap h;
    private Format[] i;

    public interface TrackOutputProvider {
        TrackOutput a(int i, int i2);
    }

    public ChunkExtractorWrapper(Extractor extractor, int i2, Format format) {
        this.f3432a = extractor;
        this.b = i2;
        this.c = format;
    }

    public void a(TrackOutputProvider trackOutputProvider, long j, long j2) {
        this.f = trackOutputProvider;
        this.g = j2;
        if (!this.e) {
            this.f3432a.a((ExtractorOutput) this);
            if (j != -9223372036854775807L) {
                this.f3432a.a(0, j);
            }
            this.e = true;
            return;
        }
        Extractor extractor = this.f3432a;
        if (j == -9223372036854775807L) {
            j = 0;
        }
        extractor.a(0, j);
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            this.d.valueAt(i2).a(trackOutputProvider, j2);
        }
    }

    public Format[] b() {
        return this.i;
    }

    public SeekMap c() {
        return this.h;
    }

    private static final class BindingTrackOutput implements TrackOutput {

        /* renamed from: a  reason: collision with root package name */
        private final int f3433a;
        private final int b;
        private final Format c;
        private final DummyTrackOutput d = new DummyTrackOutput();
        public Format e;
        private TrackOutput f;
        private long g;

        public BindingTrackOutput(int i, int i2, Format format) {
            this.f3433a = i;
            this.b = i2;
            this.c = format;
        }

        public void a(TrackOutputProvider trackOutputProvider, long j) {
            if (trackOutputProvider == null) {
                this.f = this.d;
                return;
            }
            this.g = j;
            this.f = trackOutputProvider.a(this.f3433a, this.b);
            Format format = this.e;
            if (format != null) {
                this.f.a(format);
            }
        }

        public void a(Format format) {
            Format format2 = this.c;
            if (format2 != null) {
                format = format.a(format2);
            }
            this.e = format;
            this.f.a(this.e);
        }

        public int a(ExtractorInput extractorInput, int i, boolean z) throws IOException, InterruptedException {
            return this.f.a(extractorInput, i, z);
        }

        public void a(ParsableByteArray parsableByteArray, int i) {
            this.f.a(parsableByteArray, i);
        }

        public void a(long j, int i, int i2, int i3, TrackOutput.CryptoData cryptoData) {
            long j2 = this.g;
            if (j2 != -9223372036854775807L && j >= j2) {
                this.f = this.d;
            }
            this.f.a(j, i, i2, i3, cryptoData);
        }
    }

    public TrackOutput a(int i2, int i3) {
        BindingTrackOutput bindingTrackOutput = this.d.get(i2);
        if (bindingTrackOutput == null) {
            Assertions.b(this.i == null);
            bindingTrackOutput = new BindingTrackOutput(i2, i3, i3 == this.b ? this.c : null);
            bindingTrackOutput.a(this.f, this.g);
            this.d.put(i2, bindingTrackOutput);
        }
        return bindingTrackOutput;
    }

    public void a() {
        Format[] formatArr = new Format[this.d.size()];
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            formatArr[i2] = this.d.valueAt(i2).e;
        }
        this.i = formatArr;
    }

    public void a(SeekMap seekMap) {
        this.h = seekMap;
    }
}
