package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.DefaultExtractorInput;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public final class InitializationChunk extends Chunk {
    private static final PositionHolder l = new PositionHolder();
    private final ChunkExtractorWrapper i;
    private long j;
    private volatile boolean k;

    public InitializationChunk(DataSource dataSource, DataSpec dataSpec, Format format, int i2, Object obj, ChunkExtractorWrapper chunkExtractorWrapper) {
        super(dataSource, dataSpec, 2, format, i2, obj, -9223372036854775807L, -9223372036854775807L);
        this.i = chunkExtractorWrapper;
    }

    public void cancelLoad() {
        this.k = true;
    }

    public void load() throws IOException, InterruptedException {
        DefaultExtractorInput defaultExtractorInput;
        DataSpec a2 = this.f3431a.a(this.j);
        try {
            defaultExtractorInput = new DefaultExtractorInput(this.h, a2.d, this.h.a(a2));
            if (this.j == 0) {
                this.i.a((ChunkExtractorWrapper.TrackOutputProvider) null, -9223372036854775807L, -9223372036854775807L);
            }
            Extractor extractor = this.i.f3432a;
            int i2 = 0;
            while (i2 == 0 && !this.k) {
                i2 = extractor.a((ExtractorInput) defaultExtractorInput, l);
            }
            boolean z = true;
            if (i2 == 1) {
                z = false;
            }
            Assertions.b(z);
            this.j = defaultExtractorInput.getPosition() - this.f3431a.d;
            Util.a((DataSource) this.h);
        } catch (Throwable th) {
            Util.a((DataSource) this.h);
            throw th;
        }
    }
}
