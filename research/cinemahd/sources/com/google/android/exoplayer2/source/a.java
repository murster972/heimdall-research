package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;

/* compiled from: lambda */
public final /* synthetic */ class a implements MediaSource.SourceInfoRefreshListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CompositeMediaSource f3425a;
    private final /* synthetic */ Object b;

    public /* synthetic */ a(CompositeMediaSource compositeMediaSource, Object obj) {
        this.f3425a = compositeMediaSource;
        this.b = obj;
    }

    public final void a(MediaSource mediaSource, Timeline timeline, Object obj) {
        this.f3425a.a(this.b, mediaSource, timeline, obj);
    }
}
