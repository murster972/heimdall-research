package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import java.io.IOException;

public interface SampleStream {
    int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z);

    void a() throws IOException;

    int d(long j);

    boolean isReady();
}
