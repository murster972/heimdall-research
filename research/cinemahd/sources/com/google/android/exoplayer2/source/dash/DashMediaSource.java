package com.google.android.exoplayer2.source.dash;

import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.SparseArray;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.BaseMediaSource;
import com.google.android.exoplayer2.source.CompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.DefaultCompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.ads.AdsMediaSource$MediaSourceFactory;
import com.google.android.exoplayer2.source.dash.DashChunkSource;
import com.google.android.exoplayer2.source.dash.PlayerEmsgHandler;
import com.google.android.exoplayer2.source.dash.manifest.AdaptationSet;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.manifest.DashManifestParser;
import com.google.android.exoplayer2.source.dash.manifest.Period;
import com.google.android.exoplayer2.source.dash.manifest.UtcTimingElement;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.upstream.LoaderErrorThrower;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class DashMediaSource extends BaseMediaSource {
    private Handler A;
    private Uri B;
    private Uri C;
    private DashManifest D;
    private boolean E;
    private long F;
    private long G;
    private long H;
    private int I;
    private long J;
    private int K;
    private final boolean f;
    private final DataSource.Factory g;
    private final DashChunkSource.Factory h;
    private final CompositeSequenceableLoaderFactory i;
    private final LoadErrorHandlingPolicy j;
    private final long k;
    private final boolean l;
    private final MediaSourceEventListener.EventDispatcher m;
    private final ParsingLoadable.Parser<? extends DashManifest> n;
    private final ManifestCallback o;
    private final Object p;
    private final SparseArray<DashMediaPeriod> q;
    private final Runnable r;
    private final Runnable s;
    private final PlayerEmsgHandler.PlayerEmsgCallback t;
    private final LoaderErrorThrower u;
    private final Object v;
    private DataSource w;
    /* access modifiers changed from: private */
    public Loader x;
    private TransferListener y;
    /* access modifiers changed from: private */
    public IOException z;

    private static final class DashTimeline extends Timeline {
        private final long b;
        private final long c;
        private final int d;
        private final long e;
        private final long f;
        private final long g;
        private final DashManifest h;
        private final Object i;

        public DashTimeline(long j, long j2, int i2, long j3, long j4, long j5, DashManifest dashManifest, Object obj) {
            this.b = j;
            this.c = j2;
            this.d = i2;
            this.e = j3;
            this.f = j4;
            this.g = j5;
            this.h = dashManifest;
            this.i = obj;
        }

        public int a() {
            return this.h.a();
        }

        public int b() {
            return 1;
        }

        public Timeline.Period a(int i2, Timeline.Period period, boolean z) {
            Assertions.a(i2, 0, a());
            Integer num = null;
            String str = z ? this.h.a(i2).f3464a : null;
            if (z) {
                num = Integer.valueOf(this.d + i2);
            }
            return period.a(str, num, 0, this.h.c(i2), C.a(this.h.a(i2).b - this.h.a(0).b) - this.e);
        }

        public Timeline.Window a(int i2, Timeline.Window window, boolean z, long j) {
            Assertions.a(i2, 0, 1);
            long a2 = a(j);
            Object obj = z ? this.i : null;
            DashManifest dashManifest = this.h;
            return window.a(obj, this.b, this.c, true, dashManifest.d && dashManifest.e != -9223372036854775807L && dashManifest.b == -9223372036854775807L, a2, this.f, 0, a() - 1, this.e);
        }

        public int a(Object obj) {
            int intValue;
            if ((obj instanceof Integer) && (intValue = ((Integer) obj).intValue() - this.d) >= 0 && intValue < a()) {
                return intValue;
            }
            return -1;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0050, code lost:
            r9 = r9.c.get(r10).c.get(0).d();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private long a(long r9) {
            /*
                r8 = this;
                long r0 = r8.g
                com.google.android.exoplayer2.source.dash.manifest.DashManifest r2 = r8.h
                boolean r2 = r2.d
                if (r2 != 0) goto L_0x0009
                return r0
            L_0x0009:
                r2 = 0
                int r4 = (r9 > r2 ? 1 : (r9 == r2 ? 0 : -1))
                if (r4 <= 0) goto L_0x001c
                long r0 = r0 + r9
                long r9 = r8.f
                int r2 = (r0 > r9 ? 1 : (r0 == r9 ? 0 : -1))
                if (r2 <= 0) goto L_0x001c
                r9 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
                return r9
            L_0x001c:
                long r9 = r8.e
                long r9 = r9 + r0
                com.google.android.exoplayer2.source.dash.manifest.DashManifest r2 = r8.h
                r3 = 0
                long r4 = r2.c(r3)
                r6 = r4
                r4 = r9
                r9 = 0
            L_0x0029:
                com.google.android.exoplayer2.source.dash.manifest.DashManifest r10 = r8.h
                int r10 = r10.a()
                int r10 = r10 + -1
                if (r9 >= r10) goto L_0x0041
                int r10 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r10 < 0) goto L_0x0041
                long r4 = r4 - r6
                int r9 = r9 + 1
                com.google.android.exoplayer2.source.dash.manifest.DashManifest r10 = r8.h
                long r6 = r10.c(r9)
                goto L_0x0029
            L_0x0041:
                com.google.android.exoplayer2.source.dash.manifest.DashManifest r10 = r8.h
                com.google.android.exoplayer2.source.dash.manifest.Period r9 = r10.a((int) r9)
                r10 = 2
                int r10 = r9.a(r10)
                r2 = -1
                if (r10 != r2) goto L_0x0050
                return r0
            L_0x0050:
                java.util.List<com.google.android.exoplayer2.source.dash.manifest.AdaptationSet> r9 = r9.c
                java.lang.Object r9 = r9.get(r10)
                com.google.android.exoplayer2.source.dash.manifest.AdaptationSet r9 = (com.google.android.exoplayer2.source.dash.manifest.AdaptationSet) r9
                java.util.List<com.google.android.exoplayer2.source.dash.manifest.Representation> r9 = r9.c
                java.lang.Object r9 = r9.get(r3)
                com.google.android.exoplayer2.source.dash.manifest.Representation r9 = (com.google.android.exoplayer2.source.dash.manifest.Representation) r9
                com.google.android.exoplayer2.source.dash.DashSegmentIndex r9 = r9.d()
                if (r9 == 0) goto L_0x0077
                int r10 = r9.c(r6)
                if (r10 != 0) goto L_0x006d
                goto L_0x0077
            L_0x006d:
                long r2 = r9.b(r4, r6)
                long r9 = r9.a(r2)
                long r0 = r0 + r9
                long r0 = r0 - r4
            L_0x0077:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.DashMediaSource.DashTimeline.a(long):long");
        }

        public Object a(int i2) {
            Assertions.a(i2, 0, a());
            return Integer.valueOf(this.d + i2);
        }
    }

    private final class DefaultPlayerEmsgCallback implements PlayerEmsgHandler.PlayerEmsgCallback {
        private DefaultPlayerEmsgCallback() {
        }

        public void a() {
            DashMediaSource.this.l();
        }

        public void a(long j) {
            DashMediaSource.this.b(j);
        }
    }

    static final class Iso8601Parser implements ParsingLoadable.Parser<Long> {

        /* renamed from: a  reason: collision with root package name */
        private static final Pattern f3443a = Pattern.compile("(.+?)(Z|((\\+|-|−)(\\d\\d)(:?(\\d\\d))?))");

        Iso8601Parser() {
        }

        public Long a(Uri uri, InputStream inputStream) throws IOException {
            String readLine = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8"))).readLine();
            try {
                Matcher matcher = f3443a.matcher(readLine);
                if (matcher.matches()) {
                    String group = matcher.group(1);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    long time = simpleDateFormat.parse(group).getTime();
                    if (!"Z".equals(matcher.group(2))) {
                        long j = "+".equals(matcher.group(4)) ? 1 : -1;
                        long parseLong = Long.parseLong(matcher.group(5));
                        String group2 = matcher.group(7);
                        time -= j * ((((parseLong * 60) + (TextUtils.isEmpty(group2) ? 0 : Long.parseLong(group2))) * 60) * 1000);
                    }
                    return Long.valueOf(time);
                }
                throw new ParserException("Couldn't parse timestamp: " + readLine);
            } catch (ParseException e) {
                throw new ParserException((Throwable) e);
            }
        }
    }

    final class ManifestLoadErrorThrower implements LoaderErrorThrower {
        ManifestLoadErrorThrower() {
        }

        private void b() throws IOException {
            if (DashMediaSource.this.z != null) {
                throw DashMediaSource.this.z;
            }
        }

        public void a() throws IOException {
            DashMediaSource.this.x.a();
            b();
        }
    }

    private static final class PeriodSeekInfo {

        /* renamed from: a  reason: collision with root package name */
        public final boolean f3446a;
        public final long b;
        public final long c;

        private PeriodSeekInfo(boolean z, long j, long j2) {
            this.f3446a = z;
            this.b = j;
            this.c = j2;
        }

        public static PeriodSeekInfo a(Period period, long j) {
            boolean z;
            int i;
            boolean z2;
            Period period2 = period;
            long j2 = j;
            int size = period2.c.size();
            int i2 = 0;
            int i3 = 0;
            while (true) {
                if (i3 >= size) {
                    z = false;
                    break;
                }
                int i4 = period2.c.get(i3).b;
                if (i4 == 1 || i4 == 2) {
                    z = true;
                } else {
                    i3++;
                }
            }
            long j3 = Long.MAX_VALUE;
            int i5 = 0;
            boolean z3 = false;
            boolean z4 = false;
            long j4 = 0;
            while (i5 < size) {
                AdaptationSet adaptationSet = period2.c.get(i5);
                if (!z || adaptationSet.b != 3) {
                    DashSegmentIndex d = adaptationSet.c.get(i2).d();
                    if (d == null) {
                        return new PeriodSeekInfo(true, 0, j);
                    }
                    boolean a2 = d.a() | z4;
                    int c2 = d.c(j2);
                    if (c2 == 0) {
                        i = size;
                        z2 = z;
                        z4 = a2;
                        z3 = true;
                        j4 = 0;
                        j3 = 0;
                    } else {
                        if (!z3) {
                            z2 = z;
                            long b2 = d.b();
                            i = size;
                            long max = Math.max(j4, d.a(b2));
                            if (c2 != -1) {
                                long j5 = (b2 + ((long) c2)) - 1;
                                j4 = max;
                                j3 = Math.min(j3, d.a(j5) + d.a(j5, j2));
                            } else {
                                j4 = max;
                            }
                        } else {
                            i = size;
                            z2 = z;
                        }
                        z4 = a2;
                    }
                } else {
                    i = size;
                    z2 = z;
                }
                i5++;
                i2 = 0;
                period2 = period;
                z = z2;
                size = i;
            }
            return new PeriodSeekInfo(z4, j4, j3);
        }
    }

    private static final class XsDateTimeParser implements ParsingLoadable.Parser<Long> {
        private XsDateTimeParser() {
        }

        public Long a(Uri uri, InputStream inputStream) throws IOException {
            return Long.valueOf(Util.h(new BufferedReader(new InputStreamReader(inputStream)).readLine()));
        }
    }

    static {
        ExoPlayerLibraryInfo.a("goog.exo.dash");
    }

    private void d(long j2) {
        this.A.postDelayed(this.r, j2);
    }

    private long m() {
        return (long) Math.min((this.I - 1) * 1000, 5000);
    }

    private long n() {
        if (this.H != 0) {
            return C.a(SystemClock.elapsedRealtime() + this.H);
        }
        return C.a(System.currentTimeMillis());
    }

    /* access modifiers changed from: private */
    public void o() {
        Uri uri;
        this.A.removeCallbacks(this.r);
        if (this.x.c()) {
            this.E = true;
            return;
        }
        synchronized (this.p) {
            uri = this.C;
        }
        this.E = false;
        a(new ParsingLoadable(this.w, uri, 4, this.n), this.o, this.j.a(4));
    }

    /* access modifiers changed from: package-private */
    public void c(ParsingLoadable<Long> parsingLoadable, long j2, long j3) {
        ParsingLoadable<Long> parsingLoadable2 = parsingLoadable;
        this.m.b(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), parsingLoadable2.b, j2, j3, parsingLoadable.a());
        c(parsingLoadable.c().longValue() - j2);
    }

    public void h() {
        this.E = false;
        this.w = null;
        Loader loader = this.x;
        if (loader != null) {
            loader.d();
            this.x = null;
        }
        this.F = 0;
        this.G = 0;
        this.D = this.f ? this.D : null;
        this.C = this.B;
        this.z = null;
        Handler handler = this.A;
        if (handler != null) {
            handler.removeCallbacksAndMessages((Object) null);
            this.A = null;
        }
        this.H = 0;
        this.I = 0;
        this.J = -9223372036854775807L;
        this.K = 0;
        this.q.clear();
    }

    public /* synthetic */ void k() {
        a(false);
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.A.removeCallbacks(this.s);
        o();
    }

    public static final class Factory implements AdsMediaSource$MediaSourceFactory {

        /* renamed from: a  reason: collision with root package name */
        private final DashChunkSource.Factory f3442a;
        private final DataSource.Factory b;
        private ParsingLoadable.Parser<? extends DashManifest> c;
        private CompositeSequenceableLoaderFactory d = new DefaultCompositeSequenceableLoaderFactory();
        private LoadErrorHandlingPolicy e = new DefaultLoadErrorHandlingPolicy();
        private long f = 30000;
        private boolean g;
        private boolean h;
        private Object i;

        public Factory(DashChunkSource.Factory factory, DataSource.Factory factory2) {
            Assertions.a(factory);
            this.f3442a = factory;
            this.b = factory2;
        }

        public Factory a(ParsingLoadable.Parser<? extends DashManifest> parser) {
            Assertions.b(!this.h);
            Assertions.a(parser);
            this.c = parser;
            return this;
        }

        public DashMediaSource a(Uri uri) {
            this.h = true;
            if (this.c == null) {
                this.c = new DashManifestParser();
            }
            Assertions.a(uri);
            return new DashMediaSource((DashManifest) null, uri, this.b, this.c, this.f3442a, this.d, this.e, this.f, this.g, this.i);
        }
    }

    private DashMediaSource(DashManifest dashManifest, Uri uri, DataSource.Factory factory, ParsingLoadable.Parser<? extends DashManifest> parser, DashChunkSource.Factory factory2, CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory, LoadErrorHandlingPolicy loadErrorHandlingPolicy, long j2, boolean z2, Object obj) {
        this.B = uri;
        this.D = dashManifest;
        this.C = uri;
        this.g = factory;
        this.n = parser;
        this.h = factory2;
        this.j = loadErrorHandlingPolicy;
        this.k = j2;
        this.l = z2;
        this.i = compositeSequenceableLoaderFactory;
        this.v = obj;
        this.f = dashManifest != null;
        this.m = a((MediaSource.MediaPeriodId) null);
        this.p = new Object();
        this.q = new SparseArray<>();
        this.t = new DefaultPlayerEmsgCallback();
        this.J = -9223372036854775807L;
        if (this.f) {
            Assertions.b(!dashManifest.d);
            this.o = null;
            this.r = null;
            this.s = null;
            this.u = new LoaderErrorThrower.Dummy();
            return;
        }
        this.o = new ManifestCallback();
        this.u = new ManifestLoadErrorThrower();
        this.r = new a(this);
        this.s = new b(this);
    }

    public void a(ExoPlayer exoPlayer, boolean z2, TransferListener transferListener) {
        this.y = transferListener;
        if (this.f) {
            a(false);
            return;
        }
        this.w = this.g.a();
        this.x = new Loader("Loader:DashMediaSource");
        this.A = new Handler();
        o();
    }

    public void b() throws IOException {
        this.u.a();
    }

    private final class ManifestCallback implements Loader.Callback<ParsingLoadable<DashManifest>> {
        private ManifestCallback() {
        }

        public void a(ParsingLoadable<DashManifest> parsingLoadable, long j, long j2) {
            DashMediaSource.this.b(parsingLoadable, j, j2);
        }

        public void a(ParsingLoadable<DashManifest> parsingLoadable, long j, long j2, boolean z) {
            DashMediaSource.this.a((ParsingLoadable<?>) parsingLoadable, j, j2);
        }

        public Loader.LoadErrorAction a(ParsingLoadable<DashManifest> parsingLoadable, long j, long j2, IOException iOException, int i) {
            return DashMediaSource.this.a(parsingLoadable, j, j2, iOException);
        }
    }

    private final class UtcTimestampCallback implements Loader.Callback<ParsingLoadable<Long>> {
        private UtcTimestampCallback() {
        }

        public void a(ParsingLoadable<Long> parsingLoadable, long j, long j2) {
            DashMediaSource.this.c(parsingLoadable, j, j2);
        }

        public void a(ParsingLoadable<Long> parsingLoadable, long j, long j2, boolean z) {
            DashMediaSource.this.a((ParsingLoadable<?>) parsingLoadable, j, j2);
        }

        public Loader.LoadErrorAction a(ParsingLoadable<Long> parsingLoadable, long j, long j2, IOException iOException, int i) {
            return DashMediaSource.this.b(parsingLoadable, j, j2, iOException);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(long j2) {
        long j3 = this.J;
        if (j3 == -9223372036854775807L || j3 < j2) {
            this.J = j2;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.google.android.exoplayer2.upstream.ParsingLoadable<com.google.android.exoplayer2.source.dash.manifest.DashManifest> r18, long r19, long r21) {
        /*
            r17 = this;
            r1 = r17
            r0 = r18
            r13 = r19
            com.google.android.exoplayer2.source.MediaSourceEventListener$EventDispatcher r2 = r1.m
            com.google.android.exoplayer2.upstream.DataSpec r3 = r0.f3601a
            android.net.Uri r4 = r18.d()
            java.util.Map r5 = r18.b()
            int r6 = r0.b
            long r11 = r18.a()
            r7 = r19
            r9 = r21
            r2.b(r3, r4, r5, r6, r7, r9, r11)
            java.lang.Object r2 = r18.c()
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r2 = (com.google.android.exoplayer2.source.dash.manifest.DashManifest) r2
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r3 = r1.D
            r4 = 0
            if (r3 != 0) goto L_0x002c
            r3 = 0
            goto L_0x0030
        L_0x002c:
            int r3 = r3.a()
        L_0x0030:
            com.google.android.exoplayer2.source.dash.manifest.Period r5 = r2.a((int) r4)
            long r5 = r5.b
            r7 = 0
        L_0x0037:
            if (r7 >= r3) goto L_0x0048
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r8 = r1.D
            com.google.android.exoplayer2.source.dash.manifest.Period r8 = r8.a((int) r7)
            long r8 = r8.b
            int r10 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r10 >= 0) goto L_0x0048
            int r7 = r7 + 1
            goto L_0x0037
        L_0x0048:
            boolean r5 = r2.d
            r6 = 1
            if (r5 == 0) goto L_0x00bb
            int r5 = r3 - r7
            int r8 = r2.a()
            if (r5 <= r8) goto L_0x005e
            java.lang.String r5 = "DashMediaSource"
            java.lang.String r8 = "Loaded out of sync manifest"
            com.google.android.exoplayer2.util.Log.d(r5, r8)
        L_0x005c:
            r5 = 1
            goto L_0x0097
        L_0x005e:
            long r8 = r1.J
            r10 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r5 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r5 == 0) goto L_0x0096
            long r10 = r2.h
            r15 = 1000(0x3e8, double:4.94E-321)
            long r10 = r10 * r15
            int r5 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r5 > 0) goto L_0x0096
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = "Loaded stale dynamic manifest: "
            r5.append(r8)
            long r8 = r2.h
            r5.append(r8)
            java.lang.String r8 = ", "
            r5.append(r8)
            long r8 = r1.J
            r5.append(r8)
            java.lang.String r5 = r5.toString()
            java.lang.String r8 = "DashMediaSource"
            com.google.android.exoplayer2.util.Log.d(r8, r5)
            goto L_0x005c
        L_0x0096:
            r5 = 0
        L_0x0097:
            if (r5 == 0) goto L_0x00b9
            int r2 = r1.I
            int r3 = r2 + 1
            r1.I = r3
            com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy r3 = r1.j
            int r0 = r0.b
            int r0 = r3.a(r0)
            if (r2 >= r0) goto L_0x00b1
            long r2 = r17.m()
            r1.d(r2)
            goto L_0x00b8
        L_0x00b1:
            com.google.android.exoplayer2.source.dash.DashManifestStaleException r0 = new com.google.android.exoplayer2.source.dash.DashManifestStaleException
            r0.<init>()
            r1.z = r0
        L_0x00b8:
            return
        L_0x00b9:
            r1.I = r4
        L_0x00bb:
            r1.D = r2
            boolean r2 = r1.E
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r5 = r1.D
            boolean r8 = r5.d
            r2 = r2 & r8
            r1.E = r2
            long r8 = r13 - r21
            r1.F = r8
            r1.G = r13
            android.net.Uri r2 = r5.j
            if (r2 == 0) goto L_0x00e9
            java.lang.Object r2 = r1.p
            monitor-enter(r2)
            com.google.android.exoplayer2.upstream.DataSpec r0 = r0.f3601a     // Catch:{ all -> 0x00e6 }
            android.net.Uri r0 = r0.f3586a     // Catch:{ all -> 0x00e6 }
            android.net.Uri r5 = r1.C     // Catch:{ all -> 0x00e6 }
            if (r0 != r5) goto L_0x00dc
            r4 = 1
        L_0x00dc:
            if (r4 == 0) goto L_0x00e4
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r0 = r1.D     // Catch:{ all -> 0x00e6 }
            android.net.Uri r0 = r0.j     // Catch:{ all -> 0x00e6 }
            r1.C = r0     // Catch:{ all -> 0x00e6 }
        L_0x00e4:
            monitor-exit(r2)     // Catch:{ all -> 0x00e6 }
            goto L_0x00e9
        L_0x00e6:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00e6 }
            throw r0
        L_0x00e9:
            if (r3 != 0) goto L_0x00f9
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r0 = r1.D
            com.google.android.exoplayer2.source.dash.manifest.UtcTimingElement r0 = r0.i
            if (r0 == 0) goto L_0x00f5
            r1.a((com.google.android.exoplayer2.source.dash.manifest.UtcTimingElement) r0)
            goto L_0x0101
        L_0x00f5:
            r1.a((boolean) r6)
            goto L_0x0101
        L_0x00f9:
            int r0 = r1.K
            int r0 = r0 + r7
            r1.K = r0
            r1.a((boolean) r6)
        L_0x0101:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.DashMediaSource.b(com.google.android.exoplayer2.upstream.ParsingLoadable, long, long):void");
    }

    private void c(long j2) {
        this.H = j2;
        a(true);
    }

    public MediaPeriod a(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        MediaSource.MediaPeriodId mediaPeriodId2 = mediaPeriodId;
        int intValue = ((Integer) mediaPeriodId2.f3414a).intValue() - this.K;
        MediaSourceEventListener.EventDispatcher a2 = a(mediaPeriodId2, this.D.a(intValue).b);
        DashMediaPeriod dashMediaPeriod = new DashMediaPeriod(this.K + intValue, this.D, intValue, this.h, this.y, this.j, a2, this.H, this.u, allocator, this.i, this.t);
        this.q.put(dashMediaPeriod.f3439a, dashMediaPeriod);
        return dashMediaPeriod;
    }

    public void a(MediaPeriod mediaPeriod) {
        DashMediaPeriod dashMediaPeriod = (DashMediaPeriod) mediaPeriod;
        dashMediaPeriod.g();
        this.q.remove(dashMediaPeriod.f3439a);
    }

    /* access modifiers changed from: package-private */
    public Loader.LoadErrorAction a(ParsingLoadable<DashManifest> parsingLoadable, long j2, long j3, IOException iOException) {
        ParsingLoadable<DashManifest> parsingLoadable2 = parsingLoadable;
        IOException iOException2 = iOException;
        boolean z2 = iOException2 instanceof ParserException;
        this.m.a(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), parsingLoadable2.b, j2, j3, parsingLoadable.a(), iOException2, z2);
        return z2 ? Loader.f : Loader.d;
    }

    /* access modifiers changed from: package-private */
    public void a(ParsingLoadable<?> parsingLoadable, long j2, long j3) {
        ParsingLoadable<?> parsingLoadable2 = parsingLoadable;
        this.m.a(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), parsingLoadable2.b, j2, j3, parsingLoadable.a());
    }

    private void a(UtcTimingElement utcTimingElement) {
        String str = utcTimingElement.f3472a;
        if (Util.a((Object) str, (Object) "urn:mpeg:dash:utc:direct:2014") || Util.a((Object) str, (Object) "urn:mpeg:dash:utc:direct:2012")) {
            b(utcTimingElement);
        } else if (Util.a((Object) str, (Object) "urn:mpeg:dash:utc:http-iso:2014") || Util.a((Object) str, (Object) "urn:mpeg:dash:utc:http-iso:2012")) {
            a(utcTimingElement, (ParsingLoadable.Parser<Long>) new Iso8601Parser());
        } else if (Util.a((Object) str, (Object) "urn:mpeg:dash:utc:http-xsdate:2014") || Util.a((Object) str, (Object) "urn:mpeg:dash:utc:http-xsdate:2012")) {
            a(utcTimingElement, (ParsingLoadable.Parser<Long>) new XsDateTimeParser());
        } else {
            a(new IOException("Unsupported UTC timing scheme"));
        }
    }

    /* access modifiers changed from: package-private */
    public Loader.LoadErrorAction b(ParsingLoadable<Long> parsingLoadable, long j2, long j3, IOException iOException) {
        ParsingLoadable<Long> parsingLoadable2 = parsingLoadable;
        MediaSourceEventListener.EventDispatcher eventDispatcher = this.m;
        DataSpec dataSpec = parsingLoadable2.f3601a;
        Uri d = parsingLoadable.d();
        Map<String, List<String>> b = parsingLoadable.b();
        int i2 = parsingLoadable2.b;
        eventDispatcher.a(dataSpec, d, b, i2, j2, j3, parsingLoadable.a(), iOException, true);
        a(iOException);
        return Loader.e;
    }

    private void a(UtcTimingElement utcTimingElement, ParsingLoadable.Parser<Long> parser) {
        a(new ParsingLoadable(this.w, Uri.parse(utcTimingElement.b), 5, parser), new UtcTimestampCallback(), 1);
    }

    private void a(IOException iOException) {
        Log.a("DashMediaSource", "Failed to resolve UtcTiming element.", iOException);
        a(true);
    }

    private void a(boolean z2) {
        long j2;
        boolean z3;
        long j3;
        long j4;
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            int keyAt = this.q.keyAt(i2);
            if (keyAt >= this.K) {
                this.q.valueAt(i2).a(this.D, keyAt - this.K);
            }
        }
        int a2 = this.D.a() - 1;
        PeriodSeekInfo a3 = PeriodSeekInfo.a(this.D.a(0), this.D.c(0));
        PeriodSeekInfo a4 = PeriodSeekInfo.a(this.D.a(a2), this.D.c(a2));
        long j5 = a3.b;
        long j6 = a4.c;
        if (!this.D.d || a4.f3446a) {
            j2 = j5;
            z3 = false;
        } else {
            j6 = Math.min((n() - C.a(this.D.f3459a)) - C.a(this.D.a(a2).b), j6);
            long j7 = this.D.f;
            if (j7 != -9223372036854775807L) {
                long a5 = j6 - C.a(j7);
                while (a5 < 0 && a2 > 0) {
                    a2--;
                    a5 += this.D.c(a2);
                }
                if (a2 == 0) {
                    j4 = Math.max(j5, a5);
                } else {
                    j4 = this.D.c(0);
                }
                j5 = j4;
            }
            j2 = j5;
            z3 = true;
        }
        long j8 = j6 - j2;
        for (int i3 = 0; i3 < this.D.a() - 1; i3++) {
            j8 += this.D.c(i3);
        }
        DashManifest dashManifest = this.D;
        if (dashManifest.d) {
            long j9 = this.k;
            if (!this.l) {
                long j10 = dashManifest.g;
                if (j10 != -9223372036854775807L) {
                    j9 = j10;
                }
            }
            long a6 = j8 - C.a(j9);
            if (a6 < 5000000) {
                a6 = Math.min(5000000, j8 / 2);
            }
            j3 = a6;
        } else {
            j3 = 0;
        }
        DashManifest dashManifest2 = this.D;
        long b = dashManifest2.f3459a + dashManifest2.a(0).b + C.b(j2);
        DashManifest dashManifest3 = this.D;
        a((Timeline) new DashTimeline(dashManifest3.f3459a, b, this.K, j2, j8, j3, dashManifest3, this.v), (Object) this.D);
        if (!this.f) {
            this.A.removeCallbacks(this.s);
            if (z3) {
                this.A.postDelayed(this.s, 5000);
            }
            if (this.E) {
                o();
            } else if (z2) {
                DashManifest dashManifest4 = this.D;
                if (dashManifest4.d) {
                    long j11 = dashManifest4.e;
                    if (j11 != -9223372036854775807L) {
                        if (j11 == 0) {
                            j11 = 5000;
                        }
                        d(Math.max(0, (this.F + j11) - SystemClock.elapsedRealtime()));
                    }
                }
            }
        }
    }

    private void b(UtcTimingElement utcTimingElement) {
        try {
            c(Util.h(utcTimingElement.b) - this.G);
        } catch (ParserException e) {
            a((IOException) e);
        }
    }

    private <T> void a(ParsingLoadable<T> parsingLoadable, Loader.Callback<ParsingLoadable<T>> callback, int i2) {
        this.m.a(parsingLoadable.f3601a, parsingLoadable.b, this.x.a(parsingLoadable, callback, i2));
    }
}
