package com.google.android.exoplayer2.source;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public interface MediaSourceEventListener {

    public static final class EventDispatcher {

        /* renamed from: a  reason: collision with root package name */
        public final int f3415a;
        public final MediaSource.MediaPeriodId b;
        private final CopyOnWriteArrayList<ListenerAndHandler> c;
        private final long d;

        private static final class ListenerAndHandler {

            /* renamed from: a  reason: collision with root package name */
            public final Handler f3416a;
            public final MediaSourceEventListener b;

            public ListenerAndHandler(Handler handler, MediaSourceEventListener mediaSourceEventListener) {
                this.f3416a = handler;
                this.b = mediaSourceEventListener;
            }
        }

        public EventDispatcher() {
            this(new CopyOnWriteArrayList(), 0, (MediaSource.MediaPeriodId) null, 0);
        }

        public EventDispatcher a(int i, MediaSource.MediaPeriodId mediaPeriodId, long j) {
            return new EventDispatcher(this.c, i, mediaPeriodId, j);
        }

        public void b() {
            MediaSource.MediaPeriodId mediaPeriodId = this.b;
            Assertions.a(mediaPeriodId);
            MediaSource.MediaPeriodId mediaPeriodId2 = mediaPeriodId;
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new l(this, next.b, mediaPeriodId2));
            }
        }

        public void c(LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new k(this, next.b, loadEventInfo, mediaLoadData));
            }
        }

        private EventDispatcher(CopyOnWriteArrayList<ListenerAndHandler> copyOnWriteArrayList, int i, MediaSource.MediaPeriodId mediaPeriodId, long j) {
            this.c = copyOnWriteArrayList;
            this.f3415a = i;
            this.b = mediaPeriodId;
            this.d = j;
        }

        public void a(Handler handler, MediaSourceEventListener mediaSourceEventListener) {
            Assertions.a((handler == null || mediaSourceEventListener == null) ? false : true);
            this.c.add(new ListenerAndHandler(handler, mediaSourceEventListener));
        }

        public void a(MediaSourceEventListener mediaSourceEventListener) {
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                if (next.b == mediaSourceEventListener) {
                    this.c.remove(next);
                }
            }
        }

        public /* synthetic */ void c(MediaSourceEventListener mediaSourceEventListener, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
            mediaSourceEventListener.a(this.f3415a, this.b, loadEventInfo, mediaLoadData);
        }

        public /* synthetic */ void b(MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
            mediaSourceEventListener.c(this.f3415a, mediaPeriodId);
        }

        public void c() {
            MediaSource.MediaPeriodId mediaPeriodId = this.b;
            Assertions.a(mediaPeriodId);
            MediaSource.MediaPeriodId mediaPeriodId2 = mediaPeriodId;
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new j(this, next.b, mediaPeriodId2));
            }
        }

        public void b(DataSpec dataSpec, Uri uri, Map<String, List<String>> map, int i, long j, long j2, long j3) {
            b(dataSpec, uri, map, i, -1, (Format) null, 0, (Object) null, -9223372036854775807L, -9223372036854775807L, j, j2, j3);
        }

        public void a() {
            MediaSource.MediaPeriodId mediaPeriodId = this.b;
            Assertions.a(mediaPeriodId);
            MediaSource.MediaPeriodId mediaPeriodId2 = mediaPeriodId;
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new i(this, next.b, mediaPeriodId2));
            }
        }

        public void b(DataSpec dataSpec, Uri uri, Map<String, List<String>> map, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5) {
            b(new LoadEventInfo(dataSpec, uri, map, j3, j4, j5), new MediaLoadData(i, i2, format, i3, obj, a(j), a(j2)));
        }

        public /* synthetic */ void c(MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
            mediaSourceEventListener.b(this.f3415a, mediaPeriodId);
        }

        public /* synthetic */ void a(MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
            mediaSourceEventListener.a(this.f3415a, mediaPeriodId);
        }

        public void b(LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new h(this, next.b, loadEventInfo, mediaLoadData));
            }
        }

        public void a(DataSpec dataSpec, int i, long j) {
            a(dataSpec, i, -1, (Format) null, 0, (Object) null, -9223372036854775807L, -9223372036854775807L, j);
        }

        public void a(DataSpec dataSpec, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3) {
            DataSpec dataSpec2 = dataSpec;
            c(new LoadEventInfo(dataSpec2, dataSpec2.f3586a, Collections.emptyMap(), j3, 0, 0), new MediaLoadData(i, i2, format, i3, obj, a(j), a(j2)));
        }

        public /* synthetic */ void b(MediaSourceEventListener mediaSourceEventListener, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
            mediaSourceEventListener.b(this.f3415a, this.b, loadEventInfo, mediaLoadData);
        }

        public void b(MediaLoadData mediaLoadData) {
            MediaSource.MediaPeriodId mediaPeriodId = this.b;
            Assertions.a(mediaPeriodId);
            MediaSource.MediaPeriodId mediaPeriodId2 = mediaPeriodId;
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new f(this, next.b, mediaPeriodId2, mediaLoadData));
            }
        }

        public void a(DataSpec dataSpec, Uri uri, Map<String, List<String>> map, int i, long j, long j2, long j3) {
            a(dataSpec, uri, map, i, -1, (Format) null, 0, (Object) null, -9223372036854775807L, -9223372036854775807L, j, j2, j3);
        }

        public void a(DataSpec dataSpec, Uri uri, Map<String, List<String>> map, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5) {
            a(new LoadEventInfo(dataSpec, uri, map, j3, j4, j5), new MediaLoadData(i, i2, format, i3, obj, a(j), a(j2)));
        }

        public void a(LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new e(this, next.b, loadEventInfo, mediaLoadData));
            }
        }

        public /* synthetic */ void a(MediaSourceEventListener mediaSourceEventListener, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
            mediaSourceEventListener.c(this.f3415a, this.b, loadEventInfo, mediaLoadData);
        }

        public void a(DataSpec dataSpec, Uri uri, Map<String, List<String>> map, int i, long j, long j2, long j3, IOException iOException, boolean z) {
            a(dataSpec, uri, map, i, -1, (Format) null, 0, (Object) null, -9223372036854775807L, -9223372036854775807L, j, j2, j3, iOException, z);
        }

        public void a(DataSpec dataSpec, Uri uri, Map<String, List<String>> map, int i, int i2, Format format, int i3, Object obj, long j, long j2, long j3, long j4, long j5, IOException iOException, boolean z) {
            a(new LoadEventInfo(dataSpec, uri, map, j3, j4, j5), new MediaLoadData(i, i2, format, i3, obj, a(j), a(j2)), iOException, z);
        }

        public void a(LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData, IOException iOException, boolean z) {
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new d(this, next.b, loadEventInfo, mediaLoadData, iOException, z));
            }
        }

        public /* synthetic */ void a(MediaSourceEventListener mediaSourceEventListener, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData, IOException iOException, boolean z) {
            mediaSourceEventListener.a(this.f3415a, this.b, loadEventInfo, mediaLoadData, iOException, z);
        }

        public void a(int i, long j, long j2) {
            long j3 = j;
            b(new MediaLoadData(1, i, (Format) null, 3, (Object) null, a(j), a(j2)));
        }

        public /* synthetic */ void a(MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId, MediaLoadData mediaLoadData) {
            mediaSourceEventListener.a(this.f3415a, mediaPeriodId, mediaLoadData);
        }

        public void a(int i, Format format, int i2, Object obj, long j) {
            a(new MediaLoadData(1, i, format, i2, obj, a(j), -9223372036854775807L));
        }

        public void a(MediaLoadData mediaLoadData) {
            Iterator<ListenerAndHandler> it2 = this.c.iterator();
            while (it2.hasNext()) {
                ListenerAndHandler next = it2.next();
                a(next.f3416a, (Runnable) new g(this, next.b, mediaLoadData));
            }
        }

        public /* synthetic */ void a(MediaSourceEventListener mediaSourceEventListener, MediaLoadData mediaLoadData) {
            mediaSourceEventListener.b(this.f3415a, this.b, mediaLoadData);
        }

        private long a(long j) {
            long b2 = C.b(j);
            if (b2 == -9223372036854775807L) {
                return -9223372036854775807L;
            }
            return this.d + b2;
        }

        private void a(Handler handler, Runnable runnable) {
            if (handler.getLooper() == Looper.myLooper()) {
                runnable.run();
            } else {
                handler.post(runnable);
            }
        }
    }

    public static final class LoadEventInfo {
        public LoadEventInfo(DataSpec dataSpec, Uri uri, Map<String, List<String>> map, long j, long j2, long j3) {
        }
    }

    public static final class MediaLoadData {

        /* renamed from: a  reason: collision with root package name */
        public final int f3417a;
        public final int b;
        public final Format c;
        public final int d;
        public final Object e;
        public final long f;
        public final long g;

        public MediaLoadData(int i, int i2, Format format, int i3, Object obj, long j, long j2) {
            this.f3417a = i;
            this.b = i2;
            this.c = format;
            this.d = i3;
            this.e = obj;
            this.f = j;
            this.g = j2;
        }
    }

    void a(int i, MediaSource.MediaPeriodId mediaPeriodId);

    void a(int i, MediaSource.MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData);

    void a(int i, MediaSource.MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData, IOException iOException, boolean z);

    void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaLoadData mediaLoadData);

    void b(int i, MediaSource.MediaPeriodId mediaPeriodId);

    void b(int i, MediaSource.MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData);

    void b(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaLoadData mediaLoadData);

    void c(int i, MediaSource.MediaPeriodId mediaPeriodId);

    void c(int i, MediaSource.MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData);
}
