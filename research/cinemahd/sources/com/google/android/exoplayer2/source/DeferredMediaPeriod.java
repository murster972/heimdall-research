package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.Allocator;
import java.io.IOException;

public final class DeferredMediaPeriod implements MediaPeriod, MediaPeriod.Callback {

    /* renamed from: a  reason: collision with root package name */
    public final MediaSource f3406a;
    public final MediaSource.MediaPeriodId b;
    private final Allocator c;
    private MediaPeriod d;
    private MediaPeriod.Callback e;
    private long f;
    private PrepareErrorListener g;
    private boolean h;
    private long i = -9223372036854775807L;

    public interface PrepareErrorListener {
        void a(MediaSource.MediaPeriodId mediaPeriodId, IOException iOException);
    }

    public DeferredMediaPeriod(MediaSource mediaSource, MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        this.b = mediaPeriodId;
        this.c = allocator;
        this.f3406a = mediaSource;
    }

    public long b() {
        return this.d.b();
    }

    public long c() {
        return this.d.c();
    }

    public void d(long j) {
        this.i = j;
    }

    public TrackGroupArray e() {
        return this.d.e();
    }

    public long f() {
        return this.d.f();
    }

    public void g() {
        MediaPeriod mediaPeriod = this.d;
        if (mediaPeriod != null) {
            this.f3406a.a(mediaPeriod);
        }
    }

    public long a() {
        return this.f;
    }

    public boolean b(long j) {
        MediaPeriod mediaPeriod = this.d;
        return mediaPeriod != null && mediaPeriod.b(j);
    }

    public void c(long j) {
        this.d.c(j);
    }

    public void d() throws IOException {
        try {
            if (this.d != null) {
                this.d.d();
            } else {
                this.f3406a.b();
            }
        } catch (IOException e2) {
            PrepareErrorListener prepareErrorListener = this.g;
            if (prepareErrorListener == null) {
                throw e2;
            } else if (!this.h) {
                this.h = true;
                prepareErrorListener.a(this.b, e2);
            }
        }
    }

    public void a(MediaSource.MediaPeriodId mediaPeriodId) {
        this.d = this.f3406a.a(mediaPeriodId, this.c);
        if (this.e != null) {
            long j = this.i;
            if (j == -9223372036854775807L) {
                j = this.f;
            }
            this.d.a((MediaPeriod.Callback) this, j);
        }
    }

    /* renamed from: b */
    public void a(MediaPeriod mediaPeriod) {
        this.e.a(this);
    }

    public void a(MediaPeriod.Callback callback, long j) {
        this.e = callback;
        this.f = j;
        MediaPeriod mediaPeriod = this.d;
        if (mediaPeriod != null) {
            mediaPeriod.a((MediaPeriod.Callback) this, j);
        }
    }

    public long a(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr, boolean[] zArr2, long j) {
        long j2;
        long j3 = this.i;
        if (j3 == -9223372036854775807L || j != this.f) {
            j2 = j;
        } else {
            this.i = -9223372036854775807L;
            j2 = j3;
        }
        return this.d.a(trackSelectionArr, zArr, sampleStreamArr, zArr2, j2);
    }

    public void a(long j, boolean z) {
        this.d.a(j, z);
    }

    public long a(long j) {
        return this.d.a(j);
    }

    public long a(long j, SeekParameters seekParameters) {
        return this.d.a(j, seekParameters);
    }

    public void a(MediaPeriod mediaPeriod) {
        this.e.a(this);
    }
}
