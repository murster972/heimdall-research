package com.google.android.exoplayer2.source;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class TrackGroupArray implements Parcelable {
    public static final Parcelable.Creator<TrackGroupArray> CREATOR = new Parcelable.Creator<TrackGroupArray>() {
        public TrackGroupArray createFromParcel(Parcel parcel) {
            return new TrackGroupArray(parcel);
        }

        public TrackGroupArray[] newArray(int i) {
            return new TrackGroupArray[i];
        }
    };
    public static final TrackGroupArray d = new TrackGroupArray(new TrackGroup[0]);

    /* renamed from: a  reason: collision with root package name */
    public final int f3424a;
    private final TrackGroup[] b;
    private int c;

    public TrackGroupArray(TrackGroup... trackGroupArr) {
        this.b = trackGroupArr;
        this.f3424a = trackGroupArr.length;
    }

    public TrackGroup a(int i) {
        return this.b[i];
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || TrackGroupArray.class != obj.getClass()) {
            return false;
        }
        TrackGroupArray trackGroupArray = (TrackGroupArray) obj;
        if (this.f3424a != trackGroupArray.f3424a || !Arrays.equals(this.b, trackGroupArray.b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.c == 0) {
            this.c = Arrays.hashCode(this.b);
        }
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f3424a);
        for (int i2 = 0; i2 < this.f3424a; i2++) {
            parcel.writeParcelable(this.b[i2], 0);
        }
    }

    public int a(TrackGroup trackGroup) {
        for (int i = 0; i < this.f3424a; i++) {
            if (this.b[i] == trackGroup) {
                return i;
            }
        }
        return -1;
    }

    TrackGroupArray(Parcel parcel) {
        this.f3424a = parcel.readInt();
        this.b = new TrackGroup[this.f3424a];
        for (int i = 0; i < this.f3424a; i++) {
            this.b[i] = (TrackGroup) parcel.readParcelable(TrackGroup.class.getClassLoader());
        }
    }
}
