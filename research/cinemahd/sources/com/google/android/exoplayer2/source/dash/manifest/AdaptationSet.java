package com.google.android.exoplayer2.source.dash.manifest;

import java.util.Collections;
import java.util.List;

public class AdaptationSet {

    /* renamed from: a  reason: collision with root package name */
    public final int f3458a;
    public final int b;
    public final List<Representation> c;
    public final List<Descriptor> d;
    public final List<Descriptor> e;

    public AdaptationSet(int i, int i2, List<Representation> list, List<Descriptor> list2, List<Descriptor> list3) {
        List<Descriptor> list4;
        List<Descriptor> list5;
        this.f3458a = i;
        this.b = i2;
        this.c = Collections.unmodifiableList(list);
        if (list2 == null) {
            list4 = Collections.emptyList();
        } else {
            list4 = Collections.unmodifiableList(list2);
        }
        this.d = list4;
        if (list3 == null) {
            list5 = Collections.emptyList();
        } else {
            list5 = Collections.unmodifiableList(list3);
        }
        this.e = list5;
    }
}
