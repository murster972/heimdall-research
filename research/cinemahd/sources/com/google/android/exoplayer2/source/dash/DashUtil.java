package com.google.android.exoplayer2.source.dash;

import android.net.Uri;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.ChunkIndex;
import com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor;
import com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor;
import com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper;
import com.google.android.exoplayer2.source.chunk.InitializationChunk;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.manifest.DashManifestParser;
import com.google.android.exoplayer2.source.dash.manifest.RangedUri;
import com.google.android.exoplayer2.source.dash.manifest.Representation;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import java.io.IOException;

public final class DashUtil {
    private DashUtil() {
    }

    public static DashManifest a(DataSource dataSource, Uri uri) throws IOException {
        return (DashManifest) ParsingLoadable.a(dataSource, new DashManifestParser(), uri, 4);
    }

    public static ChunkIndex a(DataSource dataSource, int i, Representation representation) throws IOException, InterruptedException {
        ChunkExtractorWrapper a2 = a(dataSource, i, representation, true);
        if (a2 == null) {
            return null;
        }
        return (ChunkIndex) a2.c();
    }

    private static ChunkExtractorWrapper a(DataSource dataSource, int i, Representation representation, boolean z) throws IOException, InterruptedException {
        RangedUri f = representation.f();
        if (f == null) {
            return null;
        }
        ChunkExtractorWrapper a2 = a(i, representation.f3467a);
        if (z) {
            RangedUri e = representation.e();
            if (e == null) {
                return null;
            }
            RangedUri a3 = f.a(e, representation.b);
            if (a3 == null) {
                a(dataSource, representation, a2, f);
                f = e;
            } else {
                f = a3;
            }
        }
        a(dataSource, representation, a2, f);
        return a2;
    }

    private static void a(DataSource dataSource, Representation representation, ChunkExtractorWrapper chunkExtractorWrapper, RangedUri rangedUri) throws IOException, InterruptedException {
        new InitializationChunk(dataSource, new DataSpec(rangedUri.a(representation.b), rangedUri.f3466a, rangedUri.b, representation.c()), representation.f3467a, 0, (Object) null, chunkExtractorWrapper).load();
    }

    private static ChunkExtractorWrapper a(int i, Format format) {
        String str = format.f;
        return new ChunkExtractorWrapper(str != null && (str.startsWith("video/webm") || str.startsWith("audio/webm")) ? new MatroskaExtractor() : new FragmentedMp4Extractor(), i, format);
    }
}
