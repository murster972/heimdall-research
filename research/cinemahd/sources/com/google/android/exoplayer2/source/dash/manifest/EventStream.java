package com.google.android.exoplayer2.source.dash.manifest;

import com.google.android.exoplayer2.metadata.emsg.EventMessage;

public final class EventStream {

    /* renamed from: a  reason: collision with root package name */
    public final EventMessage[] f3463a;
    public final long[] b;
    public final String c;
    public final String d;
    public final long e;

    public EventStream(String str, String str2, long j, long[] jArr, EventMessage[] eventMessageArr) {
        this.c = str;
        this.d = str2;
        this.e = j;
        this.b = jArr;
        this.f3463a = eventMessageArr;
    }

    public String a() {
        return this.c + "/" + this.d;
    }
}
