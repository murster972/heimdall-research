package com.google.android.exoplayer2.source.dash.manifest;

import android.net.Uri;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.dash.DashSegmentIndex;
import com.google.android.exoplayer2.source.dash.manifest.SegmentBase;
import java.util.Collections;
import java.util.List;

public abstract class Representation {

    /* renamed from: a  reason: collision with root package name */
    public final Format f3467a;
    public final String b;
    public final long c;
    public final List<Descriptor> d;
    private final RangedUri e;

    public static class MultiSegmentRepresentation extends Representation implements DashSegmentIndex {
        private final SegmentBase.MultiSegmentBase f;

        public MultiSegmentRepresentation(String str, long j, Format format, String str2, SegmentBase.MultiSegmentBase multiSegmentBase, List<Descriptor> list) {
            super(str, j, format, str2, multiSegmentBase, list);
            this.f = multiSegmentBase;
        }

        public long a(long j) {
            return this.f.b(j);
        }

        public RangedUri b(long j) {
            return this.f.a((Representation) this, j);
        }

        public int c(long j) {
            return this.f.a(j);
        }

        public String c() {
            return null;
        }

        public DashSegmentIndex d() {
            return this;
        }

        public RangedUri e() {
            return null;
        }

        public long a(long j, long j2) {
            return this.f.a(j, j2);
        }

        public long b(long j, long j2) {
            return this.f.b(j, j2);
        }

        public boolean a() {
            return this.f.c();
        }

        public long b() {
            return this.f.b();
        }
    }

    public static class SingleSegmentRepresentation extends Representation {
        private final String f;
        private final RangedUri g;
        private final SingleSegmentIndex h;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public SingleSegmentRepresentation(String str, long j, Format format, String str2, SegmentBase.SingleSegmentBase singleSegmentBase, List<Descriptor> list, String str3, long j2) {
            super(str, j, format, str2, singleSegmentBase, list);
            String str4;
            String str5 = str;
            Uri.parse(str2);
            this.g = singleSegmentBase.b();
            SingleSegmentIndex singleSegmentIndex = null;
            if (str3 != null) {
                str4 = str3;
            } else if (str5 != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(".");
                sb.append(format.f3164a);
                sb.append(".");
                long j3 = j;
                sb.append(j);
                str4 = sb.toString();
            } else {
                str4 = null;
            }
            this.f = str4;
            this.h = this.g == null ? new SingleSegmentIndex(new RangedUri((String) null, 0, j2)) : singleSegmentIndex;
        }

        public String c() {
            return this.f;
        }

        public DashSegmentIndex d() {
            return this.h;
        }

        public RangedUri e() {
            return this.g;
        }
    }

    public static Representation a(String str, long j, Format format, String str2, SegmentBase segmentBase, List<Descriptor> list) {
        return a(str, j, format, str2, segmentBase, list, (String) null);
    }

    public abstract String c();

    public abstract DashSegmentIndex d();

    public abstract RangedUri e();

    public RangedUri f() {
        return this.e;
    }

    private Representation(String str, long j, Format format, String str2, SegmentBase segmentBase, List<Descriptor> list) {
        List<Descriptor> list2;
        this.f3467a = format;
        this.b = str2;
        if (list == null) {
            list2 = Collections.emptyList();
        } else {
            list2 = Collections.unmodifiableList(list);
        }
        this.d = list2;
        this.e = segmentBase.a(this);
        this.c = segmentBase.a();
    }

    public static Representation a(String str, long j, Format format, String str2, SegmentBase segmentBase, List<Descriptor> list, String str3) {
        SegmentBase segmentBase2 = segmentBase;
        if (segmentBase2 instanceof SegmentBase.SingleSegmentBase) {
            return new SingleSegmentRepresentation(str, j, format, str2, (SegmentBase.SingleSegmentBase) segmentBase2, list, str3, -1);
        } else if (segmentBase2 instanceof SegmentBase.MultiSegmentBase) {
            return new MultiSegmentRepresentation(str, j, format, str2, (SegmentBase.MultiSegmentBase) segmentBase2, list);
        } else {
            throw new IllegalArgumentException("segmentBase must be of type SingleSegmentBase or MultiSegmentBase");
        }
    }
}
