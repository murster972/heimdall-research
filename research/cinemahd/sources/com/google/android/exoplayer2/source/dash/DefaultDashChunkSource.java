package com.google.android.exoplayer2.source.dash;

import android.os.SystemClock;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.extractor.ChunkIndex;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.chunk.BaseMediaChunkIterator;
import com.google.android.exoplayer2.source.chunk.Chunk;
import com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper;
import com.google.android.exoplayer2.source.chunk.ChunkHolder;
import com.google.android.exoplayer2.source.chunk.ContainerMediaChunk;
import com.google.android.exoplayer2.source.chunk.InitializationChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import com.google.android.exoplayer2.source.chunk.SingleSampleMediaChunk;
import com.google.android.exoplayer2.source.dash.DashChunkSource;
import com.google.android.exoplayer2.source.dash.PlayerEmsgHandler;
import com.google.android.exoplayer2.source.dash.manifest.AdaptationSet;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.manifest.RangedUri;
import com.google.android.exoplayer2.source.dash.manifest.Representation;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.LoaderErrorThrower;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DefaultDashChunkSource implements DashChunkSource {

    /* renamed from: a  reason: collision with root package name */
    private final LoaderErrorThrower f3449a;
    private final int[] b;
    private final TrackSelection c;
    private final int d;
    private final DataSource e;
    private final long f;
    private final int g;
    private final PlayerEmsgHandler.PlayerTrackEmsgHandler h;
    protected final RepresentationHolder[] i;
    private DashManifest j;
    private int k;
    private IOException l;
    private boolean m;
    private long n = -9223372036854775807L;

    public static final class Factory implements DashChunkSource.Factory {

        /* renamed from: a  reason: collision with root package name */
        private final DataSource.Factory f3450a;
        private final int b;

        public Factory(DataSource.Factory factory) {
            this(factory, 1);
        }

        public DashChunkSource a(LoaderErrorThrower loaderErrorThrower, DashManifest dashManifest, int i, int[] iArr, TrackSelection trackSelection, int i2, long j, boolean z, boolean z2, PlayerEmsgHandler.PlayerTrackEmsgHandler playerTrackEmsgHandler, TransferListener transferListener) {
            TransferListener transferListener2 = transferListener;
            DataSource a2 = this.f3450a.a();
            if (transferListener2 != null) {
                a2.a(transferListener2);
            }
            return new DefaultDashChunkSource(loaderErrorThrower, dashManifest, i, iArr, trackSelection, i2, a2, j, this.b, z, z2, playerTrackEmsgHandler);
        }

        public Factory(DataSource.Factory factory, int i) {
            this.f3450a = factory;
            this.b = i;
        }
    }

    protected static final class RepresentationHolder {

        /* renamed from: a  reason: collision with root package name */
        final ChunkExtractorWrapper f3451a;
        public final Representation b;
        public final DashSegmentIndex c;
        /* access modifiers changed from: private */
        public final long d;
        private final long e;

        RepresentationHolder(long j, int i, Representation representation, boolean z, boolean z2, TrackOutput trackOutput) {
            this(j, representation, a(i, representation, z, z2, trackOutput), 0, representation.d());
        }

        public int b() {
            return this.c.c(this.d);
        }

        public long c(long j) {
            return this.c.a(j - this.e);
        }

        public RangedUri d(long j) {
            return this.c.b(j - this.e);
        }

        /* access modifiers changed from: package-private */
        public RepresentationHolder a(long j, Representation representation) throws BehindLiveWindowException {
            long b2;
            long j2 = j;
            DashSegmentIndex d2 = this.b.d();
            DashSegmentIndex d3 = representation.d();
            if (d2 == null) {
                return new RepresentationHolder(j, representation, this.f3451a, this.e, d2);
            } else if (!d2.a()) {
                return new RepresentationHolder(j, representation, this.f3451a, this.e, d3);
            } else {
                int c2 = d2.c(j2);
                if (c2 == 0) {
                    return new RepresentationHolder(j, representation, this.f3451a, this.e, d3);
                }
                long b3 = (d2.b() + ((long) c2)) - 1;
                long a2 = d2.a(b3) + d2.a(b3, j2);
                long b4 = d3.b();
                long a3 = d3.a(b4);
                long j3 = this.e;
                int i = (a2 > a3 ? 1 : (a2 == a3 ? 0 : -1));
                if (i == 0) {
                    b2 = b3 + 1;
                } else if (i >= 0) {
                    b2 = d2.b(a3, j2);
                } else {
                    throw new BehindLiveWindowException();
                }
                long j4 = j3 + (b2 - b4);
                return new RepresentationHolder(j, representation, this.f3451a, j4, d3);
            }
        }

        public long b(long j) {
            return this.c.b(j, this.d) + this.e;
        }

        public long b(DashManifest dashManifest, int i, long j) {
            long a2;
            int b2 = b();
            if (b2 == -1) {
                a2 = b((j - C.a(dashManifest.f3459a)) - C.a(dashManifest.a(i).b));
            } else {
                a2 = a() + ((long) b2);
            }
            return a2 - 1;
        }

        private RepresentationHolder(long j, Representation representation, ChunkExtractorWrapper chunkExtractorWrapper, long j2, DashSegmentIndex dashSegmentIndex) {
            this.d = j;
            this.b = representation;
            this.e = j2;
            this.f3451a = chunkExtractorWrapper;
            this.c = dashSegmentIndex;
        }

        private static boolean b(String str) {
            return str.startsWith("video/webm") || str.startsWith("audio/webm") || str.startsWith("application/webm");
        }

        /* access modifiers changed from: package-private */
        public RepresentationHolder a(DashSegmentIndex dashSegmentIndex) {
            return new RepresentationHolder(this.d, this.b, this.f3451a, this.e, dashSegmentIndex);
        }

        public long a() {
            return this.c.b() + this.e;
        }

        public long a(long j) {
            return c(j) + this.c.a(j - this.e, this.d);
        }

        public long a(DashManifest dashManifest, int i, long j) {
            if (b() != -1 || dashManifest.f == -9223372036854775807L) {
                return a();
            }
            return Math.max(a(), b(((j - C.a(dashManifest.f3459a)) - C.a(dashManifest.a(i).b)) - C.a(dashManifest.f)));
        }

        private static boolean a(String str) {
            return MimeTypes.k(str) || "application/ttml+xml".equals(str);
        }

        /* JADX WARNING: type inference failed for: r12v9, types: [com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor] */
        /* JADX WARNING: type inference failed for: r12v10, types: [com.google.android.exoplayer2.extractor.rawcc.RawCcExtractor] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper a(int r10, com.google.android.exoplayer2.source.dash.manifest.Representation r11, boolean r12, boolean r13, com.google.android.exoplayer2.extractor.TrackOutput r14) {
            /*
                com.google.android.exoplayer2.Format r0 = r11.f3467a
                java.lang.String r0 = r0.f
                boolean r1 = a((java.lang.String) r0)
                r2 = 0
                if (r1 == 0) goto L_0x000c
                return r2
            L_0x000c:
                java.lang.String r1 = "application/x-rawcc"
                boolean r1 = r1.equals(r0)
                if (r1 == 0) goto L_0x001c
                com.google.android.exoplayer2.extractor.rawcc.RawCcExtractor r12 = new com.google.android.exoplayer2.extractor.rawcc.RawCcExtractor
                com.google.android.exoplayer2.Format r13 = r11.f3467a
                r12.<init>(r13)
                goto L_0x004c
            L_0x001c:
                boolean r0 = b((java.lang.String) r0)
                if (r0 == 0) goto L_0x0029
                com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor r12 = new com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor
                r13 = 1
                r12.<init>(r13)
                goto L_0x004c
            L_0x0029:
                r0 = 0
                if (r12 == 0) goto L_0x002f
                r12 = 4
                r4 = 4
                goto L_0x0030
            L_0x002f:
                r4 = 0
            L_0x0030:
                if (r13 == 0) goto L_0x003d
                java.lang.String r12 = "application/cea-608"
                com.google.android.exoplayer2.Format r12 = com.google.android.exoplayer2.Format.a(r2, r12, r0, r2)
                java.util.List r12 = java.util.Collections.singletonList(r12)
                goto L_0x0041
            L_0x003d:
                java.util.List r12 = java.util.Collections.emptyList()
            L_0x0041:
                r8 = r12
                com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor r12 = new com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor
                r5 = 0
                r6 = 0
                r7 = 0
                r3 = r12
                r9 = r14
                r3.<init>(r4, r5, r6, r7, r8, r9)
            L_0x004c:
                com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper r13 = new com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper
                com.google.android.exoplayer2.Format r11 = r11.f3467a
                r13.<init>(r12, r10, r11)
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.DefaultDashChunkSource.RepresentationHolder.a(int, com.google.android.exoplayer2.source.dash.manifest.Representation, boolean, boolean, com.google.android.exoplayer2.extractor.TrackOutput):com.google.android.exoplayer2.source.chunk.ChunkExtractorWrapper");
        }
    }

    protected static final class RepresentationSegmentIterator extends BaseMediaChunkIterator {
        public RepresentationSegmentIterator(RepresentationHolder representationHolder, long j, long j2) {
            super(j, j2);
        }
    }

    public DefaultDashChunkSource(LoaderErrorThrower loaderErrorThrower, DashManifest dashManifest, int i2, int[] iArr, TrackSelection trackSelection, int i3, DataSource dataSource, long j2, int i4, boolean z, boolean z2, PlayerEmsgHandler.PlayerTrackEmsgHandler playerTrackEmsgHandler) {
        TrackSelection trackSelection2 = trackSelection;
        this.f3449a = loaderErrorThrower;
        this.j = dashManifest;
        this.b = iArr;
        this.c = trackSelection2;
        this.d = i3;
        this.e = dataSource;
        this.k = i2;
        this.f = j2;
        this.g = i4;
        this.h = playerTrackEmsgHandler;
        long c2 = dashManifest.c(i2);
        ArrayList<Representation> c3 = c();
        this.i = new RepresentationHolder[trackSelection.length()];
        for (int i5 = 0; i5 < this.i.length; i5++) {
            this.i[i5] = new RepresentationHolder(c2, i3, c3.get(trackSelection2.b(i5)), z, z2, playerTrackEmsgHandler);
        }
    }

    private long b() {
        long currentTimeMillis;
        if (this.f != 0) {
            currentTimeMillis = SystemClock.elapsedRealtime() + this.f;
        } else {
            currentTimeMillis = System.currentTimeMillis();
        }
        return currentTimeMillis * 1000;
    }

    private ArrayList<Representation> c() {
        List<AdaptationSet> list = this.j.a(this.k).c;
        ArrayList<Representation> arrayList = new ArrayList<>();
        for (int i2 : this.b) {
            arrayList.addAll(list.get(i2).c);
        }
        return arrayList;
    }

    public long a(long j2, SeekParameters seekParameters) {
        for (RepresentationHolder representationHolder : this.i) {
            if (representationHolder.c != null) {
                long b2 = representationHolder.b(j2);
                long c2 = representationHolder.c(b2);
                return Util.a(j2, seekParameters, c2, (c2 >= j2 || b2 >= ((long) (representationHolder.b() + -1))) ? c2 : representationHolder.c(b2 + 1));
            }
        }
        return j2;
    }

    public void a(DashManifest dashManifest, int i2) {
        try {
            this.j = dashManifest;
            this.k = i2;
            long c2 = this.j.c(this.k);
            ArrayList<Representation> c3 = c();
            for (int i3 = 0; i3 < this.i.length; i3++) {
                this.i[i3] = this.i[i3].a(c2, c3.get(this.c.b(i3)));
            }
        } catch (BehindLiveWindowException e2) {
            this.l = e2;
        }
    }

    public void a() throws IOException {
        IOException iOException = this.l;
        if (iOException == null) {
            this.f3449a.a();
            return;
        }
        throw iOException;
    }

    public int a(long j2, List<? extends MediaChunk> list) {
        if (this.l != null || this.c.length() < 2) {
            return list.size();
        }
        return this.c.a(j2, list);
    }

    public void a(long j2, long j3, List<? extends MediaChunk> list, ChunkHolder chunkHolder) {
        MediaChunk mediaChunk;
        long j4;
        int i2;
        MediaChunkIterator[] mediaChunkIteratorArr;
        ChunkHolder chunkHolder2 = chunkHolder;
        if (this.l == null) {
            long j5 = j3 - j2;
            long a2 = a(j2);
            long a3 = C.a(this.j.f3459a) + C.a(this.j.a(this.k).b) + j3;
            PlayerEmsgHandler.PlayerTrackEmsgHandler playerTrackEmsgHandler = this.h;
            if (playerTrackEmsgHandler == null || !playerTrackEmsgHandler.a(a3)) {
                long b2 = b();
                if (list.isEmpty()) {
                    List<? extends MediaChunk> list2 = list;
                    mediaChunk = null;
                } else {
                    mediaChunk = (MediaChunk) list.get(list.size() - 1);
                }
                MediaChunkIterator[] mediaChunkIteratorArr2 = new MediaChunkIterator[this.c.length()];
                int i3 = 0;
                while (i3 < mediaChunkIteratorArr2.length) {
                    RepresentationHolder representationHolder = this.i[i3];
                    if (representationHolder.c == null) {
                        mediaChunkIteratorArr2[i3] = MediaChunkIterator.f3437a;
                        mediaChunkIteratorArr = mediaChunkIteratorArr2;
                        i2 = i3;
                        j4 = b2;
                    } else {
                        long a4 = representationHolder.a(this.j, this.k, b2);
                        long b3 = representationHolder.b(this.j, this.k, b2);
                        RepresentationHolder representationHolder2 = representationHolder;
                        mediaChunkIteratorArr = mediaChunkIteratorArr2;
                        i2 = i3;
                        j4 = b2;
                        long a5 = a(representationHolder, mediaChunk, j3, a4, b3);
                        if (a5 < a4) {
                            mediaChunkIteratorArr[i2] = MediaChunkIterator.f3437a;
                        } else {
                            mediaChunkIteratorArr[i2] = new RepresentationSegmentIterator(representationHolder2, a5, b3);
                        }
                    }
                    i3 = i2 + 1;
                    List<? extends MediaChunk> list3 = list;
                    mediaChunkIteratorArr2 = mediaChunkIteratorArr;
                    b2 = j4;
                }
                long j6 = b2;
                this.c.a(j2, j5, a2, list, mediaChunkIteratorArr2);
                RepresentationHolder representationHolder3 = this.i[this.c.a()];
                ChunkExtractorWrapper chunkExtractorWrapper = representationHolder3.f3451a;
                if (chunkExtractorWrapper != null) {
                    Representation representation = representationHolder3.b;
                    RangedUri f2 = chunkExtractorWrapper.b() == null ? representation.f() : null;
                    RangedUri e2 = representationHolder3.c == null ? representation.e() : null;
                    if (!(f2 == null && e2 == null)) {
                        chunkHolder2.f3434a = a(representationHolder3, this.e, this.c.e(), this.c.f(), this.c.b(), f2, e2);
                        return;
                    }
                }
                long a6 = representationHolder3.d;
                long j7 = -9223372036854775807L;
                int i4 = (a6 > -9223372036854775807L ? 1 : (a6 == -9223372036854775807L ? 0 : -1));
                boolean z = i4 != 0;
                if (representationHolder3.b() == 0) {
                    chunkHolder2.b = z;
                    return;
                }
                long j8 = j6;
                long a7 = representationHolder3.a(this.j, this.k, j8);
                long b4 = representationHolder3.b(this.j, this.k, j8);
                a(representationHolder3, b4);
                long j9 = b4;
                boolean z2 = z;
                long a8 = a(representationHolder3, mediaChunk, j3, a7, j9);
                if (a8 < a7) {
                    this.l = new BehindLiveWindowException();
                    return;
                }
                int i5 = (a8 > j9 ? 1 : (a8 == j9 ? 0 : -1));
                if (i5 > 0 || (this.m && i5 >= 0)) {
                    chunkHolder2.b = z2;
                } else if (!z2 || representationHolder3.c(a8) < a6) {
                    int min = (int) Math.min((long) this.g, (j9 - a8) + 1);
                    if (i4 != 0) {
                        while (min > 1 && representationHolder3.c((((long) min) + a8) - 1) >= a6) {
                            min--;
                        }
                    }
                    int i6 = min;
                    if (list.isEmpty()) {
                        j7 = j3;
                    }
                    chunkHolder2.f3434a = a(representationHolder3, this.e, this.d, this.c.e(), this.c.f(), this.c.b(), a8, i6, j7);
                } else {
                    chunkHolder2.b = true;
                }
            }
        }
    }

    public void a(Chunk chunk) {
        SeekMap c2;
        if (chunk instanceof InitializationChunk) {
            int a2 = this.c.a(((InitializationChunk) chunk).c);
            RepresentationHolder representationHolder = this.i[a2];
            if (representationHolder.c == null && (c2 = representationHolder.f3451a.c()) != null) {
                this.i[a2] = representationHolder.a((DashSegmentIndex) new DashWrappingSegmentIndex((ChunkIndex) c2, representationHolder.b.c));
            }
        }
        PlayerEmsgHandler.PlayerTrackEmsgHandler playerTrackEmsgHandler = this.h;
        if (playerTrackEmsgHandler != null) {
            playerTrackEmsgHandler.b(chunk);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
        r7 = r5.i[r5.c.a(r6.c)];
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.google.android.exoplayer2.source.chunk.Chunk r6, boolean r7, java.lang.Exception r8, long r9) {
        /*
            r5 = this;
            r0 = 0
            if (r7 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.google.android.exoplayer2.source.dash.PlayerEmsgHandler$PlayerTrackEmsgHandler r7 = r5.h
            r1 = 1
            if (r7 == 0) goto L_0x0010
            boolean r7 = r7.a((com.google.android.exoplayer2.source.chunk.Chunk) r6)
            if (r7 == 0) goto L_0x0010
            return r1
        L_0x0010:
            com.google.android.exoplayer2.source.dash.manifest.DashManifest r7 = r5.j
            boolean r7 = r7.d
            if (r7 != 0) goto L_0x0052
            boolean r7 = r6 instanceof com.google.android.exoplayer2.source.chunk.MediaChunk
            if (r7 == 0) goto L_0x0052
            boolean r7 = r8 instanceof com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException
            if (r7 == 0) goto L_0x0052
            com.google.android.exoplayer2.upstream.HttpDataSource$InvalidResponseCodeException r8 = (com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException) r8
            int r7 = r8.responseCode
            r8 = 404(0x194, float:5.66E-43)
            if (r7 != r8) goto L_0x0052
            com.google.android.exoplayer2.source.dash.DefaultDashChunkSource$RepresentationHolder[] r7 = r5.i
            com.google.android.exoplayer2.trackselection.TrackSelection r8 = r5.c
            com.google.android.exoplayer2.Format r2 = r6.c
            int r8 = r8.a((com.google.android.exoplayer2.Format) r2)
            r7 = r7[r8]
            int r8 = r7.b()
            r2 = -1
            if (r8 == r2) goto L_0x0052
            if (r8 == 0) goto L_0x0052
            long r2 = r7.a()
            long r7 = (long) r8
            long r2 = r2 + r7
            r7 = 1
            long r2 = r2 - r7
            r7 = r6
            com.google.android.exoplayer2.source.chunk.MediaChunk r7 = (com.google.android.exoplayer2.source.chunk.MediaChunk) r7
            long r7 = r7.e()
            int r4 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x0052
            r5.m = r1
            return r1
        L_0x0052:
            r7 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r2 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x006a
            com.google.android.exoplayer2.trackselection.TrackSelection r7 = r5.c
            com.google.android.exoplayer2.Format r6 = r6.c
            int r6 = r7.a((com.google.android.exoplayer2.Format) r6)
            boolean r6 = r7.a((int) r6, (long) r9)
            if (r6 == 0) goto L_0x006a
            r0 = 1
        L_0x006a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.dash.DefaultDashChunkSource.a(com.google.android.exoplayer2.source.chunk.Chunk, boolean, java.lang.Exception, long):boolean");
    }

    private long a(RepresentationHolder representationHolder, MediaChunk mediaChunk, long j2, long j3, long j4) {
        if (mediaChunk != null) {
            return mediaChunk.e();
        }
        return Util.b(representationHolder.b(j2), j3, j4);
    }

    private void a(RepresentationHolder representationHolder, long j2) {
        this.n = this.j.d ? representationHolder.a(j2) : -9223372036854775807L;
    }

    private long a(long j2) {
        if (this.j.d && this.n != -9223372036854775807L) {
            return this.n - j2;
        }
        return -9223372036854775807L;
    }

    /* access modifiers changed from: protected */
    public Chunk a(RepresentationHolder representationHolder, DataSource dataSource, Format format, int i2, Object obj, RangedUri rangedUri, RangedUri rangedUri2) {
        String str = representationHolder.b.b;
        if (rangedUri != null && (rangedUri2 = rangedUri.a(rangedUri2, str)) == null) {
            rangedUri2 = rangedUri;
        }
        return new InitializationChunk(dataSource, new DataSpec(rangedUri2.a(str), rangedUri2.f3466a, rangedUri2.b, representationHolder.b.c()), format, i2, obj, representationHolder.f3451a);
    }

    /* access modifiers changed from: protected */
    public Chunk a(RepresentationHolder representationHolder, DataSource dataSource, int i2, Format format, int i3, Object obj, long j2, int i4, long j3) {
        RepresentationHolder representationHolder2 = representationHolder;
        long j4 = j2;
        Representation representation = representationHolder2.b;
        long c2 = representationHolder2.c(j4);
        RangedUri d2 = representationHolder2.d(j4);
        String str = representation.b;
        if (representationHolder2.f3451a == null) {
            return new SingleSampleMediaChunk(dataSource, new DataSpec(d2.a(str), d2.f3466a, d2.b, representation.c()), format, i3, obj, c2, representationHolder2.a(j4), j2, i2, format);
        }
        int i5 = 1;
        RangedUri rangedUri = d2;
        int i6 = 1;
        int i7 = i4;
        while (i5 < i7) {
            RangedUri a2 = rangedUri.a(representationHolder2.d(((long) i5) + j4), str);
            if (a2 == null) {
                break;
            }
            i6++;
            i5++;
            rangedUri = a2;
        }
        long a3 = representationHolder2.a((((long) i6) + j4) - 1);
        long a4 = representationHolder.d;
        long j5 = (a4 == -9223372036854775807L || a4 > a3) ? -9223372036854775807L : a4;
        DataSpec dataSpec = r18;
        DataSpec dataSpec2 = new DataSpec(rangedUri.a(str), rangedUri.f3466a, rangedUri.b, representation.c());
        ChunkExtractorWrapper chunkExtractorWrapper = representationHolder2.f3451a;
        return new ContainerMediaChunk(dataSource, dataSpec, format, i3, obj, c2, a3, j3, j5, j2, i6, -representation.c, chunkExtractorWrapper);
    }
}
