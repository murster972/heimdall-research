package com.google.android.exoplayer2.source.dash;

import android.util.Pair;
import android.util.SparseIntArray;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.source.CompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.EmptySampleStream;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.SequenceableLoader;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.chunk.ChunkSampleStream;
import com.google.android.exoplayer2.source.dash.DashChunkSource;
import com.google.android.exoplayer2.source.dash.PlayerEmsgHandler;
import com.google.android.exoplayer2.source.dash.manifest.AdaptationSet;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.manifest.Descriptor;
import com.google.android.exoplayer2.source.dash.manifest.EventStream;
import com.google.android.exoplayer2.source.dash.manifest.Period;
import com.google.android.exoplayer2.source.dash.manifest.Representation;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.LoaderErrorThrower;
import com.google.android.exoplayer2.upstream.TransferListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;

final class DashMediaPeriod implements MediaPeriod, SequenceableLoader.Callback<ChunkSampleStream<DashChunkSource>>, ChunkSampleStream.ReleaseCallback<DashChunkSource> {

    /* renamed from: a  reason: collision with root package name */
    final int f3439a;
    private final DashChunkSource.Factory b;
    private final TransferListener c;
    private final LoadErrorHandlingPolicy d;
    private final long e;
    private final LoaderErrorThrower f;
    private final Allocator g;
    private final TrackGroupArray h;
    private final TrackGroupInfo[] i;
    private final CompositeSequenceableLoaderFactory j;
    private final PlayerEmsgHandler k;
    private final IdentityHashMap<ChunkSampleStream<DashChunkSource>, PlayerEmsgHandler.PlayerTrackEmsgHandler> l = new IdentityHashMap<>();
    private final MediaSourceEventListener.EventDispatcher m;
    private MediaPeriod.Callback n;
    private ChunkSampleStream<DashChunkSource>[] o = a(0);
    private EventSampleStream[] p = new EventSampleStream[0];
    private SequenceableLoader q;
    private DashManifest r;
    private int s;
    private List<EventStream> t;
    private boolean u;

    private static final class TrackGroupInfo {

        /* renamed from: a  reason: collision with root package name */
        public final int[] f3440a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;

        private TrackGroupInfo(int i, int i2, int[] iArr, int i3, int i4, int i5, int i6) {
            this.b = i;
            this.f3440a = iArr;
            this.c = i2;
            this.e = i3;
            this.f = i4;
            this.g = i5;
            this.d = i6;
        }

        public static TrackGroupInfo a(int i, int[] iArr, int i2, int i3, int i4) {
            return new TrackGroupInfo(i, 0, iArr, i2, i3, i4, -1);
        }

        public static TrackGroupInfo b(int[] iArr, int i) {
            return new TrackGroupInfo(4, 1, iArr, i, -1, -1, -1);
        }

        public static TrackGroupInfo a(int[] iArr, int i) {
            return new TrackGroupInfo(3, 1, iArr, i, -1, -1, -1);
        }

        public static TrackGroupInfo a(int i) {
            return new TrackGroupInfo(4, 2, (int[]) null, -1, -1, -1, i);
        }
    }

    public DashMediaPeriod(int i2, DashManifest dashManifest, int i3, DashChunkSource.Factory factory, TransferListener transferListener, LoadErrorHandlingPolicy loadErrorHandlingPolicy, MediaSourceEventListener.EventDispatcher eventDispatcher, long j2, LoaderErrorThrower loaderErrorThrower, Allocator allocator, CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory, PlayerEmsgHandler.PlayerEmsgCallback playerEmsgCallback) {
        this.f3439a = i2;
        this.r = dashManifest;
        this.s = i3;
        this.b = factory;
        this.c = transferListener;
        this.d = loadErrorHandlingPolicy;
        this.m = eventDispatcher;
        this.e = j2;
        this.f = loaderErrorThrower;
        this.g = allocator;
        this.j = compositeSequenceableLoaderFactory;
        this.k = new PlayerEmsgHandler(dashManifest, playerEmsgCallback, allocator);
        this.q = compositeSequenceableLoaderFactory.a(this.o);
        Period a2 = dashManifest.a(i3);
        this.t = a2.d;
        Pair<TrackGroupArray, TrackGroupInfo[]> a3 = a(a2.c, this.t);
        this.h = (TrackGroupArray) a3.first;
        this.i = (TrackGroupInfo[]) a3.second;
        eventDispatcher.a();
    }

    public boolean b(long j2) {
        return this.q.b(j2);
    }

    public void c(long j2) {
        this.q.c(j2);
    }

    public void d() throws IOException {
        this.f.a();
    }

    public TrackGroupArray e() {
        return this.h;
    }

    public long f() {
        return this.q.f();
    }

    public void g() {
        this.k.b();
        for (ChunkSampleStream<DashChunkSource> a2 : this.o) {
            a2.a((ChunkSampleStream.ReleaseCallback<DashChunkSource>) this);
        }
        this.n = null;
        this.m.b();
    }

    public void a(DashManifest dashManifest, int i2) {
        this.r = dashManifest;
        this.s = i2;
        this.k.a(dashManifest);
        ChunkSampleStream<DashChunkSource>[] chunkSampleStreamArr = this.o;
        if (chunkSampleStreamArr != null) {
            for (ChunkSampleStream<DashChunkSource> h2 : chunkSampleStreamArr) {
                h2.h().a(dashManifest, i2);
            }
            this.n.a(this);
        }
        this.t = dashManifest.a(i2).d;
        for (EventSampleStream eventSampleStream : this.p) {
            Iterator<EventStream> it2 = this.t.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                EventStream next = it2.next();
                if (next.a().equals(eventSampleStream.b())) {
                    boolean z = true;
                    int a2 = dashManifest.a() - 1;
                    if (!dashManifest.d || i2 != a2) {
                        z = false;
                    }
                    eventSampleStream.a(next, z);
                }
            }
        }
    }

    public long b() {
        return this.q.b();
    }

    public long c() {
        if (this.u) {
            return -9223372036854775807L;
        }
        this.m.c();
        this.u = true;
        return -9223372036854775807L;
    }

    /* renamed from: b */
    public void a(ChunkSampleStream<DashChunkSource> chunkSampleStream) {
        this.n.a(this);
    }

    private static int[][] b(List<AdaptationSet> list) {
        int size = list.size();
        SparseIntArray sparseIntArray = new SparseIntArray(size);
        for (int i2 = 0; i2 < size; i2++) {
            sparseIntArray.put(list.get(i2).f3458a, i2);
        }
        int[][] iArr = new int[size][];
        boolean[] zArr = new boolean[size];
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            if (!zArr[i4]) {
                zArr[i4] = true;
                Descriptor a2 = a(list.get(i4).e);
                if (a2 == null) {
                    iArr[i3] = new int[]{i4};
                    i3++;
                } else {
                    String[] split = a2.b.split(",");
                    int[] iArr2 = new int[(split.length + 1)];
                    iArr2[0] = i4;
                    int i5 = 0;
                    while (i5 < split.length) {
                        int i6 = sparseIntArray.get(Integer.parseInt(split[i5]));
                        zArr[i6] = true;
                        i5++;
                        iArr2[i5] = i6;
                    }
                    iArr[i3] = iArr2;
                    i3++;
                }
            }
        }
        return i3 < size ? (int[][]) Arrays.copyOf(iArr, i3) : iArr;
    }

    public synchronized void a(ChunkSampleStream<DashChunkSource> chunkSampleStream) {
        PlayerEmsgHandler.PlayerTrackEmsgHandler remove = this.l.remove(chunkSampleStream);
        if (remove != null) {
            remove.a();
        }
    }

    public void a(MediaPeriod.Callback callback, long j2) {
        this.n = callback;
        callback.a(this);
    }

    public long a(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr, boolean[] zArr2, long j2) {
        int[] a2 = a(trackSelectionArr);
        a(trackSelectionArr, zArr, sampleStreamArr);
        a(trackSelectionArr, sampleStreamArr, a2);
        a(trackSelectionArr, sampleStreamArr, zArr2, j2, a2);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (ChunkSampleStream chunkSampleStream : sampleStreamArr) {
            if (chunkSampleStream instanceof ChunkSampleStream) {
                arrayList.add(chunkSampleStream);
            } else if (chunkSampleStream instanceof EventSampleStream) {
                arrayList2.add((EventSampleStream) chunkSampleStream);
            }
        }
        this.o = a(arrayList.size());
        arrayList.toArray(this.o);
        this.p = new EventSampleStream[arrayList2.size()];
        arrayList2.toArray(this.p);
        this.q = this.j.a(this.o);
        return j2;
    }

    private static boolean b(List<AdaptationSet> list, int[] iArr) {
        for (int i2 : iArr) {
            List<Representation> list2 = list.get(i2).c;
            for (int i3 = 0; i3 < list2.size(); i3++) {
                if (!list2.get(i3).d.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void a(long j2, boolean z) {
        for (ChunkSampleStream<DashChunkSource> a2 : this.o) {
            a2.a(j2, z);
        }
    }

    public long a(long j2) {
        for (ChunkSampleStream<DashChunkSource> a2 : this.o) {
            a2.a(j2);
        }
        for (EventSampleStream a3 : this.p) {
            a3.a(j2);
        }
        return j2;
    }

    public long a(long j2, SeekParameters seekParameters) {
        for (ChunkSampleStream<DashChunkSource> chunkSampleStream : this.o) {
            if (chunkSampleStream.f3435a == 2) {
                return chunkSampleStream.a(j2, seekParameters);
            }
        }
        return j2;
    }

    private int[] a(TrackSelection[] trackSelectionArr) {
        int[] iArr = new int[trackSelectionArr.length];
        for (int i2 = 0; i2 < trackSelectionArr.length; i2++) {
            if (trackSelectionArr[i2] != null) {
                iArr[i2] = this.h.a(trackSelectionArr[i2].c());
            } else {
                iArr[i2] = -1;
            }
        }
        return iArr;
    }

    private void a(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr) {
        for (int i2 = 0; i2 < trackSelectionArr.length; i2++) {
            if (trackSelectionArr[i2] == null || !zArr[i2]) {
                if (sampleStreamArr[i2] instanceof ChunkSampleStream) {
                    sampleStreamArr[i2].a(this);
                } else if (sampleStreamArr[i2] instanceof ChunkSampleStream.EmbeddedSampleStream) {
                    sampleStreamArr[i2].b();
                }
                sampleStreamArr[i2] = null;
            }
        }
    }

    private void a(TrackSelection[] trackSelectionArr, SampleStream[] sampleStreamArr, int[] iArr) {
        boolean z;
        for (int i2 = 0; i2 < trackSelectionArr.length; i2++) {
            if ((sampleStreamArr[i2] instanceof EmptySampleStream) || (sampleStreamArr[i2] instanceof ChunkSampleStream.EmbeddedSampleStream)) {
                int a2 = a(i2, iArr);
                if (a2 == -1) {
                    z = sampleStreamArr[i2] instanceof EmptySampleStream;
                } else {
                    z = (sampleStreamArr[i2] instanceof ChunkSampleStream.EmbeddedSampleStream) && sampleStreamArr[i2].f3436a == sampleStreamArr[a2];
                }
                if (!z) {
                    if (sampleStreamArr[i2] instanceof ChunkSampleStream.EmbeddedSampleStream) {
                        sampleStreamArr[i2].b();
                    }
                    sampleStreamArr[i2] = null;
                }
            }
        }
    }

    private void a(TrackSelection[] trackSelectionArr, SampleStream[] sampleStreamArr, boolean[] zArr, long j2, int[] iArr) {
        for (int i2 = 0; i2 < trackSelectionArr.length; i2++) {
            if (sampleStreamArr[i2] == null && trackSelectionArr[i2] != null) {
                zArr[i2] = true;
                TrackGroupInfo trackGroupInfo = this.i[iArr[i2]];
                int i3 = trackGroupInfo.c;
                if (i3 == 0) {
                    sampleStreamArr[i2] = a(trackGroupInfo, trackSelectionArr[i2], j2);
                } else if (i3 == 2) {
                    sampleStreamArr[i2] = new EventSampleStream(this.t.get(trackGroupInfo.d), trackSelectionArr[i2].c().a(0), this.r.d);
                }
            }
        }
        for (int i4 = 0; i4 < trackSelectionArr.length; i4++) {
            if (sampleStreamArr[i4] == null && trackSelectionArr[i4] != null) {
                TrackGroupInfo trackGroupInfo2 = this.i[iArr[i4]];
                if (trackGroupInfo2.c == 1) {
                    int a2 = a(i4, iArr);
                    if (a2 == -1) {
                        sampleStreamArr[i4] = new EmptySampleStream();
                    } else {
                        sampleStreamArr[i4] = sampleStreamArr[a2].a(j2, trackGroupInfo2.b);
                    }
                }
            }
        }
    }

    private int a(int i2, int[] iArr) {
        int i3 = iArr[i2];
        if (i3 == -1) {
            return -1;
        }
        int i4 = this.i[i3].e;
        for (int i5 = 0; i5 < iArr.length; i5++) {
            int i6 = iArr[i5];
            if (i6 == i4 && this.i[i6].c == 0) {
                return i5;
            }
        }
        return -1;
    }

    private static Pair<TrackGroupArray, TrackGroupInfo[]> a(List<AdaptationSet> list, List<EventStream> list2) {
        int[][] b2 = b(list);
        int length = b2.length;
        boolean[] zArr = new boolean[length];
        boolean[] zArr2 = new boolean[length];
        int a2 = a(length, list, b2, zArr, zArr2) + length + list2.size();
        TrackGroup[] trackGroupArr = new TrackGroup[a2];
        TrackGroupInfo[] trackGroupInfoArr = new TrackGroupInfo[a2];
        a(list2, trackGroupArr, trackGroupInfoArr, a(list, b2, length, zArr, zArr2, trackGroupArr, trackGroupInfoArr));
        return Pair.create(new TrackGroupArray(trackGroupArr), trackGroupInfoArr);
    }

    private static int a(int i2, List<AdaptationSet> list, int[][] iArr, boolean[] zArr, boolean[] zArr2) {
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            if (b(list, iArr[i4])) {
                zArr[i4] = true;
                i3++;
            }
            if (a(list, iArr[i4])) {
                zArr2[i4] = true;
                i3++;
            }
        }
        return i3;
    }

    private static int a(List<AdaptationSet> list, int[][] iArr, int i2, boolean[] zArr, boolean[] zArr2, TrackGroup[] trackGroupArr, TrackGroupInfo[] trackGroupInfoArr) {
        int i3;
        int i4;
        List<AdaptationSet> list2 = list;
        int i5 = i2;
        int i6 = 0;
        int i7 = 0;
        while (i6 < i5) {
            int[] iArr2 = iArr[i6];
            ArrayList arrayList = new ArrayList();
            for (int i8 : iArr2) {
                arrayList.addAll(list2.get(i8).c);
            }
            Format[] formatArr = new Format[arrayList.size()];
            for (int i9 = 0; i9 < formatArr.length; i9++) {
                formatArr[i9] = ((Representation) arrayList.get(i9)).f3467a;
            }
            AdaptationSet adaptationSet = list2.get(iArr2[0]);
            int i10 = i7 + 1;
            if (zArr[i6]) {
                i3 = i10 + 1;
            } else {
                i3 = i10;
                i10 = -1;
            }
            if (zArr2[i6]) {
                i4 = i3 + 1;
            } else {
                i4 = i3;
                i3 = -1;
            }
            trackGroupArr[i7] = new TrackGroup(formatArr);
            trackGroupInfoArr[i7] = TrackGroupInfo.a(adaptationSet.b, iArr2, i7, i10, i3);
            if (i10 != -1) {
                trackGroupArr[i10] = new TrackGroup(Format.a(adaptationSet.f3458a + ":emsg", "application/x-emsg", (String) null, -1, (DrmInitData) null));
                trackGroupInfoArr[i10] = TrackGroupInfo.b(iArr2, i7);
            }
            if (i3 != -1) {
                trackGroupArr[i3] = new TrackGroup(Format.a(adaptationSet.f3458a + ":cea608", "application/cea-608", 0, (String) null));
                trackGroupInfoArr[i3] = TrackGroupInfo.a(iArr2, i7);
            }
            i6++;
            i7 = i4;
        }
        return i7;
    }

    private static void a(List<EventStream> list, TrackGroup[] trackGroupArr, TrackGroupInfo[] trackGroupInfoArr, int i2) {
        int i3 = i2;
        int i4 = 0;
        while (i4 < list.size()) {
            trackGroupArr[i3] = new TrackGroup(Format.a(list.get(i4).a(), "application/x-emsg", (String) null, -1, (DrmInitData) null));
            trackGroupInfoArr[i3] = TrackGroupInfo.a(i4);
            i4++;
            i3++;
        }
    }

    private ChunkSampleStream<DashChunkSource> a(TrackGroupInfo trackGroupInfo, TrackSelection trackSelection, long j2) {
        int i2;
        TrackGroupInfo trackGroupInfo2 = trackGroupInfo;
        int[] iArr = new int[2];
        Format[] formatArr = new Format[2];
        boolean z = trackGroupInfo2.f != -1;
        if (z) {
            formatArr[0] = this.h.a(trackGroupInfo2.f).a(0);
            iArr[0] = 4;
            i2 = 1;
        } else {
            i2 = 0;
        }
        boolean z2 = trackGroupInfo2.g != -1;
        if (z2) {
            formatArr[i2] = this.h.a(trackGroupInfo2.g).a(0);
            iArr[i2] = 3;
            i2++;
        }
        if (i2 < iArr.length) {
            formatArr = (Format[]) Arrays.copyOf(formatArr, i2);
            iArr = Arrays.copyOf(iArr, i2);
        }
        Format[] formatArr2 = formatArr;
        int[] iArr2 = iArr;
        PlayerEmsgHandler.PlayerTrackEmsgHandler a2 = (!this.r.d || !z) ? null : this.k.a();
        PlayerEmsgHandler.PlayerTrackEmsgHandler playerTrackEmsgHandler = a2;
        ChunkSampleStream chunkSampleStream = new ChunkSampleStream(trackGroupInfo2.b, iArr2, formatArr2, this.b.a(this.f, this.r, this.s, trackGroupInfo2.f3440a, trackSelection, trackGroupInfo2.b, this.e, z, z2, a2, this.c), this, this.g, j2, this.d, this.m);
        synchronized (this) {
            this.l.put(chunkSampleStream, playerTrackEmsgHandler);
        }
        return chunkSampleStream;
    }

    private static Descriptor a(List<Descriptor> list) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            Descriptor descriptor = list.get(i2);
            if ("urn:mpeg:dash:adaptation-set-switching:2016".equals(descriptor.f3462a)) {
                return descriptor;
            }
        }
        return null;
    }

    private static boolean a(List<AdaptationSet> list, int[] iArr) {
        for (int i2 : iArr) {
            List<Descriptor> list2 = list.get(i2).d;
            for (int i3 = 0; i3 < list2.size(); i3++) {
                if ("urn:scte:dash:cc:cea-608:2015".equals(list2.get(i3).f3462a)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static ChunkSampleStream<DashChunkSource>[] a(int i2) {
        return new ChunkSampleStream[i2];
    }
}
