package com.google.android.exoplayer2.source;

import android.util.Pair;
import com.google.android.exoplayer2.Timeline;

abstract class AbstractConcatenatedTimeline extends Timeline {
    private final int b;
    private final ShuffleOrder c;
    private final boolean d;

    public AbstractConcatenatedTimeline(boolean z, ShuffleOrder shuffleOrder) {
        this.d = z;
        this.c = shuffleOrder;
        this.b = shuffleOrder.getLength();
    }

    public static Object a(Object obj, Object obj2) {
        return Pair.create(obj, obj2);
    }

    public static Object c(Object obj) {
        return ((Pair) obj).second;
    }

    public static Object d(Object obj) {
        return ((Pair) obj).first;
    }

    /* access modifiers changed from: protected */
    public abstract int b(int i);

    public int b(int i, int i2, boolean z) {
        int i3 = 0;
        if (this.d) {
            if (i2 == 1) {
                i2 = 2;
            }
            z = false;
        }
        int c2 = c(i);
        int f = f(c2);
        Timeline g = g(c2);
        int i4 = i - f;
        if (i2 != 2) {
            i3 = i2;
        }
        int b2 = g.b(i4, i3, z);
        if (b2 != -1) {
            return f + b2;
        }
        int b3 = b(c2, z);
        while (b3 != -1 && g(b3).c()) {
            b3 = b(b3, z);
        }
        if (b3 != -1) {
            return f(b3) + g(b3).b(z);
        }
        if (i2 == 2) {
            return b(z);
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public abstract int b(Object obj);

    /* access modifiers changed from: protected */
    public abstract int c(int i);

    /* access modifiers changed from: protected */
    public abstract Object d(int i);

    /* access modifiers changed from: protected */
    public abstract int e(int i);

    /* access modifiers changed from: protected */
    public abstract int f(int i);

    /* access modifiers changed from: protected */
    public abstract Timeline g(int i);

    public int a(int i, int i2, boolean z) {
        int i3 = 0;
        if (this.d) {
            if (i2 == 1) {
                i2 = 2;
            }
            z = false;
        }
        int c2 = c(i);
        int f = f(c2);
        Timeline g = g(c2);
        int i4 = i - f;
        if (i2 != 2) {
            i3 = i2;
        }
        int a2 = g.a(i4, i3, z);
        if (a2 != -1) {
            return f + a2;
        }
        int a3 = a(c2, z);
        while (a3 != -1 && g(a3).c()) {
            a3 = a(a3, z);
        }
        if (a3 != -1) {
            return f(a3) + g(a3).a(z);
        }
        if (i2 == 2) {
            return a(z);
        }
        return -1;
    }

    public int b(boolean z) {
        if (this.b == 0) {
            return -1;
        }
        if (this.d) {
            z = false;
        }
        int a2 = z ? this.c.a() : this.b - 1;
        while (g(a2).c()) {
            a2 = b(a2, z);
            if (a2 == -1) {
                return -1;
            }
        }
        return f(a2) + g(a2).b(z);
    }

    public int a(boolean z) {
        if (this.b == 0) {
            return -1;
        }
        int i = 0;
        if (this.d) {
            z = false;
        }
        if (z) {
            i = this.c.c();
        }
        while (g(i).c()) {
            i = a(i, z);
            if (i == -1) {
                return -1;
            }
        }
        return f(i) + g(i).a(z);
    }

    private int b(int i, boolean z) {
        if (z) {
            return this.c.a(i);
        }
        if (i > 0) {
            return i - 1;
        }
        return -1;
    }

    public final Timeline.Window a(int i, Timeline.Window window, boolean z, long j) {
        int c2 = c(i);
        int f = f(c2);
        int e = e(c2);
        g(c2).a(i - f, window, z, j);
        window.c += e;
        window.d += e;
        return window;
    }

    public final Timeline.Period a(Object obj, Timeline.Period period) {
        Object d2 = d(obj);
        Object c2 = c(obj);
        int b2 = b(d2);
        int f = f(b2);
        g(b2).a(c2, period);
        period.b += f;
        period.f3176a = obj;
        return period;
    }

    public final Timeline.Period a(int i, Timeline.Period period, boolean z) {
        int b2 = b(i);
        int f = f(b2);
        g(b2).a(i - e(b2), period, z);
        period.b += f;
        if (z) {
            period.f3176a = a(d(b2), period.f3176a);
        }
        return period;
    }

    public final int a(Object obj) {
        int a2;
        if (!(obj instanceof Pair)) {
            return -1;
        }
        Object d2 = d(obj);
        Object c2 = c(obj);
        int b2 = b(d2);
        if (b2 == -1 || (a2 = g(b2).a(c2)) == -1) {
            return -1;
        }
        return e(b2) + a2;
    }

    public final Object a(int i) {
        int b2 = b(i);
        return a(d(b2), g(b2).a(i - e(b2)));
    }

    private int a(int i, boolean z) {
        if (z) {
            return this.c.b(i);
        }
        if (i < this.b - 1) {
            return i + 1;
        }
        return -1;
    }
}
