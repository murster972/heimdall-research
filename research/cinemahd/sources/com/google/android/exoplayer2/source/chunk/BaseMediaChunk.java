package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;

public abstract class BaseMediaChunk extends MediaChunk {
    public final long j;
    public final long k;
    private BaseMediaChunkOutput l;
    private int[] m;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseMediaChunk(DataSource dataSource, DataSpec dataSpec, Format format, int i, Object obj, long j2, long j3, long j4, long j5, long j6) {
        super(dataSource, dataSpec, format, i, obj, j2, j3, j6);
        this.j = j4;
        this.k = j5;
    }

    public void a(BaseMediaChunkOutput baseMediaChunkOutput) {
        this.l = baseMediaChunkOutput;
        this.m = baseMediaChunkOutput.a();
    }

    /* access modifiers changed from: protected */
    public final BaseMediaChunkOutput g() {
        return this.l;
    }

    public final int a(int i) {
        return this.m[i];
    }
}
