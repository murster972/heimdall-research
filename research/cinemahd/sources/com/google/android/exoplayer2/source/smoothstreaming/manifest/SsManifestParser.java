package com.google.android.exoplayer2.source.smoothstreaming.manifest;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifest;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.Util;
import com.vungle.warren.model.Advertisement;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class SsManifestParser implements ParsingLoadable.Parser<SsManifest> {

    /* renamed from: a  reason: collision with root package name */
    private final XmlPullParserFactory f3511a;

    public static class MissingFieldException extends ParserException {
        public MissingFieldException(String str) {
            super("Missing required field: " + str);
        }
    }

    private static class ProtectionParser extends ElementParser {
        private boolean e;
        private UUID f;
        private byte[] g;

        public ProtectionParser(ElementParser elementParser, String str) {
            super(elementParser, str, "Protection");
        }

        public Object a() {
            UUID uuid = this.f;
            return new SsManifest.ProtectionElement(uuid, PsshAtomUtil.a(uuid, this.g));
        }

        public boolean b(String str) {
            return "ProtectionHeader".equals(str);
        }

        public void c(XmlPullParser xmlPullParser) {
            if ("ProtectionHeader".equals(xmlPullParser.getName())) {
                this.e = true;
                this.f = UUID.fromString(c(xmlPullParser.getAttributeValue((String) null, "SystemID")));
            }
        }

        public void d(XmlPullParser xmlPullParser) {
            if (this.e) {
                this.g = Base64.decode(xmlPullParser.getText(), 0);
            }
        }

        public void b(XmlPullParser xmlPullParser) {
            if ("ProtectionHeader".equals(xmlPullParser.getName())) {
                this.e = false;
            }
        }

        private static String c(String str) {
            return (str.charAt(0) == '{' && str.charAt(str.length() - 1) == '}') ? str.substring(1, str.length() - 1) : str;
        }
    }

    public SsManifestParser() {
        try {
            this.f3511a = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    private static class StreamIndexParser extends ElementParser {
        private final String e;
        private final List<Format> f = new LinkedList();
        private int g;
        private String h;
        private long i;
        private String j;
        private String k;
        private int l;
        private int m;
        private int n;
        private int o;
        private String p;
        private ArrayList<Long> q;
        private long r;

        public StreamIndexParser(ElementParser elementParser, String str) {
            super(elementParser, str, "StreamIndex");
            this.e = str;
        }

        private void e(XmlPullParser xmlPullParser) throws ParserException {
            this.g = g(xmlPullParser);
            a("Type", (Object) Integer.valueOf(this.g));
            if (this.g == 3) {
                this.h = c(xmlPullParser, "Subtype");
            } else {
                this.h = xmlPullParser.getAttributeValue((String) null, "Subtype");
            }
            this.j = xmlPullParser.getAttributeValue((String) null, "Name");
            this.k = c(xmlPullParser, "Url");
            this.l = a(xmlPullParser, "MaxWidth", -1);
            this.m = a(xmlPullParser, "MaxHeight", -1);
            this.n = a(xmlPullParser, "DisplayWidth", -1);
            this.o = a(xmlPullParser, "DisplayHeight", -1);
            this.p = xmlPullParser.getAttributeValue((String) null, "Language");
            a("Language", (Object) this.p);
            this.i = (long) a(xmlPullParser, "TimeScale", -1);
            if (this.i == -1) {
                this.i = ((Long) a("TimeScale")).longValue();
            }
            this.q = new ArrayList<>();
        }

        private void f(XmlPullParser xmlPullParser) throws ParserException {
            int size = this.q.size();
            long a2 = a(xmlPullParser, "t", -9223372036854775807L);
            int i2 = 1;
            if (a2 == -9223372036854775807L) {
                if (size == 0) {
                    a2 = 0;
                } else if (this.r != -1) {
                    a2 = this.q.get(size - 1).longValue() + this.r;
                } else {
                    throw new ParserException("Unable to infer start time");
                }
            }
            this.q.add(Long.valueOf(a2));
            this.r = a(xmlPullParser, "d", -9223372036854775807L);
            long a3 = a(xmlPullParser, "r", 1);
            if (a3 <= 1 || this.r != -9223372036854775807L) {
                while (true) {
                    long j2 = (long) i2;
                    if (j2 < a3) {
                        this.q.add(Long.valueOf((this.r * j2) + a2));
                        i2++;
                    } else {
                        return;
                    }
                }
            } else {
                throw new ParserException("Repeated chunk with unspecified duration");
            }
        }

        private int g(XmlPullParser xmlPullParser) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue((String) null, "Type");
            if (attributeValue == null) {
                throw new MissingFieldException("Type");
            } else if ("audio".equalsIgnoreCase(attributeValue)) {
                return 1;
            } else {
                if (Advertisement.KEY_VIDEO.equalsIgnoreCase(attributeValue)) {
                    return 2;
                }
                if ("text".equalsIgnoreCase(attributeValue)) {
                    return 3;
                }
                throw new ParserException("Invalid key value[" + attributeValue + "]");
            }
        }

        public void a(Object obj) {
            if (obj instanceof Format) {
                this.f.add((Format) obj);
            }
        }

        public boolean b(String str) {
            return "c".equals(str);
        }

        public void c(XmlPullParser xmlPullParser) throws ParserException {
            if ("c".equals(xmlPullParser.getName())) {
                f(xmlPullParser);
            } else {
                e(xmlPullParser);
            }
        }

        public Object a() {
            Format[] formatArr = new Format[this.f.size()];
            this.f.toArray(formatArr);
            SsManifest.StreamElement streamElement = r2;
            SsManifest.StreamElement streamElement2 = new SsManifest.StreamElement(this.e, this.k, this.g, this.h, this.i, this.j, this.l, this.m, this.n, this.o, this.p, formatArr, this.q, this.r);
            return streamElement;
        }
    }

    public SsManifest a(Uri uri, InputStream inputStream) throws IOException {
        try {
            XmlPullParser newPullParser = this.f3511a.newPullParser();
            newPullParser.setInput(inputStream, (String) null);
            return (SsManifest) new SmoothStreamingMediaParser((ElementParser) null, uri.toString()).a(newPullParser);
        } catch (XmlPullParserException e) {
            throw new ParserException((Throwable) e);
        }
    }

    private static class SmoothStreamingMediaParser extends ElementParser {
        private final List<SsManifest.StreamElement> e = new LinkedList();
        private int f;
        private int g;
        private long h;
        private long i;
        private long j;
        private int k = -1;
        private boolean l;
        private SsManifest.ProtectionElement m = null;

        public SmoothStreamingMediaParser(ElementParser elementParser, String str) {
            super(elementParser, str, "SmoothStreamingMedia");
        }

        public void a(Object obj) {
            if (obj instanceof SsManifest.StreamElement) {
                this.e.add((SsManifest.StreamElement) obj);
            } else if (obj instanceof SsManifest.ProtectionElement) {
                Assertions.b(this.m == null);
                this.m = (SsManifest.ProtectionElement) obj;
            }
        }

        public void c(XmlPullParser xmlPullParser) throws ParserException {
            this.f = a(xmlPullParser, "MajorVersion");
            this.g = a(xmlPullParser, "MinorVersion");
            this.h = a(xmlPullParser, "TimeScale", 10000000);
            this.i = b(xmlPullParser, "Duration");
            this.j = a(xmlPullParser, "DVRWindowLength", 0);
            this.k = a(xmlPullParser, "LookaheadCount", -1);
            this.l = a(xmlPullParser, "IsLive", false);
            a("TimeScale", (Object) Long.valueOf(this.h));
        }

        public Object a() {
            SsManifest.StreamElement[] streamElementArr = new SsManifest.StreamElement[this.e.size()];
            this.e.toArray(streamElementArr);
            SsManifest.ProtectionElement protectionElement = this.m;
            if (protectionElement != null) {
                DrmInitData drmInitData = new DrmInitData(new DrmInitData.SchemeData(protectionElement.f3509a, "video/mp4", protectionElement.b));
                for (SsManifest.StreamElement streamElement : streamElementArr) {
                    int i2 = streamElement.f3510a;
                    if (i2 == 2 || i2 == 1) {
                        Format[] formatArr = streamElement.j;
                        for (int i3 = 0; i3 < formatArr.length; i3++) {
                            formatArr[i3] = formatArr[i3].a(drmInitData);
                        }
                    }
                }
            }
            return new SsManifest(this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, streamElementArr);
        }
    }

    private static abstract class ElementParser {

        /* renamed from: a  reason: collision with root package name */
        private final String f3512a;
        private final String b;
        private final ElementParser c;
        private final List<Pair<String, Object>> d = new LinkedList();

        public ElementParser(ElementParser elementParser, String str, String str2) {
            this.c = elementParser;
            this.f3512a = str;
            this.b = str2;
        }

        /* access modifiers changed from: protected */
        public abstract Object a();

        public final Object a(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
            boolean z = false;
            int i = 0;
            while (true) {
                int eventType = xmlPullParser.getEventType();
                if (eventType == 1) {
                    return null;
                }
                if (eventType == 2) {
                    String name = xmlPullParser.getName();
                    if (this.b.equals(name)) {
                        c(xmlPullParser);
                        z = true;
                    } else if (z) {
                        if (i > 0) {
                            i++;
                        } else if (b(name)) {
                            c(xmlPullParser);
                        } else {
                            ElementParser a2 = a(this, name, this.f3512a);
                            if (a2 == null) {
                                i = 1;
                            } else {
                                a(a2.a(xmlPullParser));
                            }
                        }
                    }
                } else if (eventType != 3) {
                    if (eventType == 4 && z && i == 0) {
                        d(xmlPullParser);
                    }
                } else if (!z) {
                    continue;
                } else if (i > 0) {
                    i--;
                } else {
                    String name2 = xmlPullParser.getName();
                    b(xmlPullParser);
                    if (!b(name2)) {
                        return a();
                    }
                }
                xmlPullParser.next();
            }
        }

        /* access modifiers changed from: protected */
        public void a(Object obj) {
        }

        /* access modifiers changed from: protected */
        public final long b(XmlPullParser xmlPullParser, String str) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
            if (attributeValue != null) {
                try {
                    return Long.parseLong(attributeValue);
                } catch (NumberFormatException e) {
                    throw new ParserException((Throwable) e);
                }
            } else {
                throw new MissingFieldException(str);
            }
        }

        /* access modifiers changed from: protected */
        public void b(XmlPullParser xmlPullParser) {
        }

        /* access modifiers changed from: protected */
        public boolean b(String str) {
            return false;
        }

        /* access modifiers changed from: protected */
        public final String c(XmlPullParser xmlPullParser, String str) throws MissingFieldException {
            String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
            if (attributeValue != null) {
                return attributeValue;
            }
            throw new MissingFieldException(str);
        }

        /* access modifiers changed from: protected */
        public abstract void c(XmlPullParser xmlPullParser) throws ParserException;

        /* access modifiers changed from: protected */
        public void d(XmlPullParser xmlPullParser) {
        }

        private ElementParser a(ElementParser elementParser, String str, String str2) {
            if ("QualityLevel".equals(str)) {
                return new QualityLevelParser(elementParser, str2);
            }
            if ("Protection".equals(str)) {
                return new ProtectionParser(elementParser, str2);
            }
            if ("StreamIndex".equals(str)) {
                return new StreamIndexParser(elementParser, str2);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public final void a(String str, Object obj) {
            this.d.add(Pair.create(str, obj));
        }

        /* access modifiers changed from: protected */
        public final Object a(String str) {
            for (int i = 0; i < this.d.size(); i++) {
                Pair pair = this.d.get(i);
                if (((String) pair.first).equals(str)) {
                    return pair.second;
                }
            }
            ElementParser elementParser = this.c;
            if (elementParser == null) {
                return null;
            }
            return elementParser.a(str);
        }

        /* access modifiers changed from: protected */
        public final int a(XmlPullParser xmlPullParser, String str, int i) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
            if (attributeValue == null) {
                return i;
            }
            try {
                return Integer.parseInt(attributeValue);
            } catch (NumberFormatException e) {
                throw new ParserException((Throwable) e);
            }
        }

        /* access modifiers changed from: protected */
        public final int a(XmlPullParser xmlPullParser, String str) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
            if (attributeValue != null) {
                try {
                    return Integer.parseInt(attributeValue);
                } catch (NumberFormatException e) {
                    throw new ParserException((Throwable) e);
                }
            } else {
                throw new MissingFieldException(str);
            }
        }

        /* access modifiers changed from: protected */
        public final long a(XmlPullParser xmlPullParser, String str, long j) throws ParserException {
            String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
            if (attributeValue == null) {
                return j;
            }
            try {
                return Long.parseLong(attributeValue);
            } catch (NumberFormatException e) {
                throw new ParserException((Throwable) e);
            }
        }

        /* access modifiers changed from: protected */
        public final boolean a(XmlPullParser xmlPullParser, String str, boolean z) {
            String attributeValue = xmlPullParser.getAttributeValue((String) null, str);
            return attributeValue != null ? Boolean.parseBoolean(attributeValue) : z;
        }
    }

    private static class QualityLevelParser extends ElementParser {
        private Format e;

        public QualityLevelParser(ElementParser elementParser, String str) {
            super(elementParser, str, "QualityLevel");
        }

        private static String d(String str) {
            if (str.equalsIgnoreCase("H264") || str.equalsIgnoreCase("X264") || str.equalsIgnoreCase("AVC1") || str.equalsIgnoreCase("DAVC")) {
                return "video/avc";
            }
            if (str.equalsIgnoreCase("AAC") || str.equalsIgnoreCase("AACL") || str.equalsIgnoreCase("AACH") || str.equalsIgnoreCase("AACP")) {
                return "audio/mp4a-latm";
            }
            if (str.equalsIgnoreCase("TTML") || str.equalsIgnoreCase("DFXP")) {
                return "application/ttml+xml";
            }
            if (str.equalsIgnoreCase("ac-3") || str.equalsIgnoreCase("dac3")) {
                return "audio/ac3";
            }
            if (str.equalsIgnoreCase("ec-3") || str.equalsIgnoreCase("dec3")) {
                return "audio/eac3";
            }
            if (str.equalsIgnoreCase("dtsc")) {
                return "audio/vnd.dts";
            }
            if (str.equalsIgnoreCase("dtsh") || str.equalsIgnoreCase("dtsl")) {
                return "audio/vnd.dts.hd";
            }
            if (str.equalsIgnoreCase("dtse")) {
                return "audio/vnd.dts.hd;profile=lbr";
            }
            if (str.equalsIgnoreCase("opus")) {
                return "audio/opus";
            }
            return null;
        }

        public Object a() {
            return this.e;
        }

        public void c(XmlPullParser xmlPullParser) throws ParserException {
            int intValue = ((Integer) a("Type")).intValue();
            String attributeValue = xmlPullParser.getAttributeValue((String) null, "Index");
            String str = (String) a("Name");
            int a2 = a(xmlPullParser, "Bitrate");
            String d = d(c(xmlPullParser, "FourCC"));
            if (intValue == 2) {
                this.e = Format.a(attributeValue, str, "video/mp4", d, (String) null, a2, a(xmlPullParser, "MaxWidth"), a(xmlPullParser, "MaxHeight"), -1.0f, c(xmlPullParser.getAttributeValue((String) null, "CodecPrivateData")), 0);
            } else if (intValue == 1) {
                if (d == null) {
                    d = "audio/mp4a-latm";
                }
                int a3 = a(xmlPullParser, "Channels");
                int a4 = a(xmlPullParser, "SamplingRate");
                List<byte[]> c = c(xmlPullParser.getAttributeValue((String) null, "CodecPrivateData"));
                if (c.isEmpty() && "audio/mp4a-latm".equals(d)) {
                    c = Collections.singletonList(CodecSpecificDataUtil.a(a4, a3));
                }
                this.e = Format.a(attributeValue, str, "audio/mp4", d, (String) null, a2, a3, a4, c, 0, (String) a("Language"));
            } else if (intValue == 3) {
                this.e = Format.b(attributeValue, str, "application/mp4", d, (String) null, a2, 0, (String) a("Language"));
            } else {
                this.e = Format.a(attributeValue, str, "application/mp4", d, (String) null, a2, 0, (String) null);
            }
        }

        private static List<byte[]> c(String str) {
            ArrayList arrayList = new ArrayList();
            if (!TextUtils.isEmpty(str)) {
                byte[] a2 = Util.a(str);
                byte[][] b = CodecSpecificDataUtil.b(a2);
                if (b == null) {
                    arrayList.add(a2);
                } else {
                    Collections.addAll(arrayList, b);
                }
            }
            return arrayList;
        }
    }
}
