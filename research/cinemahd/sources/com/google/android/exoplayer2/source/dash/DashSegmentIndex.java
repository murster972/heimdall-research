package com.google.android.exoplayer2.source.dash;

import com.google.android.exoplayer2.source.dash.manifest.RangedUri;

public interface DashSegmentIndex {
    long a(long j);

    long a(long j, long j2);

    boolean a();

    long b();

    long b(long j, long j2);

    RangedUri b(long j);

    int c(long j);
}
