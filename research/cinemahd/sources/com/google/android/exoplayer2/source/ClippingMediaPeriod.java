package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public final class ClippingMediaPeriod implements MediaPeriod, MediaPeriod.Callback {

    /* renamed from: a  reason: collision with root package name */
    public final MediaPeriod f3399a;
    private MediaPeriod.Callback b;
    private ClippingSampleStream[] c = new ClippingSampleStream[0];
    private long d;
    long e;
    long f;

    private final class ClippingSampleStream implements SampleStream {

        /* renamed from: a  reason: collision with root package name */
        public final SampleStream f3400a;
        private boolean b;

        public ClippingSampleStream(SampleStream sampleStream) {
            this.f3400a = sampleStream;
        }

        public void a() throws IOException {
            this.f3400a.a();
        }

        public void b() {
            this.b = false;
        }

        public int d(long j) {
            if (ClippingMediaPeriod.this.a()) {
                return -3;
            }
            return this.f3400a.d(j);
        }

        public boolean isReady() {
            return !ClippingMediaPeriod.this.a() && this.f3400a.isReady();
        }

        public int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
            if (ClippingMediaPeriod.this.a()) {
                return -3;
            }
            if (this.b) {
                decoderInputBuffer.setFlags(4);
                return -4;
            }
            int a2 = this.f3400a.a(formatHolder, decoderInputBuffer, z);
            if (a2 == -5) {
                Format format = formatHolder.f3165a;
                if (!(format.w == 0 && format.x == 0)) {
                    int i = 0;
                    int i2 = ClippingMediaPeriod.this.e != 0 ? 0 : format.w;
                    if (ClippingMediaPeriod.this.f == Long.MIN_VALUE) {
                        i = format.x;
                    }
                    formatHolder.f3165a = format.a(i2, i);
                }
                return -5;
            }
            long j = ClippingMediaPeriod.this.f;
            if (j == Long.MIN_VALUE || ((a2 != -4 || decoderInputBuffer.c < j) && (a2 != -3 || ClippingMediaPeriod.this.f() != Long.MIN_VALUE))) {
                return a2;
            }
            decoderInputBuffer.clear();
            decoderInputBuffer.setFlags(4);
            this.b = true;
            return -4;
        }
    }

    public ClippingMediaPeriod(MediaPeriod mediaPeriod, boolean z, long j, long j2) {
        this.f3399a = mediaPeriod;
        this.d = z ? j : -9223372036854775807L;
        this.e = j;
        this.f = j2;
    }

    public long b() {
        long b2 = this.f3399a.b();
        if (b2 != Long.MIN_VALUE) {
            long j = this.f;
            if (j == Long.MIN_VALUE || b2 < j) {
                return b2;
            }
        }
        return Long.MIN_VALUE;
    }

    public void c(long j) {
        this.f3399a.c(j);
    }

    public void d() throws IOException {
        this.f3399a.d();
    }

    public TrackGroupArray e() {
        return this.f3399a.e();
    }

    public long f() {
        long f2 = this.f3399a.f();
        if (f2 != Long.MIN_VALUE) {
            long j = this.f;
            if (j == Long.MIN_VALUE || f2 < j) {
                return f2;
            }
        }
        return Long.MIN_VALUE;
    }

    public void a(MediaPeriod.Callback callback, long j) {
        this.b = callback;
        this.f3399a.a((MediaPeriod.Callback) this, j);
    }

    public long c() {
        if (a()) {
            long j = this.d;
            this.d = -9223372036854775807L;
            long c2 = c();
            return c2 != -9223372036854775807L ? c2 : j;
        }
        long c3 = this.f3399a.c();
        if (c3 == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        boolean z = true;
        Assertions.b(c3 >= this.e);
        long j2 = this.f;
        if (j2 != Long.MIN_VALUE && c3 > j2) {
            z = false;
        }
        Assertions.b(z);
        return c3;
    }

    public boolean b(long j) {
        return this.f3399a.b(j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0062, code lost:
        if (r2 > r4) goto L_0x0065;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(com.google.android.exoplayer2.trackselection.TrackSelection[] r13, boolean[] r14, com.google.android.exoplayer2.source.SampleStream[] r15, boolean[] r16, long r17) {
        /*
            r12 = this;
            r0 = r12
            r1 = r15
            int r2 = r1.length
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream[] r2 = new com.google.android.exoplayer2.source.ClippingMediaPeriod.ClippingSampleStream[r2]
            r0.c = r2
            int r2 = r1.length
            com.google.android.exoplayer2.source.SampleStream[] r9 = new com.google.android.exoplayer2.source.SampleStream[r2]
            r10 = 0
            r2 = 0
        L_0x000c:
            int r3 = r1.length
            r11 = 0
            if (r2 >= r3) goto L_0x0025
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream[] r3 = r0.c
            r4 = r1[r2]
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream r4 = (com.google.android.exoplayer2.source.ClippingMediaPeriod.ClippingSampleStream) r4
            r3[r2] = r4
            r4 = r3[r2]
            if (r4 == 0) goto L_0x0020
            r3 = r3[r2]
            com.google.android.exoplayer2.source.SampleStream r11 = r3.f3400a
        L_0x0020:
            r9[r2] = r11
            int r2 = r2 + 1
            goto L_0x000c
        L_0x0025:
            com.google.android.exoplayer2.source.MediaPeriod r2 = r0.f3399a
            r3 = r13
            r4 = r14
            r5 = r9
            r6 = r16
            r7 = r17
            long r2 = r2.a(r3, r4, r5, r6, r7)
            boolean r4 = r12.a()
            if (r4 == 0) goto L_0x0047
            long r4 = r0.e
            int r6 = (r17 > r4 ? 1 : (r17 == r4 ? 0 : -1))
            if (r6 != 0) goto L_0x0047
            r6 = r13
            boolean r4 = a((long) r4, (com.google.android.exoplayer2.trackselection.TrackSelection[]) r13)
            if (r4 == 0) goto L_0x0047
            r4 = r2
            goto L_0x004c
        L_0x0047:
            r4 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L_0x004c:
            r0.d = r4
            int r4 = (r2 > r17 ? 1 : (r2 == r17 ? 0 : -1))
            if (r4 == 0) goto L_0x0067
            long r4 = r0.e
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 < 0) goto L_0x0065
            long r4 = r0.f
            r6 = -9223372036854775808
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x0067
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 > 0) goto L_0x0065
            goto L_0x0067
        L_0x0065:
            r4 = 0
            goto L_0x0068
        L_0x0067:
            r4 = 1
        L_0x0068:
            com.google.android.exoplayer2.util.Assertions.b(r4)
        L_0x006b:
            int r4 = r1.length
            if (r10 >= r4) goto L_0x0099
            r4 = r9[r10]
            if (r4 != 0) goto L_0x0077
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream[] r4 = r0.c
            r4[r10] = r11
            goto L_0x0090
        L_0x0077:
            r4 = r1[r10]
            if (r4 == 0) goto L_0x0085
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream[] r4 = r0.c
            r4 = r4[r10]
            com.google.android.exoplayer2.source.SampleStream r4 = r4.f3400a
            r5 = r9[r10]
            if (r4 == r5) goto L_0x0090
        L_0x0085:
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream[] r4 = r0.c
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream r5 = new com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream
            r6 = r9[r10]
            r5.<init>(r6)
            r4[r10] = r5
        L_0x0090:
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream[] r4 = r0.c
            r4 = r4[r10]
            r1[r10] = r4
            int r10 = r10 + 1
            goto L_0x006b
        L_0x0099:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.ClippingMediaPeriod.a(com.google.android.exoplayer2.trackselection.TrackSelection[], boolean[], com.google.android.exoplayer2.source.SampleStream[], boolean[], long):long");
    }

    /* renamed from: b */
    public void a(MediaPeriod mediaPeriod) {
        this.b.a(this);
    }

    private SeekParameters b(long j, SeekParameters seekParameters) {
        long b2 = Util.b(seekParameters.f3173a, 0, j - this.e);
        long j2 = seekParameters.b;
        long j3 = this.f;
        long b3 = Util.b(j2, 0, j3 == Long.MIN_VALUE ? Long.MAX_VALUE : j3 - j);
        if (b2 == seekParameters.f3173a && b3 == seekParameters.b) {
            return seekParameters;
        }
        return new SeekParameters(b2, b3);
    }

    public void a(long j, boolean z) {
        this.f3399a.a(j, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        if (r0 > r7) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(long r7) {
        /*
            r6 = this;
            r0 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r6.d = r0
            com.google.android.exoplayer2.source.ClippingMediaPeriod$ClippingSampleStream[] r0 = r6.c
            int r1 = r0.length
            r2 = 0
            r3 = 0
        L_0x000c:
            if (r3 >= r1) goto L_0x0018
            r4 = r0[r3]
            if (r4 == 0) goto L_0x0015
            r4.b()
        L_0x0015:
            int r3 = r3 + 1
            goto L_0x000c
        L_0x0018:
            com.google.android.exoplayer2.source.MediaPeriod r0 = r6.f3399a
            long r0 = r0.a(r7)
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 == 0) goto L_0x0034
            long r7 = r6.e
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 < 0) goto L_0x0035
            long r7 = r6.f
            r3 = -9223372036854775808
            int r5 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0034
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 > 0) goto L_0x0035
        L_0x0034:
            r2 = 1
        L_0x0035:
            com.google.android.exoplayer2.util.Assertions.b(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.ClippingMediaPeriod.a(long):long");
    }

    public long a(long j, SeekParameters seekParameters) {
        long j2 = this.e;
        if (j == j2) {
            return j2;
        }
        return this.f3399a.a(j, b(j, seekParameters));
    }

    public void a(MediaPeriod mediaPeriod) {
        this.b.a(this);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.d != -9223372036854775807L;
    }

    private static boolean a(long j, TrackSelection[] trackSelectionArr) {
        if (j != 0) {
            for (TrackSelection trackSelection : trackSelectionArr) {
                if (trackSelection != null && !MimeTypes.j(trackSelection.e().g)) {
                    return true;
                }
            }
        }
        return false;
    }
}
