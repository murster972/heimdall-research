package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.DefaultExtractorInput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public final class SingleSampleMediaChunk extends BaseMediaChunk {
    private final int n;
    private final Format o;
    private long p;
    private boolean q;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SingleSampleMediaChunk(DataSource dataSource, DataSpec dataSpec, Format format, int i, Object obj, long j, long j2, long j3, int i2, Format format2) {
        super(dataSource, dataSpec, format, i, obj, j, j2, -9223372036854775807L, -9223372036854775807L, j3);
        this.n = i2;
        this.o = format2;
    }

    public void cancelLoad() {
    }

    public boolean f() {
        return this.q;
    }

    /* JADX INFO: finally extract failed */
    public void load() throws IOException, InterruptedException {
        try {
            long a2 = this.h.a(this.f3431a.a(this.p));
            if (a2 != -1) {
                a2 += this.p;
            }
            DefaultExtractorInput defaultExtractorInput = new DefaultExtractorInput(this.h, this.p, a2);
            BaseMediaChunkOutput g = g();
            g.a(0);
            TrackOutput a3 = g.a(0, this.n);
            a3.a(this.o);
            for (int i = 0; i != -1; i = a3.a(defaultExtractorInput, Integer.MAX_VALUE, true)) {
                this.p += (long) i;
            }
            a3.a(this.f, 1, (int) this.p, 0, (TrackOutput.CryptoData) null);
            Util.a((DataSource) this.h);
            this.q = true;
        } catch (Throwable th) {
            Util.a((DataSource) this.h);
            throw th;
        }
    }
}
