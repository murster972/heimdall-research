package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.SeekParameters;
import java.io.IOException;
import java.util.List;

public interface ChunkSource {
    int a(long j, List<? extends MediaChunk> list);

    long a(long j, SeekParameters seekParameters);

    void a() throws IOException;

    void a(long j, long j2, List<? extends MediaChunk> list, ChunkHolder chunkHolder);

    void a(Chunk chunk);

    boolean a(Chunk chunk, boolean z, Exception exc, long j);
}
