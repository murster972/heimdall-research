package com.google.android.exoplayer2.source;

import android.os.Handler;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class BaseMediaSource implements MediaSource {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<MediaSource.SourceInfoRefreshListener> f3398a = new ArrayList<>(1);
    private final MediaSourceEventListener.EventDispatcher b = new MediaSourceEventListener.EventDispatcher();
    private ExoPlayer c;
    private Timeline d;
    private Object e;

    /* access modifiers changed from: protected */
    public abstract void a(ExoPlayer exoPlayer, boolean z, TransferListener transferListener);

    /* access modifiers changed from: protected */
    public final void a(Timeline timeline, Object obj) {
        this.d = timeline;
        this.e = obj;
        Iterator<MediaSource.SourceInfoRefreshListener> it2 = this.f3398a.iterator();
        while (it2.hasNext()) {
            it2.next().a(this, timeline, obj);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void h();

    /* access modifiers changed from: protected */
    public final MediaSourceEventListener.EventDispatcher a(MediaSource.MediaPeriodId mediaPeriodId) {
        return this.b.a(0, mediaPeriodId, 0);
    }

    /* access modifiers changed from: protected */
    public final MediaSourceEventListener.EventDispatcher a(MediaSource.MediaPeriodId mediaPeriodId, long j) {
        Assertions.a(mediaPeriodId != null);
        return this.b.a(0, mediaPeriodId, j);
    }

    /* access modifiers changed from: protected */
    public final MediaSourceEventListener.EventDispatcher a(int i, MediaSource.MediaPeriodId mediaPeriodId, long j) {
        return this.b.a(i, mediaPeriodId, j);
    }

    public final void a(Handler handler, MediaSourceEventListener mediaSourceEventListener) {
        this.b.a(handler, mediaSourceEventListener);
    }

    public final void a(MediaSourceEventListener mediaSourceEventListener) {
        this.b.a(mediaSourceEventListener);
    }

    public final void a(ExoPlayer exoPlayer, boolean z, MediaSource.SourceInfoRefreshListener sourceInfoRefreshListener, TransferListener transferListener) {
        ExoPlayer exoPlayer2 = this.c;
        Assertions.a(exoPlayer2 == null || exoPlayer2 == exoPlayer);
        this.f3398a.add(sourceInfoRefreshListener);
        if (this.c == null) {
            this.c = exoPlayer;
            a(exoPlayer, z, transferListener);
            return;
        }
        Timeline timeline = this.d;
        if (timeline != null) {
            sourceInfoRefreshListener.a(this, timeline, this.e);
        }
    }

    public final void a(MediaSource.SourceInfoRefreshListener sourceInfoRefreshListener) {
        this.f3398a.remove(sourceInfoRefreshListener);
        if (this.f3398a.isEmpty()) {
            this.c = null;
            this.d = null;
            this.e = null;
            h();
        }
    }
}
