package com.google.android.exoplayer2.source.hls;

import android.os.Handler;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.extractor.DummyTrackOutput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.PrivFrame;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.SampleQueue;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.SequenceableLoader;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.chunk.Chunk;
import com.google.android.exoplayer2.source.hls.HlsChunkSource;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class HlsSampleStreamWrapper implements Loader.Callback<Chunk>, Loader.ReleaseCallback, SequenceableLoader, ExtractorOutput, SampleQueue.UpstreamFormatChangedListener {
    private Format A;
    private Format B;
    private boolean C;
    private TrackGroupArray D;
    private TrackGroupArray E;
    private int[] F;
    private int G;
    private boolean H;
    private boolean[] I = new boolean[0];
    private boolean[] J = new boolean[0];
    private long K;
    private long L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private long Q;
    private int R;

    /* renamed from: a  reason: collision with root package name */
    private final int f3485a;
    private final Callback b;
    private final HlsChunkSource c;
    private final Allocator d;
    private final Format e;
    private final LoadErrorHandlingPolicy f;
    private final Loader g = new Loader("Loader:HlsSampleStreamWrapper");
    private final MediaSourceEventListener.EventDispatcher h;
    private final HlsChunkSource.HlsChunkHolder i = new HlsChunkSource.HlsChunkHolder();
    private final ArrayList<HlsMediaChunk> j = new ArrayList<>();
    private final List<HlsMediaChunk> k = Collections.unmodifiableList(this.j);
    private final Runnable l = new a(this);
    private final Runnable m = new b(this);
    private final Handler n = new Handler();
    private final ArrayList<HlsSampleStream> o = new ArrayList<>();
    private SampleQueue[] p = new SampleQueue[0];
    private int[] q = new int[0];
    private boolean r;
    private int s = -1;
    private boolean t;
    private int u = -1;
    private int v;
    private int w;
    private boolean x;
    private boolean y;
    private int z;

    public interface Callback extends SequenceableLoader.Callback<HlsSampleStreamWrapper> {
        void a();

        void a(HlsMasterPlaylist.HlsUrl hlsUrl);
    }

    private static final class PrivTimestampStrippingSampleQueue extends SampleQueue {
        public PrivTimestampStrippingSampleQueue(Allocator allocator) {
            super(allocator);
        }

        public void a(Format format) {
            super.a(format.a(a(format.e)));
        }

        private Metadata a(Metadata metadata) {
            if (metadata == null) {
                return null;
            }
            int a2 = metadata.a();
            int i = 0;
            int i2 = 0;
            while (true) {
                if (i2 >= a2) {
                    i2 = -1;
                    break;
                }
                Metadata.Entry a3 = metadata.a(i2);
                if ((a3 instanceof PrivFrame) && "com.apple.streaming.transportStreamTimestamp".equals(((PrivFrame) a3).b)) {
                    break;
                }
                i2++;
            }
            if (i2 == -1) {
                return metadata;
            }
            if (a2 == 1) {
                return null;
            }
            Metadata.Entry[] entryArr = new Metadata.Entry[(a2 - 1)];
            while (i < a2) {
                if (i != i2) {
                    entryArr[i < i2 ? i : i - 1] = metadata.a(i);
                }
                i++;
            }
            return new Metadata(entryArr);
        }
    }

    public HlsSampleStreamWrapper(int i2, Callback callback, HlsChunkSource hlsChunkSource, Allocator allocator, long j2, Format format, LoadErrorHandlingPolicy loadErrorHandlingPolicy, MediaSourceEventListener.EventDispatcher eventDispatcher) {
        this.f3485a = i2;
        this.b = callback;
        this.c = hlsChunkSource;
        this.d = allocator;
        this.e = format;
        this.f = loadErrorHandlingPolicy;
        this.h = eventDispatcher;
        this.K = j2;
        this.L = j2;
    }

    private static int d(int i2) {
        if (i2 == 1) {
            return 2;
        }
        if (i2 != 2) {
            return i2 != 3 ? 0 : 1;
        }
        return 3;
    }

    private void k() {
        int length = this.p.length;
        boolean z2 = false;
        int i2 = 0;
        int i3 = 6;
        int i4 = -1;
        while (true) {
            int i5 = 2;
            if (i2 >= length) {
                break;
            }
            String str = this.p[i2].h().g;
            if (!MimeTypes.l(str)) {
                if (MimeTypes.j(str)) {
                    i5 = 1;
                } else {
                    i5 = MimeTypes.k(str) ? 3 : 6;
                }
            }
            if (d(i5) > d(i3)) {
                i4 = i2;
                i3 = i5;
            } else if (i5 == i3 && i4 != -1) {
                i4 = -1;
            }
            i2++;
        }
        TrackGroup a2 = this.c.a();
        int i6 = a2.f3423a;
        this.G = -1;
        this.F = new int[length];
        for (int i7 = 0; i7 < length; i7++) {
            this.F[i7] = i7;
        }
        TrackGroup[] trackGroupArr = new TrackGroup[length];
        for (int i8 = 0; i8 < length; i8++) {
            Format h2 = this.p[i8].h();
            if (i8 == i4) {
                Format[] formatArr = new Format[i6];
                if (i6 == 1) {
                    formatArr[0] = h2.a(a2.a(0));
                } else {
                    for (int i9 = 0; i9 < i6; i9++) {
                        formatArr[i9] = a(a2.a(i9), h2, true);
                    }
                }
                trackGroupArr[i8] = new TrackGroup(formatArr);
                this.G = i8;
            } else {
                trackGroupArr[i8] = new TrackGroup(a((i3 != 2 || !MimeTypes.j(h2.g)) ? null : this.e, h2, false));
            }
        }
        this.D = new TrackGroupArray(trackGroupArr);
        if (this.E == null) {
            z2 = true;
        }
        Assertions.b(z2);
        this.E = TrackGroupArray.d;
    }

    private HlsMediaChunk l() {
        ArrayList<HlsMediaChunk> arrayList = this.j;
        return arrayList.get(arrayList.size() - 1);
    }

    private boolean m() {
        return this.L != -9223372036854775807L;
    }

    private void n() {
        int i2 = this.D.f3424a;
        this.F = new int[i2];
        Arrays.fill(this.F, -1);
        for (int i3 = 0; i3 < i2; i3++) {
            int i4 = 0;
            while (true) {
                SampleQueue[] sampleQueueArr = this.p;
                if (i4 >= sampleQueueArr.length) {
                    break;
                } else if (a(sampleQueueArr[i4].h(), this.D.a(i3).a(0))) {
                    this.F[i3] = i4;
                    break;
                } else {
                    i4++;
                }
            }
        }
        Iterator<HlsSampleStream> it2 = this.o.iterator();
        while (it2.hasNext()) {
            it2.next().b();
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        if (!this.C && this.F == null && this.x) {
            SampleQueue[] sampleQueueArr = this.p;
            int length = sampleQueueArr.length;
            int i2 = 0;
            while (i2 < length) {
                if (sampleQueueArr[i2].h() != null) {
                    i2++;
                } else {
                    return;
                }
            }
            if (this.D != null) {
                n();
                return;
            }
            k();
            this.y = true;
            this.b.a();
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        this.x = true;
        o();
    }

    private void q() {
        for (SampleQueue a2 : this.p) {
            a2.a(this.M);
        }
        this.M = false;
    }

    public void a(SeekMap seekMap) {
    }

    public boolean b(long j2, boolean z2) {
        this.K = j2;
        if (m()) {
            this.L = j2;
            return true;
        } else if (this.x && !z2 && e(j2)) {
            return false;
        } else {
            this.L = j2;
            this.O = false;
            this.j.clear();
            if (this.g.c()) {
                this.g.b();
            } else {
                q();
            }
            return true;
        }
    }

    public void c(int i2) {
        int i3 = this.F[i2];
        Assertions.b(this.I[i3]);
        this.I[i3] = false;
    }

    public void c(long j2) {
    }

    public void d() throws IOException {
        i();
    }

    public TrackGroupArray e() {
        return this.D;
    }

    public long f() {
        if (this.O) {
            return Long.MIN_VALUE;
        }
        if (m()) {
            return this.L;
        }
        long j2 = this.K;
        HlsMediaChunk l2 = l();
        if (!l2.f()) {
            if (this.j.size() > 1) {
                ArrayList<HlsMediaChunk> arrayList = this.j;
                l2 = arrayList.get(arrayList.size() - 2);
            } else {
                l2 = null;
            }
        }
        if (l2 != null) {
            j2 = Math.max(j2, l2.g);
        }
        if (this.x) {
            for (SampleQueue f2 : this.p) {
                j2 = Math.max(j2, f2.f());
            }
        }
        return j2;
    }

    public void g() {
        q();
    }

    public void h() {
        if (!this.y) {
            b(this.K);
        }
    }

    public void i() throws IOException {
        this.g.a();
        this.c.c();
    }

    public void j() {
        if (this.y) {
            for (SampleQueue b2 : this.p) {
                b2.b();
            }
        }
        this.g.a((Loader.ReleaseCallback) this);
        this.n.removeCallbacksAndMessages((Object) null);
        this.C = true;
        this.o.clear();
    }

    private boolean e(long j2) {
        int length = this.p.length;
        int i2 = 0;
        while (true) {
            boolean z2 = true;
            if (i2 >= length) {
                return true;
            }
            SampleQueue sampleQueue = this.p[i2];
            sampleQueue.m();
            if (sampleQueue.a(j2, true, false) == -1) {
                z2 = false;
            }
            if (z2 || (!this.J[i2] && this.H)) {
                i2++;
            }
        }
        return false;
    }

    public void d(long j2) {
        this.Q = j2;
        for (SampleQueue a2 : this.p) {
            a2.a(j2);
        }
    }

    public void a(TrackGroupArray trackGroupArray, int i2, TrackGroupArray trackGroupArray2) {
        this.y = true;
        this.D = trackGroupArray;
        this.E = trackGroupArray2;
        this.G = i2;
        this.b.a();
    }

    public int a(int i2) {
        int i3 = this.F[i2];
        if (i3 != -1) {
            boolean[] zArr = this.I;
            if (zArr[i3]) {
                return -2;
            }
            zArr[i3] = true;
            return i3;
        } else if (this.E.a(this.D.a(i2)) == -1) {
            return -2;
        } else {
            return -3;
        }
    }

    public boolean b(int i2) {
        return this.O || (!m() && this.p[i2].j());
    }

    public long b() {
        if (m()) {
            return this.L;
        }
        if (this.O) {
            return Long.MIN_VALUE;
        }
        return l().g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:67:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x013c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.google.android.exoplayer2.trackselection.TrackSelection[] r20, boolean[] r21, com.google.android.exoplayer2.source.SampleStream[] r22, boolean[] r23, long r24, boolean r26) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r22
            r12 = r24
            boolean r3 = r0.y
            com.google.android.exoplayer2.util.Assertions.b(r3)
            int r3 = r0.z
            r14 = 0
            r4 = 0
        L_0x0011:
            int r5 = r1.length
            r6 = 0
            r15 = 1
            if (r4 >= r5) goto L_0x0033
            r5 = r2[r4]
            if (r5 == 0) goto L_0x0030
            r5 = r1[r4]
            if (r5 == 0) goto L_0x0022
            boolean r5 = r21[r4]
            if (r5 != 0) goto L_0x0030
        L_0x0022:
            int r5 = r0.z
            int r5 = r5 - r15
            r0.z = r5
            r5 = r2[r4]
            com.google.android.exoplayer2.source.hls.HlsSampleStream r5 = (com.google.android.exoplayer2.source.hls.HlsSampleStream) r5
            r5.c()
            r2[r4] = r6
        L_0x0030:
            int r4 = r4 + 1
            goto L_0x0011
        L_0x0033:
            if (r26 != 0) goto L_0x0045
            boolean r4 = r0.N
            if (r4 == 0) goto L_0x003c
            if (r3 != 0) goto L_0x0043
            goto L_0x0045
        L_0x003c:
            long r3 = r0.K
            int r5 = (r12 > r3 ? 1 : (r12 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0043
            goto L_0x0045
        L_0x0043:
            r3 = 0
            goto L_0x0046
        L_0x0045:
            r3 = 1
        L_0x0046:
            com.google.android.exoplayer2.source.hls.HlsChunkSource r4 = r0.c
            com.google.android.exoplayer2.trackselection.TrackSelection r4 = r4.b()
            r16 = r3
            r11 = r4
            r3 = 0
        L_0x0050:
            int r5 = r1.length
            if (r3 >= r5) goto L_0x00b0
            r5 = r2[r3]
            if (r5 != 0) goto L_0x00ad
            r5 = r1[r3]
            if (r5 == 0) goto L_0x00ad
            int r5 = r0.z
            int r5 = r5 + r15
            r0.z = r5
            r5 = r1[r3]
            com.google.android.exoplayer2.source.TrackGroupArray r7 = r0.D
            com.google.android.exoplayer2.source.TrackGroup r8 = r5.c()
            int r7 = r7.a((com.google.android.exoplayer2.source.TrackGroup) r8)
            int r8 = r0.G
            if (r7 != r8) goto L_0x0076
            com.google.android.exoplayer2.source.hls.HlsChunkSource r8 = r0.c
            r8.a((com.google.android.exoplayer2.trackselection.TrackSelection) r5)
            r11 = r5
        L_0x0076:
            com.google.android.exoplayer2.source.hls.HlsSampleStream r5 = new com.google.android.exoplayer2.source.hls.HlsSampleStream
            r5.<init>(r0, r7)
            r2[r3] = r5
            r23[r3] = r15
            int[] r5 = r0.F
            if (r5 == 0) goto L_0x008a
            r5 = r2[r3]
            com.google.android.exoplayer2.source.hls.HlsSampleStream r5 = (com.google.android.exoplayer2.source.hls.HlsSampleStream) r5
            r5.b()
        L_0x008a:
            boolean r5 = r0.x
            if (r5 == 0) goto L_0x00ad
            if (r16 != 0) goto L_0x00ad
            com.google.android.exoplayer2.source.SampleQueue[] r5 = r0.p
            int[] r8 = r0.F
            r7 = r8[r7]
            r5 = r5[r7]
            r5.m()
            int r7 = r5.a((long) r12, (boolean) r15, (boolean) r15)
            r8 = -1
            if (r7 != r8) goto L_0x00ab
            int r5 = r5.g()
            if (r5 == 0) goto L_0x00ab
            r16 = 1
            goto L_0x00ad
        L_0x00ab:
            r16 = 0
        L_0x00ad:
            int r3 = r3 + 1
            goto L_0x0050
        L_0x00b0:
            int r1 = r0.z
            if (r1 != 0) goto L_0x00e5
            com.google.android.exoplayer2.source.hls.HlsChunkSource r1 = r0.c
            r1.d()
            r0.B = r6
            java.util.ArrayList<com.google.android.exoplayer2.source.hls.HlsMediaChunk> r1 = r0.j
            r1.clear()
            com.google.android.exoplayer2.upstream.Loader r1 = r0.g
            boolean r1 = r1.c()
            if (r1 == 0) goto L_0x00e0
            boolean r1 = r0.x
            if (r1 == 0) goto L_0x00d9
            com.google.android.exoplayer2.source.SampleQueue[] r1 = r0.p
            int r3 = r1.length
        L_0x00cf:
            if (r14 >= r3) goto L_0x00d9
            r4 = r1[r14]
            r4.b()
            int r14 = r14 + 1
            goto L_0x00cf
        L_0x00d9:
            com.google.android.exoplayer2.upstream.Loader r1 = r0.g
            r1.b()
            goto L_0x014b
        L_0x00e0:
            r19.q()
            goto L_0x014b
        L_0x00e5:
            java.util.ArrayList<com.google.android.exoplayer2.source.hls.HlsMediaChunk> r1 = r0.j
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0138
            boolean r1 = com.google.android.exoplayer2.util.Util.a((java.lang.Object) r11, (java.lang.Object) r4)
            if (r1 != 0) goto L_0x0138
            boolean r1 = r0.N
            if (r1 != 0) goto L_0x012f
            r3 = 0
            int r1 = (r12 > r3 ? 1 : (r12 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x00fe
            long r3 = -r12
        L_0x00fe:
            r6 = r3
            com.google.android.exoplayer2.source.hls.HlsMediaChunk r1 = r19.l()
            com.google.android.exoplayer2.source.hls.HlsChunkSource r3 = r0.c
            com.google.android.exoplayer2.source.chunk.MediaChunkIterator[] r17 = r3.a((com.google.android.exoplayer2.source.hls.HlsMediaChunk) r1, (long) r12)
            r8 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            java.util.List<com.google.android.exoplayer2.source.hls.HlsMediaChunk> r10 = r0.k
            r3 = r11
            r4 = r24
            r18 = r11
            r11 = r17
            r3.a(r4, r6, r8, r10, r11)
            com.google.android.exoplayer2.source.hls.HlsChunkSource r3 = r0.c
            com.google.android.exoplayer2.source.TrackGroup r3 = r3.a()
            com.google.android.exoplayer2.Format r1 = r1.c
            int r1 = r3.a((com.google.android.exoplayer2.Format) r1)
            int r3 = r18.d()
            if (r3 == r1) goto L_0x012d
            goto L_0x012f
        L_0x012d:
            r1 = 0
            goto L_0x0130
        L_0x012f:
            r1 = 1
        L_0x0130:
            if (r1 == 0) goto L_0x0138
            r0.M = r15
            r1 = 1
            r16 = 1
            goto L_0x013a
        L_0x0138:
            r1 = r26
        L_0x013a:
            if (r16 == 0) goto L_0x014b
            r0.b((long) r12, (boolean) r1)
        L_0x013f:
            int r1 = r2.length
            if (r14 >= r1) goto L_0x014b
            r1 = r2[r14]
            if (r1 == 0) goto L_0x0148
            r23[r14] = r15
        L_0x0148:
            int r14 = r14 + 1
            goto L_0x013f
        L_0x014b:
            r0.a((com.google.android.exoplayer2.source.SampleStream[]) r2)
            r0.N = r15
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper.a(com.google.android.exoplayer2.trackselection.TrackSelection[], boolean[], com.google.android.exoplayer2.source.SampleStream[], boolean[], long, boolean):boolean");
    }

    public boolean b(long j2) {
        List<HlsMediaChunk> list;
        long max;
        if (this.O || this.g.c()) {
            return false;
        }
        if (m()) {
            list = Collections.emptyList();
            max = this.L;
        } else {
            list = this.k;
            HlsMediaChunk l2 = l();
            if (l2.f()) {
                max = l2.g;
            } else {
                max = Math.max(this.K, l2.f);
            }
        }
        this.c.a(j2, max, list, this.i);
        HlsChunkSource.HlsChunkHolder hlsChunkHolder = this.i;
        boolean z2 = hlsChunkHolder.b;
        Chunk chunk = hlsChunkHolder.f3480a;
        HlsMasterPlaylist.HlsUrl hlsUrl = hlsChunkHolder.c;
        hlsChunkHolder.a();
        if (z2) {
            this.L = -9223372036854775807L;
            this.O = true;
            return true;
        } else if (chunk == null) {
            if (hlsUrl != null) {
                this.b.a(hlsUrl);
            }
            return false;
        } else {
            if (a(chunk)) {
                this.L = -9223372036854775807L;
                HlsMediaChunk hlsMediaChunk = (HlsMediaChunk) chunk;
                hlsMediaChunk.a(this);
                this.j.add(hlsMediaChunk);
                this.A = hlsMediaChunk.c;
            }
            this.h.a(chunk.f3431a, chunk.b, this.f3485a, chunk.c, chunk.d, chunk.e, chunk.f, chunk.g, this.g.a(chunk, this, this.f.a(chunk.b)));
            return true;
        }
    }

    private static DummyTrackOutput b(int i2, int i3) {
        Log.d("HlsSampleStreamWrapper", "Unmapped track with id " + i2 + " of type " + i3);
        return new DummyTrackOutput();
    }

    public void a(long j2, boolean z2) {
        if (this.x && !m()) {
            int length = this.p.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.p[i2].b(j2, z2, this.I[i2]);
            }
        }
    }

    public void a(boolean z2) {
        this.c.a(z2);
    }

    public boolean a(HlsMasterPlaylist.HlsUrl hlsUrl, long j2) {
        return this.c.a(hlsUrl, j2);
    }

    public int a(int i2, FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z2) {
        if (m()) {
            return -3;
        }
        int i3 = 0;
        if (!this.j.isEmpty()) {
            int i4 = 0;
            while (i4 < this.j.size() - 1 && a(this.j.get(i4))) {
                i4++;
            }
            Util.a(this.j, 0, i4);
            HlsMediaChunk hlsMediaChunk = this.j.get(0);
            Format format = hlsMediaChunk.c;
            if (!format.equals(this.B)) {
                this.h.a(this.f3485a, format, hlsMediaChunk.d, hlsMediaChunk.e, hlsMediaChunk.f);
            }
            this.B = format;
        }
        int a2 = this.p[i2].a(formatHolder, decoderInputBuffer, z2, this.O, this.K);
        if (a2 == -5 && i2 == this.w) {
            int k2 = this.p[i2].k();
            while (i3 < this.j.size() && this.j.get(i3).j != k2) {
                i3++;
            }
            formatHolder.f3165a = formatHolder.f3165a.a(i3 < this.j.size() ? this.j.get(i3).c : this.A);
        }
        return a2;
    }

    public int a(int i2, long j2) {
        if (m()) {
            return 0;
        }
        SampleQueue sampleQueue = this.p[i2];
        if (this.O && j2 > sampleQueue.f()) {
            return sampleQueue.a();
        }
        int a2 = sampleQueue.a(j2, true, true);
        if (a2 == -1) {
            return 0;
        }
        return a2;
    }

    public void a(Chunk chunk, long j2, long j3) {
        Chunk chunk2 = chunk;
        this.c.a(chunk2);
        this.h.b(chunk2.f3431a, chunk.d(), chunk.c(), chunk2.b, this.f3485a, chunk2.c, chunk2.d, chunk2.e, chunk2.f, chunk2.g, j2, j3, chunk.a());
        if (!this.y) {
            b(this.K);
        } else {
            this.b.a(this);
        }
    }

    public void a(Chunk chunk, long j2, long j3, boolean z2) {
        Chunk chunk2 = chunk;
        this.h.a(chunk2.f3431a, chunk.d(), chunk.c(), chunk2.b, this.f3485a, chunk2.c, chunk2.d, chunk2.e, chunk2.f, chunk2.g, j2, j3, chunk.a());
        if (!z2) {
            q();
            if (this.z > 0) {
                this.b.a(this);
            }
        }
    }

    public Loader.LoadErrorAction a(Chunk chunk, long j2, long j3, IOException iOException, int i2) {
        Loader.LoadErrorAction a2;
        Chunk chunk2 = chunk;
        long a3 = chunk.a();
        boolean a4 = a(chunk);
        long a5 = this.f.a(chunk2.b, j3, iOException, i2);
        boolean z2 = false;
        boolean a6 = a5 != -9223372036854775807L ? this.c.a(chunk2, a5) : false;
        if (a6) {
            if (a4 && a3 == 0) {
                ArrayList<HlsMediaChunk> arrayList = this.j;
                if (arrayList.remove(arrayList.size() - 1) == chunk2) {
                    z2 = true;
                }
                Assertions.b(z2);
                if (this.j.isEmpty()) {
                    this.L = this.K;
                }
            }
            a2 = Loader.e;
        } else {
            long b2 = this.f.b(chunk2.b, j3, iOException, i2);
            a2 = b2 != -9223372036854775807L ? Loader.a(false, b2) : Loader.f;
        }
        Loader.LoadErrorAction loadErrorAction = a2;
        this.h.a(chunk2.f3431a, chunk.d(), chunk.c(), chunk2.b, this.f3485a, chunk2.c, chunk2.d, chunk2.e, chunk2.f, chunk2.g, j2, j3, a3, iOException, !loadErrorAction.a());
        if (a6) {
            if (!this.y) {
                b(this.K);
            } else {
                this.b.a(this);
            }
        }
        return loadErrorAction;
    }

    public void a(int i2, boolean z2, boolean z3) {
        if (!z3) {
            this.r = false;
            this.t = false;
        }
        this.R = i2;
        for (SampleQueue c2 : this.p) {
            c2.c(i2);
        }
        if (z2) {
            for (SampleQueue n2 : this.p) {
                n2.n();
            }
        }
    }

    public TrackOutput a(int i2, int i3) {
        SampleQueue[] sampleQueueArr = this.p;
        int length = sampleQueueArr.length;
        boolean z2 = false;
        if (i3 == 1) {
            int i4 = this.s;
            if (i4 != -1) {
                if (!this.r) {
                    this.r = true;
                    this.q[i4] = i2;
                    return sampleQueueArr[i4];
                } else if (this.q[i4] == i2) {
                    return sampleQueueArr[i4];
                } else {
                    return b(i2, i3);
                }
            } else if (this.P) {
                return b(i2, i3);
            }
        } else if (i3 == 2) {
            int i5 = this.u;
            if (i5 != -1) {
                if (!this.t) {
                    this.t = true;
                    this.q[i5] = i2;
                    return sampleQueueArr[i5];
                } else if (this.q[i5] == i2) {
                    return sampleQueueArr[i5];
                } else {
                    return b(i2, i3);
                }
            } else if (this.P) {
                return b(i2, i3);
            }
        } else {
            for (int i6 = 0; i6 < length; i6++) {
                if (this.q[i6] == i2) {
                    return this.p[i6];
                }
            }
            if (this.P) {
                return b(i2, i3);
            }
        }
        PrivTimestampStrippingSampleQueue privTimestampStrippingSampleQueue = new PrivTimestampStrippingSampleQueue(this.d);
        privTimestampStrippingSampleQueue.a(this.Q);
        privTimestampStrippingSampleQueue.c(this.R);
        privTimestampStrippingSampleQueue.a((SampleQueue.UpstreamFormatChangedListener) this);
        int i7 = length + 1;
        this.q = Arrays.copyOf(this.q, i7);
        this.q[length] = i2;
        this.p = (SampleQueue[]) Arrays.copyOf(this.p, i7);
        this.p[length] = privTimestampStrippingSampleQueue;
        this.J = Arrays.copyOf(this.J, i7);
        boolean[] zArr = this.J;
        if (i3 == 1 || i3 == 2) {
            z2 = true;
        }
        zArr[length] = z2;
        this.H |= this.J[length];
        if (i3 == 1) {
            this.r = true;
            this.s = length;
        } else if (i3 == 2) {
            this.t = true;
            this.u = length;
        }
        if (d(i3) > d(this.v)) {
            this.w = length;
            this.v = i3;
        }
        this.I = Arrays.copyOf(this.I, i7);
        return privTimestampStrippingSampleQueue;
    }

    public void a() {
        this.P = true;
        this.n.post(this.m);
    }

    public void a(Format format) {
        this.n.post(this.l);
    }

    private void a(SampleStream[] sampleStreamArr) {
        this.o.clear();
        for (HlsSampleStream hlsSampleStream : sampleStreamArr) {
            if (hlsSampleStream != null) {
                this.o.add(hlsSampleStream);
            }
        }
    }

    private boolean a(HlsMediaChunk hlsMediaChunk) {
        int i2 = hlsMediaChunk.j;
        int length = this.p.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (this.I[i3] && this.p[i3].k() == i2) {
                return false;
            }
        }
        return true;
    }

    private static Format a(Format format, Format format2, boolean z2) {
        if (format == null) {
            return format2;
        }
        int i2 = z2 ? format.c : -1;
        String a2 = Util.a(format.d, MimeTypes.f(format2.g));
        String d2 = MimeTypes.d(a2);
        if (d2 == null) {
            d2 = format2.g;
        }
        return format2.a(format.f3164a, format.b, d2, a2, i2, format.l, format.m, format.y, format.z);
    }

    private static boolean a(Chunk chunk) {
        return chunk instanceof HlsMediaChunk;
    }

    private static boolean a(Format format, Format format2) {
        String str = format.g;
        String str2 = format2.g;
        int f2 = MimeTypes.f(str);
        if (f2 != 3) {
            if (f2 == MimeTypes.f(str2)) {
                return true;
            }
            return false;
        } else if (!Util.a((Object) str, (Object) str2)) {
            return false;
        } else {
            if (("application/cea-608".equals(str) || "application/cea-708".equals(str)) && format.A != format2.A) {
                return false;
            }
            return true;
        }
    }
}
