package com.google.android.exoplayer2.source.hls;

import android.util.SparseArray;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.util.TimestampAdjuster;

public final class TimestampAdjusterProvider {

    /* renamed from: a  reason: collision with root package name */
    private final SparseArray<TimestampAdjuster> f3486a = new SparseArray<>();

    public TimestampAdjuster a(int i) {
        TimestampAdjuster timestampAdjuster = this.f3486a.get(i);
        if (timestampAdjuster != null) {
            return timestampAdjuster;
        }
        TimestampAdjuster timestampAdjuster2 = new TimestampAdjuster(Clock.MAX_TIME);
        this.f3486a.put(i, timestampAdjuster2);
        return timestampAdjuster2;
    }

    public void a() {
        this.f3486a.clear();
    }
}
