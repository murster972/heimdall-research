package com.google.android.exoplayer2.source.chunk;

public final class ChunkHolder {

    /* renamed from: a  reason: collision with root package name */
    public Chunk f3434a;
    public boolean b;

    public void a() {
        this.f3434a = null;
        this.b = false;
    }
}
