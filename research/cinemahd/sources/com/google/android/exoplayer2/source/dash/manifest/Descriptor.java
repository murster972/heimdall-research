package com.google.android.exoplayer2.source.dash.manifest;

import com.google.android.exoplayer2.util.Util;

public final class Descriptor {

    /* renamed from: a  reason: collision with root package name */
    public final String f3462a;
    public final String b;
    public final String c;

    public Descriptor(String str, String str2, String str3) {
        this.f3462a = str;
        this.b = str2;
        this.c = str3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Descriptor.class != obj.getClass()) {
            return false;
        }
        Descriptor descriptor = (Descriptor) obj;
        if (!Util.a((Object) this.f3462a, (Object) descriptor.f3462a) || !Util.a((Object) this.b, (Object) descriptor.b) || !Util.a((Object) this.c, (Object) descriptor.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.f3462a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }
}
