package com.google.android.exoplayer2.source.hls.playlist;

import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.hls.HlsDataSourceFactory;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.UriUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;

public final class DefaultHlsPlaylistTracker implements HlsPlaylistTracker, Loader.Callback<ParsingLoadable<HlsPlaylist>> {
    public static final HlsPlaylistTracker.Factory p = a.f3498a;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final HlsDataSourceFactory f3491a;
    private final HlsPlaylistParserFactory b;
    /* access modifiers changed from: private */
    public final LoadErrorHandlingPolicy c;
    private final IdentityHashMap<HlsMasterPlaylist.HlsUrl, MediaPlaylistBundle> d = new IdentityHashMap<>();
    private final List<HlsPlaylistTracker.PlaylistEventListener> e = new ArrayList();
    /* access modifiers changed from: private */
    public ParsingLoadable.Parser<HlsPlaylist> f;
    /* access modifiers changed from: private */
    public MediaSourceEventListener.EventDispatcher g;
    private Loader h;
    /* access modifiers changed from: private */
    public Handler i;
    private HlsPlaylistTracker.PrimaryPlaylistListener j;
    /* access modifiers changed from: private */
    public HlsMasterPlaylist k;
    /* access modifiers changed from: private */
    public HlsMasterPlaylist.HlsUrl l;
    private HlsMediaPlaylist m;
    private boolean n;
    private long o = -9223372036854775807L;

    private final class MediaPlaylistBundle implements Loader.Callback<ParsingLoadable<HlsPlaylist>>, Runnable {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final HlsMasterPlaylist.HlsUrl f3492a;
        private final Loader b = new Loader("DefaultHlsPlaylistTracker:MediaPlaylist");
        private final ParsingLoadable<HlsPlaylist> c;
        private HlsMediaPlaylist d;
        private long e;
        private long f;
        private long g;
        /* access modifiers changed from: private */
        public long h;
        private boolean i;
        private IOException j;

        public MediaPlaylistBundle(HlsMasterPlaylist.HlsUrl hlsUrl) {
            this.f3492a = hlsUrl;
            this.c = new ParsingLoadable<>(DefaultHlsPlaylistTracker.this.f3491a.a(4), UriUtil.b(DefaultHlsPlaylistTracker.this.k.f3495a, hlsUrl.f3493a), 4, DefaultHlsPlaylistTracker.this.f);
        }

        private void f() {
            long a2 = this.b.a(this.c, this, DefaultHlsPlaylistTracker.this.c.a(this.c.b));
            MediaSourceEventListener.EventDispatcher g2 = DefaultHlsPlaylistTracker.this.g;
            ParsingLoadable<HlsPlaylist> parsingLoadable = this.c;
            g2.a(parsingLoadable.f3601a, parsingLoadable.b, a2);
        }

        public void c() {
            this.h = 0;
            if (!this.i && !this.b.c()) {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (elapsedRealtime < this.g) {
                    this.i = true;
                    DefaultHlsPlaylistTracker.this.i.postDelayed(this, this.g - elapsedRealtime);
                    return;
                }
                f();
            }
        }

        public void d() throws IOException {
            this.b.a();
            IOException iOException = this.j;
            if (iOException != null) {
                throw iOException;
            }
        }

        public void e() {
            this.b.d();
        }

        public void run() {
            this.i = false;
            f();
        }

        public boolean b() {
            int i2;
            if (this.d == null) {
                return false;
            }
            long elapsedRealtime = SystemClock.elapsedRealtime();
            long max = Math.max(30000, C.b(this.d.p));
            HlsMediaPlaylist hlsMediaPlaylist = this.d;
            if (hlsMediaPlaylist.l || (i2 = hlsMediaPlaylist.d) == 2 || i2 == 1 || this.e + max > elapsedRealtime) {
                return true;
            }
            return false;
        }

        public HlsMediaPlaylist a() {
            return this.d;
        }

        public void a(ParsingLoadable<HlsPlaylist> parsingLoadable, long j2, long j3) {
            HlsPlaylist c2 = parsingLoadable.c();
            if (c2 instanceof HlsMediaPlaylist) {
                long j4 = j3;
                a((HlsMediaPlaylist) c2, j4);
                DefaultHlsPlaylistTracker.this.g.b(parsingLoadable.f3601a, parsingLoadable.d(), parsingLoadable.b(), 4, j2, j4, parsingLoadable.a());
                return;
            }
            this.j = new ParserException("Loaded playlist has unexpected type.");
        }

        public void a(ParsingLoadable<HlsPlaylist> parsingLoadable, long j2, long j3, boolean z) {
            DefaultHlsPlaylistTracker.this.g.a(parsingLoadable.f3601a, parsingLoadable.d(), parsingLoadable.b(), 4, j2, j3, parsingLoadable.a());
        }

        public Loader.LoadErrorAction a(ParsingLoadable<HlsPlaylist> parsingLoadable, long j2, long j3, IOException iOException, int i2) {
            Loader.LoadErrorAction loadErrorAction;
            ParsingLoadable<HlsPlaylist> parsingLoadable2 = parsingLoadable;
            long a2 = DefaultHlsPlaylistTracker.this.c.a(parsingLoadable2.b, j3, iOException, i2);
            boolean z = a2 != -9223372036854775807L;
            boolean z2 = DefaultHlsPlaylistTracker.this.a(this.f3492a, a2) || !z;
            if (z) {
                z2 |= a(a2);
            }
            if (z2) {
                long b2 = DefaultHlsPlaylistTracker.this.c.b(parsingLoadable2.b, j3, iOException, i2);
                loadErrorAction = b2 != -9223372036854775807L ? Loader.a(false, b2) : Loader.f;
            } else {
                loadErrorAction = Loader.e;
            }
            DefaultHlsPlaylistTracker.this.g.a(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), 4, j2, j3, parsingLoadable.a(), iOException, !loadErrorAction.a());
            return loadErrorAction;
        }

        /* access modifiers changed from: private */
        public void a(HlsMediaPlaylist hlsMediaPlaylist, long j2) {
            HlsMediaPlaylist hlsMediaPlaylist2 = hlsMediaPlaylist;
            HlsMediaPlaylist hlsMediaPlaylist3 = this.d;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.e = elapsedRealtime;
            this.d = DefaultHlsPlaylistTracker.this.b(hlsMediaPlaylist3, hlsMediaPlaylist2);
            HlsMediaPlaylist hlsMediaPlaylist4 = this.d;
            if (hlsMediaPlaylist4 != hlsMediaPlaylist3) {
                this.j = null;
                this.f = elapsedRealtime;
                DefaultHlsPlaylistTracker.this.a(this.f3492a, hlsMediaPlaylist4);
            } else if (!hlsMediaPlaylist4.l) {
                HlsMediaPlaylist hlsMediaPlaylist5 = this.d;
                if (hlsMediaPlaylist2.i + ((long) hlsMediaPlaylist2.o.size()) < hlsMediaPlaylist5.i) {
                    this.j = new HlsPlaylistTracker.PlaylistResetException(this.f3492a.f3493a);
                    boolean unused = DefaultHlsPlaylistTracker.this.a(this.f3492a, -9223372036854775807L);
                } else if (((double) (elapsedRealtime - this.f)) > ((double) C.b(hlsMediaPlaylist5.k)) * 3.5d) {
                    this.j = new HlsPlaylistTracker.PlaylistStuckException(this.f3492a.f3493a);
                    long a2 = DefaultHlsPlaylistTracker.this.c.a(4, j2, this.j, 1);
                    boolean unused2 = DefaultHlsPlaylistTracker.this.a(this.f3492a, a2);
                    if (a2 != -9223372036854775807L) {
                        a(a2);
                    }
                }
            }
            HlsMediaPlaylist hlsMediaPlaylist6 = this.d;
            this.g = elapsedRealtime + C.b(hlsMediaPlaylist6 != hlsMediaPlaylist3 ? hlsMediaPlaylist6.k : hlsMediaPlaylist6.k / 2);
            if (this.f3492a == DefaultHlsPlaylistTracker.this.l && !this.d.l) {
                c();
            }
        }

        private boolean a(long j2) {
            this.h = SystemClock.elapsedRealtime() + j2;
            return DefaultHlsPlaylistTracker.this.l == this.f3492a && !DefaultHlsPlaylistTracker.this.d();
        }
    }

    public DefaultHlsPlaylistTracker(HlsDataSourceFactory hlsDataSourceFactory, LoadErrorHandlingPolicy loadErrorHandlingPolicy, HlsPlaylistParserFactory hlsPlaylistParserFactory) {
        this.f3491a = hlsDataSourceFactory;
        this.b = hlsPlaylistParserFactory;
        this.c = loadErrorHandlingPolicy;
    }

    public boolean isLive() {
        return this.n;
    }

    public void stop() {
        this.l = null;
        this.m = null;
        this.k = null;
        this.o = -9223372036854775807L;
        this.h.d();
        this.h = null;
        for (MediaPlaylistBundle e2 : this.d.values()) {
            e2.e();
        }
        this.i.removeCallbacksAndMessages((Object) null);
        this.i = null;
        this.d.clear();
    }

    /* access modifiers changed from: private */
    public boolean d() {
        List<HlsMasterPlaylist.HlsUrl> list = this.k.d;
        int size = list.size();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        for (int i2 = 0; i2 < size; i2++) {
            MediaPlaylistBundle mediaPlaylistBundle = this.d.get(list.get(i2));
            if (elapsedRealtime > mediaPlaylistBundle.h) {
                this.l = mediaPlaylistBundle.f3492a;
                mediaPlaylistBundle.c();
                return true;
            }
        }
        return false;
    }

    public void b(HlsPlaylistTracker.PlaylistEventListener playlistEventListener) {
        this.e.add(playlistEventListener);
    }

    public void c() throws IOException {
        Loader loader = this.h;
        if (loader != null) {
            loader.a();
        }
        HlsMasterPlaylist.HlsUrl hlsUrl = this.l;
        if (hlsUrl != null) {
            c(hlsUrl);
        }
    }

    public HlsMasterPlaylist b() {
        return this.k;
    }

    public boolean b(HlsMasterPlaylist.HlsUrl hlsUrl) {
        return this.d.get(hlsUrl).b();
    }

    /* access modifiers changed from: private */
    public HlsMediaPlaylist b(HlsMediaPlaylist hlsMediaPlaylist, HlsMediaPlaylist hlsMediaPlaylist2) {
        if (!hlsMediaPlaylist2.a(hlsMediaPlaylist)) {
            return hlsMediaPlaylist2.l ? hlsMediaPlaylist.a() : hlsMediaPlaylist;
        }
        return hlsMediaPlaylist2.a(d(hlsMediaPlaylist, hlsMediaPlaylist2), c(hlsMediaPlaylist, hlsMediaPlaylist2));
    }

    public void c(HlsMasterPlaylist.HlsUrl hlsUrl) throws IOException {
        this.d.get(hlsUrl).d();
    }

    private int c(HlsMediaPlaylist hlsMediaPlaylist, HlsMediaPlaylist hlsMediaPlaylist2) {
        HlsMediaPlaylist.Segment a2;
        if (hlsMediaPlaylist2.g) {
            return hlsMediaPlaylist2.h;
        }
        HlsMediaPlaylist hlsMediaPlaylist3 = this.m;
        int i2 = hlsMediaPlaylist3 != null ? hlsMediaPlaylist3.h : 0;
        return (hlsMediaPlaylist == null || (a2 = a(hlsMediaPlaylist, hlsMediaPlaylist2)) == null) ? i2 : (hlsMediaPlaylist.h + a2.d) - hlsMediaPlaylist2.o.get(0).d;
    }

    public void a(Uri uri, MediaSourceEventListener.EventDispatcher eventDispatcher, HlsPlaylistTracker.PrimaryPlaylistListener primaryPlaylistListener) {
        this.i = new Handler();
        this.g = eventDispatcher;
        this.j = primaryPlaylistListener;
        ParsingLoadable parsingLoadable = new ParsingLoadable(this.f3491a.a(4), uri, 4, this.b.a());
        Assertions.b(this.h == null);
        this.h = new Loader("DefaultHlsPlaylistTracker:MasterPlaylist");
        eventDispatcher.a(parsingLoadable.f3601a, parsingLoadable.b, this.h.a(parsingLoadable, this, this.c.a(parsingLoadable.b)));
    }

    private void d(HlsMasterPlaylist.HlsUrl hlsUrl) {
        if (hlsUrl != this.l && this.k.d.contains(hlsUrl)) {
            HlsMediaPlaylist hlsMediaPlaylist = this.m;
            if (hlsMediaPlaylist == null || !hlsMediaPlaylist.l) {
                this.l = hlsUrl;
                this.d.get(this.l).c();
            }
        }
    }

    private long d(HlsMediaPlaylist hlsMediaPlaylist, HlsMediaPlaylist hlsMediaPlaylist2) {
        if (hlsMediaPlaylist2.m) {
            return hlsMediaPlaylist2.f;
        }
        HlsMediaPlaylist hlsMediaPlaylist3 = this.m;
        long j2 = hlsMediaPlaylist3 != null ? hlsMediaPlaylist3.f : 0;
        if (hlsMediaPlaylist == null) {
            return j2;
        }
        int size = hlsMediaPlaylist.o.size();
        HlsMediaPlaylist.Segment a2 = a(hlsMediaPlaylist, hlsMediaPlaylist2);
        if (a2 != null) {
            return hlsMediaPlaylist.f + a2.e;
        }
        return ((long) size) == hlsMediaPlaylist2.i - hlsMediaPlaylist.i ? hlsMediaPlaylist.b() : j2;
    }

    public void a(HlsPlaylistTracker.PlaylistEventListener playlistEventListener) {
        this.e.remove(playlistEventListener);
    }

    public HlsMediaPlaylist a(HlsMasterPlaylist.HlsUrl hlsUrl, boolean z) {
        HlsMediaPlaylist a2 = this.d.get(hlsUrl).a();
        if (a2 != null && z) {
            d(hlsUrl);
        }
        return a2;
    }

    public long a() {
        return this.o;
    }

    public void a(HlsMasterPlaylist.HlsUrl hlsUrl) {
        this.d.get(hlsUrl).c();
    }

    public void a(ParsingLoadable<HlsPlaylist> parsingLoadable, long j2, long j3) {
        HlsMasterPlaylist hlsMasterPlaylist;
        HlsPlaylist c2 = parsingLoadable.c();
        boolean z = c2 instanceof HlsMediaPlaylist;
        if (z) {
            hlsMasterPlaylist = HlsMasterPlaylist.a(c2.f3495a);
        } else {
            hlsMasterPlaylist = (HlsMasterPlaylist) c2;
        }
        this.k = hlsMasterPlaylist;
        this.f = this.b.a(hlsMasterPlaylist);
        this.l = hlsMasterPlaylist.d.get(0);
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(hlsMasterPlaylist.d);
        arrayList.addAll(hlsMasterPlaylist.e);
        arrayList.addAll(hlsMasterPlaylist.f);
        a((List<HlsMasterPlaylist.HlsUrl>) arrayList);
        MediaPlaylistBundle mediaPlaylistBundle = this.d.get(this.l);
        if (z) {
            mediaPlaylistBundle.a((HlsMediaPlaylist) c2, j3);
        } else {
            long j4 = j3;
            mediaPlaylistBundle.c();
        }
        this.g.b(parsingLoadable.f3601a, parsingLoadable.d(), parsingLoadable.b(), 4, j2, j3, parsingLoadable.a());
    }

    public void a(ParsingLoadable<HlsPlaylist> parsingLoadable, long j2, long j3, boolean z) {
        MediaSourceEventListener.EventDispatcher eventDispatcher = this.g;
        DataSpec dataSpec = parsingLoadable.f3601a;
        Uri d2 = parsingLoadable.d();
        eventDispatcher.a(dataSpec, d2, parsingLoadable.b(), 4, j2, j3, parsingLoadable.a());
    }

    public Loader.LoadErrorAction a(ParsingLoadable<HlsPlaylist> parsingLoadable, long j2, long j3, IOException iOException, int i2) {
        ParsingLoadable<HlsPlaylist> parsingLoadable2 = parsingLoadable;
        long b2 = this.c.b(parsingLoadable2.b, j3, iOException, i2);
        boolean z = b2 == -9223372036854775807L;
        this.g.a(parsingLoadable2.f3601a, parsingLoadable.d(), parsingLoadable.b(), 4, j2, j3, parsingLoadable.a(), iOException, z);
        if (z) {
            return Loader.f;
        }
        return Loader.a(false, b2);
    }

    private void a(List<HlsMasterPlaylist.HlsUrl> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            HlsMasterPlaylist.HlsUrl hlsUrl = list.get(i2);
            this.d.put(hlsUrl, new MediaPlaylistBundle(hlsUrl));
        }
    }

    /* access modifiers changed from: private */
    public void a(HlsMasterPlaylist.HlsUrl hlsUrl, HlsMediaPlaylist hlsMediaPlaylist) {
        if (hlsUrl == this.l) {
            if (this.m == null) {
                this.n = !hlsMediaPlaylist.l;
                this.o = hlsMediaPlaylist.f;
            }
            this.m = hlsMediaPlaylist;
            this.j.a(hlsMediaPlaylist);
        }
        int size = this.e.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.e.get(i2).g();
        }
    }

    /* access modifiers changed from: private */
    public boolean a(HlsMasterPlaylist.HlsUrl hlsUrl, long j2) {
        int size = this.e.size();
        boolean z = false;
        for (int i2 = 0; i2 < size; i2++) {
            z |= !this.e.get(i2).a(hlsUrl, j2);
        }
        return z;
    }

    private static HlsMediaPlaylist.Segment a(HlsMediaPlaylist hlsMediaPlaylist, HlsMediaPlaylist hlsMediaPlaylist2) {
        int i2 = (int) (hlsMediaPlaylist2.i - hlsMediaPlaylist.i);
        List<HlsMediaPlaylist.Segment> list = hlsMediaPlaylist.o;
        if (i2 < list.size()) {
            return list.get(i2);
        }
        return null;
    }
}
