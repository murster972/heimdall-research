package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* compiled from: lambda */
public final /* synthetic */ class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f3499a;
    private final /* synthetic */ MediaSourceEventListener b;
    private final /* synthetic */ MediaSource.MediaPeriodId c;

    public /* synthetic */ i(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
        this.f3499a = eventDispatcher;
        this.b = mediaSourceEventListener;
        this.c = mediaPeriodId;
    }

    public final void run() {
        this.f3499a.a(this.b, this.c);
    }
}
