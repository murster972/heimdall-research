package com.google.android.exoplayer2.source.dash.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.offline.SegmentDownloadAction;
import com.google.android.exoplayer2.offline.StreamKey;
import java.util.List;

public final class DashDownloadAction extends SegmentDownloadAction {
    public static final DownloadAction.Deserializer DESERIALIZER = new SegmentDownloadAction.SegmentDownloadActionDeserializer("dash", 0) {
        /* access modifiers changed from: protected */
        public DownloadAction a(Uri uri, boolean z, byte[] bArr, List<StreamKey> list) {
            return new DashDownloadAction(uri, z, bArr, list);
        }
    };

    @Deprecated
    public DashDownloadAction(Uri uri, boolean z, byte[] bArr, List<StreamKey> list) {
        super("dash", 0, uri, z, bArr, list);
    }

    public DashDownloader a(DownloaderConstructorHelper downloaderConstructorHelper) {
        return new DashDownloader(this.c, this.g, downloaderConstructorHelper);
    }
}
