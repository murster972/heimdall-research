package com.google.android.exoplayer2.source.smoothstreaming.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.offline.SegmentDownloader;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifest;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifestParser;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsUtil;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class SsDownloader extends SegmentDownloader<SsManifest> {
    public SsDownloader(Uri uri, List<StreamKey> list, DownloaderConstructorHelper downloaderConstructorHelper) {
        super(SsUtil.a(uri), list, downloaderConstructorHelper);
    }

    /* access modifiers changed from: protected */
    public SsManifest a(DataSource dataSource, Uri uri) throws IOException {
        return (SsManifest) ParsingLoadable.a(dataSource, new SsManifestParser(), uri, 4);
    }

    /* access modifiers changed from: protected */
    public List<SegmentDownloader.Segment> a(DataSource dataSource, SsManifest ssManifest, boolean z) {
        ArrayList arrayList = new ArrayList();
        for (SsManifest.StreamElement streamElement : ssManifest.f) {
            for (int i = 0; i < streamElement.j.length; i++) {
                for (int i2 = 0; i2 < streamElement.k; i2++) {
                    arrayList.add(new SegmentDownloader.Segment(streamElement.b(i2), new DataSpec(streamElement.a(i, i2))));
                }
            }
        }
        return arrayList;
    }
}
