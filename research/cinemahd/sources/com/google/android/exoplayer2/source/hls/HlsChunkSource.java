package com.google.android.exoplayer2.source.hls;

import android.net.Uri;
import android.os.SystemClock;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.chunk.BaseMediaChunkIterator;
import com.google.android.exoplayer2.source.chunk.Chunk;
import com.google.android.exoplayer2.source.chunk.DataChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker;
import com.google.android.exoplayer2.trackselection.BaseTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

class HlsChunkSource {

    /* renamed from: a  reason: collision with root package name */
    private final HlsExtractorFactory f3479a;
    private final DataSource b;
    private final DataSource c;
    private final TimestampAdjusterProvider d;
    private final HlsMasterPlaylist.HlsUrl[] e;
    private final HlsPlaylistTracker f;
    private final TrackGroup g;
    private final List<Format> h;
    private boolean i;
    private byte[] j;
    private IOException k;
    private HlsMasterPlaylist.HlsUrl l;
    private boolean m;
    private Uri n;
    private byte[] o;
    private String p;
    private byte[] q;
    private TrackSelection r;
    private long s = -9223372036854775807L;
    private boolean t;

    private static final class EncryptionKeyChunk extends DataChunk {
        public final String k;
        private byte[] l;

        public EncryptionKeyChunk(DataSource dataSource, DataSpec dataSpec, Format format, int i, Object obj, byte[] bArr, String str) {
            super(dataSource, dataSpec, 3, format, i, obj, bArr);
            this.k = str;
        }

        /* access modifiers changed from: protected */
        public void a(byte[] bArr, int i) throws IOException {
            this.l = Arrays.copyOf(bArr, i);
        }

        public byte[] f() {
            return this.l;
        }
    }

    public static final class HlsChunkHolder {

        /* renamed from: a  reason: collision with root package name */
        public Chunk f3480a;
        public boolean b;
        public HlsMasterPlaylist.HlsUrl c;

        public HlsChunkHolder() {
            a();
        }

        public void a() {
            this.f3480a = null;
            this.b = false;
            this.c = null;
        }
    }

    private static final class HlsMediaPlaylistSegmentIterator extends BaseMediaChunkIterator {
        public HlsMediaPlaylistSegmentIterator(HlsMediaPlaylist hlsMediaPlaylist, long j, int i) {
            super((long) i, (long) (hlsMediaPlaylist.o.size() - 1));
        }
    }

    public HlsChunkSource(HlsExtractorFactory hlsExtractorFactory, HlsPlaylistTracker hlsPlaylistTracker, HlsMasterPlaylist.HlsUrl[] hlsUrlArr, HlsDataSourceFactory hlsDataSourceFactory, TransferListener transferListener, TimestampAdjusterProvider timestampAdjusterProvider, List<Format> list) {
        this.f3479a = hlsExtractorFactory;
        this.f = hlsPlaylistTracker;
        this.e = hlsUrlArr;
        this.d = timestampAdjusterProvider;
        this.h = list;
        Format[] formatArr = new Format[hlsUrlArr.length];
        int[] iArr = new int[hlsUrlArr.length];
        for (int i2 = 0; i2 < hlsUrlArr.length; i2++) {
            formatArr[i2] = hlsUrlArr[i2].b;
            iArr[i2] = i2;
        }
        this.b = hlsDataSourceFactory.a(1);
        if (transferListener != null) {
            this.b.a(transferListener);
        }
        this.c = hlsDataSourceFactory.a(3);
        this.g = new TrackGroup(formatArr);
        this.r = new InitializationTrackSelection(this.g, iArr);
    }

    private void e() {
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
    }

    public TrackGroup a() {
        return this.g;
    }

    public TrackSelection b() {
        return this.r;
    }

    public void c() throws IOException {
        IOException iOException = this.k;
        if (iOException == null) {
            HlsMasterPlaylist.HlsUrl hlsUrl = this.l;
            if (hlsUrl != null && this.t) {
                this.f.c(hlsUrl);
                return;
            }
            return;
        }
        throw iOException;
    }

    public void d() {
        this.k = null;
    }

    public void a(TrackSelection trackSelection) {
        this.r = trackSelection;
    }

    public void a(boolean z) {
        this.i = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(long r40, long r42, java.util.List<com.google.android.exoplayer2.source.hls.HlsMediaChunk> r44, com.google.android.exoplayer2.source.hls.HlsChunkSource.HlsChunkHolder r45) {
        /*
            r39 = this;
            r8 = r39
            r6 = r42
            r9 = r45
            boolean r0 = r44.isEmpty()
            r11 = 1
            if (r0 == 0) goto L_0x0011
            r1 = r44
            r4 = 0
            goto L_0x001f
        L_0x0011:
            int r0 = r44.size()
            int r0 = r0 - r11
            r1 = r44
            java.lang.Object r0 = r1.get(r0)
            com.google.android.exoplayer2.source.hls.HlsMediaChunk r0 = (com.google.android.exoplayer2.source.hls.HlsMediaChunk) r0
            r4 = r0
        L_0x001f:
            if (r4 != 0) goto L_0x0024
            r0 = -1
            r5 = -1
            goto L_0x002d
        L_0x0024:
            com.google.android.exoplayer2.source.TrackGroup r0 = r8.g
            com.google.android.exoplayer2.Format r2 = r4.c
            int r0 = r0.a((com.google.android.exoplayer2.Format) r2)
            r5 = r0
        L_0x002d:
            long r2 = r6 - r40
            long r12 = r39.a((long) r40)
            if (r4 == 0) goto L_0x0056
            boolean r0 = r8.m
            if (r0 != 0) goto L_0x0056
            long r14 = r4.b()
            long r2 = r2 - r14
            r10 = 0
            long r2 = java.lang.Math.max(r10, r2)
            r16 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r0 = (r12 > r16 ? 1 : (r12 == r16 ? 0 : -1))
            if (r0 == 0) goto L_0x0056
            long r12 = r12 - r14
            long r10 = java.lang.Math.max(r10, r12)
            r15 = r2
            r17 = r10
            goto L_0x0059
        L_0x0056:
            r15 = r2
            r17 = r12
        L_0x0059:
            com.google.android.exoplayer2.source.chunk.MediaChunkIterator[] r20 = r8.a((com.google.android.exoplayer2.source.hls.HlsMediaChunk) r4, (long) r6)
            com.google.android.exoplayer2.trackselection.TrackSelection r12 = r8.r
            r13 = r40
            r19 = r44
            r12.a(r13, r15, r17, r19, r20)
            com.google.android.exoplayer2.trackselection.TrackSelection r0 = r8.r
            int r10 = r0.d()
            r11 = 0
            if (r5 == r10) goto L_0x0071
            r12 = 1
            goto L_0x0072
        L_0x0071:
            r12 = 0
        L_0x0072:
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl[] r0 = r8.e
            r13 = r0[r10]
            com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker r0 = r8.f
            boolean r0 = r0.b((com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist.HlsUrl) r13)
            if (r0 != 0) goto L_0x008d
            r9.c = r13
            boolean r0 = r8.t
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl r1 = r8.l
            if (r1 != r13) goto L_0x0087
            r11 = 1
        L_0x0087:
            r0 = r0 & r11
            r8.t = r0
            r8.l = r13
            return
        L_0x008d:
            com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker r0 = r8.f
            r1 = 1
            com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist r14 = r0.a(r13, r1)
            boolean r0 = r14.c
            r8.m = r0
            r8.a((com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist) r14)
            long r0 = r14.f
            com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker r2 = r8.f
            long r2 = r2.a()
            long r15 = r0 - r2
            r0 = r39
            r1 = r4
            r2 = r12
            r3 = r14
            r31 = r4
            r17 = r5
            r4 = r15
            r6 = r42
            long r0 = r0.a((com.google.android.exoplayer2.source.hls.HlsMediaChunk) r1, (boolean) r2, (com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist) r3, (long) r4, (long) r6)
            long r2 = r14.i
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x00e5
            if (r31 == 0) goto L_0x00dd
            if (r12 == 0) goto L_0x00dd
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl[] r0 = r8.e
            r0 = r0[r17]
            com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker r1 = r8.f
            r2 = 1
            com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist r14 = r1.a(r0, r2)
            long r1 = r14.f
            com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker r3 = r8.f
            long r3 = r3.a()
            long r15 = r1 - r3
            long r1 = r31.e()
            r25 = r1
            r3 = r17
            goto L_0x00e9
        L_0x00dd:
            com.google.android.exoplayer2.source.BehindLiveWindowException r0 = new com.google.android.exoplayer2.source.BehindLiveWindowException
            r0.<init>()
            r8.k = r0
            return
        L_0x00e5:
            r25 = r0
            r3 = r10
            r0 = r13
        L_0x00e9:
            long r1 = r14.i
            long r1 = r25 - r1
            int r2 = (int) r1
            java.util.List<com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist$Segment> r1 = r14.o
            int r1 = r1.size()
            if (r2 < r1) goto L_0x010f
            boolean r1 = r14.l
            if (r1 == 0) goto L_0x00fe
            r1 = 1
            r9.b = r1
            goto L_0x010e
        L_0x00fe:
            r1 = 1
            r9.c = r0
            boolean r2 = r8.t
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl r3 = r8.l
            if (r3 != r0) goto L_0x0108
            r11 = 1
        L_0x0108:
            r1 = r2 & r11
            r8.t = r1
            r8.l = r0
        L_0x010e:
            return
        L_0x010f:
            r8.t = r11
            r1 = 0
            r8.l = r1
            java.util.List<com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist$Segment> r4 = r14.o
            java.lang.Object r2 = r4.get(r2)
            com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist$Segment r2 = (com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist.Segment) r2
            java.lang.String r4 = r2.g
            if (r4 == 0) goto L_0x015a
            java.lang.String r5 = r14.f3495a
            android.net.Uri r4 = com.google.android.exoplayer2.util.UriUtil.b(r5, r4)
            android.net.Uri r5 = r8.n
            boolean r5 = r4.equals(r5)
            if (r5 != 0) goto L_0x0148
            java.lang.String r2 = r2.h
            com.google.android.exoplayer2.trackselection.TrackSelection r0 = r8.r
            int r5 = r0.f()
            com.google.android.exoplayer2.trackselection.TrackSelection r0 = r8.r
            java.lang.Object r6 = r0.b()
            r0 = r39
            r1 = r4
            r4 = r5
            r5 = r6
            com.google.android.exoplayer2.source.hls.HlsChunkSource$EncryptionKeyChunk r0 = r0.a((android.net.Uri) r1, (java.lang.String) r2, (int) r3, (int) r4, (java.lang.Object) r5)
            r9.f3480a = r0
            return
        L_0x0148:
            java.lang.String r3 = r2.h
            java.lang.String r5 = r8.p
            boolean r3 = com.google.android.exoplayer2.util.Util.a((java.lang.Object) r3, (java.lang.Object) r5)
            if (r3 != 0) goto L_0x015d
            java.lang.String r3 = r2.h
            byte[] r5 = r8.o
            r8.a(r4, r3, r5)
            goto L_0x015d
        L_0x015a:
            r39.e()
        L_0x015d:
            com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist$Segment r3 = r2.b
            if (r3 == 0) goto L_0x017a
            java.lang.String r1 = r14.f3495a
            java.lang.String r4 = r3.f3494a
            android.net.Uri r18 = com.google.android.exoplayer2.util.UriUtil.b(r1, r4)
            com.google.android.exoplayer2.upstream.DataSpec r1 = new com.google.android.exoplayer2.upstream.DataSpec
            long r4 = r3.i
            long r6 = r3.j
            r23 = 0
            r17 = r1
            r19 = r4
            r21 = r6
            r17.<init>(r18, r19, r21, r23)
        L_0x017a:
            long r3 = r2.e
            long r3 = r3 + r15
            r21 = r3
            int r5 = r14.h
            int r6 = r2.d
            int r5 = r5 + r6
            r27 = r5
            com.google.android.exoplayer2.source.hls.TimestampAdjusterProvider r6 = r8.d
            com.google.android.exoplayer2.util.TimestampAdjuster r30 = r6.a(r5)
            java.lang.String r5 = r14.f3495a
            java.lang.String r6 = r2.f3494a
            android.net.Uri r33 = com.google.android.exoplayer2.util.UriUtil.b(r5, r6)
            com.google.android.exoplayer2.upstream.DataSpec r32 = new com.google.android.exoplayer2.upstream.DataSpec
            r15 = r32
            long r5 = r2.i
            long r10 = r2.j
            r38 = 0
            r34 = r5
            r36 = r10
            r32.<init>(r33, r34, r36, r38)
            com.google.android.exoplayer2.source.hls.HlsMediaChunk r5 = new com.google.android.exoplayer2.source.hls.HlsMediaChunk
            r12 = r5
            com.google.android.exoplayer2.source.hls.HlsExtractorFactory r13 = r8.f3479a
            com.google.android.exoplayer2.upstream.DataSource r14 = r8.b
            java.util.List<com.google.android.exoplayer2.Format> r6 = r8.h
            r18 = r6
            com.google.android.exoplayer2.trackselection.TrackSelection r6 = r8.r
            int r19 = r6.f()
            com.google.android.exoplayer2.trackselection.TrackSelection r6 = r8.r
            java.lang.Object r20 = r6.b()
            long r6 = r2.c
            long r23 = r3 + r6
            boolean r3 = r2.k
            r28 = r3
            boolean r3 = r8.i
            r29 = r3
            com.google.android.exoplayer2.drm.DrmInitData r2 = r2.f
            r32 = r2
            byte[] r2 = r8.o
            r33 = r2
            byte[] r2 = r8.q
            r34 = r2
            r16 = r1
            r17 = r0
            r12.<init>(r13, r14, r15, r16, r17, r18, r19, r20, r21, r23, r25, r27, r28, r29, r30, r31, r32, r33, r34)
            r9.f3480a = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.hls.HlsChunkSource.a(long, long, java.util.List, com.google.android.exoplayer2.source.hls.HlsChunkSource$HlsChunkHolder):void");
    }

    private static final class InitializationTrackSelection extends BaseTrackSelection {
        private int g;

        public InitializationTrackSelection(TrackGroup trackGroup, int[] iArr) {
            super(trackGroup, iArr);
            this.g = a(trackGroup.a(0));
        }

        public void a(long j, long j2, long j3, List<? extends MediaChunk> list, MediaChunkIterator[] mediaChunkIteratorArr) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (b(this.g, elapsedRealtime)) {
                for (int i = this.b - 1; i >= 0; i--) {
                    if (!b(i, elapsedRealtime)) {
                        this.g = i;
                        return;
                    }
                }
                throw new IllegalStateException();
            }
        }

        public Object b() {
            return null;
        }

        public int f() {
            return 0;
        }

        public int a() {
            return this.g;
        }
    }

    public void a(Chunk chunk) {
        if (chunk instanceof EncryptionKeyChunk) {
            EncryptionKeyChunk encryptionKeyChunk = (EncryptionKeyChunk) chunk;
            this.j = encryptionKeyChunk.e();
            a(encryptionKeyChunk.f3431a.f3586a, encryptionKeyChunk.k, encryptionKeyChunk.f());
        }
    }

    public boolean a(Chunk chunk, long j2) {
        TrackSelection trackSelection = this.r;
        return trackSelection.a(trackSelection.c(this.g.a(chunk.c)), j2);
    }

    public boolean a(HlsMasterPlaylist.HlsUrl hlsUrl, long j2) {
        int c2;
        int a2 = this.g.a(hlsUrl.b);
        if (a2 == -1 || (c2 = this.r.c(a2)) == -1) {
            return true;
        }
        this.t = (this.l == hlsUrl) | this.t;
        if (j2 == -9223372036854775807L || this.r.a(c2, j2)) {
            return true;
        }
        return false;
    }

    public MediaChunkIterator[] a(HlsMediaChunk hlsMediaChunk, long j2) {
        int i2;
        HlsMediaChunk hlsMediaChunk2 = hlsMediaChunk;
        if (hlsMediaChunk2 == null) {
            i2 = -1;
        } else {
            i2 = this.g.a(hlsMediaChunk2.c);
        }
        MediaChunkIterator[] mediaChunkIteratorArr = new MediaChunkIterator[this.r.length()];
        for (int i3 = 0; i3 < mediaChunkIteratorArr.length; i3++) {
            int b2 = this.r.b(i3);
            HlsMasterPlaylist.HlsUrl hlsUrl = this.e[b2];
            if (!this.f.b(hlsUrl)) {
                mediaChunkIteratorArr[i3] = MediaChunkIterator.f3437a;
            } else {
                HlsMediaPlaylist a2 = this.f.a(hlsUrl, false);
                long a3 = a2.f - this.f.a();
                long j3 = a3;
                long a4 = a(hlsMediaChunk, b2 != i2, a2, a3, j2);
                long j4 = a2.i;
                if (a4 < j4) {
                    mediaChunkIteratorArr[i3] = MediaChunkIterator.f3437a;
                } else {
                    mediaChunkIteratorArr[i3] = new HlsMediaPlaylistSegmentIterator(a2, j3, (int) (a4 - j4));
                }
            }
        }
        return mediaChunkIteratorArr;
    }

    private long a(HlsMediaChunk hlsMediaChunk, boolean z, HlsMediaPlaylist hlsMediaPlaylist, long j2, long j3) {
        long a2;
        long j4;
        if (hlsMediaChunk != null && !z) {
            return hlsMediaChunk.e();
        }
        long j5 = hlsMediaPlaylist.p + j2;
        if (hlsMediaChunk != null && !this.m) {
            j3 = hlsMediaChunk.f;
        }
        if (hlsMediaPlaylist.l || j3 < j5) {
            a2 = (long) Util.a(hlsMediaPlaylist.o, Long.valueOf(j3 - j2), true, !this.f.isLive() || hlsMediaChunk == null);
            j4 = hlsMediaPlaylist.i;
        } else {
            a2 = hlsMediaPlaylist.i;
            j4 = (long) hlsMediaPlaylist.o.size();
        }
        return a2 + j4;
    }

    private long a(long j2) {
        if (this.s != -9223372036854775807L) {
            return this.s - j2;
        }
        return -9223372036854775807L;
    }

    private void a(HlsMediaPlaylist hlsMediaPlaylist) {
        long j2;
        if (hlsMediaPlaylist.l) {
            j2 = -9223372036854775807L;
        } else {
            j2 = hlsMediaPlaylist.b() - this.f.a();
        }
        this.s = j2;
    }

    private EncryptionKeyChunk a(Uri uri, String str, int i2, int i3, Object obj) {
        return new EncryptionKeyChunk(this.c, new DataSpec(uri, 0, -1, (String) null, 1), this.e[i2].b, i3, obj, this.j, str);
    }

    private void a(Uri uri, String str, byte[] bArr) {
        byte[] byteArray = new BigInteger(Util.k(str).startsWith("0x") ? str.substring(2) : str, 16).toByteArray();
        byte[] bArr2 = new byte[16];
        int length = byteArray.length > 16 ? byteArray.length - 16 : 0;
        System.arraycopy(byteArray, length, bArr2, (bArr2.length - byteArray.length) + length, byteArray.length - length);
        this.n = uri;
        this.o = bArr;
        this.p = str;
        this.q = bArr2;
    }
}
