package com.google.android.exoplayer2.source.dash;

import com.google.android.exoplayer2.extractor.ChunkIndex;
import com.google.android.exoplayer2.source.dash.manifest.RangedUri;

public final class DashWrappingSegmentIndex implements DashSegmentIndex {

    /* renamed from: a  reason: collision with root package name */
    private final ChunkIndex f3448a;
    private final long b;

    public DashWrappingSegmentIndex(ChunkIndex chunkIndex, long j) {
        this.f3448a = chunkIndex;
        this.b = j;
    }

    public long a(long j) {
        return this.f3448a.e[(int) j] - this.b;
    }

    public boolean a() {
        return true;
    }

    public long b() {
        return 0;
    }

    public RangedUri b(long j) {
        ChunkIndex chunkIndex = this.f3448a;
        int i = (int) j;
        return new RangedUri((String) null, chunkIndex.c[i], (long) chunkIndex.b[i]);
    }

    public int c(long j) {
        return this.f3448a.f3247a;
    }

    public long a(long j, long j2) {
        return this.f3448a.d[(int) j];
    }

    public long b(long j, long j2) {
        return (long) this.f3448a.c(j + this.b);
    }
}
