package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* compiled from: lambda */
public final /* synthetic */ class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f3500a;
    private final /* synthetic */ MediaSourceEventListener b;
    private final /* synthetic */ MediaSource.MediaPeriodId c;

    public /* synthetic */ j(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
        this.f3500a = eventDispatcher;
        this.b = mediaSourceEventListener;
        this.c = mediaPeriodId;
    }

    public final void run() {
        this.f3500a.c(this.b, this.c);
    }
}
