package com.google.android.exoplayer2.source;

public interface SequenceableLoader {

    public interface Callback<T extends SequenceableLoader> {
        void a(T t);
    }

    long b();

    boolean b(long j);

    void c(long j);

    long f();
}
