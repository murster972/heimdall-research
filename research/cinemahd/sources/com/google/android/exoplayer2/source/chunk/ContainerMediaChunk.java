package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.DefaultExtractorInput;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public class ContainerMediaChunk extends BaseMediaChunk {
    private static final PositionHolder t = new PositionHolder();
    private final int n;
    private final long o;
    private final ChunkExtractorWrapper p;
    private long q;
    private volatile boolean r;
    private boolean s;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ContainerMediaChunk(DataSource dataSource, DataSpec dataSpec, Format format, int i, Object obj, long j, long j2, long j3, long j4, long j5, int i2, long j6, ChunkExtractorWrapper chunkExtractorWrapper) {
        super(dataSource, dataSpec, format, i, obj, j, j2, j3, j4, j5);
        this.n = i2;
        this.o = j6;
        this.p = chunkExtractorWrapper;
    }

    public final void cancelLoad() {
        this.r = true;
    }

    public long e() {
        return this.i + ((long) this.n);
    }

    public boolean f() {
        return this.s;
    }

    public final void load() throws IOException, InterruptedException {
        DefaultExtractorInput defaultExtractorInput;
        DataSpec a2 = this.f3431a.a(this.q);
        try {
            defaultExtractorInput = new DefaultExtractorInput(this.h, a2.d, this.h.a(a2));
            if (this.q == 0) {
                BaseMediaChunkOutput g = g();
                g.a(this.o);
                this.p.a(g, this.j == -9223372036854775807L ? -9223372036854775807L : this.j - this.o, this.k == -9223372036854775807L ? -9223372036854775807L : this.k - this.o);
            }
            Extractor extractor = this.p.f3432a;
            boolean z = false;
            int i = 0;
            while (i == 0 && !this.r) {
                i = extractor.a((ExtractorInput) defaultExtractorInput, t);
            }
            if (i != 1) {
                z = true;
            }
            Assertions.b(z);
            this.q = defaultExtractorInput.getPosition() - this.f3431a.d;
            Util.a((DataSource) this.h);
            this.s = true;
        } catch (Throwable th) {
            Util.a((DataSource) this.h);
            throw th;
        }
    }
}
