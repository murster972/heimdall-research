package com.google.android.exoplayer2.source.hls;

import android.text.TextUtils;
import com.facebook.common.util.ByteConstants;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.text.webvtt.WebvttParserUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class WebvttExtractor implements Extractor {
    private static final Pattern g = Pattern.compile("LOCAL:([^,]+)");
    private static final Pattern h = Pattern.compile("MPEGTS:(\\d+)");

    /* renamed from: a  reason: collision with root package name */
    private final String f3487a;
    private final TimestampAdjuster b;
    private final ParsableByteArray c = new ParsableByteArray();
    private ExtractorOutput d;
    private byte[] e = new byte[ByteConstants.KB];
    private int f;

    public WebvttExtractor(String str, TimestampAdjuster timestampAdjuster) {
        this.f3487a = str;
        this.b = timestampAdjuster;
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.a(this.e, 0, 6, false);
        this.c.a(this.e, 6);
        if (WebvttParserUtil.b(this.c)) {
            return true;
        }
        extractorInput.a(this.e, 6, 3, false);
        this.c.a(this.e, 9);
        return WebvttParserUtil.b(this.c);
    }

    public void release() {
    }

    public void a(ExtractorOutput extractorOutput) {
        this.d = extractorOutput;
        extractorOutput.a(new SeekMap.Unseekable(-9223372036854775807L));
    }

    public void a(long j, long j2) {
        throw new IllegalStateException();
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        int i;
        int length = (int) extractorInput.getLength();
        int i2 = this.f;
        byte[] bArr = this.e;
        if (i2 == bArr.length) {
            if (length != -1) {
                i = length;
            } else {
                i = bArr.length;
            }
            this.e = Arrays.copyOf(bArr, (i * 3) / 2);
        }
        byte[] bArr2 = this.e;
        int i3 = this.f;
        int read = extractorInput.read(bArr2, i3, bArr2.length - i3);
        if (read != -1) {
            this.f += read;
            if (length == -1 || this.f != length) {
                return 0;
            }
        }
        a();
        return -1;
    }

    private void a() throws ParserException {
        ParsableByteArray parsableByteArray = new ParsableByteArray(this.e);
        WebvttParserUtil.c(parsableByteArray);
        long j = 0;
        long j2 = 0;
        while (true) {
            String j3 = parsableByteArray.j();
            if (TextUtils.isEmpty(j3)) {
                Matcher a2 = WebvttParserUtil.a(parsableByteArray);
                if (a2 == null) {
                    a(0);
                    return;
                }
                long b2 = WebvttParserUtil.b(a2.group(1));
                long b3 = this.b.b(TimestampAdjuster.e((j + b2) - j2));
                TrackOutput a3 = a(b3 - b2);
                this.c.a(this.e, this.f);
                a3.a(this.c, this.f);
                a3.a(b3, 1, this.f, 0, (TrackOutput.CryptoData) null);
                return;
            } else if (j3.startsWith("X-TIMESTAMP-MAP")) {
                Matcher matcher = g.matcher(j3);
                if (matcher.find()) {
                    Matcher matcher2 = h.matcher(j3);
                    if (matcher2.find()) {
                        j2 = WebvttParserUtil.b(matcher.group(1));
                        j = TimestampAdjuster.d(Long.parseLong(matcher2.group(1)));
                    } else {
                        throw new ParserException("X-TIMESTAMP-MAP doesn't contain media timestamp: " + j3);
                    }
                } else {
                    throw new ParserException("X-TIMESTAMP-MAP doesn't contain local timestamp: " + j3);
                }
            }
        }
    }

    private TrackOutput a(long j) {
        TrackOutput a2 = this.d.a(0, 3);
        a2.a(Format.a((String) null, "text/vtt", (String) null, -1, 0, this.f3487a, (DrmInitData) null, j));
        this.d.a();
        return a2;
    }
}
