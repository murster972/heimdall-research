package com.google.android.exoplayer2.source;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Arrays;

public final class TrackGroup implements Parcelable {
    public static final Parcelable.Creator<TrackGroup> CREATOR = new Parcelable.Creator<TrackGroup>() {
        public TrackGroup createFromParcel(Parcel parcel) {
            return new TrackGroup(parcel);
        }

        public TrackGroup[] newArray(int i) {
            return new TrackGroup[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final int f3423a;
    private final Format[] b;
    private int c;

    public TrackGroup(Format... formatArr) {
        Assertions.b(formatArr.length > 0);
        this.b = formatArr;
        this.f3423a = formatArr.length;
    }

    public Format a(int i) {
        return this.b[i];
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || TrackGroup.class != obj.getClass()) {
            return false;
        }
        TrackGroup trackGroup = (TrackGroup) obj;
        if (this.f3423a != trackGroup.f3423a || !Arrays.equals(this.b, trackGroup.b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.c == 0) {
            this.c = 527 + Arrays.hashCode(this.b);
        }
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f3423a);
        for (int i2 = 0; i2 < this.f3423a; i2++) {
            parcel.writeParcelable(this.b[i2], 0);
        }
    }

    public int a(Format format) {
        int i = 0;
        while (true) {
            Format[] formatArr = this.b;
            if (i >= formatArr.length) {
                return -1;
            }
            if (format == formatArr[i]) {
                return i;
            }
            i++;
        }
    }

    TrackGroup(Parcel parcel) {
        this.f3423a = parcel.readInt();
        this.b = new Format[this.f3423a];
        for (int i = 0; i < this.f3423a; i++) {
            this.b[i] = (Format) parcel.readParcelable(Format.class.getClassLoader());
        }
    }
}
