package com.google.android.exoplayer2.source;

import android.os.Handler;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.TransferListener;
import java.io.IOException;

public interface MediaSource {

    public static final class MediaPeriodId {

        /* renamed from: a  reason: collision with root package name */
        public final Object f3414a;
        public final int b;
        public final int c;
        public final long d;
        public final long e;

        public MediaPeriodId(Object obj) {
            this(obj, -1);
        }

        public MediaPeriodId a(Object obj) {
            if (this.f3414a.equals(obj)) {
                return this;
            }
            return new MediaPeriodId(obj, this.b, this.c, this.d, this.e);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || MediaPeriodId.class != obj.getClass()) {
                return false;
            }
            MediaPeriodId mediaPeriodId = (MediaPeriodId) obj;
            if (this.f3414a.equals(mediaPeriodId.f3414a) && this.b == mediaPeriodId.b && this.c == mediaPeriodId.c && this.d == mediaPeriodId.d && this.e == mediaPeriodId.e) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return ((((((((527 + this.f3414a.hashCode()) * 31) + this.b) * 31) + this.c) * 31) + ((int) this.d)) * 31) + ((int) this.e);
        }

        public MediaPeriodId(Object obj, long j) {
            this(obj, -1, -1, j, Long.MIN_VALUE);
        }

        public boolean a() {
            return this.b != -1;
        }

        public MediaPeriodId(Object obj, long j, long j2) {
            this(obj, -1, -1, j, j2);
        }

        public MediaPeriodId(Object obj, int i, int i2, long j) {
            this(obj, i, i2, j, Long.MIN_VALUE);
        }

        private MediaPeriodId(Object obj, int i, int i2, long j, long j2) {
            this.f3414a = obj;
            this.b = i;
            this.c = i2;
            this.d = j;
            this.e = j2;
        }
    }

    public interface SourceInfoRefreshListener {
        void a(MediaSource mediaSource, Timeline timeline, Object obj);
    }

    MediaPeriod a(MediaPeriodId mediaPeriodId, Allocator allocator);

    void a(Handler handler, MediaSourceEventListener mediaSourceEventListener);

    void a(ExoPlayer exoPlayer, boolean z, SourceInfoRefreshListener sourceInfoRefreshListener, TransferListener transferListener);

    void a(MediaPeriod mediaPeriod);

    void a(SourceInfoRefreshListener sourceInfoRefreshListener);

    void a(MediaSourceEventListener mediaSourceEventListener);

    void b() throws IOException;
}
