package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;
import java.io.IOException;

/* compiled from: lambda */
public final /* synthetic */ class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f3438a;
    private final /* synthetic */ MediaSourceEventListener b;
    private final /* synthetic */ MediaSourceEventListener.LoadEventInfo c;
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData d;
    private final /* synthetic */ IOException e;
    private final /* synthetic */ boolean f;

    public /* synthetic */ d(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException iOException, boolean z) {
        this.f3438a = eventDispatcher;
        this.b = mediaSourceEventListener;
        this.c = loadEventInfo;
        this.d = mediaLoadData;
        this.e = iOException;
        this.f = z;
    }

    public final void run() {
        this.f3438a.a(this.b, this.c, this.d, this.e, this.f);
    }
}
