package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* compiled from: lambda */
public final /* synthetic */ class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f3474a;
    private final /* synthetic */ MediaSourceEventListener b;
    private final /* synthetic */ MediaSource.MediaPeriodId c;
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData d;

    public /* synthetic */ f(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f3474a = eventDispatcher;
        this.b = mediaSourceEventListener;
        this.c = mediaPeriodId;
        this.d = mediaLoadData;
    }

    public final void run() {
        this.f3474a.a(this.b, this.c, this.d);
    }
}
