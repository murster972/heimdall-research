package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* compiled from: lambda */
public final /* synthetic */ class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f3475a;
    private final /* synthetic */ MediaSourceEventListener b;
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData c;

    public /* synthetic */ g(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f3475a = eventDispatcher;
        this.b = mediaSourceEventListener;
        this.c = mediaLoadData;
    }

    public final void run() {
        this.f3475a.a(this.b, this.c);
    }
}
