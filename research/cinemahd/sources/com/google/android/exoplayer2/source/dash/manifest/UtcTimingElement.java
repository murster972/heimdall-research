package com.google.android.exoplayer2.source.dash.manifest;

public final class UtcTimingElement {

    /* renamed from: a  reason: collision with root package name */
    public final String f3472a;
    public final String b;

    public UtcTimingElement(String str, String str2) {
        this.f3472a = str;
        this.b = str2;
    }

    public String toString() {
        return this.f3472a + ", " + this.b;
    }
}
