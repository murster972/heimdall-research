package com.google.android.exoplayer2.source.dash.manifest;

import com.google.android.exoplayer2.source.dash.DashSegmentIndex;

final class SingleSegmentIndex implements DashSegmentIndex {

    /* renamed from: a  reason: collision with root package name */
    private final RangedUri f3470a;

    public SingleSegmentIndex(RangedUri rangedUri) {
        this.f3470a = rangedUri;
    }

    public long a(long j) {
        return 0;
    }

    public long a(long j, long j2) {
        return j2;
    }

    public boolean a() {
        return true;
    }

    public long b() {
        return 0;
    }

    public long b(long j, long j2) {
        return 0;
    }

    public RangedUri b(long j) {
        return this.f3470a;
    }

    public int c(long j) {
        return 1;
    }
}
