package com.google.android.exoplayer2.source.dash.manifest;

import android.net.Uri;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.offline.FilterableManifest;
import com.google.android.exoplayer2.offline.StreamKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DashManifest implements FilterableManifest<DashManifest> {

    /* renamed from: a  reason: collision with root package name */
    public final long f3459a;
    public final long b;
    public final long c;
    public final boolean d;
    public final long e;
    public final long f;
    public final long g;
    public final long h;
    public final UtcTimingElement i;
    public final Uri j;
    public final ProgramInformation k;
    private final List<Period> l;

    public DashManifest(long j2, long j3, long j4, boolean z, long j5, long j6, long j7, long j8, ProgramInformation programInformation, UtcTimingElement utcTimingElement, Uri uri, List<Period> list) {
        this.f3459a = j2;
        this.b = j3;
        this.c = j4;
        this.d = z;
        this.e = j5;
        this.f = j6;
        this.g = j7;
        this.h = j8;
        this.k = programInformation;
        this.i = utcTimingElement;
        this.j = uri;
        this.l = list == null ? Collections.emptyList() : list;
    }

    public final long b(int i2) {
        if (i2 != this.l.size() - 1) {
            return this.l.get(i2 + 1).b - this.l.get(i2).b;
        }
        long j2 = this.b;
        if (j2 == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        return j2 - this.l.get(i2).b;
    }

    public final long c(int i2) {
        return C.a(b(i2));
    }

    public final int a() {
        return this.l.size();
    }

    public final Period a(int i2) {
        return this.l.get(i2);
    }

    public final DashManifest a(List<StreamKey> list) {
        long j2;
        LinkedList linkedList = new LinkedList(list);
        Collections.sort(linkedList);
        linkedList.add(new StreamKey(-1, -1, -1));
        ArrayList arrayList = new ArrayList();
        long j3 = 0;
        int i2 = 0;
        while (true) {
            j2 = -9223372036854775807L;
            if (i2 >= a()) {
                break;
            }
            if (((StreamKey) linkedList.peek()).f3391a != i2) {
                long b2 = b(i2);
                if (b2 != -9223372036854775807L) {
                    j3 += b2;
                }
            } else {
                Period a2 = a(i2);
                arrayList.add(new Period(a2.f3464a, a2.b - j3, a(a2.c, linkedList), a2.d));
            }
            i2++;
        }
        long j4 = this.b;
        if (j4 != -9223372036854775807L) {
            j2 = j4 - j3;
        }
        return new DashManifest(this.f3459a, j2, this.c, this.d, this.e, this.f, this.g, this.h, this.k, this.i, this.j, arrayList);
    }

    private static ArrayList<AdaptationSet> a(List<AdaptationSet> list, LinkedList<StreamKey> linkedList) {
        StreamKey poll = linkedList.poll();
        int i2 = poll.f3391a;
        ArrayList<AdaptationSet> arrayList = new ArrayList<>();
        do {
            int i3 = poll.b;
            AdaptationSet adaptationSet = list.get(i3);
            List<Representation> list2 = adaptationSet.c;
            ArrayList arrayList2 = new ArrayList();
            do {
                arrayList2.add(list2.get(poll.c));
                poll = linkedList.poll();
                if (!(poll.f3391a == i2 && poll.b == i3)) {
                    arrayList.add(new AdaptationSet(adaptationSet.f3458a, adaptationSet.b, arrayList2, adaptationSet.d, adaptationSet.e));
                }
                arrayList2.add(list2.get(poll.c));
                poll = linkedList.poll();
                break;
            } while (poll.b == i3);
            arrayList.add(new AdaptationSet(adaptationSet.f3458a, adaptationSet.b, arrayList2, adaptationSet.d, adaptationSet.e));
        } while (poll.f3391a == i2);
        linkedList.addFirst(poll);
        return arrayList;
    }
}
