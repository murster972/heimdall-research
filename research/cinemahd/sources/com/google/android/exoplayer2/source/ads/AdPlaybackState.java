package com.google.android.exoplayer2.source.ads;

import android.net.Uri;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Arrays;

public final class AdPlaybackState {
    public static final AdPlaybackState f = new AdPlaybackState(new long[0]);

    /* renamed from: a  reason: collision with root package name */
    public final int f3426a;
    public final long[] b;
    public final AdGroup[] c;
    public final long d;
    public final long e;

    public static final class AdGroup {

        /* renamed from: a  reason: collision with root package name */
        public final int f3427a;
        public final Uri[] b;
        public final int[] c;
        public final long[] d;

        public AdGroup() {
            this(-1, new int[0], new Uri[0], new long[0]);
        }

        public int a() {
            return a(-1);
        }

        public boolean b() {
            return this.f3427a == -1 || a() < this.f3427a;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || AdGroup.class != obj.getClass()) {
                return false;
            }
            AdGroup adGroup = (AdGroup) obj;
            if (this.f3427a != adGroup.f3427a || !Arrays.equals(this.b, adGroup.b) || !Arrays.equals(this.c, adGroup.c) || !Arrays.equals(this.d, adGroup.d)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (((((this.f3427a * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c)) * 31) + Arrays.hashCode(this.d);
        }

        private AdGroup(int i, int[] iArr, Uri[] uriArr, long[] jArr) {
            Assertions.a(iArr.length == uriArr.length);
            this.f3427a = i;
            this.c = iArr;
            this.b = uriArr;
            this.d = jArr;
        }

        public int a(int i) {
            int i2 = i + 1;
            while (true) {
                int[] iArr = this.c;
                if (i2 >= iArr.length || iArr[i2] == 0 || iArr[i2] == 1) {
                    return i2;
                }
                i2++;
            }
            return i2;
        }
    }

    public AdPlaybackState(long... jArr) {
        int length = jArr.length;
        this.f3426a = length;
        this.b = Arrays.copyOf(jArr, length);
        this.c = new AdGroup[length];
        for (int i = 0; i < length; i++) {
            this.c[i] = new AdGroup();
        }
        this.d = 0;
        this.e = -9223372036854775807L;
    }

    public int a(long j) {
        int i = 0;
        while (true) {
            long[] jArr = this.b;
            if (i < jArr.length && jArr[i] != Long.MIN_VALUE && (j >= jArr[i] || !this.c[i].b())) {
                i++;
            }
        }
        if (i < this.b.length) {
            return i;
        }
        return -1;
    }

    public int b(long j) {
        int length = this.b.length - 1;
        while (length >= 0 && a(j, length)) {
            length--;
        }
        if (length < 0 || !this.c[length].b()) {
            return -1;
        }
        return length;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AdPlaybackState.class != obj.getClass()) {
            return false;
        }
        AdPlaybackState adPlaybackState = (AdPlaybackState) obj;
        if (this.f3426a == adPlaybackState.f3426a && this.d == adPlaybackState.d && this.e == adPlaybackState.e && Arrays.equals(this.b, adPlaybackState.b) && Arrays.equals(this.c, adPlaybackState.c)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((((((this.f3426a * 31) + ((int) this.d)) * 31) + ((int) this.e)) * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c);
    }

    private boolean a(long j, int i) {
        long j2 = this.b[i];
        if (j2 == Long.MIN_VALUE) {
            long j3 = this.e;
            if (j3 == -9223372036854775807L || j < j3) {
                return true;
            }
            return false;
        } else if (j < j2) {
            return true;
        } else {
            return false;
        }
    }
}
