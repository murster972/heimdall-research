package com.google.android.exoplayer2.source.hls;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.source.CompositeSequenceableLoaderFactory;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.SequenceableLoader;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistTracker;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;

public final class HlsMediaPeriod implements MediaPeriod, HlsSampleStreamWrapper.Callback, HlsPlaylistTracker.PlaylistEventListener {

    /* renamed from: a  reason: collision with root package name */
    private final HlsExtractorFactory f3482a;
    private final HlsPlaylistTracker b;
    private final HlsDataSourceFactory c;
    private final TransferListener d;
    private final LoadErrorHandlingPolicy e;
    private final MediaSourceEventListener.EventDispatcher f;
    private final Allocator g;
    private final IdentityHashMap<SampleStream, Integer> h = new IdentityHashMap<>();
    private final TimestampAdjusterProvider i = new TimestampAdjusterProvider();
    private final CompositeSequenceableLoaderFactory j;
    private final boolean k;
    private MediaPeriod.Callback l;
    private int m;
    private TrackGroupArray n;
    private HlsSampleStreamWrapper[] o = new HlsSampleStreamWrapper[0];
    private HlsSampleStreamWrapper[] p = new HlsSampleStreamWrapper[0];
    private SequenceableLoader q;
    private boolean r;

    public HlsMediaPeriod(HlsExtractorFactory hlsExtractorFactory, HlsPlaylistTracker hlsPlaylistTracker, HlsDataSourceFactory hlsDataSourceFactory, TransferListener transferListener, LoadErrorHandlingPolicy loadErrorHandlingPolicy, MediaSourceEventListener.EventDispatcher eventDispatcher, Allocator allocator, CompositeSequenceableLoaderFactory compositeSequenceableLoaderFactory, boolean z) {
        this.f3482a = hlsExtractorFactory;
        this.b = hlsPlaylistTracker;
        this.c = hlsDataSourceFactory;
        this.d = transferListener;
        this.e = loadErrorHandlingPolicy;
        this.f = eventDispatcher;
        this.g = allocator;
        this.j = compositeSequenceableLoaderFactory;
        this.k = z;
        this.q = compositeSequenceableLoaderFactory.a(new SequenceableLoader[0]);
        eventDispatcher.a();
    }

    public long a(long j2, SeekParameters seekParameters) {
        return j2;
    }

    public boolean b(long j2) {
        if (this.n != null) {
            return this.q.b(j2);
        }
        for (HlsSampleStreamWrapper h2 : this.o) {
            h2.h();
        }
        return false;
    }

    public void c(long j2) {
        this.q.c(j2);
    }

    public void d() throws IOException {
        for (HlsSampleStreamWrapper d2 : this.o) {
            d2.d();
        }
    }

    public TrackGroupArray e() {
        return this.n;
    }

    public long f() {
        return this.q.f();
    }

    public void g() {
        this.l.a(this);
    }

    public void h() {
        this.b.a((HlsPlaylistTracker.PlaylistEventListener) this);
        for (HlsSampleStreamWrapper j2 : this.o) {
            j2.j();
        }
        this.l = null;
        this.f.b();
    }

    public void a(MediaPeriod.Callback callback, long j2) {
        this.l = callback;
        this.b.b((HlsPlaylistTracker.PlaylistEventListener) this);
        d(j2);
    }

    public long c() {
        if (this.r) {
            return -9223372036854775807L;
        }
        this.f.c();
        this.r = true;
        return -9223372036854775807L;
    }

    private void d(long j2) {
        HlsMasterPlaylist b2 = this.b.b();
        List<HlsMasterPlaylist.HlsUrl> list = b2.e;
        List<HlsMasterPlaylist.HlsUrl> list2 = b2.f;
        int size = list.size() + 1 + list2.size();
        this.o = new HlsSampleStreamWrapper[size];
        this.m = size;
        a(b2, j2);
        char c2 = 0;
        int i2 = 0;
        int i3 = 1;
        while (i2 < list.size()) {
            HlsMasterPlaylist.HlsUrl hlsUrl = list.get(i2);
            HlsMasterPlaylist.HlsUrl[] hlsUrlArr = new HlsMasterPlaylist.HlsUrl[1];
            hlsUrlArr[c2] = hlsUrl;
            HlsSampleStreamWrapper a2 = a(1, hlsUrlArr, (Format) null, (List<Format>) Collections.emptyList(), j2);
            int i4 = i3 + 1;
            this.o[i3] = a2;
            Format format = hlsUrl.b;
            if (!this.k || format.d == null) {
                a2.h();
            } else {
                a2.a(new TrackGroupArray(new TrackGroup(format)), 0, TrackGroupArray.d);
            }
            i2++;
            i3 = i4;
            c2 = 0;
        }
        int i5 = 0;
        while (i5 < list2.size()) {
            HlsMasterPlaylist.HlsUrl hlsUrl2 = list2.get(i5);
            HlsSampleStreamWrapper a3 = a(3, new HlsMasterPlaylist.HlsUrl[]{hlsUrl2}, (Format) null, (List<Format>) Collections.emptyList(), j2);
            this.o[i3] = a3;
            a3.a(new TrackGroupArray(new TrackGroup(hlsUrl2.b)), 0, TrackGroupArray.d);
            i5++;
            i3++;
        }
        this.p = this.o;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ea, code lost:
        if (r5 != r8[0]) goto L_0x00ee;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(com.google.android.exoplayer2.trackselection.TrackSelection[] r21, boolean[] r22, com.google.android.exoplayer2.source.SampleStream[] r23, boolean[] r24, long r25) {
        /*
            r20 = this;
            r0 = r20
            r1 = r21
            r2 = r23
            int r3 = r1.length
            int[] r3 = new int[r3]
            int r4 = r1.length
            int[] r4 = new int[r4]
            r6 = 0
        L_0x000d:
            int r7 = r1.length
            if (r6 >= r7) goto L_0x004e
            r7 = r2[r6]
            r8 = -1
            if (r7 != 0) goto L_0x0017
            r7 = -1
            goto L_0x0025
        L_0x0017:
            java.util.IdentityHashMap<com.google.android.exoplayer2.source.SampleStream, java.lang.Integer> r7 = r0.h
            r9 = r2[r6]
            java.lang.Object r7 = r7.get(r9)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
        L_0x0025:
            r3[r6] = r7
            r4[r6] = r8
            r7 = r1[r6]
            if (r7 == 0) goto L_0x004b
            r7 = r1[r6]
            com.google.android.exoplayer2.source.TrackGroup r7 = r7.c()
            r9 = 0
        L_0x0034:
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r10 = r0.o
            int r11 = r10.length
            if (r9 >= r11) goto L_0x004b
            r10 = r10[r9]
            com.google.android.exoplayer2.source.TrackGroupArray r10 = r10.e()
            int r10 = r10.a((com.google.android.exoplayer2.source.TrackGroup) r7)
            if (r10 == r8) goto L_0x0048
            r4[r6] = r9
            goto L_0x004b
        L_0x0048:
            int r9 = r9 + 1
            goto L_0x0034
        L_0x004b:
            int r6 = r6 + 1
            goto L_0x000d
        L_0x004e:
            java.util.IdentityHashMap<com.google.android.exoplayer2.source.SampleStream, java.lang.Integer> r6 = r0.h
            r6.clear()
            int r6 = r1.length
            com.google.android.exoplayer2.source.SampleStream[] r6 = new com.google.android.exoplayer2.source.SampleStream[r6]
            int r7 = r1.length
            com.google.android.exoplayer2.source.SampleStream[] r7 = new com.google.android.exoplayer2.source.SampleStream[r7]
            int r8 = r1.length
            com.google.android.exoplayer2.trackselection.TrackSelection[] r15 = new com.google.android.exoplayer2.trackselection.TrackSelection[r8]
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r8 = r0.o
            int r8 = r8.length
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r13 = new com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[r8]
            r12 = 0
            r14 = 0
            r16 = 0
        L_0x0065:
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r8 = r0.o
            int r8 = r8.length
            if (r14 >= r8) goto L_0x010a
            r8 = 0
        L_0x006b:
            int r9 = r1.length
            if (r8 >= r9) goto L_0x0084
            r9 = r3[r8]
            r10 = 0
            if (r9 != r14) goto L_0x0076
            r9 = r2[r8]
            goto L_0x0077
        L_0x0076:
            r9 = r10
        L_0x0077:
            r7[r8] = r9
            r9 = r4[r8]
            if (r9 != r14) goto L_0x007f
            r10 = r1[r8]
        L_0x007f:
            r15[r8] = r10
            int r8 = r8 + 1
            goto L_0x006b
        L_0x0084:
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r8 = r0.o
            r11 = r8[r14]
            r8 = r11
            r9 = r15
            r10 = r22
            r5 = r11
            r11 = r7
            r2 = r12
            r12 = r24
            r17 = r2
            r18 = r13
            r2 = r14
            r13 = r25
            r19 = r15
            r15 = r16
            boolean r8 = r8.a(r9, r10, r11, r12, r13, r15)
            r9 = 0
            r10 = 0
        L_0x00a2:
            int r11 = r1.length
            r12 = 1
            if (r9 >= r11) goto L_0x00d5
            r11 = r4[r9]
            if (r11 != r2) goto L_0x00c5
            r10 = r7[r9]
            if (r10 == 0) goto L_0x00b0
            r10 = 1
            goto L_0x00b1
        L_0x00b0:
            r10 = 0
        L_0x00b1:
            com.google.android.exoplayer2.util.Assertions.b(r10)
            r10 = r7[r9]
            r6[r9] = r10
            java.util.IdentityHashMap<com.google.android.exoplayer2.source.SampleStream, java.lang.Integer> r10 = r0.h
            r11 = r7[r9]
            java.lang.Integer r13 = java.lang.Integer.valueOf(r2)
            r10.put(r11, r13)
            r10 = 1
            goto L_0x00d2
        L_0x00c5:
            r11 = r3[r9]
            if (r11 != r2) goto L_0x00d2
            r11 = r7[r9]
            if (r11 != 0) goto L_0x00ce
            goto L_0x00cf
        L_0x00ce:
            r12 = 0
        L_0x00cf:
            com.google.android.exoplayer2.util.Assertions.b(r12)
        L_0x00d2:
            int r9 = r9 + 1
            goto L_0x00a2
        L_0x00d5:
            if (r10 == 0) goto L_0x00fd
            r18[r17] = r5
            int r9 = r17 + 1
            if (r17 != 0) goto L_0x00f7
            r5.a((boolean) r12)
            if (r8 != 0) goto L_0x00ed
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r8 = r0.p
            int r10 = r8.length
            if (r10 == 0) goto L_0x00ed
            r10 = 0
            r8 = r8[r10]
            if (r5 == r8) goto L_0x00fb
            goto L_0x00ee
        L_0x00ed:
            r10 = 0
        L_0x00ee:
            com.google.android.exoplayer2.source.hls.TimestampAdjusterProvider r5 = r0.i
            r5.a()
            r12 = r9
            r16 = 1
            goto L_0x0100
        L_0x00f7:
            r10 = 0
            r5.a((boolean) r10)
        L_0x00fb:
            r12 = r9
            goto L_0x0100
        L_0x00fd:
            r10 = 0
            r12 = r17
        L_0x0100:
            int r14 = r2 + 1
            r2 = r23
            r13 = r18
            r15 = r19
            goto L_0x0065
        L_0x010a:
            r17 = r12
            r18 = r13
            r10 = 0
            int r1 = r6.length
            r2 = r23
            java.lang.System.arraycopy(r6, r10, r2, r10, r1)
            r1 = r18
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r1, r12)
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r1 = (com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[]) r1
            r0.p = r1
            com.google.android.exoplayer2.source.CompositeSequenceableLoaderFactory r1 = r0.j
            com.google.android.exoplayer2.source.hls.HlsSampleStreamWrapper[] r2 = r0.p
            com.google.android.exoplayer2.source.SequenceableLoader r1 = r1.a(r2)
            r0.q = r1
            return r25
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.hls.HlsMediaPeriod.a(com.google.android.exoplayer2.trackselection.TrackSelection[], boolean[], com.google.android.exoplayer2.source.SampleStream[], boolean[], long):long");
    }

    public long b() {
        return this.q.b();
    }

    public void a(long j2, boolean z) {
        for (HlsSampleStreamWrapper a2 : this.p) {
            a2.a(j2, z);
        }
    }

    public long a(long j2) {
        HlsSampleStreamWrapper[] hlsSampleStreamWrapperArr = this.p;
        if (hlsSampleStreamWrapperArr.length > 0) {
            boolean b2 = hlsSampleStreamWrapperArr[0].b(j2, false);
            int i2 = 1;
            while (true) {
                HlsSampleStreamWrapper[] hlsSampleStreamWrapperArr2 = this.p;
                if (i2 >= hlsSampleStreamWrapperArr2.length) {
                    break;
                }
                hlsSampleStreamWrapperArr2[i2].b(j2, b2);
                i2++;
            }
            if (b2) {
                this.i.a();
            }
        }
        return j2;
    }

    public void a() {
        int i2 = this.m - 1;
        this.m = i2;
        if (i2 <= 0) {
            int i3 = 0;
            for (HlsSampleStreamWrapper e2 : this.o) {
                i3 += e2.e().f3424a;
            }
            TrackGroup[] trackGroupArr = new TrackGroup[i3];
            HlsSampleStreamWrapper[] hlsSampleStreamWrapperArr = this.o;
            int length = hlsSampleStreamWrapperArr.length;
            int i4 = 0;
            int i5 = 0;
            while (i4 < length) {
                HlsSampleStreamWrapper hlsSampleStreamWrapper = hlsSampleStreamWrapperArr[i4];
                int i6 = hlsSampleStreamWrapper.e().f3424a;
                int i7 = i5;
                int i8 = 0;
                while (i8 < i6) {
                    trackGroupArr[i7] = hlsSampleStreamWrapper.e().a(i8);
                    i8++;
                    i7++;
                }
                i4++;
                i5 = i7;
            }
            this.n = new TrackGroupArray(trackGroupArr);
            this.l.a(this);
        }
    }

    public void a(HlsMasterPlaylist.HlsUrl hlsUrl) {
        this.b.a(hlsUrl);
    }

    public void a(HlsSampleStreamWrapper hlsSampleStreamWrapper) {
        this.l.a(this);
    }

    public boolean a(HlsMasterPlaylist.HlsUrl hlsUrl, long j2) {
        boolean z = true;
        for (HlsSampleStreamWrapper a2 : this.o) {
            z &= a2.a(hlsUrl, j2);
        }
        this.l.a(this);
        return z;
    }

    private void a(HlsMasterPlaylist hlsMasterPlaylist, long j2) {
        ArrayList arrayList;
        HlsMasterPlaylist hlsMasterPlaylist2 = hlsMasterPlaylist;
        ArrayList arrayList2 = new ArrayList(hlsMasterPlaylist2.d);
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        for (int i2 = 0; i2 < arrayList2.size(); i2++) {
            HlsMasterPlaylist.HlsUrl hlsUrl = (HlsMasterPlaylist.HlsUrl) arrayList2.get(i2);
            Format format = hlsUrl.b;
            if (format.m > 0 || Util.a(format.d, 2) != null) {
                arrayList3.add(hlsUrl);
            } else if (Util.a(format.d, 1) != null) {
                arrayList4.add(hlsUrl);
            }
        }
        if (!arrayList3.isEmpty()) {
            arrayList = arrayList3;
        } else {
            if (arrayList4.size() < arrayList2.size()) {
                arrayList2.removeAll(arrayList4);
            }
            arrayList = arrayList2;
        }
        Assertions.a(!arrayList.isEmpty());
        HlsMasterPlaylist.HlsUrl[] hlsUrlArr = (HlsMasterPlaylist.HlsUrl[]) arrayList.toArray(new HlsMasterPlaylist.HlsUrl[0]);
        String str = hlsUrlArr[0].b.d;
        HlsSampleStreamWrapper a2 = a(0, hlsUrlArr, hlsMasterPlaylist2.g, hlsMasterPlaylist2.h, j2);
        this.o[0] = a2;
        if (!this.k || str == null) {
            a2.a(true);
            a2.h();
            return;
        }
        boolean z = Util.a(str, 2) != null;
        boolean z2 = Util.a(str, 1) != null;
        ArrayList arrayList5 = new ArrayList();
        if (z) {
            Format[] formatArr = new Format[arrayList.size()];
            for (int i3 = 0; i3 < formatArr.length; i3++) {
                formatArr[i3] = a(hlsUrlArr[i3].b);
            }
            arrayList5.add(new TrackGroup(formatArr));
            if (z2 && (hlsMasterPlaylist2.g != null || hlsMasterPlaylist2.e.isEmpty())) {
                arrayList5.add(new TrackGroup(a(hlsUrlArr[0].b, hlsMasterPlaylist2.g, false)));
            }
            List<Format> list = hlsMasterPlaylist2.h;
            if (list != null) {
                for (int i4 = 0; i4 < list.size(); i4++) {
                    arrayList5.add(new TrackGroup(list.get(i4)));
                }
            }
        } else if (z2) {
            Format[] formatArr2 = new Format[arrayList.size()];
            for (int i5 = 0; i5 < formatArr2.length; i5++) {
                formatArr2[i5] = a(hlsUrlArr[i5].b, hlsMasterPlaylist2.g, true);
            }
            arrayList5.add(new TrackGroup(formatArr2));
        } else {
            throw new IllegalArgumentException("Unexpected codecs attribute: " + str);
        }
        TrackGroup trackGroup = new TrackGroup(Format.a("ID3", "application/id3", (String) null, -1, (DrmInitData) null));
        arrayList5.add(trackGroup);
        a2.a(new TrackGroupArray((TrackGroup[]) arrayList5.toArray(new TrackGroup[0])), 0, new TrackGroupArray(trackGroup));
    }

    private HlsSampleStreamWrapper a(int i2, HlsMasterPlaylist.HlsUrl[] hlsUrlArr, Format format, List<Format> list, long j2) {
        return new HlsSampleStreamWrapper(i2, this, new HlsChunkSource(this.f3482a, this.b, hlsUrlArr, this.c, this.d, this.i, list), this.g, j2, format, this.e, this.f);
    }

    private static Format a(Format format) {
        String a2 = Util.a(format.d, 2);
        return Format.a(format.f3164a, format.b, format.f, MimeTypes.d(a2), a2, format.c, format.l, format.m, format.n, (List<byte[]>) null, format.y);
    }

    private static Format a(Format format, Format format2, boolean z) {
        String str;
        int i2;
        int i3;
        String str2;
        String str3;
        Format format3 = format;
        Format format4 = format2;
        if (format4 != null) {
            String str4 = format4.d;
            int i4 = format4.t;
            int i5 = format4.y;
            String str5 = format4.z;
            str3 = format4.b;
            str2 = str4;
            i3 = i4;
            i2 = i5;
            str = str5;
        } else {
            String a2 = Util.a(format3.d, 1);
            if (z) {
                int i6 = format3.t;
                int i7 = format3.y;
                str2 = a2;
                i3 = i6;
                str3 = format3.b;
                str = str3;
                i2 = i7;
            } else {
                str2 = a2;
                str3 = null;
                str = null;
                i3 = -1;
                i2 = 0;
            }
        }
        return Format.a(format3.f3164a, str3, format3.f, MimeTypes.d(str2), str2, z ? format3.c : -1, i3, -1, (List<byte[]>) null, i2, str);
    }
}
