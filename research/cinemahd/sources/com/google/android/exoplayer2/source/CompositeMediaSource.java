package com.google.android.exoplayer2.source;

import android.os.Handler;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.HashMap;

public abstract class CompositeMediaSource<T> extends BaseMediaSource {
    private final HashMap<T, MediaSourceAndListener> f = new HashMap<>();
    private ExoPlayer g;
    private Handler h;
    private TransferListener i;

    private static final class MediaSourceAndListener {

        /* renamed from: a  reason: collision with root package name */
        public final MediaSource f3402a;
        public final MediaSource.SourceInfoRefreshListener b;
        public final MediaSourceEventListener c;

        public MediaSourceAndListener(MediaSource mediaSource, MediaSource.SourceInfoRefreshListener sourceInfoRefreshListener, MediaSourceEventListener mediaSourceEventListener) {
            this.f3402a = mediaSource;
            this.b = sourceInfoRefreshListener;
            this.c = mediaSourceEventListener;
        }
    }

    protected CompositeMediaSource() {
    }

    /* access modifiers changed from: protected */
    public int a(T t, int i2) {
        return i2;
    }

    /* access modifiers changed from: protected */
    public long a(T t, long j) {
        return j;
    }

    /* access modifiers changed from: protected */
    public MediaSource.MediaPeriodId a(T t, MediaSource.MediaPeriodId mediaPeriodId) {
        return mediaPeriodId;
    }

    public void a(ExoPlayer exoPlayer, boolean z, TransferListener transferListener) {
        this.g = exoPlayer;
        this.i = transferListener;
        this.h = new Handler();
    }

    public void b() throws IOException {
        for (MediaSourceAndListener mediaSourceAndListener : this.f.values()) {
            mediaSourceAndListener.f3402a.b();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void a(T t, MediaSource mediaSource, Timeline timeline, Object obj);

    public void h() {
        for (MediaSourceAndListener next : this.f.values()) {
            next.f3402a.a(next.b);
            next.f3402a.a(next.c);
        }
        this.f.clear();
        this.g = null;
    }

    private final class ForwardingEventListener implements MediaSourceEventListener {

        /* renamed from: a  reason: collision with root package name */
        private final T f3401a;
        private MediaSourceEventListener.EventDispatcher b;

        public ForwardingEventListener(T t) {
            this.b = CompositeMediaSource.this.a((MediaSource.MediaPeriodId) null);
            this.f3401a = t;
        }

        private boolean d(int i, MediaSource.MediaPeriodId mediaPeriodId) {
            MediaSource.MediaPeriodId mediaPeriodId2;
            if (mediaPeriodId != null) {
                mediaPeriodId2 = CompositeMediaSource.this.a(this.f3401a, mediaPeriodId);
                if (mediaPeriodId2 == null) {
                    return false;
                }
            } else {
                mediaPeriodId2 = null;
            }
            int a2 = CompositeMediaSource.this.a(this.f3401a, i);
            MediaSourceEventListener.EventDispatcher eventDispatcher = this.b;
            if (eventDispatcher.f3415a == a2 && Util.a((Object) eventDispatcher.b, (Object) mediaPeriodId2)) {
                return true;
            }
            this.b = CompositeMediaSource.this.a(a2, mediaPeriodId2, 0);
            return true;
        }

        public void a(int i, MediaSource.MediaPeriodId mediaPeriodId) {
            if (d(i, mediaPeriodId)) {
                this.b.a();
            }
        }

        public void b(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
            if (d(i, mediaPeriodId)) {
                this.b.b(loadEventInfo, a(mediaLoadData));
            }
        }

        public void c(int i, MediaSource.MediaPeriodId mediaPeriodId) {
            if (d(i, mediaPeriodId)) {
                this.b.b();
            }
        }

        public void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
            if (d(i, mediaPeriodId)) {
                this.b.c(loadEventInfo, a(mediaLoadData));
            }
        }

        public void b(int i, MediaSource.MediaPeriodId mediaPeriodId) {
            if (d(i, mediaPeriodId)) {
                this.b.c();
            }
        }

        public void c(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
            if (d(i, mediaPeriodId)) {
                this.b.a(loadEventInfo, a(mediaLoadData));
            }
        }

        public void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException iOException, boolean z) {
            if (d(i, mediaPeriodId)) {
                this.b.a(loadEventInfo, a(mediaLoadData), iOException, z);
            }
        }

        public void b(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.MediaLoadData mediaLoadData) {
            if (d(i, mediaPeriodId)) {
                this.b.a(a(mediaLoadData));
            }
        }

        public void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.MediaLoadData mediaLoadData) {
            if (d(i, mediaPeriodId)) {
                this.b.b(a(mediaLoadData));
            }
        }

        private MediaSourceEventListener.MediaLoadData a(MediaSourceEventListener.MediaLoadData mediaLoadData) {
            long a2 = CompositeMediaSource.this.a(this.f3401a, mediaLoadData.f);
            long a3 = CompositeMediaSource.this.a(this.f3401a, mediaLoadData.g);
            if (a2 == mediaLoadData.f && a3 == mediaLoadData.g) {
                return mediaLoadData;
            }
            return new MediaSourceEventListener.MediaLoadData(mediaLoadData.f3417a, mediaLoadData.b, mediaLoadData.c, mediaLoadData.d, mediaLoadData.e, a2, a3);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(T t, MediaSource mediaSource) {
        Assertions.a(!this.f.containsKey(t));
        a aVar = new a(this, t);
        ForwardingEventListener forwardingEventListener = new ForwardingEventListener(t);
        this.f.put(t, new MediaSourceAndListener(mediaSource, aVar, forwardingEventListener));
        Handler handler = this.h;
        Assertions.a(handler);
        mediaSource.a(handler, (MediaSourceEventListener) forwardingEventListener);
        ExoPlayer exoPlayer = this.g;
        Assertions.a(exoPlayer);
        mediaSource.a(exoPlayer, false, aVar, this.i);
    }

    /* access modifiers changed from: protected */
    public final void a(T t) {
        MediaSourceAndListener remove = this.f.remove(t);
        Assertions.a(remove);
        MediaSourceAndListener mediaSourceAndListener = remove;
        mediaSourceAndListener.f3402a.a(mediaSourceAndListener.b);
        mediaSourceAndListener.f3402a.a(mediaSourceAndListener.c);
    }
}
