package com.google.android.exoplayer2.source;

import android.net.Uri;
import android.os.Handler;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.ads.AdsMediaSource$MediaSourceFactory;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;

public final class ExtractorMediaSource extends BaseMediaSource implements ExtractorMediaPeriod.Listener {
    private final Uri f;
    private final DataSource.Factory g;
    private final ExtractorsFactory h;
    private final LoadErrorHandlingPolicy i;
    private final String j;
    private final int k;
    private final Object l;
    private long m;
    private boolean n;
    private TransferListener o;

    @Deprecated
    public interface EventListener {
        void a(IOException iOException);
    }

    @Deprecated
    private static final class EventListenerWrapper extends DefaultMediaSourceEventListener {

        /* renamed from: a  reason: collision with root package name */
        private final EventListener f3412a;

        public EventListenerWrapper(EventListener eventListener) {
            Assertions.a(eventListener);
            this.f3412a = eventListener;
        }

        public void a(int i, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException iOException, boolean z) {
            this.f3412a.a(iOException);
        }
    }

    public static final class Factory implements AdsMediaSource$MediaSourceFactory {

        /* renamed from: a  reason: collision with root package name */
        private final DataSource.Factory f3413a;
        private ExtractorsFactory b;
        private String c;
        private Object d;
        private LoadErrorHandlingPolicy e = new DefaultLoadErrorHandlingPolicy();
        private int f = 1048576;

        public Factory(DataSource.Factory factory) {
            this.f3413a = factory;
        }

        public ExtractorMediaSource a(Uri uri) {
            if (this.b == null) {
                this.b = new DefaultExtractorsFactory();
            }
            return new ExtractorMediaSource(uri, this.f3413a, this.b, this.e, this.c, this.f, this.d);
        }
    }

    private void b(long j2, boolean z) {
        this.m = j2;
        this.n = z;
        a((Timeline) new SinglePeriodTimeline(this.m, this.n, false, this.l), (Object) null);
    }

    public void a(ExoPlayer exoPlayer, boolean z, TransferListener transferListener) {
        this.o = transferListener;
        b(this.m, false);
    }

    public void b() throws IOException {
    }

    public void h() {
    }

    @Deprecated
    public ExtractorMediaSource(Uri uri, DataSource.Factory factory, ExtractorsFactory extractorsFactory, Handler handler, EventListener eventListener) {
        this(uri, factory, extractorsFactory, handler, eventListener, (String) null);
    }

    @Deprecated
    public ExtractorMediaSource(Uri uri, DataSource.Factory factory, ExtractorsFactory extractorsFactory, Handler handler, EventListener eventListener, String str) {
        this(uri, factory, extractorsFactory, handler, eventListener, str, 1048576);
    }

    public MediaPeriod a(MediaSource.MediaPeriodId mediaPeriodId, Allocator allocator) {
        DataSource a2 = this.g.a();
        TransferListener transferListener = this.o;
        if (transferListener != null) {
            a2.a(transferListener);
        }
        return new ExtractorMediaPeriod(this.f, a2, this.h.a(), this.i, a(mediaPeriodId), this, allocator, this.j, this.k);
    }

    @Deprecated
    public ExtractorMediaSource(Uri uri, DataSource.Factory factory, ExtractorsFactory extractorsFactory, Handler handler, EventListener eventListener, String str, int i2) {
        this(uri, factory, extractorsFactory, (LoadErrorHandlingPolicy) new DefaultLoadErrorHandlingPolicy(), str, i2, (Object) null);
        if (eventListener != null && handler != null) {
            a(handler, (MediaSourceEventListener) new EventListenerWrapper(eventListener));
        }
    }

    private ExtractorMediaSource(Uri uri, DataSource.Factory factory, ExtractorsFactory extractorsFactory, LoadErrorHandlingPolicy loadErrorHandlingPolicy, String str, int i2, Object obj) {
        this.f = uri;
        this.g = factory;
        this.h = extractorsFactory;
        this.i = loadErrorHandlingPolicy;
        this.j = str;
        this.k = i2;
        this.m = -9223372036854775807L;
        this.l = obj;
    }

    public void a(MediaPeriod mediaPeriod) {
        ((ExtractorMediaPeriod) mediaPeriod).j();
    }

    public void a(long j2, boolean z) {
        if (j2 == -9223372036854775807L) {
            j2 = this.m;
        }
        if (this.m != j2 || this.n != z) {
            b(j2, z);
        }
    }
}
