package com.google.android.exoplayer2.source.hls.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.offline.SegmentDownloader;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsMediaPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylist;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistParser;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.util.UriUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public final class HlsDownloader extends SegmentDownloader<HlsPlaylist> {
    public HlsDownloader(Uri uri, List<StreamKey> list, DownloaderConstructorHelper downloaderConstructorHelper) {
        super(uri, list, downloaderConstructorHelper);
    }

    private static HlsPlaylist b(DataSource dataSource, Uri uri) throws IOException {
        return (HlsPlaylist) ParsingLoadable.a(dataSource, new HlsPlaylistParser(), uri, 4);
    }

    /* access modifiers changed from: protected */
    public HlsPlaylist a(DataSource dataSource, Uri uri) throws IOException {
        return b(dataSource, uri);
    }

    /* access modifiers changed from: protected */
    public List<SegmentDownloader.Segment> a(DataSource dataSource, HlsPlaylist hlsPlaylist, boolean z) throws IOException {
        ArrayList arrayList = new ArrayList();
        if (hlsPlaylist instanceof HlsMasterPlaylist) {
            HlsMasterPlaylist hlsMasterPlaylist = (HlsMasterPlaylist) hlsPlaylist;
            a(hlsMasterPlaylist.f3495a, hlsMasterPlaylist.d, (List<Uri>) arrayList);
            a(hlsMasterPlaylist.f3495a, hlsMasterPlaylist.e, (List<Uri>) arrayList);
            a(hlsMasterPlaylist.f3495a, hlsMasterPlaylist.f, (List<Uri>) arrayList);
        } else {
            arrayList.add(Uri.parse(hlsPlaylist.f3495a));
        }
        ArrayList arrayList2 = new ArrayList();
        HashSet hashSet = new HashSet();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            Uri uri = (Uri) it2.next();
            try {
                HlsMediaPlaylist hlsMediaPlaylist = (HlsMediaPlaylist) b(dataSource, uri);
                arrayList2.add(new SegmentDownloader.Segment(hlsMediaPlaylist.f, new DataSpec(uri)));
                HlsMediaPlaylist.Segment segment = null;
                List<HlsMediaPlaylist.Segment> list = hlsMediaPlaylist.o;
                for (int i = 0; i < list.size(); i++) {
                    HlsMediaPlaylist.Segment segment2 = list.get(i);
                    HlsMediaPlaylist.Segment segment3 = segment2.b;
                    if (!(segment3 == null || segment3 == segment)) {
                        a(arrayList2, hlsMediaPlaylist, segment3, hashSet);
                        segment = segment3;
                    }
                    a(arrayList2, hlsMediaPlaylist, segment2, hashSet);
                }
            } catch (IOException e) {
                if (z) {
                    arrayList2.add(new SegmentDownloader.Segment(0, new DataSpec(uri)));
                } else {
                    throw e;
                }
            }
        }
        return arrayList2;
    }

    private static void a(ArrayList<SegmentDownloader.Segment> arrayList, HlsMediaPlaylist hlsMediaPlaylist, HlsMediaPlaylist.Segment segment, HashSet<Uri> hashSet) {
        long j = hlsMediaPlaylist.f + segment.e;
        String str = segment.g;
        if (str != null) {
            Uri b = UriUtil.b(hlsMediaPlaylist.f3495a, str);
            if (hashSet.add(b)) {
                arrayList.add(new SegmentDownloader.Segment(j, new DataSpec(b)));
            }
        }
        arrayList.add(new SegmentDownloader.Segment(j, new DataSpec(UriUtil.b(hlsMediaPlaylist.f3495a, segment.f3494a), segment.i, segment.j, (String) null)));
    }

    private static void a(String str, List<HlsMasterPlaylist.HlsUrl> list, List<Uri> list2) {
        for (int i = 0; i < list.size(); i++) {
            list2.add(UriUtil.b(str, list.get(i).f3493a));
        }
    }
}
