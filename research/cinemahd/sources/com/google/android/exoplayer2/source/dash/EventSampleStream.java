package com.google.android.exoplayer2.source.dash;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.metadata.emsg.EventMessageEncoder;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.dash.manifest.EventStream;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

final class EventSampleStream implements SampleStream {

    /* renamed from: a  reason: collision with root package name */
    private final Format f3452a;
    private final EventMessageEncoder b = new EventMessageEncoder();
    private long[] c;
    private boolean d;
    private EventStream e;
    private boolean f;
    private int g;
    private long h = -9223372036854775807L;

    public EventSampleStream(EventStream eventStream, Format format, boolean z) {
        this.f3452a = format;
        this.e = eventStream;
        this.c = eventStream.b;
        a(eventStream, z);
    }

    public void a() throws IOException {
    }

    public void a(EventStream eventStream, boolean z) {
        int i = this.g;
        long j = i == 0 ? -9223372036854775807L : this.c[i - 1];
        this.d = z;
        this.e = eventStream;
        this.c = eventStream.b;
        long j2 = this.h;
        if (j2 != -9223372036854775807L) {
            a(j2);
        } else if (j != -9223372036854775807L) {
            this.g = Util.a(this.c, j, false, false);
        }
    }

    public String b() {
        return this.e.a();
    }

    public int d(long j) {
        int max = Math.max(this.g, Util.a(this.c, j, true, false));
        int i = max - this.g;
        this.g = max;
        return i;
    }

    public boolean isReady() {
        return true;
    }

    public void a(long j) {
        boolean z = false;
        this.g = Util.a(this.c, j, true, false);
        if (this.d && this.g == this.c.length) {
            z = true;
        }
        if (!z) {
            j = -9223372036854775807L;
        }
        this.h = j;
    }

    public int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
        if (z || !this.f) {
            formatHolder.f3165a = this.f3452a;
            this.f = true;
            return -5;
        }
        int i = this.g;
        if (i != this.c.length) {
            this.g = i + 1;
            EventMessageEncoder eventMessageEncoder = this.b;
            EventStream eventStream = this.e;
            byte[] a2 = eventMessageEncoder.a(eventStream.f3463a[i], eventStream.e);
            if (a2 == null) {
                return -3;
            }
            decoderInputBuffer.b(a2.length);
            decoderInputBuffer.setFlags(1);
            decoderInputBuffer.b.put(a2);
            decoderInputBuffer.c = this.c[i];
            return -4;
        } else if (this.d) {
            return -3;
        } else {
            decoderInputBuffer.setFlags(4);
            return -4;
        }
    }
}
