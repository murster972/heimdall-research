package com.google.android.exoplayer2.source.chunk;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.SampleQueue;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.SequenceableLoader;
import com.google.android.exoplayer2.source.chunk.ChunkSource;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChunkSampleStream<T extends ChunkSource> implements SampleStream, SequenceableLoader, Loader.Callback<Chunk>, Loader.ReleaseCallback {

    /* renamed from: a  reason: collision with root package name */
    public final int f3435a;
    /* access modifiers changed from: private */
    public final int[] b;
    /* access modifiers changed from: private */
    public final Format[] c;
    /* access modifiers changed from: private */
    public final boolean[] d;
    private final T e;
    private final SequenceableLoader.Callback<ChunkSampleStream<T>> f;
    /* access modifiers changed from: private */
    public final MediaSourceEventListener.EventDispatcher g;
    private final LoadErrorHandlingPolicy h;
    private final Loader i = new Loader("Loader:ChunkSampleStream");
    private final ChunkHolder j = new ChunkHolder();
    private final ArrayList<BaseMediaChunk> k = new ArrayList<>();
    private final List<BaseMediaChunk> l = Collections.unmodifiableList(this.k);
    private final SampleQueue m;
    private final SampleQueue[] n;
    private final BaseMediaChunkOutput o;
    private Format p;
    private ReleaseCallback<T> q;
    private long r;
    /* access modifiers changed from: private */
    public long s;
    private int t;
    long u;
    boolean v;

    public final class EmbeddedSampleStream implements SampleStream {

        /* renamed from: a  reason: collision with root package name */
        public final ChunkSampleStream<T> f3436a;
        private final SampleQueue b;
        private final int c;
        private boolean d;

        public EmbeddedSampleStream(ChunkSampleStream<T> chunkSampleStream, SampleQueue sampleQueue, int i) {
            this.f3436a = chunkSampleStream;
            this.b = sampleQueue;
            this.c = i;
        }

        private void c() {
            if (!this.d) {
                ChunkSampleStream.this.g.a(ChunkSampleStream.this.b[this.c], ChunkSampleStream.this.c[this.c], 0, (Object) null, ChunkSampleStream.this.s);
                this.d = true;
            }
        }

        public int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
            if (ChunkSampleStream.this.i()) {
                return -3;
            }
            c();
            SampleQueue sampleQueue = this.b;
            ChunkSampleStream chunkSampleStream = ChunkSampleStream.this;
            return sampleQueue.a(formatHolder, decoderInputBuffer, z, chunkSampleStream.v, chunkSampleStream.u);
        }

        public void a() throws IOException {
        }

        public void b() {
            Assertions.b(ChunkSampleStream.this.d[this.c]);
            ChunkSampleStream.this.d[this.c] = false;
        }

        public int d(long j) {
            if (ChunkSampleStream.this.i()) {
                return 0;
            }
            c();
            if (ChunkSampleStream.this.v && j > this.b.f()) {
                return this.b.a();
            }
            int a2 = this.b.a(j, true, true);
            if (a2 == -1) {
                return 0;
            }
            return a2;
        }

        public boolean isReady() {
            ChunkSampleStream chunkSampleStream = ChunkSampleStream.this;
            return chunkSampleStream.v || (!chunkSampleStream.i() && this.b.j());
        }
    }

    public interface ReleaseCallback<T extends ChunkSource> {
        void a(ChunkSampleStream<T> chunkSampleStream);
    }

    public ChunkSampleStream(int i2, int[] iArr, Format[] formatArr, T t2, SequenceableLoader.Callback<ChunkSampleStream<T>> callback, Allocator allocator, long j2, LoadErrorHandlingPolicy loadErrorHandlingPolicy, MediaSourceEventListener.EventDispatcher eventDispatcher) {
        int i3;
        this.f3435a = i2;
        this.b = iArr;
        this.c = formatArr;
        this.e = t2;
        this.f = callback;
        this.g = eventDispatcher;
        this.h = loadErrorHandlingPolicy;
        int i4 = 0;
        if (iArr == null) {
            i3 = 0;
        } else {
            i3 = iArr.length;
        }
        this.n = new SampleQueue[i3];
        this.d = new boolean[i3];
        int i5 = i3 + 1;
        int[] iArr2 = new int[i5];
        SampleQueue[] sampleQueueArr = new SampleQueue[i5];
        this.m = new SampleQueue(allocator);
        iArr2[0] = i2;
        sampleQueueArr[0] = this.m;
        while (i4 < i3) {
            SampleQueue sampleQueue = new SampleQueue(allocator);
            this.n[i4] = sampleQueue;
            int i6 = i4 + 1;
            sampleQueueArr[i6] = sampleQueue;
            iArr2[i6] = iArr[i4];
            i4 = i6;
        }
        this.o = new BaseMediaChunkOutput(iArr2, sampleQueueArr);
        this.r = j2;
        this.s = j2;
    }

    private BaseMediaChunk k() {
        ArrayList<BaseMediaChunk> arrayList = this.k;
        return arrayList.get(arrayList.size() - 1);
    }

    private void l() {
        int a2 = a(this.m.g(), this.t - 1);
        while (true) {
            int i2 = this.t;
            if (i2 <= a2) {
                this.t = i2 + 1;
                d(i2);
            } else {
                return;
            }
        }
    }

    public long f() {
        if (this.v) {
            return Long.MIN_VALUE;
        }
        if (i()) {
            return this.r;
        }
        long j2 = this.s;
        BaseMediaChunk k2 = k();
        if (!k2.f()) {
            if (this.k.size() > 1) {
                ArrayList<BaseMediaChunk> arrayList = this.k;
                k2 = arrayList.get(arrayList.size() - 2);
            } else {
                k2 = null;
            }
        }
        if (k2 != null) {
            j2 = Math.max(j2, k2.g);
        }
        return Math.max(j2, this.m.f());
    }

    public void g() {
        this.m.l();
        for (SampleQueue l2 : this.n) {
            l2.l();
        }
        ReleaseCallback<T> releaseCallback = this.q;
        if (releaseCallback != null) {
            releaseCallback.a(this);
        }
    }

    public T h() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.r != -9223372036854775807L;
    }

    public boolean isReady() {
        return this.v || (!i() && this.m.j());
    }

    public void j() {
        a((ReleaseCallback) null);
    }

    public boolean b(long j2) {
        long j3;
        List<BaseMediaChunk> list;
        long j4;
        boolean z = false;
        if (this.v || this.i.c()) {
            return false;
        }
        boolean i2 = i();
        if (i2) {
            list = Collections.emptyList();
            j3 = this.r;
        } else {
            list = this.l;
            j3 = k().g;
        }
        T t2 = this.e;
        long j5 = j2;
        t2.a(j5, j3, (List<? extends MediaChunk>) list, this.j);
        ChunkHolder chunkHolder = this.j;
        boolean z2 = chunkHolder.b;
        Chunk chunk = chunkHolder.f3434a;
        chunkHolder.a();
        if (z2) {
            this.r = -9223372036854775807L;
            this.v = true;
            return true;
        } else if (chunk == null) {
            return false;
        } else {
            if (a(chunk)) {
                BaseMediaChunk baseMediaChunk = (BaseMediaChunk) chunk;
                if (i2) {
                    if (baseMediaChunk.f == this.r) {
                        z = true;
                    }
                    if (z) {
                        j4 = 0;
                    } else {
                        j4 = this.r;
                    }
                    this.u = j4;
                    this.r = -9223372036854775807L;
                }
                baseMediaChunk.a(this.o);
                this.k.add(baseMediaChunk);
            }
            this.g.a(chunk.f3431a, chunk.b, this.f3435a, chunk.c, chunk.d, chunk.e, chunk.f, chunk.g, this.i.a(chunk, this, this.h.a(chunk.b)));
            return true;
        }
    }

    public void c(long j2) {
        int size;
        int a2;
        if (!this.i.c() && !i() && (size = this.k.size()) > (a2 = this.e.a(j2, (List<? extends MediaChunk>) this.l))) {
            while (true) {
                if (a2 >= size) {
                    a2 = size;
                    break;
                } else if (!c(a2)) {
                    break;
                } else {
                    a2++;
                }
            }
            if (a2 != size) {
                long j3 = k().g;
                BaseMediaChunk b2 = b(a2);
                if (this.k.isEmpty()) {
                    this.r = this.s;
                }
                this.v = false;
                this.g.a(this.f3435a, b2.f, j3);
            }
        }
    }

    public int d(long j2) {
        int i2 = 0;
        if (i()) {
            return 0;
        }
        if (!this.v || j2 <= this.m.f()) {
            int a2 = this.m.a(j2, true, true);
            if (a2 != -1) {
                i2 = a2;
            }
        } else {
            i2 = this.m.a();
        }
        l();
        return i2;
    }

    public void a(long j2, boolean z) {
        if (!i()) {
            int d2 = this.m.d();
            this.m.b(j2, z, true);
            int d3 = this.m.d();
            if (d3 > d2) {
                long e2 = this.m.e();
                int i2 = 0;
                while (true) {
                    SampleQueue[] sampleQueueArr = this.n;
                    if (i2 >= sampleQueueArr.length) {
                        break;
                    }
                    sampleQueueArr[i2].b(e2, z, this.d[i2]);
                    i2++;
                }
            }
            a(d3);
        }
    }

    private void d(int i2) {
        BaseMediaChunk baseMediaChunk = this.k.get(i2);
        Format format = baseMediaChunk.c;
        if (!format.equals(this.p)) {
            this.g.a(this.f3435a, format, baseMediaChunk.d, baseMediaChunk.e, baseMediaChunk.f);
        }
        this.p = format;
    }

    private boolean c(int i2) {
        int g2;
        BaseMediaChunk baseMediaChunk = this.k.get(i2);
        if (this.m.g() > baseMediaChunk.a(0)) {
            return true;
        }
        int i3 = 0;
        do {
            SampleQueue[] sampleQueueArr = this.n;
            if (i3 >= sampleQueueArr.length) {
                return false;
            }
            g2 = sampleQueueArr[i3].g();
            i3++;
        } while (g2 <= baseMediaChunk.a(i3));
        return true;
    }

    public ChunkSampleStream<T>.EmbeddedSampleStream a(long j2, int i2) {
        for (int i3 = 0; i3 < this.n.length; i3++) {
            if (this.b[i3] == i2) {
                Assertions.b(!this.d[i3]);
                this.d[i3] = true;
                this.n[i3].m();
                this.n[i3].a(j2, true, true);
                return new EmbeddedSampleStream(this, this.n[i3], i3);
            }
        }
        throw new IllegalStateException();
    }

    public long a(long j2, SeekParameters seekParameters) {
        return this.e.a(j2, seekParameters);
    }

    public void a(long j2) {
        boolean z;
        this.s = j2;
        if (i()) {
            this.r = j2;
            return;
        }
        BaseMediaChunk baseMediaChunk = null;
        int i2 = 0;
        while (true) {
            if (i2 >= this.k.size()) {
                break;
            }
            BaseMediaChunk baseMediaChunk2 = this.k.get(i2);
            int i3 = (baseMediaChunk2.f > j2 ? 1 : (baseMediaChunk2.f == j2 ? 0 : -1));
            if (i3 == 0 && baseMediaChunk2.j == -9223372036854775807L) {
                baseMediaChunk = baseMediaChunk2;
                break;
            } else if (i3 > 0) {
                break;
            } else {
                i2++;
            }
        }
        this.m.m();
        if (baseMediaChunk != null) {
            z = this.m.b(baseMediaChunk.a(0));
            this.u = 0;
        } else {
            z = this.m.a(j2, true, (j2 > b() ? 1 : (j2 == b() ? 0 : -1)) < 0) != -1;
            this.u = this.s;
        }
        if (z) {
            this.t = a(this.m.g(), 0);
            for (SampleQueue sampleQueue : this.n) {
                sampleQueue.m();
                sampleQueue.a(j2, true, false);
            }
            return;
        }
        this.r = j2;
        this.v = false;
        this.k.clear();
        this.t = 0;
        if (this.i.c()) {
            this.i.b();
            return;
        }
        this.m.l();
        for (SampleQueue l2 : this.n) {
            l2.l();
        }
    }

    public long b() {
        if (i()) {
            return this.r;
        }
        if (this.v) {
            return Long.MIN_VALUE;
        }
        return k().g;
    }

    private BaseMediaChunk b(int i2) {
        BaseMediaChunk baseMediaChunk = this.k.get(i2);
        ArrayList<BaseMediaChunk> arrayList = this.k;
        Util.a(arrayList, i2, arrayList.size());
        this.t = Math.max(this.t, this.k.size());
        int i3 = 0;
        this.m.a(baseMediaChunk.a(0));
        while (true) {
            SampleQueue[] sampleQueueArr = this.n;
            if (i3 >= sampleQueueArr.length) {
                return baseMediaChunk;
            }
            SampleQueue sampleQueue = sampleQueueArr[i3];
            i3++;
            sampleQueue.a(baseMediaChunk.a(i3));
        }
    }

    public void a(ReleaseCallback<T> releaseCallback) {
        this.q = releaseCallback;
        this.m.b();
        for (SampleQueue b2 : this.n) {
            b2.b();
        }
        this.i.a((Loader.ReleaseCallback) this);
    }

    public void a() throws IOException {
        this.i.a();
        if (!this.i.c()) {
            this.e.a();
        }
    }

    public int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
        if (i()) {
            return -3;
        }
        l();
        return this.m.a(formatHolder, decoderInputBuffer, z, this.v, this.u);
    }

    public void a(Chunk chunk, long j2, long j3) {
        Chunk chunk2 = chunk;
        this.e.a(chunk2);
        this.g.b(chunk2.f3431a, chunk.d(), chunk.c(), chunk2.b, this.f3435a, chunk2.c, chunk2.d, chunk2.e, chunk2.f, chunk2.g, j2, j3, chunk.a());
        this.f.a(this);
    }

    public void a(Chunk chunk, long j2, long j3, boolean z) {
        Chunk chunk2 = chunk;
        this.g.a(chunk2.f3431a, chunk.d(), chunk.c(), chunk2.b, this.f3435a, chunk2.c, chunk2.d, chunk2.e, chunk2.f, chunk2.g, j2, j3, chunk.a());
        if (!z) {
            this.m.l();
            for (SampleQueue l2 : this.n) {
                l2.l();
            }
            this.f.a(this);
        }
    }

    public Loader.LoadErrorAction a(Chunk chunk, long j2, long j3, IOException iOException, int i2) {
        Chunk chunk2 = chunk;
        long a2 = chunk.a();
        boolean a3 = a(chunk);
        int size = this.k.size() - 1;
        boolean z = a2 == 0 || !a3 || !c(size);
        Loader.LoadErrorAction loadErrorAction = null;
        if (this.e.a(chunk, z, (Exception) iOException, z ? this.h.a(chunk2.b, j3, iOException, i2) : -9223372036854775807L)) {
            if (z) {
                loadErrorAction = Loader.e;
                if (a3) {
                    Assertions.b(b(size) == chunk2);
                    if (this.k.isEmpty()) {
                        this.r = this.s;
                    }
                }
            } else {
                Log.d("ChunkSampleStream", "Ignoring attempt to cancel non-cancelable load.");
            }
        }
        if (loadErrorAction == null) {
            long b2 = this.h.b(chunk2.b, j3, iOException, i2);
            loadErrorAction = b2 != -9223372036854775807L ? Loader.a(false, b2) : Loader.f;
        }
        Loader.LoadErrorAction loadErrorAction2 = loadErrorAction;
        boolean z2 = !loadErrorAction2.a();
        this.g.a(chunk2.f3431a, chunk.d(), chunk.c(), chunk2.b, this.f3435a, chunk2.c, chunk2.d, chunk2.e, chunk2.f, chunk2.g, j2, j3, a2, iOException, z2);
        if (z2) {
            this.f.a(this);
        }
        return loadErrorAction2;
    }

    private boolean a(Chunk chunk) {
        return chunk instanceof BaseMediaChunk;
    }

    private void a(int i2) {
        int min = Math.min(a(i2, 0), this.t);
        if (min > 0) {
            Util.a(this.k, 0, min);
            this.t -= min;
        }
    }

    private int a(int i2, int i3) {
        do {
            i3++;
            if (i3 >= this.k.size()) {
                return this.k.size() - 1;
            }
        } while (this.k.get(i3).a(0) <= i2);
        return i3 - 1;
    }
}
