package com.google.android.exoplayer2.source.smoothstreaming.manifest;

import android.net.Uri;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.offline.FilterableManifest;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.UriUtil;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class SsManifest implements FilterableManifest<SsManifest> {

    /* renamed from: a  reason: collision with root package name */
    public final int f3508a;
    public final int b;
    public final int c;
    public final boolean d;
    public final ProtectionElement e;
    public final StreamElement[] f;
    public final long g;
    public final long h;

    public static class ProtectionElement {

        /* renamed from: a  reason: collision with root package name */
        public final UUID f3509a;
        public final byte[] b;

        public ProtectionElement(UUID uuid, byte[] bArr) {
            this.f3509a = uuid;
            this.b = bArr;
        }
    }

    public static class StreamElement {

        /* renamed from: a  reason: collision with root package name */
        public final int f3510a;
        public final String b;
        public final long c;
        public final String d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final String i;
        public final Format[] j;
        public final int k;
        private final String l;
        private final String m;
        private final List<Long> n;
        private final long[] o;
        private final long p;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public StreamElement(String str, String str2, int i2, String str3, long j2, String str4, int i3, int i4, int i5, int i6, String str5, Format[] formatArr, List<Long> list, long j3) {
            this(str, str2, i2, str3, j2, str4, i3, i4, i5, i6, str5, formatArr, list, Util.a(list, 1000000, j2), Util.c(j3, 1000000, j2));
            String str6 = str;
            String str7 = str2;
            int i7 = i2;
        }

        public StreamElement a(Format[] formatArr) {
            String str = this.l;
            return new StreamElement(str, this.m, this.f3510a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, formatArr, this.n, this.o, this.p);
        }

        public long b(int i2) {
            return this.o[i2];
        }

        public int a(long j2) {
            return Util.b(this.o, j2, true, true);
        }

        public long a(int i2) {
            if (i2 == this.k - 1) {
                return this.p;
            }
            long[] jArr = this.o;
            return jArr[i2 + 1] - jArr[i2];
        }

        private StreamElement(String str, String str2, int i2, String str3, long j2, String str4, int i3, int i4, int i5, int i6, String str5, Format[] formatArr, List<Long> list, long[] jArr, long j3) {
            this.l = str;
            this.m = str2;
            this.f3510a = i2;
            this.b = str3;
            this.c = j2;
            this.d = str4;
            this.e = i3;
            this.f = i4;
            this.g = i5;
            this.h = i6;
            this.i = str5;
            this.j = formatArr;
            this.n = list;
            this.o = jArr;
            this.p = j3;
            this.k = list.size();
        }

        public Uri a(int i2, int i3) {
            boolean z = true;
            Assertions.b(this.j != null);
            Assertions.b(this.n != null);
            if (i3 >= this.n.size()) {
                z = false;
            }
            Assertions.b(z);
            String num = Integer.toString(this.j[i2].c);
            String l2 = this.n.get(i3).toString();
            return UriUtil.b(this.l, this.m.replace("{bitrate}", num).replace("{Bitrate}", num).replace("{start time}", l2).replace("{start_time}", l2));
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SsManifest(int i, int i2, long j, long j2, long j3, int i3, boolean z, ProtectionElement protectionElement, StreamElement[] streamElementArr) {
        this(i, i2, j2 == 0 ? -9223372036854775807L : Util.c(j2, 1000000, j), j3 != 0 ? Util.c(j3, 1000000, j) : -9223372036854775807L, i3, z, protectionElement, streamElementArr);
    }

    public final SsManifest a(List<StreamKey> list) {
        ArrayList arrayList = new ArrayList(list);
        Collections.sort(arrayList);
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        StreamElement streamElement = null;
        int i = 0;
        while (i < arrayList.size()) {
            StreamKey streamKey = (StreamKey) arrayList.get(i);
            StreamElement streamElement2 = this.f[streamKey.b];
            if (!(streamElement2 == streamElement || streamElement == null)) {
                arrayList2.add(streamElement.a((Format[]) arrayList3.toArray(new Format[0])));
                arrayList3.clear();
            }
            arrayList3.add(streamElement2.j[streamKey.c]);
            i++;
            streamElement = streamElement2;
        }
        if (streamElement != null) {
            arrayList2.add(streamElement.a((Format[]) arrayList3.toArray(new Format[0])));
        }
        return new SsManifest(this.f3508a, this.b, this.g, this.h, this.c, this.d, this.e, (StreamElement[]) arrayList2.toArray(new StreamElement[0]));
    }

    private SsManifest(int i, int i2, long j, long j2, int i3, boolean z, ProtectionElement protectionElement, StreamElement[] streamElementArr) {
        this.f3508a = i;
        this.b = i2;
        this.g = j;
        this.h = j2;
        this.c = i3;
        this.d = z;
        this.e = protectionElement;
        this.f = streamElementArr;
    }
}
