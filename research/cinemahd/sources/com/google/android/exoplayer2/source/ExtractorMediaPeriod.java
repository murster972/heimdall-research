package com.google.android.exoplayer2.source;

import android.net.Uri;
import android.os.Handler;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.extractor.DefaultExtractorInput;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.SampleQueue;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.upstream.StatsDataSource;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ConditionVariable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

final class ExtractorMediaPeriod implements MediaPeriod, ExtractorOutput, Loader.Callback<ExtractingLoadable>, Loader.ReleaseCallback, SampleQueue.UpstreamFormatChangedListener {
    private int A;
    private long B;
    private long C;
    private long D;
    private long E;
    private boolean F;
    private int G;
    private boolean H;
    private boolean I;

    /* renamed from: a  reason: collision with root package name */
    private final Uri f3407a;
    private final DataSource b;
    private final LoadErrorHandlingPolicy c;
    private final MediaSourceEventListener.EventDispatcher d;
    private final Listener e;
    private final Allocator f;
    /* access modifiers changed from: private */
    public final String g;
    /* access modifiers changed from: private */
    public final long h;
    private final Loader i = new Loader("Loader:ExtractorMediaPeriod");
    private final ExtractorHolder j;
    private final ConditionVariable k;
    private final Runnable l;
    /* access modifiers changed from: private */
    public final Runnable m;
    /* access modifiers changed from: private */
    public final Handler n;
    private MediaPeriod.Callback o;
    private SeekMap p;
    private SampleQueue[] q;
    private int[] r;
    private boolean s;
    private boolean t;
    private PreparedState u;
    private boolean v;
    private int w;
    private boolean x;
    private boolean y;
    private boolean z;

    interface Listener {
        void a(long j, boolean z);
    }

    private static final class PreparedState {

        /* renamed from: a  reason: collision with root package name */
        public final SeekMap f3410a;
        public final TrackGroupArray b;
        public final boolean[] c;
        public final boolean[] d;
        public final boolean[] e;

        public PreparedState(SeekMap seekMap, TrackGroupArray trackGroupArray, boolean[] zArr) {
            this.f3410a = seekMap;
            this.b = trackGroupArray;
            this.c = zArr;
            int i = trackGroupArray.f3424a;
            this.d = new boolean[i];
            this.e = new boolean[i];
        }
    }

    private final class SampleStreamImpl implements SampleStream {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final int f3411a;

        public SampleStreamImpl(int i) {
            this.f3411a = i;
        }

        public int d(long j) {
            return ExtractorMediaPeriod.this.a(this.f3411a, j);
        }

        public boolean isReady() {
            return ExtractorMediaPeriod.this.a(this.f3411a);
        }

        public void a() throws IOException {
            ExtractorMediaPeriod.this.i();
        }

        public int a(FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z) {
            return ExtractorMediaPeriod.this.a(this.f3411a, formatHolder, decoderInputBuffer, z);
        }
    }

    public ExtractorMediaPeriod(Uri uri, DataSource dataSource, Extractor[] extractorArr, LoadErrorHandlingPolicy loadErrorHandlingPolicy, MediaSourceEventListener.EventDispatcher eventDispatcher, Listener listener, Allocator allocator, String str, int i2) {
        this.f3407a = uri;
        this.b = dataSource;
        this.c = loadErrorHandlingPolicy;
        this.d = eventDispatcher;
        this.e = listener;
        this.f = allocator;
        this.g = str;
        this.h = (long) i2;
        this.j = new ExtractorHolder(extractorArr);
        this.k = new ConditionVariable();
        this.l = new c(this);
        this.m = new b(this);
        this.n = new Handler();
        this.r = new int[0];
        this.q = new SampleQueue[0];
        this.E = -9223372036854775807L;
        this.C = -1;
        this.B = -9223372036854775807L;
        this.w = 1;
        eventDispatcher.a();
    }

    private int k() {
        int i2 = 0;
        for (SampleQueue i3 : this.q) {
            i2 += i3.i();
        }
        return i2;
    }

    private long l() {
        long j2 = Long.MIN_VALUE;
        for (SampleQueue f2 : this.q) {
            j2 = Math.max(j2, f2.f());
        }
        return j2;
    }

    private PreparedState m() {
        PreparedState preparedState = this.u;
        Assertions.a(preparedState);
        return preparedState;
    }

    private boolean n() {
        return this.E != -9223372036854775807L;
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, boolean], vars: [r6v0 ?, r6v1 ?, r6v3 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:51)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    public void o() {
        /*
            r11 = this;
            com.google.android.exoplayer2.extractor.SeekMap r0 = r11.p
            boolean r1 = r11.I
            if (r1 != 0) goto L_0x00a5
            boolean r1 = r11.t
            if (r1 != 0) goto L_0x00a5
            boolean r1 = r11.s
            if (r1 == 0) goto L_0x00a5
            if (r0 != 0) goto L_0x0012
            goto L_0x00a5
        L_0x0012:
            com.google.android.exoplayer2.source.SampleQueue[] r1 = r11.q
            int r2 = r1.length
            r3 = 0
            r4 = 0
        L_0x0017:
            if (r4 >= r2) goto L_0x0025
            r5 = r1[r4]
            com.google.android.exoplayer2.Format r5 = r5.h()
            if (r5 != 0) goto L_0x0022
            return
        L_0x0022:
            int r4 = r4 + 1
            goto L_0x0017
        L_0x0025:
            com.google.android.exoplayer2.util.ConditionVariable r1 = r11.k
            r1.b()
            com.google.android.exoplayer2.source.SampleQueue[] r1 = r11.q
            int r1 = r1.length
            com.google.android.exoplayer2.source.TrackGroup[] r2 = new com.google.android.exoplayer2.source.TrackGroup[r1]
            boolean[] r4 = new boolean[r1]
            long r5 = r0.getDurationUs()
            r11.B = r5
            r5 = 0
        L_0x0038:
            r6 = 1
            if (r5 >= r1) goto L_0x0068
            com.google.android.exoplayer2.source.SampleQueue[] r7 = r11.q
            r7 = r7[r5]
            com.google.android.exoplayer2.Format r7 = r7.h()
            com.google.android.exoplayer2.source.TrackGroup r8 = new com.google.android.exoplayer2.source.TrackGroup
            com.google.android.exoplayer2.Format[] r9 = new com.google.android.exoplayer2.Format[r6]
            r9[r3] = r7
            r8.<init>((com.google.android.exoplayer2.Format[]) r9)
            r2[r5] = r8
            java.lang.String r7 = r7.g
            boolean r8 = com.google.android.exoplayer2.util.MimeTypes.l(r7)
            if (r8 != 0) goto L_0x005e
            boolean r7 = com.google.android.exoplayer2.util.MimeTypes.j(r7)
            if (r7 == 0) goto L_0x005d
            goto L_0x005e
        L_0x005d:
            r6 = 0
        L_0x005e:
            r4[r5] = r6
            boolean r7 = r11.v
            r6 = r6 | r7
            r11.v = r6
            int r5 = r5 + 1
            goto L_0x0038
        L_0x0068:
            long r7 = r11.C
            r9 = -1
            int r1 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r1 != 0) goto L_0x007f
            long r7 = r0.getDurationUs()
            r9 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r1 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r1 != 0) goto L_0x007f
            r1 = 7
            goto L_0x0080
        L_0x007f:
            r1 = 1
        L_0x0080:
            r11.w = r1
            com.google.android.exoplayer2.source.ExtractorMediaPeriod$PreparedState r1 = new com.google.android.exoplayer2.source.ExtractorMediaPeriod$PreparedState
            com.google.android.exoplayer2.source.TrackGroupArray r3 = new com.google.android.exoplayer2.source.TrackGroupArray
            r3.<init>((com.google.android.exoplayer2.source.TrackGroup[]) r2)
            r1.<init>(r0, r3, r4)
            r11.u = r1
            r11.t = r6
            com.google.android.exoplayer2.source.ExtractorMediaPeriod$Listener r1 = r11.e
            long r2 = r11.B
            boolean r0 = r0.b()
            r1.a(r2, r0)
            com.google.android.exoplayer2.source.MediaPeriod$Callback r0 = r11.o
            com.google.android.exoplayer2.util.Assertions.a(r0)
            com.google.android.exoplayer2.source.MediaPeriod$Callback r0 = (com.google.android.exoplayer2.source.MediaPeriod.Callback) r0
            r0.a(r11)
        L_0x00a5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.ExtractorMediaPeriod.o():void");
    }

    private void p() {
        ExtractingLoadable extractingLoadable = new ExtractingLoadable(this.f3407a, this.b, this.j, this, this.k);
        if (this.t) {
            SeekMap seekMap = m().f3410a;
            Assertions.b(n());
            long j2 = this.B;
            if (j2 == -9223372036854775807L || this.E < j2) {
                extractingLoadable.a(seekMap.b(this.E).f3255a.b, this.E);
                this.E = -9223372036854775807L;
            } else {
                this.H = true;
                this.E = -9223372036854775807L;
                return;
            }
        }
        this.G = k();
        this.d.a(extractingLoadable.j, 1, -1, (Format) null, 0, (Object) null, extractingLoadable.i, this.B, this.i.a(extractingLoadable, this, this.c.a(this.w)));
    }

    private boolean q() {
        return this.y || n();
    }

    public void c(long j2) {
    }

    public TrackGroupArray e() {
        return m().b;
    }

    public long f() {
        long j2;
        boolean[] zArr = m().c;
        if (this.H) {
            return Long.MIN_VALUE;
        }
        if (n()) {
            return this.E;
        }
        if (this.v) {
            j2 = Clock.MAX_TIME;
            int length = this.q.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (zArr[i2]) {
                    j2 = Math.min(j2, this.q[i2].f());
                }
            }
        } else {
            j2 = l();
        }
        return j2 == Long.MIN_VALUE ? this.D : j2;
    }

    public void g() {
        for (SampleQueue l2 : this.q) {
            l2.l();
        }
        this.j.a();
    }

    public /* synthetic */ void h() {
        if (!this.I) {
            MediaPeriod.Callback callback = this.o;
            Assertions.a(callback);
            callback.a(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() throws IOException {
        this.i.a(this.c.a(this.w));
    }

    public void j() {
        if (this.t) {
            for (SampleQueue b2 : this.q) {
                b2.b();
            }
        }
        this.i.a((Loader.ReleaseCallback) this);
        this.n.removeCallbacksAndMessages((Object) null);
        this.o = null;
        this.I = true;
        this.d.b();
    }

    final class ExtractingLoadable implements Loader.Loadable {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f3408a;
        /* access modifiers changed from: private */
        public final StatsDataSource b;
        private final ExtractorHolder c;
        private final ExtractorOutput d;
        private final ConditionVariable e;
        private final PositionHolder f = new PositionHolder();
        private volatile boolean g;
        private boolean h = true;
        /* access modifiers changed from: private */
        public long i;
        /* access modifiers changed from: private */
        public DataSpec j;
        /* access modifiers changed from: private */
        public long k = -1;

        public ExtractingLoadable(Uri uri, DataSource dataSource, ExtractorHolder extractorHolder, ExtractorOutput extractorOutput, ConditionVariable conditionVariable) {
            this.f3408a = uri;
            this.b = new StatsDataSource(dataSource);
            this.c = extractorHolder;
            this.d = extractorOutput;
            this.e = conditionVariable;
            this.j = new DataSpec(uri, this.f.f3254a, -1, ExtractorMediaPeriod.this.g);
        }

        public void cancelLoad() {
            this.g = true;
        }

        public void load() throws IOException, InterruptedException {
            int i2 = 0;
            while (i2 == 0 && !this.g) {
                DefaultExtractorInput defaultExtractorInput = null;
                try {
                    long j2 = this.f.f3254a;
                    this.j = new DataSpec(this.f3408a, j2, -1, ExtractorMediaPeriod.this.g);
                    this.k = this.b.a(this.j);
                    if (this.k != -1) {
                        this.k += j2;
                    }
                    Uri uri = this.b.getUri();
                    Assertions.a(uri);
                    Uri uri2 = uri;
                    DefaultExtractorInput defaultExtractorInput2 = new DefaultExtractorInput(this.b, j2, this.k);
                    try {
                        Extractor a2 = this.c.a(defaultExtractorInput2, this.d, uri2);
                        if (this.h) {
                            a2.a(j2, this.i);
                            this.h = false;
                        }
                        while (i2 == 0 && !this.g) {
                            this.e.a();
                            i2 = a2.a((ExtractorInput) defaultExtractorInput2, this.f);
                            if (defaultExtractorInput2.getPosition() > ExtractorMediaPeriod.this.h + j2) {
                                j2 = defaultExtractorInput2.getPosition();
                                this.e.b();
                                ExtractorMediaPeriod.this.n.post(ExtractorMediaPeriod.this.m);
                            }
                        }
                        if (i2 == 1) {
                            i2 = 0;
                        } else {
                            this.f.f3254a = defaultExtractorInput2.getPosition();
                        }
                        Util.a((DataSource) this.b);
                    } catch (Throwable th) {
                        th = th;
                        defaultExtractorInput = defaultExtractorInput2;
                        this.f.f3254a = defaultExtractorInput.getPosition();
                        Util.a((DataSource) this.b);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (!(i2 == 1 || defaultExtractorInput == null)) {
                        this.f.f3254a = defaultExtractorInput.getPosition();
                    }
                    Util.a((DataSource) this.b);
                    throw th;
                }
            }
        }

        /* access modifiers changed from: private */
        public void a(long j2, long j3) {
            this.f.f3254a = j2;
            this.i = j3;
            this.h = true;
        }
    }

    public boolean b(long j2) {
        if (this.H || this.F) {
            return false;
        }
        if (this.t && this.A == 0) {
            return false;
        }
        boolean c2 = this.k.c();
        if (this.i.c()) {
            return c2;
        }
        p();
        return true;
    }

    public long c() {
        if (!this.z) {
            this.d.c();
            this.z = true;
        }
        if (!this.y) {
            return -9223372036854775807L;
        }
        if (!this.H && k() <= this.G) {
            return -9223372036854775807L;
        }
        this.y = false;
        return this.D;
    }

    public void d() throws IOException {
        i();
    }

    public void a(MediaPeriod.Callback callback, long j2) {
        this.o = callback;
        this.k.c();
        p();
    }

    public long b() {
        if (this.A == 0) {
            return Long.MIN_VALUE;
        }
        return f();
    }

    private void b(int i2) {
        PreparedState m2 = m();
        boolean[] zArr = m2.e;
        if (!zArr[i2]) {
            Format a2 = m2.b.a(i2).a(0);
            this.d.a(MimeTypes.f(a2.g), a2, 0, (Object) null, this.D);
            zArr[i2] = true;
        }
    }

    public long a(TrackSelection[] trackSelectionArr, boolean[] zArr, SampleStream[] sampleStreamArr, boolean[] zArr2, long j2) {
        PreparedState m2 = m();
        TrackGroupArray trackGroupArray = m2.b;
        boolean[] zArr3 = m2.d;
        int i2 = this.A;
        int i3 = 0;
        for (int i4 = 0; i4 < trackSelectionArr.length; i4++) {
            if (sampleStreamArr[i4] != null && (trackSelectionArr[i4] == null || !zArr[i4])) {
                int a2 = sampleStreamArr[i4].f3411a;
                Assertions.b(zArr3[a2]);
                this.A--;
                zArr3[a2] = false;
                sampleStreamArr[i4] = null;
            }
        }
        boolean z2 = !this.x ? j2 != 0 : i2 == 0;
        for (int i5 = 0; i5 < trackSelectionArr.length; i5++) {
            if (sampleStreamArr[i5] == null && trackSelectionArr[i5] != null) {
                TrackSelection trackSelection = trackSelectionArr[i5];
                Assertions.b(trackSelection.length() == 1);
                Assertions.b(trackSelection.b(0) == 0);
                int a3 = trackGroupArray.a(trackSelection.c());
                Assertions.b(!zArr3[a3]);
                this.A++;
                zArr3[a3] = true;
                sampleStreamArr[i5] = new SampleStreamImpl(a3);
                zArr2[i5] = true;
                if (!z2) {
                    SampleQueue sampleQueue = this.q[a3];
                    sampleQueue.m();
                    z2 = sampleQueue.a(j2, true, true) == -1 && sampleQueue.g() != 0;
                }
            }
        }
        if (this.A == 0) {
            this.F = false;
            this.y = false;
            if (this.i.c()) {
                SampleQueue[] sampleQueueArr = this.q;
                int length = sampleQueueArr.length;
                while (i3 < length) {
                    sampleQueueArr[i3].b();
                    i3++;
                }
                this.i.b();
            } else {
                SampleQueue[] sampleQueueArr2 = this.q;
                int length2 = sampleQueueArr2.length;
                while (i3 < length2) {
                    sampleQueueArr2[i3].l();
                    i3++;
                }
            }
        } else if (z2) {
            j2 = a(j2);
            while (i3 < sampleStreamArr.length) {
                if (sampleStreamArr[i3] != null) {
                    zArr2[i3] = true;
                }
                i3++;
            }
        }
        this.x = true;
        return j2;
    }

    private void c(int i2) {
        boolean[] zArr = m().c;
        if (this.F && zArr[i2] && !this.q[i2].j()) {
            this.E = 0;
            this.F = false;
            this.y = true;
            this.D = 0;
            this.G = 0;
            for (SampleQueue l2 : this.q) {
                l2.l();
            }
            MediaPeriod.Callback callback = this.o;
            Assertions.a(callback);
            callback.a(this);
        }
    }

    private static final class ExtractorHolder {

        /* renamed from: a  reason: collision with root package name */
        private final Extractor[] f3409a;
        private Extractor b;

        public ExtractorHolder(Extractor[] extractorArr) {
            this.f3409a = extractorArr;
        }

        public Extractor a(ExtractorInput extractorInput, ExtractorOutput extractorOutput, Uri uri) throws IOException, InterruptedException {
            Extractor extractor = this.b;
            if (extractor != null) {
                return extractor;
            }
            Extractor[] extractorArr = this.f3409a;
            int length = extractorArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Extractor extractor2 = extractorArr[i];
                try {
                    if (extractor2.a(extractorInput)) {
                        this.b = extractor2;
                        extractorInput.a();
                        break;
                    }
                    extractorInput.a();
                    i++;
                } catch (EOFException unused) {
                } catch (Throwable th) {
                    extractorInput.a();
                    throw th;
                }
            }
            Extractor extractor3 = this.b;
            if (extractor3 != null) {
                extractor3.a(extractorOutput);
                return this.b;
            }
            throw new UnrecognizedInputFormatException("None of the available extractors (" + Util.b((Object[]) this.f3409a) + ") could read the stream.", uri);
        }

        public void a() {
            Extractor extractor = this.b;
            if (extractor != null) {
                extractor.release();
                this.b = null;
            }
        }
    }

    public void a(long j2, boolean z2) {
        if (!n()) {
            boolean[] zArr = m().d;
            int length = this.q.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.q[i2].b(j2, z2, zArr[i2]);
            }
        }
    }

    public long a(long j2) {
        PreparedState m2 = m();
        SeekMap seekMap = m2.f3410a;
        boolean[] zArr = m2.c;
        if (!seekMap.b()) {
            j2 = 0;
        }
        this.y = false;
        this.D = j2;
        if (n()) {
            this.E = j2;
            return j2;
        } else if (this.w != 7 && a(zArr, j2)) {
            return j2;
        } else {
            this.F = false;
            this.E = j2;
            this.H = false;
            if (this.i.c()) {
                this.i.b();
            } else {
                for (SampleQueue l2 : this.q) {
                    l2.l();
                }
            }
            return j2;
        }
    }

    public long a(long j2, SeekParameters seekParameters) {
        SeekMap seekMap = m().f3410a;
        if (!seekMap.b()) {
            return 0;
        }
        SeekMap.SeekPoints b2 = seekMap.b(j2);
        return Util.a(j2, seekParameters, b2.f3255a.f3257a, b2.b.f3257a);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        return !q() && (this.H || this.q[i2].j());
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, FormatHolder formatHolder, DecoderInputBuffer decoderInputBuffer, boolean z2) {
        if (q()) {
            return -3;
        }
        b(i2);
        int a2 = this.q[i2].a(formatHolder, decoderInputBuffer, z2, this.H, this.D);
        if (a2 == -3) {
            c(i2);
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, long j2) {
        int i3 = 0;
        if (q()) {
            return 0;
        }
        b(i2);
        SampleQueue sampleQueue = this.q[i2];
        if (!this.H || j2 <= sampleQueue.f()) {
            int a2 = sampleQueue.a(j2, true, true);
            if (a2 != -1) {
                i3 = a2;
            }
        } else {
            i3 = sampleQueue.a();
        }
        if (i3 == 0) {
            c(i2);
        }
        return i3;
    }

    public void a(ExtractingLoadable extractingLoadable, long j2, long j3) {
        if (this.B == -9223372036854775807L) {
            SeekMap seekMap = this.p;
            Assertions.a(seekMap);
            SeekMap seekMap2 = seekMap;
            long l2 = l();
            this.B = l2 == Long.MIN_VALUE ? 0 : l2 + 10000;
            this.e.a(this.B, seekMap2.b());
        }
        this.d.b(extractingLoadable.j, extractingLoadable.b.c(), extractingLoadable.b.d(), 1, -1, (Format) null, 0, (Object) null, extractingLoadable.i, this.B, j2, j3, extractingLoadable.b.b());
        a(extractingLoadable);
        this.H = true;
        MediaPeriod.Callback callback = this.o;
        Assertions.a(callback);
        callback.a(this);
    }

    public void a(ExtractingLoadable extractingLoadable, long j2, long j3, boolean z2) {
        this.d.a(extractingLoadable.j, extractingLoadable.b.c(), extractingLoadable.b.d(), 1, -1, (Format) null, 0, (Object) null, extractingLoadable.i, this.B, j2, j3, extractingLoadable.b.b());
        if (!z2) {
            a(extractingLoadable);
            for (SampleQueue l2 : this.q) {
                l2.l();
            }
            if (this.A > 0) {
                MediaPeriod.Callback callback = this.o;
                Assertions.a(callback);
                callback.a(this);
            }
        }
    }

    public Loader.LoadErrorAction a(ExtractingLoadable extractingLoadable, long j2, long j3, IOException iOException, int i2) {
        Loader.LoadErrorAction loadErrorAction;
        boolean z2;
        ExtractingLoadable extractingLoadable2;
        a(extractingLoadable);
        long b2 = this.c.b(this.w, this.B, iOException, i2);
        if (b2 == -9223372036854775807L) {
            loadErrorAction = Loader.f;
            ExtractingLoadable extractingLoadable3 = extractingLoadable;
        } else {
            int k2 = k();
            if (k2 > this.G) {
                extractingLoadable2 = extractingLoadable;
                z2 = true;
            } else {
                extractingLoadable2 = extractingLoadable;
                z2 = false;
            }
            loadErrorAction = a(extractingLoadable2, k2) ? Loader.a(z2, b2) : Loader.e;
        }
        this.d.a(extractingLoadable.j, extractingLoadable.b.c(), extractingLoadable.b.d(), 1, -1, (Format) null, 0, (Object) null, extractingLoadable.i, this.B, j2, j3, extractingLoadable.b.b(), iOException, !loadErrorAction.a());
        return loadErrorAction;
    }

    public TrackOutput a(int i2, int i3) {
        int length = this.q.length;
        for (int i4 = 0; i4 < length; i4++) {
            if (this.r[i4] == i2) {
                return this.q[i4];
            }
        }
        SampleQueue sampleQueue = new SampleQueue(this.f);
        sampleQueue.a((SampleQueue.UpstreamFormatChangedListener) this);
        int i5 = length + 1;
        this.r = Arrays.copyOf(this.r, i5);
        this.r[length] = i2;
        SampleQueue[] sampleQueueArr = (SampleQueue[]) Arrays.copyOf(this.q, i5);
        sampleQueueArr[length] = sampleQueue;
        Util.a((T[]) sampleQueueArr);
        this.q = sampleQueueArr;
        return sampleQueue;
    }

    public void a() {
        this.s = true;
        this.n.post(this.l);
    }

    public void a(SeekMap seekMap) {
        this.p = seekMap;
        this.n.post(this.l);
    }

    public void a(Format format) {
        this.n.post(this.l);
    }

    private void a(ExtractingLoadable extractingLoadable) {
        if (this.C == -1) {
            this.C = extractingLoadable.k;
        }
    }

    private boolean a(ExtractingLoadable extractingLoadable, int i2) {
        SeekMap seekMap;
        if (this.C == -1 && ((seekMap = this.p) == null || seekMap.getDurationUs() == -9223372036854775807L)) {
            if (!this.t || q()) {
                this.y = this.t;
                this.D = 0;
                this.G = 0;
                for (SampleQueue l2 : this.q) {
                    l2.l();
                }
                extractingLoadable.a(0, 0);
                return true;
            }
            this.F = true;
            return false;
        }
        this.G = i2;
        return true;
    }

    private boolean a(boolean[] zArr, long j2) {
        int length = this.q.length;
        int i2 = 0;
        while (true) {
            boolean z2 = true;
            if (i2 >= length) {
                return true;
            }
            SampleQueue sampleQueue = this.q[i2];
            sampleQueue.m();
            if (sampleQueue.a(j2, true, false) == -1) {
                z2 = false;
            }
            if (z2 || (!zArr[i2] && this.v)) {
                i2++;
            }
        }
        return false;
    }
}
