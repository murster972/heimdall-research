package com.google.android.exoplayer2.source.dash.manifest;

import android.net.Uri;
import com.google.android.exoplayer2.util.UriUtil;

public final class RangedUri {

    /* renamed from: a  reason: collision with root package name */
    public final long f3466a;
    public final long b;
    private final String c;
    private int d;

    public RangedUri(String str, long j, long j2) {
        this.c = str == null ? "" : str;
        this.f3466a = j;
        this.b = j2;
    }

    public Uri a(String str) {
        return UriUtil.b(str, this.c);
    }

    public String b(String str) {
        return UriUtil.a(str, this.c);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || RangedUri.class != obj.getClass()) {
            return false;
        }
        RangedUri rangedUri = (RangedUri) obj;
        if (this.f3466a == rangedUri.f3466a && this.b == rangedUri.b && this.c.equals(rangedUri.c)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.d == 0) {
            this.d = ((((527 + ((int) this.f3466a)) * 31) + ((int) this.b)) * 31) + this.c.hashCode();
        }
        return this.d;
    }

    public String toString() {
        return "RangedUri(referenceUri=" + this.c + ", start=" + this.f3466a + ", length=" + this.b + ")";
    }

    public RangedUri a(RangedUri rangedUri, String str) {
        String b2 = b(str);
        if (rangedUri != null && b2.equals(rangedUri.b(str))) {
            long j = this.b;
            long j2 = -1;
            if (j != -1) {
                long j3 = this.f3466a;
                if (j3 + j == rangedUri.f3466a) {
                    long j4 = rangedUri.b;
                    if (j4 != -1) {
                        j2 = j + j4;
                    }
                    return new RangedUri(b2, j3, j2);
                }
            }
            long j5 = rangedUri.b;
            if (j5 != -1) {
                long j6 = rangedUri.f3466a;
                if (j6 + j5 == this.f3466a) {
                    long j7 = this.b;
                    if (j7 != -1) {
                        j2 = j5 + j7;
                    }
                    return new RangedUri(b2, j6, j2);
                }
            }
        }
        return null;
    }
}
