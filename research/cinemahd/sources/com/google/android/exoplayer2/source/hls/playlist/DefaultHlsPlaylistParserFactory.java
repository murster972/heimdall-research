package com.google.android.exoplayer2.source.hls.playlist;

import com.google.android.exoplayer2.offline.FilteringManifestParser;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import java.util.Collections;
import java.util.List;

public final class DefaultHlsPlaylistParserFactory implements HlsPlaylistParserFactory {

    /* renamed from: a  reason: collision with root package name */
    private final List<StreamKey> f3490a;

    public DefaultHlsPlaylistParserFactory() {
        this(Collections.emptyList());
    }

    public ParsingLoadable.Parser<HlsPlaylist> a() {
        return new FilteringManifestParser(new HlsPlaylistParser(), this.f3490a);
    }

    public DefaultHlsPlaylistParserFactory(List<StreamKey> list) {
        this.f3490a = list;
    }

    public ParsingLoadable.Parser<HlsPlaylist> a(HlsMasterPlaylist hlsMasterPlaylist) {
        return new FilteringManifestParser(new HlsPlaylistParser(hlsMasterPlaylist), this.f3490a);
    }
}
