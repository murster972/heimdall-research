package com.google.android.exoplayer2.source.hls;

import android.util.Pair;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.DefaultExtractorInput;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.Id3Decoder;
import com.google.android.exoplayer2.metadata.id3.PrivFrame;
import com.google.android.exoplayer2.source.chunk.MediaChunk;
import com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

final class HlsMediaChunk extends MediaChunk {
    private static final AtomicInteger G = new AtomicInteger();
    private HlsSampleStreamWrapper A;
    private int B;
    private int C;
    private boolean D;
    private volatile boolean E;
    private boolean F;
    public final int j;
    public final int k;
    public final HlsMasterPlaylist.HlsUrl l;
    private final DataSource m;
    private final DataSpec n;
    private final boolean o;
    private final boolean p;
    private final boolean q;
    private final TimestampAdjuster r;
    private final boolean s;
    private final HlsExtractorFactory t;
    private final List<Format> u;
    private final DrmInitData v;
    private final Extractor w;
    private final Id3Decoder x;
    private final ParsableByteArray y;
    private Extractor z;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HlsMediaChunk(com.google.android.exoplayer2.source.hls.HlsExtractorFactory r17, com.google.android.exoplayer2.upstream.DataSource r18, com.google.android.exoplayer2.upstream.DataSpec r19, com.google.android.exoplayer2.upstream.DataSpec r20, com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist.HlsUrl r21, java.util.List<com.google.android.exoplayer2.Format> r22, int r23, java.lang.Object r24, long r25, long r27, long r29, int r31, boolean r32, boolean r33, com.google.android.exoplayer2.util.TimestampAdjuster r34, com.google.android.exoplayer2.source.hls.HlsMediaChunk r35, com.google.android.exoplayer2.drm.DrmInitData r36, byte[] r37, byte[] r38) {
        /*
            r16 = this;
            r12 = r16
            r13 = r18
            r14 = r21
            r15 = r31
            r10 = r35
            r11 = r37
            r0 = r38
            com.google.android.exoplayer2.upstream.DataSource r1 = a(r13, r11, r0)
            com.google.android.exoplayer2.Format r3 = r14.b
            r0 = r16
            r2 = r19
            r4 = r23
            r5 = r24
            r6 = r25
            r8 = r27
            r13 = r10
            r10 = r29
            r0.<init>(r1, r2, r3, r4, r5, r6, r8, r10)
            r12.k = r15
            r0 = r20
            r12.n = r0
            r12.l = r14
            r0 = r33
            r12.p = r0
            r0 = r34
            r12.r = r0
            r0 = 1
            r1 = 0
            if (r37 == 0) goto L_0x003c
            r2 = 1
            goto L_0x003d
        L_0x003c:
            r2 = 0
        L_0x003d:
            r12.o = r2
            r2 = r32
            r12.q = r2
            r2 = r17
            r12.t = r2
            r2 = r22
            r12.u = r2
            r2 = r36
            r12.v = r2
            r2 = 0
            if (r13 == 0) goto L_0x0073
            com.google.android.exoplayer2.metadata.id3.Id3Decoder r3 = r13.x
            r12.x = r3
            com.google.android.exoplayer2.util.ParsableByteArray r3 = r13.y
            r12.y = r3
            com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl r3 = r13.l
            if (r3 != r14) goto L_0x0064
            boolean r3 = r13.F
            if (r3 != 0) goto L_0x0063
            goto L_0x0064
        L_0x0063:
            r0 = 0
        L_0x0064:
            r12.s = r0
            int r0 = r13.k
            if (r0 != r15) goto L_0x0085
            boolean r0 = r12.s
            if (r0 == 0) goto L_0x006f
            goto L_0x0085
        L_0x006f:
            com.google.android.exoplayer2.extractor.Extractor r0 = r13.z
            r2 = r0
            goto L_0x0085
        L_0x0073:
            com.google.android.exoplayer2.metadata.id3.Id3Decoder r0 = new com.google.android.exoplayer2.metadata.id3.Id3Decoder
            r0.<init>()
            r12.x = r0
            com.google.android.exoplayer2.util.ParsableByteArray r0 = new com.google.android.exoplayer2.util.ParsableByteArray
            r3 = 10
            r0.<init>((int) r3)
            r12.y = r0
            r12.s = r1
        L_0x0085:
            r12.w = r2
            r0 = r18
            r12.m = r0
            java.util.concurrent.atomic.AtomicInteger r0 = G
            int r0 = r0.getAndIncrement()
            r12.j = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.hls.HlsMediaChunk.<init>(com.google.android.exoplayer2.source.hls.HlsExtractorFactory, com.google.android.exoplayer2.upstream.DataSource, com.google.android.exoplayer2.upstream.DataSpec, com.google.android.exoplayer2.upstream.DataSpec, com.google.android.exoplayer2.source.hls.playlist.HlsMasterPlaylist$HlsUrl, java.util.List, int, java.lang.Object, long, long, long, int, boolean, boolean, com.google.android.exoplayer2.util.TimestampAdjuster, com.google.android.exoplayer2.source.hls.HlsMediaChunk, com.google.android.exoplayer2.drm.DrmInitData, byte[], byte[]):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003f A[Catch:{ all -> 0x0052, all -> 0x0072 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046 A[SYNTHETIC, Splitter:B:18:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void g() throws java.io.IOException, java.lang.InterruptedException {
        /*
            r8 = this;
            boolean r0 = r8.o
            r1 = 0
            if (r0 == 0) goto L_0x000d
            com.google.android.exoplayer2.upstream.DataSpec r0 = r8.f3431a
            int r2 = r8.C
            if (r2 == 0) goto L_0x0016
            r2 = 1
            goto L_0x0017
        L_0x000d:
            com.google.android.exoplayer2.upstream.DataSpec r0 = r8.f3431a
            int r2 = r8.C
            long r2 = (long) r2
            com.google.android.exoplayer2.upstream.DataSpec r0 = r0.a((long) r2)
        L_0x0016:
            r2 = 0
        L_0x0017:
            boolean r3 = r8.p
            if (r3 != 0) goto L_0x0021
            com.google.android.exoplayer2.util.TimestampAdjuster r3 = r8.r
            r3.e()
            goto L_0x0037
        L_0x0021:
            com.google.android.exoplayer2.util.TimestampAdjuster r3 = r8.r
            long r3 = r3.a()
            r5 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 != 0) goto L_0x0037
            com.google.android.exoplayer2.util.TimestampAdjuster r3 = r8.r
            long r4 = r8.f
            r3.c(r4)
        L_0x0037:
            com.google.android.exoplayer2.upstream.StatsDataSource r3 = r8.h     // Catch:{ all -> 0x0072 }
            com.google.android.exoplayer2.extractor.DefaultExtractorInput r0 = r8.a(r3, r0)     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0044
            int r2 = r8.C     // Catch:{ all -> 0x0072 }
            r0.c(r2)     // Catch:{ all -> 0x0072 }
        L_0x0044:
            if (r1 != 0) goto L_0x0060
            boolean r1 = r8.E     // Catch:{ all -> 0x0052 }
            if (r1 != 0) goto L_0x0060
            com.google.android.exoplayer2.extractor.Extractor r1 = r8.z     // Catch:{ all -> 0x0052 }
            r2 = 0
            int r1 = r1.a((com.google.android.exoplayer2.extractor.ExtractorInput) r0, (com.google.android.exoplayer2.extractor.PositionHolder) r2)     // Catch:{ all -> 0x0052 }
            goto L_0x0044
        L_0x0052:
            r1 = move-exception
            long r2 = r0.getPosition()     // Catch:{ all -> 0x0072 }
            com.google.android.exoplayer2.upstream.DataSpec r0 = r8.f3431a     // Catch:{ all -> 0x0072 }
            long r4 = r0.d     // Catch:{ all -> 0x0072 }
            long r2 = r2 - r4
            int r0 = (int) r2     // Catch:{ all -> 0x0072 }
            r8.C = r0     // Catch:{ all -> 0x0072 }
            throw r1     // Catch:{ all -> 0x0072 }
        L_0x0060:
            long r0 = r0.getPosition()     // Catch:{ all -> 0x0072 }
            com.google.android.exoplayer2.upstream.DataSpec r2 = r8.f3431a     // Catch:{ all -> 0x0072 }
            long r2 = r2.d     // Catch:{ all -> 0x0072 }
            long r0 = r0 - r2
            int r1 = (int) r0     // Catch:{ all -> 0x0072 }
            r8.C = r1     // Catch:{ all -> 0x0072 }
            com.google.android.exoplayer2.upstream.StatsDataSource r0 = r8.h
            com.google.android.exoplayer2.util.Util.a((com.google.android.exoplayer2.upstream.DataSource) r0)
            return
        L_0x0072:
            r0 = move-exception
            com.google.android.exoplayer2.upstream.StatsDataSource r1 = r8.h
            com.google.android.exoplayer2.util.Util.a((com.google.android.exoplayer2.upstream.DataSource) r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.source.hls.HlsMediaChunk.g():void");
    }

    private void h() throws IOException, InterruptedException {
        DataSpec dataSpec;
        DefaultExtractorInput a2;
        if (!this.D && (dataSpec = this.n) != null) {
            try {
                a2 = a(this.m, dataSpec.a((long) this.B));
                int i = 0;
                while (i == 0) {
                    if (this.E) {
                        break;
                    }
                    i = this.z.a((ExtractorInput) a2, (PositionHolder) null);
                }
                this.B = (int) (a2.getPosition() - this.n.d);
                Util.a(this.m);
                this.D = true;
            } catch (Throwable th) {
                Util.a(this.m);
                throw th;
            }
        }
    }

    public void a(HlsSampleStreamWrapper hlsSampleStreamWrapper) {
        this.A = hlsSampleStreamWrapper;
    }

    public void cancelLoad() {
        this.E = true;
    }

    public boolean f() {
        return this.F;
    }

    public void load() throws IOException, InterruptedException {
        h();
        if (!this.E) {
            if (!this.q) {
                g();
            }
            this.F = true;
        }
    }

    private DefaultExtractorInput a(DataSource dataSource, DataSpec dataSpec) throws IOException, InterruptedException {
        DataSpec dataSpec2 = dataSpec;
        DefaultExtractorInput defaultExtractorInput = new DefaultExtractorInput(dataSource, dataSpec2.d, dataSource.a(dataSpec));
        if (this.z != null) {
            return defaultExtractorInput;
        }
        long a2 = a((ExtractorInput) defaultExtractorInput);
        defaultExtractorInput.a();
        DefaultExtractorInput defaultExtractorInput2 = defaultExtractorInput;
        Pair<Extractor, Boolean> a3 = this.t.a(this.w, dataSpec2.f3586a, this.c, this.u, this.v, this.r, dataSource.a(), defaultExtractorInput2);
        this.z = (Extractor) a3.first;
        boolean z2 = true;
        boolean z3 = this.z == this.w;
        if (((Boolean) a3.second).booleanValue()) {
            this.A.d(a2 != -9223372036854775807L ? this.r.b(a2) : this.f);
        }
        if (!z3 || this.n == null) {
            z2 = false;
        }
        this.D = z2;
        this.A.a(this.j, this.s, z3);
        if (z3) {
            return defaultExtractorInput2;
        }
        this.z.a((ExtractorOutput) this.A);
        return defaultExtractorInput2;
    }

    private long a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.a();
        try {
            extractorInput.a(this.y.f3641a, 0, 10);
            this.y.c(10);
            if (this.y.w() != Id3Decoder.b) {
                return -9223372036854775807L;
            }
            this.y.f(3);
            int s2 = this.y.s();
            int i = s2 + 10;
            if (i > this.y.b()) {
                ParsableByteArray parsableByteArray = this.y;
                byte[] bArr = parsableByteArray.f3641a;
                parsableByteArray.c(i);
                System.arraycopy(bArr, 0, this.y.f3641a, 0, 10);
            }
            extractorInput.a(this.y.f3641a, 10, s2);
            Metadata a2 = this.x.a(this.y.f3641a, s2);
            if (a2 == null) {
                return -9223372036854775807L;
            }
            int a3 = a2.a();
            for (int i2 = 0; i2 < a3; i2++) {
                Metadata.Entry a4 = a2.a(i2);
                if (a4 instanceof PrivFrame) {
                    PrivFrame privFrame = (PrivFrame) a4;
                    if ("com.apple.streaming.transportStreamTimestamp".equals(privFrame.b)) {
                        System.arraycopy(privFrame.c, 0, this.y.f3641a, 0, 8);
                        this.y.c(8);
                        return this.y.p() & 8589934591L;
                    }
                }
            }
            return -9223372036854775807L;
        } catch (EOFException unused) {
        }
    }

    private static DataSource a(DataSource dataSource, byte[] bArr, byte[] bArr2) {
        return bArr != null ? new Aes128DataSource(dataSource, bArr, bArr2) : dataSource;
    }
}
