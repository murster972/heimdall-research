package com.google.android.exoplayer2;

import android.util.Pair;
import com.google.android.exoplayer2.source.ads.AdPlaybackState;
import com.google.android.exoplayer2.util.Assertions;

public abstract class Timeline {

    /* renamed from: a  reason: collision with root package name */
    public static final Timeline f3175a = new Timeline() {
        public int a() {
            return 0;
        }

        public int a(Object obj) {
            return -1;
        }

        public Window a(int i, Window window, boolean z, long j) {
            throw new IndexOutOfBoundsException();
        }

        public int b() {
            return 0;
        }

        public Period a(int i, Period period, boolean z) {
            throw new IndexOutOfBoundsException();
        }

        public Object a(int i) {
            throw new IndexOutOfBoundsException();
        }
    };

    public static final class Period {

        /* renamed from: a  reason: collision with root package name */
        public Object f3176a;
        public int b;
        public long c;
        private long d;
        private AdPlaybackState e;

        public Period a(Object obj, Object obj2, int i, long j, long j2) {
            return a(obj, obj2, i, j, j2, AdPlaybackState.f);
        }

        public long b(int i) {
            return this.e.b[i];
        }

        public long c() {
            return C.b(this.c);
        }

        public long d() {
            return this.c;
        }

        public long e() {
            return C.b(this.d);
        }

        public long f() {
            return this.d;
        }

        public Period a(Object obj, Object obj2, int i, long j, long j2, AdPlaybackState adPlaybackState) {
            this.f3176a = obj2;
            this.b = i;
            this.c = j;
            this.d = j2;
            this.e = adPlaybackState;
            return this;
        }

        public int b(int i, int i2) {
            return this.e.c[i].a(i2);
        }

        public int c(int i) {
            return this.e.c[i].a();
        }

        public boolean d(int i) {
            return !this.e.c[i].b();
        }

        public int b(long j) {
            return this.e.b(j);
        }

        public boolean c(int i, int i2) {
            AdPlaybackState.AdGroup adGroup = this.e.c[i];
            return (adGroup.f3427a == -1 || adGroup.c[i2] == 0) ? false : true;
        }

        public long b() {
            return this.e.d;
        }

        public int a() {
            return this.e.f3426a;
        }

        public int a(long j) {
            return this.e.a(j);
        }

        public int a(int i) {
            return this.e.c[i].f3427a;
        }

        public long a(int i, int i2) {
            AdPlaybackState.AdGroup adGroup = this.e.c[i];
            if (adGroup.f3427a != -1) {
                return adGroup.d[i2];
            }
            return -9223372036854775807L;
        }
    }

    public abstract int a();

    public int a(int i, int i2, boolean z) {
        if (i2 != 0) {
            if (i2 == 1) {
                return i;
            }
            if (i2 == 2) {
                return i == b(z) ? a(z) : i + 1;
            }
            throw new IllegalStateException();
        } else if (i == b(z)) {
            return -1;
        } else {
            return i + 1;
        }
    }

    public abstract int a(Object obj);

    public abstract Period a(int i, Period period, boolean z);

    public abstract Window a(int i, Window window, boolean z, long j);

    public abstract Object a(int i);

    public abstract int b();

    public int b(int i, int i2, boolean z) {
        if (i2 != 0) {
            if (i2 == 1) {
                return i;
            }
            if (i2 == 2) {
                return i == a(z) ? b(z) : i - 1;
            }
            throw new IllegalStateException();
        } else if (i == a(z)) {
            return -1;
        } else {
            return i - 1;
        }
    }

    public final boolean c() {
        return b() == 0;
    }

    public int a(boolean z) {
        return c() ? -1 : 0;
    }

    public int b(boolean z) {
        if (c()) {
            return -1;
        }
        return b() - 1;
    }

    public final Window a(int i, Window window) {
        return a(i, window, false);
    }

    public final boolean b(int i, Period period, Window window, int i2, boolean z) {
        return a(i, period, window, i2, z) == -1;
    }

    public static final class Window {

        /* renamed from: a  reason: collision with root package name */
        public boolean f3177a;
        public boolean b;
        public int c;
        public int d;
        public long e;
        public long f;
        public long g;

        public Window a(Object obj, long j, long j2, boolean z, boolean z2, long j3, long j4, int i, int i2, long j5) {
            this.f3177a = z;
            this.b = z2;
            this.e = j3;
            this.f = j4;
            this.c = i;
            this.d = i2;
            this.g = j5;
            return this;
        }

        public long b() {
            return this.e;
        }

        public long c() {
            return C.b(this.f);
        }

        public long d() {
            return this.g;
        }

        public long a() {
            return C.b(this.e);
        }
    }

    public final Window a(int i, Window window, boolean z) {
        return a(i, window, z, 0);
    }

    public final int a(int i, Period period, Window window, int i2, boolean z) {
        int i3 = a(i, period).b;
        if (a(i3, window).d != i) {
            return i + 1;
        }
        int a2 = a(i3, i2, z);
        if (a2 == -1) {
            return -1;
        }
        return a(a2, window).c;
    }

    public final Pair<Object, Long> a(Window window, Period period, int i, long j) {
        return a(window, period, i, j, 0);
    }

    public final Pair<Object, Long> a(Window window, Period period, int i, long j, long j2) {
        Assertions.a(i, 0, b());
        a(i, window, false, j2);
        if (j == -9223372036854775807L) {
            j = window.b();
            if (j == -9223372036854775807L) {
                return null;
            }
        }
        int i2 = window.c;
        long d = window.d() + j;
        long d2 = a(i2, period, true).d();
        while (d2 != -9223372036854775807L && d >= d2 && i2 < window.d) {
            d -= d2;
            i2++;
            d2 = a(i2, period, true).d();
        }
        return Pair.create(period.f3176a, Long.valueOf(d));
    }

    public Period a(Object obj, Period period) {
        return a(a(obj), period, true);
    }

    public final Period a(int i, Period period) {
        return a(i, period, false);
    }
}
