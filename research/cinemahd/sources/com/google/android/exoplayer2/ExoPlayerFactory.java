package com.google.android.exoplayer2;

import android.content.Context;
import android.os.Looper;
import com.google.android.exoplayer2.analytics.AnalyticsCollector;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;

public final class ExoPlayerFactory {

    /* renamed from: a  reason: collision with root package name */
    private static BandwidthMeter f3155a;

    private ExoPlayerFactory() {
    }

    @Deprecated
    public static SimpleExoPlayer a(Context context, TrackSelector trackSelector, LoadControl loadControl) {
        return a(context, (RenderersFactory) new DefaultRenderersFactory(context), trackSelector, loadControl);
    }

    public static SimpleExoPlayer a(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager) {
        return a(context, renderersFactory, trackSelector, new DefaultLoadControl(), drmSessionManager);
    }

    public static SimpleExoPlayer a(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, LoadControl loadControl) {
        return a(context, renderersFactory, trackSelector, loadControl, (DrmSessionManager<FrameworkMediaCrypto>) null, Util.a());
    }

    public static SimpleExoPlayer a(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, LoadControl loadControl, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager) {
        return a(context, renderersFactory, trackSelector, loadControl, drmSessionManager, Util.a());
    }

    public static SimpleExoPlayer a(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, LoadControl loadControl, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, Looper looper) {
        return a(context, renderersFactory, trackSelector, loadControl, drmSessionManager, new AnalyticsCollector.Factory(), looper);
    }

    public static SimpleExoPlayer a(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, LoadControl loadControl, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, AnalyticsCollector.Factory factory, Looper looper) {
        return a(context, renderersFactory, trackSelector, loadControl, drmSessionManager, a(), factory, looper);
    }

    public static SimpleExoPlayer a(Context context, RenderersFactory renderersFactory, TrackSelector trackSelector, LoadControl loadControl, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, BandwidthMeter bandwidthMeter, AnalyticsCollector.Factory factory, Looper looper) {
        return new SimpleExoPlayer(context, renderersFactory, trackSelector, loadControl, drmSessionManager, bandwidthMeter, factory, looper);
    }

    private static synchronized BandwidthMeter a() {
        BandwidthMeter bandwidthMeter;
        synchronized (ExoPlayerFactory.class) {
            if (f3155a == null) {
                f3155a = new DefaultBandwidthMeter.Builder().a();
            }
            bandwidthMeter = f3155a;
        }
        return bandwidthMeter;
    }
}
