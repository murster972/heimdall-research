package com.google.android.exoplayer2;

import com.google.android.exoplayer2.source.MediaSource;

final class MediaPeriodInfo {

    /* renamed from: a  reason: collision with root package name */
    public final MediaSource.MediaPeriodId f3167a;
    public final long b;
    public final long c;
    public final long d;
    public final boolean e;
    public final boolean f;

    MediaPeriodInfo(MediaSource.MediaPeriodId mediaPeriodId, long j, long j2, long j3, boolean z, boolean z2) {
        this.f3167a = mediaPeriodId;
        this.b = j;
        this.c = j2;
        this.d = j3;
        this.e = z;
        this.f = z2;
    }

    public MediaPeriodInfo a(long j) {
        return new MediaPeriodInfo(this.f3167a, j, this.c, this.d, this.e, this.f);
    }
}
