package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.upstream.DataSource;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface HttpDataSource extends DataSource {

    public static abstract class BaseFactory implements Factory {

        /* renamed from: a  reason: collision with root package name */
        private final RequestProperties f3595a = new RequestProperties();

        /* access modifiers changed from: protected */
        public abstract HttpDataSource a(RequestProperties requestProperties);

        public final RequestProperties b() {
            return this.f3595a;
        }

        public final HttpDataSource a() {
            return a(this.f3595a);
        }
    }

    public interface Factory extends DataSource.Factory {
        HttpDataSource a();

        RequestProperties b();
    }

    public static final class InvalidContentTypeException extends HttpDataSourceException {
        public final String contentType;

        public InvalidContentTypeException(String str, DataSpec dataSpec) {
            super("Invalid content type: " + str, dataSpec, 1);
            this.contentType = str;
        }
    }

    public static final class InvalidResponseCodeException extends HttpDataSourceException {
        public final Map<String, List<String>> headerFields;
        public final int responseCode;
        public final String responseMessage;

        public InvalidResponseCodeException(int i, String str, Map<String, List<String>> map, DataSpec dataSpec) {
            super("Response code: " + i, dataSpec, 1);
            this.responseCode = i;
            this.responseMessage = str;
            this.headerFields = map;
        }
    }

    static {
        c cVar = c.f3607a;
    }

    void a(String str, String str2);

    public static class HttpDataSourceException extends IOException {
        public final DataSpec dataSpec;
        public final int type;

        public HttpDataSourceException(String str, DataSpec dataSpec2, int i) {
            super(str);
            this.dataSpec = dataSpec2;
            this.type = i;
        }

        public HttpDataSourceException(IOException iOException, DataSpec dataSpec2, int i) {
            super(iOException);
            this.dataSpec = dataSpec2;
            this.type = i;
        }

        public HttpDataSourceException(String str, IOException iOException, DataSpec dataSpec2, int i) {
            super(str, iOException);
            this.dataSpec = dataSpec2;
            this.type = i;
        }
    }

    public static final class RequestProperties {

        /* renamed from: a  reason: collision with root package name */
        private final Map<String, String> f3596a = new HashMap();
        private Map<String, String> b;

        public synchronized void a(String str, String str2) {
            this.b = null;
            this.f3596a.put(str, str2);
        }

        public synchronized Map<String, String> a() {
            if (this.b == null) {
                this.b = Collections.unmodifiableMap(new HashMap(this.f3596a));
            }
            return this.b;
        }
    }
}
