package com.google.android.exoplayer2.upstream.cache;

import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class SimpleCacheSpan extends CacheSpan {
    private static final Pattern f = Pattern.compile("^(.+)\\.(\\d+)\\.(\\d+)\\.v1\\.exo$", 32);
    private static final Pattern g = Pattern.compile("^(.+)\\.(\\d+)\\.(\\d+)\\.v2\\.exo$", 32);
    private static final Pattern h = Pattern.compile("^(\\d+)\\.(\\d+)\\.(\\d+)\\.v3\\.exo$", 32);

    private SimpleCacheSpan(String str, long j, long j2, long j3, File file) {
        super(str, j, j2, j3, file);
    }

    public static File a(File file, int i, long j, long j2) {
        return new File(file, i + "." + j + "." + j2 + ".v3.exo");
    }

    public static SimpleCacheSpan b(String str, long j) {
        return new SimpleCacheSpan(str, j, -1, -9223372036854775807L, (File) null);
    }

    public static SimpleCacheSpan a(String str, long j) {
        return new SimpleCacheSpan(str, j, -1, -9223372036854775807L, (File) null);
    }

    private static File b(File file, CachedContentIndex cachedContentIndex) {
        String str;
        String name = file.getName();
        Matcher matcher = g.matcher(name);
        if (matcher.matches()) {
            str = Util.m(matcher.group(1));
            if (str == null) {
                return null;
            }
        } else {
            matcher = f.matcher(name);
            if (!matcher.matches()) {
                return null;
            }
            str = matcher.group(1);
        }
        File a2 = a(file.getParentFile(), cachedContentIndex.a(str), Long.parseLong(matcher.group(2)), Long.parseLong(matcher.group(3)));
        if (!file.renameTo(a2)) {
            return null;
        }
        return a2;
    }

    public static SimpleCacheSpan a(String str, long j, long j2) {
        return new SimpleCacheSpan(str, j, j2, -9223372036854775807L, (File) null);
    }

    public static SimpleCacheSpan a(File file, CachedContentIndex cachedContentIndex) {
        String name = file.getName();
        if (!name.endsWith(".v3.exo")) {
            file = b(file, cachedContentIndex);
            if (file == null) {
                return null;
            }
            name = file.getName();
        }
        File file2 = file;
        Matcher matcher = h.matcher(name);
        if (!matcher.matches()) {
            return null;
        }
        long length = file2.length();
        String a2 = cachedContentIndex.a(Integer.parseInt(matcher.group(1)));
        if (a2 == null) {
            return null;
        }
        return new SimpleCacheSpan(a2, Long.parseLong(matcher.group(2)), length, Long.parseLong(matcher.group(3)), file2);
    }

    public SimpleCacheSpan a(int i) {
        Assertions.b(this.d);
        long currentTimeMillis = System.currentTimeMillis();
        return new SimpleCacheSpan(this.f3611a, this.b, this.c, currentTimeMillis, a(this.e.getParentFile(), i, this.b, currentTimeMillis));
    }
}
