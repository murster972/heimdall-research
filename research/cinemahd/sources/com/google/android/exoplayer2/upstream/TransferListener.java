package com.google.android.exoplayer2.upstream;

public interface TransferListener {
    void a(DataSource dataSource, DataSpec dataSpec, boolean z);

    void a(DataSource dataSource, DataSpec dataSpec, boolean z, int i);

    void b(DataSource dataSource, DataSpec dataSpec, boolean z);

    void c(DataSource dataSource, DataSpec dataSpec, boolean z);
}
