package com.google.android.exoplayer2.upstream.cache;

import java.io.File;

public class CacheSpan implements Comparable<CacheSpan> {

    /* renamed from: a  reason: collision with root package name */
    public final String f3611a;
    public final long b;
    public final long c;
    public final boolean d;
    public final File e;

    public CacheSpan(String str, long j, long j2, long j3, File file) {
        this.f3611a = str;
        this.b = j;
        this.c = j2;
        this.d = file != null;
        this.e = file;
    }

    public boolean a() {
        return !this.d;
    }

    public boolean b() {
        return this.c == -1;
    }

    /* renamed from: a */
    public int compareTo(CacheSpan cacheSpan) {
        if (!this.f3611a.equals(cacheSpan.f3611a)) {
            return this.f3611a.compareTo(cacheSpan.f3611a);
        }
        int i = ((this.b - cacheSpan.b) > 0 ? 1 : ((this.b - cacheSpan.b) == 0 ? 0 : -1));
        if (i == 0) {
            return 0;
        }
        return i < 0 ? -1 : 1;
    }
}
