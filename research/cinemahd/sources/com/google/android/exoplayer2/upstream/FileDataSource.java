package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

public final class FileDataSource extends BaseDataSource {
    private RandomAccessFile e;
    private Uri f;
    private long g;
    private boolean h;

    public static class FileDataSourceException extends IOException {
        public FileDataSourceException(IOException iOException) {
            super(iOException);
        }
    }

    public FileDataSource() {
        super(false);
    }

    public long a(DataSpec dataSpec) throws FileDataSourceException {
        try {
            this.f = dataSpec.f3586a;
            b(dataSpec);
            this.e = new RandomAccessFile(dataSpec.f3586a.getPath(), "r");
            this.e.seek(dataSpec.e);
            this.g = dataSpec.f == -1 ? this.e.length() - dataSpec.e : dataSpec.f;
            if (this.g >= 0) {
                this.h = true;
                c(dataSpec);
                return this.g;
            }
            throw new EOFException();
        } catch (IOException e2) {
            throw new FileDataSourceException(e2);
        }
    }

    public void close() throws FileDataSourceException {
        this.f = null;
        try {
            if (this.e != null) {
                this.e.close();
            }
            this.e = null;
            if (this.h) {
                this.h = false;
                b();
            }
        } catch (IOException e2) {
            throw new FileDataSourceException(e2);
        } catch (Throwable th) {
            this.e = null;
            if (this.h) {
                this.h = false;
                b();
            }
            throw th;
        }
    }

    public Uri getUri() {
        return this.f;
    }

    public int read(byte[] bArr, int i, int i2) throws FileDataSourceException {
        if (i2 == 0) {
            return 0;
        }
        long j = this.g;
        if (j == 0) {
            return -1;
        }
        try {
            int read = this.e.read(bArr, i, (int) Math.min(j, (long) i2));
            if (read > 0) {
                this.g -= (long) read;
                a(read);
            }
            return read;
        } catch (IOException e2) {
            throw new FileDataSourceException(e2);
        }
    }
}
