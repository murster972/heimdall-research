package com.google.android.exoplayer2.upstream.cache;

import android.os.ConditionVariable;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

public final class SimpleCache implements Cache {
    private static final HashSet<File> g = new HashSet<>();
    private static boolean h;

    /* renamed from: a  reason: collision with root package name */
    private final File f3618a;
    /* access modifiers changed from: private */
    public final CacheEvictor b;
    private final CachedContentIndex c;
    private final HashMap<String, ArrayList<Cache.Listener>> d;
    private long e;
    private boolean f;

    public SimpleCache(File file, CacheEvictor cacheEvictor) {
        this(file, cacheEvictor, (byte[]) null, false);
    }

    private SimpleCacheSpan d(String str, long j) throws Cache.CacheException {
        SimpleCacheSpan a2;
        CachedContent b2 = this.c.b(str);
        if (b2 == null) {
            return SimpleCacheSpan.b(str, j);
        }
        while (true) {
            a2 = b2.a(j);
            if (!a2.d || a2.e.exists()) {
                return a2;
            }
            c();
        }
        return a2;
    }

    public SimpleCache(File file, CacheEvictor cacheEvictor, byte[] bArr, boolean z) {
        this(file, cacheEvictor, new CachedContentIndex(file, bArr, z));
    }

    public synchronized long a() {
        Assertions.b(!this.f);
        return this.e;
    }

    public synchronized NavigableSet<CacheSpan> c(String str) {
        TreeSet treeSet;
        Assertions.b(!this.f);
        CachedContent b2 = this.c.b(str);
        if (b2 != null) {
            if (!b2.c()) {
                treeSet = new TreeSet(b2.b());
            }
        }
        treeSet = new TreeSet();
        return treeSet;
    }

    SimpleCache(File file, CacheEvictor cacheEvictor, CachedContentIndex cachedContentIndex) {
        if (b(file)) {
            this.f3618a = file;
            this.b = cacheEvictor;
            this.c = cachedContentIndex;
            this.d = new HashMap<>();
            final ConditionVariable conditionVariable = new ConditionVariable();
            new Thread("SimpleCache.initialize()") {
                public void run() {
                    synchronized (SimpleCache.this) {
                        conditionVariable.open();
                        SimpleCache.this.b();
                        SimpleCache.this.b.a();
                    }
                }
            }.start();
            conditionVariable.block();
            return;
        }
        throw new IllegalStateException("Another SimpleCache instance uses the folder: " + file);
    }

    public synchronized SimpleCacheSpan b(String str, long j) throws Cache.CacheException {
        Assertions.b(!this.f);
        SimpleCacheSpan d2 = d(str, j);
        if (d2.d) {
            try {
                SimpleCacheSpan b2 = this.c.b(str).b(d2);
                a(d2, (CacheSpan) b2);
                return b2;
            } catch (Cache.CacheException unused) {
                return d2;
            }
        } else {
            CachedContent d3 = this.c.d(str);
            if (d3.d()) {
                return null;
            }
            d3.a(true);
            return d2;
        }
    }

    public synchronized File a(String str, long j, long j2) throws Cache.CacheException {
        CachedContent b2;
        Assertions.b(!this.f);
        b2 = this.c.b(str);
        Assertions.a(b2);
        Assertions.b(b2.d());
        if (!this.f3618a.exists()) {
            this.f3618a.mkdirs();
            c();
        }
        this.b.a(this, str, j, j2);
        return SimpleCacheSpan.a(this.f3618a, b2.f3614a, j, System.currentTimeMillis());
    }

    public synchronized SimpleCacheSpan c(String str, long j) throws InterruptedException, Cache.CacheException {
        SimpleCacheSpan b2;
        while (true) {
            b2 = b(str, j);
            if (b2 == null) {
                wait();
            }
        }
        return b2;
    }

    private void c() throws Cache.CacheException {
        ArrayList arrayList = new ArrayList();
        for (CachedContent b2 : this.c.a()) {
            Iterator<SimpleCacheSpan> it2 = b2.b().iterator();
            while (it2.hasNext()) {
                CacheSpan next = it2.next();
                if (!next.e.exists()) {
                    arrayList.add(next);
                }
            }
        }
        for (int i = 0; i < arrayList.size(); i++) {
            a((CacheSpan) arrayList.get(i), false);
        }
        this.c.c();
        this.c.d();
    }

    public synchronized void a(File file) throws Cache.CacheException {
        boolean z = true;
        Assertions.b(!this.f);
        SimpleCacheSpan a2 = SimpleCacheSpan.a(file, this.c);
        Assertions.b(a2 != null);
        CachedContent b2 = this.c.b(a2.f3611a);
        Assertions.a(b2);
        Assertions.b(b2.d());
        if (file.exists()) {
            if (file.length() == 0) {
                file.delete();
                return;
            }
            long a3 = ContentMetadataInternal.a(b2.a());
            if (a3 != -1) {
                if (a2.b + a2.c > a3) {
                    z = false;
                }
                Assertions.b(z);
            }
            a(a2);
            this.c.d();
            notifyAll();
        }
    }

    public synchronized void b(CacheSpan cacheSpan) throws Cache.CacheException {
        Assertions.b(!this.f);
        a(cacheSpan, true);
    }

    public synchronized long b(String str, long j, long j2) {
        CachedContent b2;
        Assertions.b(!this.f);
        b2 = this.c.b(str);
        return b2 != null ? b2.a(j, j2) : -j2;
    }

    private void c(CacheSpan cacheSpan) {
        ArrayList arrayList = this.d.get(cacheSpan.f3611a);
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                ((Cache.Listener) arrayList.get(size)).b(this, cacheSpan);
            }
        }
        this.b.b(this, cacheSpan);
    }

    public synchronized long b(String str) {
        return ContentMetadataInternal.a(a(str));
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.f3618a.exists()) {
            this.f3618a.mkdirs();
            return;
        }
        this.c.b();
        File[] listFiles = this.f3618a.listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!file.getName().equals("cached_content_index.exi")) {
                    SimpleCacheSpan a2 = file.length() > 0 ? SimpleCacheSpan.a(file, this.c) : null;
                    if (a2 != null) {
                        a(a2);
                    } else {
                        file.delete();
                    }
                }
            }
            this.c.c();
            try {
                this.c.d();
            } catch (Cache.CacheException e2) {
                Log.a("SimpleCache", "Storing index file failed", e2);
            }
        }
    }

    public synchronized void a(CacheSpan cacheSpan) {
        Assertions.b(!this.f);
        CachedContent b2 = this.c.b(cacheSpan.f3611a);
        Assertions.a(b2);
        Assertions.b(b2.d());
        b2.a(false);
        this.c.e(b2.b);
        notifyAll();
    }

    private void b(SimpleCacheSpan simpleCacheSpan) {
        ArrayList arrayList = this.d.get(simpleCacheSpan.f3611a);
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                ((Cache.Listener) arrayList.get(size)).a(this, simpleCacheSpan);
            }
        }
        this.b.a(this, simpleCacheSpan);
    }

    private static synchronized boolean b(File file) {
        synchronized (SimpleCache.class) {
            if (h) {
                return true;
            }
            boolean add = g.add(file.getAbsoluteFile());
            return add;
        }
    }

    public synchronized void a(String str, long j) throws Cache.CacheException {
        ContentMetadataMutations contentMetadataMutations = new ContentMetadataMutations();
        ContentMetadataInternal.a(contentMetadataMutations, j);
        a(str, contentMetadataMutations);
    }

    public synchronized void a(String str, ContentMetadataMutations contentMetadataMutations) throws Cache.CacheException {
        Assertions.b(!this.f);
        this.c.a(str, contentMetadataMutations);
        this.c.d();
    }

    public synchronized ContentMetadata a(String str) {
        Assertions.b(!this.f);
        return this.c.c(str);
    }

    private void a(SimpleCacheSpan simpleCacheSpan) {
        this.c.d(simpleCacheSpan.f3611a).a(simpleCacheSpan);
        this.e += simpleCacheSpan.c;
        b(simpleCacheSpan);
    }

    private void a(CacheSpan cacheSpan, boolean z) throws Cache.CacheException {
        CachedContent b2 = this.c.b(cacheSpan.f3611a);
        if (b2 != null && b2.a(cacheSpan)) {
            this.e -= cacheSpan.c;
            if (z) {
                try {
                    this.c.e(b2.b);
                    this.c.d();
                } catch (Throwable th) {
                    c(cacheSpan);
                    throw th;
                }
            }
            c(cacheSpan);
        }
    }

    private void a(SimpleCacheSpan simpleCacheSpan, CacheSpan cacheSpan) {
        ArrayList arrayList = this.d.get(simpleCacheSpan.f3611a);
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                ((Cache.Listener) arrayList.get(size)).a(this, simpleCacheSpan, cacheSpan);
            }
        }
        this.b.a(this, simpleCacheSpan, cacheSpan);
    }
}
