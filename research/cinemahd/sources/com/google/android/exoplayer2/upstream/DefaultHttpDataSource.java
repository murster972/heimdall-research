package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import android.support.v4.media.session.PlaybackStateCompat;
import com.facebook.common.util.UriUtil;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Predicate;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.NoRouteToHostException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

public class DefaultHttpDataSource extends BaseDataSource implements HttpDataSource {
    private static final Pattern t = Pattern.compile("^bytes (\\d+)-(\\d+)/(\\d+)$");
    private static final AtomicReference<byte[]> u = new AtomicReference<>();
    private final boolean e;
    private final int f;
    private final int g;
    private final String h;
    private final Predicate<String> i;
    private final HttpDataSource.RequestProperties j;
    private final HttpDataSource.RequestProperties k = new HttpDataSource.RequestProperties();
    private DataSpec l;
    private HttpURLConnection m;
    private InputStream n;
    private boolean o;
    private long p;
    private long q;
    private long r;
    private long s;

    public DefaultHttpDataSource(String str, Predicate<String> predicate, int i2, int i3, boolean z, HttpDataSource.RequestProperties requestProperties) {
        super(true);
        Assertions.a(str);
        this.h = str;
        this.i = predicate;
        this.f = i2;
        this.g = i3;
        this.e = z;
        this.j = requestProperties;
    }

    private HttpURLConnection d(DataSpec dataSpec) throws IOException {
        HttpURLConnection a2;
        DataSpec dataSpec2 = dataSpec;
        URL url = new URL(dataSpec2.f3586a.toString());
        int i2 = dataSpec2.b;
        byte[] bArr = dataSpec2.c;
        long j2 = dataSpec2.e;
        long j3 = dataSpec2.f;
        boolean a3 = dataSpec2.a(1);
        if (!this.e) {
            return a(url, i2, bArr, j2, j3, a3, true);
        }
        int i3 = 0;
        while (true) {
            int i4 = i3 + 1;
            if (i3 <= 20) {
                int i5 = i4;
                long j4 = j3;
                a2 = a(url, i2, bArr, j2, j3, a3, false);
                int responseCode = a2.getResponseCode();
                String headerField = a2.getHeaderField("Location");
                if ((i2 == 1 || i2 == 3) && (responseCode == 300 || responseCode == 301 || responseCode == 302 || responseCode == 303 || responseCode == 307 || responseCode == 308)) {
                    a2.disconnect();
                    url = a(url, headerField);
                } else if (i2 != 2 || (responseCode != 300 && responseCode != 301 && responseCode != 302 && responseCode != 303)) {
                    return a2;
                } else {
                    a2.disconnect();
                    url = a(url, headerField);
                    bArr = null;
                    i2 = 1;
                }
                i3 = i5;
                j3 = j4;
            } else {
                throw new NoRouteToHostException("Too many redirects: " + i4);
            }
        }
        return a2;
    }

    private void e() throws IOException {
        if (this.r != this.p) {
            byte[] andSet = u.getAndSet((Object) null);
            if (andSet == null) {
                andSet = new byte[4096];
            }
            while (true) {
                long j2 = this.r;
                long j3 = this.p;
                if (j2 != j3) {
                    int read = this.n.read(andSet, 0, (int) Math.min(j3 - j2, (long) andSet.length));
                    if (Thread.currentThread().isInterrupted()) {
                        throw new InterruptedIOException();
                    } else if (read != -1) {
                        this.r += (long) read;
                        a(read);
                    } else {
                        throw new EOFException();
                    }
                } else {
                    u.set(andSet);
                    return;
                }
            }
        }
    }

    public Map<String, List<String>> a() {
        HttpURLConnection httpURLConnection = this.m;
        return httpURLConnection == null ? Collections.emptyMap() : httpURLConnection.getHeaderFields();
    }

    /* access modifiers changed from: protected */
    public final long c() {
        long j2 = this.q;
        return j2 == -1 ? j2 : j2 - this.s;
    }

    public void close() throws HttpDataSource.HttpDataSourceException {
        try {
            if (this.n != null) {
                a(this.m, c());
                this.n.close();
            }
            this.n = null;
            d();
            if (this.o) {
                this.o = false;
                b();
            }
        } catch (IOException e2) {
            throw new HttpDataSource.HttpDataSourceException(e2, this.l, 3);
        } catch (Throwable th) {
            this.n = null;
            d();
            if (this.o) {
                this.o = false;
                b();
            }
            throw th;
        }
    }

    public Uri getUri() {
        HttpURLConnection httpURLConnection = this.m;
        if (httpURLConnection == null) {
            return null;
        }
        return Uri.parse(httpURLConnection.getURL().toString());
    }

    public int read(byte[] bArr, int i2, int i3) throws HttpDataSource.HttpDataSourceException {
        try {
            e();
            return a(bArr, i2, i3);
        } catch (IOException e2) {
            throw new HttpDataSource.HttpDataSourceException(e2, this.l, 2);
        }
    }

    public void a(String str, String str2) {
        Assertions.a(str);
        Assertions.a(str2);
        this.k.a(str, str2);
    }

    public long a(DataSpec dataSpec) throws HttpDataSource.HttpDataSourceException {
        this.l = dataSpec;
        long j2 = 0;
        this.s = 0;
        this.r = 0;
        b(dataSpec);
        try {
            this.m = d(dataSpec);
            try {
                int responseCode = this.m.getResponseCode();
                String responseMessage = this.m.getResponseMessage();
                if (responseCode < 200 || responseCode > 299) {
                    Map headerFields = this.m.getHeaderFields();
                    d();
                    HttpDataSource.InvalidResponseCodeException invalidResponseCodeException = new HttpDataSource.InvalidResponseCodeException(responseCode, responseMessage, headerFields, dataSpec);
                    if (responseCode == 416) {
                        invalidResponseCodeException.initCause(new DataSourceException(0));
                    }
                    throw invalidResponseCodeException;
                }
                String contentType = this.m.getContentType();
                Predicate<String> predicate = this.i;
                if (predicate == null || predicate.a(contentType)) {
                    if (responseCode == 200) {
                        long j3 = dataSpec.e;
                        if (j3 != 0) {
                            j2 = j3;
                        }
                    }
                    this.p = j2;
                    if (!dataSpec.a(1)) {
                        long j4 = dataSpec.f;
                        long j5 = -1;
                        if (j4 != -1) {
                            this.q = j4;
                        } else {
                            long a2 = a(this.m);
                            if (a2 != -1) {
                                j5 = a2 - this.p;
                            }
                            this.q = j5;
                        }
                    } else {
                        this.q = dataSpec.f;
                    }
                    try {
                        this.n = this.m.getInputStream();
                        this.o = true;
                        c(dataSpec);
                        return this.q;
                    } catch (IOException e2) {
                        d();
                        throw new HttpDataSource.HttpDataSourceException(e2, dataSpec, 1);
                    }
                } else {
                    d();
                    throw new HttpDataSource.InvalidContentTypeException(contentType, dataSpec);
                }
            } catch (IOException e3) {
                d();
                throw new HttpDataSource.HttpDataSourceException("Unable to connect to " + dataSpec.f3586a.toString(), e3, dataSpec, 1);
            }
        } catch (IOException e4) {
            throw new HttpDataSource.HttpDataSourceException("Unable to connect to " + dataSpec.f3586a.toString(), e4, dataSpec, 1);
        }
    }

    private void d() {
        HttpURLConnection httpURLConnection = this.m;
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Exception e2) {
                Log.a("DefaultHttpDataSource", "Unexpected error while disconnecting", e2);
            }
            this.m = null;
        }
    }

    private HttpURLConnection a(URL url, int i2, byte[] bArr, long j2, long j3, boolean z, boolean z2) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(this.f);
        httpURLConnection.setReadTimeout(this.g);
        HttpDataSource.RequestProperties requestProperties = this.j;
        if (requestProperties != null) {
            for (Map.Entry next : requestProperties.a().entrySet()) {
                httpURLConnection.setRequestProperty((String) next.getKey(), (String) next.getValue());
            }
        }
        for (Map.Entry next2 : this.k.a().entrySet()) {
            httpURLConnection.setRequestProperty((String) next2.getKey(), (String) next2.getValue());
        }
        if (!(j2 == 0 && j3 == -1)) {
            String str = "bytes=" + j2 + "-";
            if (j3 != -1) {
                str = str + ((j2 + j3) - 1);
            }
            httpURLConnection.setRequestProperty("Range", str);
        }
        httpURLConnection.setRequestProperty("User-Agent", this.h);
        if (!z) {
            httpURLConnection.setRequestProperty("Accept-Encoding", InterpolationAnimatedNode.EXTRAPOLATE_TYPE_IDENTITY);
        }
        httpURLConnection.setInstanceFollowRedirects(z2);
        httpURLConnection.setDoOutput(bArr != null);
        httpURLConnection.setRequestMethod(DataSpec.b(i2));
        if (bArr != null) {
            httpURLConnection.setFixedLengthStreamingMode(bArr.length);
            httpURLConnection.connect();
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.close();
        } else {
            httpURLConnection.connect();
        }
        return httpURLConnection;
    }

    private static URL a(URL url, String str) throws IOException {
        if (str != null) {
            URL url2 = new URL(url, str);
            String protocol = url2.getProtocol();
            if (UriUtil.HTTPS_SCHEME.equals(protocol) || UriUtil.HTTP_SCHEME.equals(protocol)) {
                return url2;
            }
            throw new ProtocolException("Unsupported protocol redirect: " + protocol);
        }
        throw new ProtocolException("Null location redirect");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long a(java.net.HttpURLConnection r10) {
        /*
            java.lang.String r0 = "Content-Length"
            java.lang.String r0 = r10.getHeaderField(r0)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            java.lang.String r2 = "]"
            java.lang.String r3 = "DefaultHttpDataSource"
            if (r1 != 0) goto L_0x002c
            long r4 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x0015 }
            goto L_0x002e
        L_0x0015:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "Unexpected Content-Length ["
            r1.append(r4)
            r1.append(r0)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.google.android.exoplayer2.util.Log.b(r3, r1)
        L_0x002c:
            r4 = -1
        L_0x002e:
            java.lang.String r1 = "Content-Range"
            java.lang.String r10 = r10.getHeaderField(r1)
            boolean r1 = android.text.TextUtils.isEmpty(r10)
            if (r1 != 0) goto L_0x00a4
            java.util.regex.Pattern r1 = t
            java.util.regex.Matcher r1 = r1.matcher(r10)
            boolean r6 = r1.find()
            if (r6 == 0) goto L_0x00a4
            r6 = 2
            java.lang.String r6 = r1.group(r6)     // Catch:{ NumberFormatException -> 0x008d }
            long r6 = java.lang.Long.parseLong(r6)     // Catch:{ NumberFormatException -> 0x008d }
            r8 = 1
            java.lang.String r1 = r1.group(r8)     // Catch:{ NumberFormatException -> 0x008d }
            long r8 = java.lang.Long.parseLong(r1)     // Catch:{ NumberFormatException -> 0x008d }
            long r6 = r6 - r8
            r8 = 1
            long r6 = r6 + r8
            r8 = 0
            int r1 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r1 >= 0) goto L_0x0064
            r4 = r6
            goto L_0x00a4
        L_0x0064:
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 == 0) goto L_0x00a4
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x008d }
            r1.<init>()     // Catch:{ NumberFormatException -> 0x008d }
            java.lang.String r8 = "Inconsistent headers ["
            r1.append(r8)     // Catch:{ NumberFormatException -> 0x008d }
            r1.append(r0)     // Catch:{ NumberFormatException -> 0x008d }
            java.lang.String r0 = "] ["
            r1.append(r0)     // Catch:{ NumberFormatException -> 0x008d }
            r1.append(r10)     // Catch:{ NumberFormatException -> 0x008d }
            r1.append(r2)     // Catch:{ NumberFormatException -> 0x008d }
            java.lang.String r0 = r1.toString()     // Catch:{ NumberFormatException -> 0x008d }
            com.google.android.exoplayer2.util.Log.d(r3, r0)     // Catch:{ NumberFormatException -> 0x008d }
            long r0 = java.lang.Math.max(r4, r6)     // Catch:{ NumberFormatException -> 0x008d }
            r4 = r0
            goto L_0x00a4
        L_0x008d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unexpected Content-Range ["
            r0.append(r1)
            r0.append(r10)
            r0.append(r2)
            java.lang.String r10 = r0.toString()
            com.google.android.exoplayer2.util.Log.b(r3, r10)
        L_0x00a4:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.upstream.DefaultHttpDataSource.a(java.net.HttpURLConnection):long");
    }

    private int a(byte[] bArr, int i2, int i3) throws IOException {
        if (i3 == 0) {
            return 0;
        }
        long j2 = this.q;
        if (j2 != -1) {
            long j3 = j2 - this.s;
            if (j3 == 0) {
                return -1;
            }
            i3 = (int) Math.min((long) i3, j3);
        }
        int read = this.n.read(bArr, i2, i3);
        if (read != -1) {
            this.s += (long) read;
            a(read);
            return read;
        } else if (this.q == -1) {
            return -1;
        } else {
            throw new EOFException();
        }
    }

    private static void a(HttpURLConnection httpURLConnection, long j2) {
        int i2 = Util.f3651a;
        if (i2 == 19 || i2 == 20) {
            try {
                InputStream inputStream = httpURLConnection.getInputStream();
                if (j2 == -1) {
                    if (inputStream.read() == -1) {
                        return;
                    }
                } else if (j2 <= PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) {
                    return;
                }
                String name = inputStream.getClass().getName();
                if ("com.android.okhttp.internal.http.HttpTransport$ChunkedInputStream".equals(name) || "com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream".equals(name)) {
                    Method declaredMethod = inputStream.getClass().getSuperclass().getDeclaredMethod("unexpectedEndOfInput", new Class[0]);
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(inputStream, new Object[0]);
                }
            } catch (Exception unused) {
            }
        }
    }
}
