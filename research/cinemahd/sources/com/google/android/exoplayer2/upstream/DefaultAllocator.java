package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;

public final class DefaultAllocator implements Allocator {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f3587a;
    private final int b;
    private final byte[] c;
    private final Allocation[] d;
    private int e;
    private int f;
    private int g;
    private Allocation[] h;

    public DefaultAllocator(boolean z, int i) {
        this(z, i, 0);
    }

    public synchronized void a(int i) {
        boolean z = i < this.e;
        this.e = i;
        if (z) {
            b();
        }
    }

    public synchronized void b() {
        int i = 0;
        int max = Math.max(0, Util.a(this.e, this.b) - this.f);
        if (max < this.g) {
            if (this.c != null) {
                int i2 = this.g - 1;
                while (i <= i2) {
                    Allocation allocation = this.h[i];
                    if (allocation.f3583a == this.c) {
                        i++;
                    } else {
                        Allocation allocation2 = this.h[i2];
                        if (allocation2.f3583a != this.c) {
                            i2--;
                        } else {
                            this.h[i] = allocation2;
                            this.h[i2] = allocation;
                            i2--;
                            i++;
                        }
                    }
                }
                max = Math.max(max, i);
                if (max >= this.g) {
                    return;
                }
            }
            Arrays.fill(this.h, max, this.g, (Object) null);
            this.g = max;
        }
    }

    public int c() {
        return this.b;
    }

    public synchronized int d() {
        return this.f * this.b;
    }

    public synchronized void e() {
        if (this.f3587a) {
            a(0);
        }
    }

    public DefaultAllocator(boolean z, int i, int i2) {
        Assertions.a(i > 0);
        Assertions.a(i2 >= 0);
        this.f3587a = z;
        this.b = i;
        this.g = i2;
        this.h = new Allocation[(i2 + 100)];
        if (i2 > 0) {
            this.c = new byte[(i2 * i)];
            for (int i3 = 0; i3 < i2; i3++) {
                this.h[i3] = new Allocation(this.c, i3 * i);
            }
        } else {
            this.c = null;
        }
        this.d = new Allocation[1];
    }

    public synchronized Allocation a() {
        Allocation allocation;
        this.f++;
        if (this.g > 0) {
            Allocation[] allocationArr = this.h;
            int i = this.g - 1;
            this.g = i;
            allocation = allocationArr[i];
            this.h[this.g] = null;
        } else {
            allocation = new Allocation(new byte[this.b], 0);
        }
        return allocation;
    }

    public synchronized void a(Allocation allocation) {
        this.d[0] = allocation;
        a(this.d);
    }

    public synchronized void a(Allocation[] allocationArr) {
        if (this.g + allocationArr.length >= this.h.length) {
            this.h = (Allocation[]) Arrays.copyOf(this.h, Math.max(this.h.length * 2, this.g + allocationArr.length));
        }
        for (Allocation allocation : allocationArr) {
            Allocation[] allocationArr2 = this.h;
            int i = this.g;
            this.g = i + 1;
            allocationArr2[i] = allocation;
        }
        this.f -= allocationArr.length;
        notifyAll();
    }
}
