package com.google.android.exoplayer2.upstream;

import android.text.TextUtils;
import com.google.android.exoplayer2.util.Util;

/* compiled from: HttpDataSource */
public final /* synthetic */ class e {
    public static /* synthetic */ boolean a(String str) {
        String k = Util.k(str);
        return !TextUtils.isEmpty(k) && (!k.contains("text") || k.contains("text/vtt")) && !k.contains("html") && !k.contains("xml");
    }
}
