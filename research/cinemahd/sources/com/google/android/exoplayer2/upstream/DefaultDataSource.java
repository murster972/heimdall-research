package com.google.android.exoplayer2.upstream;

import android.content.Context;
import android.net.Uri;
import com.facebook.common.util.UriUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class DefaultDataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3590a;
    private final List<TransferListener> b = new ArrayList();
    private final DataSource c;
    private DataSource d;
    private DataSource e;
    private DataSource f;
    private DataSource g;
    private DataSource h;
    private DataSource i;
    private DataSource j;

    public DefaultDataSource(Context context, DataSource dataSource) {
        this.f3590a = context.getApplicationContext();
        Assertions.a(dataSource);
        this.c = dataSource;
    }

    private DataSource b() {
        if (this.e == null) {
            this.e = new AssetDataSource(this.f3590a);
            a(this.e);
        }
        return this.e;
    }

    private DataSource c() {
        if (this.f == null) {
            this.f = new ContentDataSource(this.f3590a);
            a(this.f);
        }
        return this.f;
    }

    private DataSource d() {
        if (this.h == null) {
            this.h = new DataSchemeDataSource();
            a(this.h);
        }
        return this.h;
    }

    private DataSource e() {
        if (this.d == null) {
            this.d = new FileDataSource();
            a(this.d);
        }
        return this.d;
    }

    private DataSource f() {
        if (this.i == null) {
            this.i = new RawResourceDataSource(this.f3590a);
            a(this.i);
        }
        return this.i;
    }

    private DataSource g() {
        if (this.g == null) {
            try {
                this.g = (DataSource) Class.forName("com.google.android.exoplayer2.ext.rtmp.RtmpDataSource").getConstructor(new Class[0]).newInstance(new Object[0]);
                a(this.g);
            } catch (ClassNotFoundException unused) {
                Log.d("DefaultDataSource", "Attempting to play RTMP stream without depending on the RTMP extension");
            } catch (Exception e2) {
                throw new RuntimeException("Error instantiating RTMP extension", e2);
            }
            if (this.g == null) {
                this.g = this.c;
            }
        }
        return this.g;
    }

    public void a(TransferListener transferListener) {
        this.c.a(transferListener);
        this.b.add(transferListener);
        a(this.d, transferListener);
        a(this.e, transferListener);
        a(this.f, transferListener);
        a(this.g, transferListener);
        a(this.h, transferListener);
        a(this.i, transferListener);
    }

    public void close() throws IOException {
        DataSource dataSource = this.j;
        if (dataSource != null) {
            try {
                dataSource.close();
            } finally {
                this.j = null;
            }
        }
    }

    public Uri getUri() {
        DataSource dataSource = this.j;
        if (dataSource == null) {
            return null;
        }
        return dataSource.getUri();
    }

    public int read(byte[] bArr, int i2, int i3) throws IOException {
        DataSource dataSource = this.j;
        Assertions.a(dataSource);
        return dataSource.read(bArr, i2, i3);
    }

    public long a(DataSpec dataSpec) throws IOException {
        Assertions.b(this.j == null);
        String scheme = dataSpec.f3586a.getScheme();
        if (Util.b(dataSpec.f3586a)) {
            if (dataSpec.f3586a.getPath().startsWith("/android_asset/")) {
                this.j = b();
            } else {
                this.j = e();
            }
        } else if (UriUtil.LOCAL_ASSET_SCHEME.equals(scheme)) {
            this.j = b();
        } else if ("content".equals(scheme)) {
            this.j = c();
        } else if ("rtmp".equals(scheme)) {
            this.j = g();
        } else if (UriUtil.DATA_SCHEME.equals(scheme)) {
            this.j = d();
        } else if ("rawresource".equals(scheme)) {
            this.j = f();
        } else {
            this.j = this.c;
        }
        return this.j.a(dataSpec);
    }

    public Map<String, List<String>> a() {
        DataSource dataSource = this.j;
        return dataSource == null ? Collections.emptyMap() : dataSource.a();
    }

    private void a(DataSource dataSource) {
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            dataSource.a(this.b.get(i2));
        }
    }

    private void a(DataSource dataSource, TransferListener transferListener) {
        if (dataSource != null) {
            dataSource.a(transferListener);
        }
    }
}
