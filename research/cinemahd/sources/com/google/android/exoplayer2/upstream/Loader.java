package com.google.android.exoplayer2.upstream;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

public final class Loader implements LoaderErrorThrower {
    public static final LoadErrorAction d = a(false, -9223372036854775807L);
    public static final LoadErrorAction e = new LoadErrorAction(2, -9223372036854775807L);
    public static final LoadErrorAction f = new LoadErrorAction(3, -9223372036854775807L);
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final ExecutorService f3597a;
    /* access modifiers changed from: private */
    public LoadTask<? extends Loadable> b;
    /* access modifiers changed from: private */
    public IOException c;

    public interface Callback<T extends Loadable> {
        LoadErrorAction a(T t, long j, long j2, IOException iOException, int i);

        void a(T t, long j, long j2);

        void a(T t, long j, long j2, boolean z);
    }

    public static final class LoadErrorAction {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final int f3598a;
        /* access modifiers changed from: private */
        public final long b;

        private LoadErrorAction(int i, long j) {
            this.f3598a = i;
            this.b = j;
        }

        public boolean a() {
            int i = this.f3598a;
            return i == 0 || i == 1;
        }
    }

    public interface Loadable {
        void cancelLoad();

        void load() throws IOException, InterruptedException;
    }

    public interface ReleaseCallback {
        void g();
    }

    private static final class ReleaseTask implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final ReleaseCallback f3600a;

        public ReleaseTask(ReleaseCallback releaseCallback) {
            this.f3600a = releaseCallback;
        }

        public void run() {
            this.f3600a.g();
        }
    }

    public static final class UnexpectedLoaderException extends IOException {
        public UnexpectedLoaderException(Throwable th) {
            super("Unexpected " + th.getClass().getSimpleName() + ": " + th.getMessage(), th);
        }
    }

    static {
        a(true, -9223372036854775807L);
    }

    public Loader(String str) {
        this.f3597a = Util.f(str);
    }

    public boolean c() {
        return this.b != null;
    }

    public void d() {
        a((ReleaseCallback) null);
    }

    @SuppressLint({"HandlerLeak"})
    private final class LoadTask<T extends Loadable> extends Handler implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final int f3599a;
        private final T b;
        private final long c;
        private Callback<T> d;
        private IOException e;
        private int f;
        private volatile Thread g;
        private volatile boolean h;
        private volatile boolean i;

        public LoadTask(Looper looper, T t, Callback<T> callback, int i2, long j2) {
            super(looper);
            this.b = t;
            this.d = callback;
            this.f3599a = i2;
            this.c = j2;
        }

        private void b() {
            LoadTask unused = Loader.this.b = null;
        }

        private long c() {
            return (long) Math.min((this.f - 1) * 1000, 5000);
        }

        public void a(int i2) throws IOException {
            IOException iOException = this.e;
            if (iOException != null && this.f > i2) {
                throw iOException;
            }
        }

        public void handleMessage(Message message) {
            long j2;
            if (!this.i) {
                int i2 = message.what;
                if (i2 == 0) {
                    a();
                } else if (i2 != 4) {
                    b();
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    long j3 = elapsedRealtime - this.c;
                    if (this.h) {
                        this.d.a(this.b, elapsedRealtime, j3, false);
                        return;
                    }
                    int i3 = message.what;
                    if (i3 == 1) {
                        this.d.a(this.b, elapsedRealtime, j3, false);
                    } else if (i3 == 2) {
                        try {
                            this.d.a(this.b, elapsedRealtime, j3);
                        } catch (RuntimeException e2) {
                            Log.a("LoadTask", "Unexpected exception handling load completed", e2);
                            IOException unused = Loader.this.c = new UnexpectedLoaderException(e2);
                        }
                    } else if (i3 == 3) {
                        this.e = (IOException) message.obj;
                        this.f++;
                        LoadErrorAction a2 = this.d.a(this.b, elapsedRealtime, j3, this.e, this.f);
                        if (a2.f3598a == 3) {
                            IOException unused2 = Loader.this.c = this.e;
                        } else if (a2.f3598a != 2) {
                            if (a2.f3598a == 1) {
                                this.f = 1;
                            }
                            if (a2.b != -9223372036854775807L) {
                                j2 = a2.b;
                            } else {
                                j2 = c();
                            }
                            a(j2);
                        }
                    }
                } else {
                    throw ((Error) message.obj);
                }
            }
        }

        public void run() {
            try {
                this.g = Thread.currentThread();
                if (!this.h) {
                    TraceUtil.a("load:" + this.b.getClass().getSimpleName());
                    this.b.load();
                    TraceUtil.a();
                }
                if (!this.i) {
                    sendEmptyMessage(2);
                }
            } catch (IOException e2) {
                if (!this.i) {
                    obtainMessage(3, e2).sendToTarget();
                }
            } catch (InterruptedException unused) {
                Assertions.b(this.h);
                if (!this.i) {
                    sendEmptyMessage(2);
                }
            } catch (Exception e3) {
                Log.a("LoadTask", "Unexpected exception loading stream", e3);
                if (!this.i) {
                    obtainMessage(3, new UnexpectedLoaderException(e3)).sendToTarget();
                }
            } catch (OutOfMemoryError e4) {
                Log.a("LoadTask", "OutOfMemory error loading stream", e4);
                if (!this.i) {
                    obtainMessage(3, new UnexpectedLoaderException(e4)).sendToTarget();
                }
            } catch (Error e5) {
                Log.a("LoadTask", "Unexpected error loading stream", e5);
                if (!this.i) {
                    obtainMessage(4, e5).sendToTarget();
                }
                throw e5;
            } catch (Throwable th) {
                TraceUtil.a();
                throw th;
            }
        }

        public void a(long j2) {
            Assertions.b(Loader.this.b == null);
            LoadTask unused = Loader.this.b = this;
            if (j2 > 0) {
                sendEmptyMessageDelayed(0, j2);
            } else {
                a();
            }
        }

        public void a(boolean z) {
            this.i = z;
            this.e = null;
            if (hasMessages(0)) {
                removeMessages(0);
                if (!z) {
                    sendEmptyMessage(1);
                }
            } else {
                this.h = true;
                this.b.cancelLoad();
                if (this.g != null) {
                    this.g.interrupt();
                }
            }
            if (z) {
                b();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                this.d.a(this.b, elapsedRealtime, elapsedRealtime - this.c, true);
                this.d = null;
            }
        }

        private void a() {
            this.e = null;
            Loader.this.f3597a.execute(Loader.this.b);
        }
    }

    public void b() {
        this.b.a(false);
    }

    public static LoadErrorAction a(boolean z, long j) {
        return new LoadErrorAction(z ? 1 : 0, j);
    }

    public <T extends Loadable> long a(T t, Callback<T> callback, int i) {
        Looper myLooper = Looper.myLooper();
        Assertions.b(myLooper != null);
        this.c = null;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        new LoadTask(myLooper, t, callback, i, elapsedRealtime).a(0);
        return elapsedRealtime;
    }

    public void a(ReleaseCallback releaseCallback) {
        LoadTask<? extends Loadable> loadTask = this.b;
        if (loadTask != null) {
            loadTask.a(true);
        }
        if (releaseCallback != null) {
            this.f3597a.execute(new ReleaseTask(releaseCallback));
        }
        this.f3597a.shutdown();
    }

    public void a() throws IOException {
        a(Integer.MIN_VALUE);
    }

    public void a(int i) throws IOException {
        IOException iOException = this.c;
        if (iOException == null) {
            LoadTask<? extends Loadable> loadTask = this.b;
            if (loadTask != null) {
                if (i == Integer.MIN_VALUE) {
                    i = loadTask.f3599a;
                }
                loadTask.a(i);
                return;
            }
            return;
        }
        throw iOException;
    }
}
