package com.google.android.exoplayer2.upstream;

import android.content.Context;
import com.google.android.exoplayer2.upstream.DataSource;

public final class DefaultDataSourceFactory implements DataSource.Factory {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3591a;
    private final TransferListener b;
    private final DataSource.Factory c;

    public DefaultDataSourceFactory(Context context, String str) {
        this(context, str, (TransferListener) null);
    }

    public DefaultDataSourceFactory(Context context, String str, TransferListener transferListener) {
        this(context, transferListener, (DataSource.Factory) new DefaultHttpDataSourceFactory(str, transferListener));
    }

    public DefaultDataSource a() {
        DefaultDataSource defaultDataSource = new DefaultDataSource(this.f3591a, this.c.a());
        TransferListener transferListener = this.b;
        if (transferListener != null) {
            defaultDataSource.a(transferListener);
        }
        return defaultDataSource;
    }

    public DefaultDataSourceFactory(Context context, DataSource.Factory factory) {
        this(context, (TransferListener) null, factory);
    }

    public DefaultDataSourceFactory(Context context, TransferListener transferListener, DataSource.Factory factory) {
        this.f3591a = context.getApplicationContext();
        this.b = transferListener;
        this.c = factory;
    }
}
