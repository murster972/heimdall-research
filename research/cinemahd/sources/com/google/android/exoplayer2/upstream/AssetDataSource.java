package com.google.android.exoplayer2.upstream;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public final class AssetDataSource extends BaseDataSource {
    private final AssetManager e;
    private Uri f;
    private InputStream g;
    private long h;
    private boolean i;

    public static final class AssetDataSourceException extends IOException {
        public AssetDataSourceException(IOException iOException) {
            super(iOException);
        }
    }

    public AssetDataSource(Context context) {
        super(false);
        this.e = context.getAssets();
    }

    public long a(DataSpec dataSpec) throws AssetDataSourceException {
        try {
            this.f = dataSpec.f3586a;
            String path = this.f.getPath();
            if (path.startsWith("/android_asset/")) {
                path = path.substring(15);
            } else if (path.startsWith("/")) {
                path = path.substring(1);
            }
            b(dataSpec);
            this.g = this.e.open(path, 1);
            if (this.g.skip(dataSpec.e) >= dataSpec.e) {
                if (dataSpec.f != -1) {
                    this.h = dataSpec.f;
                } else {
                    this.h = (long) this.g.available();
                    if (this.h == 2147483647L) {
                        this.h = -1;
                    }
                }
                this.i = true;
                c(dataSpec);
                return this.h;
            }
            throw new EOFException();
        } catch (IOException e2) {
            throw new AssetDataSourceException(e2);
        }
    }

    public void close() throws AssetDataSourceException {
        this.f = null;
        try {
            if (this.g != null) {
                this.g.close();
            }
            this.g = null;
            if (this.i) {
                this.i = false;
                b();
            }
        } catch (IOException e2) {
            throw new AssetDataSourceException(e2);
        } catch (Throwable th) {
            this.g = null;
            if (this.i) {
                this.i = false;
                b();
            }
            throw th;
        }
    }

    public Uri getUri() {
        return this.f;
    }

    public int read(byte[] bArr, int i2, int i3) throws AssetDataSourceException {
        if (i3 == 0) {
            return 0;
        }
        long j = this.h;
        if (j == 0) {
            return -1;
        }
        if (j != -1) {
            try {
                i3 = (int) Math.min(j, (long) i3);
            } catch (IOException e2) {
                throw new AssetDataSourceException(e2);
            }
        }
        int read = this.g.read(bArr, i2, i3);
        if (read != -1) {
            long j2 = this.h;
            if (j2 != -1) {
                this.h = j2 - ((long) read);
            }
            a(read);
            return read;
        } else if (this.h == -1) {
            return -1;
        } else {
            throw new AssetDataSourceException(new EOFException());
        }
    }
}
