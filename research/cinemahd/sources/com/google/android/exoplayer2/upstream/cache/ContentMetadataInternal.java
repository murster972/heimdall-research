package com.google.android.exoplayer2.upstream.cache;

import android.net.Uri;

final class ContentMetadataInternal {
    private ContentMetadataInternal() {
    }

    public static long a(ContentMetadata contentMetadata) {
        return contentMetadata.a("exo_len", -1);
    }

    public static Uri b(ContentMetadata contentMetadata) {
        String str = contentMetadata.get("exo_redir", (String) null);
        if (str == null) {
            return null;
        }
        return Uri.parse(str);
    }

    public static void a(ContentMetadataMutations contentMetadataMutations, long j) {
        contentMetadataMutations.a("exo_len", j);
    }

    public static void a(ContentMetadataMutations contentMetadataMutations, Uri uri) {
        contentMetadataMutations.a("exo_redir", uri.toString());
    }

    public static void a(ContentMetadataMutations contentMetadataMutations) {
        contentMetadataMutations.a("exo_redir");
    }
}
