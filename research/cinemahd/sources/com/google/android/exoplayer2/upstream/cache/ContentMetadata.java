package com.google.android.exoplayer2.upstream.cache;

public interface ContentMetadata {
    long a(String str, long j);

    String get(String str, String str2);
}
