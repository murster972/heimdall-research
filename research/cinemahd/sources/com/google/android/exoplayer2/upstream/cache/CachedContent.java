package com.google.android.exoplayer2.upstream.cache;

import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Assertions;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.TreeSet;

final class CachedContent {

    /* renamed from: a  reason: collision with root package name */
    public final int f3614a;
    public final String b;
    private final TreeSet<SimpleCacheSpan> c = new TreeSet<>();
    private DefaultContentMetadata d = DefaultContentMetadata.c;
    private boolean e;

    public CachedContent(int i, String str) {
        this.f3614a = i;
        this.b = str;
    }

    public static CachedContent a(int i, DataInputStream dataInputStream) throws IOException {
        CachedContent cachedContent = new CachedContent(dataInputStream.readInt(), dataInputStream.readUTF());
        if (i < 2) {
            long readLong = dataInputStream.readLong();
            ContentMetadataMutations contentMetadataMutations = new ContentMetadataMutations();
            ContentMetadataInternal.a(contentMetadataMutations, readLong);
            cachedContent.a(contentMetadataMutations);
        } else {
            cachedContent.d = DefaultContentMetadata.a(dataInputStream);
        }
        return cachedContent;
    }

    public TreeSet<SimpleCacheSpan> b() {
        return this.c;
    }

    public boolean c() {
        return this.c.isEmpty();
    }

    public boolean d() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || CachedContent.class != obj.getClass()) {
            return false;
        }
        CachedContent cachedContent = (CachedContent) obj;
        if (this.f3614a != cachedContent.f3614a || !this.b.equals(cachedContent.b) || !this.c.equals(cachedContent.c) || !this.d.equals(cachedContent.d)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (a(Integer.MAX_VALUE) * 31) + this.c.hashCode();
    }

    public SimpleCacheSpan b(SimpleCacheSpan simpleCacheSpan) throws Cache.CacheException {
        SimpleCacheSpan a2 = simpleCacheSpan.a(this.f3614a);
        if (simpleCacheSpan.e.renameTo(a2.e)) {
            Assertions.b(this.c.remove(simpleCacheSpan));
            this.c.add(a2);
            return a2;
        }
        throw new Cache.CacheException("Renaming of " + simpleCacheSpan.e + " to " + a2.e + " failed.");
    }

    public void a(DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeInt(this.f3614a);
        dataOutputStream.writeUTF(this.b);
        this.d.a(dataOutputStream);
    }

    public ContentMetadata a() {
        return this.d;
    }

    public boolean a(ContentMetadataMutations contentMetadataMutations) {
        DefaultContentMetadata defaultContentMetadata = this.d;
        this.d = defaultContentMetadata.a(contentMetadataMutations);
        return !this.d.equals(defaultContentMetadata);
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void a(SimpleCacheSpan simpleCacheSpan) {
        this.c.add(simpleCacheSpan);
    }

    public SimpleCacheSpan a(long j) {
        SimpleCacheSpan a2 = SimpleCacheSpan.a(this.b, j);
        SimpleCacheSpan floor = this.c.floor(a2);
        if (floor != null && floor.b + floor.c > j) {
            return floor;
        }
        SimpleCacheSpan ceiling = this.c.ceiling(a2);
        if (ceiling == null) {
            return SimpleCacheSpan.b(this.b, j);
        }
        return SimpleCacheSpan.a(this.b, j, ceiling.b - j);
    }

    public long a(long j, long j2) {
        SimpleCacheSpan a2 = a(j);
        if (a2.a()) {
            return -Math.min(a2.b() ? Clock.MAX_TIME : a2.c, j2);
        }
        long j3 = j + j2;
        long j4 = a2.b + a2.c;
        if (j4 < j3) {
            for (SimpleCacheSpan next : this.c.tailSet(a2, false)) {
                long j5 = next.b;
                if (j5 <= j4) {
                    j4 = Math.max(j4, j5 + next.c);
                    if (j4 >= j3) {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return Math.min(j4 - j, j2);
    }

    public boolean a(CacheSpan cacheSpan) {
        if (!this.c.remove(cacheSpan)) {
            return false;
        }
        cacheSpan.e.delete();
        return true;
    }

    public int a(int i) {
        int i2;
        int i3;
        int hashCode = (this.f3614a * 31) + this.b.hashCode();
        if (i < 2) {
            long a2 = ContentMetadataInternal.a((ContentMetadata) this.d);
            i3 = hashCode * 31;
            i2 = (int) (a2 ^ (a2 >>> 32));
        } else {
            i3 = hashCode * 31;
            i2 = this.d.hashCode();
        }
        return i3 + i2;
    }
}
