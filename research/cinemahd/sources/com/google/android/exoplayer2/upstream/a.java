package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.EventDispatcher;

/* compiled from: lambda */
public final /* synthetic */ class a implements EventDispatcher.Event {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ int f3605a;
    private final /* synthetic */ long b;
    private final /* synthetic */ long c;

    public /* synthetic */ a(int i, long j, long j2) {
        this.f3605a = i;
        this.b = j;
        this.c = j2;
    }

    public final void a(Object obj) {
        ((BandwidthMeter.EventListener) obj).b(this.f3605a, this.b, this.c);
    }
}
