package com.google.android.exoplayer2.upstream.cache;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DefaultContentMetadata implements ContentMetadata {
    public static final DefaultContentMetadata c = new DefaultContentMetadata(Collections.emptyMap());

    /* renamed from: a  reason: collision with root package name */
    private int f3617a;
    private final Map<String, byte[]> b;

    private DefaultContentMetadata(Map<String, byte[]> map) {
        this.b = Collections.unmodifiableMap(map);
    }

    public static DefaultContentMetadata a(DataInputStream dataInputStream) throws IOException {
        int readInt = dataInputStream.readInt();
        HashMap hashMap = new HashMap();
        for (int i = 0; i < readInt; i++) {
            String readUTF = dataInputStream.readUTF();
            int readInt2 = dataInputStream.readInt();
            if (readInt2 < 0 || readInt2 > 10485760) {
                throw new IOException("Invalid value size: " + readInt2);
            }
            byte[] bArr = new byte[readInt2];
            dataInputStream.readFully(bArr);
            hashMap.put(readUTF, bArr);
        }
        return new DefaultContentMetadata(hashMap);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || DefaultContentMetadata.class != obj.getClass()) {
            return false;
        }
        return a(((DefaultContentMetadata) obj).b);
    }

    public final String get(String str, String str2) {
        return this.b.containsKey(str) ? new String(this.b.get(str), Charset.forName("UTF-8")) : str2;
    }

    public int hashCode() {
        if (this.f3617a == 0) {
            int i = 0;
            for (Map.Entry next : this.b.entrySet()) {
                i += Arrays.hashCode((byte[]) next.getValue()) ^ ((String) next.getKey()).hashCode();
            }
            this.f3617a = i;
        }
        return this.f3617a;
    }

    public DefaultContentMetadata a(ContentMetadataMutations contentMetadataMutations) {
        Map<String, byte[]> a2 = a(this.b, contentMetadataMutations);
        if (a(a2)) {
            return this;
        }
        return new DefaultContentMetadata(a2);
    }

    public void a(DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeInt(this.b.size());
        for (Map.Entry next : this.b.entrySet()) {
            dataOutputStream.writeUTF((String) next.getKey());
            byte[] bArr = (byte[]) next.getValue();
            dataOutputStream.writeInt(bArr.length);
            dataOutputStream.write(bArr);
        }
    }

    public final long a(String str, long j) {
        return this.b.containsKey(str) ? ByteBuffer.wrap(this.b.get(str)).getLong() : j;
    }

    private boolean a(Map<String, byte[]> map) {
        if (this.b.size() != map.size()) {
            return false;
        }
        for (Map.Entry next : this.b.entrySet()) {
            if (!Arrays.equals((byte[]) next.getValue(), map.get(next.getKey()))) {
                return false;
            }
        }
        return true;
    }

    private static Map<String, byte[]> a(Map<String, byte[]> map, ContentMetadataMutations contentMetadataMutations) {
        HashMap hashMap = new HashMap(map);
        a((HashMap<String, byte[]>) hashMap, contentMetadataMutations.b());
        a((HashMap<String, byte[]>) hashMap, contentMetadataMutations.a());
        return hashMap;
    }

    private static void a(HashMap<String, byte[]> hashMap, List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            hashMap.remove(list.get(i));
        }
    }

    private static void a(HashMap<String, byte[]> hashMap, Map<String, Object> map) {
        for (String next : map.keySet()) {
            byte[] a2 = a(map.get(next));
            if (a2.length <= 10485760) {
                hashMap.put(next, a2);
            } else {
                throw new IllegalArgumentException("The size of " + next + " (" + a2.length + ") is greater than maximum allowed: " + 10485760);
            }
        }
    }

    private static byte[] a(Object obj) {
        if (obj instanceof Long) {
            return ByteBuffer.allocate(8).putLong(((Long) obj).longValue()).array();
        }
        if (obj instanceof String) {
            return ((String) obj).getBytes(Charset.forName("UTF-8"));
        }
        if (obj instanceof byte[]) {
            return (byte[]) obj;
        }
        throw new IllegalArgumentException();
    }
}
