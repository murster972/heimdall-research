package com.google.android.exoplayer2.upstream.cache;

import com.google.android.exoplayer2.upstream.DataSink;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;

public final class CacheDataSourceFactory implements DataSource.Factory {

    /* renamed from: a  reason: collision with root package name */
    private final Cache f3610a;
    private final DataSource.Factory b;
    private final DataSource.Factory c;
    private final DataSink.Factory d;
    private final int e;
    private final CacheDataSource.EventListener f;

    public CacheDataSourceFactory(Cache cache, DataSource.Factory factory, DataSource.Factory factory2, DataSink.Factory factory3, int i, CacheDataSource.EventListener eventListener) {
        this.f3610a = cache;
        this.b = factory;
        this.c = factory2;
        this.d = factory3;
        this.e = i;
        this.f = eventListener;
    }

    public CacheDataSource a() {
        Cache cache = this.f3610a;
        DataSource a2 = this.b.a();
        DataSource a3 = this.c.a();
        DataSink.Factory factory = this.d;
        return new CacheDataSource(cache, a2, a3, factory != null ? factory.a() : null, this.e, this.f);
    }
}
