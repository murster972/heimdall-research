package com.google.android.exoplayer2.upstream.cache;

import android.net.Uri;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import java.io.EOFException;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public final class CacheUtil {

    /* renamed from: a  reason: collision with root package name */
    public static final CacheKeyFactory f3612a = a.f3620a;

    public static class CachingCounters {

        /* renamed from: a  reason: collision with root package name */
        public volatile long f3613a;
        public volatile long b;
        public volatile long c = -1;

        public long a() {
            return this.f3613a + this.b;
        }
    }

    private CacheUtil() {
    }

    public static String a(Uri uri) {
        return uri.toString();
    }

    public static String a(DataSpec dataSpec) {
        String str = dataSpec.g;
        return str != null ? str : a(dataSpec.f3586a);
    }

    public static void a(DataSpec dataSpec, Cache cache, CachingCounters cachingCounters) {
        DataSpec dataSpec2 = dataSpec;
        CachingCounters cachingCounters2 = cachingCounters;
        String a2 = a(dataSpec);
        long j = dataSpec2.d;
        long j2 = dataSpec2.f;
        if (j2 != -1) {
            Cache cache2 = cache;
        } else {
            j2 = cache.b(a2);
        }
        cachingCounters2.c = j2;
        cachingCounters2.f3613a = 0;
        cachingCounters2.b = 0;
        long j3 = j;
        long j4 = j2;
        while (j4 != 0) {
            int i = (j4 > -1 ? 1 : (j4 == -1 ? 0 : -1));
            long b = cache.b(a2, j3, i != 0 ? j4 : Long.MAX_VALUE);
            if (b > 0) {
                cachingCounters2.f3613a += b;
            } else {
                b = -b;
                if (b == Clock.MAX_TIME) {
                    return;
                }
            }
            j3 += b;
            if (i == 0) {
                b = 0;
            }
            j4 -= b;
        }
    }

    public static void a(DataSpec dataSpec, Cache cache, CacheDataSource cacheDataSource, byte[] bArr, PriorityTaskManager priorityTaskManager, int i, CachingCounters cachingCounters, AtomicBoolean atomicBoolean, boolean z) throws IOException, InterruptedException {
        long j;
        DataSpec dataSpec2 = dataSpec;
        Cache cache2 = cache;
        CachingCounters cachingCounters2 = cachingCounters;
        Assertions.a(cacheDataSource);
        Assertions.a(bArr);
        if (cachingCounters2 != null) {
            a(dataSpec2, cache2, cachingCounters2);
        } else {
            cachingCounters2 = new CachingCounters();
        }
        CachingCounters cachingCounters3 = cachingCounters2;
        String a2 = a(dataSpec);
        long j2 = dataSpec2.d;
        long j3 = dataSpec2.f;
        if (j3 == -1) {
            j3 = cache2.b(a2);
        }
        long j4 = j2;
        long j5 = j3;
        while (j5 != 0) {
            a(atomicBoolean);
            int i2 = (j5 > -1 ? 1 : (j5 == -1 ? 0 : -1));
            long b = cache.b(a2, j4, i2 != 0 ? j5 : Long.MAX_VALUE);
            if (b > 0) {
                j = b;
            } else {
                long j6 = -b;
                j = j6;
                if (a(dataSpec, j4, j6, (DataSource) cacheDataSource, bArr, priorityTaskManager, i, cachingCounters3, atomicBoolean) < j) {
                    if (z && i2 != 0) {
                        throw new EOFException();
                    }
                    return;
                }
            }
            j4 += j;
            if (i2 == 0) {
                j = 0;
            }
            j5 -= j;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0083, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0084, code lost:
        com.google.android.exoplayer2.util.Util.a(r22);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0087, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0088, code lost:
        r17 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0083 A[ExcHandler: all (r0v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x000d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long a(com.google.android.exoplayer2.upstream.DataSpec r17, long r18, long r20, com.google.android.exoplayer2.upstream.DataSource r22, byte[] r23, com.google.android.exoplayer2.util.PriorityTaskManager r24, int r25, com.google.android.exoplayer2.upstream.cache.CacheUtil.CachingCounters r26, java.util.concurrent.atomic.AtomicBoolean r27) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r1 = r22
            r0 = r23
            r2 = r26
            r3 = r17
        L_0x0008:
            if (r24 == 0) goto L_0x000d
            r24.b(r25)
        L_0x000d:
            a((java.util.concurrent.atomic.AtomicBoolean) r27)     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            com.google.android.exoplayer2.upstream.DataSpec r15 = new com.google.android.exoplayer2.upstream.DataSpec     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            android.net.Uri r5 = r3.f3586a     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            int r6 = r3.b     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            byte[] r7 = r3.c     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            long r8 = r3.e     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            long r8 = r8 + r18
            long r10 = r3.d     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            long r10 = r8 - r10
            r12 = -1
            java.lang.String r14 = r3.g     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            int r4 = r3.h     // Catch:{ PriorityTooLowException -> 0x0088, all -> 0x0083 }
            r16 = r4 | 2
            r4 = r15
            r8 = r18
            r17 = r3
            r3 = r15
            r15 = r16
            r4.<init>(r5, r6, r7, r8, r10, r12, r14, r15)     // Catch:{ PriorityTooLowException -> 0x0080, all -> 0x0083 }
            long r4 = r1.a((com.google.android.exoplayer2.upstream.DataSpec) r3)     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            long r6 = r2.c     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            r8 = -1
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 != 0) goto L_0x0048
            int r6 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r6 == 0) goto L_0x0048
            long r6 = r3.d     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            long r6 = r6 + r4
            r2.c = r6     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
        L_0x0048:
            r4 = 0
        L_0x004a:
            int r6 = (r4 > r20 ? 1 : (r4 == r20 ? 0 : -1))
            if (r6 == 0) goto L_0x007c
            a((java.util.concurrent.atomic.AtomicBoolean) r27)     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            r6 = 0
            int r7 = (r20 > r8 ? 1 : (r20 == r8 ? 0 : -1))
            if (r7 == 0) goto L_0x0060
            int r7 = r0.length     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            long r10 = (long) r7     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            long r12 = r20 - r4
            long r10 = java.lang.Math.min(r10, r12)     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            int r7 = (int) r10     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            goto L_0x0061
        L_0x0060:
            int r7 = r0.length     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
        L_0x0061:
            int r6 = r1.read(r0, r6, r7)     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            r7 = -1
            if (r6 != r7) goto L_0x0074
            long r6 = r2.c     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 != 0) goto L_0x007c
            long r6 = r3.d     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            long r6 = r6 + r4
            r2.c = r6     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            goto L_0x007c
        L_0x0074:
            long r6 = (long) r6     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            long r4 = r4 + r6
            long r10 = r2.b     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            long r10 = r10 + r6
            r2.b = r10     // Catch:{ PriorityTooLowException -> 0x008a, all -> 0x0083 }
            goto L_0x004a
        L_0x007c:
            com.google.android.exoplayer2.util.Util.a((com.google.android.exoplayer2.upstream.DataSource) r22)
            return r4
        L_0x0080:
            r3 = r17
            goto L_0x008a
        L_0x0083:
            r0 = move-exception
            com.google.android.exoplayer2.util.Util.a((com.google.android.exoplayer2.upstream.DataSource) r22)
            throw r0
        L_0x0088:
            r17 = r3
        L_0x008a:
            com.google.android.exoplayer2.util.Util.a((com.google.android.exoplayer2.upstream.DataSource) r22)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.upstream.cache.CacheUtil.a(com.google.android.exoplayer2.upstream.DataSpec, long, long, com.google.android.exoplayer2.upstream.DataSource, byte[], com.google.android.exoplayer2.util.PriorityTaskManager, int, com.google.android.exoplayer2.upstream.cache.CacheUtil$CachingCounters, java.util.concurrent.atomic.AtomicBoolean):long");
    }

    public static void a(Cache cache, String str) {
        for (CacheSpan b : cache.c(str)) {
            try {
                cache.b(b);
            } catch (Cache.CacheException unused) {
            }
        }
    }

    private static void a(AtomicBoolean atomicBoolean) throws InterruptedException {
        if (Thread.interrupted() || (atomicBoolean != null && atomicBoolean.get())) {
            throw new InterruptedException();
        }
    }
}
