package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public final class PriorityDataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    private final DataSource f3602a;
    private final PriorityTaskManager b;
    private final int c;

    public PriorityDataSource(DataSource dataSource, PriorityTaskManager priorityTaskManager, int i) {
        Assertions.a(dataSource);
        this.f3602a = dataSource;
        Assertions.a(priorityTaskManager);
        this.b = priorityTaskManager;
        this.c = i;
    }

    public void a(TransferListener transferListener) {
        this.f3602a.a(transferListener);
    }

    public void close() throws IOException {
        this.f3602a.close();
    }

    public Uri getUri() {
        return this.f3602a.getUri();
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        this.b.c(this.c);
        return this.f3602a.read(bArr, i, i2);
    }

    public long a(DataSpec dataSpec) throws IOException {
        this.b.c(this.c);
        return this.f3602a.a(dataSpec);
    }

    public Map<String, List<String>> a() {
        return this.f3602a.a();
    }
}
