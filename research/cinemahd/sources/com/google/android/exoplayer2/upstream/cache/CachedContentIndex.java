package com.google.android.exoplayer2.upstream.cache;

import android.util.SparseArray;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.AtomicFile;
import com.google.android.exoplayer2.util.ReusableBufferedOutputStream;
import com.google.android.exoplayer2.util.Util;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class CachedContentIndex {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<String, CachedContent> f3615a;
    private final SparseArray<String> b;
    private final AtomicFile c;
    private final Cipher d;
    private final SecretKeySpec e;
    private final boolean f;
    private boolean g;
    private ReusableBufferedOutputStream h;

    public CachedContentIndex(File file, byte[] bArr, boolean z) {
        this.f = z;
        boolean z2 = true;
        if (bArr != null) {
            Assertions.a(bArr.length != 16 ? false : z2);
            try {
                this.d = e();
                this.e = new SecretKeySpec(bArr, "AES");
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e2) {
                throw new IllegalStateException(e2);
            }
        } else {
            Assertions.b(!z);
            this.d = null;
            this.e = null;
        }
        this.f3615a = new HashMap<>();
        this.b = new SparseArray<>();
        this.c = new AtomicFile(new File(file, "cached_content_index.exi"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean f() {
        /*
            r9 = this;
            r0 = 0
            r1 = 0
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x009f, all -> 0x0097 }
            com.google.android.exoplayer2.util.AtomicFile r3 = r9.c     // Catch:{ IOException -> 0x009f, all -> 0x0097 }
            java.io.InputStream r3 = r3.b()     // Catch:{ IOException -> 0x009f, all -> 0x0097 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x009f, all -> 0x0097 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ IOException -> 0x009f, all -> 0x0097 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x009f, all -> 0x0097 }
            int r1 = r3.readInt()     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            if (r1 < 0) goto L_0x008f
            r4 = 2
            if (r1 <= r4) goto L_0x001d
            goto L_0x008f
        L_0x001d:
            int r5 = r3.readInt()     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r6 = 1
            r5 = r5 & r6
            if (r5 == 0) goto L_0x0057
            javax.crypto.Cipher r5 = r9.d     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            if (r5 != 0) goto L_0x002d
            com.google.android.exoplayer2.util.Util.a((java.io.Closeable) r3)
            return r0
        L_0x002d:
            r5 = 16
            byte[] r5 = new byte[r5]     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r3.readFully(r5)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            javax.crypto.spec.IvParameterSpec r7 = new javax.crypto.spec.IvParameterSpec     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r7.<init>(r5)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            javax.crypto.Cipher r5 = r9.d     // Catch:{ InvalidKeyException -> 0x0050, InvalidAlgorithmParameterException -> 0x004e }
            javax.crypto.spec.SecretKeySpec r8 = r9.e     // Catch:{ InvalidKeyException -> 0x0050, InvalidAlgorithmParameterException -> 0x004e }
            r5.init(r4, r8, r7)     // Catch:{ InvalidKeyException -> 0x0050, InvalidAlgorithmParameterException -> 0x004e }
            java.io.DataInputStream r4 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            javax.crypto.CipherInputStream r5 = new javax.crypto.CipherInputStream     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            javax.crypto.Cipher r7 = r9.d     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r5.<init>(r2, r7)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r3 = r4
            goto L_0x005d
        L_0x004e:
            r1 = move-exception
            goto L_0x0051
        L_0x0050:
            r1 = move-exception
        L_0x0051:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            throw r2     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
        L_0x0057:
            boolean r2 = r9.f     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            if (r2 == 0) goto L_0x005d
            r9.g = r6     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
        L_0x005d:
            int r2 = r3.readInt()     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r4 = 0
            r5 = 0
        L_0x0063:
            if (r4 >= r2) goto L_0x0074
            com.google.android.exoplayer2.upstream.cache.CachedContent r7 = com.google.android.exoplayer2.upstream.cache.CachedContent.a((int) r1, (java.io.DataInputStream) r3)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r9.a((com.google.android.exoplayer2.upstream.cache.CachedContent) r7)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            int r7 = r7.a((int) r1)     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            int r5 = r5 + r7
            int r4 = r4 + 1
            goto L_0x0063
        L_0x0074:
            int r1 = r3.readInt()     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            int r2 = r3.read()     // Catch:{ IOException -> 0x0095, all -> 0x0093 }
            r4 = -1
            if (r2 != r4) goto L_0x0081
            r2 = 1
            goto L_0x0082
        L_0x0081:
            r2 = 0
        L_0x0082:
            if (r1 != r5) goto L_0x008b
            if (r2 != 0) goto L_0x0087
            goto L_0x008b
        L_0x0087:
            com.google.android.exoplayer2.util.Util.a((java.io.Closeable) r3)
            return r6
        L_0x008b:
            com.google.android.exoplayer2.util.Util.a((java.io.Closeable) r3)
            return r0
        L_0x008f:
            com.google.android.exoplayer2.util.Util.a((java.io.Closeable) r3)
            return r0
        L_0x0093:
            r0 = move-exception
            goto L_0x0099
        L_0x0095:
            goto L_0x00a0
        L_0x0097:
            r0 = move-exception
            r3 = r1
        L_0x0099:
            if (r3 == 0) goto L_0x009e
            com.google.android.exoplayer2.util.Util.a((java.io.Closeable) r3)
        L_0x009e:
            throw r0
        L_0x009f:
            r3 = r1
        L_0x00a0:
            if (r3 == 0) goto L_0x00a5
            com.google.android.exoplayer2.util.Util.a((java.io.Closeable) r3)
        L_0x00a5:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.upstream.cache.CachedContentIndex.f():boolean");
    }

    private void g() throws Cache.CacheException {
        DataOutputStream dataOutputStream;
        IOException e2;
        try {
            OutputStream c2 = this.c.c();
            if (this.h == null) {
                this.h = new ReusableBufferedOutputStream(c2);
            } else {
                this.h.a(c2);
            }
            dataOutputStream = new DataOutputStream(this.h);
            try {
                dataOutputStream.writeInt(2);
                int i = 0;
                dataOutputStream.writeInt(this.f ? 1 : 0);
                if (this.f) {
                    byte[] bArr = new byte[16];
                    new Random().nextBytes(bArr);
                    dataOutputStream.write(bArr);
                    try {
                        this.d.init(1, this.e, new IvParameterSpec(bArr));
                        dataOutputStream.flush();
                        dataOutputStream = new DataOutputStream(new CipherOutputStream(this.h, this.d));
                    } catch (InvalidKeyException e3) {
                        e = e3;
                        throw new IllegalStateException(e);
                    } catch (InvalidAlgorithmParameterException e4) {
                        e = e4;
                        throw new IllegalStateException(e);
                    }
                }
                dataOutputStream.writeInt(this.f3615a.size());
                for (CachedContent next : this.f3615a.values()) {
                    next.a(dataOutputStream);
                    i += next.a(2);
                }
                dataOutputStream.writeInt(i);
                this.c.a(dataOutputStream);
                Util.a((Closeable) null);
            } catch (IOException e5) {
                e2 = e5;
                try {
                    throw new Cache.CacheException((Throwable) e2);
                } catch (Throwable th) {
                    th = th;
                    Util.a((Closeable) dataOutputStream);
                    throw th;
                }
            }
        } catch (IOException e6) {
            IOException iOException = e6;
            dataOutputStream = null;
            e2 = iOException;
            throw new Cache.CacheException((Throwable) e2);
        } catch (Throwable th2) {
            Throwable th3 = th2;
            dataOutputStream = null;
            th = th3;
            Util.a((Closeable) dataOutputStream);
            throw th;
        }
    }

    public Collection<CachedContent> a() {
        return this.f3615a.values();
    }

    public void b() {
        Assertions.b(!this.g);
        if (!f()) {
            this.c.a();
            this.f3615a.clear();
            this.b.clear();
        }
    }

    public void c() {
        String[] strArr = new String[this.f3615a.size()];
        this.f3615a.keySet().toArray(strArr);
        for (String e2 : strArr) {
            e(e2);
        }
    }

    public void d() throws Cache.CacheException {
        if (this.g) {
            g();
            this.g = false;
        }
    }

    public void e(String str) {
        CachedContent cachedContent = this.f3615a.get(str);
        if (cachedContent != null && cachedContent.c() && !cachedContent.d()) {
            this.f3615a.remove(str);
            this.b.remove(cachedContent.f3614a);
            this.g = true;
        }
    }

    public int a(String str) {
        return d(str).f3614a;
    }

    public String a(int i) {
        return this.b.get(i);
    }

    public void a(String str, ContentMetadataMutations contentMetadataMutations) {
        if (d(str).a(contentMetadataMutations)) {
            this.g = true;
        }
    }

    public CachedContent d(String str) {
        CachedContent cachedContent = this.f3615a.get(str);
        return cachedContent == null ? f(str) : cachedContent;
    }

    public ContentMetadata c(String str) {
        CachedContent b2 = b(str);
        return b2 != null ? b2.a() : DefaultContentMetadata.c;
    }

    private static Cipher e() throws NoSuchPaddingException, NoSuchAlgorithmException {
        if (Util.f3651a == 18) {
            try {
                return Cipher.getInstance("AES/CBC/PKCS5PADDING", "BC");
            } catch (Throwable unused) {
            }
        }
        return Cipher.getInstance("AES/CBC/PKCS5PADDING");
    }

    public CachedContent b(String str) {
        return this.f3615a.get(str);
    }

    private void a(CachedContent cachedContent) {
        this.f3615a.put(cachedContent.b, cachedContent);
        this.b.put(cachedContent.f3614a, cachedContent.b);
    }

    public static int a(SparseArray<String> sparseArray) {
        int i;
        int size = sparseArray.size();
        if (size == 0) {
            i = 0;
        } else {
            i = sparseArray.keyAt(size - 1) + 1;
        }
        if (i < 0) {
            int i2 = 0;
            while (i < size && i == sparseArray.keyAt(i)) {
                i2 = i + 1;
            }
        }
        return i;
    }

    private CachedContent f(String str) {
        CachedContent cachedContent = new CachedContent(a(this.b), str);
        a(cachedContent);
        this.g = true;
        return cachedContent;
    }
}
