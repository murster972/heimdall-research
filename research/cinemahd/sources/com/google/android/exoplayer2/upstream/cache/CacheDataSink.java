package com.google.android.exoplayer2.upstream.cache;

import com.google.android.exoplayer2.upstream.DataSink;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ReusableBufferedOutputStream;
import com.google.android.exoplayer2.util.Util;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class CacheDataSink implements DataSink {

    /* renamed from: a  reason: collision with root package name */
    private final Cache f3608a;
    private final long b;
    private final int c;
    private final boolean d;
    private DataSpec e;
    private File f;
    private OutputStream g;
    private FileOutputStream h;
    private long i;
    private long j;
    private ReusableBufferedOutputStream k;

    public static class CacheDataSinkException extends Cache.CacheException {
        public CacheDataSinkException(IOException iOException) {
            super((Throwable) iOException);
        }
    }

    public CacheDataSink(Cache cache, long j2) {
        this(cache, j2, 20480, true);
    }

    private void b() throws IOException {
        long j2;
        long j3 = this.e.f;
        if (j3 == -1) {
            j2 = this.b;
        } else {
            j2 = Math.min(j3 - this.j, this.b);
        }
        long j4 = j2;
        Cache cache = this.f3608a;
        DataSpec dataSpec = this.e;
        this.f = cache.a(dataSpec.g, this.j + dataSpec.d, j4);
        this.h = new FileOutputStream(this.f);
        int i2 = this.c;
        if (i2 > 0) {
            ReusableBufferedOutputStream reusableBufferedOutputStream = this.k;
            if (reusableBufferedOutputStream == null) {
                this.k = new ReusableBufferedOutputStream(this.h, i2);
            } else {
                reusableBufferedOutputStream.a(this.h);
            }
            this.g = this.k;
        } else {
            this.g = this.h;
        }
        this.i = 0;
    }

    public void a(DataSpec dataSpec) throws CacheDataSinkException {
        if (dataSpec.f != -1 || dataSpec.a(2)) {
            this.e = dataSpec;
            this.j = 0;
            try {
                b();
            } catch (IOException e2) {
                throw new CacheDataSinkException(e2);
            }
        } else {
            this.e = null;
        }
    }

    public void close() throws CacheDataSinkException {
        if (this.e != null) {
            try {
                a();
            } catch (IOException e2) {
                throw new CacheDataSinkException(e2);
            }
        }
    }

    public void write(byte[] bArr, int i2, int i3) throws CacheDataSinkException {
        if (this.e != null) {
            int i4 = 0;
            while (i4 < i3) {
                try {
                    if (this.i == this.b) {
                        a();
                        b();
                    }
                    int min = (int) Math.min((long) (i3 - i4), this.b - this.i);
                    this.g.write(bArr, i2 + i4, min);
                    i4 += min;
                    long j2 = (long) min;
                    this.i += j2;
                    this.j += j2;
                } catch (IOException e2) {
                    throw new CacheDataSinkException(e2);
                }
            }
        }
    }

    public CacheDataSink(Cache cache, long j2, int i2, boolean z) {
        Assertions.a(cache);
        this.f3608a = cache;
        this.b = j2;
        this.c = i2;
        this.d = z;
    }

    private void a() throws IOException {
        OutputStream outputStream = this.g;
        if (outputStream != null) {
            try {
                outputStream.flush();
                if (this.d) {
                    this.h.getFD().sync();
                }
                Util.a((Closeable) this.g);
                this.g = null;
                File file = this.f;
                this.f = null;
                this.f3608a.a(file);
            } catch (Throwable th) {
                Util.a((Closeable) this.g);
                this.g = null;
                File file2 = this.f;
                this.f = null;
                file2.delete();
                throw th;
            }
        }
    }
}
