package com.google.android.exoplayer2.upstream.cache;

import com.google.android.exoplayer2.util.Assertions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContentMetadataMutations {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, Object> f3616a = new HashMap();
    private final List<String> b = new ArrayList();

    public ContentMetadataMutations a(String str, String str2) {
        a(str, (Object) str2);
        return this;
    }

    public List<String> b() {
        return Collections.unmodifiableList(new ArrayList(this.b));
    }

    public ContentMetadataMutations a(String str, long j) {
        a(str, (Object) Long.valueOf(j));
        return this;
    }

    public ContentMetadataMutations a(String str) {
        this.b.add(str);
        this.f3616a.remove(str);
        return this;
    }

    public Map<String, Object> a() {
        HashMap hashMap = new HashMap(this.f3616a);
        for (Map.Entry entry : hashMap.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof byte[]) {
                byte[] bArr = (byte[]) value;
                entry.setValue(Arrays.copyOf(bArr, bArr.length));
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    private ContentMetadataMutations a(String str, Object obj) {
        Map<String, Object> map = this.f3616a;
        Assertions.a(str);
        Assertions.a(obj);
        map.put(str, obj);
        this.b.remove(str);
        return this;
    }
}
