package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class BaseDataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f3584a;
    private final ArrayList<TransferListener> b = new ArrayList<>(1);
    private int c;
    private DataSpec d;

    protected BaseDataSource(boolean z) {
        this.f3584a = z;
    }

    public /* synthetic */ Map<String, List<String>> a() {
        return d.a(this);
    }

    public final void a(TransferListener transferListener) {
        if (!this.b.contains(transferListener)) {
            this.b.add(transferListener);
            this.c++;
        }
    }

    /* access modifiers changed from: protected */
    public final void b(DataSpec dataSpec) {
        for (int i = 0; i < this.c; i++) {
            this.b.get(i).c(this, dataSpec, this.f3584a);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(DataSpec dataSpec) {
        this.d = dataSpec;
        for (int i = 0; i < this.c; i++) {
            this.b.get(i).b(this, dataSpec, this.f3584a);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        DataSpec dataSpec = this.d;
        Util.a(dataSpec);
        DataSpec dataSpec2 = dataSpec;
        for (int i = 0; i < this.c; i++) {
            this.b.get(i).a(this, dataSpec2, this.f3584a);
        }
        this.d = null;
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        DataSpec dataSpec = this.d;
        Util.a(dataSpec);
        DataSpec dataSpec2 = dataSpec;
        for (int i2 = 0; i2 < this.c; i2++) {
            this.b.get(i2).a(this, dataSpec2, this.f3584a, i);
        }
    }
}
