package com.google.android.exoplayer2.upstream;

import android.os.Handler;

public interface BandwidthMeter {

    public interface EventListener {
        void b(int i, long j, long j2);
    }

    TransferListener a();

    void a(Handler handler, EventListener eventListener);

    void a(EventListener eventListener);

    long b();
}
