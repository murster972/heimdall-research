package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Arrays;

public final class DataSpec {

    /* renamed from: a  reason: collision with root package name */
    public final Uri f3586a;
    public final int b;
    public final byte[] c;
    public final long d;
    public final long e;
    public final long f;
    public final String g;
    public final int h;

    public DataSpec(Uri uri) {
        this(uri, 0);
    }

    public static String b(int i) {
        if (i == 1) {
            return "GET";
        }
        if (i == 2) {
            return "POST";
        }
        if (i == 3) {
            return "HEAD";
        }
        throw new AssertionError(i);
    }

    public boolean a(int i) {
        return (this.h & i) == i;
    }

    public String toString() {
        return "DataSpec[" + a() + " " + this.f3586a + ", " + Arrays.toString(this.c) + ", " + this.d + ", " + this.e + ", " + this.f + ", " + this.g + ", " + this.h + "]";
    }

    public DataSpec(Uri uri, int i) {
        this(uri, 0, -1, (String) null, i);
    }

    public final String a() {
        return b(this.b);
    }

    public DataSpec(Uri uri, long j, long j2, String str) {
        this(uri, j, j, j2, str, 0);
    }

    public DataSpec a(long j) {
        long j2 = this.f;
        long j3 = -1;
        if (j2 != -1) {
            j3 = j2 - j;
        }
        return a(j, j3);
    }

    public DataSpec(Uri uri, long j, long j2, String str, int i) {
        this(uri, j, j, j2, str, i);
    }

    public DataSpec a(long j, long j2) {
        if (j == 0 && this.f == j2) {
            return this;
        }
        return new DataSpec(this.f3586a, this.b, this.c, this.d + j, this.e + j, j2, this.g, this.h);
    }

    public DataSpec(Uri uri, long j, long j2, long j3, String str, int i) {
        this(uri, (byte[]) null, j, j2, j3, str, i);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DataSpec(Uri uri, byte[] bArr, long j, long j2, long j3, String str, int i) {
        this(uri, bArr != null ? 2 : 1, bArr, j, j2, j3, str, i);
    }

    public DataSpec(Uri uri, int i, byte[] bArr, long j, long j2, long j3, String str, int i2) {
        byte[] bArr2 = bArr;
        long j4 = j;
        long j5 = j2;
        long j6 = j3;
        boolean z = true;
        Assertions.a(j4 >= 0);
        Assertions.a(j5 >= 0);
        if (j6 <= 0 && j6 != -1) {
            z = false;
        }
        Assertions.a(z);
        this.f3586a = uri;
        this.b = i;
        this.c = (bArr2 == null || bArr2.length == 0) ? null : bArr2;
        this.d = j4;
        this.e = j5;
        this.f = j6;
        this.g = str;
        this.h = i2;
    }
}
