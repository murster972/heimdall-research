package com.google.android.exoplayer2.upstream.cache;

public final class NoOpCacheEvictor implements CacheEvictor {
    public void a() {
    }

    public void a(Cache cache, CacheSpan cacheSpan) {
    }

    public void a(Cache cache, CacheSpan cacheSpan, CacheSpan cacheSpan2) {
    }

    public void a(Cache cache, String str, long j, long j2) {
    }

    public void b(Cache cache, CacheSpan cacheSpan) {
    }
}
