package com.google.android.exoplayer2.upstream.cache;

import com.google.android.exoplayer2.upstream.cache.Cache;

public interface CacheEvictor extends Cache.Listener {
    void a();

    void a(Cache cache, String str, long j, long j2);
}
