package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import java.io.IOException;

public class DefaultLoadErrorHandlingPolicy implements LoadErrorHandlingPolicy {

    /* renamed from: a  reason: collision with root package name */
    private final int f3592a;

    public DefaultLoadErrorHandlingPolicy() {
        this(-1);
    }

    public long a(int i, long j, IOException iOException, int i2) {
        if (!(iOException instanceof HttpDataSource.InvalidResponseCodeException)) {
            return -9223372036854775807L;
        }
        int i3 = ((HttpDataSource.InvalidResponseCodeException) iOException).responseCode;
        if (i3 == 404 || i3 == 410) {
            return 60000;
        }
        return -9223372036854775807L;
    }

    public long b(int i, long j, IOException iOException, int i2) {
        if (iOException instanceof ParserException) {
            return -9223372036854775807L;
        }
        return (long) Math.min((i2 - 1) * 1000, 5000);
    }

    public DefaultLoadErrorHandlingPolicy(int i) {
        this.f3592a = i;
    }

    public int a(int i) {
        int i2 = this.f3592a;
        if (i2 == -1) {
            return i == 7 ? 6 : 3;
        }
        return i2;
    }
}
