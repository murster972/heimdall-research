package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public final class DummyDataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    public static final DummyDataSource f3593a = new DummyDataSource();

    static {
        b bVar = b.f3606a;
    }

    private DummyDataSource() {
    }

    public static /* synthetic */ DummyDataSource b() {
        return new DummyDataSource();
    }

    public long a(DataSpec dataSpec) throws IOException {
        throw new IOException("Dummy source");
    }

    public /* synthetic */ Map<String, List<String>> a() {
        return d.a(this);
    }

    public void a(TransferListener transferListener) {
    }

    public void close() throws IOException {
    }

    public Uri getUri() {
        return null;
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        throw new UnsupportedOperationException();
    }
}
