package com.google.android.exoplayer2.upstream;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class RawResourceDataSource extends BaseDataSource {
    private final Resources e;
    private Uri f;
    private AssetFileDescriptor g;
    private InputStream h;
    private long i;
    private boolean j;

    public static class RawResourceDataSourceException extends IOException {
        public RawResourceDataSourceException(String str) {
            super(str);
        }

        public RawResourceDataSourceException(IOException iOException) {
            super(iOException);
        }
    }

    public RawResourceDataSource(Context context) {
        super(false);
        this.e = context.getResources();
    }

    public long a(DataSpec dataSpec) throws RawResourceDataSourceException {
        try {
            this.f = dataSpec.f3586a;
            if (TextUtils.equals("rawresource", this.f.getScheme())) {
                int parseInt = Integer.parseInt(this.f.getLastPathSegment());
                b(dataSpec);
                this.g = this.e.openRawResourceFd(parseInt);
                this.h = new FileInputStream(this.g.getFileDescriptor());
                this.h.skip(this.g.getStartOffset());
                if (this.h.skip(dataSpec.e) >= dataSpec.e) {
                    long j2 = -1;
                    if (dataSpec.f != -1) {
                        this.i = dataSpec.f;
                    } else {
                        long length = this.g.getLength();
                        if (length != -1) {
                            j2 = length - dataSpec.e;
                        }
                        this.i = j2;
                    }
                    this.j = true;
                    c(dataSpec);
                    return this.i;
                }
                throw new EOFException();
            }
            throw new RawResourceDataSourceException("URI must use scheme rawresource");
        } catch (NumberFormatException unused) {
            throw new RawResourceDataSourceException("Resource identifier must be an integer.");
        } catch (IOException e2) {
            throw new RawResourceDataSourceException(e2);
        }
    }

    public void close() throws RawResourceDataSourceException {
        this.f = null;
        try {
            if (this.h != null) {
                this.h.close();
            }
            this.h = null;
            try {
                if (this.g != null) {
                    this.g.close();
                }
                this.g = null;
                if (this.j) {
                    this.j = false;
                    b();
                }
            } catch (IOException e2) {
                throw new RawResourceDataSourceException(e2);
            } catch (Throwable th) {
                this.g = null;
                if (this.j) {
                    this.j = false;
                    b();
                }
                throw th;
            }
        } catch (IOException e3) {
            throw new RawResourceDataSourceException(e3);
        } catch (Throwable th2) {
            this.h = null;
            try {
                if (this.g != null) {
                    this.g.close();
                }
                this.g = null;
                if (this.j) {
                    this.j = false;
                    b();
                }
                throw th2;
            } catch (IOException e4) {
                throw new RawResourceDataSourceException(e4);
            } catch (Throwable th3) {
                this.g = null;
                if (this.j) {
                    this.j = false;
                    b();
                }
                throw th3;
            }
        }
    }

    public Uri getUri() {
        return this.f;
    }

    public int read(byte[] bArr, int i2, int i3) throws RawResourceDataSourceException {
        if (i3 == 0) {
            return 0;
        }
        long j2 = this.i;
        if (j2 == 0) {
            return -1;
        }
        if (j2 != -1) {
            try {
                i3 = (int) Math.min(j2, (long) i3);
            } catch (IOException e2) {
                throw new RawResourceDataSourceException(e2);
            }
        }
        int read = this.h.read(bArr, i2, i3);
        if (read != -1) {
            long j3 = this.i;
            if (j3 != -1) {
                this.i = j3 - ((long) read);
            }
            a(read);
            return read;
        } else if (this.i == -1) {
            return -1;
        } else {
            throw new RawResourceDataSourceException((IOException) new EOFException());
        }
    }
}
