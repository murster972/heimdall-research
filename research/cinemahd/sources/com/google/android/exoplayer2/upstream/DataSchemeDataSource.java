package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import android.util.Base64;
import com.facebook.common.util.UriUtil;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.net.URLDecoder;

public final class DataSchemeDataSource extends BaseDataSource {
    private DataSpec e;
    private int f;
    private byte[] g;

    public DataSchemeDataSource() {
        super(false);
    }

    public long a(DataSpec dataSpec) throws IOException {
        b(dataSpec);
        this.e = dataSpec;
        Uri uri = dataSpec.f3586a;
        String scheme = uri.getScheme();
        if (UriUtil.DATA_SCHEME.equals(scheme)) {
            String[] a2 = Util.a(uri.getSchemeSpecificPart(), ",");
            if (a2.length == 2) {
                String str = a2[1];
                if (a2[0].contains(";base64")) {
                    try {
                        this.g = Base64.decode(str, 0);
                    } catch (IllegalArgumentException e2) {
                        throw new ParserException("Error while parsing Base64 encoded string: " + str, e2);
                    }
                } else {
                    this.g = Util.d(URLDecoder.decode(str, "US-ASCII"));
                }
                c(dataSpec);
                return (long) this.g.length;
            }
            throw new ParserException("Unexpected URI format: " + uri);
        }
        throw new ParserException("Unsupported scheme: " + scheme);
    }

    public void close() throws IOException {
        if (this.g != null) {
            this.g = null;
            b();
        }
        this.e = null;
    }

    public Uri getUri() {
        DataSpec dataSpec = this.e;
        if (dataSpec != null) {
            return dataSpec.f3586a;
        }
        return null;
    }

    public int read(byte[] bArr, int i, int i2) {
        if (i2 == 0) {
            return 0;
        }
        int length = this.g.length - this.f;
        if (length == 0) {
            return -1;
        }
        int min = Math.min(i2, length);
        System.arraycopy(this.g, this.f, bArr, i, min);
        this.f += min;
        a(min);
        return min;
    }
}
