package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public final class TeeDataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    private final DataSource f3604a;
    private final DataSink b;
    private boolean c;
    private long d;

    public TeeDataSource(DataSource dataSource, DataSink dataSink) {
        Assertions.a(dataSource);
        this.f3604a = dataSource;
        Assertions.a(dataSink);
        this.b = dataSink;
    }

    public void a(TransferListener transferListener) {
        this.f3604a.a(transferListener);
    }

    public void close() throws IOException {
        try {
            this.f3604a.close();
        } finally {
            if (this.c) {
                this.c = false;
                this.b.close();
            }
        }
    }

    public Uri getUri() {
        return this.f3604a.getUri();
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (this.d == 0) {
            return -1;
        }
        int read = this.f3604a.read(bArr, i, i2);
        if (read > 0) {
            this.b.write(bArr, i, read);
            long j = this.d;
            if (j != -1) {
                this.d = j - ((long) read);
            }
        }
        return read;
    }

    public long a(DataSpec dataSpec) throws IOException {
        this.d = this.f3604a.a(dataSpec);
        long j = this.d;
        if (j == 0) {
            return 0;
        }
        if (dataSpec.f == -1 && j != -1) {
            dataSpec = dataSpec.a(0, j);
        }
        this.c = true;
        this.b.a(dataSpec);
        return this.d;
    }

    public Map<String, List<String>> a() {
        return this.f3604a.a();
    }
}
