package com.google.android.exoplayer2.upstream.cache;

import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSink;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSourceException;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TeeDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.upstream.cache.Cache;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class CacheDataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    private final Cache f3609a;
    private final DataSource b;
    private final DataSource c;
    private final DataSource d;
    private final CacheKeyFactory e;
    private final EventListener f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private DataSource j;
    private boolean k;
    private Uri l;
    private Uri m;
    private int n;
    private int o;
    private String p;
    private long q;
    private long r;
    private CacheSpan s;
    private boolean t;
    private boolean u;
    private long v;
    private long w;

    public interface EventListener {
        void a(int i);

        void a(long j, long j2);
    }

    public CacheDataSource(Cache cache, DataSource dataSource, DataSource dataSource2, DataSink dataSink, int i2, EventListener eventListener) {
        this(cache, dataSource, dataSource2, dataSink, i2, eventListener, (CacheKeyFactory) null);
    }

    private static boolean b(IOException iOException) {
        Throwable th;
        while (th != null) {
            if ((th instanceof DataSourceException) && ((DataSourceException) th).reason == 0) {
                return true;
            }
            Throwable cause = th.getCause();
            th = iOException;
            th = cause;
        }
        return false;
    }

    private boolean c() {
        return this.j == this.d;
    }

    private boolean d() {
        return this.j == this.b;
    }

    private boolean e() {
        return !d();
    }

    private boolean f() {
        return this.j == this.c;
    }

    private void g() {
        EventListener eventListener = this.f;
        if (eventListener != null && this.v > 0) {
            eventListener.a(this.f3609a.a(), this.v);
            this.v = 0;
        }
    }

    private void h() throws IOException {
        this.r = 0;
        if (f()) {
            this.f3609a.a(this.p, this.q);
        }
    }

    public void a(TransferListener transferListener) {
        this.b.a(transferListener);
        this.d.a(transferListener);
    }

    public void close() throws IOException {
        this.l = null;
        this.m = null;
        this.n = 1;
        g();
        try {
            b();
        } catch (IOException e2) {
            a(e2);
            throw e2;
        }
    }

    public Uri getUri() {
        return this.m;
    }

    public int read(byte[] bArr, int i2, int i3) throws IOException {
        if (i3 == 0) {
            return 0;
        }
        if (this.r == 0) {
            return -1;
        }
        try {
            if (this.q >= this.w) {
                a(true);
            }
            int read = this.j.read(bArr, i2, i3);
            if (read != -1) {
                if (d()) {
                    this.v += (long) read;
                }
                long j2 = (long) read;
                this.q += j2;
                if (this.r != -1) {
                    this.r -= j2;
                }
            } else if (this.k) {
                h();
            } else {
                if (this.r <= 0) {
                    if (this.r == -1) {
                    }
                }
                b();
                a(false);
                return read(bArr, i2, i3);
            }
            return read;
        } catch (IOException e2) {
            if (!this.k || !b(e2)) {
                a(e2);
                throw e2;
            }
            h();
            return -1;
        }
    }

    public CacheDataSource(Cache cache, DataSource dataSource, DataSource dataSource2, DataSink dataSink, int i2, EventListener eventListener, CacheKeyFactory cacheKeyFactory) {
        this.f3609a = cache;
        this.b = dataSource2;
        this.e = cacheKeyFactory == null ? CacheUtil.f3612a : cacheKeyFactory;
        boolean z = false;
        this.g = (i2 & 1) != 0;
        this.h = (i2 & 2) != 0;
        this.i = (i2 & 4) != 0 ? true : z;
        this.d = dataSource;
        if (dataSink != null) {
            this.c = new TeeDataSource(dataSource, dataSink);
        } else {
            this.c = null;
        }
        this.f = eventListener;
    }

    public long a(DataSpec dataSpec) throws IOException {
        try {
            this.p = this.e.a(dataSpec);
            this.l = dataSpec.f3586a;
            this.m = a(this.f3609a, this.p, this.l);
            this.n = dataSpec.b;
            this.o = dataSpec.h;
            this.q = dataSpec.e;
            int b2 = b(dataSpec);
            this.u = b2 != -1;
            if (this.u) {
                a(b2);
            }
            if (dataSpec.f == -1) {
                if (!this.u) {
                    this.r = this.f3609a.b(this.p);
                    if (this.r != -1) {
                        this.r -= dataSpec.e;
                        if (this.r <= 0) {
                            throw new DataSourceException(0);
                        }
                    }
                    a(false);
                    return this.r;
                }
            }
            this.r = dataSpec.f;
            a(false);
            return this.r;
        } catch (IOException e2) {
            a(e2);
            throw e2;
        }
    }

    private void b() throws IOException {
        DataSource dataSource = this.j;
        if (dataSource != null) {
            try {
                dataSource.close();
            } finally {
                this.j = null;
                this.k = false;
                CacheSpan cacheSpan = this.s;
                if (cacheSpan != null) {
                    this.f3609a.a(cacheSpan);
                    this.s = null;
                }
            }
        }
    }

    private int b(DataSpec dataSpec) {
        if (!this.h || !this.t) {
            return (!this.i || dataSpec.f != -1) ? -1 : 1;
        }
        return 0;
    }

    public Map<String, List<String>> a() {
        if (e()) {
            return this.d.a();
        }
        return Collections.emptyMap();
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r20) throws java.io.IOException {
        /*
            r19 = this;
            r1 = r19
            boolean r0 = r1.u
            r2 = 0
            if (r0 == 0) goto L_0x0009
            r0 = r2
            goto L_0x002f
        L_0x0009:
            boolean r0 = r1.g
            if (r0 == 0) goto L_0x0025
            com.google.android.exoplayer2.upstream.cache.Cache r0 = r1.f3609a     // Catch:{ InterruptedException -> 0x0018 }
            java.lang.String r3 = r1.p     // Catch:{ InterruptedException -> 0x0018 }
            long r4 = r1.q     // Catch:{ InterruptedException -> 0x0018 }
            com.google.android.exoplayer2.upstream.cache.CacheSpan r0 = r0.c(r3, r4)     // Catch:{ InterruptedException -> 0x0018 }
            goto L_0x002f
        L_0x0018:
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            java.io.InterruptedIOException r0 = new java.io.InterruptedIOException
            r0.<init>()
            throw r0
        L_0x0025:
            com.google.android.exoplayer2.upstream.cache.Cache r0 = r1.f3609a
            java.lang.String r3 = r1.p
            long r4 = r1.q
            com.google.android.exoplayer2.upstream.cache.CacheSpan r0 = r0.b(r3, r4)
        L_0x002f:
            r3 = -1
            if (r0 != 0) goto L_0x0050
            com.google.android.exoplayer2.upstream.DataSource r2 = r1.d
            com.google.android.exoplayer2.upstream.DataSpec r17 = new com.google.android.exoplayer2.upstream.DataSpec
            android.net.Uri r6 = r1.l
            int r7 = r1.n
            r8 = 0
            long r11 = r1.q
            long r13 = r1.r
            java.lang.String r15 = r1.p
            int r9 = r1.o
            r5 = r17
            r16 = r9
            r9 = r11
            r5.<init>(r6, r7, r8, r9, r11, r13, r15, r16)
            r6 = r2
        L_0x004d:
            r2 = r0
            goto L_0x00b7
        L_0x0050:
            boolean r5 = r0.d
            if (r5 == 0) goto L_0x007f
            java.io.File r2 = r0.e
            android.net.Uri r6 = android.net.Uri.fromFile(r2)
            long r7 = r1.q
            long r9 = r0.b
            long r9 = r7 - r9
            long r7 = r0.c
            long r7 = r7 - r9
            long r11 = r1.r
            int r2 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r2 == 0) goto L_0x006d
            long r7 = java.lang.Math.min(r7, r11)
        L_0x006d:
            r11 = r7
            com.google.android.exoplayer2.upstream.DataSpec r2 = new com.google.android.exoplayer2.upstream.DataSpec
            long r7 = r1.q
            java.lang.String r13 = r1.p
            int r14 = r1.o
            r5 = r2
            r5.<init>(r6, r7, r9, r11, r13, r14)
            com.google.android.exoplayer2.upstream.DataSource r5 = r1.b
            r6 = r5
            r5 = r2
            goto L_0x004d
        L_0x007f:
            boolean r5 = r0.b()
            if (r5 == 0) goto L_0x0088
            long r5 = r1.r
            goto L_0x0094
        L_0x0088:
            long r5 = r0.c
            long r7 = r1.r
            int r9 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r9 == 0) goto L_0x0094
            long r5 = java.lang.Math.min(r5, r7)
        L_0x0094:
            r15 = r5
            com.google.android.exoplayer2.upstream.DataSpec r5 = new com.google.android.exoplayer2.upstream.DataSpec
            android.net.Uri r8 = r1.l
            int r9 = r1.n
            r10 = 0
            long r13 = r1.q
            java.lang.String r6 = r1.p
            int r11 = r1.o
            r7 = r5
            r18 = r11
            r11 = r13
            r17 = r6
            r7.<init>(r8, r9, r10, r11, r13, r15, r17, r18)
            com.google.android.exoplayer2.upstream.DataSource r6 = r1.c
            if (r6 == 0) goto L_0x00b0
            goto L_0x004d
        L_0x00b0:
            com.google.android.exoplayer2.upstream.DataSource r6 = r1.d
            com.google.android.exoplayer2.upstream.cache.Cache r7 = r1.f3609a
            r7.a((com.google.android.exoplayer2.upstream.cache.CacheSpan) r0)
        L_0x00b7:
            boolean r0 = r1.u
            if (r0 != 0) goto L_0x00c6
            com.google.android.exoplayer2.upstream.DataSource r0 = r1.d
            if (r6 != r0) goto L_0x00c6
            long r7 = r1.q
            r9 = 102400(0x19000, double:5.05923E-319)
            long r7 = r7 + r9
            goto L_0x00cb
        L_0x00c6:
            r7 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x00cb:
            r1.w = r7
            if (r20 == 0) goto L_0x00ed
            boolean r0 = r19.c()
            com.google.android.exoplayer2.util.Assertions.b(r0)
            com.google.android.exoplayer2.upstream.DataSource r0 = r1.d
            if (r6 != r0) goto L_0x00db
            return
        L_0x00db:
            r19.b()     // Catch:{ all -> 0x00df }
            goto L_0x00ed
        L_0x00df:
            r0 = move-exception
            r3 = r0
            boolean r0 = r2.a()
            if (r0 == 0) goto L_0x00ec
            com.google.android.exoplayer2.upstream.cache.Cache r0 = r1.f3609a
            r0.a((com.google.android.exoplayer2.upstream.cache.CacheSpan) r2)
        L_0x00ec:
            throw r3
        L_0x00ed:
            if (r2 == 0) goto L_0x00f7
            boolean r0 = r2.a()
            if (r0 == 0) goto L_0x00f7
            r1.s = r2
        L_0x00f7:
            r1.j = r6
            long r7 = r5.f
            r0 = 1
            int r2 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r2 != 0) goto L_0x0102
            r2 = 1
            goto L_0x0103
        L_0x0102:
            r2 = 0
        L_0x0103:
            r1.k = r2
            long r5 = r6.a((com.google.android.exoplayer2.upstream.DataSpec) r5)
            com.google.android.exoplayer2.upstream.cache.ContentMetadataMutations r2 = new com.google.android.exoplayer2.upstream.cache.ContentMetadataMutations
            r2.<init>()
            boolean r7 = r1.k
            if (r7 == 0) goto L_0x0120
            int r7 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r7 == 0) goto L_0x0120
            r1.r = r5
            long r3 = r1.q
            long r5 = r1.r
            long r3 = r3 + r5
            com.google.android.exoplayer2.upstream.cache.ContentMetadataInternal.a((com.google.android.exoplayer2.upstream.cache.ContentMetadataMutations) r2, (long) r3)
        L_0x0120:
            boolean r3 = r19.e()
            if (r3 == 0) goto L_0x0142
            com.google.android.exoplayer2.upstream.DataSource r3 = r1.j
            android.net.Uri r3 = r3.getUri()
            r1.m = r3
            android.net.Uri r3 = r1.l
            android.net.Uri r4 = r1.m
            boolean r3 = r3.equals(r4)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x013f
            android.net.Uri r0 = r1.m
            com.google.android.exoplayer2.upstream.cache.ContentMetadataInternal.a((com.google.android.exoplayer2.upstream.cache.ContentMetadataMutations) r2, (android.net.Uri) r0)
            goto L_0x0142
        L_0x013f:
            com.google.android.exoplayer2.upstream.cache.ContentMetadataInternal.a((com.google.android.exoplayer2.upstream.cache.ContentMetadataMutations) r2)
        L_0x0142:
            boolean r0 = r19.f()
            if (r0 == 0) goto L_0x014f
            com.google.android.exoplayer2.upstream.cache.Cache r0 = r1.f3609a
            java.lang.String r3 = r1.p
            r0.a((java.lang.String) r3, (com.google.android.exoplayer2.upstream.cache.ContentMetadataMutations) r2)
        L_0x014f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.upstream.cache.CacheDataSource.a(boolean):void");
    }

    private static Uri a(Cache cache, String str, Uri uri) {
        Uri b2 = ContentMetadataInternal.b(cache.a(str));
        return b2 == null ? uri : b2;
    }

    private void a(IOException iOException) {
        if (d() || (iOException instanceof Cache.CacheException)) {
            this.t = true;
        }
    }

    private void a(int i2) {
        EventListener eventListener = this.f;
        if (eventListener != null) {
            eventListener.a(i2);
        }
    }
}
