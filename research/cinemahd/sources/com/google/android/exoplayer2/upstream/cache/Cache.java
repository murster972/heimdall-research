package com.google.android.exoplayer2.upstream.cache;

import java.io.File;
import java.io.IOException;
import java.util.NavigableSet;

public interface Cache {

    public static class CacheException extends IOException {
        public CacheException(String str) {
            super(str);
        }

        public CacheException(Throwable th) {
            super(th);
        }
    }

    public interface Listener {
        void a(Cache cache, CacheSpan cacheSpan);

        void a(Cache cache, CacheSpan cacheSpan, CacheSpan cacheSpan2);

        void b(Cache cache, CacheSpan cacheSpan);
    }

    long a();

    ContentMetadata a(String str);

    File a(String str, long j, long j2) throws CacheException;

    void a(CacheSpan cacheSpan);

    void a(File file) throws CacheException;

    void a(String str, long j) throws CacheException;

    void a(String str, ContentMetadataMutations contentMetadataMutations) throws CacheException;

    long b(String str);

    long b(String str, long j, long j2);

    CacheSpan b(String str, long j) throws CacheException;

    void b(CacheSpan cacheSpan) throws CacheException;

    CacheSpan c(String str, long j) throws InterruptedException, CacheException;

    NavigableSet<CacheSpan> c(String str);
}
