package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.upstream.DataSource;

public final class FileDataSourceFactory implements DataSource.Factory {

    /* renamed from: a  reason: collision with root package name */
    private final TransferListener f3594a;

    public FileDataSourceFactory() {
        this((TransferListener) null);
    }

    public DataSource a() {
        FileDataSource fileDataSource = new FileDataSource();
        TransferListener transferListener = this.f3594a;
        if (transferListener != null) {
            fileDataSource.a(transferListener);
        }
        return fileDataSource;
    }

    public FileDataSourceFactory(TransferListener transferListener) {
        this.f3594a = transferListener;
    }
}
