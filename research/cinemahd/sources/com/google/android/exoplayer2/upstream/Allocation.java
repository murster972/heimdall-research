package com.google.android.exoplayer2.upstream;

public final class Allocation {

    /* renamed from: a  reason: collision with root package name */
    public final byte[] f3583a;
    public final int b;

    public Allocation(byte[] bArr, int i) {
        this.f3583a = bArr;
        this.b = i;
    }
}
