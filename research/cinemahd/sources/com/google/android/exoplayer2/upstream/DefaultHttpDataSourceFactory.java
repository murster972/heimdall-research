package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Predicate;

public final class DefaultHttpDataSourceFactory extends HttpDataSource.BaseFactory {
    private final String b;
    private final TransferListener c;
    private final int d;
    private final int e;
    private final boolean f;

    public DefaultHttpDataSourceFactory(String str) {
        this(str, (TransferListener) null);
    }

    public DefaultHttpDataSourceFactory(String str, TransferListener transferListener) {
        this(str, transferListener, 8000, 8000, false);
    }

    /* access modifiers changed from: protected */
    public DefaultHttpDataSource a(HttpDataSource.RequestProperties requestProperties) {
        DefaultHttpDataSource defaultHttpDataSource = new DefaultHttpDataSource(this.b, (Predicate<String>) null, this.d, this.e, this.f, requestProperties);
        TransferListener transferListener = this.c;
        if (transferListener != null) {
            defaultHttpDataSource.a(transferListener);
        }
        return defaultHttpDataSource;
    }

    public DefaultHttpDataSourceFactory(String str, TransferListener transferListener, int i, int i2, boolean z) {
        this.b = str;
        this.c = transferListener;
        this.d = i;
        this.e = i2;
        this.f = z;
    }
}
