package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class StatsDataSource implements DataSource {

    /* renamed from: a  reason: collision with root package name */
    private final DataSource f3603a;
    private long b;
    private Uri c = Uri.EMPTY;
    private Map<String, List<String>> d = Collections.emptyMap();

    public StatsDataSource(DataSource dataSource) {
        Assertions.a(dataSource);
        this.f3603a = dataSource;
    }

    public void a(TransferListener transferListener) {
        this.f3603a.a(transferListener);
    }

    public long b() {
        return this.b;
    }

    public Uri c() {
        return this.c;
    }

    public void close() throws IOException {
        this.f3603a.close();
    }

    public Map<String, List<String>> d() {
        return this.d;
    }

    public void e() {
        this.b = 0;
    }

    public Uri getUri() {
        return this.f3603a.getUri();
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read = this.f3603a.read(bArr, i, i2);
        if (read != -1) {
            this.b += (long) read;
        }
        return read;
    }

    public long a(DataSpec dataSpec) throws IOException {
        this.c = dataSpec.f3586a;
        this.d = Collections.emptyMap();
        long a2 = this.f3603a.a(dataSpec);
        Uri uri = getUri();
        Assertions.a(uri);
        this.c = uri;
        this.d = a();
        return a2;
    }

    public Map<String, List<String>> a() {
        return this.f3603a.a();
    }
}
