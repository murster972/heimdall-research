package com.google.android.exoplayer2.upstream;

import java.io.IOException;

public interface LoadErrorHandlingPolicy {
    int a(int i);

    long a(int i, long j, IOException iOException, int i2);

    long b(int i, long j, IOException iOException, int i2);
}
