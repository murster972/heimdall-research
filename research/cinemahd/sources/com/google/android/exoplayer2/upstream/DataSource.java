package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface DataSource {

    public interface Factory {
        DataSource a();
    }

    long a(DataSpec dataSpec) throws IOException;

    Map<String, List<String>> a();

    void a(TransferListener transferListener);

    void close() throws IOException;

    Uri getUri();

    int read(byte[] bArr, int i, int i2) throws IOException;
}
