package com.google.android.exoplayer2.upstream;

import android.net.Uri;
import com.google.android.exoplayer2.upstream.Loader;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public final class ParsingLoadable<T> implements Loader.Loadable {

    /* renamed from: a  reason: collision with root package name */
    public final DataSpec f3601a;
    public final int b;
    private final StatsDataSource c;
    private final Parser<? extends T> d;
    private volatile T e;

    public interface Parser<T> {
        T a(Uri uri, InputStream inputStream) throws IOException;
    }

    public ParsingLoadable(DataSource dataSource, Uri uri, int i, Parser<? extends T> parser) {
        this(dataSource, new DataSpec(uri, 3), i, parser);
    }

    public static <T> T a(DataSource dataSource, Parser<? extends T> parser, Uri uri, int i) throws IOException {
        ParsingLoadable parsingLoadable = new ParsingLoadable(dataSource, uri, i, parser);
        parsingLoadable.load();
        T c2 = parsingLoadable.c();
        Assertions.a(c2);
        return c2;
    }

    public Map<String, List<String>> b() {
        return this.c.d();
    }

    public final T c() {
        return this.e;
    }

    public final void cancelLoad() {
    }

    public Uri d() {
        return this.c.c();
    }

    public final void load() throws IOException {
        this.c.e();
        DataSourceInputStream dataSourceInputStream = new DataSourceInputStream(this.c, this.f3601a);
        try {
            dataSourceInputStream.s();
            Uri uri = this.c.getUri();
            Assertions.a(uri);
            this.e = this.d.a(uri, dataSourceInputStream);
        } finally {
            Util.a((Closeable) dataSourceInputStream);
        }
    }

    public ParsingLoadable(DataSource dataSource, DataSpec dataSpec, int i, Parser<? extends T> parser) {
        this.c = new StatsDataSource(dataSource);
        this.f3601a = dataSpec;
        this.b = i;
        this.d = parser;
    }

    public long a() {
        return this.c.b();
    }
}
