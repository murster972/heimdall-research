package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.io.InputStream;

public final class DataSourceInputStream extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final DataSource f3585a;
    private final DataSpec b;
    private final byte[] c;
    private boolean d = false;
    private boolean e = false;
    private long f;

    public DataSourceInputStream(DataSource dataSource, DataSpec dataSpec) {
        this.f3585a = dataSource;
        this.b = dataSpec;
        this.c = new byte[1];
    }

    private void t() throws IOException {
        if (!this.d) {
            this.f3585a.a(this.b);
            this.d = true;
        }
    }

    public void close() throws IOException {
        if (!this.e) {
            this.f3585a.close();
            this.e = true;
        }
    }

    public int read() throws IOException {
        if (read(this.c) == -1) {
            return -1;
        }
        return this.c[0] & 255;
    }

    public void s() throws IOException {
        t();
    }

    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        Assertions.b(!this.e);
        t();
        int read = this.f3585a.read(bArr, i, i2);
        if (read == -1) {
            return -1;
        }
        this.f += (long) read;
        return read;
    }
}
