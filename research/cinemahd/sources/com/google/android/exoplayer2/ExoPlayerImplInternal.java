package com.google.android.exoplayer2;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Pair;
import com.google.android.exoplayer2.DefaultMediaClock;
import com.google.android.exoplayer2.PlayerMessage;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaPeriod;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectorResult;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Clock;
import com.google.android.exoplayer2.util.HandlerWrapper;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

final class ExoPlayerImplInternal implements Handler.Callback, MediaPeriod.Callback, TrackSelector.InvalidationListener, MediaSource.SourceInfoRefreshListener, DefaultMediaClock.PlaybackParameterListener, PlayerMessage.Sender {
    private int A;
    private boolean B;
    private int C;
    private SeekPosition D;
    private long E;
    private int F;

    /* renamed from: a  reason: collision with root package name */
    private final Renderer[] f3158a;
    private final RendererCapabilities[] b;
    private final TrackSelector c;
    private final TrackSelectorResult d;
    private final LoadControl e;
    private final BandwidthMeter f;
    private final HandlerWrapper g;
    private final HandlerThread h;
    private final Handler i;
    private final ExoPlayer j;
    private final Timeline.Window k;
    private final Timeline.Period l;
    private final long m;
    private final boolean n;
    private final DefaultMediaClock o;
    private final PlaybackInfoUpdate p;
    private final ArrayList<PendingMessageInfo> q;
    private final Clock r;
    private final MediaPeriodQueue s = new MediaPeriodQueue();
    private SeekParameters t;
    private PlaybackInfo u;
    private MediaSource v;
    private Renderer[] w;
    private boolean x;
    private boolean y;
    private boolean z;

    private static final class MediaSourceRefreshInfo {

        /* renamed from: a  reason: collision with root package name */
        public final MediaSource f3159a;
        public final Timeline b;
        public final Object c;

        public MediaSourceRefreshInfo(MediaSource mediaSource, Timeline timeline, Object obj) {
            this.f3159a = mediaSource;
            this.b = timeline;
            this.c = obj;
        }
    }

    private static final class PlaybackInfoUpdate {

        /* renamed from: a  reason: collision with root package name */
        private PlaybackInfo f3161a;
        /* access modifiers changed from: private */
        public int b;
        /* access modifiers changed from: private */
        public boolean c;
        /* access modifiers changed from: private */
        public int d;

        private PlaybackInfoUpdate() {
        }

        public boolean a(PlaybackInfo playbackInfo) {
            return playbackInfo != this.f3161a || this.b > 0 || this.c;
        }

        public void b(PlaybackInfo playbackInfo) {
            this.f3161a = playbackInfo;
            this.b = 0;
            this.c = false;
        }

        public void a(int i) {
            this.b += i;
        }

        public void b(int i) {
            boolean z = true;
            if (!this.c || this.d == 4) {
                this.c = true;
                this.d = i;
                return;
            }
            if (i != 4) {
                z = false;
            }
            Assertions.a(z);
        }
    }

    private static final class SeekPosition {

        /* renamed from: a  reason: collision with root package name */
        public final Timeline f3162a;
        public final int b;
        public final long c;

        public SeekPosition(Timeline timeline, int i, long j) {
            this.f3162a = timeline;
            this.b = i;
            this.c = j;
        }
    }

    public ExoPlayerImplInternal(Renderer[] rendererArr, TrackSelector trackSelector, TrackSelectorResult trackSelectorResult, LoadControl loadControl, BandwidthMeter bandwidthMeter, boolean z2, int i2, boolean z3, Handler handler, ExoPlayer exoPlayer, Clock clock) {
        this.f3158a = rendererArr;
        this.c = trackSelector;
        this.d = trackSelectorResult;
        this.e = loadControl;
        this.f = bandwidthMeter;
        this.y = z2;
        this.A = i2;
        this.B = z3;
        this.i = handler;
        this.j = exoPlayer;
        this.r = clock;
        this.m = loadControl.c();
        this.n = loadControl.b();
        this.t = SeekParameters.d;
        this.u = PlaybackInfo.a(-9223372036854775807L, trackSelectorResult);
        this.p = new PlaybackInfoUpdate();
        this.b = new RendererCapabilities[rendererArr.length];
        for (int i3 = 0; i3 < rendererArr.length; i3++) {
            rendererArr[i3].setIndex(i3);
            this.b[i3] = rendererArr[i3].g();
        }
        this.o = new DefaultMediaClock(this, clock);
        this.q = new ArrayList<>();
        this.w = new Renderer[0];
        this.k = new Timeline.Window();
        this.l = new Timeline.Period();
        trackSelector.a((TrackSelector.InvalidationListener) this, bandwidthMeter);
        this.h = new HandlerThread("ExoPlayerImplInternal:Handler", -16);
        this.h.start();
        this.g = clock.a(this.h.getLooper(), this);
    }

    private void d() throws ExoPlaybackException, IOException {
        int i2;
        long b2 = this.r.b();
        r();
        if (!this.s.g()) {
            j();
            b(b2, 10);
            return;
        }
        MediaPeriodHolder e2 = this.s.e();
        TraceUtil.a("doSomeWork");
        s();
        long elapsedRealtime = SystemClock.elapsedRealtime() * 1000;
        e2.f3166a.a(this.u.m - this.m, this.n);
        boolean z2 = true;
        boolean z3 = true;
        for (Renderer renderer : this.w) {
            renderer.a(this.E, elapsedRealtime);
            z3 = z3 && renderer.a();
            boolean z4 = renderer.isReady() || renderer.a() || c(renderer);
            if (!z4) {
                renderer.e();
            }
            z2 = z2 && z4;
        }
        if (!z2) {
            j();
        }
        long j2 = e2.g.d;
        if (z3 && ((j2 == -9223372036854775807L || j2 <= this.u.m) && e2.g.f)) {
            c(4);
            q();
        } else if (this.u.f == 2 && i(z2)) {
            c(3);
            if (this.y) {
                p();
            }
        } else if (this.u.f == 3 && (this.w.length != 0 ? !z2 : !g())) {
            this.z = this.y;
            c(2);
            q();
        }
        if (this.u.f == 2) {
            for (Renderer e3 : this.w) {
                e3.e();
            }
        }
        if ((this.y && this.u.f == 3) || (i2 = this.u.f) == 2) {
            b(b2, 10);
        } else if (this.w.length == 0 || i2 == 4) {
            this.g.b(2);
        } else {
            b(b2, 1000);
        }
        TraceUtil.a();
    }

    private void e(boolean z2) throws ExoPlaybackException {
        MediaSource.MediaPeriodId mediaPeriodId = this.s.e().g.f3167a;
        long a2 = a(mediaPeriodId, this.u.m, true);
        if (a2 != this.u.m) {
            PlaybackInfo playbackInfo = this.u;
            this.u = playbackInfo.a(mediaPeriodId, a2, playbackInfo.e, e());
            if (z2) {
                this.p.b(4);
            }
        }
    }

    private void f(boolean z2) {
        PlaybackInfo playbackInfo = this.u;
        if (playbackInfo.g != z2) {
            this.u = playbackInfo.a(z2);
        }
    }

    private void g(boolean z2) throws ExoPlaybackException {
        this.z = false;
        this.y = z2;
        if (!z2) {
            q();
            s();
            return;
        }
        int i2 = this.u.f;
        if (i2 == 3) {
            p();
            this.g.a(2);
        } else if (i2 == 2) {
            this.g.a(2);
        }
    }

    private void h(boolean z2) throws ExoPlaybackException {
        this.B = z2;
        if (!this.s.b(z2)) {
            e(true);
        }
        d(false);
    }

    private void i() {
        if (this.p.a(this.u)) {
            this.i.obtainMessage(0, this.p.b, this.p.c ? this.p.d : -1, this.u).sendToTarget();
            this.p.b(this.u);
        }
    }

    private void j() throws IOException {
        MediaPeriodHolder d2 = this.s.d();
        MediaPeriodHolder f2 = this.s.f();
        if (d2 != null && !d2.e) {
            if (f2 == null || f2.h == d2) {
                Renderer[] rendererArr = this.w;
                int length = rendererArr.length;
                int i2 = 0;
                while (i2 < length) {
                    if (rendererArr[i2].c()) {
                        i2++;
                    } else {
                        return;
                    }
                }
                d2.f3166a.d();
            }
        }
    }

    private void k() throws IOException {
        if (this.s.d() != null) {
            Renderer[] rendererArr = this.w;
            int length = rendererArr.length;
            int i2 = 0;
            while (i2 < length) {
                if (rendererArr[i2].c()) {
                    i2++;
                } else {
                    return;
                }
            }
        }
        this.v.b();
    }

    private void l() throws IOException {
        this.s.a(this.E);
        if (this.s.h()) {
            MediaPeriodInfo a2 = this.s.a(this.E, this.u);
            if (a2 == null) {
                k();
                return;
            }
            this.s.a(this.b, this.c, this.e.d(), this.v, a2).a((MediaPeriod.Callback) this, a2.b);
            f(true);
            d(false);
        }
    }

    private void m() {
        a(true, true, true);
        this.e.e();
        c(1);
        this.h.quit();
        synchronized (this) {
            this.x = true;
            notifyAll();
        }
    }

    private void n() throws ExoPlaybackException {
        if (this.s.g()) {
            float f2 = this.o.b().f3170a;
            MediaPeriodHolder e2 = this.s.e();
            MediaPeriodHolder f3 = this.s.f();
            boolean z2 = true;
            while (e2 != null && e2.e) {
                if (e2.b(f2)) {
                    if (z2) {
                        MediaPeriodHolder e3 = this.s.e();
                        boolean a2 = this.s.a(e3);
                        boolean[] zArr = new boolean[this.f3158a.length];
                        long a3 = e3.a(this.u.m, a2, zArr);
                        PlaybackInfo playbackInfo = this.u;
                        if (!(playbackInfo.f == 4 || a3 == playbackInfo.m)) {
                            PlaybackInfo playbackInfo2 = this.u;
                            this.u = playbackInfo2.a(playbackInfo2.c, a3, playbackInfo2.e, e());
                            this.p.b(4);
                            b(a3);
                        }
                        boolean[] zArr2 = new boolean[this.f3158a.length];
                        int i2 = 0;
                        int i3 = 0;
                        while (true) {
                            Renderer[] rendererArr = this.f3158a;
                            if (i2 >= rendererArr.length) {
                                break;
                            }
                            Renderer renderer = rendererArr[i2];
                            zArr2[i2] = renderer.getState() != 0;
                            SampleStream sampleStream = e3.c[i2];
                            if (sampleStream != null) {
                                i3++;
                            }
                            if (zArr2[i2]) {
                                if (sampleStream != renderer.i()) {
                                    a(renderer);
                                } else if (zArr[i2]) {
                                    renderer.a(this.E);
                                }
                            }
                            i2++;
                        }
                        this.u = this.u.a(e3.i, e3.j);
                        a(zArr2, i3);
                    } else {
                        this.s.a(e2);
                        if (e2.e) {
                            e2.a(Math.max(e2.g.b, e2.c(this.E)), false);
                        }
                    }
                    d(true);
                    if (this.u.f != 4) {
                        h();
                        s();
                        this.g.a(2);
                        return;
                    }
                    return;
                }
                if (e2 == f3) {
                    z2 = false;
                }
                e2 = e2.h;
            }
        }
    }

    private void o() {
        for (int size = this.q.size() - 1; size >= 0; size--) {
            if (!a(this.q.get(size))) {
                this.q.get(size).f3160a.a(false);
                this.q.remove(size);
            }
        }
        Collections.sort(this.q);
    }

    private void p() throws ExoPlaybackException {
        this.z = false;
        this.o.a();
        for (Renderer start : this.w) {
            start.start();
        }
    }

    private void q() throws ExoPlaybackException {
        this.o.c();
        for (Renderer b2 : this.w) {
            b(b2);
        }
    }

    private void r() throws ExoPlaybackException, IOException {
        MediaSource mediaSource = this.v;
        if (mediaSource != null) {
            if (this.C > 0) {
                mediaSource.b();
                return;
            }
            l();
            MediaPeriodHolder d2 = this.s.d();
            int i2 = 0;
            if (d2 == null || d2.e()) {
                f(false);
            } else if (!this.u.g) {
                h();
            }
            if (this.s.g()) {
                MediaPeriodHolder e2 = this.s.e();
                MediaPeriodHolder f2 = this.s.f();
                boolean z2 = false;
                while (this.y && e2 != f2 && this.E >= e2.h.d()) {
                    if (z2) {
                        i();
                    }
                    int i3 = e2.g.e ? 0 : 3;
                    MediaPeriodHolder a2 = this.s.a();
                    a(e2);
                    PlaybackInfo playbackInfo = this.u;
                    MediaPeriodInfo mediaPeriodInfo = a2.g;
                    this.u = playbackInfo.a(mediaPeriodInfo.f3167a, mediaPeriodInfo.b, mediaPeriodInfo.c, e());
                    this.p.b(i3);
                    s();
                    e2 = a2;
                    z2 = true;
                }
                if (f2.g.f) {
                    while (true) {
                        Renderer[] rendererArr = this.f3158a;
                        if (i2 < rendererArr.length) {
                            Renderer renderer = rendererArr[i2];
                            SampleStream sampleStream = f2.c[i2];
                            if (sampleStream != null && renderer.i() == sampleStream && renderer.c()) {
                                renderer.d();
                            }
                            i2++;
                        } else {
                            return;
                        }
                    }
                } else if (f2.h != null) {
                    int i4 = 0;
                    while (true) {
                        Renderer[] rendererArr2 = this.f3158a;
                        if (i4 < rendererArr2.length) {
                            Renderer renderer2 = rendererArr2[i4];
                            SampleStream sampleStream2 = f2.c[i4];
                            if (renderer2.i() != sampleStream2) {
                                return;
                            }
                            if (sampleStream2 == null || renderer2.c()) {
                                i4++;
                            } else {
                                return;
                            }
                        } else if (!f2.h.e) {
                            j();
                            return;
                        } else {
                            TrackSelectorResult trackSelectorResult = f2.j;
                            MediaPeriodHolder b2 = this.s.b();
                            TrackSelectorResult trackSelectorResult2 = b2.j;
                            boolean z3 = b2.f3166a.c() != -9223372036854775807L;
                            int i5 = 0;
                            while (true) {
                                Renderer[] rendererArr3 = this.f3158a;
                                if (i5 < rendererArr3.length) {
                                    Renderer renderer3 = rendererArr3[i5];
                                    if (trackSelectorResult.a(i5)) {
                                        if (z3) {
                                            renderer3.d();
                                        } else if (!renderer3.f()) {
                                            TrackSelection a3 = trackSelectorResult2.c.a(i5);
                                            boolean a4 = trackSelectorResult2.a(i5);
                                            boolean z4 = this.b[i5].getTrackType() == 6;
                                            RendererConfiguration rendererConfiguration = trackSelectorResult.b[i5];
                                            RendererConfiguration rendererConfiguration2 = trackSelectorResult2.b[i5];
                                            if (!a4 || !rendererConfiguration2.equals(rendererConfiguration) || z4) {
                                                renderer3.d();
                                            } else {
                                                renderer3.a(a(a3), b2.c[i5], b2.c());
                                            }
                                        }
                                    }
                                    i5++;
                                } else {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void s() throws ExoPlaybackException {
        if (this.s.g()) {
            MediaPeriodHolder e2 = this.s.e();
            long c2 = e2.f3166a.c();
            if (c2 != -9223372036854775807L) {
                b(c2);
                if (c2 != this.u.m) {
                    PlaybackInfo playbackInfo = this.u;
                    this.u = playbackInfo.a(playbackInfo.c, c2, playbackInfo.e, e());
                    this.p.b(4);
                }
            } else {
                this.E = this.o.d();
                long c3 = e2.c(this.E);
                a(this.u.m, c3);
                this.u.m = c3;
            }
            MediaPeriodHolder d2 = this.s.d();
            this.u.k = d2.a();
            this.u.l = e();
        }
    }

    public void b(boolean z2) {
        this.g.a(13, z2 ? 1 : 0, 0).sendToTarget();
    }

    public void c(boolean z2) {
        this.g.a(6, z2 ? 1 : 0, 0).sendToTarget();
    }

    public boolean handleMessage(Message message) {
        try {
            switch (message.what) {
                case 0:
                    b((MediaSource) message.obj, message.arg1 != 0, message.arg2 != 0);
                    break;
                case 1:
                    g(message.arg1 != 0);
                    break;
                case 2:
                    d();
                    break;
                case 3:
                    a((SeekPosition) message.obj);
                    break;
                case 4:
                    b((PlaybackParameters) message.obj);
                    break;
                case 5:
                    a((SeekParameters) message.obj);
                    break;
                case 6:
                    a(message.arg1 != 0, true);
                    break;
                case 7:
                    m();
                    return true;
                case 8:
                    a((MediaSourceRefreshInfo) message.obj);
                    break;
                case 9:
                    d((MediaPeriod) message.obj);
                    break;
                case 10:
                    c((MediaPeriod) message.obj);
                    break;
                case 11:
                    n();
                    break;
                case 12:
                    b(message.arg1);
                    break;
                case 13:
                    h(message.arg1 != 0);
                    break;
                case 14:
                    d((PlayerMessage) message.obj);
                    break;
                case 15:
                    f((PlayerMessage) message.obj);
                    break;
                case 16:
                    a((PlaybackParameters) message.obj);
                    break;
                default:
                    return false;
            }
            i();
        } catch (ExoPlaybackException e2) {
            Log.a("ExoPlayerImplInternal", "Playback error.", e2);
            a(false, false);
            this.i.obtainMessage(2, e2).sendToTarget();
            i();
        } catch (IOException e3) {
            Log.a("ExoPlayerImplInternal", "Source error.", e3);
            a(false, false);
            this.i.obtainMessage(2, ExoPlaybackException.a(e3)).sendToTarget();
            i();
        } catch (RuntimeException e4) {
            Log.a("ExoPlayerImplInternal", "Internal runtime error.", e4);
            a(false, false);
            this.i.obtainMessage(2, ExoPlaybackException.a(e4)).sendToTarget();
            i();
        }
        return true;
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        this.g.a(16, (Object) playbackParameters).sendToTarget();
    }

    public void a(MediaSource mediaSource, boolean z2, boolean z3) {
        this.g.a(0, z2 ? 1 : 0, z3 ? 1 : 0, mediaSource).sendToTarget();
    }

    public Looper b() {
        return this.h.getLooper();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0022, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void c() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.x     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r2)
            return
        L_0x0007:
            com.google.android.exoplayer2.util.HandlerWrapper r0 = r2.g     // Catch:{ all -> 0x0023 }
            r1 = 7
            r0.a(r1)     // Catch:{ all -> 0x0023 }
            r0 = 0
        L_0x000e:
            boolean r1 = r2.x     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0018
            r2.wait()     // Catch:{ InterruptedException -> 0x0016 }
            goto L_0x000e
        L_0x0016:
            r0 = 1
            goto L_0x000e
        L_0x0018:
            if (r0 == 0) goto L_0x0021
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0023 }
            r0.interrupt()     // Catch:{ all -> 0x0023 }
        L_0x0021:
            monitor-exit(r2)
            return
        L_0x0023:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ExoPlayerImplInternal.c():void");
    }

    private static final class PendingMessageInfo implements Comparable<PendingMessageInfo> {

        /* renamed from: a  reason: collision with root package name */
        public final PlayerMessage f3160a;
        public int b;
        public long c;
        public Object d;

        public PendingMessageInfo(PlayerMessage playerMessage) {
            this.f3160a = playerMessage;
        }

        public void a(int i, long j, Object obj) {
            this.b = i;
            this.c = j;
            this.d = obj;
        }

        /* renamed from: a */
        public int compareTo(PendingMessageInfo pendingMessageInfo) {
            if ((this.d == null) != (pendingMessageInfo.d == null)) {
                if (this.d != null) {
                    return -1;
                }
                return 1;
            } else if (this.d == null) {
                return 0;
            } else {
                int i = this.b - pendingMessageInfo.b;
                if (i != 0) {
                    return i;
                }
                return Util.b(this.c, pendingMessageInfo.c);
            }
        }
    }

    private void f(PlayerMessage playerMessage) {
        playerMessage.c().post(new a(this, playerMessage));
    }

    /* renamed from: b */
    public void a(MediaPeriod mediaPeriod) {
        this.g.a(10, (Object) mediaPeriod).sendToTarget();
    }

    private void b(MediaSource mediaSource, boolean z2, boolean z3) {
        this.C++;
        a(true, z2, z3);
        this.e.a();
        this.v = mediaSource;
        c(2);
        mediaSource.a(this.j, true, this, this.f.a());
        this.g.a(2);
    }

    private void f() {
        c(4);
        a(false, true, false);
    }

    private void h() {
        MediaPeriodHolder d2 = this.s.d();
        long b2 = d2.b();
        if (b2 == Long.MIN_VALUE) {
            f(false);
            return;
        }
        boolean a2 = this.e.a(a(b2), this.o.b().f3170a);
        f(a2);
        if (a2) {
            d2.a(this.E);
        }
    }

    public void a(boolean z2) {
        this.g.a(1, z2 ? 1 : 0, 0).sendToTarget();
    }

    public void a(int i2) {
        this.g.a(12, i2, 0).sendToTarget();
    }

    public void a(Timeline timeline, int i2, long j2) {
        this.g.a(3, (Object) new SeekPosition(timeline, i2, j2)).sendToTarget();
    }

    private void c(int i2) {
        PlaybackInfo playbackInfo = this.u;
        if (playbackInfo.f != i2) {
            this.u = playbackInfo.a(i2);
        }
    }

    private void e(PlayerMessage playerMessage) throws ExoPlaybackException {
        if (playerMessage.c().getLooper() == this.g.a()) {
            c(playerMessage);
            int i2 = this.u.f;
            if (i2 == 3 || i2 == 2) {
                this.g.a(2);
                return;
            }
            return;
        }
        this.g.a(15, (Object) playerMessage).sendToTarget();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001b, code lost:
        r0 = r0.h;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean g() {
        /*
            r6 = this;
            com.google.android.exoplayer2.MediaPeriodQueue r0 = r6.s
            com.google.android.exoplayer2.MediaPeriodHolder r0 = r0.e()
            com.google.android.exoplayer2.MediaPeriodInfo r1 = r0.g
            long r1 = r1.d
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0030
            com.google.android.exoplayer2.PlaybackInfo r3 = r6.u
            long r3 = r3.m
            int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r5 < 0) goto L_0x0030
            com.google.android.exoplayer2.MediaPeriodHolder r0 = r0.h
            if (r0 == 0) goto L_0x002e
            boolean r1 = r0.e
            if (r1 != 0) goto L_0x0030
            com.google.android.exoplayer2.MediaPeriodInfo r0 = r0.g
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r0 = r0.f3167a
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x002e
            goto L_0x0030
        L_0x002e:
            r0 = 0
            goto L_0x0031
        L_0x0030:
            r0 = 1
        L_0x0031:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ExoPlayerImplInternal.g():boolean");
    }

    private boolean i(boolean z2) {
        if (this.w.length == 0) {
            return g();
        }
        if (!z2) {
            return false;
        }
        if (!this.u.g) {
            return true;
        }
        MediaPeriodHolder d2 = this.s.d();
        if ((d2.e() && d2.g.f) || this.e.a(e(), this.o.b().f3170a, this.z)) {
            return true;
        }
        return false;
    }

    public synchronized void a(PlayerMessage playerMessage) {
        if (this.x) {
            Log.d("ExoPlayerImplInternal", "Ignoring messages sent after release.");
            playerMessage.a(false);
            return;
        }
        this.g.a(14, (Object) playerMessage).sendToTarget();
    }

    private void c(PlayerMessage playerMessage) throws ExoPlaybackException {
        if (!playerMessage.j()) {
            try {
                playerMessage.f().a(playerMessage.h(), playerMessage.d());
            } finally {
                playerMessage.a(true);
            }
        }
    }

    private void b(int i2) throws ExoPlaybackException {
        this.A = i2;
        if (!this.s.a(i2)) {
            e(true);
        }
        d(false);
    }

    private long e() {
        return a(this.u.k);
    }

    private void c(MediaPeriod mediaPeriod) {
        if (this.s.a(mediaPeriod)) {
            this.s.a(this.E);
            h();
        }
    }

    public void a(MediaSource mediaSource, Timeline timeline, Object obj) {
        this.g.a(8, (Object) new MediaSourceRefreshInfo(mediaSource, timeline, obj)).sendToTarget();
    }

    private void b(long j2, long j3) {
        this.g.b(2);
        this.g.a(2, j2 + j3);
    }

    public void a(MediaPeriod mediaPeriod) {
        this.g.a(9, (Object) mediaPeriod).sendToTarget();
    }

    private boolean c(Renderer renderer) {
        MediaPeriodHolder mediaPeriodHolder = this.s.f().h;
        return mediaPeriodHolder != null && mediaPeriodHolder.e && renderer.c();
    }

    public void a() {
        this.g.a(11);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c A[Catch:{ all -> 0x00de }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.google.android.exoplayer2.ExoPlayerImplInternal.SeekPosition r23) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            r22 = this;
            r1 = r22
            r0 = r23
            com.google.android.exoplayer2.ExoPlayerImplInternal$PlaybackInfoUpdate r2 = r1.p
            r3 = 1
            r2.a((int) r3)
            android.util.Pair r2 = r1.a((com.google.android.exoplayer2.ExoPlayerImplInternal.SeekPosition) r0, (boolean) r3)
            r4 = 0
            r6 = 0
            r7 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r2 != 0) goto L_0x0028
            com.google.android.exoplayer2.PlaybackInfo r2 = r1.u
            boolean r9 = r1.B
            com.google.android.exoplayer2.Timeline$Window r10 = r1.k
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r2 = r2.a((boolean) r9, (com.google.android.exoplayer2.Timeline.Window) r10)
            r15 = r2
            r12 = r7
            r18 = r12
        L_0x0026:
            r2 = 1
            goto L_0x0057
        L_0x0028:
            java.lang.Object r9 = r2.first
            java.lang.Object r10 = r2.second
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            com.google.android.exoplayer2.MediaPeriodQueue r12 = r1.s
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r9 = r12.a((java.lang.Object) r9, (long) r10)
            boolean r12 = r9.a()
            if (r12 == 0) goto L_0x0043
            r12 = r4
            r15 = r9
            r18 = r10
            goto L_0x0026
        L_0x0043:
            java.lang.Object r2 = r2.second
            java.lang.Long r2 = (java.lang.Long) r2
            long r12 = r2.longValue()
            long r14 = r0.c
            int r2 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r2 != 0) goto L_0x0053
            r2 = 1
            goto L_0x0054
        L_0x0053:
            r2 = 0
        L_0x0054:
            r15 = r9
            r18 = r10
        L_0x0057:
            r9 = 2
            com.google.android.exoplayer2.source.MediaSource r10 = r1.v     // Catch:{ all -> 0x00de }
            if (r10 == 0) goto L_0x00c6
            int r10 = r1.C     // Catch:{ all -> 0x00de }
            if (r10 <= 0) goto L_0x0061
            goto L_0x00c6
        L_0x0061:
            int r0 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x006d
            r0 = 4
            r1.c((int) r0)     // Catch:{ all -> 0x00de }
            r1.a((boolean) r6, (boolean) r3, (boolean) r6)     // Catch:{ all -> 0x00de }
            goto L_0x00c8
        L_0x006d:
            com.google.android.exoplayer2.PlaybackInfo r0 = r1.u     // Catch:{ all -> 0x00de }
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r0 = r0.c     // Catch:{ all -> 0x00de }
            boolean r0 = r15.equals(r0)     // Catch:{ all -> 0x00de }
            if (r0 == 0) goto L_0x00b7
            com.google.android.exoplayer2.MediaPeriodQueue r0 = r1.s     // Catch:{ all -> 0x00de }
            com.google.android.exoplayer2.MediaPeriodHolder r0 = r0.e()     // Catch:{ all -> 0x00de }
            if (r0 == 0) goto L_0x008c
            int r7 = (r12 > r4 ? 1 : (r12 == r4 ? 0 : -1))
            if (r7 == 0) goto L_0x008c
            com.google.android.exoplayer2.source.MediaPeriod r0 = r0.f3166a     // Catch:{ all -> 0x00de }
            com.google.android.exoplayer2.SeekParameters r4 = r1.t     // Catch:{ all -> 0x00de }
            long r4 = r0.a((long) r12, (com.google.android.exoplayer2.SeekParameters) r4)     // Catch:{ all -> 0x00de }
            goto L_0x008d
        L_0x008c:
            r4 = r12
        L_0x008d:
            long r7 = com.google.android.exoplayer2.C.b(r4)     // Catch:{ all -> 0x00de }
            com.google.android.exoplayer2.PlaybackInfo r0 = r1.u     // Catch:{ all -> 0x00de }
            long r10 = r0.m     // Catch:{ all -> 0x00de }
            long r10 = com.google.android.exoplayer2.C.b(r10)     // Catch:{ all -> 0x00de }
            int r0 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r0 != 0) goto L_0x00b8
            com.google.android.exoplayer2.PlaybackInfo r0 = r1.u     // Catch:{ all -> 0x00de }
            long r3 = r0.m     // Catch:{ all -> 0x00de }
            com.google.android.exoplayer2.PlaybackInfo r14 = r1.u
            long r20 = r22.e()
            r16 = r3
            com.google.android.exoplayer2.PlaybackInfo r0 = r14.a(r15, r16, r18, r20)
            r1.u = r0
            if (r2 == 0) goto L_0x00b6
            com.google.android.exoplayer2.ExoPlayerImplInternal$PlaybackInfoUpdate r0 = r1.p
            r0.b((int) r9)
        L_0x00b6:
            return
        L_0x00b7:
            r4 = r12
        L_0x00b8:
            long r4 = r1.a((com.google.android.exoplayer2.source.MediaSource.MediaPeriodId) r15, (long) r4)     // Catch:{ all -> 0x00de }
            int r0 = (r12 > r4 ? 1 : (r12 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x00c1
            goto L_0x00c2
        L_0x00c1:
            r3 = 0
        L_0x00c2:
            r2 = r2 | r3
            r16 = r4
            goto L_0x00ca
        L_0x00c6:
            r1.D = r0     // Catch:{ all -> 0x00de }
        L_0x00c8:
            r16 = r12
        L_0x00ca:
            com.google.android.exoplayer2.PlaybackInfo r14 = r1.u
            long r20 = r22.e()
            com.google.android.exoplayer2.PlaybackInfo r0 = r14.a(r15, r16, r18, r20)
            r1.u = r0
            if (r2 == 0) goto L_0x00dd
            com.google.android.exoplayer2.ExoPlayerImplInternal$PlaybackInfoUpdate r0 = r1.p
            r0.b((int) r9)
        L_0x00dd:
            return
        L_0x00de:
            r0 = move-exception
            com.google.android.exoplayer2.PlaybackInfo r14 = r1.u
            long r20 = r22.e()
            r16 = r12
            com.google.android.exoplayer2.PlaybackInfo r3 = r14.a(r15, r16, r18, r20)
            r1.u = r3
            if (r2 == 0) goto L_0x00f4
            com.google.android.exoplayer2.ExoPlayerImplInternal$PlaybackInfoUpdate r2 = r1.p
            r2.b((int) r9)
        L_0x00f4:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ExoPlayerImplInternal.a(com.google.android.exoplayer2.ExoPlayerImplInternal$SeekPosition):void");
    }

    private void b(long j2) throws ExoPlaybackException {
        if (this.s.g()) {
            j2 = this.s.e().d(j2);
        }
        this.E = j2;
        this.o.a(this.E);
        for (Renderer a2 : this.w) {
            a2.a(this.E);
        }
    }

    private void b(PlaybackParameters playbackParameters) {
        this.o.a(playbackParameters);
    }

    public /* synthetic */ void b(PlayerMessage playerMessage) {
        try {
            c(playerMessage);
        } catch (ExoPlaybackException e2) {
            Log.a("ExoPlayerImplInternal", "Unexpected error delivering message on external thread.", e2);
            throw new RuntimeException(e2);
        }
    }

    private void b(Renderer renderer) throws ExoPlaybackException {
        if (renderer.getState() == 2) {
            renderer.stop();
        }
    }

    private Pair<Object, Long> b(Timeline timeline, int i2, long j2) {
        return timeline.a(this.k, this.l, i2, j2);
    }

    private void d(PlayerMessage playerMessage) throws ExoPlaybackException {
        if (playerMessage.e() == -9223372036854775807L) {
            e(playerMessage);
        } else if (this.v == null || this.C > 0) {
            this.q.add(new PendingMessageInfo(playerMessage));
        } else {
            PendingMessageInfo pendingMessageInfo = new PendingMessageInfo(playerMessage);
            if (a(pendingMessageInfo)) {
                this.q.add(pendingMessageInfo);
                Collections.sort(this.q);
                return;
            }
            playerMessage.a(false);
        }
    }

    private void d(MediaPeriod mediaPeriod) throws ExoPlaybackException {
        if (this.s.a(mediaPeriod)) {
            MediaPeriodHolder d2 = this.s.d();
            d2.a(this.o.b().f3170a);
            a(d2.i, d2.j);
            if (!this.s.g()) {
                b(this.s.a().g.b);
                a((MediaPeriodHolder) null);
            }
            h();
        }
    }

    private long a(MediaSource.MediaPeriodId mediaPeriodId, long j2) throws ExoPlaybackException {
        return a(mediaPeriodId, j2, this.s.e() != this.s.f());
    }

    private long a(MediaSource.MediaPeriodId mediaPeriodId, long j2, boolean z2) throws ExoPlaybackException {
        q();
        this.z = false;
        c(2);
        MediaPeriodHolder e2 = this.s.e();
        MediaPeriodHolder mediaPeriodHolder = e2;
        while (true) {
            if (mediaPeriodHolder != null) {
                if (mediaPeriodId.equals(mediaPeriodHolder.g.f3167a) && mediaPeriodHolder.e) {
                    this.s.a(mediaPeriodHolder);
                    break;
                }
                mediaPeriodHolder = this.s.a();
            } else {
                break;
            }
        }
        if (e2 != mediaPeriodHolder || z2) {
            for (Renderer a2 : this.w) {
                a(a2);
            }
            this.w = new Renderer[0];
            e2 = null;
        }
        if (mediaPeriodHolder != null) {
            a(e2);
            if (mediaPeriodHolder.f) {
                long a3 = mediaPeriodHolder.f3166a.a(j2);
                mediaPeriodHolder.f3166a.a(a3 - this.m, this.n);
                j2 = a3;
            }
            b(j2);
            h();
        } else {
            this.s.a(true);
            this.u = this.u.a(TrackGroupArray.d, this.d);
            b(j2);
        }
        d(false);
        this.g.a(2);
        return j2;
    }

    private void d(boolean z2) {
        long j2;
        MediaPeriodHolder d2 = this.s.d();
        MediaSource.MediaPeriodId mediaPeriodId = d2 == null ? this.u.c : d2.g.f3167a;
        boolean z3 = !this.u.j.equals(mediaPeriodId);
        if (z3) {
            this.u = this.u.a(mediaPeriodId);
        }
        PlaybackInfo playbackInfo = this.u;
        if (d2 == null) {
            j2 = playbackInfo.m;
        } else {
            j2 = d2.a();
        }
        playbackInfo.k = j2;
        this.u.l = e();
        if ((z3 || z2) && d2 != null && d2.e) {
            a(d2.i, d2.j);
        }
    }

    private void a(SeekParameters seekParameters) {
        this.t = seekParameters;
    }

    private void a(boolean z2, boolean z3) {
        a(true, z2, z2);
        this.p.a(this.C + (z3 ? 1 : 0));
        this.C = 0;
        this.e.onStopped();
        c(1);
    }

    private void a(boolean z2, boolean z3, boolean z4) {
        long j2;
        MediaSource mediaSource;
        this.g.b(2);
        this.z = false;
        this.o.c();
        this.E = 0;
        for (Renderer a2 : this.w) {
            try {
                a(a2);
            } catch (ExoPlaybackException | RuntimeException e2) {
                Log.a("ExoPlayerImplInternal", "Stop failed.", e2);
            }
        }
        this.w = new Renderer[0];
        this.s.a(!z3);
        f(false);
        if (z3) {
            this.D = null;
        }
        if (z4) {
            this.s.a(Timeline.f3175a);
            Iterator<PendingMessageInfo> it2 = this.q.iterator();
            while (it2.hasNext()) {
                it2.next().f3160a.a(false);
            }
            this.q.clear();
            this.F = 0;
        }
        MediaSource.MediaPeriodId a3 = z3 ? this.u.a(this.B, this.k) : this.u.c;
        long j3 = -9223372036854775807L;
        if (z3) {
            j2 = -9223372036854775807L;
        } else {
            j2 = this.u.m;
        }
        if (!z3) {
            j3 = this.u.e;
        }
        long j4 = j3;
        Timeline timeline = z4 ? Timeline.f3175a : this.u.f3169a;
        Object obj = z4 ? null : this.u.b;
        PlaybackInfo playbackInfo = this.u;
        this.u = new PlaybackInfo(timeline, obj, a3, j2, j4, playbackInfo.f, false, z4 ? TrackGroupArray.d : playbackInfo.h, z4 ? this.d : this.u.i, a3, j2, 0, j2);
        if (z2 && (mediaSource = this.v) != null) {
            mediaSource.a((MediaSource.SourceInfoRefreshListener) this);
            this.v = null;
        }
    }

    private boolean a(PendingMessageInfo pendingMessageInfo) {
        Object obj = pendingMessageInfo.d;
        if (obj == null) {
            Pair<Object, Long> a2 = a(new SeekPosition(pendingMessageInfo.f3160a.g(), pendingMessageInfo.f3160a.i(), C.a(pendingMessageInfo.f3160a.e())), false);
            if (a2 == null) {
                return false;
            }
            pendingMessageInfo.a(this.u.f3169a.a(a2.first), ((Long) a2.second).longValue(), a2.first);
            return true;
        }
        int a3 = this.u.f3169a.a(obj);
        if (a3 == -1) {
            return false;
        }
        pendingMessageInfo.b = a3;
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0097 A[LOOP:1: B:24:0x0075->B:34:0x0097, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x005f A[EDGE_INSN: B:57:0x005f->B:20:0x005f ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0074 A[EDGE_INSN: B:60:0x0074->B:23:0x0074 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(long r7, long r9) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            r6 = this;
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r0 = r6.q
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00f1
            com.google.android.exoplayer2.PlaybackInfo r0 = r6.u
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r0 = r0.c
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0014
            goto L_0x00f1
        L_0x0014:
            com.google.android.exoplayer2.PlaybackInfo r0 = r6.u
            long r0 = r0.d
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 != 0) goto L_0x001f
            r0 = 1
            long r7 = r7 - r0
        L_0x001f:
            com.google.android.exoplayer2.PlaybackInfo r0 = r6.u
            com.google.android.exoplayer2.Timeline r1 = r0.f3169a
            com.google.android.exoplayer2.source.MediaSource$MediaPeriodId r0 = r0.c
            java.lang.Object r0 = r0.f3414a
            int r0 = r1.a((java.lang.Object) r0)
            int r1 = r6.F
            r2 = 0
            if (r1 <= 0) goto L_0x003b
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r3 = r6.q
            int r1 = r1 + -1
            java.lang.Object r1 = r3.get(r1)
            com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo r1 = (com.google.android.exoplayer2.ExoPlayerImplInternal.PendingMessageInfo) r1
            goto L_0x003c
        L_0x003b:
            r1 = r2
        L_0x003c:
            if (r1 == 0) goto L_0x005f
            int r3 = r1.b
            if (r3 > r0) goto L_0x004a
            if (r3 != r0) goto L_0x005f
            long r3 = r1.c
            int r1 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r1 <= 0) goto L_0x005f
        L_0x004a:
            int r1 = r6.F
            int r1 = r1 + -1
            r6.F = r1
            int r1 = r6.F
            if (r1 <= 0) goto L_0x003b
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r3 = r6.q
            int r1 = r1 + -1
            java.lang.Object r1 = r3.get(r1)
            com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo r1 = (com.google.android.exoplayer2.ExoPlayerImplInternal.PendingMessageInfo) r1
            goto L_0x003c
        L_0x005f:
            int r1 = r6.F
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r3 = r6.q
            int r3 = r3.size()
            if (r1 >= r3) goto L_0x0074
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r1 = r6.q
            int r3 = r6.F
            java.lang.Object r1 = r1.get(r3)
            com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo r1 = (com.google.android.exoplayer2.ExoPlayerImplInternal.PendingMessageInfo) r1
            goto L_0x0075
        L_0x0074:
            r1 = r2
        L_0x0075:
            if (r1 == 0) goto L_0x00a2
            java.lang.Object r3 = r1.d
            if (r3 == 0) goto L_0x00a2
            int r3 = r1.b
            if (r3 < r0) goto L_0x0087
            if (r3 != r0) goto L_0x00a2
            long r3 = r1.c
            int r5 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r5 > 0) goto L_0x00a2
        L_0x0087:
            int r1 = r6.F
            int r1 = r1 + 1
            r6.F = r1
            int r1 = r6.F
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r3 = r6.q
            int r3 = r3.size()
            if (r1 >= r3) goto L_0x0074
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r1 = r6.q
            int r3 = r6.F
            java.lang.Object r1 = r1.get(r3)
            com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo r1 = (com.google.android.exoplayer2.ExoPlayerImplInternal.PendingMessageInfo) r1
            goto L_0x0075
        L_0x00a2:
            if (r1 == 0) goto L_0x00f1
            java.lang.Object r3 = r1.d
            if (r3 == 0) goto L_0x00f1
            int r3 = r1.b
            if (r3 != r0) goto L_0x00f1
            long r3 = r1.c
            int r5 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r5 <= 0) goto L_0x00f1
            int r5 = (r3 > r9 ? 1 : (r3 == r9 ? 0 : -1))
            if (r5 > 0) goto L_0x00f1
            com.google.android.exoplayer2.PlayerMessage r3 = r1.f3160a
            r6.e((com.google.android.exoplayer2.PlayerMessage) r3)
            com.google.android.exoplayer2.PlayerMessage r3 = r1.f3160a
            boolean r3 = r3.b()
            if (r3 != 0) goto L_0x00d3
            com.google.android.exoplayer2.PlayerMessage r1 = r1.f3160a
            boolean r1 = r1.j()
            if (r1 == 0) goto L_0x00cc
            goto L_0x00d3
        L_0x00cc:
            int r1 = r6.F
            int r1 = r1 + 1
            r6.F = r1
            goto L_0x00da
        L_0x00d3:
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r1 = r6.q
            int r3 = r6.F
            r1.remove(r3)
        L_0x00da:
            int r1 = r6.F
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r3 = r6.q
            int r3 = r3.size()
            if (r1 >= r3) goto L_0x00ef
            java.util.ArrayList<com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo> r1 = r6.q
            int r3 = r6.F
            java.lang.Object r1 = r1.get(r3)
            com.google.android.exoplayer2.ExoPlayerImplInternal$PendingMessageInfo r1 = (com.google.android.exoplayer2.ExoPlayerImplInternal.PendingMessageInfo) r1
            goto L_0x00a2
        L_0x00ef:
            r1 = r2
            goto L_0x00a2
        L_0x00f1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ExoPlayerImplInternal.a(long, long):void");
    }

    private void a(Renderer renderer) throws ExoPlaybackException {
        this.o.a(renderer);
        b(renderer);
        renderer.disable();
    }

    private void a(float f2) {
        for (MediaPeriodHolder c2 = this.s.c(); c2 != null; c2 = c2.h) {
            TrackSelectorResult trackSelectorResult = c2.j;
            if (trackSelectorResult != null) {
                for (TrackSelection trackSelection : trackSelectorResult.c.a()) {
                    if (trackSelection != null) {
                        trackSelection.a(f2);
                    }
                }
            }
        }
    }

    private void a(MediaSourceRefreshInfo mediaSourceRefreshInfo) throws ExoPlaybackException {
        MediaSourceRefreshInfo mediaSourceRefreshInfo2 = mediaSourceRefreshInfo;
        if (mediaSourceRefreshInfo2.f3159a == this.v) {
            Timeline timeline = this.u.f3169a;
            Timeline timeline2 = mediaSourceRefreshInfo2.b;
            Object obj = mediaSourceRefreshInfo2.c;
            this.s.a(timeline2);
            this.u = this.u.a(timeline2, obj);
            o();
            int i2 = this.C;
            long j2 = 0;
            if (i2 > 0) {
                this.p.a(i2);
                this.C = 0;
                SeekPosition seekPosition = this.D;
                if (seekPosition != null) {
                    try {
                        Pair<Object, Long> a2 = a(seekPosition, true);
                        this.D = null;
                        if (a2 == null) {
                            f();
                            return;
                        }
                        Object obj2 = a2.first;
                        long longValue = ((Long) a2.second).longValue();
                        MediaSource.MediaPeriodId a3 = this.s.a(obj2, longValue);
                        this.u = this.u.a(a3, a3.a() ? 0 : longValue, longValue);
                    } catch (IllegalSeekPositionException e2) {
                        IllegalSeekPositionException illegalSeekPositionException = e2;
                        this.u = this.u.a(this.u.a(this.B, this.k), -9223372036854775807L, -9223372036854775807L);
                        throw illegalSeekPositionException;
                    }
                } else if (this.u.d != -9223372036854775807L) {
                } else {
                    if (timeline2.c()) {
                        f();
                        return;
                    }
                    Pair<Object, Long> b2 = b(timeline2, timeline2.a(this.B), -9223372036854775807L);
                    Object obj3 = b2.first;
                    long longValue2 = ((Long) b2.second).longValue();
                    MediaSource.MediaPeriodId a4 = this.s.a(obj3, longValue2);
                    this.u = this.u.a(a4, a4.a() ? 0 : longValue2, longValue2);
                }
            } else if (!timeline.c()) {
                MediaPeriodHolder c2 = this.s.c();
                PlaybackInfo playbackInfo = this.u;
                long j3 = playbackInfo.e;
                Object obj4 = c2 == null ? playbackInfo.c.f3414a : c2.b;
                if (timeline2.a(obj4) == -1) {
                    Object a5 = a(obj4, timeline, timeline2);
                    if (a5 == null) {
                        f();
                        return;
                    }
                    Pair<Object, Long> b3 = b(timeline2, timeline2.a(a5, this.l).b, -9223372036854775807L);
                    Object obj5 = b3.first;
                    long longValue3 = ((Long) b3.second).longValue();
                    MediaSource.MediaPeriodId a6 = this.s.a(obj5, longValue3);
                    if (c2 != null) {
                        while (true) {
                            c2 = c2.h;
                            if (c2 == null) {
                                break;
                            } else if (c2.g.f3167a.equals(a6)) {
                                c2.g = this.s.a(c2.g);
                            }
                        }
                    }
                    if (!a6.a()) {
                        j2 = longValue3;
                    }
                    this.u = this.u.a(a6, a(a6, j2), longValue3, e());
                    return;
                }
                MediaSource.MediaPeriodId mediaPeriodId = this.u.c;
                if (mediaPeriodId.a()) {
                    MediaSource.MediaPeriodId a7 = this.s.a(obj4, j3);
                    if (!a7.equals(mediaPeriodId)) {
                        if (!a7.a()) {
                            j2 = j3;
                        }
                        this.u = this.u.a(a7, a(a7, j2), j3, e());
                        return;
                    }
                }
                if (!this.s.a(mediaPeriodId, this.E)) {
                    e(false);
                }
                d(false);
            } else if (!timeline2.c()) {
                Pair<Object, Long> b4 = b(timeline2, timeline2.a(this.B), -9223372036854775807L);
                Object obj6 = b4.first;
                long longValue4 = ((Long) b4.second).longValue();
                MediaSource.MediaPeriodId a8 = this.s.a(obj6, longValue4);
                this.u = this.u.a(a8, a8.a() ? 0 : longValue4, longValue4);
            }
        }
    }

    private Object a(Object obj, Timeline timeline, Timeline timeline2) {
        int a2 = timeline.a(obj);
        int a3 = timeline.a();
        int i2 = a2;
        int i3 = -1;
        for (int i4 = 0; i4 < a3 && i3 == -1; i4++) {
            i2 = timeline.a(i2, this.l, this.k, this.A, this.B);
            if (i2 == -1) {
                break;
            }
            i3 = timeline2.a(timeline.a(i2));
        }
        if (i3 == -1) {
            return null;
        }
        return timeline2.a(i3);
    }

    private Pair<Object, Long> a(SeekPosition seekPosition, boolean z2) {
        int a2;
        Timeline timeline = this.u.f3169a;
        Timeline timeline2 = seekPosition.f3162a;
        if (timeline.c()) {
            return null;
        }
        if (timeline2.c()) {
            timeline2 = timeline;
        }
        try {
            Pair<Object, Long> a3 = timeline2.a(this.k, this.l, seekPosition.b, seekPosition.c);
            if (timeline == timeline2 || (a2 = timeline.a(a3.first)) != -1) {
                return a3;
            }
            if (!z2 || a(a3.first, timeline2, timeline) == null) {
                return null;
            }
            return b(timeline, timeline.a(a2, this.l).b, -9223372036854775807L);
        } catch (IndexOutOfBoundsException unused) {
            throw new IllegalSeekPositionException(timeline, seekPosition.b, seekPosition.c);
        }
    }

    private void a(PlaybackParameters playbackParameters) throws ExoPlaybackException {
        this.i.obtainMessage(1, playbackParameters).sendToTarget();
        a(playbackParameters.f3170a);
        for (Renderer renderer : this.f3158a) {
            if (renderer != null) {
                renderer.a(playbackParameters.f3170a);
            }
        }
    }

    private void a(MediaPeriodHolder mediaPeriodHolder) throws ExoPlaybackException {
        MediaPeriodHolder e2 = this.s.e();
        if (e2 != null && mediaPeriodHolder != e2) {
            boolean[] zArr = new boolean[this.f3158a.length];
            int i2 = 0;
            int i3 = 0;
            while (true) {
                Renderer[] rendererArr = this.f3158a;
                if (i2 < rendererArr.length) {
                    Renderer renderer = rendererArr[i2];
                    zArr[i2] = renderer.getState() != 0;
                    if (e2.j.a(i2)) {
                        i3++;
                    }
                    if (zArr[i2] && (!e2.j.a(i2) || (renderer.f() && renderer.i() == mediaPeriodHolder.c[i2]))) {
                        a(renderer);
                    }
                    i2++;
                } else {
                    this.u = this.u.a(e2.i, e2.j);
                    a(zArr, i3);
                    return;
                }
            }
        }
    }

    private void a(boolean[] zArr, int i2) throws ExoPlaybackException {
        this.w = new Renderer[i2];
        MediaPeriodHolder e2 = this.s.e();
        int i3 = 0;
        for (int i4 = 0; i4 < this.f3158a.length; i4++) {
            if (e2.j.a(i4)) {
                a(i4, zArr[i4], i3);
                i3++;
            }
        }
    }

    private void a(int i2, boolean z2, int i3) throws ExoPlaybackException {
        MediaPeriodHolder e2 = this.s.e();
        Renderer renderer = this.f3158a[i2];
        this.w[i3] = renderer;
        if (renderer.getState() == 0) {
            TrackSelectorResult trackSelectorResult = e2.j;
            RendererConfiguration rendererConfiguration = trackSelectorResult.b[i2];
            Format[] a2 = a(trackSelectorResult.c.a(i2));
            boolean z3 = this.y && this.u.f == 3;
            renderer.a(rendererConfiguration, a2, e2.c[i2], this.E, !z2 && z3, e2.c());
            this.o.b(renderer);
            if (z3) {
                renderer.start();
            }
        }
    }

    private long a(long j2) {
        MediaPeriodHolder d2 = this.s.d();
        if (d2 == null) {
            return 0;
        }
        return j2 - d2.c(this.E);
    }

    private void a(TrackGroupArray trackGroupArray, TrackSelectorResult trackSelectorResult) {
        this.e.a(this.f3158a, trackGroupArray, trackSelectorResult.c);
    }

    private static Format[] a(TrackSelection trackSelection) {
        int length = trackSelection != null ? trackSelection.length() : 0;
        Format[] formatArr = new Format[length];
        for (int i2 = 0; i2 < length; i2++) {
            formatArr[i2] = trackSelection.a(i2);
        }
        return formatArr;
    }
}
