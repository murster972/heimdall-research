package com.google.android.exoplayer2.drm;

import android.media.MediaDrm;
import com.google.android.exoplayer2.drm.ExoMediaDrm;

/* compiled from: lambda */
public final /* synthetic */ class d implements MediaDrm.OnEventListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ FrameworkMediaDrm f3237a;
    private final /* synthetic */ ExoMediaDrm.OnEventListener b;

    public /* synthetic */ d(FrameworkMediaDrm frameworkMediaDrm, ExoMediaDrm.OnEventListener onEventListener) {
        this.f3237a = frameworkMediaDrm;
        this.b = onEventListener;
    }

    public final void onEvent(MediaDrm mediaDrm, byte[] bArr, int i, int i2, byte[] bArr2) {
        this.f3237a.a(this.b, mediaDrm, bArr, i, i2, bArr2);
    }
}
