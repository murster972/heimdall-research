package com.google.android.exoplayer2.drm;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.media.DeniedByServerException;
import android.media.MediaCrypto;
import android.media.MediaCryptoException;
import android.media.MediaDrm;
import android.media.MediaDrmException;
import android.media.NotProvisionedException;
import android.media.UnsupportedSchemeException;
import android.text.TextUtils;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.ExoMediaDrm;
import com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@TargetApi(23)
public final class FrameworkMediaDrm implements ExoMediaDrm<FrameworkMediaCrypto> {

    /* renamed from: a  reason: collision with root package name */
    private final UUID f3232a;
    private final MediaDrm b;

    private FrameworkMediaDrm(UUID uuid) throws UnsupportedSchemeException {
        Assertions.a(uuid);
        Assertions.a(!C.b.equals(uuid), "Use C.CLEARKEY_UUID instead");
        this.f3232a = uuid;
        this.b = new MediaDrm(a(uuid));
        if (C.d.equals(uuid) && c()) {
            a(this.b);
        }
    }

    public void a(ExoMediaDrm.OnEventListener<? super FrameworkMediaCrypto> onEventListener) {
        this.b.setOnEventListener(onEventListener == null ? null : new d(this, onEventListener));
    }

    public void c(byte[] bArr) throws DeniedByServerException {
        this.b.provideProvisionResponse(bArr);
    }

    public void d(byte[] bArr) {
        this.b.closeSession(bArr);
    }

    public static FrameworkMediaDrm b(UUID uuid) throws UnsupportedDrmException {
        try {
            return new FrameworkMediaDrm(uuid);
        } catch (UnsupportedSchemeException e) {
            throw new UnsupportedDrmException(1, e);
        } catch (Exception e2) {
            throw new UnsupportedDrmException(2, e2);
        }
    }

    private static boolean c() {
        return "ASUS_Z00AD".equals(Util.d);
    }

    public /* synthetic */ void a(ExoMediaDrm.OnEventListener onEventListener, MediaDrm mediaDrm, byte[] bArr, int i, int i2, byte[] bArr2) {
        onEventListener.a(this, bArr, i, i2, bArr2);
    }

    public ExoMediaDrm.KeyRequest a(byte[] bArr, List<DrmInitData.SchemeData> list, int i, HashMap<String, String> hashMap) throws NotProvisionedException {
        String str;
        byte[] bArr2;
        DrmInitData.SchemeData schemeData = null;
        if (list != null) {
            schemeData = a(this.f3232a, list);
            bArr2 = b(this.f3232a, schemeData.e);
            str = a(this.f3232a, schemeData.d);
        } else {
            bArr2 = null;
            str = null;
        }
        MediaDrm.KeyRequest keyRequest = this.b.getKeyRequest(bArr, bArr2, str, i, hashMap);
        byte[] a2 = a(this.f3232a, keyRequest.getData());
        String defaultUrl = keyRequest.getDefaultUrl();
        if (TextUtils.isEmpty(defaultUrl) && schemeData != null && !TextUtils.isEmpty(schemeData.c)) {
            defaultUrl = schemeData.c;
        }
        return new ExoMediaDrm.KeyRequest(a2, defaultUrl);
    }

    public byte[] b() throws MediaDrmException {
        return this.b.openSession();
    }

    public byte[] b(byte[] bArr, byte[] bArr2) throws NotProvisionedException, DeniedByServerException {
        if (C.c.equals(this.f3232a)) {
            bArr2 = ClearKeyUtil.b(bArr2);
        }
        return this.b.provideKeyResponse(bArr, bArr2);
    }

    public FrameworkMediaCrypto b(byte[] bArr) throws MediaCryptoException {
        return new FrameworkMediaCrypto(new MediaCrypto(a(this.f3232a), bArr), Util.f3651a < 21 && C.d.equals(this.f3232a) && "L3".equals(a("securityLevel")));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003e, code lost:
        r2 = com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil.a(r3, r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] b(java.util.UUID r2, byte[] r3) {
        /*
            int r0 = com.google.android.exoplayer2.util.Util.f3651a
            r1 = 21
            if (r0 >= r1) goto L_0x000e
            java.util.UUID r0 = com.google.android.exoplayer2.C.d
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x003e
        L_0x000e:
            java.util.UUID r0 = com.google.android.exoplayer2.C.e
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = com.google.android.exoplayer2.util.Util.c
            java.lang.String r1 = "Amazon"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = com.google.android.exoplayer2.util.Util.d
            java.lang.String r1 = "AFTB"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x003e
            java.lang.String r0 = com.google.android.exoplayer2.util.Util.d
            java.lang.String r1 = "AFTS"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x003e
            java.lang.String r0 = com.google.android.exoplayer2.util.Util.d
            java.lang.String r1 = "AFTM"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0045
        L_0x003e:
            byte[] r2 = com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil.a((byte[]) r3, (java.util.UUID) r2)
            if (r2 == 0) goto L_0x0045
            return r2
        L_0x0045:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.drm.FrameworkMediaDrm.b(java.util.UUID, byte[]):byte[]");
    }

    public ExoMediaDrm.ProvisionRequest a() {
        MediaDrm.ProvisionRequest provisionRequest = this.b.getProvisionRequest();
        return new ExoMediaDrm.ProvisionRequest(provisionRequest.getData(), provisionRequest.getDefaultUrl());
    }

    public Map<String, String> a(byte[] bArr) {
        return this.b.queryKeyStatus(bArr);
    }

    public void a(byte[] bArr, byte[] bArr2) {
        this.b.restoreKeys(bArr, bArr2);
    }

    public String a(String str) {
        return this.b.getPropertyString(str);
    }

    public void a(String str, String str2) {
        this.b.setPropertyString(str, str2);
    }

    private static DrmInitData.SchemeData a(UUID uuid, List<DrmInitData.SchemeData> list) {
        boolean z;
        if (!C.d.equals(uuid)) {
            return list.get(0);
        }
        if (Util.f3651a >= 28 && list.size() > 1) {
            DrmInitData.SchemeData schemeData = list.get(0);
            int i = 0;
            int i2 = 0;
            while (true) {
                if (i >= list.size()) {
                    z = true;
                    break;
                }
                DrmInitData.SchemeData schemeData2 = list.get(i);
                if (schemeData2.f != schemeData.f || !Util.a((Object) schemeData2.d, (Object) schemeData.d) || !Util.a((Object) schemeData2.c, (Object) schemeData.c) || !PsshAtomUtil.a(schemeData2.e)) {
                    z = false;
                } else {
                    i2 += schemeData2.e.length;
                    i++;
                }
            }
            z = false;
            if (z) {
                byte[] bArr = new byte[i2];
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    byte[] bArr2 = list.get(i4).e;
                    int length = bArr2.length;
                    System.arraycopy(bArr2, 0, bArr, i3, length);
                    i3 += length;
                }
                return schemeData.a(bArr);
            }
        }
        for (int i5 = 0; i5 < list.size(); i5++) {
            DrmInitData.SchemeData schemeData3 = list.get(i5);
            int d = PsshAtomUtil.d(schemeData3.e);
            if (Util.f3651a < 23 && d == 0) {
                return schemeData3;
            }
            if (Util.f3651a >= 23 && d == 1) {
                return schemeData3;
            }
        }
        return list.get(0);
    }

    private static UUID a(UUID uuid) {
        return (Util.f3651a >= 27 || !C.c.equals(uuid)) ? uuid : C.b;
    }

    private static String a(UUID uuid, String str) {
        return (Util.f3651a >= 26 || !C.c.equals(uuid) || (!"video/mp4".equals(str) && !"audio/mp4".equals(str))) ? str : "cenc";
    }

    private static byte[] a(UUID uuid, byte[] bArr) {
        return C.c.equals(uuid) ? ClearKeyUtil.a(bArr) : bArr;
    }

    @SuppressLint({"WrongConstant"})
    private static void a(MediaDrm mediaDrm) {
        mediaDrm.setPropertyString("securityLevel", "L3");
    }
}
