package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.util.EventDispatcher;

/* compiled from: lambda */
public final /* synthetic */ class b implements EventDispatcher.Event {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Exception f3235a;

    public /* synthetic */ b(Exception exc) {
        this.f3235a = exc;
    }

    public final void a(Object obj) {
        ((DefaultDrmSessionEventListener) obj).a(this.f3235a);
    }
}
