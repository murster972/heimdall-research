package com.google.android.exoplayer2.drm;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public final class DrmInitData implements Comparator<SchemeData>, Parcelable {
    public static final Parcelable.Creator<DrmInitData> CREATOR = new Parcelable.Creator<DrmInitData>() {
        public DrmInitData createFromParcel(Parcel parcel) {
            return new DrmInitData(parcel);
        }

        public DrmInitData[] newArray(int i) {
            return new DrmInitData[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final SchemeData[] f3226a;
    private int b;
    public final String c;
    public final int d;

    public static final class SchemeData implements Parcelable {
        public static final Parcelable.Creator<SchemeData> CREATOR = new Parcelable.Creator<SchemeData>() {
            public SchemeData createFromParcel(Parcel parcel) {
                return new SchemeData(parcel);
            }

            public SchemeData[] newArray(int i) {
                return new SchemeData[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private int f3227a;
        /* access modifiers changed from: private */
        public final UUID b;
        public final String c;
        public final String d;
        public final byte[] e;
        public final boolean f;

        public SchemeData(UUID uuid, String str, byte[] bArr) {
            this(uuid, str, bArr, false);
        }

        public boolean a(UUID uuid) {
            return C.f3151a.equals(this.b) || uuid.equals(this.b);
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof SchemeData)) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            SchemeData schemeData = (SchemeData) obj;
            if (!Util.a((Object) this.c, (Object) schemeData.c) || !Util.a((Object) this.d, (Object) schemeData.d) || !Util.a((Object) this.b, (Object) schemeData.b) || !Arrays.equals(this.e, schemeData.e)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            if (this.f3227a == 0) {
                int hashCode = this.b.hashCode() * 31;
                String str = this.c;
                this.f3227a = ((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.d.hashCode()) * 31) + Arrays.hashCode(this.e);
            }
            return this.f3227a;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeLong(this.b.getMostSignificantBits());
            parcel.writeLong(this.b.getLeastSignificantBits());
            parcel.writeString(this.c);
            parcel.writeString(this.d);
            parcel.writeByteArray(this.e);
            parcel.writeByte(this.f ? (byte) 1 : 0);
        }

        public SchemeData(UUID uuid, String str, byte[] bArr, boolean z) {
            this(uuid, (String) null, str, bArr, z);
        }

        public boolean a(SchemeData schemeData) {
            return a() && !schemeData.a() && a(schemeData.b);
        }

        public SchemeData(UUID uuid, String str, String str2, byte[] bArr, boolean z) {
            Assertions.a(uuid);
            this.b = uuid;
            this.c = str;
            Assertions.a(str2);
            this.d = str2;
            this.e = bArr;
            this.f = z;
        }

        public boolean a() {
            return this.e != null;
        }

        public SchemeData a(byte[] bArr) {
            return new SchemeData(this.b, this.c, this.d, bArr, this.f);
        }

        SchemeData(Parcel parcel) {
            this.b = new UUID(parcel.readLong(), parcel.readLong());
            this.c = parcel.readString();
            this.d = parcel.readString();
            this.e = parcel.createByteArray();
            this.f = parcel.readByte() != 0;
        }
    }

    public DrmInitData(List<SchemeData> list) {
        this((String) null, false, (SchemeData[]) list.toArray(new SchemeData[list.size()]));
    }

    public static DrmInitData a(DrmInitData drmInitData, DrmInitData drmInitData2) {
        String str;
        ArrayList arrayList = new ArrayList();
        if (drmInitData != null) {
            str = drmInitData.c;
            for (SchemeData schemeData : drmInitData.f3226a) {
                if (schemeData.a()) {
                    arrayList.add(schemeData);
                }
            }
        } else {
            str = null;
        }
        if (drmInitData2 != null) {
            if (str == null) {
                str = drmInitData2.c;
            }
            int size = arrayList.size();
            for (SchemeData schemeData2 : drmInitData2.f3226a) {
                if (schemeData2.a() && !a(arrayList, size, schemeData2.b)) {
                    arrayList.add(schemeData2);
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new DrmInitData(str, (List<SchemeData>) arrayList);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || DrmInitData.class != obj.getClass()) {
            return false;
        }
        DrmInitData drmInitData = (DrmInitData) obj;
        if (!Util.a((Object) this.c, (Object) drmInitData.c) || !Arrays.equals(this.f3226a, drmInitData.f3226a)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.b == 0) {
            String str = this.c;
            this.b = ((str == null ? 0 : str.hashCode()) * 31) + Arrays.hashCode(this.f3226a);
        }
        return this.b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.c);
        parcel.writeTypedArray(this.f3226a, 0);
    }

    public DrmInitData(String str, List<SchemeData> list) {
        this(str, false, (SchemeData[]) list.toArray(new SchemeData[list.size()]));
    }

    public DrmInitData(SchemeData... schemeDataArr) {
        this((String) null, schemeDataArr);
    }

    public DrmInitData(String str, SchemeData... schemeDataArr) {
        this(str, true, schemeDataArr);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: com.google.android.exoplayer2.drm.DrmInitData$SchemeData[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private DrmInitData(java.lang.String r1, boolean r2, com.google.android.exoplayer2.drm.DrmInitData.SchemeData... r3) {
        /*
            r0 = this;
            r0.<init>()
            r0.c = r1
            if (r2 == 0) goto L_0x000e
            java.lang.Object r1 = r3.clone()
            r3 = r1
            com.google.android.exoplayer2.drm.DrmInitData$SchemeData[] r3 = (com.google.android.exoplayer2.drm.DrmInitData.SchemeData[]) r3
        L_0x000e:
            java.util.Arrays.sort(r3, r0)
            r0.f3226a = r3
            int r1 = r3.length
            r0.d = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.drm.DrmInitData.<init>(java.lang.String, boolean, com.google.android.exoplayer2.drm.DrmInitData$SchemeData[]):void");
    }

    DrmInitData(Parcel parcel) {
        this.c = parcel.readString();
        this.f3226a = (SchemeData[]) parcel.createTypedArray(SchemeData.CREATOR);
        this.d = this.f3226a.length;
    }

    public SchemeData a(int i) {
        return this.f3226a[i];
    }

    public DrmInitData a(String str) {
        if (Util.a((Object) this.c, (Object) str)) {
            return this;
        }
        return new DrmInitData(str, false, this.f3226a);
    }

    /* renamed from: a */
    public int compare(SchemeData schemeData, SchemeData schemeData2) {
        if (C.f3151a.equals(schemeData.b)) {
            return C.f3151a.equals(schemeData2.b) ? 0 : 1;
        }
        return schemeData.b.compareTo(schemeData2.b);
    }

    private static boolean a(ArrayList<SchemeData> arrayList, int i, UUID uuid) {
        for (int i2 = 0; i2 < i; i2++) {
            if (arrayList.get(i2).b.equals(uuid)) {
                return true;
            }
        }
        return false;
    }
}
