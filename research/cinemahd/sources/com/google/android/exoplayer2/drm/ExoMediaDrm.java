package com.google.android.exoplayer2.drm;

import android.media.DeniedByServerException;
import android.media.MediaCryptoException;
import android.media.MediaDrmException;
import android.media.NotProvisionedException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ExoMediaDrm<T extends ExoMediaCrypto> {

    public static final class KeyRequest {

        /* renamed from: a  reason: collision with root package name */
        private final byte[] f3229a;
        private final String b;

        public KeyRequest(byte[] bArr, String str) {
            this.f3229a = bArr;
            this.b = str;
        }

        public byte[] a() {
            return this.f3229a;
        }

        public String b() {
            return this.b;
        }
    }

    public interface OnEventListener<T extends ExoMediaCrypto> {
        void a(ExoMediaDrm<? extends T> exoMediaDrm, byte[] bArr, int i, int i2, byte[] bArr2);
    }

    public static final class ProvisionRequest {

        /* renamed from: a  reason: collision with root package name */
        private final byte[] f3230a;
        private final String b;

        public ProvisionRequest(byte[] bArr, String str) {
            this.f3230a = bArr;
            this.b = str;
        }

        public byte[] a() {
            return this.f3230a;
        }

        public String b() {
            return this.b;
        }
    }

    KeyRequest a(byte[] bArr, List<DrmInitData.SchemeData> list, int i, HashMap<String, String> hashMap) throws NotProvisionedException;

    ProvisionRequest a();

    Map<String, String> a(byte[] bArr);

    void a(OnEventListener<? super T> onEventListener);

    void a(String str, String str2);

    void a(byte[] bArr, byte[] bArr2);

    T b(byte[] bArr) throws MediaCryptoException;

    byte[] b() throws MediaDrmException;

    byte[] b(byte[] bArr, byte[] bArr2) throws NotProvisionedException, DeniedByServerException;

    void c(byte[] bArr) throws DeniedByServerException;

    void d(byte[] bArr);
}
