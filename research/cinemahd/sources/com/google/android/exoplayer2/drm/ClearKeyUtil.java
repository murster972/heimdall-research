package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class ClearKeyUtil {
    private ClearKeyUtil() {
    }

    public static byte[] a(byte[] bArr) {
        if (Util.f3651a >= 27) {
            return bArr;
        }
        return Util.d(a(Util.a(bArr)));
    }

    public static byte[] b(byte[] bArr) {
        if (Util.f3651a >= 27) {
            return bArr;
        }
        try {
            JSONObject jSONObject = new JSONObject(Util.a(bArr));
            StringBuilder sb = new StringBuilder("{\"keys\":[");
            JSONArray jSONArray = jSONObject.getJSONArray("keys");
            for (int i = 0; i < jSONArray.length(); i++) {
                if (i != 0) {
                    sb.append(",");
                }
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                sb.append("{\"k\":\"");
                sb.append(b(jSONObject2.getString("k")));
                sb.append("\",\"kid\":\"");
                sb.append(b(jSONObject2.getString("kid")));
                sb.append("\",\"kty\":\"");
                sb.append(jSONObject2.getString("kty"));
                sb.append("\"}");
            }
            sb.append("]}");
            return Util.d(sb.toString());
        } catch (JSONException e) {
            Log.a("ClearKeyUtil", "Failed to adjust response data: " + Util.a(bArr), e);
            return bArr;
        }
    }

    private static String a(String str) {
        return str.replace('+', '-').replace('/', '_');
    }

    private static String b(String str) {
        return str.replace('-', '+').replace('_', '/');
    }
}
