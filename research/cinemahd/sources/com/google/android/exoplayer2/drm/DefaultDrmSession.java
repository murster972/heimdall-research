package com.google.android.exoplayer2.drm;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.media.NotProvisionedException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.drm.ExoMediaDrm;
import com.google.android.exoplayer2.util.EventDispatcher;
import com.google.android.exoplayer2.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@TargetApi(18)
class DefaultDrmSession<T extends ExoMediaCrypto> implements DrmSession<T> {

    /* renamed from: a  reason: collision with root package name */
    public final List<DrmInitData.SchemeData> f3220a;
    private final ExoMediaDrm<T> b;
    private final ProvisioningManager<T> c;
    private final int d;
    private final HashMap<String, String> e;
    private final EventDispatcher<DefaultDrmSessionEventListener> f;
    /* access modifiers changed from: private */
    public final int g;
    final MediaDrmCallback h;
    final UUID i;
    final DefaultDrmSession<T>.PostResponseHandler j;
    private int k;
    private int l;
    private HandlerThread m;
    private DefaultDrmSession<T>.PostRequestHandler n;
    private T o;
    private DrmSession.DrmSessionException p;
    private byte[] q;
    private byte[] r;
    private ExoMediaDrm.KeyRequest s;
    private ExoMediaDrm.ProvisionRequest t;

    @SuppressLint({"HandlerLeak"})
    private class PostRequestHandler extends Handler {
        public PostRequestHandler(Looper looper) {
            super(looper);
        }

        /* access modifiers changed from: package-private */
        public void a(int i, Object obj, boolean z) {
            obtainMessage(i, z ? 1 : 0, 0, obj).sendToTarget();
        }

        public void handleMessage(Message message) {
            Object obj = message.obj;
            try {
                int i = message.what;
                if (i == 0) {
                    e = DefaultDrmSession.this.h.a(DefaultDrmSession.this.i, (ExoMediaDrm.ProvisionRequest) obj);
                } else if (i == 1) {
                    e = DefaultDrmSession.this.h.a(DefaultDrmSession.this.i, (ExoMediaDrm.KeyRequest) obj);
                } else {
                    throw new RuntimeException();
                }
            } catch (Exception e) {
                e = e;
                if (a(message)) {
                    return;
                }
            }
            DefaultDrmSession.this.j.obtainMessage(message.what, Pair.create(obj, e)).sendToTarget();
        }

        private boolean a(Message message) {
            int i;
            if (!(message.arg1 == 1) || (i = message.arg2 + 1) > DefaultDrmSession.this.g) {
                return false;
            }
            Message obtain = Message.obtain(message);
            obtain.arg2 = i;
            sendMessageDelayed(obtain, a(i));
            return true;
        }

        private long a(int i) {
            return (long) Math.min((i - 1) * 1000, 5000);
        }
    }

    @SuppressLint({"HandlerLeak"})
    private class PostResponseHandler extends Handler {
        public PostResponseHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            Pair pair = (Pair) message.obj;
            Object obj = pair.first;
            Object obj2 = pair.second;
            int i = message.what;
            if (i == 0) {
                DefaultDrmSession.this.b(obj, obj2);
            } else if (i == 1) {
                DefaultDrmSession.this.a(obj, obj2);
            }
        }
    }

    public interface ProvisioningManager<T extends ExoMediaCrypto> {
        void a();

        void a(DefaultDrmSession<T> defaultDrmSession);

        void a(Exception exc);
    }

    public DefaultDrmSession(UUID uuid, ExoMediaDrm<T> exoMediaDrm, ProvisioningManager<T> provisioningManager, List<DrmInitData.SchemeData> list, int i2, byte[] bArr, HashMap<String, String> hashMap, MediaDrmCallback mediaDrmCallback, Looper looper, EventDispatcher<DefaultDrmSessionEventListener> eventDispatcher, int i3) {
        this.i = uuid;
        this.c = provisioningManager;
        this.b = exoMediaDrm;
        this.d = i2;
        this.r = bArr;
        this.f3220a = bArr == null ? Collections.unmodifiableList(list) : null;
        this.e = hashMap;
        this.h = mediaDrmCallback;
        this.g = i3;
        this.f = eventDispatcher;
        this.k = 2;
        this.j = new PostResponseHandler(looper);
        this.m = new HandlerThread("DrmRequestHandler");
        this.m.start();
        this.n = new PostRequestHandler(this.m.getLooper());
    }

    private long h() {
        if (!C.d.equals(this.i)) {
            return Clock.MAX_TIME;
        }
        Pair<Long, Long> a2 = WidevineUtil.a(this);
        return Math.min(((Long) a2.first).longValue(), ((Long) a2.second).longValue());
    }

    private boolean i() {
        int i2 = this.k;
        return i2 == 3 || i2 == 4;
    }

    private void j() {
        if (this.k == 4) {
            this.k = 3;
            b((Exception) new KeysExpiredException());
        }
    }

    private boolean k() {
        try {
            this.b.a(this.q, this.r);
            return true;
        } catch (Exception e2) {
            Log.a("DefaultDrmSession", "Error trying to restore Widevine keys.", e2);
            b(e2);
            return false;
        }
    }

    public Map<String, String> c() {
        byte[] bArr = this.q;
        if (bArr == null) {
            return null;
        }
        return this.b.a(bArr);
    }

    public void d() {
        int i2 = this.l + 1;
        this.l = i2;
        if (i2 == 1 && this.k != 1 && b(true)) {
            a(true);
        }
    }

    public void e() {
        if (b(false)) {
            a(true);
        }
    }

    public void f() {
        this.t = this.b.a();
        this.n.a(0, this.t, true);
    }

    public boolean g() {
        int i2 = this.l - 1;
        this.l = i2;
        if (i2 != 0) {
            return false;
        }
        this.k = 0;
        this.j.removeCallbacksAndMessages((Object) null);
        this.n.removeCallbacksAndMessages((Object) null);
        this.n = null;
        this.m.quit();
        this.m = null;
        this.o = null;
        this.p = null;
        this.s = null;
        this.t = null;
        byte[] bArr = this.q;
        if (bArr != null) {
            this.b.d(bArr);
            this.q = null;
            this.f.a(a.f3234a);
        }
        return true;
    }

    public final int getState() {
        return this.k;
    }

    private void c(Exception exc) {
        if (exc instanceof NotProvisionedException) {
            this.c.a(this);
        } else {
            b(exc);
        }
    }

    public final DrmSession.DrmSessionException b() {
        if (this.k == 1) {
            return this.p;
        }
        return null;
    }

    private boolean b(boolean z) {
        if (i()) {
            return true;
        }
        try {
            this.q = this.b.b();
            this.f.a(e.f3238a);
            this.o = this.b.b(this.q);
            this.k = 3;
            return true;
        } catch (NotProvisionedException e2) {
            if (z) {
                this.c.a(this);
                return false;
            }
            b((Exception) e2);
            return false;
        } catch (Exception e3) {
            b(e3);
            return false;
        }
    }

    public boolean a(byte[] bArr) {
        return Arrays.equals(this.q, bArr);
    }

    public void a(int i2) {
        if (i()) {
            if (i2 == 1) {
                this.k = 3;
                this.c.a(this);
            } else if (i2 == 2) {
                a(false);
            } else if (i2 == 3) {
                j();
            }
        }
    }

    public void a(Exception exc) {
        b(exc);
    }

    public final T a() {
        return this.o;
    }

    private void a(boolean z) {
        int i2 = this.d;
        if (i2 == 0 || i2 == 1) {
            if (this.r == null) {
                a(1, z);
            } else if (this.k == 4 || k()) {
                long h2 = h();
                if (this.d == 0 && h2 <= 60) {
                    Log.a("DefaultDrmSession", "Offline license has expired or will expire soon. Remaining seconds: " + h2);
                    a(2, z);
                } else if (h2 <= 0) {
                    b((Exception) new KeysExpiredException());
                } else {
                    this.k = 4;
                    this.f.a(f.f3239a);
                }
            }
        } else if (i2 != 2) {
            if (i2 == 3 && k()) {
                a(3, z);
            }
        } else if (this.r == null) {
            a(2, z);
        } else if (k()) {
            a(2, z);
        }
    }

    /* access modifiers changed from: private */
    public void b(Object obj, Object obj2) {
        if (obj != this.t) {
            return;
        }
        if (this.k == 2 || i()) {
            this.t = null;
            if (obj2 instanceof Exception) {
                this.c.a((Exception) obj2);
                return;
            }
            try {
                this.b.c((byte[]) obj2);
                this.c.a();
            } catch (Exception e2) {
                this.c.a(e2);
            }
        }
    }

    private void b(Exception exc) {
        this.p = new DrmSession.DrmSessionException(exc);
        this.f.a(new b(exc));
        if (this.k != 4) {
            this.k = 1;
        }
    }

    private void a(int i2, boolean z) {
        try {
            this.s = this.b.a(i2 == 3 ? this.r : this.q, this.f3220a, i2, this.e);
            this.n.a(1, this.s, z);
        } catch (Exception e2) {
            c(e2);
        }
    }

    /* access modifiers changed from: private */
    public void a(Object obj, Object obj2) {
        if (obj == this.s && i()) {
            this.s = null;
            if (obj2 instanceof Exception) {
                c((Exception) obj2);
                return;
            }
            try {
                byte[] bArr = (byte[]) obj2;
                if (this.d == 3) {
                    this.b.b(this.r, bArr);
                    this.f.a(f.f3239a);
                    return;
                }
                byte[] b2 = this.b.b(this.q, bArr);
                if (!((this.d != 2 && (this.d != 0 || this.r == null)) || b2 == null || b2.length == 0)) {
                    this.r = b2;
                }
                this.k = 4;
                this.f.a(g.f3240a);
            } catch (Exception e2) {
                c(e2);
            }
        }
    }
}
