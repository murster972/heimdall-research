package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.util.EventDispatcher;

/* compiled from: lambda */
public final /* synthetic */ class c implements EventDispatcher.Event {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ DefaultDrmSessionManager.MissingSchemeDataException f3236a;

    public /* synthetic */ c(DefaultDrmSessionManager.MissingSchemeDataException missingSchemeDataException) {
        this.f3236a = missingSchemeDataException;
    }

    public final void a(Object obj) {
        ((DefaultDrmSessionEventListener) obj).a(this.f3236a);
    }
}
