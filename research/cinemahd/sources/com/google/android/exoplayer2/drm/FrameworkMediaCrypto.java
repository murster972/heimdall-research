package com.google.android.exoplayer2.drm;

import android.annotation.TargetApi;
import android.media.MediaCrypto;
import com.google.android.exoplayer2.util.Assertions;

@TargetApi(16)
public final class FrameworkMediaCrypto implements ExoMediaCrypto {

    /* renamed from: a  reason: collision with root package name */
    private final MediaCrypto f3231a;
    private final boolean b;

    public FrameworkMediaCrypto(MediaCrypto mediaCrypto, boolean z) {
        Assertions.a(mediaCrypto);
        this.f3231a = mediaCrypto;
        this.b = z;
    }

    public MediaCrypto a() {
        return this.f3231a;
    }

    public boolean a(String str) {
        return !this.b && this.f3231a.requiresSecureDecoderComponent(str);
    }
}
