package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Map;

public final class ErrorStateDrmSession<T extends ExoMediaCrypto> implements DrmSession<T> {

    /* renamed from: a  reason: collision with root package name */
    private final DrmSession.DrmSessionException f3228a;

    public ErrorStateDrmSession(DrmSession.DrmSessionException drmSessionException) {
        Assertions.a(drmSessionException);
        this.f3228a = drmSessionException;
    }

    public T a() {
        return null;
    }

    public DrmSession.DrmSessionException b() {
        return this.f3228a;
    }

    public Map<String, String> c() {
        return null;
    }

    public int getState() {
        return 1;
    }
}
