package com.google.android.exoplayer2.drm;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DefaultDrmSession;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.drm.ExoMediaDrm;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.EventDispatcher;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@TargetApi(18)
public class DefaultDrmSessionManager<T extends ExoMediaCrypto> implements DrmSessionManager<T>, DefaultDrmSession.ProvisioningManager<T> {

    /* renamed from: a  reason: collision with root package name */
    private final UUID f3223a;
    private final ExoMediaDrm<T> b;
    private final MediaDrmCallback c;
    private final HashMap<String, String> d;
    private final EventDispatcher<DefaultDrmSessionEventListener> e;
    private final boolean f;
    private final int g;
    /* access modifiers changed from: private */
    public final List<DefaultDrmSession<T>> h;
    private final List<DefaultDrmSession<T>> i;
    private Looper j;
    /* access modifiers changed from: private */
    public int k;
    private byte[] l;
    volatile DefaultDrmSessionManager<T>.MediaDrmHandler m;

    private class MediaDrmEventListener implements ExoMediaDrm.OnEventListener<T> {
        private MediaDrmEventListener() {
        }

        public void a(ExoMediaDrm<? extends T> exoMediaDrm, byte[] bArr, int i, int i2, byte[] bArr2) {
            if (DefaultDrmSessionManager.this.k == 0) {
                DefaultDrmSessionManager.this.m.obtainMessage(i, bArr).sendToTarget();
            }
        }
    }

    @SuppressLint({"HandlerLeak"})
    private class MediaDrmHandler extends Handler {
        public MediaDrmHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            byte[] bArr = (byte[]) message.obj;
            for (DefaultDrmSession defaultDrmSession : DefaultDrmSessionManager.this.h) {
                if (defaultDrmSession.a(bArr)) {
                    defaultDrmSession.a(message.what);
                    return;
                }
            }
        }
    }

    public static final class MissingSchemeDataException extends Exception {
        private MissingSchemeDataException(UUID uuid) {
            super("Media does not support uuid: " + uuid);
        }
    }

    public DefaultDrmSessionManager(UUID uuid, ExoMediaDrm<T> exoMediaDrm, MediaDrmCallback mediaDrmCallback, HashMap<String, String> hashMap, boolean z) {
        this(uuid, exoMediaDrm, mediaDrmCallback, hashMap, z, 3);
    }

    public DefaultDrmSessionManager(UUID uuid, ExoMediaDrm<T> exoMediaDrm, MediaDrmCallback mediaDrmCallback, HashMap<String, String> hashMap, boolean z, int i2) {
        Assertions.a(uuid);
        Assertions.a(exoMediaDrm);
        Assertions.a(!C.b.equals(uuid), "Use C.CLEARKEY_UUID instead");
        this.f3223a = uuid;
        this.b = exoMediaDrm;
        this.c = mediaDrmCallback;
        this.d = hashMap;
        this.e = new EventDispatcher<>();
        this.f = z;
        this.g = i2;
        this.k = 0;
        this.h = new ArrayList();
        this.i = new ArrayList();
        if (z && C.d.equals(uuid) && Util.f3651a >= 19) {
            exoMediaDrm.a("sessionSharing", "enable");
        }
        exoMediaDrm.a((ExoMediaDrm.OnEventListener<? super T>) new MediaDrmEventListener());
    }

    public final void a(Handler handler, DefaultDrmSessionEventListener defaultDrmSessionEventListener) {
        this.e.a(handler, defaultDrmSessionEventListener);
    }

    public boolean a(DrmInitData drmInitData) {
        if (this.l != null) {
            return true;
        }
        if (a(drmInitData, this.f3223a, true).isEmpty()) {
            if (drmInitData.d != 1 || !drmInitData.a(0).a(C.b)) {
                return false;
            }
            Log.d("DefaultDrmSessionMgr", "DrmInitData only contains common PSSH SchemeData. Assuming support for: " + this.f3223a);
        }
        String str = drmInitData.c;
        if (str == null || "cenc".equals(str)) {
            return true;
        }
        if (("cbc1".equals(str) || "cbcs".equals(str) || "cens".equals(str)) && Util.f3651a < 25) {
            return false;
        }
        return true;
    }

    public DrmSession<T> a(Looper looper, DrmInitData drmInitData) {
        List<DrmInitData.SchemeData> list;
        DefaultDrmSession defaultDrmSession;
        Looper looper2 = this.j;
        Assertions.b(looper2 == null || looper2 == looper);
        if (this.h.isEmpty()) {
            this.j = looper;
            if (this.m == null) {
                this.m = new MediaDrmHandler(looper);
            }
        }
        DefaultDrmSession defaultDrmSession2 = null;
        if (this.l == null) {
            List<DrmInitData.SchemeData> a2 = a(drmInitData, this.f3223a, false);
            if (a2.isEmpty()) {
                MissingSchemeDataException missingSchemeDataException = new MissingSchemeDataException(this.f3223a);
                this.e.a(new c(missingSchemeDataException));
                return new ErrorStateDrmSession(new DrmSession.DrmSessionException(missingSchemeDataException));
            }
            list = a2;
        } else {
            list = null;
        }
        if (this.f) {
            Iterator<DefaultDrmSession<T>> it2 = this.h.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                DefaultDrmSession next = it2.next();
                if (Util.a((Object) next.f3220a, (Object) list)) {
                    defaultDrmSession2 = next;
                    break;
                }
            }
        } else if (!this.h.isEmpty()) {
            defaultDrmSession2 = this.h.get(0);
        }
        if (defaultDrmSession2 == null) {
            defaultDrmSession = new DefaultDrmSession(this.f3223a, this.b, this, list, this.k, this.l, this.d, this.c, looper, this.e, this.g);
            this.h.add(defaultDrmSession);
        } else {
            defaultDrmSession = defaultDrmSession2;
        }
        defaultDrmSession.d();
        return defaultDrmSession;
    }

    public void a(DrmSession<T> drmSession) {
        if (!(drmSession instanceof ErrorStateDrmSession)) {
            DefaultDrmSession<T> defaultDrmSession = (DefaultDrmSession) drmSession;
            if (defaultDrmSession.g()) {
                this.h.remove(defaultDrmSession);
                if (this.i.size() > 1 && this.i.get(0) == defaultDrmSession) {
                    this.i.get(1).f();
                }
                this.i.remove(defaultDrmSession);
            }
        }
    }

    public void a(DefaultDrmSession<T> defaultDrmSession) {
        this.i.add(defaultDrmSession);
        if (this.i.size() == 1) {
            defaultDrmSession.f();
        }
    }

    public void a() {
        for (DefaultDrmSession<T> e2 : this.i) {
            e2.e();
        }
        this.i.clear();
    }

    public void a(Exception exc) {
        for (DefaultDrmSession<T> a2 : this.i) {
            a2.a(exc);
        }
        this.i.clear();
    }

    private static List<DrmInitData.SchemeData> a(DrmInitData drmInitData, UUID uuid, boolean z) {
        ArrayList arrayList = new ArrayList(drmInitData.d);
        for (int i2 = 0; i2 < drmInitData.d; i2++) {
            DrmInitData.SchemeData a2 = drmInitData.a(i2);
            if ((a2.a(uuid) || (C.c.equals(uuid) && a2.a(C.b))) && (a2.e != null || z)) {
                arrayList.add(a2);
            }
        }
        return arrayList;
    }
}
