package com.google.android.exoplayer2;

import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.Allocator;

public interface LoadControl {
    void a();

    void a(Renderer[] rendererArr, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray);

    boolean a(long j, float f);

    boolean a(long j, float f, boolean z);

    boolean b();

    long c();

    Allocator d();

    void e();

    void onStopped();
}
