package com.google.android.exoplayer2.extractor;

public final class SeekPoint {
    public static final SeekPoint c = new SeekPoint(0, 0);

    /* renamed from: a  reason: collision with root package name */
    public final long f3257a;
    public final long b;

    public SeekPoint(long j, long j2) {
        this.f3257a = j;
        this.b = j2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SeekPoint.class != obj.getClass()) {
            return false;
        }
        SeekPoint seekPoint = (SeekPoint) obj;
        if (this.f3257a == seekPoint.f3257a && this.b == seekPoint.b) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((int) this.f3257a) * 31) + ((int) this.b);
    }

    public String toString() {
        return "[timeUs=" + this.f3257a + ", position=" + this.b + "]";
    }
}
