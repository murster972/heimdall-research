package com.google.android.exoplayer2.extractor.mp3;

import com.google.android.exoplayer2.extractor.MpegAudioHeader;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.SeekPoint;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;

final class VbriSeeker implements Mp3Extractor.Seeker {

    /* renamed from: a  reason: collision with root package name */
    private final long[] f3275a;
    private final long[] b;
    private final long c;
    private final long d;

    private VbriSeeker(long[] jArr, long[] jArr2, long j, long j2) {
        this.f3275a = jArr;
        this.b = jArr2;
        this.c = j;
        this.d = j2;
    }

    public static VbriSeeker a(long j, long j2, MpegAudioHeader mpegAudioHeader, ParsableByteArray parsableByteArray) {
        int i;
        long j3 = j;
        MpegAudioHeader mpegAudioHeader2 = mpegAudioHeader;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        parsableByteArray2.f(10);
        int h = parsableByteArray.h();
        if (h <= 0) {
            return null;
        }
        int i2 = mpegAudioHeader2.d;
        long c2 = Util.c((long) h, 1000000 * ((long) (i2 >= 32000 ? 1152 : 576)), (long) i2);
        int z = parsableByteArray.z();
        int z2 = parsableByteArray.z();
        int z3 = parsableByteArray.z();
        parsableByteArray2.f(2);
        long j4 = j2 + ((long) mpegAudioHeader2.c);
        long[] jArr = new long[z];
        long[] jArr2 = new long[z];
        int i3 = 0;
        long j5 = j2;
        while (i3 < z) {
            int i4 = z2;
            jArr[i3] = (((long) i3) * c2) / ((long) z);
            long j6 = j4;
            jArr2[i3] = Math.max(j5, j6);
            if (z3 == 1) {
                i = parsableByteArray.t();
            } else if (z3 == 2) {
                i = parsableByteArray.z();
            } else if (z3 == 3) {
                i = parsableByteArray.w();
            } else if (z3 != 4) {
                return null;
            } else {
                i = parsableByteArray.x();
            }
            j5 += (long) (i * i4);
            i3++;
            j4 = j6;
            z2 = i4;
        }
        if (!(j3 == -1 || j3 == j5)) {
            Log.d("VbriSeeker", "VBRI data size mismatch: " + j3 + ", " + j5);
        }
        return new VbriSeeker(jArr, jArr2, c2, j5);
    }

    public SeekMap.SeekPoints b(long j) {
        int b2 = Util.b(this.f3275a, j, true, true);
        SeekPoint seekPoint = new SeekPoint(this.f3275a[b2], this.b[b2]);
        if (seekPoint.f3257a < j) {
            long[] jArr = this.f3275a;
            if (b2 != jArr.length - 1) {
                int i = b2 + 1;
                return new SeekMap.SeekPoints(seekPoint, new SeekPoint(jArr[i], this.b[i]));
            }
        }
        return new SeekMap.SeekPoints(seekPoint);
    }

    public boolean b() {
        return true;
    }

    public long getDurationUs() {
        return this.c;
    }

    public long a(long j) {
        return this.f3275a[Util.b(this.b, j, true, true)];
    }

    public long a() {
        return this.d;
    }
}
