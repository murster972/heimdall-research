package com.google.android.exoplayer2.extractor.amr;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* compiled from: lambda */
public final /* synthetic */ class a implements ExtractorsFactory {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ a f3260a = new a();

    private /* synthetic */ a() {
    }

    public final Extractor[] a() {
        return AmrExtractor.a();
    }
}
