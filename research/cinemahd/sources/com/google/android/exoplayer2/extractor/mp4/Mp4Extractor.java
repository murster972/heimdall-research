package com.google.android.exoplayer2.extractor.mp4;

import android.support.v4.media.session.PlaybackStateCompat;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.GaplessInfoHolder;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.SeekPoint;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.mp4.Atom;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;

public final class Mp4Extractor implements Extractor, SeekMap {
    private static final int t = Util.c("qt  ");

    /* renamed from: a  reason: collision with root package name */
    private final int f3292a;
    private final ParsableByteArray b;
    private final ParsableByteArray c;
    private final ParsableByteArray d;
    private final ArrayDeque<Atom.ContainerAtom> e;
    private int f;
    private int g;
    private long h;
    private int i;
    private ParsableByteArray j;
    private int k;
    private int l;
    private int m;
    private ExtractorOutput n;
    private Mp4Track[] o;
    private long[][] p;
    private int q;
    private long r;
    private boolean s;

    private static final class Mp4Track {

        /* renamed from: a  reason: collision with root package name */
        public final Track f3293a;
        public final TrackSampleTable b;
        public final TrackOutput c;
        public int d;

        public Mp4Track(Track track, TrackSampleTable trackSampleTable, TrackOutput trackOutput) {
            this.f3293a = track;
            this.b = trackSampleTable;
            this.c = trackOutput;
        }
    }

    static {
        b bVar = b.f3301a;
    }

    public Mp4Extractor() {
        this(0);
    }

    private void c() {
        this.f = 0;
        this.i = 0;
    }

    static /* synthetic */ Extractor[] d() {
        return new Extractor[]{new Mp4Extractor()};
    }

    private void e(long j2) {
        for (Mp4Track mp4Track : this.o) {
            TrackSampleTable trackSampleTable = mp4Track.b;
            int a2 = trackSampleTable.a(j2);
            if (a2 == -1) {
                a2 = trackSampleTable.b(j2);
            }
            mp4Track.d = a2;
        }
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return Sniffer.b(extractorInput);
    }

    public SeekMap.SeekPoints b(long j2) {
        long j3;
        long j4;
        long j5;
        long j6;
        int b2;
        Mp4Track[] mp4TrackArr = this.o;
        if (mp4TrackArr.length == 0) {
            return new SeekMap.SeekPoints(SeekPoint.c);
        }
        int i2 = this.q;
        if (i2 != -1) {
            TrackSampleTable trackSampleTable = mp4TrackArr[i2].b;
            int a2 = a(trackSampleTable, j2);
            if (a2 == -1) {
                return new SeekMap.SeekPoints(SeekPoint.c);
            }
            long j7 = trackSampleTable.f[a2];
            j3 = trackSampleTable.c[a2];
            if (j7 >= j2 || a2 >= trackSampleTable.b - 1 || (b2 = trackSampleTable.b(j2)) == -1 || b2 == a2) {
                j6 = -1;
                j5 = -9223372036854775807L;
            } else {
                j5 = trackSampleTable.f[b2];
                j6 = trackSampleTable.c[b2];
            }
            j4 = j6;
            j2 = j7;
        } else {
            j3 = Clock.MAX_TIME;
            j4 = -1;
            j5 = -9223372036854775807L;
        }
        int i3 = 0;
        while (true) {
            Mp4Track[] mp4TrackArr2 = this.o;
            if (i3 >= mp4TrackArr2.length) {
                break;
            }
            if (i3 != this.q) {
                TrackSampleTable trackSampleTable2 = mp4TrackArr2[i3].b;
                long a3 = a(trackSampleTable2, j2, j3);
                if (j5 != -9223372036854775807L) {
                    j4 = a(trackSampleTable2, j5, j4);
                }
                j3 = a3;
            }
            i3++;
        }
        SeekPoint seekPoint = new SeekPoint(j2, j3);
        if (j5 == -9223372036854775807L) {
            return new SeekMap.SeekPoints(seekPoint);
        }
        return new SeekMap.SeekPoints(seekPoint, new SeekPoint(j5, j4));
    }

    public boolean b() {
        return true;
    }

    public long getDurationUs() {
        return this.r;
    }

    public void release() {
    }

    public Mp4Extractor(int i2) {
        this.f3292a = i2;
        this.d = new ParsableByteArray(16);
        this.e = new ArrayDeque<>();
        this.b = new ParsableByteArray(NalUnitUtil.f3637a);
        this.c = new ParsableByteArray(4);
        this.k = -1;
    }

    private void d(long j2) throws ParserException {
        while (!this.e.isEmpty() && this.e.peek().V0 == j2) {
            Atom.ContainerAtom pop = this.e.pop();
            if (pop.f3279a == Atom.C) {
                a(pop);
                this.e.clear();
                this.f = 2;
            } else if (!this.e.isEmpty()) {
                this.e.peek().a(pop);
            }
        }
        if (this.f != 2) {
            c();
        }
    }

    public void a(ExtractorOutput extractorOutput) {
        this.n = extractorOutput;
    }

    private int c(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        long position = extractorInput.getPosition();
        if (this.k == -1) {
            this.k = c(position);
            if (this.k == -1) {
                return -1;
            }
        }
        Mp4Track mp4Track = this.o[this.k];
        TrackOutput trackOutput = mp4Track.c;
        int i2 = mp4Track.d;
        TrackSampleTable trackSampleTable = mp4Track.b;
        long j2 = trackSampleTable.c[i2];
        int i3 = trackSampleTable.d[i2];
        long j3 = (j2 - position) + ((long) this.l);
        if (j3 < 0 || j3 >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
            positionHolder.f3254a = j2;
            return 1;
        }
        if (mp4Track.f3293a.g == 1) {
            j3 += 8;
            i3 -= 8;
        }
        extractorInput.c((int) j3);
        int i4 = mp4Track.f3293a.j;
        if (i4 == 0) {
            while (true) {
                int i5 = this.l;
                if (i5 >= i3) {
                    break;
                }
                int a2 = trackOutput.a(extractorInput, i3 - i5, false);
                this.l += a2;
                this.m -= a2;
            }
        } else {
            byte[] bArr = this.c.f3641a;
            bArr[0] = 0;
            bArr[1] = 0;
            bArr[2] = 0;
            int i6 = 4 - i4;
            while (this.l < i3) {
                int i7 = this.m;
                if (i7 == 0) {
                    extractorInput.readFully(this.c.f3641a, i6, i4);
                    this.c.e(0);
                    this.m = this.c.x();
                    this.b.e(0);
                    trackOutput.a(this.b, 4);
                    this.l += 4;
                    i3 += i6;
                } else {
                    int a3 = trackOutput.a(extractorInput, i7, false);
                    this.l += a3;
                    this.m -= a3;
                }
            }
        }
        TrackSampleTable trackSampleTable2 = mp4Track.b;
        trackOutput.a(trackSampleTable2.f[i2], trackSampleTable2.g[i2], i3, 0, (TrackOutput.CryptoData) null);
        mp4Track.d++;
        this.k = -1;
        this.l = 0;
        this.m = 0;
        return 0;
    }

    public void a(long j2, long j3) {
        this.e.clear();
        this.i = 0;
        this.k = -1;
        this.l = 0;
        this.m = 0;
        if (j2 == 0) {
            c();
        } else if (this.o != null) {
            e(j3);
        }
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        while (true) {
            int i2 = this.f;
            if (i2 != 0) {
                if (i2 != 1) {
                    if (i2 == 2) {
                        return c(extractorInput, positionHolder);
                    }
                    throw new IllegalStateException();
                } else if (b(extractorInput, positionHolder)) {
                    return 1;
                }
            } else if (!b(extractorInput)) {
                return -1;
            }
        }
    }

    private void a(Atom.ContainerAtom containerAtom) throws ParserException {
        Metadata metadata;
        Atom.ContainerAtom containerAtom2 = containerAtom;
        ArrayList arrayList = new ArrayList();
        GaplessInfoHolder gaplessInfoHolder = new GaplessInfoHolder();
        Atom.LeafAtom e2 = containerAtom2.e(Atom.A0);
        if (e2 != null) {
            metadata = AtomParsers.a(e2, this.s);
            if (metadata != null) {
                gaplessInfoHolder.a(metadata);
            }
        } else {
            metadata = null;
        }
        int i2 = 1;
        int i3 = 0;
        ArrayList<TrackSampleTable> a2 = a(containerAtom2, gaplessInfoHolder, (this.f3292a & 1) != 0);
        int size = a2.size();
        int i4 = -1;
        long j2 = -9223372036854775807L;
        while (i3 < size) {
            TrackSampleTable trackSampleTable = a2.get(i3);
            Track track = trackSampleTable.f3299a;
            Mp4Track mp4Track = new Mp4Track(track, trackSampleTable, this.n.a(i3, track.b));
            Format a3 = track.f.a(trackSampleTable.e + 30);
            if (track.b == i2) {
                if (gaplessInfoHolder.a()) {
                    a3 = a3.a(gaplessInfoHolder.f3251a, gaplessInfoHolder.b);
                }
                if (metadata != null) {
                    a3 = a3.a(metadata);
                }
            }
            mp4Track.c.a(a3);
            long j3 = track.e;
            if (j3 == -9223372036854775807L) {
                j3 = trackSampleTable.h;
            }
            j2 = Math.max(j2, j3);
            if (track.b == 2) {
                if (i4 == -1) {
                    i4 = arrayList.size();
                }
            }
            arrayList.add(mp4Track);
            i3++;
            i2 = 1;
        }
        this.q = i4;
        this.r = j2;
        this.o = (Mp4Track[]) arrayList.toArray(new Mp4Track[arrayList.size()]);
        this.p = a(this.o);
        this.n.a();
        this.n.a(this);
    }

    private boolean b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (this.i == 0) {
            if (!extractorInput.b(this.d.f3641a, 0, 8, true)) {
                return false;
            }
            this.i = 8;
            this.d.e(0);
            this.h = this.d.v();
            this.g = this.d.h();
        }
        long j2 = this.h;
        if (j2 == 1) {
            extractorInput.readFully(this.d.f3641a, 8, 8);
            this.i += 8;
            this.h = this.d.y();
        } else if (j2 == 0) {
            long length = extractorInput.getLength();
            if (length == -1 && !this.e.isEmpty()) {
                length = this.e.peek().V0;
            }
            if (length != -1) {
                this.h = (length - extractorInput.getPosition()) + ((long) this.i);
            }
        }
        if (this.h >= ((long) this.i)) {
            if (a(this.g)) {
                long position = (extractorInput.getPosition() + this.h) - ((long) this.i);
                this.e.push(new Atom.ContainerAtom(this.g, position));
                if (this.h == ((long) this.i)) {
                    d(position);
                } else {
                    c();
                }
            } else if (b(this.g)) {
                Assertions.b(this.i == 8);
                Assertions.b(this.h <= 2147483647L);
                this.j = new ParsableByteArray((int) this.h);
                System.arraycopy(this.d.f3641a, 0, this.j.f3641a, 0, 8);
                this.f = 1;
            } else {
                this.j = null;
                this.f = 1;
            }
            return true;
        }
        throw new ParserException("Atom size less than header length (unsupported).");
    }

    private int c(long j2) {
        int i2 = 0;
        long j3 = Clock.MAX_TIME;
        boolean z = true;
        long j4 = Clock.MAX_TIME;
        int i3 = -1;
        int i4 = -1;
        boolean z2 = true;
        long j5 = Clock.MAX_TIME;
        while (true) {
            Mp4Track[] mp4TrackArr = this.o;
            if (i2 >= mp4TrackArr.length) {
                break;
            }
            Mp4Track mp4Track = mp4TrackArr[i2];
            int i5 = mp4Track.d;
            TrackSampleTable trackSampleTable = mp4Track.b;
            if (i5 != trackSampleTable.b) {
                long j6 = trackSampleTable.c[i5];
                long j7 = this.p[i2][i5];
                long j8 = j6 - j2;
                boolean z3 = j8 < 0 || j8 >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE;
                if ((!z3 && z2) || (z3 == z2 && j8 < j5)) {
                    z2 = z3;
                    i4 = i2;
                    j5 = j8;
                    j4 = j7;
                }
                if (j7 < j3) {
                    z = z3;
                    i3 = i2;
                    j3 = j7;
                }
            }
            i2++;
        }
        return (j3 == Clock.MAX_TIME || !z || j4 < j3 + 10485760) ? i4 : i3;
    }

    private ArrayList<TrackSampleTable> a(Atom.ContainerAtom containerAtom, GaplessInfoHolder gaplessInfoHolder, boolean z) throws ParserException {
        Track a2;
        ArrayList<TrackSampleTable> arrayList = new ArrayList<>();
        for (int i2 = 0; i2 < containerAtom.X0.size(); i2++) {
            Atom.ContainerAtom containerAtom2 = containerAtom.X0.get(i2);
            if (containerAtom2.f3279a == Atom.E && (a2 = AtomParsers.a(containerAtom2, containerAtom.e(Atom.D), -9223372036854775807L, (DrmInitData) null, z, this.s)) != null) {
                TrackSampleTable a3 = AtomParsers.a(a2, containerAtom2.d(Atom.F).d(Atom.G).d(Atom.H), gaplessInfoHolder);
                if (a3.b != 0) {
                    arrayList.add(a3);
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(com.google.android.exoplayer2.extractor.ExtractorInput r10, com.google.android.exoplayer2.extractor.PositionHolder r11) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r9 = this;
            long r0 = r9.h
            int r2 = r9.i
            long r2 = (long) r2
            long r0 = r0 - r2
            long r2 = r10.getPosition()
            long r2 = r2 + r0
            com.google.android.exoplayer2.util.ParsableByteArray r4 = r9.j
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L_0x0045
            byte[] r11 = r4.f3641a
            int r4 = r9.i
            int r1 = (int) r0
            r10.readFully(r11, r4, r1)
            int r10 = r9.g
            int r11 = com.google.android.exoplayer2.extractor.mp4.Atom.b
            if (r10 != r11) goto L_0x0028
            com.google.android.exoplayer2.util.ParsableByteArray r10 = r9.j
            boolean r10 = a((com.google.android.exoplayer2.util.ParsableByteArray) r10)
            r9.s = r10
            goto L_0x0050
        L_0x0028:
            java.util.ArrayDeque<com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom> r10 = r9.e
            boolean r10 = r10.isEmpty()
            if (r10 != 0) goto L_0x0050
            java.util.ArrayDeque<com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom> r10 = r9.e
            java.lang.Object r10 = r10.peek()
            com.google.android.exoplayer2.extractor.mp4.Atom$ContainerAtom r10 = (com.google.android.exoplayer2.extractor.mp4.Atom.ContainerAtom) r10
            com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom r11 = new com.google.android.exoplayer2.extractor.mp4.Atom$LeafAtom
            int r0 = r9.g
            com.google.android.exoplayer2.util.ParsableByteArray r1 = r9.j
            r11.<init>(r0, r1)
            r10.a((com.google.android.exoplayer2.extractor.mp4.Atom.LeafAtom) r11)
            goto L_0x0050
        L_0x0045:
            r7 = 262144(0x40000, double:1.295163E-318)
            int r4 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r4 >= 0) goto L_0x0052
            int r11 = (int) r0
            r10.c(r11)
        L_0x0050:
            r10 = 0
            goto L_0x005a
        L_0x0052:
            long r7 = r10.getPosition()
            long r7 = r7 + r0
            r11.f3254a = r7
            r10 = 1
        L_0x005a:
            r9.d(r2)
            if (r10 == 0) goto L_0x0065
            int r10 = r9.f
            r11 = 2
            if (r10 == r11) goto L_0x0065
            goto L_0x0066
        L_0x0065:
            r5 = 0
        L_0x0066:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp4.Mp4Extractor.b(com.google.android.exoplayer2.extractor.ExtractorInput, com.google.android.exoplayer2.extractor.PositionHolder):boolean");
    }

    private static long[][] a(Mp4Track[] mp4TrackArr) {
        long[][] jArr = new long[mp4TrackArr.length][];
        int[] iArr = new int[mp4TrackArr.length];
        long[] jArr2 = new long[mp4TrackArr.length];
        boolean[] zArr = new boolean[mp4TrackArr.length];
        for (int i2 = 0; i2 < mp4TrackArr.length; i2++) {
            jArr[i2] = new long[mp4TrackArr[i2].b.b];
            jArr2[i2] = mp4TrackArr[i2].b.f[0];
        }
        long j2 = 0;
        int i3 = 0;
        while (i3 < mp4TrackArr.length) {
            int i4 = -1;
            long j3 = Long.MAX_VALUE;
            for (int i5 = 0; i5 < mp4TrackArr.length; i5++) {
                if (!zArr[i5] && jArr2[i5] <= j3) {
                    j3 = jArr2[i5];
                    i4 = i5;
                }
            }
            int i6 = iArr[i4];
            jArr[i4][i6] = j2;
            j2 += (long) mp4TrackArr[i4].b.d[i6];
            int i7 = i6 + 1;
            iArr[i4] = i7;
            if (i7 < jArr[i4].length) {
                jArr2[i4] = mp4TrackArr[i4].b.f[i7];
            } else {
                zArr[i4] = true;
                i3++;
            }
        }
        return jArr;
    }

    private static boolean b(int i2) {
        return i2 == Atom.S || i2 == Atom.D || i2 == Atom.T || i2 == Atom.U || i2 == Atom.m0 || i2 == Atom.n0 || i2 == Atom.o0 || i2 == Atom.R || i2 == Atom.p0 || i2 == Atom.q0 || i2 == Atom.r0 || i2 == Atom.s0 || i2 == Atom.t0 || i2 == Atom.P || i2 == Atom.b || i2 == Atom.A0;
    }

    private static long a(TrackSampleTable trackSampleTable, long j2, long j3) {
        int a2 = a(trackSampleTable, j2);
        if (a2 == -1) {
            return j3;
        }
        return Math.min(trackSampleTable.c[a2], j3);
    }

    private static int a(TrackSampleTable trackSampleTable, long j2) {
        int a2 = trackSampleTable.a(j2);
        return a2 == -1 ? trackSampleTable.b(j2) : a2;
    }

    private static boolean a(ParsableByteArray parsableByteArray) {
        parsableByteArray.e(8);
        if (parsableByteArray.h() == t) {
            return true;
        }
        parsableByteArray.f(4);
        while (parsableByteArray.a() > 0) {
            if (parsableByteArray.h() == t) {
                return true;
            }
        }
        return false;
    }

    private static boolean a(int i2) {
        return i2 == Atom.C || i2 == Atom.E || i2 == Atom.F || i2 == Atom.G || i2 == Atom.H || i2 == Atom.Q;
    }
}
