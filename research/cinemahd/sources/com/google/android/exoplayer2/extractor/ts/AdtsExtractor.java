package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.ConstantBitrateSeekMap;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public final class AdtsExtractor implements Extractor {
    private static final int n = Util.c("ID3");

    /* renamed from: a  reason: collision with root package name */
    private final int f3318a;
    private final AdtsReader b;
    private final ParsableByteArray c;
    private final ParsableByteArray d;
    private final ParsableBitArray e;
    private final long f;
    private ExtractorOutput g;
    private long h;
    private long i;
    private int j;
    private boolean k;
    private boolean l;
    private boolean m;

    static {
        b bVar = b.f3352a;
    }

    public AdtsExtractor() {
        this(0);
    }

    static /* synthetic */ Extractor[] a() {
        return new Extractor[]{new AdtsExtractor()};
    }

    private void b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (!this.k) {
            this.j = -1;
            extractorInput.a();
            long j2 = 0;
            if (extractorInput.getPosition() == 0) {
                c(extractorInput);
            }
            int i2 = 0;
            while (true) {
                if (!extractorInput.a(this.d.f3641a, 0, 2, true)) {
                    break;
                }
                this.d.e(0);
                if (!AdtsReader.a(this.d.z())) {
                    i2 = 0;
                    break;
                } else if (!extractorInput.a(this.d.f3641a, 0, 4, true)) {
                    break;
                } else {
                    this.e.b(14);
                    int a2 = this.e.a(13);
                    if (a2 > 6) {
                        j2 += (long) a2;
                        i2++;
                        if (i2 != 1000) {
                            if (!extractorInput.a(a2 - 6, true)) {
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        this.k = true;
                        throw new ParserException("Malformed ADTS stream");
                    }
                }
            }
            extractorInput.a();
            if (i2 > 0) {
                this.j = (int) (j2 / ((long) i2));
            } else {
                this.j = -1;
            }
            this.k = true;
        }
    }

    private int c(ExtractorInput extractorInput) throws IOException, InterruptedException {
        int i2 = 0;
        while (true) {
            extractorInput.a(this.d.f3641a, 0, 10);
            this.d.e(0);
            if (this.d.w() != n) {
                break;
            }
            this.d.f(3);
            int s = this.d.s();
            i2 += s + 10;
            extractorInput.a(s);
        }
        extractorInput.a();
        extractorInput.a(i2);
        if (this.i == -1) {
            this.i = (long) i2;
        }
        return i2;
    }

    public void release() {
    }

    public AdtsExtractor(long j2) {
        this(j2, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0021, code lost:
        r9.a();
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002a, code lost:
        if ((r3 - r0) < 8192) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x002c, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.google.android.exoplayer2.extractor.ExtractorInput r9) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r8 = this;
            int r0 = r8.c(r9)
            r1 = 0
            r3 = r0
        L_0x0006:
            r2 = 0
            r4 = 0
        L_0x0008:
            com.google.android.exoplayer2.util.ParsableByteArray r5 = r8.d
            byte[] r5 = r5.f3641a
            r6 = 2
            r9.a(r5, r1, r6)
            com.google.android.exoplayer2.util.ParsableByteArray r5 = r8.d
            r5.e(r1)
            com.google.android.exoplayer2.util.ParsableByteArray r5 = r8.d
            int r5 = r5.z()
            boolean r5 = com.google.android.exoplayer2.extractor.ts.AdtsReader.a((int) r5)
            if (r5 != 0) goto L_0x0031
            r9.a()
            int r3 = r3 + 1
            int r2 = r3 - r0
            r4 = 8192(0x2000, float:1.14794E-41)
            if (r2 < r4) goto L_0x002d
            return r1
        L_0x002d:
            r9.a(r3)
            goto L_0x0006
        L_0x0031:
            r5 = 1
            int r2 = r2 + r5
            r6 = 4
            if (r2 < r6) goto L_0x003b
            r7 = 188(0xbc, float:2.63E-43)
            if (r4 <= r7) goto L_0x003b
            return r5
        L_0x003b:
            com.google.android.exoplayer2.util.ParsableByteArray r5 = r8.d
            byte[] r5 = r5.f3641a
            r9.a(r5, r1, r6)
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r8.e
            r6 = 14
            r5.b(r6)
            com.google.android.exoplayer2.util.ParsableBitArray r5 = r8.e
            r6 = 13
            int r5 = r5.a((int) r6)
            r6 = 6
            if (r5 > r6) goto L_0x0055
            return r1
        L_0x0055:
            int r6 = r5 + -6
            r9.a(r6)
            int r4 = r4 + r5
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.ts.AdtsExtractor.a(com.google.android.exoplayer2.extractor.ExtractorInput):boolean");
    }

    public AdtsExtractor(long j2, int i2) {
        this.f = j2;
        this.h = j2;
        this.f3318a = i2;
        this.b = new AdtsReader(true);
        this.c = new ParsableByteArray(2048);
        this.j = -1;
        this.i = -1;
        this.d = new ParsableByteArray(10);
        this.e = new ParsableBitArray(this.d.f3641a);
    }

    public void a(ExtractorOutput extractorOutput) {
        this.g = extractorOutput;
        this.b.a(extractorOutput, new TsPayloadReader.TrackIdGenerator(0, 1));
        extractorOutput.a();
    }

    public void a(long j2, long j3) {
        this.l = false;
        this.b.a();
        this.h = this.f + j3;
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        long length = extractorInput.getLength();
        boolean z = ((this.f3318a & 1) == 0 || length == -1) ? false : true;
        if (z) {
            b(extractorInput);
        }
        int read = extractorInput.read(this.c.f3641a, 0, 2048);
        boolean z2 = read == -1;
        a(length, z, z2);
        if (z2) {
            return -1;
        }
        this.c.e(0);
        this.c.d(read);
        if (!this.l) {
            this.b.a(this.h, true);
            this.l = true;
        }
        this.b.a(this.c);
        return 0;
    }

    private void a(long j2, boolean z, boolean z2) {
        if (!this.m) {
            boolean z3 = z && this.j > 0;
            if (!z3 || this.b.c() != -9223372036854775807L || z2) {
                ExtractorOutput extractorOutput = this.g;
                Assertions.a(extractorOutput);
                ExtractorOutput extractorOutput2 = extractorOutput;
                if (!z3 || this.b.c() == -9223372036854775807L) {
                    extractorOutput2.a(new SeekMap.Unseekable(-9223372036854775807L));
                } else {
                    extractorOutput2.a(a(j2));
                }
                this.m = true;
            }
        }
    }

    private SeekMap a(long j2) {
        return new ConstantBitrateSeekMap(j2, this.i, a(this.j, this.b.c()), this.j);
    }

    private static int a(int i2, long j2) {
        return (int) ((((long) (i2 * 8)) * 1000000) / j2);
    }
}
