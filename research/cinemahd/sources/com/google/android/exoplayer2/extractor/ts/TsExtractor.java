package com.google.android.exoplayer2.extractor.ts;

import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.BinarySearchSeeker;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class TsExtractor implements Extractor {
    /* access modifiers changed from: private */
    public static final long s = ((long) Util.c("AC-3"));
    /* access modifiers changed from: private */
    public static final long t = ((long) Util.c("EAC3"));
    /* access modifiers changed from: private */
    public static final long u = ((long) Util.c("HEVC"));
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final int f3344a;
    /* access modifiers changed from: private */
    public final List<TimestampAdjuster> b;
    private final ParsableByteArray c;
    private final SparseIntArray d;
    /* access modifiers changed from: private */
    public final TsPayloadReader.Factory e;
    /* access modifiers changed from: private */
    public final SparseArray<TsPayloadReader> f;
    /* access modifiers changed from: private */
    public final SparseBooleanArray g;
    /* access modifiers changed from: private */
    public final SparseBooleanArray h;
    private final TsDurationReader i;
    private TsBinarySearchSeeker j;
    /* access modifiers changed from: private */
    public ExtractorOutput k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public boolean m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public TsPayloadReader p;
    private int q;
    /* access modifiers changed from: private */
    public int r;

    private class PatReader implements SectionPayloadReader {

        /* renamed from: a  reason: collision with root package name */
        private final ParsableBitArray f3345a = new ParsableBitArray(new byte[4]);

        public PatReader() {
        }

        public void a(ParsableByteArray parsableByteArray) {
            if (parsableByteArray.t() == 0) {
                parsableByteArray.f(7);
                int a2 = parsableByteArray.a() / 4;
                for (int i = 0; i < a2; i++) {
                    parsableByteArray.a(this.f3345a, 4);
                    int a3 = this.f3345a.a(16);
                    this.f3345a.c(3);
                    if (a3 == 0) {
                        this.f3345a.c(13);
                    } else {
                        int a4 = this.f3345a.a(13);
                        TsExtractor.this.f.put(a4, new SectionReader(new PmtReader(a4)));
                        TsExtractor.d(TsExtractor.this);
                    }
                }
                if (TsExtractor.this.f3344a != 2) {
                    TsExtractor.this.f.remove(0);
                }
            }
        }

        public void a(TimestampAdjuster timestampAdjuster, ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        }
    }

    static {
        d dVar = d.f3354a;
    }

    public TsExtractor() {
        this(0);
    }

    static /* synthetic */ int d(TsExtractor tsExtractor) {
        int i2 = tsExtractor.l;
        tsExtractor.l = i2 + 1;
        return i2;
    }

    public void release() {
    }

    public TsExtractor(int i2) {
        this(1, i2);
    }

    private int d() throws ParserException {
        int c2 = this.c.c();
        int d2 = this.c.d();
        int a2 = TsUtil.a(this.c.f3641a, c2, d2);
        this.c.e(a2);
        int i2 = a2 + 188;
        if (i2 > d2) {
            this.q += a2 - c2;
            if (this.f3344a == 2 && this.q > 376) {
                throw new ParserException("Cannot find sync byte. Most likely not a Transport Stream.");
            }
        } else {
            this.q = 0;
        }
        return i2;
    }

    static /* synthetic */ Extractor[] e() {
        return new Extractor[]{new TsExtractor()};
    }

    private void f() {
        this.g.clear();
        this.f.clear();
        SparseArray<TsPayloadReader> a2 = this.e.a();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f.put(a2.keyAt(i2), a2.valueAt(i2));
        }
        this.f.put(0, new SectionReader(new PatReader()));
        this.p = null;
    }

    public TsExtractor(int i2, int i3) {
        this(i2, new TimestampAdjuster(0), new DefaultTsPayloadReaderFactory(i3));
    }

    public TsExtractor(int i2, TimestampAdjuster timestampAdjuster, TsPayloadReader.Factory factory) {
        Assertions.a(factory);
        this.e = factory;
        this.f3344a = i2;
        if (i2 == 1 || i2 == 2) {
            this.b = Collections.singletonList(timestampAdjuster);
        } else {
            this.b = new ArrayList();
            this.b.add(timestampAdjuster);
        }
        this.c = new ParsableByteArray(new byte[9400], 0);
        this.g = new SparseBooleanArray();
        this.h = new SparseBooleanArray();
        this.f = new SparseArray<>();
        this.d = new SparseIntArray();
        this.i = new TsDurationReader();
        this.r = -1;
        f();
    }

    private boolean b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        ParsableByteArray parsableByteArray = this.c;
        byte[] bArr = parsableByteArray.f3641a;
        if (9400 - parsableByteArray.c() < 188) {
            int a2 = this.c.a();
            if (a2 > 0) {
                System.arraycopy(bArr, this.c.c(), bArr, 0, a2);
            }
            this.c.a(bArr, a2);
        }
        while (this.c.a() < 188) {
            int d2 = this.c.d();
            int read = extractorInput.read(bArr, d2, 9400 - d2);
            if (read == -1) {
                return false;
            }
            this.c.d(d2 + read);
        }
        return true;
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        boolean z;
        byte[] bArr = this.c.f3641a;
        extractorInput.a(bArr, 0, 940);
        for (int i2 = 0; i2 < 188; i2++) {
            int i3 = 0;
            while (true) {
                if (i3 >= 5) {
                    z = true;
                    break;
                } else if (bArr[(i3 * 188) + i2] != 71) {
                    z = false;
                    break;
                } else {
                    i3++;
                }
            }
            if (z) {
                extractorInput.c(i2);
                return true;
            }
        }
        return false;
    }

    public void a(ExtractorOutput extractorOutput) {
        this.k = extractorOutput;
    }

    public void a(long j2, long j3) {
        TsBinarySearchSeeker tsBinarySearchSeeker;
        Assertions.b(this.f3344a != 2);
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            TimestampAdjuster timestampAdjuster = this.b.get(i2);
            if ((timestampAdjuster.c() == -9223372036854775807L) || !(timestampAdjuster.c() == 0 || timestampAdjuster.a() == j3)) {
                timestampAdjuster.d();
                timestampAdjuster.c(j3);
            }
        }
        if (!(j3 == 0 || (tsBinarySearchSeeker = this.j) == null)) {
            tsBinarySearchSeeker.b(j3);
        }
        this.c.B();
        this.d.clear();
        for (int i3 = 0; i3 < this.f.size(); i3++) {
            this.f.valueAt(i3).a();
        }
        this.q = 0;
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        ExtractorInput extractorInput2 = extractorInput;
        PositionHolder positionHolder2 = positionHolder;
        long length = extractorInput.getLength();
        TsPayloadReader tsPayloadReader = null;
        if (this.m) {
            if (((length == -1 || this.f3344a == 2) ? false : true) && !this.i.c()) {
                return this.i.a(extractorInput2, positionHolder2, this.r);
            }
            a(length);
            if (this.o) {
                this.o = false;
                a(0, 0);
                if (extractorInput.getPosition() != 0) {
                    positionHolder2.f3254a = 0;
                    return 1;
                }
            }
            TsBinarySearchSeeker tsBinarySearchSeeker = this.j;
            if (tsBinarySearchSeeker != null && tsBinarySearchSeeker.b()) {
                return this.j.a(extractorInput2, positionHolder2, (BinarySearchSeeker.OutputFrameHolder) null);
            }
        }
        if (!b(extractorInput)) {
            return -1;
        }
        int d2 = d();
        int d3 = this.c.d();
        if (d2 > d3) {
            return 0;
        }
        int h2 = this.c.h();
        if ((8388608 & h2) != 0) {
            this.c.e(d2);
            return 0;
        }
        boolean z = (4194304 & h2) != 0;
        int i2 = (2096896 & h2) >> 8;
        boolean z2 = (h2 & 32) != 0;
        if ((h2 & 16) != 0) {
            tsPayloadReader = this.f.get(i2);
        }
        if (tsPayloadReader == null) {
            this.c.e(d2);
            return 0;
        }
        if (this.f3344a != 2) {
            int i3 = h2 & 15;
            int i4 = this.d.get(i2, i3 - 1);
            this.d.put(i2, i3);
            if (i4 == i3) {
                this.c.e(d2);
                return 0;
            } else if (i3 != ((i4 + 1) & 15)) {
                tsPayloadReader.a();
            }
        }
        if (z2) {
            this.c.f(this.c.t());
        }
        boolean z3 = this.m;
        if (a(i2)) {
            this.c.d(d2);
            tsPayloadReader.a(this.c, z);
            this.c.d(d3);
        }
        if (this.f3344a != 2 && !z3 && this.m && length != -1) {
            this.o = true;
        }
        this.c.e(d2);
        return 0;
    }

    private class PmtReader implements SectionPayloadReader {

        /* renamed from: a  reason: collision with root package name */
        private final ParsableBitArray f3346a = new ParsableBitArray(new byte[5]);
        private final SparseArray<TsPayloadReader> b = new SparseArray<>();
        private final SparseIntArray c = new SparseIntArray();
        private final int d;

        public PmtReader(int i) {
            this.d = i;
        }

        public void a(ParsableByteArray parsableByteArray) {
            TimestampAdjuster timestampAdjuster;
            TsPayloadReader tsPayloadReader;
            ParsableByteArray parsableByteArray2 = parsableByteArray;
            if (parsableByteArray.t() == 2) {
                int i = 0;
                if (TsExtractor.this.f3344a == 1 || TsExtractor.this.f3344a == 2 || TsExtractor.this.l == 1) {
                    timestampAdjuster = (TimestampAdjuster) TsExtractor.this.b.get(0);
                } else {
                    timestampAdjuster = new TimestampAdjuster(((TimestampAdjuster) TsExtractor.this.b.get(0)).a());
                    TsExtractor.this.b.add(timestampAdjuster);
                }
                parsableByteArray2.f(2);
                int z = parsableByteArray.z();
                int i2 = 3;
                parsableByteArray2.f(3);
                parsableByteArray2.a(this.f3346a, 2);
                this.f3346a.c(3);
                int i3 = 13;
                int unused = TsExtractor.this.r = this.f3346a.a(13);
                parsableByteArray2.a(this.f3346a, 2);
                int i4 = 4;
                this.f3346a.c(4);
                parsableByteArray2.f(this.f3346a.a(12));
                if (TsExtractor.this.f3344a == 2 && TsExtractor.this.p == null) {
                    TsPayloadReader.EsInfo esInfo = new TsPayloadReader.EsInfo(21, (String) null, (List<TsPayloadReader.DvbSubtitleInfo>) null, Util.f);
                    TsExtractor tsExtractor = TsExtractor.this;
                    TsPayloadReader unused2 = tsExtractor.p = tsExtractor.e.a(21, esInfo);
                    TsExtractor.this.p.a(timestampAdjuster, TsExtractor.this.k, new TsPayloadReader.TrackIdGenerator(z, 21, 8192));
                }
                this.b.clear();
                this.c.clear();
                int a2 = parsableByteArray.a();
                while (a2 > 0) {
                    parsableByteArray2.a(this.f3346a, 5);
                    int a3 = this.f3346a.a(8);
                    this.f3346a.c(i2);
                    int a4 = this.f3346a.a(i3);
                    this.f3346a.c(i4);
                    int a5 = this.f3346a.a(12);
                    TsPayloadReader.EsInfo a6 = a(parsableByteArray2, a5);
                    if (a3 == 6) {
                        a3 = a6.f3348a;
                    }
                    a2 -= a5 + 5;
                    int i5 = TsExtractor.this.f3344a == 2 ? a3 : a4;
                    if (!TsExtractor.this.g.get(i5)) {
                        if (TsExtractor.this.f3344a == 2 && a3 == 21) {
                            tsPayloadReader = TsExtractor.this.p;
                        } else {
                            tsPayloadReader = TsExtractor.this.e.a(a3, a6);
                        }
                        if (TsExtractor.this.f3344a != 2 || a4 < this.c.get(i5, 8192)) {
                            this.c.put(i5, a4);
                            this.b.put(i5, tsPayloadReader);
                        }
                    }
                    i2 = 3;
                    i4 = 4;
                    i3 = 13;
                }
                int size = this.c.size();
                for (int i6 = 0; i6 < size; i6++) {
                    int keyAt = this.c.keyAt(i6);
                    int valueAt = this.c.valueAt(i6);
                    TsExtractor.this.g.put(keyAt, true);
                    TsExtractor.this.h.put(valueAt, true);
                    TsPayloadReader valueAt2 = this.b.valueAt(i6);
                    if (valueAt2 != null) {
                        if (valueAt2 != TsExtractor.this.p) {
                            valueAt2.a(timestampAdjuster, TsExtractor.this.k, new TsPayloadReader.TrackIdGenerator(z, keyAt, 8192));
                        }
                        TsExtractor.this.f.put(valueAt, valueAt2);
                    }
                }
                if (TsExtractor.this.f3344a != 2) {
                    TsExtractor.this.f.remove(this.d);
                    TsExtractor tsExtractor2 = TsExtractor.this;
                    if (tsExtractor2.f3344a != 1) {
                        i = TsExtractor.this.l - 1;
                    }
                    int unused3 = tsExtractor2.l = i;
                    if (TsExtractor.this.l == 0) {
                        TsExtractor.this.k.a();
                        boolean unused4 = TsExtractor.this.m = true;
                    }
                } else if (!TsExtractor.this.m) {
                    TsExtractor.this.k.a();
                    int unused5 = TsExtractor.this.l = 0;
                    boolean unused6 = TsExtractor.this.m = true;
                }
            }
        }

        public void a(TimestampAdjuster timestampAdjuster, ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        }

        private TsPayloadReader.EsInfo a(ParsableByteArray parsableByteArray, int i) {
            int c2 = parsableByteArray.c();
            int i2 = i + c2;
            String str = null;
            int i3 = -1;
            ArrayList arrayList = null;
            while (parsableByteArray.c() < i2) {
                int t = parsableByteArray.t();
                int c3 = parsableByteArray.c() + parsableByteArray.t();
                if (t == 5) {
                    long v = parsableByteArray.v();
                    if (v != TsExtractor.s) {
                        if (v != TsExtractor.t) {
                            if (v == TsExtractor.u) {
                                i3 = 36;
                            }
                            parsableByteArray.f(c3 - parsableByteArray.c());
                        }
                        i3 = 135;
                        parsableByteArray.f(c3 - parsableByteArray.c());
                    }
                } else if (t != 106) {
                    if (t != 122) {
                        if (t == 123) {
                            i3 = 138;
                        } else if (t == 10) {
                            str = parsableByteArray.b(3).trim();
                        } else if (t == 89) {
                            ArrayList arrayList2 = new ArrayList();
                            while (parsableByteArray.c() < c3) {
                                String trim = parsableByteArray.b(3).trim();
                                int t2 = parsableByteArray.t();
                                byte[] bArr = new byte[4];
                                parsableByteArray.a(bArr, 0, 4);
                                arrayList2.add(new TsPayloadReader.DvbSubtitleInfo(trim, t2, bArr));
                            }
                            arrayList = arrayList2;
                            i3 = 89;
                        }
                        parsableByteArray.f(c3 - parsableByteArray.c());
                    }
                    i3 = 135;
                    parsableByteArray.f(c3 - parsableByteArray.c());
                }
                i3 = 129;
                parsableByteArray.f(c3 - parsableByteArray.c());
            }
            parsableByteArray.e(i2);
            return new TsPayloadReader.EsInfo(i3, str, arrayList, Arrays.copyOfRange(parsableByteArray.f3641a, c2, i2));
        }
    }

    private void a(long j2) {
        if (!this.n) {
            this.n = true;
            if (this.i.a() != -9223372036854775807L) {
                this.j = new TsBinarySearchSeeker(this.i.b(), this.i.a(), j2, this.r);
                this.k.a(this.j.a());
                return;
            }
            this.k.a(new SeekMap.Unseekable(this.i.a()));
        }
    }

    private boolean a(int i2) {
        if (this.f3344a == 2 || this.m || !this.h.get(i2, false)) {
            return true;
        }
        return false;
    }
}
