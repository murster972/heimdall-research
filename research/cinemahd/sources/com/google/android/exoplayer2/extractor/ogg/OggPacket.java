package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;
import java.util.Arrays;

final class OggPacket {

    /* renamed from: a  reason: collision with root package name */
    private final OggPageHeader f3306a = new OggPageHeader();
    private final ParsableByteArray b = new ParsableByteArray(new byte[65025], 0);
    private int c = -1;
    private int d;
    private boolean e;

    OggPacket() {
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        int i;
        Assertions.b(extractorInput != null);
        if (this.e) {
            this.e = false;
            this.b.B();
        }
        while (!this.e) {
            if (this.c < 0) {
                if (!this.f3306a.a(extractorInput, true)) {
                    return false;
                }
                OggPageHeader oggPageHeader = this.f3306a;
                int i2 = oggPageHeader.e;
                if ((oggPageHeader.b & 1) == 1 && this.b.d() == 0) {
                    i2 += a(0);
                    i = this.d + 0;
                } else {
                    i = 0;
                }
                extractorInput.c(i2);
                this.c = i;
            }
            int a2 = a(this.c);
            int i3 = this.c + this.d;
            if (a2 > 0) {
                if (this.b.b() < this.b.d() + a2) {
                    ParsableByteArray parsableByteArray = this.b;
                    parsableByteArray.f3641a = Arrays.copyOf(parsableByteArray.f3641a, parsableByteArray.d() + a2);
                }
                ParsableByteArray parsableByteArray2 = this.b;
                extractorInput.readFully(parsableByteArray2.f3641a, parsableByteArray2.d(), a2);
                ParsableByteArray parsableByteArray3 = this.b;
                parsableByteArray3.d(parsableByteArray3.d() + a2);
                this.e = this.f3306a.g[i3 + -1] != 255;
            }
            if (i3 == this.f3306a.d) {
                i3 = -1;
            }
            this.c = i3;
        }
        return true;
    }

    public ParsableByteArray b() {
        return this.b;
    }

    public void c() {
        this.f3306a.a();
        this.b.B();
        this.c = -1;
        this.e = false;
    }

    public void d() {
        ParsableByteArray parsableByteArray = this.b;
        byte[] bArr = parsableByteArray.f3641a;
        if (bArr.length != 65025) {
            parsableByteArray.f3641a = Arrays.copyOf(bArr, Math.max(65025, parsableByteArray.d()));
        }
    }

    public OggPageHeader a() {
        return this.f3306a;
    }

    private int a(int i) {
        int i2;
        int i3 = 0;
        this.d = 0;
        do {
            int i4 = this.d;
            int i5 = i + i4;
            OggPageHeader oggPageHeader = this.f3306a;
            if (i5 >= oggPageHeader.d) {
                break;
            }
            int[] iArr = oggPageHeader.g;
            this.d = i4 + 1;
            i2 = iArr[i4 + i];
            i3 += i2;
        } while (i2 == 255);
        return i3;
    }
}
