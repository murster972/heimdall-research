package com.google.android.exoplayer2.extractor.mkv;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.ArrayDeque;

final class DefaultEbmlReader implements EbmlReader {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f3264a = new byte[8];
    private final ArrayDeque<MasterElement> b = new ArrayDeque<>();
    private final VarintReader c = new VarintReader();
    private EbmlReaderOutput d;
    private int e;
    private int f;
    private long g;

    private static final class MasterElement {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final int f3265a;
        /* access modifiers changed from: private */
        public final long b;

        private MasterElement(int i, long j) {
            this.f3265a = i;
            this.b = j;
        }
    }

    private long b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.a();
        while (true) {
            extractorInput.a(this.f3264a, 0, 4);
            int a2 = VarintReader.a(this.f3264a[0]);
            if (a2 != -1 && a2 <= 4) {
                int a3 = (int) VarintReader.a(this.f3264a, a2, false);
                if (this.d.c(a3)) {
                    extractorInput.c(a2);
                    return (long) a3;
                }
            }
            extractorInput.c(1);
        }
    }

    private String c(ExtractorInput extractorInput, int i) throws IOException, InterruptedException {
        if (i == 0) {
            return "";
        }
        byte[] bArr = new byte[i];
        extractorInput.readFully(bArr, 0, i);
        while (i > 0 && bArr[i - 1] == 0) {
            i--;
        }
        return new String(bArr, 0, i);
    }

    public void a(EbmlReaderOutput ebmlReaderOutput) {
        this.d = ebmlReaderOutput;
    }

    public void reset() {
        this.e = 0;
        this.b.clear();
        this.c.b();
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        Assertions.b(this.d != null);
        while (true) {
            if (this.b.isEmpty() || extractorInput.getPosition() < this.b.peek().b) {
                if (this.e == 0) {
                    long a2 = this.c.a(extractorInput, true, false, 4);
                    if (a2 == -2) {
                        a2 = b(extractorInput);
                    }
                    if (a2 == -1) {
                        return false;
                    }
                    this.f = (int) a2;
                    this.e = 1;
                }
                if (this.e == 1) {
                    this.g = this.c.a(extractorInput, false, true, 8);
                    this.e = 2;
                }
                int b2 = this.d.b(this.f);
                if (b2 == 0) {
                    extractorInput.c((int) this.g);
                    this.e = 0;
                } else if (b2 == 1) {
                    long position = extractorInput.getPosition();
                    this.b.push(new MasterElement(this.f, this.g + position));
                    this.d.a(this.f, position, this.g);
                    this.e = 0;
                    return true;
                } else if (b2 == 2) {
                    long j = this.g;
                    if (j <= 8) {
                        this.d.a(this.f, b(extractorInput, (int) j));
                        this.e = 0;
                        return true;
                    }
                    throw new ParserException("Invalid integer size: " + this.g);
                } else if (b2 == 3) {
                    long j2 = this.g;
                    if (j2 <= 2147483647L) {
                        this.d.a(this.f, c(extractorInput, (int) j2));
                        this.e = 0;
                        return true;
                    }
                    throw new ParserException("String element size: " + this.g);
                } else if (b2 == 4) {
                    this.d.a(this.f, (int) this.g, extractorInput);
                    this.e = 0;
                    return true;
                } else if (b2 == 5) {
                    long j3 = this.g;
                    if (j3 == 4 || j3 == 8) {
                        this.d.a(this.f, a(extractorInput, (int) this.g));
                        this.e = 0;
                        return true;
                    }
                    throw new ParserException("Invalid float size: " + this.g);
                } else {
                    throw new ParserException("Invalid element type " + b2);
                }
            } else {
                this.d.a(this.b.pop().f3265a);
                return true;
            }
        }
    }

    private long b(ExtractorInput extractorInput, int i) throws IOException, InterruptedException {
        extractorInput.readFully(this.f3264a, 0, i);
        long j = 0;
        for (int i2 = 0; i2 < i; i2++) {
            j = (j << 8) | ((long) (this.f3264a[i2] & 255));
        }
        return j;
    }

    private double a(ExtractorInput extractorInput, int i) throws IOException, InterruptedException {
        long b2 = b(extractorInput, i);
        if (i == 4) {
            return (double) Float.intBitsToFloat((int) b2);
        }
        return Double.longBitsToDouble(b2);
    }
}
