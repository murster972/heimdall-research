package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.MpegAudioHeader;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.List;

public final class MpegAudioReader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3332a;
    private final MpegAudioHeader b;
    private final String c;
    private String d;
    private TrackOutput e;
    private int f;
    private int g;
    private boolean h;
    private boolean i;
    private long j;
    private int k;
    private long l;

    public MpegAudioReader() {
        this((String) null);
    }

    private void b(ParsableByteArray parsableByteArray) {
        byte[] bArr = parsableByteArray.f3641a;
        int d2 = parsableByteArray.d();
        for (int c2 = parsableByteArray.c(); c2 < d2; c2++) {
            boolean z = (bArr[c2] & 255) == 255;
            boolean z2 = this.i && (bArr[c2] & 224) == 224;
            this.i = z;
            if (z2) {
                parsableByteArray.e(c2 + 1);
                this.i = false;
                this.f3332a.f3641a[1] = bArr[c2];
                this.g = 2;
                this.f = 1;
                return;
            }
        }
        parsableByteArray.e(d2);
    }

    private void c(ParsableByteArray parsableByteArray) {
        int min = Math.min(parsableByteArray.a(), this.k - this.g);
        this.e.a(parsableByteArray, min);
        this.g += min;
        int i2 = this.g;
        int i3 = this.k;
        if (i2 >= i3) {
            this.e.a(this.l, 1, i3, 0, (TrackOutput.CryptoData) null);
            this.l += this.j;
            this.g = 0;
            this.f = 0;
        }
    }

    private void d(ParsableByteArray parsableByteArray) {
        int min = Math.min(parsableByteArray.a(), 4 - this.g);
        parsableByteArray.a(this.f3332a.f3641a, this.g, min);
        this.g += min;
        if (this.g >= 4) {
            this.f3332a.e(0);
            if (!MpegAudioHeader.a(this.f3332a.h(), this.b)) {
                this.g = 0;
                this.f = 1;
                return;
            }
            MpegAudioHeader mpegAudioHeader = this.b;
            this.k = mpegAudioHeader.c;
            if (!this.h) {
                int i2 = mpegAudioHeader.d;
                this.j = (((long) mpegAudioHeader.g) * 1000000) / ((long) i2);
                this.e.a(Format.a(this.d, mpegAudioHeader.b, (String) null, -1, 4096, mpegAudioHeader.e, i2, (List<byte[]>) null, (DrmInitData) null, 0, this.c));
                this.h = true;
            }
            this.f3332a.e(0);
            this.e.a(this.f3332a, 4);
            this.f = 2;
        }
    }

    public void a() {
        this.f = 0;
        this.g = 0;
        this.i = false;
    }

    public void b() {
    }

    public MpegAudioReader(String str) {
        this.f = 0;
        this.f3332a = new ParsableByteArray(4);
        this.f3332a.f3641a[0] = -1;
        this.b = new MpegAudioHeader();
        this.c = str;
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.d = trackIdGenerator.b();
        this.e = extractorOutput.a(trackIdGenerator.c(), 1);
    }

    public void a(long j2, boolean z) {
        this.l = j2;
    }

    public void a(ParsableByteArray parsableByteArray) {
        while (parsableByteArray.a() > 0) {
            int i2 = this.f;
            if (i2 == 0) {
                b(parsableByteArray);
            } else if (i2 == 1) {
                d(parsableByteArray);
            } else if (i2 == 2) {
                c(parsableByteArray);
            } else {
                throw new IllegalStateException();
            }
        }
    }
}
