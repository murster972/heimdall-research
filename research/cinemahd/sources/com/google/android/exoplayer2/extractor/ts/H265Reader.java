package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.ParsableNalUnitBitArray;
import java.util.Collections;
import java.util.List;

public final class H265Reader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final SeiReader f3328a;
    private String b;
    private TrackOutput c;
    private SampleReader d;
    private boolean e;
    private final boolean[] f = new boolean[3];
    private final NalUnitTargetBuffer g = new NalUnitTargetBuffer(32, 128);
    private final NalUnitTargetBuffer h = new NalUnitTargetBuffer(33, 128);
    private final NalUnitTargetBuffer i = new NalUnitTargetBuffer(34, 128);
    private final NalUnitTargetBuffer j = new NalUnitTargetBuffer(39, 128);
    private final NalUnitTargetBuffer k = new NalUnitTargetBuffer(40, 128);
    private long l;
    private long m;
    private final ParsableByteArray n = new ParsableByteArray();

    public H265Reader(SeiReader seiReader) {
        this.f3328a = seiReader;
    }

    private void b(long j2, int i2, int i3, long j3) {
        if (this.e) {
            this.d.a(j2, i2, i3, j3);
        } else {
            this.g.b(i3);
            this.h.b(i3);
            this.i.b(i3);
        }
        this.j.b(i3);
        this.k.b(i3);
    }

    public void a() {
        NalUnitUtil.a(this.f);
        this.g.b();
        this.h.b();
        this.i.b();
        this.j.b();
        this.k.b();
        this.d.a();
        this.l = 0;
    }

    public void b() {
    }

    private static final class SampleReader {

        /* renamed from: a  reason: collision with root package name */
        private final TrackOutput f3329a;
        private long b;
        private boolean c;
        private int d;
        private long e;
        private boolean f;
        private boolean g;
        private boolean h;
        private boolean i;
        private boolean j;
        private long k;
        private long l;
        private boolean m;

        public SampleReader(TrackOutput trackOutput) {
            this.f3329a = trackOutput;
        }

        public void a() {
            this.f = false;
            this.g = false;
            this.h = false;
            this.i = false;
            this.j = false;
        }

        public void a(long j2, int i2, int i3, long j3) {
            this.g = false;
            this.h = false;
            this.e = j3;
            this.d = 0;
            this.b = j2;
            boolean z = true;
            if (i3 >= 32) {
                if (!this.j && this.i) {
                    a(i2);
                    this.i = false;
                }
                if (i3 <= 34) {
                    this.h = !this.j;
                    this.j = true;
                }
            }
            this.c = i3 >= 16 && i3 <= 21;
            if (!this.c && i3 > 9) {
                z = false;
            }
            this.f = z;
        }

        public void a(byte[] bArr, int i2, int i3) {
            if (this.f) {
                int i4 = this.d;
                int i5 = (i2 + 2) - i4;
                if (i5 < i3) {
                    this.g = (bArr[i5] & 128) != 0;
                    this.f = false;
                    return;
                }
                this.d = i4 + (i3 - i2);
            }
        }

        public void a(long j2, int i2) {
            if (this.j && this.g) {
                this.m = this.c;
                this.j = false;
            } else if (this.h || this.g) {
                if (this.i) {
                    a(i2 + ((int) (j2 - this.b)));
                }
                this.k = this.b;
                this.l = this.e;
                this.i = true;
                this.m = this.c;
            }
        }

        private void a(int i2) {
            boolean z = this.m;
            int i3 = (int) (this.b - this.k);
            this.f3329a.a(this.l, z ? 1 : 0, i3, i2, (TrackOutput.CryptoData) null);
        }
    }

    private static void b(ParsableNalUnitBitArray parsableNalUnitBitArray) {
        int d2 = parsableNalUnitBitArray.d();
        boolean z = false;
        int i2 = 0;
        for (int i3 = 0; i3 < d2; i3++) {
            if (i3 != 0) {
                z = parsableNalUnitBitArray.b();
            }
            if (z) {
                parsableNalUnitBitArray.e();
                parsableNalUnitBitArray.d();
                for (int i4 = 0; i4 <= i2; i4++) {
                    if (parsableNalUnitBitArray.b()) {
                        parsableNalUnitBitArray.e();
                    }
                }
            } else {
                int d3 = parsableNalUnitBitArray.d();
                int d4 = parsableNalUnitBitArray.d();
                int i5 = d3 + d4;
                for (int i6 = 0; i6 < d3; i6++) {
                    parsableNalUnitBitArray.d();
                    parsableNalUnitBitArray.e();
                }
                for (int i7 = 0; i7 < d4; i7++) {
                    parsableNalUnitBitArray.d();
                    parsableNalUnitBitArray.e();
                }
                i2 = i5;
            }
        }
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.b = trackIdGenerator.b();
        this.c = extractorOutput.a(trackIdGenerator.c(), 2);
        this.d = new SampleReader(this.c);
        this.f3328a.a(extractorOutput, trackIdGenerator);
    }

    public void a(long j2, boolean z) {
        this.m = j2;
    }

    public void a(ParsableByteArray parsableByteArray) {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        while (parsableByteArray.a() > 0) {
            int c2 = parsableByteArray.c();
            int d2 = parsableByteArray.d();
            byte[] bArr = parsableByteArray2.f3641a;
            this.l += (long) parsableByteArray.a();
            this.c.a(parsableByteArray2, parsableByteArray.a());
            while (true) {
                if (c2 < d2) {
                    int a2 = NalUnitUtil.a(bArr, c2, d2, this.f);
                    if (a2 == d2) {
                        a(bArr, c2, d2);
                        return;
                    }
                    int a3 = NalUnitUtil.a(bArr, a2);
                    int i2 = a2 - c2;
                    if (i2 > 0) {
                        a(bArr, c2, a2);
                    }
                    int i3 = d2 - a2;
                    long j2 = this.l - ((long) i3);
                    int i4 = i2 < 0 ? -i2 : 0;
                    long j3 = j2;
                    int i5 = i3;
                    a(j3, i5, i4, this.m);
                    b(j3, i5, a3, this.m);
                    c2 = a2 + 3;
                }
            }
        }
    }

    private void a(byte[] bArr, int i2, int i3) {
        if (this.e) {
            this.d.a(bArr, i2, i3);
        } else {
            this.g.a(bArr, i2, i3);
            this.h.a(bArr, i2, i3);
            this.i.a(bArr, i2, i3);
        }
        this.j.a(bArr, i2, i3);
        this.k.a(bArr, i2, i3);
    }

    private void a(long j2, int i2, int i3, long j3) {
        if (this.e) {
            this.d.a(j2, i2);
        } else {
            this.g.a(i3);
            this.h.a(i3);
            this.i.a(i3);
            if (this.g.a() && this.h.a() && this.i.a()) {
                this.c.a(a(this.b, this.g, this.h, this.i));
                this.e = true;
            }
        }
        if (this.j.a(i3)) {
            NalUnitTargetBuffer nalUnitTargetBuffer = this.j;
            this.n.a(this.j.d, NalUnitUtil.c(nalUnitTargetBuffer.d, nalUnitTargetBuffer.e));
            this.n.f(5);
            this.f3328a.a(j3, this.n);
        }
        if (this.k.a(i3)) {
            NalUnitTargetBuffer nalUnitTargetBuffer2 = this.k;
            this.n.a(this.k.d, NalUnitUtil.c(nalUnitTargetBuffer2.d, nalUnitTargetBuffer2.e));
            this.n.f(5);
            this.f3328a.a(j3, this.n);
        }
    }

    private static Format a(String str, NalUnitTargetBuffer nalUnitTargetBuffer, NalUnitTargetBuffer nalUnitTargetBuffer2, NalUnitTargetBuffer nalUnitTargetBuffer3) {
        float f2;
        NalUnitTargetBuffer nalUnitTargetBuffer4 = nalUnitTargetBuffer;
        NalUnitTargetBuffer nalUnitTargetBuffer5 = nalUnitTargetBuffer2;
        NalUnitTargetBuffer nalUnitTargetBuffer6 = nalUnitTargetBuffer3;
        int i2 = nalUnitTargetBuffer4.e;
        byte[] bArr = new byte[(nalUnitTargetBuffer5.e + i2 + nalUnitTargetBuffer6.e)];
        System.arraycopy(nalUnitTargetBuffer4.d, 0, bArr, 0, i2);
        System.arraycopy(nalUnitTargetBuffer5.d, 0, bArr, nalUnitTargetBuffer4.e, nalUnitTargetBuffer5.e);
        System.arraycopy(nalUnitTargetBuffer6.d, 0, bArr, nalUnitTargetBuffer4.e + nalUnitTargetBuffer5.e, nalUnitTargetBuffer6.e);
        ParsableNalUnitBitArray parsableNalUnitBitArray = new ParsableNalUnitBitArray(nalUnitTargetBuffer5.d, 0, nalUnitTargetBuffer5.e);
        parsableNalUnitBitArray.c(44);
        int b2 = parsableNalUnitBitArray.b(3);
        parsableNalUnitBitArray.e();
        parsableNalUnitBitArray.c(88);
        parsableNalUnitBitArray.c(8);
        int i3 = 0;
        for (int i4 = 0; i4 < b2; i4++) {
            if (parsableNalUnitBitArray.b()) {
                i3 += 89;
            }
            if (parsableNalUnitBitArray.b()) {
                i3 += 8;
            }
        }
        parsableNalUnitBitArray.c(i3);
        if (b2 > 0) {
            parsableNalUnitBitArray.c((8 - b2) * 2);
        }
        parsableNalUnitBitArray.d();
        int d2 = parsableNalUnitBitArray.d();
        if (d2 == 3) {
            parsableNalUnitBitArray.e();
        }
        int d3 = parsableNalUnitBitArray.d();
        int d4 = parsableNalUnitBitArray.d();
        if (parsableNalUnitBitArray.b()) {
            int d5 = parsableNalUnitBitArray.d();
            int d6 = parsableNalUnitBitArray.d();
            int d7 = parsableNalUnitBitArray.d();
            int d8 = parsableNalUnitBitArray.d();
            d3 -= ((d2 == 1 || d2 == 2) ? 2 : 1) * (d5 + d6);
            d4 -= (d2 == 1 ? 2 : 1) * (d7 + d8);
        }
        int i5 = d3;
        int i6 = d4;
        parsableNalUnitBitArray.d();
        parsableNalUnitBitArray.d();
        int d9 = parsableNalUnitBitArray.d();
        for (int i7 = parsableNalUnitBitArray.b() ? 0 : b2; i7 <= b2; i7++) {
            parsableNalUnitBitArray.d();
            parsableNalUnitBitArray.d();
            parsableNalUnitBitArray.d();
        }
        parsableNalUnitBitArray.d();
        parsableNalUnitBitArray.d();
        parsableNalUnitBitArray.d();
        parsableNalUnitBitArray.d();
        parsableNalUnitBitArray.d();
        parsableNalUnitBitArray.d();
        if (parsableNalUnitBitArray.b() && parsableNalUnitBitArray.b()) {
            a(parsableNalUnitBitArray);
        }
        parsableNalUnitBitArray.c(2);
        if (parsableNalUnitBitArray.b()) {
            parsableNalUnitBitArray.c(8);
            parsableNalUnitBitArray.d();
            parsableNalUnitBitArray.d();
            parsableNalUnitBitArray.e();
        }
        b(parsableNalUnitBitArray);
        if (parsableNalUnitBitArray.b()) {
            for (int i8 = 0; i8 < parsableNalUnitBitArray.d(); i8++) {
                parsableNalUnitBitArray.c(d9 + 4 + 1);
            }
        }
        parsableNalUnitBitArray.c(2);
        float f3 = 1.0f;
        if (parsableNalUnitBitArray.b() && parsableNalUnitBitArray.b()) {
            int b3 = parsableNalUnitBitArray.b(8);
            if (b3 == 255) {
                int b4 = parsableNalUnitBitArray.b(16);
                int b5 = parsableNalUnitBitArray.b(16);
                if (!(b4 == 0 || b5 == 0)) {
                    f3 = ((float) b4) / ((float) b5);
                }
                f2 = f3;
            } else {
                float[] fArr = NalUnitUtil.b;
                if (b3 < fArr.length) {
                    f2 = fArr[b3];
                } else {
                    Log.d("H265Reader", "Unexpected aspect_ratio_idc value: " + b3);
                }
            }
            return Format.a(str, "video/hevc", (String) null, -1, -1, i5, i6, -1.0f, (List<byte[]>) Collections.singletonList(bArr), -1, f2, (DrmInitData) null);
        }
        f2 = 1.0f;
        return Format.a(str, "video/hevc", (String) null, -1, -1, i5, i6, -1.0f, (List<byte[]>) Collections.singletonList(bArr), -1, f2, (DrmInitData) null);
    }

    private static void a(ParsableNalUnitBitArray parsableNalUnitBitArray) {
        for (int i2 = 0; i2 < 4; i2++) {
            int i3 = 0;
            while (i3 < 6) {
                if (!parsableNalUnitBitArray.b()) {
                    parsableNalUnitBitArray.d();
                } else {
                    int min = Math.min(64, 1 << ((i2 << 1) + 4));
                    if (i2 > 1) {
                        parsableNalUnitBitArray.c();
                    }
                    for (int i4 = 0; i4 < min; i4++) {
                        parsableNalUnitBitArray.c();
                    }
                }
                int i5 = 3;
                if (i2 != 3) {
                    i5 = 1;
                }
                i3 += i5;
            }
        }
    }
}
