package com.google.android.exoplayer2.extractor;

import java.io.IOException;

public interface ExtractorInput {
    void a();

    void a(int i) throws IOException, InterruptedException;

    void a(byte[] bArr, int i, int i2) throws IOException, InterruptedException;

    boolean a(int i, boolean z) throws IOException, InterruptedException;

    boolean a(byte[] bArr, int i, int i2, boolean z) throws IOException, InterruptedException;

    int b(int i) throws IOException, InterruptedException;

    long b();

    boolean b(byte[] bArr, int i, int i2, boolean z) throws IOException, InterruptedException;

    void c(int i) throws IOException, InterruptedException;

    long getLength();

    long getPosition();

    int read(byte[] bArr, int i, int i2) throws IOException, InterruptedException;

    void readFully(byte[] bArr, int i, int i2) throws IOException, InterruptedException;
}
