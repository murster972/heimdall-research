package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;

abstract class StreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final OggPacket f3308a = new OggPacket();
    private TrackOutput b;
    private ExtractorOutput c;
    private OggSeeker d;
    private long e;
    private long f;
    private long g;
    private int h;
    private int i;
    private SetupData j;
    private long k;
    private boolean l;
    private boolean m;

    static class SetupData {

        /* renamed from: a  reason: collision with root package name */
        Format f3309a;
        OggSeeker b;

        SetupData() {
        }
    }

    private static final class UnseekableOggSeeker implements OggSeeker {
        private UnseekableOggSeeker() {
        }

        public long a(ExtractorInput extractorInput) throws IOException, InterruptedException {
            return -1;
        }

        public long c(long j) {
            return 0;
        }

        public SeekMap c() {
            return new SeekMap.Unseekable(-9223372036854775807L);
        }
    }

    private int b(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        ExtractorInput extractorInput2 = extractorInput;
        long a2 = this.d.a(extractorInput2);
        if (a2 >= 0) {
            positionHolder.f3254a = a2;
            return 1;
        }
        if (a2 < -1) {
            c(-(a2 + 2));
        }
        if (!this.l) {
            this.c.a(this.d.c());
            this.l = true;
        }
        if (this.k > 0 || this.f3308a.a(extractorInput2)) {
            this.k = 0;
            ParsableByteArray b2 = this.f3308a.b();
            long a3 = a(b2);
            if (a3 >= 0) {
                long j2 = this.g;
                if (j2 + a3 >= this.e) {
                    long a4 = a(j2);
                    this.b.a(b2, b2.d());
                    this.b.a(a4, 1, b2.d(), 0, (TrackOutput.CryptoData) null);
                    this.e = -1;
                }
            }
            this.g += a3;
            return 0;
        }
        this.h = 3;
        return -1;
    }

    /* access modifiers changed from: protected */
    public abstract long a(ParsableByteArray parsableByteArray);

    /* access modifiers changed from: package-private */
    public void a(ExtractorOutput extractorOutput, TrackOutput trackOutput) {
        this.c = extractorOutput;
        this.b = trackOutput;
        a(true);
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(ParsableByteArray parsableByteArray, long j2, SetupData setupData) throws IOException, InterruptedException;

    /* access modifiers changed from: protected */
    public void c(long j2) {
        this.g = j2;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (z) {
            this.j = new SetupData();
            this.f = 0;
            this.h = 0;
        } else {
            this.h = 1;
        }
        this.e = -1;
        this.g = 0;
    }

    /* access modifiers changed from: package-private */
    public final void a(long j2, long j3) {
        this.f3308a.c();
        if (j2 == 0) {
            a(!this.l);
        } else if (this.h != 0) {
            this.e = this.d.c(j3);
            this.h = 2;
        }
    }

    /* access modifiers changed from: package-private */
    public final int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        int i2 = this.h;
        if (i2 == 0) {
            return a(extractorInput);
        }
        if (i2 == 1) {
            extractorInput.c((int) this.f);
            this.h = 2;
            return 0;
        } else if (i2 == 2) {
            return b(extractorInput, positionHolder);
        } else {
            throw new IllegalStateException();
        }
    }

    /* access modifiers changed from: protected */
    public long b(long j2) {
        return (((long) this.i) * j2) / 1000000;
    }

    private int a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        boolean z = true;
        while (z) {
            if (!this.f3308a.a(extractorInput)) {
                this.h = 3;
                return -1;
            }
            this.k = extractorInput.getPosition() - this.f;
            z = a(this.f3308a.b(), this.f, this.j);
            if (z) {
                this.f = extractorInput.getPosition();
            }
        }
        ExtractorInput extractorInput2 = extractorInput;
        Format format = this.j.f3309a;
        this.i = format.u;
        if (!this.m) {
            this.b.a(format);
            this.m = true;
        }
        OggSeeker oggSeeker = this.j.b;
        if (oggSeeker != null) {
            this.d = oggSeeker;
        } else if (extractorInput.getLength() == -1) {
            this.d = new UnseekableOggSeeker();
        } else {
            OggPageHeader a2 = this.f3308a.a();
            this.d = new DefaultOggSeeker(this.f, extractorInput.getLength(), this, (long) (a2.e + a2.f), a2.c, (a2.b & 4) != 0);
        }
        this.j = null;
        this.h = 2;
        this.f3308a.d();
        return 0;
    }

    /* access modifiers changed from: protected */
    public long a(long j2) {
        return (j2 * 1000000) / ((long) this.i);
    }
}
