package com.google.android.exoplayer2.extractor.ts;

import android.util.Pair;
import com.facebook.common.util.ByteConstants;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.DummyTrackOutput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.gms.ads.AdRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class AdtsReader implements ElementaryStreamReader {
    private static final byte[] v = {73, 68, 51};

    /* renamed from: a  reason: collision with root package name */
    private final boolean f3319a;
    private final ParsableBitArray b;
    private final ParsableByteArray c;
    private final String d;
    private String e;
    private TrackOutput f;
    private TrackOutput g;
    private int h;
    private int i;
    private int j;
    private boolean k;
    private boolean l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private long q;
    private int r;
    private long s;
    private TrackOutput t;
    private long u;

    public AdtsReader(boolean z) {
        this(z, (String) null);
    }

    public static boolean a(int i2) {
        return (i2 & 65526) == 65520;
    }

    private void b(ParsableByteArray parsableByteArray) {
        if (parsableByteArray.a() != 0) {
            this.b.f3640a[0] = parsableByteArray.f3641a[parsableByteArray.c()];
            this.b.b(2);
            int a2 = this.b.a(4);
            int i2 = this.n;
            if (i2 == -1 || a2 == i2) {
                if (!this.l) {
                    this.l = true;
                    this.m = this.o;
                    this.n = a2;
                }
                i();
                return;
            }
            f();
        }
    }

    private void d() throws ParserException {
        this.b.b(0);
        if (!this.p) {
            int a2 = this.b.a(2) + 1;
            if (a2 != 2) {
                Log.d("AdtsReader", "Detected audio object type: " + a2 + ", but assuming AAC LC.");
                a2 = 2;
            }
            this.b.c(5);
            byte[] a3 = CodecSpecificDataUtil.a(a2, this.n, this.b.a(3));
            Pair<Integer, Integer> a4 = CodecSpecificDataUtil.a(a3);
            Format a5 = Format.a(this.e, "audio/mp4a-latm", (String) null, -1, -1, ((Integer) a4.second).intValue(), ((Integer) a4.first).intValue(), (List<byte[]>) Collections.singletonList(a3), (DrmInitData) null, 0, this.d);
            this.q = 1024000000 / ((long) a5.u);
            this.f.a(a5);
            this.p = true;
        } else {
            this.b.c(10);
        }
        this.b.c(4);
        int a6 = (this.b.a(13) - 2) - 5;
        if (this.k) {
            a6 -= 2;
        }
        a(this.f, this.q, 0, a6);
    }

    private void e() {
        this.g.a(this.c, 10);
        this.c.e(6);
        a(this.g, 0, 10, this.c.s() + 10);
    }

    private void f() {
        this.l = false;
        h();
    }

    private void g() {
        this.h = 1;
        this.i = 0;
    }

    private void h() {
        this.h = 0;
        this.i = 0;
        this.j = 256;
    }

    private void i() {
        this.h = 3;
        this.i = 0;
    }

    private void j() {
        this.h = 2;
        this.i = v.length;
        this.r = 0;
        this.c.e(0);
    }

    public void a() {
        f();
    }

    public void b() {
    }

    public long c() {
        return this.q;
    }

    public AdtsReader(boolean z, String str) {
        this.b = new ParsableBitArray(new byte[7]);
        this.c = new ParsableByteArray(Arrays.copyOf(v, 10));
        h();
        this.m = -1;
        this.n = -1;
        this.q = -9223372036854775807L;
        this.f3319a = z;
        this.d = str;
    }

    private void c(ParsableByteArray parsableByteArray) {
        byte[] bArr = parsableByteArray.f3641a;
        int c2 = parsableByteArray.c();
        int d2 = parsableByteArray.d();
        while (c2 < d2) {
            int i2 = c2 + 1;
            byte b2 = bArr[c2] & 255;
            if (this.j != 512 || !a((byte) -1, (byte) b2) || (!this.l && !a(parsableByteArray, i2 - 2))) {
                int i3 = this.j;
                byte b3 = b2 | i3;
                if (b3 == 329) {
                    this.j = 768;
                } else if (b3 == 511) {
                    this.j = AdRequest.MAX_CONTENT_URL_LENGTH;
                } else if (b3 == 836) {
                    this.j = ByteConstants.KB;
                } else if (b3 == 1075) {
                    j();
                    parsableByteArray.e(i2);
                    return;
                } else if (i3 != 256) {
                    this.j = 256;
                    i2--;
                }
                c2 = i2;
            } else {
                this.o = (b2 & 8) >> 3;
                boolean z = true;
                if ((b2 & 1) != 0) {
                    z = false;
                }
                this.k = z;
                if (!this.l) {
                    g();
                } else {
                    i();
                }
                parsableByteArray.e(i2);
                return;
            }
        }
        parsableByteArray.e(c2);
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.e = trackIdGenerator.b();
        this.f = extractorOutput.a(trackIdGenerator.c(), 1);
        if (this.f3319a) {
            trackIdGenerator.a();
            this.g = extractorOutput.a(trackIdGenerator.c(), 4);
            this.g.a(Format.a(trackIdGenerator.b(), "application/id3", (String) null, -1, (DrmInitData) null));
            return;
        }
        this.g = new DummyTrackOutput();
    }

    public void a(long j2, boolean z) {
        this.s = j2;
    }

    public void a(ParsableByteArray parsableByteArray) throws ParserException {
        while (parsableByteArray.a() > 0) {
            int i2 = this.h;
            if (i2 == 0) {
                c(parsableByteArray);
            } else if (i2 == 1) {
                b(parsableByteArray);
            } else if (i2 != 2) {
                if (i2 == 3) {
                    if (a(parsableByteArray, this.b.f3640a, this.k ? 7 : 5)) {
                        d();
                    }
                } else if (i2 == 4) {
                    d(parsableByteArray);
                } else {
                    throw new IllegalStateException();
                }
            } else if (a(parsableByteArray, this.c.f3641a, 10)) {
                e();
            }
        }
    }

    private boolean b(ParsableByteArray parsableByteArray, byte[] bArr, int i2) {
        if (parsableByteArray.a() < i2) {
            return false;
        }
        parsableByteArray.a(bArr, 0, i2);
        return true;
    }

    private boolean a(ParsableByteArray parsableByteArray, byte[] bArr, int i2) {
        int min = Math.min(parsableByteArray.a(), i2 - this.i);
        parsableByteArray.a(bArr, this.i, min);
        this.i += min;
        return this.i == i2;
    }

    private void d(ParsableByteArray parsableByteArray) {
        int min = Math.min(parsableByteArray.a(), this.r - this.i);
        this.t.a(parsableByteArray, min);
        this.i += min;
        int i2 = this.i;
        int i3 = this.r;
        if (i2 == i3) {
            this.t.a(this.s, 1, i3, 0, (TrackOutput.CryptoData) null);
            this.s += this.u;
            h();
        }
    }

    private void a(TrackOutput trackOutput, long j2, int i2, int i3) {
        this.h = 4;
        this.i = i2;
        this.t = trackOutput;
        this.u = j2;
        this.r = i3;
    }

    private boolean a(ParsableByteArray parsableByteArray, int i2) {
        parsableByteArray.e(i2 + 1);
        if (!b(parsableByteArray, this.b.f3640a, 1)) {
            return false;
        }
        this.b.b(4);
        int a2 = this.b.a(1);
        int i3 = this.m;
        if (i3 != -1 && a2 != i3) {
            return false;
        }
        if (this.n != -1) {
            if (!b(parsableByteArray, this.b.f3640a, 1)) {
                return true;
            }
            this.b.b(2);
            if (this.b.a(4) != this.n) {
                return false;
            }
            parsableByteArray.e(i2 + 2);
        }
        if (!b(parsableByteArray, this.b.f3640a, 4)) {
            return true;
        }
        this.b.b(14);
        int a3 = this.b.a(13);
        if (a3 <= 6) {
            return false;
        }
        int i4 = i2 + a3;
        int i5 = i4 + 1;
        if (i5 >= parsableByteArray.d()) {
            return true;
        }
        byte[] bArr = parsableByteArray.f3641a;
        if (!a(bArr[i4], bArr[i5]) || (this.m != -1 && ((parsableByteArray.f3641a[i5] & 8) >> 3) != a2)) {
            return false;
        }
        return true;
    }

    private boolean a(byte b2, byte b3) {
        return a((int) ((b2 & 255) << 8) | (b3 & 255));
    }
}
