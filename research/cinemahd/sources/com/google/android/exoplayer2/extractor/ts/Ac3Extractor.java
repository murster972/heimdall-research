package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public final class Ac3Extractor implements Extractor {
    private static final int e = Util.c("ID3");

    /* renamed from: a  reason: collision with root package name */
    private final long f3316a;
    private final Ac3Reader b;
    private final ParsableByteArray c;
    private boolean d;

    static {
        a aVar = a.f3351a;
    }

    public Ac3Extractor() {
        this(0);
    }

    static /* synthetic */ Extractor[] a() {
        return new Extractor[]{new Ac3Extractor()};
    }

    public void release() {
    }

    public Ac3Extractor(long j) {
        this.f3316a = j;
        this.b = new Ac3Reader();
        this.c = new ParsableByteArray(2786);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0032, code lost:
        r8.a();
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003b, code lost:
        if ((r4 - r3) < 8192) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003d, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.google.android.exoplayer2.extractor.ExtractorInput r8) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r7 = this;
            com.google.android.exoplayer2.util.ParsableByteArray r0 = new com.google.android.exoplayer2.util.ParsableByteArray
            r1 = 10
            r0.<init>((int) r1)
            r2 = 0
            r3 = 0
        L_0x0009:
            byte[] r4 = r0.f3641a
            r8.a(r4, r2, r1)
            r0.e(r2)
            int r4 = r0.w()
            int r5 = e
            if (r4 == r5) goto L_0x0058
            r8.a()
            r8.a(r3)
            r4 = r3
        L_0x0020:
            r1 = 0
        L_0x0021:
            byte[] r5 = r0.f3641a
            r6 = 6
            r8.a(r5, r2, r6)
            r0.e(r2)
            int r5 = r0.z()
            r6 = 2935(0xb77, float:4.113E-42)
            if (r5 == r6) goto L_0x0042
            r8.a()
            int r4 = r4 + 1
            int r1 = r4 - r3
            r5 = 8192(0x2000, float:1.14794E-41)
            if (r1 < r5) goto L_0x003e
            return r2
        L_0x003e:
            r8.a(r4)
            goto L_0x0020
        L_0x0042:
            r5 = 1
            int r1 = r1 + r5
            r6 = 4
            if (r1 < r6) goto L_0x0048
            return r5
        L_0x0048:
            byte[] r5 = r0.f3641a
            int r5 = com.google.android.exoplayer2.audio.Ac3Util.a((byte[]) r5)
            r6 = -1
            if (r5 != r6) goto L_0x0052
            return r2
        L_0x0052:
            int r5 = r5 + -6
            r8.a(r5)
            goto L_0x0021
        L_0x0058:
            r4 = 3
            r0.f(r4)
            int r4 = r0.s()
            int r5 = r4 + 10
            int r3 = r3 + r5
            r8.a(r4)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.ts.Ac3Extractor.a(com.google.android.exoplayer2.extractor.ExtractorInput):boolean");
    }

    public void a(ExtractorOutput extractorOutput) {
        this.b.a(extractorOutput, new TsPayloadReader.TrackIdGenerator(0, 1));
        extractorOutput.a();
        extractorOutput.a(new SeekMap.Unseekable(-9223372036854775807L));
    }

    public void a(long j, long j2) {
        this.d = false;
        this.b.a();
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        int read = extractorInput.read(this.c.f3641a, 0, 2786);
        if (read == -1) {
            return -1;
        }
        this.c.e(0);
        this.c.d(read);
        if (!this.d) {
            this.b.a(this.f3316a, true);
            this.d = true;
        }
        this.b.a(this.c);
        return 0;
    }
}
