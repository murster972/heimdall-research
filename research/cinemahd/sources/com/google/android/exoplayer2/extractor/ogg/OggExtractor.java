package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;

public class OggExtractor implements Extractor {

    /* renamed from: a  reason: collision with root package name */
    private ExtractorOutput f3305a;
    private StreamReader b;
    private boolean c;

    static {
        a aVar = a.f3314a;
    }

    static /* synthetic */ Extractor[] a() {
        return new Extractor[]{new OggExtractor()};
    }

    private boolean b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        OggPageHeader oggPageHeader = new OggPageHeader();
        if (oggPageHeader.a(extractorInput, true) && (oggPageHeader.b & 2) == 2) {
            int min = Math.min(oggPageHeader.f, 8);
            ParsableByteArray parsableByteArray = new ParsableByteArray(min);
            extractorInput.a(parsableByteArray.f3641a, 0, min);
            a(parsableByteArray);
            if (FlacReader.c(parsableByteArray)) {
                this.b = new FlacReader();
            } else {
                a(parsableByteArray);
                if (VorbisReader.c(parsableByteArray)) {
                    this.b = new VorbisReader();
                } else {
                    a(parsableByteArray);
                    if (OpusReader.b(parsableByteArray)) {
                        this.b = new OpusReader();
                    }
                }
            }
            return true;
        }
        return false;
    }

    public void release() {
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        try {
            return b(extractorInput);
        } catch (ParserException unused) {
            return false;
        }
    }

    public void a(ExtractorOutput extractorOutput) {
        this.f3305a = extractorOutput;
    }

    public void a(long j, long j2) {
        StreamReader streamReader = this.b;
        if (streamReader != null) {
            streamReader.a(j, j2);
        }
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        if (this.b == null) {
            if (b(extractorInput)) {
                extractorInput.a();
            } else {
                throw new ParserException("Failed to determine bitstream type");
            }
        }
        if (!this.c) {
            TrackOutput a2 = this.f3305a.a(0, 1);
            this.f3305a.a();
            this.b.a(this.f3305a, a2);
            this.c = true;
        }
        return this.b.a(extractorInput, positionHolder);
    }

    private static ParsableByteArray a(ParsableByteArray parsableByteArray) {
        parsableByteArray.e(0);
        return parsableByteArray;
    }
}
