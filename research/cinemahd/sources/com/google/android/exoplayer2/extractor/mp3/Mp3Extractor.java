package com.google.android.exoplayer2.extractor.mp3;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.GaplessInfoHolder;
import com.google.android.exoplayer2.extractor.Id3Peeker;
import com.google.android.exoplayer2.extractor.MpegAudioHeader;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.Id3Decoder;
import com.google.android.exoplayer2.metadata.id3.MlltFrame;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.util.List;
import okhttp3.internal.http2.Http2;

public final class Mp3Extractor implements Extractor {
    private static final Id3Decoder.FramePredicate o = b.f3278a;
    private static final int p = Util.c("Xing");
    private static final int q = Util.c("Info");
    private static final int r = Util.c("VBRI");

    /* renamed from: a  reason: collision with root package name */
    private final int f3274a;
    private final long b;
    private final ParsableByteArray c;
    private final MpegAudioHeader d;
    private final GaplessInfoHolder e;
    private final Id3Peeker f;
    private ExtractorOutput g;
    private TrackOutput h;
    private int i;
    private Metadata j;
    private Seeker k;
    private long l;
    private long m;
    private int n;

    interface Seeker extends SeekMap {
        long a();

        long a(long j);
    }

    static {
        a aVar = a.f3277a;
    }

    public Mp3Extractor() {
        this(0);
    }

    static /* synthetic */ boolean a(int i2, int i3, int i4, int i5, int i6) {
        return (i3 == 67 && i4 == 79 && i5 == 77 && (i6 == 77 || i2 == 2)) || (i3 == 77 && i4 == 76 && i5 == 76 && (i6 == 84 || i2 == 2));
    }

    private static boolean a(int i2, long j2) {
        return ((long) (i2 & -128000)) == (j2 & -128000);
    }

    static /* synthetic */ Extractor[] a() {
        return new Extractor[]{new Mp3Extractor()};
    }

    private Seeker b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.a(this.c.f3641a, 0, 4);
        this.c.e(0);
        MpegAudioHeader.a(this.c.h(), this.d);
        return new ConstantBitrateSeeker(extractorInput.getLength(), extractorInput.getPosition(), this.d);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.android.exoplayer2.extractor.mp3.Mp3Extractor.Seeker c(com.google.android.exoplayer2.extractor.ExtractorInput r10) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r9 = this;
            com.google.android.exoplayer2.util.ParsableByteArray r5 = new com.google.android.exoplayer2.util.ParsableByteArray
            com.google.android.exoplayer2.extractor.MpegAudioHeader r0 = r9.d
            int r0 = r0.c
            r5.<init>((int) r0)
            byte[] r0 = r5.f3641a
            com.google.android.exoplayer2.extractor.MpegAudioHeader r1 = r9.d
            int r1 = r1.c
            r6 = 0
            r10.a(r0, r6, r1)
            com.google.android.exoplayer2.extractor.MpegAudioHeader r0 = r9.d
            int r1 = r0.f3253a
            r2 = 1
            r1 = r1 & r2
            r3 = 21
            int r0 = r0.e
            if (r1 == 0) goto L_0x0026
            if (r0 == r2) goto L_0x0028
            r3 = 36
            r7 = 36
            goto L_0x002f
        L_0x0026:
            if (r0 == r2) goto L_0x002b
        L_0x0028:
            r7 = 21
            goto L_0x002f
        L_0x002b:
            r3 = 13
            r7 = 13
        L_0x002f:
            int r8 = a((com.google.android.exoplayer2.util.ParsableByteArray) r5, (int) r7)
            int r0 = p
            if (r8 == r0) goto L_0x005b
            int r0 = q
            if (r8 != r0) goto L_0x003c
            goto L_0x005b
        L_0x003c:
            int r0 = r
            if (r8 != r0) goto L_0x0056
            long r0 = r10.getLength()
            long r2 = r10.getPosition()
            com.google.android.exoplayer2.extractor.MpegAudioHeader r4 = r9.d
            com.google.android.exoplayer2.extractor.mp3.VbriSeeker r0 = com.google.android.exoplayer2.extractor.mp3.VbriSeeker.a(r0, r2, r4, r5)
            com.google.android.exoplayer2.extractor.MpegAudioHeader r1 = r9.d
            int r1 = r1.c
            r10.c(r1)
            goto L_0x00ab
        L_0x0056:
            r0 = 0
            r10.a()
            goto L_0x00ab
        L_0x005b:
            long r0 = r10.getLength()
            long r2 = r10.getPosition()
            com.google.android.exoplayer2.extractor.MpegAudioHeader r4 = r9.d
            com.google.android.exoplayer2.extractor.mp3.XingSeeker r0 = com.google.android.exoplayer2.extractor.mp3.XingSeeker.a(r0, r2, r4, r5)
            if (r0 == 0) goto L_0x0093
            com.google.android.exoplayer2.extractor.GaplessInfoHolder r1 = r9.e
            boolean r1 = r1.a()
            if (r1 != 0) goto L_0x0093
            r10.a()
            int r7 = r7 + 141
            r10.a(r7)
            com.google.android.exoplayer2.util.ParsableByteArray r1 = r9.c
            byte[] r1 = r1.f3641a
            r2 = 3
            r10.a(r1, r6, r2)
            com.google.android.exoplayer2.util.ParsableByteArray r1 = r9.c
            r1.e(r6)
            com.google.android.exoplayer2.extractor.GaplessInfoHolder r1 = r9.e
            com.google.android.exoplayer2.util.ParsableByteArray r2 = r9.c
            int r2 = r2.w()
            r1.a((int) r2)
        L_0x0093:
            com.google.android.exoplayer2.extractor.MpegAudioHeader r1 = r9.d
            int r1 = r1.c
            r10.c(r1)
            if (r0 == 0) goto L_0x00ab
            boolean r1 = r0.b()
            if (r1 != 0) goto L_0x00ab
            int r1 = q
            if (r8 != r1) goto L_0x00ab
            com.google.android.exoplayer2.extractor.mp3.Mp3Extractor$Seeker r10 = r9.b(r10)
            return r10
        L_0x00ab:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp3.Mp3Extractor.c(com.google.android.exoplayer2.extractor.ExtractorInput):com.google.android.exoplayer2.extractor.mp3.Mp3Extractor$Seeker");
    }

    private boolean d(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if ((this.k == null || extractorInput.b() != this.k.a()) && extractorInput.a(this.c.f3641a, 0, 4, true)) {
            return false;
        }
        return true;
    }

    private int e(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (this.n == 0) {
            extractorInput.a();
            if (d(extractorInput)) {
                return -1;
            }
            this.c.e(0);
            int h2 = this.c.h();
            if (!a(h2, (long) this.i) || MpegAudioHeader.a(h2) == -1) {
                extractorInput.c(1);
                this.i = 0;
                return 0;
            }
            MpegAudioHeader.a(h2, this.d);
            if (this.l == -9223372036854775807L) {
                this.l = this.k.a(extractorInput.getPosition());
                if (this.b != -9223372036854775807L) {
                    this.l += this.b - this.k.a(0);
                }
            }
            this.n = this.d.c;
        }
        int a2 = this.h.a(extractorInput, this.n, true);
        if (a2 == -1) {
            return -1;
        }
        this.n -= a2;
        if (this.n > 0) {
            return 0;
        }
        long j2 = this.l;
        MpegAudioHeader mpegAudioHeader = this.d;
        this.h.a(j2 + ((this.m * 1000000) / ((long) mpegAudioHeader.d)), 1, mpegAudioHeader.c, 0, (TrackOutput.CryptoData) null);
        this.m += (long) this.d.g;
        this.n = 0;
        return 0;
    }

    public void release() {
    }

    public Mp3Extractor(int i2) {
        this(i2, -9223372036854775807L);
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return a(extractorInput, true);
    }

    public Mp3Extractor(int i2, long j2) {
        this.f3274a = i2;
        this.b = j2;
        this.c = new ParsableByteArray(10);
        this.d = new MpegAudioHeader();
        this.e = new GaplessInfoHolder();
        this.l = -9223372036854775807L;
        this.f = new Id3Peeker();
    }

    public void a(ExtractorOutput extractorOutput) {
        this.g = extractorOutput;
        this.h = this.g.a(0, 1);
        this.g.a();
    }

    public void a(long j2, long j3) {
        this.i = 0;
        this.l = -9223372036854775807L;
        this.m = 0;
        this.n = 0;
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        if (this.i == 0) {
            try {
                a(extractorInput, false);
            } catch (EOFException unused) {
                return -1;
            }
        } else {
            ExtractorInput extractorInput2 = extractorInput;
        }
        if (this.k == null) {
            Seeker c2 = c(extractorInput);
            MlltSeeker a2 = a(this.j, extractorInput.getPosition());
            if (a2 != null) {
                this.k = a2;
            } else if (c2 != null) {
                this.k = c2;
            }
            Seeker seeker = this.k;
            if (seeker == null || (!seeker.b() && (this.f3274a & 1) != 0)) {
                this.k = b(extractorInput);
            }
            this.g.a(this.k);
            TrackOutput trackOutput = this.h;
            MpegAudioHeader mpegAudioHeader = this.d;
            String str = mpegAudioHeader.b;
            int i2 = mpegAudioHeader.e;
            int i3 = mpegAudioHeader.d;
            GaplessInfoHolder gaplessInfoHolder = this.e;
            trackOutput.a(Format.a((String) null, str, (String) null, -1, 4096, i2, i3, -1, gaplessInfoHolder.f3251a, gaplessInfoHolder.b, (List<byte[]>) null, (DrmInitData) null, 0, (String) null, (this.f3274a & 2) != 0 ? null : this.j));
        }
        return e(extractorInput);
    }

    private boolean a(ExtractorInput extractorInput, boolean z) throws IOException, InterruptedException {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int a2;
        Id3Decoder.FramePredicate framePredicate;
        int i7 = z ? Http2.INITIAL_MAX_FRAME_SIZE : 131072;
        extractorInput.a();
        if (extractorInput.getPosition() == 0) {
            if ((this.f3274a & 2) == 0) {
                framePredicate = null;
            } else {
                framePredicate = o;
            }
            this.j = this.f.a(extractorInput, framePredicate);
            Metadata metadata = this.j;
            if (metadata != null) {
                this.e.a(metadata);
            }
            int b2 = (int) extractorInput.b();
            if (!z) {
                extractorInput.c(b2);
            }
            i2 = b2;
            i5 = 0;
            i4 = 0;
            i3 = 0;
        } else {
            i5 = 0;
            i4 = 0;
            i3 = 0;
            i2 = 0;
        }
        while (true) {
            if (!d(extractorInput)) {
                this.c.e(0);
                int h2 = this.c.h();
                if ((i5 == 0 || a(h2, (long) i5)) && (a2 = MpegAudioHeader.a(h2)) != -1) {
                    i6 = i4 + 1;
                    if (i6 != 1) {
                        if (i6 == 4) {
                            break;
                        }
                    } else {
                        MpegAudioHeader.a(h2, this.d);
                        i5 = h2;
                    }
                    extractorInput.a(a2 - 4);
                } else {
                    int i8 = i3 + 1;
                    if (i3 != i7) {
                        if (z) {
                            extractorInput.a();
                            extractorInput.a(i2 + i8);
                        } else {
                            extractorInput.c(1);
                        }
                        i3 = i8;
                        i5 = 0;
                        i6 = 0;
                    } else if (z) {
                        return false;
                    } else {
                        throw new ParserException("Searched too many bytes.");
                    }
                }
            } else if (i4 <= 0) {
                throw new EOFException();
            }
        }
        if (z) {
            extractorInput.c(i2 + i3);
        } else {
            extractorInput.a();
        }
        this.i = i5;
        return true;
    }

    private static int a(ParsableByteArray parsableByteArray, int i2) {
        if (parsableByteArray.d() >= i2 + 4) {
            parsableByteArray.e(i2);
            int h2 = parsableByteArray.h();
            if (h2 == p || h2 == q) {
                return h2;
            }
        }
        if (parsableByteArray.d() < 40) {
            return 0;
        }
        parsableByteArray.e(36);
        int h3 = parsableByteArray.h();
        int i3 = r;
        if (h3 == i3) {
            return i3;
        }
        return 0;
    }

    private static MlltSeeker a(Metadata metadata, long j2) {
        if (metadata == null) {
            return null;
        }
        int a2 = metadata.a();
        for (int i2 = 0; i2 < a2; i2++) {
            Metadata.Entry a3 = metadata.a(i2);
            if (a3 instanceof MlltFrame) {
                return MlltSeeker.a(j2, (MlltFrame) a3);
            }
        }
        return null;
    }
}
