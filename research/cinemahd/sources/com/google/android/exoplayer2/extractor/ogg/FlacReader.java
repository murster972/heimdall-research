package com.google.android.exoplayer2.extractor.ogg;

import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.SeekPoint;
import com.google.android.exoplayer2.extractor.ogg.StreamReader;
import com.google.android.exoplayer2.util.FlacStreamInfo;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class FlacReader extends StreamReader {
    /* access modifiers changed from: private */
    public FlacStreamInfo n;
    private FlacOggSeeker o;

    FlacReader() {
    }

    private int b(ParsableByteArray parsableByteArray) {
        int i;
        int i2;
        int i3 = (parsableByteArray.f3641a[2] & 255) >> 4;
        switch (i3) {
            case 1:
                return JfifUtil.MARKER_SOFn;
            case 2:
            case 3:
            case 4:
            case 5:
                i = 576;
                i2 = i3 - 2;
                break;
            case 6:
            case 7:
                parsableByteArray.f(4);
                parsableByteArray.A();
                int t = i3 == 6 ? parsableByteArray.t() : parsableByteArray.z();
                parsableByteArray.e(0);
                return t + 1;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                i = 256;
                i2 = i3 - 8;
                break;
            default:
                return -1;
        }
        return i << i2;
    }

    public static boolean c(ParsableByteArray parsableByteArray) {
        return parsableByteArray.a() >= 5 && parsableByteArray.t() == 127 && parsableByteArray.v() == 1179402563;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        super.a(z);
        if (z) {
            this.n = null;
            this.o = null;
        }
    }

    private static boolean a(byte[] bArr) {
        return bArr[0] == -1;
    }

    /* access modifiers changed from: protected */
    public long a(ParsableByteArray parsableByteArray) {
        if (!a(parsableByteArray.f3641a)) {
            return -1;
        }
        return (long) b(parsableByteArray);
    }

    private class FlacOggSeeker implements OggSeeker, SeekMap {

        /* renamed from: a  reason: collision with root package name */
        private long[] f3304a;
        private long[] b;
        private long c = -1;
        private long d = -1;

        public FlacOggSeeker() {
        }

        public void a(ParsableByteArray parsableByteArray) {
            parsableByteArray.f(1);
            int w = parsableByteArray.w() / 18;
            this.f3304a = new long[w];
            this.b = new long[w];
            for (int i = 0; i < w; i++) {
                this.f3304a[i] = parsableByteArray.p();
                this.b[i] = parsableByteArray.p();
                parsableByteArray.f(2);
            }
        }

        public SeekMap.SeekPoints b(long j) {
            int b2 = Util.b(this.f3304a, FlacReader.this.b(j), true, true);
            long a2 = FlacReader.this.a(this.f3304a[b2]);
            SeekPoint seekPoint = new SeekPoint(a2, this.c + this.b[b2]);
            if (a2 < j) {
                long[] jArr = this.f3304a;
                if (b2 != jArr.length - 1) {
                    int i = b2 + 1;
                    return new SeekMap.SeekPoints(seekPoint, new SeekPoint(FlacReader.this.a(jArr[i]), this.c + this.b[i]));
                }
            }
            return new SeekMap.SeekPoints(seekPoint);
        }

        public boolean b() {
            return true;
        }

        public long c(long j) {
            long b2 = FlacReader.this.b(j);
            this.d = this.f3304a[Util.b(this.f3304a, b2, true, true)];
            return b2;
        }

        public SeekMap c() {
            return this;
        }

        public void d(long j) {
            this.c = j;
        }

        public long getDurationUs() {
            return FlacReader.this.n.b();
        }

        public long a(ExtractorInput extractorInput) throws IOException, InterruptedException {
            long j = this.d;
            if (j < 0) {
                return -1;
            }
            long j2 = -(j + 2);
            this.d = -1;
            return j2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(ParsableByteArray parsableByteArray, long j, StreamReader.SetupData setupData) throws IOException, InterruptedException {
        byte[] bArr = parsableByteArray.f3641a;
        if (this.n == null) {
            this.n = new FlacStreamInfo(bArr, 17);
            byte[] copyOfRange = Arrays.copyOfRange(bArr, 9, parsableByteArray.d());
            copyOfRange[4] = Byte.MIN_VALUE;
            List singletonList = Collections.singletonList(copyOfRange);
            int a2 = this.n.a();
            FlacStreamInfo flacStreamInfo = this.n;
            setupData.f3309a = Format.a((String) null, "audio/flac", (String) null, -1, a2, flacStreamInfo.b, flacStreamInfo.f3631a, (List<byte[]>) singletonList, (DrmInitData) null, 0, (String) null);
            return true;
        } else if ((bArr[0] & Byte.MAX_VALUE) == 3) {
            this.o = new FlacOggSeeker();
            this.o.a(parsableByteArray);
            return true;
        } else if (!a(bArr)) {
            return true;
        } else {
            FlacOggSeeker flacOggSeeker = this.o;
            if (flacOggSeeker != null) {
                flacOggSeeker.d(j);
                setupData.b = this.o;
            }
            return false;
        }
    }
}
