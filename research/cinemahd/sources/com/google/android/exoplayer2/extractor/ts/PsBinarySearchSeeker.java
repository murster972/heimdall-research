package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.extractor.BinarySearchSeeker;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

final class PsBinarySearchSeeker extends BinarySearchSeeker {

    private static final class PsScrSeeker implements BinarySearchSeeker.TimestampSeeker {

        /* renamed from: a  reason: collision with root package name */
        private final TimestampAdjuster f3335a;
        private final ParsableByteArray b;

        public BinarySearchSeeker.TimestampSearchResult a(ExtractorInput extractorInput, long j, BinarySearchSeeker.OutputFrameHolder outputFrameHolder) throws IOException, InterruptedException {
            long position = extractorInput.getPosition();
            int min = (int) Math.min(20000, extractorInput.getLength() - position);
            this.b.c(min);
            extractorInput.a(this.b.f3641a, 0, min);
            return a(this.b, j, position);
        }

        private PsScrSeeker(TimestampAdjuster timestampAdjuster) {
            this.f3335a = timestampAdjuster;
            this.b = new ParsableByteArray();
        }

        public void a() {
            this.b.a(Util.f);
        }

        private BinarySearchSeeker.TimestampSearchResult a(ParsableByteArray parsableByteArray, long j, long j2) {
            int i = -1;
            long j3 = -9223372036854775807L;
            int i2 = -1;
            while (parsableByteArray.a() >= 4) {
                if (PsBinarySearchSeeker.b(parsableByteArray.f3641a, parsableByteArray.c()) != 442) {
                    parsableByteArray.f(1);
                } else {
                    parsableByteArray.f(4);
                    long c = PsDurationReader.c(parsableByteArray);
                    if (c != -9223372036854775807L) {
                        long b2 = this.f3335a.b(c);
                        if (b2 > j) {
                            if (j3 == -9223372036854775807L) {
                                return BinarySearchSeeker.TimestampSearchResult.a(b2, j2);
                            }
                            return BinarySearchSeeker.TimestampSearchResult.a(j2 + ((long) i2));
                        } else if (100000 + b2 > j) {
                            return BinarySearchSeeker.TimestampSearchResult.a(j2 + ((long) parsableByteArray.c()));
                        } else {
                            i2 = parsableByteArray.c();
                            j3 = b2;
                        }
                    }
                    a(parsableByteArray);
                    i = parsableByteArray.c();
                }
            }
            if (j3 != -9223372036854775807L) {
                return BinarySearchSeeker.TimestampSearchResult.b(j3, j2 + ((long) i));
            }
            return BinarySearchSeeker.TimestampSearchResult.d;
        }

        private static void a(ParsableByteArray parsableByteArray) {
            int d = parsableByteArray.d();
            if (parsableByteArray.a() < 10) {
                parsableByteArray.e(d);
                return;
            }
            parsableByteArray.f(9);
            int t = parsableByteArray.t() & 7;
            if (parsableByteArray.a() < t) {
                parsableByteArray.e(d);
                return;
            }
            parsableByteArray.f(t);
            if (parsableByteArray.a() < 4) {
                parsableByteArray.e(d);
                return;
            }
            if (PsBinarySearchSeeker.b(parsableByteArray.f3641a, parsableByteArray.c()) == 443) {
                parsableByteArray.f(4);
                int z = parsableByteArray.z();
                if (parsableByteArray.a() < z) {
                    parsableByteArray.e(d);
                    return;
                }
                parsableByteArray.f(z);
            }
            while (parsableByteArray.a() >= 4 && (r1 = PsBinarySearchSeeker.b(parsableByteArray.f3641a, parsableByteArray.c())) != 442 && r1 != 441 && (r1 >>> 8) == 1) {
                parsableByteArray.f(4);
                if (parsableByteArray.a() < 2) {
                    parsableByteArray.e(d);
                    return;
                } else {
                    parsableByteArray.e(Math.min(parsableByteArray.d(), parsableByteArray.c() + parsableByteArray.z()));
                }
            }
        }
    }

    public PsBinarySearchSeeker(TimestampAdjuster timestampAdjuster, long j, long j2) {
        super(new BinarySearchSeeker.DefaultSeekTimestampConverter(), new PsScrSeeker(timestampAdjuster), j, 0, j + 1, 0, j2, 188, 1000);
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }
}
