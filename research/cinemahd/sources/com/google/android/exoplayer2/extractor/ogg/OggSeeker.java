package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.SeekMap;
import java.io.IOException;

interface OggSeeker {
    long a(ExtractorInput extractorInput) throws IOException, InterruptedException;

    long c(long j);

    SeekMap c();
}
