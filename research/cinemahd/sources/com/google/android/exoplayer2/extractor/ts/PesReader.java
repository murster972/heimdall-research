package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;

public final class PesReader implements TsPayloadReader {

    /* renamed from: a  reason: collision with root package name */
    private final ElementaryStreamReader f3334a;
    private final ParsableBitArray b = new ParsableBitArray(new byte[10]);
    private int c = 0;
    private int d;
    private TimestampAdjuster e;
    private boolean f;
    private boolean g;
    private boolean h;
    private int i;
    private int j;
    private boolean k;
    private long l;

    public PesReader(ElementaryStreamReader elementaryStreamReader) {
        this.f3334a = elementaryStreamReader;
    }

    private boolean b() {
        this.b.b(0);
        int a2 = this.b.a(24);
        if (a2 != 1) {
            Log.d("PesReader", "Unexpected start code prefix: " + a2);
            this.j = -1;
            return false;
        }
        this.b.c(8);
        int a3 = this.b.a(16);
        this.b.c(5);
        this.k = this.b.e();
        this.b.c(2);
        this.f = this.b.e();
        this.g = this.b.e();
        this.b.c(6);
        this.i = this.b.a(8);
        if (a3 == 0) {
            this.j = -1;
        } else {
            this.j = ((a3 + 6) - 9) - this.i;
        }
        return true;
    }

    private void c() {
        this.b.b(0);
        this.l = -9223372036854775807L;
        if (this.f) {
            this.b.c(4);
            this.b.c(1);
            this.b.c(1);
            long a2 = (((long) this.b.a(3)) << 30) | ((long) (this.b.a(15) << 15)) | ((long) this.b.a(15));
            this.b.c(1);
            if (!this.h && this.g) {
                this.b.c(4);
                this.b.c(1);
                this.b.c(1);
                this.b.c(1);
                this.e.b((((long) this.b.a(3)) << 30) | ((long) (this.b.a(15) << 15)) | ((long) this.b.a(15)));
                this.h = true;
            }
            this.l = this.e.b(a2);
        }
    }

    public void a(TimestampAdjuster timestampAdjuster, ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        this.e = timestampAdjuster;
        this.f3334a.a(extractorOutput, trackIdGenerator);
    }

    public final void a() {
        this.c = 0;
        this.d = 0;
        this.h = false;
        this.f3334a.a();
    }

    public final void a(ParsableByteArray parsableByteArray, boolean z) throws ParserException {
        if (z) {
            int i2 = this.c;
            if (!(i2 == 0 || i2 == 1)) {
                if (i2 == 2) {
                    Log.d("PesReader", "Unexpected start indicator reading extended header");
                } else if (i2 == 3) {
                    if (this.j != -1) {
                        Log.d("PesReader", "Unexpected start indicator: expected " + this.j + " more bytes");
                    }
                    this.f3334a.b();
                } else {
                    throw new IllegalStateException();
                }
            }
            a(1);
        }
        while (parsableByteArray.a() > 0) {
            int i3 = this.c;
            if (i3 != 0) {
                int i4 = 0;
                if (i3 != 1) {
                    if (i3 == 2) {
                        if (a(parsableByteArray, this.b.f3640a, Math.min(10, this.i)) && a(parsableByteArray, (byte[]) null, this.i)) {
                            c();
                            this.f3334a.a(this.l, this.k);
                            a(3);
                        }
                    } else if (i3 == 3) {
                        int a2 = parsableByteArray.a();
                        int i5 = this.j;
                        if (i5 != -1) {
                            i4 = a2 - i5;
                        }
                        if (i4 > 0) {
                            a2 -= i4;
                            parsableByteArray.d(parsableByteArray.c() + a2);
                        }
                        this.f3334a.a(parsableByteArray);
                        int i6 = this.j;
                        if (i6 != -1) {
                            this.j = i6 - a2;
                            if (this.j == 0) {
                                this.f3334a.b();
                                a(1);
                            }
                        }
                    } else {
                        throw new IllegalStateException();
                    }
                } else if (a(parsableByteArray, this.b.f3640a, 9)) {
                    if (b()) {
                        i4 = 2;
                    }
                    a(i4);
                }
            } else {
                parsableByteArray.f(parsableByteArray.a());
            }
        }
    }

    private void a(int i2) {
        this.c = i2;
        this.d = 0;
    }

    private boolean a(ParsableByteArray parsableByteArray, byte[] bArr, int i2) {
        int min = Math.min(parsableByteArray.a(), i2 - this.d);
        if (min <= 0) {
            return true;
        }
        if (bArr == null) {
            parsableByteArray.f(min);
        } else {
            parsableByteArray.a(bArr, this.d, min);
        }
        this.d += min;
        if (this.d == i2) {
            return true;
        }
        return false;
    }
}
