package com.google.android.exoplayer2.extractor.wav;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.Assertions;
import java.io.IOException;
import java.util.List;

public final class WavExtractor implements Extractor {

    /* renamed from: a  reason: collision with root package name */
    private ExtractorOutput f3355a;
    private TrackOutput b;
    private WavHeader c;
    private int d;
    private int e;

    static {
        a aVar = a.f3358a;
    }

    static /* synthetic */ Extractor[] a() {
        return new Extractor[]{new WavExtractor()};
    }

    public void release() {
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return WavHeaderReader.a(extractorInput) != null;
    }

    public void a(ExtractorOutput extractorOutput) {
        this.f3355a = extractorOutput;
        this.b = extractorOutput.a(0, 1);
        this.c = null;
        extractorOutput.a();
    }

    public void a(long j, long j2) {
        this.e = 0;
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        if (this.c == null) {
            this.c = WavHeaderReader.a(extractorInput);
            WavHeader wavHeader = this.c;
            if (wavHeader != null) {
                this.b.a(Format.a((String) null, "audio/raw", (String) null, wavHeader.c(), 32768, this.c.g(), this.c.h(), this.c.f(), (List<byte[]>) null, (DrmInitData) null, 0, (String) null));
                this.d = this.c.d();
            } else {
                throw new ParserException("Unsupported or unrecognized wav header.");
            }
        }
        if (!this.c.i()) {
            WavHeaderReader.a(extractorInput, this.c);
            this.f3355a.a(this.c);
        }
        long e2 = this.c.e();
        Assertions.b(e2 != -1);
        long position = e2 - extractorInput.getPosition();
        if (position <= 0) {
            return -1;
        }
        int a2 = this.b.a(extractorInput, (int) Math.min((long) (32768 - this.e), position), true);
        if (a2 != -1) {
            this.e += a2;
        }
        int i = this.e / this.d;
        if (i > 0) {
            long a3 = this.c.a(extractorInput.getPosition() - ((long) this.e));
            int i2 = i * this.d;
            this.e -= i2;
            this.b.a(a3, 1, i2, this.e, (TrackOutput.CryptoData) null);
        }
        if (a2 == -1) {
            return -1;
        }
        return 0;
    }
}
