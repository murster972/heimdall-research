package com.google.android.exoplayer2.extractor.mp4;

import com.facebook.common.util.UriUtil;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class Atom {
    public static final int A = Util.c("trun");
    public static final int A0 = Util.c("udta");
    public static final int B = Util.c("sidx");
    public static final int B0 = Util.c("meta");
    public static final int C = Util.c("moov");
    public static final int C0 = Util.c("ilst");
    public static final int D = Util.c("mvhd");
    public static final int D0 = Util.c("mean");
    public static final int E = Util.c("trak");
    public static final int E0 = Util.c(MediationMetaData.KEY_NAME);
    public static final int F = Util.c("mdia");
    public static final int F0 = Util.c(UriUtil.DATA_SCHEME);
    public static final int G = Util.c("minf");
    public static final int G0 = Util.c("emsg");
    public static final int H = Util.c("stbl");
    public static final int H0 = Util.c("st3d");
    public static final int I = Util.c("avcC");
    public static final int I0 = Util.c("sv3d");
    public static final int J = Util.c("hvcC");
    public static final int J0 = Util.c("proj");
    public static final int K = Util.c("esds");
    public static final int K0 = Util.c("vp08");
    public static final int L = Util.c("moof");
    public static final int L0 = Util.c("vp09");
    public static final int M = Util.c("traf");
    public static final int M0 = Util.c("vpcC");
    public static final int N = Util.c("mvex");
    public static final int N0 = Util.c("camm");
    public static final int O = Util.c("mehd");
    public static final int O0 = Util.c("alac");
    public static final int P = Util.c("tkhd");
    public static final int P0 = Util.c("alaw");
    public static final int Q = Util.c("edts");
    public static final int Q0 = Util.c("ulaw");
    public static final int R = Util.c("elst");
    public static final int R0 = Util.c("Opus");
    public static final int S = Util.c("mdhd");
    public static final int S0 = Util.c("dOps");
    public static final int T = Util.c("hdlr");
    public static final int T0 = Util.c("fLaC");
    public static final int U = Util.c("stsd");
    public static final int U0 = Util.c("dfLa");
    public static final int V = Util.c("pssh");
    public static final int W = Util.c("sinf");
    public static final int X = Util.c("schm");
    public static final int Y = Util.c("schi");
    public static final int Z = Util.c("tenc");
    public static final int a0 = Util.c("encv");
    public static final int b = Util.c("ftyp");
    public static final int b0 = Util.c("enca");
    public static final int c = Util.c("avc1");
    public static final int c0 = Util.c("frma");
    public static final int d = Util.c("avc3");
    public static final int d0 = Util.c("saiz");
    public static final int e = Util.c("hvc1");
    public static final int e0 = Util.c("saio");
    public static final int f = Util.c("hev1");
    public static final int f0 = Util.c("sbgp");
    public static final int g = Util.c("s263");
    public static final int g0 = Util.c("sgpd");
    public static final int h = Util.c("d263");
    public static final int h0 = Util.c("uuid");
    public static final int i = Util.c("mdat");
    public static final int i0 = Util.c("senc");
    public static final int j = Util.c("mp4a");
    public static final int j0 = Util.c("pasp");
    public static final int k = Util.c(".mp3");
    public static final int k0 = Util.c("TTML");
    public static final int l = Util.c("wave");
    public static final int l0 = Util.c("mp4v");
    public static final int m = Util.c("lpcm");
    public static final int m0 = Util.c("stts");
    public static final int n = Util.c("sowt");
    public static final int n0 = Util.c("stss");
    public static final int o = Util.c("ac-3");
    public static final int o0 = Util.c("ctts");
    public static final int p = Util.c("dac3");
    public static final int p0 = Util.c("stsc");
    public static final int q = Util.c("ec-3");
    public static final int q0 = Util.c("stsz");
    public static final int r = Util.c("dec3");
    public static final int r0 = Util.c("stz2");
    public static final int s = Util.c("dtsc");
    public static final int s0 = Util.c("stco");
    public static final int t = Util.c("dtsh");
    public static final int t0 = Util.c("co64");
    public static final int u = Util.c("dtsl");
    public static final int u0 = Util.c("tx3g");
    public static final int v = Util.c("dtse");
    public static final int v0 = Util.c("wvtt");
    public static final int w = Util.c("ddts");
    public static final int w0 = Util.c("stpp");
    public static final int x = Util.c("tfdt");
    public static final int x0 = Util.c("c608");
    public static final int y = Util.c("tfhd");
    public static final int y0 = Util.c("samr");
    public static final int z = Util.c("trex");
    public static final int z0 = Util.c("sawb");

    /* renamed from: a  reason: collision with root package name */
    public final int f3279a;

    static final class ContainerAtom extends Atom {
        public final long V0;
        public final List<LeafAtom> W0 = new ArrayList();
        public final List<ContainerAtom> X0 = new ArrayList();

        public ContainerAtom(int i, long j) {
            super(i);
            this.V0 = j;
        }

        public void a(LeafAtom leafAtom) {
            this.W0.add(leafAtom);
        }

        public ContainerAtom d(int i) {
            int size = this.X0.size();
            for (int i2 = 0; i2 < size; i2++) {
                ContainerAtom containerAtom = this.X0.get(i2);
                if (containerAtom.f3279a == i) {
                    return containerAtom;
                }
            }
            return null;
        }

        public LeafAtom e(int i) {
            int size = this.W0.size();
            for (int i2 = 0; i2 < size; i2++) {
                LeafAtom leafAtom = this.W0.get(i2);
                if (leafAtom.f3279a == i) {
                    return leafAtom;
                }
            }
            return null;
        }

        public String toString() {
            return Atom.a(this.f3279a) + " leaves: " + Arrays.toString(this.W0.toArray()) + " containers: " + Arrays.toString(this.X0.toArray());
        }

        public void a(ContainerAtom containerAtom) {
            this.X0.add(containerAtom);
        }
    }

    static final class LeafAtom extends Atom {
        public final ParsableByteArray V0;

        public LeafAtom(int i, ParsableByteArray parsableByteArray) {
            super(i);
            this.V0 = parsableByteArray;
        }
    }

    static {
        Util.c("vmhd");
    }

    public Atom(int i2) {
        this.f3279a = i2;
    }

    public static String a(int i2) {
        return "" + ((char) ((i2 >> 24) & JfifUtil.MARKER_FIRST_BYTE)) + ((char) ((i2 >> 16) & JfifUtil.MARKER_FIRST_BYTE)) + ((char) ((i2 >> 8) & JfifUtil.MARKER_FIRST_BYTE)) + ((char) (i2 & JfifUtil.MARKER_FIRST_BYTE));
    }

    public static int b(int i2) {
        return i2 & 16777215;
    }

    public static int c(int i2) {
        return (i2 >> 24) & JfifUtil.MARKER_FIRST_BYTE;
    }

    public String toString() {
        return a(this.f3279a);
    }
}
