package com.google.android.exoplayer2.extractor.wav;

import com.facebook.common.util.UriUtil;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.audio.WavUtil;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

final class WavHeaderReader {

    private static final class ChunkHeader {

        /* renamed from: a  reason: collision with root package name */
        public final int f3357a;
        public final long b;

        private ChunkHeader(int i, long j) {
            this.f3357a = i;
            this.b = j;
        }

        public static ChunkHeader a(ExtractorInput extractorInput, ParsableByteArray parsableByteArray) throws IOException, InterruptedException {
            extractorInput.a(parsableByteArray.f3641a, 0, 8);
            parsableByteArray.e(0);
            return new ChunkHeader(parsableByteArray.h(), parsableByteArray.m());
        }
    }

    private WavHeaderReader() {
    }

    public static WavHeader a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        Assertions.a(extractorInput);
        ParsableByteArray parsableByteArray = new ParsableByteArray(16);
        if (ChunkHeader.a(extractorInput, parsableByteArray).f3357a != WavUtil.f3206a) {
            return null;
        }
        extractorInput.a(parsableByteArray.f3641a, 0, 4);
        parsableByteArray.e(0);
        int h = parsableByteArray.h();
        if (h != WavUtil.b) {
            Log.b("WavHeaderReader", "Unsupported RIFF format: " + h);
            return null;
        }
        ChunkHeader a2 = ChunkHeader.a(extractorInput, parsableByteArray);
        while (a2.f3357a != WavUtil.c) {
            extractorInput.a((int) a2.b);
            a2 = ChunkHeader.a(extractorInput, parsableByteArray);
        }
        Assertions.b(a2.b >= 16);
        extractorInput.a(parsableByteArray.f3641a, 0, 16);
        parsableByteArray.e(0);
        int o = parsableByteArray.o();
        int o2 = parsableByteArray.o();
        int n = parsableByteArray.n();
        int n2 = parsableByteArray.n();
        int o3 = parsableByteArray.o();
        int o4 = parsableByteArray.o();
        int i = (o2 * o4) / 8;
        if (o3 == i) {
            int a3 = WavUtil.a(o, o4);
            if (a3 == 0) {
                Log.b("WavHeaderReader", "Unsupported WAV format: " + o4 + " bit/sample, type " + o);
                return null;
            }
            extractorInput.a(((int) a2.b) - 16);
            return new WavHeader(o2, n, n2, o3, o4, a3);
        }
        throw new ParserException("Expected block alignment: " + i + "; got: " + o3);
    }

    public static void a(ExtractorInput extractorInput, WavHeader wavHeader) throws IOException, InterruptedException {
        Assertions.a(extractorInput);
        Assertions.a(wavHeader);
        extractorInput.a();
        ParsableByteArray parsableByteArray = new ParsableByteArray(8);
        ChunkHeader a2 = ChunkHeader.a(extractorInput, parsableByteArray);
        while (a2.f3357a != Util.c(UriUtil.DATA_SCHEME)) {
            Log.d("WavHeaderReader", "Ignoring unknown WAV chunk: " + a2.f3357a);
            long j = a2.b + 8;
            if (a2.f3357a == Util.c("RIFF")) {
                j = 12;
            }
            if (j <= 2147483647L) {
                extractorInput.c((int) j);
                a2 = ChunkHeader.a(extractorInput, parsableByteArray);
            } else {
                throw new ParserException("Chunk is too large (~2GB+) to skip; id: " + a2.f3357a);
            }
        }
        extractorInput.c(8);
        wavHeader.a(extractorInput.getPosition(), a2.b);
    }
}
