package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;

public final class Id3Reader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3330a = new ParsableByteArray(10);
    private TrackOutput b;
    private boolean c;
    private long d;
    private int e;
    private int f;

    public void a() {
        this.c = false;
    }

    public void b() {
        int i;
        if (this.c && (i = this.e) != 0 && this.f == i) {
            this.b.a(this.d, 1, i, 0, (TrackOutput.CryptoData) null);
            this.c = false;
        }
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.b = extractorOutput.a(trackIdGenerator.c(), 4);
        this.b.a(Format.a(trackIdGenerator.b(), "application/id3", (String) null, -1, (DrmInitData) null));
    }

    public void a(long j, boolean z) {
        if (z) {
            this.c = true;
            this.d = j;
            this.e = 0;
            this.f = 0;
        }
    }

    public void a(ParsableByteArray parsableByteArray) {
        if (this.c) {
            int a2 = parsableByteArray.a();
            int i = this.f;
            if (i < 10) {
                int min = Math.min(a2, 10 - i);
                System.arraycopy(parsableByteArray.f3641a, parsableByteArray.c(), this.f3330a.f3641a, this.f, min);
                if (this.f + min == 10) {
                    this.f3330a.e(0);
                    if (73 == this.f3330a.t() && 68 == this.f3330a.t() && 51 == this.f3330a.t()) {
                        this.f3330a.f(3);
                        this.e = this.f3330a.s() + 10;
                    } else {
                        Log.d("Id3Reader", "Discarding invalid ID3 tag");
                        this.c = false;
                        return;
                    }
                }
            }
            int min2 = Math.min(a2, this.e - this.f);
            this.b.a(parsableByteArray, min2);
            this.f += min2;
        }
    }
}
