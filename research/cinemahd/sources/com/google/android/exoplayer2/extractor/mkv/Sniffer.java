package com.google.android.exoplayer2.extractor.mkv;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;

final class Sniffer {

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3270a = new ParsableByteArray(8);
    private int b;

    private long b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        int i = 0;
        extractorInput.a(this.f3270a.f3641a, 0, 1);
        byte b2 = this.f3270a.f3641a[0] & 255;
        if (b2 == 0) {
            return Long.MIN_VALUE;
        }
        int i2 = 128;
        int i3 = 0;
        while ((b2 & i2) == 0) {
            i2 >>= 1;
            i3++;
        }
        int i4 = b2 & (~i2);
        extractorInput.a(this.f3270a.f3641a, 1, i3);
        while (i < i3) {
            i++;
            i4 = (this.f3270a.f3641a[i] & 255) + (i4 << 8);
        }
        this.b += i3 + 1;
        return (long) i4;
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        long b2;
        int i;
        ExtractorInput extractorInput2 = extractorInput;
        long length = extractorInput.getLength();
        long j = 1024;
        int i2 = (length > -1 ? 1 : (length == -1 ? 0 : -1));
        if (i2 != 0 && length <= 1024) {
            j = length;
        }
        int i3 = (int) j;
        extractorInput2.a(this.f3270a.f3641a, 0, 4);
        this.b = 4;
        for (long v = this.f3270a.v(); v != 440786851; v = ((v << 8) & -256) | ((long) (this.f3270a.f3641a[0] & 255))) {
            int i4 = this.b + 1;
            this.b = i4;
            if (i4 == i3) {
                return false;
            }
            extractorInput2.a(this.f3270a.f3641a, 0, 1);
        }
        long b3 = b(extractorInput);
        long j2 = (long) this.b;
        if (b3 == Long.MIN_VALUE || (i2 != 0 && j2 + b3 >= length)) {
            return false;
        }
        while (true) {
            int i5 = this.b;
            long j3 = j2 + b3;
            if (((long) i5) < j3) {
                if (b(extractorInput) != Long.MIN_VALUE && b2 >= 0 && b2 <= 2147483647L) {
                    if (i != 0) {
                        int b4 = (int) (b2 = b(extractorInput));
                        extractorInput2.a(b4);
                        this.b += b4;
                    }
                }
            } else if (((long) i5) == j3) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
