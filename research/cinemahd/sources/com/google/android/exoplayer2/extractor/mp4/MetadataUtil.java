package com.google.android.exoplayer2.extractor.mp4;

import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.ApicFrame;
import com.google.android.exoplayer2.metadata.id3.CommentFrame;
import com.google.android.exoplayer2.metadata.id3.Id3Frame;
import com.google.android.exoplayer2.metadata.id3.InternalFrame;
import com.google.android.exoplayer2.metadata.id3.TextInformationFrame;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;

final class MetadataUtil {
    private static final int A = Util.c("sosn");
    private static final int B = Util.c("tvsh");
    private static final int C = Util.c("----");
    private static final String[] D = {"Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "AlternRock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta", "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk-Rock", "National Folk", "Swing", "Fast Fusion", "Bebob", "Latin", "Revival", "Celtic", "Bluegrass", "Avantgarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", "Terror", "Indie", "BritPop", "Negerpunk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", "Contemporary Christian", "Christian Rock", "Merengue", "Salsa", "Thrash Metal", "Anime", "Jpop", "Synthpop"};

    /* renamed from: a  reason: collision with root package name */
    private static final int f3291a = Util.c("nam");
    private static final int b = Util.c("trk");
    private static final int c = Util.c("cmt");
    private static final int d = Util.c("day");
    private static final int e = Util.c("ART");
    private static final int f = Util.c("too");
    private static final int g = Util.c("alb");
    private static final int h = Util.c("com");
    private static final int i = Util.c("wrt");
    private static final int j = Util.c("lyr");
    private static final int k = Util.c("gen");
    private static final int l = Util.c("covr");
    private static final int m = Util.c("gnre");
    private static final int n = Util.c("grp");
    private static final int o = Util.c("disk");
    private static final int p = Util.c("trkn");
    private static final int q = Util.c("tmpo");
    private static final int r = Util.c("cpil");
    private static final int s = Util.c("aART");
    private static final int t = Util.c("sonm");
    private static final int u = Util.c("soal");
    private static final int v = Util.c("soar");
    private static final int w = Util.c("soaa");
    private static final int x = Util.c("soco");
    private static final int y = Util.c("rtng");
    private static final int z = Util.c("pgap");

    private MetadataUtil() {
    }

    private static CommentFrame a(int i2, ParsableByteArray parsableByteArray) {
        int h2 = parsableByteArray.h();
        if (parsableByteArray.h() == Atom.F0) {
            parsableByteArray.f(8);
            String a2 = parsableByteArray.a(h2 - 16);
            return new CommentFrame("und", a2, a2);
        }
        Log.d("MetadataUtil", "Failed to parse comment attribute: " + Atom.a(i2));
        return null;
    }

    public static Metadata.Entry b(ParsableByteArray parsableByteArray) {
        int c2 = parsableByteArray.c() + parsableByteArray.h();
        int h2 = parsableByteArray.h();
        int i2 = (h2 >> 24) & JfifUtil.MARKER_FIRST_BYTE;
        if (i2 == 169 || i2 == 65533) {
            int i3 = 16777215 & h2;
            if (i3 == c) {
                CommentFrame a2 = a(h2, parsableByteArray);
                parsableByteArray.e(c2);
                return a2;
            }
            if (i3 != f3291a) {
                if (i3 != b) {
                    if (i3 != h) {
                        if (i3 != i) {
                            if (i3 == d) {
                                TextInformationFrame b2 = b(h2, "TDRC", parsableByteArray);
                                parsableByteArray.e(c2);
                                return b2;
                            } else if (i3 == e) {
                                TextInformationFrame b3 = b(h2, "TPE1", parsableByteArray);
                                parsableByteArray.e(c2);
                                return b3;
                            } else if (i3 == f) {
                                TextInformationFrame b4 = b(h2, "TSSE", parsableByteArray);
                                parsableByteArray.e(c2);
                                return b4;
                            } else if (i3 == g) {
                                TextInformationFrame b5 = b(h2, "TALB", parsableByteArray);
                                parsableByteArray.e(c2);
                                return b5;
                            } else if (i3 == j) {
                                TextInformationFrame b6 = b(h2, "USLT", parsableByteArray);
                                parsableByteArray.e(c2);
                                return b6;
                            } else if (i3 == k) {
                                TextInformationFrame b7 = b(h2, "TCON", parsableByteArray);
                                parsableByteArray.e(c2);
                                return b7;
                            } else if (i3 == n) {
                                TextInformationFrame b8 = b(h2, "TIT1", parsableByteArray);
                                parsableByteArray.e(c2);
                                return b8;
                            }
                        }
                    }
                    TextInformationFrame b9 = b(h2, "TCOM", parsableByteArray);
                    parsableByteArray.e(c2);
                    return b9;
                }
            }
            TextInformationFrame b10 = b(h2, "TIT2", parsableByteArray);
            parsableByteArray.e(c2);
            return b10;
        }
        try {
            if (h2 == m) {
                return c(parsableByteArray);
            }
            if (h2 == o) {
                TextInformationFrame a3 = a(h2, "TPOS", parsableByteArray);
                parsableByteArray.e(c2);
                return a3;
            } else if (h2 == p) {
                TextInformationFrame a4 = a(h2, "TRCK", parsableByteArray);
                parsableByteArray.e(c2);
                return a4;
            } else if (h2 == q) {
                Id3Frame a5 = a(h2, "TBPM", parsableByteArray, true, false);
                parsableByteArray.e(c2);
                return a5;
            } else if (h2 == r) {
                Id3Frame a6 = a(h2, "TCMP", parsableByteArray, true, true);
                parsableByteArray.e(c2);
                return a6;
            } else if (h2 == l) {
                ApicFrame a7 = a(parsableByteArray);
                parsableByteArray.e(c2);
                return a7;
            } else if (h2 == s) {
                TextInformationFrame b11 = b(h2, "TPE2", parsableByteArray);
                parsableByteArray.e(c2);
                return b11;
            } else if (h2 == t) {
                TextInformationFrame b12 = b(h2, "TSOT", parsableByteArray);
                parsableByteArray.e(c2);
                return b12;
            } else if (h2 == u) {
                TextInformationFrame b13 = b(h2, "TSO2", parsableByteArray);
                parsableByteArray.e(c2);
                return b13;
            } else if (h2 == v) {
                TextInformationFrame b14 = b(h2, "TSOA", parsableByteArray);
                parsableByteArray.e(c2);
                return b14;
            } else if (h2 == w) {
                TextInformationFrame b15 = b(h2, "TSOP", parsableByteArray);
                parsableByteArray.e(c2);
                return b15;
            } else if (h2 == x) {
                TextInformationFrame b16 = b(h2, "TSOC", parsableByteArray);
                parsableByteArray.e(c2);
                return b16;
            } else if (h2 == y) {
                Id3Frame a8 = a(h2, "ITUNESADVISORY", parsableByteArray, false, false);
                parsableByteArray.e(c2);
                return a8;
            } else if (h2 == z) {
                Id3Frame a9 = a(h2, "ITUNESGAPLESS", parsableByteArray, false, true);
                parsableByteArray.e(c2);
                return a9;
            } else if (h2 == A) {
                TextInformationFrame b17 = b(h2, "TVSHOWSORT", parsableByteArray);
                parsableByteArray.e(c2);
                return b17;
            } else if (h2 == B) {
                TextInformationFrame b18 = b(h2, "TVSHOW", parsableByteArray);
                parsableByteArray.e(c2);
                return b18;
            } else if (h2 == C) {
                Id3Frame a10 = a(parsableByteArray, c2);
                parsableByteArray.e(c2);
                return a10;
            }
        } finally {
            parsableByteArray.e(c2);
        }
        Log.a("MetadataUtil", "Skipped unknown metadata entry: " + Atom.a(h2));
        parsableByteArray.e(c2);
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.exoplayer2.metadata.id3.TextInformationFrame c(com.google.android.exoplayer2.util.ParsableByteArray r3) {
        /*
            int r3 = d(r3)
            r0 = 0
            if (r3 <= 0) goto L_0x0011
            java.lang.String[] r1 = D
            int r2 = r1.length
            if (r3 > r2) goto L_0x0011
            int r3 = r3 + -1
            r3 = r1[r3]
            goto L_0x0012
        L_0x0011:
            r3 = r0
        L_0x0012:
            if (r3 == 0) goto L_0x001c
            com.google.android.exoplayer2.metadata.id3.TextInformationFrame r1 = new com.google.android.exoplayer2.metadata.id3.TextInformationFrame
            java.lang.String r2 = "TCON"
            r1.<init>(r2, r0, r3)
            return r1
        L_0x001c:
            java.lang.String r3 = "MetadataUtil"
            java.lang.String r1 = "Failed to parse standard genre code"
            com.google.android.exoplayer2.util.Log.d(r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp4.MetadataUtil.c(com.google.android.exoplayer2.util.ParsableByteArray):com.google.android.exoplayer2.metadata.id3.TextInformationFrame");
    }

    private static int d(ParsableByteArray parsableByteArray) {
        parsableByteArray.f(4);
        if (parsableByteArray.h() == Atom.F0) {
            parsableByteArray.f(8);
            return parsableByteArray.t();
        }
        Log.d("MetadataUtil", "Failed to parse uint8 attribute value");
        return -1;
    }

    private static Id3Frame a(int i2, String str, ParsableByteArray parsableByteArray, boolean z2, boolean z3) {
        int d2 = d(parsableByteArray);
        if (z3) {
            d2 = Math.min(1, d2);
        }
        if (d2 < 0) {
            Log.d("MetadataUtil", "Failed to parse uint8 attribute: " + Atom.a(i2));
            return null;
        } else if (z2) {
            return new TextInformationFrame(str, (String) null, Integer.toString(d2));
        } else {
            return new CommentFrame("und", str, Integer.toString(d2));
        }
    }

    private static TextInformationFrame a(int i2, String str, ParsableByteArray parsableByteArray) {
        int h2 = parsableByteArray.h();
        if (parsableByteArray.h() == Atom.F0 && h2 >= 22) {
            parsableByteArray.f(10);
            int z2 = parsableByteArray.z();
            if (z2 > 0) {
                String str2 = "" + z2;
                int z3 = parsableByteArray.z();
                if (z3 > 0) {
                    str2 = str2 + "/" + z3;
                }
                return new TextInformationFrame(str, (String) null, str2);
            }
        }
        Log.d("MetadataUtil", "Failed to parse index/count attribute: " + Atom.a(i2));
        return null;
    }

    private static ApicFrame a(ParsableByteArray parsableByteArray) {
        int h2 = parsableByteArray.h();
        if (parsableByteArray.h() == Atom.F0) {
            int b2 = Atom.b(parsableByteArray.h());
            String str = b2 == 13 ? "image/jpeg" : b2 == 14 ? "image/png" : null;
            if (str == null) {
                Log.d("MetadataUtil", "Unrecognized cover art flags: " + b2);
                return null;
            }
            parsableByteArray.f(4);
            byte[] bArr = new byte[(h2 - 16)];
            parsableByteArray.a(bArr, 0, bArr.length);
            return new ApicFrame(str, (String) null, 3, bArr);
        }
        Log.d("MetadataUtil", "Failed to parse cover art attribute");
        return null;
    }

    private static Id3Frame a(ParsableByteArray parsableByteArray, int i2) {
        String str = null;
        String str2 = null;
        int i3 = -1;
        int i4 = -1;
        while (parsableByteArray.c() < i2) {
            int c2 = parsableByteArray.c();
            int h2 = parsableByteArray.h();
            int h3 = parsableByteArray.h();
            parsableByteArray.f(4);
            if (h3 == Atom.D0) {
                str = parsableByteArray.a(h2 - 12);
            } else if (h3 == Atom.E0) {
                str2 = parsableByteArray.a(h2 - 12);
            } else {
                if (h3 == Atom.F0) {
                    i3 = c2;
                    i4 = h2;
                }
                parsableByteArray.f(h2 - 12);
            }
        }
        if (str == null || str2 == null || i3 == -1) {
            return null;
        }
        parsableByteArray.e(i3);
        parsableByteArray.f(16);
        return new InternalFrame(str, str2, parsableByteArray.a(i4 - 16));
    }

    private static TextInformationFrame b(int i2, String str, ParsableByteArray parsableByteArray) {
        int h2 = parsableByteArray.h();
        if (parsableByteArray.h() == Atom.F0) {
            parsableByteArray.f(8);
            return new TextInformationFrame(str, (String) null, parsableByteArray.a(h2 - 16));
        }
        Log.d("MetadataUtil", "Failed to parse text attribute: " + Atom.a(i2));
        return null;
    }
}
