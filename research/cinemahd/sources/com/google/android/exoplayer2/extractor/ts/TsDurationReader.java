package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

final class TsDurationReader {

    /* renamed from: a  reason: collision with root package name */
    private final TimestampAdjuster f3343a = new TimestampAdjuster(0);
    private final ParsableByteArray b = new ParsableByteArray();
    private boolean c;
    private boolean d;
    private boolean e;
    private long f = -9223372036854775807L;
    private long g = -9223372036854775807L;
    private long h = -9223372036854775807L;

    TsDurationReader() {
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder, int i) throws IOException, InterruptedException {
        if (i <= 0) {
            return a(extractorInput);
        }
        if (!this.e) {
            return c(extractorInput, positionHolder, i);
        }
        if (this.g == -9223372036854775807L) {
            return a(extractorInput);
        }
        if (!this.d) {
            return b(extractorInput, positionHolder, i);
        }
        long j = this.f;
        if (j == -9223372036854775807L) {
            return a(extractorInput);
        }
        this.h = this.f3343a.b(this.g) - this.f3343a.b(j);
        return a(extractorInput);
    }

    public TimestampAdjuster b() {
        return this.f3343a;
    }

    public boolean c() {
        return this.c;
    }

    private int b(ExtractorInput extractorInput, PositionHolder positionHolder, int i) throws IOException, InterruptedException {
        int min = (int) Math.min(112800, extractorInput.getLength());
        long j = (long) 0;
        if (extractorInput.getPosition() != j) {
            positionHolder.f3254a = j;
            return 1;
        }
        this.b.c(min);
        extractorInput.a();
        extractorInput.a(this.b.f3641a, 0, min);
        this.f = a(this.b, i);
        this.d = true;
        return 0;
    }

    private int c(ExtractorInput extractorInput, PositionHolder positionHolder, int i) throws IOException, InterruptedException {
        long length = extractorInput.getLength();
        int min = (int) Math.min(112800, length);
        long j = length - ((long) min);
        if (extractorInput.getPosition() != j) {
            positionHolder.f3254a = j;
            return 1;
        }
        this.b.c(min);
        extractorInput.a();
        extractorInput.a(this.b.f3641a, 0, min);
        this.g = b(this.b, i);
        this.e = true;
        return 0;
    }

    private long b(ParsableByteArray parsableByteArray, int i) {
        int c2 = parsableByteArray.c();
        int d2 = parsableByteArray.d();
        while (true) {
            d2--;
            if (d2 < c2) {
                return -9223372036854775807L;
            }
            if (parsableByteArray.f3641a[d2] == 71) {
                long a2 = TsUtil.a(parsableByteArray, d2, i);
                if (a2 != -9223372036854775807L) {
                    return a2;
                }
            }
        }
    }

    public long a() {
        return this.h;
    }

    private int a(ExtractorInput extractorInput) {
        this.b.a(Util.f);
        this.c = true;
        extractorInput.a();
        return 0;
    }

    private long a(ParsableByteArray parsableByteArray, int i) {
        int d2 = parsableByteArray.d();
        for (int c2 = parsableByteArray.c(); c2 < d2; c2++) {
            if (parsableByteArray.f3641a[c2] == 71) {
                long a2 = TsUtil.a(parsableByteArray, c2, i);
                if (a2 != -9223372036854775807L) {
                    return a2;
                }
            }
        }
        return -9223372036854775807L;
    }
}
