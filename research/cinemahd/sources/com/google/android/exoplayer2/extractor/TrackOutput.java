package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;
import java.util.Arrays;

public interface TrackOutput {

    public static final class CryptoData {

        /* renamed from: a  reason: collision with root package name */
        public final int f3258a;
        public final byte[] b;
        public final int c;
        public final int d;

        public CryptoData(int i, byte[] bArr, int i2, int i3) {
            this.f3258a = i;
            this.b = bArr;
            this.c = i2;
            this.d = i3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || CryptoData.class != obj.getClass()) {
                return false;
            }
            CryptoData cryptoData = (CryptoData) obj;
            if (this.f3258a == cryptoData.f3258a && this.c == cryptoData.c && this.d == cryptoData.d && Arrays.equals(this.b, cryptoData.b)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((((this.f3258a * 31) + Arrays.hashCode(this.b)) * 31) + this.c) * 31) + this.d;
        }
    }

    int a(ExtractorInput extractorInput, int i, boolean z) throws IOException, InterruptedException;

    void a(long j, int i, int i2, int i3, CryptoData cryptoData);

    void a(Format format);

    void a(ParsableByteArray parsableByteArray, int i);
}
