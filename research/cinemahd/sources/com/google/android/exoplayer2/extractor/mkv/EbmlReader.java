package com.google.android.exoplayer2.extractor.mkv;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import java.io.IOException;

interface EbmlReader {
    void a(EbmlReaderOutput ebmlReaderOutput);

    boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException;

    void reset();
}
