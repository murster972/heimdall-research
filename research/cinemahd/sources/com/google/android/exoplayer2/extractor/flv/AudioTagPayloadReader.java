package com.google.android.exoplayer2.extractor.flv;

import android.util.Pair;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.flv.TagPayloadReader;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.Collections;
import java.util.List;

final class AudioTagPayloadReader extends TagPayloadReader {
    private static final int[] e = {5512, 11025, 22050, 44100};
    private boolean b;
    private boolean c;
    private int d;

    public AudioTagPayloadReader(TrackOutput trackOutput) {
        super(trackOutput);
    }

    /* access modifiers changed from: protected */
    public boolean a(ParsableByteArray parsableByteArray) throws TagPayloadReader.UnsupportedFormatException {
        if (!this.b) {
            int t = parsableByteArray.t();
            this.d = (t >> 4) & 15;
            int i = this.d;
            if (i == 2) {
                this.f3262a.a(Format.a((String) null, "audio/mpeg", (String) null, -1, -1, 1, e[(t >> 2) & 3], (List<byte[]>) null, (DrmInitData) null, 0, (String) null));
                this.c = true;
            } else if (i == 7 || i == 8) {
                this.f3262a.a(Format.a((String) null, this.d == 7 ? "audio/g711-alaw" : "audio/g711-mlaw", (String) null, -1, -1, 1, 8000, (t & 1) == 1 ? 2 : 3, (List<byte[]>) null, (DrmInitData) null, 0, (String) null));
                this.c = true;
            } else if (i != 10) {
                throw new TagPayloadReader.UnsupportedFormatException("Audio format not supported: " + this.d);
            }
            this.b = true;
        } else {
            parsableByteArray.f(1);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void b(ParsableByteArray parsableByteArray, long j) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        if (this.d == 2) {
            int a2 = parsableByteArray.a();
            this.f3262a.a(parsableByteArray2, a2);
            this.f3262a.a(j, 1, a2, 0, (TrackOutput.CryptoData) null);
            return;
        }
        int t = parsableByteArray.t();
        if (t == 0 && !this.c) {
            byte[] bArr = new byte[parsableByteArray.a()];
            parsableByteArray2.a(bArr, 0, bArr.length);
            Pair<Integer, Integer> a3 = CodecSpecificDataUtil.a(bArr);
            this.f3262a.a(Format.a((String) null, "audio/mp4a-latm", (String) null, -1, -1, ((Integer) a3.second).intValue(), ((Integer) a3.first).intValue(), (List<byte[]>) Collections.singletonList(bArr), (DrmInitData) null, 0, (String) null));
            this.c = true;
        } else if (this.d != 10 || t == 1) {
            int a4 = parsableByteArray.a();
            this.f3262a.a(parsableByteArray2, a4);
            this.f3262a.a(j, 1, a4, 0, (TrackOutput.CryptoData) null);
        }
    }
}
