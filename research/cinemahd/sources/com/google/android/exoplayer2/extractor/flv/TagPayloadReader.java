package com.google.android.exoplayer2.extractor.flv;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.ParsableByteArray;

abstract class TagPayloadReader {

    /* renamed from: a  reason: collision with root package name */
    protected final TrackOutput f3262a;

    public static final class UnsupportedFormatException extends ParserException {
        public UnsupportedFormatException(String str) {
            super(str);
        }
    }

    protected TagPayloadReader(TrackOutput trackOutput) {
        this.f3262a = trackOutput;
    }

    public final void a(ParsableByteArray parsableByteArray, long j) throws ParserException {
        if (a(parsableByteArray)) {
            b(parsableByteArray, j);
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(ParsableByteArray parsableByteArray) throws ParserException;

    /* access modifiers changed from: protected */
    public abstract void b(ParsableByteArray parsableByteArray, long j) throws ParserException;
}
