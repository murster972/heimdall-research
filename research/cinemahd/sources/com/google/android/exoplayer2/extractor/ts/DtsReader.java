package com.google.android.exoplayer2.extractor.ts;

import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.audio.DtsUtil;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableByteArray;

public final class DtsReader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3321a = new ParsableByteArray(new byte[18]);
    private final String b;
    private String c;
    private TrackOutput d;
    private int e = 0;
    private int f;
    private int g;
    private long h;
    private Format i;
    private int j;
    private long k;

    public DtsReader(String str) {
        this.b = str;
    }

    private boolean b(ParsableByteArray parsableByteArray) {
        while (parsableByteArray.a() > 0) {
            this.g <<= 8;
            this.g |= parsableByteArray.t();
            if (DtsUtil.a(this.g)) {
                byte[] bArr = this.f3321a.f3641a;
                int i2 = this.g;
                bArr[0] = (byte) ((i2 >> 24) & JfifUtil.MARKER_FIRST_BYTE);
                bArr[1] = (byte) ((i2 >> 16) & JfifUtil.MARKER_FIRST_BYTE);
                bArr[2] = (byte) ((i2 >> 8) & JfifUtil.MARKER_FIRST_BYTE);
                bArr[3] = (byte) (i2 & JfifUtil.MARKER_FIRST_BYTE);
                this.f = 4;
                this.g = 0;
                return true;
            }
        }
        return false;
    }

    private void c() {
        byte[] bArr = this.f3321a.f3641a;
        if (this.i == null) {
            this.i = DtsUtil.a(bArr, this.c, this.b, (DrmInitData) null);
            this.d.a(this.i);
        }
        this.j = DtsUtil.a(bArr);
        this.h = (long) ((int) ((((long) DtsUtil.d(bArr)) * 1000000) / ((long) this.i.u)));
    }

    public void a() {
        this.e = 0;
        this.f = 0;
        this.g = 0;
    }

    public void b() {
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.c = trackIdGenerator.b();
        this.d = extractorOutput.a(trackIdGenerator.c(), 1);
    }

    public void a(long j2, boolean z) {
        this.k = j2;
    }

    public void a(ParsableByteArray parsableByteArray) {
        while (parsableByteArray.a() > 0) {
            int i2 = this.e;
            if (i2 != 0) {
                if (i2 != 1) {
                    if (i2 == 2) {
                        int min = Math.min(parsableByteArray.a(), this.j - this.f);
                        this.d.a(parsableByteArray, min);
                        this.f += min;
                        int i3 = this.f;
                        int i4 = this.j;
                        if (i3 == i4) {
                            this.d.a(this.k, 1, i4, 0, (TrackOutput.CryptoData) null);
                            this.k += this.h;
                            this.e = 0;
                        }
                    } else {
                        throw new IllegalStateException();
                    }
                } else if (a(parsableByteArray, this.f3321a.f3641a, 18)) {
                    c();
                    this.f3321a.e(0);
                    this.d.a(this.f3321a, 18);
                    this.e = 2;
                }
            } else if (b(parsableByteArray)) {
                this.e = 1;
            }
        }
    }

    private boolean a(ParsableByteArray parsableByteArray, byte[] bArr, int i2) {
        int min = Math.min(parsableByteArray.a(), i2 - this.f);
        parsableByteArray.a(bArr, this.f, min);
        this.f += min;
        return this.f == i2;
    }
}
