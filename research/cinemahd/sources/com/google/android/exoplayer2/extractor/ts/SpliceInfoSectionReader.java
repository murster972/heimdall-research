package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;

public final class SpliceInfoSectionReader implements SectionPayloadReader {

    /* renamed from: a  reason: collision with root package name */
    private TimestampAdjuster f3341a;
    private TrackOutput b;
    private boolean c;

    public void a(TimestampAdjuster timestampAdjuster, ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        this.f3341a = timestampAdjuster;
        trackIdGenerator.a();
        this.b = extractorOutput.a(trackIdGenerator.c(), 4);
        this.b.a(Format.a(trackIdGenerator.b(), "application/x-scte35", (String) null, -1, (DrmInitData) null));
    }

    public void a(ParsableByteArray parsableByteArray) {
        if (!this.c) {
            if (this.f3341a.c() != -9223372036854775807L) {
                this.b.a(Format.a((String) null, "application/x-scte35", this.f3341a.c()));
                this.c = true;
            } else {
                return;
            }
        }
        int a2 = parsableByteArray.a();
        this.b.a(parsableByteArray, a2);
        this.b.a(this.f3341a.b(), 1, a2, 0, (TrackOutput.CryptoData) null);
    }
}
