package com.google.android.exoplayer2.extractor.mkv;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import java.io.IOException;

interface EbmlReaderOutput {
    void a(int i) throws ParserException;

    void a(int i, double d) throws ParserException;

    void a(int i, int i2, ExtractorInput extractorInput) throws IOException, InterruptedException;

    void a(int i, long j) throws ParserException;

    void a(int i, long j, long j2) throws ParserException;

    void a(int i, String str) throws ParserException;

    int b(int i);

    boolean c(int i);
}
