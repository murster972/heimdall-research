package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.Collections;
import java.util.List;

public final class DvbSubtitleReader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final List<TsPayloadReader.DvbSubtitleInfo> f3322a;
    private final TrackOutput[] b;
    private boolean c;
    private int d;
    private int e;
    private long f;

    public DvbSubtitleReader(List<TsPayloadReader.DvbSubtitleInfo> list) {
        this.f3322a = list;
        this.b = new TrackOutput[list.size()];
    }

    public void a() {
        this.c = false;
    }

    public void b() {
        if (this.c) {
            for (TrackOutput a2 : this.b) {
                a2.a(this.f, 1, this.e, 0, (TrackOutput.CryptoData) null);
            }
            this.c = false;
        }
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        for (int i = 0; i < this.b.length; i++) {
            TsPayloadReader.DvbSubtitleInfo dvbSubtitleInfo = this.f3322a.get(i);
            trackIdGenerator.a();
            TrackOutput a2 = extractorOutput.a(trackIdGenerator.c(), 3);
            a2.a(Format.a(trackIdGenerator.b(), "application/dvbsubs", (String) null, -1, 0, (List<byte[]>) Collections.singletonList(dvbSubtitleInfo.b), dvbSubtitleInfo.f3347a, (DrmInitData) null));
            this.b[i] = a2;
        }
    }

    public void a(long j, boolean z) {
        if (z) {
            this.c = true;
            this.f = j;
            this.e = 0;
            this.d = 2;
        }
    }

    public void a(ParsableByteArray parsableByteArray) {
        if (!this.c) {
            return;
        }
        if (this.d != 2 || a(parsableByteArray, 32)) {
            if (this.d != 1 || a(parsableByteArray, 0)) {
                int c2 = parsableByteArray.c();
                int a2 = parsableByteArray.a();
                for (TrackOutput a3 : this.b) {
                    parsableByteArray.e(c2);
                    a3.a(parsableByteArray, a2);
                }
                this.e += a2;
            }
        }
    }

    private boolean a(ParsableByteArray parsableByteArray, int i) {
        if (parsableByteArray.a() == 0) {
            return false;
        }
        if (parsableByteArray.t() != i) {
            this.c = false;
        }
        this.d--;
        return this.c;
    }
}
