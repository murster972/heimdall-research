package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.audio.Ac3Util;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.List;

public final class Ac3Reader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final ParsableBitArray f3317a;
    private final ParsableByteArray b;
    private final String c;
    private String d;
    private TrackOutput e;
    private int f;
    private int g;
    private boolean h;
    private long i;
    private Format j;
    private int k;
    private long l;

    public Ac3Reader() {
        this((String) null);
    }

    private boolean b(ParsableByteArray parsableByteArray) {
        while (true) {
            boolean z = false;
            if (parsableByteArray.a() <= 0) {
                return false;
            }
            if (!this.h) {
                if (parsableByteArray.t() == 11) {
                    z = true;
                }
                this.h = z;
            } else {
                int t = parsableByteArray.t();
                if (t == 119) {
                    this.h = false;
                    return true;
                }
                if (t == 11) {
                    z = true;
                }
                this.h = z;
            }
        }
    }

    private void c() {
        this.f3317a.b(0);
        Ac3Util.SyncFrameInfo a2 = Ac3Util.a(this.f3317a);
        Format format = this.j;
        if (!(format != null && a2.c == format.t && a2.b == format.u && a2.f3184a == format.g)) {
            this.j = Format.a(this.d, a2.f3184a, (String) null, -1, -1, a2.c, a2.b, (List<byte[]>) null, (DrmInitData) null, 0, this.c);
            this.e.a(this.j);
        }
        this.k = a2.d;
        this.i = (((long) a2.e) * 1000000) / ((long) this.j.u);
    }

    public void a() {
        this.f = 0;
        this.g = 0;
        this.h = false;
    }

    public void b() {
    }

    public Ac3Reader(String str) {
        this.f3317a = new ParsableBitArray(new byte[128]);
        this.b = new ParsableByteArray(this.f3317a.f3640a);
        this.f = 0;
        this.c = str;
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.d = trackIdGenerator.b();
        this.e = extractorOutput.a(trackIdGenerator.c(), 1);
    }

    public void a(long j2, boolean z) {
        this.l = j2;
    }

    public void a(ParsableByteArray parsableByteArray) {
        while (parsableByteArray.a() > 0) {
            int i2 = this.f;
            if (i2 != 0) {
                if (i2 != 1) {
                    if (i2 == 2) {
                        int min = Math.min(parsableByteArray.a(), this.k - this.g);
                        this.e.a(parsableByteArray, min);
                        this.g += min;
                        int i3 = this.g;
                        int i4 = this.k;
                        if (i3 == i4) {
                            this.e.a(this.l, 1, i4, 0, (TrackOutput.CryptoData) null);
                            this.l += this.i;
                            this.f = 0;
                        }
                    }
                } else if (a(parsableByteArray, this.b.f3641a, 128)) {
                    c();
                    this.b.e(0);
                    this.e.a(this.b, 128);
                    this.f = 2;
                }
            } else if (b(parsableByteArray)) {
                this.f = 1;
                byte[] bArr = this.b.f3641a;
                bArr[0] = 11;
                bArr[1] = 119;
                this.g = 2;
            }
        }
    }

    private boolean a(ParsableByteArray parsableByteArray, byte[] bArr, int i2) {
        int min = Math.min(parsableByteArray.a(), i2 - this.g);
        parsableByteArray.a(bArr, this.g, min);
        this.g += min;
        return this.g == i2;
    }
}
