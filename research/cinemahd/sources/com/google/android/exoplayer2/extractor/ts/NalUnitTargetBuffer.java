package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.util.Assertions;
import java.util.Arrays;

final class NalUnitTargetBuffer {

    /* renamed from: a  reason: collision with root package name */
    private final int f3333a;
    private boolean b;
    private boolean c;
    public byte[] d;
    public int e;

    public NalUnitTargetBuffer(int i, int i2) {
        this.f3333a = i;
        this.d = new byte[(i2 + 3)];
        this.d[2] = 1;
    }

    public boolean a() {
        return this.c;
    }

    public void b() {
        this.b = false;
        this.c = false;
    }

    public void a(byte[] bArr, int i, int i2) {
        if (this.b) {
            int i3 = i2 - i;
            byte[] bArr2 = this.d;
            int length = bArr2.length;
            int i4 = this.e;
            if (length < i4 + i3) {
                this.d = Arrays.copyOf(bArr2, (i4 + i3) * 2);
            }
            System.arraycopy(bArr, i, this.d, this.e, i3);
            this.e += i3;
        }
    }

    public void b(int i) {
        boolean z = true;
        Assertions.b(!this.b);
        if (i != this.f3333a) {
            z = false;
        }
        this.b = z;
        if (this.b) {
            this.e = 3;
            this.c = false;
        }
    }

    public boolean a(int i) {
        if (!this.b) {
            return false;
        }
        this.e -= i;
        this.b = false;
        this.c = true;
        return true;
    }
}
