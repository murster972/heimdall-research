package com.google.android.exoplayer2.extractor.flv;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public final class FlvExtractor implements Extractor {
    private static final int p = Util.c("FLV");

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3261a = new ParsableByteArray(4);
    private final ParsableByteArray b = new ParsableByteArray(9);
    private final ParsableByteArray c = new ParsableByteArray(11);
    private final ParsableByteArray d = new ParsableByteArray();
    private final ScriptTagPayloadReader e = new ScriptTagPayloadReader();
    private ExtractorOutput f;
    private int g = 1;
    private long h = -9223372036854775807L;
    private int i;
    private int j;
    private int k;
    private long l;
    private boolean m;
    private AudioTagPayloadReader n;
    private VideoTagPayloadReader o;

    static {
        a aVar = a.f3263a;
    }

    static /* synthetic */ Extractor[] b() {
        return new Extractor[]{new FlvExtractor()};
    }

    private boolean c(ExtractorInput extractorInput) throws IOException, InterruptedException {
        boolean z = false;
        if (!extractorInput.b(this.b.f3641a, 0, 9, true)) {
            return false;
        }
        this.b.e(0);
        this.b.f(4);
        int t = this.b.t();
        boolean z2 = (t & 4) != 0;
        if ((t & 1) != 0) {
            z = true;
        }
        if (z2 && this.n == null) {
            this.n = new AudioTagPayloadReader(this.f.a(8, 1));
        }
        if (z && this.o == null) {
            this.o = new VideoTagPayloadReader(this.f.a(9, 2));
        }
        this.f.a();
        this.i = (this.b.h() - 9) + 4;
        this.g = 2;
        return true;
    }

    private boolean d(ExtractorInput extractorInput) throws IOException, InterruptedException {
        boolean z = true;
        if (this.j == 8 && this.n != null) {
            a();
            this.n.a(b(extractorInput), this.h + this.l);
        } else if (this.j == 9 && this.o != null) {
            a();
            this.o.a(b(extractorInput), this.h + this.l);
        } else if (this.j != 18 || this.m) {
            extractorInput.c(this.k);
            z = false;
        } else {
            this.e.a(b(extractorInput), this.l);
            long a2 = this.e.a();
            if (a2 != -9223372036854775807L) {
                this.f.a(new SeekMap.Unseekable(a2));
                this.m = true;
            }
        }
        this.i = 4;
        this.g = 2;
        return z;
    }

    private boolean e(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (!extractorInput.b(this.c.f3641a, 0, 11, true)) {
            return false;
        }
        this.c.e(0);
        this.j = this.c.t();
        this.k = this.c.w();
        this.l = (long) this.c.w();
        this.l = (((long) (this.c.t() << 24)) | this.l) * 1000;
        this.c.f(3);
        this.g = 4;
        return true;
    }

    private void f(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.c(this.i);
        this.i = 0;
        this.g = 3;
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.a(this.f3261a.f3641a, 0, 3);
        this.f3261a.e(0);
        if (this.f3261a.w() != p) {
            return false;
        }
        extractorInput.a(this.f3261a.f3641a, 0, 2);
        this.f3261a.e(0);
        if ((this.f3261a.z() & 250) != 0) {
            return false;
        }
        extractorInput.a(this.f3261a.f3641a, 0, 4);
        this.f3261a.e(0);
        int h2 = this.f3261a.h();
        extractorInput.a();
        extractorInput.a(h2);
        extractorInput.a(this.f3261a.f3641a, 0, 4);
        this.f3261a.e(0);
        if (this.f3261a.h() == 0) {
            return true;
        }
        return false;
    }

    public void release() {
    }

    private ParsableByteArray b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (this.k > this.d.b()) {
            ParsableByteArray parsableByteArray = this.d;
            parsableByteArray.a(new byte[Math.max(parsableByteArray.b() * 2, this.k)], 0);
        } else {
            this.d.e(0);
        }
        this.d.d(this.k);
        extractorInput.readFully(this.d.f3641a, 0, this.k);
        return this.d;
    }

    public void a(ExtractorOutput extractorOutput) {
        this.f = extractorOutput;
    }

    public void a(long j2, long j3) {
        this.g = 1;
        this.h = -9223372036854775807L;
        this.i = 0;
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        while (true) {
            int i2 = this.g;
            if (i2 != 1) {
                if (i2 == 2) {
                    f(extractorInput);
                } else if (i2 != 3) {
                    if (i2 != 4) {
                        throw new IllegalStateException();
                    } else if (d(extractorInput)) {
                        return 0;
                    }
                } else if (!e(extractorInput)) {
                    return -1;
                }
            } else if (!c(extractorInput)) {
                return -1;
            }
        }
    }

    private void a() {
        if (!this.m) {
            this.f.a(new SeekMap.Unseekable(-9223372036854775807L));
            this.m = true;
        }
        if (this.h == -9223372036854775807L) {
            this.h = this.e.a() == -9223372036854775807L ? -this.l : 0;
        }
    }
}
