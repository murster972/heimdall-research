package com.google.android.exoplayer2.extractor.mkv;

import android.util.Pair;
import android.util.SparseArray;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.audio.Ac3Util;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ChunkIndex;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.LongArray;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import com.original.tase.model.socket.UserResponces;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.joda.time.DateTimeConstants;

public final class MatroskaExtractor implements Extractor {
    private static final byte[] Z = {49, 10, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 10};
    private static final byte[] a0 = {32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32};
    /* access modifiers changed from: private */
    public static final byte[] b0 = Util.d("Format: Start, End, ReadOrder, Layer, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
    private static final byte[] c0 = {68, 105, 97, 108, 111, 103, 117, 101, 58, 32, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44};
    private static final byte[] d0 = {32, 32, 32, 32, 32, 32, 32, 32, 32, 32};
    /* access modifiers changed from: private */
    public static final UUID e0 = new UUID(72057594037932032L, -9223371306706625679L);
    private long A;
    private LongArray B;
    private LongArray C;
    private boolean D;
    private int E;
    private long F;
    private long G;
    private int H;
    private int I;
    private int[] J;
    private int K;
    private int L;
    private int M;
    private int N;
    private boolean O;
    private boolean P;
    private boolean Q;
    private boolean R;
    private byte S;
    private int T;
    private int U;
    private int V;
    private boolean W;
    private boolean X;
    private ExtractorOutput Y;

    /* renamed from: a  reason: collision with root package name */
    private final EbmlReader f3266a;
    private final VarintReader b;
    private final SparseArray<Track> c;
    private final boolean d;
    private final ParsableByteArray e;
    private final ParsableByteArray f;
    private final ParsableByteArray g;
    private final ParsableByteArray h;
    private final ParsableByteArray i;
    private final ParsableByteArray j;
    private final ParsableByteArray k;
    private final ParsableByteArray l;
    private final ParsableByteArray m;
    private ByteBuffer n;
    private long o;
    private long p;
    private long q;
    private long r;
    private long s;
    private Track t;
    private boolean u;
    private int v;
    private long w;
    private boolean x;
    private long y;
    private long z;

    private final class InnerEbmlReaderOutput implements EbmlReaderOutput {
        private InnerEbmlReaderOutput() {
        }

        public void a(int i, long j, long j2) throws ParserException {
            MatroskaExtractor.this.a(i, j, j2);
        }

        public int b(int i) {
            switch (i) {
                case 131:
                case 136:
                case 155:
                case 159:
                case 176:
                case 179:
                case 186:
                case JfifUtil.MARKER_RST7:
                case 231:
                case 241:
                case 251:
                case 16980:
                case 17029:
                case 17143:
                case 18401:
                case 18408:
                case 20529:
                case 20530:
                case 21420:
                case 21432:
                case 21680:
                case 21682:
                case 21690:
                case 21930:
                case 21945:
                case 21946:
                case 21947:
                case 21948:
                case 21949:
                case 22186:
                case 22203:
                case 25188:
                case 2352003:
                case 2807729:
                    return 2;
                case 134:
                case 17026:
                case 21358:
                case 2274716:
                    return 3;
                case 160:
                case 174:
                case 183:
                case 187:
                case 224:
                case JfifUtil.MARKER_APP1:
                case 18407:
                case 19899:
                case 20532:
                case 20533:
                case 21936:
                case 21968:
                case 25152:
                case 28032:
                case 30320:
                case 290298740:
                case 357149030:
                case 374648427:
                case 408125543:
                case 440786851:
                case 475249515:
                case 524531317:
                    return 1;
                case 161:
                case 163:
                case 16981:
                case 18402:
                case 21419:
                case 25506:
                case 30322:
                    return 4;
                case 181:
                case 17545:
                case 21969:
                case 21970:
                case 21971:
                case 21972:
                case 21973:
                case 21974:
                case 21975:
                case 21976:
                case 21977:
                case 21978:
                    return 5;
                default:
                    return 0;
            }
        }

        public boolean c(int i) {
            return i == 357149030 || i == 524531317 || i == 475249515 || i == 374648427;
        }

        public void a(int i) throws ParserException {
            MatroskaExtractor.this.a(i);
        }

        public void a(int i, long j) throws ParserException {
            MatroskaExtractor.this.a(i, j);
        }

        public void a(int i, double d) throws ParserException {
            MatroskaExtractor.this.a(i, d);
        }

        public void a(int i, String str) throws ParserException {
            MatroskaExtractor.this.a(i, str);
        }

        public void a(int i, int i2, ExtractorInput extractorInput) throws IOException, InterruptedException {
            MatroskaExtractor.this.a(i, i2, extractorInput);
        }
    }

    private static final class Track {
        public float A;
        public float B;
        public float C;
        public float D;
        public float E;
        public float F;
        public float G;
        public int H;
        public int I;
        public int J;
        public long K;
        public long L;
        public TrueHdSampleRechunker M;
        public boolean N;
        public boolean O;
        /* access modifiers changed from: private */
        public String P;
        public TrackOutput Q;
        public int R;

        /* renamed from: a  reason: collision with root package name */
        public String f3268a;
        public String b;
        public int c;
        public int d;
        public int e;
        public boolean f;
        public byte[] g;
        public TrackOutput.CryptoData h;
        public byte[] i;
        public DrmInitData j;
        public int k;
        public int l;
        public int m;
        public int n;
        public int o;
        public byte[] p;
        public int q;
        public boolean r;
        public int s;
        public int t;
        public int u;
        public int v;
        public int w;
        public float x;
        public float y;
        public float z;

        private Track() {
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = 0;
            this.p = null;
            this.q = -1;
            this.r = false;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = 1000;
            this.w = UserResponces.USER_RESPONCE_SUCCSES;
            this.x = -1.0f;
            this.y = -1.0f;
            this.z = -1.0f;
            this.A = -1.0f;
            this.B = -1.0f;
            this.C = -1.0f;
            this.D = -1.0f;
            this.E = -1.0f;
            this.F = -1.0f;
            this.G = -1.0f;
            this.H = 1;
            this.I = -1;
            this.J = 8000;
            this.K = 0;
            this.L = 0;
            this.O = true;
            this.P = "eng";
        }

        private byte[] c() {
            if (this.x == -1.0f || this.y == -1.0f || this.z == -1.0f || this.A == -1.0f || this.B == -1.0f || this.C == -1.0f || this.D == -1.0f || this.E == -1.0f || this.F == -1.0f || this.G == -1.0f) {
                return null;
            }
            byte[] bArr = new byte[25];
            ByteBuffer wrap = ByteBuffer.wrap(bArr);
            wrap.put((byte) 0);
            wrap.putShort((short) ((int) ((this.x * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) ((this.y * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) ((this.z * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) ((this.A * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) ((this.B * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) ((this.C * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) ((this.D * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) ((this.E * 50000.0f) + 0.5f)));
            wrap.putShort((short) ((int) (this.F + 0.5f)));
            wrap.putShort((short) ((int) (this.G + 0.5f)));
            wrap.putShort((short) this.v);
            wrap.putShort((short) this.w);
            return bArr;
        }

        public void b() {
            TrueHdSampleRechunker trueHdSampleRechunker = this.M;
            if (trueHdSampleRechunker != null) {
                trueHdSampleRechunker.a();
            }
        }

        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x01d4, code lost:
            r24 = r1;
            r1 = "audio/raw";
            r3 = null;
            r21 = -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x0225, code lost:
            r1 = "audio/x-unknown";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x0252, code lost:
            r3 = null;
            r21 = 4096;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x02cb, code lost:
            r21 = -1;
            r24 = -1;
            r32 = r3;
            r3 = r1;
            r1 = r32;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:135:0x0313, code lost:
            r3 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:136:0x0315, code lost:
            r21 = -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:137:0x0317, code lost:
            r24 = -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:138:0x0319, code lost:
            r12 = r0.O | false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:139:0x031e, code lost:
            if (r0.N == false) goto L_0x0322;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:140:0x0320, code lost:
            r13 = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:141:0x0322, code lost:
            r13 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:142:0x0323, code lost:
            r12 = r12 | r13;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:143:0x0328, code lost:
            if (com.google.android.exoplayer2.util.MimeTypes.j(r1) == false) goto L_0x034f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:144:0x032a, code lost:
            r1 = com.google.android.exoplayer2.Format.a(java.lang.Integer.toString(r35), r1, (java.lang.String) null, -1, r21, r0.H, r0.J, r24, r3, r0.j, r12 ? 1 : 0, r0.P);
            r7 = 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:146:0x0353, code lost:
            if (com.google.android.exoplayer2.util.MimeTypes.l(r1) == false) goto L_0x0400;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:148:0x0357, code lost:
            if (r0.o != 0) goto L_0x0369;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:149:0x0359, code lost:
            r2 = r0.m;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:150:0x035b, code lost:
            if (r2 != -1) goto L_0x035f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:151:0x035d, code lost:
            r2 = r0.k;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:152:0x035f, code lost:
            r0.m = r2;
            r2 = r0.n;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:153:0x0363, code lost:
            if (r2 != -1) goto L_0x0367;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:154:0x0365, code lost:
            r2 = r0.l;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:155:0x0367, code lost:
            r0.n = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:156:0x0369, code lost:
            r4 = r0.m;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:157:0x036d, code lost:
            if (r4 == -1) goto L_0x0381;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:158:0x036f, code lost:
            r7 = r0.n;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:159:0x0371, code lost:
            if (r7 == -1) goto L_0x0381;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:160:0x0373, code lost:
            r27 = ((float) (r0.l * r4)) / ((float) (r0.k * r7));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:161:0x0381, code lost:
            r27 = -1.0f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:163:0x0385, code lost:
            if (r0.r == false) goto L_0x0399;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:164:0x0387, code lost:
            r30 = new com.google.android.exoplayer2.video.ColorInfo(r0.s, r0.u, r0.t, c());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:165:0x0399, code lost:
            r30 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:167:0x03a3, code lost:
            if ("htc_video_rotA-000".equals(r0.f3268a) == false) goto L_0x03a8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:168:0x03a5, code lost:
            r26 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:170:0x03b0, code lost:
            if ("htc_video_rotA-090".equals(r0.f3268a) == false) goto L_0x03b7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:171:0x03b2, code lost:
            r26 = 90;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:173:0x03bf, code lost:
            if ("htc_video_rotA-180".equals(r0.f3268a) == false) goto L_0x03c6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:174:0x03c1, code lost:
            r26 = com.facebook.imagepipeline.common.RotationOptions.ROTATE_180;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:176:0x03ce, code lost:
            if ("htc_video_rotA-270".equals(r0.f3268a) == false) goto L_0x03d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:177:0x03d0, code lost:
            r26 = com.facebook.imagepipeline.common.RotationOptions.ROTATE_270;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:178:0x03d5, code lost:
            r26 = -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:179:0x03d7, code lost:
            r1 = com.google.android.exoplayer2.Format.a(java.lang.Integer.toString(r35), r1, (java.lang.String) null, -1, r21, r0.k, r0.l, -1.0f, r3, r26, r27, r0.p, r0.q, r30, r0.j);
            r7 = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:181:0x0404, code lost:
            if ("application/x-subrip".equals(r1) == false) goto L_0x0414;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:182:0x0406, code lost:
            r1 = com.google.android.exoplayer2.Format.a(java.lang.Integer.toString(r35), r1, r12 ? 1 : 0, r0.P, r0.j);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:184:0x0418, code lost:
            if ("text/x-ssa".equals(r1) == false) goto L_0x044d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:185:0x041a, code lost:
            r2 = new java.util.ArrayList(2);
            r2.add(com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.a());
            r2.add(r0.i);
            r1 = com.google.android.exoplayer2.Format.a(java.lang.Integer.toString(r35), r1, (java.lang.String) null, -1, r12 ? 1 : 0, r0.P, -1, r0.j, com.facebook.common.time.Clock.MAX_TIME, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:187:0x0451, code lost:
            if ("application/vobsub".equals(r1) != false) goto L_0x046a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:189:0x0457, code lost:
            if ("application/pgs".equals(r1) != false) goto L_0x046a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:191:0x045f, code lost:
            if ("application/dvbsubs".equals(r1) == false) goto L_0x0462;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:193:0x0469, code lost:
            throw new com.google.android.exoplayer2.ParserException("Unexpected MIME type.");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:194:0x046a, code lost:
            r1 = com.google.android.exoplayer2.Format.a(java.lang.Integer.toString(r35), r1, (java.lang.String) null, -1, r12 ? 1 : 0, r3, r0.P, r0.j);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:195:0x0484, code lost:
            r0.Q = r34.a(r0.c, r7);
            r0.Q.a(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:196:0x0493, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.google.android.exoplayer2.extractor.ExtractorOutput r34, int r35) throws com.google.android.exoplayer2.ParserException {
            /*
                r33 = this;
                r0 = r33
                java.lang.String r1 = r0.b
                int r2 = r1.hashCode()
                r3 = 8
                r4 = 1
                r5 = 2
                r6 = 0
                r7 = 3
                r8 = -1
                switch(r2) {
                    case -2095576542: goto L_0x0155;
                    case -2095575984: goto L_0x014b;
                    case -1985379776: goto L_0x0140;
                    case -1784763192: goto L_0x0135;
                    case -1730367663: goto L_0x012a;
                    case -1482641358: goto L_0x011f;
                    case -1482641357: goto L_0x0114;
                    case -1373388978: goto L_0x0109;
                    case -933872740: goto L_0x00fe;
                    case -538363189: goto L_0x00f3;
                    case -538363109: goto L_0x00e8;
                    case -425012669: goto L_0x00dc;
                    case -356037306: goto L_0x00d0;
                    case 62923557: goto L_0x00c4;
                    case 62923603: goto L_0x00b8;
                    case 62927045: goto L_0x00ac;
                    case 82338133: goto L_0x00a1;
                    case 82338134: goto L_0x0096;
                    case 99146302: goto L_0x008a;
                    case 444813526: goto L_0x007e;
                    case 542569478: goto L_0x0072;
                    case 725957860: goto L_0x0066;
                    case 738597099: goto L_0x005a;
                    case 855502857: goto L_0x004f;
                    case 1422270023: goto L_0x0043;
                    case 1809237540: goto L_0x0038;
                    case 1950749482: goto L_0x002c;
                    case 1950789798: goto L_0x0020;
                    case 1951062397: goto L_0x0014;
                    default: goto L_0x0012;
                }
            L_0x0012:
                goto L_0x015f
            L_0x0014:
                java.lang.String r2 = "A_OPUS"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 11
                goto L_0x0160
            L_0x0020:
                java.lang.String r2 = "A_FLAC"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 21
                goto L_0x0160
            L_0x002c:
                java.lang.String r2 = "A_EAC3"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 16
                goto L_0x0160
            L_0x0038:
                java.lang.String r2 = "V_MPEG2"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 2
                goto L_0x0160
            L_0x0043:
                java.lang.String r2 = "S_TEXT/UTF8"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 24
                goto L_0x0160
            L_0x004f:
                java.lang.String r2 = "V_MPEGH/ISO/HEVC"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 7
                goto L_0x0160
            L_0x005a:
                java.lang.String r2 = "S_TEXT/ASS"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 25
                goto L_0x0160
            L_0x0066:
                java.lang.String r2 = "A_PCM/INT/LIT"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 23
                goto L_0x0160
            L_0x0072:
                java.lang.String r2 = "A_DTS/EXPRESS"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 19
                goto L_0x0160
            L_0x007e:
                java.lang.String r2 = "V_THEORA"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 9
                goto L_0x0160
            L_0x008a:
                java.lang.String r2 = "S_HDMV/PGS"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 27
                goto L_0x0160
            L_0x0096:
                java.lang.String r2 = "V_VP9"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 1
                goto L_0x0160
            L_0x00a1:
                java.lang.String r2 = "V_VP8"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 0
                goto L_0x0160
            L_0x00ac:
                java.lang.String r2 = "A_DTS"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 18
                goto L_0x0160
            L_0x00b8:
                java.lang.String r2 = "A_AC3"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 15
                goto L_0x0160
            L_0x00c4:
                java.lang.String r2 = "A_AAC"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 12
                goto L_0x0160
            L_0x00d0:
                java.lang.String r2 = "A_DTS/LOSSLESS"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 20
                goto L_0x0160
            L_0x00dc:
                java.lang.String r2 = "S_VOBSUB"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 26
                goto L_0x0160
            L_0x00e8:
                java.lang.String r2 = "V_MPEG4/ISO/AVC"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 6
                goto L_0x0160
            L_0x00f3:
                java.lang.String r2 = "V_MPEG4/ISO/ASP"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 4
                goto L_0x0160
            L_0x00fe:
                java.lang.String r2 = "S_DVBSUB"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 28
                goto L_0x0160
            L_0x0109:
                java.lang.String r2 = "V_MS/VFW/FOURCC"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 8
                goto L_0x0160
            L_0x0114:
                java.lang.String r2 = "A_MPEG/L3"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 14
                goto L_0x0160
            L_0x011f:
                java.lang.String r2 = "A_MPEG/L2"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 13
                goto L_0x0160
            L_0x012a:
                java.lang.String r2 = "A_VORBIS"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 10
                goto L_0x0160
            L_0x0135:
                java.lang.String r2 = "A_TRUEHD"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 17
                goto L_0x0160
            L_0x0140:
                java.lang.String r2 = "A_MS/ACM"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 22
                goto L_0x0160
            L_0x014b:
                java.lang.String r2 = "V_MPEG4/ISO/SP"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 3
                goto L_0x0160
            L_0x0155:
                java.lang.String r2 = "V_MPEG4/ISO/AP"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x015f
                r1 = 5
                goto L_0x0160
            L_0x015f:
                r1 = -1
            L_0x0160:
                java.lang.String r2 = "application/pgs"
                java.lang.String r9 = "application/vobsub"
                java.lang.String r10 = "text/x-ssa"
                java.lang.String r11 = "application/x-subrip"
                java.lang.String r12 = "audio/raw"
                r13 = 4096(0x1000, float:5.74E-42)
                java.lang.String r14 = "MatroskaExtractor"
                java.lang.String r15 = "audio/x-unknown"
                r16 = 0
                switch(r1) {
                    case 0: goto L_0x0311;
                    case 1: goto L_0x030e;
                    case 2: goto L_0x030b;
                    case 3: goto L_0x02fd;
                    case 4: goto L_0x02fd;
                    case 5: goto L_0x02fd;
                    case 6: goto L_0x02e9;
                    case 7: goto L_0x02d5;
                    case 8: goto L_0x02b8;
                    case 9: goto L_0x02b5;
                    case 10: goto L_0x02a7;
                    case 11: goto L_0x0261;
                    case 12: goto L_0x0258;
                    case 13: goto L_0x0250;
                    case 14: goto L_0x024d;
                    case 15: goto L_0x0249;
                    case 16: goto L_0x0245;
                    case 17: goto L_0x023a;
                    case 18: goto L_0x0236;
                    case 19: goto L_0x0236;
                    case 20: goto L_0x0232;
                    case 21: goto L_0x0228;
                    case 22: goto L_0x01dd;
                    case 23: goto L_0x01ad;
                    case 24: goto L_0x01aa;
                    case 25: goto L_0x01a7;
                    case 26: goto L_0x019d;
                    case 27: goto L_0x019a;
                    case 28: goto L_0x017d;
                    default: goto L_0x0175;
                }
            L_0x0175:
                com.google.android.exoplayer2.ParserException r1 = new com.google.android.exoplayer2.ParserException
                java.lang.String r2 = "Unrecognized codec identifier."
                r1.<init>((java.lang.String) r2)
                throw r1
            L_0x017d:
                r1 = 4
                byte[] r1 = new byte[r1]
                byte[] r3 = r0.i
                byte r12 = r3[r6]
                r1[r6] = r12
                byte r12 = r3[r4]
                r1[r4] = r12
                byte r12 = r3[r5]
                r1[r5] = r12
                byte r3 = r3[r7]
                r1[r7] = r3
                java.util.List r1 = java.util.Collections.singletonList(r1)
                java.lang.String r3 = "application/dvbsubs"
                goto L_0x02cb
            L_0x019a:
                r1 = r2
                goto L_0x0313
            L_0x019d:
                byte[] r1 = r0.i
                java.util.List r1 = java.util.Collections.singletonList(r1)
                r3 = r1
                r1 = r9
                goto L_0x0315
            L_0x01a7:
                r1 = r10
                goto L_0x0313
            L_0x01aa:
                r1 = r11
                goto L_0x0313
            L_0x01ad:
                int r1 = r0.I
                int r1 = com.google.android.exoplayer2.util.Util.c((int) r1)
                if (r1 != 0) goto L_0x01d4
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r3 = "Unsupported PCM bit depth: "
                r1.append(r3)
                int r3 = r0.I
                r1.append(r3)
                java.lang.String r3 = ". Setting mimeType to "
                r1.append(r3)
                r1.append(r15)
                java.lang.String r1 = r1.toString()
                com.google.android.exoplayer2.util.Log.d(r14, r1)
                goto L_0x0225
            L_0x01d4:
                r24 = r1
                r1 = r12
                r3 = r16
                r21 = -1
                goto L_0x0319
            L_0x01dd:
                com.google.android.exoplayer2.util.ParsableByteArray r1 = new com.google.android.exoplayer2.util.ParsableByteArray
                byte[] r3 = r0.i
                r1.<init>((byte[]) r3)
                boolean r1 = b(r1)
                if (r1 == 0) goto L_0x0211
                int r1 = r0.I
                int r1 = com.google.android.exoplayer2.util.Util.c((int) r1)
                if (r1 != 0) goto L_0x01d4
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r3 = "Unsupported PCM bit depth: "
                r1.append(r3)
                int r3 = r0.I
                r1.append(r3)
                java.lang.String r3 = ". Setting mimeType to "
                r1.append(r3)
                r1.append(r15)
                java.lang.String r1 = r1.toString()
                com.google.android.exoplayer2.util.Log.d(r14, r1)
                goto L_0x0225
            L_0x0211:
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r3 = "Non-PCM MS/ACM is unsupported. Setting mimeType to "
                r1.append(r3)
                r1.append(r15)
                java.lang.String r1 = r1.toString()
                com.google.android.exoplayer2.util.Log.d(r14, r1)
            L_0x0225:
                r1 = r15
                goto L_0x0313
            L_0x0228:
                byte[] r1 = r0.i
                java.util.List r1 = java.util.Collections.singletonList(r1)
                java.lang.String r3 = "audio/flac"
                goto L_0x02cb
            L_0x0232:
                java.lang.String r1 = "audio/vnd.dts.hd"
                goto L_0x0313
            L_0x0236:
                java.lang.String r1 = "audio/vnd.dts"
                goto L_0x0313
            L_0x023a:
                com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$TrueHdSampleRechunker r1 = new com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$TrueHdSampleRechunker
                r1.<init>()
                r0.M = r1
                java.lang.String r1 = "audio/true-hd"
                goto L_0x0313
            L_0x0245:
                java.lang.String r1 = "audio/eac3"
                goto L_0x0313
            L_0x0249:
                java.lang.String r1 = "audio/ac3"
                goto L_0x0313
            L_0x024d:
                java.lang.String r1 = "audio/mpeg"
                goto L_0x0252
            L_0x0250:
                java.lang.String r1 = "audio/mpeg-L2"
            L_0x0252:
                r3 = r16
                r21 = 4096(0x1000, float:5.74E-42)
                goto L_0x0317
            L_0x0258:
                byte[] r1 = r0.i
                java.util.List r1 = java.util.Collections.singletonList(r1)
                java.lang.String r3 = "audio/mp4a-latm"
                goto L_0x02cb
            L_0x0261:
                r1 = 5760(0x1680, float:8.071E-42)
                java.util.ArrayList r12 = new java.util.ArrayList
                r12.<init>(r7)
                byte[] r13 = r0.i
                r12.add(r13)
                java.nio.ByteBuffer r13 = java.nio.ByteBuffer.allocate(r3)
                java.nio.ByteOrder r14 = java.nio.ByteOrder.nativeOrder()
                java.nio.ByteBuffer r13 = r13.order(r14)
                long r14 = r0.K
                java.nio.ByteBuffer r13 = r13.putLong(r14)
                byte[] r13 = r13.array()
                r12.add(r13)
                java.nio.ByteBuffer r3 = java.nio.ByteBuffer.allocate(r3)
                java.nio.ByteOrder r13 = java.nio.ByteOrder.nativeOrder()
                java.nio.ByteBuffer r3 = r3.order(r13)
                long r13 = r0.L
                java.nio.ByteBuffer r3 = r3.putLong(r13)
                byte[] r3 = r3.array()
                r12.add(r3)
                java.lang.String r3 = "audio/opus"
                r1 = r3
                r3 = r12
                r21 = 5760(0x1680, float:8.071E-42)
                goto L_0x0317
            L_0x02a7:
                r1 = 8192(0x2000, float:1.14794E-41)
                byte[] r3 = r0.i
                java.util.List r3 = a((byte[]) r3)
                java.lang.String r12 = "audio/vorbis"
                r1 = r12
                r21 = 8192(0x2000, float:1.14794E-41)
                goto L_0x0317
            L_0x02b5:
                java.lang.String r1 = "video/x-unknown"
                goto L_0x0313
            L_0x02b8:
                com.google.android.exoplayer2.util.ParsableByteArray r1 = new com.google.android.exoplayer2.util.ParsableByteArray
                byte[] r3 = r0.i
                r1.<init>((byte[]) r3)
                android.util.Pair r1 = a((com.google.android.exoplayer2.util.ParsableByteArray) r1)
                java.lang.Object r3 = r1.first
                java.lang.String r3 = (java.lang.String) r3
                java.lang.Object r1 = r1.second
                java.util.List r1 = (java.util.List) r1
            L_0x02cb:
                r21 = -1
                r24 = -1
                r32 = r3
                r3 = r1
                r1 = r32
                goto L_0x0319
            L_0x02d5:
                com.google.android.exoplayer2.util.ParsableByteArray r1 = new com.google.android.exoplayer2.util.ParsableByteArray
                byte[] r3 = r0.i
                r1.<init>((byte[]) r3)
                com.google.android.exoplayer2.video.HevcConfig r1 = com.google.android.exoplayer2.video.HevcConfig.a(r1)
                java.util.List<byte[]> r3 = r1.f3660a
                int r1 = r1.b
                r0.R = r1
                java.lang.String r1 = "video/hevc"
                goto L_0x0315
            L_0x02e9:
                com.google.android.exoplayer2.util.ParsableByteArray r1 = new com.google.android.exoplayer2.util.ParsableByteArray
                byte[] r3 = r0.i
                r1.<init>((byte[]) r3)
                com.google.android.exoplayer2.video.AvcConfig r1 = com.google.android.exoplayer2.video.AvcConfig.b(r1)
                java.util.List<byte[]> r3 = r1.f3656a
                int r1 = r1.b
                r0.R = r1
                java.lang.String r1 = "video/avc"
                goto L_0x0315
            L_0x02fd:
                byte[] r1 = r0.i
                if (r1 != 0) goto L_0x0304
                r1 = r16
                goto L_0x0308
            L_0x0304:
                java.util.List r1 = java.util.Collections.singletonList(r1)
            L_0x0308:
                java.lang.String r3 = "video/mp4v-es"
                goto L_0x02cb
            L_0x030b:
                java.lang.String r1 = "video/mpeg2"
                goto L_0x0313
            L_0x030e:
                java.lang.String r1 = "video/x-vnd.on2.vp9"
                goto L_0x0313
            L_0x0311:
                java.lang.String r1 = "video/x-vnd.on2.vp8"
            L_0x0313:
                r3 = r16
            L_0x0315:
                r21 = -1
            L_0x0317:
                r24 = -1
            L_0x0319:
                boolean r12 = r0.O
                r12 = r12 | r6
                boolean r13 = r0.N
                if (r13 == 0) goto L_0x0322
                r13 = 2
                goto L_0x0323
            L_0x0322:
                r13 = 0
            L_0x0323:
                r12 = r12 | r13
                boolean r13 = com.google.android.exoplayer2.util.MimeTypes.j(r1)
                if (r13 == 0) goto L_0x034f
                java.lang.String r17 = java.lang.Integer.toString(r35)
                r19 = 0
                r20 = -1
                int r2 = r0.H
                int r5 = r0.J
                com.google.android.exoplayer2.drm.DrmInitData r6 = r0.j
                java.lang.String r7 = r0.P
                r18 = r1
                r22 = r2
                r23 = r5
                r25 = r3
                r26 = r6
                r27 = r12
                r28 = r7
                com.google.android.exoplayer2.Format r1 = com.google.android.exoplayer2.Format.a((java.lang.String) r17, (java.lang.String) r18, (java.lang.String) r19, (int) r20, (int) r21, (int) r22, (int) r23, (int) r24, (java.util.List<byte[]>) r25, (com.google.android.exoplayer2.drm.DrmInitData) r26, (int) r27, (java.lang.String) r28)
                r7 = 1
                goto L_0x0484
            L_0x034f:
                boolean r4 = com.google.android.exoplayer2.util.MimeTypes.l(r1)
                if (r4 == 0) goto L_0x0400
                int r2 = r0.o
                if (r2 != 0) goto L_0x0369
                int r2 = r0.m
                if (r2 != r8) goto L_0x035f
                int r2 = r0.k
            L_0x035f:
                r0.m = r2
                int r2 = r0.n
                if (r2 != r8) goto L_0x0367
                int r2 = r0.l
            L_0x0367:
                r0.n = r2
            L_0x0369:
                r2 = -1082130432(0xffffffffbf800000, float:-1.0)
                int r4 = r0.m
                if (r4 == r8) goto L_0x0381
                int r7 = r0.n
                if (r7 == r8) goto L_0x0381
                int r2 = r0.l
                int r2 = r2 * r4
                float r2 = (float) r2
                int r4 = r0.k
                int r4 = r4 * r7
                float r4 = (float) r4
                float r2 = r2 / r4
                r27 = r2
                goto L_0x0383
            L_0x0381:
                r27 = -1082130432(0xffffffffbf800000, float:-1.0)
            L_0x0383:
                boolean r2 = r0.r
                if (r2 == 0) goto L_0x0399
                byte[] r2 = r33.c()
                com.google.android.exoplayer2.video.ColorInfo r4 = new com.google.android.exoplayer2.video.ColorInfo
                int r7 = r0.s
                int r9 = r0.u
                int r10 = r0.t
                r4.<init>(r7, r9, r10, r2)
                r30 = r4
                goto L_0x039b
            L_0x0399:
                r30 = r16
            L_0x039b:
                java.lang.String r2 = r0.f3268a
                java.lang.String r4 = "htc_video_rotA-000"
                boolean r2 = r4.equals(r2)
                if (r2 == 0) goto L_0x03a8
                r26 = 0
                goto L_0x03d7
            L_0x03a8:
                java.lang.String r2 = r0.f3268a
                java.lang.String r4 = "htc_video_rotA-090"
                boolean r2 = r4.equals(r2)
                if (r2 == 0) goto L_0x03b7
                r6 = 90
                r26 = 90
                goto L_0x03d7
            L_0x03b7:
                java.lang.String r2 = r0.f3268a
                java.lang.String r4 = "htc_video_rotA-180"
                boolean r2 = r4.equals(r2)
                if (r2 == 0) goto L_0x03c6
                r6 = 180(0xb4, float:2.52E-43)
                r26 = 180(0xb4, float:2.52E-43)
                goto L_0x03d7
            L_0x03c6:
                java.lang.String r2 = r0.f3268a
                java.lang.String r4 = "htc_video_rotA-270"
                boolean r2 = r4.equals(r2)
                if (r2 == 0) goto L_0x03d5
                r6 = 270(0x10e, float:3.78E-43)
                r26 = 270(0x10e, float:3.78E-43)
                goto L_0x03d7
            L_0x03d5:
                r26 = -1
            L_0x03d7:
                java.lang.String r17 = java.lang.Integer.toString(r35)
                r19 = 0
                r20 = -1
                int r2 = r0.k
                int r4 = r0.l
                r24 = -1082130432(0xffffffffbf800000, float:-1.0)
                byte[] r6 = r0.p
                int r7 = r0.q
                com.google.android.exoplayer2.drm.DrmInitData r8 = r0.j
                r18 = r1
                r22 = r2
                r23 = r4
                r25 = r3
                r28 = r6
                r29 = r7
                r31 = r8
                com.google.android.exoplayer2.Format r1 = com.google.android.exoplayer2.Format.a((java.lang.String) r17, (java.lang.String) r18, (java.lang.String) r19, (int) r20, (int) r21, (int) r22, (int) r23, (float) r24, (java.util.List<byte[]>) r25, (int) r26, (float) r27, (byte[]) r28, (int) r29, (com.google.android.exoplayer2.video.ColorInfo) r30, (com.google.android.exoplayer2.drm.DrmInitData) r31)
                r7 = 2
                goto L_0x0484
            L_0x0400:
                boolean r4 = r11.equals(r1)
                if (r4 == 0) goto L_0x0414
                java.lang.String r2 = java.lang.Integer.toString(r35)
                java.lang.String r3 = r0.P
                com.google.android.exoplayer2.drm.DrmInitData r4 = r0.j
                com.google.android.exoplayer2.Format r1 = com.google.android.exoplayer2.Format.a((java.lang.String) r2, (java.lang.String) r1, (int) r12, (java.lang.String) r3, (com.google.android.exoplayer2.drm.DrmInitData) r4)
                goto L_0x0484
            L_0x0414:
                boolean r4 = r10.equals(r1)
                if (r4 == 0) goto L_0x044d
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>(r5)
                byte[] r3 = com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.b0
                r2.add(r3)
                byte[] r3 = r0.i
                r2.add(r3)
                java.lang.String r17 = java.lang.Integer.toString(r35)
                r19 = 0
                r20 = -1
                java.lang.String r3 = r0.P
                r23 = -1
                com.google.android.exoplayer2.drm.DrmInitData r4 = r0.j
                r25 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                r18 = r1
                r21 = r12
                r22 = r3
                r24 = r4
                r27 = r2
                com.google.android.exoplayer2.Format r1 = com.google.android.exoplayer2.Format.a(r17, r18, r19, r20, r21, r22, r23, r24, r25, r27)
                goto L_0x0484
            L_0x044d:
                boolean r4 = r9.equals(r1)
                if (r4 != 0) goto L_0x046a
                boolean r2 = r2.equals(r1)
                if (r2 != 0) goto L_0x046a
                java.lang.String r2 = "application/dvbsubs"
                boolean r2 = r2.equals(r1)
                if (r2 == 0) goto L_0x0462
                goto L_0x046a
            L_0x0462:
                com.google.android.exoplayer2.ParserException r1 = new com.google.android.exoplayer2.ParserException
                java.lang.String r2 = "Unexpected MIME type."
                r1.<init>((java.lang.String) r2)
                throw r1
            L_0x046a:
                java.lang.String r17 = java.lang.Integer.toString(r35)
                r19 = 0
                r20 = -1
                java.lang.String r2 = r0.P
                com.google.android.exoplayer2.drm.DrmInitData r4 = r0.j
                r18 = r1
                r21 = r12
                r22 = r3
                r23 = r2
                r24 = r4
                com.google.android.exoplayer2.Format r1 = com.google.android.exoplayer2.Format.a((java.lang.String) r17, (java.lang.String) r18, (java.lang.String) r19, (int) r20, (int) r21, (java.util.List<byte[]>) r22, (java.lang.String) r23, (com.google.android.exoplayer2.drm.DrmInitData) r24)
            L_0x0484:
                int r2 = r0.c
                r3 = r34
                com.google.android.exoplayer2.extractor.TrackOutput r2 = r3.a(r2, r7)
                r0.Q = r2
                com.google.android.exoplayer2.extractor.TrackOutput r2 = r0.Q
                r2.a(r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.Track.a(com.google.android.exoplayer2.extractor.ExtractorOutput, int):void");
        }

        private static boolean b(ParsableByteArray parsableByteArray) throws ParserException {
            try {
                int o2 = parsableByteArray.o();
                if (o2 == 1) {
                    return true;
                }
                if (o2 != 65534) {
                    return false;
                }
                parsableByteArray.e(24);
                if (parsableByteArray.p() == MatroskaExtractor.e0.getMostSignificantBits() && parsableByteArray.p() == MatroskaExtractor.e0.getLeastSignificantBits()) {
                    return true;
                }
                return false;
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw new ParserException("Error parsing MS/ACM codec private");
            }
        }

        public void a() {
            TrueHdSampleRechunker trueHdSampleRechunker = this.M;
            if (trueHdSampleRechunker != null) {
                trueHdSampleRechunker.a(this);
            }
        }

        private static Pair<String, List<byte[]>> a(ParsableByteArray parsableByteArray) throws ParserException {
            try {
                parsableByteArray.f(16);
                long m2 = parsableByteArray.m();
                if (m2 == 1482049860) {
                    return new Pair<>("video/3gpp", (Object) null);
                }
                if (m2 == 826496599) {
                    byte[] bArr = parsableByteArray.f3641a;
                    for (int c2 = parsableByteArray.c() + 20; c2 < bArr.length - 4; c2++) {
                        if (bArr[c2] == 0 && bArr[c2 + 1] == 0 && bArr[c2 + 2] == 1 && bArr[c2 + 3] == 15) {
                            return new Pair<>("video/wvc1", Collections.singletonList(Arrays.copyOfRange(bArr, c2, bArr.length)));
                        }
                    }
                    throw new ParserException("Failed to find FourCC VC1 initialization data");
                }
                Log.d("MatroskaExtractor", "Unknown FourCC. Setting mimeType to video/x-unknown");
                return new Pair<>("video/x-unknown", (Object) null);
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw new ParserException("Error parsing FourCC private data");
            }
        }

        private static List<byte[]> a(byte[] bArr) throws ParserException {
            try {
                if (bArr[0] == 2) {
                    int i2 = 1;
                    int i3 = 0;
                    while (bArr[i2] == -1) {
                        i3 += JfifUtil.MARKER_FIRST_BYTE;
                        i2++;
                    }
                    int i4 = i2 + 1;
                    int i5 = i3 + bArr[i2];
                    int i6 = 0;
                    while (bArr[i4] == -1) {
                        i6 += JfifUtil.MARKER_FIRST_BYTE;
                        i4++;
                    }
                    int i7 = i4 + 1;
                    int i8 = i6 + bArr[i4];
                    if (bArr[i7] == 1) {
                        byte[] bArr2 = new byte[i5];
                        System.arraycopy(bArr, i7, bArr2, 0, i5);
                        int i9 = i7 + i5;
                        if (bArr[i9] == 3) {
                            int i10 = i9 + i8;
                            if (bArr[i10] == 5) {
                                byte[] bArr3 = new byte[(bArr.length - i10)];
                                System.arraycopy(bArr, i10, bArr3, 0, bArr.length - i10);
                                ArrayList arrayList = new ArrayList(2);
                                arrayList.add(bArr2);
                                arrayList.add(bArr3);
                                return arrayList;
                            }
                            throw new ParserException("Error parsing vorbis codec private");
                        }
                        throw new ParserException("Error parsing vorbis codec private");
                    }
                    throw new ParserException("Error parsing vorbis codec private");
                }
                throw new ParserException("Error parsing vorbis codec private");
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw new ParserException("Error parsing vorbis codec private");
            }
        }
    }

    private static final class TrueHdSampleRechunker {

        /* renamed from: a  reason: collision with root package name */
        private final byte[] f3269a = new byte[10];
        private boolean b;
        private int c;
        private int d;
        private long e;
        private int f;

        public void a() {
            this.b = false;
        }

        public void a(ExtractorInput extractorInput, int i, int i2) throws IOException, InterruptedException {
            if (!this.b) {
                extractorInput.a(this.f3269a, 0, 10);
                extractorInput.a();
                if (Ac3Util.b(this.f3269a) != 0) {
                    this.b = true;
                    this.c = 0;
                } else {
                    return;
                }
            }
            if (this.c == 0) {
                this.f = i;
                this.d = 0;
            }
            this.d += i2;
        }

        public void a(Track track, long j) {
            if (this.b) {
                int i = this.c;
                this.c = i + 1;
                if (i == 0) {
                    this.e = j;
                }
                if (this.c >= 16) {
                    track.Q.a(this.e, this.f, this.d, 0, track.h);
                    this.c = 0;
                }
            }
        }

        public void a(Track track) {
            if (this.b && this.c > 0) {
                track.Q.a(this.e, this.f, this.d, 0, track.h);
                this.c = 0;
            }
        }
    }

    static {
        a aVar = a.f3272a;
    }

    public MatroskaExtractor() {
        this(0);
    }

    private SeekMap c() {
        LongArray longArray;
        LongArray longArray2;
        if (this.p == -1 || this.s == -9223372036854775807L || (longArray = this.B) == null || longArray.a() == 0 || (longArray2 = this.C) == null || longArray2.a() != this.B.a()) {
            this.B = null;
            this.C = null;
            return new SeekMap.Unseekable(this.s);
        }
        int a2 = this.B.a();
        int[] iArr = new int[a2];
        long[] jArr = new long[a2];
        long[] jArr2 = new long[a2];
        long[] jArr3 = new long[a2];
        int i2 = 0;
        for (int i3 = 0; i3 < a2; i3++) {
            jArr3[i3] = this.B.a(i3);
            jArr[i3] = this.p + this.C.a(i3);
        }
        while (true) {
            int i4 = a2 - 1;
            if (i2 < i4) {
                int i5 = i2 + 1;
                iArr[i2] = (int) (jArr[i5] - jArr[i2]);
                jArr2[i2] = jArr3[i5] - jArr3[i2];
                i2 = i5;
            } else {
                iArr[i4] = (int) ((this.p + this.o) - jArr[i4]);
                jArr2[i4] = this.s - jArr3[i4];
                this.B = null;
                this.C = null;
                return new ChunkIndex(iArr, jArr, jArr2, jArr3);
            }
        }
    }

    static /* synthetic */ Extractor[] d() {
        return new Extractor[]{new MatroskaExtractor()};
    }

    private void e() {
        this.N = 0;
        this.V = 0;
        this.U = 0;
        this.O = false;
        this.P = false;
        this.R = false;
        this.T = 0;
        this.S = 0;
        this.Q = false;
        this.j.B();
    }

    public void release() {
    }

    public MatroskaExtractor(int i2) {
        this(new DefaultEbmlReader(), i2);
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return new Sniffer().a(extractorInput);
    }

    MatroskaExtractor(EbmlReader ebmlReader, int i2) {
        this.p = -1;
        this.q = -9223372036854775807L;
        this.r = -9223372036854775807L;
        this.s = -9223372036854775807L;
        this.y = -1;
        this.z = -1;
        this.A = -9223372036854775807L;
        this.f3266a = ebmlReader;
        this.f3266a.a((EbmlReaderOutput) new InnerEbmlReaderOutput());
        this.d = (i2 & 1) != 0 ? false : true;
        this.b = new VarintReader();
        this.c = new SparseArray<>();
        this.g = new ParsableByteArray(4);
        this.h = new ParsableByteArray(ByteBuffer.allocate(4).putInt(-1).array());
        this.i = new ParsableByteArray(4);
        this.e = new ParsableByteArray(NalUnitUtil.f3637a);
        this.f = new ParsableByteArray(4);
        this.j = new ParsableByteArray();
        this.k = new ParsableByteArray();
        this.l = new ParsableByteArray(8);
        this.m = new ParsableByteArray();
    }

    public void a(ExtractorOutput extractorOutput) {
        this.Y = extractorOutput;
    }

    public void a(long j2, long j3) {
        this.A = -9223372036854775807L;
        this.E = 0;
        this.f3266a.reset();
        this.b.b();
        e();
        for (int i2 = 0; i2 < this.c.size(); i2++) {
            this.c.valueAt(i2).b();
        }
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        this.W = false;
        boolean z2 = true;
        while (z2 && !this.W) {
            z2 = this.f3266a.a(extractorInput);
            if (z2 && a(positionHolder, extractorInput.getPosition())) {
                return 1;
            }
        }
        if (z2) {
            return 0;
        }
        for (int i2 = 0; i2 < this.c.size(); i2++) {
            this.c.valueAt(i2).a();
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, long j2, long j3) throws ParserException {
        if (i2 == 160) {
            this.X = false;
        } else if (i2 == 174) {
            this.t = new Track();
        } else if (i2 == 187) {
            this.D = false;
        } else if (i2 == 19899) {
            this.v = -1;
            this.w = -1;
        } else if (i2 == 20533) {
            this.t.f = true;
        } else if (i2 == 21968) {
            this.t.r = true;
        } else if (i2 == 25152) {
        } else {
            if (i2 == 408125543) {
                long j4 = this.p;
                if (j4 == -1 || j4 == j2) {
                    this.p = j2;
                    this.o = j3;
                    return;
                }
                throw new ParserException("Multiple Segment elements not supported");
            } else if (i2 == 475249515) {
                this.B = new LongArray();
                this.C = new LongArray();
            } else if (i2 != 524531317 || this.u) {
            } else {
                if (!this.d || this.y == -1) {
                    this.Y.a(new SeekMap.Unseekable(this.s));
                    this.u = true;
                    return;
                }
                this.x = true;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) throws ParserException {
        if (i2 != 160) {
            if (i2 == 174) {
                if (a(this.t.b)) {
                    Track track = this.t;
                    track.a(this.Y, track.c);
                    SparseArray<Track> sparseArray = this.c;
                    Track track2 = this.t;
                    sparseArray.put(track2.c, track2);
                }
                this.t = null;
            } else if (i2 == 19899) {
                int i3 = this.v;
                if (i3 != -1) {
                    long j2 = this.w;
                    if (j2 != -1) {
                        if (i3 == 475249515) {
                            this.y = j2;
                            return;
                        }
                        return;
                    }
                }
                throw new ParserException("Mandatory element SeekID or SeekPosition not found");
            } else if (i2 == 25152) {
                Track track3 = this.t;
                if (track3.f) {
                    TrackOutput.CryptoData cryptoData = track3.h;
                    if (cryptoData != null) {
                        track3.j = new DrmInitData(new DrmInitData.SchemeData(C.f3151a, "video/webm", cryptoData.b));
                        return;
                    }
                    throw new ParserException("Encrypted Track found but ContentEncKeyID was not found");
                }
            } else if (i2 == 28032) {
                Track track4 = this.t;
                if (track4.f && track4.g != null) {
                    throw new ParserException("Combining encryption and compression is not supported");
                }
            } else if (i2 == 357149030) {
                if (this.q == -9223372036854775807L) {
                    this.q = 1000000;
                }
                long j3 = this.r;
                if (j3 != -9223372036854775807L) {
                    this.s = a(j3);
                }
            } else if (i2 != 374648427) {
                if (i2 == 475249515 && !this.u) {
                    this.Y.a(c());
                    this.u = true;
                }
            } else if (this.c.size() != 0) {
                this.Y.a();
            } else {
                throw new ParserException("No valid tracks were found");
            }
        } else if (this.E == 2) {
            if (!this.X) {
                this.M |= 1;
            }
            a(this.c.get(this.K), this.F);
            this.E = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, long j2) throws ParserException {
        if (i2 != 20529) {
            if (i2 != 20530) {
                boolean z2 = false;
                switch (i2) {
                    case 131:
                        this.t.d = (int) j2;
                        return;
                    case 136:
                        Track track = this.t;
                        if (j2 == 1) {
                            z2 = true;
                        }
                        track.O = z2;
                        return;
                    case 155:
                        this.G = a(j2);
                        return;
                    case 159:
                        this.t.H = (int) j2;
                        return;
                    case 176:
                        this.t.k = (int) j2;
                        return;
                    case 179:
                        this.B.a(a(j2));
                        return;
                    case 186:
                        this.t.l = (int) j2;
                        return;
                    case JfifUtil.MARKER_RST7:
                        this.t.c = (int) j2;
                        return;
                    case 231:
                        this.A = a(j2);
                        return;
                    case 241:
                        if (!this.D) {
                            this.C.a(j2);
                            this.D = true;
                            return;
                        }
                        return;
                    case 251:
                        this.X = true;
                        return;
                    case 16980:
                        if (j2 != 3) {
                            throw new ParserException("ContentCompAlgo " + j2 + " not supported");
                        }
                        return;
                    case 17029:
                        if (j2 < 1 || j2 > 2) {
                            throw new ParserException("DocTypeReadVersion " + j2 + " not supported");
                        }
                        return;
                    case 17143:
                        if (j2 != 1) {
                            throw new ParserException("EBMLReadVersion " + j2 + " not supported");
                        }
                        return;
                    case 18401:
                        if (j2 != 5) {
                            throw new ParserException("ContentEncAlgo " + j2 + " not supported");
                        }
                        return;
                    case 18408:
                        if (j2 != 1) {
                            throw new ParserException("AESSettingsCipherMode " + j2 + " not supported");
                        }
                        return;
                    case 21420:
                        this.w = j2 + this.p;
                        return;
                    case 21432:
                        int i3 = (int) j2;
                        if (i3 == 0) {
                            this.t.q = 0;
                            return;
                        } else if (i3 == 1) {
                            this.t.q = 2;
                            return;
                        } else if (i3 == 3) {
                            this.t.q = 1;
                            return;
                        } else if (i3 == 15) {
                            this.t.q = 3;
                            return;
                        } else {
                            return;
                        }
                    case 21680:
                        this.t.m = (int) j2;
                        return;
                    case 21682:
                        this.t.o = (int) j2;
                        return;
                    case 21690:
                        this.t.n = (int) j2;
                        return;
                    case 21930:
                        Track track2 = this.t;
                        if (j2 == 1) {
                            z2 = true;
                        }
                        track2.N = z2;
                        return;
                    case 22186:
                        this.t.K = j2;
                        return;
                    case 22203:
                        this.t.L = j2;
                        return;
                    case 25188:
                        this.t.I = (int) j2;
                        return;
                    case 2352003:
                        this.t.e = (int) j2;
                        return;
                    case 2807729:
                        this.q = j2;
                        return;
                    default:
                        switch (i2) {
                            case 21945:
                                int i4 = (int) j2;
                                if (i4 == 1) {
                                    this.t.u = 2;
                                    return;
                                } else if (i4 == 2) {
                                    this.t.u = 1;
                                    return;
                                } else {
                                    return;
                                }
                            case 21946:
                                int i5 = (int) j2;
                                if (i5 != 1) {
                                    if (i5 == 16) {
                                        this.t.t = 6;
                                        return;
                                    } else if (i5 == 18) {
                                        this.t.t = 7;
                                        return;
                                    } else if (!(i5 == 6 || i5 == 7)) {
                                        return;
                                    }
                                }
                                this.t.t = 3;
                                return;
                            case 21947:
                                Track track3 = this.t;
                                track3.r = true;
                                int i6 = (int) j2;
                                if (i6 == 1) {
                                    track3.s = 1;
                                    return;
                                } else if (i6 == 9) {
                                    track3.s = 6;
                                    return;
                                } else if (i6 == 4 || i6 == 5 || i6 == 6 || i6 == 7) {
                                    this.t.s = 2;
                                    return;
                                } else {
                                    return;
                                }
                            case 21948:
                                this.t.v = (int) j2;
                                return;
                            case 21949:
                                this.t.w = (int) j2;
                                return;
                            default:
                                return;
                        }
                }
            } else if (j2 != 1) {
                throw new ParserException("ContentEncodingScope " + j2 + " not supported");
            }
        } else if (j2 != 0) {
            throw new ParserException("ContentEncodingOrder " + j2 + " not supported");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, double d2) {
        if (i2 == 181) {
            this.t.J = (int) d2;
        } else if (i2 != 17545) {
            switch (i2) {
                case 21969:
                    this.t.x = (float) d2;
                    return;
                case 21970:
                    this.t.y = (float) d2;
                    return;
                case 21971:
                    this.t.z = (float) d2;
                    return;
                case 21972:
                    this.t.A = (float) d2;
                    return;
                case 21973:
                    this.t.B = (float) d2;
                    return;
                case 21974:
                    this.t.C = (float) d2;
                    return;
                case 21975:
                    this.t.D = (float) d2;
                    return;
                case 21976:
                    this.t.E = (float) d2;
                    return;
                case 21977:
                    this.t.F = (float) d2;
                    return;
                case 21978:
                    this.t.G = (float) d2;
                    return;
                default:
                    return;
            }
        } else {
            this.r = (long) d2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, String str) throws ParserException {
        if (i2 == 134) {
            this.t.b = str;
        } else if (i2 != 17026) {
            if (i2 == 21358) {
                this.t.f3268a = str;
            } else if (i2 == 2274716) {
                String unused = this.t.P = str;
            }
        } else if (!"webm".equals(str) && !"matroska".equals(str)) {
            throw new ParserException("DocType " + str + " not supported");
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01fd, code lost:
        throw new com.google.android.exoplayer2.ParserException("EBML lacing sample size out of range.");
     */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x024b  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0253  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r20, int r21, com.google.android.exoplayer2.extractor.ExtractorInput r22) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r22
            r4 = 161(0xa1, float:2.26E-43)
            r5 = 163(0xa3, float:2.28E-43)
            r6 = 4
            r7 = 0
            r8 = 1
            if (r1 == r4) goto L_0x0094
            if (r1 == r5) goto L_0x0094
            r4 = 16981(0x4255, float:2.3795E-41)
            if (r1 == r4) goto L_0x0087
            r4 = 18402(0x47e2, float:2.5787E-41)
            if (r1 == r4) goto L_0x0077
            r4 = 21419(0x53ab, float:3.0014E-41)
            if (r1 == r4) goto L_0x0058
            r4 = 25506(0x63a2, float:3.5742E-41)
            if (r1 == r4) goto L_0x004b
            r4 = 30322(0x7672, float:4.249E-41)
            if (r1 != r4) goto L_0x0034
            com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$Track r1 = r0.t
            byte[] r4 = new byte[r2]
            r1.p = r4
            byte[] r1 = r1.p
            r3.readFully(r1, r7, r2)
            goto L_0x02b1
        L_0x0034:
            com.google.android.exoplayer2.ParserException r2 = new com.google.android.exoplayer2.ParserException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unexpected id: "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>((java.lang.String) r1)
            throw r2
        L_0x004b:
            com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$Track r1 = r0.t
            byte[] r4 = new byte[r2]
            r1.i = r4
            byte[] r1 = r1.i
            r3.readFully(r1, r7, r2)
            goto L_0x02b1
        L_0x0058:
            com.google.android.exoplayer2.util.ParsableByteArray r1 = r0.i
            byte[] r1 = r1.f3641a
            java.util.Arrays.fill(r1, r7)
            com.google.android.exoplayer2.util.ParsableByteArray r1 = r0.i
            byte[] r1 = r1.f3641a
            int r6 = r6 - r2
            r3.readFully(r1, r6, r2)
            com.google.android.exoplayer2.util.ParsableByteArray r1 = r0.i
            r1.e(r7)
            com.google.android.exoplayer2.util.ParsableByteArray r1 = r0.i
            long r1 = r1.v()
            int r2 = (int) r1
            r0.v = r2
            goto L_0x02b1
        L_0x0077:
            byte[] r1 = new byte[r2]
            r3.readFully(r1, r7, r2)
            com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$Track r2 = r0.t
            com.google.android.exoplayer2.extractor.TrackOutput$CryptoData r3 = new com.google.android.exoplayer2.extractor.TrackOutput$CryptoData
            r3.<init>(r8, r1, r7, r7)
            r2.h = r3
            goto L_0x02b1
        L_0x0087:
            com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$Track r1 = r0.t
            byte[] r4 = new byte[r2]
            r1.g = r4
            byte[] r1 = r1.g
            r3.readFully(r1, r7, r2)
            goto L_0x02b1
        L_0x0094:
            int r4 = r0.E
            r9 = 8
            if (r4 != 0) goto L_0x00b9
            com.google.android.exoplayer2.extractor.mkv.VarintReader r4 = r0.b
            long r10 = r4.a(r3, r7, r8, r9)
            int r4 = (int) r10
            r0.K = r4
            com.google.android.exoplayer2.extractor.mkv.VarintReader r4 = r0.b
            int r4 = r4.a()
            r0.L = r4
            r10 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r0.G = r10
            r0.E = r8
            com.google.android.exoplayer2.util.ParsableByteArray r4 = r0.g
            r4.B()
        L_0x00b9:
            android.util.SparseArray<com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$Track> r4 = r0.c
            int r10 = r0.K
            java.lang.Object r4 = r4.get(r10)
            com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor$Track r4 = (com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.Track) r4
            if (r4 != 0) goto L_0x00cf
            int r1 = r0.L
            int r1 = r2 - r1
            r3.c(r1)
            r0.E = r7
            return
        L_0x00cf:
            int r10 = r0.E
            if (r10 != r8) goto L_0x027e
            r10 = 3
            r0.a((com.google.android.exoplayer2.extractor.ExtractorInput) r3, (int) r10)
            com.google.android.exoplayer2.util.ParsableByteArray r11 = r0.g
            byte[] r11 = r11.f3641a
            r12 = 2
            byte r11 = r11[r12]
            r11 = r11 & 6
            int r11 = r11 >> r8
            r13 = 255(0xff, float:3.57E-43)
            if (r11 != 0) goto L_0x00fa
            r0.I = r8
            int[] r6 = r0.J
            int[] r6 = a((int[]) r6, (int) r8)
            r0.J = r6
            int[] r6 = r0.J
            int r11 = r0.L
            int r2 = r2 - r11
            int r2 = r2 - r10
            r6[r7] = r2
        L_0x00f7:
            r6 = 1
            goto L_0x0211
        L_0x00fa:
            if (r1 != r5) goto L_0x0276
            r0.a((com.google.android.exoplayer2.extractor.ExtractorInput) r3, (int) r6)
            com.google.android.exoplayer2.util.ParsableByteArray r14 = r0.g
            byte[] r14 = r14.f3641a
            byte r14 = r14[r10]
            r14 = r14 & r13
            int r14 = r14 + r8
            r0.I = r14
            int[] r14 = r0.J
            int r15 = r0.I
            int[] r14 = a((int[]) r14, (int) r15)
            r0.J = r14
            if (r11 != r12) goto L_0x0122
            int r10 = r0.L
            int r2 = r2 - r10
            int r2 = r2 - r6
            int r6 = r0.I
            int r2 = r2 / r6
            int[] r10 = r0.J
            java.util.Arrays.fill(r10, r7, r6, r2)
            goto L_0x00f7
        L_0x0122:
            if (r11 != r8) goto L_0x0159
            r6 = 0
            r10 = 4
            r11 = 0
        L_0x0127:
            int r14 = r0.I
            int r15 = r14 + -1
            if (r6 >= r15) goto L_0x014e
            int[] r14 = r0.J
            r14[r6] = r7
        L_0x0131:
            int r10 = r10 + r8
            r0.a((com.google.android.exoplayer2.extractor.ExtractorInput) r3, (int) r10)
            com.google.android.exoplayer2.util.ParsableByteArray r14 = r0.g
            byte[] r14 = r14.f3641a
            int r15 = r10 + -1
            byte r14 = r14[r15]
            r14 = r14 & r13
            int[] r15 = r0.J
            r16 = r15[r6]
            int r16 = r16 + r14
            r15[r6] = r16
            if (r14 == r13) goto L_0x0131
            r14 = r15[r6]
            int r11 = r11 + r14
            int r6 = r6 + 1
            goto L_0x0127
        L_0x014e:
            int[] r6 = r0.J
            int r14 = r14 - r8
            int r15 = r0.L
            int r2 = r2 - r15
            int r2 = r2 - r10
            int r2 = r2 - r11
            r6[r14] = r2
            goto L_0x00f7
        L_0x0159:
            if (r11 != r10) goto L_0x025f
            r6 = 0
            r10 = 4
            r11 = 0
        L_0x015e:
            int r14 = r0.I
            int r15 = r14 + -1
            if (r6 >= r15) goto L_0x0206
            int[] r14 = r0.J
            r14[r6] = r7
            int r10 = r10 + 1
            r0.a((com.google.android.exoplayer2.extractor.ExtractorInput) r3, (int) r10)
            com.google.android.exoplayer2.util.ParsableByteArray r14 = r0.g
            byte[] r14 = r14.f3641a
            int r15 = r10 + -1
            byte r14 = r14[r15]
            if (r14 == 0) goto L_0x01fe
            r16 = 0
            r14 = 0
        L_0x017a:
            if (r14 >= r9) goto L_0x01c9
            int r18 = 7 - r14
            int r5 = r8 << r18
            com.google.android.exoplayer2.util.ParsableByteArray r12 = r0.g
            byte[] r12 = r12.f3641a
            byte r12 = r12[r15]
            r12 = r12 & r5
            if (r12 == 0) goto L_0x01bf
            int r10 = r10 + r14
            r0.a((com.google.android.exoplayer2.extractor.ExtractorInput) r3, (int) r10)
            com.google.android.exoplayer2.util.ParsableByteArray r12 = r0.g
            byte[] r12 = r12.f3641a
            int r16 = r15 + 1
            byte r12 = r12[r15]
            r12 = r12 & r13
            int r5 = ~r5
            r5 = r5 & r12
            long r7 = (long) r5
            r5 = r16
        L_0x019b:
            r16 = r7
            if (r5 >= r10) goto L_0x01b1
            long r7 = r16 << r9
            com.google.android.exoplayer2.util.ParsableByteArray r15 = r0.g
            byte[] r15 = r15.f3641a
            int r16 = r5 + 1
            byte r5 = r15[r5]
            r5 = r5 & r13
            long r12 = (long) r5
            long r7 = r7 | r12
            r5 = r16
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x019b
        L_0x01b1:
            if (r6 <= 0) goto L_0x01c9
            int r14 = r14 * 7
            int r14 = r14 + 6
            r7 = 1
            long r12 = r7 << r14
            long r12 = r12 - r7
            long r16 = r16 - r12
            goto L_0x01c9
        L_0x01bf:
            int r14 = r14 + 1
            r5 = 163(0xa3, float:2.28E-43)
            r7 = 0
            r8 = 1
            r12 = 2
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x017a
        L_0x01c9:
            r7 = r16
            r12 = -2147483648(0xffffffff80000000, double:NaN)
            int r5 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r5 < 0) goto L_0x01f6
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r5 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r5 > 0) goto L_0x01f6
            int r5 = (int) r7
            int[] r7 = r0.J
            if (r6 != 0) goto L_0x01df
            goto L_0x01e4
        L_0x01df:
            int r8 = r6 + -1
            r8 = r7[r8]
            int r5 = r5 + r8
        L_0x01e4:
            r7[r6] = r5
            int[] r5 = r0.J
            r5 = r5[r6]
            int r11 = r11 + r5
            int r6 = r6 + 1
            r5 = 163(0xa3, float:2.28E-43)
            r7 = 0
            r8 = 1
            r12 = 2
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x015e
        L_0x01f6:
            com.google.android.exoplayer2.ParserException r1 = new com.google.android.exoplayer2.ParserException
            java.lang.String r2 = "EBML lacing sample size out of range."
            r1.<init>((java.lang.String) r2)
            throw r1
        L_0x01fe:
            com.google.android.exoplayer2.ParserException r1 = new com.google.android.exoplayer2.ParserException
            java.lang.String r2 = "No valid varint length mask found"
            r1.<init>((java.lang.String) r2)
            throw r1
        L_0x0206:
            int[] r5 = r0.J
            r6 = 1
            int r14 = r14 - r6
            int r7 = r0.L
            int r2 = r2 - r7
            int r2 = r2 - r10
            int r2 = r2 - r11
            r5[r14] = r2
        L_0x0211:
            com.google.android.exoplayer2.util.ParsableByteArray r2 = r0.g
            byte[] r2 = r2.f3641a
            r5 = 0
            byte r7 = r2[r5]
            int r5 = r7 << 8
            byte r2 = r2[r6]
            r6 = 255(0xff, float:3.57E-43)
            r2 = r2 & r6
            r2 = r2 | r5
            long r5 = r0.A
            long r7 = (long) r2
            long r7 = r0.a((long) r7)
            long r5 = r5 + r7
            r0.F = r5
            com.google.android.exoplayer2.util.ParsableByteArray r2 = r0.g
            byte[] r2 = r2.f3641a
            r5 = 2
            byte r2 = r2[r5]
            r2 = r2 & r9
            if (r2 != r9) goto L_0x0236
            r2 = 1
            goto L_0x0237
        L_0x0236:
            r2 = 0
        L_0x0237:
            int r6 = r4.d
            if (r6 == r5) goto L_0x024d
            r6 = 163(0xa3, float:2.28E-43)
            if (r1 != r6) goto L_0x024b
            com.google.android.exoplayer2.util.ParsableByteArray r6 = r0.g
            byte[] r6 = r6.f3641a
            byte r6 = r6[r5]
            r5 = 128(0x80, float:1.794E-43)
            r6 = r6 & r5
            if (r6 != r5) goto L_0x024b
            goto L_0x024d
        L_0x024b:
            r5 = 0
            goto L_0x024e
        L_0x024d:
            r5 = 1
        L_0x024e:
            if (r2 == 0) goto L_0x0253
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x0254
        L_0x0253:
            r7 = 0
        L_0x0254:
            r2 = r5 | r7
            r0.M = r2
            r2 = 2
            r0.E = r2
            r2 = 0
            r0.H = r2
            goto L_0x027e
        L_0x025f:
            com.google.android.exoplayer2.ParserException r1 = new com.google.android.exoplayer2.ParserException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unexpected lacing value: "
            r2.append(r3)
            r2.append(r11)
            java.lang.String r2 = r2.toString()
            r1.<init>((java.lang.String) r2)
            throw r1
        L_0x0276:
            com.google.android.exoplayer2.ParserException r1 = new com.google.android.exoplayer2.ParserException
            java.lang.String r2 = "Lacing only supported in SimpleBlocks."
            r1.<init>((java.lang.String) r2)
            throw r1
        L_0x027e:
            r2 = 163(0xa3, float:2.28E-43)
            if (r1 != r2) goto L_0x02a9
        L_0x0282:
            int r1 = r0.H
            int r2 = r0.I
            if (r1 >= r2) goto L_0x02a5
            int[] r2 = r0.J
            r1 = r2[r1]
            r0.a((com.google.android.exoplayer2.extractor.ExtractorInput) r3, (com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.Track) r4, (int) r1)
            long r1 = r0.F
            int r5 = r0.H
            int r6 = r4.e
            int r5 = r5 * r6
            int r5 = r5 / 1000
            long r5 = (long) r5
            long r1 = r1 + r5
            r0.a((com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.Track) r4, (long) r1)
            int r1 = r0.H
            r2 = 1
            int r1 = r1 + r2
            r0.H = r1
            goto L_0x0282
        L_0x02a5:
            r1 = 0
            r0.E = r1
            goto L_0x02b1
        L_0x02a9:
            r1 = 0
            int[] r2 = r0.J
            r1 = r2[r1]
            r0.a((com.google.android.exoplayer2.extractor.ExtractorInput) r3, (com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.Track) r4, (int) r1)
        L_0x02b1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor.a(int, int, com.google.android.exoplayer2.extractor.ExtractorInput):void");
    }

    private void a(Track track, long j2) {
        Track track2 = track;
        TrueHdSampleRechunker trueHdSampleRechunker = track2.M;
        if (trueHdSampleRechunker != null) {
            trueHdSampleRechunker.a(track2, j2);
        } else {
            long j3 = j2;
            if ("S_TEXT/UTF8".equals(track2.b)) {
                a(track, "%02d:%02d:%02d,%03d", 19, 1000, a0);
            } else if ("S_TEXT/ASS".equals(track2.b)) {
                a(track, "%01d:%02d:%02d:%02d", 21, 10000, d0);
            }
            track2.Q.a(j2, this.M, this.V, 0, track2.h);
        }
        this.W = true;
        e();
    }

    private void a(ExtractorInput extractorInput, int i2) throws IOException, InterruptedException {
        if (this.g.d() < i2) {
            if (this.g.b() < i2) {
                ParsableByteArray parsableByteArray = this.g;
                byte[] bArr = parsableByteArray.f3641a;
                parsableByteArray.a(Arrays.copyOf(bArr, Math.max(bArr.length * 2, i2)), this.g.d());
            }
            ParsableByteArray parsableByteArray2 = this.g;
            extractorInput.readFully(parsableByteArray2.f3641a, parsableByteArray2.d(), i2 - this.g.d());
            this.g.d(i2);
        }
    }

    private void a(ExtractorInput extractorInput, Track track, int i2) throws IOException, InterruptedException {
        int i3;
        if ("S_TEXT/UTF8".equals(track.b)) {
            a(extractorInput, Z, i2);
        } else if ("S_TEXT/ASS".equals(track.b)) {
            a(extractorInput, c0, i2);
        } else {
            TrackOutput trackOutput = track.Q;
            boolean z2 = true;
            if (!this.O) {
                if (track.f) {
                    this.M &= -1073741825;
                    int i4 = 128;
                    if (!this.P) {
                        extractorInput.readFully(this.g.f3641a, 0, 1);
                        this.N++;
                        byte[] bArr = this.g.f3641a;
                        if ((bArr[0] & 128) != 128) {
                            this.S = bArr[0];
                            this.P = true;
                        } else {
                            throw new ParserException("Extension bit is set in signal byte");
                        }
                    }
                    if ((this.S & 1) == 1) {
                        boolean z3 = (this.S & 2) == 2;
                        this.M |= 1073741824;
                        if (!this.Q) {
                            extractorInput.readFully(this.l.f3641a, 0, 8);
                            this.N += 8;
                            this.Q = true;
                            byte[] bArr2 = this.g.f3641a;
                            if (!z3) {
                                i4 = 0;
                            }
                            bArr2[0] = (byte) (i4 | 8);
                            this.g.e(0);
                            trackOutput.a(this.g, 1);
                            this.V++;
                            this.l.e(0);
                            trackOutput.a(this.l, 8);
                            this.V += 8;
                        }
                        if (z3) {
                            if (!this.R) {
                                extractorInput.readFully(this.g.f3641a, 0, 1);
                                this.N++;
                                this.g.e(0);
                                this.T = this.g.t();
                                this.R = true;
                            }
                            int i5 = this.T * 4;
                            this.g.c(i5);
                            extractorInput.readFully(this.g.f3641a, 0, i5);
                            this.N += i5;
                            short s2 = (short) ((this.T / 2) + 1);
                            int i6 = (s2 * 6) + 2;
                            ByteBuffer byteBuffer = this.n;
                            if (byteBuffer == null || byteBuffer.capacity() < i6) {
                                this.n = ByteBuffer.allocate(i6);
                            }
                            this.n.position(0);
                            this.n.putShort(s2);
                            int i7 = 0;
                            int i8 = 0;
                            while (true) {
                                i3 = this.T;
                                if (i7 >= i3) {
                                    break;
                                }
                                int x2 = this.g.x();
                                if (i7 % 2 == 0) {
                                    this.n.putShort((short) (x2 - i8));
                                } else {
                                    this.n.putInt(x2 - i8);
                                }
                                i7++;
                                i8 = x2;
                            }
                            int i9 = (i2 - this.N) - i8;
                            if (i3 % 2 == 1) {
                                this.n.putInt(i9);
                            } else {
                                this.n.putShort((short) i9);
                                this.n.putInt(0);
                            }
                            this.m.a(this.n.array(), i6);
                            trackOutput.a(this.m, i6);
                            this.V += i6;
                        }
                    }
                } else {
                    byte[] bArr3 = track.g;
                    if (bArr3 != null) {
                        this.j.a(bArr3, bArr3.length);
                    }
                }
                this.O = true;
            }
            int d2 = i2 + this.j.d();
            if (!"V_MPEG4/ISO/AVC".equals(track.b) && !"V_MPEGH/ISO/HEVC".equals(track.b)) {
                if (track.M != null) {
                    if (this.j.d() != 0) {
                        z2 = false;
                    }
                    Assertions.b(z2);
                    track.M.a(extractorInput, this.M, d2);
                }
                while (true) {
                    int i10 = this.N;
                    if (i10 >= d2) {
                        break;
                    }
                    a(extractorInput, trackOutput, d2 - i10);
                }
            } else {
                byte[] bArr4 = this.f.f3641a;
                bArr4[0] = 0;
                bArr4[1] = 0;
                bArr4[2] = 0;
                int i11 = track.R;
                int i12 = 4 - i11;
                while (this.N < d2) {
                    int i13 = this.U;
                    if (i13 == 0) {
                        a(extractorInput, bArr4, i12, i11);
                        this.f.e(0);
                        this.U = this.f.x();
                        this.e.e(0);
                        trackOutput.a(this.e, 4);
                        this.V += 4;
                    } else {
                        this.U = i13 - a(extractorInput, trackOutput, i13);
                    }
                }
            }
            if ("A_VORBIS".equals(track.b)) {
                this.h.e(0);
                trackOutput.a(this.h, 4);
                this.V += 4;
            }
        }
    }

    private void a(ExtractorInput extractorInput, byte[] bArr, int i2) throws IOException, InterruptedException {
        int length = bArr.length + i2;
        if (this.k.b() < length) {
            this.k.f3641a = Arrays.copyOf(bArr, length + i2);
        } else {
            System.arraycopy(bArr, 0, this.k.f3641a, 0, bArr.length);
        }
        extractorInput.readFully(this.k.f3641a, bArr.length, i2);
        this.k.c(length);
    }

    private void a(Track track, String str, int i2, long j2, byte[] bArr) {
        a(this.k.f3641a, this.G, str, i2, j2, bArr);
        TrackOutput trackOutput = track.Q;
        ParsableByteArray parsableByteArray = this.k;
        trackOutput.a(parsableByteArray, parsableByteArray.d());
        this.V += this.k.d();
    }

    private static void a(byte[] bArr, long j2, String str, int i2, long j3, byte[] bArr2) {
        byte[] bArr3;
        byte[] bArr4;
        if (j2 == -9223372036854775807L) {
            bArr4 = bArr2;
            bArr3 = bArr4;
        } else {
            int i3 = (int) (j2 / 3600000000L);
            long j4 = j2 - (((long) (i3 * DateTimeConstants.SECONDS_PER_HOUR)) * 1000000);
            int i4 = (int) (j4 / 60000000);
            long j5 = j4 - (((long) (i4 * 60)) * 1000000);
            int i5 = (int) (j5 / 1000000);
            int i6 = (int) ((j5 - (((long) i5) * 1000000)) / j3);
            Locale locale = Locale.US;
            Object[] objArr = {Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6)};
            String str2 = str;
            bArr3 = Util.d(String.format(locale, str, objArr));
            bArr4 = bArr2;
        }
        byte[] bArr5 = bArr;
        int i7 = i2;
        System.arraycopy(bArr3, 0, bArr, i2, bArr4.length);
    }

    private void a(ExtractorInput extractorInput, byte[] bArr, int i2, int i3) throws IOException, InterruptedException {
        int min = Math.min(i3, this.j.a());
        extractorInput.readFully(bArr, i2 + min, i3 - min);
        if (min > 0) {
            this.j.a(bArr, i2, min);
        }
        this.N += i3;
    }

    private int a(ExtractorInput extractorInput, TrackOutput trackOutput, int i2) throws IOException, InterruptedException {
        int i3;
        int a2 = this.j.a();
        if (a2 > 0) {
            i3 = Math.min(i2, a2);
            trackOutput.a(this.j, i3);
        } else {
            i3 = trackOutput.a(extractorInput, i2, false);
        }
        this.N += i3;
        this.V += i3;
        return i3;
    }

    private boolean a(PositionHolder positionHolder, long j2) {
        if (this.x) {
            this.z = j2;
            positionHolder.f3254a = this.y;
            this.x = false;
            return true;
        }
        if (this.u) {
            long j3 = this.z;
            if (j3 != -1) {
                positionHolder.f3254a = j3;
                this.z = -1;
                return true;
            }
        }
        return false;
    }

    private long a(long j2) throws ParserException {
        long j3 = this.q;
        if (j3 != -9223372036854775807L) {
            return Util.c(j2, j3, 1000);
        }
        throw new ParserException("Can't scale timecode prior to timecodeScale being set.");
    }

    private static boolean a(String str) {
        return "V_VP8".equals(str) || "V_VP9".equals(str) || "V_MPEG2".equals(str) || "V_MPEG4/ISO/SP".equals(str) || "V_MPEG4/ISO/ASP".equals(str) || "V_MPEG4/ISO/AP".equals(str) || "V_MPEG4/ISO/AVC".equals(str) || "V_MPEGH/ISO/HEVC".equals(str) || "V_MS/VFW/FOURCC".equals(str) || "V_THEORA".equals(str) || "A_OPUS".equals(str) || "A_VORBIS".equals(str) || "A_AAC".equals(str) || "A_MPEG/L2".equals(str) || "A_MPEG/L3".equals(str) || "A_AC3".equals(str) || "A_EAC3".equals(str) || "A_TRUEHD".equals(str) || "A_DTS".equals(str) || "A_DTS/EXPRESS".equals(str) || "A_DTS/LOSSLESS".equals(str) || "A_FLAC".equals(str) || "A_MS/ACM".equals(str) || "A_PCM/INT/LIT".equals(str) || "S_TEXT/UTF8".equals(str) || "S_TEXT/ASS".equals(str) || "S_VOBSUB".equals(str) || "S_HDMV/PGS".equals(str) || "S_DVBSUB".equals(str);
    }

    private static int[] a(int[] iArr, int i2) {
        if (iArr == null) {
            return new int[i2];
        }
        if (iArr.length >= i2) {
            return iArr;
        }
        return new int[Math.max(iArr.length * 2, i2)];
    }
}
