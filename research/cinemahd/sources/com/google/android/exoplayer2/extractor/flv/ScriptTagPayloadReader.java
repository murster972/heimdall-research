package com.google.android.exoplayer2.extractor.flv;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

final class ScriptTagPayloadReader extends TagPayloadReader {
    private long b = -9223372036854775807L;

    public ScriptTagPayloadReader() {
        super((TrackOutput) null);
    }

    private static Date c(ParsableByteArray parsableByteArray) {
        Date date = new Date((long) d(parsableByteArray).doubleValue());
        parsableByteArray.f(2);
        return date;
    }

    private static Double d(ParsableByteArray parsableByteArray) {
        return Double.valueOf(Double.longBitsToDouble(parsableByteArray.p()));
    }

    private static HashMap<String, Object> e(ParsableByteArray parsableByteArray) {
        int x = parsableByteArray.x();
        HashMap<String, Object> hashMap = new HashMap<>(x);
        for (int i = 0; i < x; i++) {
            hashMap.put(h(parsableByteArray), a(parsableByteArray, i(parsableByteArray)));
        }
        return hashMap;
    }

    private static HashMap<String, Object> f(ParsableByteArray parsableByteArray) {
        HashMap<String, Object> hashMap = new HashMap<>();
        while (true) {
            String h = h(parsableByteArray);
            int i = i(parsableByteArray);
            if (i == 9) {
                return hashMap;
            }
            hashMap.put(h, a(parsableByteArray, i));
        }
    }

    private static ArrayList<Object> g(ParsableByteArray parsableByteArray) {
        int x = parsableByteArray.x();
        ArrayList<Object> arrayList = new ArrayList<>(x);
        for (int i = 0; i < x; i++) {
            arrayList.add(a(parsableByteArray, i(parsableByteArray)));
        }
        return arrayList;
    }

    private static String h(ParsableByteArray parsableByteArray) {
        int z = parsableByteArray.z();
        int c = parsableByteArray.c();
        parsableByteArray.f(z);
        return new String(parsableByteArray.f3641a, c, z);
    }

    private static int i(ParsableByteArray parsableByteArray) {
        return parsableByteArray.t();
    }

    public long a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public boolean a(ParsableByteArray parsableByteArray) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void b(ParsableByteArray parsableByteArray, long j) throws ParserException {
        if (i(parsableByteArray) != 2) {
            throw new ParserException();
        } else if ("onMetaData".equals(h(parsableByteArray)) && i(parsableByteArray) == 8) {
            HashMap<String, Object> e = e(parsableByteArray);
            if (e.containsKey("duration")) {
                double doubleValue = ((Double) e.get("duration")).doubleValue();
                if (doubleValue > 0.0d) {
                    this.b = (long) (doubleValue * 1000000.0d);
                }
            }
        }
    }

    private static Object a(ParsableByteArray parsableByteArray, int i) {
        if (i == 0) {
            return d(parsableByteArray);
        }
        if (i == 1) {
            return b(parsableByteArray);
        }
        if (i == 2) {
            return h(parsableByteArray);
        }
        if (i == 3) {
            return f(parsableByteArray);
        }
        if (i == 8) {
            return e(parsableByteArray);
        }
        if (i == 10) {
            return g(parsableByteArray);
        }
        if (i != 11) {
            return null;
        }
        return c(parsableByteArray);
    }

    private static Boolean b(ParsableByteArray parsableByteArray) {
        boolean z = true;
        if (parsableByteArray.t() != 1) {
            z = false;
        }
        return Boolean.valueOf(z);
    }
}
