package com.google.android.exoplayer2.extractor.mp4;

import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.nio.ByteBuffer;
import java.util.UUID;
import okhttp3.internal.http2.Http2Connection;

public final class PsshAtomUtil {

    private static class PsshAtom {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final UUID f3294a;
        /* access modifiers changed from: private */
        public final int b;
        /* access modifiers changed from: private */
        public final byte[] c;

        public PsshAtom(UUID uuid, int i, byte[] bArr) {
            this.f3294a = uuid;
            this.b = i;
            this.c = bArr;
        }
    }

    private PsshAtomUtil() {
    }

    public static byte[] a(UUID uuid, byte[] bArr) {
        return a(uuid, (UUID[]) null, bArr);
    }

    private static PsshAtom b(byte[] bArr) {
        ParsableByteArray parsableByteArray = new ParsableByteArray(bArr);
        if (parsableByteArray.d() < 32) {
            return null;
        }
        parsableByteArray.e(0);
        if (parsableByteArray.h() != parsableByteArray.a() + 4 || parsableByteArray.h() != Atom.V) {
            return null;
        }
        int c = Atom.c(parsableByteArray.h());
        if (c > 1) {
            Log.d("PsshAtomUtil", "Unsupported pssh version: " + c);
            return null;
        }
        UUID uuid = new UUID(parsableByteArray.p(), parsableByteArray.p());
        if (c == 1) {
            parsableByteArray.f(parsableByteArray.x() * 16);
        }
        int x = parsableByteArray.x();
        if (x != parsableByteArray.a()) {
            return null;
        }
        byte[] bArr2 = new byte[x];
        parsableByteArray.a(bArr2, 0, x);
        return new PsshAtom(uuid, c, bArr2);
    }

    public static UUID c(byte[] bArr) {
        PsshAtom b = b(bArr);
        if (b == null) {
            return null;
        }
        return b.f3294a;
    }

    public static int d(byte[] bArr) {
        PsshAtom b = b(bArr);
        if (b == null) {
            return -1;
        }
        return b.b;
    }

    public static byte[] a(UUID uuid, UUID[] uuidArr, byte[] bArr) {
        int length = (bArr != null ? bArr.length : 0) + 32;
        if (uuidArr != null) {
            length += (uuidArr.length * 16) + 4;
        }
        ByteBuffer allocate = ByteBuffer.allocate(length);
        allocate.putInt(length);
        allocate.putInt(Atom.V);
        allocate.putInt(uuidArr != null ? Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE : 0);
        allocate.putLong(uuid.getMostSignificantBits());
        allocate.putLong(uuid.getLeastSignificantBits());
        if (uuidArr != null) {
            allocate.putInt(uuidArr.length);
            for (UUID uuid2 : uuidArr) {
                allocate.putLong(uuid2.getMostSignificantBits());
                allocate.putLong(uuid2.getLeastSignificantBits());
            }
        }
        if (!(bArr == null || bArr.length == 0)) {
            allocate.putInt(bArr.length);
            allocate.put(bArr);
        }
        return allocate.array();
    }

    public static boolean a(byte[] bArr) {
        return b(bArr) != null;
    }

    public static byte[] a(byte[] bArr, UUID uuid) {
        PsshAtom b = b(bArr);
        if (b == null) {
            return null;
        }
        if (uuid == null || uuid.equals(b.f3294a)) {
            return b.c;
        }
        Log.d("PsshAtomUtil", "UUID mismatch. Expected: " + uuid + ", got: " + b.f3294a + ".");
        return null;
    }
}
