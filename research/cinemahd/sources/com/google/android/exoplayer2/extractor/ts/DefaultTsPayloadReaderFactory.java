package com.google.android.exoplayer2.extractor.ts;

import android.util.SparseArray;
import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.text.cea.Cea708InitializationData;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class DefaultTsPayloadReaderFactory implements TsPayloadReader.Factory {

    /* renamed from: a  reason: collision with root package name */
    private final int f3320a;
    private final List<Format> b;

    public DefaultTsPayloadReaderFactory() {
        this(0);
    }

    private UserDataReader b(TsPayloadReader.EsInfo esInfo) {
        return new UserDataReader(c(esInfo));
    }

    private List<Format> c(TsPayloadReader.EsInfo esInfo) {
        int i;
        String str;
        List<byte[]> list;
        if (a(32)) {
            return this.b;
        }
        ParsableByteArray parsableByteArray = new ParsableByteArray(esInfo.d);
        List<Format> list2 = this.b;
        while (parsableByteArray.a() > 0) {
            int t = parsableByteArray.t();
            int c = parsableByteArray.c() + parsableByteArray.t();
            if (t == 134) {
                list2 = new ArrayList<>();
                int t2 = parsableByteArray.t() & 31;
                for (int i2 = 0; i2 < t2; i2++) {
                    String b2 = parsableByteArray.b(3);
                    int t3 = parsableByteArray.t();
                    boolean z = true;
                    boolean z2 = (t3 & 128) != 0;
                    if (z2) {
                        i = t3 & 63;
                        str = "application/cea-708";
                    } else {
                        str = "application/cea-608";
                        i = 1;
                    }
                    byte t4 = (byte) parsableByteArray.t();
                    parsableByteArray.f(1);
                    if (z2) {
                        if ((t4 & 64) == 0) {
                            z = false;
                        }
                        list = Cea708InitializationData.a(z);
                    } else {
                        list = null;
                    }
                    list2.add(Format.a((String) null, str, (String) null, -1, 0, b2, i, (DrmInitData) null, Clock.MAX_TIME, list));
                }
            }
            parsableByteArray.e(c);
        }
        return list2;
    }

    public SparseArray<TsPayloadReader> a() {
        return new SparseArray<>();
    }

    public DefaultTsPayloadReaderFactory(int i) {
        this(i, Collections.singletonList(Format.a((String) null, "application/cea-608", 0, (String) null)));
    }

    public TsPayloadReader a(int i, TsPayloadReader.EsInfo esInfo) {
        if (i == 2) {
            return new PesReader(new H262Reader(b(esInfo)));
        }
        if (i == 3 || i == 4) {
            return new PesReader(new MpegAudioReader(esInfo.b));
        }
        if (i != 15) {
            if (i != 17) {
                if (i == 21) {
                    return new PesReader(new Id3Reader());
                }
                if (i != 27) {
                    if (i == 36) {
                        return new PesReader(new H265Reader(a(esInfo)));
                    }
                    if (i == 89) {
                        return new PesReader(new DvbSubtitleReader(esInfo.c));
                    }
                    if (i != 138) {
                        if (i != 129) {
                            if (i != 130) {
                                if (i != 134) {
                                    if (i != 135) {
                                        return null;
                                    }
                                } else if (a(16)) {
                                    return null;
                                } else {
                                    return new SectionReader(new SpliceInfoSectionReader());
                                }
                            }
                        }
                        return new PesReader(new Ac3Reader(esInfo.b));
                    }
                    return new PesReader(new DtsReader(esInfo.b));
                } else if (a(4)) {
                    return null;
                } else {
                    return new PesReader(new H264Reader(a(esInfo), a(1), a(8)));
                }
            } else if (a(2)) {
                return null;
            } else {
                return new PesReader(new LatmReader(esInfo.b));
            }
        } else if (a(2)) {
            return null;
        } else {
            return new PesReader(new AdtsReader(false, esInfo.b));
        }
    }

    public DefaultTsPayloadReaderFactory(int i, List<Format> list) {
        this.f3320a = i;
        this.b = list;
    }

    private SeiReader a(TsPayloadReader.EsInfo esInfo) {
        return new SeiReader(c(esInfo));
    }

    private boolean a(int i) {
        return (i & this.f3320a) != 0;
    }
}
