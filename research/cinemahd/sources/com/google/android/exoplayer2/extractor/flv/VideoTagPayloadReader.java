package com.google.android.exoplayer2.extractor.flv;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.flv.TagPayloadReader;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.video.AvcConfig;

final class VideoTagPayloadReader extends TagPayloadReader {
    private final ParsableByteArray b = new ParsableByteArray(NalUnitUtil.f3637a);
    private final ParsableByteArray c = new ParsableByteArray(4);
    private int d;
    private boolean e;
    private int f;

    public VideoTagPayloadReader(TrackOutput trackOutput) {
        super(trackOutput);
    }

    /* access modifiers changed from: protected */
    public boolean a(ParsableByteArray parsableByteArray) throws TagPayloadReader.UnsupportedFormatException {
        int t = parsableByteArray.t();
        int i = (t >> 4) & 15;
        int i2 = t & 15;
        if (i2 == 7) {
            this.f = i;
            return i != 5;
        }
        throw new TagPayloadReader.UnsupportedFormatException("Video format not supported: " + i2);
    }

    /* access modifiers changed from: protected */
    public void b(ParsableByteArray parsableByteArray, long j) throws ParserException {
        int t = parsableByteArray.t();
        long i = j + (((long) parsableByteArray.i()) * 1000);
        if (t == 0 && !this.e) {
            ParsableByteArray parsableByteArray2 = new ParsableByteArray(new byte[parsableByteArray.a()]);
            parsableByteArray.a(parsableByteArray2.f3641a, 0, parsableByteArray.a());
            AvcConfig b2 = AvcConfig.b(parsableByteArray2);
            this.d = b2.b;
            this.f3262a.a(Format.a((String) null, "video/avc", (String) null, -1, -1, b2.c, b2.d, -1.0f, b2.f3656a, -1, b2.e, (DrmInitData) null));
            this.e = true;
        } else if (t == 1 && this.e) {
            byte[] bArr = this.c.f3641a;
            bArr[0] = 0;
            bArr[1] = 0;
            bArr[2] = 0;
            int i2 = 4 - this.d;
            int i3 = 0;
            while (parsableByteArray.a() > 0) {
                parsableByteArray.a(this.c.f3641a, i2, this.d);
                this.c.e(0);
                int x = this.c.x();
                this.b.e(0);
                this.f3262a.a(this.b, 4);
                this.f3262a.a(parsableByteArray, x);
                i3 = i3 + 4 + x;
            }
            this.f3262a.a(i, this.f == 1 ? 1 : 0, i3, 0, (TrackOutput.CryptoData) null);
        }
    }
}
