package com.google.android.exoplayer2.extractor.mp4;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

final class Sniffer {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f3295a = {Util.c("isom"), Util.c("iso2"), Util.c("iso3"), Util.c("iso4"), Util.c("iso5"), Util.c("iso6"), Util.c("avc1"), Util.c("hvc1"), Util.c("hev1"), Util.c("mp41"), Util.c("mp42"), Util.c("3g2a"), Util.c("3g2b"), Util.c("3gr6"), Util.c("3gs6"), Util.c("3ge6"), Util.c("3gg6"), Util.c("M4V "), Util.c("M4A "), Util.c("f4v "), Util.c("kddi"), Util.c("M4VP"), Util.c("qt  "), Util.c("MSNV")};

    private Sniffer() {
    }

    public static boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return a(extractorInput, true);
    }

    public static boolean b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return a(extractorInput, false);
    }

    private static boolean a(ExtractorInput extractorInput, boolean z) throws IOException, InterruptedException {
        boolean z2;
        ExtractorInput extractorInput2 = extractorInput;
        long length = extractorInput.getLength();
        long j = -1;
        if (length == -1 || length > PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM) {
            length = 4096;
        }
        int i = (int) length;
        ParsableByteArray parsableByteArray = new ParsableByteArray(64);
        int i2 = 0;
        boolean z3 = false;
        while (true) {
            if (i2 >= i) {
                break;
            }
            parsableByteArray.c(8);
            extractorInput2.a(parsableByteArray.f3641a, 0, 8);
            long v = parsableByteArray.v();
            int h = parsableByteArray.h();
            int i3 = 16;
            if (v == 1) {
                extractorInput2.a(parsableByteArray.f3641a, 8, 8);
                parsableByteArray.d(16);
                v = parsableByteArray.y();
            } else {
                if (v == 0) {
                    long length2 = extractorInput.getLength();
                    if (length2 != j) {
                        v = ((long) 8) + (length2 - extractorInput.getPosition());
                    }
                }
                i3 = 8;
            }
            long j2 = (long) i3;
            if (v < j2) {
                return false;
            }
            i2 += i3;
            if (h != Atom.C) {
                if (h == Atom.L || h == Atom.N) {
                    z2 = true;
                } else if ((((long) i2) + v) - j2 >= ((long) i)) {
                    break;
                } else {
                    int i4 = (int) (v - j2);
                    i2 += i4;
                    if (h == Atom.b) {
                        if (i4 < 8) {
                            return false;
                        }
                        parsableByteArray.c(i4);
                        extractorInput2.a(parsableByteArray.f3641a, 0, i4);
                        int i5 = i4 / 4;
                        int i6 = 0;
                        while (true) {
                            if (i6 >= i5) {
                                break;
                            }
                            if (i6 == 1) {
                                parsableByteArray.f(4);
                            } else if (a(parsableByteArray.h())) {
                                z3 = true;
                                break;
                            }
                            i6++;
                        }
                        if (!z3) {
                            return false;
                        }
                    } else if (i4 != 0) {
                        extractorInput2.a(i4);
                    }
                    j = -1;
                }
            }
        }
        z2 = false;
        if (!z3 || z != z2) {
            return false;
        }
        return true;
    }

    private static boolean a(int i) {
        if ((i >>> 8) == Util.c("3gp")) {
            return true;
        }
        for (int i2 : f3295a) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }
}
