package com.google.android.exoplayer2.extractor.mp4;

import android.util.Pair;
import android.util.SparseArray;
import com.facebook.common.time.Clock;
import com.facebook.common.util.ByteConstants;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ChunkIndex;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.mp4.Atom;
import com.google.android.exoplayer2.text.cea.CeaUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdRequest;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public final class FragmentedMp4Extractor implements Extractor {
    private static final int H = Util.c("seig");
    private static final byte[] I = {-94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12};
    private static final Format J = Format.a((String) null, "application/x-emsg", Clock.MAX_TIME);
    private int A;
    private int B;
    private boolean C;
    private ExtractorOutput D;
    private TrackOutput[] E;
    private TrackOutput[] F;
    private boolean G;

    /* renamed from: a  reason: collision with root package name */
    private final int f3288a;
    private final Track b;
    private final List<Format> c;
    private final DrmInitData d;
    private final SparseArray<TrackBundle> e;
    private final ParsableByteArray f;
    private final ParsableByteArray g;
    private final ParsableByteArray h;
    private final TimestampAdjuster i;
    private final ParsableByteArray j;
    private final byte[] k;
    private final ArrayDeque<Atom.ContainerAtom> l;
    private final ArrayDeque<MetadataSampleInfo> m;
    private final TrackOutput n;
    private int o;
    private int p;
    private long q;
    private int r;
    private ParsableByteArray s;
    private long t;
    private int u;
    private long v;
    private long w;
    private long x;
    private TrackBundle y;
    private int z;

    private static final class MetadataSampleInfo {

        /* renamed from: a  reason: collision with root package name */
        public final long f3289a;
        public final int b;

        public MetadataSampleInfo(long j, int i) {
            this.f3289a = j;
            this.b = i;
        }
    }

    private static final class TrackBundle {

        /* renamed from: a  reason: collision with root package name */
        public final TrackOutput f3290a;
        public final TrackFragment b = new TrackFragment();
        public Track c;
        public DefaultSampleValues d;
        public int e;
        public int f;
        public int g;
        public int h;
        private final ParsableByteArray i = new ParsableByteArray(1);
        private final ParsableByteArray j = new ParsableByteArray();

        public TrackBundle(TrackOutput trackOutput) {
            this.f3290a = trackOutput;
        }

        /* access modifiers changed from: private */
        public TrackEncryptionBox d() {
            TrackFragment trackFragment = this.b;
            int i2 = trackFragment.f3298a.f3286a;
            TrackEncryptionBox trackEncryptionBox = trackFragment.o;
            if (trackEncryptionBox == null) {
                trackEncryptionBox = this.c.a(i2);
            }
            if (trackEncryptionBox == null || !trackEncryptionBox.f3297a) {
                return null;
            }
            return trackEncryptionBox;
        }

        /* access modifiers changed from: private */
        public void e() {
            TrackEncryptionBox d2 = d();
            if (d2 != null) {
                ParsableByteArray parsableByteArray = this.b.q;
                int i2 = d2.d;
                if (i2 != 0) {
                    parsableByteArray.f(i2);
                }
                if (this.b.c(this.e)) {
                    parsableByteArray.f(parsableByteArray.z() * 6);
                }
            }
        }

        public void c() {
            this.b.a();
            this.e = 0;
            this.g = 0;
            this.f = 0;
            this.h = 0;
        }

        public void a(Track track, DefaultSampleValues defaultSampleValues) {
            Assertions.a(track);
            this.c = track;
            Assertions.a(defaultSampleValues);
            this.d = defaultSampleValues;
            this.f3290a.a(track.f);
            c();
        }

        public int b() {
            ParsableByteArray parsableByteArray;
            int i2;
            TrackEncryptionBox d2 = d();
            if (d2 == null) {
                return 0;
            }
            int i3 = d2.d;
            if (i3 != 0) {
                int i4 = i3;
                parsableByteArray = this.b.q;
                i2 = i4;
            } else {
                byte[] bArr = d2.e;
                this.j.a(bArr, bArr.length);
                parsableByteArray = this.j;
                i2 = bArr.length;
            }
            boolean c2 = this.b.c(this.e);
            this.i.f3641a[0] = (byte) ((c2 ? 128 : 0) | i2);
            this.i.e(0);
            this.f3290a.a(this.i, 1);
            this.f3290a.a(parsableByteArray, i2);
            if (!c2) {
                return i2 + 1;
            }
            ParsableByteArray parsableByteArray2 = this.b.q;
            int z = parsableByteArray2.z();
            parsableByteArray2.f(-2);
            int i5 = (z * 6) + 2;
            this.f3290a.a(parsableByteArray2, i5);
            return i2 + 1 + i5;
        }

        public void a(DrmInitData drmInitData) {
            TrackEncryptionBox a2 = this.c.a(this.b.f3298a.f3286a);
            this.f3290a.a(this.c.f.a(drmInitData.a(a2 != null ? a2.b : null)));
        }

        public void a(long j2) {
            long b2 = C.b(j2);
            int i2 = this.e;
            while (true) {
                TrackFragment trackFragment = this.b;
                if (i2 < trackFragment.f && trackFragment.a(i2) < b2) {
                    if (this.b.l[i2]) {
                        this.h = i2;
                    }
                    i2++;
                } else {
                    return;
                }
            }
        }

        public boolean a() {
            this.e++;
            this.f++;
            int i2 = this.f;
            int[] iArr = this.b.h;
            int i3 = this.g;
            if (i2 != iArr[i3]) {
                return true;
            }
            this.g = i3 + 1;
            this.f = 0;
            return false;
        }
    }

    static {
        a aVar = a.f3300a;
    }

    public FragmentedMp4Extractor() {
        this(0);
    }

    static /* synthetic */ Extractor[] b() {
        return new Extractor[]{new FragmentedMp4Extractor()};
    }

    private void c(ExtractorInput extractorInput) throws IOException, InterruptedException {
        int i2 = ((int) this.q) - this.r;
        ParsableByteArray parsableByteArray = this.s;
        if (parsableByteArray != null) {
            extractorInput.readFully(parsableByteArray.f3641a, 8, i2);
            a(new Atom.LeafAtom(this.p, this.s), extractorInput.getPosition());
        } else {
            extractorInput.c(i2);
        }
        b(extractorInput.getPosition());
    }

    private static Pair<Integer, DefaultSampleValues> d(ParsableByteArray parsableByteArray) {
        parsableByteArray.e(12);
        return Pair.create(Integer.valueOf(parsableByteArray.h()), new DefaultSampleValues(parsableByteArray.x() - 1, parsableByteArray.x(), parsableByteArray.x(), parsableByteArray.h()));
    }

    private boolean e(ExtractorInput extractorInput) throws IOException, InterruptedException {
        TrackOutput.CryptoData cryptoData;
        boolean z2;
        int i2;
        ExtractorInput extractorInput2 = extractorInput;
        int i3 = 4;
        int i4 = 1;
        int i5 = 0;
        if (this.o == 3) {
            if (this.y == null) {
                TrackBundle a2 = a(this.e);
                if (a2 == null) {
                    int position = (int) (this.t - extractorInput.getPosition());
                    if (position >= 0) {
                        extractorInput2.c(position);
                        a();
                        return false;
                    }
                    throw new ParserException("Offset to end of mdat was negative.");
                }
                int position2 = (int) (a2.b.g[a2.g] - extractorInput.getPosition());
                if (position2 < 0) {
                    Log.d("FragmentedMp4Extractor", "Ignoring negative offset to sample data.");
                    position2 = 0;
                }
                extractorInput2.c(position2);
                this.y = a2;
            }
            TrackBundle trackBundle = this.y;
            int[] iArr = trackBundle.b.i;
            int i6 = trackBundle.e;
            this.z = iArr[i6];
            if (i6 < trackBundle.h) {
                extractorInput2.c(this.z);
                this.y.e();
                if (!this.y.a()) {
                    this.y = null;
                }
                this.o = 3;
                return true;
            }
            if (trackBundle.c.g == 1) {
                this.z -= 8;
                extractorInput2.c(8);
            }
            this.A = this.y.b();
            this.z += this.A;
            this.o = 4;
            this.B = 0;
        }
        TrackBundle trackBundle2 = this.y;
        TrackFragment trackFragment = trackBundle2.b;
        Track track = trackBundle2.c;
        TrackOutput trackOutput = trackBundle2.f3290a;
        int i7 = trackBundle2.e;
        long a3 = trackFragment.a(i7) * 1000;
        TimestampAdjuster timestampAdjuster = this.i;
        if (timestampAdjuster != null) {
            a3 = timestampAdjuster.a(a3);
        }
        long j2 = a3;
        int i8 = track.j;
        if (i8 == 0) {
            while (true) {
                int i9 = this.A;
                int i10 = this.z;
                if (i9 >= i10) {
                    break;
                }
                this.A += trackOutput.a(extractorInput2, i10 - i9, false);
            }
        } else {
            byte[] bArr = this.g.f3641a;
            bArr[0] = 0;
            bArr[1] = 0;
            bArr[2] = 0;
            int i11 = i8 + 1;
            int i12 = 4 - i8;
            while (this.A < this.z) {
                int i13 = this.B;
                if (i13 == 0) {
                    extractorInput2.readFully(bArr, i12, i11);
                    this.g.e(i5);
                    this.B = this.g.x() - i4;
                    this.f.e(i5);
                    trackOutput.a(this.f, i3);
                    trackOutput.a(this.g, i4);
                    this.C = this.F.length > 0 && NalUnitUtil.a(track.f.g, bArr[i3]);
                    this.A += 5;
                    this.z += i12;
                } else {
                    if (this.C) {
                        this.h.c(i13);
                        extractorInput2.readFully(this.h.f3641a, i5, this.B);
                        trackOutput.a(this.h, this.B);
                        i2 = this.B;
                        ParsableByteArray parsableByteArray = this.h;
                        int c2 = NalUnitUtil.c(parsableByteArray.f3641a, parsableByteArray.d());
                        this.h.e("video/hevc".equals(track.f.g) ? 1 : 0);
                        this.h.d(c2);
                        CeaUtil.a(j2, this.h, this.F);
                    } else {
                        i2 = trackOutput.a(extractorInput2, i13, false);
                    }
                    this.A += i2;
                    this.B -= i2;
                    i3 = 4;
                    i4 = 1;
                    i5 = 0;
                }
            }
        }
        boolean z3 = trackFragment.l[i7];
        TrackEncryptionBox b2 = this.y.d();
        if (b2 != null) {
            z2 = z3 | true;
            cryptoData = b2.c;
        } else {
            z2 = z3;
            cryptoData = null;
        }
        trackOutput.a(j2, z2 ? 1 : 0, this.z, 0, cryptoData);
        a(j2);
        if (!this.y.a()) {
            this.y = null;
        }
        this.o = 3;
        return true;
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return Sniffer.a(extractorInput);
    }

    public void release() {
    }

    public FragmentedMp4Extractor(int i2) {
        this(i2, (TimestampAdjuster) null);
    }

    private boolean b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (this.r == 0) {
            if (!extractorInput.b(this.j.f3641a, 0, 8, true)) {
                return false;
            }
            this.r = 8;
            this.j.e(0);
            this.q = this.j.v();
            this.p = this.j.h();
        }
        long j2 = this.q;
        if (j2 == 1) {
            extractorInput.readFully(this.j.f3641a, 8, 8);
            this.r += 8;
            this.q = this.j.y();
        } else if (j2 == 0) {
            long length = extractorInput.getLength();
            if (length == -1 && !this.l.isEmpty()) {
                length = this.l.peek().V0;
            }
            if (length != -1) {
                this.q = (length - extractorInput.getPosition()) + ((long) this.r);
            }
        }
        if (this.q >= ((long) this.r)) {
            long position = extractorInput.getPosition() - ((long) this.r);
            if (this.p == Atom.L) {
                int size = this.e.size();
                for (int i2 = 0; i2 < size; i2++) {
                    TrackFragment trackFragment = this.e.valueAt(i2).b;
                    trackFragment.b = position;
                    trackFragment.d = position;
                    trackFragment.c = position;
                }
            }
            int i3 = this.p;
            if (i3 == Atom.i) {
                this.y = null;
                this.t = this.q + position;
                if (!this.G) {
                    this.D.a(new SeekMap.Unseekable(this.w, position));
                    this.G = true;
                }
                this.o = 2;
                return true;
            }
            if (a(i3)) {
                long position2 = (extractorInput.getPosition() + this.q) - 8;
                this.l.push(new Atom.ContainerAtom(this.p, position2));
                if (this.q == ((long) this.r)) {
                    b(position2);
                } else {
                    a();
                }
            } else if (b(this.p)) {
                if (this.r == 8) {
                    long j3 = this.q;
                    if (j3 <= 2147483647L) {
                        this.s = new ParsableByteArray((int) j3);
                        System.arraycopy(this.j.f3641a, 0, this.s.f3641a, 0, 8);
                        this.o = 1;
                    } else {
                        throw new ParserException("Leaf atom with length > 2147483647 (unsupported).");
                    }
                } else {
                    throw new ParserException("Leaf atom defines extended atom size (unsupported).");
                }
            } else if (this.q <= 2147483647L) {
                this.s = null;
                this.o = 1;
            } else {
                throw new ParserException("Skipping atom with length > 2147483647 (unsupported).");
            }
            return true;
        }
        throw new ParserException("Atom size less than header length (unsupported).");
    }

    public void a(ExtractorOutput extractorOutput) {
        this.D = extractorOutput;
        Track track = this.b;
        if (track != null) {
            TrackBundle trackBundle = new TrackBundle(extractorOutput.a(0, track.b));
            trackBundle.a(this.b, new DefaultSampleValues(0, 0, 0, 0));
            this.e.put(0, trackBundle);
            c();
            this.D.a();
        }
    }

    public FragmentedMp4Extractor(int i2, TimestampAdjuster timestampAdjuster) {
        this(i2, timestampAdjuster, (Track) null, (DrmInitData) null);
    }

    public FragmentedMp4Extractor(int i2, TimestampAdjuster timestampAdjuster, Track track, DrmInitData drmInitData) {
        this(i2, timestampAdjuster, track, drmInitData, Collections.emptyList());
    }

    public FragmentedMp4Extractor(int i2, TimestampAdjuster timestampAdjuster, Track track, DrmInitData drmInitData, List<Format> list) {
        this(i2, timestampAdjuster, track, drmInitData, list, (TrackOutput) null);
    }

    public FragmentedMp4Extractor(int i2, TimestampAdjuster timestampAdjuster, Track track, DrmInitData drmInitData, List<Format> list, TrackOutput trackOutput) {
        this.f3288a = i2 | (track != null ? 8 : 0);
        this.i = timestampAdjuster;
        this.b = track;
        this.d = drmInitData;
        this.c = Collections.unmodifiableList(list);
        this.n = trackOutput;
        this.j = new ParsableByteArray(16);
        this.f = new ParsableByteArray(NalUnitUtil.f3637a);
        this.g = new ParsableByteArray(5);
        this.h = new ParsableByteArray();
        this.k = new byte[16];
        this.l = new ArrayDeque<>();
        this.m = new ArrayDeque<>();
        this.e = new SparseArray<>();
        this.w = -9223372036854775807L;
        this.v = -9223372036854775807L;
        this.x = -9223372036854775807L;
        a();
    }

    private void c(Atom.ContainerAtom containerAtom) throws ParserException {
        int i2;
        int i3;
        Atom.ContainerAtom containerAtom2 = containerAtom;
        boolean z2 = true;
        int i4 = 0;
        Assertions.b(this.b == null, "Unexpected moov box.");
        DrmInitData drmInitData = this.d;
        if (drmInitData == null) {
            drmInitData = a(containerAtom2.W0);
        }
        Atom.ContainerAtom d2 = containerAtom2.d(Atom.N);
        SparseArray sparseArray = new SparseArray();
        int size = d2.W0.size();
        long j2 = -9223372036854775807L;
        for (int i5 = 0; i5 < size; i5++) {
            Atom.LeafAtom leafAtom = d2.W0.get(i5);
            int i6 = leafAtom.f3279a;
            if (i6 == Atom.z) {
                Pair<Integer, DefaultSampleValues> d3 = d(leafAtom.V0);
                sparseArray.put(((Integer) d3.first).intValue(), d3.second);
            } else if (i6 == Atom.O) {
                j2 = b(leafAtom.V0);
            }
        }
        SparseArray sparseArray2 = new SparseArray();
        int size2 = containerAtom2.X0.size();
        int i7 = 0;
        while (i7 < size2) {
            Atom.ContainerAtom containerAtom3 = containerAtom2.X0.get(i7);
            if (containerAtom3.f3279a == Atom.E) {
                i2 = i7;
                i3 = size2;
                Track a2 = AtomParsers.a(containerAtom3, containerAtom2.e(Atom.D), j2, drmInitData, (this.f3288a & 16) != 0, false);
                if (a2 != null) {
                    sparseArray2.put(a2.f3296a, a2);
                }
            } else {
                i2 = i7;
                i3 = size2;
            }
            i7 = i2 + 1;
            size2 = i3;
        }
        int size3 = sparseArray2.size();
        if (this.e.size() == 0) {
            while (i4 < size3) {
                Track track = (Track) sparseArray2.valueAt(i4);
                TrackBundle trackBundle = new TrackBundle(this.D.a(i4, track.b));
                trackBundle.a(track, a((SparseArray<DefaultSampleValues>) sparseArray, track.f3296a));
                this.e.put(track.f3296a, trackBundle);
                this.w = Math.max(this.w, track.e);
                i4++;
            }
            c();
            this.D.a();
            return;
        }
        if (this.e.size() != size3) {
            z2 = false;
        }
        Assertions.b(z2);
        while (i4 < size3) {
            Track track2 = (Track) sparseArray2.valueAt(i4);
            this.e.get(track2.f3296a).a(track2, a((SparseArray<DefaultSampleValues>) sparseArray, track2.f3296a));
            i4++;
        }
    }

    private void d(ExtractorInput extractorInput) throws IOException, InterruptedException {
        int size = this.e.size();
        TrackBundle trackBundle = null;
        long j2 = Clock.MAX_TIME;
        for (int i2 = 0; i2 < size; i2++) {
            TrackFragment trackFragment = this.e.valueAt(i2).b;
            if (trackFragment.r) {
                long j3 = trackFragment.d;
                if (j3 < j2) {
                    trackBundle = this.e.valueAt(i2);
                    j2 = j3;
                }
            }
        }
        if (trackBundle == null) {
            this.o = 3;
            return;
        }
        int position = (int) (j2 - extractorInput.getPosition());
        if (position >= 0) {
            extractorInput.c(position);
            trackBundle.b.a(extractorInput);
            return;
        }
        throw new ParserException("Offset to encryption data was negative.");
    }

    public void a(long j2, long j3) {
        int size = this.e.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.e.valueAt(i2).c();
        }
        this.m.clear();
        this.u = 0;
        this.v = j3;
        this.l.clear();
        a();
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        while (true) {
            int i2 = this.o;
            if (i2 != 0) {
                if (i2 == 1) {
                    c(extractorInput);
                } else if (i2 == 2) {
                    d(extractorInput);
                } else if (e(extractorInput)) {
                    return 0;
                }
            } else if (!b(extractorInput)) {
                return -1;
            }
        }
    }

    private void a() {
        this.o = 0;
        this.r = 0;
    }

    private void a(Atom.LeafAtom leafAtom, long j2) throws ParserException {
        if (!this.l.isEmpty()) {
            this.l.peek().a(leafAtom);
            return;
        }
        int i2 = leafAtom.f3279a;
        if (i2 == Atom.B) {
            Pair<Long, ChunkIndex> a2 = a(leafAtom.V0, j2);
            this.x = ((Long) a2.first).longValue();
            this.D.a((SeekMap) a2.second);
            this.G = true;
        } else if (i2 == Atom.G0) {
            a(leafAtom.V0);
        }
    }

    private void a(Atom.ContainerAtom containerAtom) throws ParserException {
        int i2 = containerAtom.f3279a;
        if (i2 == Atom.C) {
            c(containerAtom);
        } else if (i2 == Atom.L) {
            b(containerAtom);
        } else if (!this.l.isEmpty()) {
            this.l.peek().a(containerAtom);
        }
    }

    private DefaultSampleValues a(SparseArray<DefaultSampleValues> sparseArray, int i2) {
        if (sparseArray.size() == 1) {
            return sparseArray.valueAt(0);
        }
        DefaultSampleValues defaultSampleValues = sparseArray.get(i2);
        Assertions.a(defaultSampleValues);
        return defaultSampleValues;
    }

    private void c() {
        int i2;
        if (this.E == null) {
            this.E = new TrackOutput[2];
            TrackOutput trackOutput = this.n;
            if (trackOutput != null) {
                this.E[0] = trackOutput;
                i2 = 1;
            } else {
                i2 = 0;
            }
            if ((this.f3288a & 4) != 0) {
                this.E[i2] = this.D.a(this.e.size(), 4);
                i2++;
            }
            this.E = (TrackOutput[]) Arrays.copyOf(this.E, i2);
            for (TrackOutput a2 : this.E) {
                a2.a(J);
            }
        }
        if (this.F == null) {
            this.F = new TrackOutput[this.c.size()];
            for (int i3 = 0; i3 < this.F.length; i3++) {
                TrackOutput a3 = this.D.a(this.e.size() + 1 + i3, 3);
                a3.a(this.c.get(i3));
                this.F[i3] = a3;
            }
        }
    }

    private void a(ParsableByteArray parsableByteArray) {
        TrackOutput[] trackOutputArr = this.E;
        if (trackOutputArr != null && trackOutputArr.length != 0) {
            parsableByteArray.e(12);
            int a2 = parsableByteArray.a();
            parsableByteArray.q();
            parsableByteArray.q();
            long c2 = Util.c(parsableByteArray.v(), 1000000, parsableByteArray.v());
            for (TrackOutput a3 : this.E) {
                parsableByteArray.e(12);
                a3.a(parsableByteArray, a2);
            }
            long j2 = this.x;
            if (j2 != -9223372036854775807L) {
                long j3 = j2 + c2;
                TimestampAdjuster timestampAdjuster = this.i;
                long a4 = timestampAdjuster != null ? timestampAdjuster.a(j3) : j3;
                for (TrackOutput a5 : this.E) {
                    a5.a(a4, 1, a2, 0, (TrackOutput.CryptoData) null);
                }
                return;
            }
            this.m.addLast(new MetadataSampleInfo(c2, a2));
            this.u += a2;
        }
    }

    private void b(long j2) throws ParserException {
        while (!this.l.isEmpty() && this.l.peek().V0 == j2) {
            a(this.l.pop());
        }
        a();
    }

    private void b(Atom.ContainerAtom containerAtom) throws ParserException {
        DrmInitData drmInitData;
        a(containerAtom, this.e, this.f3288a, this.k);
        if (this.d != null) {
            drmInitData = null;
        } else {
            drmInitData = a(containerAtom.W0);
        }
        if (drmInitData != null) {
            int size = this.e.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.e.valueAt(i2).a(drmInitData);
            }
        }
        if (this.v != -9223372036854775807L) {
            int size2 = this.e.size();
            for (int i3 = 0; i3 < size2; i3++) {
                this.e.valueAt(i3).a(this.v);
            }
            this.v = -9223372036854775807L;
        }
    }

    private static long c(ParsableByteArray parsableByteArray) {
        parsableByteArray.e(8);
        return Atom.c(parsableByteArray.h()) == 1 ? parsableByteArray.y() : parsableByteArray.v();
    }

    private static void a(Atom.ContainerAtom containerAtom, SparseArray<TrackBundle> sparseArray, int i2, byte[] bArr) throws ParserException {
        int size = containerAtom.X0.size();
        for (int i3 = 0; i3 < size; i3++) {
            Atom.ContainerAtom containerAtom2 = containerAtom.X0.get(i3);
            if (containerAtom2.f3279a == Atom.M) {
                b(containerAtom2, sparseArray, i2, bArr);
            }
        }
    }

    private static void a(Atom.ContainerAtom containerAtom, TrackBundle trackBundle, long j2, int i2) {
        List<Atom.LeafAtom> list = containerAtom.W0;
        int size = list.size();
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            Atom.LeafAtom leafAtom = list.get(i5);
            if (leafAtom.f3279a == Atom.A) {
                ParsableByteArray parsableByteArray = leafAtom.V0;
                parsableByteArray.e(12);
                int x2 = parsableByteArray.x();
                if (x2 > 0) {
                    i4 += x2;
                    i3++;
                }
            }
        }
        trackBundle.g = 0;
        trackBundle.f = 0;
        trackBundle.e = 0;
        trackBundle.b.a(i3, i4);
        int i6 = 0;
        int i7 = 0;
        for (int i8 = 0; i8 < size; i8++) {
            Atom.LeafAtom leafAtom2 = list.get(i8);
            if (leafAtom2.f3279a == Atom.A) {
                i7 = a(trackBundle, i6, j2, i2, leafAtom2.V0, i7);
                i6++;
            }
        }
    }

    private static long b(ParsableByteArray parsableByteArray) {
        parsableByteArray.e(8);
        return Atom.c(parsableByteArray.h()) == 0 ? parsableByteArray.v() : parsableByteArray.y();
    }

    private static void b(Atom.ContainerAtom containerAtom, SparseArray<TrackBundle> sparseArray, int i2, byte[] bArr) throws ParserException {
        TrackBundle a2 = a(containerAtom.e(Atom.y).V0, sparseArray);
        if (a2 != null) {
            TrackFragment trackFragment = a2.b;
            long j2 = trackFragment.s;
            a2.c();
            if (containerAtom.e(Atom.x) != null && (i2 & 2) == 0) {
                j2 = c(containerAtom.e(Atom.x).V0);
            }
            a(containerAtom, a2, j2, i2);
            TrackEncryptionBox a3 = a2.c.a(trackFragment.f3298a.f3286a);
            Atom.LeafAtom e2 = containerAtom.e(Atom.d0);
            if (e2 != null) {
                a(a3, e2.V0, trackFragment);
            }
            Atom.LeafAtom e3 = containerAtom.e(Atom.e0);
            if (e3 != null) {
                a(e3.V0, trackFragment);
            }
            Atom.LeafAtom e4 = containerAtom.e(Atom.i0);
            if (e4 != null) {
                b(e4.V0, trackFragment);
            }
            Atom.LeafAtom e5 = containerAtom.e(Atom.f0);
            Atom.LeafAtom e6 = containerAtom.e(Atom.g0);
            if (!(e5 == null || e6 == null)) {
                a(e5.V0, e6.V0, a3 != null ? a3.b : null, trackFragment);
            }
            int size = containerAtom.W0.size();
            for (int i3 = 0; i3 < size; i3++) {
                Atom.LeafAtom leafAtom = containerAtom.W0.get(i3);
                if (leafAtom.f3279a == Atom.h0) {
                    a(leafAtom.V0, trackFragment, bArr);
                }
            }
        }
    }

    private static void a(TrackEncryptionBox trackEncryptionBox, ParsableByteArray parsableByteArray, TrackFragment trackFragment) throws ParserException {
        int i2;
        int i3 = trackEncryptionBox.d;
        parsableByteArray.e(8);
        boolean z2 = true;
        if ((Atom.b(parsableByteArray.h()) & 1) == 1) {
            parsableByteArray.f(8);
        }
        int t2 = parsableByteArray.t();
        int x2 = parsableByteArray.x();
        if (x2 == trackFragment.f) {
            if (t2 == 0) {
                boolean[] zArr = trackFragment.n;
                i2 = 0;
                for (int i4 = 0; i4 < x2; i4++) {
                    int t3 = parsableByteArray.t();
                    i2 += t3;
                    zArr[i4] = t3 > i3;
                }
            } else {
                if (t2 <= i3) {
                    z2 = false;
                }
                i2 = (t2 * x2) + 0;
                Arrays.fill(trackFragment.n, 0, x2, z2);
            }
            trackFragment.b(i2);
            return;
        }
        throw new ParserException("Length mismatch: " + x2 + ", " + trackFragment.f);
    }

    private static TrackBundle b(SparseArray<TrackBundle> sparseArray, int i2) {
        if (sparseArray.size() == 1) {
            return sparseArray.valueAt(0);
        }
        return sparseArray.get(i2);
    }

    private static void a(ParsableByteArray parsableByteArray, TrackFragment trackFragment) throws ParserException {
        parsableByteArray.e(8);
        int h2 = parsableByteArray.h();
        if ((Atom.b(h2) & 1) == 1) {
            parsableByteArray.f(8);
        }
        int x2 = parsableByteArray.x();
        if (x2 == 1) {
            trackFragment.d += Atom.c(h2) == 0 ? parsableByteArray.v() : parsableByteArray.y();
            return;
        }
        throw new ParserException("Unexpected saio entry count: " + x2);
    }

    private static void b(ParsableByteArray parsableByteArray, TrackFragment trackFragment) throws ParserException {
        a(parsableByteArray, 0, trackFragment);
    }

    private static boolean b(int i2) {
        return i2 == Atom.T || i2 == Atom.S || i2 == Atom.D || i2 == Atom.B || i2 == Atom.U || i2 == Atom.x || i2 == Atom.y || i2 == Atom.P || i2 == Atom.z || i2 == Atom.A || i2 == Atom.V || i2 == Atom.d0 || i2 == Atom.e0 || i2 == Atom.i0 || i2 == Atom.h0 || i2 == Atom.f0 || i2 == Atom.g0 || i2 == Atom.R || i2 == Atom.O || i2 == Atom.G0;
    }

    private static TrackBundle a(ParsableByteArray parsableByteArray, SparseArray<TrackBundle> sparseArray) {
        parsableByteArray.e(8);
        int b2 = Atom.b(parsableByteArray.h());
        TrackBundle b3 = b(sparseArray, parsableByteArray.h());
        if (b3 == null) {
            return null;
        }
        if ((b2 & 1) != 0) {
            long y2 = parsableByteArray.y();
            TrackFragment trackFragment = b3.b;
            trackFragment.c = y2;
            trackFragment.d = y2;
        }
        DefaultSampleValues defaultSampleValues = b3.d;
        b3.b.f3298a = new DefaultSampleValues((b2 & 2) != 0 ? parsableByteArray.x() - 1 : defaultSampleValues.f3286a, (b2 & 8) != 0 ? parsableByteArray.x() : defaultSampleValues.b, (b2 & 16) != 0 ? parsableByteArray.x() : defaultSampleValues.c, (b2 & 32) != 0 ? parsableByteArray.x() : defaultSampleValues.d);
        return b3;
    }

    private static int a(TrackBundle trackBundle, int i2, long j2, int i3, ParsableByteArray parsableByteArray, int i4) {
        boolean z2;
        int i5;
        boolean z3;
        int i6;
        boolean z4;
        boolean z5;
        boolean z6;
        TrackBundle trackBundle2 = trackBundle;
        parsableByteArray.e(8);
        int b2 = Atom.b(parsableByteArray.h());
        Track track = trackBundle2.c;
        TrackFragment trackFragment = trackBundle2.b;
        DefaultSampleValues defaultSampleValues = trackFragment.f3298a;
        trackFragment.h[i2] = parsableByteArray.x();
        long[] jArr = trackFragment.g;
        jArr[i2] = trackFragment.c;
        if ((b2 & 1) != 0) {
            jArr[i2] = jArr[i2] + ((long) parsableByteArray.h());
        }
        boolean z7 = (b2 & 4) != 0;
        int i7 = defaultSampleValues.d;
        if (z7) {
            i7 = parsableByteArray.x();
        }
        boolean z8 = (b2 & 256) != 0;
        boolean z9 = (b2 & AdRequest.MAX_CONTENT_URL_LENGTH) != 0;
        boolean z10 = (b2 & ByteConstants.KB) != 0;
        boolean z11 = (b2 & 2048) != 0;
        long[] jArr2 = track.h;
        long j3 = 0;
        if (jArr2 != null && jArr2.length == 1 && jArr2[0] == 0) {
            j3 = Util.c(track.i[0], 1000, track.c);
        }
        int[] iArr = trackFragment.i;
        int[] iArr2 = trackFragment.j;
        long[] jArr3 = trackFragment.k;
        boolean[] zArr = trackFragment.l;
        int i8 = i7;
        boolean z12 = track.b == 2 && (i3 & 1) != 0;
        int i9 = i4 + trackFragment.h[i2];
        long j4 = j3;
        boolean[] zArr2 = zArr;
        long j5 = track.c;
        boolean[] zArr3 = zArr2;
        long[] jArr4 = jArr3;
        long j6 = i2 > 0 ? trackFragment.s : j2;
        int i10 = i4;
        while (i10 < i9) {
            int x2 = z8 ? parsableByteArray.x() : defaultSampleValues.b;
            if (z9) {
                z2 = z8;
                i5 = parsableByteArray.x();
            } else {
                z2 = z8;
                i5 = defaultSampleValues.c;
            }
            if (i10 == 0 && z7) {
                z3 = z7;
                i6 = i8;
            } else if (z10) {
                z3 = z7;
                i6 = parsableByteArray.h();
            } else {
                z3 = z7;
                i6 = defaultSampleValues.d;
            }
            if (z11) {
                z6 = z11;
                z5 = z9;
                z4 = z10;
                iArr2[i10] = (int) ((((long) parsableByteArray.h()) * 1000) / j5);
            } else {
                z6 = z11;
                z5 = z9;
                z4 = z10;
                iArr2[i10] = 0;
            }
            jArr4[i10] = Util.c(j6, 1000, j5) - j4;
            iArr[i10] = i5;
            zArr3[i10] = ((i6 >> 16) & 1) == 0 && (!z12 || i10 == 0);
            i10++;
            j6 += (long) x2;
            z8 = z2;
            z7 = z3;
            z11 = z6;
            z9 = z5;
            z10 = z4;
            i9 = i9;
        }
        int i11 = i9;
        trackFragment.s = j6;
        return i11;
    }

    private static void a(ParsableByteArray parsableByteArray, TrackFragment trackFragment, byte[] bArr) throws ParserException {
        parsableByteArray.e(8);
        parsableByteArray.a(bArr, 0, 16);
        if (Arrays.equals(bArr, I)) {
            a(parsableByteArray, 16, trackFragment);
        }
    }

    private static void a(ParsableByteArray parsableByteArray, int i2, TrackFragment trackFragment) throws ParserException {
        parsableByteArray.e(i2 + 8);
        int b2 = Atom.b(parsableByteArray.h());
        if ((b2 & 1) == 0) {
            boolean z2 = (b2 & 2) != 0;
            int x2 = parsableByteArray.x();
            if (x2 == trackFragment.f) {
                Arrays.fill(trackFragment.n, 0, x2, z2);
                trackFragment.b(parsableByteArray.a());
                trackFragment.a(parsableByteArray);
                return;
            }
            throw new ParserException("Length mismatch: " + x2 + ", " + trackFragment.f);
        }
        throw new ParserException("Overriding TrackEncryptionBox parameters is unsupported.");
    }

    private static void a(ParsableByteArray parsableByteArray, ParsableByteArray parsableByteArray2, String str, TrackFragment trackFragment) throws ParserException {
        byte[] bArr;
        parsableByteArray.e(8);
        int h2 = parsableByteArray.h();
        if (parsableByteArray.h() == H) {
            if (Atom.c(h2) == 1) {
                parsableByteArray.f(4);
            }
            if (parsableByteArray.h() == 1) {
                parsableByteArray2.e(8);
                int h3 = parsableByteArray2.h();
                if (parsableByteArray2.h() == H) {
                    int c2 = Atom.c(h3);
                    if (c2 == 1) {
                        if (parsableByteArray2.v() == 0) {
                            throw new ParserException("Variable length description in sgpd found (unsupported)");
                        }
                    } else if (c2 >= 2) {
                        parsableByteArray2.f(4);
                    }
                    if (parsableByteArray2.v() == 1) {
                        parsableByteArray2.f(1);
                        int t2 = parsableByteArray2.t();
                        int i2 = (t2 & 240) >> 4;
                        int i3 = t2 & 15;
                        boolean z2 = parsableByteArray2.t() == 1;
                        if (z2) {
                            int t3 = parsableByteArray2.t();
                            byte[] bArr2 = new byte[16];
                            parsableByteArray2.a(bArr2, 0, bArr2.length);
                            if (!z2 || t3 != 0) {
                                bArr = null;
                            } else {
                                int t4 = parsableByteArray2.t();
                                byte[] bArr3 = new byte[t4];
                                parsableByteArray2.a(bArr3, 0, t4);
                                bArr = bArr3;
                            }
                            trackFragment.m = true;
                            trackFragment.o = new TrackEncryptionBox(z2, str, t3, bArr2, i2, i3, bArr);
                            return;
                        }
                        return;
                    }
                    throw new ParserException("Entry count in sgpd != 1 (unsupported).");
                }
                return;
            }
            throw new ParserException("Entry count in sbgp != 1 (unsupported).");
        }
    }

    private static Pair<Long, ChunkIndex> a(ParsableByteArray parsableByteArray, long j2) throws ParserException {
        long j3;
        long j4;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        parsableByteArray2.e(8);
        int c2 = Atom.c(parsableByteArray.h());
        parsableByteArray2.f(4);
        long v2 = parsableByteArray.v();
        if (c2 == 0) {
            j4 = parsableByteArray.v();
            j3 = parsableByteArray.v();
        } else {
            j4 = parsableByteArray.y();
            j3 = parsableByteArray.y();
        }
        long j5 = j4;
        long j6 = j2 + j3;
        long c3 = Util.c(j5, 1000000, v2);
        parsableByteArray2.f(2);
        int z2 = parsableByteArray.z();
        int[] iArr = new int[z2];
        long[] jArr = new long[z2];
        long[] jArr2 = new long[z2];
        long[] jArr3 = new long[z2];
        long j7 = j5;
        long j8 = c3;
        int i2 = 0;
        while (i2 < z2) {
            int h2 = parsableByteArray.h();
            if ((h2 & Integer.MIN_VALUE) == 0) {
                long v3 = parsableByteArray.v();
                iArr[i2] = h2 & Integer.MAX_VALUE;
                jArr[i2] = j6;
                jArr3[i2] = j8;
                j7 += v3;
                long[] jArr4 = jArr2;
                long[] jArr5 = jArr3;
                int i3 = z2;
                int[] iArr2 = iArr;
                j8 = Util.c(j7, 1000000, v2);
                jArr4[i2] = j8 - jArr5[i2];
                parsableByteArray2.f(4);
                j6 += (long) iArr2[i2];
                i2++;
                iArr = iArr2;
                jArr3 = jArr5;
                jArr2 = jArr4;
                jArr = jArr;
                z2 = i3;
            } else {
                throw new ParserException("Unhandled indirect reference");
            }
        }
        return Pair.create(Long.valueOf(c3), new ChunkIndex(iArr, jArr, jArr2, jArr3));
    }

    private void a(long j2) {
        while (!this.m.isEmpty()) {
            MetadataSampleInfo removeFirst = this.m.removeFirst();
            this.u -= removeFirst.b;
            long j3 = removeFirst.f3289a + j2;
            TimestampAdjuster timestampAdjuster = this.i;
            if (timestampAdjuster != null) {
                j3 = timestampAdjuster.a(j3);
            }
            for (TrackOutput a2 : this.E) {
                a2.a(j3, 1, removeFirst.b, this.u, (TrackOutput.CryptoData) null);
            }
        }
    }

    private static TrackBundle a(SparseArray<TrackBundle> sparseArray) {
        int size = sparseArray.size();
        TrackBundle trackBundle = null;
        long j2 = Clock.MAX_TIME;
        for (int i2 = 0; i2 < size; i2++) {
            TrackBundle valueAt = sparseArray.valueAt(i2);
            int i3 = valueAt.g;
            TrackFragment trackFragment = valueAt.b;
            if (i3 != trackFragment.e) {
                long j3 = trackFragment.g[i3];
                if (j3 < j2) {
                    trackBundle = valueAt;
                    j2 = j3;
                }
            }
        }
        return trackBundle;
    }

    private static DrmInitData a(List<Atom.LeafAtom> list) {
        int size = list.size();
        ArrayList arrayList = null;
        for (int i2 = 0; i2 < size; i2++) {
            Atom.LeafAtom leafAtom = list.get(i2);
            if (leafAtom.f3279a == Atom.V) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                byte[] bArr = leafAtom.V0.f3641a;
                UUID c2 = PsshAtomUtil.c(bArr);
                if (c2 == null) {
                    Log.d("FragmentedMp4Extractor", "Skipped pssh atom (failed to extract uuid)");
                } else {
                    arrayList.add(new DrmInitData.SchemeData(c2, "video/mp4", bArr));
                }
            }
        }
        if (arrayList == null) {
            return null;
        }
        return new DrmInitData((List<DrmInitData.SchemeData>) arrayList);
    }

    private static boolean a(int i2) {
        return i2 == Atom.C || i2 == Atom.E || i2 == Atom.F || i2 == Atom.G || i2 == Atom.H || i2 == Atom.L || i2 == Atom.M || i2 == Atom.N || i2 == Atom.Q;
    }
}
