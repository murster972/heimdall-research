package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.SeekPoint;
import com.google.android.exoplayer2.util.Assertions;
import java.io.EOFException;
import java.io.IOException;

final class DefaultOggSeeker implements OggSeeker {

    /* renamed from: a  reason: collision with root package name */
    private final OggPageHeader f3302a = new OggPageHeader();
    /* access modifiers changed from: private */
    public final long b;
    private final long c;
    /* access modifiers changed from: private */
    public final StreamReader d;
    private int e;
    /* access modifiers changed from: private */
    public long f;
    private long g;
    private long h;
    private long i;
    private long j;
    private long k;
    private long l;

    private class OggSeekMap implements SeekMap {
        private OggSeekMap() {
        }

        public SeekMap.SeekPoints b(long j) {
            if (j == 0) {
                return new SeekMap.SeekPoints(new SeekPoint(0, DefaultOggSeeker.this.b));
            }
            long b = DefaultOggSeeker.this.d.b(j);
            DefaultOggSeeker defaultOggSeeker = DefaultOggSeeker.this;
            return new SeekMap.SeekPoints(new SeekPoint(j, defaultOggSeeker.a(defaultOggSeeker.b, b, 30000)));
        }

        public boolean b() {
            return true;
        }

        public long getDurationUs() {
            return DefaultOggSeeker.this.d.a(DefaultOggSeeker.this.f);
        }
    }

    public DefaultOggSeeker(long j2, long j3, StreamReader streamReader, long j4, long j5, boolean z) {
        Assertions.a(j2 >= 0 && j3 > j2);
        this.d = streamReader;
        this.b = j2;
        this.c = j3;
        if (j4 == j3 - j2 || z) {
            this.f = j5;
            this.e = 3;
            return;
        }
        this.e = 0;
    }

    /* access modifiers changed from: package-private */
    public long b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        c(extractorInput);
        this.f3302a.a();
        while ((this.f3302a.b & 4) != 4 && extractorInput.getPosition() < this.c) {
            this.f3302a.a(extractorInput, false);
            OggPageHeader oggPageHeader = this.f3302a;
            extractorInput.c(oggPageHeader.e + oggPageHeader.f);
        }
        return this.f3302a.c;
    }

    public long a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        int i2 = this.e;
        if (i2 == 0) {
            this.g = extractorInput.getPosition();
            this.e = 1;
            long j2 = this.c - 65307;
            if (j2 > this.g) {
                return j2;
            }
        } else if (i2 != 1) {
            if (i2 == 2) {
                long j3 = this.h;
                long j4 = 0;
                if (j3 != 0) {
                    long a2 = a(j3, extractorInput);
                    if (a2 >= 0) {
                        return a2;
                    }
                    j4 = a(extractorInput, this.h, -(a2 + 2));
                }
                this.e = 3;
                return -(j4 + 2);
            } else if (i2 == 3) {
                return -1;
            } else {
                throw new IllegalStateException();
            }
        }
        this.f = b(extractorInput);
        this.e = 3;
        return this.g;
    }

    public long c(long j2) {
        int i2 = this.e;
        Assertions.a(i2 == 3 || i2 == 2);
        long j3 = 0;
        if (j2 != 0) {
            j3 = this.d.b(j2);
        }
        this.h = j3;
        this.e = 2;
        a();
        return this.h;
    }

    public OggSeekMap c() {
        if (this.f != 0) {
            return new OggSeekMap();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void c(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (!a(extractorInput, this.c)) {
            throw new EOFException();
        }
    }

    public void a() {
        this.i = this.b;
        this.j = this.c;
        this.k = 0;
        this.l = this.f;
    }

    public long a(long j2, ExtractorInput extractorInput) throws IOException, InterruptedException {
        long j3 = 2;
        if (this.i == this.j) {
            return -(this.k + 2);
        }
        long position = extractorInput.getPosition();
        if (!a(extractorInput, this.j)) {
            long j4 = this.i;
            if (j4 != position) {
                return j4;
            }
            throw new IOException("No ogg page can be found.");
        }
        this.f3302a.a(extractorInput, false);
        extractorInput.a();
        OggPageHeader oggPageHeader = this.f3302a;
        long j5 = j2 - oggPageHeader.c;
        int i2 = oggPageHeader.e + oggPageHeader.f;
        int i3 = (j5 > 0 ? 1 : (j5 == 0 ? 0 : -1));
        if (i3 < 0 || j5 > 72000) {
            if (i3 < 0) {
                this.j = position;
                this.l = this.f3302a.c;
            } else {
                long j6 = (long) i2;
                this.i = extractorInput.getPosition() + j6;
                this.k = this.f3302a.c;
                if ((this.j - this.i) + j6 < 100000) {
                    extractorInput.c(i2);
                    return -(this.k + 2);
                }
            }
            long j7 = this.j;
            long j8 = this.i;
            if (j7 - j8 < 100000) {
                this.j = j8;
                return j8;
            }
            long j9 = (long) i2;
            if (i3 > 0) {
                j3 = 1;
            }
            long position2 = extractorInput.getPosition() - (j9 * j3);
            long j10 = this.j;
            long j11 = this.i;
            return Math.min(Math.max(position2 + ((j5 * (j10 - j11)) / (this.l - this.k)), j11), this.j - 1);
        }
        extractorInput.c(i2);
        return -(this.f3302a.c + 2);
    }

    /* access modifiers changed from: private */
    public long a(long j2, long j3, long j4) {
        long j5 = this.c;
        long j6 = this.b;
        long j7 = j2 + (((j3 * (j5 - j6)) / this.f) - j4);
        if (j7 < j6) {
            j7 = j6;
        }
        long j8 = this.c;
        return j7 >= j8 ? j8 - 1 : j7;
    }

    /* access modifiers changed from: package-private */
    public boolean a(ExtractorInput extractorInput, long j2) throws IOException, InterruptedException {
        int i2;
        long min = Math.min(j2 + 3, this.c);
        byte[] bArr = new byte[2048];
        int length = bArr.length;
        while (true) {
            int i3 = 0;
            if (extractorInput.getPosition() + ((long) length) > min) {
                int position = (int) (min - extractorInput.getPosition());
                if (position < 4) {
                    return false;
                }
                length = position;
            }
            extractorInput.a(bArr, 0, length, false);
            while (true) {
                i2 = length - 3;
                if (i3 >= i2) {
                    break;
                } else if (bArr[i3] == 79 && bArr[i3 + 1] == 103 && bArr[i3 + 2] == 103 && bArr[i3 + 3] == 83) {
                    extractorInput.c(i3);
                    return true;
                } else {
                    i3++;
                }
            }
            extractorInput.c(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public long a(ExtractorInput extractorInput, long j2, long j3) throws IOException, InterruptedException {
        this.f3302a.a(extractorInput, false);
        while (true) {
            OggPageHeader oggPageHeader = this.f3302a;
            if (oggPageHeader.c < j2) {
                extractorInput.c(oggPageHeader.e + oggPageHeader.f);
                OggPageHeader oggPageHeader2 = this.f3302a;
                long j4 = oggPageHeader2.c;
                oggPageHeader2.a(extractorInput, false);
                j3 = j4;
            } else {
                extractorInput.a();
                return j3;
            }
        }
    }
}
