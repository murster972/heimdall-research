package com.google.android.exoplayer2.extractor.ts;

import android.util.SparseArray;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import java.util.Collections;
import java.util.List;

public interface TsPayloadReader {

    public static final class DvbSubtitleInfo {

        /* renamed from: a  reason: collision with root package name */
        public final String f3347a;
        public final byte[] b;

        public DvbSubtitleInfo(String str, int i, byte[] bArr) {
            this.f3347a = str;
            this.b = bArr;
        }
    }

    public static final class EsInfo {

        /* renamed from: a  reason: collision with root package name */
        public final int f3348a;
        public final String b;
        public final List<DvbSubtitleInfo> c;
        public final byte[] d;

        public EsInfo(int i, String str, List<DvbSubtitleInfo> list, byte[] bArr) {
            List<DvbSubtitleInfo> list2;
            this.f3348a = i;
            this.b = str;
            if (list == null) {
                list2 = Collections.emptyList();
            } else {
                list2 = Collections.unmodifiableList(list);
            }
            this.c = list2;
            this.d = bArr;
        }
    }

    public interface Factory {
        SparseArray<TsPayloadReader> a();

        TsPayloadReader a(int i, EsInfo esInfo);
    }

    public static final class TrackIdGenerator {

        /* renamed from: a  reason: collision with root package name */
        private final String f3349a;
        private final int b;
        private final int c;
        private int d;
        private String e;

        public TrackIdGenerator(int i, int i2) {
            this(Integer.MIN_VALUE, i, i2);
        }

        private void d() {
            if (this.d == Integer.MIN_VALUE) {
                throw new IllegalStateException("generateNewId() must be called before retrieving ids.");
            }
        }

        public void a() {
            int i = this.d;
            this.d = i == Integer.MIN_VALUE ? this.b : i + this.c;
            this.e = this.f3349a + this.d;
        }

        public String b() {
            d();
            return this.e;
        }

        public int c() {
            d();
            return this.d;
        }

        public TrackIdGenerator(int i, int i2, int i3) {
            String str;
            if (i != Integer.MIN_VALUE) {
                str = i + "/";
            } else {
                str = "";
            }
            this.f3349a = str;
            this.b = i2;
            this.c = i3;
            this.d = Integer.MIN_VALUE;
        }
    }

    void a();

    void a(ParsableByteArray parsableByteArray, boolean z) throws ParserException;

    void a(TimestampAdjuster timestampAdjuster, ExtractorOutput extractorOutput, TrackIdGenerator trackIdGenerator);
}
