package com.google.android.exoplayer2.extractor.ogg;

import com.facebook.imageutils.JfifUtil;

final class VorbisBitArray {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f3310a;
    private final int b;
    private int c;
    private int d;

    public VorbisBitArray(byte[] bArr) {
        this.f3310a = bArr;
        this.b = bArr.length;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r2.b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r2 = this;
            int r0 = r2.c
            if (r0 < 0) goto L_0x0010
            int r1 = r2.b
            if (r0 < r1) goto L_0x000e
            if (r0 != r1) goto L_0x0010
            int r0 = r2.d
            if (r0 != 0) goto L_0x0010
        L_0x000e:
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            com.google.android.exoplayer2.util.Assertions.b(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.ogg.VorbisBitArray.c():void");
    }

    public int a(int i) {
        int i2 = this.c;
        int min = Math.min(i, 8 - this.d);
        int i3 = i2 + 1;
        int i4 = ((this.f3310a[i2] & 255) >> this.d) & (JfifUtil.MARKER_FIRST_BYTE >> (8 - min));
        while (min < i) {
            i4 |= (this.f3310a[i3] & 255) << min;
            min += 8;
            i3++;
        }
        int i5 = i4 & (-1 >>> (32 - i));
        b(i);
        return i5;
    }

    public boolean b() {
        boolean z = (((this.f3310a[this.c] & 255) >> this.d) & 1) == 1;
        b(1);
        return z;
    }

    public void b(int i) {
        int i2 = i / 8;
        this.c += i2;
        this.d += i - (i2 * 8);
        int i3 = this.d;
        if (i3 > 7) {
            this.c++;
            this.d = i3 - 8;
        }
        c();
    }

    public int a() {
        return (this.c * 8) + this.d;
    }
}
