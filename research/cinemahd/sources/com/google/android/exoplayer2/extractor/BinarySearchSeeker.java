package com.google.android.exoplayer2.extractor;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public abstract class BinarySearchSeeker {

    /* renamed from: a  reason: collision with root package name */
    protected final BinarySearchSeekMap f3243a;
    protected final TimestampSeeker b;
    protected SeekOperationParams c;
    private final int d;

    public static class BinarySearchSeekMap implements SeekMap {

        /* renamed from: a  reason: collision with root package name */
        private final SeekTimestampConverter f3244a;
        private final long b;
        /* access modifiers changed from: private */
        public final long c;
        /* access modifiers changed from: private */
        public final long d;
        /* access modifiers changed from: private */
        public final long e;
        /* access modifiers changed from: private */
        public final long f;
        /* access modifiers changed from: private */
        public final long g;

        public BinarySearchSeekMap(SeekTimestampConverter seekTimestampConverter, long j, long j2, long j3, long j4, long j5, long j6) {
            this.f3244a = seekTimestampConverter;
            this.b = j;
            this.c = j2;
            this.d = j3;
            this.e = j4;
            this.f = j5;
            this.g = j6;
        }

        public boolean b() {
            return true;
        }

        public long getDurationUs() {
            return this.b;
        }

        public SeekMap.SeekPoints b(long j) {
            return new SeekMap.SeekPoints(new SeekPoint(j, SeekOperationParams.a(this.f3244a.a(j), this.c, this.d, this.e, this.f, this.g)));
        }

        public long c(long j) {
            return this.f3244a.a(j);
        }
    }

    public static final class DefaultSeekTimestampConverter implements SeekTimestampConverter {
        public long a(long j) {
            return j;
        }
    }

    public static final class OutputFrameHolder {
    }

    protected static class SeekOperationParams {

        /* renamed from: a  reason: collision with root package name */
        private final long f3245a;
        private final long b;
        private final long c;
        private long d;
        private long e;
        private long f;
        private long g;
        private long h;

        protected SeekOperationParams(long j, long j2, long j3, long j4, long j5, long j6, long j7) {
            this.f3245a = j;
            this.b = j2;
            this.d = j3;
            this.e = j4;
            this.f = j5;
            this.g = j6;
            this.c = j7;
            this.h = a(j2, j3, j4, j5, j6, j7);
        }

        private void f() {
            this.h = a(this.b, this.d, this.e, this.f, this.g, this.c);
        }

        /* access modifiers changed from: private */
        public long c() {
            return this.h;
        }

        /* access modifiers changed from: private */
        public long d() {
            return this.f3245a;
        }

        /* access modifiers changed from: private */
        public long e() {
            return this.b;
        }

        protected static long a(long j, long j2, long j3, long j4, long j5, long j6) {
            if (j4 + 1 >= j5 || j2 + 1 >= j3) {
                return j4;
            }
            long j7 = (long) (((float) (j - j2)) * (((float) (j5 - j4)) / ((float) (j3 - j2))));
            return Util.b(((j7 + j4) - j6) - (j7 / 20), j4, j5 - 1);
        }

        /* access modifiers changed from: private */
        public long b() {
            return this.f;
        }

        /* access modifiers changed from: private */
        public void b(long j, long j2) {
            this.d = j;
            this.f = j2;
            f();
        }

        /* access modifiers changed from: private */
        public long a() {
            return this.g;
        }

        /* access modifiers changed from: private */
        public void a(long j, long j2) {
            this.e = j;
            this.g = j2;
            f();
        }
    }

    protected interface SeekTimestampConverter {
        long a(long j);
    }

    public static final class TimestampSearchResult {
        public static final TimestampSearchResult d = new TimestampSearchResult(-3, -9223372036854775807L, -1);
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final int f3246a;
        /* access modifiers changed from: private */
        public final long b;
        /* access modifiers changed from: private */
        public final long c;

        private TimestampSearchResult(int i, long j, long j2) {
            this.f3246a = i;
            this.b = j;
            this.c = j2;
        }

        public static TimestampSearchResult a(long j, long j2) {
            return new TimestampSearchResult(-1, j, j2);
        }

        public static TimestampSearchResult b(long j, long j2) {
            return new TimestampSearchResult(-2, j, j2);
        }

        public static TimestampSearchResult a(long j) {
            return new TimestampSearchResult(0, -9223372036854775807L, j);
        }
    }

    protected interface TimestampSeeker {
        TimestampSearchResult a(ExtractorInput extractorInput, long j, OutputFrameHolder outputFrameHolder) throws IOException, InterruptedException;

        void a();
    }

    protected BinarySearchSeeker(SeekTimestampConverter seekTimestampConverter, TimestampSeeker timestampSeeker, long j, long j2, long j3, long j4, long j5, long j6, int i) {
        this.b = timestampSeeker;
        this.d = i;
        this.f3243a = new BinarySearchSeekMap(seekTimestampConverter, j, j2, j3, j4, j5, j6);
    }

    public final SeekMap a() {
        return this.f3243a;
    }

    public final void b(long j) {
        SeekOperationParams seekOperationParams = this.c;
        if (seekOperationParams == null || seekOperationParams.d() != j) {
            this.c = a(j);
        }
    }

    /* access modifiers changed from: protected */
    public void b(boolean z, long j) {
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder, OutputFrameHolder outputFrameHolder) throws InterruptedException, IOException {
        TimestampSeeker timestampSeeker = this.b;
        Assertions.a(timestampSeeker);
        TimestampSeeker timestampSeeker2 = timestampSeeker;
        while (true) {
            SeekOperationParams seekOperationParams = this.c;
            Assertions.a(seekOperationParams);
            SeekOperationParams seekOperationParams2 = seekOperationParams;
            long b2 = seekOperationParams2.b();
            long c2 = seekOperationParams2.a();
            long d2 = seekOperationParams2.c();
            if (c2 - b2 <= ((long) this.d)) {
                a(false, b2);
                return a(extractorInput, b2, positionHolder);
            } else if (!a(extractorInput, d2)) {
                return a(extractorInput, d2, positionHolder);
            } else {
                extractorInput.a();
                TimestampSearchResult a2 = timestampSeeker2.a(extractorInput, seekOperationParams2.e(), outputFrameHolder);
                int a3 = a2.f3246a;
                if (a3 == -3) {
                    a(false, d2);
                    return a(extractorInput, d2, positionHolder);
                } else if (a3 == -2) {
                    seekOperationParams2.b(a2.b, a2.c);
                } else if (a3 == -1) {
                    seekOperationParams2.a(a2.b, a2.c);
                } else if (a3 == 0) {
                    a(true, a2.c);
                    a(extractorInput, a2.c);
                    return a(extractorInput, a2.c, positionHolder);
                } else {
                    throw new IllegalStateException("Invalid case");
                }
            }
        }
    }

    public final boolean b() {
        return this.c != null;
    }

    /* access modifiers changed from: protected */
    public SeekOperationParams a(long j) {
        long j2 = j;
        return new SeekOperationParams(j2, this.f3243a.c(j2), this.f3243a.c, this.f3243a.d, this.f3243a.e, this.f3243a.f, this.f3243a.g);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z, long j) {
        this.c = null;
        this.b.a();
        b(z, j);
    }

    /* access modifiers changed from: protected */
    public final boolean a(ExtractorInput extractorInput, long j) throws IOException, InterruptedException {
        long position = j - extractorInput.getPosition();
        if (position < 0 || position > PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
            return false;
        }
        extractorInput.c((int) position);
        return true;
    }

    /* access modifiers changed from: protected */
    public final int a(ExtractorInput extractorInput, long j, PositionHolder positionHolder) {
        if (j == extractorInput.getPosition()) {
            return 0;
        }
        positionHolder.f3254a = j;
        return 1;
    }
}
