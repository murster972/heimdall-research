package com.google.android.exoplayer2.extractor.mp4;

final class DefaultSampleValues {

    /* renamed from: a  reason: collision with root package name */
    public final int f3286a;
    public final int b;
    public final int c;
    public final int d;

    public DefaultSampleValues(int i, int i2, int i3, int i4) {
        this.f3286a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }
}
