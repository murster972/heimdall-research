package com.google.android.exoplayer2.extractor.mp3;

import com.google.android.exoplayer2.extractor.MpegAudioHeader;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.SeekPoint;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;

final class XingSeeker implements Mp3Extractor.Seeker {

    /* renamed from: a  reason: collision with root package name */
    private final long f3276a;
    private final int b;
    private final long c;
    private final long d;
    private final long e;
    private final long[] f;

    private XingSeeker(long j, int i, long j2) {
        this(j, i, j2, -1, (long[]) null);
    }

    public static XingSeeker a(long j, long j2, MpegAudioHeader mpegAudioHeader, ParsableByteArray parsableByteArray) {
        int x;
        long j3 = j;
        MpegAudioHeader mpegAudioHeader2 = mpegAudioHeader;
        int i = mpegAudioHeader2.g;
        int i2 = mpegAudioHeader2.d;
        int h = parsableByteArray.h();
        if ((h & 1) != 1 || (x = parsableByteArray.x()) == 0) {
            return null;
        }
        long c2 = Util.c((long) x, ((long) i) * 1000000, (long) i2);
        if ((h & 6) != 6) {
            return new XingSeeker(j2, mpegAudioHeader2.c, c2);
        }
        long x2 = (long) parsableByteArray.x();
        long[] jArr = new long[100];
        for (int i3 = 0; i3 < 100; i3++) {
            jArr[i3] = (long) parsableByteArray.t();
        }
        if (j3 != -1) {
            long j4 = j2 + x2;
            if (j3 != j4) {
                Log.d("XingSeeker", "XING data size mismatch: " + j3 + ", " + j4);
            }
        }
        return new XingSeeker(j2, mpegAudioHeader2.c, c2, x2, jArr);
    }

    public boolean b() {
        return this.f != null;
    }

    public long getDurationUs() {
        return this.c;
    }

    private XingSeeker(long j, int i, long j2, long j3, long[] jArr) {
        this.f3276a = j;
        this.b = i;
        this.c = j2;
        this.f = jArr;
        this.d = j3;
        this.e = j3 != -1 ? j + j3 : -1;
    }

    public SeekMap.SeekPoints b(long j) {
        double d2;
        if (!b()) {
            return new SeekMap.SeekPoints(new SeekPoint(0, this.f3276a + ((long) this.b)));
        }
        long b2 = Util.b(j, 0, this.c);
        double d3 = (((double) b2) * 100.0d) / ((double) this.c);
        double d4 = 0.0d;
        if (d3 > 0.0d) {
            if (d3 >= 100.0d) {
                d4 = 256.0d;
            } else {
                int i = (int) d3;
                long[] jArr = this.f;
                Assertions.a(jArr);
                long[] jArr2 = jArr;
                double d5 = (double) jArr2[i];
                if (i == 99) {
                    d2 = 256.0d;
                } else {
                    d2 = (double) jArr2[i + 1];
                }
                d4 = d5 + ((d3 - ((double) i)) * (d2 - d5));
            }
        }
        return new SeekMap.SeekPoints(new SeekPoint(b2, this.f3276a + Util.b(Math.round((d4 / 256.0d) * ((double) this.d)), (long) this.b, this.d - 1)));
    }

    public long a(long j) {
        long j2;
        long j3 = j - this.f3276a;
        if (!b() || j3 <= ((long) this.b)) {
            return 0;
        }
        long[] jArr = this.f;
        Assertions.a(jArr);
        long[] jArr2 = jArr;
        double d2 = (((double) j3) * 256.0d) / ((double) this.d);
        int b2 = Util.b(jArr2, (long) d2, true, true);
        long a2 = a(b2);
        long j4 = jArr2[b2];
        int i = b2 + 1;
        long a3 = a(i);
        if (b2 == 99) {
            j2 = 256;
        } else {
            j2 = jArr2[i];
        }
        return a2 + Math.round((j4 == j2 ? 0.0d : (d2 - ((double) j4)) / ((double) (j2 - j4))) * ((double) (a3 - a2)));
    }

    public long a() {
        return this.e;
    }

    private long a(int i) {
        return (this.c * ((long) i)) / 100;
    }
}
