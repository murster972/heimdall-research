package com.google.android.exoplayer2.extractor.ogg;

import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;

final class OggPageHeader {
    private static final int i = Util.c("OggS");

    /* renamed from: a  reason: collision with root package name */
    public int f3307a;
    public int b;
    public long c;
    public int d;
    public int e;
    public int f;
    public final int[] g = new int[JfifUtil.MARKER_FIRST_BYTE];
    private final ParsableByteArray h = new ParsableByteArray((int) JfifUtil.MARKER_FIRST_BYTE);

    OggPageHeader() {
    }

    public void a() {
        this.f3307a = 0;
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
    }

    public boolean a(ExtractorInput extractorInput, boolean z) throws IOException, InterruptedException {
        this.h.B();
        a();
        if (!(extractorInput.getLength() == -1 || extractorInput.getLength() - extractorInput.b() >= 27) || !extractorInput.a(this.h.f3641a, 0, 27, true)) {
            if (z) {
                return false;
            }
            throw new EOFException();
        } else if (this.h.v() == ((long) i)) {
            this.f3307a = this.h.t();
            if (this.f3307a == 0) {
                this.b = this.h.t();
                this.c = this.h.l();
                this.h.m();
                this.h.m();
                this.h.m();
                this.d = this.h.t();
                this.e = this.d + 27;
                this.h.B();
                extractorInput.a(this.h.f3641a, 0, this.d);
                for (int i2 = 0; i2 < this.d; i2++) {
                    this.g[i2] = this.h.t();
                    this.f += this.g[i2];
                }
                return true;
            } else if (z) {
                return false;
            } else {
                throw new ParserException("unsupported bit stream revision");
            }
        } else if (z) {
            return false;
        } else {
            throw new ParserException("expected OggS capture pattern at begin of page");
        }
    }
}
