package com.google.android.exoplayer2.extractor.rawcc;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

public final class RawCcExtractor implements Extractor {
    private static final int i = Util.c("RCC\u0001");

    /* renamed from: a  reason: collision with root package name */
    private final Format f3315a;
    private final ParsableByteArray b = new ParsableByteArray(9);
    private TrackOutput c;
    private int d = 0;
    private int e;
    private long f;
    private int g;
    private int h;

    public RawCcExtractor(Format format) {
        this.f3315a = format;
    }

    private boolean b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        this.b.B();
        if (!extractorInput.b(this.b.f3641a, 0, 8, true)) {
            return false;
        }
        if (this.b.h() == i) {
            this.e = this.b.t();
            return true;
        }
        throw new IOException("Input not RawCC");
    }

    private void c(ExtractorInput extractorInput) throws IOException, InterruptedException {
        while (this.g > 0) {
            this.b.B();
            extractorInput.readFully(this.b.f3641a, 0, 3);
            this.c.a(this.b, 3);
            this.h += 3;
            this.g--;
        }
        int i2 = this.h;
        if (i2 > 0) {
            this.c.a(this.f, 1, i2, 0, (TrackOutput.CryptoData) null);
        }
    }

    private boolean d(ExtractorInput extractorInput) throws IOException, InterruptedException {
        this.b.B();
        int i2 = this.e;
        if (i2 == 0) {
            if (!extractorInput.b(this.b.f3641a, 0, 5, true)) {
                return false;
            }
            this.f = (this.b.v() * 1000) / 45;
        } else if (i2 != 1) {
            throw new ParserException("Unsupported version number: " + this.e);
        } else if (!extractorInput.b(this.b.f3641a, 0, 9, true)) {
            return false;
        } else {
            this.f = this.b.p();
        }
        this.g = this.b.t();
        this.h = 0;
        return true;
    }

    public void a(ExtractorOutput extractorOutput) {
        extractorOutput.a(new SeekMap.Unseekable(-9223372036854775807L));
        this.c = extractorOutput.a(0, 3);
        extractorOutput.a();
        this.c.a(this.f3315a);
    }

    public void release() {
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        this.b.B();
        extractorInput.a(this.b.f3641a, 0, 8);
        if (this.b.h() == i) {
            return true;
        }
        return false;
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        while (true) {
            int i2 = this.d;
            if (i2 != 0) {
                if (i2 != 1) {
                    if (i2 == 2) {
                        c(extractorInput);
                        this.d = 1;
                        return 0;
                    }
                    throw new IllegalStateException();
                } else if (d(extractorInput)) {
                    this.d = 2;
                } else {
                    this.d = 0;
                    return -1;
                }
            } else if (!b(extractorInput)) {
                return -1;
            } else {
                this.d = 1;
            }
        }
    }

    public void a(long j, long j2) {
        this.d = 0;
    }
}
