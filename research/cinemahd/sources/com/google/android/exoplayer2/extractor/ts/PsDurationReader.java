package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

final class PsDurationReader {

    /* renamed from: a  reason: collision with root package name */
    private final TimestampAdjuster f3336a = new TimestampAdjuster(0);
    private final ParsableByteArray b = new ParsableByteArray();
    private boolean c;
    private boolean d;
    private boolean e;
    private long f = -9223372036854775807L;
    private long g = -9223372036854775807L;
    private long h = -9223372036854775807L;

    PsDurationReader() {
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        if (!this.e) {
            return c(extractorInput, positionHolder);
        }
        if (this.g == -9223372036854775807L) {
            return a(extractorInput);
        }
        if (!this.d) {
            return b(extractorInput, positionHolder);
        }
        long j = this.f;
        if (j == -9223372036854775807L) {
            return a(extractorInput);
        }
        this.h = this.f3336a.b(this.g) - this.f3336a.b(j);
        return a(extractorInput);
    }

    public TimestampAdjuster b() {
        return this.f3336a;
    }

    public boolean c() {
        return this.c;
    }

    private int b(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        int min = (int) Math.min(20000, extractorInput.getLength());
        long j = (long) 0;
        if (extractorInput.getPosition() != j) {
            positionHolder.f3254a = j;
            return 1;
        }
        this.b.c(min);
        extractorInput.a();
        extractorInput.a(this.b.f3641a, 0, min);
        this.f = a(this.b);
        this.d = true;
        return 0;
    }

    public static long c(ParsableByteArray parsableByteArray) {
        int c2 = parsableByteArray.c();
        if (parsableByteArray.a() < 9) {
            return -9223372036854775807L;
        }
        byte[] bArr = new byte[9];
        parsableByteArray.a(bArr, 0, bArr.length);
        parsableByteArray.e(c2);
        if (!a(bArr)) {
            return -9223372036854775807L;
        }
        return b(bArr);
    }

    private int c(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        long length = extractorInput.getLength();
        int min = (int) Math.min(20000, length);
        long j = length - ((long) min);
        if (extractorInput.getPosition() != j) {
            positionHolder.f3254a = j;
            return 1;
        }
        this.b.c(min);
        extractorInput.a();
        extractorInput.a(this.b.f3641a, 0, min);
        this.g = b(this.b);
        this.e = true;
        return 0;
    }

    private long b(ParsableByteArray parsableByteArray) {
        int c2 = parsableByteArray.c();
        for (int d2 = parsableByteArray.d() - 4; d2 >= c2; d2--) {
            if (a(parsableByteArray.f3641a, d2) == 442) {
                parsableByteArray.e(d2 + 4);
                long c3 = c(parsableByteArray);
                if (c3 != -9223372036854775807L) {
                    return c3;
                }
            }
        }
        return -9223372036854775807L;
    }

    public long a() {
        return this.h;
    }

    private int a(ExtractorInput extractorInput) {
        this.b.a(Util.f);
        this.c = true;
        extractorInput.a();
        return 0;
    }

    private static long b(byte[] bArr) {
        return (((((long) bArr[0]) & 56) >> 3) << 30) | ((((long) bArr[0]) & 3) << 28) | ((((long) bArr[1]) & 255) << 20) | (((((long) bArr[2]) & 248) >> 3) << 15) | ((((long) bArr[2]) & 3) << 13) | ((((long) bArr[3]) & 255) << 5) | ((((long) bArr[4]) & 248) >> 3);
    }

    private long a(ParsableByteArray parsableByteArray) {
        int d2 = parsableByteArray.d();
        for (int c2 = parsableByteArray.c(); c2 < d2 - 3; c2++) {
            if (a(parsableByteArray.f3641a, c2) == 442) {
                parsableByteArray.e(c2 + 4);
                long c3 = c(parsableByteArray);
                if (c3 != -9223372036854775807L) {
                    return c3;
                }
            }
        }
        return -9223372036854775807L;
    }

    private int a(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    private static boolean a(byte[] bArr) {
        if ((bArr[0] & 196) == 68 && (bArr[2] & 4) == 4 && (bArr[4] & 4) == 4 && (bArr[5] & 1) == 1 && (bArr[8] & 3) == 3) {
            return true;
        }
        return false;
    }
}
