package com.google.android.exoplayer2.extractor.mp4;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;

final class TrackFragment {

    /* renamed from: a  reason: collision with root package name */
    public DefaultSampleValues f3298a;
    public long b;
    public long c;
    public long d;
    public int e;
    public int f;
    public long[] g;
    public int[] h;
    public int[] i;
    public int[] j;
    public long[] k;
    public boolean[] l;
    public boolean m;
    public boolean[] n;
    public TrackEncryptionBox o;
    public int p;
    public ParsableByteArray q;
    public boolean r;
    public long s;

    TrackFragment() {
    }

    public void a() {
        this.e = 0;
        this.s = 0;
        this.m = false;
        this.r = false;
        this.o = null;
    }

    public void b(int i2) {
        ParsableByteArray parsableByteArray = this.q;
        if (parsableByteArray == null || parsableByteArray.d() < i2) {
            this.q = new ParsableByteArray(i2);
        }
        this.p = i2;
        this.m = true;
        this.r = true;
    }

    public boolean c(int i2) {
        return this.m && this.n[i2];
    }

    public void a(int i2, int i3) {
        this.e = i2;
        this.f = i3;
        int[] iArr = this.h;
        if (iArr == null || iArr.length < i2) {
            this.g = new long[i2];
            this.h = new int[i2];
        }
        int[] iArr2 = this.i;
        if (iArr2 == null || iArr2.length < i3) {
            int i4 = (i3 * 125) / 100;
            this.i = new int[i4];
            this.j = new int[i4];
            this.k = new long[i4];
            this.l = new boolean[i4];
            this.n = new boolean[i4];
        }
    }

    public void a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.readFully(this.q.f3641a, 0, this.p);
        this.q.e(0);
        this.r = false;
    }

    public void a(ParsableByteArray parsableByteArray) {
        parsableByteArray.a(this.q.f3641a, 0, this.p);
        this.q.e(0);
        this.r = false;
    }

    public long a(int i2) {
        return this.k[i2] + ((long) this.j[i2]);
    }
}
