package com.google.android.exoplayer2.extractor;

import java.io.IOException;

public interface Extractor {
    int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException;

    void a(long j, long j2);

    void a(ExtractorOutput extractorOutput);

    boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException;

    void release();
}
