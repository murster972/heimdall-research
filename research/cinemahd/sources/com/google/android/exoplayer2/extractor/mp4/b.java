package com.google.android.exoplayer2.extractor.mp4;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* compiled from: lambda */
public final /* synthetic */ class b implements ExtractorsFactory {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ b f3301a = new b();

    private /* synthetic */ b() {
    }

    public final Extractor[] a() {
        return Mp4Extractor.d();
    }
}
