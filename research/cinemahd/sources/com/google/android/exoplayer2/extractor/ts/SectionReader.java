package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;

public final class SectionReader implements TsPayloadReader {

    /* renamed from: a  reason: collision with root package name */
    private final SectionPayloadReader f3339a;
    private final ParsableByteArray b = new ParsableByteArray(32);
    private int c;
    private int d;
    private boolean e;
    private boolean f;

    public SectionReader(SectionPayloadReader sectionPayloadReader) {
        this.f3339a = sectionPayloadReader;
    }

    public void a(TimestampAdjuster timestampAdjuster, ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        this.f3339a.a(timestampAdjuster, extractorOutput, trackIdGenerator);
        this.f = true;
    }

    public void a() {
        this.f = true;
    }

    public void a(ParsableByteArray parsableByteArray, boolean z) {
        int t = z ? parsableByteArray.t() + parsableByteArray.c() : -1;
        if (this.f) {
            if (z) {
                this.f = false;
                parsableByteArray.e(t);
                this.d = 0;
            } else {
                return;
            }
        }
        while (parsableByteArray.a() > 0) {
            int i = this.d;
            boolean z2 = true;
            if (i < 3) {
                if (i == 0) {
                    int t2 = parsableByteArray.t();
                    parsableByteArray.e(parsableByteArray.c() - 1);
                    if (t2 == 255) {
                        this.f = true;
                        return;
                    }
                }
                int min = Math.min(parsableByteArray.a(), 3 - this.d);
                parsableByteArray.a(this.b.f3641a, this.d, min);
                this.d += min;
                if (this.d == 3) {
                    this.b.c(3);
                    this.b.f(1);
                    int t3 = this.b.t();
                    int t4 = this.b.t();
                    if ((t3 & 128) == 0) {
                        z2 = false;
                    }
                    this.e = z2;
                    this.c = (((t3 & 15) << 8) | t4) + 3;
                    int b2 = this.b.b();
                    int i2 = this.c;
                    if (b2 < i2) {
                        ParsableByteArray parsableByteArray2 = this.b;
                        byte[] bArr = parsableByteArray2.f3641a;
                        parsableByteArray2.c(Math.min(4098, Math.max(i2, bArr.length * 2)));
                        System.arraycopy(bArr, 0, this.b.f3641a, 0, 3);
                    }
                }
            } else {
                int min2 = Math.min(parsableByteArray.a(), this.c - this.d);
                parsableByteArray.a(this.b.f3641a, this.d, min2);
                this.d += min2;
                int i3 = this.d;
                int i4 = this.c;
                if (i3 != i4) {
                    continue;
                } else {
                    if (!this.e) {
                        this.b.c(i4);
                    } else if (Util.a(this.b.f3641a, 0, i4, -1) != 0) {
                        this.f = true;
                        return;
                    } else {
                        this.b.c(this.c - 4);
                    }
                    this.f3339a.a(this.b);
                    this.d = 0;
                }
            }
        }
    }
}
