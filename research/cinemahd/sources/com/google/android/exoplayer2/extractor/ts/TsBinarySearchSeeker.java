package com.google.android.exoplayer2.extractor.ts;

import com.google.android.exoplayer2.extractor.BinarySearchSeeker;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;

final class TsBinarySearchSeeker extends BinarySearchSeeker {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TsBinarySearchSeeker(com.google.android.exoplayer2.util.TimestampAdjuster r17, long r18, long r20, int r22) {
        /*
            r16 = this;
            com.google.android.exoplayer2.extractor.BinarySearchSeeker$DefaultSeekTimestampConverter r1 = new com.google.android.exoplayer2.extractor.BinarySearchSeeker$DefaultSeekTimestampConverter
            r1.<init>()
            com.google.android.exoplayer2.extractor.ts.TsBinarySearchSeeker$TsPcrSeeker r2 = new com.google.android.exoplayer2.extractor.ts.TsBinarySearchSeeker$TsPcrSeeker
            r0 = r17
            r3 = r22
            r2.<init>(r3, r0)
            r3 = 1
            long r7 = r18 + r3
            r5 = 0
            r9 = 0
            r13 = 188(0xbc, double:9.3E-322)
            r15 = 940(0x3ac, float:1.317E-42)
            r0 = r16
            r3 = r18
            r11 = r20
            r0.<init>(r1, r2, r3, r5, r7, r9, r11, r13, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.ts.TsBinarySearchSeeker.<init>(com.google.android.exoplayer2.util.TimestampAdjuster, long, long, int):void");
    }

    private static final class TsPcrSeeker implements BinarySearchSeeker.TimestampSeeker {

        /* renamed from: a  reason: collision with root package name */
        private final TimestampAdjuster f3342a;
        private final ParsableByteArray b = new ParsableByteArray();
        private final int c;

        public TsPcrSeeker(int i, TimestampAdjuster timestampAdjuster) {
            this.c = i;
            this.f3342a = timestampAdjuster;
        }

        public BinarySearchSeeker.TimestampSearchResult a(ExtractorInput extractorInput, long j, BinarySearchSeeker.OutputFrameHolder outputFrameHolder) throws IOException, InterruptedException {
            long position = extractorInput.getPosition();
            int min = (int) Math.min(112800, extractorInput.getLength() - position);
            this.b.c(min);
            extractorInput.a(this.b.f3641a, 0, min);
            return a(this.b, j, position);
        }

        private BinarySearchSeeker.TimestampSearchResult a(ParsableByteArray parsableByteArray, long j, long j2) {
            int a2;
            int i;
            ParsableByteArray parsableByteArray2 = parsableByteArray;
            long j3 = j2;
            int d = parsableByteArray.d();
            long j4 = -1;
            long j5 = -1;
            long j6 = -9223372036854775807L;
            while (parsableByteArray.a() >= 188 && (i = a2 + 188) <= d) {
                long a3 = TsUtil.a(parsableByteArray2, (a2 = TsUtil.a(parsableByteArray2.f3641a, parsableByteArray.c(), d)), this.c);
                if (a3 != -9223372036854775807L) {
                    long b2 = this.f3342a.b(a3);
                    if (b2 > j) {
                        if (j6 == -9223372036854775807L) {
                            return BinarySearchSeeker.TimestampSearchResult.a(b2, j3);
                        }
                        return BinarySearchSeeker.TimestampSearchResult.a(j3 + j5);
                    } else if (100000 + b2 > j) {
                        return BinarySearchSeeker.TimestampSearchResult.a(j3 + ((long) a2));
                    } else {
                        j5 = (long) a2;
                        j6 = b2;
                    }
                }
                parsableByteArray2.e(i);
                j4 = (long) i;
            }
            if (j6 != -9223372036854775807L) {
                return BinarySearchSeeker.TimestampSearchResult.b(j6, j3 + j4);
            }
            return BinarySearchSeeker.TimestampSearchResult.d;
        }

        public void a() {
            this.b.a(Util.f);
        }
    }
}
