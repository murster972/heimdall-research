package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.util.Assertions;

public interface SeekMap {

    public static final class SeekPoints {

        /* renamed from: a  reason: collision with root package name */
        public final SeekPoint f3255a;
        public final SeekPoint b;

        public SeekPoints(SeekPoint seekPoint) {
            this(seekPoint, seekPoint);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || SeekPoints.class != obj.getClass()) {
                return false;
            }
            SeekPoints seekPoints = (SeekPoints) obj;
            if (!this.f3255a.equals(seekPoints.f3255a) || !this.b.equals(seekPoints.b)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (this.f3255a.hashCode() * 31) + this.b.hashCode();
        }

        public String toString() {
            String str;
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(this.f3255a);
            if (this.f3255a.equals(this.b)) {
                str = "";
            } else {
                str = ", " + this.b;
            }
            sb.append(str);
            sb.append("]");
            return sb.toString();
        }

        public SeekPoints(SeekPoint seekPoint, SeekPoint seekPoint2) {
            Assertions.a(seekPoint);
            this.f3255a = seekPoint;
            Assertions.a(seekPoint2);
            this.b = seekPoint2;
        }
    }

    public static final class Unseekable implements SeekMap {

        /* renamed from: a  reason: collision with root package name */
        private final long f3256a;
        private final SeekPoints b;

        public Unseekable(long j) {
            this(j, 0);
        }

        public SeekPoints b(long j) {
            return this.b;
        }

        public boolean b() {
            return false;
        }

        public long getDurationUs() {
            return this.f3256a;
        }

        public Unseekable(long j, long j2) {
            this.f3256a = j;
            this.b = new SeekPoints(j2 == 0 ? SeekPoint.c : new SeekPoint(0, j2));
        }
    }

    SeekPoints b(long j);

    boolean b();

    long getDurationUs();
}
