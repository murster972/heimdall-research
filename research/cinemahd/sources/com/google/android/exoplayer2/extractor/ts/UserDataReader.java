package com.google.android.exoplayer2.extractor.ts;

import com.facebook.common.time.Clock;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.text.cea.CeaUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.List;

final class UserDataReader {

    /* renamed from: a  reason: collision with root package name */
    private final List<Format> f3350a;
    private final TrackOutput[] b;

    public UserDataReader(List<Format> list) {
        this.f3350a = list;
        this.b = new TrackOutput[list.size()];
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        for (int i = 0; i < this.b.length; i++) {
            trackIdGenerator.a();
            TrackOutput a2 = extractorOutput.a(trackIdGenerator.c(), 3);
            Format format = this.f3350a.get(i);
            String str = format.g;
            boolean z = "application/cea-608".equals(str) || "application/cea-708".equals(str);
            Assertions.a(z, "Invalid closed caption mime type provided: " + str);
            a2.a(Format.a(trackIdGenerator.b(), str, (String) null, -1, format.y, format.z, format.A, (DrmInitData) null, Clock.MAX_TIME, format.i));
            this.b[i] = a2;
        }
    }

    public void a(long j, ParsableByteArray parsableByteArray) {
        if (parsableByteArray.a() >= 9) {
            int h = parsableByteArray.h();
            int h2 = parsableByteArray.h();
            int t = parsableByteArray.t();
            if (h == 434 && h2 == CeaUtil.f3523a && t == 3) {
                CeaUtil.b(j, parsableByteArray, this.b);
            }
        }
    }
}
