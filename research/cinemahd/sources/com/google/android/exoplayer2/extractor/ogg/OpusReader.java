package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ogg.StreamReader;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.joda.time.DateTimeConstants;

final class OpusReader extends StreamReader {
    private static final int o = Util.c("Opus");
    private static final byte[] p = {79, 112, 117, 115, 72, 101, 97, 100};
    private boolean n;

    OpusReader() {
    }

    public static boolean b(ParsableByteArray parsableByteArray) {
        int a2 = parsableByteArray.a();
        byte[] bArr = p;
        if (a2 < bArr.length) {
            return false;
        }
        byte[] bArr2 = new byte[bArr.length];
        parsableByteArray.a(bArr2, 0, bArr.length);
        return Arrays.equals(bArr2, p);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        super.a(z);
        if (z) {
            this.n = false;
        }
    }

    /* access modifiers changed from: protected */
    public long a(ParsableByteArray parsableByteArray) {
        return b(a(parsableByteArray.f3641a));
    }

    /* access modifiers changed from: protected */
    public boolean a(ParsableByteArray parsableByteArray, long j, StreamReader.SetupData setupData) {
        boolean z = true;
        if (!this.n) {
            byte[] copyOf = Arrays.copyOf(parsableByteArray.f3641a, parsableByteArray.d());
            byte b = copyOf[9] & 255;
            ArrayList arrayList = new ArrayList(3);
            arrayList.add(copyOf);
            a(arrayList, ((copyOf[11] & 255) << 8) | (copyOf[10] & 255));
            a(arrayList, 3840);
            setupData.f3309a = Format.a((String) null, "audio/opus", (String) null, -1, -1, (int) b, 48000, (List<byte[]>) arrayList, (DrmInitData) null, 0, (String) null);
            this.n = true;
            return true;
        }
        if (parsableByteArray.h() != o) {
            z = false;
        }
        parsableByteArray.e(0);
        return z;
    }

    private void a(List<byte[]> list, int i) {
        list.add(ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong((((long) i) * 1000000000) / 48000).array());
    }

    private long a(byte[] bArr) {
        byte b = bArr[0] & 255;
        byte b2 = b & 3;
        byte b3 = 2;
        if (b2 == 0) {
            b3 = 1;
        } else if (!(b2 == 1 || b2 == 2)) {
            b3 = bArr[1] & 63;
        }
        int i = b >> 3;
        int i2 = i & 3;
        return ((long) b3) * ((long) (i >= 16 ? 2500 << i2 : i >= 12 ? 10000 << (i2 & 1) : i2 == 3 ? DateTimeConstants.MILLIS_PER_MINUTE : 10000 << i2));
    }
}
