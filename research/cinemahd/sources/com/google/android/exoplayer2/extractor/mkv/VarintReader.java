package com.google.android.exoplayer2.extractor.mkv;

import com.google.android.exoplayer2.extractor.ExtractorInput;
import java.io.IOException;

final class VarintReader {
    private static final long[] d = {128, 64, 32, 16, 8, 4, 2, 1};

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f3271a = new byte[8];
    private int b;
    private int c;

    public long a(ExtractorInput extractorInput, boolean z, boolean z2, int i) throws IOException, InterruptedException {
        if (this.b == 0) {
            if (!extractorInput.b(this.f3271a, 0, 1, z)) {
                return -1;
            }
            this.c = a(this.f3271a[0] & 255);
            if (this.c != -1) {
                this.b = 1;
            } else {
                throw new IllegalStateException("No valid varint length mask found");
            }
        }
        int i2 = this.c;
        if (i2 > i) {
            this.b = 0;
            return -2;
        }
        if (i2 != 1) {
            extractorInput.readFully(this.f3271a, 1, i2 - 1);
        }
        this.b = 0;
        return a(this.f3271a, this.c, z2);
    }

    public void b() {
        this.b = 0;
        this.c = 0;
    }

    public int a() {
        return this.c;
    }

    public static int a(int i) {
        int i2 = 0;
        while (true) {
            long[] jArr = d;
            if (i2 >= jArr.length) {
                return -1;
            }
            if ((jArr[i2] & ((long) i)) != 0) {
                return i2 + 1;
            }
            i2++;
        }
    }

    public static long a(byte[] bArr, int i, boolean z) {
        long j = ((long) bArr[0]) & 255;
        if (z) {
            j &= ~d[i - 1];
        }
        for (int i2 = 1; i2 < i; i2++) {
            j = (j << 8) | (((long) bArr[i2]) & 255);
        }
        return j;
    }
}
