package com.google.android.exoplayer2.extractor.ogg;

import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ogg.StreamReader;
import com.google.android.exoplayer2.extractor.ogg.VorbisUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

final class VorbisReader extends StreamReader {
    private VorbisSetup n;
    private int o;
    private boolean p;
    private VorbisUtil.VorbisIdHeader q;
    private VorbisUtil.CommentHeader r;

    static final class VorbisSetup {

        /* renamed from: a  reason: collision with root package name */
        public final VorbisUtil.VorbisIdHeader f3311a;
        public final byte[] b;
        public final VorbisUtil.Mode[] c;
        public final int d;

        public VorbisSetup(VorbisUtil.VorbisIdHeader vorbisIdHeader, VorbisUtil.CommentHeader commentHeader, byte[] bArr, VorbisUtil.Mode[] modeArr, int i) {
            this.f3311a = vorbisIdHeader;
            this.b = bArr;
            this.c = modeArr;
            this.d = i;
        }
    }

    VorbisReader() {
    }

    static int a(byte b, int i, int i2) {
        return (b >> i2) & (JfifUtil.MARKER_FIRST_BYTE >>> (8 - i));
    }

    public static boolean c(ParsableByteArray parsableByteArray) {
        try {
            return VorbisUtil.a(1, parsableByteArray, true);
        } catch (ParserException unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        super.a(z);
        if (z) {
            this.n = null;
            this.q = null;
            this.r = null;
        }
        this.o = 0;
        this.p = false;
    }

    /* access modifiers changed from: package-private */
    public VorbisSetup b(ParsableByteArray parsableByteArray) throws IOException {
        if (this.q == null) {
            this.q = VorbisUtil.b(parsableByteArray);
            return null;
        } else if (this.r == null) {
            this.r = VorbisUtil.a(parsableByteArray);
            return null;
        } else {
            byte[] bArr = new byte[parsableByteArray.d()];
            System.arraycopy(parsableByteArray.f3641a, 0, bArr, 0, parsableByteArray.d());
            VorbisUtil.Mode[] a2 = VorbisUtil.a(parsableByteArray, this.q.f3313a);
            return new VorbisSetup(this.q, this.r, bArr, a2, VorbisUtil.a(a2.length - 1));
        }
    }

    /* access modifiers changed from: protected */
    public void c(long j) {
        super.c(j);
        int i = 0;
        this.p = j != 0;
        VorbisUtil.VorbisIdHeader vorbisIdHeader = this.q;
        if (vorbisIdHeader != null) {
            i = vorbisIdHeader.d;
        }
        this.o = i;
    }

    /* access modifiers changed from: protected */
    public long a(ParsableByteArray parsableByteArray) {
        byte[] bArr = parsableByteArray.f3641a;
        int i = 0;
        if ((bArr[0] & 1) == 1) {
            return -1;
        }
        int a2 = a(bArr[0], this.n);
        if (this.p) {
            i = (this.o + a2) / 4;
        }
        long j = (long) i;
        a(parsableByteArray, j);
        this.p = true;
        this.o = a2;
        return j;
    }

    /* access modifiers changed from: protected */
    public boolean a(ParsableByteArray parsableByteArray, long j, StreamReader.SetupData setupData) throws IOException, InterruptedException {
        if (this.n != null) {
            return false;
        }
        this.n = b(parsableByteArray);
        if (this.n == null) {
            return true;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.n.f3311a.f);
        arrayList.add(this.n.b);
        VorbisUtil.VorbisIdHeader vorbisIdHeader = this.n.f3311a;
        setupData.f3309a = Format.a((String) null, "audio/vorbis", (String) null, vorbisIdHeader.c, -1, vorbisIdHeader.f3313a, (int) vorbisIdHeader.b, (List<byte[]>) arrayList, (DrmInitData) null, 0, (String) null);
        return true;
    }

    static void a(ParsableByteArray parsableByteArray, long j) {
        parsableByteArray.d(parsableByteArray.d() + 4);
        parsableByteArray.f3641a[parsableByteArray.d() - 4] = (byte) ((int) (j & 255));
        parsableByteArray.f3641a[parsableByteArray.d() - 3] = (byte) ((int) ((j >>> 8) & 255));
        parsableByteArray.f3641a[parsableByteArray.d() - 2] = (byte) ((int) ((j >>> 16) & 255));
        parsableByteArray.f3641a[parsableByteArray.d() - 1] = (byte) ((int) ((j >>> 24) & 255));
    }

    private static int a(byte b, VorbisSetup vorbisSetup) {
        if (!vorbisSetup.c[a(b, vorbisSetup.d, 1)].f3312a) {
            return vorbisSetup.f3311a.d;
        }
        return vorbisSetup.f3311a.e;
    }
}
