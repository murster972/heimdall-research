package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.Arrays;

final class VorbisUtil {

    public static final class CodeBook {
        public CodeBook(int i, int i2, long[] jArr, int i3, boolean z) {
        }
    }

    public static final class CommentHeader {
        public CommentHeader(String str, String[] strArr, int i) {
        }
    }

    public static final class Mode {

        /* renamed from: a  reason: collision with root package name */
        public final boolean f3312a;

        public Mode(boolean z, int i, int i2, int i3) {
            this.f3312a = z;
        }
    }

    public static final class VorbisIdHeader {

        /* renamed from: a  reason: collision with root package name */
        public final int f3313a;
        public final long b;
        public final int c;
        public final int d;
        public final int e;
        public final byte[] f;

        public VorbisIdHeader(long j, int i, long j2, int i2, int i3, int i4, int i5, int i6, boolean z, byte[] bArr) {
            this.f3313a = i;
            this.b = j2;
            this.c = i3;
            this.d = i5;
            this.e = i6;
            this.f = bArr;
        }
    }

    private VorbisUtil() {
    }

    public static int a(int i) {
        int i2 = 0;
        while (i > 0) {
            i2++;
            i >>>= 1;
        }
        return i2;
    }

    public static CommentHeader a(ParsableByteArray parsableByteArray) throws ParserException {
        a(3, parsableByteArray, false);
        String b = parsableByteArray.b((int) parsableByteArray.m());
        int length = 11 + b.length();
        long m = parsableByteArray.m();
        String[] strArr = new String[((int) m)];
        int i = length + 4;
        for (int i2 = 0; ((long) i2) < m; i2++) {
            strArr[i2] = parsableByteArray.b((int) parsableByteArray.m());
            i = i + 4 + strArr[i2].length();
        }
        if ((parsableByteArray.t() & 1) != 0) {
            return new CommentHeader(b, strArr, i + 1);
        }
        throw new ParserException("framing bit expected to be set");
    }

    public static VorbisIdHeader b(ParsableByteArray parsableByteArray) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        a(1, parsableByteArray2, false);
        long m = parsableByteArray.m();
        int t = parsableByteArray.t();
        long m2 = parsableByteArray.m();
        int k = parsableByteArray.k();
        int k2 = parsableByteArray.k();
        int k3 = parsableByteArray.k();
        int t2 = parsableByteArray.t();
        return new VorbisIdHeader(m, t, m2, k, k2, k3, (int) Math.pow(2.0d, (double) (t2 & 15)), (int) Math.pow(2.0d, (double) ((t2 & 240) >> 4)), (parsableByteArray.t() & 1) > 0, Arrays.copyOf(parsableByteArray2.f3641a, parsableByteArray.d()));
    }

    private static Mode[] c(VorbisBitArray vorbisBitArray) {
        int a2 = vorbisBitArray.a(6) + 1;
        Mode[] modeArr = new Mode[a2];
        for (int i = 0; i < a2; i++) {
            modeArr[i] = new Mode(vorbisBitArray.b(), vorbisBitArray.a(16), vorbisBitArray.a(16), vorbisBitArray.a(8));
        }
        return modeArr;
    }

    private static void d(VorbisBitArray vorbisBitArray) throws ParserException {
        int a2 = vorbisBitArray.a(6) + 1;
        int i = 0;
        while (i < a2) {
            if (vorbisBitArray.a(16) <= 2) {
                vorbisBitArray.b(24);
                vorbisBitArray.b(24);
                vorbisBitArray.b(24);
                int a3 = vorbisBitArray.a(6) + 1;
                vorbisBitArray.b(8);
                int[] iArr = new int[a3];
                for (int i2 = 0; i2 < a3; i2++) {
                    iArr[i2] = ((vorbisBitArray.b() ? vorbisBitArray.a(5) : 0) * 8) + vorbisBitArray.a(3);
                }
                for (int i3 = 0; i3 < a3; i3++) {
                    for (int i4 = 0; i4 < 8; i4++) {
                        if ((iArr[i3] & (1 << i4)) != 0) {
                            vorbisBitArray.b(8);
                        }
                    }
                }
                i++;
            } else {
                throw new ParserException("residueType greater than 2 is not decodable");
            }
        }
    }

    public static boolean a(int i, ParsableByteArray parsableByteArray, boolean z) throws ParserException {
        if (parsableByteArray.a() < 7) {
            if (z) {
                return false;
            }
            throw new ParserException("too short header: " + parsableByteArray.a());
        } else if (parsableByteArray.t() != i) {
            if (z) {
                return false;
            }
            throw new ParserException("expected header type " + Integer.toHexString(i));
        } else if (parsableByteArray.t() == 118 && parsableByteArray.t() == 111 && parsableByteArray.t() == 114 && parsableByteArray.t() == 98 && parsableByteArray.t() == 105 && parsableByteArray.t() == 115) {
            return true;
        } else {
            if (z) {
                return false;
            }
            throw new ParserException("expected characters 'vorbis'");
        }
    }

    private static void b(VorbisBitArray vorbisBitArray) throws ParserException {
        int a2 = vorbisBitArray.a(6) + 1;
        for (int i = 0; i < a2; i++) {
            int a3 = vorbisBitArray.a(16);
            if (a3 == 0) {
                vorbisBitArray.b(8);
                vorbisBitArray.b(16);
                vorbisBitArray.b(16);
                vorbisBitArray.b(6);
                vorbisBitArray.b(8);
                int a4 = vorbisBitArray.a(4) + 1;
                for (int i2 = 0; i2 < a4; i2++) {
                    vorbisBitArray.b(8);
                }
            } else if (a3 == 1) {
                int a5 = vorbisBitArray.a(5);
                int[] iArr = new int[a5];
                int i3 = -1;
                for (int i4 = 0; i4 < a5; i4++) {
                    iArr[i4] = vorbisBitArray.a(4);
                    if (iArr[i4] > i3) {
                        i3 = iArr[i4];
                    }
                }
                int[] iArr2 = new int[(i3 + 1)];
                for (int i5 = 0; i5 < iArr2.length; i5++) {
                    iArr2[i5] = vorbisBitArray.a(3) + 1;
                    int a6 = vorbisBitArray.a(2);
                    if (a6 > 0) {
                        vorbisBitArray.b(8);
                    }
                    for (int i6 = 0; i6 < (1 << a6); i6++) {
                        vorbisBitArray.b(8);
                    }
                }
                vorbisBitArray.b(2);
                int a7 = vorbisBitArray.a(4);
                int i7 = 0;
                int i8 = 0;
                for (int i9 = 0; i9 < a5; i9++) {
                    i7 += iArr2[iArr[i9]];
                    while (i8 < i7) {
                        vorbisBitArray.b(a7);
                        i8++;
                    }
                }
            } else {
                throw new ParserException("floor type greater than 1 not decodable: " + a3);
            }
        }
    }

    public static Mode[] a(ParsableByteArray parsableByteArray, int i) throws ParserException {
        int i2 = 0;
        a(5, parsableByteArray, false);
        int t = parsableByteArray.t() + 1;
        VorbisBitArray vorbisBitArray = new VorbisBitArray(parsableByteArray.f3641a);
        vorbisBitArray.b(parsableByteArray.c() * 8);
        for (int i3 = 0; i3 < t; i3++) {
            a(vorbisBitArray);
        }
        int a2 = vorbisBitArray.a(6) + 1;
        while (i2 < a2) {
            if (vorbisBitArray.a(16) == 0) {
                i2++;
            } else {
                throw new ParserException("placeholder of time domain transforms not zeroed out");
            }
        }
        b(vorbisBitArray);
        d(vorbisBitArray);
        a(i, vorbisBitArray);
        Mode[] c = c(vorbisBitArray);
        if (vorbisBitArray.b()) {
            return c;
        }
        throw new ParserException("framing bit after modes not set as expected");
    }

    private static void a(int i, VorbisBitArray vorbisBitArray) throws ParserException {
        int a2 = vorbisBitArray.a(6) + 1;
        for (int i2 = 0; i2 < a2; i2++) {
            int a3 = vorbisBitArray.a(16);
            if (a3 != 0) {
                Log.b("VorbisUtil", "mapping type other than 0 not supported: " + a3);
            } else {
                int a4 = vorbisBitArray.b() ? vorbisBitArray.a(4) + 1 : 1;
                if (vorbisBitArray.b()) {
                    int a5 = vorbisBitArray.a(8) + 1;
                    for (int i3 = 0; i3 < a5; i3++) {
                        int i4 = i - 1;
                        vorbisBitArray.b(a(i4));
                        vorbisBitArray.b(a(i4));
                    }
                }
                if (vorbisBitArray.a(2) == 0) {
                    if (a4 > 1) {
                        for (int i5 = 0; i5 < i; i5++) {
                            vorbisBitArray.b(4);
                        }
                    }
                    for (int i6 = 0; i6 < a4; i6++) {
                        vorbisBitArray.b(8);
                        vorbisBitArray.b(8);
                        vorbisBitArray.b(8);
                    }
                } else {
                    throw new ParserException("to reserved bits must be zero after mapping coupling steps");
                }
            }
        }
    }

    private static CodeBook a(VorbisBitArray vorbisBitArray) throws ParserException {
        if (vorbisBitArray.a(24) == 5653314) {
            int a2 = vorbisBitArray.a(16);
            int a3 = vorbisBitArray.a(24);
            long[] jArr = new long[a3];
            boolean b = vorbisBitArray.b();
            long j = 0;
            if (!b) {
                boolean b2 = vorbisBitArray.b();
                for (int i = 0; i < jArr.length; i++) {
                    if (!b2) {
                        jArr[i] = (long) (vorbisBitArray.a(5) + 1);
                    } else if (vorbisBitArray.b()) {
                        jArr[i] = (long) (vorbisBitArray.a(5) + 1);
                    } else {
                        jArr[i] = 0;
                    }
                }
            } else {
                int a4 = vorbisBitArray.a(5) + 1;
                int i2 = 0;
                while (i2 < jArr.length) {
                    int a5 = vorbisBitArray.a(a(a3 - i2));
                    int i3 = i2;
                    for (int i4 = 0; i4 < a5 && i3 < jArr.length; i4++) {
                        jArr[i3] = (long) a4;
                        i3++;
                    }
                    a4++;
                    i2 = i3;
                }
            }
            int a6 = vorbisBitArray.a(4);
            if (a6 <= 2) {
                if (a6 == 1 || a6 == 2) {
                    vorbisBitArray.b(32);
                    vorbisBitArray.b(32);
                    int a7 = vorbisBitArray.a(4) + 1;
                    vorbisBitArray.b(1);
                    if (a6 != 1) {
                        j = ((long) a3) * ((long) a2);
                    } else if (a2 != 0) {
                        j = a((long) a3, (long) a2);
                    }
                    vorbisBitArray.b((int) (j * ((long) a7)));
                }
                return new CodeBook(a2, a3, jArr, a6, b);
            }
            throw new ParserException("lookup type greater than 2 not decodable: " + a6);
        }
        throw new ParserException("expected code book to start with [0x56, 0x43, 0x42] at " + vorbisBitArray.a());
    }

    private static long a(long j, long j2) {
        return (long) Math.floor(Math.pow((double) j, 1.0d / ((double) j2)));
    }
}
