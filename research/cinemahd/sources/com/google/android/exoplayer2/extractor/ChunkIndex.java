package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;

public final class ChunkIndex implements SeekMap {

    /* renamed from: a  reason: collision with root package name */
    public final int f3247a;
    public final int[] b;
    public final long[] c;
    public final long[] d;
    public final long[] e;
    private final long f;

    public ChunkIndex(int[] iArr, long[] jArr, long[] jArr2, long[] jArr3) {
        this.b = iArr;
        this.c = jArr;
        this.d = jArr2;
        this.e = jArr3;
        this.f3247a = iArr.length;
        int i = this.f3247a;
        if (i > 0) {
            this.f = jArr2[i - 1] + jArr3[i - 1];
        } else {
            this.f = 0;
        }
    }

    public SeekMap.SeekPoints b(long j) {
        int c2 = c(j);
        SeekPoint seekPoint = new SeekPoint(this.e[c2], this.c[c2]);
        if (seekPoint.f3257a >= j || c2 == this.f3247a - 1) {
            return new SeekMap.SeekPoints(seekPoint);
        }
        int i = c2 + 1;
        return new SeekMap.SeekPoints(seekPoint, new SeekPoint(this.e[i], this.c[i]));
    }

    public boolean b() {
        return true;
    }

    public int c(long j) {
        return Util.b(this.e, j, true, true);
    }

    public long getDurationUs() {
        return this.f;
    }

    public String toString() {
        return "ChunkIndex(length=" + this.f3247a + ", sizes=" + Arrays.toString(this.b) + ", offsets=" + Arrays.toString(this.c) + ", timeUs=" + Arrays.toString(this.e) + ", durationsUs=" + Arrays.toString(this.d) + ")";
    }
}
