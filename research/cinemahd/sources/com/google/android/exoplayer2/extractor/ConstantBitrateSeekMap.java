package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.util.Util;

public class ConstantBitrateSeekMap implements SeekMap {

    /* renamed from: a  reason: collision with root package name */
    private final long f3248a;
    private final long b;
    private final int c;
    private final long d;
    private final int e;
    private final long f;

    public ConstantBitrateSeekMap(long j, long j2, int i, int i2) {
        this.f3248a = j;
        this.b = j2;
        this.c = i2 == -1 ? 1 : i2;
        this.e = i;
        if (j == -1) {
            this.d = -1;
            this.f = -9223372036854775807L;
            return;
        }
        this.d = j - j2;
        this.f = a(j, j2, i);
    }

    private static long a(long j, long j2, int i) {
        return ((Math.max(0, j - j2) * 8) * 1000000) / ((long) i);
    }

    private long d(long j) {
        int i = this.c;
        return this.b + Util.b((((j * ((long) this.e)) / 8000000) / ((long) i)) * ((long) i), 0, this.d - ((long) i));
    }

    public boolean b() {
        return this.d != -1;
    }

    public long c(long j) {
        return a(j, this.b, this.e);
    }

    public long getDurationUs() {
        return this.f;
    }

    public SeekMap.SeekPoints b(long j) {
        if (this.d == -1) {
            return new SeekMap.SeekPoints(new SeekPoint(0, this.b));
        }
        long d2 = d(j);
        long c2 = c(d2);
        SeekPoint seekPoint = new SeekPoint(c2, d2);
        if (c2 < j) {
            int i = this.c;
            if (((long) i) + d2 < this.f3248a) {
                long j2 = d2 + ((long) i);
                return new SeekMap.SeekPoints(seekPoint, new SeekPoint(c(j2), j2));
            }
        }
        return new SeekMap.SeekPoints(seekPoint);
    }
}
