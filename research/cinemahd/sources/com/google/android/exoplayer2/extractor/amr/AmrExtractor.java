package com.google.android.exoplayer2.extractor.amr;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ConstantBitrateSeekMap;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public final class AmrExtractor implements Extractor {
    private static final int[] p = {13, 14, 16, 18, 20, 21, 27, 32, 6, 7, 6, 6, 1, 1, 1, 1};
    private static final int[] q = {18, 24, 33, 37, 41, 47, 51, 59, 61, 6, 1, 1, 1, 1, 1, 1};
    private static final byte[] r = Util.d("#!AMR\n");
    private static final byte[] s = Util.d("#!AMR-WB\n");
    private static final int t = q[8];

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f3259a;
    private final int b;
    private boolean c;
    private long d;
    private int e;
    private int f;
    private boolean g;
    private long h;
    private int i;
    private int j;
    private long k;
    private ExtractorOutput l;
    private TrackOutput m;
    private SeekMap n;
    private boolean o;

    static {
        a aVar = a.f3260a;
    }

    public AmrExtractor() {
        this(0);
    }

    static /* synthetic */ Extractor[] a() {
        return new Extractor[]{new AmrExtractor()};
    }

    private void b() {
        if (!this.o) {
            this.o = true;
            this.m.a(Format.a((String) null, this.c ? "audio/amr-wb" : "audio/3gpp", (String) null, -1, t, 1, this.c ? 16000 : 8000, -1, (List<byte[]>) null, (DrmInitData) null, 0, (String) null));
        }
    }

    private boolean c(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (a(extractorInput, r)) {
            this.c = false;
            extractorInput.c(r.length);
            return true;
        } else if (!a(extractorInput, s)) {
            return false;
        } else {
            this.c = true;
            extractorInput.c(s.length);
            return true;
        }
    }

    private int d(ExtractorInput extractorInput) throws IOException, InterruptedException {
        if (this.f == 0) {
            try {
                this.e = b(extractorInput);
                this.f = this.e;
                if (this.i == -1) {
                    this.h = extractorInput.getPosition();
                    this.i = this.e;
                }
                if (this.i == this.e) {
                    this.j++;
                }
            } catch (EOFException unused) {
                return -1;
            }
        }
        int a2 = this.m.a(extractorInput, this.f, true);
        if (a2 == -1) {
            return -1;
        }
        this.f -= a2;
        if (this.f > 0) {
            return 0;
        }
        this.m.a(this.k + this.d, 1, this.e, 0, (TrackOutput.CryptoData) null);
        this.d += 20000;
        return 0;
    }

    public void release() {
    }

    public AmrExtractor(int i2) {
        this.b = i2;
        this.f3259a = new byte[1];
        this.i = -1;
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        return c(extractorInput);
    }

    public void a(ExtractorOutput extractorOutput) {
        this.l = extractorOutput;
        this.m = extractorOutput.a(0, 1);
        extractorOutput.a();
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        if (extractorInput.getPosition() != 0 || c(extractorInput)) {
            b();
            int d2 = d(extractorInput);
            a(extractorInput.getLength(), d2);
            return d2;
        }
        throw new ParserException("Could not find AMR header.");
    }

    private boolean c(int i2) {
        return i2 >= 0 && i2 <= 15 && (d(i2) || b(i2));
    }

    private int b(ExtractorInput extractorInput) throws IOException, InterruptedException {
        extractorInput.a();
        extractorInput.a(this.f3259a, 0, 1);
        byte b2 = this.f3259a[0];
        if ((b2 & 131) <= 0) {
            return a((b2 >> 3) & 15);
        }
        throw new ParserException("Invalid padding bits for frame header " + b2);
    }

    public void a(long j2, long j3) {
        this.d = 0;
        this.e = 0;
        this.f = 0;
        if (j2 != 0) {
            SeekMap seekMap = this.n;
            if (seekMap instanceof ConstantBitrateSeekMap) {
                this.k = ((ConstantBitrateSeekMap) seekMap).c(j2);
                return;
            }
        }
        this.k = 0;
    }

    private boolean b(int i2) {
        return !this.c && (i2 < 12 || i2 > 14);
    }

    private boolean d(int i2) {
        return this.c && (i2 < 10 || i2 > 13);
    }

    private boolean a(ExtractorInput extractorInput, byte[] bArr) throws IOException, InterruptedException {
        extractorInput.a();
        byte[] bArr2 = new byte[bArr.length];
        extractorInput.a(bArr2, 0, bArr.length);
        return Arrays.equals(bArr2, bArr);
    }

    private int a(int i2) throws ParserException {
        if (c(i2)) {
            return this.c ? q[i2] : p[i2];
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Illegal AMR ");
        sb.append(this.c ? "WB" : "NB");
        sb.append(" frame type ");
        sb.append(i2);
        throw new ParserException(sb.toString());
    }

    private void a(long j2, int i2) {
        int i3;
        if (!this.g) {
            if ((this.b & 1) == 0 || j2 == -1 || !((i3 = this.i) == -1 || i3 == this.e)) {
                this.n = new SeekMap.Unseekable(-9223372036854775807L);
                this.l.a(this.n);
                this.g = true;
            } else if (this.j >= 20 || i2 == -1) {
                this.n = a(j2);
                this.l.a(this.n);
                this.g = true;
            }
        }
    }

    private SeekMap a(long j2) {
        return new ConstantBitrateSeekMap(j2, this.h, a(this.i, 20000), this.i);
    }

    private static int a(int i2, long j2) {
        return (int) ((((long) (i2 * 8)) * 1000000) / j2);
    }
}
