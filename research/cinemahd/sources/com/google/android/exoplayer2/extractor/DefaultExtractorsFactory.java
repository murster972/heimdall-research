package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.extractor.amr.AmrExtractor;
import com.google.android.exoplayer2.extractor.flv.FlvExtractor;
import com.google.android.exoplayer2.extractor.mkv.MatroskaExtractor;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor;
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor;
import com.google.android.exoplayer2.extractor.ogg.OggExtractor;
import com.google.android.exoplayer2.extractor.ts.Ac3Extractor;
import com.google.android.exoplayer2.extractor.ts.AdtsExtractor;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.exoplayer2.extractor.wav.WavExtractor;
import java.lang.reflect.Constructor;

public final class DefaultExtractorsFactory implements ExtractorsFactory {
    private static final Constructor<? extends Extractor> j;

    /* renamed from: a  reason: collision with root package name */
    private boolean f3250a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h = 1;
    private int i;

    static {
        Constructor<? extends U> constructor;
        try {
            constructor = Class.forName("com.google.android.exoplayer2.ext.flac.FlacExtractor").asSubclass(Extractor.class).getConstructor(new Class[0]);
        } catch (ClassNotFoundException unused) {
            constructor = null;
        } catch (Exception e2) {
            throw new RuntimeException("Error instantiating FLAC extension", e2);
        }
        j = constructor;
    }

    public synchronized Extractor[] a() {
        Extractor[] extractorArr;
        extractorArr = new Extractor[(j == null ? 12 : 13)];
        extractorArr[0] = new MatroskaExtractor(this.d);
        int i2 = 1;
        extractorArr[1] = new FragmentedMp4Extractor(this.f);
        extractorArr[2] = new Mp4Extractor(this.e);
        extractorArr[3] = new Mp3Extractor(this.g | (this.f3250a ? 1 : 0));
        extractorArr[4] = new AdtsExtractor(0, this.b | (this.f3250a ? 1 : 0));
        extractorArr[5] = new Ac3Extractor();
        extractorArr[6] = new TsExtractor(this.h, this.i);
        extractorArr[7] = new FlvExtractor();
        extractorArr[8] = new OggExtractor();
        extractorArr[9] = new PsExtractor();
        extractorArr[10] = new WavExtractor();
        int i3 = this.c;
        if (!this.f3250a) {
            i2 = 0;
        }
        extractorArr[11] = new AmrExtractor(i2 | i3);
        if (j != null) {
            try {
                extractorArr[12] = (Extractor) j.newInstance(new Object[0]);
            } catch (Exception e2) {
                throw new IllegalStateException("Unexpected error creating FLAC extractor", e2);
            }
        }
        return extractorArr;
    }
}
