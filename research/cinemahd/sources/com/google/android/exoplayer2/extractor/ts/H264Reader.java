package com.google.android.exoplayer2.extractor.ts;

import android.util.SparseArray;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.ParsableNalUnitBitArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class H264Reader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final SeiReader f3325a;
    private final boolean b;
    private final boolean c;
    private final NalUnitTargetBuffer d = new NalUnitTargetBuffer(7, 128);
    private final NalUnitTargetBuffer e = new NalUnitTargetBuffer(8, 128);
    private final NalUnitTargetBuffer f = new NalUnitTargetBuffer(6, 128);
    private long g;
    private final boolean[] h = new boolean[3];
    private String i;
    private TrackOutput j;
    private SampleReader k;
    private boolean l;
    private long m;
    private final ParsableByteArray n = new ParsableByteArray();

    private static final class SampleReader {

        /* renamed from: a  reason: collision with root package name */
        private final TrackOutput f3326a;
        private final boolean b;
        private final boolean c;
        private final SparseArray<NalUnitUtil.SpsData> d = new SparseArray<>();
        private final SparseArray<NalUnitUtil.PpsData> e = new SparseArray<>();
        private final ParsableNalUnitBitArray f = new ParsableNalUnitBitArray(this.g, 0, 0);
        private byte[] g = new byte[128];
        private int h;
        private int i;
        private long j;
        private boolean k;
        private long l;
        private SliceHeaderData m = new SliceHeaderData();
        private SliceHeaderData n = new SliceHeaderData();
        private boolean o;
        private long p;
        private long q;
        private boolean r;

        private static final class SliceHeaderData {

            /* renamed from: a  reason: collision with root package name */
            private boolean f3327a;
            private boolean b;
            private NalUnitUtil.SpsData c;
            private int d;
            private int e;
            private int f;
            private int g;
            private boolean h;
            private boolean i;
            private boolean j;
            private boolean k;
            private int l;
            private int m;
            private int n;
            private int o;
            private int p;

            private SliceHeaderData() {
            }

            /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
                r0 = r2.e;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public boolean b() {
                /*
                    r2 = this;
                    boolean r0 = r2.b
                    if (r0 == 0) goto L_0x000e
                    int r0 = r2.e
                    r1 = 7
                    if (r0 == r1) goto L_0x000c
                    r1 = 2
                    if (r0 != r1) goto L_0x000e
                L_0x000c:
                    r0 = 1
                    goto L_0x000f
                L_0x000e:
                    r0 = 0
                L_0x000f:
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.ts.H264Reader.SampleReader.SliceHeaderData.b():boolean");
            }

            public void a() {
                this.b = false;
                this.f3327a = false;
            }

            public void a(int i2) {
                this.e = i2;
                this.b = true;
            }

            public void a(NalUnitUtil.SpsData spsData, int i2, int i3, int i4, int i5, boolean z, boolean z2, boolean z3, boolean z4, int i6, int i7, int i8, int i9, int i10) {
                this.c = spsData;
                this.d = i2;
                this.e = i3;
                this.f = i4;
                this.g = i5;
                this.h = z;
                this.i = z2;
                this.j = z3;
                this.k = z4;
                this.l = i6;
                this.m = i7;
                this.n = i8;
                this.o = i9;
                this.p = i10;
                this.f3327a = true;
                this.b = true;
            }

            /* access modifiers changed from: private */
            public boolean a(SliceHeaderData sliceHeaderData) {
                boolean z;
                boolean z2;
                if (this.f3327a) {
                    if (!sliceHeaderData.f3327a || this.f != sliceHeaderData.f || this.g != sliceHeaderData.g || this.h != sliceHeaderData.h) {
                        return true;
                    }
                    if (this.i && sliceHeaderData.i && this.j != sliceHeaderData.j) {
                        return true;
                    }
                    int i2 = this.d;
                    int i3 = sliceHeaderData.d;
                    if (i2 != i3 && (i2 == 0 || i3 == 0)) {
                        return true;
                    }
                    if (this.c.k == 0 && sliceHeaderData.c.k == 0 && (this.m != sliceHeaderData.m || this.n != sliceHeaderData.n)) {
                        return true;
                    }
                    if ((this.c.k != 1 || sliceHeaderData.c.k != 1 || (this.o == sliceHeaderData.o && this.p == sliceHeaderData.p)) && (z = this.k) == (z2 = sliceHeaderData.k)) {
                        return z && z2 && this.l != sliceHeaderData.l;
                    }
                    return true;
                }
            }
        }

        public SampleReader(TrackOutput trackOutput, boolean z, boolean z2) {
            this.f3326a = trackOutput;
            this.b = z;
            this.c = z2;
            b();
        }

        public boolean a() {
            return this.c;
        }

        public void b() {
            this.k = false;
            this.o = false;
            this.n.a();
        }

        public void a(NalUnitUtil.SpsData spsData) {
            this.d.append(spsData.d, spsData);
        }

        public void a(NalUnitUtil.PpsData ppsData) {
            this.e.append(ppsData.f3638a, ppsData);
        }

        public void a(long j2, int i2, long j3) {
            this.i = i2;
            this.l = j3;
            this.j = j2;
            if (!this.b || this.i != 1) {
                if (this.c) {
                    int i3 = this.i;
                    if (!(i3 == 5 || i3 == 1 || i3 == 2)) {
                        return;
                    }
                } else {
                    return;
                }
            }
            SliceHeaderData sliceHeaderData = this.m;
            this.m = this.n;
            this.n = sliceHeaderData;
            this.n.a();
            this.h = 0;
            this.k = true;
        }

        /* JADX WARNING: Removed duplicated region for block: B:51:0x0101  */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x0104  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0108  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x011a  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x0120  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0150  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(byte[] r24, int r25, int r26) {
            /*
                r23 = this;
                r0 = r23
                r1 = r25
                boolean r2 = r0.k
                if (r2 != 0) goto L_0x0009
                return
            L_0x0009:
                int r2 = r26 - r1
                byte[] r3 = r0.g
                int r4 = r3.length
                int r5 = r0.h
                int r6 = r5 + r2
                r7 = 2
                if (r4 >= r6) goto L_0x001e
                int r5 = r5 + r2
                int r5 = r5 * 2
                byte[] r3 = java.util.Arrays.copyOf(r3, r5)
                r0.g = r3
            L_0x001e:
                byte[] r3 = r0.g
                int r4 = r0.h
                r5 = r24
                java.lang.System.arraycopy(r5, r1, r3, r4, r2)
                int r1 = r0.h
                int r1 = r1 + r2
                r0.h = r1
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                byte[] r2 = r0.g
                int r3 = r0.h
                r4 = 0
                r1.a(r2, r4, r3)
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                r2 = 8
                boolean r1 = r1.a(r2)
                if (r1 != 0) goto L_0x0041
                return
            L_0x0041:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                r1.e()
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                int r10 = r1.b(r7)
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                r2 = 5
                r1.c(r2)
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                boolean r1 = r1.a()
                if (r1 != 0) goto L_0x005b
                return
            L_0x005b:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                r1.d()
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                boolean r1 = r1.a()
                if (r1 != 0) goto L_0x0069
                return
            L_0x0069:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                int r11 = r1.d()
                boolean r1 = r0.c
                if (r1 != 0) goto L_0x007b
                r0.k = r4
                com.google.android.exoplayer2.extractor.ts.H264Reader$SampleReader$SliceHeaderData r1 = r0.n
                r1.a((int) r11)
                return
            L_0x007b:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                boolean r1 = r1.a()
                if (r1 != 0) goto L_0x0084
                return
            L_0x0084:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                int r13 = r1.d()
                android.util.SparseArray<com.google.android.exoplayer2.util.NalUnitUtil$PpsData> r1 = r0.e
                int r1 = r1.indexOfKey(r13)
                if (r1 >= 0) goto L_0x0095
                r0.k = r4
                return
            L_0x0095:
                android.util.SparseArray<com.google.android.exoplayer2.util.NalUnitUtil$PpsData> r1 = r0.e
                java.lang.Object r1 = r1.get(r13)
                com.google.android.exoplayer2.util.NalUnitUtil$PpsData r1 = (com.google.android.exoplayer2.util.NalUnitUtil.PpsData) r1
                android.util.SparseArray<com.google.android.exoplayer2.util.NalUnitUtil$SpsData> r3 = r0.d
                int r5 = r1.b
                java.lang.Object r3 = r3.get(r5)
                r9 = r3
                com.google.android.exoplayer2.util.NalUnitUtil$SpsData r9 = (com.google.android.exoplayer2.util.NalUnitUtil.SpsData) r9
                boolean r3 = r9.h
                if (r3 == 0) goto L_0x00ba
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r3 = r0.f
                boolean r3 = r3.a(r7)
                if (r3 != 0) goto L_0x00b5
                return
            L_0x00b5:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r3 = r0.f
                r3.c(r7)
            L_0x00ba:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r3 = r0.f
                int r5 = r9.j
                boolean r3 = r3.a(r5)
                if (r3 != 0) goto L_0x00c5
                return
            L_0x00c5:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r3 = r0.f
                int r5 = r9.j
                int r12 = r3.b(r5)
                boolean r3 = r9.i
                r5 = 1
                if (r3 != 0) goto L_0x00f9
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r3 = r0.f
                boolean r3 = r3.a(r5)
                if (r3 != 0) goto L_0x00db
                return
            L_0x00db:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r3 = r0.f
                boolean r3 = r3.b()
                if (r3 == 0) goto L_0x00f7
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r6 = r0.f
                boolean r6 = r6.a(r5)
                if (r6 != 0) goto L_0x00ec
                return
            L_0x00ec:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r6 = r0.f
                boolean r6 = r6.b()
                r14 = r3
                r16 = r6
                r15 = 1
                goto L_0x00fd
            L_0x00f7:
                r14 = r3
                goto L_0x00fa
            L_0x00f9:
                r14 = 0
            L_0x00fa:
                r15 = 0
                r16 = 0
            L_0x00fd:
                int r3 = r0.i
                if (r3 != r2) goto L_0x0104
                r17 = 1
                goto L_0x0106
            L_0x0104:
                r17 = 0
            L_0x0106:
                if (r17 == 0) goto L_0x011a
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r2 = r0.f
                boolean r2 = r2.a()
                if (r2 != 0) goto L_0x0111
                return
            L_0x0111:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r2 = r0.f
                int r2 = r2.d()
                r18 = r2
                goto L_0x011c
            L_0x011a:
                r18 = 0
            L_0x011c:
                int r2 = r9.k
                if (r2 != 0) goto L_0x0150
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r2 = r0.f
                int r3 = r9.l
                boolean r2 = r2.a(r3)
                if (r2 != 0) goto L_0x012b
                return
            L_0x012b:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r2 = r0.f
                int r3 = r9.l
                int r2 = r2.b(r3)
                boolean r1 = r1.c
                if (r1 == 0) goto L_0x014d
                if (r14 != 0) goto L_0x014d
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                boolean r1 = r1.a()
                if (r1 != 0) goto L_0x0142
                return
            L_0x0142:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                int r1 = r1.c()
                r20 = r1
                r19 = r2
                goto L_0x018e
            L_0x014d:
                r19 = r2
                goto L_0x018c
            L_0x0150:
                if (r2 != r5) goto L_0x018a
                boolean r2 = r9.m
                if (r2 != 0) goto L_0x018a
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r2 = r0.f
                boolean r2 = r2.a()
                if (r2 != 0) goto L_0x015f
                return
            L_0x015f:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r2 = r0.f
                int r2 = r2.c()
                boolean r1 = r1.c
                if (r1 == 0) goto L_0x0183
                if (r14 != 0) goto L_0x0183
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                boolean r1 = r1.a()
                if (r1 != 0) goto L_0x0174
                return
            L_0x0174:
                com.google.android.exoplayer2.util.ParsableNalUnitBitArray r1 = r0.f
                int r1 = r1.c()
                r22 = r1
                r21 = r2
                r19 = 0
                r20 = 0
                goto L_0x0192
            L_0x0183:
                r21 = r2
                r19 = 0
                r20 = 0
                goto L_0x0190
            L_0x018a:
                r19 = 0
            L_0x018c:
                r20 = 0
            L_0x018e:
                r21 = 0
            L_0x0190:
                r22 = 0
            L_0x0192:
                com.google.android.exoplayer2.extractor.ts.H264Reader$SampleReader$SliceHeaderData r8 = r0.n
                r8.a(r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22)
                r0.k = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.ts.H264Reader.SampleReader.a(byte[], int, int):void");
        }

        public void a(long j2, int i2) {
            boolean z = false;
            if (this.i == 9 || (this.c && this.n.a(this.m))) {
                if (this.o) {
                    a(i2 + ((int) (j2 - this.j)));
                }
                this.p = this.j;
                this.q = this.l;
                this.r = false;
                this.o = true;
            }
            boolean z2 = this.r;
            int i3 = this.i;
            if (i3 == 5 || (this.b && i3 == 1 && this.n.b())) {
                z = true;
            }
            this.r = z2 | z;
        }

        private void a(int i2) {
            boolean z = this.r;
            int i3 = (int) (this.j - this.p);
            this.f3326a.a(this.q, z ? 1 : 0, i3, i2, (TrackOutput.CryptoData) null);
        }
    }

    public H264Reader(SeiReader seiReader, boolean z, boolean z2) {
        this.f3325a = seiReader;
        this.b = z;
        this.c = z2;
    }

    public void a() {
        NalUnitUtil.a(this.h);
        this.d.b();
        this.e.b();
        this.f.b();
        this.k.b();
        this.g = 0;
    }

    public void b() {
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.i = trackIdGenerator.b();
        this.j = extractorOutput.a(trackIdGenerator.c(), 2);
        this.k = new SampleReader(this.j, this.b, this.c);
        this.f3325a.a(extractorOutput, trackIdGenerator);
    }

    public void a(long j2, boolean z) {
        this.m = j2;
    }

    public void a(ParsableByteArray parsableByteArray) {
        int c2 = parsableByteArray.c();
        int d2 = parsableByteArray.d();
        byte[] bArr = parsableByteArray.f3641a;
        this.g += (long) parsableByteArray.a();
        this.j.a(parsableByteArray, parsableByteArray.a());
        while (true) {
            int a2 = NalUnitUtil.a(bArr, c2, d2, this.h);
            if (a2 == d2) {
                a(bArr, c2, d2);
                return;
            }
            int b2 = NalUnitUtil.b(bArr, a2);
            int i2 = a2 - c2;
            if (i2 > 0) {
                a(bArr, c2, a2);
            }
            int i3 = d2 - a2;
            long j2 = this.g - ((long) i3);
            a(j2, i3, i2 < 0 ? -i2 : 0, this.m);
            a(j2, b2, this.m);
            c2 = a2 + 3;
        }
    }

    private void a(long j2, int i2, long j3) {
        if (!this.l || this.k.a()) {
            this.d.b(i2);
            this.e.b(i2);
        }
        this.f.b(i2);
        this.k.a(j2, i2, j3);
    }

    private void a(byte[] bArr, int i2, int i3) {
        if (!this.l || this.k.a()) {
            this.d.a(bArr, i2, i3);
            this.e.a(bArr, i2, i3);
        }
        this.f.a(bArr, i2, i3);
        this.k.a(bArr, i2, i3);
    }

    private void a(long j2, int i2, int i3, long j3) {
        int i4 = i3;
        if (!this.l || this.k.a()) {
            this.d.a(i4);
            this.e.a(i4);
            if (!this.l) {
                if (this.d.a() && this.e.a()) {
                    ArrayList arrayList = new ArrayList();
                    NalUnitTargetBuffer nalUnitTargetBuffer = this.d;
                    arrayList.add(Arrays.copyOf(nalUnitTargetBuffer.d, nalUnitTargetBuffer.e));
                    NalUnitTargetBuffer nalUnitTargetBuffer2 = this.e;
                    arrayList.add(Arrays.copyOf(nalUnitTargetBuffer2.d, nalUnitTargetBuffer2.e));
                    NalUnitTargetBuffer nalUnitTargetBuffer3 = this.d;
                    NalUnitUtil.SpsData c2 = NalUnitUtil.c(nalUnitTargetBuffer3.d, 3, nalUnitTargetBuffer3.e);
                    NalUnitTargetBuffer nalUnitTargetBuffer4 = this.e;
                    NalUnitUtil.PpsData b2 = NalUnitUtil.b(nalUnitTargetBuffer4.d, 3, nalUnitTargetBuffer4.e);
                    this.j.a(Format.a(this.i, "video/avc", CodecSpecificDataUtil.b(c2.f3639a, c2.b, c2.c), -1, -1, c2.e, c2.f, -1.0f, (List<byte[]>) arrayList, -1, c2.g, (DrmInitData) null));
                    this.l = true;
                    this.k.a(c2);
                    this.k.a(b2);
                    this.d.b();
                    this.e.b();
                }
            } else if (this.d.a()) {
                NalUnitTargetBuffer nalUnitTargetBuffer5 = this.d;
                this.k.a(NalUnitUtil.c(nalUnitTargetBuffer5.d, 3, nalUnitTargetBuffer5.e));
                this.d.b();
            } else if (this.e.a()) {
                NalUnitTargetBuffer nalUnitTargetBuffer6 = this.e;
                this.k.a(NalUnitUtil.b(nalUnitTargetBuffer6.d, 3, nalUnitTargetBuffer6.e));
                this.e.b();
            }
        }
        if (this.f.a(i3)) {
            NalUnitTargetBuffer nalUnitTargetBuffer7 = this.f;
            this.n.a(this.f.d, NalUnitUtil.c(nalUnitTargetBuffer7.d, nalUnitTargetBuffer7.e));
            this.n.e(4);
            this.f3325a.a(j3, this.n);
        }
        this.k.a(j2, i2);
    }
}
