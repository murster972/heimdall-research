package com.google.android.exoplayer2.extractor.wav;

import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.SeekPoint;
import com.google.android.exoplayer2.util.Util;

final class WavHeader implements SeekMap {

    /* renamed from: a  reason: collision with root package name */
    private final int f3356a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private final int f;
    private long g;
    private long h;

    public WavHeader(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f3356a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
    }

    public void a(long j, long j2) {
        this.g = j;
        this.h = j2;
    }

    public SeekMap.SeekPoints b(long j) {
        int i = this.d;
        long b2 = Util.b((((((long) this.c) * j) / 1000000) / ((long) i)) * ((long) i), 0, this.h - ((long) i));
        long j2 = this.g + b2;
        long a2 = a(j2);
        SeekPoint seekPoint = new SeekPoint(a2, j2);
        if (a2 < j) {
            long j3 = this.h;
            int i2 = this.d;
            if (b2 != j3 - ((long) i2)) {
                long j4 = j2 + ((long) i2);
                return new SeekMap.SeekPoints(seekPoint, new SeekPoint(a(j4), j4));
            }
        }
        return new SeekMap.SeekPoints(seekPoint);
    }

    public boolean b() {
        return true;
    }

    public int c() {
        return this.b * this.e * this.f3356a;
    }

    public int d() {
        return this.d;
    }

    public long e() {
        if (i()) {
            return this.g + this.h;
        }
        return -1;
    }

    public int f() {
        return this.f;
    }

    public int g() {
        return this.f3356a;
    }

    public long getDurationUs() {
        return ((this.h / ((long) this.d)) * 1000000) / ((long) this.b);
    }

    public int h() {
        return this.b;
    }

    public boolean i() {
        return (this.g == 0 || this.h == 0) ? false : true;
    }

    public long a(long j) {
        return (Math.max(0, j - this.g) * 1000000) / ((long) this.c);
    }
}
