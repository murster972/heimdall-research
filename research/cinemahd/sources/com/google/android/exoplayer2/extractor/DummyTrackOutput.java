package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.EOFException;
import java.io.IOException;

public final class DummyTrackOutput implements TrackOutput {
    public int a(ExtractorInput extractorInput, int i, boolean z) throws IOException, InterruptedException {
        int b = extractorInput.b(i);
        if (b != -1) {
            return b;
        }
        if (z) {
            return -1;
        }
        throw new EOFException();
    }

    public void a(long j, int i, int i2, int i3, TrackOutput.CryptoData cryptoData) {
    }

    public void a(Format format) {
    }

    public void a(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.f(i);
    }
}
