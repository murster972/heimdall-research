package com.google.android.exoplayer2.extractor.mp4;

import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;

final class TrackSampleTable {

    /* renamed from: a  reason: collision with root package name */
    public final Track f3299a;
    public final int b;
    public final long[] c;
    public final int[] d;
    public final int e;
    public final long[] f;
    public final int[] g;
    public final long h;

    public TrackSampleTable(Track track, long[] jArr, int[] iArr, int i, long[] jArr2, int[] iArr2, long j) {
        boolean z = true;
        Assertions.a(iArr.length == jArr2.length);
        Assertions.a(jArr.length == jArr2.length);
        Assertions.a(iArr2.length != jArr2.length ? false : z);
        this.f3299a = track;
        this.c = jArr;
        this.d = iArr;
        this.e = i;
        this.f = jArr2;
        this.g = iArr2;
        this.h = j;
        this.b = jArr.length;
    }

    public int a(long j) {
        for (int b2 = Util.b(this.f, j, true, false); b2 >= 0; b2--) {
            if ((this.g[b2] & 1) != 0) {
                return b2;
            }
        }
        return -1;
    }

    public int b(long j) {
        for (int a2 = Util.a(this.f, j, true, false); a2 < this.f.length; a2++) {
            if ((this.g[a2] & 1) != 0) {
                return a2;
            }
        }
        return -1;
    }
}
