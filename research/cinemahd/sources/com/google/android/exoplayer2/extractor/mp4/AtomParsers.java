package com.google.android.exoplayer2.extractor.mp4;

import android.util.Pair;
import com.facebook.common.time.Clock;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.GaplessInfoHolder;
import com.google.android.exoplayer2.extractor.mp4.Atom;
import com.google.android.exoplayer2.extractor.mp4.FixedSampleSizeRechunker;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.AvcConfig;
import com.google.android.exoplayer2.video.ColorInfo;
import com.google.android.exoplayer2.video.HevcConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class AtomParsers {

    /* renamed from: a  reason: collision with root package name */
    private static final int f3280a = Util.c("vide");
    private static final int b = Util.c("soun");
    private static final int c = Util.c("text");
    private static final int d = Util.c("sbtl");
    private static final int e = Util.c("subt");
    private static final int f = Util.c("clcp");
    private static final int g = Util.c("meta");
    private static final byte[] h = Util.d("OpusHead");

    private static final class ChunkIterator {

        /* renamed from: a  reason: collision with root package name */
        public final int f3281a;
        public int b;
        public int c;
        public long d;
        private final boolean e;
        private final ParsableByteArray f;
        private final ParsableByteArray g;
        private int h;
        private int i;

        public ChunkIterator(ParsableByteArray parsableByteArray, ParsableByteArray parsableByteArray2, boolean z) {
            this.g = parsableByteArray;
            this.f = parsableByteArray2;
            this.e = z;
            parsableByteArray2.e(12);
            this.f3281a = parsableByteArray2.x();
            parsableByteArray.e(12);
            this.i = parsableByteArray.x();
            Assertions.b(parsableByteArray.h() != 1 ? false : true, "first_chunk must be 1");
            this.b = -1;
        }

        public boolean a() {
            long j;
            int i2 = this.b + 1;
            this.b = i2;
            if (i2 == this.f3281a) {
                return false;
            }
            if (this.e) {
                j = this.f.y();
            } else {
                j = this.f.v();
            }
            this.d = j;
            if (this.b == this.h) {
                this.c = this.g.x();
                this.g.f(4);
                int i3 = this.i - 1;
                this.i = i3;
                this.h = i3 > 0 ? this.g.x() - 1 : -1;
            }
            return true;
        }
    }

    private interface SampleSizeBox {
        boolean a();

        int b();

        int c();
    }

    private static final class StsdData {

        /* renamed from: a  reason: collision with root package name */
        public final TrackEncryptionBox[] f3282a;
        public Format b;
        public int c;
        public int d = 0;

        public StsdData(int i) {
            this.f3282a = new TrackEncryptionBox[i];
        }
    }

    static final class StszSampleSizeBox implements SampleSizeBox {

        /* renamed from: a  reason: collision with root package name */
        private final int f3283a = this.c.x();
        private final int b = this.c.x();
        private final ParsableByteArray c;

        public StszSampleSizeBox(Atom.LeafAtom leafAtom) {
            this.c = leafAtom.V0;
            this.c.e(12);
        }

        public boolean a() {
            return this.f3283a != 0;
        }

        public int b() {
            int i = this.f3283a;
            return i == 0 ? this.c.x() : i;
        }

        public int c() {
            return this.b;
        }
    }

    static final class Stz2SampleSizeBox implements SampleSizeBox {

        /* renamed from: a  reason: collision with root package name */
        private final ParsableByteArray f3284a;
        private final int b = this.f3284a.x();
        private final int c = (this.f3284a.x() & JfifUtil.MARKER_FIRST_BYTE);
        private int d;
        private int e;

        public Stz2SampleSizeBox(Atom.LeafAtom leafAtom) {
            this.f3284a = leafAtom.V0;
            this.f3284a.e(12);
        }

        public boolean a() {
            return false;
        }

        public int b() {
            int i = this.c;
            if (i == 8) {
                return this.f3284a.t();
            }
            if (i == 16) {
                return this.f3284a.z();
            }
            int i2 = this.d;
            this.d = i2 + 1;
            if (i2 % 2 != 0) {
                return this.e & 15;
            }
            this.e = this.f3284a.t();
            return (this.e & 240) >> 4;
        }

        public int c() {
            return this.b;
        }
    }

    private static final class TkhdData {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final int f3285a;
        /* access modifiers changed from: private */
        public final long b;
        /* access modifiers changed from: private */
        public final int c;

        public TkhdData(int i, long j, int i2) {
            this.f3285a = i;
            this.b = j;
            this.c = i2;
        }
    }

    private AtomParsers() {
    }

    public static Track a(Atom.ContainerAtom containerAtom, Atom.LeafAtom leafAtom, long j, DrmInitData drmInitData, boolean z, boolean z2) throws ParserException {
        long j2;
        Atom.LeafAtom leafAtom2;
        long[] jArr;
        long[] jArr2;
        Atom.ContainerAtom containerAtom2 = containerAtom;
        Atom.ContainerAtom d2 = containerAtom2.d(Atom.F);
        int b2 = b(d2.e(Atom.T).V0);
        if (b2 == -1) {
            return null;
        }
        TkhdData e2 = e(containerAtom2.e(Atom.P).V0);
        long j3 = -9223372036854775807L;
        if (j == -9223372036854775807L) {
            j2 = e2.b;
            leafAtom2 = leafAtom;
        } else {
            leafAtom2 = leafAtom;
            j2 = j;
        }
        long d3 = d(leafAtom2.V0);
        if (j2 != -9223372036854775807L) {
            j3 = Util.c(j2, 1000000, d3);
        }
        long j4 = j3;
        Atom.ContainerAtom d4 = d2.d(Atom.G).d(Atom.H);
        Pair<Long, String> c2 = c(d2.e(Atom.S).V0);
        StsdData a2 = a(d4.e(Atom.U).V0, e2.f3285a, e2.c, (String) c2.second, drmInitData, z2);
        if (!z) {
            Pair<long[], long[]> a3 = a(containerAtom2.d(Atom.Q));
            jArr = (long[]) a3.second;
            jArr2 = (long[]) a3.first;
        } else {
            jArr2 = null;
            jArr = null;
        }
        if (a2.b == null) {
            return null;
        }
        return new Track(e2.f3285a, b2, ((Long) c2.first).longValue(), d3, j4, a2.b, a2.d, a2.f3282a, a2.c, jArr2, jArr);
    }

    private static Metadata b(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.f(8);
        ArrayList arrayList = new ArrayList();
        while (parsableByteArray.c() < i) {
            Metadata.Entry b2 = MetadataUtil.b(parsableByteArray);
            if (b2 != null) {
                arrayList.add(b2);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata((List<? extends Metadata.Entry>) arrayList);
    }

    private static Metadata c(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.f(12);
        while (parsableByteArray.c() < i) {
            int c2 = parsableByteArray.c();
            int h2 = parsableByteArray.h();
            if (parsableByteArray.h() == Atom.C0) {
                parsableByteArray.e(c2);
                return b(parsableByteArray, c2 + h2);
            }
            parsableByteArray.f(h2 - 8);
        }
        return null;
    }

    private static long d(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.e(8);
        if (Atom.c(parsableByteArray.h()) != 0) {
            i = 16;
        }
        parsableByteArray.f(i);
        return parsableByteArray.v();
    }

    private static TkhdData e(ParsableByteArray parsableByteArray) {
        boolean z;
        int i = 8;
        parsableByteArray.e(8);
        int c2 = Atom.c(parsableByteArray.h());
        parsableByteArray.f(c2 == 0 ? 8 : 16);
        int h2 = parsableByteArray.h();
        parsableByteArray.f(4);
        int c3 = parsableByteArray.c();
        if (c2 == 0) {
            i = 4;
        }
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i3 >= i) {
                z = true;
                break;
            } else if (parsableByteArray.f3641a[c3 + i3] != -1) {
                z = false;
                break;
            } else {
                i3++;
            }
        }
        long j = -9223372036854775807L;
        if (z) {
            parsableByteArray.f(i);
        } else {
            long v = c2 == 0 ? parsableByteArray.v() : parsableByteArray.y();
            if (v != 0) {
                j = v;
            }
        }
        parsableByteArray.f(16);
        int h3 = parsableByteArray.h();
        int h4 = parsableByteArray.h();
        parsableByteArray.f(4);
        int h5 = parsableByteArray.h();
        int h6 = parsableByteArray.h();
        if (h3 == 0 && h4 == 65536 && h5 == -65536 && h6 == 0) {
            i2 = 90;
        } else if (h3 == 0 && h4 == -65536 && h5 == 65536 && h6 == 0) {
            i2 = RotationOptions.ROTATE_270;
        } else if (h3 == -65536 && h4 == 0 && h5 == 0 && h6 == -65536) {
            i2 = RotationOptions.ROTATE_180;
        }
        return new TkhdData(h2, j, i2);
    }

    private static float d(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.e(i + 8);
        return ((float) parsableByteArray.x()) / ((float) parsableByteArray.x());
    }

    private static int b(ParsableByteArray parsableByteArray) {
        parsableByteArray.e(16);
        int h2 = parsableByteArray.h();
        if (h2 == b) {
            return 1;
        }
        if (h2 == f3280a) {
            return 2;
        }
        if (h2 == c || h2 == d || h2 == e || h2 == f) {
            return 3;
        }
        return h2 == g ? 4 : -1;
    }

    private static Pair<Integer, TrackEncryptionBox> d(ParsableByteArray parsableByteArray, int i, int i2) {
        Pair<Integer, TrackEncryptionBox> b2;
        int c2 = parsableByteArray.c();
        while (c2 - i < i2) {
            parsableByteArray.e(c2);
            int h2 = parsableByteArray.h();
            Assertions.a(h2 > 0, "childAtomSize should be positive");
            if (parsableByteArray.h() == Atom.W && (b2 = b(parsableByteArray, c2, h2)) != null) {
                return b2;
            }
            c2 += h2;
        }
        return null;
    }

    private static Pair<Long, String> c(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.e(8);
        int c2 = Atom.c(parsableByteArray.h());
        parsableByteArray.f(c2 == 0 ? 8 : 16);
        long v = parsableByteArray.v();
        if (c2 == 0) {
            i = 4;
        }
        parsableByteArray.f(i);
        int z = parsableByteArray.z();
        return Pair.create(Long.valueOf(v), "" + ((char) (((z >> 10) & 31) + 96)) + ((char) (((z >> 5) & 31) + 96)) + ((char) ((z & 31) + 96)));
    }

    static Pair<Integer, TrackEncryptionBox> b(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        String str = null;
        Integer num = null;
        int i4 = -1;
        int i5 = 0;
        while (i3 - i < i2) {
            parsableByteArray.e(i3);
            int h2 = parsableByteArray.h();
            int h3 = parsableByteArray.h();
            if (h3 == Atom.c0) {
                num = Integer.valueOf(parsableByteArray.h());
            } else if (h3 == Atom.X) {
                parsableByteArray.f(4);
                str = parsableByteArray.b(4);
            } else if (h3 == Atom.Y) {
                i4 = i3;
                i5 = h2;
            }
            i3 += h2;
        }
        if (!"cenc".equals(str) && !"cbc1".equals(str) && !"cens".equals(str) && !"cbcs".equals(str)) {
            return null;
        }
        boolean z = true;
        Assertions.a(num != null, "frma atom is mandatory");
        Assertions.a(i4 != -1, "schi atom is mandatory");
        TrackEncryptionBox a2 = a(parsableByteArray, i4, i5, str);
        if (a2 == null) {
            z = false;
        }
        Assertions.a(z, "tenc atom is mandatory");
        return Pair.create(num, a2);
    }

    public static TrackSampleTable a(Track track, Atom.ContainerAtom containerAtom, GaplessInfoHolder gaplessInfoHolder) throws ParserException {
        SampleSizeBox sampleSizeBox;
        boolean z;
        int i;
        int i2;
        int i3;
        long j;
        int[] iArr;
        long[] jArr;
        int i4;
        int[] iArr2;
        long[] jArr2;
        int i5;
        int[] iArr3;
        long[] jArr3;
        int[] iArr4;
        int[] iArr5;
        int i6;
        boolean z2;
        int i7;
        int i8;
        boolean z3;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        Track track2 = track;
        Atom.ContainerAtom containerAtom2 = containerAtom;
        GaplessInfoHolder gaplessInfoHolder2 = gaplessInfoHolder;
        Atom.LeafAtom e2 = containerAtom2.e(Atom.q0);
        if (e2 != null) {
            sampleSizeBox = new StszSampleSizeBox(e2);
        } else {
            Atom.LeafAtom e3 = containerAtom2.e(Atom.r0);
            if (e3 != null) {
                sampleSizeBox = new Stz2SampleSizeBox(e3);
            } else {
                throw new ParserException("Track has no sample table size information");
            }
        }
        int c2 = sampleSizeBox.c();
        if (c2 == 0) {
            return new TrackSampleTable(track, new long[0], new int[0], 0, new long[0], new int[0], -9223372036854775807L);
        }
        Atom.LeafAtom e4 = containerAtom2.e(Atom.s0);
        if (e4 == null) {
            e4 = containerAtom2.e(Atom.t0);
            z = true;
        } else {
            z = false;
        }
        ParsableByteArray parsableByteArray = e4.V0;
        ParsableByteArray parsableByteArray2 = containerAtom2.e(Atom.p0).V0;
        ParsableByteArray parsableByteArray3 = containerAtom2.e(Atom.m0).V0;
        Atom.LeafAtom e5 = containerAtom2.e(Atom.n0);
        ParsableByteArray parsableByteArray4 = e5 != null ? e5.V0 : null;
        Atom.LeafAtom e6 = containerAtom2.e(Atom.o0);
        ParsableByteArray parsableByteArray5 = e6 != null ? e6.V0 : null;
        ChunkIterator chunkIterator = new ChunkIterator(parsableByteArray2, parsableByteArray, z);
        parsableByteArray3.e(12);
        int x = parsableByteArray3.x() - 1;
        int x2 = parsableByteArray3.x();
        int x3 = parsableByteArray3.x();
        if (parsableByteArray5 != null) {
            parsableByteArray5.e(12);
            i = parsableByteArray5.x();
        } else {
            i = 0;
        }
        int i14 = -1;
        if (parsableByteArray4 != null) {
            parsableByteArray4.e(12);
            i2 = parsableByteArray4.x();
            if (i2 > 0) {
                i14 = parsableByteArray4.x() - 1;
            } else {
                parsableByteArray4 = null;
            }
        } else {
            i2 = 0;
        }
        if (!(sampleSizeBox.a() && "audio/raw".equals(track2.f.g) && x == 0 && i == 0 && i2 == 0)) {
            long[] jArr4 = new long[c2];
            int[] iArr6 = new int[c2];
            long[] jArr5 = new long[c2];
            int i15 = i2;
            iArr = new int[c2];
            int i16 = x;
            ParsableByteArray parsableByteArray6 = parsableByteArray3;
            int i17 = x3;
            long j2 = 0;
            long j3 = 0;
            int i18 = 0;
            int i19 = 0;
            int i20 = 0;
            int i21 = 0;
            int i22 = i15;
            int i23 = i;
            int i24 = x2;
            int i25 = i14;
            int i26 = 0;
            while (true) {
                if (i19 >= c2) {
                    i3 = c2;
                    i7 = i22;
                    i8 = i24;
                    break;
                }
                long j4 = j3;
                boolean z4 = true;
                while (i26 == 0) {
                    z4 = chunkIterator.a();
                    if (!z4) {
                        break;
                    }
                    int i27 = i22;
                    long j5 = chunkIterator.d;
                    i26 = chunkIterator.c;
                    j4 = j5;
                    i22 = i27;
                    i24 = i24;
                    c2 = c2;
                }
                int i28 = c2;
                i7 = i22;
                i8 = i24;
                if (!z4) {
                    Log.d("AtomParsers", "Unexpected end of chunk data");
                    jArr4 = Arrays.copyOf(jArr4, i19);
                    iArr6 = Arrays.copyOf(iArr6, i19);
                    jArr5 = Arrays.copyOf(jArr5, i19);
                    iArr = Arrays.copyOf(iArr, i19);
                    i3 = i19;
                    break;
                }
                if (parsableByteArray5 != null) {
                    int i29 = i23;
                    while (i20 == 0 && i29 > 0) {
                        i20 = parsableByteArray5.x();
                        i21 = parsableByteArray5.h();
                        i29--;
                    }
                    i20--;
                    i11 = i29;
                } else {
                    i11 = i23;
                }
                int i30 = i21;
                jArr4[i19] = j4;
                iArr6[i19] = sampleSizeBox.b();
                if (iArr6[i19] > i18) {
                    i18 = iArr6[i19];
                }
                jArr5[i19] = j2 + ((long) i30);
                iArr[i19] = parsableByteArray4 == null ? 1 : 0;
                if (i19 == i25) {
                    iArr[i19] = 1;
                    int i31 = i7 - 1;
                    if (i31 > 0) {
                        i25 = parsableByteArray4.x() - 1;
                    }
                    i12 = i18;
                    i22 = i31;
                    i13 = i30;
                } else {
                    i12 = i18;
                    i13 = i30;
                    i22 = i7;
                }
                j2 += (long) i17;
                int i32 = i8 - 1;
                if (i32 == 0 && i16 > 0) {
                    i32 = parsableByteArray6.x();
                    i16--;
                    i17 = parsableByteArray6.h();
                }
                i26--;
                i19++;
                i21 = i13;
                i24 = i32;
                j3 = j4 + ((long) iArr6[i19]);
                i18 = i12;
                i23 = i11;
                c2 = i28;
            }
            int i33 = i26;
            j = j2 + ((long) i21);
            int i34 = i23;
            while (true) {
                if (i34 <= 0) {
                    z3 = true;
                    break;
                } else if (parsableByteArray5.x() != 0) {
                    z3 = false;
                    break;
                } else {
                    parsableByteArray5.h();
                    i34--;
                }
            }
            if (i7 == 0 && i8 == 0 && i33 == 0 && i16 == 0) {
                i10 = i20;
                if (i10 == 0 && z3) {
                    i9 = i18;
                    track2 = track;
                    jArr2 = jArr4;
                    jArr = jArr5;
                    i4 = i9;
                    iArr2 = iArr6;
                }
            } else {
                i10 = i20;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Inconsistent stbl box for track ");
            i9 = i18;
            track2 = track;
            sb.append(track2.f3296a);
            sb.append(": remainingSynchronizationSamples ");
            sb.append(i7);
            sb.append(", remainingSamplesAtTimestampDelta ");
            sb.append(i8);
            sb.append(", remainingSamplesInChunk ");
            sb.append(i33);
            sb.append(", remainingTimestampDeltaChanges ");
            sb.append(i16);
            sb.append(", remainingSamplesAtTimestampOffset ");
            sb.append(i10);
            sb.append(!z3 ? ", ctts invalid" : "");
            Log.d("AtomParsers", sb.toString());
            jArr2 = jArr4;
            jArr = jArr5;
            i4 = i9;
            iArr2 = iArr6;
        } else {
            i3 = c2;
            int i35 = chunkIterator.f3281a;
            long[] jArr6 = new long[i35];
            int[] iArr7 = new int[i35];
            while (chunkIterator.a()) {
                int i36 = chunkIterator.b;
                jArr6[i36] = chunkIterator.d;
                iArr7[i36] = chunkIterator.c;
            }
            Format format = track2.f;
            FixedSampleSizeRechunker.Results a2 = FixedSampleSizeRechunker.a(Util.b(format.v, format.t), jArr6, iArr7, (long) x3);
            jArr2 = a2.f3287a;
            iArr2 = a2.b;
            i4 = a2.c;
            jArr = a2.d;
            iArr = a2.e;
            j = a2.f;
        }
        int i37 = i3;
        long c3 = Util.c(j, 1000000, track2.c);
        if (track2.h == null || gaplessInfoHolder.a()) {
            long[] jArr7 = jArr2;
            Util.a(jArr, 1000000, track2.c);
            return new TrackSampleTable(track, jArr7, iArr2, i4, jArr, iArr, c3);
        }
        long[] jArr8 = track2.h;
        if (jArr8.length == 1 && track2.b == 1 && jArr.length >= 2) {
            long j6 = track2.i[0];
            long c4 = j6 + Util.c(jArr8[0], track2.c, track2.d);
            iArr3 = iArr2;
            i5 = i4;
            if (a(jArr, j, j6, c4)) {
                long j7 = j - c4;
                long c5 = Util.c(j6 - jArr[0], (long) track2.f.u, track2.c);
                long c6 = Util.c(j7, (long) track2.f.u, track2.c);
                if (!(c5 == 0 && c6 == 0) && c5 <= 2147483647L && c6 <= 2147483647L) {
                    int i38 = (int) c5;
                    GaplessInfoHolder gaplessInfoHolder3 = gaplessInfoHolder;
                    gaplessInfoHolder3.f3251a = i38;
                    gaplessInfoHolder3.b = (int) c6;
                    Util.a(jArr, 1000000, track2.c);
                    return new TrackSampleTable(track, jArr2, iArr3, i5, jArr, iArr, Util.c(track2.h[0], 1000000, track2.d));
                }
            }
        } else {
            iArr3 = iArr2;
            i5 = i4;
        }
        long[] jArr9 = track2.h;
        if (jArr9.length == 1 && jArr9[0] == 0) {
            long j8 = track2.i[0];
            for (int i39 = 0; i39 < jArr.length; i39++) {
                jArr[i39] = Util.c(jArr[i39] - j8, 1000000, track2.c);
            }
            return new TrackSampleTable(track, jArr2, iArr3, i5, jArr, iArr, Util.c(j - j8, 1000000, track2.c));
        }
        boolean z5 = track2.b == 1;
        long[] jArr10 = track2.h;
        int[] iArr8 = new int[jArr10.length];
        int[] iArr9 = new int[jArr10.length];
        int i40 = 0;
        boolean z6 = false;
        int i41 = 0;
        int i42 = 0;
        while (true) {
            long[] jArr11 = track2.h;
            if (i40 >= jArr11.length) {
                break;
            }
            long j9 = track2.i[i40];
            if (j9 != -1) {
                boolean z7 = z6;
                int i43 = i41;
                long c7 = Util.c(jArr11[i40], track2.c, track2.d);
                iArr8[i40] = Util.a(jArr, j9, true, true);
                iArr9[i40] = Util.a(jArr, j9 + c7, z5, false);
                while (iArr8[i40] < iArr9[i40] && (iArr[iArr8[i40]] & 1) == 0) {
                    iArr8[i40] = iArr8[i40] + 1;
                }
                i41 = i43 + (iArr9[i40] - iArr8[i40]);
                z2 = z7 | (i42 != iArr8[i40]);
                i6 = iArr9[i40];
            } else {
                int i44 = i41;
                i6 = i42;
                z2 = z6;
            }
            i40++;
            z6 = z2;
            i42 = i6;
        }
        boolean z8 = z6;
        int i45 = 0;
        boolean z9 = true;
        if (i41 == i37) {
            z9 = false;
        }
        boolean z10 = z8 | z9;
        long[] jArr12 = z10 ? new long[i41] : jArr2;
        int[] iArr10 = z10 ? new int[i41] : iArr3;
        int i46 = z10 ? 0 : i5;
        int[] iArr11 = z10 ? new int[i41] : iArr;
        long[] jArr13 = new long[i41];
        int i47 = i46;
        long j10 = 0;
        int i48 = 0;
        while (i45 < track2.h.length) {
            long j11 = track2.i[i45];
            int i49 = iArr8[i45];
            int[] iArr12 = iArr8;
            int i50 = iArr9[i45];
            if (z10) {
                iArr4 = iArr9;
                int i51 = i50 - i49;
                System.arraycopy(jArr2, i49, jArr12, i48, i51);
                jArr3 = jArr2;
                iArr5 = iArr3;
                System.arraycopy(iArr5, i49, iArr10, i48, i51);
                System.arraycopy(iArr, i49, iArr11, i48, i51);
            } else {
                jArr3 = jArr2;
                iArr4 = iArr9;
                iArr5 = iArr3;
            }
            int i52 = i47;
            while (i49 < i50) {
                int[] iArr13 = iArr;
                int i53 = i50;
                int[] iArr14 = iArr11;
                long j12 = j10;
                jArr13[i48] = Util.c(j10, 1000000, track2.d) + Util.c(jArr[i49] - j11, 1000000, track2.c);
                if (z10 && iArr10[i48] > i52) {
                    i52 = iArr5[i49];
                }
                i48++;
                i49++;
                i50 = i53;
                iArr = iArr13;
                j10 = j12;
                iArr11 = iArr14;
            }
            j10 += track2.h[i45];
            i45++;
            i47 = i52;
            iArr = iArr;
            iArr8 = iArr12;
            iArr9 = iArr4;
            iArr11 = iArr11;
            iArr3 = iArr5;
            jArr2 = jArr3;
        }
        return new TrackSampleTable(track, jArr12, iArr10, i47, jArr13, iArr11, Util.c(j10, 1000000, track2.d));
    }

    private static byte[] c(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.e(i3);
            int h2 = parsableByteArray.h();
            if (parsableByteArray.h() == Atom.J0) {
                return Arrays.copyOfRange(parsableByteArray.f3641a, i3, h2 + i3);
            }
            i3 += h2;
        }
        return null;
    }

    public static Metadata a(Atom.LeafAtom leafAtom, boolean z) {
        if (z) {
            return null;
        }
        ParsableByteArray parsableByteArray = leafAtom.V0;
        parsableByteArray.e(8);
        while (parsableByteArray.a() >= 8) {
            int c2 = parsableByteArray.c();
            int h2 = parsableByteArray.h();
            if (parsableByteArray.h() == Atom.B0) {
                parsableByteArray.e(c2);
                return c(parsableByteArray, c2 + h2);
            }
            parsableByteArray.f(h2 - 8);
        }
        return null;
    }

    private static StsdData a(ParsableByteArray parsableByteArray, int i, int i2, String str, DrmInitData drmInitData, boolean z) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        parsableByteArray2.e(12);
        int h2 = parsableByteArray.h();
        StsdData stsdData = new StsdData(h2);
        for (int i3 = 0; i3 < h2; i3++) {
            int c2 = parsableByteArray.c();
            int h3 = parsableByteArray.h();
            Assertions.a(h3 > 0, "childAtomSize should be positive");
            int h4 = parsableByteArray.h();
            if (h4 == Atom.c || h4 == Atom.d || h4 == Atom.a0 || h4 == Atom.l0 || h4 == Atom.e || h4 == Atom.f || h4 == Atom.g || h4 == Atom.K0 || h4 == Atom.L0) {
                a(parsableByteArray, h4, c2, h3, i, i2, drmInitData, stsdData, i3);
            } else if (h4 == Atom.j || h4 == Atom.b0 || h4 == Atom.o || h4 == Atom.q || h4 == Atom.s || h4 == Atom.v || h4 == Atom.t || h4 == Atom.u || h4 == Atom.y0 || h4 == Atom.z0 || h4 == Atom.m || h4 == Atom.n || h4 == Atom.k || h4 == Atom.O0 || h4 == Atom.P0 || h4 == Atom.Q0 || h4 == Atom.R0 || h4 == Atom.T0) {
                a(parsableByteArray, h4, c2, h3, i, str, z, drmInitData, stsdData, i3);
            } else if (h4 == Atom.k0 || h4 == Atom.u0 || h4 == Atom.v0 || h4 == Atom.w0 || h4 == Atom.x0) {
                a(parsableByteArray, h4, c2, h3, i, str, stsdData);
            } else if (h4 == Atom.N0) {
                stsdData.b = Format.a(Integer.toString(i), "application/x-camera-motion", (String) null, -1, (DrmInitData) null);
            }
            parsableByteArray2.e(c2 + h3);
        }
        return stsdData;
    }

    private static void a(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, String str, StsdData stsdData) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i5 = i;
        StsdData stsdData2 = stsdData;
        parsableByteArray2.e(i2 + 8 + 8);
        int i6 = Atom.k0;
        String str2 = "application/ttml+xml";
        List list = null;
        long j = Clock.MAX_TIME;
        if (i5 != i6) {
            if (i5 == Atom.u0) {
                int i7 = (i3 - 8) - 8;
                byte[] bArr = new byte[i7];
                parsableByteArray2.a(bArr, 0, i7);
                list = Collections.singletonList(bArr);
                str2 = "application/x-quicktime-tx3g";
            } else if (i5 == Atom.v0) {
                str2 = "application/x-mp4-vtt";
            } else if (i5 == Atom.w0) {
                j = 0;
            } else if (i5 == Atom.x0) {
                stsdData2.d = 1;
                str2 = "application/x-mp4-cea-608";
            } else {
                throw new IllegalStateException();
            }
        }
        stsdData2.b = Format.a(Integer.toString(i4), str2, (String) null, -1, 0, str, -1, (DrmInitData) null, j, list);
    }

    private static void a(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, int i5, DrmInitData drmInitData, StsdData stsdData, int i6) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i7 = i2;
        int i8 = i3;
        DrmInitData drmInitData2 = drmInitData;
        StsdData stsdData2 = stsdData;
        parsableByteArray2.e(i7 + 8 + 8);
        parsableByteArray2.f(16);
        int z = parsableByteArray.z();
        int z2 = parsableByteArray.z();
        parsableByteArray2.f(50);
        int c2 = parsableByteArray.c();
        String str = null;
        int i9 = i;
        if (i9 == Atom.a0) {
            Pair<Integer, TrackEncryptionBox> d2 = d(parsableByteArray2, i7, i8);
            if (d2 != null) {
                i9 = ((Integer) d2.first).intValue();
                if (drmInitData2 == null) {
                    drmInitData2 = null;
                } else {
                    drmInitData2 = drmInitData2.a(((TrackEncryptionBox) d2.second).b);
                }
                stsdData2.f3282a[i6] = (TrackEncryptionBox) d2.second;
            }
            parsableByteArray2.e(c2);
        }
        DrmInitData drmInitData3 = drmInitData2;
        List<byte[]> list = null;
        byte[] bArr = null;
        boolean z3 = false;
        float f2 = 1.0f;
        int i10 = -1;
        while (c2 - i7 < i8) {
            parsableByteArray2.e(c2);
            int c3 = parsableByteArray.c();
            int h2 = parsableByteArray.h();
            if (h2 == 0 && parsableByteArray.c() - i7 == i8) {
                break;
            }
            Assertions.a(h2 > 0, "childAtomSize should be positive");
            int h3 = parsableByteArray.h();
            if (h3 == Atom.I) {
                Assertions.b(str == null);
                parsableByteArray2.e(c3 + 8);
                AvcConfig b2 = AvcConfig.b(parsableByteArray);
                list = b2.f3656a;
                stsdData2.c = b2.b;
                if (!z3) {
                    f2 = b2.e;
                }
                str = "video/avc";
            } else if (h3 == Atom.J) {
                Assertions.b(str == null);
                parsableByteArray2.e(c3 + 8);
                HevcConfig a2 = HevcConfig.a(parsableByteArray);
                list = a2.f3660a;
                stsdData2.c = a2.b;
                str = "video/hevc";
            } else if (h3 == Atom.M0) {
                Assertions.b(str == null);
                str = i9 == Atom.K0 ? "video/x-vnd.on2.vp8" : "video/x-vnd.on2.vp9";
            } else if (h3 == Atom.h) {
                Assertions.b(str == null);
                str = "video/3gpp";
            } else if (h3 == Atom.K) {
                Assertions.b(str == null);
                Pair<String, byte[]> a3 = a(parsableByteArray2, c3);
                str = (String) a3.first;
                list = Collections.singletonList(a3.second);
            } else if (h3 == Atom.j0) {
                f2 = d(parsableByteArray2, c3);
                z3 = true;
            } else if (h3 == Atom.I0) {
                bArr = c(parsableByteArray2, c3, h2);
            } else if (h3 == Atom.H0) {
                int t = parsableByteArray.t();
                parsableByteArray2.f(3);
                if (t == 0) {
                    int t2 = parsableByteArray.t();
                    if (t2 == 0) {
                        i10 = 0;
                    } else if (t2 == 1) {
                        i10 = 1;
                    } else if (t2 == 2) {
                        i10 = 2;
                    } else if (t2 == 3) {
                        i10 = 3;
                    }
                }
            }
            c2 += h2;
        }
        if (str != null) {
            stsdData2.b = Format.a(Integer.toString(i4), str, (String) null, -1, -1, z, z2, -1.0f, list, i5, f2, bArr, i10, (ColorInfo) null, drmInitData3);
        }
    }

    private static Pair<long[], long[]> a(Atom.ContainerAtom containerAtom) {
        Atom.LeafAtom e2;
        if (containerAtom == null || (e2 = containerAtom.e(Atom.R)) == null) {
            return Pair.create((Object) null, (Object) null);
        }
        ParsableByteArray parsableByteArray = e2.V0;
        parsableByteArray.e(8);
        int c2 = Atom.c(parsableByteArray.h());
        int x = parsableByteArray.x();
        long[] jArr = new long[x];
        long[] jArr2 = new long[x];
        int i = 0;
        while (i < x) {
            jArr[i] = c2 == 1 ? parsableByteArray.y() : parsableByteArray.v();
            jArr2[i] = c2 == 1 ? parsableByteArray.p() : (long) parsableByteArray.h();
            if (parsableByteArray.r() == 1) {
                parsableByteArray.f(2);
                i++;
            } else {
                throw new IllegalArgumentException("Unsupported media rate.");
            }
        }
        return Pair.create(jArr, jArr2);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v7, resolved type: java.lang.String} */
    /*  JADX ERROR: JadxRuntimeException in pass: IfRegionVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Don't wrap MOVE or CONST insns: 0x0255: MOVE  (r7v3 java.lang.String) = (r25v0 java.lang.String)
        	at jadx.core.dex.instructions.args.InsnArg.wrapArg(InsnArg.java:164)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.assignInline(CodeShrinkVisitor.java:133)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.checkInline(CodeShrinkVisitor.java:118)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkBlock(CodeShrinkVisitor.java:65)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkMethod(CodeShrinkVisitor.java:43)
        	at jadx.core.dex.visitors.regions.TernaryMod.makeTernaryInsn(TernaryMod.java:122)
        	at jadx.core.dex.visitors.regions.TernaryMod.visitRegion(TernaryMod.java:34)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:73)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterativeStepInternal(DepthRegionTraversal.java:78)
        	at jadx.core.dex.visitors.regions.DepthRegionTraversal.traverseIterative(DepthRegionTraversal.java:27)
        	at jadx.core.dex.visitors.regions.IfRegionVisitor.visit(IfRegionVisitor.java:31)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    private static void a(com.google.android.exoplayer2.util.ParsableByteArray r28, int r29, int r30, int r31, int r32, java.lang.String r33, boolean r34, com.google.android.exoplayer2.drm.DrmInitData r35, com.google.android.exoplayer2.extractor.mp4.AtomParsers.StsdData r36, int r37) throws com.google.android.exoplayer2.ParserException {
        /*
            r0 = r28
            r1 = r30
            r2 = r31
            r14 = r33
            r3 = r35
            r15 = r36
            int r4 = r1 + 8
            r5 = 8
            int r4 = r4 + r5
            r0.e(r4)
            r4 = 6
            r13 = 0
            if (r34 == 0) goto L_0x0020
            int r5 = r28.z()
            r0.f(r4)
            goto L_0x0024
        L_0x0020:
            r0.f(r5)
            r5 = 0
        L_0x0024:
            r12 = 2
            r6 = 16
            r11 = 1
            if (r5 == 0) goto L_0x0048
            if (r5 != r11) goto L_0x002d
            goto L_0x0048
        L_0x002d:
            if (r5 != r12) goto L_0x0047
            r0.f(r6)
            double r4 = r28.g()
            long r4 = java.lang.Math.round(r4)
            int r5 = (int) r4
            int r4 = r28.x()
            r6 = 20
            r0.f(r6)
            r7 = r4
            r4 = r5
            goto L_0x0058
        L_0x0047:
            return
        L_0x0048:
            int r7 = r28.z()
            r0.f(r4)
            int r4 = r28.u()
            if (r5 != r11) goto L_0x0058
            r0.f(r6)
        L_0x0058:
            int r5 = r28.c()
            int r6 = com.google.android.exoplayer2.extractor.mp4.Atom.b0
            r16 = 0
            r8 = r29
            if (r8 != r6) goto L_0x008c
            android.util.Pair r6 = d(r0, r1, r2)
            if (r6 == 0) goto L_0x0089
            java.lang.Object r8 = r6.first
            java.lang.Integer r8 = (java.lang.Integer) r8
            int r8 = r8.intValue()
            if (r3 != 0) goto L_0x0077
            r3 = r16
            goto L_0x0081
        L_0x0077:
            java.lang.Object r9 = r6.second
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox r9 = (com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox) r9
            java.lang.String r9 = r9.b
            com.google.android.exoplayer2.drm.DrmInitData r3 = r3.a((java.lang.String) r9)
        L_0x0081:
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox[] r9 = r15.f3282a
            java.lang.Object r6 = r6.second
            com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox r6 = (com.google.android.exoplayer2.extractor.mp4.TrackEncryptionBox) r6
            r9[r37] = r6
        L_0x0089:
            r0.e(r5)
        L_0x008c:
            r10 = r3
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.o
            java.lang.String r9 = "audio/raw"
            if (r8 != r3) goto L_0x0097
            java.lang.String r3 = "audio/ac3"
            goto L_0x00ff
        L_0x0097:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.q
            if (r8 != r3) goto L_0x009f
            java.lang.String r3 = "audio/eac3"
            goto L_0x00ff
        L_0x009f:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.s
            if (r8 != r3) goto L_0x00a7
            java.lang.String r3 = "audio/vnd.dts"
            goto L_0x00ff
        L_0x00a7:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.t
            if (r8 == r3) goto L_0x00fd
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.u
            if (r8 != r3) goto L_0x00b0
            goto L_0x00fd
        L_0x00b0:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.v
            if (r8 != r3) goto L_0x00b7
            java.lang.String r3 = "audio/vnd.dts.hd;profile=lbr"
            goto L_0x00ff
        L_0x00b7:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.y0
            if (r8 != r3) goto L_0x00be
            java.lang.String r3 = "audio/3gpp"
            goto L_0x00ff
        L_0x00be:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.z0
            if (r8 != r3) goto L_0x00c5
            java.lang.String r3 = "audio/amr-wb"
            goto L_0x00ff
        L_0x00c5:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.m
            if (r8 == r3) goto L_0x00fb
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.n
            if (r8 != r3) goto L_0x00ce
            goto L_0x00fb
        L_0x00ce:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.k
            if (r8 != r3) goto L_0x00d5
            java.lang.String r3 = "audio/mpeg"
            goto L_0x00ff
        L_0x00d5:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.O0
            if (r8 != r3) goto L_0x00dc
            java.lang.String r3 = "audio/alac"
            goto L_0x00ff
        L_0x00dc:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.P0
            if (r8 != r3) goto L_0x00e3
            java.lang.String r3 = "audio/g711-alaw"
            goto L_0x00ff
        L_0x00e3:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.Q0
            if (r8 != r3) goto L_0x00ea
            java.lang.String r3 = "audio/g711-mlaw"
            goto L_0x00ff
        L_0x00ea:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.R0
            if (r8 != r3) goto L_0x00f1
            java.lang.String r3 = "audio/opus"
            goto L_0x00ff
        L_0x00f1:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.T0
            if (r8 != r3) goto L_0x00f8
            java.lang.String r3 = "audio/flac"
            goto L_0x00ff
        L_0x00f8:
            r3 = r16
            goto L_0x00ff
        L_0x00fb:
            r3 = r9
            goto L_0x00ff
        L_0x00fd:
            java.lang.String r3 = "audio/vnd.dts.hd"
        L_0x00ff:
            r18 = r4
            r8 = r5
            r17 = r7
            r19 = r16
            r7 = r3
        L_0x0107:
            int r3 = r8 - r1
            r4 = -1
            if (r3 >= r2) goto L_0x0249
            r0.e(r8)
            int r6 = r28.h()
            if (r6 <= 0) goto L_0x0117
            r3 = 1
            goto L_0x0118
        L_0x0117:
            r3 = 0
        L_0x0118:
            java.lang.String r5 = "childAtomSize should be positive"
            com.google.android.exoplayer2.util.Assertions.a(r3, r5)
            int r3 = r28.h()
            int r5 = com.google.android.exoplayer2.extractor.mp4.Atom.K
            if (r3 == r5) goto L_0x01f8
            if (r34 == 0) goto L_0x012d
            int r5 = com.google.android.exoplayer2.extractor.mp4.Atom.l
            if (r3 != r5) goto L_0x012d
            goto L_0x01f8
        L_0x012d:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.p
            if (r3 != r4) goto L_0x014f
            int r3 = r8 + 8
            r0.e(r3)
            java.lang.String r3 = java.lang.Integer.toString(r32)
            com.google.android.exoplayer2.Format r3 = com.google.android.exoplayer2.audio.Ac3Util.a(r0, r3, r14, r10)
            r15.b = r3
        L_0x0140:
            r5 = r6
            r25 = r7
            r6 = r8
            r27 = r9
            r20 = r10
            r1 = 0
            r21 = 1
            r22 = 2
            goto L_0x01f5
        L_0x014f:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.r
            if (r3 != r4) goto L_0x0163
            int r3 = r8 + 8
            r0.e(r3)
            java.lang.String r3 = java.lang.Integer.toString(r32)
            com.google.android.exoplayer2.Format r3 = com.google.android.exoplayer2.audio.Ac3Util.b(r0, r3, r14, r10)
            r15.b = r3
            goto L_0x0140
        L_0x0163:
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.w
            if (r3 != r4) goto L_0x019f
            java.lang.String r3 = java.lang.Integer.toString(r32)
            r5 = 0
            r20 = -1
            r21 = -1
            r22 = 0
            r23 = 0
            r4 = r7
            r24 = r6
            r6 = r20
            r25 = r7
            r7 = r21
            r26 = r8
            r8 = r17
            r27 = r9
            r9 = r18
            r20 = r10
            r10 = r22
            r21 = 1
            r11 = r20
            r22 = 2
            r12 = r23
            r1 = 0
            r13 = r33
            com.google.android.exoplayer2.Format r3 = com.google.android.exoplayer2.Format.a((java.lang.String) r3, (java.lang.String) r4, (java.lang.String) r5, (int) r6, (int) r7, (int) r8, (int) r9, (java.util.List<byte[]>) r10, (com.google.android.exoplayer2.drm.DrmInitData) r11, (int) r12, (java.lang.String) r13)
            r15.b = r3
            r5 = r24
            r6 = r26
            goto L_0x01f5
        L_0x019f:
            r24 = r6
            r25 = r7
            r26 = r8
            r27 = r9
            r20 = r10
            r1 = 0
            r21 = 1
            r22 = 2
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.O0
            if (r3 != r4) goto L_0x01c1
            r5 = r24
            byte[] r3 = new byte[r5]
            r6 = r26
            r0.e(r6)
            r0.a(r3, r1, r5)
            r19 = r3
            goto L_0x01f5
        L_0x01c1:
            r5 = r24
            r6 = r26
            int r4 = com.google.android.exoplayer2.extractor.mp4.Atom.S0
            if (r3 != r4) goto L_0x01e3
            int r3 = r5 + -8
            byte[] r4 = h
            int r7 = r4.length
            int r7 = r7 + r3
            byte[] r7 = new byte[r7]
            int r8 = r4.length
            java.lang.System.arraycopy(r4, r1, r7, r1, r8)
            int r8 = r6 + 8
            r0.e(r8)
            byte[] r4 = h
            int r4 = r4.length
            r0.a(r7, r4, r3)
            r19 = r7
            goto L_0x01f5
        L_0x01e3:
            int r3 = com.google.android.exoplayer2.extractor.mp4.Atom.U0
            if (r5 != r3) goto L_0x01f5
            int r3 = r5 + -12
            byte[] r4 = new byte[r3]
            int r8 = r6 + 12
            r0.e(r8)
            r0.a(r4, r1, r3)
            r19 = r4
        L_0x01f5:
            r7 = r25
            goto L_0x023c
        L_0x01f8:
            r5 = r6
            r25 = r7
            r6 = r8
            r27 = r9
            r20 = r10
            r1 = 0
            r21 = 1
            r22 = 2
            int r7 = com.google.android.exoplayer2.extractor.mp4.Atom.K
            if (r3 != r7) goto L_0x020b
            r8 = r6
            goto L_0x020f
        L_0x020b:
            int r8 = a((com.google.android.exoplayer2.util.ParsableByteArray) r0, (int) r6, (int) r5)
        L_0x020f:
            if (r8 == r4) goto L_0x01f5
            android.util.Pair r3 = a((com.google.android.exoplayer2.util.ParsableByteArray) r0, (int) r8)
            java.lang.Object r4 = r3.first
            r7 = r4
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r3 = r3.second
            r19 = r3
            byte[] r19 = (byte[]) r19
            java.lang.String r3 = "audio/mp4a-latm"
            boolean r3 = r3.equals(r7)
            if (r3 == 0) goto L_0x023c
            android.util.Pair r3 = com.google.android.exoplayer2.util.CodecSpecificDataUtil.a((byte[]) r19)
            java.lang.Object r4 = r3.first
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r18 = r4.intValue()
            java.lang.Object r3 = r3.second
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r17 = r3.intValue()
        L_0x023c:
            int r8 = r6 + r5
            r1 = r30
            r10 = r20
            r9 = r27
            r11 = 1
            r12 = 2
            r13 = 0
            goto L_0x0107
        L_0x0249:
            r25 = r7
            r27 = r9
            r20 = r10
            r22 = 2
            com.google.android.exoplayer2.Format r0 = r15.b
            if (r0 != 0) goto L_0x0287
            r7 = r25
            if (r7 == 0) goto L_0x0287
            r0 = r27
            boolean r0 = r0.equals(r7)
            if (r0 == 0) goto L_0x0262
            goto L_0x0264
        L_0x0262:
            r22 = -1
        L_0x0264:
            java.lang.String r0 = java.lang.Integer.toString(r32)
            r2 = 0
            r3 = -1
            r4 = -1
            if (r19 != 0) goto L_0x0270
            r8 = r16
            goto L_0x0275
        L_0x0270:
            java.util.List r1 = java.util.Collections.singletonList(r19)
            r8 = r1
        L_0x0275:
            r10 = 0
            r1 = r7
            r5 = r17
            r6 = r18
            r7 = r22
            r9 = r20
            r11 = r33
            com.google.android.exoplayer2.Format r0 = com.google.android.exoplayer2.Format.a((java.lang.String) r0, (java.lang.String) r1, (java.lang.String) r2, (int) r3, (int) r4, (int) r5, (int) r6, (int) r7, (java.util.List<byte[]>) r8, (com.google.android.exoplayer2.drm.DrmInitData) r9, (int) r10, (java.lang.String) r11)
            r15.b = r0
        L_0x0287:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.mp4.AtomParsers.a(com.google.android.exoplayer2.util.ParsableByteArray, int, int, int, int, java.lang.String, boolean, com.google.android.exoplayer2.drm.DrmInitData, com.google.android.exoplayer2.extractor.mp4.AtomParsers$StsdData, int):void");
    }

    private static int a(ParsableByteArray parsableByteArray, int i, int i2) {
        int c2 = parsableByteArray.c();
        while (c2 - i < i2) {
            parsableByteArray.e(c2);
            int h2 = parsableByteArray.h();
            Assertions.a(h2 > 0, "childAtomSize should be positive");
            if (parsableByteArray.h() == Atom.K) {
                return c2;
            }
            c2 += h2;
        }
        return -1;
    }

    private static Pair<String, byte[]> a(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.e(i + 8 + 4);
        parsableByteArray.f(1);
        a(parsableByteArray);
        parsableByteArray.f(2);
        int t = parsableByteArray.t();
        if ((t & 128) != 0) {
            parsableByteArray.f(2);
        }
        if ((t & 64) != 0) {
            parsableByteArray.f(parsableByteArray.z());
        }
        if ((t & 32) != 0) {
            parsableByteArray.f(2);
        }
        parsableByteArray.f(1);
        a(parsableByteArray);
        String a2 = MimeTypes.a(parsableByteArray.t());
        if ("audio/mpeg".equals(a2) || "audio/vnd.dts".equals(a2) || "audio/vnd.dts.hd".equals(a2)) {
            return Pair.create(a2, (Object) null);
        }
        parsableByteArray.f(12);
        parsableByteArray.f(1);
        int a3 = a(parsableByteArray);
        byte[] bArr = new byte[a3];
        parsableByteArray.a(bArr, 0, a3);
        return Pair.create(a2, bArr);
    }

    private static TrackEncryptionBox a(ParsableByteArray parsableByteArray, int i, int i2, String str) {
        int i3;
        int i4;
        int i5 = i + 8;
        while (true) {
            byte[] bArr = null;
            if (i5 - i >= i2) {
                return null;
            }
            parsableByteArray.e(i5);
            int h2 = parsableByteArray.h();
            if (parsableByteArray.h() == Atom.Z) {
                int c2 = Atom.c(parsableByteArray.h());
                parsableByteArray.f(1);
                if (c2 == 0) {
                    parsableByteArray.f(1);
                    i4 = 0;
                    i3 = 0;
                } else {
                    int t = parsableByteArray.t();
                    i3 = t & 15;
                    i4 = (t & 240) >> 4;
                }
                boolean z = parsableByteArray.t() == 1;
                int t2 = parsableByteArray.t();
                byte[] bArr2 = new byte[16];
                parsableByteArray.a(bArr2, 0, bArr2.length);
                if (z && t2 == 0) {
                    int t3 = parsableByteArray.t();
                    bArr = new byte[t3];
                    parsableByteArray.a(bArr, 0, t3);
                }
                return new TrackEncryptionBox(z, str, t2, bArr2, i4, i3, bArr);
            }
            i5 += h2;
        }
    }

    private static int a(ParsableByteArray parsableByteArray) {
        int t = parsableByteArray.t();
        int i = t & 127;
        while ((t & 128) == 128) {
            t = parsableByteArray.t();
            i = (i << 7) | (t & 127);
        }
        return i;
    }

    private static boolean a(long[] jArr, long j, long j2, long j3) {
        int length = jArr.length - 1;
        int a2 = Util.a(3, 0, length);
        int a3 = Util.a(jArr.length - 3, 0, length);
        if (jArr[0] > j2 || j2 >= jArr[a2] || jArr[a3] >= j3 || j3 > j) {
            return false;
        }
        return true;
    }
}
