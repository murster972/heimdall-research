package com.google.android.exoplayer2.extractor.ts;

import android.util.Pair;
import com.facebook.common.util.ByteConstants;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.Collections;
import java.util.List;

public final class LatmReader implements ElementaryStreamReader {

    /* renamed from: a  reason: collision with root package name */
    private final String f3331a;
    private final ParsableByteArray b = new ParsableByteArray((int) ByteConstants.KB);
    private final ParsableBitArray c = new ParsableBitArray(this.b.f3641a);
    private TrackOutput d;
    private Format e;
    private String f;
    private int g;
    private int h;
    private int i;
    private int j;
    private long k;
    private boolean l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private long q;
    private int r;
    private long s;
    private int t;

    public LatmReader(String str) {
        this.f3331a = str;
    }

    private void b(ParsableBitArray parsableBitArray) throws ParserException {
        if (!parsableBitArray.e()) {
            this.l = true;
            f(parsableBitArray);
        } else if (!this.l) {
            return;
        }
        if (this.m != 0) {
            throw new ParserException();
        } else if (this.n == 0) {
            a(parsableBitArray, e(parsableBitArray));
            if (this.p) {
                parsableBitArray.c((int) this.q);
            }
        } else {
            throw new ParserException();
        }
    }

    private int c(ParsableBitArray parsableBitArray) throws ParserException {
        int a2 = parsableBitArray.a();
        Pair<Integer, Integer> a3 = CodecSpecificDataUtil.a(parsableBitArray, true);
        this.r = ((Integer) a3.first).intValue();
        this.t = ((Integer) a3.second).intValue();
        return a2 - parsableBitArray.a();
    }

    private void d(ParsableBitArray parsableBitArray) {
        this.o = parsableBitArray.a(3);
        int i2 = this.o;
        if (i2 == 0) {
            parsableBitArray.c(8);
        } else if (i2 == 1) {
            parsableBitArray.c(9);
        } else if (i2 == 3 || i2 == 4 || i2 == 5) {
            parsableBitArray.c(6);
        } else if (i2 == 6 || i2 == 7) {
            parsableBitArray.c(1);
        } else {
            throw new IllegalStateException();
        }
    }

    private int e(ParsableBitArray parsableBitArray) throws ParserException {
        int a2;
        if (this.o == 0) {
            int i2 = 0;
            do {
                a2 = parsableBitArray.a(8);
                i2 += a2;
            } while (a2 == 255);
            return i2;
        }
        throw new ParserException();
    }

    private void f(ParsableBitArray parsableBitArray) throws ParserException {
        boolean e2;
        ParsableBitArray parsableBitArray2 = parsableBitArray;
        int a2 = parsableBitArray2.a(1);
        this.m = a2 == 1 ? parsableBitArray2.a(1) : 0;
        if (this.m == 0) {
            if (a2 == 1) {
                a(parsableBitArray);
            }
            if (parsableBitArray.e()) {
                this.n = parsableBitArray2.a(6);
                int a3 = parsableBitArray2.a(4);
                int a4 = parsableBitArray2.a(3);
                if (a3 == 0 && a4 == 0) {
                    if (a2 == 0) {
                        int d2 = parsableBitArray.d();
                        int c2 = c(parsableBitArray);
                        parsableBitArray2.b(d2);
                        byte[] bArr = new byte[((c2 + 7) / 8)];
                        parsableBitArray2.a(bArr, 0, c2);
                        Format a5 = Format.a(this.f, "audio/mp4a-latm", (String) null, -1, -1, this.t, this.r, (List<byte[]>) Collections.singletonList(bArr), (DrmInitData) null, 0, this.f3331a);
                        if (!a5.equals(this.e)) {
                            this.e = a5;
                            this.s = 1024000000 / ((long) a5.u);
                            this.d.a(a5);
                        }
                    } else {
                        parsableBitArray2.c(((int) a(parsableBitArray)) - c(parsableBitArray));
                    }
                    d(parsableBitArray);
                    this.p = parsableBitArray.e();
                    this.q = 0;
                    if (this.p) {
                        if (a2 == 1) {
                            this.q = a(parsableBitArray);
                        } else {
                            do {
                                e2 = parsableBitArray.e();
                                this.q = (this.q << 8) + ((long) parsableBitArray2.a(8));
                            } while (e2);
                        }
                    }
                    if (parsableBitArray.e()) {
                        parsableBitArray2.c(8);
                        return;
                    }
                    return;
                }
                throw new ParserException();
            }
            throw new ParserException();
        }
        throw new ParserException();
    }

    public void a() {
        this.g = 0;
        this.l = false;
    }

    public void b() {
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.d = extractorOutput.a(trackIdGenerator.c(), 1);
        this.f = trackIdGenerator.b();
    }

    public void a(long j2, boolean z) {
        this.k = j2;
    }

    public void a(ParsableByteArray parsableByteArray) throws ParserException {
        while (parsableByteArray.a() > 0) {
            int i2 = this.g;
            if (i2 != 0) {
                if (i2 == 1) {
                    int t2 = parsableByteArray.t();
                    if ((t2 & 224) == 224) {
                        this.j = t2;
                        this.g = 2;
                    } else if (t2 != 86) {
                        this.g = 0;
                    }
                } else if (i2 == 2) {
                    this.i = ((this.j & -225) << 8) | parsableByteArray.t();
                    int i3 = this.i;
                    if (i3 > this.b.f3641a.length) {
                        a(i3);
                    }
                    this.h = 0;
                    this.g = 3;
                } else if (i2 == 3) {
                    int min = Math.min(parsableByteArray.a(), this.i - this.h);
                    parsableByteArray.a(this.c.f3640a, this.h, min);
                    this.h += min;
                    if (this.h == this.i) {
                        this.c.b(0);
                        b(this.c);
                        this.g = 0;
                    }
                } else {
                    throw new IllegalStateException();
                }
            } else if (parsableByteArray.t() == 86) {
                this.g = 1;
            }
        }
    }

    private void a(ParsableBitArray parsableBitArray, int i2) {
        int d2 = parsableBitArray.d();
        if ((d2 & 7) == 0) {
            this.b.e(d2 >> 3);
        } else {
            parsableBitArray.a(this.b.f3641a, 0, i2 * 8);
            this.b.e(0);
        }
        this.d.a(this.b, i2);
        this.d.a(this.k, 1, i2, 0, (TrackOutput.CryptoData) null);
        this.k += this.s;
    }

    private void a(int i2) {
        this.b.c(i2);
        this.c.a(this.b.f3641a);
    }

    private static long a(ParsableBitArray parsableBitArray) {
        return (long) parsableBitArray.a((parsableBitArray.a(2) + 1) * 8);
    }
}
