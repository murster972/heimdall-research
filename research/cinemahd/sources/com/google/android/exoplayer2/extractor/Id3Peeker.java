package com.google.android.exoplayer2.extractor;

import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.Id3Decoder;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.io.EOFException;
import java.io.IOException;

public final class Id3Peeker {

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3252a = new ParsableByteArray(10);

    public Metadata a(ExtractorInput extractorInput, Id3Decoder.FramePredicate framePredicate) throws IOException, InterruptedException {
        Metadata metadata = null;
        int i = 0;
        while (true) {
            try {
                extractorInput.a(this.f3252a.f3641a, 0, 10);
                this.f3252a.e(0);
                if (this.f3252a.w() != Id3Decoder.b) {
                    break;
                }
                this.f3252a.f(3);
                int s = this.f3252a.s();
                int i2 = s + 10;
                if (metadata == null) {
                    byte[] bArr = new byte[i2];
                    System.arraycopy(this.f3252a.f3641a, 0, bArr, 0, 10);
                    extractorInput.a(bArr, 10, s);
                    metadata = new Id3Decoder(framePredicate).a(bArr, i2);
                } else {
                    extractorInput.a(s);
                }
                i += i2;
            } catch (EOFException unused) {
            }
        }
        extractorInput.a();
        extractorInput.a(i);
        return metadata;
    }
}
