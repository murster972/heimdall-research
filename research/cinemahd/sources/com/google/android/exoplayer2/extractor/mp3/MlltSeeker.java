package com.google.android.exoplayer2.extractor.mp3;

import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.SeekPoint;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.metadata.id3.MlltFrame;
import com.google.android.exoplayer2.util.Util;

final class MlltSeeker implements Mp3Extractor.Seeker {

    /* renamed from: a  reason: collision with root package name */
    private final long[] f3273a;
    private final long[] b;
    private final long c;

    private MlltSeeker(long[] jArr, long[] jArr2) {
        this.f3273a = jArr;
        this.b = jArr2;
        this.c = C.a(jArr2[jArr2.length - 1]);
    }

    public static MlltSeeker a(long j, MlltFrame mlltFrame) {
        int length = mlltFrame.e.length;
        int i = length + 1;
        long[] jArr = new long[i];
        long[] jArr2 = new long[i];
        jArr[0] = j;
        long j2 = 0;
        jArr2[0] = 0;
        for (int i2 = 1; i2 <= length; i2++) {
            int i3 = i2 - 1;
            j += (long) (mlltFrame.c + mlltFrame.e[i3]);
            j2 += (long) (mlltFrame.d + mlltFrame.f[i3]);
            jArr[i2] = j;
            jArr2[i2] = j2;
        }
        return new MlltSeeker(jArr, jArr2);
    }

    public long a() {
        return -1;
    }

    public SeekMap.SeekPoints b(long j) {
        Pair<Long, Long> a2 = a(C.b(Util.b(j, 0, this.c)), this.b, this.f3273a);
        return new SeekMap.SeekPoints(new SeekPoint(C.a(((Long) a2.first).longValue()), ((Long) a2.second).longValue()));
    }

    public boolean b() {
        return true;
    }

    public long getDurationUs() {
        return this.c;
    }

    public long a(long j) {
        return C.a(((Long) a(j, this.f3273a, this.b).second).longValue());
    }

    private static Pair<Long, Long> a(long j, long[] jArr, long[] jArr2) {
        int b2 = Util.b(jArr, j, true, true);
        long j2 = jArr[b2];
        long j3 = jArr2[b2];
        int i = b2 + 1;
        if (i == jArr.length) {
            return Pair.create(Long.valueOf(j2), Long.valueOf(j3));
        }
        long j4 = jArr[i];
        return Pair.create(Long.valueOf(j), Long.valueOf(((long) ((j4 == j2 ? 0.0d : (((double) j) - ((double) j2)) / ((double) (j4 - j2))) * ((double) (jArr2[i] - j3)))) + j3));
    }
}
