package com.google.android.exoplayer2.extractor.ts;

import android.util.Pair;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.TrackOutput;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.NalUnitUtil;
import com.google.android.exoplayer2.util.ParsableByteArray;
import java.util.Arrays;

public final class H262Reader implements ElementaryStreamReader {
    private static final double[] q = {23.976023976023978d, 24.0d, 25.0d, 29.97002997002997d, 30.0d, 50.0d, 59.94005994005994d, 60.0d};

    /* renamed from: a  reason: collision with root package name */
    private String f3323a;
    private TrackOutput b;
    private boolean c;
    private long d;
    private final UserDataReader e;
    private final ParsableByteArray f;
    private final boolean[] g;
    private final CsdBuffer h;
    private final NalUnitTargetBuffer i;
    private long j;
    private boolean k;
    private long l;
    private long m;
    private long n;
    private boolean o;
    private boolean p;

    public H262Reader() {
        this((UserDataReader) null);
    }

    public void a() {
        NalUnitUtil.a(this.g);
        this.h.a();
        if (this.e != null) {
            this.i.b();
        }
        this.j = 0;
        this.k = false;
    }

    public void b() {
    }

    public H262Reader(UserDataReader userDataReader) {
        this.e = userDataReader;
        this.g = new boolean[4];
        this.h = new CsdBuffer(128);
        if (userDataReader != null) {
            this.i = new NalUnitTargetBuffer(178, 128);
            this.f = new ParsableByteArray();
            return;
        }
        this.i = null;
        this.f = null;
    }

    private static final class CsdBuffer {
        private static final byte[] e = {0, 0, 1};

        /* renamed from: a  reason: collision with root package name */
        private boolean f3324a;
        public int b;
        public int c;
        public byte[] d;

        public CsdBuffer(int i) {
            this.d = new byte[i];
        }

        public void a() {
            this.f3324a = false;
            this.b = 0;
            this.c = 0;
        }

        public boolean a(int i, int i2) {
            if (this.f3324a) {
                this.b -= i2;
                if (this.c == 0 && i == 181) {
                    this.c = this.b;
                } else {
                    this.f3324a = false;
                    return true;
                }
            } else if (i == 179) {
                this.f3324a = true;
            }
            byte[] bArr = e;
            a(bArr, 0, bArr.length);
            return false;
        }

        public void a(byte[] bArr, int i, int i2) {
            if (this.f3324a) {
                int i3 = i2 - i;
                byte[] bArr2 = this.d;
                int length = bArr2.length;
                int i4 = this.b;
                if (length < i4 + i3) {
                    this.d = Arrays.copyOf(bArr2, (i4 + i3) * 2);
                }
                System.arraycopy(bArr, i, this.d, this.b, i3);
                this.b += i3;
            }
        }
    }

    public void a(ExtractorOutput extractorOutput, TsPayloadReader.TrackIdGenerator trackIdGenerator) {
        trackIdGenerator.a();
        this.f3323a = trackIdGenerator.b();
        this.b = extractorOutput.a(trackIdGenerator.c(), 2);
        UserDataReader userDataReader = this.e;
        if (userDataReader != null) {
            userDataReader.a(extractorOutput, trackIdGenerator);
        }
    }

    public void a(long j2, boolean z) {
        this.l = j2;
    }

    public void a(ParsableByteArray parsableByteArray) {
        int i2;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int c2 = parsableByteArray.c();
        int d2 = parsableByteArray.d();
        byte[] bArr = parsableByteArray2.f3641a;
        this.j += (long) parsableByteArray.a();
        this.b.a(parsableByteArray2, parsableByteArray.a());
        while (true) {
            int a2 = NalUnitUtil.a(bArr, c2, d2, this.g);
            if (a2 == d2) {
                break;
            }
            int i3 = a2 + 3;
            byte b2 = parsableByteArray2.f3641a[i3] & 255;
            int i4 = a2 - c2;
            boolean z = false;
            if (!this.c) {
                if (i4 > 0) {
                    this.h.a(bArr, c2, a2);
                }
                if (this.h.a(b2, i4 < 0 ? -i4 : 0)) {
                    Pair<Format, Long> a3 = a(this.h, this.f3323a);
                    this.b.a((Format) a3.first);
                    this.d = ((Long) a3.second).longValue();
                    this.c = true;
                }
            }
            if (this.e != null) {
                if (i4 > 0) {
                    this.i.a(bArr, c2, a2);
                    i2 = 0;
                } else {
                    i2 = -i4;
                }
                if (this.i.a(i2)) {
                    NalUnitTargetBuffer nalUnitTargetBuffer = this.i;
                    this.f.a(this.i.d, NalUnitUtil.c(nalUnitTargetBuffer.d, nalUnitTargetBuffer.e));
                    this.e.a(this.n, this.f);
                }
                if (b2 == 178 && parsableByteArray2.f3641a[a2 + 2] == 1) {
                    this.i.b(b2);
                }
            }
            if (b2 == 0 || b2 == 179) {
                int i5 = d2 - a2;
                if (this.k && this.p && this.c) {
                    this.b.a(this.n, this.o ? 1 : 0, ((int) (this.j - this.m)) - i5, i5, (TrackOutput.CryptoData) null);
                }
                if (!this.k || this.p) {
                    this.m = this.j - ((long) i5);
                    long j2 = this.l;
                    if (j2 == -9223372036854775807L) {
                        j2 = this.k ? this.n + this.d : 0;
                    }
                    this.n = j2;
                    this.o = false;
                    this.l = -9223372036854775807L;
                    this.k = true;
                }
                if (b2 == 0) {
                    z = true;
                }
                this.p = z;
            } else if (b2 == 184) {
                this.o = true;
            }
            c2 = i3;
        }
        if (!this.c) {
            this.h.a(bArr, c2, d2);
        }
        if (this.e != null) {
            this.i.a(bArr, c2, d2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.util.Pair<com.google.android.exoplayer2.Format, java.lang.Long> a(com.google.android.exoplayer2.extractor.ts.H262Reader.CsdBuffer r20, java.lang.String r21) {
        /*
            r0 = r20
            byte[] r1 = r0.d
            int r2 = r0.b
            byte[] r1 = java.util.Arrays.copyOf(r1, r2)
            r2 = 4
            byte r3 = r1[r2]
            r3 = r3 & 255(0xff, float:3.57E-43)
            r4 = 5
            byte r5 = r1[r4]
            r5 = r5 & 255(0xff, float:3.57E-43)
            r6 = 6
            byte r6 = r1[r6]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r3 = r3 << r2
            int r7 = r5 >> 4
            r13 = r3 | r7
            r3 = r5 & 15
            int r3 = r3 << 8
            r14 = r3 | r6
            r3 = 7
            byte r5 = r1[r3]
            r5 = r5 & 240(0xf0, float:3.36E-43)
            int r5 = r5 >> r2
            r6 = 2
            if (r5 == r6) goto L_0x0043
            r6 = 3
            if (r5 == r6) goto L_0x003d
            if (r5 == r2) goto L_0x0037
            r2 = 1065353216(0x3f800000, float:1.0)
            r18 = 1065353216(0x3f800000, float:1.0)
            goto L_0x004c
        L_0x0037:
            int r2 = r14 * 121
            float r2 = (float) r2
            int r5 = r13 * 100
            goto L_0x0048
        L_0x003d:
            int r2 = r14 * 16
            float r2 = (float) r2
            int r5 = r13 * 9
            goto L_0x0048
        L_0x0043:
            int r2 = r14 * 4
            float r2 = (float) r2
            int r5 = r13 * 3
        L_0x0048:
            float r5 = (float) r5
            float r2 = r2 / r5
            r18 = r2
        L_0x004c:
            r10 = 0
            r11 = -1
            r12 = -1
            r15 = -1082130432(0xffffffffbf800000, float:-1.0)
            java.util.List r16 = java.util.Collections.singletonList(r1)
            r17 = -1
            r19 = 0
            java.lang.String r9 = "video/mpeg2"
            r8 = r21
            com.google.android.exoplayer2.Format r2 = com.google.android.exoplayer2.Format.a((java.lang.String) r8, (java.lang.String) r9, (java.lang.String) r10, (int) r11, (int) r12, (int) r13, (int) r14, (float) r15, (java.util.List<byte[]>) r16, (int) r17, (float) r18, (com.google.android.exoplayer2.drm.DrmInitData) r19)
            r5 = 0
            byte r3 = r1[r3]
            r3 = r3 & 15
            int r3 = r3 + -1
            if (r3 < 0) goto L_0x0092
            double[] r7 = q
            int r8 = r7.length
            if (r3 >= r8) goto L_0x0092
            r5 = r7[r3]
            int r0 = r0.c
            int r0 = r0 + 9
            byte r3 = r1[r0]
            r3 = r3 & 96
            int r3 = r3 >> r4
            byte r0 = r1[r0]
            r0 = r0 & 31
            if (r3 == r0) goto L_0x008b
            double r3 = (double) r3
            r7 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r3 = r3 + r7
            int r0 = r0 + 1
            double r0 = (double) r0
            double r3 = r3 / r0
            double r5 = r5 * r3
        L_0x008b:
            r0 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r0 = r0 / r5
            long r5 = (long) r0
        L_0x0092:
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            android.util.Pair r0 = android.util.Pair.create(r2, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.extractor.ts.H262Reader.a(com.google.android.exoplayer2.extractor.ts.H262Reader$CsdBuffer, java.lang.String):android.util.Pair");
    }
}
