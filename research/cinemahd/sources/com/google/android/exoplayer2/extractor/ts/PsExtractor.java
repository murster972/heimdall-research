package com.google.android.exoplayer2.extractor.ts;

import android.support.v4.media.session.PlaybackStateCompat;
import android.util.SparseArray;
import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.BinarySearchSeeker;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorInput;
import com.google.android.exoplayer2.extractor.ExtractorOutput;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.extractor.SeekMap;
import com.google.android.exoplayer2.extractor.ts.TsPayloadReader;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import java.io.IOException;

public final class PsExtractor implements Extractor {

    /* renamed from: a  reason: collision with root package name */
    private final TimestampAdjuster f3337a;
    private final SparseArray<PesReader> b;
    private final ParsableByteArray c;
    private final PsDurationReader d;
    private boolean e;
    private boolean f;
    private boolean g;
    private long h;
    private PsBinarySearchSeeker i;
    private ExtractorOutput j;
    private boolean k;

    static {
        c cVar = c.f3353a;
    }

    public PsExtractor() {
        this(new TimestampAdjuster(0));
    }

    static /* synthetic */ Extractor[] a() {
        return new Extractor[]{new PsExtractor()};
    }

    public void release() {
    }

    private static final class PesReader {

        /* renamed from: a  reason: collision with root package name */
        private final ElementaryStreamReader f3338a;
        private final TimestampAdjuster b;
        private final ParsableBitArray c = new ParsableBitArray(new byte[64]);
        private boolean d;
        private boolean e;
        private boolean f;
        private int g;
        private long h;

        public PesReader(ElementaryStreamReader elementaryStreamReader, TimestampAdjuster timestampAdjuster) {
            this.f3338a = elementaryStreamReader;
            this.b = timestampAdjuster;
        }

        private void b() {
            this.c.c(8);
            this.d = this.c.e();
            this.e = this.c.e();
            this.c.c(6);
            this.g = this.c.a(8);
        }

        private void c() {
            this.h = 0;
            if (this.d) {
                this.c.c(4);
                this.c.c(1);
                this.c.c(1);
                long a2 = (((long) this.c.a(3)) << 30) | ((long) (this.c.a(15) << 15)) | ((long) this.c.a(15));
                this.c.c(1);
                if (!this.f && this.e) {
                    this.c.c(4);
                    this.c.c(1);
                    this.c.c(1);
                    this.c.c(1);
                    this.b.b((((long) this.c.a(3)) << 30) | ((long) (this.c.a(15) << 15)) | ((long) this.c.a(15)));
                    this.f = true;
                }
                this.h = this.b.b(a2);
            }
        }

        public void a() {
            this.f = false;
            this.f3338a.a();
        }

        public void a(ParsableByteArray parsableByteArray) throws ParserException {
            parsableByteArray.a(this.c.f3640a, 0, 3);
            this.c.b(0);
            b();
            parsableByteArray.a(this.c.f3640a, 0, this.g);
            this.c.b(0);
            c();
            this.f3338a.a(this.h, true);
            this.f3338a.a(parsableByteArray);
            this.f3338a.b();
        }
    }

    public PsExtractor(TimestampAdjuster timestampAdjuster) {
        this.f3337a = timestampAdjuster;
        this.c = new ParsableByteArray(4096);
        this.b = new SparseArray<>();
        this.d = new PsDurationReader();
    }

    public boolean a(ExtractorInput extractorInput) throws IOException, InterruptedException {
        byte[] bArr = new byte[14];
        extractorInput.a(bArr, 0, 14);
        if (442 != (((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255)) || (bArr[4] & 196) != 68 || (bArr[6] & 4) != 4 || (bArr[8] & 4) != 4 || (bArr[9] & 1) != 1 || (bArr[12] & 3) != 3) {
            return false;
        }
        extractorInput.a(bArr[13] & 7);
        extractorInput.a(bArr, 0, 3);
        if (1 == (((bArr[0] & 255) << 16) | ((bArr[1] & 255) << 8) | (bArr[2] & 255))) {
            return true;
        }
        return false;
    }

    public void a(ExtractorOutput extractorOutput) {
        this.j = extractorOutput;
    }

    public void a(long j2, long j3) {
        if ((this.f3337a.c() == -9223372036854775807L) || !(this.f3337a.a() == 0 || this.f3337a.a() == j3)) {
            this.f3337a.d();
            this.f3337a.c(j3);
        }
        PsBinarySearchSeeker psBinarySearchSeeker = this.i;
        if (psBinarySearchSeeker != null) {
            psBinarySearchSeeker.b(j3);
        }
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            this.b.valueAt(i2).a();
        }
    }

    public int a(ExtractorInput extractorInput, PositionHolder positionHolder) throws IOException, InterruptedException {
        long length = extractorInput.getLength();
        int i2 = (length > -1 ? 1 : (length == -1 ? 0 : -1));
        if ((i2 != 0) && !this.d.c()) {
            return this.d.a(extractorInput, positionHolder);
        }
        a(length);
        PsBinarySearchSeeker psBinarySearchSeeker = this.i;
        ElementaryStreamReader elementaryStreamReader = null;
        if (psBinarySearchSeeker != null && psBinarySearchSeeker.b()) {
            return this.i.a(extractorInput, positionHolder, (BinarySearchSeeker.OutputFrameHolder) null);
        }
        extractorInput.a();
        long b2 = i2 != 0 ? length - extractorInput.b() : -1;
        if ((b2 != -1 && b2 < 4) || !extractorInput.a(this.c.f3641a, 0, 4, true)) {
            return -1;
        }
        this.c.e(0);
        int h2 = this.c.h();
        if (h2 == 441) {
            return -1;
        }
        if (h2 == 442) {
            extractorInput.a(this.c.f3641a, 0, 10);
            this.c.e(9);
            extractorInput.c((this.c.t() & 7) + 14);
            return 0;
        } else if (h2 == 443) {
            extractorInput.a(this.c.f3641a, 0, 2);
            this.c.e(0);
            extractorInput.c(this.c.z() + 6);
            return 0;
        } else if (((h2 & -256) >> 8) != 1) {
            extractorInput.c(1);
            return 0;
        } else {
            int i3 = h2 & JfifUtil.MARKER_FIRST_BYTE;
            PesReader pesReader = this.b.get(i3);
            if (!this.e) {
                if (pesReader == null) {
                    if (i3 == 189) {
                        elementaryStreamReader = new Ac3Reader();
                        this.f = true;
                        this.h = extractorInput.getPosition();
                    } else if ((i3 & 224) == 192) {
                        elementaryStreamReader = new MpegAudioReader();
                        this.f = true;
                        this.h = extractorInput.getPosition();
                    } else if ((i3 & 240) == 224) {
                        elementaryStreamReader = new H262Reader();
                        this.g = true;
                        this.h = extractorInput.getPosition();
                    }
                    if (elementaryStreamReader != null) {
                        elementaryStreamReader.a(this.j, new TsPayloadReader.TrackIdGenerator(i3, 256));
                        pesReader = new PesReader(elementaryStreamReader, this.f3337a);
                        this.b.put(i3, pesReader);
                    }
                }
                if (extractorInput.getPosition() > ((!this.f || !this.g) ? PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED : this.h + PlaybackStateCompat.ACTION_PLAY_FROM_URI)) {
                    this.e = true;
                    this.j.a();
                }
            }
            extractorInput.a(this.c.f3641a, 0, 2);
            this.c.e(0);
            int z = this.c.z() + 6;
            if (pesReader == null) {
                extractorInput.c(z);
            } else {
                this.c.c(z);
                extractorInput.readFully(this.c.f3641a, 0, z);
                this.c.e(6);
                pesReader.a(this.c);
                ParsableByteArray parsableByteArray = this.c;
                parsableByteArray.d(parsableByteArray.b());
            }
            return 0;
        }
    }

    private void a(long j2) {
        if (!this.k) {
            this.k = true;
            if (this.d.a() != -9223372036854775807L) {
                this.i = new PsBinarySearchSeeker(this.d.b(), this.d.a(), j2);
                this.j.a(this.i.a());
                return;
            }
            this.j.a(new SeekMap.Unseekable(this.d.a()));
        }
    }
}
