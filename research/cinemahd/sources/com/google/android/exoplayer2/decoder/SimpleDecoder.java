package com.google.android.exoplayer2.decoder;

import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.decoder.OutputBuffer;
import com.google.android.exoplayer2.util.Assertions;
import java.lang.Exception;
import java.util.ArrayDeque;

public abstract class SimpleDecoder<I extends DecoderInputBuffer, O extends OutputBuffer, E extends Exception> implements Decoder<I, O, E> {

    /* renamed from: a  reason: collision with root package name */
    private final Thread f3217a;
    private final Object b = new Object();
    private final ArrayDeque<I> c = new ArrayDeque<>();
    private final ArrayDeque<O> d = new ArrayDeque<>();
    private final I[] e;
    private final O[] f;
    private int g;
    private int h;
    private I i;
    private E j;
    private boolean k;
    private boolean l;
    private int m;

    protected SimpleDecoder(I[] iArr, O[] oArr) {
        this.e = iArr;
        this.g = iArr.length;
        for (int i2 = 0; i2 < this.g; i2++) {
            this.e[i2] = c();
        }
        this.f = oArr;
        this.h = oArr.length;
        for (int i3 = 0; i3 < this.h; i3++) {
            this.f[i3] = d();
        }
        this.f3217a = new Thread() {
            public void run() {
                SimpleDecoder.this.i();
            }
        };
        this.f3217a.start();
    }

    private boolean e() {
        return !this.c.isEmpty() && this.h > 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        if (r1.isEndOfStream() == false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        r3.addFlag(4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        if (r1.isDecodeOnly() == false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0042, code lost:
        r3.addFlag(Integer.MIN_VALUE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r6.j = a(r1, r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004f, code lost:
        r6.j = a((java.lang.Throwable) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0056, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        r6.j = a((java.lang.Throwable) r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean f() throws java.lang.InterruptedException {
        /*
            r6 = this;
            java.lang.Object r0 = r6.b
            monitor-enter(r0)
        L_0x0003:
            boolean r1 = r6.l     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0013
            boolean r1 = r6.e()     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0013
            java.lang.Object r1 = r6.b     // Catch:{ all -> 0x0096 }
            r1.wait()     // Catch:{ all -> 0x0096 }
            goto L_0x0003
        L_0x0013:
            boolean r1 = r6.l     // Catch:{ all -> 0x0096 }
            r2 = 0
            if (r1 == 0) goto L_0x001a
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            return r2
        L_0x001a:
            java.util.ArrayDeque<I> r1 = r6.c     // Catch:{ all -> 0x0096 }
            java.lang.Object r1 = r1.removeFirst()     // Catch:{ all -> 0x0096 }
            com.google.android.exoplayer2.decoder.DecoderInputBuffer r1 = (com.google.android.exoplayer2.decoder.DecoderInputBuffer) r1     // Catch:{ all -> 0x0096 }
            O[] r3 = r6.f     // Catch:{ all -> 0x0096 }
            int r4 = r6.h     // Catch:{ all -> 0x0096 }
            r5 = 1
            int r4 = r4 - r5
            r6.h = r4     // Catch:{ all -> 0x0096 }
            r3 = r3[r4]     // Catch:{ all -> 0x0096 }
            boolean r4 = r6.k     // Catch:{ all -> 0x0096 }
            r6.k = r2     // Catch:{ all -> 0x0096 }
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            boolean r0 = r1.isEndOfStream()
            if (r0 == 0) goto L_0x003c
            r0 = 4
            r3.addFlag(r0)
            goto L_0x0069
        L_0x003c:
            boolean r0 = r1.isDecodeOnly()
            if (r0 == 0) goto L_0x0047
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r3.addFlag(r0)
        L_0x0047:
            java.lang.Exception r0 = r6.a(r1, r3, r4)     // Catch:{ RuntimeException -> 0x0056, OutOfMemoryError -> 0x004e }
            r6.j = r0     // Catch:{ RuntimeException -> 0x0056, OutOfMemoryError -> 0x004e }
            goto L_0x005d
        L_0x004e:
            r0 = move-exception
            java.lang.Exception r0 = r6.a((java.lang.Throwable) r0)
            r6.j = r0
            goto L_0x005d
        L_0x0056:
            r0 = move-exception
            java.lang.Exception r0 = r6.a((java.lang.Throwable) r0)
            r6.j = r0
        L_0x005d:
            E r0 = r6.j
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r6.b
            monitor-enter(r0)
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            return r2
        L_0x0066:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            throw r1
        L_0x0069:
            java.lang.Object r4 = r6.b
            monitor-enter(r4)
            boolean r0 = r6.k     // Catch:{ all -> 0x0093 }
            if (r0 == 0) goto L_0x0074
            r3.release()     // Catch:{ all -> 0x0093 }
            goto L_0x008e
        L_0x0074:
            boolean r0 = r3.isDecodeOnly()     // Catch:{ all -> 0x0093 }
            if (r0 == 0) goto L_0x0083
            int r0 = r6.m     // Catch:{ all -> 0x0093 }
            int r0 = r0 + r5
            r6.m = r0     // Catch:{ all -> 0x0093 }
            r3.release()     // Catch:{ all -> 0x0093 }
            goto L_0x008e
        L_0x0083:
            int r0 = r6.m     // Catch:{ all -> 0x0093 }
            r3.skippedOutputBufferCount = r0     // Catch:{ all -> 0x0093 }
            r6.m = r2     // Catch:{ all -> 0x0093 }
            java.util.ArrayDeque<O> r0 = r6.d     // Catch:{ all -> 0x0093 }
            r0.addLast(r3)     // Catch:{ all -> 0x0093 }
        L_0x008e:
            r6.b(r1)     // Catch:{ all -> 0x0093 }
            monitor-exit(r4)     // Catch:{ all -> 0x0093 }
            return r5
        L_0x0093:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0093 }
            throw r0
        L_0x0096:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.decoder.SimpleDecoder.f():boolean");
    }

    private void g() {
        if (e()) {
            this.b.notify();
        }
    }

    private void h() throws Exception {
        E e2 = this.j;
        if (e2 != null) {
            throw e2;
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        do {
            try {
            } catch (InterruptedException e2) {
                throw new IllegalStateException(e2);
            }
        } while (f());
    }

    /* access modifiers changed from: protected */
    public abstract E a(I i2, O o, boolean z);

    /* access modifiers changed from: protected */
    public abstract E a(Throwable th);

    /* access modifiers changed from: protected */
    public abstract I c();

    /* access modifiers changed from: protected */
    public abstract O d();

    public final void flush() {
        synchronized (this.b) {
            this.k = true;
            this.m = 0;
            if (this.i != null) {
                b(this.i);
                this.i = null;
            }
            while (!this.c.isEmpty()) {
                b((DecoderInputBuffer) this.c.removeFirst());
            }
            while (!this.d.isEmpty()) {
                ((OutputBuffer) this.d.removeFirst()).release();
            }
        }
    }

    public void release() {
        synchronized (this.b) {
            this.l = true;
            this.b.notify();
        }
        try {
            this.f3217a.join();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
    }

    public final I b() throws Exception {
        I i2;
        I i3;
        synchronized (this.b) {
            h();
            Assertions.b(this.i == null);
            if (this.g == 0) {
                i2 = null;
            } else {
                I[] iArr = this.e;
                int i4 = this.g - 1;
                this.g = i4;
                i2 = iArr[i4];
            }
            this.i = i2;
            i3 = this.i;
        }
        return i3;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        Assertions.b(this.g == this.e.length);
        for (I b2 : this.e) {
            b2.b(i2);
        }
    }

    public final void a(I i2) throws Exception {
        synchronized (this.b) {
            h();
            Assertions.a(i2 == this.i);
            this.c.addLast(i2);
            g();
            this.i = null;
        }
    }

    private void b(I i2) {
        i2.clear();
        I[] iArr = this.e;
        int i3 = this.g;
        this.g = i3 + 1;
        iArr[i3] = i2;
    }

    private void b(O o) {
        o.clear();
        O[] oArr = this.f;
        int i2 = this.h;
        this.h = i2 + 1;
        oArr[i2] = o;
    }

    public final O a() throws Exception {
        synchronized (this.b) {
            h();
            if (this.d.isEmpty()) {
                return null;
            }
            O o = (OutputBuffer) this.d.removeFirst();
            return o;
        }
    }

    /* access modifiers changed from: protected */
    public void a(O o) {
        synchronized (this.b) {
            b(o);
            g();
        }
    }
}
