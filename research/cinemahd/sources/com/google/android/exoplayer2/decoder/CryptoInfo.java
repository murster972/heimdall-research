package com.google.android.exoplayer2.decoder;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import com.google.android.exoplayer2.util.Util;

public final class CryptoInfo {

    /* renamed from: a  reason: collision with root package name */
    public byte[] f3213a;
    public byte[] b;
    public int c;
    public int[] d;
    public int[] e;
    public int f;
    public int g;
    public int h;
    private final MediaCodec.CryptoInfo i;
    private final PatternHolderV24 j;

    @TargetApi(24)
    private static final class PatternHolderV24 {

        /* renamed from: a  reason: collision with root package name */
        private final MediaCodec.CryptoInfo f3214a;
        private final MediaCodec.CryptoInfo.Pattern b;

        private PatternHolderV24(MediaCodec.CryptoInfo cryptoInfo) {
            this.f3214a = cryptoInfo;
            this.b = new MediaCodec.CryptoInfo.Pattern(0, 0);
        }

        /* access modifiers changed from: private */
        public void a(int i, int i2) {
            this.b.set(i, i2);
            this.f3214a.setPattern(this.b);
        }
    }

    public CryptoInfo() {
        this.i = Util.f3651a >= 16 ? b() : null;
        this.j = Util.f3651a >= 24 ? new PatternHolderV24(this.i) : null;
    }

    @TargetApi(16)
    private MediaCodec.CryptoInfo b() {
        return new MediaCodec.CryptoInfo();
    }

    @TargetApi(16)
    private void c() {
        MediaCodec.CryptoInfo cryptoInfo = this.i;
        cryptoInfo.numSubSamples = this.f;
        cryptoInfo.numBytesOfClearData = this.d;
        cryptoInfo.numBytesOfEncryptedData = this.e;
        cryptoInfo.key = this.b;
        cryptoInfo.iv = this.f3213a;
        cryptoInfo.mode = this.c;
        if (Util.f3651a >= 24) {
            this.j.a(this.g, this.h);
        }
    }

    public void a(int i2, int[] iArr, int[] iArr2, byte[] bArr, byte[] bArr2, int i3, int i4, int i5) {
        this.f = i2;
        this.d = iArr;
        this.e = iArr2;
        this.b = bArr;
        this.f3213a = bArr2;
        this.c = i3;
        this.g = i4;
        this.h = i5;
        if (Util.f3651a >= 16) {
            c();
        }
    }

    @TargetApi(16)
    public MediaCodec.CryptoInfo a() {
        return this.i;
    }
}
