package com.google.android.exoplayer2.decoder;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SimpleOutputBuffer extends OutputBuffer {

    /* renamed from: a  reason: collision with root package name */
    private final SimpleDecoder<?, SimpleOutputBuffer, ?> f3219a;
    public ByteBuffer b;

    public SimpleOutputBuffer(SimpleDecoder<?, SimpleOutputBuffer, ?> simpleDecoder) {
        this.f3219a = simpleDecoder;
    }

    public void clear() {
        super.clear();
        ByteBuffer byteBuffer = this.b;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
    }

    public ByteBuffer init(long j, int i) {
        this.timeUs = j;
        ByteBuffer byteBuffer = this.b;
        if (byteBuffer == null || byteBuffer.capacity() < i) {
            this.b = ByteBuffer.allocateDirect(i).order(ByteOrder.nativeOrder());
        }
        this.b.position(0);
        this.b.limit(i);
        return this.b;
    }

    public void release() {
        this.f3219a.a(this);
    }
}
