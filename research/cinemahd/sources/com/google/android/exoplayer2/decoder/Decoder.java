package com.google.android.exoplayer2.decoder;

import java.lang.Exception;

public interface Decoder<I, O, E extends Exception> {
    O a() throws Exception;

    void a(I i) throws Exception;

    I b() throws Exception;

    void flush();

    String getName();

    void release();
}
