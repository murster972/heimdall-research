package com.google.android.exoplayer2.decoder;

import java.nio.ByteBuffer;

public class DecoderInputBuffer extends Buffer {

    /* renamed from: a  reason: collision with root package name */
    public final CryptoInfo f3216a = new CryptoInfo();
    public ByteBuffer b;
    public long c;
    private final int d;

    public DecoderInputBuffer(int i) {
        this.d = i;
    }

    public static DecoderInputBuffer e() {
        return new DecoderInputBuffer(0);
    }

    public void b(int i) {
        ByteBuffer byteBuffer = this.b;
        if (byteBuffer == null) {
            this.b = c(i);
            return;
        }
        int capacity = byteBuffer.capacity();
        int position = this.b.position();
        int i2 = i + position;
        if (capacity < i2) {
            ByteBuffer c2 = c(i2);
            if (position > 0) {
                this.b.position(0);
                this.b.limit(position);
                c2.put(this.b);
            }
            this.b = c2;
        }
    }

    public final boolean c() {
        return getFlag(1073741824);
    }

    public void clear() {
        super.clear();
        ByteBuffer byteBuffer = this.b;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
    }

    public final boolean d() {
        return this.b == null && this.d == 0;
    }

    private ByteBuffer c(int i) {
        int i2 = this.d;
        if (i2 == 1) {
            return ByteBuffer.allocate(i);
        }
        if (i2 == 2) {
            return ByteBuffer.allocateDirect(i);
        }
        ByteBuffer byteBuffer = this.b;
        int capacity = byteBuffer == null ? 0 : byteBuffer.capacity();
        throw new IllegalStateException("Buffer too small (" + capacity + " < " + i + ")");
    }

    public final void b() {
        this.b.flip();
    }
}
