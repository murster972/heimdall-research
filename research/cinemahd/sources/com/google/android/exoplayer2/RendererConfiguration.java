package com.google.android.exoplayer2;

public final class RendererConfiguration {
    public static final RendererConfiguration b = new RendererConfiguration(0);

    /* renamed from: a  reason: collision with root package name */
    public final int f3172a;

    public RendererConfiguration(int i) {
        this.f3172a = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && RendererConfiguration.class == obj.getClass() && this.f3172a == ((RendererConfiguration) obj).f3172a) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.f3172a;
    }
}
