package com.google.android.exoplayer2.offline;

/* compiled from: lambda */
public final /* synthetic */ class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ DownloadManager f3393a;
    private final /* synthetic */ DownloadAction[] b;

    public /* synthetic */ b(DownloadManager downloadManager, DownloadAction[] downloadActionArr) {
        this.f3393a = downloadManager;
        this.b = downloadActionArr;
    }

    public final void run() {
        this.f3393a.b(this.b);
    }
}
