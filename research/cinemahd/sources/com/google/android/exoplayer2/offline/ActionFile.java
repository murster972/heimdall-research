package com.google.android.exoplayer2.offline;

import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.util.AtomicFile;
import com.google.android.exoplayer2.util.Util;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class ActionFile {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicFile f3380a;
    private final File b;

    public ActionFile(File file) {
        this.b = file;
        this.f3380a = new AtomicFile(file);
    }

    public DownloadAction[] a(DownloadAction.Deserializer... deserializerArr) throws IOException {
        if (!this.b.exists()) {
            return new DownloadAction[0];
        }
        InputStream inputStream = null;
        try {
            inputStream = this.f3380a.b();
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            int readInt = dataInputStream.readInt();
            if (readInt <= 0) {
                int readInt2 = dataInputStream.readInt();
                DownloadAction[] downloadActionArr = new DownloadAction[readInt2];
                for (int i = 0; i < readInt2; i++) {
                    downloadActionArr[i] = DownloadAction.a(deserializerArr, (InputStream) dataInputStream);
                }
                return downloadActionArr;
            }
            throw new IOException("Unsupported action file version: " + readInt);
        } finally {
            Util.a((Closeable) inputStream);
        }
    }

    public void a(DownloadAction... downloadActionArr) throws IOException {
        DataOutputStream dataOutputStream;
        try {
            dataOutputStream = new DataOutputStream(this.f3380a.c());
            try {
                dataOutputStream.writeInt(0);
                dataOutputStream.writeInt(downloadActionArr.length);
                for (DownloadAction a2 : downloadActionArr) {
                    DownloadAction.a(a2, (OutputStream) dataOutputStream);
                }
                this.f3380a.a(dataOutputStream);
                Util.a((Closeable) null);
            } catch (Throwable th) {
                th = th;
                Util.a((Closeable) dataOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            dataOutputStream = null;
            Util.a((Closeable) dataOutputStream);
            throw th;
        }
    }
}
