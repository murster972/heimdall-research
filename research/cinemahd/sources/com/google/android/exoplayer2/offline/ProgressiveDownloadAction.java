package com.google.android.exoplayer2.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.util.Util;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public final class ProgressiveDownloadAction extends DownloadAction {
    public static final DownloadAction.Deserializer h = new DownloadAction.Deserializer("progressive", 0) {
        public ProgressiveDownloadAction a(int i, DataInputStream dataInputStream) throws IOException {
            Uri parse = Uri.parse(dataInputStream.readUTF());
            boolean readBoolean = dataInputStream.readBoolean();
            byte[] bArr = new byte[dataInputStream.readInt()];
            dataInputStream.readFully(bArr);
            return new ProgressiveDownloadAction(parse, readBoolean, bArr, dataInputStream.readBoolean() ? dataInputStream.readUTF() : null);
        }
    };
    private final String g;

    @Deprecated
    public ProgressiveDownloadAction(Uri uri, boolean z, byte[] bArr, String str) {
        super("progressive", 0, uri, z, bArr);
        this.g = str;
    }

    private String c() {
        String str = this.g;
        return str != null ? str : CacheUtil.a(this.c);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        return Util.a((Object) this.g, (Object) ((ProgressiveDownloadAction) obj).g);
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.g;
        return hashCode + (str != null ? str.hashCode() : 0);
    }

    public ProgressiveDownloader a(DownloaderConstructorHelper downloaderConstructorHelper) {
        return new ProgressiveDownloader(this.c, this.g, downloaderConstructorHelper);
    }

    /* access modifiers changed from: protected */
    public void a(DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeUTF(this.c.toString());
        dataOutputStream.writeBoolean(this.d);
        dataOutputStream.writeInt(this.e.length);
        dataOutputStream.write(this.e);
        boolean z = this.g != null;
        dataOutputStream.writeBoolean(z);
        if (z) {
            dataOutputStream.writeUTF(this.g);
        }
    }

    public boolean a(DownloadAction downloadAction) {
        return (downloadAction instanceof ProgressiveDownloadAction) && c().equals(((ProgressiveDownloadAction) downloadAction).c());
    }
}
