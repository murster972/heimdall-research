package com.google.android.exoplayer2.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.FilterableManifest;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public final class FilteringManifestParser<T extends FilterableManifest<T>> implements ParsingLoadable.Parser<T> {

    /* renamed from: a  reason: collision with root package name */
    private final ParsingLoadable.Parser<T> f3387a;
    private final List<StreamKey> b;

    public FilteringManifestParser(ParsingLoadable.Parser<T> parser, List<StreamKey> list) {
        this.f3387a = parser;
        this.b = list;
    }

    public T a(Uri uri, InputStream inputStream) throws IOException {
        T t = (FilterableManifest) this.f3387a.a(uri, inputStream);
        List<StreamKey> list = this.b;
        return (list == null || list.isEmpty()) ? t : (FilterableManifest) t.a(this.b);
    }
}
