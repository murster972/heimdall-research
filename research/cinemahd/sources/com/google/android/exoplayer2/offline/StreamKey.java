package com.google.android.exoplayer2.offline;

public final class StreamKey implements Comparable<StreamKey> {

    /* renamed from: a  reason: collision with root package name */
    public final int f3391a;
    public final int b;
    public final int c;

    public StreamKey(int i, int i2) {
        this(0, i, i2);
    }

    /* renamed from: a */
    public int compareTo(StreamKey streamKey) {
        int i = this.f3391a - streamKey.f3391a;
        if (i != 0) {
            return i;
        }
        int i2 = this.b - streamKey.b;
        return i2 == 0 ? this.c - streamKey.c : i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || StreamKey.class != obj.getClass()) {
            return false;
        }
        StreamKey streamKey = (StreamKey) obj;
        if (this.f3391a == streamKey.f3391a && this.b == streamKey.b && this.c == streamKey.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((this.f3391a * 31) + this.b) * 31) + this.c;
    }

    public String toString() {
        return this.f3391a + "." + this.b + "." + this.c;
    }

    public StreamKey(int i, int i2, int i3) {
        this.f3391a = i;
        this.b = i2;
        this.c = i3;
    }
}
