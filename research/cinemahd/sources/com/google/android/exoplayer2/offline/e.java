package com.google.android.exoplayer2.offline;

/* compiled from: lambda */
public final /* synthetic */ class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ DownloadManager f3396a;
    private final /* synthetic */ DownloadAction[] b;

    public /* synthetic */ e(DownloadManager downloadManager, DownloadAction[] downloadActionArr) {
        this.f3396a = downloadManager;
        this.b = downloadActionArr;
    }

    public final void run() {
        this.f3396a.a(this.b);
    }
}
