package com.google.android.exoplayer2.offline;

import java.io.IOException;

public interface Downloader {
    long a();

    void b() throws InterruptedException, IOException;

    float c();

    void cancel();

    void remove() throws InterruptedException;
}
