package com.google.android.exoplayer2.offline;

import android.net.Uri;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class DownloadAction {
    private static Deserializer[] f;

    /* renamed from: a  reason: collision with root package name */
    public final String f3381a;
    public final int b;
    public final Uri c;
    public final boolean d;
    public final byte[] e;

    public static abstract class Deserializer {

        /* renamed from: a  reason: collision with root package name */
        public final String f3382a;
        public final int b;

        public Deserializer(String str, int i) {
            this.f3382a = str;
            this.b = i;
        }

        public abstract DownloadAction a(int i, DataInputStream dataInputStream) throws IOException;
    }

    protected DownloadAction(String str, int i, Uri uri, boolean z, byte[] bArr) {
        this.f3381a = str;
        this.b = i;
        this.c = uri;
        this.d = z;
        this.e = bArr == null ? Util.f : bArr;
    }

    public static DownloadAction a(Deserializer[] deserializerArr, InputStream inputStream) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        String readUTF = dataInputStream.readUTF();
        int readInt = dataInputStream.readInt();
        for (Deserializer deserializer : deserializerArr) {
            if (readUTF.equals(deserializer.f3382a) && deserializer.b >= readInt) {
                return deserializer.a(readInt, dataInputStream);
            }
        }
        throw new DownloadException("No deserializer found for:" + readUTF + ", " + readInt);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(23:8|9|10|11|12|13|14|(2:15|16)|19|21|22|23|(2:24|25)|28|30|31|32|33|34|37|38|39|40) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0043 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized com.google.android.exoplayer2.offline.DownloadAction.Deserializer[] b() {
        /*
            java.lang.Class<com.google.android.exoplayer2.offline.DownloadAction> r0 = com.google.android.exoplayer2.offline.DownloadAction.class
            monitor-enter(r0)
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer[] r1 = f     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x000b
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer[] r1 = f     // Catch:{ all -> 0x0054 }
            monitor-exit(r0)
            return r1
        L_0x000b:
            r1 = 4
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer[] r1 = new com.google.android.exoplayer2.offline.DownloadAction.Deserializer[r1]     // Catch:{ all -> 0x0054 }
            r2 = 0
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer r3 = com.google.android.exoplayer2.offline.ProgressiveDownloadAction.h     // Catch:{ all -> 0x0054 }
            r1[r2] = r3     // Catch:{ all -> 0x0054 }
            java.lang.String r2 = "com.google.android.exoplayer2.source.dash.offline.DashDownloadAction"
            r3 = 1
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Exception -> 0x0022 }
            r4 = 2
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer r2 = a((java.lang.Class<?>) r2)     // Catch:{ Exception -> 0x0023 }
            r1[r3] = r2     // Catch:{ Exception -> 0x0023 }
            goto L_0x0023
        L_0x0022:
            r4 = 1
        L_0x0023:
            java.lang.String r2 = "com.google.android.exoplayer2.source.hls.offline.HlsDownloadAction"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Exception -> 0x0032 }
            int r3 = r4 + 1
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer r2 = a((java.lang.Class<?>) r2)     // Catch:{ Exception -> 0x0033 }
            r1[r4] = r2     // Catch:{ Exception -> 0x0033 }
            goto L_0x0033
        L_0x0032:
            r3 = r4
        L_0x0033:
            java.lang.String r2 = "com.google.android.exoplayer2.source.smoothstreaming.offline.SsDownloadAction"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Exception -> 0x0042 }
            int r4 = r3 + 1
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer r2 = a((java.lang.Class<?>) r2)     // Catch:{ Exception -> 0x0043 }
            r1[r3] = r2     // Catch:{ Exception -> 0x0043 }
            goto L_0x0043
        L_0x0042:
            r4 = r3
        L_0x0043:
            com.google.android.exoplayer2.util.Assertions.a(r1)     // Catch:{ all -> 0x0054 }
            java.lang.Object[] r1 = (java.lang.Object[]) r1     // Catch:{ all -> 0x0054 }
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r1, r4)     // Catch:{ all -> 0x0054 }
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer[] r1 = (com.google.android.exoplayer2.offline.DownloadAction.Deserializer[]) r1     // Catch:{ all -> 0x0054 }
            f = r1     // Catch:{ all -> 0x0054 }
            com.google.android.exoplayer2.offline.DownloadAction$Deserializer[] r1 = f     // Catch:{ all -> 0x0054 }
            monitor-exit(r0)
            return r1
        L_0x0054:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.offline.DownloadAction.b():com.google.android.exoplayer2.offline.DownloadAction$Deserializer[]");
    }

    public abstract Downloader a(DownloaderConstructorHelper downloaderConstructorHelper);

    /* access modifiers changed from: protected */
    public abstract void a(DataOutputStream dataOutputStream) throws IOException;

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DownloadAction downloadAction = (DownloadAction) obj;
        if (!this.f3381a.equals(downloadAction.f3381a) || this.b != downloadAction.b || !this.c.equals(downloadAction.c) || this.d != downloadAction.d || !Arrays.equals(this.e, downloadAction.e)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.c.hashCode() * 31) + (this.d ? 1 : 0)) * 31) + Arrays.hashCode(this.e);
    }

    public static void a(DownloadAction downloadAction, OutputStream outputStream) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        dataOutputStream.writeUTF(downloadAction.f3381a);
        dataOutputStream.writeInt(downloadAction.b);
        downloadAction.a(dataOutputStream);
        dataOutputStream.flush();
    }

    public boolean a(DownloadAction downloadAction) {
        return this.c.equals(downloadAction.c);
    }

    public List<StreamKey> a() {
        return Collections.emptyList();
    }

    private static Deserializer a(Class<?> cls) throws NoSuchFieldException, IllegalAccessException {
        Object obj = cls.getDeclaredField("DESERIALIZER").get((Object) null);
        Assertions.a(obj);
        return (Deserializer) obj;
    }
}
