package com.google.android.exoplayer2.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.FilterableManifest;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import com.google.android.exoplayer2.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SegmentDownloader<M extends FilterableManifest<M>> implements Downloader {

    /* renamed from: a  reason: collision with root package name */
    private final Uri f3389a;
    private final PriorityTaskManager b;
    private final Cache c;
    private final CacheDataSource d;
    private final CacheDataSource e;
    private final ArrayList<StreamKey> f;
    private final AtomicBoolean g = new AtomicBoolean();
    private volatile int h = -1;
    private volatile int i;
    private volatile long j;

    protected static class Segment implements Comparable<Segment> {

        /* renamed from: a  reason: collision with root package name */
        public final long f3390a;
        public final DataSpec b;

        public Segment(long j, DataSpec dataSpec) {
            this.f3390a = j;
            this.b = dataSpec;
        }

        /* renamed from: a */
        public int compareTo(Segment segment) {
            return Util.b(this.f3390a, segment.f3390a);
        }
    }

    public SegmentDownloader(Uri uri, List<StreamKey> list, DownloaderConstructorHelper downloaderConstructorHelper) {
        this.f3389a = uri;
        this.f = new ArrayList<>(list);
        this.c = downloaderConstructorHelper.a();
        this.d = downloaderConstructorHelper.a(false);
        this.e = downloaderConstructorHelper.a(true);
        this.b = downloaderConstructorHelper.b();
    }

    private List<Segment> d() throws IOException, InterruptedException {
        FilterableManifest a2 = a(this.d, this.f3389a);
        if (!this.f.isEmpty()) {
            a2 = (FilterableManifest) a2.a(this.f);
        }
        List<Segment> a3 = a(this.d, a2, false);
        CacheUtil.CachingCounters cachingCounters = new CacheUtil.CachingCounters();
        this.h = a3.size();
        this.i = 0;
        this.j = 0;
        for (int size = a3.size() - 1; size >= 0; size--) {
            CacheUtil.a(a3.get(size).b, this.c, cachingCounters);
            this.j += cachingCounters.f3613a;
            if (cachingCounters.f3613a == cachingCounters.c) {
                this.i++;
                a3.remove(size);
            }
        }
        return a3;
    }

    public final long a() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public abstract M a(DataSource dataSource, Uri uri) throws IOException;

    /* access modifiers changed from: protected */
    public abstract List<Segment> a(DataSource dataSource, M m, boolean z) throws InterruptedException, IOException;

    public final void b() throws IOException, InterruptedException {
        CacheUtil.CachingCounters cachingCounters;
        this.b.a(-1000);
        try {
            List<Segment> d2 = d();
            Collections.sort(d2);
            byte[] bArr = new byte[131072];
            cachingCounters = new CacheUtil.CachingCounters();
            for (int i2 = 0; i2 < d2.size(); i2++) {
                CacheUtil.a(d2.get(i2).b, this.c, this.d, bArr, this.b, -1000, cachingCounters, this.g, true);
                this.i++;
                this.j += cachingCounters.b;
            }
            this.b.d(-1000);
        } catch (Throwable th) {
            this.b.d(-1000);
            throw th;
        }
    }

    public final float c() {
        int i2 = this.h;
        int i3 = this.i;
        if (i2 == -1 || i3 == -1) {
            return -1.0f;
        }
        if (i2 == 0) {
            return 100.0f;
        }
        return (((float) i3) * 100.0f) / ((float) i2);
    }

    public void cancel() {
        this.g.set(true);
    }

    public final void remove() throws InterruptedException {
        try {
            List<Segment> a2 = a(this.e, a(this.e, this.f3389a), true);
            for (int i2 = 0; i2 < a2.size(); i2++) {
                a(a2.get(i2).b.f3586a);
            }
        } catch (IOException unused) {
        } catch (Throwable th) {
            a(this.f3389a);
            throw th;
        }
        a(this.f3389a);
    }

    private void a(Uri uri) {
        CacheUtil.a(this.c, CacheUtil.a(uri));
    }
}
