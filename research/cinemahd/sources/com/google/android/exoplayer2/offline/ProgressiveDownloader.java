package com.google.android.exoplayer2.offline;

import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ProgressiveDownloader implements Downloader {

    /* renamed from: a  reason: collision with root package name */
    private final DataSpec f3388a;
    private final Cache b;
    private final CacheDataSource c;
    private final PriorityTaskManager d;
    private final CacheUtil.CachingCounters e = new CacheUtil.CachingCounters();
    private final AtomicBoolean f = new AtomicBoolean();

    public ProgressiveDownloader(Uri uri, String str, DownloaderConstructorHelper downloaderConstructorHelper) {
        this.f3388a = new DataSpec(uri, 0, -1, str, 0);
        this.b = downloaderConstructorHelper.a();
        this.c = downloaderConstructorHelper.a(false);
        this.d = downloaderConstructorHelper.b();
    }

    public long a() {
        return this.e.a();
    }

    public void b() throws InterruptedException, IOException {
        this.d.a(-1000);
        try {
            CacheUtil.a(this.f3388a, this.b, this.c, new byte[131072], this.d, -1000, this.e, this.f, true);
        } finally {
            this.d.d(-1000);
        }
    }

    public float c() {
        long j = this.e.c;
        if (j == -1) {
            return -1.0f;
        }
        return (((float) this.e.a()) * 100.0f) / ((float) j);
    }

    public void cancel() {
        this.f.set(true);
    }

    public void remove() {
        CacheUtil.a(this.b, CacheUtil.a(this.f3388a));
    }
}
