package com.google.android.exoplayer2.offline;

import com.google.android.exoplayer2.offline.DownloadManager;

/* compiled from: lambda */
public final /* synthetic */ class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ DownloadManager.Task f3395a;
    private final /* synthetic */ Throwable b;

    public /* synthetic */ d(DownloadManager.Task task, Throwable th) {
        this.f3395a = task;
        this.b = th;
    }

    public final void run() {
        this.f3395a.a(this.b);
    }
}
