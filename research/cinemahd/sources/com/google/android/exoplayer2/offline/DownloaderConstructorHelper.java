package com.google.android.exoplayer2.offline;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.upstream.DataSink;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DummyDataSource;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.PriorityDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.PriorityTaskManager;

public final class DownloaderConstructorHelper {

    /* renamed from: a  reason: collision with root package name */
    private final Cache f3386a;
    private final DataSource.Factory b;
    private final DataSource.Factory c;
    private final DataSink.Factory d;
    private final PriorityTaskManager e;

    public DownloaderConstructorHelper(Cache cache, DataSource.Factory factory) {
        this(cache, factory, (DataSource.Factory) null, (DataSink.Factory) null, (PriorityTaskManager) null);
    }

    public Cache a() {
        return this.f3386a;
    }

    public PriorityTaskManager b() {
        PriorityTaskManager priorityTaskManager = this.e;
        return priorityTaskManager != null ? priorityTaskManager : new PriorityTaskManager();
    }

    public DownloaderConstructorHelper(Cache cache, DataSource.Factory factory, DataSource.Factory factory2, DataSink.Factory factory3, PriorityTaskManager priorityTaskManager) {
        Assertions.a(factory);
        this.f3386a = cache;
        this.b = factory;
        this.c = factory2;
        this.d = factory3;
        this.e = priorityTaskManager;
    }

    public CacheDataSource a(boolean z) {
        DataSource.Factory factory = this.c;
        DataSource a2 = factory != null ? factory.a() : new FileDataSource();
        if (z) {
            return new CacheDataSource(this.f3386a, DummyDataSource.f3593a, a2, (DataSink) null, 1, (CacheDataSource.EventListener) null);
        }
        DataSink.Factory factory2 = this.d;
        DataSink a3 = factory2 != null ? factory2.a() : new CacheDataSink(this.f3386a, PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE);
        DataSource a4 = this.b.a();
        PriorityTaskManager priorityTaskManager = this.e;
        return new CacheDataSource(this.f3386a, priorityTaskManager == null ? a4 : new PriorityDataSource(a4, priorityTaskManager, -1000), a2, a3, 1, (CacheDataSource.EventListener) null);
    }
}
