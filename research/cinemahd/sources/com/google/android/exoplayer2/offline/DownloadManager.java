package com.google.android.exoplayer2.offline;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

public final class DownloadManager {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final DownloaderConstructorHelper f3383a;
    private final int b;
    private final int c;
    private final ActionFile d;
    private final DownloadAction.Deserializer[] e;
    private final ArrayList<Task> f;
    private final ArrayList<Task> g;
    /* access modifiers changed from: private */
    public final Handler h;
    private final HandlerThread i;
    private final Handler j;
    private final CopyOnWriteArraySet<Listener> k;
    private int l;
    private boolean m;
    private boolean n;
    private boolean o;

    public interface Listener {
        void a(DownloadManager downloadManager);

        void a(DownloadManager downloadManager, TaskState taskState);

        void b(DownloadManager downloadManager);
    }

    private static final class Task implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final int f3384a;
        private final DownloadManager b;
        /* access modifiers changed from: private */
        public final DownloadAction c;
        private final int d;
        /* access modifiers changed from: private */
        public volatile int e;
        private volatile Downloader f;
        private Thread g;
        private Throwable h;

        /* access modifiers changed from: private */
        public boolean g() {
            return this.e == 0;
        }

        /* access modifiers changed from: private */
        public void h() {
            if (a(0, 5)) {
                this.b.h.post(new c(this));
            } else if (a(1, 6)) {
                i();
            }
        }

        private void i() {
            if (this.f != null) {
                this.f.cancel();
            }
            this.g.interrupt();
        }

        private int j() {
            int i = this.e;
            if (i == 5) {
                return 0;
            }
            if (i == 6 || i == 7) {
                return 1;
            }
            return this.e;
        }

        /* access modifiers changed from: private */
        public void k() {
            if (a(0, 1)) {
                this.g = new Thread(this);
                this.g.start();
            }
        }

        public /* synthetic */ void f() {
            a(5, 3);
        }

        public void run() {
            long j;
            int i;
            DownloadManager.b("Task is started", this);
            try {
                this.f = this.c.a(this.b.f3383a);
                if (this.c.d) {
                    this.f.remove();
                } else {
                    j = -1;
                    i = 0;
                    while (!Thread.interrupted()) {
                        this.f.b();
                    }
                }
                th = null;
            } catch (IOException e2) {
                long a2 = this.f.a();
                if (a2 != j) {
                    DownloadManager.b("Reset error count. downloadedBytes = " + a2, this);
                    j = a2;
                    i = 0;
                }
                if (this.e != 1 || (i = i + 1) > this.d) {
                    throw e2;
                }
                DownloadManager.b("Download error. Retry " + i, this);
                Thread.sleep((long) a(i));
            } catch (Throwable th) {
                th = th;
            }
            this.b.h.post(new d(this, th));
        }

        public String toString() {
            return super.toString();
        }

        private Task(int i, DownloadManager downloadManager, DownloadAction downloadAction, int i2) {
            this.f3384a = i;
            this.b = downloadManager;
            this.c = downloadAction;
            this.e = 0;
            this.d = i2;
        }

        public float a() {
            if (this.f != null) {
                return this.f.c();
            }
            return -1.0f;
        }

        public TaskState b() {
            return new TaskState(this.f3384a, this.c, j(), a(), c(), this.h);
        }

        public long c() {
            if (this.f != null) {
                return this.f.a();
            }
            return 0;
        }

        public boolean d() {
            return this.e == 5 || this.e == 1 || this.e == 7 || this.e == 6;
        }

        public boolean e() {
            return this.e == 4 || this.e == 2 || this.e == 3;
        }

        private boolean a(int i, int i2) {
            return a(i, i2, (Throwable) null);
        }

        private boolean a(int i, int i2, Throwable th) {
            boolean z = false;
            if (this.e != i) {
                return false;
            }
            this.e = i2;
            this.h = th;
            if (this.e != j()) {
                z = true;
            }
            if (!z) {
                this.b.b(this);
            }
            return true;
        }

        public /* synthetic */ void a(Throwable th) {
            if (!a(1, th != null ? 4 : 2, th) && !a(6, 3) && !a(7, 0)) {
                throw new IllegalStateException();
            }
        }

        private int a(int i) {
            return Math.min((i - 1) * 1000, 5000);
        }
    }

    public static final class TaskState {

        /* renamed from: a  reason: collision with root package name */
        public final DownloadAction f3385a;
        public final int b;

        private TaskState(int i, DownloadAction downloadAction, int i2, float f, long j, Throwable th) {
            this.f3385a = downloadAction;
            this.b = i2;
        }
    }

    public DownloadManager(DownloaderConstructorHelper downloaderConstructorHelper, int i2, int i3, File file, DownloadAction.Deserializer... deserializerArr) {
        this.f3383a = downloaderConstructorHelper;
        this.b = i2;
        this.c = i3;
        this.d = new ActionFile(file);
        this.e = deserializerArr.length <= 0 ? DownloadAction.b() : deserializerArr;
        this.o = true;
        this.f = new ArrayList<>();
        this.g = new ArrayList<>();
        Looper myLooper = Looper.myLooper();
        this.h = new Handler(myLooper == null ? Looper.getMainLooper() : myLooper);
        this.i = new HandlerThread("DownloadManager file i/o");
        this.i.start();
        this.j = new Handler(this.i.getLooper());
        this.k = new CopyOnWriteArraySet<>();
        c();
        a("Created");
    }

    private static void a(String str) {
    }

    private void c() {
        this.j.post(new a(this));
    }

    private void d() {
        if (a()) {
            a("Notify idle state");
            Iterator<Listener> it2 = this.k.iterator();
            while (it2.hasNext()) {
                it2.next().a(this);
            }
        }
    }

    private void e() {
        DownloadAction b2;
        boolean z;
        if (this.m && !this.n) {
            boolean z2 = this.o || this.g.size() == this.b;
            for (int i2 = 0; i2 < this.f.size(); i2++) {
                Task task = this.f.get(i2);
                if (task.g() && ((z = b2.d) || !z2)) {
                    int i3 = 0;
                    boolean z3 = true;
                    while (true) {
                        if (i3 >= i2) {
                            break;
                        }
                        Task task2 = this.f.get(i3);
                        if (task2.c.a((b2 = task.c))) {
                            if (z) {
                                a(task + " clashes with " + task2);
                                task2.h();
                                z3 = false;
                            } else if (task2.c.d) {
                                z2 = true;
                                z3 = false;
                                break;
                            }
                        }
                        i3++;
                    }
                    if (z3) {
                        task.k();
                        if (!z) {
                            this.g.add(task);
                            z2 = this.g.size() == this.b;
                        }
                    }
                }
            }
        }
    }

    private void f() {
        if (!this.n) {
            DownloadAction[] downloadActionArr = new DownloadAction[this.f.size()];
            for (int i2 = 0; i2 < this.f.size(); i2++) {
                downloadActionArr[i2] = this.f.get(i2).c;
            }
            this.j.post(new b(this, downloadActionArr));
        }
    }

    /* access modifiers changed from: private */
    public void b(Task task) {
        if (!this.n) {
            boolean z = !task.d();
            if (z) {
                this.g.remove(task);
            }
            a(task);
            if (task.e()) {
                this.f.remove(task);
                f();
            }
            if (z) {
                e();
                d();
            }
        }
    }

    public void a(Listener listener) {
        this.k.add(listener);
    }

    public boolean a() {
        Assertions.b(!this.n);
        if (!this.m) {
            return false;
        }
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            if (this.f.get(i2).d()) {
                return false;
            }
        }
        return true;
    }

    private Task a(DownloadAction downloadAction) {
        int i2 = this.l;
        this.l = i2 + 1;
        Task task = new Task(i2, this, downloadAction, this.c);
        this.f.add(task);
        b("Task is added", task);
        return task;
    }

    public /* synthetic */ void b() {
        DownloadAction[] downloadActionArr;
        try {
            downloadActionArr = this.d.a(this.e);
            a("Action file is loaded.");
        } catch (Throwable th) {
            Log.a("DownloadManager", "Action file loading failed.", th);
            downloadActionArr = new DownloadAction[0];
        }
        this.h.post(new e(this, downloadActionArr));
    }

    private void a(Task task) {
        b("Task state is changed", task);
        TaskState b2 = task.b();
        Iterator<Listener> it2 = this.k.iterator();
        while (it2.hasNext()) {
            it2.next().a(this, b2);
        }
    }

    public /* synthetic */ void b(DownloadAction[] downloadActionArr) {
        try {
            this.d.a(downloadActionArr);
            a("Actions persisted.");
        } catch (IOException e2) {
            Log.a("DownloadManager", "Persisting actions failed.", e2);
        }
    }

    public /* synthetic */ void a(DownloadAction[] downloadActionArr) {
        if (!this.n) {
            ArrayList arrayList = new ArrayList(this.f);
            this.f.clear();
            for (DownloadAction a2 : downloadActionArr) {
                a(a2);
            }
            a("Tasks are created.");
            this.m = true;
            Iterator<Listener> it2 = this.k.iterator();
            while (it2.hasNext()) {
                it2.next().b(this);
            }
            if (!arrayList.isEmpty()) {
                this.f.addAll(arrayList);
                f();
            }
            e();
            for (int i2 = 0; i2 < this.f.size(); i2++) {
                Task task = this.f.get(i2);
                if (task.e == 0) {
                    a(task);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, Task task) {
        a(str + ": " + task);
    }
}
