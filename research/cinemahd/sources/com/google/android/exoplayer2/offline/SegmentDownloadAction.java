package com.google.android.exoplayer2.offline;

import android.net.Uri;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.util.Assertions;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class SegmentDownloadAction extends DownloadAction {
    public final List<StreamKey> g;

    protected static abstract class SegmentDownloadActionDeserializer extends DownloadAction.Deserializer {
        public SegmentDownloadActionDeserializer(String str, int i) {
            super(str, i);
        }

        public final DownloadAction a(int i, DataInputStream dataInputStream) throws IOException {
            Uri parse = Uri.parse(dataInputStream.readUTF());
            boolean readBoolean = dataInputStream.readBoolean();
            byte[] bArr = new byte[dataInputStream.readInt()];
            dataInputStream.readFully(bArr);
            int readInt = dataInputStream.readInt();
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < readInt; i2++) {
                arrayList.add(b(i, dataInputStream));
            }
            return a(parse, readBoolean, bArr, arrayList);
        }

        /* access modifiers changed from: protected */
        public abstract DownloadAction a(Uri uri, boolean z, byte[] bArr, List<StreamKey> list);

        /* access modifiers changed from: protected */
        public StreamKey b(int i, DataInputStream dataInputStream) throws IOException {
            return new StreamKey(dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readInt());
        }
    }

    protected SegmentDownloadAction(String str, int i, Uri uri, boolean z, byte[] bArr, List<StreamKey> list) {
        super(str, i, uri, z, bArr);
        if (z) {
            Assertions.a(list.isEmpty());
            this.g = Collections.emptyList();
            return;
        }
        ArrayList arrayList = new ArrayList(list);
        Collections.sort(arrayList);
        this.g = Collections.unmodifiableList(arrayList);
    }

    public List<StreamKey> a() {
        return this.g;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        return this.g.equals(((SegmentDownloadAction) obj).g);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + this.g.hashCode();
    }

    public final void a(DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeUTF(this.c.toString());
        dataOutputStream.writeBoolean(this.d);
        dataOutputStream.writeInt(this.e.length);
        dataOutputStream.write(this.e);
        dataOutputStream.writeInt(this.g.size());
        for (int i = 0; i < this.g.size(); i++) {
            a(dataOutputStream, this.g.get(i));
        }
    }

    private void a(DataOutputStream dataOutputStream, StreamKey streamKey) throws IOException {
        dataOutputStream.writeInt(streamKey.f3391a);
        dataOutputStream.writeInt(streamKey.b);
        dataOutputStream.writeInt(streamKey.c);
    }
}
