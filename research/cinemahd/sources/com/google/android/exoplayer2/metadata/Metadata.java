package com.google.android.exoplayer2.metadata;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.List;

public final class Metadata implements Parcelable {
    public static final Parcelable.Creator<Metadata> CREATOR = new Parcelable.Creator<Metadata>() {
        public Metadata[] newArray(int i) {
            return new Metadata[0];
        }

        public Metadata createFromParcel(Parcel parcel) {
            return new Metadata(parcel);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Entry[] f3364a;

    public interface Entry extends Parcelable {
    }

    public Metadata(Entry... entryArr) {
        this.f3364a = entryArr == null ? new Entry[0] : entryArr;
    }

    public int a() {
        return this.f3364a.length;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Metadata.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.f3364a, ((Metadata) obj).f3364a);
    }

    public int hashCode() {
        return Arrays.hashCode(this.f3364a);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f3364a.length);
        for (Entry writeParcelable : this.f3364a) {
            parcel.writeParcelable(writeParcelable, 0);
        }
    }

    public Entry a(int i) {
        return this.f3364a[i];
    }

    public Metadata(List<? extends Entry> list) {
        if (list != null) {
            this.f3364a = new Entry[list.size()];
            list.toArray(this.f3364a);
            return;
        }
        this.f3364a = new Entry[0];
    }

    Metadata(Parcel parcel) {
        this.f3364a = new Entry[parcel.readInt()];
        int i = 0;
        while (true) {
            Entry[] entryArr = this.f3364a;
            if (i < entryArr.length) {
                entryArr[i] = (Entry) parcel.readParcelable(Entry.class.getClassLoader());
                i++;
            } else {
                return;
            }
        }
    }
}
