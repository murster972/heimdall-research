package com.google.android.exoplayer2.metadata.emsg;

import com.facebook.imageutils.JfifUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdRequest;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public final class EventMessageEncoder {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f3367a = new ByteArrayOutputStream(AdRequest.MAX_CONTENT_URL_LENGTH);
    private final DataOutputStream b = new DataOutputStream(this.f3367a);

    public byte[] a(EventMessage eventMessage, long j) {
        Assertions.a(j >= 0);
        this.f3367a.reset();
        try {
            a(this.b, eventMessage.f3366a);
            a(this.b, eventMessage.b != null ? eventMessage.b : "");
            a(this.b, j);
            a(this.b, Util.c(eventMessage.d, j, 1000000));
            a(this.b, Util.c(eventMessage.c, j, 1000));
            a(this.b, eventMessage.e);
            this.b.write(eventMessage.f);
            this.b.flush();
            return this.f3367a.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void a(DataOutputStream dataOutputStream, String str) throws IOException {
        dataOutputStream.writeBytes(str);
        dataOutputStream.writeByte(0);
    }

    private static void a(DataOutputStream dataOutputStream, long j) throws IOException {
        dataOutputStream.writeByte(((int) (j >>> 24)) & JfifUtil.MARKER_FIRST_BYTE);
        dataOutputStream.writeByte(((int) (j >>> 16)) & JfifUtil.MARKER_FIRST_BYTE);
        dataOutputStream.writeByte(((int) (j >>> 8)) & JfifUtil.MARKER_FIRST_BYTE);
        dataOutputStream.writeByte(((int) j) & JfifUtil.MARKER_FIRST_BYTE);
    }
}
