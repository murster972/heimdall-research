package com.google.android.exoplayer2.metadata;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.exoplayer2.BaseRenderer;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.FormatHolder;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;

public final class MetadataRenderer extends BaseRenderer implements Handler.Callback {
    private final MetadataDecoderFactory j;
    private final MetadataOutput k;
    private final Handler l;
    private final FormatHolder m;
    private final MetadataInputBuffer n;
    private final Metadata[] o;
    private final long[] p;
    private int q;
    private int r;
    private MetadataDecoder s;
    private boolean t;

    public MetadataRenderer(MetadataOutput metadataOutput, Looper looper) {
        this(metadataOutput, looper, MetadataDecoderFactory.f3365a);
    }

    private void b(Metadata metadata) {
        this.k.a(metadata);
    }

    private void s() {
        Arrays.fill(this.o, (Object) null);
        this.q = 0;
        this.r = 0;
    }

    public int a(Format format) {
        if (this.j.a(format)) {
            return BaseRenderer.a((DrmSessionManager<?>) null, format.j) ? 4 : 2;
        }
        return 0;
    }

    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            b((Metadata) message.obj);
            return true;
        }
        throw new IllegalStateException();
    }

    public boolean isReady() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void p() {
        s();
        this.s = null;
    }

    public MetadataRenderer(MetadataOutput metadataOutput, Looper looper, MetadataDecoderFactory metadataDecoderFactory) {
        super(4);
        Handler handler;
        Assertions.a(metadataOutput);
        this.k = metadataOutput;
        if (looper == null) {
            handler = null;
        } else {
            handler = Util.a(looper, (Handler.Callback) this);
        }
        this.l = handler;
        Assertions.a(metadataDecoderFactory);
        this.j = metadataDecoderFactory;
        this.m = new FormatHolder();
        this.n = new MetadataInputBuffer();
        this.o = new Metadata[5];
        this.p = new long[5];
    }

    /* access modifiers changed from: protected */
    public void a(Format[] formatArr, long j2) throws ExoPlaybackException {
        this.s = this.j.b(formatArr[0]);
    }

    /* access modifiers changed from: protected */
    public void a(long j2, boolean z) {
        s();
        this.t = false;
    }

    public void a(long j2, long j3) throws ExoPlaybackException {
        if (!this.t && this.r < 5) {
            this.n.clear();
            if (a(this.m, (DecoderInputBuffer) this.n, false) == -4) {
                if (this.n.isEndOfStream()) {
                    this.t = true;
                } else if (!this.n.isDecodeOnly()) {
                    MetadataInputBuffer metadataInputBuffer = this.n;
                    metadataInputBuffer.e = this.m.f3165a.k;
                    metadataInputBuffer.b();
                    int i = (this.q + this.r) % 5;
                    Metadata a2 = this.s.a(this.n);
                    if (a2 != null) {
                        this.o[i] = a2;
                        this.p[i] = this.n.c;
                        this.r++;
                    }
                }
            }
        }
        if (this.r > 0) {
            long[] jArr = this.p;
            int i2 = this.q;
            if (jArr[i2] <= j2) {
                a(this.o[i2]);
                Metadata[] metadataArr = this.o;
                int i3 = this.q;
                metadataArr[i3] = null;
                this.q = (i3 + 1) % 5;
                this.r--;
            }
        }
    }

    public boolean a() {
        return this.t;
    }

    private void a(Metadata metadata) {
        Handler handler = this.l;
        if (handler != null) {
            handler.obtainMessage(0, metadata).sendToTarget();
        } else {
            b(metadata);
        }
    }
}
