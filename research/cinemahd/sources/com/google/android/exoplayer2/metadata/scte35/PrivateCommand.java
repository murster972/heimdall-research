package com.google.android.exoplayer2.metadata.scte35;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.exoplayer2.util.ParsableByteArray;

public final class PrivateCommand extends SpliceCommand {
    public static final Parcelable.Creator<PrivateCommand> CREATOR = new Parcelable.Creator<PrivateCommand>() {
        public PrivateCommand createFromParcel(Parcel parcel) {
            return new PrivateCommand(parcel);
        }

        public PrivateCommand[] newArray(int i) {
            return new PrivateCommand[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final long f3372a;
    public final long b;
    public final byte[] c;

    static PrivateCommand a(ParsableByteArray parsableByteArray, int i, long j) {
        long v = parsableByteArray.v();
        byte[] bArr = new byte[(i - 4)];
        parsableByteArray.a(bArr, 0, bArr.length);
        return new PrivateCommand(v, bArr, j);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f3372a);
        parcel.writeLong(this.b);
        parcel.writeInt(this.c.length);
        parcel.writeByteArray(this.c);
    }

    private PrivateCommand(long j, byte[] bArr, long j2) {
        this.f3372a = j2;
        this.b = j;
        this.c = bArr;
    }

    private PrivateCommand(Parcel parcel) {
        this.f3372a = parcel.readLong();
        this.b = parcel.readLong();
        this.c = new byte[parcel.readInt()];
        parcel.readByteArray(this.c);
    }
}
