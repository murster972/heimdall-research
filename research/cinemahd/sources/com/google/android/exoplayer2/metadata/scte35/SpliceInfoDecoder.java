package com.google.android.exoplayer2.metadata.scte35;

import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataDecoder;
import com.google.android.exoplayer2.metadata.MetadataInputBuffer;
import com.google.android.exoplayer2.util.ParsableBitArray;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import java.nio.ByteBuffer;

public final class SpliceInfoDecoder implements MetadataDecoder {

    /* renamed from: a  reason: collision with root package name */
    private final ParsableByteArray f3373a = new ParsableByteArray();
    private final ParsableBitArray b = new ParsableBitArray();
    private TimestampAdjuster c;

    public Metadata a(MetadataInputBuffer metadataInputBuffer) {
        TimestampAdjuster timestampAdjuster = this.c;
        if (timestampAdjuster == null || metadataInputBuffer.e != timestampAdjuster.c()) {
            this.c = new TimestampAdjuster(metadataInputBuffer.c);
            this.c.a(metadataInputBuffer.c - metadataInputBuffer.e);
        }
        ByteBuffer byteBuffer = metadataInputBuffer.b;
        byte[] array = byteBuffer.array();
        int limit = byteBuffer.limit();
        this.f3373a.a(array, limit);
        this.b.a(array, limit);
        this.b.c(39);
        long a2 = (((long) this.b.a(1)) << 32) | ((long) this.b.a(32));
        this.b.c(20);
        int a3 = this.b.a(12);
        int a4 = this.b.a(8);
        Metadata.Entry entry = null;
        this.f3373a.f(14);
        if (a4 == 0) {
            entry = new SpliceNullCommand();
        } else if (a4 == 255) {
            entry = PrivateCommand.a(this.f3373a, a3, a2);
        } else if (a4 == 4) {
            entry = SpliceScheduleCommand.a(this.f3373a);
        } else if (a4 == 5) {
            entry = SpliceInsertCommand.a(this.f3373a, a2, this.c);
        } else if (a4 == 6) {
            entry = TimeSignalCommand.a(this.f3373a, a2, this.c);
        }
        if (entry == null) {
            return new Metadata(new Metadata.Entry[0]);
        }
        return new Metadata(entry);
    }
}
