package com.google.android.exoplayer2.metadata.emsg;

import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataDecoder;
import com.google.android.exoplayer2.metadata.MetadataInputBuffer;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import java.nio.ByteBuffer;
import java.util.Arrays;

public final class EventMessageDecoder implements MetadataDecoder {
    public Metadata a(MetadataInputBuffer metadataInputBuffer) {
        ByteBuffer byteBuffer = metadataInputBuffer.b;
        byte[] array = byteBuffer.array();
        int limit = byteBuffer.limit();
        ParsableByteArray parsableByteArray = new ParsableByteArray(array, limit);
        String q = parsableByteArray.q();
        Assertions.a(q);
        String q2 = parsableByteArray.q();
        Assertions.a(q2);
        long v = parsableByteArray.v();
        long c = Util.c(parsableByteArray.v(), 1000000, v);
        return new Metadata(new EventMessage(q, q2, Util.c(parsableByteArray.v(), 1000, v), parsableByteArray.v(), Arrays.copyOfRange(array, parsableByteArray.c(), limit), c));
    }
}
