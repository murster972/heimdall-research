package com.google.android.exoplayer2.metadata.emsg;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;

public final class EventMessage implements Metadata.Entry {
    public static final Parcelable.Creator<EventMessage> CREATOR = new Parcelable.Creator<EventMessage>() {
        public EventMessage createFromParcel(Parcel parcel) {
            return new EventMessage(parcel);
        }

        public EventMessage[] newArray(int i) {
            return new EventMessage[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final String f3366a;
    public final String b;
    public final long c;
    public final long d;
    public final long e;
    public final byte[] f;
    private int g;

    public EventMessage(String str, String str2, long j, long j2, byte[] bArr, long j3) {
        this.f3366a = str;
        this.b = str2;
        this.c = j;
        this.e = j2;
        this.f = bArr;
        this.d = j3;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || EventMessage.class != obj.getClass()) {
            return false;
        }
        EventMessage eventMessage = (EventMessage) obj;
        if (this.d != eventMessage.d || this.c != eventMessage.c || this.e != eventMessage.e || !Util.a((Object) this.f3366a, (Object) eventMessage.f3366a) || !Util.a((Object) this.b, (Object) eventMessage.b) || !Arrays.equals(this.f, eventMessage.f)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.g == 0) {
            String str = this.f3366a;
            int i = 0;
            int hashCode = (527 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            long j = this.d;
            long j2 = this.c;
            long j3 = this.e;
            this.g = ((((((((hashCode + i) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + Arrays.hashCode(this.f);
        }
        return this.g;
    }

    public String toString() {
        return "EMSG: scheme=" + this.f3366a + ", id=" + this.e + ", value=" + this.b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3366a);
        parcel.writeString(this.b);
        parcel.writeLong(this.d);
        parcel.writeLong(this.c);
        parcel.writeLong(this.e);
        parcel.writeByteArray(this.f);
    }

    EventMessage(Parcel parcel) {
        String readString = parcel.readString();
        Util.a(readString);
        this.f3366a = readString;
        String readString2 = parcel.readString();
        Util.a(readString2);
        this.b = readString2;
        this.d = parcel.readLong();
        this.c = parcel.readLong();
        this.e = parcel.readLong();
        byte[] createByteArray = parcel.createByteArray();
        Util.a(createByteArray);
        this.f = createByteArray;
    }
}
