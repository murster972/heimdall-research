package com.google.android.exoplayer2.metadata.scte35;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.TimestampAdjuster;

public final class TimeSignalCommand extends SpliceCommand {
    public static final Parcelable.Creator<TimeSignalCommand> CREATOR = new Parcelable.Creator<TimeSignalCommand>() {
        public TimeSignalCommand createFromParcel(Parcel parcel) {
            return new TimeSignalCommand(parcel.readLong(), parcel.readLong());
        }

        public TimeSignalCommand[] newArray(int i) {
            return new TimeSignalCommand[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final long f3379a;
    public final long b;

    static TimeSignalCommand a(ParsableByteArray parsableByteArray, long j, TimestampAdjuster timestampAdjuster) {
        long a2 = a(parsableByteArray, j);
        return new TimeSignalCommand(a2, timestampAdjuster.b(a2));
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f3379a);
        parcel.writeLong(this.b);
    }

    private TimeSignalCommand(long j, long j2) {
        this.f3379a = j;
        this.b = j2;
    }

    static long a(ParsableByteArray parsableByteArray, long j) {
        long t = (long) parsableByteArray.t();
        if ((128 & t) != 0) {
            return 8589934591L & ((((t & 1) << 32) | parsableByteArray.v()) + j);
        }
        return -9223372036854775807L;
    }
}
