package com.google.android.exoplayer2.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArraySet;

public class DefaultTimeBar extends View implements TimeBar {
    private int[] A;
    private Point B;
    private boolean C;
    private long D;
    private long E;
    private long F;
    private long G;
    private int H;
    private long[] I;
    private boolean[] J;

    /* renamed from: a  reason: collision with root package name */
    private final Rect f3569a = new Rect();
    private final Rect b = new Rect();
    private final Rect c = new Rect();
    private final Rect d = new Rect();
    private final Paint e = new Paint();
    private final Paint f = new Paint();
    private final Paint g = new Paint();
    private final Paint h = new Paint();
    private final Paint i = new Paint();
    private final Paint j = new Paint();
    private final Drawable k;
    private final int l;
    private final int m;
    private final int n;
    private final int o;
    private final int p;
    private final int q;
    private final int r;
    private final int s;
    private final StringBuilder t;
    private final Formatter u;
    private final Runnable v;
    private final CopyOnWriteArraySet<TimeBar.OnScrubListener> w;
    private int x;
    private long y;
    private int z;

    public DefaultTimeBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.j.setAntiAlias(true);
        this.w = new CopyOnWriteArraySet<>();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.s = a(displayMetrics, -50);
        int a2 = a(displayMetrics, 4);
        int a3 = a(displayMetrics, 26);
        int a4 = a(displayMetrics, 4);
        int a5 = a(displayMetrics, 12);
        int a6 = a(displayMetrics, 0);
        int a7 = a(displayMetrics, 16);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R$styleable.DefaultTimeBar, 0, 0);
            try {
                this.k = obtainStyledAttributes.getDrawable(R$styleable.DefaultTimeBar_scrubber_drawable);
                if (this.k != null) {
                    a(this.k);
                    a3 = Math.max(this.k.getMinimumHeight(), a3);
                }
                this.l = obtainStyledAttributes.getDimensionPixelSize(R$styleable.DefaultTimeBar_bar_height, a2);
                this.m = obtainStyledAttributes.getDimensionPixelSize(R$styleable.DefaultTimeBar_touch_target_height, a3);
                this.n = obtainStyledAttributes.getDimensionPixelSize(R$styleable.DefaultTimeBar_ad_marker_width, a4);
                this.o = obtainStyledAttributes.getDimensionPixelSize(R$styleable.DefaultTimeBar_scrubber_enabled_size, a5);
                this.p = obtainStyledAttributes.getDimensionPixelSize(R$styleable.DefaultTimeBar_scrubber_disabled_size, a6);
                this.q = obtainStyledAttributes.getDimensionPixelSize(R$styleable.DefaultTimeBar_scrubber_dragged_size, a7);
                int i2 = obtainStyledAttributes.getInt(R$styleable.DefaultTimeBar_played_color, -1);
                int i3 = obtainStyledAttributes.getInt(R$styleable.DefaultTimeBar_scrubber_color, c(i2));
                int i4 = obtainStyledAttributes.getInt(R$styleable.DefaultTimeBar_buffered_color, a(i2));
                int i5 = obtainStyledAttributes.getInt(R$styleable.DefaultTimeBar_unplayed_color, d(i2));
                int i6 = obtainStyledAttributes.getInt(R$styleable.DefaultTimeBar_ad_marker_color, -1291845888);
                int i7 = obtainStyledAttributes.getInt(R$styleable.DefaultTimeBar_played_ad_marker_color, b(i6));
                this.e.setColor(i2);
                this.j.setColor(i3);
                this.f.setColor(i4);
                this.g.setColor(i5);
                this.h.setColor(i6);
                this.i.setColor(i7);
            } finally {
                obtainStyledAttributes.recycle();
            }
        } else {
            this.l = a2;
            this.m = a3;
            this.n = a4;
            this.o = a5;
            this.p = a6;
            this.q = a7;
            this.e.setColor(-1);
            this.j.setColor(c(-1));
            this.f.setColor(a(-1));
            this.g.setColor(d(-1));
            this.h.setColor(-1291845888);
            this.k = null;
        }
        this.t = new StringBuilder();
        this.u = new Formatter(this.t, Locale.getDefault());
        this.v = new Runnable() {
            public void run() {
                DefaultTimeBar.this.a(false);
            }
        };
        Drawable drawable = this.k;
        if (drawable != null) {
            this.r = (drawable.getMinimumWidth() + 1) / 2;
        } else {
            this.r = (Math.max(this.p, Math.max(this.o, this.q)) + 1) / 2;
        }
        this.E = -9223372036854775807L;
        this.y = -9223372036854775807L;
        this.x = 20;
        setFocusable(true);
        if (Util.f3651a >= 16) {
            a();
        }
    }

    public static int a(int i2) {
        return (i2 & 16777215) | -872415232;
    }

    public static int b(int i2) {
        return (i2 & 16777215) | 855638016;
    }

    private void b() {
        this.C = true;
        setPressed(true);
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        Iterator<TimeBar.OnScrubListener> it2 = this.w.iterator();
        while (it2.hasNext()) {
            it2.next().b(this, getScrubberPosition());
        }
    }

    public static int c(int i2) {
        return i2 | -16777216;
    }

    private void c() {
        this.c.set(this.b);
        this.d.set(this.b);
        long j2 = this.C ? this.D : this.F;
        if (this.E > 0) {
            int width = (int) ((((long) this.b.width()) * this.G) / this.E);
            Rect rect = this.c;
            Rect rect2 = this.b;
            rect.right = Math.min(rect2.left + width, rect2.right);
            int width2 = (int) ((((long) this.b.width()) * j2) / this.E);
            Rect rect3 = this.d;
            Rect rect4 = this.b;
            rect3.right = Math.min(rect4.left + width2, rect4.right);
        } else {
            Rect rect5 = this.c;
            int i2 = this.b.left;
            rect5.right = i2;
            this.d.right = i2;
        }
        invalidate(this.f3569a);
    }

    public static int d(int i2) {
        return (i2 & 16777215) | 855638016;
    }

    private void d() {
        Drawable drawable = this.k;
        if (drawable != null && drawable.isStateful() && this.k.setState(getDrawableState())) {
            invalidate();
        }
    }

    private long getPositionIncrement() {
        long j2 = this.y;
        if (j2 != -9223372036854775807L) {
            return j2;
        }
        long j3 = this.E;
        if (j3 == -9223372036854775807L) {
            return 0;
        }
        return j3 / ((long) this.x);
    }

    private String getProgressText() {
        return Util.a(this.t, this.u, this.F);
    }

    private long getScrubberPosition() {
        if (this.b.width() <= 0 || this.E == -9223372036854775807L) {
            return 0;
        }
        return (((long) this.d.width()) * this.E) / ((long) this.b.width());
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        d();
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.k;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    public void onDraw(Canvas canvas) {
        canvas.save();
        b(canvas);
        a(canvas);
        canvas.restore();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (accessibilityEvent.getEventType() == 4) {
            accessibilityEvent.getText().add(getProgressText());
        }
        accessibilityEvent.setClassName(DefaultTimeBar.class.getName());
    }

    @TargetApi(21)
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(DefaultTimeBar.class.getCanonicalName());
        accessibilityNodeInfo.setContentDescription(getProgressText());
        if (this.E > 0) {
            int i2 = Util.f3651a;
            if (i2 >= 21) {
                accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
                accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
            } else if (i2 >= 16) {
                accessibilityNodeInfo.addAction(4096);
                accessibilityNodeInfo.addAction(8192);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        if (a(r0) == false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        removeCallbacks(r4.v);
        postDelayed(r4.v, 1000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0026, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onKeyDown(int r5, android.view.KeyEvent r6) {
        /*
            r4 = this;
            boolean r0 = r4.isEnabled()
            if (r0 == 0) goto L_0x0036
            long r0 = r4.getPositionIncrement()
            r2 = 66
            r3 = 1
            if (r5 == r2) goto L_0x0027
            switch(r5) {
                case 21: goto L_0x0013;
                case 22: goto L_0x0014;
                case 23: goto L_0x0027;
                default: goto L_0x0012;
            }
        L_0x0012:
            goto L_0x0036
        L_0x0013:
            long r0 = -r0
        L_0x0014:
            boolean r0 = r4.a((long) r0)
            if (r0 == 0) goto L_0x0036
            java.lang.Runnable r5 = r4.v
            r4.removeCallbacks(r5)
            java.lang.Runnable r5 = r4.v
            r0 = 1000(0x3e8, double:4.94E-321)
            r4.postDelayed(r5, r0)
            return r3
        L_0x0027:
            boolean r0 = r4.C
            if (r0 == 0) goto L_0x0036
            java.lang.Runnable r5 = r4.v
            r4.removeCallbacks(r5)
            java.lang.Runnable r5 = r4.v
            r5.run()
            return r3
        L_0x0036:
            boolean r5 = super.onKeyDown(r5, r6)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.DefaultTimeBar.onKeyDown(int, android.view.KeyEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6 = ((i5 - i3) - this.m) / 2;
        int paddingLeft = getPaddingLeft();
        int paddingRight = (i4 - i2) - getPaddingRight();
        int i7 = this.m;
        int i8 = ((i7 - this.l) / 2) + i6;
        this.f3569a.set(paddingLeft, i6, paddingRight, i7 + i6);
        Rect rect = this.b;
        Rect rect2 = this.f3569a;
        int i9 = rect2.left;
        int i10 = this.r;
        rect.set(i9 + i10, i8, rect2.right - i10, this.l + i8);
        c();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode == 0) {
            size = this.m;
        } else if (mode != 1073741824) {
            size = Math.min(this.m, size);
        }
        setMeasuredDimension(View.MeasureSpec.getSize(i2), size);
        d();
    }

    public void onRtlPropertiesChanged(int i2) {
        Drawable drawable = this.k;
        if (drawable != null && a(drawable, i2)) {
            invalidate();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        if (r3 != 3) goto L_0x0090;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            boolean r0 = r7.isEnabled()
            r1 = 0
            if (r0 == 0) goto L_0x0090
            long r2 = r7.E
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 > 0) goto L_0x0011
            goto L_0x0090
        L_0x0011:
            android.graphics.Point r0 = r7.a((android.view.MotionEvent) r8)
            int r2 = r0.x
            int r0 = r0.y
            int r3 = r8.getAction()
            r4 = 1
            if (r3 == 0) goto L_0x0075
            r5 = 3
            if (r3 == r4) goto L_0x0066
            r6 = 2
            if (r3 == r6) goto L_0x0029
            if (r3 == r5) goto L_0x0066
            goto L_0x0090
        L_0x0029:
            boolean r8 = r7.C
            if (r8 == 0) goto L_0x0090
            int r8 = r7.s
            if (r0 >= r8) goto L_0x003b
            int r8 = r7.z
            int r2 = r2 - r8
            int r2 = r2 / r5
            int r8 = r8 + r2
            float r8 = (float) r8
            r7.a((float) r8)
            goto L_0x0041
        L_0x003b:
            r7.z = r2
            float r8 = (float) r2
            r7.a((float) r8)
        L_0x0041:
            long r0 = r7.getScrubberPosition()
            r7.D = r0
            java.util.concurrent.CopyOnWriteArraySet<com.google.android.exoplayer2.ui.TimeBar$OnScrubListener> r8 = r7.w
            java.util.Iterator r8 = r8.iterator()
        L_0x004d:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x005f
            java.lang.Object r0 = r8.next()
            com.google.android.exoplayer2.ui.TimeBar$OnScrubListener r0 = (com.google.android.exoplayer2.ui.TimeBar.OnScrubListener) r0
            long r1 = r7.D
            r0.a(r7, r1)
            goto L_0x004d
        L_0x005f:
            r7.c()
            r7.invalidate()
            return r4
        L_0x0066:
            boolean r0 = r7.C
            if (r0 == 0) goto L_0x0090
            int r8 = r8.getAction()
            if (r8 != r5) goto L_0x0071
            r1 = 1
        L_0x0071:
            r7.a((boolean) r1)
            return r4
        L_0x0075:
            float r8 = (float) r2
            float r0 = (float) r0
            boolean r0 = r7.a((float) r8, (float) r0)
            if (r0 == 0) goto L_0x0090
            r7.b()
            r7.a((float) r8)
            long r0 = r7.getScrubberPosition()
            r7.D = r0
            r7.c()
            r7.invalidate()
            return r4
        L_0x0090:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.DefaultTimeBar.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @TargetApi(16)
    public boolean performAccessibilityAction(int i2, Bundle bundle) {
        if (super.performAccessibilityAction(i2, bundle)) {
            return true;
        }
        if (this.E <= 0) {
            return false;
        }
        if (i2 == 8192) {
            if (a(-getPositionIncrement())) {
                a(false);
            }
        } else if (i2 != 4096) {
            return false;
        } else {
            if (a(getPositionIncrement())) {
                a(false);
            }
        }
        sendAccessibilityEvent(4);
        return true;
    }

    public void setBufferedPosition(long j2) {
        this.G = j2;
        c();
    }

    public void setDuration(long j2) {
        this.E = j2;
        if (this.C && j2 == -9223372036854775807L) {
            a(true);
        }
        c();
    }

    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (this.C && !z2) {
            a(true);
        }
    }

    public void setKeyCountIncrement(int i2) {
        Assertions.a(i2 > 0);
        this.x = i2;
        this.y = -9223372036854775807L;
    }

    public void setKeyTimeIncrement(long j2) {
        Assertions.a(j2 > 0);
        this.x = -1;
        this.y = j2;
    }

    public void setPosition(long j2) {
        this.F = j2;
        setContentDescription(getProgressText());
        c();
    }

    public void a(TimeBar.OnScrubListener onScrubListener) {
        this.w.add(onScrubListener);
    }

    public void a(long[] jArr, boolean[] zArr, int i2) {
        Assertions.a(i2 == 0 || !(jArr == null || zArr == null));
        this.H = i2;
        this.I = jArr;
        this.J = zArr;
        c();
    }

    private void b(Canvas canvas) {
        int height = this.b.height();
        int centerY = this.b.centerY() - (height / 2);
        int i2 = height + centerY;
        if (this.E <= 0) {
            Rect rect = this.b;
            canvas.drawRect((float) rect.left, (float) centerY, (float) rect.right, (float) i2, this.g);
            return;
        }
        Rect rect2 = this.c;
        int i3 = rect2.left;
        int i4 = rect2.right;
        int max = Math.max(Math.max(this.b.left, i4), this.d.right);
        int i5 = this.b.right;
        if (max < i5) {
            canvas.drawRect((float) max, (float) centerY, (float) i5, (float) i2, this.g);
        }
        int max2 = Math.max(i3, this.d.right);
        if (i4 > max2) {
            canvas.drawRect((float) max2, (float) centerY, (float) i4, (float) i2, this.f);
        }
        if (this.d.width() > 0) {
            Rect rect3 = this.d;
            canvas.drawRect((float) rect3.left, (float) centerY, (float) rect3.right, (float) i2, this.e);
        }
        int i6 = this.n / 2;
        for (int i7 = 0; i7 < this.H; i7++) {
            long b2 = Util.b(this.I[i7], 0, this.E);
            Rect rect4 = this.b;
            int min = rect4.left + Math.min(rect4.width() - this.n, Math.max(0, ((int) ((((long) this.b.width()) * b2) / this.E)) - i6));
            canvas.drawRect((float) min, (float) centerY, (float) (min + this.n), (float) i2, this.J[i7] ? this.i : this.h);
        }
    }

    @TargetApi(16)
    private void a() {
        if (getImportantForAccessibility() == 0) {
            setImportantForAccessibility(1);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        this.C = false;
        setPressed(false);
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(false);
        }
        invalidate();
        Iterator<TimeBar.OnScrubListener> it2 = this.w.iterator();
        while (it2.hasNext()) {
            it2.next().a(this, getScrubberPosition(), z2);
        }
    }

    private void a(float f2) {
        Rect rect = this.d;
        Rect rect2 = this.b;
        rect.right = Util.a((int) f2, rect2.left, rect2.right);
    }

    private Point a(MotionEvent motionEvent) {
        if (this.A == null) {
            this.A = new int[2];
            this.B = new Point();
        }
        getLocationOnScreen(this.A);
        this.B.set(((int) motionEvent.getRawX()) - this.A[0], ((int) motionEvent.getRawY()) - this.A[1]);
        return this.B;
    }

    private boolean a(float f2, float f3) {
        return this.f3569a.contains((int) f2, (int) f3);
    }

    private void a(Canvas canvas) {
        int i2;
        if (this.E > 0) {
            Rect rect = this.d;
            int a2 = Util.a(rect.right, rect.left, this.b.right);
            int centerY = this.d.centerY();
            Drawable drawable = this.k;
            if (drawable == null) {
                if (this.C || isFocused()) {
                    i2 = this.q;
                } else {
                    i2 = isEnabled() ? this.o : this.p;
                }
                canvas.drawCircle((float) a2, (float) centerY, (float) (i2 / 2), this.j);
                return;
            }
            int intrinsicWidth = drawable.getIntrinsicWidth() / 2;
            int intrinsicHeight = this.k.getIntrinsicHeight() / 2;
            this.k.setBounds(a2 - intrinsicWidth, centerY - intrinsicHeight, a2 + intrinsicWidth, centerY + intrinsicHeight);
            this.k.draw(canvas);
        }
    }

    private boolean a(long j2) {
        if (this.E <= 0) {
            return false;
        }
        long scrubberPosition = getScrubberPosition();
        this.D = Util.b(scrubberPosition + j2, 0, this.E);
        if (this.D == scrubberPosition) {
            return false;
        }
        if (!this.C) {
            b();
        }
        Iterator<TimeBar.OnScrubListener> it2 = this.w.iterator();
        while (it2.hasNext()) {
            it2.next().a(this, this.D);
        }
        c();
        return true;
    }

    private boolean a(Drawable drawable) {
        return Util.f3651a >= 23 && a(drawable, getLayoutDirection());
    }

    private static boolean a(Drawable drawable, int i2) {
        return Util.f3651a >= 23 && drawable.setLayoutDirection(i2);
    }

    private static int a(DisplayMetrics displayMetrics, int i2) {
        return (int) ((((float) i2) * displayMetrics.density) + 0.5f);
    }
}
