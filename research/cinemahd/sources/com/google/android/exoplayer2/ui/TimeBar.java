package com.google.android.exoplayer2.ui;

public interface TimeBar {

    public interface OnScrubListener {
        void a(TimeBar timeBar, long j);

        void a(TimeBar timeBar, long j, boolean z);

        void b(TimeBar timeBar, long j);
    }

    void a(OnScrubListener onScrubListener);

    void a(long[] jArr, boolean[] zArr, int i);

    void setBufferedPosition(long j);

    void setDuration(long j);

    void setEnabled(boolean z);

    void setPosition(long j);
}
