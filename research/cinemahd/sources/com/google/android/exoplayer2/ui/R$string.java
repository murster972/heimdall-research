package com.google.android.exoplayer2.ui;

public final class R$string {
    public static final int exo_controls_fastforward_description = 2131755277;
    public static final int exo_controls_fullscreen_description = 2131755278;
    public static final int exo_controls_next_description = 2131755279;
    public static final int exo_controls_pause_description = 2131755280;
    public static final int exo_controls_play_description = 2131755281;
    public static final int exo_controls_previous_description = 2131755282;
    public static final int exo_controls_repeat_all_description = 2131755283;
    public static final int exo_controls_repeat_off_description = 2131755284;
    public static final int exo_controls_repeat_one_description = 2131755285;
    public static final int exo_controls_rewind_description = 2131755286;
    public static final int exo_controls_scale_description = 2131755287;
    public static final int exo_controls_shuffle_description = 2131755288;
    public static final int exo_controls_stop_description = 2131755289;
    public static final int exo_controls_subtitle_description = 2131755290;
    public static final int exo_download_completed = 2131755291;
    public static final int exo_download_description = 2131755292;
    public static final int exo_download_downloading = 2131755293;
    public static final int exo_download_failed = 2131755294;
    public static final int exo_download_notification_channel_name = 2131755295;
    public static final int exo_download_removing = 2131755296;
    public static final int exo_download_waiting = 2131755297;
    public static final int exo_item_list = 2131755298;
    public static final int exo_track_bitrate = 2131755299;
    public static final int exo_track_mono = 2131755300;
    public static final int exo_track_resolution = 2131755301;
    public static final int exo_track_selection_auto = 2131755302;
    public static final int exo_track_selection_none = 2131755303;
    public static final int exo_track_selection_title_audio = 2131755304;
    public static final int exo_track_selection_title_text = 2131755305;
    public static final int exo_track_selection_title_video = 2131755306;
    public static final int exo_track_stereo = 2131755307;
    public static final int exo_track_surround = 2131755308;
    public static final int exo_track_surround_5_point_1 = 2131755309;
    public static final int exo_track_surround_7_point_1 = 2131755310;
    public static final int exo_track_unknown = 2131755311;
    public static final int status_bar_notification_info_overflow = 2131755551;

    private R$string() {
    }
}
