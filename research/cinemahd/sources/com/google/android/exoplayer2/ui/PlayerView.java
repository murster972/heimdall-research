package com.google.android.exoplayer2.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.ApicFrame;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.h;
import java.util.List;

public class PlayerView extends FrameLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final AspectRatioFrameLayout f3576a;
    /* access modifiers changed from: private */
    public final View b;
    /* access modifiers changed from: private */
    public final View c;
    private final ImageView d;
    /* access modifiers changed from: private */
    public final SubtitleView e;
    private final View f;
    private final TextView g;
    private final PlayerControlView h;
    private final ComponentListener i;
    private final FrameLayout j;
    private Player k;
    private boolean l;
    private boolean m;
    private Bitmap n;
    private boolean o;
    private ErrorMessageProvider<? super ExoPlaybackException> p;
    private CharSequence q;
    private int r;
    private boolean s;
    /* access modifiers changed from: private */
    public boolean t;
    private boolean u;
    /* access modifiers changed from: private */
    public int v;

    private final class ComponentListener extends Player.DefaultEventListener implements TextOutput, VideoListener, View.OnLayoutChangeListener {
        private ComponentListener() {
        }

        public /* synthetic */ void a(int i, int i2) {
            h.a(this, i, i2);
        }

        public void a(List<Cue> list) {
            if (PlayerView.this.e != null) {
                PlayerView.this.e.a(list);
            }
        }

        public void b(int i) {
            if (PlayerView.this.f() && PlayerView.this.t) {
                PlayerView.this.c();
            }
        }

        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            PlayerView.b((TextureView) view, PlayerView.this.v);
        }

        public void onPlayerStateChanged(boolean z, int i) {
            PlayerView.this.h();
            PlayerView.this.i();
            if (!PlayerView.this.f() || !PlayerView.this.t) {
                PlayerView.this.a(false);
            } else {
                PlayerView.this.c();
            }
        }

        public void onRenderedFirstFrame() {
            if (PlayerView.this.b != null) {
                PlayerView.this.b.setVisibility(4);
            }
        }

        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            PlayerView.this.j();
        }

        public void onVideoSizeChanged(int i, int i2, int i3, float f) {
            if (PlayerView.this.f3576a != null) {
                float f2 = (i2 == 0 || i == 0) ? 1.0f : (((float) i) * f) / ((float) i2);
                if (PlayerView.this.c instanceof TextureView) {
                    if (i3 == 90 || i3 == 270) {
                        f2 = 1.0f / f2;
                    }
                    if (PlayerView.this.v != 0) {
                        PlayerView.this.c.removeOnLayoutChangeListener(this);
                    }
                    int unused = PlayerView.this.v = i3;
                    if (PlayerView.this.v != 0) {
                        PlayerView.this.c.addOnLayoutChangeListener(this);
                    }
                    PlayerView.b((TextureView) PlayerView.this.c, PlayerView.this.v);
                }
                PlayerView.this.f3576a.setAspectRatio(f2);
            }
        }
    }

    public PlayerView(Context context) {
        this(context, (AttributeSet) null);
    }

    @SuppressLint({"InlinedApi"})
    private boolean b(int i2) {
        return i2 == 19 || i2 == 270 || i2 == 22 || i2 == 271 || i2 == 20 || i2 == 269 || i2 == 21 || i2 == 268 || i2 == 23;
    }

    public void a() {
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        Player player = this.k;
        if (player == null || !player.c()) {
            boolean z = b(keyEvent.getKeyCode()) && this.l && !this.h.b();
            a(true);
            if (z || a(keyEvent) || super.dispatchKeyEvent(keyEvent)) {
                return true;
            }
            return false;
        }
        this.j.requestFocus();
        return super.dispatchKeyEvent(keyEvent);
    }

    public PlayerControlView getController() {
        return this.h;
    }

    public boolean getControllerAutoShow() {
        return this.s;
    }

    public boolean getControllerHideOnTouch() {
        return this.u;
    }

    public int getControllerShowTimeoutMs() {
        return this.r;
    }

    public Bitmap getDefaultArtwork() {
        return this.n;
    }

    public FrameLayout getOverlayFrameLayout() {
        return this.j;
    }

    public Player getPlayer() {
        return this.k;
    }

    public int getResizeMode() {
        Assertions.b(this.f3576a != null);
        return this.f3576a.getResizeMode();
    }

    public SubtitleView getSubtitleView() {
        return this.e;
    }

    public boolean getUseArtwork() {
        return this.m;
    }

    public boolean getUseController() {
        return this.l;
    }

    public View getVideoSurfaceView() {
        return this.c;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.l || this.k == null || motionEvent.getActionMasked() != 0) {
            return false;
        }
        if (!this.h.b()) {
            a(true);
        } else if (this.u) {
            this.h.a();
        }
        return true;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!this.l || this.k == null) {
            return false;
        }
        a(true);
        return true;
    }

    public void setAspectRatioListener(AspectRatioFrameLayout.AspectRatioListener aspectRatioListener) {
        Assertions.b(this.f3576a != null);
        this.f3576a.setAspectRatioListener(aspectRatioListener);
    }

    public void setControlDispatcher(ControlDispatcher controlDispatcher) {
        Assertions.b(this.h != null);
        this.h.setControlDispatcher(controlDispatcher);
    }

    public void setControllerAutoShow(boolean z) {
        this.s = z;
    }

    public void setControllerHideDuringAds(boolean z) {
        this.t = z;
    }

    public void setControllerHideOnTouch(boolean z) {
        Assertions.b(this.h != null);
        this.u = z;
    }

    public void setControllerShowTimeoutMs(int i2) {
        Assertions.b(this.h != null);
        this.r = i2;
        if (this.h.b()) {
            d();
        }
    }

    public void setControllerVisibilityListener(PlayerControlView.VisibilityListener visibilityListener) {
        Assertions.b(this.h != null);
        this.h.setVisibilityListener(visibilityListener);
    }

    public void setCustomErrorMessage(CharSequence charSequence) {
        Assertions.b(this.g != null);
        this.q = charSequence;
        i();
    }

    public void setDefaultArtwork(Bitmap bitmap) {
        if (this.n != bitmap) {
            this.n = bitmap;
            j();
        }
    }

    public void setErrorMessageProvider(ErrorMessageProvider<? super ExoPlaybackException> errorMessageProvider) {
        if (this.p != errorMessageProvider) {
            this.p = errorMessageProvider;
            i();
        }
    }

    public void setFastForwardIncrementMs(int i2) {
        Assertions.b(this.h != null);
        this.h.setFastForwardIncrementMs(i2);
    }

    public void setPlaybackPreparer(PlaybackPreparer playbackPreparer) {
        Assertions.b(this.h != null);
        this.h.setPlaybackPreparer(playbackPreparer);
    }

    public void setPlayer(Player player) {
        Player player2 = this.k;
        if (player2 != player) {
            if (player2 != null) {
                player2.a((Player.EventListener) this.i);
                Player.VideoComponent g2 = this.k.g();
                if (g2 != null) {
                    g2.a((VideoListener) this.i);
                    View view = this.c;
                    if (view instanceof TextureView) {
                        g2.b((TextureView) view);
                    } else if (view instanceof SurfaceView) {
                        g2.b((SurfaceView) view);
                    }
                }
                Player.TextComponent l2 = this.k.l();
                if (l2 != null) {
                    l2.a(this.i);
                }
            }
            this.k = player;
            if (this.l) {
                this.h.setPlayer(player);
            }
            View view2 = this.b;
            if (view2 != null) {
                view2.setVisibility(0);
            }
            SubtitleView subtitleView = this.e;
            if (subtitleView != null) {
                subtitleView.setCues((List<Cue>) null);
            }
            h();
            i();
            if (player != null) {
                Player.VideoComponent g3 = player.g();
                if (g3 != null) {
                    View view3 = this.c;
                    if (view3 instanceof TextureView) {
                        g3.a((TextureView) view3);
                    } else if (view3 instanceof SurfaceView) {
                        g3.a((SurfaceView) view3);
                    }
                    g3.b((VideoListener) this.i);
                }
                Player.TextComponent l3 = player.l();
                if (l3 != null) {
                    l3.b(this.i);
                }
                player.b((Player.EventListener) this.i);
                a(false);
                j();
                return;
            }
            c();
            e();
        }
    }

    public void setRepeatToggleModes(int i2) {
        Assertions.b(this.h != null);
        this.h.setRepeatToggleModes(i2);
    }

    public void setResizeMode(int i2) {
        Assertions.b(this.f3576a != null);
        this.f3576a.setResizeMode(i2);
    }

    public void setRewindIncrementMs(int i2) {
        Assertions.b(this.h != null);
        this.h.setRewindIncrementMs(i2);
    }

    public void setShowBuffering(boolean z) {
        if (this.o != z) {
            this.o = z;
            h();
        }
    }

    public void setShowMultiWindowTimeBar(boolean z) {
        Assertions.b(this.h != null);
        this.h.setShowMultiWindowTimeBar(z);
    }

    public void setShowShuffleButton(boolean z) {
        Assertions.b(this.h != null);
        this.h.setShowShuffleButton(z);
    }

    public void setShutterBackgroundColor(int i2) {
        View view = this.b;
        if (view != null) {
            view.setBackgroundColor(i2);
        }
    }

    public void setUseArtwork(boolean z) {
        Assertions.b(!z || this.d != null);
        if (this.m != z) {
            this.m = z;
            j();
        }
    }

    public void setUseController(boolean z) {
        Assertions.b(!z || this.h != null);
        if (this.l != z) {
            this.l = z;
            if (z) {
                this.h.setPlayer(this.k);
                return;
            }
            PlayerControlView playerControlView = this.h;
            if (playerControlView != null) {
                playerControlView.a();
                this.h.setPlayer((Player) null);
            }
        }
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        View view = this.c;
        if (view instanceof SurfaceView) {
            view.setVisibility(i2);
        }
    }

    public PlayerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    private void e() {
        ImageView imageView = this.d;
        if (imageView != null) {
            imageView.setImageResource(17170445);
            this.d.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        Player player = this.k;
        return player != null && player.c() && this.k.m();
    }

    private boolean g() {
        Player player = this.k;
        if (player == null) {
            return true;
        }
        int playbackState = player.getPlaybackState();
        if (!this.s || (playbackState != 1 && playbackState != 4 && this.k.m())) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0009, code lost:
        r0 = r3.k;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h() {
        /*
            r3 = this;
            android.view.View r0 = r3.f
            if (r0 == 0) goto L_0x0029
            boolean r0 = r3.o
            r1 = 0
            if (r0 == 0) goto L_0x001e
            com.google.android.exoplayer2.Player r0 = r3.k
            if (r0 == 0) goto L_0x001e
            int r0 = r0.getPlaybackState()
            r2 = 2
            if (r0 != r2) goto L_0x001e
            com.google.android.exoplayer2.Player r0 = r3.k
            boolean r0 = r0.m()
            if (r0 == 0) goto L_0x001e
            r0 = 1
            goto L_0x001f
        L_0x001e:
            r0 = 0
        L_0x001f:
            android.view.View r2 = r3.f
            if (r0 == 0) goto L_0x0024
            goto L_0x0026
        L_0x0024:
            r1 = 8
        L_0x0026:
            r2.setVisibility(r1)
        L_0x0029:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.PlayerView.h():void");
    }

    /* access modifiers changed from: private */
    public void i() {
        TextView textView = this.g;
        if (textView != null) {
            CharSequence charSequence = this.q;
            if (charSequence != null) {
                textView.setText(charSequence);
                this.g.setVisibility(0);
                return;
            }
            ExoPlaybackException exoPlaybackException = null;
            Player player = this.k;
            if (!(player == null || player.getPlaybackState() != 1 || this.p == null)) {
                exoPlaybackException = this.k.e();
            }
            if (exoPlaybackException != null) {
                this.g.setText((CharSequence) this.p.a(exoPlaybackException).second);
                this.g.setVisibility(0);
                return;
            }
            this.g.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        Player player = this.k;
        if (player != null) {
            TrackSelectionArray k2 = player.k();
            int i2 = 0;
            while (i2 < k2.f3564a) {
                if (this.k.a(i2) != 2 || k2.a(i2) == null) {
                    i2++;
                } else {
                    e();
                    return;
                }
            }
            View view = this.b;
            if (view != null) {
                view.setVisibility(0);
            }
            if (this.m) {
                for (int i3 = 0; i3 < k2.f3564a; i3++) {
                    TrackSelection a2 = k2.a(i3);
                    if (a2 != null) {
                        int i4 = 0;
                        while (i4 < a2.length()) {
                            Metadata metadata = a2.a(i4).e;
                            if (metadata == null || !a(metadata)) {
                                i4++;
                            } else {
                                return;
                            }
                        }
                        continue;
                    }
                }
                if (a(this.n)) {
                    return;
                }
            }
            e();
        }
    }

    public void b() {
        int resizeMode = getResizeMode();
        if (resizeMode == 0) {
            setResizeMode(1);
        } else if (resizeMode == 1) {
            setResizeMode(2);
        } else if (resizeMode == 2) {
            setResizeMode(3);
        } else if (resizeMode == 3) {
            setResizeMode(4);
        } else if (resizeMode == 4) {
            setResizeMode(0);
        }
        this.h.setResizeModeButtonIconDrawable(a(getResizeMode()).intValue());
    }

    public void c() {
        PlayerControlView playerControlView = this.h;
        if (playerControlView != null) {
            playerControlView.a();
        }
    }

    public void d() {
        b(g());
    }

    /* JADX INFO: finally extract failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PlayerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean z;
        int i3;
        int i4;
        boolean z2;
        int i5;
        boolean z3;
        int i6;
        boolean z4;
        boolean z5;
        int i7;
        boolean z6;
        boolean z7;
        boolean z8;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        if (isInEditMode()) {
            this.f3576a = null;
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
            this.g = null;
            this.h = null;
            this.i = null;
            this.j = null;
            ImageView imageView = new ImageView(context2);
            if (Util.f3651a >= 23) {
                b(getResources(), imageView);
            } else {
                a(getResources(), imageView);
            }
            addView(imageView);
            return;
        }
        int i8 = R$layout.exo_player_view;
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, R$styleable.PlayerView, 0, 0);
            try {
                z4 = obtainStyledAttributes.hasValue(R$styleable.PlayerView_shutter_background_color);
                i6 = obtainStyledAttributes.getColor(R$styleable.PlayerView_shutter_background_color, 0);
                int resourceId = obtainStyledAttributes.getResourceId(R$styleable.PlayerView_player_layout_id, i8);
                z3 = obtainStyledAttributes.getBoolean(R$styleable.PlayerView_use_artwork, true);
                i5 = obtainStyledAttributes.getResourceId(R$styleable.PlayerView_default_artwork, 0);
                boolean z9 = obtainStyledAttributes.getBoolean(R$styleable.PlayerView_use_controller, true);
                i4 = obtainStyledAttributes.getInt(R$styleable.PlayerView_surface_type, 1);
                i3 = obtainStyledAttributes.getInt(R$styleable.PlayerView_resize_mode, 0);
                int i9 = obtainStyledAttributes.getInt(R$styleable.PlayerView_show_timeout, 5000);
                boolean z10 = obtainStyledAttributes.getBoolean(R$styleable.PlayerView_hide_on_touch, true);
                boolean z11 = obtainStyledAttributes.getBoolean(R$styleable.PlayerView_auto_show, true);
                int i10 = resourceId;
                z6 = obtainStyledAttributes.getBoolean(R$styleable.PlayerView_show_buffering, false);
                boolean z12 = z10;
                boolean z13 = obtainStyledAttributes.getBoolean(R$styleable.PlayerView_hide_during_ads, true);
                obtainStyledAttributes.recycle();
                z5 = z11;
                z = z9;
                z2 = z13;
                i7 = i9;
                i8 = i10;
                z7 = z12;
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        } else {
            z7 = true;
            z6 = false;
            i7 = 5000;
            z5 = true;
            z4 = false;
            i6 = 0;
            z3 = true;
            i5 = 0;
            z2 = true;
            i4 = 1;
            i3 = 0;
            z = true;
        }
        LayoutInflater.from(context).inflate(i8, this);
        this.i = new ComponentListener();
        setDescendantFocusability(262144);
        this.f3576a = (AspectRatioFrameLayout) findViewById(R$id.exo_content_frame);
        AspectRatioFrameLayout aspectRatioFrameLayout = this.f3576a;
        if (aspectRatioFrameLayout != null) {
            a(aspectRatioFrameLayout, i3);
        }
        this.b = findViewById(R$id.exo_shutter);
        View view = this.b;
        if (view != null && z4) {
            view.setBackgroundColor(i6);
        }
        if (this.f3576a == null || i4 == 0) {
            this.c = null;
        } else {
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            this.c = i4 == 2 ? new TextureView(context2) : new SurfaceView(context2);
            this.c.setLayoutParams(layoutParams);
            this.f3576a.addView(this.c, 0);
        }
        this.j = (FrameLayout) findViewById(R$id.exo_overlay);
        this.d = (ImageView) findViewById(R$id.exo_artwork);
        this.m = z3 && this.d != null;
        if (i5 != 0) {
            this.n = BitmapFactory.decodeResource(context.getResources(), i5);
        }
        this.e = (SubtitleView) findViewById(R$id.exo_subtitles);
        SubtitleView subtitleView = this.e;
        if (subtitleView != null) {
            subtitleView.a();
            this.e.b();
        }
        this.f = findViewById(R$id.exo_buffering);
        View view2 = this.f;
        if (view2 != null) {
            view2.setVisibility(8);
        }
        this.o = z6;
        this.g = (TextView) findViewById(R$id.exo_error_message);
        TextView textView = this.g;
        if (textView != null) {
            textView.setVisibility(8);
        }
        PlayerControlView playerControlView = (PlayerControlView) findViewById(R$id.exo_controller);
        View findViewById = findViewById(R$id.exo_controller_placeholder);
        if (playerControlView != null) {
            this.h = playerControlView;
            z8 = false;
        } else if (findViewById != null) {
            z8 = false;
            this.h = new PlayerControlView(context2, (AttributeSet) null, 0, attributeSet2);
            this.h.setLayoutParams(findViewById.getLayoutParams());
            ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
            int indexOfChild = viewGroup.indexOfChild(findViewById);
            viewGroup.removeView(findViewById);
            viewGroup.addView(this.h, indexOfChild);
        } else {
            z8 = false;
            this.h = null;
        }
        this.h.setPlayerView(this);
        this.r = this.h == null ? 0 : i7;
        this.u = z7;
        this.s = z5;
        this.t = z2;
        if (z && this.h != null) {
            z8 = true;
        }
        this.l = z8;
        c();
    }

    private Integer a(int i2) {
        if (i2 == 0) {
            return Integer.valueOf(R$drawable.ic_zoom_out_map_white_36dp);
        }
        if (i2 == 1) {
            return Integer.valueOf(R$drawable.ic_swap_vert_white_36dp);
        }
        if (i2 == 2) {
            return Integer.valueOf(R$drawable.ic_aspect_ratio_white_36dp);
        }
        if (i2 == 3) {
            return Integer.valueOf(R$drawable.ic_crop_white_36dp);
        }
        if (i2 != 4) {
            return null;
        }
        return Integer.valueOf(R$drawable.ic_swap_horiz_white_36dp);
    }

    private void b(boolean z) {
        if (this.l) {
            this.h.setShowTimeoutMs(z ? 0 : this.r);
            this.h.d();
        }
    }

    public boolean a(KeyEvent keyEvent) {
        return this.l && this.h.a(keyEvent);
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if ((!f() || !this.t) && this.l) {
            boolean z2 = this.h.b() && this.h.getShowTimeoutMs() <= 0;
            boolean g2 = g();
            if (z || z2 || g2) {
                b(g2);
            }
        }
    }

    @TargetApi(23)
    private static void b(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(R$drawable.exo_edit_mode_logo, (Resources.Theme) null));
        imageView.setBackgroundColor(resources.getColor(R$color.exo_edit_mode_background_color, (Resources.Theme) null));
    }

    /* access modifiers changed from: private */
    public static void b(TextureView textureView, int i2) {
        float width = (float) textureView.getWidth();
        float height = (float) textureView.getHeight();
        if (width == 0.0f || height == 0.0f || i2 == 0) {
            textureView.setTransform((Matrix) null);
            return;
        }
        Matrix matrix = new Matrix();
        float f2 = width / 2.0f;
        float f3 = height / 2.0f;
        matrix.postRotate((float) i2, f2, f3);
        RectF rectF = new RectF(0.0f, 0.0f, width, height);
        RectF rectF2 = new RectF();
        matrix.mapRect(rectF2, rectF);
        matrix.postScale(width / rectF2.width(), height / rectF2.height(), f2, f3);
        textureView.setTransform(matrix);
    }

    private boolean a(Metadata metadata) {
        for (int i2 = 0; i2 < metadata.a(); i2++) {
            Metadata.Entry a2 = metadata.a(i2);
            if (a2 instanceof ApicFrame) {
                byte[] bArr = ((ApicFrame) a2).e;
                return a(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
            }
        }
        return false;
    }

    private boolean a(Bitmap bitmap) {
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (width > 0 && height > 0) {
                AspectRatioFrameLayout aspectRatioFrameLayout = this.f3576a;
                if (aspectRatioFrameLayout != null) {
                    aspectRatioFrameLayout.setAspectRatio(((float) width) / ((float) height));
                }
                this.d.setImageBitmap(bitmap);
                this.d.setVisibility(0);
                return true;
            }
        }
        return false;
    }

    private static void a(Resources resources, ImageView imageView) {
        imageView.setImageDrawable(resources.getDrawable(R$drawable.exo_edit_mode_logo));
        imageView.setBackgroundColor(resources.getColor(R$color.exo_edit_mode_background_color));
    }

    private static void a(AspectRatioFrameLayout aspectRatioFrameLayout, int i2) {
        aspectRatioFrameLayout.setResizeMode(i2);
    }
}
