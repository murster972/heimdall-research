package com.google.android.exoplayer2.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public final class AspectRatioFrameLayout extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private final AspectRatioUpdateDispatcher f3567a;
    /* access modifiers changed from: private */
    public AspectRatioListener b;
    private float c;
    private int d;

    public interface AspectRatioListener {
        void a(float f, float f2, boolean z);
    }

    private final class AspectRatioUpdateDispatcher implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private float f3568a;
        private float b;
        private boolean c;
        private boolean d;

        private AspectRatioUpdateDispatcher() {
        }

        public void a(float f, float f2, boolean z) {
            this.f3568a = f;
            this.b = f2;
            this.c = z;
            if (!this.d) {
                this.d = true;
                AspectRatioFrameLayout.this.post(this);
            }
        }

        public void run() {
            this.d = false;
            if (AspectRatioFrameLayout.this.b != null) {
                AspectRatioFrameLayout.this.b.a(this.f3568a, this.b, this.c);
            }
        }
    }

    public AspectRatioFrameLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    public int getResizeMode() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        float f;
        float f2;
        super.onMeasure(i, i2);
        if (this.c > 0.0f) {
            int measuredWidth = getMeasuredWidth();
            int measuredHeight = getMeasuredHeight();
            float f3 = (float) measuredWidth;
            float f4 = (float) measuredHeight;
            float f5 = f3 / f4;
            float f6 = (this.c / f5) - 1.0f;
            if (Math.abs(f6) <= 0.01f) {
                this.f3567a.a(this.c, f5, false);
                return;
            }
            int i3 = this.d;
            if (i3 != 0) {
                if (i3 == 1) {
                    f2 = this.c;
                } else if (i3 != 2) {
                    if (i3 == 4) {
                        if (f6 > 0.0f) {
                            f = this.c;
                        } else {
                            f2 = this.c;
                        }
                    }
                    this.f3567a.a(this.c, f5, true);
                    super.onMeasure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824));
                } else {
                    f = this.c;
                }
                measuredHeight = (int) (f3 / f2);
                this.f3567a.a(this.c, f5, true);
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824));
            } else if (f6 > 0.0f) {
                f2 = this.c;
                measuredHeight = (int) (f3 / f2);
                this.f3567a.a(this.c, f5, true);
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824));
            } else {
                f = this.c;
            }
            measuredWidth = (int) (f4 * f);
            this.f3567a.a(this.c, f5, true);
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824));
        }
    }

    public void setAspectRatio(float f) {
        if (this.c != f) {
            this.c = f;
            requestLayout();
        }
    }

    public void setAspectRatioListener(AspectRatioListener aspectRatioListener) {
        this.b = aspectRatioListener;
    }

    public void setResizeMode(int i) {
        if (this.d != i) {
            this.d = i;
            requestLayout();
        }
    }

    public AspectRatioFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = 0;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R$styleable.AspectRatioFrameLayout, 0, 0);
            try {
                this.d = obtainStyledAttributes.getInt(R$styleable.AspectRatioFrameLayout_resize_mode, 0);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.f3567a = new AspectRatioUpdateDispatcher();
    }
}
