package com.google.android.exoplayer2.ui;

public final class R$drawable {
    public static final int btn_border = 2131230864;
    public static final int exo_controls_fastforward = 2131230982;
    public static final int exo_controls_fullscreen_enter = 2131230983;
    public static final int exo_controls_fullscreen_exit = 2131230984;
    public static final int exo_controls_next = 2131230985;
    public static final int exo_controls_pause = 2131230986;
    public static final int exo_controls_play = 2131230987;
    public static final int exo_controls_previous = 2131230988;
    public static final int exo_controls_repeat_all = 2131230989;
    public static final int exo_controls_repeat_off = 2131230990;
    public static final int exo_controls_repeat_one = 2131230991;
    public static final int exo_controls_rewind = 2131230992;
    public static final int exo_controls_shuffle = 2131230993;
    public static final int exo_edit_mode_logo = 2131230994;
    public static final int exo_icon_fastforward = 2131230995;
    public static final int exo_icon_next = 2131230996;
    public static final int exo_icon_pause = 2131230997;
    public static final int exo_icon_play = 2131230998;
    public static final int exo_icon_previous = 2131230999;
    public static final int exo_icon_rewind = 2131231000;
    public static final int exo_icon_stop = 2131231001;
    public static final int exo_notification_fastforward = 2131231002;
    public static final int exo_notification_next = 2131231003;
    public static final int exo_notification_pause = 2131231004;
    public static final int exo_notification_play = 2131231005;
    public static final int exo_notification_previous = 2131231006;
    public static final int exo_notification_rewind = 2131231007;
    public static final int exo_notification_small_icon = 2131231008;
    public static final int exo_notification_stop = 2131231009;
    public static final int ic_aspect_ratio_white_36dp = 2131231026;
    public static final int ic_closed_caption_white_36dp = 2131231032;
    public static final int ic_crop_white_36dp = 2131231034;
    public static final int ic_swap_horiz_white_36dp = 2131231254;
    public static final int ic_swap_vert_white_36dp = 2131231255;
    public static final int ic_zoom_out_map_white_36dp = 2131231268;
    public static final int notification_action_background = 2131231305;
    public static final int notification_bg = 2131231306;
    public static final int notification_bg_low = 2131231307;
    public static final int notification_bg_low_normal = 2131231308;
    public static final int notification_bg_low_pressed = 2131231309;
    public static final int notification_bg_normal = 2131231310;
    public static final int notification_bg_normal_pressed = 2131231311;
    public static final int notification_icon_background = 2131231312;
    public static final int notification_template_icon_bg = 2131231313;
    public static final int notification_template_icon_low_bg = 2131231314;
    public static final int notification_tile_bg = 2131231315;
    public static final int notify_panel_notification_icon_bg = 2131231316;

    private R$drawable() {
    }
}
