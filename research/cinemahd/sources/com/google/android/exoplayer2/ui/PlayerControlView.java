package com.google.android.exoplayer2.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.res.ResourcesCompat;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.DefaultControlDispatcher;
import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.RepeatModeUtil;
import com.google.android.exoplayer2.util.Util;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Locale;

public class PlayerControlView extends FrameLayout {
    /* access modifiers changed from: private */
    public ControlDispatcher A;
    /* access modifiers changed from: private */
    public VisibilityListener B;
    private PlaybackPreparer C;
    private boolean D;
    private boolean E;
    private boolean F;
    /* access modifiers changed from: private */
    public boolean G;
    private int H;
    private int I;
    private int J;
    /* access modifiers changed from: private */
    public int K;
    private boolean L;
    private long M;
    private long[] N;
    private boolean[] O;
    private long[] P;
    private boolean[] Q;
    private final Runnable R;
    /* access modifiers changed from: private */
    public final Runnable S;

    /* renamed from: a  reason: collision with root package name */
    private final ComponentListener f3572a;
    /* access modifiers changed from: private */
    public final View b;
    /* access modifiers changed from: private */
    public final View c;
    /* access modifiers changed from: private */
    public final View d;
    /* access modifiers changed from: private */
    public final View e;
    /* access modifiers changed from: private */
    public final View f;
    /* access modifiers changed from: private */
    public final View g;
    /* access modifiers changed from: private */
    public final View h;
    /* access modifiers changed from: private */
    public final View i;
    /* access modifiers changed from: private */
    public final ImageView j;
    /* access modifiers changed from: private */
    public final View k;
    private final TextView l;
    /* access modifiers changed from: private */
    public final TextView m;
    private final TimeBar n;
    /* access modifiers changed from: private */
    public final StringBuilder o;
    /* access modifiers changed from: private */
    public final Formatter p;
    private final Timeline.Period q;
    private final Timeline.Window r;
    private final Drawable s;
    private final Drawable t;
    private final Drawable u;
    private final String v;
    private final String w;
    private final String x;
    /* access modifiers changed from: private */
    public Player y;
    /* access modifiers changed from: private */
    public PlayerView z;

    public interface VisibilityListener {
        void c(int i);

        void d(int i);
    }

    static {
        ExoPlayerLibraryInfo.a("goog.exo.ui");
    }

    public PlayerControlView(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, attributeSet);
    }

    @SuppressLint({"InlinedApi"})
    private static boolean a(int i2) {
        return i2 == 90 || i2 == 89 || i2 == 85 || i2 == 126 || i2 == 127 || i2 == 87 || i2 == 88;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return a(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    public Player getPlayer() {
        return this.y;
    }

    public int getRepeatToggleModes() {
        return this.K;
    }

    public boolean getShowShuffleButton() {
        return this.L;
    }

    public int getShowTimeoutMs() {
        return this.J;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.D = true;
        long j2 = this.M;
        if (j2 != -9223372036854775807L) {
            long uptimeMillis = j2 - SystemClock.uptimeMillis();
            if (uptimeMillis <= 0) {
                a();
            } else {
                postDelayed(this.S, uptimeMillis);
            }
        } else if (b()) {
            f();
        }
        l();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.D = false;
        removeCallbacks(this.R);
        removeCallbacks(this.S);
    }

    public void setCcButtonEnable(boolean z2) {
        View view = this.b;
        if (view != null) {
            view.setEnabled(z2);
            this.b.setAlpha(z2 ? 1.0f : 0.3f);
        }
    }

    public void setCcButtonVisible(boolean z2) {
        View view = this.b;
        if (view != null) {
            view.setAlpha(z2 ? 1.0f : 0.3f);
        }
    }

    public void setControlDispatcher(ControlDispatcher controlDispatcher) {
        if (controlDispatcher == null) {
            controlDispatcher = new DefaultControlDispatcher();
        }
        this.A = controlDispatcher;
    }

    public void setFastForwardIncrementMs(int i2) {
        this.I = i2;
        m();
    }

    public void setPlaybackPreparer(PlaybackPreparer playbackPreparer) {
        this.C = playbackPreparer;
    }

    public void setPlayer(Player player) {
        Player player2 = this.y;
        if (player2 != player) {
            if (player2 != null) {
                player2.a((Player.EventListener) this.f3572a);
            }
            this.y = player;
            if (player != null) {
                player.b((Player.EventListener) this.f3572a);
            }
            l();
        }
    }

    public void setPlayerView(PlayerView playerView) {
        this.z = playerView;
    }

    public void setRepeatToggleModes(int i2) {
        this.K = i2;
        Player player = this.y;
        if (player != null) {
            int repeatMode = player.getRepeatMode();
            if (i2 == 0 && repeatMode != 0) {
                this.A.a(this.y, 0);
            } else if (i2 == 1 && repeatMode == 2) {
                this.A.a(this.y, 1);
            } else if (i2 == 2 && repeatMode == 1) {
                this.A.a(this.y, 2);
            }
        }
    }

    public void setResizeModeButtonIconDrawable(int i2) {
        View view = this.c;
        if (view != null && (view instanceof ImageButton)) {
            ((ImageButton) view).setImageDrawable(ResourcesCompat.b(getResources(), i2, getContext().getTheme()));
        }
    }

    public void setRewindIncrementMs(int i2) {
        this.H = i2;
        m();
    }

    public void setShowMultiWindowTimeBar(boolean z2) {
        this.E = z2;
        r();
    }

    public void setShowShuffleButton(boolean z2) {
        this.L = z2;
        q();
    }

    public void setShowTimeoutMs(int i2) {
        this.J = i2;
        if (b()) {
            f();
        }
    }

    public void setVisibilityListener(VisibilityListener visibilityListener) {
        this.B = visibilityListener;
    }

    private final class ComponentListener extends Player.DefaultEventListener implements TimeBar.OnScrubListener, View.OnClickListener {
        private ComponentListener() {
        }

        public void a(TimeBar timeBar, long j) {
            if (PlayerControlView.this.m != null) {
                PlayerControlView.this.m.setText(Util.a(PlayerControlView.this.o, PlayerControlView.this.p, j));
            }
        }

        public void b(TimeBar timeBar, long j) {
            PlayerControlView playerControlView = PlayerControlView.this;
            playerControlView.removeCallbacks(playerControlView.S);
            boolean unused = PlayerControlView.this.G = true;
        }

        public void onClick(View view) {
            if (PlayerControlView.this.y != null) {
                if (PlayerControlView.this.b == view) {
                    if (PlayerControlView.this.z != null) {
                        PlayerControlView.this.z.a();
                    }
                } else if (PlayerControlView.this.c == view) {
                    if (PlayerControlView.this.z != null) {
                        PlayerControlView.this.z.b();
                    }
                } else if (PlayerControlView.this.e == view) {
                    PlayerControlView.this.h();
                } else if (PlayerControlView.this.d == view) {
                    PlayerControlView.this.i();
                } else if (PlayerControlView.this.h == view) {
                    PlayerControlView.this.e();
                } else if (PlayerControlView.this.i == view) {
                    PlayerControlView.this.k();
                } else if (PlayerControlView.this.f == view) {
                    PlayerControlView.this.c();
                } else if (PlayerControlView.this.g == view) {
                    PlayerControlView.this.A.b(PlayerControlView.this.y, false);
                } else if (PlayerControlView.this.j == view) {
                    PlayerControlView.this.A.a(PlayerControlView.this.y, RepeatModeUtil.a(PlayerControlView.this.y.getRepeatMode(), PlayerControlView.this.K));
                } else if (PlayerControlView.this.k == view) {
                    PlayerControlView.this.A.a(PlayerControlView.this.y, !PlayerControlView.this.y.r());
                }
                PlayerControlView.this.B.d(view.getId());
            }
            PlayerControlView.this.f();
        }

        public void onPlayerStateChanged(boolean z, int i) {
            PlayerControlView.this.n();
            PlayerControlView.this.o();
        }

        public void onRepeatModeChanged(int i) {
            PlayerControlView.this.p();
            PlayerControlView.this.m();
        }

        public void a(TimeBar timeBar, long j, boolean z) {
            boolean unused = PlayerControlView.this.G = false;
            if (!z && PlayerControlView.this.y != null) {
                PlayerControlView.this.b(j);
            }
            PlayerControlView.this.f();
        }

        public void b(boolean z) {
            PlayerControlView.this.q();
            PlayerControlView.this.m();
        }

        public void b(int i) {
            PlayerControlView.this.m();
            PlayerControlView.this.o();
        }

        public void a(Timeline timeline, Object obj, int i) {
            PlayerControlView.this.m();
            PlayerControlView.this.r();
            PlayerControlView.this.o();
        }
    }

    public PlayerControlView(Context context, AttributeSet attributeSet, int i2, AttributeSet attributeSet2) {
        super(context, attributeSet, i2);
        this.R = new Runnable() {
            public void run() {
                PlayerControlView.this.o();
            }
        };
        this.S = new Runnable() {
            public void run() {
                PlayerControlView.this.a();
            }
        };
        int i3 = R$layout.exo_player_control_view;
        this.H = 5000;
        this.I = 15000;
        this.J = 5000;
        this.K = 0;
        this.M = -9223372036854775807L;
        this.L = false;
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, R$styleable.PlayerControlView, 0, 0);
            try {
                this.H = obtainStyledAttributes.getInt(R$styleable.PlayerControlView_rewind_increment, this.H);
                this.I = obtainStyledAttributes.getInt(R$styleable.PlayerControlView_fastforward_increment, this.I);
                this.J = obtainStyledAttributes.getInt(R$styleable.PlayerControlView_show_timeout, this.J);
                i3 = obtainStyledAttributes.getResourceId(R$styleable.PlayerControlView_controller_layout_id, i3);
                this.K = a(obtainStyledAttributes, this.K);
                this.L = obtainStyledAttributes.getBoolean(R$styleable.PlayerControlView_show_shuffle_button, this.L);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.q = new Timeline.Period();
        this.r = new Timeline.Window();
        this.o = new StringBuilder();
        this.p = new Formatter(this.o, Locale.getDefault());
        this.N = new long[0];
        this.O = new boolean[0];
        this.P = new long[0];
        this.Q = new boolean[0];
        this.f3572a = new ComponentListener();
        this.A = new DefaultControlDispatcher();
        LayoutInflater.from(context).inflate(i3, this);
        setDescendantFocusability(262144);
        this.l = (TextView) findViewById(R$id.exo_duration);
        this.m = (TextView) findViewById(R$id.exo_position);
        this.n = (TimeBar) findViewById(R$id.exo_progress);
        TimeBar timeBar = this.n;
        if (timeBar != null) {
            timeBar.a(this.f3572a);
        }
        this.f = findViewById(R$id.exo_play);
        View view = this.f;
        if (view != null) {
            view.setOnClickListener(this.f3572a);
        }
        this.g = findViewById(R$id.exo_pause);
        View view2 = this.g;
        if (view2 != null) {
            view2.setOnClickListener(this.f3572a);
        }
        this.b = findViewById(R$id.exo_cc);
        View view3 = this.b;
        if (view3 != null) {
            view3.setOnClickListener(this.f3572a);
        }
        this.c = findViewById(R$id.exo_resize);
        View view4 = this.c;
        if (view4 != null) {
            view4.setOnClickListener(this.f3572a);
        }
        this.d = findViewById(R$id.exo_prev);
        View view5 = this.d;
        if (view5 != null) {
            view5.setOnClickListener(this.f3572a);
        }
        this.e = findViewById(R$id.exo_next);
        View view6 = this.e;
        if (view6 != null) {
            view6.setOnClickListener(this.f3572a);
        }
        this.i = findViewById(R$id.exo_rew);
        View view7 = this.i;
        if (view7 != null) {
            view7.setOnClickListener(this.f3572a);
        }
        this.h = findViewById(R$id.exo_ffwd);
        View view8 = this.h;
        if (view8 != null) {
            view8.setOnClickListener(this.f3572a);
        }
        this.j = (ImageView) findViewById(R$id.exo_repeat_toggle);
        ImageView imageView = this.j;
        if (imageView != null) {
            imageView.setOnClickListener(this.f3572a);
        }
        this.k = findViewById(R$id.exo_shuffle);
        View view9 = this.k;
        if (view9 != null) {
            view9.setOnClickListener(this.f3572a);
        }
        Resources resources = context.getResources();
        this.s = resources.getDrawable(R$drawable.exo_controls_repeat_off);
        this.t = resources.getDrawable(R$drawable.exo_controls_repeat_one);
        this.u = resources.getDrawable(R$drawable.exo_controls_repeat_all);
        this.v = resources.getString(R$string.exo_controls_repeat_off_description);
        this.w = resources.getString(R$string.exo_controls_repeat_one_description);
        this.x = resources.getString(R$string.exo_controls_repeat_all_description);
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.I > 0) {
            long duration = this.y.getDuration();
            long currentPosition = this.y.getCurrentPosition() + ((long) this.I);
            if (duration != -9223372036854775807L) {
                currentPosition = Math.min(currentPosition, duration);
            }
            a(currentPosition);
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        removeCallbacks(this.S);
        if (this.J > 0) {
            long uptimeMillis = SystemClock.uptimeMillis();
            int i2 = this.J;
            this.M = uptimeMillis + ((long) i2);
            if (this.D) {
                postDelayed(this.S, (long) i2);
                return;
            }
            return;
        }
        this.M = -9223372036854775807L;
    }

    private boolean g() {
        Player player = this.y;
        if (player == null || player.getPlaybackState() == 4 || this.y.getPlaybackState() == 1 || !this.y.m()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void h() {
        Timeline i2 = this.y.i();
        if (!i2.c()) {
            int f2 = this.y.f();
            int q2 = this.y.q();
            if (q2 != -1) {
                a(q2, -9223372036854775807L);
            } else if (i2.a(f2, this.r, false).b) {
                a(f2, -9223372036854775807L);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        if (r1.f3177a == false) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i() {
        /*
            r6 = this;
            com.google.android.exoplayer2.Player r0 = r6.y
            com.google.android.exoplayer2.Timeline r0 = r0.i()
            boolean r1 = r0.c()
            if (r1 == 0) goto L_0x000d
            return
        L_0x000d:
            com.google.android.exoplayer2.Player r1 = r6.y
            int r1 = r1.f()
            com.google.android.exoplayer2.Timeline$Window r2 = r6.r
            r0.a((int) r1, (com.google.android.exoplayer2.Timeline.Window) r2)
            com.google.android.exoplayer2.Player r0 = r6.y
            int r0 = r0.p()
            r1 = -1
            if (r0 == r1) goto L_0x0040
            com.google.android.exoplayer2.Player r1 = r6.y
            long r1 = r1.getCurrentPosition()
            r3 = 3000(0xbb8, double:1.482E-320)
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0037
            com.google.android.exoplayer2.Timeline$Window r1 = r6.r
            boolean r2 = r1.b
            if (r2 == 0) goto L_0x0040
            boolean r1 = r1.f3177a
            if (r1 != 0) goto L_0x0040
        L_0x0037:
            r1 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r6.a((int) r0, (long) r1)
            goto L_0x0045
        L_0x0040:
            r0 = 0
            r6.a((long) r0)
        L_0x0045:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.PlayerControlView.i():void");
    }

    private void j() {
        View view;
        View view2;
        boolean g2 = g();
        if (!g2 && (view2 = this.f) != null) {
            view2.requestFocus();
        } else if (g2 && (view = this.g) != null) {
            view.requestFocus();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.H > 0) {
            a(Math.max(this.y.getCurrentPosition() - ((long) this.H), 0));
        }
    }

    private void l() {
        n();
        m();
        p();
        q();
        o();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m() {
        /*
            r6 = this;
            boolean r0 = r6.b()
            if (r0 == 0) goto L_0x008e
            boolean r0 = r6.D
            if (r0 != 0) goto L_0x000c
            goto L_0x008e
        L_0x000c:
            com.google.android.exoplayer2.Player r0 = r6.y
            if (r0 == 0) goto L_0x0015
            com.google.android.exoplayer2.Timeline r0 = r0.i()
            goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0022
            boolean r3 = r0.c()
            if (r3 != 0) goto L_0x0022
            r3 = 1
            goto L_0x0023
        L_0x0022:
            r3 = 0
        L_0x0023:
            if (r3 == 0) goto L_0x005f
            com.google.android.exoplayer2.Player r3 = r6.y
            boolean r3 = r3.c()
            if (r3 != 0) goto L_0x005f
            com.google.android.exoplayer2.Player r3 = r6.y
            int r3 = r3.f()
            com.google.android.exoplayer2.Timeline$Window r4 = r6.r
            r0.a((int) r3, (com.google.android.exoplayer2.Timeline.Window) r4)
            com.google.android.exoplayer2.Timeline$Window r0 = r6.r
            boolean r3 = r0.f3177a
            r4 = -1
            if (r3 != 0) goto L_0x004e
            boolean r0 = r0.b
            if (r0 == 0) goto L_0x004e
            com.google.android.exoplayer2.Player r0 = r6.y
            int r0 = r0.p()
            if (r0 == r4) goto L_0x004c
            goto L_0x004e
        L_0x004c:
            r0 = 0
            goto L_0x004f
        L_0x004e:
            r0 = 1
        L_0x004f:
            com.google.android.exoplayer2.Timeline$Window r5 = r6.r
            boolean r5 = r5.b
            if (r5 != 0) goto L_0x005d
            com.google.android.exoplayer2.Player r5 = r6.y
            int r5 = r5.q()
            if (r5 == r4) goto L_0x0061
        L_0x005d:
            r4 = 1
            goto L_0x0062
        L_0x005f:
            r0 = 0
            r3 = 0
        L_0x0061:
            r4 = 0
        L_0x0062:
            android.view.View r5 = r6.d
            r6.a((boolean) r0, (android.view.View) r5)
            android.view.View r0 = r6.e
            r6.a((boolean) r4, (android.view.View) r0)
            int r0 = r6.I
            if (r0 <= 0) goto L_0x0074
            if (r3 == 0) goto L_0x0074
            r0 = 1
            goto L_0x0075
        L_0x0074:
            r0 = 0
        L_0x0075:
            android.view.View r4 = r6.h
            r6.a((boolean) r0, (android.view.View) r4)
            int r0 = r6.H
            if (r0 <= 0) goto L_0x0081
            if (r3 == 0) goto L_0x0081
            goto L_0x0082
        L_0x0081:
            r1 = 0
        L_0x0082:
            android.view.View r0 = r6.i
            r6.a((boolean) r1, (android.view.View) r0)
            com.google.android.exoplayer2.ui.TimeBar r0 = r6.n
            if (r0 == 0) goto L_0x008e
            r0.setEnabled(r3)
        L_0x008e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.PlayerControlView.m():void");
    }

    /* access modifiers changed from: private */
    public void n() {
        boolean z2;
        if (b() && this.D) {
            boolean g2 = g();
            View view = this.f;
            int i2 = 8;
            boolean z3 = true;
            if (view != null) {
                z2 = (g2 && view.isFocused()) | false;
                this.f.setVisibility(g2 ? 8 : 0);
            } else {
                z2 = false;
            }
            View view2 = this.g;
            if (view2 != null) {
                if (g2 || !view2.isFocused()) {
                    z3 = false;
                }
                z2 |= z3;
                View view3 = this.g;
                if (g2) {
                    i2 = 0;
                }
                view3.setVisibility(i2);
            }
            if (z2) {
                j();
            }
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        long j2;
        long j3;
        long j4;
        int i2;
        long j5;
        Timeline.Window window;
        int i3;
        if (b() && this.D) {
            Player player = this.y;
            long j6 = 0;
            boolean z2 = true;
            if (player != null) {
                Timeline i4 = player.i();
                if (!i4.c()) {
                    int f2 = this.y.f();
                    int i5 = this.F ? 0 : f2;
                    int b2 = this.F ? i4.b() - 1 : f2;
                    j5 = 0;
                    j4 = 0;
                    i2 = 0;
                    while (true) {
                        if (i5 > b2) {
                            break;
                        }
                        if (i5 == f2) {
                            j4 = j5;
                        }
                        i4.a(i5, this.r);
                        Timeline.Window window2 = this.r;
                        int i6 = i5;
                        if (window2.f == -9223372036854775807L) {
                            Assertions.b(this.F ^ z2);
                            break;
                        }
                        int i7 = window2.c;
                        while (true) {
                            window = this.r;
                            if (i7 > window.d) {
                                break;
                            }
                            i4.a(i7, this.q);
                            int a2 = this.q.a();
                            int i8 = i2;
                            int i9 = 0;
                            while (i9 < a2) {
                                long b3 = this.q.b(i9);
                                if (b3 == Long.MIN_VALUE) {
                                    i3 = f2;
                                    long j7 = this.q.c;
                                    if (j7 == -9223372036854775807L) {
                                        i9++;
                                        f2 = i3;
                                    } else {
                                        b3 = j7;
                                    }
                                } else {
                                    i3 = f2;
                                }
                                long f3 = b3 + this.q.f();
                                if (f3 >= 0 && f3 <= this.r.f) {
                                    long[] jArr = this.N;
                                    if (i8 == jArr.length) {
                                        int length = jArr.length == 0 ? 1 : jArr.length * 2;
                                        this.N = Arrays.copyOf(this.N, length);
                                        this.O = Arrays.copyOf(this.O, length);
                                    }
                                    this.N[i8] = C.b(j5 + f3);
                                    this.O[i8] = this.q.d(i9);
                                    i8++;
                                }
                                i9++;
                                f2 = i3;
                            }
                            int i10 = f2;
                            i7++;
                            i2 = i8;
                        }
                        j5 += window.f;
                        i5 = i6 + 1;
                        f2 = f2;
                        z2 = true;
                    }
                } else {
                    j5 = 0;
                    j4 = 0;
                    i2 = 0;
                }
                j6 = C.b(j5);
                long b4 = C.b(j4);
                if (this.y.c()) {
                    j3 = b4 + this.y.o();
                    j2 = j3;
                } else {
                    long currentPosition = this.y.getCurrentPosition() + b4;
                    long bufferedPosition = b4 + this.y.getBufferedPosition();
                    j3 = currentPosition;
                    j2 = bufferedPosition;
                }
                if (this.n != null) {
                    int length2 = this.P.length;
                    int i11 = i2 + length2;
                    long[] jArr2 = this.N;
                    if (i11 > jArr2.length) {
                        this.N = Arrays.copyOf(jArr2, i11);
                        this.O = Arrays.copyOf(this.O, i11);
                    }
                    System.arraycopy(this.P, 0, this.N, i2, length2);
                    System.arraycopy(this.Q, 0, this.O, i2, length2);
                    this.n.a(this.N, this.O, i11);
                }
            } else {
                j3 = 0;
                j2 = 0;
            }
            TextView textView = this.l;
            if (textView != null) {
                textView.setText(Util.a(this.o, this.p, j6));
            }
            TextView textView2 = this.m;
            if (textView2 != null && !this.G) {
                textView2.setText(Util.a(this.o, this.p, j3));
            }
            TimeBar timeBar = this.n;
            if (timeBar != null) {
                timeBar.setPosition(j3);
                this.n.setBufferedPosition(j2);
                this.n.setDuration(j6);
            }
            removeCallbacks(this.R);
            Player player2 = this.y;
            int playbackState = player2 == null ? 1 : player2.getPlaybackState();
            if (playbackState != 1 && playbackState != 4) {
                long j8 = 1000;
                if (this.y.m() && playbackState == 3) {
                    float f4 = this.y.b().f3170a;
                    if (f4 > 0.1f) {
                        if (f4 <= 5.0f) {
                            long max = (long) (1000 / Math.max(1, Math.round(1.0f / f4)));
                            long j9 = max - (j3 % max);
                            if (j9 < max / 5) {
                                j9 += max;
                            }
                            if (f4 != 1.0f) {
                                j9 = (long) (((float) j9) / f4);
                            }
                            j8 = j9;
                        } else {
                            j8 = 200;
                        }
                    }
                }
                postDelayed(this.R, j8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        ImageView imageView;
        if (b() && this.D && (imageView = this.j) != null) {
            if (this.K == 0) {
                imageView.setVisibility(8);
            } else if (this.y == null) {
                a(false, (View) imageView);
            } else {
                a(true, (View) imageView);
                int repeatMode = this.y.getRepeatMode();
                if (repeatMode == 0) {
                    this.j.setImageDrawable(this.s);
                    this.j.setContentDescription(this.v);
                } else if (repeatMode == 1) {
                    this.j.setImageDrawable(this.t);
                    this.j.setContentDescription(this.w);
                } else if (repeatMode == 2) {
                    this.j.setImageDrawable(this.u);
                    this.j.setContentDescription(this.x);
                }
                this.j.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        View view;
        if (b() && this.D && (view = this.k) != null) {
            if (!this.L) {
                view.setVisibility(8);
                return;
            }
            Player player = this.y;
            if (player == null) {
                a(false, view);
                return;
            }
            view.setAlpha(player.r() ? 1.0f : 0.3f);
            this.k.setEnabled(true);
            this.k.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        Player player = this.y;
        if (player != null) {
            this.F = this.E && a(player.i(), this.r);
        }
    }

    public boolean b() {
        return getVisibility() == 0;
    }

    public void c() {
        if (this.y.getPlaybackState() == 1) {
            PlaybackPreparer playbackPreparer = this.C;
            if (playbackPreparer != null) {
                playbackPreparer.a();
            }
        } else if (this.y.getPlaybackState() == 4) {
            ControlDispatcher controlDispatcher = this.A;
            Player player = this.y;
            controlDispatcher.a(player, player.f(), -9223372036854775807L);
        }
        this.A.b(this.y, true);
    }

    public void d() {
        if (!b()) {
            setVisibility(0);
            VisibilityListener visibilityListener = this.B;
            if (visibilityListener != null) {
                visibilityListener.c(getVisibility());
            }
            l();
            j();
        }
        f();
    }

    /* access modifiers changed from: private */
    public void b(long j2) {
        int i2;
        Timeline i3 = this.y.i();
        if (this.F && !i3.c()) {
            int b2 = i3.b();
            i2 = 0;
            while (true) {
                long c2 = i3.a(i2, this.r).c();
                if (j2 < c2) {
                    break;
                } else if (i2 == b2 - 1) {
                    j2 = c2;
                    break;
                } else {
                    j2 -= c2;
                    i2++;
                }
            }
        } else {
            i2 = this.y.f();
        }
        a(i2, j2);
    }

    private static int a(TypedArray typedArray, int i2) {
        return typedArray.getInt(R$styleable.PlayerControlView_repeat_toggle_modes, i2);
    }

    public void a() {
        if (b()) {
            setVisibility(8);
            VisibilityListener visibilityListener = this.B;
            if (visibilityListener != null) {
                visibilityListener.c(getVisibility());
            }
            removeCallbacks(this.R);
            removeCallbacks(this.S);
            this.M = -9223372036854775807L;
        }
    }

    public void a(boolean z2, View view) {
        if (view != null) {
            view.setEnabled(z2);
            view.setAlpha(z2 ? 1.0f : 0.3f);
            view.setVisibility(0);
        }
    }

    private void a(long j2) {
        a(this.y.f(), j2);
    }

    public void a(int i2, long j2) {
        if (!this.A.a(this.y, i2, j2)) {
            o();
        }
    }

    public boolean a(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (this.y == null || !a(keyCode)) {
            return false;
        }
        if (keyEvent.getAction() == 0) {
            if (keyCode == 90) {
                e();
            } else if (keyCode == 89) {
                k();
            } else if (keyEvent.getRepeatCount() == 0) {
                if (keyCode == 85) {
                    ControlDispatcher controlDispatcher = this.A;
                    Player player = this.y;
                    controlDispatcher.b(player, !player.m());
                } else if (keyCode == 87) {
                    h();
                } else if (keyCode == 88) {
                    i();
                } else if (keyCode == 126) {
                    this.A.b(this.y, true);
                } else if (keyCode == 127) {
                    this.A.b(this.y, false);
                }
            }
        }
        return true;
    }

    private static boolean a(Timeline timeline, Timeline.Window window) {
        if (timeline.b() > 100) {
            return false;
        }
        int b2 = timeline.b();
        for (int i2 = 0; i2 < b2; i2++) {
            if (timeline.a(i2, window).f == -9223372036854775807L) {
                return false;
            }
        }
        return true;
    }
}
