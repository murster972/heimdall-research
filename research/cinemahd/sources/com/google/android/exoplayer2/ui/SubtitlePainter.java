package com.google.android.exoplayer2.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.util.Log;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.util.Util;

final class SubtitlePainter {
    private float A;
    private float B;
    private int C;
    private int D;
    private int E;
    private int F;
    private StaticLayout G;
    private int H;
    private int I;
    private int J;
    private Rect K;

    /* renamed from: a  reason: collision with root package name */
    private final RectF f3579a = new RectF();
    private final float b;
    private final float c;
    private final float d;
    private final float e;
    private final float f;
    private final float g;
    private final TextPaint h;
    private final Paint i;
    private CharSequence j;
    private Layout.Alignment k;
    private Bitmap l;
    private float m;
    private int n;
    private int o;
    private float p;
    private int q;
    private float r;
    private float s;
    private boolean t;
    private boolean u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public SubtitlePainter(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, new int[]{16843287, 16843288}, 0, 0);
        this.g = (float) obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.f = obtainStyledAttributes.getFloat(1, 1.0f);
        obtainStyledAttributes.recycle();
        float round = (float) Math.round((((float) context.getResources().getDisplayMetrics().densityDpi) * 2.0f) / 160.0f);
        this.b = round;
        this.c = round;
        this.d = round;
        this.e = round;
        this.h = new TextPaint();
        this.h.setAntiAlias(true);
        this.h.setSubpixelText(true);
        this.i = new Paint();
        this.i.setAntiAlias(true);
        this.i.setStyle(Paint.Style.FILL);
    }

    private void b() {
        String str;
        int i2;
        int i3;
        int i4;
        int round;
        int i5;
        int i6 = this.E - this.C;
        int i7 = this.F - this.D;
        this.h.setTextSize(this.A);
        int i8 = (int) ((this.A * 0.125f) + 0.5f);
        int i9 = i8 * 2;
        int i10 = i6 - i9;
        float f2 = this.r;
        if (f2 != Float.MIN_VALUE) {
            i10 = (int) (((float) i10) * f2);
        }
        if (i10 <= 0) {
            Log.w("SubtitlePainter", "Skipped drawing subtitle cue (insufficient space)");
            return;
        }
        if (this.u && this.t) {
            str = this.j;
        } else if (!this.t) {
            str = this.j.toString();
        } else {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.j);
            int length = spannableStringBuilder.length();
            AbsoluteSizeSpan[] absoluteSizeSpanArr = (AbsoluteSizeSpan[]) spannableStringBuilder.getSpans(0, length, AbsoluteSizeSpan.class);
            RelativeSizeSpan[] relativeSizeSpanArr = (RelativeSizeSpan[]) spannableStringBuilder.getSpans(0, length, RelativeSizeSpan.class);
            for (AbsoluteSizeSpan removeSpan : absoluteSizeSpanArr) {
                spannableStringBuilder.removeSpan(removeSpan);
            }
            for (RelativeSizeSpan removeSpan2 : relativeSizeSpanArr) {
                spannableStringBuilder.removeSpan(removeSpan2);
            }
            str = spannableStringBuilder;
        }
        CharSequence charSequence = str;
        Layout.Alignment alignment = this.k;
        if (alignment == null) {
            alignment = Layout.Alignment.ALIGN_CENTER;
        }
        Layout.Alignment alignment2 = alignment;
        StaticLayout staticLayout = r8;
        int i11 = i8;
        StaticLayout staticLayout2 = new StaticLayout(charSequence, this.h, i10, alignment2, this.f, this.g, true);
        this.G = staticLayout;
        int height = this.G.getHeight();
        int lineCount = this.G.getLineCount();
        int i12 = 0;
        for (int i13 = 0; i13 < lineCount; i13++) {
            i12 = Math.max((int) Math.ceil((double) this.G.getLineWidth(i13)), i12);
        }
        if (this.r == Float.MIN_VALUE || i12 >= i10) {
            i10 = i12;
        }
        int i14 = i10 + i9;
        float f3 = this.p;
        if (f3 != Float.MIN_VALUE) {
            int round2 = Math.round(((float) i6) * f3) + this.C;
            int i15 = this.q;
            if (i15 == 2) {
                round2 -= i14;
            } else if (i15 == 1) {
                round2 = ((round2 * 2) - i14) / 2;
            }
            i3 = Math.max(round2, this.C);
            i2 = Math.min(i14 + i3, this.E);
        } else {
            i3 = (i6 - i14) / 2;
            i2 = i3 + i14;
        }
        int i16 = i2 - i3;
        if (i16 <= 0) {
            Log.w("SubtitlePainter", "Skipped drawing subtitle cue (invalid horizontal positioning)");
            return;
        }
        float f4 = this.m;
        if (f4 != Float.MIN_VALUE) {
            if (this.n == 0) {
                round = Math.round(((float) i7) * f4);
                i5 = this.D;
            } else {
                int lineBottom = this.G.getLineBottom(0) - this.G.getLineTop(0);
                float f5 = this.m;
                if (f5 >= 0.0f) {
                    round = Math.round(f5 * ((float) lineBottom));
                    i5 = this.D;
                } else {
                    round = Math.round((f5 + 1.0f) * ((float) lineBottom));
                    i5 = this.F;
                }
            }
            i4 = round + i5;
            int i17 = this.o;
            if (i17 == 2) {
                i4 -= height;
            } else if (i17 == 1) {
                i4 = ((i4 * 2) - height) / 2;
            }
            int i18 = i4 + height;
            int i19 = this.F;
            if (i18 > i19) {
                i4 = i19 - height;
            } else {
                int i20 = this.D;
                if (i4 < i20) {
                    i4 = i20;
                }
            }
        } else {
            i4 = (this.F - height) - ((int) (((float) i7) * this.B));
        }
        this.G = new StaticLayout(charSequence, this.h, i16, alignment2, this.f, this.g, true);
        this.H = i3;
        this.I = i4;
        this.J = i11;
    }

    public void a(Cue cue, boolean z2, boolean z3, CaptionStyleCompat captionStyleCompat, float f2, float f3, Canvas canvas, int i2, int i3, int i4, int i5) {
        boolean z4 = cue.c == null;
        int i6 = -16777216;
        if (z4) {
            if (!TextUtils.isEmpty(cue.f3514a)) {
                i6 = (!cue.k || !z2) ? captionStyleCompat.c : cue.l;
            } else {
                return;
            }
        }
        if (a(this.j, cue.f3514a) && Util.a((Object) this.k, (Object) cue.b) && this.l == cue.c && this.m == cue.d && this.n == cue.e && Util.a((Object) Integer.valueOf(this.o), (Object) Integer.valueOf(cue.f)) && this.p == cue.g && Util.a((Object) Integer.valueOf(this.q), (Object) Integer.valueOf(cue.h)) && this.r == cue.i && this.s == cue.j && this.t == z2 && this.u == z3 && this.v == captionStyleCompat.f3513a && this.w == captionStyleCompat.b && this.x == i6 && this.z == captionStyleCompat.d && this.y == captionStyleCompat.e && Util.a((Object) this.h.getTypeface(), (Object) captionStyleCompat.f) && this.A == f2 && this.B == f3 && this.C == i2 && this.D == i3 && this.E == i4 && this.F == i5) {
            a(canvas, z4);
            return;
        }
        this.j = cue.f3514a;
        this.k = cue.b;
        this.l = cue.c;
        this.m = cue.d;
        this.n = cue.e;
        this.o = cue.f;
        this.p = cue.g;
        this.q = cue.h;
        this.r = cue.i;
        this.s = cue.j;
        this.t = z2;
        this.u = z3;
        this.v = captionStyleCompat.f3513a;
        this.w = captionStyleCompat.b;
        this.x = i6;
        this.z = captionStyleCompat.d;
        this.y = captionStyleCompat.e;
        this.h.setTypeface(captionStyleCompat.f);
        this.A = f2;
        this.B = f3;
        this.C = i2;
        this.D = i3;
        this.E = i4;
        this.F = i5;
        if (z4) {
            b();
        } else {
            a();
        }
        a(canvas, z4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r7 = this;
            int r0 = r7.E
            int r1 = r7.C
            int r0 = r0 - r1
            int r2 = r7.F
            int r3 = r7.D
            int r2 = r2 - r3
            float r1 = (float) r1
            float r0 = (float) r0
            float r4 = r7.p
            float r4 = r4 * r0
            float r1 = r1 + r4
            float r3 = (float) r3
            float r2 = (float) r2
            float r4 = r7.m
            float r4 = r4 * r2
            float r3 = r3 + r4
            float r4 = r7.r
            float r0 = r0 * r4
            int r0 = java.lang.Math.round(r0)
            float r4 = r7.s
            r5 = 1
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 == 0) goto L_0x002e
            float r2 = r2 * r4
            int r2 = java.lang.Math.round(r2)
            goto L_0x0044
        L_0x002e:
            float r2 = (float) r0
            android.graphics.Bitmap r4 = r7.l
            int r4 = r4.getHeight()
            float r4 = (float) r4
            android.graphics.Bitmap r5 = r7.l
            int r5 = r5.getWidth()
            float r5 = (float) r5
            float r4 = r4 / r5
            float r2 = r2 * r4
            int r2 = java.lang.Math.round(r2)
        L_0x0044:
            int r4 = r7.o
            r5 = 1
            r6 = 2
            if (r4 != r6) goto L_0x004d
            float r4 = (float) r0
        L_0x004b:
            float r1 = r1 - r4
            goto L_0x0053
        L_0x004d:
            if (r4 != r5) goto L_0x0053
            int r4 = r0 / 2
            float r4 = (float) r4
            goto L_0x004b
        L_0x0053:
            int r1 = java.lang.Math.round(r1)
            int r4 = r7.q
            if (r4 != r6) goto L_0x005e
            float r4 = (float) r2
        L_0x005c:
            float r3 = r3 - r4
            goto L_0x0064
        L_0x005e:
            if (r4 != r5) goto L_0x0064
            int r4 = r2 / 2
            float r4 = (float) r4
            goto L_0x005c
        L_0x0064:
            int r3 = java.lang.Math.round(r3)
            android.graphics.Rect r4 = new android.graphics.Rect
            int r0 = r0 + r1
            int r2 = r2 + r3
            r4.<init>(r1, r3, r0, r2)
            r7.K = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.SubtitlePainter.a():void");
    }

    private void a(Canvas canvas, boolean z2) {
        if (z2) {
            b(canvas);
        } else {
            a(canvas);
        }
    }

    private void b(Canvas canvas) {
        int i2;
        StaticLayout staticLayout = this.G;
        if (staticLayout != null) {
            int save = canvas.save();
            canvas.translate((float) this.H, (float) this.I);
            if (Color.alpha(this.x) > 0) {
                this.i.setColor(this.x);
                canvas.drawRect((float) (-this.J), 0.0f, (float) (staticLayout.getWidth() + this.J), (float) staticLayout.getHeight(), this.i);
            }
            if (Color.alpha(this.w) > 0) {
                this.i.setColor(this.w);
                int lineCount = staticLayout.getLineCount();
                float lineTop = (float) staticLayout.getLineTop(0);
                int i3 = 0;
                while (i3 < lineCount) {
                    this.f3579a.left = staticLayout.getLineLeft(i3) - ((float) this.J);
                    this.f3579a.right = staticLayout.getLineRight(i3) + ((float) this.J);
                    RectF rectF = this.f3579a;
                    rectF.top = lineTop;
                    rectF.bottom = (float) staticLayout.getLineBottom(i3);
                    RectF rectF2 = this.f3579a;
                    float f2 = rectF2.bottom;
                    float f3 = this.b;
                    canvas.drawRoundRect(rectF2, f3, f3, this.i);
                    i3++;
                    lineTop = f2;
                }
            }
            int i4 = this.z;
            boolean z2 = true;
            if (i4 == 1) {
                this.h.setStrokeJoin(Paint.Join.ROUND);
                this.h.setStrokeWidth(this.c);
                this.h.setColor(this.y);
                this.h.setStyle(Paint.Style.FILL_AND_STROKE);
                staticLayout.draw(canvas);
            } else if (i4 == 2) {
                TextPaint textPaint = this.h;
                float f4 = this.d;
                float f5 = this.e;
                textPaint.setShadowLayer(f4, f5, f5, this.y);
            } else if (i4 == 3 || i4 == 4) {
                if (this.z != 3) {
                    z2 = false;
                }
                int i5 = -1;
                if (z2) {
                    i2 = -1;
                } else {
                    i2 = this.y;
                }
                if (z2) {
                    i5 = this.y;
                }
                float f6 = this.d / 2.0f;
                this.h.setColor(this.v);
                this.h.setStyle(Paint.Style.FILL);
                float f7 = -f6;
                this.h.setShadowLayer(this.d, f7, f7, i2);
                staticLayout.draw(canvas);
                this.h.setShadowLayer(this.d, f6, f6, i5);
            }
            this.h.setColor(this.v);
            this.h.setStyle(Paint.Style.FILL);
            staticLayout.draw(canvas);
            this.h.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
            canvas.restoreToCount(save);
        }
    }

    private void a(Canvas canvas) {
        canvas.drawBitmap(this.l, (Rect) null, this.K, (Paint) null);
    }

    private static boolean a(CharSequence charSequence, CharSequence charSequence2) {
        return charSequence == charSequence2 || (charSequence != null && charSequence.equals(charSequence2));
    }
}
