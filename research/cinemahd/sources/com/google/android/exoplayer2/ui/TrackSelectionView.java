package com.google.android.exoplayer2.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Arrays;

public class TrackSelectionView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private final int f3581a;
    private final LayoutInflater b;
    private final CheckedTextView c;
    private final CheckedTextView d;
    private final ComponentListener e;
    private boolean f;
    private TrackNameProvider g;
    private CheckedTextView[][] h;
    private DefaultTrackSelector i;
    private int j;
    private TrackGroupArray k;
    private boolean l;
    private DefaultTrackSelector.SelectionOverride m;

    /* renamed from: com.google.android.exoplayer2.ui.TrackSelectionView$1  reason: invalid class name */
    final class AnonymousClass1 implements DialogInterface.OnClickListener {
    }

    private class ComponentListener implements View.OnClickListener {
        private ComponentListener() {
        }

        public void onClick(View view) {
            TrackSelectionView.this.a(view);
        }

        /* synthetic */ ComponentListener(TrackSelectionView trackSelectionView, AnonymousClass1 r2) {
            this();
        }
    }

    public TrackSelectionView(Context context) {
        this(context, (AttributeSet) null);
    }

    private void b() {
        this.l = true;
        this.m = null;
    }

    private void c() {
        this.c.setChecked(this.l);
        this.d.setChecked(!this.l && this.m == null);
        int i2 = 0;
        while (i2 < this.h.length) {
            int i3 = 0;
            while (true) {
                CheckedTextView[][] checkedTextViewArr = this.h;
                if (i3 >= checkedTextViewArr[i2].length) {
                    break;
                }
                CheckedTextView checkedTextView = checkedTextViewArr[i2][i3];
                DefaultTrackSelector.SelectionOverride selectionOverride = this.m;
                checkedTextView.setChecked(selectionOverride != null && selectionOverride.f3561a == i2 && selectionOverride.a(i3));
                i3++;
            }
            i2++;
        }
    }

    private void d() {
        for (int childCount = getChildCount() - 1; childCount >= 3; childCount--) {
            removeViewAt(childCount);
        }
        if (this.i == null) {
            this.c.setEnabled(false);
            this.d.setEnabled(false);
            return;
        }
        this.c.setEnabled(true);
        this.d.setEnabled(true);
        MappingTrackSelector.MappedTrackInfo c2 = this.i.c();
        this.k = c2.c(this.j);
        DefaultTrackSelector.Parameters d2 = this.i.d();
        this.l = d2.a(this.j);
        this.m = d2.a(this.j, this.k);
        this.h = new CheckedTextView[this.k.f3424a][];
        int i2 = 0;
        while (true) {
            TrackGroupArray trackGroupArray = this.k;
            if (i2 < trackGroupArray.f3424a) {
                TrackGroup a2 = trackGroupArray.a(i2);
                boolean z = this.f && this.k.a(i2).f3423a > 1 && c2.a(this.j, i2, false) != 0;
                this.h[i2] = new CheckedTextView[a2.f3423a];
                for (int i3 = 0; i3 < a2.f3423a; i3++) {
                    if (i3 == 0) {
                        addView(this.b.inflate(R$layout.exo_list_divider, this, false));
                    }
                    CheckedTextView checkedTextView = (CheckedTextView) this.b.inflate(z ? 17367056 : 17367055, this, false);
                    checkedTextView.setBackgroundResource(this.f3581a);
                    checkedTextView.setText(this.g.a(a2.a(i3)));
                    if (c2.a(this.j, i2, i3) == 4) {
                        checkedTextView.setFocusable(true);
                        checkedTextView.setTag(Pair.create(Integer.valueOf(i2), Integer.valueOf(i3)));
                        checkedTextView.setOnClickListener(this.e);
                    } else {
                        checkedTextView.setFocusable(false);
                        checkedTextView.setEnabled(false);
                    }
                    this.h[i2][i3] = checkedTextView;
                    addView(checkedTextView);
                }
                i2++;
            } else {
                c();
                return;
            }
        }
    }

    public void setAllowAdaptiveSelections(boolean z) {
        if ((!this.f) == z) {
            this.f = z;
            d();
        }
    }

    public void setShowDisableOption(boolean z) {
        this.c.setVisibility(z ? 0 : 8);
    }

    public void setTrackNameProvider(TrackNameProvider trackNameProvider) {
        Assertions.a(trackNameProvider);
        this.g = trackNameProvider;
    }

    public TrackSelectionView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        if (view == this.c) {
            b();
        } else if (view == this.d) {
            a();
        } else {
            b(view);
        }
        c();
    }

    public TrackSelectionView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{16843534});
        this.f3581a = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        this.b = LayoutInflater.from(context);
        this.e = new ComponentListener(this, (AnonymousClass1) null);
        this.g = new DefaultTrackNameProvider(getResources());
        this.c = (CheckedTextView) this.b.inflate(17367055, this, false);
        this.c.setBackgroundResource(this.f3581a);
        this.c.setText(R$string.exo_track_selection_none);
        this.c.setEnabled(false);
        this.c.setFocusable(true);
        this.c.setOnClickListener(this.e);
        this.c.setVisibility(8);
        addView(this.c);
        addView(this.b.inflate(R$layout.exo_list_divider, this, false));
        this.d = (CheckedTextView) this.b.inflate(17367055, this, false);
        this.d.setBackgroundResource(this.f3581a);
        this.d.setText(R$string.exo_track_selection_auto);
        this.d.setEnabled(false);
        this.d.setFocusable(true);
        this.d.setOnClickListener(this.e);
        addView(this.d);
    }

    private void b(View view) {
        this.l = false;
        Pair pair = (Pair) view.getTag();
        int intValue = ((Integer) pair.first).intValue();
        int intValue2 = ((Integer) pair.second).intValue();
        DefaultTrackSelector.SelectionOverride selectionOverride = this.m;
        if (selectionOverride == null || selectionOverride.f3561a != intValue || !this.f) {
            this.m = new DefaultTrackSelector.SelectionOverride(intValue, intValue2);
            return;
        }
        boolean isChecked = ((CheckedTextView) view).isChecked();
        DefaultTrackSelector.SelectionOverride selectionOverride2 = this.m;
        int i2 = selectionOverride2.c;
        if (!isChecked) {
            this.m = new DefaultTrackSelector.SelectionOverride(intValue, a(selectionOverride2.b, intValue2));
        } else if (i2 == 1) {
            this.m = null;
            this.l = true;
        } else {
            this.m = new DefaultTrackSelector.SelectionOverride(intValue, b(selectionOverride2.b, intValue2));
        }
    }

    private void a() {
        this.l = false;
        this.m = null;
    }

    private static int[] a(int[] iArr, int i2) {
        int[] copyOf = Arrays.copyOf(iArr, iArr.length + 1);
        copyOf[copyOf.length - 1] = i2;
        return copyOf;
    }

    private static int[] b(int[] iArr, int i2) {
        int[] iArr2 = new int[(iArr.length - 1)];
        int i3 = 0;
        for (int i4 : iArr) {
            if (i4 != i2) {
                iArr2[i3] = i4;
                i3++;
            }
        }
        return iArr2;
    }
}
