package com.google.android.exoplayer2.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.CaptioningManager;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.util.Util;
import java.util.ArrayList;
import java.util.List;

public final class SubtitleView extends View implements TextOutput {

    /* renamed from: a  reason: collision with root package name */
    private final List<SubtitlePainter> f3580a;
    private List<Cue> b;
    private int c;
    private float d;
    private boolean e;
    private boolean f;
    private CaptionStyleCompat g;
    private float h;

    public SubtitleView(Context context) {
        this(context, (AttributeSet) null);
    }

    @TargetApi(19)
    private float getUserCaptionFontScaleV19() {
        return ((CaptioningManager) getContext().getSystemService("captioning")).getFontScale();
    }

    @TargetApi(19)
    private CaptionStyleCompat getUserCaptionStyleV19() {
        return CaptionStyleCompat.a(((CaptioningManager) getContext().getSystemService("captioning")).getUserStyle());
    }

    public void a(List<Cue> list) {
        setCues(list);
    }

    public void b() {
        setFractionalTextSize(((Util.f3651a < 19 || isInEditMode()) ? 1.0f : getUserCaptionFontScaleV19()) * 0.0533f);
    }

    public void dispatchDraw(Canvas canvas) {
        float f2;
        List<Cue> list = this.b;
        int i = 0;
        int size = list == null ? 0 : list.size();
        int top = getTop();
        int bottom = getBottom();
        int left = getLeft() + getPaddingLeft();
        int paddingTop = getPaddingTop() + top;
        int right = getRight() + getPaddingRight();
        int paddingBottom = bottom - getPaddingBottom();
        if (paddingBottom > paddingTop && right > left) {
            int i2 = this.c;
            if (i2 == 2) {
                f2 = this.d;
            } else {
                f2 = this.d * ((float) (i2 == 0 ? paddingBottom - paddingTop : bottom - top));
            }
            if (f2 > 0.0f) {
                while (i < size) {
                    int i3 = paddingBottom;
                    int i4 = right;
                    this.f3580a.get(i).a(this.b.get(i), this.e, this.f, this.g, f2, this.h, canvas, left, paddingTop, i4, i3);
                    i++;
                    paddingBottom = i3;
                    right = i4;
                }
            }
        }
    }

    public void setApplyEmbeddedFontSizes(boolean z) {
        if (this.f != z) {
            this.f = z;
            invalidate();
        }
    }

    public void setApplyEmbeddedStyles(boolean z) {
        if (this.e != z || this.f != z) {
            this.e = z;
            this.f = z;
            invalidate();
        }
    }

    public void setBottomPaddingFraction(float f2) {
        if (this.h != f2) {
            this.h = f2;
            invalidate();
        }
    }

    public void setCues(List<Cue> list) {
        int i;
        if (this.b != list) {
            this.b = list;
            if (list == null) {
                i = 0;
            } else {
                i = list.size();
            }
            while (this.f3580a.size() < i) {
                this.f3580a.add(new SubtitlePainter(getContext()));
            }
            invalidate();
        }
    }

    public void setFractionalTextSize(float f2) {
        a(f2, false);
    }

    public void setStyle(CaptionStyleCompat captionStyleCompat) {
        if (this.g != captionStyleCompat) {
            this.g = captionStyleCompat;
            invalidate();
        }
    }

    public SubtitleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3580a = new ArrayList();
        this.c = 0;
        this.d = 0.0533f;
        this.e = true;
        this.f = true;
        this.g = CaptionStyleCompat.g;
        this.h = 0.08f;
    }

    public void a(float f2, boolean z) {
        a(z ? 1 : 0, f2);
    }

    private void a(int i, float f2) {
        if (this.c != i || this.d != f2) {
            this.c = i;
            this.d = f2;
            invalidate();
        }
    }

    public void a() {
        setStyle((Util.f3651a < 19 || isInEditMode()) ? CaptionStyleCompat.g : getUserCaptionStyleV19());
    }
}
