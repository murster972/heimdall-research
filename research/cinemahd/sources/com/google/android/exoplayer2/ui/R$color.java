package com.google.android.exoplayer2.ui;

public final class R$color {
    public static final int exo_edit_mode_background_color = 2131099789;
    public static final int exo_error_message_background_color = 2131099790;
    public static final int notification_action_color_filter = 2131100106;
    public static final int notification_icon_bg_color = 2131100107;
    public static final int notification_material_background_media_default_color = 2131100108;
    public static final int primary_text_default_material_dark = 2131100116;
    public static final int ripple_material_light = 2131100121;
    public static final int secondary_text_default_material_dark = 2131100123;
    public static final int secondary_text_default_material_light = 2131100124;

    private R$color() {
    }
}
