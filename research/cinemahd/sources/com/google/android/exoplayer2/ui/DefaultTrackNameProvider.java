package com.google.android.exoplayer2.ui;

import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import java.util.Locale;

public class DefaultTrackNameProvider implements TrackNameProvider {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f3571a;

    public DefaultTrackNameProvider(Resources resources) {
        Assertions.a(resources);
        this.f3571a = resources;
    }

    private String b(Format format) {
        int i = format.t;
        if (i == -1 || i < 1) {
            return "";
        }
        if (i == 1) {
            return this.f3571a.getString(R$string.exo_track_mono);
        }
        if (i == 2) {
            return this.f3571a.getString(R$string.exo_track_stereo);
        }
        if (i == 6 || i == 7) {
            return this.f3571a.getString(R$string.exo_track_surround_5_point_1);
        }
        if (i != 8) {
            return this.f3571a.getString(R$string.exo_track_surround);
        }
        return this.f3571a.getString(R$string.exo_track_surround_7_point_1);
    }

    private String c(Format format) {
        int i = format.c;
        if (i == -1) {
            return "";
        }
        return this.f3571a.getString(R$string.exo_track_bitrate, new Object[]{Float.valueOf(((float) i) / 1000000.0f)});
    }

    private String d(Format format) {
        String str = format.z;
        return (TextUtils.isEmpty(str) || "und".equals(str)) ? "" : a(str);
    }

    private String e(Format format) {
        int i = format.l;
        int i2 = format.m;
        if (i == -1 || i2 == -1) {
            return "";
        }
        return this.f3571a.getString(R$string.exo_track_resolution, new Object[]{Integer.valueOf(i), Integer.valueOf(i2)});
    }

    private static int f(Format format) {
        int f = MimeTypes.f(format.g);
        if (f != -1) {
            return f;
        }
        if (MimeTypes.i(format.d) != null) {
            return 2;
        }
        if (MimeTypes.a(format.d) != null) {
            return 1;
        }
        if (format.l != -1 || format.m != -1) {
            return 2;
        }
        if (format.t == -1 && format.u == -1) {
            return -1;
        }
        return 1;
    }

    public String a(Format format) {
        String str;
        int f = f(format);
        if (f == 2) {
            str = a(e(format), c(format));
        } else if (f == 1) {
            str = a(d(format), b(format), c(format));
        } else {
            str = d(format);
        }
        return str.length() == 0 ? this.f3571a.getString(R$string.exo_track_unknown) : str;
    }

    private String a(String str) {
        return (Util.f3651a >= 21 ? Locale.forLanguageTag(str) : new Locale(str)).getDisplayLanguage();
    }

    private String a(String... strArr) {
        String str = "";
        for (String str2 : strArr) {
            if (str2.length() > 0) {
                if (TextUtils.isEmpty(str)) {
                    str = str2;
                } else {
                    str = this.f3571a.getString(R$string.exo_item_list, new Object[]{str, str2});
                }
            }
        }
        return str;
    }
}
