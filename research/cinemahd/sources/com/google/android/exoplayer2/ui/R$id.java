package com.google.android.exoplayer2.ui;

public final class R$id {
    public static final int action0 = 2131296312;
    public static final int action_container = 2131296320;
    public static final int action_divider = 2131296323;
    public static final int action_image = 2131296324;
    public static final int action_text = 2131296334;
    public static final int actions = 2131296335;
    public static final int async = 2131296384;
    public static final int blocking = 2131296406;
    public static final int cancel_action = 2131296461;
    public static final int chronometer = 2131296502;
    public static final int end_padder = 2131296588;
    public static final int exo_artwork = 2131296595;
    public static final int exo_buffering = 2131296596;
    public static final int exo_cc = 2131296597;
    public static final int exo_content_frame = 2131296598;
    public static final int exo_controller = 2131296599;
    public static final int exo_controller_placeholder = 2131296600;
    public static final int exo_duration = 2131296601;
    public static final int exo_error_message = 2131296602;
    public static final int exo_ffwd = 2131296603;
    public static final int exo_next = 2131296604;
    public static final int exo_overlay = 2131296605;
    public static final int exo_pause = 2131296606;
    public static final int exo_play = 2131296607;
    public static final int exo_position = 2131296608;
    public static final int exo_prev = 2131296609;
    public static final int exo_progress = 2131296610;
    public static final int exo_repeat_toggle = 2131296611;
    public static final int exo_resize = 2131296612;
    public static final int exo_rew = 2131296613;
    public static final int exo_shuffle = 2131296614;
    public static final int exo_shutter = 2131296615;
    public static final int exo_subtitles = 2131296616;
    public static final int exo_track_selection_view = 2131296617;
    public static final int fill = 2131296632;
    public static final int fit = 2131296636;
    public static final int fixed_height = 2131296643;
    public static final int fixed_width = 2131296644;
    public static final int forever = 2131296650;
    public static final int icon = 2131296680;
    public static final int icon_group = 2131296682;

    /* renamed from: info  reason: collision with root package name */
    public static final int f3578info = 2131296704;
    public static final int italic = 2131296711;
    public static final int line1 = 2131296747;
    public static final int line3 = 2131296748;
    public static final int media_actions = 2131296777;
    public static final int none = 2131296922;
    public static final int normal = 2131296923;
    public static final int notification_background = 2131296925;
    public static final int notification_main_column = 2131296926;
    public static final int notification_main_column_container = 2131296927;
    public static final int right_icon = 2131296986;
    public static final int right_side = 2131296987;
    public static final int status_bar_latest_event_content = 2131297071;
    public static final int surface_view = 2131297081;
    public static final int tag_transition_group = 2131297103;
    public static final int tag_unhandled_key_event_manager = 2131297104;
    public static final int tag_unhandled_key_listeners = 2131297105;
    public static final int text = 2131297106;
    public static final int text2 = 2131297108;
    public static final int texture_view = 2131297145;
    public static final int time = 2131297150;
    public static final int title = 2131297151;
    public static final int zoom = 2131297245;

    private R$id() {
    }
}
