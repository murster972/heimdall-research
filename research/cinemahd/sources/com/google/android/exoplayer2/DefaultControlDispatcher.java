package com.google.android.exoplayer2;

public class DefaultControlDispatcher implements ControlDispatcher {
    public boolean a(Player player, int i, long j) {
        player.a(i, j);
        return true;
    }

    public boolean b(Player player, boolean z) {
        player.a(z);
        return true;
    }

    public boolean a(Player player, int i) {
        player.setRepeatMode(i);
        return true;
    }

    public boolean a(Player player, boolean z) {
        player.b(z);
        return true;
    }
}
