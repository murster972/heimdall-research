package com.google.android.exoplayer2;

import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import com.google.android.exoplayer2.util.Util;
import com.google.ar.core.ImageMetadata;

public class DefaultLoadControl implements LoadControl {

    /* renamed from: a  reason: collision with root package name */
    private final DefaultAllocator f3152a;
    private final long b;
    private final long c;
    private final long d;
    private final long e;
    private final int f;
    private final boolean g;
    private final PriorityTaskManager h;
    private final long i;
    private final boolean j;
    private int k;
    private boolean l;

    public DefaultLoadControl() {
        this(new DefaultAllocator(true, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE));
    }

    public void a() {
        a(false);
    }

    public boolean b() {
        return this.j;
    }

    public long c() {
        return this.i;
    }

    public Allocator d() {
        return this.f3152a;
    }

    public void e() {
        a(true);
    }

    public void onStopped() {
        a(true);
    }

    @Deprecated
    public DefaultLoadControl(DefaultAllocator defaultAllocator) {
        this(defaultAllocator, 15000, 50000, 2500, 5000, -1, true);
    }

    public void a(Renderer[] rendererArr, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        int i2 = this.f;
        if (i2 == -1) {
            i2 = a(rendererArr, trackSelectionArray);
        }
        this.k = i2;
        this.f3152a.a(this.k);
    }

    @Deprecated
    public DefaultLoadControl(DefaultAllocator defaultAllocator, int i2, int i3, int i4, int i5, int i6, boolean z) {
        this(defaultAllocator, i2, i3, i4, i5, i6, z, (PriorityTaskManager) null);
    }

    @Deprecated
    public DefaultLoadControl(DefaultAllocator defaultAllocator, int i2, int i3, int i4, int i5, int i6, boolean z, PriorityTaskManager priorityTaskManager) {
        this(defaultAllocator, i2, i3, i4, i5, i6, z, priorityTaskManager, 0, false);
    }

    protected DefaultLoadControl(DefaultAllocator defaultAllocator, int i2, int i3, int i4, int i5, int i6, boolean z, PriorityTaskManager priorityTaskManager, int i7, boolean z2) {
        a(i4, 0, "bufferForPlaybackMs", "0");
        a(i5, 0, "bufferForPlaybackAfterRebufferMs", "0");
        a(i2, i4, "minBufferMs", "bufferForPlaybackMs");
        a(i2, i5, "minBufferMs", "bufferForPlaybackAfterRebufferMs");
        a(i3, i2, "maxBufferMs", "minBufferMs");
        a(i7, 0, "backBufferDurationMs", "0");
        this.f3152a = defaultAllocator;
        this.b = C.a((long) i2);
        this.c = C.a((long) i3);
        this.d = C.a((long) i4);
        this.e = C.a((long) i5);
        this.f = i6;
        this.g = z;
        this.h = priorityTaskManager;
        this.i = C.a((long) i7);
        this.j = z2;
    }

    public boolean a(long j2, float f2) {
        boolean z;
        boolean z2 = true;
        boolean z3 = this.f3152a.d() >= this.k;
        boolean z4 = this.l;
        long j3 = this.b;
        if (f2 > 1.0f) {
            j3 = Math.min(Util.a(j3, f2), this.c);
        }
        if (j2 < j3) {
            if (!this.g && z3) {
                z2 = false;
            }
            this.l = z2;
        } else if (j2 >= this.c || z3) {
            this.l = false;
        }
        PriorityTaskManager priorityTaskManager = this.h;
        if (!(priorityTaskManager == null || (z = this.l) == z4)) {
            if (z) {
                priorityTaskManager.a(0);
            } else {
                priorityTaskManager.d(0);
            }
        }
        return this.l;
    }

    public boolean a(long j2, float f2, boolean z) {
        long b2 = Util.b(j2, f2);
        long j3 = z ? this.e : this.d;
        return j3 <= 0 || b2 >= j3 || (!this.g && this.f3152a.d() >= this.k);
    }

    /* access modifiers changed from: protected */
    public int a(Renderer[] rendererArr, TrackSelectionArray trackSelectionArray) {
        int i2 = 0;
        for (int i3 = 0; i3 < rendererArr.length; i3++) {
            if (trackSelectionArray.a(i3) != null) {
                i2 += Util.b(rendererArr[i3].getTrackType());
            }
        }
        return i2;
    }

    private void a(boolean z) {
        this.k = 0;
        PriorityTaskManager priorityTaskManager = this.h;
        if (priorityTaskManager != null && this.l) {
            priorityTaskManager.d(0);
        }
        this.l = false;
        if (z) {
            this.f3152a.e();
        }
    }

    private static void a(int i2, int i3, String str, String str2) {
        boolean z = i2 >= i3;
        Assertions.a(z, str + " cannot be less than " + str2);
    }
}
