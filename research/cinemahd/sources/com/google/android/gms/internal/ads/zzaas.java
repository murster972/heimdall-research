package com.google.android.gms.internal.ads;

public final class zzaas<T> extends zzaan<T> {
    /* JADX WARNING: type inference failed for: r3v0, types: [int, java.lang.Integer] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected zzaas(java.lang.String r1, T r2, java.lang.Integer r3) {
        /*
            r0 = this;
            r0.<init>(r1, r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzaas.<init>(java.lang.String, java.lang.Object, int):void");
    }

    public static zzaan<Boolean> zzf(String str, boolean z) {
        return new zzaas(str, true, zzaap.zzcsh);
    }

    public final T get() {
        if (zzabn.zzcuz.get()) {
            return super.get();
        }
        throw new IllegalStateException("Striped code is accessed: 54c42518-856a-44fb-aae0-cd6676d514e5");
    }
}
