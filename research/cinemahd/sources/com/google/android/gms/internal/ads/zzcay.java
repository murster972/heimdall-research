package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import java.util.concurrent.Executor;

public final class zzcay implements zzdxg<zzcat> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzazb> zzfhn;
    private final zzdxp<zzdq> zzfky;
    private final zzdxp<zza> zzfpp;
    private final zzdxp<zzbdr> zzfqw;

    public zzcay(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzdq> zzdxp3, zzdxp<zzazb> zzdxp4, zzdxp<zza> zzdxp5, zzdxp<zzbdr> zzdxp6) {
        this.zzejv = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfky = zzdxp3;
        this.zzfhn = zzdxp4;
        this.zzfpp = zzdxp5;
        this.zzfqw = zzdxp6;
    }

    public final /* synthetic */ Object get() {
        return new zzcat(this.zzejv.get(), this.zzfei.get(), this.zzfky.get(), this.zzfhn.get(), this.zzfpp.get(), this.zzfqw.get());
    }
}
