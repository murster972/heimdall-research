package com.google.android.gms.internal.ads;

final /* synthetic */ class zzatz implements Runnable {
    private final String zzdbl;
    private final zzatv zzdps;
    private final zzaul zzdpt;

    zzatz(zzatv zzatv, zzaul zzaul, String str) {
        this.zzdps = zzatv;
        this.zzdpt = zzaul;
        this.zzdbl = str;
    }

    public final void run() {
        this.zzdps.zza(this.zzdpt, this.zzdbl);
    }
}
