package com.google.android.gms.internal.ads;

final class zzdez extends zzdeu<E> {
    private final transient int length;
    private final transient int offset;
    private final /* synthetic */ zzdeu zzguo;

    zzdez(zzdeu zzdeu, int i, int i2) {
        this.zzguo = zzdeu;
        this.offset = i;
        this.length = i2;
    }

    public final E get(int i) {
        zzdei.zzs(i, this.length);
        return this.zzguo.get(i + this.offset);
    }

    public final int size() {
        return this.length;
    }

    /* access modifiers changed from: package-private */
    public final Object[] zzaqy() {
        return this.zzguo.zzaqy();
    }

    /* access modifiers changed from: package-private */
    public final int zzaqz() {
        return this.zzguo.zzaqz() + this.offset;
    }

    /* access modifiers changed from: package-private */
    public final int zzara() {
        return this.zzguo.zzaqz() + this.offset + this.length;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return true;
    }

    /* renamed from: zzu */
    public final zzdeu<E> subList(int i, int i2) {
        zzdei.zzf(i, i2, this.length);
        zzdeu zzdeu = this.zzguo;
        int i3 = this.offset;
        return (zzdeu) zzdeu.subList(i + i3, i2 + i3);
    }
}
