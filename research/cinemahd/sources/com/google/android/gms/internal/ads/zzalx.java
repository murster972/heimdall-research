package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.Adapter;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;

final class zzalx implements MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> {
    private final /* synthetic */ zzali zzddk;
    private final /* synthetic */ Adapter zzddl;
    private final /* synthetic */ zzaly zzddm;

    zzalx(zzaly zzaly, zzali zzali, Adapter adapter) {
        this.zzddm = zzaly;
        this.zzddk = zzali;
        this.zzddl = adapter;
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final MediationRewardedAdCallback onSuccess(MediationRewardedAd mediationRewardedAd) {
        try {
            MediationRewardedAd unused = this.zzddm.zzddr = mediationRewardedAd;
            this.zzddk.onAdLoaded();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
        return new zzatd(this.zzddk);
    }

    public final void onFailure(String str) {
        try {
            String canonicalName = this.zzddl.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 30 + String.valueOf(str).length());
            sb.append(canonicalName);
            sb.append("failed to loaded medation ad: ");
            sb.append(str);
            zzayu.zzea(sb.toString());
            this.zzddk.onAdFailedToLoad(0);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
