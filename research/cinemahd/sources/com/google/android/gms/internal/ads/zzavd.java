package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.common.util.Clock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public final class zzavd {
    private final Object lock = new Object();
    /* access modifiers changed from: private */
    public final Clock zzbmq;
    private final String zzdja;
    private boolean zzdkk = false;
    private long zzdko = -1;
    private final zzavp zzdpz;
    private final LinkedList<zzavc> zzdqa;
    private final String zzdqb;
    private long zzdqc = -1;
    private long zzdqd = -1;
    private long zzdqe = 0;
    private long zzdqf = -1;
    private long zzdqg = -1;

    zzavd(Clock clock, zzavp zzavp, String str, String str2) {
        this.zzbmq = clock;
        this.zzdpz = zzavp;
        this.zzdqb = str;
        this.zzdja = str2;
        this.zzdqa = new LinkedList<>();
    }

    public final Bundle toBundle() {
        Bundle bundle;
        synchronized (this.lock) {
            bundle = new Bundle();
            bundle.putString("seq_num", this.zzdqb);
            bundle.putString("slotid", this.zzdja);
            bundle.putBoolean("ismediation", false);
            bundle.putLong("treq", this.zzdqf);
            bundle.putLong("tresponse", this.zzdqg);
            bundle.putLong("timp", this.zzdqc);
            bundle.putLong("tload", this.zzdqd);
            bundle.putLong("pcc", this.zzdqe);
            bundle.putLong("tfetch", this.zzdko);
            ArrayList arrayList = new ArrayList();
            Iterator it2 = this.zzdqa.iterator();
            while (it2.hasNext()) {
                arrayList.add(((zzavc) it2.next()).toBundle());
            }
            bundle.putParcelableArrayList("tclick", arrayList);
        }
        return bundle;
    }

    public final void zzan(boolean z) {
        synchronized (this.lock) {
            if (this.zzdqg != -1) {
                this.zzdqd = this.zzbmq.a();
            }
        }
    }

    public final void zze(zzug zzug) {
        synchronized (this.lock) {
            this.zzdqf = this.zzbmq.a();
            this.zzdpz.zza(zzug, this.zzdqf);
        }
    }

    public final void zzey(long j) {
        synchronized (this.lock) {
            this.zzdqg = j;
            if (this.zzdqg != -1) {
                this.zzdpz.zzb(this);
            }
        }
    }

    public final void zzuv() {
        synchronized (this.lock) {
            if (this.zzdqg != -1 && this.zzdqc == -1) {
                this.zzdqc = this.zzbmq.a();
                this.zzdpz.zzb(this);
            }
            this.zzdpz.zzuv();
        }
    }

    public final void zzuw() {
        synchronized (this.lock) {
            if (this.zzdqg != -1) {
                zzavc zzavc = new zzavc(this);
                zzavc.zzuu();
                this.zzdqa.add(zzavc);
                this.zzdqe++;
                this.zzdpz.zzuw();
                this.zzdpz.zzb(this);
            }
        }
    }

    public final void zzux() {
        synchronized (this.lock) {
            if (this.zzdqg != -1 && !this.zzdqa.isEmpty()) {
                zzavc last = this.zzdqa.getLast();
                if (last.zzus() == -1) {
                    last.zzut();
                    this.zzdpz.zzb(this);
                }
            }
        }
    }

    public final String zzuy() {
        return this.zzdqb;
    }
}
