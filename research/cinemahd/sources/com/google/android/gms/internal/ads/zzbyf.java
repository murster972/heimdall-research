package com.google.android.gms.internal.ads;

public final class zzbyf implements zzdxg<zzbxa> {
    private final zzdxp<zzbye> zzetg;
    private final zzbyg zzfon;

    public zzbyf(zzbyg zzbyg, zzdxp<zzbye> zzdxp) {
        this.zzfon = zzbyg;
        this.zzetg = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbxa) zzdxm.zza(this.zzetg.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
