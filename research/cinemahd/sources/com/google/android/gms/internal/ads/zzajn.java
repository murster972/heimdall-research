package com.google.android.gms.internal.ads;

final /* synthetic */ class zzajn implements Runnable {
    private final zzajo zzdah;
    private final zzaif zzdai;

    zzajn(zzajo zzajo, zzaif zzaif) {
        this.zzdah = zzajo;
        this.zzdai = zzaif;
    }

    public final void run() {
        zzajo zzajo = this.zzdah;
        zzaif zzaif = this.zzdai;
        zzajo.zzdaj.zzczn.zzh(zzaif);
        zzaif.destroy();
    }
}
