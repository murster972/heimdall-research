package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzccj implements zzdxg<zzsm> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Integer> zzfdc;
    private final zzdxp<zzazb> zzfhn;
    private final zzdxp<String> zzfrr;
    private final zzdxp<String> zzfrs;

    private zzccj(zzdxp<Context> zzdxp, zzdxp<String> zzdxp2, zzdxp<zzazb> zzdxp3, zzdxp<Integer> zzdxp4, zzdxp<String> zzdxp5) {
        this.zzejv = zzdxp;
        this.zzfrr = zzdxp2;
        this.zzfhn = zzdxp3;
        this.zzfdc = zzdxp4;
        this.zzfrs = zzdxp5;
    }

    public static zzccj zze(zzdxp<Context> zzdxp, zzdxp<String> zzdxp2, zzdxp<zzazb> zzdxp3, zzdxp<Integer> zzdxp4, zzdxp<String> zzdxp5) {
        return new zzccj(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5);
    }

    public final /* synthetic */ Object get() {
        String str = this.zzfrr.get();
        zzazb zzazb = this.zzfhn.get();
        int intValue = this.zzfdc.get().intValue();
        String str2 = this.zzfrs.get();
        zzsm zzsm = new zzsm(new zzsr(this.zzejv.get()));
        zztt zztt = new zztt();
        zztt.zzcam = Integer.valueOf(zzazb.zzdvz);
        zztt.zzcan = Integer.valueOf(zzazb.zzdwa);
        zztt.zzcao = Integer.valueOf(zzazb.zzdwb ? 0 : 2);
        zzsm.zza((zzsp) new zzcck(intValue, str, zztt, str2));
        return (zzsm) zzdxm.zza(zzsm, "Cannot return null from a non-@Nullable @Provides method");
    }
}
