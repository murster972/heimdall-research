package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzwd extends IInterface {
    zzacm zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) throws RemoteException;

    zzacp zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) throws RemoteException;

    zzarl zza(IObjectWrapper iObjectWrapper, zzalc zzalc, int i) throws RemoteException;

    zzvn zza(IObjectWrapper iObjectWrapper, String str, zzalc zzalc, int i) throws RemoteException;

    zzvu zza(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, int i) throws RemoteException;

    zzvu zza(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, zzalc zzalc, int i) throws RemoteException;

    zzwk zza(IObjectWrapper iObjectWrapper, int i) throws RemoteException;

    zzaot zzb(IObjectWrapper iObjectWrapper) throws RemoteException;

    zzasg zzb(IObjectWrapper iObjectWrapper, String str, zzalc zzalc, int i) throws RemoteException;

    zzvu zzb(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, zzalc zzalc, int i) throws RemoteException;

    zzvu zzc(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, zzalc zzalc, int i) throws RemoteException;

    zzwk zzc(IObjectWrapper iObjectWrapper) throws RemoteException;

    zzapd zzd(IObjectWrapper iObjectWrapper) throws RemoteException;
}
