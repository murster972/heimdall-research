package com.google.android.gms.internal.ads;

final class zzbab implements Runnable {
    private final /* synthetic */ zzazx zzdxn;
    private final /* synthetic */ String zzdxo;
    private final /* synthetic */ String zzdxp;

    zzbab(zzazx zzazx, String str, String str2) {
        this.zzdxn = zzazx;
        this.zzdxo = str;
        this.zzdxp = str2;
    }

    public final void run() {
        if (this.zzdxn.zzdxm != null) {
            this.zzdxn.zzdxm.zzm(this.zzdxo, this.zzdxp);
        }
    }
}
