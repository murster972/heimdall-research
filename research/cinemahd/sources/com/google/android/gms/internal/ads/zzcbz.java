package com.google.android.gms.internal.ads;

final class zzcbz implements zzaon {
    private final /* synthetic */ zzcbp zzfrk;

    zzcbz(zzcbp zzcbp) {
        this.zzfrk = zzcbp;
    }

    public final void zza(int i, int i2, int i3, int i4) {
        this.zzfrk.zzfgg.onAdOpened();
    }

    public final void zzti() {
        this.zzfrk.zzfgg.onAdClosed();
    }

    public final void zztj() {
        this.zzfrk.zzfrh.zzagj();
    }
}
