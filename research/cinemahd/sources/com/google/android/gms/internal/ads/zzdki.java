package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdki implements zzdis<zzdie> {
    zzdki() {
    }

    public final /* synthetic */ Object zza(zzdiq zzdiq) throws GeneralSecurityException {
        return new zzdkl(zzdiq);
    }

    public final Class<zzdie> zzarz() {
        return zzdie.class;
    }
}
