package com.google.android.gms.internal.ads;

import android.view.View;

final class zzbfc implements View.OnAttachStateChangeListener {
    private final /* synthetic */ zzato zzeev;
    private final /* synthetic */ zzbfb zzehv;

    zzbfc(zzbfb zzbfb, zzato zzato) {
        this.zzehv = zzbfb;
        this.zzeev = zzato;
    }

    public final void onViewAttachedToWindow(View view) {
        this.zzehv.zza(view, this.zzeev, 10);
    }

    public final void onViewDetachedFromWindow(View view) {
    }
}
