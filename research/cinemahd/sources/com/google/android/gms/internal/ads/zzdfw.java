package com.google.android.gms.internal.ads;

final class zzdfw<I, O> extends zzdfu<I, O, zzded<? super I, ? extends O>, O> {
    zzdfw(zzdhe<? extends I> zzdhe, zzded<? super I, ? extends O> zzded) {
        super(zzdhe, zzded);
    }

    /* access modifiers changed from: package-private */
    public final void setResult(O o) {
        set(o);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzc(Object obj, Object obj2) throws Exception {
        return ((zzded) obj).apply(obj2);
    }
}
