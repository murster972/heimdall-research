package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdma extends zzdrt<zzdma, zza> implements zzdtg {
    private static volatile zzdtn<zzdma> zzdz;
    /* access modifiers changed from: private */
    public static final zzdma zzhbb;

    public static final class zza extends zzdrt.zzb<zzdma, zza> implements zzdtg {
        private zza() {
            super(zzdma.zzhbb);
        }

        /* synthetic */ zza(zzdmb zzdmb) {
            this();
        }
    }

    static {
        zzdma zzdma = new zzdma();
        zzhbb = zzdma;
        zzdrt.zza(zzdma.class, zzdma);
    }

    private zzdma() {
    }

    public static zzdma zzai(zzdqk zzdqk) throws zzdse {
        return (zzdma) zzdrt.zza(zzhbb, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmb.zzdk[i - 1]) {
            case 1:
                return new zzdma();
            case 2:
                return new zza((zzdmb) null);
            case 3:
                return zzdrt.zza((zzdte) zzhbb, "\u0000\u0000", (Object[]) null);
            case 4:
                return zzhbb;
            case 5:
                zzdtn<zzdma> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdma.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbb);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
