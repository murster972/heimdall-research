package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.ArrayList;

public final class zzcpu implements zzcub<zzcpv> {
    private final zzczu zzfgl;
    private final zzdhd zzgej;
    private final View zzgek;
    private final Context zzup;

    public zzcpu(zzdhd zzdhd, Context context, zzczu zzczu, ViewGroup viewGroup) {
        this.zzgej = zzdhd;
        this.zzup = context;
        this.zzfgl = zzczu;
        this.zzgek = viewGroup;
    }

    public final zzdhe<zzcpv> zzanc() {
        return this.zzgej.zzd(new zzcpx(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcpv zzand() throws Exception {
        Context context = this.zzup;
        zzuj zzuj = this.zzfgl.zzblm;
        ArrayList arrayList = new ArrayList();
        View view = this.zzgek;
        while (view != null) {
            ViewParent parent = view.getParent();
            if (parent == null) {
                break;
            }
            int i = -1;
            if (parent instanceof ViewGroup) {
                i = ((ViewGroup) parent).indexOfChild(view);
            }
            Bundle bundle = new Bundle();
            bundle.putString("type", parent.getClass().getName());
            bundle.putInt("index_of_child", i);
            arrayList.add(bundle);
            if (!(parent instanceof View)) {
                break;
            }
            view = (View) parent;
        }
        return new zzcpv(context, zzuj, arrayList);
    }
}
