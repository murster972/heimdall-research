package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;

public interface zzbfy extends MediationInterstitialAdapter {
    Bundle getInterstitialAdapterInfo();
}
