package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzckp implements zzdxg<zzckl> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbup> zzfyl;

    public zzckp(zzdxp<Context> zzdxp, zzdxp<zzbup> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfyl = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzckl(this.zzejv.get(), this.zzfyl.get());
    }
}
