package com.google.android.gms.internal.ads;

public interface zzad {
    int zza();

    void zza(zzae zzae) throws zzae;

    int zzb();
}
