package com.google.android.gms.internal.ads;

final class zzkd {
    public int index;
    public final int length;
    public int zzaup;
    public long zzauq;
    private final boolean zzaur;
    private final zzoj zzaus;
    private final zzoj zzaut;
    private int zzauu;
    private int zzauv;

    public zzkd(zzoj zzoj, zzoj zzoj2, boolean z) {
        this.zzaut = zzoj;
        this.zzaus = zzoj2;
        this.zzaur = z;
        zzoj2.zzbe(12);
        this.length = zzoj2.zzis();
        zzoj.zzbe(12);
        this.zzauv = zzoj.zzis();
        zzoc.checkState(zzoj.readInt() != 1 ? false : true, "first_chunk must be 1");
        this.index = -1;
    }

    public final boolean zzgp() {
        long j;
        int i = this.index + 1;
        this.index = i;
        if (i == this.length) {
            return false;
        }
        if (this.zzaur) {
            j = this.zzaus.zzit();
        } else {
            j = this.zzaus.zzip();
        }
        this.zzauq = j;
        if (this.index == this.zzauu) {
            this.zzaup = this.zzaut.zzis();
            this.zzaut.zzbf(4);
            int i2 = this.zzauv - 1;
            this.zzauv = i2;
            this.zzauu = i2 > 0 ? this.zzaut.zzis() - 1 : -1;
        }
        return true;
    }
}
