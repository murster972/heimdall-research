package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdko extends zzdik<zzdio, zzdky> {
    zzdko(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdky zzdky = (zzdky) obj;
        return new zzdod(zzdky.zzass().toByteArray(), zzdky.zzast().zzasx());
    }
}
