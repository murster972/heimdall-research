package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import com.google.android.gms.internal.ads.zzdnk;
import com.google.android.gms.internal.ads.zzdnl;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;

final class zzdiz {
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    public static zzdnl zzb(zzdnk zzdnk) {
        zzdnl.zza zzer = zzdnl.zzawd().zzer(zzdnk.zzavv());
        for (zzdnk.zza next : zzdnk.zzavw()) {
            zzer.zzb((zzdnl.zzb) zzdnl.zzb.zzawf().zzhc(next.zzawa().zzavi()).zza(next.zzasj()).zza(next.zzask()).zzes(next.zzawb()).zzbaf());
        }
        return (zzdnl) zzer.zzbaf();
    }

    public static void zzc(zzdnk zzdnk) throws GeneralSecurityException {
        int zzavv = zzdnk.zzavv();
        int i = 0;
        boolean z = false;
        boolean z2 = true;
        for (zzdnk.zza next : zzdnk.zzavw()) {
            if (next.zzasj() == zzdne.ENABLED) {
                if (!next.zzavz()) {
                    throw new GeneralSecurityException(String.format("key %d has no key data", new Object[]{Integer.valueOf(next.zzawb())}));
                } else if (next.zzask() == zzdnw.UNKNOWN_PREFIX) {
                    throw new GeneralSecurityException(String.format("key %d has unknown prefix", new Object[]{Integer.valueOf(next.zzawb())}));
                } else if (next.zzasj() != zzdne.UNKNOWN_STATUS) {
                    if (next.zzawb() == zzavv) {
                        if (!z) {
                            z = true;
                        } else {
                            throw new GeneralSecurityException("keyset contains multiple primary keys");
                        }
                    }
                    if (next.zzawa().zzavk() != zzdna.zzb.ASYMMETRIC_PUBLIC) {
                        z2 = false;
                    }
                    i++;
                } else {
                    throw new GeneralSecurityException(String.format("key %d has unknown status", new Object[]{Integer.valueOf(next.zzawb())}));
                }
            }
        }
        if (i == 0) {
            throw new GeneralSecurityException("keyset must contain at least one ENABLED key");
        } else if (!z && !z2) {
            throw new GeneralSecurityException("keyset doesn't contain a valid primary key");
        }
    }
}
