package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

final class zzlg implements Parcelable.Creator<zzle> {
    zzlg() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzle(parcel);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzle[0];
    }
}
