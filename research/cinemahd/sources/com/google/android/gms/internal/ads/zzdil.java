package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public final class zzdil {
    private static final CopyOnWriteArrayList<zzdim> zzgxz = new CopyOnWriteArrayList<>();

    public static zzdim zzgu(String str) throws GeneralSecurityException {
        Iterator<zzdim> it2 = zzgxz.iterator();
        while (it2.hasNext()) {
            zzdim next = it2.next();
            if (next.zzgv(str)) {
                return next;
            }
        }
        String valueOf = String.valueOf(str);
        throw new GeneralSecurityException(valueOf.length() != 0 ? "No KMS client does support: ".concat(valueOf) : new String("No KMS client does support: "));
    }
}
