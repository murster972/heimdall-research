package com.google.android.gms.internal.ads;

public final class zzccf implements zzdxg<zzccg> {
    private final zzdxp<zzbdi> zzfef;

    private zzccf(zzdxp<zzbdi> zzdxp) {
        this.zzfef = zzdxp;
    }

    public static zzccf zzy(zzdxp<zzbdi> zzdxp) {
        return new zzccf(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzccg(this.zzfef.get());
    }
}
