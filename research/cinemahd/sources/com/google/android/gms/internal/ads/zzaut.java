package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzaut extends IInterface {
    zzauo zzf(IObjectWrapper iObjectWrapper, int i) throws RemoteException;
}
