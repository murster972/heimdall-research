package com.google.android.gms.internal.cast;

import android.text.format.DateUtils;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class zzbh extends UIController {
    private static int zzte = zzbj.zzth;
    private SimpleDateFormat zztd;

    private final MediaMetadata getMetadata() {
        MediaInfo e;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || (e = remoteMediaClient.e()) == null) {
            return null;
        }
        return e.y();
    }

    private final Long zzdv() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && remoteMediaClient.k() && remoteMediaClient.m()) {
            MediaInfo e = remoteMediaClient.e();
            MediaMetadata metadata = getMetadata();
            if (!(e == null || metadata == null || !metadata.e("com.google.android.gms.cast.metadata.SECTION_START_TIME_IN_MEDIA"))) {
                return Long.valueOf(metadata.g("com.google.android.gms.cast.metadata.SECTION_START_TIME_IN_MEDIA"));
            }
        }
        return null;
    }

    private final Long zzdx() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && remoteMediaClient.k() && remoteMediaClient.m()) {
            MediaStatus f = remoteMediaClient.f();
        }
        return null;
    }

    private final Long zzdy() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && remoteMediaClient.k() && remoteMediaClient.m()) {
            MediaStatus f = remoteMediaClient.f();
        }
        return null;
    }

    private final Long zzea() {
        MediaInfo e;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || !remoteMediaClient.m() || (e = remoteMediaClient.e()) == null || e.E() == -1) {
            return null;
        }
        return Long.valueOf(e.E());
    }

    private static String zzf(long j) {
        if (j >= 0) {
            return DateUtils.formatElapsedTime(j / 1000);
        }
        String valueOf = String.valueOf(DateUtils.formatElapsedTime((-j) / 1000));
        return valueOf.length() != 0 ? "-".concat(valueOf) : new String("-");
    }

    public final int zzdm() {
        return Math.max((int) (zzdu() - zzdt()), 1);
    }

    public final int zzdn() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            return 0;
        }
        if (!remoteMediaClient.m() && remoteMediaClient.n()) {
            return 0;
        }
        int b = (int) (remoteMediaClient.b() - zzdt());
        if (zzdo()) {
            b = zzdk.zzb(b, zzdr(), zzds());
        }
        return zzdk.zzb(b, 0, zzdm());
    }

    public final boolean zzdo() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && remoteMediaClient.k()) {
            if (!remoteMediaClient.m()) {
                return true;
            }
            MediaStatus f = remoteMediaClient.f();
            if (f == null) {
                return false;
            }
            f.h(2);
        }
        return false;
    }

    public final boolean zzdp() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || !zzdo() || (((long) zzdn()) + zzdt()) - (((long) zzdr()) + zzdt()) >= 10000) {
            return false;
        }
        return true;
    }

    public final boolean zzdq() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || !zzdo()) {
            return false;
        }
        if ((((long) zzds()) + zzdt()) - (((long) zzdn()) + zzdt()) < 10000) {
            return true;
        }
        return false;
    }

    public final int zzdr() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || !remoteMediaClient.m()) {
            return 0;
        }
        return zzdk.zzb((int) (zzdx().longValue() - zzdt()), 0, zzdm());
    }

    public final int zzds() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || !remoteMediaClient.m()) {
            return zzdm();
        }
        return zzdk.zzb((int) (zzdy().longValue() - zzdt()), 0, zzdm());
    }

    public final long zzdt() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || !remoteMediaClient.m()) {
            return 0;
        }
        Long zzdv = zzdv();
        if (zzdv != null) {
            return zzdv.longValue();
        }
        Long zzdx = zzdx();
        if (zzdx != null) {
            return zzdx.longValue();
        }
        return remoteMediaClient.b();
    }

    public final long zzdu() {
        MediaInfo v;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            return 1;
        }
        if (remoteMediaClient.m()) {
            Long zzdw = zzdw();
            if (zzdw != null) {
                return zzdw.longValue();
            }
            Long zzdy = zzdy();
            if (zzdy != null) {
                return zzdy.longValue();
            }
            return Math.max(remoteMediaClient.b(), 1);
        } else if (!remoteMediaClient.n()) {
            return Math.max(remoteMediaClient.j(), 1);
        } else {
            MediaQueueItem d = remoteMediaClient.d();
            if (d == null || (v = d.v()) == null) {
                return 1;
            }
            return Math.max(v.z(), 1);
        }
    }

    public final Long zzdw() {
        MediaMetadata metadata;
        Long zzdv;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || !remoteMediaClient.m() || (metadata = getMetadata()) == null || !metadata.e("com.google.android.gms.cast.metadata.SECTION_DURATION") || (zzdv = zzdv()) == null) {
            return null;
        }
        return Long.valueOf(zzdv.longValue() + metadata.g("com.google.android.gms.cast.metadata.SECTION_DURATION"));
    }

    public final int zzdz() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            return zzbj.zztg;
        }
        if (!remoteMediaClient.m() || zzte == zzbj.zztg) {
            return zzbj.zztg;
        }
        if (zzea() != null) {
            return zzbj.zzth;
        }
        return zzbj.zztg;
    }

    public final String zze(long j) {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            return null;
        }
        int i = zzbi.zztf[zzdz() - 1];
        if (i == 1) {
            long longValue = zzea().longValue() + j;
            if (this.zztd == null) {
                this.zztd = new SimpleDateFormat("hh:mm:ss", Locale.US);
            }
            return this.zztd.format(new Date(longValue));
        } else if (i != 2) {
            return null;
        } else {
            if (!remoteMediaClient.m() || zzdv() != null) {
                return zzf(j - zzdt());
            }
            return zzf(j);
        }
    }

    public final long zzo(int i) {
        return ((long) i) + zzdt();
    }
}
