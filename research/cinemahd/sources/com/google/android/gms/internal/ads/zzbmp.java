package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbmp implements zzdgf {
    private final zzbmo zzffz;
    private final zzdgt zzfga;
    private final zzdhe zzfgb;

    zzbmp(zzbmo zzbmo, zzdgt zzdgt, zzdhe zzdhe) {
        this.zzffz = zzbmo;
        this.zzfga = zzdgt;
        this.zzfgb = zzdhe;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzffz.zza(this.zzfga, this.zzfgb, (zzbmd) obj);
    }
}
