package com.google.android.gms.internal.cast;

import android.widget.TextView;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzby extends UIController implements RemoteMediaClient.ProgressListener {
    private final zzbh zzrw;
    private final TextView zzvs;

    public zzby(TextView textView, zzbh zzbh) {
        this.zzvs = textView;
        this.zzrw = zzbh;
        zzdj();
    }

    private final void zzdj() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            TextView textView = this.zzvs;
            textView.setText(textView.getContext().getString(R$string.cast_invalid_stream_duration_text));
            return;
        }
        int i = zzbz.zztf[this.zzrw.zzdz() - 1];
        if (i == 1) {
            TextView textView2 = this.zzvs;
            zzbh zzbh = this.zzrw;
            textView2.setText(zzbh.zze(zzbh.zzo(zzbh.zzdn())));
        } else if (i == 2) {
            TextView textView3 = this.zzvs;
            zzbh zzbh2 = this.zzrw;
            textView3.setText(zzbh2.zze(zzbh2.zzdt()));
        }
    }

    public final void onMediaStatusUpdated() {
        zzdj();
    }

    public final void onProgressUpdated(long j, long j2) {
        zzdj();
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this, 1000);
        }
        zzdj();
    }

    public final void onSessionEnded() {
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this);
        }
        super.onSessionEnded();
        zzdj();
    }
}
