package com.google.android.gms.internal.ads;

import android.graphics.Bitmap;
import com.google.android.gms.internal.ads.zzdvx;
import java.io.ByteArrayOutputStream;

final class zzatg implements Runnable {
    private final /* synthetic */ Bitmap val$bitmap;
    private final /* synthetic */ zzatf zzdop;

    zzatg(zzatf zzatf, Bitmap bitmap) {
        this.zzdop = zzatf;
        this.val$bitmap = bitmap;
    }

    public final void run() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.val$bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        synchronized (this.zzdop.lock) {
            this.zzdop.zzdoe.zzhxy = new zzdwj();
            this.zzdop.zzdoe.zzhxy.zzhyj = byteArrayOutputStream.toByteArray();
            this.zzdop.zzdoe.zzhxy.mimeType = "image/png";
            this.zzdop.zzdoe.zzhxy.zzhyi = zzdvx.zzb.zzf.C0045zzb.TYPE_CREATIVE;
        }
    }
}
