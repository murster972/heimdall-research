package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzys extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzys> CREATOR = new zzyr();
    public final String zzblv;

    public zzys(SearchAdRequest searchAdRequest) {
        this.zzblv = searchAdRequest.getQuery();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 15, this.zzblv, false);
        SafeParcelWriter.a(parcel, a2);
    }

    zzys(String str) {
        this.zzblv = str;
    }
}
