package com.google.android.gms.internal.cast;

import android.animation.Animator;

public final class zzfc extends zzfb {
    protected final Animator animator;
    /* access modifiers changed from: private */
    public final Runnable zzabj;
    private final int zzabk;
    private int zzabl;
    private zzfg zzabm = new zzfd(this);

    private zzfc(Animator animator2, int i, Runnable runnable) {
        this.animator = animator2;
        this.zzabk = -1;
        this.zzabj = null;
    }

    public static void zza(Animator animator2, int i, Runnable runnable) {
        animator2.addListener(new zzfc(animator2, -1, (Runnable) null));
    }

    /* access modifiers changed from: private */
    public final boolean zzfg() {
        int i = this.zzabk;
        if (i != -1 && this.zzabl >= i) {
            return true;
        }
        return false;
    }

    public final void onAnimationEnd(Animator animator2) {
        if (!zzb(animator2)) {
            zzfe.zzfh().zza(this.zzabm);
        }
    }

    static /* synthetic */ int zza(zzfc zzfc) {
        int i = zzfc.zzabl;
        zzfc.zzabl = i + 1;
        return i;
    }
}
