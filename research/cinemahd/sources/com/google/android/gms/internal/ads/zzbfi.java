package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.Map;

final class zzbfi implements zzafn<zzbdi> {
    private final /* synthetic */ zzbfg zzehy;

    zzbfi(zzbfg zzbfg) {
        this.zzehy = zzbfg;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (map != null) {
            String str = (String) map.get("height");
            if (!TextUtils.isEmpty(str)) {
                try {
                    int parseInt = Integer.parseInt(str);
                    synchronized (this.zzehy) {
                        if (this.zzehy.zzegu != parseInt) {
                            int unused = this.zzehy.zzegu = parseInt;
                            this.zzehy.requestLayout();
                        }
                    }
                } catch (Exception e) {
                    zzayu.zzd("Exception occurred while getting webview content height", e);
                }
            }
        }
    }
}
