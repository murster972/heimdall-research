package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcyd implements zzcxo {
    private final String zzcyr;
    private final String zzdbl;
    private final zzare zzfhs;

    zzcyd(zzare zzare, String str, String str2) {
        this.zzfhs = zzare;
        this.zzcyr = str;
        this.zzdbl = str2;
    }

    public final void zzt(Object obj) {
        zzare zzare = this.zzfhs;
        ((zzast) obj).zza(new zzatc(zzare.getType(), zzare.getAmount()), this.zzcyr, this.zzdbl);
    }
}
