package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;

public final class zzcbn {
    private final zzazb zzbll;
    private final zzbdr zzbmj;
    private final zzsm zzeeg;
    private final zzdq zzefv;
    private final zza zzefx;
    private final zzro zzefz;
    private final zzczu zzfgl;
    /* access modifiers changed from: private */
    public final zzbqp zzfre;
    private final Context zzup;

    public zzcbn(zzbdr zzbdr, Context context, zzczu zzczu, zzdq zzdq, zzazb zzazb, zza zza, zzsm zzsm, zzbqp zzbqp, zzbts zzbts) {
        this.zzbmj = zzbdr;
        this.zzup = context;
        this.zzfgl = zzczu;
        this.zzefv = zzdq;
        this.zzbll = zzazb;
        this.zzefx = zza;
        this.zzeeg = zzsm;
        this.zzfre = zzbqp;
        this.zzefz = zzbts;
    }

    public final zzbdi zza(zzuj zzuj, boolean z) throws zzbdv {
        return zzbdr.zza(this.zzup, zzbey.zzb(zzuj), zzuj.zzabg, false, false, this.zzefv, this.zzbll, (zzaae) null, new zzcbq(this), this.zzefx, this.zzeeg, this.zzefz, z);
    }

    public final zzbdi zzc(zzuj zzuj) throws zzbdv {
        return zza(zzuj, false);
    }
}
