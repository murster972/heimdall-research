package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.TimeUnit;

public final class zzbmz<T> {
    private final zzcfx zzfgi;
    private final zzcge zzfgj;
    private final zzdxp<zzdhe<zzaqk>> zzfgk;
    private final zzczu zzfgl;
    private final zzdcr zzfgm;
    private final zzbim zzfgn;
    private final zzclu<T> zzfgo;
    private final zzbrf zzfgp;
    private final zzczt zzfgq;
    private final zzcgu zzfgr;

    zzbmz(zzcfx zzcfx, zzcge zzcge, zzdxp<zzdhe<zzaqk>> zzdxp, zzczu zzczu, zzdcr zzdcr, zzbim zzbim, zzclu<T> zzclu, zzbrf zzbrf, zzczt zzczt, zzcgu zzcgu) {
        this.zzfgi = zzcfx;
        this.zzfgj = zzcge;
        this.zzfgk = zzdxp;
        this.zzfgl = zzczu;
        this.zzfgm = zzdcr;
        this.zzfgn = zzbim;
        this.zzfgo = zzclu;
        this.zzfgp = zzbrf;
        this.zzfgq = zzczt;
        this.zzfgr = zzcgu;
    }

    public final zzdhe<T> zza(zzdhe<zzczt> zzdhe) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcow)).booleanValue()) {
            return this.zzfgm.zza(zzdco.RENDERER, zzdhe).zza(this.zzfgn).zza(this.zzfgo).zzaqg();
        }
        return this.zzfgm.zza(zzdco.RENDERER, zzdhe).zza(this.zzfgn).zza(this.zzfgo).zza((long) ((Integer) zzve.zzoy().zzd(zzzn.zzcox)).intValue(), TimeUnit.SECONDS).zzaqg();
    }

    public final zzdhe<zzczt> zzagz() {
        zzdhe zzdhe = this.zzfgk.get();
        if (this.zzfgq != null) {
            return this.zzfgm.zzu(zzdco.SERVER_TRANSACTION).zzc(zzdgs.zzaj(this.zzfgq)).zzaqg();
        }
        zzq.zzkw().zzmo();
        if (this.zzfgl.zzgml.zzccm != null) {
            return this.zzfgm.zzu(zzdco.SERVER_TRANSACTION).zzc(this.zzfgj.zzalt()).zzaqg();
        }
        return this.zzfgm.zza(zzdco.SERVER_TRANSACTION, zzdhe).zza(this.zzfgi).zzaqg();
    }

    public final zzdhe<T> zzaha() {
        return zza(zzagz());
    }

    public final zzbrf zzahb() {
        return this.zzfgp;
    }
}
