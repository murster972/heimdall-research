package com.google.android.gms.internal.ads;

final class zzkx implements zzky {
    zzkx() {
    }

    public final zzkt zzb(String str, boolean z) throws zzlb {
        return zzla.zzb(str, z);
    }

    public final zzkt zzhb() throws zzlb {
        return zzla.zzhb();
    }
}
