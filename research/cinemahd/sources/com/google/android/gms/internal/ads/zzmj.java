package com.google.android.gms.internal.ads;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzmj implements zzjo {
    private final zzoj zzanv = new zzoj(32);
    private final zznj zzazv;
    private final int zzbch;
    private final zzmh zzbci = new zzmh();
    private final zzmk zzbcj = new zzmk();
    private final AtomicInteger zzbck = new AtomicInteger();
    private zzmm zzbcl;
    private zzmm zzbcm;
    private zzgw zzbcn;
    private boolean zzbco;
    private zzgw zzbcp;
    private long zzbcq;
    private int zzbcr;
    private zzml zzbcs;

    public zzmj(zznj zznj) {
        this.zzazv = zznj;
        this.zzbch = zznj.zzig();
        int i = this.zzbch;
        this.zzbcr = i;
        this.zzbcl = new zzmm(0, i);
        this.zzbcm = this.zzbcl;
    }

    private final int zzau(int i) {
        if (this.zzbcr == this.zzbch) {
            this.zzbcr = 0;
            zzmm zzmm = this.zzbcm;
            if (zzmm.zzbcv) {
                this.zzbcm = zzmm.zzbcx;
            }
            zzmm zzmm2 = this.zzbcm;
            zznk zzif = this.zzazv.zzif();
            zzmm zzmm3 = new zzmm(this.zzbcm.zzaum, this.zzbch);
            zzmm2.zzbcw = zzif;
            zzmm2.zzbcx = zzmm3;
            zzmm2.zzbcv = true;
        }
        return Math.min(i, this.zzbch - this.zzbcr);
    }

    private final void zzej(long j) {
        while (true) {
            zzmm zzmm = this.zzbcl;
            if (j >= zzmm.zzaum) {
                this.zzazv.zza(zzmm.zzbcw);
                this.zzbcl = this.zzbcl.zzic();
            } else {
                return;
            }
        }
    }

    private final void zzht() {
        this.zzbci.zzht();
        zzmm zzmm = this.zzbcl;
        if (zzmm.zzbcv) {
            zzmm zzmm2 = this.zzbcm;
            zznk[] zznkArr = new zznk[((zzmm2.zzbcv ? 1 : 0) + (((int) (zzmm2.zzbcu - zzmm.zzbcu)) / this.zzbch))];
            for (int i = 0; i < zznkArr.length; i++) {
                zznkArr[i] = zzmm.zzbcw;
                zzmm = zzmm.zzic();
            }
            this.zzazv.zza(zznkArr);
        }
        this.zzbcl = new zzmm(0, this.zzbch);
        this.zzbcm = this.zzbcl;
        this.zzbcq = 0;
        this.zzbcr = this.zzbch;
        this.zzazv.zzm();
    }

    private final boolean zzia() {
        return this.zzbck.compareAndSet(0, 1);
    }

    private final void zzib() {
        if (!this.zzbck.compareAndSet(1, 0)) {
            zzht();
        }
    }

    public final void disable() {
        if (this.zzbck.getAndSet(2) == 0) {
            zzht();
        }
    }

    public final int zza(zzgy zzgy, zzis zzis, boolean z, boolean z2, long j) {
        int i;
        zzis zzis2 = zzis;
        int zza = this.zzbci.zza(zzgy, zzis, z, z2, this.zzbcn, this.zzbcj);
        if (zza == -5) {
            this.zzbcn = zzgy.zzafz;
            return -5;
        } else if (zza == -4) {
            if (!zzis.zzgb()) {
                if (zzis2.zzamd < j) {
                    zzis2.zzw(Integer.MIN_VALUE);
                }
                if (zzis.zzgd()) {
                    zzmk zzmk = this.zzbcj;
                    long j2 = zzmk.zzauq;
                    this.zzanv.reset(1);
                    zza(j2, this.zzanv.data, 1);
                    long j3 = j2 + 1;
                    byte b = this.zzanv.data[0];
                    boolean z3 = (b & 128) != 0;
                    byte b2 = b & Byte.MAX_VALUE;
                    zzio zzio = zzis2.zzamc;
                    if (zzio.iv == null) {
                        zzio.iv = new byte[16];
                    }
                    zza(j3, zzis2.zzamc.iv, (int) b2);
                    long j4 = j3 + ((long) b2);
                    if (z3) {
                        this.zzanv.reset(2);
                        zza(j4, this.zzanv.data, 2);
                        j4 += 2;
                        i = this.zzanv.readUnsignedShort();
                    } else {
                        i = 1;
                    }
                    int[] iArr = zzis2.zzamc.numBytesOfClearData;
                    if (iArr == null || iArr.length < i) {
                        iArr = new int[i];
                    }
                    int[] iArr2 = iArr;
                    int[] iArr3 = zzis2.zzamc.numBytesOfEncryptedData;
                    if (iArr3 == null || iArr3.length < i) {
                        iArr3 = new int[i];
                    }
                    int[] iArr4 = iArr3;
                    if (z3) {
                        int i2 = i * 6;
                        this.zzanv.reset(i2);
                        zza(j4, this.zzanv.data, i2);
                        j4 += (long) i2;
                        this.zzanv.zzbe(0);
                        for (int i3 = 0; i3 < i; i3++) {
                            iArr2[i3] = this.zzanv.readUnsignedShort();
                            iArr4[i3] = this.zzanv.zzis();
                        }
                    } else {
                        iArr2[0] = 0;
                        iArr4[0] = zzmk.size - ((int) (j4 - zzmk.zzauq));
                    }
                    zzjn zzjn = zzmk.zzapt;
                    zzio zzio2 = zzis2.zzamc;
                    zzio2.set(i, iArr2, iArr4, zzjn.zzand, zzio2.iv, zzjn.zzanc);
                    long j5 = zzmk.zzauq;
                    int i4 = (int) (j4 - j5);
                    zzmk.zzauq = j5 + ((long) i4);
                    zzmk.size -= i4;
                }
                zzis2.zzy(this.zzbcj.size);
                zzmk zzmk2 = this.zzbcj;
                long j6 = zzmk2.zzauq;
                ByteBuffer byteBuffer = zzis2.zzcs;
                int i5 = zzmk2.size;
                zzej(j6);
                while (i5 > 0) {
                    int i6 = (int) (j6 - this.zzbcl.zzbcu);
                    int min = Math.min(i5, this.zzbch - i6);
                    zznk zznk = this.zzbcl.zzbcw;
                    byteBuffer.put(zznk.data, zznk.zzaz(i6), min);
                    j6 += (long) min;
                    i5 -= min;
                    if (j6 == this.zzbcl.zzaum) {
                        this.zzazv.zza(zznk);
                        this.zzbcl = this.zzbcl.zzic();
                    }
                }
                zzej(this.zzbcj.zzbct);
            }
            return -4;
        } else if (zza == -3) {
            return -3;
        } else {
            throw new IllegalStateException();
        }
    }

    public final boolean zze(long j, boolean z) {
        long zzd = this.zzbci.zzd(j, z);
        if (zzd == -1) {
            return false;
        }
        zzej(zzd);
        return true;
    }

    public final long zzhn() {
        return this.zzbci.zzhn();
    }

    public final int zzhv() {
        return this.zzbci.zzhv();
    }

    public final boolean zzhw() {
        return this.zzbci.zzhw();
    }

    public final zzgw zzhx() {
        return this.zzbci.zzhx();
    }

    public final void zzhz() {
        long zzhy = this.zzbci.zzhy();
        if (zzhy != -1) {
            zzej(zzhy);
        }
    }

    public final void zzk(boolean z) {
        int andSet = this.zzbck.getAndSet(z ? 0 : 2);
        zzht();
        this.zzbci.zzhu();
        if (andSet == 2) {
            this.zzbcn = null;
        }
    }

    public final void zze(zzgw zzgw) {
        zzgw zzgw2 = zzgw == null ? null : zzgw;
        boolean zzg = this.zzbci.zzg(zzgw2);
        this.zzbcp = zzgw;
        this.zzbco = false;
        zzml zzml = this.zzbcs;
        if (zzml != null && zzg) {
            zzml.zzf(zzgw2);
        }
    }

    private final void zza(long j, byte[] bArr, int i) {
        zzej(j);
        int i2 = 0;
        while (i2 < i) {
            int i3 = (int) (j - this.zzbcl.zzbcu);
            int min = Math.min(i - i2, this.zzbch - i3);
            zznk zznk = this.zzbcl.zzbcw;
            System.arraycopy(zznk.data, zznk.zzaz(i3), bArr, i2, min);
            j += (long) min;
            i2 += min;
            if (j == this.zzbcl.zzaum) {
                this.zzazv.zza(zznk);
                this.zzbcl = this.zzbcl.zzic();
            }
        }
    }

    public final void zza(zzml zzml) {
        this.zzbcs = zzml;
    }

    public final int zza(zzjg zzjg, int i, boolean z) throws IOException, InterruptedException {
        if (!zzia()) {
            int zzab = zzjg.zzab(i);
            if (zzab != -1) {
                return zzab;
            }
            throw new EOFException();
        }
        try {
            int zzau = zzau(i);
            zznk zznk = this.zzbcm.zzbcw;
            int read = zzjg.read(zznk.data, zznk.zzaz(this.zzbcr), zzau);
            if (read != -1) {
                this.zzbcr += read;
                this.zzbcq += (long) read;
                return read;
            }
            throw new EOFException();
        } finally {
            zzib();
        }
    }

    public final void zza(zzoj zzoj, int i) {
        if (!zzia()) {
            zzoj.zzbf(i);
            return;
        }
        while (i > 0) {
            int zzau = zzau(i);
            zznk zznk = this.zzbcm.zzbcw;
            zzoj.zze(zznk.data, zznk.zzaz(this.zzbcr), zzau);
            this.zzbcr += zzau;
            this.zzbcq += (long) zzau;
            i -= zzau;
        }
        zzib();
    }

    public final void zza(long j, int i, int i2, int i3, zzjn zzjn) {
        if (!zzia()) {
            long j2 = j;
            this.zzbci.zzei(j);
            return;
        }
        long j3 = j;
        try {
            this.zzbci.zza(j, i, this.zzbcq - ((long) i2), i2, zzjn);
        } finally {
            zzib();
        }
    }
}
