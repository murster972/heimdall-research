package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledExecutorService;

public final class zzcjv implements zzdxg<zzcjr> {
    private final zzdxp<zzblg> zzewm;
    private final zzdxp<zzcix> zzewq;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<ScheduledExecutorService> zzfpr;
    private final zzdxp<zzbou> zzfzc;

    public zzcjv(zzdxp<zzblg> zzdxp, zzdxp<zzcix> zzdxp2, zzdxp<zzbou> zzdxp3, zzdxp<ScheduledExecutorService> zzdxp4, zzdxp<zzdhd> zzdxp5) {
        this.zzewm = zzdxp;
        this.zzewq = zzdxp2;
        this.zzfzc = zzdxp3;
        this.zzfpr = zzdxp4;
        this.zzfcv = zzdxp5;
    }

    public final /* synthetic */ Object get() {
        return new zzcjr(this.zzewm.get(), this.zzewq.get(), this.zzfzc.get(), this.zzfpr.get(), this.zzfcv.get());
    }
}
