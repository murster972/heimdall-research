package com.google.android.gms.internal.ads;

public enum zzbz implements zzdry {
    UNKNOWN_ENCRYPTION_METHOD(0),
    BITSLICER(1),
    TINK_HYBRID(2),
    UNENCRYPTED(3);
    
    private static final zzdrx<zzbz> zzen = null;
    private final int value;

    static {
        zzen = new zzcb();
    }

    private zzbz(int i) {
        this.value = i;
    }

    public static zzdsa zzaf() {
        return zzca.zzew;
    }

    public static zzbz zzi(int i) {
        if (i == 0) {
            return UNKNOWN_ENCRYPTION_METHOD;
        }
        if (i == 1) {
            return BITSLICER;
        }
        if (i == 2) {
            return TINK_HYBRID;
        }
        if (i != 3) {
            return null;
        }
        return UNENCRYPTED;
    }

    public final String toString() {
        return "<" + zzbz.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
    }

    public final int zzae() {
        return this.value;
    }
}
