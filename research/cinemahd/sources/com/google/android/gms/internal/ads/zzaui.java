package com.google.android.gms.internal.ads;

import android.os.RemoteException;

interface zzaui<T> {
    T zzb(zzbfq zzbfq) throws RemoteException;
}
