package com.google.android.gms.internal.common;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

public final class zzl {
    private static volatile boolean zzjs = (!zzan());
    private static boolean zzjt = false;

    @TargetApi(24)
    public static Context getDeviceProtectedStorageContext(Context context) {
        if (context.isDeviceProtectedStorage()) {
            return context;
        }
        return context.createDeviceProtectedStorageContext();
    }

    public static boolean zzan() {
        return Build.VERSION.SDK_INT >= 24;
    }
}
