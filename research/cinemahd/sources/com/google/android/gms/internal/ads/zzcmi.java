package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcmi implements zzcio<zzcbb> {
    private final zzazb zzbli;
    private final Executor zzfci;
    private final zzczu zzfgl;
    private final zzcbn zzfod;
    private final zzcbi zzgal;
    private final Context zzup;

    public zzcmi(Context context, zzazb zzazb, zzczu zzczu, Executor executor, zzcbi zzcbi, zzcbn zzcbn) {
        this.zzup = context;
        this.zzfgl = zzczu;
        this.zzgal = zzcbi;
        this.zzfci = executor;
        this.zzbli = zzazb;
        this.zzfod = zzcbn;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        zzczp zzczp = zzczl.zzglo;
        return (zzczp == null || zzczp.zzdht == null) ? false : true;
    }

    public final zzdhe<zzcbb> zzb(zzczt zzczt, zzczl zzczl) {
        zzccd zzccd = new zzccd();
        zzdhe<zzcbb> zzb = zzdgs.zzb(zzdgs.zzaj(null), new zzcmh(this, zzczl, zzccd, zzczt), this.zzfci);
        zzb.addListener(zzcmk.zza(zzccd), this.zzfci);
        return zzb;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzb(zzczl zzczl, zzccd zzccd, zzczt zzczt, Object obj) throws Exception {
        zzdhe<?> zzdhe;
        zzczl zzczl2 = zzczl;
        zzbdi zza = this.zzfod.zza(this.zzfgl.zzblm, zzczl2.zzega);
        zza.zzba(zzczl2.zzdll);
        zzccd.zza(this.zzup, zza.getView());
        zzazl zzazl = new zzazl();
        zzcbi zzcbi = this.zzgal;
        zzbmt zzbmt = new zzbmt(zzczt, zzczl2, (String) null);
        zzcmo zzcmo = r1;
        zzcmo zzcmo2 = new zzcmo(this.zzup, this.zzfod, this.zzfgl, this.zzbli, zzczl, zzazl, zza);
        zzcbd zza2 = zzcbi.zza(zzbmt, new zzcbg(zzcmo, zza));
        zzazl.set(zza2);
        zzafy.zza(zza, (zzafx) zza2.zzaev());
        zza2.zzadk().zza(new zzcmj(zza), zzazd.zzdwj);
        zza2.zzadx().zzb(zza, true);
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcqj)).booleanValue() || !zzczl2.zzega) {
            zza2.zzadx();
            zzczp zzczp = zzczl2.zzglo;
            zzdhe = zzcbp.zza(zza, zzczp.zzdhr, zzczp.zzdht);
        } else {
            zzdhe = zzdgs.zzaj(null);
        }
        return zzdgs.zzb(zzdhe, new zzcmm(this, zza, zzczl2, zza2), this.zzfci);
    }
}
