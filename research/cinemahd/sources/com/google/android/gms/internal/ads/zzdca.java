package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class zzdca<E, V> implements zzdhe<V> {
    private final E zzgpx;
    private final String zzgpy;
    private final zzdhe<V> zzgpz;

    public zzdca(E e, String str, zzdhe<V> zzdhe) {
        this.zzgpx = e;
        this.zzgpy = str;
        this.zzgpz = zzdhe;
    }

    public final void addListener(Runnable runnable, Executor executor) {
        this.zzgpz.addListener(runnable, executor);
    }

    public final boolean cancel(boolean z) {
        return this.zzgpz.cancel(z);
    }

    public final V get() throws InterruptedException, ExecutionException {
        return this.zzgpz.get();
    }

    public final boolean isCancelled() {
        return this.zzgpz.isCancelled();
    }

    public final boolean isDone() {
        return this.zzgpz.isDone();
    }

    public final String toString() {
        String str = this.zzgpy;
        int identityHashCode = System.identityHashCode(this);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12);
        sb.append(str);
        sb.append("@");
        sb.append(identityHashCode);
        return sb.toString();
    }

    public final E zzaqd() {
        return this.zzgpx;
    }

    public final String zzaqe() {
        return this.zzgpy;
    }

    public final V get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.zzgpz.get(j, timeUnit);
    }
}
