package com.google.android.gms.internal.ads;

public interface zzboe<RequestComponentT> {
    zzboe<RequestComponentT> zza(zzcxw zzcxw);

    zzboe<RequestComponentT> zza(zzczt zzczt);

    RequestComponentT zzadg();
}
