package com.google.android.gms.internal.ads;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.os.ConditionVariable;
import android.os.SystemClock;
import android.util.Log;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;

public final class zzhw {
    private static boolean zzahp = false;
    private static boolean zzahq = false;
    private int streamType;
    private zzhc zzadi;
    private int zzafp;
    private final zzhn zzahr = null;
    private final zzih zzahs;
    private final zzim zzaht;
    private final zzhm[] zzahu;
    private final zzic zzahv;
    /* access modifiers changed from: private */
    public final ConditionVariable zzahw;
    private final long[] zzahx;
    private final zzhy zzahy;
    private final LinkedList<zzif> zzahz;
    private AudioTrack zzaia;
    private int zzaib;
    private int zzaic;
    private int zzaid;
    private boolean zzaie;
    private int zzaif;
    private long zzaig;
    private zzhc zzaih;
    private long zzaii;
    private long zzaij;
    private ByteBuffer zzaik;
    private int zzail;
    private int zzaim;
    private int zzain;
    private long zzaio;
    private long zzaip;
    private boolean zzaiq;
    private long zzair;
    private Method zzais;
    private int zzait;
    private long zzaiu;
    private long zzaiv;
    private int zzaiw;
    private long zzaix;
    private long zzaiy;
    private int zzaiz;
    private int zzaja;
    private long zzajb;
    private long zzajc;
    private long zzajd;
    private zzhm[] zzaje;
    private ByteBuffer[] zzajf;
    private ByteBuffer zzajg;
    private ByteBuffer zzajh;
    private byte[] zzaji;
    private int zzajj;
    private int zzajk;
    private boolean zzajl;
    private boolean zzajm;
    private int zzajn;
    private boolean zzajo;
    private boolean zzajp;
    private long zzajq;
    private float zzcy;

    public zzhw(zzhn zzhn, zzhm[] zzhmArr, zzic zzic) {
        this.zzahv = zzic;
        this.zzahw = new ConditionVariable(true);
        if (zzoq.SDK_INT >= 18) {
            try {
                this.zzais = AudioTrack.class.getMethod("getLatency", (Class[]) null);
            } catch (NoSuchMethodException unused) {
            }
        }
        if (zzoq.SDK_INT >= 19) {
            this.zzahy = new zzib();
        } else {
            this.zzahy = new zzhy((zzhz) null);
        }
        this.zzahs = new zzih();
        this.zzaht = new zzim();
        this.zzahu = new zzhm[(zzhmArr.length + 3)];
        this.zzahu[0] = new zzik();
        zzhm[] zzhmArr2 = this.zzahu;
        zzhmArr2[1] = this.zzahs;
        System.arraycopy(zzhmArr, 0, zzhmArr2, 2, zzhmArr.length);
        this.zzahu[zzhmArr.length + 2] = this.zzaht;
        this.zzahx = new long[10];
        this.zzcy = 1.0f;
        this.zzaja = 0;
        this.streamType = 3;
        this.zzajn = 0;
        this.zzadi = zzhc.zzagb;
        this.zzajk = -1;
        this.zzaje = new zzhm[0];
        this.zzajf = new ByteBuffer[0];
        this.zzahz = new LinkedList<>();
    }

    private final boolean isInitialized() {
        return this.zzaia != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d9, code lost:
        if (r11 < r10) goto L_0x0076;
     */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0117  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzb(java.nio.ByteBuffer r9, long r10) throws com.google.android.gms.internal.ads.zzie {
        /*
            r8 = this;
            boolean r0 = r9.hasRemaining()
            r1 = 1
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            java.nio.ByteBuffer r0 = r8.zzajh
            r2 = 21
            r3 = 0
            if (r0 == 0) goto L_0x0018
            if (r0 != r9) goto L_0x0013
            r0 = 1
            goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            com.google.android.gms.internal.ads.zzoc.checkArgument(r0)
            goto L_0x003b
        L_0x0018:
            r8.zzajh = r9
            int r0 = com.google.android.gms.internal.ads.zzoq.SDK_INT
            if (r0 >= r2) goto L_0x003b
            int r0 = r9.remaining()
            byte[] r4 = r8.zzaji
            if (r4 == 0) goto L_0x0029
            int r4 = r4.length
            if (r4 >= r0) goto L_0x002d
        L_0x0029:
            byte[] r4 = new byte[r0]
            r8.zzaji = r4
        L_0x002d:
            int r4 = r9.position()
            byte[] r5 = r8.zzaji
            r9.get(r5, r3, r0)
            r9.position(r4)
            r8.zzajj = r3
        L_0x003b:
            int r0 = r9.remaining()
            int r4 = com.google.android.gms.internal.ads.zzoq.SDK_INT
            if (r4 >= r2) goto L_0x0079
            long r10 = r8.zzaix
            com.google.android.gms.internal.ads.zzhy r2 = r8.zzahy
            long r4 = r2.zzfo()
            int r2 = r8.zzaiw
            long r6 = (long) r2
            long r4 = r4 * r6
            long r10 = r10 - r4
            int r11 = (int) r10
            int r10 = r8.zzaif
            int r10 = r10 - r11
            if (r10 <= 0) goto L_0x0076
            int r10 = java.lang.Math.min(r0, r10)
            android.media.AudioTrack r11 = r8.zzaia
            byte[] r2 = r8.zzaji
            int r4 = r8.zzajj
            int r10 = r11.write(r2, r4, r10)
            if (r10 <= 0) goto L_0x00f2
            int r11 = r8.zzajj
            int r11 = r11 + r10
            r8.zzajj = r11
            int r11 = r9.position()
            int r11 = r11 + r10
            r9.position(r11)
            goto L_0x00f2
        L_0x0076:
            r10 = 0
            goto L_0x00f2
        L_0x0079:
            boolean r2 = r8.zzajo
            if (r2 == 0) goto L_0x00ec
            r4 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r2 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r2 == 0) goto L_0x0088
            r2 = 1
            goto L_0x0089
        L_0x0088:
            r2 = 0
        L_0x0089:
            com.google.android.gms.internal.ads.zzoc.checkState(r2)
            android.media.AudioTrack r2 = r8.zzaia
            java.nio.ByteBuffer r4 = r8.zzaik
            if (r4 != 0) goto L_0x00a9
            r4 = 16
            java.nio.ByteBuffer r4 = java.nio.ByteBuffer.allocate(r4)
            r8.zzaik = r4
            java.nio.ByteBuffer r4 = r8.zzaik
            java.nio.ByteOrder r5 = java.nio.ByteOrder.BIG_ENDIAN
            r4.order(r5)
            java.nio.ByteBuffer r4 = r8.zzaik
            r5 = 1431633921(0x55550001, float:1.46372496E13)
            r4.putInt(r5)
        L_0x00a9:
            int r4 = r8.zzail
            if (r4 != 0) goto L_0x00c5
            java.nio.ByteBuffer r4 = r8.zzaik
            r5 = 4
            r4.putInt(r5, r0)
            java.nio.ByteBuffer r4 = r8.zzaik
            r5 = 8
            r6 = 1000(0x3e8, double:4.94E-321)
            long r10 = r10 * r6
            r4.putLong(r5, r10)
            java.nio.ByteBuffer r10 = r8.zzaik
            r10.position(r3)
            r8.zzail = r0
        L_0x00c5:
            java.nio.ByteBuffer r10 = r8.zzaik
            int r10 = r10.remaining()
            if (r10 <= 0) goto L_0x00dc
            java.nio.ByteBuffer r11 = r8.zzaik
            int r11 = r2.write(r11, r10, r1)
            if (r11 >= 0) goto L_0x00d9
            r8.zzail = r3
            r10 = r11
            goto L_0x00f2
        L_0x00d9:
            if (r11 >= r10) goto L_0x00dc
            goto L_0x0076
        L_0x00dc:
            int r9 = r2.write(r9, r0, r1)
            if (r9 >= 0) goto L_0x00e5
            r8.zzail = r3
            goto L_0x00ea
        L_0x00e5:
            int r10 = r8.zzail
            int r10 = r10 - r9
            r8.zzail = r10
        L_0x00ea:
            r10 = r9
            goto L_0x00f2
        L_0x00ec:
            android.media.AudioTrack r10 = r8.zzaia
            int r10 = r10.write(r9, r0, r1)
        L_0x00f2:
            long r4 = android.os.SystemClock.elapsedRealtime()
            r8.zzajq = r4
            if (r10 < 0) goto L_0x0117
            boolean r9 = r8.zzaie
            if (r9 != 0) goto L_0x0104
            long r4 = r8.zzaix
            long r6 = (long) r10
            long r4 = r4 + r6
            r8.zzaix = r4
        L_0x0104:
            if (r10 != r0) goto L_0x0116
            boolean r9 = r8.zzaie
            if (r9 == 0) goto L_0x0112
            long r9 = r8.zzaiy
            int r11 = r8.zzaiz
            long r2 = (long) r11
            long r9 = r9 + r2
            r8.zzaiy = r9
        L_0x0112:
            r9 = 0
            r8.zzajh = r9
            return r1
        L_0x0116:
            return r3
        L_0x0117:
            com.google.android.gms.internal.ads.zzie r9 = new com.google.android.gms.internal.ads.zzie
            r9.<init>(r10)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzhw.zzb(java.nio.ByteBuffer, long):boolean");
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int zzbb(java.lang.String r5) {
        /*
            int r0 = r5.hashCode()
            r1 = 0
            r2 = 3
            r3 = 2
            r4 = 1
            switch(r0) {
                case -1095064472: goto L_0x002a;
                case 187078296: goto L_0x0020;
                case 1504578661: goto L_0x0016;
                case 1505942594: goto L_0x000c;
                default: goto L_0x000b;
            }
        L_0x000b:
            goto L_0x0034
        L_0x000c:
            java.lang.String r0 = "audio/vnd.dts.hd"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0034
            r5 = 3
            goto L_0x0035
        L_0x0016:
            java.lang.String r0 = "audio/eac3"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0034
            r5 = 1
            goto L_0x0035
        L_0x0020:
            java.lang.String r0 = "audio/ac3"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0034
            r5 = 0
            goto L_0x0035
        L_0x002a:
            java.lang.String r0 = "audio/vnd.dts"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0034
            r5 = 2
            goto L_0x0035
        L_0x0034:
            r5 = -1
        L_0x0035:
            if (r5 == 0) goto L_0x0045
            if (r5 == r4) goto L_0x0043
            if (r5 == r3) goto L_0x0041
            if (r5 == r2) goto L_0x003e
            return r1
        L_0x003e:
            r5 = 8
            return r5
        L_0x0041:
            r5 = 7
            return r5
        L_0x0043:
            r5 = 6
            return r5
        L_0x0045:
            r5 = 5
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzhw.zzbb(java.lang.String):int");
    }

    private final void zzdv(long j) throws zzie {
        ByteBuffer byteBuffer;
        int length = this.zzaje.length;
        int i = length;
        while (i >= 0) {
            if (i > 0) {
                byteBuffer = this.zzajf[i - 1];
            } else {
                byteBuffer = this.zzajg;
                if (byteBuffer == null) {
                    byteBuffer = zzhm.zzaha;
                }
            }
            if (i == length) {
                zzb(byteBuffer, j);
            } else {
                zzhm zzhm = this.zzaje[i];
                zzhm.zzi(byteBuffer);
                ByteBuffer zzfc = zzhm.zzfc();
                this.zzajf[i] = zzfc;
                if (zzfc.hasRemaining()) {
                    i++;
                }
            }
            if (!byteBuffer.hasRemaining()) {
                i--;
            } else {
                return;
            }
        }
    }

    private final long zzdw(long j) {
        return (j * 1000000) / ((long) this.zzafp);
    }

    private final long zzdx(long j) {
        return (j * ((long) this.zzafp)) / 1000000;
    }

    private final void zzfd() {
        ArrayList arrayList = new ArrayList();
        for (zzhm zzhm : this.zzahu) {
            if (zzhm.isActive()) {
                arrayList.add(zzhm);
            } else {
                zzhm.flush();
            }
        }
        int size = arrayList.size();
        this.zzaje = (zzhm[]) arrayList.toArray(new zzhm[size]);
        this.zzajf = new ByteBuffer[size];
        for (int i = 0; i < size; i++) {
            zzhm zzhm2 = this.zzaje[i];
            zzhm2.flush();
            this.zzajf[i] = zzhm2.zzfc();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzfg() throws com.google.android.gms.internal.ads.zzie {
        /*
            r9 = this;
            int r0 = r9.zzajk
            r1 = -1
            r2 = 1
            r3 = 0
            if (r0 != r1) goto L_0x0014
            boolean r0 = r9.zzaie
            if (r0 == 0) goto L_0x000f
            com.google.android.gms.internal.ads.zzhm[] r0 = r9.zzaje
            int r0 = r0.length
            goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            r9.zzajk = r0
        L_0x0012:
            r0 = 1
            goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            int r4 = r9.zzajk
            com.google.android.gms.internal.ads.zzhm[] r5 = r9.zzaje
            int r6 = r5.length
            r7 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r4 >= r6) goto L_0x0038
            r4 = r5[r4]
            if (r0 == 0) goto L_0x0028
            r4.zzfb()
        L_0x0028:
            r9.zzdv(r7)
            boolean r0 = r4.zzeu()
            if (r0 != 0) goto L_0x0032
            return r3
        L_0x0032:
            int r0 = r9.zzajk
            int r0 = r0 + r2
            r9.zzajk = r0
            goto L_0x0012
        L_0x0038:
            java.nio.ByteBuffer r0 = r9.zzajh
            if (r0 == 0) goto L_0x0044
            r9.zzb(r0, r7)
            java.nio.ByteBuffer r0 = r9.zzajh
            if (r0 == 0) goto L_0x0044
            return r3
        L_0x0044:
            r9.zzajk = r1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzhw.zzfg():boolean");
    }

    private final void zzfk() {
        if (!isInitialized()) {
            return;
        }
        if (zzoq.SDK_INT >= 21) {
            this.zzaia.setVolume(this.zzcy);
            return;
        }
        AudioTrack audioTrack = this.zzaia;
        float f = this.zzcy;
        audioTrack.setStereoVolume(f, f);
    }

    private final long zzfl() {
        return this.zzaie ? this.zzaiy : this.zzaix / ((long) this.zzaiw);
    }

    private final void zzfm() {
        this.zzaio = 0;
        this.zzain = 0;
        this.zzaim = 0;
        this.zzaip = 0;
        this.zzaiq = false;
        this.zzair = 0;
    }

    private final boolean zzfn() {
        if (zzoq.SDK_INT >= 23) {
            return false;
        }
        int i = this.zzaid;
        return i == 5 || i == 6;
    }

    public final void pause() {
        this.zzajm = false;
        if (isInitialized()) {
            zzfm();
            this.zzahy.pause();
        }
    }

    public final void play() {
        this.zzajm = true;
        if (isInitialized()) {
            this.zzajc = System.nanoTime() / 1000;
            this.zzaia.play();
        }
    }

    public final void release() {
        reset();
        for (zzhm reset : this.zzahu) {
            reset.reset();
        }
        this.zzajn = 0;
        this.zzajm = false;
    }

    public final void reset() {
        if (isInitialized()) {
            this.zzaiu = 0;
            this.zzaiv = 0;
            this.zzaix = 0;
            this.zzaiy = 0;
            this.zzaiz = 0;
            zzhc zzhc = this.zzaih;
            if (zzhc != null) {
                this.zzadi = zzhc;
                this.zzaih = null;
            } else if (!this.zzahz.isEmpty()) {
                this.zzadi = this.zzahz.getLast().zzadi;
            }
            this.zzahz.clear();
            this.zzaii = 0;
            this.zzaij = 0;
            this.zzajg = null;
            this.zzajh = null;
            int i = 0;
            while (true) {
                zzhm[] zzhmArr = this.zzaje;
                if (i >= zzhmArr.length) {
                    break;
                }
                zzhm zzhm = zzhmArr[i];
                zzhm.flush();
                this.zzajf[i] = zzhm.zzfc();
                i++;
            }
            this.zzajl = false;
            this.zzajk = -1;
            this.zzaik = null;
            this.zzail = 0;
            this.zzaja = 0;
            this.zzajd = 0;
            zzfm();
            if (this.zzaia.getPlayState() == 3) {
                this.zzaia.pause();
            }
            AudioTrack audioTrack = this.zzaia;
            this.zzaia = null;
            this.zzahy.zza((AudioTrack) null, false);
            this.zzahw.close();
            new zzhz(this, audioTrack).start();
        }
    }

    public final void setStreamType(int i) {
        if (this.streamType != i) {
            this.streamType = i;
            if (!this.zzajo) {
                reset();
                this.zzajn = 0;
            }
        }
    }

    public final void setVolume(float f) {
        if (this.zzcy != f) {
            this.zzcy = f;
            zzfk();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x00d8 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00d9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.String r8, int r9, int r10, int r11, int r12, int[] r13) throws com.google.android.gms.internal.ads.zzia {
        /*
            r7 = this;
            java.lang.String r12 = "audio/raw"
            boolean r12 = r12.equals(r8)
            r0 = 1
            r12 = r12 ^ r0
            if (r12 == 0) goto L_0x000f
            int r8 = zzbb(r8)
            goto L_0x0010
        L_0x000f:
            r8 = r11
        L_0x0010:
            r1 = 0
            if (r12 != 0) goto L_0x004f
            int r11 = com.google.android.gms.internal.ads.zzoq.zzg(r11, r9)
            r7.zzait = r11
            com.google.android.gms.internal.ads.zzih r11 = r7.zzahs
            r11.zzb(r13)
            com.google.android.gms.internal.ads.zzhm[] r11 = r7.zzahu
            int r13 = r11.length
            r3 = r8
            r2 = r9
            r8 = 0
            r9 = 0
        L_0x0025:
            if (r8 >= r13) goto L_0x0046
            r4 = r11[r8]
            boolean r5 = r4.zzb(r10, r2, r3)     // Catch:{ zzhp -> 0x003f }
            r9 = r9 | r5
            boolean r5 = r4.isActive()
            if (r5 == 0) goto L_0x003c
            int r2 = r4.zzez()
            int r3 = r4.zzfa()
        L_0x003c:
            int r8 = r8 + 1
            goto L_0x0025
        L_0x003f:
            r8 = move-exception
            com.google.android.gms.internal.ads.zzia r9 = new com.google.android.gms.internal.ads.zzia
            r9.<init>((java.lang.Throwable) r8)
            throw r9
        L_0x0046:
            if (r9 == 0) goto L_0x004b
            r7.zzfd()
        L_0x004b:
            r11 = r9
            r9 = r2
            r8 = r3
            goto L_0x0050
        L_0x004f:
            r11 = 0
        L_0x0050:
            r13 = 252(0xfc, float:3.53E-43)
            r2 = 12
            switch(r9) {
                case 1: goto L_0x0085;
                case 2: goto L_0x0082;
                case 3: goto L_0x007f;
                case 4: goto L_0x007c;
                case 5: goto L_0x0079;
                case 6: goto L_0x0076;
                case 7: goto L_0x0073;
                case 8: goto L_0x0070;
                default: goto L_0x0057;
            }
        L_0x0057:
            com.google.android.gms.internal.ads.zzia r8 = new com.google.android.gms.internal.ads.zzia
            r10 = 38
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>(r10)
            java.lang.String r10 = "Unsupported channel count: "
            r11.append(r10)
            r11.append(r9)
            java.lang.String r9 = r11.toString()
            r8.<init>((java.lang.String) r9)
            throw r8
        L_0x0070:
            int r3 = com.google.android.gms.internal.ads.zzgi.CHANNEL_OUT_7POINT1_SURROUND
            goto L_0x0086
        L_0x0073:
            r3 = 1276(0x4fc, float:1.788E-42)
            goto L_0x0086
        L_0x0076:
            r3 = 252(0xfc, float:3.53E-43)
            goto L_0x0086
        L_0x0079:
            r3 = 220(0xdc, float:3.08E-43)
            goto L_0x0086
        L_0x007c:
            r3 = 204(0xcc, float:2.86E-43)
            goto L_0x0086
        L_0x007f:
            r3 = 28
            goto L_0x0086
        L_0x0082:
            r3 = 12
            goto L_0x0086
        L_0x0085:
            r3 = 4
        L_0x0086:
            int r4 = com.google.android.gms.internal.ads.zzoq.SDK_INT
            r5 = 23
            r6 = 5
            if (r4 > r5) goto L_0x00ad
            java.lang.String r4 = com.google.android.gms.internal.ads.zzoq.DEVICE
            java.lang.String r5 = "foster"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x00ad
            java.lang.String r4 = com.google.android.gms.internal.ads.zzoq.MANUFACTURER
            java.lang.String r5 = "NVIDIA"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x00ad
            r4 = 3
            if (r9 == r4) goto L_0x00ae
            if (r9 == r6) goto L_0x00ae
            r13 = 7
            if (r9 == r13) goto L_0x00aa
            goto L_0x00ad
        L_0x00aa:
            int r13 = com.google.android.gms.internal.ads.zzgi.CHANNEL_OUT_7POINT1_SURROUND
            goto L_0x00ae
        L_0x00ad:
            r13 = r3
        L_0x00ae:
            int r3 = com.google.android.gms.internal.ads.zzoq.SDK_INT
            r4 = 25
            if (r3 > r4) goto L_0x00c4
            java.lang.String r3 = com.google.android.gms.internal.ads.zzoq.DEVICE
            java.lang.String r4 = "fugu"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x00c4
            if (r12 == 0) goto L_0x00c4
            if (r9 != r0) goto L_0x00c4
            r13 = 12
        L_0x00c4:
            if (r11 != 0) goto L_0x00d9
            boolean r11 = r7.isInitialized()
            if (r11 == 0) goto L_0x00d9
            int r11 = r7.zzaic
            if (r11 != r8) goto L_0x00d9
            int r11 = r7.zzafp
            if (r11 != r10) goto L_0x00d9
            int r11 = r7.zzaib
            if (r11 != r13) goto L_0x00d9
            return
        L_0x00d9:
            r7.reset()
            r7.zzaic = r8
            r7.zzaie = r12
            r7.zzafp = r10
            r7.zzaib = r13
            r11 = 2
            if (r12 == 0) goto L_0x00e8
            goto L_0x00e9
        L_0x00e8:
            r8 = 2
        L_0x00e9:
            r7.zzaid = r8
            int r8 = com.google.android.gms.internal.ads.zzoq.zzg(r11, r9)
            r7.zzaiw = r8
            if (r12 == 0) goto L_0x0102
            int r8 = r7.zzaid
            if (r8 == r6) goto L_0x00ff
            r9 = 6
            if (r8 != r9) goto L_0x00fb
            goto L_0x00ff
        L_0x00fb:
            r8 = 49152(0xc000, float:6.8877E-41)
            goto L_0x0138
        L_0x00ff:
            r8 = 20480(0x5000, float:2.8699E-41)
            goto L_0x0138
        L_0x0102:
            int r8 = r7.zzaid
            int r8 = android.media.AudioTrack.getMinBufferSize(r10, r13, r8)
            r9 = -2
            if (r8 == r9) goto L_0x010c
            goto L_0x010d
        L_0x010c:
            r0 = 0
        L_0x010d:
            com.google.android.gms.internal.ads.zzoc.checkState(r0)
            int r9 = r8 << 2
            r10 = 250000(0x3d090, double:1.235164E-318)
            long r10 = r7.zzdx(r10)
            int r11 = (int) r10
            int r10 = r7.zzaiw
            int r10 = r10 * r11
            long r0 = (long) r8
            r2 = 750000(0xb71b0, double:3.70549E-318)
            long r2 = r7.zzdx(r2)
            int r8 = r7.zzaiw
            long r4 = (long) r8
            long r2 = r2 * r4
            long r0 = java.lang.Math.max(r0, r2)
            int r8 = (int) r0
            if (r9 >= r10) goto L_0x0134
            r8 = r10
            goto L_0x0138
        L_0x0134:
            if (r9 <= r8) goto L_0x0137
            goto L_0x0138
        L_0x0137:
            r8 = r9
        L_0x0138:
            r7.zzaif = r8
            if (r12 == 0) goto L_0x0142
            r8 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            goto L_0x014c
        L_0x0142:
            int r8 = r7.zzaif
            int r9 = r7.zzaiw
            int r8 = r8 / r9
            long r8 = (long) r8
            long r8 = r7.zzdw(r8)
        L_0x014c:
            r7.zzaig = r8
            com.google.android.gms.internal.ads.zzhc r8 = r7.zzadi
            r7.zzb(r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzhw.zza(java.lang.String, int, int, int, int, int[]):void");
    }

    public final boolean zzba(String str) {
        zzhn zzhn = this.zzahr;
        return zzhn != null && zzhn.zzq(zzbb(str));
    }

    public final boolean zzeu() {
        if (isInitialized()) {
            return this.zzajl && !zzfh();
        }
        return true;
    }

    public final void zzfe() {
        if (this.zzaja == 1) {
            this.zzaja = 2;
        }
    }

    public final void zzff() throws zzie {
        if (!this.zzajl && isInitialized() && zzfg()) {
            this.zzahy.zzdy(zzfl());
            this.zzail = 0;
            this.zzajl = true;
        }
    }

    public final boolean zzfh() {
        if (isInitialized()) {
            if (zzfl() <= this.zzahy.zzfo()) {
                if (zzfn() && this.zzaia.getPlayState() == 2 && this.zzaia.getPlaybackHeadPosition() == 0) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }

    public final zzhc zzfi() {
        return this.zzadi;
    }

    public final void zzfj() {
        if (this.zzajo) {
            this.zzajo = false;
            this.zzajn = 0;
            reset();
        }
    }

    public final long zzj(boolean z) {
        long j;
        long j2;
        long j3;
        long j4;
        if (!(isInitialized() && this.zzaja != 0)) {
            return Long.MIN_VALUE;
        }
        if (this.zzaia.getPlayState() == 3) {
            long zzfp = this.zzahy.zzfp();
            if (zzfp != 0) {
                long nanoTime = System.nanoTime() / 1000;
                if (nanoTime - this.zzaip >= 30000) {
                    long[] jArr = this.zzahx;
                    int i = this.zzaim;
                    jArr[i] = zzfp - nanoTime;
                    this.zzaim = (i + 1) % 10;
                    int i2 = this.zzain;
                    if (i2 < 10) {
                        this.zzain = i2 + 1;
                    }
                    this.zzaip = nanoTime;
                    this.zzaio = 0;
                    int i3 = 0;
                    while (true) {
                        int i4 = this.zzain;
                        if (i3 >= i4) {
                            break;
                        }
                        this.zzaio += this.zzahx[i3] / ((long) i4);
                        i3++;
                    }
                }
                if (!zzfn() && nanoTime - this.zzair >= 500000) {
                    this.zzaiq = this.zzahy.zzfq();
                    if (this.zzaiq) {
                        long zzfr = this.zzahy.zzfr() / 1000;
                        long zzfs = this.zzahy.zzfs();
                        if (zzfr < this.zzajc) {
                            this.zzaiq = false;
                        } else if (Math.abs(zzfr - nanoTime) > 5000000) {
                            StringBuilder sb = new StringBuilder(136);
                            sb.append("Spurious audio timestamp (system clock mismatch): ");
                            sb.append(zzfs);
                            sb.append(", ");
                            sb.append(zzfr);
                            sb.append(", ");
                            sb.append(nanoTime);
                            sb.append(", ");
                            sb.append(zzfp);
                            Log.w("AudioTrack", sb.toString());
                            this.zzaiq = false;
                        } else if (Math.abs(zzdw(zzfs) - zzfp) > 5000000) {
                            StringBuilder sb2 = new StringBuilder(138);
                            sb2.append("Spurious audio timestamp (frame position mismatch): ");
                            sb2.append(zzfs);
                            sb2.append(", ");
                            sb2.append(zzfr);
                            sb2.append(", ");
                            sb2.append(nanoTime);
                            sb2.append(", ");
                            sb2.append(zzfp);
                            Log.w("AudioTrack", sb2.toString());
                            this.zzaiq = false;
                        }
                    }
                    Method method = this.zzais;
                    if (method != null && !this.zzaie) {
                        try {
                            this.zzajd = (((long) ((Integer) method.invoke(this.zzaia, (Object[]) null)).intValue()) * 1000) - this.zzaig;
                            this.zzajd = Math.max(this.zzajd, 0);
                            if (this.zzajd > 5000000) {
                                long j5 = this.zzajd;
                                StringBuilder sb3 = new StringBuilder(61);
                                sb3.append("Ignoring impossibly large audio latency: ");
                                sb3.append(j5);
                                Log.w("AudioTrack", sb3.toString());
                                this.zzajd = 0;
                            }
                        } catch (Exception unused) {
                            this.zzais = null;
                        }
                    }
                    this.zzair = nanoTime;
                }
            }
        }
        long nanoTime2 = System.nanoTime() / 1000;
        if (this.zzaiq) {
            j = zzdw(this.zzahy.zzfs() + zzdx(nanoTime2 - (this.zzahy.zzfr() / 1000)));
        } else {
            if (this.zzain == 0) {
                j = this.zzahy.zzfp();
            } else {
                j = nanoTime2 + this.zzaio;
            }
            if (!z) {
                j -= this.zzajd;
            }
        }
        long j6 = this.zzajb;
        while (!this.zzahz.isEmpty() && j >= this.zzahz.getFirst().zzaex) {
            zzif remove = this.zzahz.remove();
            this.zzadi = remove.zzadi;
            this.zzaij = remove.zzaex;
            this.zzaii = remove.zzakg - this.zzajb;
        }
        if (this.zzadi.zzagc == 1.0f) {
            j2 = (j + this.zzaii) - this.zzaij;
        } else {
            if (!this.zzahz.isEmpty() || this.zzaht.zzfw() < 1024) {
                j3 = this.zzaii;
                j4 = (long) (((double) this.zzadi.zzagc) * ((double) (j - this.zzaij)));
            } else {
                j3 = this.zzaii;
                j4 = zzoq.zza(j - this.zzaij, this.zzaht.zzfv(), this.zzaht.zzfw());
            }
            j2 = j4 + j3;
        }
        return j6 + j2;
    }

    public final void zzt(int i) {
        zzoc.checkState(zzoq.SDK_INT >= 21);
        if (!this.zzajo || this.zzajn != i) {
            this.zzajo = true;
            this.zzajn = i;
            reset();
        }
    }

    public final boolean zza(ByteBuffer byteBuffer, long j) throws zzid, zzie {
        int i;
        int i2;
        ByteBuffer byteBuffer2 = byteBuffer;
        long j2 = j;
        ByteBuffer byteBuffer3 = this.zzajg;
        zzoc.checkArgument(byteBuffer3 == null || byteBuffer2 == byteBuffer3);
        if (!isInitialized()) {
            this.zzahw.block();
            if (this.zzajo) {
                this.zzaia = new AudioTrack(new AudioAttributes.Builder().setUsage(1).setContentType(3).setFlags(16).build(), new AudioFormat.Builder().setChannelMask(this.zzaib).setEncoding(this.zzaid).setSampleRate(this.zzafp).build(), this.zzaif, 1, this.zzajn);
            } else {
                int i3 = this.zzajn;
                if (i3 == 0) {
                    this.zzaia = new AudioTrack(this.streamType, this.zzafp, this.zzaib, this.zzaid, this.zzaif, 1);
                } else {
                    this.zzaia = new AudioTrack(this.streamType, this.zzafp, this.zzaib, this.zzaid, this.zzaif, 1, i3);
                }
            }
            int state = this.zzaia.getState();
            if (state == 1) {
                int audioSessionId = this.zzaia.getAudioSessionId();
                if (this.zzajn != audioSessionId) {
                    this.zzajn = audioSessionId;
                    this.zzahv.zzr(audioSessionId);
                }
                this.zzahy.zza(this.zzaia, zzfn());
                zzfk();
                this.zzajp = false;
                if (this.zzajm) {
                    play();
                }
            } else {
                try {
                    this.zzaia.release();
                } catch (Exception unused) {
                } finally {
                    this.zzaia = null;
                }
                throw new zzid(state, this.zzafp, this.zzaib, this.zzaif);
            }
        }
        if (zzfn()) {
            if (this.zzaia.getPlayState() == 2) {
                this.zzajp = false;
                return false;
            } else if (this.zzaia.getPlayState() == 1 && this.zzahy.zzfo() != 0) {
                return false;
            }
        }
        boolean z = this.zzajp;
        this.zzajp = zzfh();
        if (z && !this.zzajp && this.zzaia.getPlayState() != 1) {
            this.zzahv.zzc(this.zzaif, zzgi.zzdm(this.zzaig), SystemClock.elapsedRealtime() - this.zzajq);
        }
        if (this.zzajg == null) {
            if (!byteBuffer.hasRemaining()) {
                return true;
            }
            if (this.zzaie && this.zzaiz == 0) {
                int i4 = this.zzaid;
                if (i4 == 7 || i4 == 8) {
                    i2 = zzig.zzj(byteBuffer);
                } else if (i4 == 5) {
                    i2 = zzhk.zzey();
                } else if (i4 == 6) {
                    i2 = zzhk.zzh(byteBuffer);
                } else {
                    StringBuilder sb = new StringBuilder(38);
                    sb.append("Unexpected audio encoding: ");
                    sb.append(i4);
                    throw new IllegalStateException(sb.toString());
                }
                this.zzaiz = i2;
            }
            if (this.zzaih != null) {
                if (!zzfg()) {
                    return false;
                }
                LinkedList<zzif> linkedList = this.zzahz;
                zzif zzif = r11;
                zzif zzif2 = new zzif(this.zzaih, Math.max(0, j2), zzdw(zzfl()), (zzhz) null);
                linkedList.add(zzif);
                this.zzaih = null;
                zzfd();
            }
            if (this.zzaja == 0) {
                this.zzajb = Math.max(0, j2);
                this.zzaja = 1;
            } else {
                long zzdw = this.zzajb + zzdw(this.zzaie ? this.zzaiv : this.zzaiu / ((long) this.zzait));
                if (this.zzaja != 1 || Math.abs(zzdw - j2) <= 200000) {
                    i = 2;
                } else {
                    StringBuilder sb2 = new StringBuilder(80);
                    sb2.append("Discontinuity detected [expected ");
                    sb2.append(zzdw);
                    sb2.append(", got ");
                    sb2.append(j2);
                    sb2.append("]");
                    Log.e("AudioTrack", sb2.toString());
                    i = 2;
                    this.zzaja = 2;
                }
                if (this.zzaja == i) {
                    this.zzajb += j2 - zzdw;
                    this.zzaja = 1;
                    this.zzahv.zzed();
                }
            }
            if (this.zzaie) {
                this.zzaiv += (long) this.zzaiz;
            } else {
                this.zzaiu += (long) byteBuffer.remaining();
            }
            this.zzajg = byteBuffer2;
        }
        if (this.zzaie) {
            zzb(this.zzajg, j2);
        } else {
            zzdv(j2);
        }
        if (this.zzajg.hasRemaining()) {
            return false;
        }
        this.zzajg = null;
        return true;
    }

    public final zzhc zzb(zzhc zzhc) {
        if (this.zzaie) {
            this.zzadi = zzhc.zzagb;
            return this.zzadi;
        }
        zzhc zzhc2 = new zzhc(this.zzaht.zza(zzhc.zzagc), this.zzaht.zzb(zzhc.zzagd));
        zzhc zzhc3 = this.zzaih;
        if (zzhc3 == null) {
            if (!this.zzahz.isEmpty()) {
                zzhc3 = this.zzahz.getLast().zzadi;
            } else {
                zzhc3 = this.zzadi;
            }
        }
        if (!zzhc2.equals(zzhc3)) {
            if (isInitialized()) {
                this.zzaih = zzhc2;
            } else {
                this.zzadi = zzhc2;
            }
        }
        return this.zzadi;
    }
}
