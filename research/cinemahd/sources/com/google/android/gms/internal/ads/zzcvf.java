package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcvf implements zzdxg<zzcvd> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzsa> zzeqj;
    private final zzdxp<zzdhd> zzfcv;

    public zzcvf(zzdxp<zzsa> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<Context> zzdxp3) {
        this.zzeqj = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzejv = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcvd(this.zzeqj.get(), this.zzfcv.get(), this.zzejv.get());
    }
}
