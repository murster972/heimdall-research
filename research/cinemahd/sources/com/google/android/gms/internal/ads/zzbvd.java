package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;

public final class zzbvd implements zzdxg<zzbve> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzatv> zzfay;
    private final zzdxp<View> zzfbq;
    private final zzdxp<Integer> zzfdc;
    private final zzdxp<zzats> zzffn;

    private zzbvd(zzdxp<zzats> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzatv> zzdxp3, zzdxp<View> zzdxp4, zzdxp<Integer> zzdxp5) {
        this.zzffn = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfay = zzdxp3;
        this.zzfbq = zzdxp4;
        this.zzfdc = zzdxp5;
    }

    public static zzbvd zzd(zzdxp<zzats> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzatv> zzdxp3, zzdxp<View> zzdxp4, zzdxp<Integer> zzdxp5) {
        return new zzbvd(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5);
    }

    public final /* synthetic */ Object get() {
        return new zzbve(this.zzffn.get(), this.zzejv.get(), this.zzfay.get(), this.zzfbq.get(), this.zzfdc.get().intValue());
    }
}
