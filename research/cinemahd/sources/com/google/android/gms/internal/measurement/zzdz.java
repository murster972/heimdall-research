package com.google.android.gms.internal.measurement;

import java.util.Arrays;

final class zzdz implements zzeb {
    private zzdz() {
    }

    public final byte[] zza(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }

    /* synthetic */ zzdz(zzdu zzdu) {
        this();
    }
}
