package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public final class zzayx {
    public static <T> T zza(Context context, String str, zzayw<IBinder, T> zzayw) throws zzayz {
        try {
            return zzayw.apply(zzbq(context).a(str));
        } catch (Exception e) {
            throw new zzayz(e);
        }
    }

    public static Context zzbp(Context context) throws zzayz {
        return zzbq(context).a();
    }

    private static DynamiteModule zzbq(Context context) throws zzayz {
        try {
            return DynamiteModule.a(context, DynamiteModule.i, ModuleDescriptor.MODULE_ID);
        } catch (Exception e) {
            throw new zzayz(e);
        }
    }
}
