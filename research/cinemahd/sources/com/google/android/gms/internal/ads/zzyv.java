package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzyv implements Parcelable.Creator<zzyw> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                z = SafeParcelReader.s(parcel, a2);
            } else if (a3 == 3) {
                z2 = SafeParcelReader.s(parcel, a2);
            } else if (a3 != 4) {
                SafeParcelReader.A(parcel, a2);
            } else {
                z3 = SafeParcelReader.s(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzyw(z, z2, z3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzyw[i];
    }
}
