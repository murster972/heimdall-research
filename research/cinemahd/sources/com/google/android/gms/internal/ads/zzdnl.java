package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdnl extends zzdrt<zzdnl, zza> implements zzdtg {
    private static volatile zzdtn<zzdnl> zzdz;
    /* access modifiers changed from: private */
    public static final zzdnl zzhdy;
    private int zzhdq;
    private zzdsb<zzb> zzhdx = zzdrt.zzazw();

    public static final class zza extends zzdrt.zzb<zzdnl, zza> implements zzdtg {
        private zza() {
            super(zzdnl.zzhdy);
        }

        public final zza zzb(zzb zzb) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdnl) this.zzhmp).zza(zzb);
            return this;
        }

        public final zza zzer(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdnl) this.zzhmp).zzeq(i);
            return this;
        }

        /* synthetic */ zza(zzdnm zzdnm) {
            this();
        }
    }

    public static final class zzb extends zzdrt<zzb, zza> implements zzdtg {
        private static volatile zzdtn<zzb> zzdz;
        /* access modifiers changed from: private */
        public static final zzb zzhdz;
        private String zzhcs = "";
        private int zzhdj;
        private int zzhdu;
        private int zzhdv;

        public static final class zza extends zzdrt.zzb<zzb, zza> implements zzdtg {
            private zza() {
                super(zzb.zzhdz);
            }

            public final zza zza(zzdne zzdne) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzb) this.zzhmp).zzb(zzdne);
                return this;
            }

            public final zza zzes(int i) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzb) this.zzhmp).zzet(i);
                return this;
            }

            public final zza zzhc(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzb) this.zzhmp).zzha(str);
                return this;
            }

            /* synthetic */ zza(zzdnm zzdnm) {
                this();
            }

            public final zza zza(zzdnw zzdnw) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzb) this.zzhmp).zzb(zzdnw);
                return this;
            }
        }

        static {
            zzb zzb = new zzb();
            zzhdz = zzb;
            zzdrt.zza(zzb.class, zzb);
        }

        private zzb() {
        }

        public static zza zzawf() {
            return (zza) zzhdz.zzazt();
        }

        /* access modifiers changed from: private */
        public final void zzb(zzdne zzdne) {
            this.zzhdu = zzdne.zzae();
        }

        /* access modifiers changed from: private */
        public final void zzet(int i) {
            this.zzhdv = i;
        }

        /* access modifiers changed from: private */
        public final void zzha(String str) {
            str.getClass();
            this.zzhcs = str;
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzdnm.zzdk[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza((zzdnm) null);
                case 3:
                    return zzdrt.zza((zzdte) zzhdz, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001Ȉ\u0002\f\u0003\u000b\u0004\f", new Object[]{"zzhcs", "zzhdu", "zzhdv", "zzhdj"});
                case 4:
                    return zzhdz;
                case 5:
                    zzdtn<zzb> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzb.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzhdz);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        /* access modifiers changed from: private */
        public final void zzb(zzdnw zzdnw) {
            this.zzhdj = zzdnw.zzae();
        }
    }

    static {
        zzdnl zzdnl = new zzdnl();
        zzhdy = zzdnl;
        zzdrt.zza(zzdnl.class, zzdnl);
    }

    private zzdnl() {
    }

    /* access modifiers changed from: private */
    public final void zza(zzb zzb2) {
        zzb2.getClass();
        if (!this.zzhdx.zzaxp()) {
            this.zzhdx = zzdrt.zza(this.zzhdx);
        }
        this.zzhdx.add(zzb2);
    }

    public static zza zzawd() {
        return (zza) zzhdy.zzazt();
    }

    /* access modifiers changed from: private */
    public final void zzeq(int i) {
        this.zzhdq = i;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnm.zzdk[i - 1]) {
            case 1:
                return new zzdnl();
            case 2:
                return new zza((zzdnm) null);
            case 3:
                return zzdrt.zza((zzdte) zzhdy, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u000b\u0002\u001b", new Object[]{"zzhdq", "zzhdx", zzb.class});
            case 4:
                return zzhdy;
            case 5:
                zzdtn<zzdnl> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdnl.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhdy);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
