package com.google.android.gms.internal.ads;

import com.facebook.react.uimanager.ViewProps;
import java.util.Map;

final class zzafl implements zzafn<zzbdi> {
    zzafl() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (map.keySet().contains(ViewProps.START)) {
            zzbdi.zzaz(true);
        }
        if (map.keySet().contains("stop")) {
            zzbdi.zzaz(false);
        }
    }
}
