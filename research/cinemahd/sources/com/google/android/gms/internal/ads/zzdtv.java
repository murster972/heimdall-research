package com.google.android.gms.internal.ads;

import java.util.ArrayDeque;
import java.util.Arrays;

final class zzdtv {
    private final ArrayDeque<zzdqk> zzhqa;

    private zzdtv() {
        this.zzhqa = new ArrayDeque<>();
    }

    private final void zzbh(zzdqk zzdqk) {
        while (!zzdqk.zzaxx()) {
            if (zzdqk instanceof zzdtt) {
                zzdtt zzdtt = (zzdtt) zzdqk;
                zzbh(zzdtt.zzhpw);
                zzdqk = zzdtt.zzhpx;
            } else {
                String valueOf = String.valueOf(zzdqk.getClass());
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 49);
                sb.append("Has a new type of ByteString been created? Found ");
                sb.append(valueOf);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        int zzgu = zzgu(zzdqk.size());
        int i = zzdtt.zzhpu[zzgu + 1];
        if (this.zzhqa.isEmpty() || this.zzhqa.peek().size() >= i) {
            this.zzhqa.push(zzdqk);
            return;
        }
        int i2 = zzdtt.zzhpu[zzgu];
        zzdqk pop = this.zzhqa.pop();
        while (!this.zzhqa.isEmpty() && this.zzhqa.peek().size() < i2) {
            pop = new zzdtt(this.zzhqa.pop(), pop, (zzdtw) null);
        }
        zzdtt zzdtt2 = new zzdtt(pop, zzdqk, (zzdtw) null);
        while (!this.zzhqa.isEmpty()) {
            if (this.zzhqa.peek().size() >= zzdtt.zzhpu[zzgu(zzdtt2.size()) + 1]) {
                break;
            }
            zzdtt2 = new zzdtt(this.zzhqa.pop(), zzdtt2, (zzdtw) null);
        }
        this.zzhqa.push(zzdtt2);
    }

    /* access modifiers changed from: private */
    public final zzdqk zzc(zzdqk zzdqk, zzdqk zzdqk2) {
        zzbh(zzdqk);
        zzbh(zzdqk2);
        zzdqk pop = this.zzhqa.pop();
        while (!this.zzhqa.isEmpty()) {
            pop = new zzdtt(this.zzhqa.pop(), pop, (zzdtw) null);
        }
        return pop;
    }

    private static int zzgu(int i) {
        int binarySearch = Arrays.binarySearch(zzdtt.zzhpu, i);
        return binarySearch < 0 ? (-(binarySearch + 1)) - 1 : binarySearch;
    }

    /* synthetic */ zzdtv(zzdtw zzdtw) {
        this();
    }
}
