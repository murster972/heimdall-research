package com.google.android.gms.internal.ads;

import java.io.IOException;

interface zzjr {
    void reset();

    void zza(zzju zzju);

    boolean zzb(zzjg zzjg) throws IOException, InterruptedException;
}
