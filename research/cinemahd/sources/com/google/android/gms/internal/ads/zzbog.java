package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbog implements zzdxg<Context> {
    private final zzdxp<Context> zzfhb;
    private final zzbod zzfhi;

    private zzbog(zzbod zzbod, zzdxp<Context> zzdxp) {
        this.zzfhi = zzbod;
        this.zzfhb = zzdxp;
    }

    public static zzbog zza(zzbod zzbod, zzdxp<Context> zzdxp) {
        return new zzbog(zzbod, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (Context) zzdxm.zza(this.zzfhi.zzby(this.zzfhb.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
