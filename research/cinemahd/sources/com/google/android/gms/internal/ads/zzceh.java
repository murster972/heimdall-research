package com.google.android.gms.internal.ads;

import java.util.Set;
import java.util.concurrent.Executor;

public final class zzceh implements zzdxg<Set<zzbsu<zzbqx>>> {
    public static Set<zzbsu<zzbqx>> zza(zzcee zzcee, zzceo zzceo, Executor executor) {
        return (Set) zzdxm.zza(zzcee.zzi(zzceo, executor), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
