package com.google.android.gms.internal.ads;

final class zzbho implements zzcpf {
    private zzbod zzelr;
    private final /* synthetic */ zzbgr zzerr;
    private zzcpj zzezx;

    private zzbho(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    public final /* synthetic */ zzcpf zza(zzcpj zzcpj) {
        this.zzezx = (zzcpj) zzdxm.checkNotNull(zzcpj);
        return this;
    }

    public final zzcpg zzaet() {
        zzdxm.zza(this.zzelr, zzbod.class);
        zzdxm.zza(this.zzezx, zzcpj.class);
        return new zzbhr(this.zzerr, this.zzezx, new zzbnb(), new zzcee(), this.zzelr, new zzdaq(), (zzcxw) null, (zzbgq) null);
    }

    @Deprecated
    public final /* synthetic */ zzcpf zzf(zzbrm zzbrm) {
        zzdxm.checkNotNull(zzbrm);
        return this;
    }

    public final /* synthetic */ zzcpf zzf(zzbod zzbod) {
        this.zzelr = (zzbod) zzdxm.checkNotNull(zzbod);
        return this;
    }
}
