package com.google.android.gms.internal.ads;

import android.os.RemoteException;

public interface zzcxo<T> {
    void zzt(T t) throws RemoteException;
}
