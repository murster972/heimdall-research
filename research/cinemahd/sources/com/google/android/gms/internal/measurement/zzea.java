package com.google.android.gms.internal.measurement;

import java.util.Iterator;

public interface zzea extends Iterator<Byte> {
    byte zza();
}
