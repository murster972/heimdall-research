package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import java.util.Set;

public final class zzbrc implements zzdxg<zzbra> {
    private final zzdxp<Set<zzbsu<AppEventListener>>> zzfeo;

    private zzbrc(zzdxp<Set<zzbsu<AppEventListener>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbrc zzp(zzdxp<Set<zzbsu<AppEventListener>>> zzdxp) {
        return new zzbrc(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbra(this.zzfeo.get());
    }
}
