package com.google.android.gms.internal.ads;

public interface zzcxt<RequestComponentT, ResponseT> {
    zzdhe<ResponseT> zza(zzcxs zzcxs, zzcxv<RequestComponentT> zzcxv);

    RequestComponentT zzaog();
}
