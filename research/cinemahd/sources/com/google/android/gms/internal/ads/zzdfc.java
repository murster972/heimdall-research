package com.google.android.gms.internal.ads;

import java.util.ArrayList;

public final class zzdfc {
    public static <E> ArrayList<E> zzdz(int i) {
        zzdeo.zze(i, "initialArraySize");
        return new ArrayList<>(i);
    }
}
