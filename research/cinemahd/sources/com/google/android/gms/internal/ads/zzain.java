package com.google.android.gms.internal.ads;

final class zzain implements Runnable {
    private final /* synthetic */ zzaih zzcze;
    private final /* synthetic */ String zzczg;

    zzain(zzaih zzaih, String str) {
        this.zzcze = zzaih;
        this.zzczg = str;
    }

    public final void run() {
        this.zzcze.zzcza.loadUrl(this.zzczg);
    }
}
