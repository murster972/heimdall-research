package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.ArrayList;

final /* synthetic */ class zzcqt implements zzcty {
    private final ArrayList zzgex;

    zzcqt(ArrayList arrayList) {
        this.zzgex = arrayList;
    }

    public final void zzr(Object obj) {
        ((Bundle) obj).putStringArrayList("android_permissions", this.zzgex);
    }
}
