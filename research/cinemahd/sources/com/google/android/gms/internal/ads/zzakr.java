package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import java.util.List;
import java.util.Map;

public final class zzakr extends zzbft {
    private final AppMeasurementSdk zzdbj;

    zzakr(AppMeasurementSdk appMeasurementSdk) {
        this.zzdbj = appMeasurementSdk;
    }

    public final void beginAdUnitExposure(String str) throws RemoteException {
        this.zzdbj.a(str);
    }

    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        this.zzdbj.a(str, str2, bundle);
    }

    public final void endAdUnitExposure(String str) throws RemoteException {
        this.zzdbj.b(str);
    }

    public final long generateEventId() throws RemoteException {
        return this.zzdbj.a();
    }

    public final String getAppIdOrigin() throws RemoteException {
        return this.zzdbj.b();
    }

    public final String getAppInstanceId() throws RemoteException {
        return this.zzdbj.c();
    }

    public final List getConditionalUserProperties(String str, String str2) throws RemoteException {
        return this.zzdbj.a(str, str2);
    }

    public final String getCurrentScreenClass() throws RemoteException {
        return this.zzdbj.d();
    }

    public final String getCurrentScreenName() throws RemoteException {
        return this.zzdbj.e();
    }

    public final String getGmpAppId() throws RemoteException {
        return this.zzdbj.f();
    }

    public final int getMaxUserProperties(String str) throws RemoteException {
        return this.zzdbj.c(str);
    }

    public final Map getUserProperties(String str, String str2, boolean z) throws RemoteException {
        return this.zzdbj.a(str, str2, z);
    }

    public final void logEvent(String str, String str2, Bundle bundle) throws RemoteException {
        this.zzdbj.b(str, str2, bundle);
    }

    public final void performAction(Bundle bundle) throws RemoteException {
        this.zzdbj.a(bundle);
    }

    public final Bundle performActionWithResponse(Bundle bundle) throws RemoteException {
        return this.zzdbj.b(bundle);
    }

    public final void setConditionalUserProperty(Bundle bundle) throws RemoteException {
        this.zzdbj.c(bundle);
    }

    public final void zza(String str, String str2, IObjectWrapper iObjectWrapper) throws RemoteException {
        this.zzdbj.a(str, str2, iObjectWrapper != null ? ObjectWrapper.a(iObjectWrapper) : null);
    }

    public final void zzb(IObjectWrapper iObjectWrapper, String str, String str2) throws RemoteException {
        this.zzdbj.a(iObjectWrapper != null ? (Activity) ObjectWrapper.a(iObjectWrapper) : null, str, str2);
    }
}
