package com.google.android.gms.internal.ads;

public enum zzcd implements zzdry {
    ENUM_FALSE(0),
    ENUM_TRUE(1),
    ENUM_FAILURE(2),
    ENUM_UNKNOWN(1000);
    
    private static final zzdrx<zzcd> zzen = null;
    private final int value;

    static {
        zzen = new zzcc();
    }

    private zzcd(int i) {
        this.value = i;
    }

    public static zzdsa zzaf() {
        return zzcf.zzew;
    }

    public static zzcd zzj(int i) {
        if (i == 0) {
            return ENUM_FALSE;
        }
        if (i == 1) {
            return ENUM_TRUE;
        }
        if (i == 2) {
            return ENUM_FAILURE;
        }
        if (i != 1000) {
            return null;
        }
        return ENUM_UNKNOWN;
    }

    public final String toString() {
        return "<" + zzcd.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
    }

    public final int zzae() {
        return this.value;
    }
}
