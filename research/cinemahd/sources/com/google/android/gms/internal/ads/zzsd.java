package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzsd extends IInterface {
    zzrx zza(zzry zzry) throws RemoteException;
}
