package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzbxt implements zzafn {
    private final zzbxr zzfof;

    zzbxt(zzbxr zzbxr) {
        this.zzfof = zzbxr;
    }

    public final void zza(Object obj, Map map) {
        this.zzfof.zzc((zzbdi) obj, map);
    }
}
