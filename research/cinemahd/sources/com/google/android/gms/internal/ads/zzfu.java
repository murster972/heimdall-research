package com.google.android.gms.internal.ads;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class zzfu {
    private static final String TAG = "zzfu";
    private final String className;
    private final String zzaac;
    private final int zzaad = 2;
    private volatile Method zzaae = null;
    private final Class<?>[] zzaaf;
    private CountDownLatch zzaag = new CountDownLatch(1);
    private final zzei zzuv;

    public zzfu(zzei zzei, String str, String str2, Class<?>... clsArr) {
        this.zzuv = zzei;
        this.className = str;
        this.zzaac = str2;
        this.zzaaf = clsArr;
        this.zzuv.zzbx().submit(new zzft(this));
    }

    private final String zzb(byte[] bArr, String str) throws zzdv, UnsupportedEncodingException {
        return new String(this.zzuv.zzbz().zza(bArr, str), "UTF-8");
    }

    /* access modifiers changed from: private */
    public final void zzcr() {
        try {
            Class loadClass = this.zzuv.zzby().loadClass(zzb(this.zzuv.zzca(), this.className));
            if (loadClass != null) {
                this.zzaae = loadClass.getMethod(zzb(this.zzuv.zzca(), this.zzaac), this.zzaaf);
                if (this.zzaae == null) {
                    this.zzaag.countDown();
                } else {
                    this.zzaag.countDown();
                }
            }
        } catch (zzdv unused) {
        } catch (UnsupportedEncodingException unused2) {
        } catch (ClassNotFoundException unused3) {
        } catch (NoSuchMethodException unused4) {
        } catch (NullPointerException unused5) {
        } finally {
            this.zzaag.countDown();
        }
    }

    public final Method zzcs() {
        if (this.zzaae != null) {
            return this.zzaae;
        }
        try {
            if (!this.zzaag.await(2, TimeUnit.SECONDS)) {
                return null;
            }
            return this.zzaae;
        } catch (InterruptedException unused) {
            return null;
        }
    }
}
