package com.google.android.gms.internal.ads;

import android.media.AudioTrack;
import android.os.SystemClock;

class zzhy {
    private int zzafp;
    protected AudioTrack zzaia;
    private boolean zzajs;
    private long zzajt;
    private long zzaju;
    private long zzajv;
    private long zzajw;
    private long zzajx;
    private long zzajy;

    private zzhy() {
    }

    public final void pause() {
        if (this.zzajw == -9223372036854775807L) {
            this.zzaia.pause();
        }
    }

    public void zza(AudioTrack audioTrack, boolean z) {
        this.zzaia = audioTrack;
        this.zzajs = z;
        this.zzajw = -9223372036854775807L;
        this.zzajt = 0;
        this.zzaju = 0;
        this.zzajv = 0;
        if (audioTrack != null) {
            this.zzafp = audioTrack.getSampleRate();
        }
    }

    public final void zzdy(long j) {
        this.zzajx = zzfo();
        this.zzajw = SystemClock.elapsedRealtime() * 1000;
        this.zzajy = j;
        this.zzaia.stop();
    }

    public final long zzfo() {
        if (this.zzajw != -9223372036854775807L) {
            return Math.min(this.zzajy, this.zzajx + ((((SystemClock.elapsedRealtime() * 1000) - this.zzajw) * ((long) this.zzafp)) / 1000000));
        }
        int playState = this.zzaia.getPlayState();
        if (playState == 1) {
            return 0;
        }
        long playbackHeadPosition = 4294967295L & ((long) this.zzaia.getPlaybackHeadPosition());
        if (this.zzajs) {
            if (playState == 2 && playbackHeadPosition == 0) {
                this.zzajv = this.zzajt;
            }
            playbackHeadPosition += this.zzajv;
        }
        if (this.zzajt > playbackHeadPosition) {
            this.zzaju++;
        }
        this.zzajt = playbackHeadPosition;
        return playbackHeadPosition + (this.zzaju << 32);
    }

    public final long zzfp() {
        return (zzfo() * 1000000) / ((long) this.zzafp);
    }

    public boolean zzfq() {
        return false;
    }

    public long zzfr() {
        throw new UnsupportedOperationException();
    }

    public long zzfs() {
        throw new UnsupportedOperationException();
    }

    /* synthetic */ zzhy(zzhz zzhz) {
        this();
    }
}
