package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzagz extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzagz> CREATOR = new zzahc();
    public final int versionCode;
    public final int zzbjx;
    public final int zzcyi;
    public final String zzcyj;

    public zzagz(zzahl zzahl) {
        this(2, 1, zzahl.zzrw(), zzahl.getMediaAspectRatio());
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzcyi);
        SafeParcelWriter.a(parcel, 2, this.zzcyj, false);
        SafeParcelWriter.a(parcel, 3, this.zzbjx);
        SafeParcelWriter.a(parcel, 1000, this.versionCode);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzagz(int i, int i2, String str, int i3) {
        this.versionCode = i;
        this.zzcyi = i2;
        this.zzcyj = str;
        this.zzbjx = i3;
    }
}
