package com.google.android.gms.internal.ads;

final class zzmf implements zzme {
    private final /* synthetic */ int zzbbl;
    private final /* synthetic */ zzmg zzbbm;

    zzmf(zzmg zzmg, int i) {
        this.zzbbm = zzmg;
        this.zzbbl = i;
    }

    public final void zzb(zzhg zzhg, Object obj) {
        this.zzbbm.zza(this.zzbbl, zzhg, obj);
    }
}
