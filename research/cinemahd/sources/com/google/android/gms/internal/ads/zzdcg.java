package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzdcg implements Callable {
    private final zzdcb zzgqe;

    zzdcg(zzdcb zzdcb) {
        this.zzgqe = zzdcb;
    }

    public final Object call() {
        this.zzgqe.run();
        return null;
    }
}
