package com.google.android.gms.internal.ads;

import java.io.IOException;

public interface zzjd {
    void release();

    int zza(zzjg zzjg, zzjj zzjj) throws IOException, InterruptedException;

    void zza(zzjf zzjf);

    boolean zza(zzjg zzjg) throws IOException, InterruptedException;

    void zzc(long j, long j2);
}
