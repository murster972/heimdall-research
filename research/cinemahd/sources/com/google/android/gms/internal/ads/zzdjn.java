package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdjn extends zzdii<zzdlz> {
    zzdjn() {
        super(zzdlz.class, new zzdjq(zzdhx.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    public final zzdih<zzdma, zzdlz> zzasg() {
        return new zzdjp(this, zzdma.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdlz zzdlz = (zzdlz) zzdte;
        zzdpo.zzx(zzdlz.getVersion(), 0);
        if (zzdlz.zzass().size() != 32) {
            throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
        }
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdlz.zzah(zzdqk);
    }
}
