package com.google.android.gms.internal.ads;

final /* synthetic */ class zzapm implements Runnable {
    private final String zzcyr;
    private final zzayy zzdie;

    zzapm(zzayy zzayy, String str) {
        this.zzdie = zzayy;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzdie.zzen(this.zzcyr);
    }
}
