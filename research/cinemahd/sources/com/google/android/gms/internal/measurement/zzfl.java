package com.google.android.gms.internal.measurement;

public interface zzfl extends zzfk<Long> {
    void zza(long j);

    long zzb(int i);

    zzfl zzc(int i);
}
