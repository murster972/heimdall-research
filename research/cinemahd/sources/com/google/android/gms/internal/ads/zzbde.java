package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;

final class zzbde implements DialogInterface.OnCancelListener {
    private final /* synthetic */ JsPromptResult zzeeb;

    zzbde(JsPromptResult jsPromptResult) {
        this.zzeeb = jsPromptResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.zzeeb.cancel();
    }
}
