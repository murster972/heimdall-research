package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.ArrayList;

final /* synthetic */ class zzcsr implements zzcsk {
    private final zzcsn zzgfz;
    private final ArrayList zzggb;

    zzcsr(zzcsn zzcsn, ArrayList arrayList) {
        this.zzgfz = zzcsn;
        this.zzggb = arrayList;
    }

    public final void zzr(Object obj) {
        this.zzgfz.zza(this.zzggb, (Bundle) obj);
    }
}
