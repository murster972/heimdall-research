package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

@Deprecated
public final class zzdnh extends zzdrt<zzdnh, zza> implements zzdtg {
    private static volatile zzdtn<zzdnh> zzdz;
    /* access modifiers changed from: private */
    public static final zzdnh zzhdp;
    private String zzhcs = "";
    private String zzhdl = "";
    private int zzhdm;
    private boolean zzhdn;
    private String zzhdo = "";

    public static final class zza extends zzdrt.zzb<zzdnh, zza> implements zzdtg {
        private zza() {
            super(zzdnh.zzhdp);
        }

        /* synthetic */ zza(zzdni zzdni) {
            this();
        }
    }

    static {
        zzdnh zzdnh = new zzdnh();
        zzhdp = zzdnh;
        zzdrt.zza(zzdnh.class, zzdnh);
    }

    private zzdnh() {
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdni.zzdk[i - 1]) {
            case 1:
                return new zzdnh();
            case 2:
                return new zza((zzdni) null);
            case 3:
                return zzdrt.zza((zzdte) zzhdp, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ\u0003\u000b\u0004\u0007\u0005Ȉ", new Object[]{"zzhdl", "zzhcs", "zzhdm", "zzhdn", "zzhdo"});
            case 4:
                return zzhdp;
            case 5:
                zzdtn<zzdnh> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdnh.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhdp);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final String zzavi() {
        return this.zzhcs;
    }

    public final String zzavq() {
        return this.zzhdl;
    }

    public final int zzavr() {
        return this.zzhdm;
    }

    public final boolean zzavs() {
        return this.zzhdn;
    }

    public final String zzavt() {
        return this.zzhdo;
    }
}
