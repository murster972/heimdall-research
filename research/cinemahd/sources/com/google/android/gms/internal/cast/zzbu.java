package com.google.android.gms.internal.cast;

import android.view.View;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbu extends UIController {
    private final View view;
    private final int zztm;

    public zzbu(View view2, int i) {
        this.view = view2;
        this.zztm = i;
    }

    private final void zzdk() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            this.view.setVisibility(this.zztm);
        } else if (remoteMediaClient.f().B() == 0) {
            this.view.setVisibility(this.zztm);
        } else {
            this.view.setVisibility(0);
        }
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        zzdk();
    }

    public final void onSessionEnded() {
        this.view.setVisibility(this.zztm);
        super.onSessionEnded();
    }
}
