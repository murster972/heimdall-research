package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzcks implements zzcis<zzdac, zzcjy> {
    private final zzcka zzftq;

    public zzcks(zzcka zzcka) {
        this.zzftq = zzcka;
    }

    public final zzcip<zzdac, zzcjy> zzd(String str, JSONObject jSONObject) throws zzdab {
        zzdac zze = this.zzftq.zze(str, jSONObject);
        if (zze == null) {
            return null;
        }
        return new zzcip<>(zze, new zzcjy(), str);
    }
}
