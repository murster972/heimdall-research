package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.common.util.CollectionUtils;
import com.vungle.warren.analytics.AnalyticsEvent;
import java.util.HashMap;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public final class zzbed extends zzxe {
    private final Object lock = new Object();
    private boolean zzabw;
    private boolean zzabx;
    private int zzada;
    private zzxg zzddj;
    private final zzbaz zzdxu;
    private final boolean zzehf;
    private final boolean zzehg;
    private boolean zzehh;
    private boolean zzehi = true;
    private float zzehj;
    private float zzehk;
    private float zzehl;

    public zzbed(zzbaz zzbaz, float f, boolean z, boolean z2) {
        this.zzdxu = zzbaz;
        this.zzehj = f;
        this.zzehf = z;
        this.zzehg = z2;
    }

    private final void zzf(String str, Map<String, String> map) {
        HashMap hashMap = map == null ? new HashMap() : new HashMap(map);
        hashMap.put("action", str);
        zzazd.zzdwi.execute(new zzbec(this, hashMap));
    }

    public final float getAspectRatio() {
        float f;
        synchronized (this.lock) {
            f = this.zzehl;
        }
        return f;
    }

    public final int getPlaybackState() {
        int i;
        synchronized (this.lock) {
            i = this.zzada;
        }
        return i;
    }

    public final boolean isClickToExpandEnabled() {
        boolean z;
        boolean isCustomControlsEnabled = isCustomControlsEnabled();
        synchronized (this.lock) {
            if (!isCustomControlsEnabled) {
                try {
                    if (this.zzabx && this.zzehg) {
                        z = true;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
            z = false;
        }
        return z;
    }

    public final boolean isCustomControlsEnabled() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzehf && this.zzabw;
        }
        return z;
    }

    public final boolean isMuted() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzehi;
        }
        return z;
    }

    public final void mute(boolean z) {
        zzf(z ? AnalyticsEvent.Ad.mute : AnalyticsEvent.Ad.unmute, (Map<String, String>) null);
    }

    public final void pause() {
        zzf("pause", (Map<String, String>) null);
    }

    public final void play() {
        zzf("play", (Map<String, String>) null);
    }

    public final void stop() {
        zzf("stop", (Map<String, String>) null);
    }

    public final void zza(zzxg zzxg) {
        synchronized (this.lock) {
            this.zzddj = zzxg;
        }
    }

    public final void zzabo() {
        boolean z;
        int i;
        synchronized (this.lock) {
            z = this.zzehi;
            i = this.zzada;
            this.zzada = 3;
        }
        zza(i, 3, z, z);
    }

    public final void zzb(zzyw zzyw) {
        boolean z = zzyw.zzabv;
        boolean z2 = zzyw.zzabw;
        boolean z3 = zzyw.zzabx;
        synchronized (this.lock) {
            this.zzabw = z2;
            this.zzabx = z3;
        }
        zzf("initialState", CollectionUtils.a("muteStart", z ? DiskLruCache.VERSION_1 : "0", "customControlsRequested", z2 ? DiskLruCache.VERSION_1 : "0", "clickToExpandRequested", z3 ? DiskLruCache.VERSION_1 : "0"));
    }

    public final void zze(float f) {
        synchronized (this.lock) {
            this.zzehk = f;
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzj(Map map) {
        this.zzdxu.zza("pubVideoCmd", map);
    }

    public final float zzpk() {
        float f;
        synchronized (this.lock) {
            f = this.zzehj;
        }
        return f;
    }

    public final float zzpl() {
        float f;
        synchronized (this.lock) {
            f = this.zzehk;
        }
        return f;
    }

    public final zzxg zzpm() throws RemoteException {
        zzxg zzxg;
        synchronized (this.lock) {
            zzxg = this.zzddj;
        }
        return zzxg;
    }

    public final void zza(float f, float f2, int i, boolean z, float f3) {
        boolean z2;
        int i2;
        synchronized (this.lock) {
            this.zzehj = f2;
            this.zzehk = f;
            z2 = this.zzehi;
            this.zzehi = z;
            i2 = this.zzada;
            this.zzada = i;
            float f4 = this.zzehl;
            this.zzehl = f3;
            if (Math.abs(this.zzehl - f4) > 1.0E-4f) {
                this.zzdxu.getView().invalidate();
            }
        }
        zza(i2, i, z2, z);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(int i, int i2, boolean z, boolean z2) {
        synchronized (this.lock) {
            boolean z3 = false;
            boolean z4 = i != i2;
            boolean z5 = !this.zzehh && i2 == 1;
            boolean z6 = z4 && i2 == 1;
            boolean z7 = z4 && i2 == 2;
            boolean z8 = z4 && i2 == 3;
            boolean z9 = z != z2;
            if (this.zzehh || z5) {
                z3 = true;
            }
            this.zzehh = z3;
            if (z5) {
                try {
                    if (this.zzddj != null) {
                        this.zzddj.onVideoStart();
                    }
                } catch (RemoteException e) {
                    zzayu.zze("#007 Could not call remote method.", e);
                }
            }
            if (z6 && this.zzddj != null) {
                this.zzddj.onVideoPlay();
            }
            if (z7 && this.zzddj != null) {
                this.zzddj.onVideoPause();
            }
            if (z8) {
                if (this.zzddj != null) {
                    this.zzddj.onVideoEnd();
                }
                this.zzdxu.zzyu();
            }
            if (z9 && this.zzddj != null) {
                this.zzddj.onVideoMute(z2);
            }
        }
    }

    private final void zza(int i, int i2, boolean z, boolean z2) {
        zzazd.zzdwi.execute(new zzbef(this, i, i2, z, z2));
    }
}
