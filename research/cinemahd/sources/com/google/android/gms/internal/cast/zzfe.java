package com.google.android.gms.internal.cast;

public abstract class zzfe {
    private static final ThreadLocal<zzfe> zzabo = new zzff();

    public static zzfe zzfh() {
        return zzabo.get();
    }

    public abstract void zza(zzfg zzfg);
}
