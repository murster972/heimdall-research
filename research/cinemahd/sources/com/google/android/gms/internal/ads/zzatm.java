package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class zzatm implements Parcelable.Creator<zzatn> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        ArrayList<String> arrayList = null;
        ArrayList<String> arrayList2 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    str = SafeParcelReader.o(parcel, a2);
                    break;
                case 3:
                    str2 = SafeParcelReader.o(parcel, a2);
                    break;
                case 4:
                    z = SafeParcelReader.s(parcel, a2);
                    break;
                case 5:
                    z2 = SafeParcelReader.s(parcel, a2);
                    break;
                case 6:
                    arrayList = SafeParcelReader.q(parcel, a2);
                    break;
                case 7:
                    z3 = SafeParcelReader.s(parcel, a2);
                    break;
                case 8:
                    z4 = SafeParcelReader.s(parcel, a2);
                    break;
                case 9:
                    arrayList2 = SafeParcelReader.q(parcel, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzatn(str, str2, z, z2, arrayList, z3, z4, arrayList2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzatn[i];
    }
}
