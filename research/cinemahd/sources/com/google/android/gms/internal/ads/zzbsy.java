package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbsy extends zzbrl<zzbsz> {
    public zzbsy(Set<zzbsu<zzbsz>> set) {
        super(set);
    }

    public final synchronized void zzaia() {
        zza(zzbsx.zzfhp);
    }
}
