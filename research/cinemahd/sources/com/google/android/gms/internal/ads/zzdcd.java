package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ScheduledExecutorService;

public abstract class zzdcd<E> {
    /* access modifiers changed from: private */
    public static final zzdhe<?> zzgqa = zzdgs.zzaj(null);
    /* access modifiers changed from: private */
    public final ScheduledExecutorService zzfdi;
    /* access modifiers changed from: private */
    public final zzdhd zzfov;
    /* access modifiers changed from: private */
    public final zzdcp<E> zzgqb;

    public zzdcd(zzdhd zzdhd, ScheduledExecutorService scheduledExecutorService, zzdcp<E> zzdcp) {
        this.zzfov = zzdhd;
        this.zzfdi = scheduledExecutorService;
        this.zzgqb = zzdcp;
    }

    public final <I> zzdcj<I> zza(E e, zzdhe<I> zzdhe) {
        return new zzdcj(this, e, (String) null, zzdhe, Collections.singletonList(zzdhe), zzdhe, (zzdcc) null);
    }

    public final zzdch zzu(E e) {
        return new zzdch(this, e);
    }

    /* access modifiers changed from: protected */
    public abstract String zzv(E e);

    public final zzdcf zza(E e, zzdhe<?>... zzdheArr) {
        return new zzdcf(this, e, Arrays.asList(zzdheArr));
    }
}
