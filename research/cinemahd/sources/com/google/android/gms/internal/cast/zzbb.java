package com.google.android.gms.internal.cast;

import android.widget.TextView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import java.util.ArrayList;
import java.util.List;

public final class zzbb extends UIController {
    private final TextView zzso;
    private final List<String> zzsp = new ArrayList();

    public zzbb(TextView textView, List<String> list) {
        this.zzso = textView;
        this.zzsp.addAll(list);
    }

    public final void onMediaStatusUpdated() {
        MediaQueueItem i;
        MediaInfo v;
        MediaMetadata y;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && remoteMediaClient.k() && (i = remoteMediaClient.i()) != null && (v = i.v()) != null && (y = v.y()) != null) {
            for (String next : this.zzsp) {
                if (y.e(next)) {
                    this.zzso.setText(y.f(next));
                    return;
                }
            }
            this.zzso.setText("");
        }
    }
}
