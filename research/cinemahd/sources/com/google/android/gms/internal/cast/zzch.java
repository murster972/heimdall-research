package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.games.GameManagerClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import org.json.JSONObject;

public abstract class zzch extends zzcj<GameManagerClient.GameManagerResult> {
    final /* synthetic */ zzcb zzwi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzch(zzcb zzcb) {
        super(zzcb);
        this.zzwi = zzcb;
        this.zzwq = new zzci(this, zzcb);
    }

    public static GameManagerClient.GameManagerResult zzb(Status status) {
        return new zzcn(status, (String) null, -1, (JSONObject) null);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return zzb(status);
    }
}
