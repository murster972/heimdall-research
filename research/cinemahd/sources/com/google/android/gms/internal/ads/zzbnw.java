package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;

public final class zzbnw implements zzbqx {
    private final zzazb zzbli;
    private final zzavu zzdrk;
    private final zzceq zzfas;
    private final zzczu zzfgl;
    private final Context zzyv;

    public zzbnw(Context context, zzczu zzczu, zzazb zzazb, zzavu zzavu, zzceq zzceq) {
        this.zzyv = context;
        this.zzfgl = zzczu;
        this.zzbli = zzazb;
        this.zzdrk = zzavu;
        this.zzfas = zzceq;
    }

    public final void zzb(zzaqk zzaqk) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcmr)).booleanValue()) {
            zzq.zzky().zza(this.zzyv, this.zzbli, this.zzfgl.zzgmm, this.zzdrk.zzwa());
        }
        this.zzfas.zzall();
    }

    public final void zzb(zzczt zzczt) {
    }
}
