package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzamo implements Runnable {
    private final /* synthetic */ zzamf zzdef;

    zzamo(zzamf zzamf) {
        this.zzdef = zzamf;
    }

    public final void run() {
        try {
            this.zzdef.zzdds.onAdOpened();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
