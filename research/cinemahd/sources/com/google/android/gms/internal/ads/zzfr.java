package com.google.android.gms.internal.ads;

import android.util.DisplayMetrics;
import android.view.View;
import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfr extends zzfw {
    private final View zzzl;

    public zzfr(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2, View view) {
        super(zzei, str, str2, zzb, i, 57);
        this.zzzl = view;
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (this.zzzl != null) {
            Boolean bool = (Boolean) zzve.zzoy().zzd(zzzn.zzclp);
            DisplayMetrics displayMetrics = this.zzuv.getContext().getResources().getDisplayMetrics();
            zzes zzes = new zzes((String) this.zzaae.invoke((Object) null, new Object[]{this.zzzl, displayMetrics, bool}));
            zzbs.zza.zzf.C0038zza zzat = zzbs.zza.zzf.zzat();
            zzat.zzdc(zzes.zzzf.longValue()).zzdd(zzes.zzzg.longValue()).zzde(zzes.zzzh.longValue());
            if (bool.booleanValue()) {
                zzat.zzdf(zzes.zzzi.longValue());
            }
            this.zzzt.zzb((zzbs.zza.zzf) zzat.zzbaf());
        }
    }
}
