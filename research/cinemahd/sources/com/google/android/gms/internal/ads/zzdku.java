package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public final class zzdku {
    @Deprecated
    private static final zzdny zzgyy;
    @Deprecated
    private static final zzdny zzgyz;
    @Deprecated
    private static final zzdny zzgza = zzgyy;
    private static final String zzgzz = new zzdkq().getKeyType();

    static {
        zzdny zzawv = zzdny.zzawv();
        zzgyy = zzawv;
        zzgyz = zzawv;
        try {
            zzasq();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static void zzasq() throws GeneralSecurityException {
        zzdit.zza(new zzdkq(), true);
        zzdit.zza(new zzdkp(), true);
        zzdit.zza(new zzdkx());
    }
}
