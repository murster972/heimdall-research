package com.google.android.gms.internal.ads;

import android.net.Uri;

final /* synthetic */ class zzbeh implements zzbej {
    private final zzbdi zzehp;

    zzbeh(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void zzh(Uri uri) {
        zzbev zzaaa = this.zzehp.zzaaa();
        if (zzaaa == null) {
            zzayu.zzex("Unable to pass GMSG, no AdWebViewClient for AdWebView!");
        } else {
            zzaaa.zzh(uri);
        }
    }
}
