package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzdrs extends zzdqe<Float> implements zzdsb<Float>, zzdtq, RandomAccess {
    private static final zzdrs zzhmi;
    private int size;
    private float[] zzhmj;

    static {
        zzdrs zzdrs = new zzdrs(new float[0], 0);
        zzhmi = zzdrs;
        zzdrs.zzaxq();
    }

    zzdrs() {
        this(new float[10], 0);
    }

    private final void zzfb(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
    }

    private final String zzfc(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        float floatValue = ((Float) obj).floatValue();
        zzaxr();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
        float[] fArr = this.zzhmj;
        if (i2 < fArr.length) {
            System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
        } else {
            float[] fArr2 = new float[(((i2 * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            System.arraycopy(this.zzhmj, i, fArr2, i + 1, this.size - i);
            this.zzhmj = fArr2;
        }
        this.zzhmj[i] = floatValue;
        this.size++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Float> collection) {
        zzaxr();
        zzdrv.checkNotNull(collection);
        if (!(collection instanceof zzdrs)) {
            return super.addAll(collection);
        }
        zzdrs zzdrs = (zzdrs) collection;
        int i = zzdrs.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.zzhmj;
            if (i3 > fArr.length) {
                this.zzhmj = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(zzdrs.zzhmj, 0, this.zzhmj, this.size, zzdrs.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdrs)) {
            return super.equals(obj);
        }
        zzdrs zzdrs = (zzdrs) obj;
        if (this.size != zzdrs.size) {
            return false;
        }
        float[] fArr = zzdrs.zzhmj;
        for (int i = 0; i < this.size; i++) {
            if (Float.floatToIntBits(this.zzhmj[i]) != Float.floatToIntBits(fArr[i])) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        zzfb(i);
        return Float.valueOf(this.zzhmj[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.zzhmj[i2]);
        }
        return i;
    }

    public final boolean remove(Object obj) {
        zzaxr();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Float.valueOf(this.zzhmj[i]))) {
                float[] fArr = this.zzhmj;
                System.arraycopy(fArr, i + 1, fArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zzaxr();
        if (i2 >= i) {
            float[] fArr = this.zzhmj;
            System.arraycopy(fArr, i2, fArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        zzaxr();
        zzfb(i);
        float[] fArr = this.zzhmj;
        float f = fArr[i];
        fArr[i] = floatValue;
        return Float.valueOf(f);
    }

    public final int size() {
        return this.size;
    }

    public final /* synthetic */ zzdsb zzfd(int i) {
        if (i >= this.size) {
            return new zzdrs(Arrays.copyOf(this.zzhmj, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    public final void zzh(float f) {
        zzaxr();
        int i = this.size;
        float[] fArr = this.zzhmj;
        if (i == fArr.length) {
            float[] fArr2 = new float[(((i * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            this.zzhmj = fArr2;
        }
        float[] fArr3 = this.zzhmj;
        int i2 = this.size;
        this.size = i2 + 1;
        fArr3[i2] = f;
    }

    private zzdrs(float[] fArr, int i) {
        this.zzhmj = fArr;
        this.size = i;
    }

    public final /* synthetic */ Object remove(int i) {
        zzaxr();
        zzfb(i);
        float[] fArr = this.zzhmj;
        float f = fArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Float.valueOf(f);
    }

    public final /* synthetic */ boolean add(Object obj) {
        zzh(((Float) obj).floatValue());
        return true;
    }
}
