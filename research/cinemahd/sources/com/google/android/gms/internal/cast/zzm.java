package com.google.android.gms.internal.cast;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzm extends zza implements zzl {
    zzm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.internal.IMediaRouterCallback");
    }

    public final void zza(String str, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        zzc.zza(zza, (Parcelable) bundle);
        zzb(1, zza);
    }

    public final void zzb(String str, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        zzc.zza(zza, (Parcelable) bundle);
        zzb(2, zza);
    }

    public final void zzc(String str, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        zzc.zza(zza, (Parcelable) bundle);
        zzb(3, zza);
    }

    public final void zzd(String str, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        zzc.zza(zza, (Parcelable) bundle);
        zzb(4, zza);
    }

    public final void zza(String str, Bundle bundle, int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        zzc.zza(zza, (Parcelable) bundle);
        zza.writeInt(i);
        zzb(6, zza);
    }
}
