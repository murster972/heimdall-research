package com.google.android.gms.internal.ads;

public final class zzbnp implements zzdxg<zzbmg> {
    private final zzdxp<zzbpg> zzerw;
    private final zzdxp<zzbpw> zzesq;
    private final zzdxp<zzczt> zzfbo;
    private final zzdxp<zzczl> zzffb;
    private final zzdxp<zzcxq> zzfgw;
    private final zzdxp<zzbom> zzfgx;

    private zzbnp(zzdxp<zzczt> zzdxp, zzdxp<zzczl> zzdxp2, zzdxp<zzbpg> zzdxp3, zzdxp<zzbpw> zzdxp4, zzdxp<zzcxq> zzdxp5, zzdxp<zzbom> zzdxp6) {
        this.zzfbo = zzdxp;
        this.zzffb = zzdxp2;
        this.zzerw = zzdxp3;
        this.zzesq = zzdxp4;
        this.zzfgw = zzdxp5;
        this.zzfgx = zzdxp6;
    }

    public static zzbnp zzb(zzdxp<zzczt> zzdxp, zzdxp<zzczl> zzdxp2, zzdxp<zzbpg> zzdxp3, zzdxp<zzbpw> zzdxp4, zzdxp<zzcxq> zzdxp5, zzdxp<zzbom> zzdxp6) {
        return new zzbnp(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6);
    }

    public final /* synthetic */ Object get() {
        return new zzbmg(this.zzfbo.get(), this.zzffb.get(), this.zzerw.get(), this.zzesq.get(), this.zzfgw.get(), this.zzfgx.get());
    }
}
