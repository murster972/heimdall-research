package com.google.android.gms.internal.ads;

import java.security.SecureRandom;

public final class zzdpn {
    private static final ThreadLocal<SecureRandom> zzhgx = new zzdpm();

    /* access modifiers changed from: private */
    public static SecureRandom zzaxf() {
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextLong();
        return secureRandom;
    }

    public static byte[] zzey(int i) {
        byte[] bArr = new byte[i];
        zzhgx.get().nextBytes(bArr);
        return bArr;
    }
}
