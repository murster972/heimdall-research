package com.google.android.gms.internal.cast;

import org.json.JSONObject;

final class zzcf extends zzch {
    private final /* synthetic */ zzcb zzwi;
    private final /* synthetic */ String zzwl;
    private final /* synthetic */ JSONObject zzwm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcf(zzcb zzcb, String str, JSONObject jSONObject) {
        super(zzcb);
        this.zzwi = zzcb;
        this.zzwl = str;
        this.zzwm = jSONObject;
    }

    public final void execute() {
        this.zzwi.zza(this.zzwl, 6, this.zzwm, this.zzwq);
    }
}
