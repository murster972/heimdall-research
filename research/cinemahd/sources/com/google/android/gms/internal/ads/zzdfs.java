package com.google.android.gms.internal.ads;

import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;

public class zzdfs<V> extends zzdhv implements zzdhe<V> {
    /* access modifiers changed from: private */
    public static final boolean GENERATE_CANCELLATION_CAUSES = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    private static final Object NULL = new Object();
    private static final Logger zzgvj = Logger.getLogger(zzdfs.class.getName());
    /* access modifiers changed from: private */
    public static final zza zzgvk;
    /* access modifiers changed from: private */
    public volatile zzd listeners;
    /* access modifiers changed from: private */
    public volatile Object value;
    /* access modifiers changed from: private */
    public volatile zzk waiters;

    static abstract class zza {
        private zza() {
        }

        /* access modifiers changed from: package-private */
        public abstract void zza(zzk zzk, zzk zzk2);

        /* access modifiers changed from: package-private */
        public abstract void zza(zzk zzk, Thread thread);

        /* access modifiers changed from: package-private */
        public abstract boolean zza(zzdfs<?> zzdfs, zzd zzd, zzd zzd2);

        /* access modifiers changed from: package-private */
        public abstract boolean zza(zzdfs<?> zzdfs, zzk zzk, zzk zzk2);

        /* access modifiers changed from: package-private */
        public abstract boolean zza(zzdfs<?> zzdfs, Object obj, Object obj2);
    }

    static final class zzb {
        static final zzb zzgvl = new zzb(new Throwable("Failure occurred while trying to finish a future.") {
            public synchronized Throwable fillInStackTrace() {
                return this;
            }
        });
        final Throwable exception;

        zzb(Throwable th) {
            this.exception = (Throwable) zzdei.checkNotNull(th);
        }
    }

    static final class zzc {
        static final zzc zzgvm;
        static final zzc zzgvn;
        final Throwable cause;
        final boolean wasInterrupted;

        static {
            if (zzdfs.GENERATE_CANCELLATION_CAUSES) {
                zzgvn = null;
                zzgvm = null;
                return;
            }
            zzgvn = new zzc(false, (Throwable) null);
            zzgvm = new zzc(true, (Throwable) null);
        }

        zzc(boolean z, Throwable th) {
            this.wasInterrupted = z;
            this.cause = th;
        }
    }

    static final class zzd {
        static final zzd zzgvo = new zzd((Runnable) null, (Executor) null);
        final Executor executor;
        zzd next;
        final Runnable task;

        zzd(Runnable runnable, Executor executor2) {
            this.task = runnable;
            this.executor = executor2;
        }
    }

    static final class zze<V> implements Runnable {
        final zzdhe<? extends V> future;
        final zzdfs<V> zzgvp;

        zze(zzdfs<V> zzdfs, zzdhe<? extends V> zzdhe) {
            this.zzgvp = zzdfs;
            this.future = zzdhe;
        }

        public final void run() {
            if (this.zzgvp.value == this) {
                if (zzdfs.zzgvk.zza((zzdfs<?>) this.zzgvp, (Object) this, zzdfs.getFutureValue(this.future))) {
                    zzdfs.zza((zzdfs<?>) this.zzgvp);
                }
            }
        }
    }

    static final class zzf extends zza {
        final AtomicReferenceFieldUpdater<zzdfs, zzd> listenersUpdater;
        final AtomicReferenceFieldUpdater<zzdfs, Object> valueUpdater;
        final AtomicReferenceFieldUpdater<zzk, zzk> waiterNextUpdater;
        final AtomicReferenceFieldUpdater<zzk, Thread> waiterThreadUpdater;
        final AtomicReferenceFieldUpdater<zzdfs, zzk> waitersUpdater;

        zzf(AtomicReferenceFieldUpdater<zzk, Thread> atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater<zzk, zzk> atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater<zzdfs, zzk> atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater<zzdfs, zzd> atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater<zzdfs, Object> atomicReferenceFieldUpdater5) {
            super();
            this.waiterThreadUpdater = atomicReferenceFieldUpdater;
            this.waiterNextUpdater = atomicReferenceFieldUpdater2;
            this.waitersUpdater = atomicReferenceFieldUpdater3;
            this.listenersUpdater = atomicReferenceFieldUpdater4;
            this.valueUpdater = atomicReferenceFieldUpdater5;
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzk zzk, Thread thread) {
            this.waiterThreadUpdater.lazySet(zzk, thread);
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzk zzk, zzk zzk2) {
            this.waiterNextUpdater.lazySet(zzk, zzk2);
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, zzk zzk, zzk zzk2) {
            return this.waitersUpdater.compareAndSet(zzdfs, zzk, zzk2);
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, zzd zzd, zzd zzd2) {
            return this.listenersUpdater.compareAndSet(zzdfs, zzd, zzd2);
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, Object obj, Object obj2) {
            return this.valueUpdater.compareAndSet(zzdfs, obj, obj2);
        }
    }

    interface zzg<V> extends zzdhe<V> {
    }

    static final class zzh extends zza {
        private zzh() {
            super();
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzk zzk, Thread thread) {
            zzk.thread = thread;
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzk zzk, zzk zzk2) {
            zzk.next = zzk2;
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, zzk zzk, zzk zzk2) {
            synchronized (zzdfs) {
                if (zzdfs.waiters != zzk) {
                    return false;
                }
                zzk unused = zzdfs.waiters = zzk2;
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, zzd zzd, zzd zzd2) {
            synchronized (zzdfs) {
                if (zzdfs.listeners != zzd) {
                    return false;
                }
                zzd unused = zzdfs.listeners = zzd2;
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, Object obj, Object obj2) {
            synchronized (zzdfs) {
                if (zzdfs.value != obj) {
                    return false;
                }
                Object unused = zzdfs.value = obj2;
                return true;
            }
        }
    }

    static final class zzi extends zza {
        static final Unsafe zzgvq;
        static final long zzgvr;
        static final long zzgvs;
        static final long zzgvt;
        static final long zzgvu;
        static final long zzgvv;

        /* JADX WARNING: Code restructure failed: missing block: B:24:0x005d, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0069, code lost:
            throw new java.lang.RuntimeException("Could not initialize intrinsics", r0.getCause());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:4:?, code lost:
            r1 = (sun.misc.Unsafe) java.security.AccessController.doPrivileged(new com.google.android.gms.internal.ads.zzdfs.zzi.AnonymousClass1());
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0007 */
        static {
            /*
                java.lang.Class<com.google.android.gms.internal.ads.zzdfs$zzk> r0 = com.google.android.gms.internal.ads.zzdfs.zzk.class
                sun.misc.Unsafe r1 = sun.misc.Unsafe.getUnsafe()     // Catch:{ SecurityException -> 0x0007 }
                goto L_0x0012
            L_0x0007:
                com.google.android.gms.internal.ads.zzdfs$zzi$1 r1 = new com.google.android.gms.internal.ads.zzdfs$zzi$1     // Catch:{ PrivilegedActionException -> 0x005d }
                r1.<init>()     // Catch:{ PrivilegedActionException -> 0x005d }
                java.lang.Object r1 = java.security.AccessController.doPrivileged(r1)     // Catch:{ PrivilegedActionException -> 0x005d }
                sun.misc.Unsafe r1 = (sun.misc.Unsafe) r1     // Catch:{ PrivilegedActionException -> 0x005d }
            L_0x0012:
                java.lang.Class<com.google.android.gms.internal.ads.zzdfs> r2 = com.google.android.gms.internal.ads.zzdfs.class
                java.lang.String r3 = "waiters"
                java.lang.reflect.Field r3 = r2.getDeclaredField(r3)     // Catch:{ Exception -> 0x0053 }
                long r3 = r1.objectFieldOffset(r3)     // Catch:{ Exception -> 0x0053 }
                zzgvs = r3     // Catch:{ Exception -> 0x0053 }
                java.lang.String r3 = "listeners"
                java.lang.reflect.Field r3 = r2.getDeclaredField(r3)     // Catch:{ Exception -> 0x0053 }
                long r3 = r1.objectFieldOffset(r3)     // Catch:{ Exception -> 0x0053 }
                zzgvr = r3     // Catch:{ Exception -> 0x0053 }
                java.lang.String r3 = "value"
                java.lang.reflect.Field r2 = r2.getDeclaredField(r3)     // Catch:{ Exception -> 0x0053 }
                long r2 = r1.objectFieldOffset(r2)     // Catch:{ Exception -> 0x0053 }
                zzgvt = r2     // Catch:{ Exception -> 0x0053 }
                java.lang.String r2 = "thread"
                java.lang.reflect.Field r2 = r0.getDeclaredField(r2)     // Catch:{ Exception -> 0x0053 }
                long r2 = r1.objectFieldOffset(r2)     // Catch:{ Exception -> 0x0053 }
                zzgvu = r2     // Catch:{ Exception -> 0x0053 }
                java.lang.String r2 = "next"
                java.lang.reflect.Field r0 = r0.getDeclaredField(r2)     // Catch:{ Exception -> 0x0053 }
                long r2 = r1.objectFieldOffset(r0)     // Catch:{ Exception -> 0x0053 }
                zzgvv = r2     // Catch:{ Exception -> 0x0053 }
                zzgvq = r1     // Catch:{ Exception -> 0x0053 }
                return
            L_0x0053:
                r0 = move-exception
                com.google.android.gms.internal.ads.zzdem.zzg(r0)
                java.lang.RuntimeException r1 = new java.lang.RuntimeException
                r1.<init>(r0)
                throw r1
            L_0x005d:
                r0 = move-exception
                java.lang.RuntimeException r1 = new java.lang.RuntimeException
                java.lang.Throwable r0 = r0.getCause()
                java.lang.String r2 = "Could not initialize intrinsics"
                r1.<init>(r2, r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdfs.zzi.<clinit>():void");
        }

        private zzi() {
            super();
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzk zzk, Thread thread) {
            zzgvq.putObject(zzk, zzgvu, thread);
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzk zzk, zzk zzk2) {
            zzgvq.putObject(zzk, zzgvv, zzk2);
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, zzk zzk, zzk zzk2) {
            return zzgvq.compareAndSwapObject(zzdfs, zzgvs, zzk, zzk2);
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, zzd zzd, zzd zzd2) {
            return zzgvq.compareAndSwapObject(zzdfs, zzgvr, zzd, zzd2);
        }

        /* access modifiers changed from: package-private */
        public final boolean zza(zzdfs<?> zzdfs, Object obj, Object obj2) {
            return zzgvq.compareAndSwapObject(zzdfs, zzgvt, obj, obj2);
        }
    }

    static abstract class zzj<V> extends zzdfs<V> implements zzg<V> {
        zzj() {
        }

        public final V get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
            return zzdfs.super.get(j, timeUnit);
        }
    }

    static final class zzk {
        static final zzk zzgvw = new zzk(false);
        volatile zzk next;
        volatile Thread thread;

        private zzk(boolean z) {
        }

        /* access modifiers changed from: package-private */
        public final void zzb(zzk zzk) {
            zzdfs.zzgvk.zza(this, zzk);
        }

        zzk() {
            zzdfs.zzgvk.zza(this, Thread.currentThread());
        }
    }

    static {
        Throwable th;
        Throwable th2;
        zza zza2;
        Class<zzk> cls = zzk.class;
        try {
            zza2 = new zzi();
            th2 = null;
            th = null;
        } catch (Throwable th3) {
            th2 = th3;
            th = th;
            zza2 = new zzh();
        }
        zzgvk = zza2;
        if (th2 != null) {
            zzgvj.logp(Level.SEVERE, "com.google.common.util.concurrent.AbstractFuture", "<clinit>", "UnsafeAtomicHelper is broken!", th);
            zzgvj.logp(Level.SEVERE, "com.google.common.util.concurrent.AbstractFuture", "<clinit>", "SafeAtomicHelper is broken!", th2);
        }
    }

    protected zzdfs() {
    }

    /* access modifiers changed from: private */
    public static Object getFutureValue(zzdhe<?> zzdhe) {
        Throwable zza2;
        if (zzdhe instanceof zzg) {
            Object obj = ((zzdfs) zzdhe).value;
            if (!(obj instanceof zzc)) {
                return obj;
            }
            zzc zzc2 = (zzc) obj;
            if (!zzc2.wasInterrupted) {
                return obj;
            }
            Throwable th = zzc2.cause;
            if (th != null) {
                return new zzc(false, th);
            }
            return zzc.zzgvn;
        } else if ((zzdhe instanceof zzdhv) && (zza2 = zzdhy.zza((zzdhv) zzdhe)) != null) {
            return new zzb(zza2);
        } else {
            boolean isCancelled = zzdhe.isCancelled();
            if ((!GENERATE_CANCELLATION_CAUSES) && isCancelled) {
                return zzc.zzgvn;
            }
            try {
                Object zza3 = zza(zzdhe);
                if (!isCancelled) {
                    return zza3 == null ? NULL : zza3;
                }
                String valueOf = String.valueOf(zzdhe);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 84);
                sb.append("get() did not throw CancellationException, despite reporting isCancelled() == true: ");
                sb.append(valueOf);
                return new zzc(false, new IllegalArgumentException(sb.toString()));
            } catch (ExecutionException e) {
                if (!isCancelled) {
                    return new zzb(e.getCause());
                }
                String valueOf2 = String.valueOf(zzdhe);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 84);
                sb2.append("get() did not throw CancellationException, despite reporting isCancelled() == true: ");
                sb2.append(valueOf2);
                return new zzc(false, new IllegalArgumentException(sb2.toString(), e));
            } catch (CancellationException e2) {
                if (isCancelled) {
                    return new zzc(false, e2);
                }
                String valueOf3 = String.valueOf(zzdhe);
                StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf3).length() + 77);
                sb3.append("get() threw CancellationException, despite reporting isCancelled() == false: ");
                sb3.append(valueOf3);
                return new zzb(new IllegalArgumentException(sb3.toString(), e2));
            } catch (Throwable th2) {
                return new zzb(th2);
            }
        }
    }

    private final void zza(zzk zzk2) {
        zzk2.thread = null;
        while (true) {
            zzk zzk3 = this.waiters;
            if (zzk3 != zzk.zzgvw) {
                zzk zzk4 = null;
                while (zzk3 != null) {
                    zzk zzk5 = zzk3.next;
                    if (zzk3.thread != null) {
                        zzk4 = zzk3;
                    } else if (zzk4 != null) {
                        zzk4.next = zzk5;
                        if (zzk4.thread == null) {
                        }
                    } else if (zzgvk.zza((zzdfs<?>) this, zzk3, zzk5)) {
                    }
                    zzk3 = zzk5;
                }
                return;
            }
            return;
        }
    }

    private static V zzah(Object obj) throws ExecutionException {
        if (obj instanceof zzc) {
            Throwable th = ((zzc) obj).cause;
            CancellationException cancellationException = new CancellationException("Task was cancelled.");
            cancellationException.initCause(th);
            throw cancellationException;
        } else if (obj instanceof zzb) {
            throw new ExecutionException(((zzb) obj).exception);
        } else if (obj == NULL) {
            return null;
        } else {
            return obj;
        }
    }

    private final String zzai(Object obj) {
        return obj == this ? "this future" : String.valueOf(obj);
    }

    public void addListener(Runnable runnable, Executor executor) {
        zzd zzd2;
        zzdei.checkNotNull(runnable, "Runnable was null.");
        zzdei.checkNotNull(executor, "Executor was null.");
        if (isDone() || (zzd2 = this.listeners) == zzd.zzgvo) {
            zza(runnable, executor);
        }
        zzd zzd3 = new zzd(runnable, executor);
        do {
            zzd3.next = zzd2;
            if (!zzgvk.zza((zzdfs<?>) this, zzd2, zzd3)) {
                zzd2 = this.listeners;
            } else {
                return;
            }
        } while (zzd2 != zzd.zzgvo);
        zza(runnable, executor);
    }

    /* access modifiers changed from: protected */
    public void afterDone() {
    }

    public boolean cancel(boolean z) {
        zzc zzc2;
        Object obj = this.value;
        if (!(obj == null) && !(obj instanceof zze)) {
            return false;
        }
        if (GENERATE_CANCELLATION_CAUSES) {
            zzc2 = new zzc(z, new CancellationException("Future.cancel() was called."));
        } else if (z) {
            zzc2 = zzc.zzgvm;
        } else {
            zzc2 = zzc.zzgvn;
        }
        boolean z2 = false;
        Object obj2 = obj;
        zzdfs zzdfs = this;
        while (true) {
            if (zzgvk.zza((zzdfs<?>) zzdfs, obj2, (Object) zzc2)) {
                if (z) {
                    zzdfs.interruptTask();
                }
                zza((zzdfs<?>) zzdfs);
                if (!(obj2 instanceof zze)) {
                    return true;
                }
                zzdhe<? extends V> zzdhe = ((zze) obj2).future;
                if (zzdhe instanceof zzg) {
                    zzdfs = (zzdfs) zzdhe;
                    obj2 = zzdfs.value;
                    if (!(obj2 == null) && !(obj2 instanceof zze)) {
                        return true;
                    }
                    z2 = true;
                } else {
                    zzdhe.cancel(z);
                    return true;
                }
            } else {
                obj2 = zzdfs.value;
                if (!(obj2 instanceof zze)) {
                    return z2;
                }
            }
        }
    }

    public V get(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException, ExecutionException {
        long j2 = j;
        TimeUnit timeUnit2 = timeUnit;
        long nanos = timeUnit2.toNanos(j2);
        if (!Thread.interrupted()) {
            Object obj = this.value;
            if ((obj != null) && (!(obj instanceof zze))) {
                return zzah(obj);
            }
            long nanoTime = nanos > 0 ? System.nanoTime() + nanos : 0;
            if (nanos >= 1000) {
                zzk zzk2 = this.waiters;
                if (zzk2 != zzk.zzgvw) {
                    zzk zzk3 = new zzk();
                    do {
                        zzk3.zzb(zzk2);
                        if (zzgvk.zza((zzdfs<?>) this, zzk2, zzk3)) {
                            do {
                                LockSupport.parkNanos(this, nanos);
                                if (!Thread.interrupted()) {
                                    Object obj2 = this.value;
                                    if ((obj2 != null) && (!(obj2 instanceof zze))) {
                                        return zzah(obj2);
                                    }
                                    nanos = nanoTime - System.nanoTime();
                                } else {
                                    zza(zzk3);
                                    throw new InterruptedException();
                                }
                            } while (nanos >= 1000);
                            zza(zzk3);
                        } else {
                            zzk2 = this.waiters;
                        }
                    } while (zzk2 != zzk.zzgvw);
                }
                return zzah(this.value);
            }
            while (nanos > 0) {
                Object obj3 = this.value;
                if ((obj3 != null) && (!(obj3 instanceof zze))) {
                    return zzah(obj3);
                }
                if (!Thread.interrupted()) {
                    nanos = nanoTime - System.nanoTime();
                } else {
                    throw new InterruptedException();
                }
            }
            String zzdfs = toString();
            String lowerCase = timeUnit.toString().toLowerCase(Locale.ROOT);
            String lowerCase2 = timeUnit.toString().toLowerCase(Locale.ROOT);
            StringBuilder sb = new StringBuilder(String.valueOf(lowerCase2).length() + 28);
            sb.append("Waited ");
            sb.append(j2);
            sb.append(" ");
            sb.append(lowerCase2);
            String sb2 = sb.toString();
            if (nanos + 1000 < 0) {
                String concat = String.valueOf(sb2).concat(" (plus ");
                long j3 = -nanos;
                long convert = timeUnit2.convert(j3, TimeUnit.NANOSECONDS);
                long nanos2 = j3 - timeUnit2.toNanos(convert);
                int i = (convert > 0 ? 1 : (convert == 0 ? 0 : -1));
                boolean z = i == 0 || nanos2 > 1000;
                if (i > 0) {
                    String valueOf = String.valueOf(concat);
                    StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf).length() + 21 + String.valueOf(lowerCase).length());
                    sb3.append(valueOf);
                    sb3.append(convert);
                    sb3.append(" ");
                    sb3.append(lowerCase);
                    String sb4 = sb3.toString();
                    if (z) {
                        sb4 = String.valueOf(sb4).concat(",");
                    }
                    concat = String.valueOf(sb4).concat(" ");
                }
                if (z) {
                    String valueOf2 = String.valueOf(concat);
                    StringBuilder sb5 = new StringBuilder(String.valueOf(valueOf2).length() + 33);
                    sb5.append(valueOf2);
                    sb5.append(nanos2);
                    sb5.append(" nanoseconds ");
                    concat = sb5.toString();
                }
                sb2 = String.valueOf(concat).concat("delay)");
            }
            if (isDone()) {
                throw new TimeoutException(String.valueOf(sb2).concat(" but future completed as timeout expired"));
            }
            StringBuilder sb6 = new StringBuilder(String.valueOf(sb2).length() + 5 + String.valueOf(zzdfs).length());
            sb6.append(sb2);
            sb6.append(" for ");
            sb6.append(zzdfs);
            throw new TimeoutException(sb6.toString());
        }
        throw new InterruptedException();
    }

    /* access modifiers changed from: protected */
    public void interruptTask() {
    }

    public boolean isCancelled() {
        return this.value instanceof zzc;
    }

    public boolean isDone() {
        Object obj = this.value;
        return (!(obj instanceof zze)) & (obj != null);
    }

    /* access modifiers changed from: package-private */
    public final void maybePropagateCancellationTo(Future<?> future) {
        if ((future != null) && isCancelled()) {
            future.cancel(wasInterrupted());
        }
    }

    /* access modifiers changed from: protected */
    public String pendingToString() {
        Object obj = this.value;
        if (obj instanceof zze) {
            String zzai = zzai(((zze) obj).future);
            StringBuilder sb = new StringBuilder(String.valueOf(zzai).length() + 12);
            sb.append("setFuture=[");
            sb.append(zzai);
            sb.append("]");
            return sb.toString();
        } else if (!(this instanceof ScheduledFuture)) {
            return null;
        } else {
            long delay = ((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS);
            StringBuilder sb2 = new StringBuilder(41);
            sb2.append("remaining delay=[");
            sb2.append(delay);
            sb2.append(" ms]");
            return sb2.toString();
        }
    }

    /* access modifiers changed from: protected */
    public boolean set(V v) {
        if (v == null) {
            v = NULL;
        }
        if (!zzgvk.zza((zzdfs<?>) this, (Object) null, (Object) v)) {
            return false;
        }
        zza((zzdfs<?>) this);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean setException(Throwable th) {
        if (!zzgvk.zza((zzdfs<?>) this, (Object) null, (Object) new zzb((Throwable) zzdei.checkNotNull(th)))) {
            return false;
        }
        zza((zzdfs<?>) this);
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean setFuture(zzdhe<? extends V> zzdhe) {
        zze zze2;
        zzb zzb2;
        zzdei.checkNotNull(zzdhe);
        Object obj = this.value;
        if (obj == null) {
            if (zzdhe.isDone()) {
                if (!zzgvk.zza((zzdfs<?>) this, (Object) null, getFutureValue(zzdhe))) {
                    return false;
                }
                zza((zzdfs<?>) this);
                return true;
            }
            zze2 = new zze(this, zzdhe);
            if (zzgvk.zza((zzdfs<?>) this, (Object) null, (Object) zze2)) {
                try {
                    zzdhe.addListener(zze2, zzdgl.INSTANCE);
                } catch (Throwable unused) {
                    zzb2 = zzb.zzgvl;
                }
                return true;
            }
            obj = this.value;
        }
        if (obj instanceof zzc) {
            zzdhe.cancel(((zzc) obj).wasInterrupted);
        }
        return false;
        zzgvk.zza((zzdfs<?>) this, (Object) zze2, (Object) zzb2);
        return true;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (isCancelled()) {
            sb.append("CANCELLED");
        } else if (isDone()) {
            zza(sb);
        } else {
            try {
                str = pendingToString();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(e.getClass());
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 38);
                sb2.append("Exception thrown from implementation: ");
                sb2.append(valueOf);
                str = sb2.toString();
            }
            if (str != null && !str.isEmpty()) {
                sb.append("PENDING, info=[");
                sb.append(str);
                sb.append("]");
            } else if (isDone()) {
                zza(sb);
            } else {
                sb.append("PENDING");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final boolean wasInterrupted() {
        Object obj = this.value;
        return (obj instanceof zzc) && ((zzc) obj).wasInterrupted;
    }

    /* access modifiers changed from: protected */
    public final Throwable zzark() {
        if (!(this instanceof zzg)) {
            return null;
        }
        Object obj = this.value;
        if (obj instanceof zzb) {
            return ((zzb) obj).exception;
        }
        return null;
    }

    private static <V> V zza(Future<V> future) throws ExecutionException {
        V v;
        boolean z = false;
        while (true) {
            try {
                v = future.get();
                break;
            } catch (InterruptedException unused) {
                z = true;
            } catch (Throwable th) {
                if (z) {
                    Thread.currentThread().interrupt();
                }
                throw th;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return v;
    }

    /* access modifiers changed from: private */
    public static void zza(zzdfs<?> zzdfs) {
        zzdfs<V> zzdfs2;
        zzd zzd2;
        zzd zzd3;
        zzd zzd4 = null;
        zzdfs<?> zzdfs3 = zzdfs;
        while (true) {
            zzk zzk2 = zzdfs3.waiters;
            if (zzgvk.zza(zzdfs3, zzk2, zzk.zzgvw)) {
                while (zzk2 != null) {
                    Thread thread = zzk2.thread;
                    if (thread != null) {
                        zzk2.thread = null;
                        LockSupport.unpark(thread);
                    }
                    zzk2 = zzk2.next;
                }
                zzdfs3.afterDone();
                do {
                    zzd2 = zzdfs3.listeners;
                } while (!zzgvk.zza(zzdfs3, zzd2, zzd.zzgvo));
                while (true) {
                    zzd3 = zzd4;
                    zzd4 = zzd2;
                    if (zzd4 == null) {
                        break;
                    }
                    zzd2 = zzd4.next;
                    zzd4.next = zzd3;
                }
                while (zzd3 != null) {
                    zzd4 = zzd3.next;
                    Runnable runnable = zzd3.task;
                    if (runnable instanceof zze) {
                        zze zze2 = (zze) runnable;
                        zzdfs<V> zzdfs4 = zze2.zzgvp;
                        if (zzdfs4.value == zze2) {
                            if (zzgvk.zza((zzdfs<?>) zzdfs4, (Object) zze2, getFutureValue(zze2.future))) {
                                zzdfs2 = zzdfs4;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        zza(runnable, zzd3.executor);
                    }
                    zzd3 = zzd4;
                }
                return;
            }
            zzdfs2 = zzdfs3;
            zzdfs3 = zzdfs2;
        }
    }

    private final void zza(StringBuilder sb) {
        try {
            Object zza2 = zza(this);
            sb.append("SUCCESS, result=[");
            sb.append(zzai(zza2));
            sb.append("]");
        } catch (ExecutionException e) {
            sb.append("FAILURE, cause=[");
            sb.append(e.getCause());
            sb.append("]");
        } catch (CancellationException unused) {
            sb.append("CANCELLED");
        } catch (RuntimeException e2) {
            sb.append("UNKNOWN, cause=[");
            sb.append(e2.getClass());
            sb.append(" thrown from get()]");
        }
    }

    private static void zza(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e) {
            Logger logger = zzgvj;
            Level level = Level.SEVERE;
            String valueOf = String.valueOf(runnable);
            String valueOf2 = String.valueOf(executor);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57 + String.valueOf(valueOf2).length());
            sb.append("RuntimeException while executing runnable ");
            sb.append(valueOf);
            sb.append(" with executor ");
            sb.append(valueOf2);
            logger.logp(level, "com.google.common.util.concurrent.AbstractFuture", "executeListener", sb.toString(), e);
        }
    }

    public V get() throws InterruptedException, ExecutionException {
        Object obj;
        if (!Thread.interrupted()) {
            Object obj2 = this.value;
            if ((obj2 != null) && (!(obj2 instanceof zze))) {
                return zzah(obj2);
            }
            zzk zzk2 = this.waiters;
            if (zzk2 != zzk.zzgvw) {
                zzk zzk3 = new zzk();
                do {
                    zzk3.zzb(zzk2);
                    if (zzgvk.zza((zzdfs<?>) this, zzk2, zzk3)) {
                        do {
                            LockSupport.park(this);
                            if (!Thread.interrupted()) {
                                obj = this.value;
                            } else {
                                zza(zzk3);
                                throw new InterruptedException();
                            }
                        } while (!((obj != null) & (!(obj instanceof zze))));
                        return zzah(obj);
                    }
                    zzk2 = this.waiters;
                } while (zzk2 != zzk.zzgvw);
            }
            return zzah(this.value);
        }
        throw new InterruptedException();
    }
}
