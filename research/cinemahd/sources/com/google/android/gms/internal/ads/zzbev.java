package com.google.android.gms.internal.ads;

import android.net.Uri;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.zzc;

public interface zzbev {
    void zza(int i, int i2, boolean z);

    void zza(zzbeu zzbeu);

    void zza(zzbex zzbex);

    void zza(zzty zzty, zzaew zzaew, zzo zzo, zzaey zzaey, zzt zzt, boolean z, zzafq zzafq, zzc zzc, zzaon zzaon, zzato zzato);

    zzc zzaas();

    boolean zzaat();

    void zzaaz();

    void zzaba();

    void zzabb();

    void zzabc();

    zzato zzabf();

    void zzbb(boolean z);

    void zzbc(boolean z);

    void zzh(Uri uri);

    void zzi(int i, int i2);

    void zztn();
}
