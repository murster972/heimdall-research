package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcrq implements Callable {
    private final zzcrr zzgfs;

    zzcrq(zzcrr zzcrr) {
        this.zzgfs = zzcrr;
    }

    public final Object call() {
        return this.zzgfs.zzanh();
    }
}
