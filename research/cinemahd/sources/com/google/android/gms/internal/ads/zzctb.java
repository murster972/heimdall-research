package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzctb implements zzcty<Bundle> {
    private final Bundle zzfhf;

    public zzctb(Bundle bundle) {
        this.zzfhf = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        Bundle bundle2 = this.zzfhf;
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
    }
}
