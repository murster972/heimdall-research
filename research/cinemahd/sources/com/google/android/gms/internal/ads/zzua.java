package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzua extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzua> CREATOR = new zztz();
    public final String zzcbu;
    public final String zzcbv;

    public zzua(String str, String str2) {
        this.zzcbu = str;
        this.zzcbv = str2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzcbu, false);
        SafeParcelWriter.a(parcel, 2, this.zzcbv, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
