package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzc;
import java.util.concurrent.Executor;

public final class zzccc implements zzdxg<zzcbp> {
    private final zzdxp<zzbra> zzerj;
    private final zzdxp<zzbqa> zzerq;
    private final zzdxp<zzbpm> zzesd;
    private final zzdxp<zzboq> zzesg;
    private final zzdxp<zzbqj> zzesu;
    private final zzdxp<zzbjd> zzeue;
    private final zzdxp<zzbst> zzeun;
    private final zzdxp<zzbqw> zzevv;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzato> zzfgz;
    private final zzdxp<zzdq> zzfky;
    private final zzdxp<zzc> zzfrn;
    private final zzdxp<zzbpg> zzfro;

    private zzccc(zzdxp<zzboq> zzdxp, zzdxp<zzbpm> zzdxp2, zzdxp<zzbqa> zzdxp3, zzdxp<zzbqj> zzdxp4, zzdxp<zzbra> zzdxp5, zzdxp<Executor> zzdxp6, zzdxp<zzbst> zzdxp7, zzdxp<zzbjd> zzdxp8, zzdxp<zzc> zzdxp9, zzdxp<zzbpg> zzdxp10, zzdxp<zzato> zzdxp11, zzdxp<zzdq> zzdxp12, zzdxp<zzbqw> zzdxp13) {
        this.zzesg = zzdxp;
        this.zzesd = zzdxp2;
        this.zzerq = zzdxp3;
        this.zzesu = zzdxp4;
        this.zzerj = zzdxp5;
        this.zzfcv = zzdxp6;
        this.zzeun = zzdxp7;
        this.zzeue = zzdxp8;
        this.zzfrn = zzdxp9;
        this.zzfro = zzdxp10;
        this.zzfgz = zzdxp11;
        this.zzfky = zzdxp12;
        this.zzevv = zzdxp13;
    }

    public static zzccc zza(zzdxp<zzboq> zzdxp, zzdxp<zzbpm> zzdxp2, zzdxp<zzbqa> zzdxp3, zzdxp<zzbqj> zzdxp4, zzdxp<zzbra> zzdxp5, zzdxp<Executor> zzdxp6, zzdxp<zzbst> zzdxp7, zzdxp<zzbjd> zzdxp8, zzdxp<zzc> zzdxp9, zzdxp<zzbpg> zzdxp10, zzdxp<zzato> zzdxp11, zzdxp<zzdq> zzdxp12, zzdxp<zzbqw> zzdxp13) {
        return new zzccc(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6, zzdxp7, zzdxp8, zzdxp9, zzdxp10, zzdxp11, zzdxp12, zzdxp13);
    }

    public final /* synthetic */ Object get() {
        return new zzcbp(this.zzesg.get(), this.zzesd.get(), this.zzerq.get(), this.zzesu.get(), this.zzerj.get(), this.zzfcv.get(), this.zzeun.get(), this.zzeue.get(), this.zzfrn.get(), this.zzfro.get(), this.zzfgz.get(), this.zzfky.get(), this.zzevv.get());
    }
}
