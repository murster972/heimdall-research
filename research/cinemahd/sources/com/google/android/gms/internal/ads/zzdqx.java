package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

final class zzdqx extends zzdqw {
    private final byte[] buffer;
    private int pos;
    private int zzaif;
    private final InputStream zzhij;
    private int zzhik;
    private int zzhil;
    private int zzhim;
    private int zzhin;
    private zzdra zzhio;

    private zzdqx(InputStream inputStream, int i) {
        super();
        this.zzhin = Integer.MAX_VALUE;
        this.zzhio = null;
        zzdrv.zza(inputStream, "input");
        this.zzhij = inputStream;
        this.buffer = new byte[i];
        this.zzaif = 0;
        this.pos = 0;
        this.zzhim = 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
        if (r2[r3] >= 0) goto L_0x0068;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int zzayu() throws java.io.IOException {
        /*
            r5 = this;
            int r0 = r5.pos
            int r1 = r5.zzaif
            if (r1 == r0) goto L_0x006b
            byte[] r2 = r5.buffer
            int r3 = r0 + 1
            byte r0 = r2[r0]
            if (r0 < 0) goto L_0x0011
            r5.pos = r3
            return r0
        L_0x0011:
            int r1 = r1 - r3
            r4 = 9
            if (r1 < r4) goto L_0x006b
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 7
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x0022
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
            goto L_0x0068
        L_0x0022:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x002f
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
        L_0x002d:
            r1 = r3
            goto L_0x0068
        L_0x002f:
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 21
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x003d
            r2 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r2
            goto L_0x0068
        L_0x003d:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r4 = r1 << 28
            r0 = r0 ^ r4
            r4 = 266354560(0xfe03f80, float:2.2112565E-29)
            r0 = r0 ^ r4
            if (r1 >= 0) goto L_0x002d
            int r1 = r3 + 1
            byte r3 = r2[r3]
            if (r3 >= 0) goto L_0x0068
            int r3 = r1 + 1
            byte r1 = r2[r1]
            if (r1 >= 0) goto L_0x002d
            int r1 = r3 + 1
            byte r3 = r2[r3]
            if (r3 >= 0) goto L_0x0068
            int r3 = r1 + 1
            byte r1 = r2[r1]
            if (r1 >= 0) goto L_0x002d
            int r1 = r3 + 1
            byte r2 = r2[r3]
            if (r2 < 0) goto L_0x006b
        L_0x0068:
            r5.pos = r1
            return r0
        L_0x006b:
            long r0 = r5.zzayr()
            int r1 = (int) r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqx.zzayu():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b0, code lost:
        if (((long) r2[r0]) >= 0) goto L_0x00b4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final long zzayv() throws java.io.IOException {
        /*
            r11 = this;
            int r0 = r11.pos
            int r1 = r11.zzaif
            if (r1 == r0) goto L_0x00b8
            byte[] r2 = r11.buffer
            int r3 = r0 + 1
            byte r0 = r2[r0]
            if (r0 < 0) goto L_0x0012
            r11.pos = r3
            long r0 = (long) r0
            return r0
        L_0x0012:
            int r1 = r1 - r3
            r4 = 9
            if (r1 < r4) goto L_0x00b8
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 7
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x0025
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
        L_0x0022:
            long r2 = (long) r0
            goto L_0x00b5
        L_0x0025:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0036
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            long r0 = (long) r0
            r9 = r0
            r1 = r3
            r2 = r9
            goto L_0x00b5
        L_0x0036:
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 21
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x0044
            r2 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r2
            goto L_0x0022
        L_0x0044:
            long r3 = (long) r0
            int r0 = r1 + 1
            byte r1 = r2[r1]
            long r5 = (long) r1
            r1 = 28
            long r5 = r5 << r1
            long r3 = r3 ^ r5
            r5 = 0
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 < 0) goto L_0x005b
            r1 = 266354560(0xfe03f80, double:1.315966377E-315)
        L_0x0057:
            long r2 = r3 ^ r1
            r1 = r0
            goto L_0x00b5
        L_0x005b:
            int r1 = r0 + 1
            byte r0 = r2[r0]
            long r7 = (long) r0
            r0 = 35
            long r7 = r7 << r0
            long r3 = r3 ^ r7
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0070
            r5 = -34093383808(0xfffffff80fe03f80, double:NaN)
        L_0x006d:
            long r2 = r3 ^ r5
            goto L_0x00b5
        L_0x0070:
            int r0 = r1 + 1
            byte r1 = r2[r1]
            long r7 = (long) r1
            r1 = 42
            long r7 = r7 << r1
            long r3 = r3 ^ r7
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 < 0) goto L_0x0083
            r1 = 4363953127296(0x3f80fe03f80, double:2.1560793202584E-311)
            goto L_0x0057
        L_0x0083:
            int r1 = r0 + 1
            byte r0 = r2[r0]
            long r7 = (long) r0
            r0 = 49
            long r7 = r7 << r0
            long r3 = r3 ^ r7
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0096
            r5 = -558586000294016(0xfffe03f80fe03f80, double:NaN)
            goto L_0x006d
        L_0x0096:
            int r0 = r1 + 1
            byte r1 = r2[r1]
            long r7 = (long) r1
            r1 = 56
            long r7 = r7 << r1
            long r3 = r3 ^ r7
            r7 = 71499008037633920(0xfe03f80fe03f80, double:6.838959413692434E-304)
            long r3 = r3 ^ r7
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 >= 0) goto L_0x00b3
            int r1 = r0 + 1
            byte r0 = r2[r0]
            long r7 = (long) r0
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x00b8
            goto L_0x00b4
        L_0x00b3:
            r1 = r0
        L_0x00b4:
            r2 = r3
        L_0x00b5:
            r11.pos = r1
            return r2
        L_0x00b8:
            long r0 = r11.zzayr()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqx.zzayv():long");
    }

    private final int zzayw() throws IOException {
        int i = this.pos;
        if (this.zzaif - i < 4) {
            zzfm(4);
            i = this.pos;
        }
        byte[] bArr = this.buffer;
        this.pos = i + 4;
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    private final long zzayx() throws IOException {
        int i = this.pos;
        if (this.zzaif - i < 8) {
            zzfm(8);
            i = this.pos;
        }
        byte[] bArr = this.buffer;
        this.pos = i + 8;
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    private final void zzayy() {
        this.zzaif += this.zzhik;
        int i = this.zzhim;
        int i2 = this.zzaif;
        int i3 = i + i2;
        int i4 = this.zzhin;
        if (i3 > i4) {
            this.zzhik = i3 - i4;
            this.zzaif = i2 - this.zzhik;
            return;
        }
        this.zzhik = 0;
    }

    private final byte zzayz() throws IOException {
        if (this.pos == this.zzaif) {
            zzfm(1);
        }
        byte[] bArr = this.buffer;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i];
    }

    private final void zzfm(int i) throws IOException {
        if (zzfn(i)) {
            return;
        }
        if (i > (this.zzhig - this.zzhim) - this.pos) {
            throw zzdse.zzbap();
        }
        throw zzdse.zzbaj();
    }

    private final boolean zzfn(int i) throws IOException {
        do {
            int i2 = this.pos;
            int i3 = i2 + i;
            int i4 = this.zzaif;
            if (i3 > i4) {
                int i5 = this.zzhig;
                int i6 = this.zzhim;
                if (i > (i5 - i6) - i2 || i6 + i2 + i > this.zzhin) {
                    return false;
                }
                if (i2 > 0) {
                    if (i4 > i2) {
                        byte[] bArr = this.buffer;
                        System.arraycopy(bArr, i2, bArr, 0, i4 - i2);
                    }
                    this.zzhim += i2;
                    this.zzaif -= i2;
                    this.pos = 0;
                }
                InputStream inputStream = this.zzhij;
                byte[] bArr2 = this.buffer;
                int i7 = this.zzaif;
                int read = inputStream.read(bArr2, i7, Math.min(bArr2.length - i7, (this.zzhig - this.zzhim) - i7));
                if (read == 0 || read < -1 || read > this.buffer.length) {
                    String valueOf = String.valueOf(this.zzhij.getClass());
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 91);
                    sb.append(valueOf);
                    sb.append("#read(byte[]) returned invalid result: ");
                    sb.append(read);
                    sb.append("\nThe InputStream implementation is buggy.");
                    throw new IllegalStateException(sb.toString());
                } else if (read <= 0) {
                    return false;
                } else {
                    this.zzaif += read;
                    zzayy();
                }
            } else {
                StringBuilder sb2 = new StringBuilder(77);
                sb2.append("refillBuffer() called when ");
                sb2.append(i);
                sb2.append(" bytes were already available in buffer");
                throw new IllegalStateException(sb2.toString());
            }
        } while (this.zzaif < i);
        return true;
    }

    private final byte[] zzfo(int i) throws IOException {
        if (i == 0) {
            return zzdrv.zzhng;
        }
        if (i >= 0) {
            int i2 = this.zzhim;
            int i3 = this.pos;
            int i4 = i2 + i3 + i;
            if (i4 - this.zzhig <= 0) {
                int i5 = this.zzhin;
                if (i4 <= i5) {
                    int i6 = this.zzaif - i3;
                    int i7 = i - i6;
                    if (i7 >= 4096 && i7 > this.zzhij.available()) {
                        return null;
                    }
                    byte[] bArr = new byte[i];
                    System.arraycopy(this.buffer, this.pos, bArr, 0, i6);
                    this.zzhim += this.zzaif;
                    this.pos = 0;
                    this.zzaif = 0;
                    while (i6 < bArr.length) {
                        int read = this.zzhij.read(bArr, i6, i - i6);
                        if (read != -1) {
                            this.zzhim += read;
                            i6 += read;
                        } else {
                            throw zzdse.zzbaj();
                        }
                    }
                    return bArr;
                }
                zzfq((i5 - i2) - i3);
                throw zzdse.zzbaj();
            }
            throw zzdse.zzbap();
        }
        throw zzdse.zzbak();
    }

    private final List<byte[]> zzfp(int i) throws IOException {
        ArrayList arrayList = new ArrayList();
        while (i > 0) {
            byte[] bArr = new byte[Math.min(i, 4096)];
            int i2 = 0;
            while (i2 < bArr.length) {
                int read = this.zzhij.read(bArr, i2, bArr.length - i2);
                if (read != -1) {
                    this.zzhim += read;
                    i2 += read;
                } else {
                    throw zzdse.zzbaj();
                }
            }
            i -= bArr.length;
            arrayList.add(bArr);
        }
        return arrayList;
    }

    private final void zzfq(int i) throws IOException {
        int i2 = this.zzaif;
        int i3 = this.pos;
        if (i <= i2 - i3 && i >= 0) {
            this.pos = i3 + i;
        } else if (i >= 0) {
            int i4 = this.zzhim;
            int i5 = this.pos;
            int i6 = i4 + i5 + i;
            int i7 = this.zzhin;
            if (i6 <= i7) {
                this.zzhim = i4 + i5;
                int i8 = this.zzaif - i5;
                this.zzaif = 0;
                this.pos = 0;
                while (i8 < i) {
                    try {
                        long j = (long) (i - i8);
                        long skip = this.zzhij.skip(j);
                        int i9 = (skip > 0 ? 1 : (skip == 0 ? 0 : -1));
                        if (i9 >= 0 && skip <= j) {
                            if (i9 == 0) {
                                break;
                            }
                            i8 += (int) skip;
                        } else {
                            String valueOf = String.valueOf(this.zzhij.getClass());
                            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 92);
                            sb.append(valueOf);
                            sb.append("#skip returned invalid result: ");
                            sb.append(skip);
                            sb.append("\nThe InputStream implementation is buggy.");
                            throw new IllegalStateException(sb.toString());
                        }
                    } catch (Throwable th) {
                        this.zzhim += i8;
                        zzayy();
                        throw th;
                    }
                }
                this.zzhim += i8;
                zzayy();
                if (i8 < i) {
                    int i10 = this.zzaif;
                    int i11 = i10 - this.pos;
                    this.pos = i10;
                    zzfm(1);
                    while (true) {
                        int i12 = i - i11;
                        int i13 = this.zzaif;
                        if (i12 > i13) {
                            i11 += i13;
                            this.pos = i13;
                            zzfm(1);
                        } else {
                            this.pos = i12;
                            return;
                        }
                    }
                }
            } else {
                zzfq((i7 - i4) - i5);
                throw zzdse.zzbaj();
            }
        } else {
            throw zzdse.zzbak();
        }
    }

    private final byte[] zzg(int i, boolean z) throws IOException {
        byte[] zzfo = zzfo(i);
        if (zzfo != null) {
            return zzfo;
        }
        int i2 = this.pos;
        int i3 = this.zzaif;
        int i4 = i3 - i2;
        this.zzhim += i3;
        this.pos = 0;
        this.zzaif = 0;
        List<byte[]> zzfp = zzfp(i - i4);
        byte[] bArr = new byte[i];
        System.arraycopy(this.buffer, i2, bArr, 0, i4);
        for (byte[] next : zzfp) {
            System.arraycopy(next, 0, bArr, i4, next.length);
            i4 += next.length;
        }
        return bArr;
    }

    public final double readDouble() throws IOException {
        return Double.longBitsToDouble(zzayx());
    }

    public final float readFloat() throws IOException {
        return Float.intBitsToFloat(zzayw());
    }

    public final String readString() throws IOException {
        int zzayu = zzayu();
        if (zzayu > 0) {
            int i = this.zzaif;
            int i2 = this.pos;
            if (zzayu <= i - i2) {
                String str = new String(this.buffer, i2, zzayu, zzdrv.UTF_8);
                this.pos += zzayu;
                return str;
            }
        }
        if (zzayu == 0) {
            return "";
        }
        if (zzayu > this.zzaif) {
            return new String(zzg(zzayu, false), zzdrv.UTF_8);
        }
        zzfm(zzayu);
        String str2 = new String(this.buffer, this.pos, zzayu, zzdrv.UTF_8);
        this.pos += zzayu;
        return str2;
    }

    public final int zzayc() throws IOException {
        if (zzays()) {
            this.zzhil = 0;
            return 0;
        }
        this.zzhil = zzayu();
        int i = this.zzhil;
        if ((i >>> 3) != 0) {
            return i;
        }
        throw zzdse.zzbam();
    }

    public final long zzayd() throws IOException {
        return zzayv();
    }

    public final long zzaye() throws IOException {
        return zzayv();
    }

    public final int zzayf() throws IOException {
        return zzayu();
    }

    public final long zzayg() throws IOException {
        return zzayx();
    }

    public final int zzayh() throws IOException {
        return zzayw();
    }

    public final boolean zzayi() throws IOException {
        return zzayv() != 0;
    }

    public final String zzayj() throws IOException {
        byte[] bArr;
        int zzayu = zzayu();
        int i = this.pos;
        int i2 = 0;
        if (zzayu <= this.zzaif - i && zzayu > 0) {
            bArr = this.buffer;
            this.pos = i + zzayu;
            i2 = i;
        } else if (zzayu == 0) {
            return "";
        } else {
            if (zzayu <= this.zzaif) {
                zzfm(zzayu);
                bArr = this.buffer;
                this.pos = zzayu;
            } else {
                bArr = zzg(zzayu, false);
            }
        }
        return zzdva.zzo(bArr, i2, zzayu);
    }

    public final zzdqk zzayk() throws IOException {
        int zzayu = zzayu();
        int i = this.zzaif;
        int i2 = this.pos;
        if (zzayu <= i - i2 && zzayu > 0) {
            zzdqk zzi = zzdqk.zzi(this.buffer, i2, zzayu);
            this.pos += zzayu;
            return zzi;
        } else if (zzayu == 0) {
            return zzdqk.zzhhx;
        } else {
            byte[] zzfo = zzfo(zzayu);
            if (zzfo != null) {
                return zzdqk.zzu(zzfo);
            }
            int i3 = this.pos;
            int i4 = this.zzaif;
            int i5 = i4 - i3;
            this.zzhim += i4;
            this.pos = 0;
            this.zzaif = 0;
            List<byte[]> zzfp = zzfp(zzayu - i5);
            byte[] bArr = new byte[zzayu];
            System.arraycopy(this.buffer, i3, bArr, 0, i5);
            for (byte[] next : zzfp) {
                System.arraycopy(next, 0, bArr, i5, next.length);
                i5 += next.length;
            }
            return zzdqk.zzv(bArr);
        }
    }

    public final int zzayl() throws IOException {
        return zzayu();
    }

    public final int zzaym() throws IOException {
        return zzayu();
    }

    public final int zzayn() throws IOException {
        return zzayw();
    }

    public final long zzayo() throws IOException {
        return zzayx();
    }

    public final int zzayp() throws IOException {
        return zzdqw.zzfl(zzayu());
    }

    public final long zzayq() throws IOException {
        return zzdqw.zzff(zzayv());
    }

    /* access modifiers changed from: package-private */
    public final long zzayr() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte zzayz = zzayz();
            j |= ((long) (zzayz & Byte.MAX_VALUE)) << i;
            if ((zzayz & 128) == 0) {
                return j;
            }
        }
        throw zzdse.zzbal();
    }

    public final boolean zzays() throws IOException {
        return this.pos == this.zzaif && !zzfn(1);
    }

    public final int zzayt() {
        return this.zzhim + this.pos;
    }

    public final void zzfh(int i) throws zzdse {
        if (this.zzhil != i) {
            throw zzdse.zzban();
        }
    }

    public final boolean zzfi(int i) throws IOException {
        int zzayc;
        int i2 = i & 7;
        int i3 = 0;
        if (i2 == 0) {
            if (this.zzaif - this.pos >= 10) {
                while (i3 < 10) {
                    byte[] bArr = this.buffer;
                    int i4 = this.pos;
                    this.pos = i4 + 1;
                    if (bArr[i4] < 0) {
                        i3++;
                    }
                }
                throw zzdse.zzbal();
            }
            while (i3 < 10) {
                if (zzayz() < 0) {
                    i3++;
                }
            }
            throw zzdse.zzbal();
            return true;
        } else if (i2 == 1) {
            zzfq(8);
            return true;
        } else if (i2 == 2) {
            zzfq(zzayu());
            return true;
        } else if (i2 == 3) {
            do {
                zzayc = zzayc();
                if (zzayc == 0 || !zzfi(zzayc)) {
                    zzfh(((i >>> 3) << 3) | 4);
                }
                zzayc = zzayc();
                break;
            } while (!zzfi(zzayc));
            zzfh(((i >>> 3) << 3) | 4);
            return true;
        } else if (i2 == 4) {
            return false;
        } else {
            if (i2 == 5) {
                zzfq(4);
                return true;
            }
            throw zzdse.zzbao();
        }
    }

    public final int zzfj(int i) throws zzdse {
        if (i >= 0) {
            int i2 = i + this.zzhim + this.pos;
            int i3 = this.zzhin;
            if (i2 <= i3) {
                this.zzhin = i2;
                zzayy();
                return i3;
            }
            throw zzdse.zzbaj();
        }
        throw zzdse.zzbak();
    }

    public final void zzfk(int i) {
        this.zzhin = i;
        zzayy();
    }
}
