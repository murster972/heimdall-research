package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Collections;

public final class zzbzv extends zzahe implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener, zzabr {
    private boolean zzegh = false;
    private zzxb zzfma;
    private View zzfmf;
    private zzbwk zzfnf;
    private boolean zzfqa = false;

    public zzbzv(zzbwk zzbwk, zzbws zzbws) {
        this.zzfmf = zzbws.zzaje();
        this.zzfma = zzbws.getVideoController();
        this.zzfnf = zzbwk;
        if (zzbws.zzajf() != null) {
            zzbws.zzajf().zza((zzabr) this);
        }
    }

    private final void zzakn() {
        View view = this.zzfmf;
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.zzfmf);
            }
        }
    }

    private final void zzako() {
        View view;
        zzbwk zzbwk = this.zzfnf;
        if (zzbwk != null && (view = this.zzfmf) != null) {
            zzbwk.zzb(view, Collections.emptyMap(), Collections.emptyMap(), zzbwk.zzy(this.zzfmf));
        }
    }

    public final void destroy() throws RemoteException {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzakn();
        zzbwk zzbwk = this.zzfnf;
        if (zzbwk != null) {
            zzbwk.destroy();
        }
        this.zzfnf = null;
        this.zzfmf = null;
        this.zzfma = null;
        this.zzegh = true;
    }

    public final zzxb getVideoController() throws RemoteException {
        Preconditions.a("#008 Must be called on the main UI thread.");
        if (!this.zzegh) {
            return this.zzfma;
        }
        zzayu.zzex("getVideoController: Instream ad should not be used after destroyed");
        return null;
    }

    public final void onGlobalLayout() {
        zzako();
    }

    public final void onScrollChanged() {
        zzako();
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzahg zzahg) throws RemoteException {
        Preconditions.a("#008 Must be called on the main UI thread.");
        if (this.zzegh) {
            zzayu.zzex("Instream ad can not be shown after destroy().");
            zza(zzahg, 2);
        } else if (this.zzfmf == null || this.zzfma == null) {
            String str = this.zzfmf == null ? "can not get video view." : "can not get video controller.";
            zzayu.zzex(str.length() != 0 ? "Instream internal error: ".concat(str) : new String("Instream internal error: "));
            zza(zzahg, 0);
        } else if (this.zzfqa) {
            zzayu.zzex("Instream ad should not be used again.");
            zza(zzahg, 1);
        } else {
            this.zzfqa = true;
            zzakn();
            ((ViewGroup) ObjectWrapper.a(iObjectWrapper)).addView(this.zzfmf, new ViewGroup.LayoutParams(-1, -1));
            zzq.zzln();
            zzazt.zza(this.zzfmf, (ViewTreeObserver.OnGlobalLayoutListener) this);
            zzq.zzln();
            zzazt.zza(this.zzfmf, (ViewTreeObserver.OnScrollChangedListener) this);
            zzako();
            try {
                zzahg.zzrv();
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzakp() {
        try {
            destroy();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void zzr(IObjectWrapper iObjectWrapper) throws RemoteException {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zza(iObjectWrapper, (zzahg) new zzbzx(this));
    }

    public final void zzrb() {
        zzawb.zzdsr.post(new zzbzy(this));
    }

    private static void zza(zzahg zzahg, int i) {
        try {
            zzahg.zzcn(i);
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
