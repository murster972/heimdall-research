package com.google.android.gms.internal.ads;

public final class zzbul implements zzdxg<zzbdi> {
    private final zzbtv zzfje;

    private zzbul(zzbtv zzbtv) {
        this.zzfje = zzbtv;
    }

    public static zzbul zzc(zzbtv zzbtv) {
        return new zzbul(zzbtv);
    }

    public final /* synthetic */ Object get() {
        return this.zzfje.zzaft();
    }
}
