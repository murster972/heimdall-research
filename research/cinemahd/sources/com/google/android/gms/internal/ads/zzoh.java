package com.google.android.gms.internal.ads;

public final class zzoh {
    public final int height;
    public final int width;
    private final int zzbge;
    public final float zzbgf;
    private final boolean zzbgg;
    private final boolean zzbgh;
    private final int zzbgi;
    private final int zzbgj;
    private final int zzbgk;
    private final boolean zzbgl;

    public zzoh(int i, int i2, int i3, float f, boolean z, boolean z2, int i4, int i5, int i6, boolean z3) {
        this.zzbge = i;
        this.width = i2;
        this.height = i3;
        this.zzbgf = f;
        this.zzbgg = z;
        this.zzbgh = z2;
        this.zzbgi = i4;
        this.zzbgj = i5;
        this.zzbgk = i6;
        this.zzbgl = z3;
    }
}
