package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;

final class zzbu implements zzdsa {
    static final zzdsa zzew = new zzbu();

    private zzbu() {
    }

    public final boolean zzf(int i) {
        return zzbs.zza.C0036zza.zzg(i) != null;
    }
}
