package com.google.android.gms.internal.ads;

import android.content.Context;

public interface zzcww {
    zzcwx zzaeb();

    zzcww zzbt(Context context);

    zzcww zzfq(String str);
}
