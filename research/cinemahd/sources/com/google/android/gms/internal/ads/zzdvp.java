package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvq;

public final class zzdvp<M extends zzdvq<M>, T> {
    protected final Class<T> zzgxv;

    public final boolean equals(Object obj) {
        throw new NoSuchMethodError();
    }

    public final int hashCode() {
        throw new NoSuchMethodError();
    }
}
