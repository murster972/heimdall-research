package com.google.android.gms.internal.ads;

import android.view.View;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;

public class zzbtv {
    private final zzbdi zzcza;
    private final zzbuv zzfjd;

    public zzbtv(zzbuv zzbuv) {
        this(zzbuv, (zzbdi) null);
    }

    public Set<zzbsu<zzbov>> zza(zzbva zzbva) {
        return Collections.singleton(zzbsu.zzb(zzbva, zzazd.zzdwj));
    }

    public final zzbdi zzaft() {
        return this.zzcza;
    }

    public final zzbuv zzaie() {
        return this.zzfjd;
    }

    public final View zzaif() {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi != null) {
            return zzbdi.getWebView();
        }
        return null;
    }

    public final View zzaig() {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi == null) {
            return null;
        }
        return zzbdi.getWebView();
    }

    public final zzbsu<zzbrb> zzb(Executor executor) {
        return new zzbsu<>(new zzbtx(this.zzcza), executor);
    }

    public zzbtv(zzbuv zzbuv, zzbdi zzbdi) {
        this.zzfjd = zzbuv;
        this.zzcza = zzbdi;
    }
}
