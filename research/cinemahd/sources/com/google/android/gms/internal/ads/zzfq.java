package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfq extends zzfw {
    private final zzer zzvl;
    private long zzze;

    public zzfq(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2, zzer zzer) {
        super(zzei, str, str2, zzb, i, 53);
        this.zzvl = zzer;
        if (zzer != null) {
            this.zzze = zzer.zzcl();
        }
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (this.zzvl != null) {
            this.zzzt.zzbl(((Long) this.zzaae.invoke((Object) null, new Object[]{Long.valueOf(this.zzze)})).longValue());
        }
    }
}
