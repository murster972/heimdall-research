package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.gass.zzf;
import java.lang.ref.WeakReference;

public final class zzcbb extends zzbmd {
    private final zzasf zzdny;
    private final WeakReference<zzbdi> zzfix;
    private final zzbsk zzfiy;
    private final zzbmx zzfja;
    private final zzf zzfjb;
    private final zzbuv zzfjd;
    private boolean zzfqa = false;
    private final zzbqa zzfqp;
    private final zzboz zzfra;
    private final Context zzup;

    zzcbb(zzbmg zzbmg, Context context, zzbdi zzbdi, zzbuv zzbuv, zzbsk zzbsk, zzboz zzboz, zzbqa zzbqa, zzbmx zzbmx, zzczl zzczl, zzf zzf) {
        super(zzbmg);
        this.zzup = context;
        this.zzfjd = zzbuv;
        this.zzfix = new WeakReference<>(zzbdi);
        this.zzfiy = zzbsk;
        this.zzfra = zzboz;
        this.zzfqp = zzbqa;
        this.zzfja = zzbmx;
        this.zzfjb = zzf;
        this.zzdny = new zzatc(zzczl.zzdky);
    }

    public final void finalize() throws Throwable {
        try {
            zzbdi zzbdi = (zzbdi) this.zzfix.get();
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcrd)).booleanValue()) {
                if (!this.zzfqa && zzbdi != null) {
                    zzdhd zzdhd = zzazd.zzdwi;
                    zzbdi.getClass();
                    zzdhd.execute(zzcbe.zzh(zzbdi));
                }
            } else if (zzbdi != null) {
                zzbdi.destroy();
            }
        } finally {
            super.finalize();
        }
    }

    public final Bundle getAdMetadata() {
        return this.zzfqp.getAdMetadata();
    }

    public final boolean isClosed() {
        return this.zzfja.isClosed();
    }

    public final boolean zzaks() {
        return this.zzfqa;
    }

    public final void zzb(boolean z, Activity activity) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcin)).booleanValue()) {
            zzq.zzkq();
            if (zzawb.zzau(this.zzup)) {
                zzayu.zzez("Rewarded ads that show when your app is in the background are a violation of AdMob policies and may lead to blocked ad serving. To learn more, visit https://googlemobileadssdk.page.link/admob-interstitial-policies");
                this.zzfra.zzco(3);
                if (((Boolean) zzve.zzoy().zzd(zzzn.zzcio)).booleanValue()) {
                    this.zzfjb.a(this.zzfbl.zzgmi.zzgmf.zzbzo);
                    return;
                }
                return;
            }
        }
        if (this.zzfqa) {
            zzayu.zzez("The rewarded ad have been showed.");
            this.zzfra.zzco(1);
            return;
        }
        this.zzfqa = true;
        this.zzfiy.zzahx();
        Context context = activity;
        if (activity == null) {
            context = this.zzup;
        }
        this.zzfjd.zza(z, context);
    }

    public final zzasf zzpz() {
        return this.zzdny;
    }

    public final boolean zzqa() {
        zzbdi zzbdi = (zzbdi) this.zzfix.get();
        return zzbdi != null && !zzbdi.zzaap();
    }
}
