package com.google.android.gms.internal.ads;

public abstract class zzaef extends zzgb implements zzaeg {
    public zzaef() {
        super("com.google.android.gms.ads.internal.formats.client.IUnifiedNativeAd");
    }

    /* JADX WARNING: type inference failed for: r2v2, types: [android.os.IInterface] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(int r1, android.os.Parcel r2, android.os.Parcel r3, int r4) throws android.os.RemoteException {
        /*
            r0 = this;
            switch(r1) {
                case 2: goto L_0x0178;
                case 3: goto L_0x016d;
                case 4: goto L_0x0162;
                case 5: goto L_0x0157;
                case 6: goto L_0x014c;
                case 7: goto L_0x0141;
                case 8: goto L_0x0136;
                case 9: goto L_0x012b;
                case 10: goto L_0x0120;
                case 11: goto L_0x0115;
                case 12: goto L_0x0109;
                case 13: goto L_0x0101;
                case 14: goto L_0x00f5;
                case 15: goto L_0x00e5;
                case 16: goto L_0x00d1;
                case 17: goto L_0x00c1;
                case 18: goto L_0x00b5;
                case 19: goto L_0x00a9;
                case 20: goto L_0x009d;
                case 21: goto L_0x0079;
                case 22: goto L_0x0071;
                case 23: goto L_0x0065;
                case 24: goto L_0x0059;
                case 25: goto L_0x0049;
                case 26: goto L_0x0039;
                case 27: goto L_0x0031;
                case 28: goto L_0x0029;
                case 29: goto L_0x001d;
                case 30: goto L_0x0011;
                case 31: goto L_0x0005;
                default: goto L_0x0003;
            }
        L_0x0003:
            r1 = 0
            return r1
        L_0x0005:
            com.google.android.gms.internal.ads.zzxa r1 = r0.zzkb()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x0182
        L_0x0011:
            boolean r1 = r0.isCustomClickGestureEnabled()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.writeBoolean(r3, r1)
            goto L_0x0182
        L_0x001d:
            com.google.android.gms.internal.ads.zzacd r1 = r0.zzrq()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x0182
        L_0x0029:
            r0.recordCustomClickGesture()
            r3.writeNoException()
            goto L_0x0182
        L_0x0031:
            r0.zzrp()
            r3.writeNoException()
            goto L_0x0182
        L_0x0039:
            android.os.IBinder r1 = r2.readStrongBinder()
            com.google.android.gms.internal.ads.zzwn r1 = com.google.android.gms.internal.ads.zzwq.zzf(r1)
            r0.zza((com.google.android.gms.internal.ads.zzwn) r1)
            r3.writeNoException()
            goto L_0x0182
        L_0x0049:
            android.os.IBinder r1 = r2.readStrongBinder()
            com.google.android.gms.internal.ads.zzwr r1 = com.google.android.gms.internal.ads.zzwu.zzg(r1)
            r0.zza((com.google.android.gms.internal.ads.zzwr) r1)
            r3.writeNoException()
            goto L_0x0182
        L_0x0059:
            boolean r1 = r0.isCustomMuteThisAdEnabled()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.writeBoolean(r3, r1)
            goto L_0x0182
        L_0x0065:
            java.util.List r1 = r0.getMuteThisAdReasons()
            r3.writeNoException()
            r3.writeList(r1)
            goto L_0x0182
        L_0x0071:
            r0.cancelUnconfirmedClick()
            r3.writeNoException()
            goto L_0x0182
        L_0x0079:
            android.os.IBinder r1 = r2.readStrongBinder()
            if (r1 != 0) goto L_0x0081
            r1 = 0
            goto L_0x0095
        L_0x0081:
            java.lang.String r2 = "com.google.android.gms.ads.internal.formats.client.IUnconfirmedClickListener"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r4 = r2 instanceof com.google.android.gms.internal.ads.zzaeb
            if (r4 == 0) goto L_0x008f
            r1 = r2
            com.google.android.gms.internal.ads.zzaeb r1 = (com.google.android.gms.internal.ads.zzaeb) r1
            goto L_0x0095
        L_0x008f:
            com.google.android.gms.internal.ads.zzaed r2 = new com.google.android.gms.internal.ads.zzaed
            r2.<init>(r1)
            r1 = r2
        L_0x0095:
            r0.zza((com.google.android.gms.internal.ads.zzaeb) r1)
            r3.writeNoException()
            goto L_0x0182
        L_0x009d:
            android.os.Bundle r1 = r0.getExtras()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zzb(r3, r1)
            goto L_0x0182
        L_0x00a9:
            com.google.android.gms.dynamic.IObjectWrapper r1 = r0.zzri()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x0182
        L_0x00b5:
            com.google.android.gms.dynamic.IObjectWrapper r1 = r0.zzrf()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x0182
        L_0x00c1:
            android.os.Parcelable$Creator r1 = android.os.Bundle.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            android.os.Bundle r1 = (android.os.Bundle) r1
            r0.reportTouchEvent(r1)
            r3.writeNoException()
            goto L_0x0182
        L_0x00d1:
            android.os.Parcelable$Creator r1 = android.os.Bundle.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            android.os.Bundle r1 = (android.os.Bundle) r1
            boolean r1 = r0.recordImpression(r1)
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.writeBoolean(r3, r1)
            goto L_0x0182
        L_0x00e5:
            android.os.Parcelable$Creator r1 = android.os.Bundle.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            android.os.Bundle r1 = (android.os.Bundle) r1
            r0.performClick(r1)
            r3.writeNoException()
            goto L_0x0182
        L_0x00f5:
            com.google.android.gms.internal.ads.zzaca r1 = r0.zzrh()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x0182
        L_0x0101:
            r0.destroy()
            r3.writeNoException()
            goto L_0x0182
        L_0x0109:
            java.lang.String r1 = r0.getMediationAdapterClassName()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x0182
        L_0x0115:
            com.google.android.gms.internal.ads.zzxb r1 = r0.getVideoController()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x0182
        L_0x0120:
            java.lang.String r1 = r0.getPrice()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x0182
        L_0x012b:
            java.lang.String r1 = r0.getStore()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x0182
        L_0x0136:
            double r1 = r0.getStarRating()
            r3.writeNoException()
            r3.writeDouble(r1)
            goto L_0x0182
        L_0x0141:
            java.lang.String r1 = r0.getAdvertiser()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x0182
        L_0x014c:
            java.lang.String r1 = r0.getCallToAction()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x0182
        L_0x0157:
            com.google.android.gms.internal.ads.zzaci r1 = r0.zzrg()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x0182
        L_0x0162:
            java.lang.String r1 = r0.getBody()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x0182
        L_0x016d:
            java.util.List r1 = r0.getImages()
            r3.writeNoException()
            r3.writeList(r1)
            goto L_0x0182
        L_0x0178:
            java.lang.String r1 = r0.getHeadline()
            r3.writeNoException()
            r3.writeString(r1)
        L_0x0182:
            r1 = 1
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzaef.zza(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
