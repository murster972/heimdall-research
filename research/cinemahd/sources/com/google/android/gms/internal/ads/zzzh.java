package com.google.android.gms.internal.ads;

import android.content.SharedPreferences;
import org.json.JSONObject;

final class zzzh {
    private final /* synthetic */ SharedPreferences.Editor zzcfv;
    private final /* synthetic */ JSONObject zzcfw;

    zzzh(zzzi zzzi, SharedPreferences.Editor editor, JSONObject jSONObject) {
        this.zzcfv = editor;
        this.zzcfw = jSONObject;
    }
}
