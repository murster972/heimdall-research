package com.google.android.gms.internal.ads;

public interface zzky {
    public static final zzky zzazf = new zzkx();

    zzkt zzb(String str, boolean z) throws zzlb;

    zzkt zzhb() throws zzlb;
}
