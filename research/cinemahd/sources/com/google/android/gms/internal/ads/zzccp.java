package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzccp implements zzdxg<zzbsu<zzbqx>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzccw> zzfdd;

    private zzccp(zzdxp<zzccw> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzccp zzq(zzdxp<zzccw> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzccp(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
