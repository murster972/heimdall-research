package com.google.android.gms.internal.cast;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.facebook.ads.AdError;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.zzad;
import com.google.android.gms.cast.zzag;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClient;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public final class zzdd extends GmsClient<zzdp> {
    /* access modifiers changed from: private */
    public static final zzdw zzbf = new zzdw("CastClientImpl");
    /* access modifiers changed from: private */
    public static final Object zzyr = new Object();
    private static final Object zzys = new Object();
    private final Bundle extras;
    /* access modifiers changed from: private */
    public final Cast.Listener zzak;
    private double zzet;
    private boolean zzeu;
    /* access modifiers changed from: private */
    public final CastDevice zzie;
    /* access modifiers changed from: private */
    public ApplicationMetadata zzxz;
    /* access modifiers changed from: private */
    public final Map<String, Cast.MessageReceivedCallback> zzya = new HashMap();
    private final long zzyb;
    private zzdf zzyc;
    /* access modifiers changed from: private */
    public String zzyd;
    private boolean zzye;
    private boolean zzyf;
    private boolean zzyg;
    private zzad zzyh;
    private int zzyi;
    private int zzyj;
    private final AtomicLong zzyk = new AtomicLong(0);
    /* access modifiers changed from: private */
    public String zzyl;
    /* access modifiers changed from: private */
    public String zzym;
    private Bundle zzyn;
    private final Map<Long, BaseImplementation$ResultHolder<Status>> zzyo = new HashMap();
    /* access modifiers changed from: private */
    public BaseImplementation$ResultHolder<Cast.ApplicationConnectionResult> zzyp;
    private BaseImplementation$ResultHolder<Status> zzyq;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzdd(Context context, Looper looper, ClientSettings clientSettings, CastDevice castDevice, long j, Cast.Listener listener, Bundle bundle, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 10, clientSettings, connectionCallbacks, onConnectionFailedListener);
        this.zzie = castDevice;
        this.zzak = listener;
        this.zzyb = j;
        this.extras = bundle;
        zzet();
    }

    private final void zzc(BaseImplementation$ResultHolder<Status> baseImplementation$ResultHolder) {
        synchronized (zzys) {
            if (this.zzyq != null) {
                baseImplementation$ResultHolder.setResult(new Status(AdError.INTERNAL_ERROR_CODE));
            } else {
                this.zzyq = baseImplementation$ResultHolder;
            }
        }
    }

    /* access modifiers changed from: private */
    public final void zzet() {
        this.zzyg = false;
        this.zzyi = -1;
        this.zzyj = -1;
        this.zzxz = null;
        this.zzyd = null;
        this.zzet = 0.0d;
        this.zzeu = false;
        this.zzyh = null;
    }

    private final void zzeu() {
        zzbf.d("removing all MessageReceivedCallbacks", new Object[0]);
        synchronized (this.zzya) {
            this.zzya.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r1.zzyc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzev() {
        /*
            r1 = this;
            boolean r0 = r1.zzyg
            if (r0 == 0) goto L_0x0010
            com.google.android.gms.internal.cast.zzdf r0 = r1.zzyc
            if (r0 == 0) goto L_0x0010
            boolean r0 = r0.isDisposed()
            if (r0 != 0) goto L_0x0010
            r0 = 1
            return r0
        L_0x0010:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzdd.zzev():boolean");
    }

    /* access modifiers changed from: private */
    public final void zzr(int i) {
        synchronized (zzys) {
            if (this.zzyq != null) {
                this.zzyq.setResult(new Status(i));
                this.zzyq = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ IInterface createServiceInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.internal.ICastDeviceController");
        if (queryLocalInterface instanceof zzdp) {
            return (zzdp) queryLocalInterface;
        }
        return new zzdq(iBinder);
    }

    public final void disconnect() {
        zzbf.d("disconnect(); ServiceListener=%s, isConnected=%b", this.zzyc, Boolean.valueOf(isConnected()));
        zzdf zzdf = this.zzyc;
        this.zzyc = null;
        if (zzdf == null || zzdf.zzex() == null) {
            zzbf.d("already disposed, so short-circuiting", new Object[0]);
            return;
        }
        zzeu();
        try {
            ((zzdp) getService()).disconnect();
        } catch (RemoteException | IllegalStateException e) {
            zzbf.zza(e, "Error while disconnecting the controller interface: %s", e.getMessage());
        } finally {
            super.disconnect();
        }
    }

    public final int getActiveInputState() throws IllegalStateException {
        checkConnected();
        return this.zzyi;
    }

    public final ApplicationMetadata getApplicationMetadata() throws IllegalStateException {
        checkConnected();
        return this.zzxz;
    }

    public final String getApplicationStatus() throws IllegalStateException {
        checkConnected();
        return this.zzyd;
    }

    public final Bundle getConnectionHint() {
        Bundle bundle = this.zzyn;
        if (bundle == null) {
            return super.getConnectionHint();
        }
        this.zzyn = null;
        return bundle;
    }

    /* access modifiers changed from: protected */
    public final Bundle getGetServiceRequestExtraArgs() {
        Bundle bundle = new Bundle();
        zzbf.d("getRemoteService(): mLastApplicationId=%s, mLastSessionId=%s", this.zzyl, this.zzym);
        this.zzie.a(bundle);
        bundle.putLong("com.google.android.gms.cast.EXTRA_CAST_FLAGS", this.zzyb);
        Bundle bundle2 = this.extras;
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
        this.zzyc = new zzdf(this);
        bundle.putParcelable("listener", new BinderWrapper(this.zzyc.asBinder()));
        String str = this.zzyl;
        if (str != null) {
            bundle.putString("last_application_id", str);
            String str2 = this.zzym;
            if (str2 != null) {
                bundle.putString("last_session_id", str2);
            }
        }
        return bundle;
    }

    public final int getMinApkVersion() {
        return 12800000;
    }

    /* access modifiers changed from: protected */
    public final String getServiceDescriptor() {
        return "com.google.android.gms.cast.internal.ICastDeviceController";
    }

    public final int getStandbyState() throws IllegalStateException {
        checkConnected();
        return this.zzyj;
    }

    /* access modifiers changed from: protected */
    public final String getStartServiceAction() {
        return "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE";
    }

    public final double getVolume() throws IllegalStateException {
        checkConnected();
        return this.zzet;
    }

    public final boolean isMute() throws IllegalStateException {
        checkConnected();
        return this.zzeu;
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        super.onConnectionFailed(connectionResult);
        zzeu();
    }

    /* access modifiers changed from: protected */
    public final void onPostInitHandler(int i, IBinder iBinder, Bundle bundle, int i2) {
        zzbf.d("in onPostInitHandler; statusCode=%d", Integer.valueOf(i));
        if (i == 0 || i == 1001) {
            this.zzyg = true;
            this.zzye = true;
            this.zzyf = true;
        } else {
            this.zzyg = false;
        }
        if (i == 1001) {
            this.zzyn = new Bundle();
            this.zzyn.putBoolean("com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING", true);
            i = 0;
        }
        super.onPostInitHandler(i, iBinder, bundle, i2);
    }

    public final void removeMessageReceivedCallbacks(String str) throws IllegalArgumentException, RemoteException {
        Cast.MessageReceivedCallback remove;
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.zzya) {
                remove = this.zzya.remove(str);
            }
            if (remove != null) {
                try {
                    ((zzdp) getService()).zzt(str);
                } catch (IllegalStateException e) {
                    zzbf.zza(e, "Error unregistering namespace (%s): %s", str, e.getMessage());
                }
            }
        } else {
            throw new IllegalArgumentException("Channel namespace cannot be null or empty");
        }
    }

    public final void requestStatus() throws IllegalStateException, RemoteException {
        zzdp zzdp = (zzdp) getService();
        if (zzev()) {
            zzdp.requestStatus();
        }
    }

    public final void setMessageReceivedCallbacks(String str, Cast.MessageReceivedCallback messageReceivedCallback) throws IllegalArgumentException, IllegalStateException, RemoteException {
        zzdk.zzp(str);
        removeMessageReceivedCallbacks(str);
        if (messageReceivedCallback != null) {
            synchronized (this.zzya) {
                this.zzya.put(str, messageReceivedCallback);
            }
            zzdp zzdp = (zzdp) getService();
            if (zzev()) {
                zzdp.zzs(str);
            }
        }
    }

    public final void setMute(boolean z) throws IllegalStateException, RemoteException {
        zzdp zzdp = (zzdp) getService();
        if (zzev()) {
            zzdp.zza(z, this.zzet, this.zzeu);
        }
    }

    public final void setVolume(double d) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            StringBuilder sb = new StringBuilder(41);
            sb.append("Volume cannot be ");
            sb.append(d);
            throw new IllegalArgumentException(sb.toString());
        }
        zzdp zzdp = (zzdp) getService();
        if (zzev()) {
            zzdp.zza(d, this.zzet, this.zzeu);
        }
    }

    public final void zza(String str, String str2, BaseImplementation$ResultHolder<Status> baseImplementation$ResultHolder) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (TextUtils.isEmpty(str2)) {
            throw new IllegalArgumentException("The message payload cannot be null or empty");
        } else if (str2.length() <= 524288) {
            zzdk.zzp(str);
            long incrementAndGet = this.zzyk.incrementAndGet();
            try {
                this.zzyo.put(Long.valueOf(incrementAndGet), baseImplementation$ResultHolder);
                zzdp zzdp = (zzdp) getService();
                if (zzev()) {
                    zzdp.zza(str, str2, incrementAndGet);
                } else {
                    zzb(incrementAndGet, 2016);
                }
            } catch (Throwable th) {
                this.zzyo.remove(Long.valueOf(incrementAndGet));
                throw th;
            }
        } else {
            zzbf.w("Message send failed. Message exceeds maximum size", new Object[0]);
            throw new IllegalArgumentException("Message exceeds maximum size");
        }
    }

    public final void zzb(BaseImplementation$ResultHolder<Status> baseImplementation$ResultHolder) throws IllegalStateException, RemoteException {
        zzc(baseImplementation$ResultHolder);
        zzdp zzdp = (zzdp) getService();
        if (zzev()) {
            zzdp.zzfa();
        } else {
            zzr(2016);
        }
    }

    public final void zzq(int i) {
        synchronized (zzyr) {
            if (this.zzyp != null) {
                this.zzyp.setResult(new zzde(new Status(i)));
                this.zzyp = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public final void zzb(long j, int i) {
        BaseImplementation$ResultHolder remove;
        synchronized (this.zzyo) {
            remove = this.zzyo.remove(Long.valueOf(j));
        }
        if (remove != null) {
            remove.setResult(new Status(i));
        }
    }

    public final void zza(String str, LaunchOptions launchOptions, BaseImplementation$ResultHolder<Cast.ApplicationConnectionResult> baseImplementation$ResultHolder) throws IllegalStateException, RemoteException {
        zza(baseImplementation$ResultHolder);
        zzdp zzdp = (zzdp) getService();
        if (zzev()) {
            zzdp.zzb(str, launchOptions);
        } else {
            zzq(2016);
        }
    }

    public final void zza(String str, String str2, zzag zzag, BaseImplementation$ResultHolder<Cast.ApplicationConnectionResult> baseImplementation$ResultHolder) throws IllegalStateException, RemoteException {
        zza(baseImplementation$ResultHolder);
        if (zzag == null) {
            zzag = new zzag();
        }
        zzdp zzdp = (zzdp) getService();
        if (zzev()) {
            zzdp.zza(str, str2, zzag);
        } else {
            zzq(2016);
        }
    }

    private final void zza(BaseImplementation$ResultHolder<Cast.ApplicationConnectionResult> baseImplementation$ResultHolder) {
        synchronized (zzyr) {
            if (this.zzyp != null) {
                this.zzyp.setResult(new zzde(new Status(AdError.CACHE_ERROR_CODE)));
            }
            this.zzyp = baseImplementation$ResultHolder;
        }
    }

    public final void zza(String str, BaseImplementation$ResultHolder<Status> baseImplementation$ResultHolder) throws IllegalStateException, RemoteException {
        zzc(baseImplementation$ResultHolder);
        zzdp zzdp = (zzdp) getService();
        if (zzev()) {
            zzdp.zzj(str);
        } else {
            zzr(2016);
        }
    }

    /* access modifiers changed from: private */
    public final void zza(zzdl zzdl) {
        boolean z;
        boolean z2;
        boolean z3;
        ApplicationMetadata applicationMetadata = zzdl.getApplicationMetadata();
        if (!zzdk.zza(applicationMetadata, this.zzxz)) {
            this.zzxz = applicationMetadata;
            this.zzak.onApplicationMetadataChanged(this.zzxz);
        }
        double volume = zzdl.getVolume();
        if (Double.isNaN(volume) || Math.abs(volume - this.zzet) <= 1.0E-7d) {
            z = false;
        } else {
            this.zzet = volume;
            z = true;
        }
        boolean zzey = zzdl.zzey();
        if (zzey != this.zzeu) {
            this.zzeu = zzey;
            z = true;
        }
        zzbf.d("hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z), Boolean.valueOf(this.zzyf));
        if (this.zzak != null && (z || this.zzyf)) {
            this.zzak.onVolumeChanged();
        }
        int activeInputState = zzdl.getActiveInputState();
        if (activeInputState != this.zzyi) {
            this.zzyi = activeInputState;
            z2 = true;
        } else {
            z2 = false;
        }
        zzbf.d("hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z2), Boolean.valueOf(this.zzyf));
        if (this.zzak != null && (z2 || this.zzyf)) {
            this.zzak.onActiveInputStateChanged(this.zzyi);
        }
        int standbyState = zzdl.getStandbyState();
        if (standbyState != this.zzyj) {
            this.zzyj = standbyState;
            z3 = true;
        } else {
            z3 = false;
        }
        zzbf.d("hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z3), Boolean.valueOf(this.zzyf));
        if (this.zzak != null && (z3 || this.zzyf)) {
            this.zzak.onStandbyStateChanged(this.zzyj);
        }
        if (!zzdk.zza(this.zzyh, zzdl.zzez())) {
            this.zzyh = zzdl.zzez();
        }
        this.zzyf = false;
    }

    /* access modifiers changed from: private */
    public final void zza(zzct zzct) {
        boolean z;
        String zzep = zzct.zzep();
        if (!zzdk.zza(zzep, this.zzyd)) {
            this.zzyd = zzep;
            z = true;
        } else {
            z = false;
        }
        zzbf.d("hasChanged=%b, mFirstApplicationStatusUpdate=%b", Boolean.valueOf(z), Boolean.valueOf(this.zzye));
        if (this.zzak != null && (z || this.zzye)) {
            this.zzak.onApplicationStatusChanged();
        }
        this.zzye = false;
    }
}
