package com.google.android.gms.internal.ads;

public interface zzgn {
    void zza(zzgl zzgl);

    void zza(zzhc zzhc);

    void zza(zzhg zzhg, Object obj);

    void zza(zzmr zzmr, zzng zzng);

    void zza(boolean z, int i);

    void zzed();

    void zzg(boolean z);
}
