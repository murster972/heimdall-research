package com.google.android.gms.internal.ads;

final class zzkq {
    public int zzavh;
    public final zzks zzaxc;
    public final zzku zzaxd;
    public final zzjo zzaxe;

    public zzkq(zzks zzks, zzku zzku, zzjo zzjo) {
        this.zzaxc = zzks;
        this.zzaxd = zzku;
        this.zzaxe = zzjo;
    }
}
