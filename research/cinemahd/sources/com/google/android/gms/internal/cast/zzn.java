package com.google.android.gms.internal.cast;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityManager;
import android.widget.RelativeLayout;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.R$layout;
import com.google.android.gms.cast.framework.internal.featurehighlight.zza;
import com.google.android.gms.cast.framework.internal.featurehighlight.zzi;

public final class zzn extends RelativeLayout implements IntroductoryOverlay {
    private int color;
    /* access modifiers changed from: private */
    public Activity zzij;
    private View zzik;
    private String zzim;
    /* access modifiers changed from: private */
    public IntroductoryOverlay.OnOverlayDismissedListener zzin;
    private final boolean zzje;
    /* access modifiers changed from: private */
    public zza zzjf;
    /* access modifiers changed from: private */
    public boolean zzjg;

    @TargetApi(15)
    public zzn(IntroductoryOverlay.Builder builder) {
        super(builder.b());
        this.zzij = builder.b();
        this.zzje = builder.g();
        this.zzin = builder.e();
        this.zzik = builder.d();
        this.zzim = builder.h();
        this.color = builder.f();
    }

    /* access modifiers changed from: private */
    public final void reset() {
        removeAllViews();
        this.zzij = null;
        this.zzin = null;
        this.zzik = null;
        this.zzjf = null;
        this.zzim = null;
        this.color = 0;
        this.zzjg = false;
    }

    static boolean zzg(Context context) {
        AccessibilityManager accessibilityManager = (AccessibilityManager) context.getSystemService("accessibility");
        return accessibilityManager != null && accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled();
    }

    public final void remove() {
        if (this.zzjg) {
            ((ViewGroup) this.zzij.getWindow().getDecorView()).removeView(this);
            reset();
        }
    }

    public final void show() {
        Activity activity = this.zzij;
        if (activity != null && this.zzik != null && !this.zzjg && !zzg(activity)) {
            if (!this.zzje || !IntroductoryOverlay.zza.b(this.zzij)) {
                this.zzjf = new zza(this.zzij);
                int i = this.color;
                if (i != 0) {
                    this.zzjf.a(i);
                }
                addView(this.zzjf);
                zzi zzi = (zzi) this.zzij.getLayoutInflater().inflate(R$layout.cast_help_text, this.zzjf, false);
                zzi.setText(this.zzim, (CharSequence) null);
                this.zzjf.a(zzi);
                this.zzjf.a(this.zzik, (View) null, true, new zzo(this));
                this.zzjg = true;
                ((ViewGroup) this.zzij.getWindow().getDecorView()).addView(this);
                this.zzjf.a((Runnable) null);
                return;
            }
            reset();
        }
    }
}
