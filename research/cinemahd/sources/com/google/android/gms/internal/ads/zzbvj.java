package com.google.android.gms.internal.ads;

public final class zzbvj implements zzdxg<zzbwz> {
    private final zzbvi zzfjv;

    private zzbvj(zzbvi zzbvi) {
        this.zzfjv = zzbvi;
    }

    public static zzbvj zzc(zzbvi zzbvi) {
        return new zzbvj(zzbvi);
    }

    public static zzbwz zzd(zzbvi zzbvi) {
        return (zzbwz) zzdxm.zza(zzbvi.zzaij(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzd(this.zzfjv);
    }
}
