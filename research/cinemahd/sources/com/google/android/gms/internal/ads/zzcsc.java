package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcsc implements zzcty<Bundle> {
    private final Bundle zzdjn;

    public zzcsc(Bundle bundle) {
        this.zzdjn = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        Bundle zza = zzdaa.zza(bundle, "device");
        zza.putBundle("android_mem_info", this.zzdjn);
        bundle.putBundle("device", zza);
    }
}
