package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcya implements zzcxo {
    private final zzare zzfhs;

    zzcya(zzare zzare) {
        this.zzfhs = zzare;
    }

    public final void zzt(Object obj) {
        zzare zzare = this.zzfhs;
        ((zzasl) obj).zza(new zzatc(zzare.getType(), zzare.getAmount()));
    }
}
