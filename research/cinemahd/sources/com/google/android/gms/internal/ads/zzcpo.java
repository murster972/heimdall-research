package com.google.android.gms.internal.ads;

import java.util.concurrent.TimeUnit;

public final class zzcpo implements zzdxg<zzdhe<zzcps>> {
    private final zzdxp<zzdcr> zzfet;
    private final zzdxp<zzdhe<zzaqk>> zzfgk;
    private final zzdxp<zzcpr> zzgee;

    public zzcpo(zzdxp<zzdcr> zzdxp, zzdxp<zzcpr> zzdxp2, zzdxp<zzdhe<zzaqk>> zzdxp3) {
        this.zzfet = zzdxp;
        this.zzgee = zzdxp2;
        this.zzfgk = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return (zzdhe) zzdxm.zza(this.zzfet.get().zza(zzdco.GENERATE_SIGNALS, this.zzfgk.get()).zza(this.zzgee.get()).zza((long) ((Integer) zzve.zzoy().zzd(zzzn.zzcox)).intValue(), TimeUnit.SECONDS).zzaqg(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
