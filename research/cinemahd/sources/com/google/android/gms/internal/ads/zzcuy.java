package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcuy implements Callable {
    private final zzcuz zzghr;

    zzcuy(zzcuz zzcuz) {
        this.zzghr = zzcuz;
    }

    public final Object call() {
        return this.zzghr.zzans();
    }
}
