package com.google.android.gms.internal.ads;

enum zzdrp {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);
    
    private final boolean zzhmd;

    private zzdrp(boolean z) {
        this.zzhmd = z;
    }
}
