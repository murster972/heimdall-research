package com.google.android.gms.internal.ads;

public final class zzcqo implements zzdxg<zzcqm> {
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<zzczu> zzfep;

    private zzcqo(zzdxp<zzdhd> zzdxp, zzdxp<zzczu> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzfep = zzdxp2;
    }

    public static zzcqo zzal(zzdxp<zzdhd> zzdxp, zzdxp<zzczu> zzdxp2) {
        return new zzcqo(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcqm(this.zzfcv.get(), this.zzfep.get());
    }
}
