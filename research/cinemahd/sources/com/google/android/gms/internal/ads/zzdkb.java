package com.google.android.gms.internal.ads;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.ECPrivateKeySpec;

final class zzdkb extends zzdik<zzdib, zzdmk> {
    zzdkb(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdmk zzdmk = (zzdmk) obj;
        zzdmj zzauf = zzdmk.zzaum().zzauf();
        zzdmo zzauh = zzauf.zzauh();
        zzdox zza = zzdkk.zza(zzauh.zzauu());
        byte[] byteArray = zzdmk.zzass().toByteArray();
        ECPrivateKeySpec eCPrivateKeySpec = new ECPrivateKeySpec(new BigInteger(1, byteArray), zzdov.zza(zza));
        return new zzdoo((ECPrivateKey) zzdoy.zzhgm.zzhd("EC").generatePrivate(eCPrivateKeySpec), zzauh.zzauw().toByteArray(), zzdkk.zza(zzauh.zzauv()), zzdkk.zza(zzauf.zzauj()), new zzdkm(zzauf.zzaui().zzauc()));
    }
}
