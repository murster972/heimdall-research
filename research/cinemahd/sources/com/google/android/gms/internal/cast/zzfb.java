package com.google.android.gms.internal.cast;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import androidx.collection.SimpleArrayMap;

public class zzfb extends AnimatorListenerAdapter {
    private SimpleArrayMap<Animator, Boolean> zzabi = new SimpleArrayMap<>();

    public void onAnimationCancel(Animator animator) {
        this.zzabi.put(animator, true);
    }

    public void onAnimationStart(Animator animator) {
        this.zzabi.put(animator, false);
    }

    /* access modifiers changed from: protected */
    public final boolean zzb(Animator animator) {
        return this.zzabi.containsKey(animator) && this.zzabi.get(animator).booleanValue();
    }
}
