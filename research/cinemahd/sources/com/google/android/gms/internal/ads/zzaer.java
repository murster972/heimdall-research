package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

public final class zzaer extends zzady {
    private final UnifiedNativeAd.OnUnifiedNativeAdLoadedListener zzcwm;

    public zzaer(UnifiedNativeAd.OnUnifiedNativeAdLoadedListener onUnifiedNativeAdLoadedListener) {
        this.zzcwm = onUnifiedNativeAdLoadedListener;
    }

    public final void zza(zzaeg zzaeg) {
        this.zzcwm.onUnifiedNativeAdLoaded(new zzaeh(zzaeg));
    }
}
