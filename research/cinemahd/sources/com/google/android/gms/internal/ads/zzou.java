package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

final class zzou implements Parcelable.Creator<zzor> {
    zzou() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzor(parcel);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzor[0];
    }
}
