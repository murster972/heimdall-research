package com.google.android.gms.internal.ads;

final class zzdhr extends zzdha<zzdhe<V>> {
    private final zzdgd<V> zzgxn;
    private final /* synthetic */ zzdhs zzgxo;

    zzdhr(zzdhs zzdhs, zzdgd<V> zzdgd) {
        this.zzgxo = zzdhs;
        this.zzgxn = (zzdgd) zzdei.checkNotNull(zzdgd);
    }

    /* access modifiers changed from: package-private */
    public final boolean isDone() {
        return this.zzgxo.isDone();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzars() throws Exception {
        return (zzdhe) zzdei.zza(this.zzgxn.zzanm(), "AsyncCallable.call returned null instead of a Future. Did you mean to return immediateFuture(null)? %s", this.zzgxn);
    }

    /* access modifiers changed from: package-private */
    public final String zzart() {
        return this.zzgxn.toString();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(Object obj, Throwable th) {
        zzdhe zzdhe = (zzdhe) obj;
        if (th == null) {
            this.zzgxo.setFuture(zzdhe);
        } else {
            this.zzgxo.setException(th);
        }
    }
}
