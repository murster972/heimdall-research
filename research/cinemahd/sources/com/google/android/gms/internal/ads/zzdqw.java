package com.google.android.gms.internal.ads;

import java.io.IOException;

public abstract class zzdqw {
    int zzhie;
    int zzhif;
    int zzhig;
    zzdqz zzhih;
    private boolean zzhii;

    private zzdqw() {
        this.zzhif = 100;
        this.zzhig = Integer.MAX_VALUE;
        this.zzhii = false;
    }

    static zzdqw zzb(byte[] bArr, int i, int i2, boolean z) {
        zzdqy zzdqy = new zzdqy(bArr, i, i2, z);
        try {
            zzdqy.zzfj(i2);
            return zzdqy;
        } catch (zzdse e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static long zzff(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static int zzfl(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public abstract double readDouble() throws IOException;

    public abstract float readFloat() throws IOException;

    public abstract String readString() throws IOException;

    public abstract int zzayc() throws IOException;

    public abstract long zzayd() throws IOException;

    public abstract long zzaye() throws IOException;

    public abstract int zzayf() throws IOException;

    public abstract long zzayg() throws IOException;

    public abstract int zzayh() throws IOException;

    public abstract boolean zzayi() throws IOException;

    public abstract String zzayj() throws IOException;

    public abstract zzdqk zzayk() throws IOException;

    public abstract int zzayl() throws IOException;

    public abstract int zzaym() throws IOException;

    public abstract int zzayn() throws IOException;

    public abstract long zzayo() throws IOException;

    public abstract int zzayp() throws IOException;

    public abstract long zzayq() throws IOException;

    /* access modifiers changed from: package-private */
    public abstract long zzayr() throws IOException;

    public abstract boolean zzays() throws IOException;

    public abstract int zzayt();

    public abstract void zzfh(int i) throws zzdse;

    public abstract boolean zzfi(int i) throws IOException;

    public abstract int zzfj(int i) throws zzdse;

    public abstract void zzfk(int i);
}
