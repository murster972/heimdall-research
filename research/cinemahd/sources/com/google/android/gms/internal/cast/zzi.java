package com.google.android.gms.internal.cast;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.zzab;
import com.google.android.gms.cast.framework.zzh;
import com.google.android.gms.cast.framework.zzj;
import com.google.android.gms.cast.framework.zzl;
import com.google.android.gms.cast.framework.zzr;
import com.google.android.gms.cast.framework.zzt;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.cast.zzah;
import java.util.Map;

public final class zzi extends zza implements zzh {
    zzi(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.internal.ICastDynamiteModule");
    }

    public final zzj zza(IObjectWrapper iObjectWrapper, CastOptions castOptions, zzj zzj, Map map) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) iObjectWrapper);
        zzc.zza(zza, (Parcelable) castOptions);
        zzc.zza(zza, (IInterface) zzj);
        zza.writeMap(map);
        Parcel zza2 = zza(1, zza);
        zzj a2 = zzj.zza.a(zza2.readStrongBinder());
        zza2.recycle();
        return a2;
    }

    public final zzt zza(String str, String str2, zzab zzab) throws RemoteException {
        Parcel zza = zza();
        zza.writeString(str);
        zza.writeString(str2);
        zzc.zza(zza, (IInterface) zzab);
        Parcel zza2 = zza(2, zza);
        zzt a2 = zzt.zza.a(zza2.readStrongBinder());
        zza2.recycle();
        return a2;
    }

    public final zzl zza(CastOptions castOptions, IObjectWrapper iObjectWrapper, zzh zzh) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (Parcelable) castOptions);
        zzc.zza(zza, (IInterface) iObjectWrapper);
        zzc.zza(zza, (IInterface) zzh);
        Parcel zza2 = zza(3, zza);
        zzl a2 = zzl.zza.a(zza2.readStrongBinder());
        zza2.recycle();
        return a2;
    }

    public final zzr zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) iObjectWrapper);
        zzc.zza(zza, (IInterface) iObjectWrapper2);
        zzc.zza(zza, (IInterface) iObjectWrapper3);
        Parcel zza2 = zza(5, zza);
        zzr a2 = zzr.zza.a(zza2.readStrongBinder());
        zza2.recycle();
        return a2;
    }

    public final zzah zza(IObjectWrapper iObjectWrapper, zzaj zzaj, int i, int i2, boolean z, long j, int i3, int i4, int i5) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) iObjectWrapper);
        zzc.zza(zza, (IInterface) zzaj);
        zza.writeInt(i);
        zza.writeInt(i2);
        zzc.writeBoolean(zza, z);
        zza.writeLong(j);
        zza.writeInt(i3);
        zza.writeInt(i4);
        zza.writeInt(i5);
        Parcel zza2 = zza(6, zza);
        zzah zze = zzah.zza.zze(zza2.readStrongBinder());
        zza2.recycle();
        return zze;
    }
}
