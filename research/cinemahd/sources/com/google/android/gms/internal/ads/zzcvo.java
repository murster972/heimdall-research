package com.google.android.gms.internal.ads;

import android.location.Location;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzcvo implements zzcty<JSONObject> {
    private final Location zzmi;

    public zzcvo(Location location) {
        this.zzmi = location;
    }

    public final /* synthetic */ void zzr(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        try {
            if (this.zzmi != null) {
                JSONObject jSONObject2 = new JSONObject();
                Float valueOf = Float.valueOf(this.zzmi.getAccuracy() * 1000.0f);
                Long valueOf2 = Long.valueOf(this.zzmi.getTime() * 1000);
                Long valueOf3 = Long.valueOf((long) (this.zzmi.getLatitude() * 1.0E7d));
                Long valueOf4 = Long.valueOf((long) (this.zzmi.getLongitude() * 1.0E7d));
                jSONObject2.put("radius", valueOf);
                jSONObject2.put("lat", valueOf3);
                jSONObject2.put("long", valueOf4);
                jSONObject2.put("time", valueOf2);
                jSONObject.put("uule", jSONObject2);
            }
        } catch (JSONException e) {
            zzavs.zza("Failed adding location to the request JSON.", e);
        }
    }
}
