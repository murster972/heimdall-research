package com.google.android.gms.internal.ads;

import android.view.View;

public final class zzbke extends zzbmd {
    private final View view;
    private final zzbdi zzcza;
    private final zzczk zzfdo;
    private final int zzfdp;
    private zzrl zzfdt;

    zzbke(zzbmg zzbmg, View view2, zzbdi zzbdi, zzczk zzczk, int i) {
        super(zzbmg);
        this.view = view2;
        this.zzcza = zzbdi;
        this.zzfdo = zzczk;
        this.zzfdp = i;
    }

    public final void zza(zzra zzra) {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi != null) {
            zzbdi.zza(zzra);
        }
    }

    public final boolean zzaat() {
        zzbdi zzbdi = this.zzcza;
        return (zzbdi == null || zzbdi.zzaaa() == null || !this.zzcza.zzaaa().zzaat()) ? false : true;
    }

    public final int zzafw() {
        return this.zzfdp;
    }

    public final zzczk zzafz() {
        return zzczy.zza(this.zzffc.zzgln, this.zzfdo);
    }

    public final View zzaga() {
        return this.view;
    }

    public final boolean zzagb() {
        zzbdi zzbdi = this.zzcza;
        return zzbdi != null && zzbdi.zzaac();
    }

    public final zzrl zzagc() {
        return this.zzfdt;
    }

    public final void zza(zzrl zzrl) {
        this.zzfdt = zzrl;
    }
}
