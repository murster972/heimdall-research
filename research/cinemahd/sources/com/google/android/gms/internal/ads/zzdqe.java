package com.google.android.gms.internal.ads;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

abstract class zzdqe<E> extends AbstractList<E> implements zzdsb<E> {
    private boolean zzhhm = true;

    zzdqe() {
    }

    public boolean add(E e) {
        zzaxr();
        return super.add(e);
    }

    public boolean addAll(Collection<? extends E> collection) {
        zzaxr();
        return super.addAll(collection);
    }

    public void clear() {
        zzaxr();
        super.clear();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        if (!(obj instanceof RandomAccess)) {
            return super.equals(obj);
        }
        List list = (List) obj;
        int size = size();
        if (size != list.size()) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!get(i).equals(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    public E remove(int i) {
        zzaxr();
        return super.remove(i);
    }

    public boolean removeAll(Collection<?> collection) {
        zzaxr();
        return super.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        zzaxr();
        return super.retainAll(collection);
    }

    public E set(int i, E e) {
        zzaxr();
        return super.set(i, e);
    }

    public boolean zzaxp() {
        return this.zzhhm;
    }

    public final void zzaxq() {
        this.zzhhm = false;
    }

    /* access modifiers changed from: protected */
    public final void zzaxr() {
        if (!this.zzhhm) {
            throw new UnsupportedOperationException();
        }
    }

    public void add(int i, E e) {
        zzaxr();
        super.add(i, e);
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        zzaxr();
        return super.addAll(i, collection);
    }

    public boolean remove(Object obj) {
        zzaxr();
        return super.remove(obj);
    }
}
