package com.google.android.gms.internal.ads;

public final class zzcsd implements zzdxg<zzcsb> {
    private final zzdxp<zzdhd> zzfcv;

    private zzcsd(zzdxp<zzdhd> zzdxp) {
        this.zzfcv = zzdxp;
    }

    public static zzcsd zzaj(zzdxp<zzdhd> zzdxp) {
        return new zzcsd(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcsb(this.zzfcv.get());
    }
}
