package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

final class zzlh implements Parcelable.Creator<zzli> {
    zzlh() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzli(parcel);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzli[i];
    }
}
