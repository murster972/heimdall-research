package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzahg extends IInterface {
    void zzcn(int i) throws RemoteException;

    void zzrv() throws RemoteException;
}
