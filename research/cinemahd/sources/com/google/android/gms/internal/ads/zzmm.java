package com.google.android.gms.internal.ads;

final class zzmm {
    public final long zzaum;
    public final long zzbcu;
    public boolean zzbcv;
    public zznk zzbcw;
    public zzmm zzbcx;

    public zzmm(long j, int i) {
        this.zzbcu = j;
        this.zzaum = j + ((long) i);
    }

    public final zzmm zzic() {
        this.zzbcw = null;
        return this.zzbcx;
    }
}
