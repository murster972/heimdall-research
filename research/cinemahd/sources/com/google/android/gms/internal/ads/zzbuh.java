package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbuh implements zzdxg<zzbsu<zzbov>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbve> zzfdq;
    private final zzbtv zzfje;

    private zzbuh(zzbtv zzbtv, zzdxp<zzbve> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfje = zzbtv;
        this.zzfdq = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzbuh zzb(zzbtv zzbtv, zzdxp<zzbve> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzbuh(zzbtv, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
