package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import androidx.collection.SimpleArrayMap;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.unity3d.services.purchasing.core.TransactionErrorDetailsUtilities;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.Collections;
import java.util.List;

public final class zzbws {
    private Bundle extras;
    private zzaca zzcvq;
    private List<zzxy> zzcwf = Collections.emptyList();
    private List<?> zzeim;
    private double zzeip;
    private float zzejb;
    private IObjectWrapper zzffd;
    private int zzflz;
    private zzxb zzfma;
    private View zzfmb;
    private zzxy zzfmc;
    private zzbdi zzfmd;
    private zzbdi zzfme;
    private View zzfmf;
    private IObjectWrapper zzfmg;
    private zzaci zzfmh;
    private zzaci zzfmi;
    private String zzfmj;
    private SimpleArrayMap<String, zzabu> zzfmk = new SimpleArrayMap<>();
    private SimpleArrayMap<String, String> zzfml = new SimpleArrayMap<>();
    private String zzfmm;

    private final synchronized void setMediaContentAspectRatio(float f) {
        this.zzejb = f;
    }

    private static <T> T zzar(IObjectWrapper iObjectWrapper) {
        if (iObjectWrapper == null) {
            return null;
        }
        return ObjectWrapper.a(iObjectWrapper);
    }

    private final synchronized String zzfy(String str) {
        return this.zzfml.get(str);
    }

    public final synchronized void destroy() {
        if (this.zzfmd != null) {
            this.zzfmd.destroy();
            this.zzfmd = null;
        }
        if (this.zzfme != null) {
            this.zzfme.destroy();
            this.zzfme = null;
        }
        this.zzffd = null;
        this.zzfmk.clear();
        this.zzfml.clear();
        this.zzfma = null;
        this.zzcvq = null;
        this.zzfmb = null;
        this.zzeim = null;
        this.extras = null;
        this.zzfmf = null;
        this.zzfmg = null;
        this.zzfmh = null;
        this.zzfmi = null;
        this.zzfmj = null;
    }

    public final synchronized String getAdvertiser() {
        return zzfy(VisionDataDBAdapter.VisionDataColumns.COLUMN_ADVERTISER);
    }

    public final synchronized String getBody() {
        return zzfy("body");
    }

    public final synchronized String getCallToAction() {
        return zzfy("call_to_action");
    }

    public final synchronized String getCustomTemplateId() {
        return this.zzfmj;
    }

    public final synchronized Bundle getExtras() {
        if (this.extras == null) {
            this.extras = new Bundle();
        }
        return this.extras;
    }

    public final synchronized String getHeadline() {
        return zzfy("headline");
    }

    public final synchronized List<?> getImages() {
        return this.zzeim;
    }

    public final synchronized float getMediaContentAspectRatio() {
        return this.zzejb;
    }

    public final synchronized List<zzxy> getMuteThisAdReasons() {
        return this.zzcwf;
    }

    public final synchronized String getPrice() {
        return zzfy("price");
    }

    public final synchronized double getStarRating() {
        return this.zzeip;
    }

    public final synchronized String getStore() {
        return zzfy(TransactionErrorDetailsUtilities.STORE);
    }

    public final synchronized zzxb getVideoController() {
        return this.zzfma;
    }

    public final synchronized void setImages(List<zzabu> list) {
        this.zzeim = list;
    }

    public final synchronized void setStarRating(double d) {
        this.zzeip = d;
    }

    public final synchronized void zza(zzaca zzaca) {
        this.zzcvq = zzaca;
    }

    public final synchronized void zzab(View view) {
        this.zzfmf = view;
    }

    public final synchronized int zzaja() {
        return this.zzflz;
    }

    public final synchronized View zzajb() {
        return this.zzfmb;
    }

    public final zzaci zzajc() {
        List<?> list = this.zzeim;
        if (!(list == null || list.size() == 0)) {
            Object obj = this.zzeim.get(0);
            if (obj instanceof IBinder) {
                return zzach.zzm((IBinder) obj);
            }
        }
        return null;
    }

    public final synchronized zzxy zzajd() {
        return this.zzfmc;
    }

    public final synchronized View zzaje() {
        return this.zzfmf;
    }

    public final synchronized zzbdi zzajf() {
        return this.zzfmd;
    }

    public final synchronized zzbdi zzajg() {
        return this.zzfme;
    }

    public final synchronized IObjectWrapper zzajh() {
        return this.zzffd;
    }

    public final synchronized SimpleArrayMap<String, zzabu> zzaji() {
        return this.zzfmk;
    }

    public final synchronized String zzajj() {
        return this.zzfmm;
    }

    public final synchronized SimpleArrayMap<String, String> zzajk() {
        return this.zzfml;
    }

    public final synchronized void zzaq(IObjectWrapper iObjectWrapper) {
        this.zzffd = iObjectWrapper;
    }

    public final synchronized void zzb(zzxb zzxb) {
        this.zzfma = zzxb;
    }

    public final synchronized void zzdj(int i) {
        this.zzflz = i;
    }

    public final synchronized void zzf(List<zzxy> list) {
        this.zzcwf = list;
    }

    public final synchronized void zzfw(String str) {
        this.zzfmj = str;
    }

    public final synchronized void zzfx(String str) {
        this.zzfmm = str;
    }

    public final synchronized void zzi(zzbdi zzbdi) {
        this.zzfmd = zzbdi;
    }

    public final synchronized void zzj(zzbdi zzbdi) {
        this.zzfme = zzbdi;
    }

    public final synchronized void zzn(String str, String str2) {
        if (str2 == null) {
            this.zzfml.remove(str);
        } else {
            this.zzfml.put(str, str2);
        }
    }

    public final synchronized zzaci zzrg() {
        return this.zzfmh;
    }

    public final synchronized zzaca zzrh() {
        return this.zzcvq;
    }

    public final synchronized IObjectWrapper zzri() {
        return this.zzfmg;
    }

    public final synchronized zzaci zzrj() {
        return this.zzfmi;
    }

    public final synchronized void zza(zzxy zzxy) {
        this.zzfmc = zzxy;
    }

    public final synchronized void zzb(zzaci zzaci) {
        this.zzfmi = zzaci;
    }

    public static zzbws zzb(zzalr zzalr) {
        try {
            return zza(zzalr.getVideoController(), zzalr.zzrh(), (View) zzar(zzalr.zzsu()), zzalr.getHeadline(), zzalr.getImages(), zzalr.getBody(), zzalr.getExtras(), zzalr.getCallToAction(), (View) zzar(zzalr.zzsv()), zzalr.zzri(), zzalr.getStore(), zzalr.getPrice(), zzalr.getStarRating(), zzalr.zzrg(), zzalr.getAdvertiser(), zzalr.getMediaContentAspectRatio());
        } catch (RemoteException e) {
            zzayu.zzd("Failed to get native ad assets from unified ad mapper", e);
            return null;
        }
    }

    public final synchronized void zza(zzaci zzaci) {
        this.zzfmh = zzaci;
    }

    public final synchronized void zza(String str, zzabu zzabu) {
        if (zzabu == null) {
            this.zzfmk.remove(str);
        } else {
            this.zzfmk.put(str, zzabu);
        }
    }

    public static zzbws zza(zzalq zzalq) {
        try {
            zzxb videoController = zzalq.getVideoController();
            zzaca zzrh = zzalq.zzrh();
            String headline = zzalq.getHeadline();
            List<?> images = zzalq.getImages();
            String body = zzalq.getBody();
            Bundle extras2 = zzalq.getExtras();
            String callToAction = zzalq.getCallToAction();
            IObjectWrapper zzri = zzalq.zzri();
            String advertiser = zzalq.getAdvertiser();
            zzaci zzrj = zzalq.zzrj();
            zzbws zzbws = new zzbws();
            zzbws.zzflz = 1;
            zzbws.zzfma = videoController;
            zzbws.zzcvq = zzrh;
            zzbws.zzfmb = (View) zzar(zzalq.zzsu());
            zzbws.zzn("headline", headline);
            zzbws.zzeim = images;
            zzbws.zzn("body", body);
            zzbws.extras = extras2;
            zzbws.zzn("call_to_action", callToAction);
            zzbws.zzfmf = (View) zzar(zzalq.zzsv());
            zzbws.zzfmg = zzri;
            zzbws.zzn(VisionDataDBAdapter.VisionDataColumns.COLUMN_ADVERTISER, advertiser);
            zzbws.zzfmi = zzrj;
            return zzbws;
        } catch (RemoteException e) {
            zzayu.zzd("Failed to get native ad from content ad mapper", e);
            return null;
        }
    }

    public static zzbws zzb(zzall zzall) {
        try {
            return zza(zzall.getVideoController(), zzall.zzrh(), (View) zzar(zzall.zzsu()), zzall.getHeadline(), zzall.getImages(), zzall.getBody(), zzall.getExtras(), zzall.getCallToAction(), (View) zzar(zzall.zzsv()), zzall.zzri(), zzall.getStore(), zzall.getPrice(), zzall.getStarRating(), zzall.zzrg(), (String) null, 0.0f);
        } catch (RemoteException e) {
            zzayu.zzd("Failed to get native ad assets from app install ad mapper", e);
            return null;
        }
    }

    public static zzbws zza(zzall zzall) {
        try {
            zzxb videoController = zzall.getVideoController();
            zzaca zzrh = zzall.zzrh();
            String headline = zzall.getHeadline();
            List<?> images = zzall.getImages();
            String body = zzall.getBody();
            Bundle extras2 = zzall.getExtras();
            String callToAction = zzall.getCallToAction();
            IObjectWrapper zzri = zzall.zzri();
            String store = zzall.getStore();
            String price = zzall.getPrice();
            double starRating = zzall.getStarRating();
            zzaci zzrg = zzall.zzrg();
            zzbws zzbws = new zzbws();
            zzbws.zzflz = 2;
            zzbws.zzfma = videoController;
            zzbws.zzcvq = zzrh;
            zzbws.zzfmb = (View) zzar(zzall.zzsu());
            zzbws.zzn("headline", headline);
            zzbws.zzeim = images;
            zzbws.zzn("body", body);
            zzbws.extras = extras2;
            zzbws.zzn("call_to_action", callToAction);
            zzbws.zzfmf = (View) zzar(zzall.zzsv());
            zzbws.zzfmg = zzri;
            zzbws.zzn(TransactionErrorDetailsUtilities.STORE, store);
            zzbws.zzn("price", price);
            zzbws.zzeip = starRating;
            zzbws.zzfmh = zzrg;
            return zzbws;
        } catch (RemoteException e) {
            zzayu.zzd("Failed to get native ad from app install ad mapper", e);
            return null;
        }
    }

    public static zzbws zzb(zzalq zzalq) {
        try {
            return zza(zzalq.getVideoController(), zzalq.zzrh(), (View) zzar(zzalq.zzsu()), zzalq.getHeadline(), zzalq.getImages(), zzalq.getBody(), zzalq.getExtras(), zzalq.getCallToAction(), (View) zzar(zzalq.zzsv()), zzalq.zzri(), (String) null, (String) null, -1.0d, zzalq.zzrj(), zzalq.getAdvertiser(), 0.0f);
        } catch (RemoteException e) {
            zzayu.zzd("Failed to get native ad assets from content ad mapper", e);
            return null;
        }
    }

    private static zzbws zza(zzxb zzxb, zzaca zzaca, View view, String str, List list, String str2, Bundle bundle, String str3, View view2, IObjectWrapper iObjectWrapper, String str4, String str5, double d, zzaci zzaci, String str6, float f) {
        zzbws zzbws = new zzbws();
        zzbws.zzflz = 6;
        zzbws.zzfma = zzxb;
        zzbws.zzcvq = zzaca;
        zzbws.zzfmb = view;
        String str7 = str;
        zzbws.zzn("headline", str);
        zzbws.zzeim = list;
        String str8 = str2;
        zzbws.zzn("body", str2);
        zzbws.extras = bundle;
        String str9 = str3;
        zzbws.zzn("call_to_action", str3);
        zzbws.zzfmf = view2;
        zzbws.zzfmg = iObjectWrapper;
        String str10 = str4;
        zzbws.zzn(TransactionErrorDetailsUtilities.STORE, str4);
        String str11 = str5;
        zzbws.zzn("price", str5);
        zzbws.zzeip = d;
        zzbws.zzfmh = zzaci;
        zzbws.zzn(VisionDataDBAdapter.VisionDataColumns.COLUMN_ADVERTISER, str6);
        zzbws.setMediaContentAspectRatio(f);
        return zzbws;
    }
}
