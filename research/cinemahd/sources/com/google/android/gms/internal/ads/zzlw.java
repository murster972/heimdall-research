package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.IOException;

final class zzlw implements zznx {
    private final Uri uri;
    private final zznl zzamu;
    private final zzlv zzazz;
    private final zzoe zzbaa;
    private final /* synthetic */ zzlp zzbat;
    private final zzjj zzbay = new zzjj();
    private volatile boolean zzbaz;
    private boolean zzbba = true;
    private long zzbbb;
    /* access modifiers changed from: private */
    public long zzce = -1;

    public zzlw(zzlp zzlp, Uri uri2, zznl zznl, zzlv zzlv, zzoe zzoe) {
        this.zzbat = zzlp;
        this.uri = (Uri) zzoc.checkNotNull(uri2);
        this.zzamu = (zznl) zzoc.checkNotNull(zznl);
        this.zzazz = (zzlv) zzoc.checkNotNull(zzlv);
        this.zzbaa = zzoe;
    }

    public final void cancelLoad() {
        this.zzbaz = true;
    }

    public final void zze(long j, long j2) {
        this.zzbay.zzamw = j;
        this.zzbbb = j2;
        this.zzbba = true;
    }

    public final boolean zzhp() {
        return this.zzbaz;
    }

    public final void zzhq() throws IOException, InterruptedException {
        zzje zzje;
        int i = 0;
        while (i == 0 && !this.zzbaz) {
            try {
                long j = this.zzbay.zzamw;
                this.zzce = this.zzamu.zza(new zznq(this.uri, j, -1, this.zzbat.zzazw));
                if (this.zzce != -1) {
                    this.zzce += j;
                }
                zzje = new zzje(this.zzamu, j, this.zzce);
                try {
                    zzjd zza = this.zzazz.zza(zzje, this.zzamu.getUri());
                    if (this.zzbba) {
                        zza.zzc(j, this.zzbbb);
                        this.zzbba = false;
                    }
                    while (i == 0 && !this.zzbaz) {
                        this.zzbaa.block();
                        i = zza.zza(zzje, this.zzbay);
                        if (zzje.getPosition() > this.zzbat.zzazx + j) {
                            j = zzje.getPosition();
                            this.zzbaa.zzim();
                            this.zzbat.zzaee.post(this.zzbat.zzbac);
                        }
                    }
                    if (i == 1) {
                        i = 0;
                    } else {
                        this.zzbay.zzamw = zzje.getPosition();
                    }
                    zzoq.zza(this.zzamu);
                } catch (Throwable th) {
                    th = th;
                    if (!(i == 1 || zzje == null)) {
                        this.zzbay.zzamw = zzje.getPosition();
                    }
                    zzoq.zza(this.zzamu);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                zzje = null;
                this.zzbay.zzamw = zzje.getPosition();
                zzoq.zza(this.zzamu);
                throw th;
            }
        }
    }
}
