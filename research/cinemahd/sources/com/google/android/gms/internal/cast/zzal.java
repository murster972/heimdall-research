package com.google.android.gms.internal.cast;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.ReconnectionService;
import com.google.android.gms.cast.framework.media.MediaNotificationService;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.util.PlatformVersion;

public final class zzal implements RemoteMediaClient.Listener {
    private final Handler handler;
    private CastDevice zzaj;
    private final Context zzhe;
    /* access modifiers changed from: private */
    public RemoteMediaClient zzid;
    private final zzw zzjd;
    private final CastOptions zzrb;
    private final ComponentName zzrc;
    private final zzaa zzrd;
    private final zzaa zzre;
    private final Runnable zzrf;
    private MediaSessionCompat zzrg;
    private MediaSessionCompat.Callback zzrh;
    private boolean zzri;

    public zzal(Context context, CastOptions castOptions, zzw zzw) {
        this.zzhe = context;
        this.zzrb = castOptions;
        this.zzjd = zzw;
        if (this.zzrb.s() == null || TextUtils.isEmpty(this.zzrb.s().s())) {
            this.zzrc = null;
        } else {
            this.zzrc = new ComponentName(this.zzhe, this.zzrb.s().s());
        }
        this.zzrd = new zzaa(this.zzhe);
        this.zzrd.zza((zzab) new zzan(this));
        this.zzre = new zzaa(this.zzhe);
        this.zzre.zza((zzab) new zzao(this));
        this.handler = new zzez(Looper.getMainLooper());
        this.zzrf = new zzam(this);
    }

    private final MediaMetadataCompat.Builder zzcz() {
        MediaMetadataCompat metadata = this.zzrg.getController().getMetadata();
        if (metadata == null) {
            return new MediaMetadataCompat.Builder();
        }
        return new MediaMetadataCompat.Builder(metadata);
    }

    private final void zzda() {
        if (this.zzrb.s().v() != null) {
            Intent intent = new Intent(this.zzhe, MediaNotificationService.class);
            intent.setPackage(this.zzhe.getPackageName());
            intent.setAction("com.google.android.gms.cast.framework.action.UPDATE_NOTIFICATION");
            this.zzhe.stopService(intent);
        }
    }

    private final void zzdb() {
        if (this.zzrb.t()) {
            this.handler.removeCallbacks(this.zzrf);
            Intent intent = new Intent(this.zzhe, ReconnectionService.class);
            intent.setPackage(this.zzhe.getPackageName());
            this.zzhe.stopService(intent);
        }
    }

    private final void zzh(boolean z) {
        if (this.zzrb.t()) {
            this.handler.removeCallbacks(this.zzrf);
            Intent intent = new Intent(this.zzhe, ReconnectionService.class);
            intent.setPackage(this.zzhe.getPackageName());
            try {
                this.zzhe.startService(intent);
            } catch (IllegalStateException unused) {
                if (z) {
                    this.handler.postDelayed(this.zzrf, 1000);
                }
            }
        }
    }

    public final void onAdBreakStatusUpdated() {
        zzg(false);
    }

    public final void onMetadataUpdated() {
        zzg(false);
    }

    public final void onPreloadStatusUpdated() {
        zzg(false);
    }

    public final void onQueueStatusUpdated() {
        zzg(false);
    }

    public final void onSendingRemoteMediaRequest() {
    }

    public final void onStatusUpdated() {
        zzg(false);
    }

    public final void zza(RemoteMediaClient remoteMediaClient, CastDevice castDevice) {
        CastOptions castOptions;
        if (!this.zzri && (castOptions = this.zzrb) != null && castOptions.s() != null && remoteMediaClient != null && castDevice != null) {
            this.zzid = remoteMediaClient;
            this.zzid.a((RemoteMediaClient.Listener) this);
            this.zzaj = castDevice;
            if (!PlatformVersion.h()) {
                ((AudioManager) this.zzhe.getSystemService("audio")).requestAudioFocus((AudioManager.OnAudioFocusChangeListener) null, 3, 3);
            }
            ComponentName componentName = new ComponentName(this.zzhe, this.zzrb.s().u());
            Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
            intent.setComponent(componentName);
            this.zzrg = new MediaSessionCompat(this.zzhe, "CastMediaSession", componentName, PendingIntent.getBroadcast(this.zzhe, 0, intent, 0));
            this.zzrg.setFlags(3);
            zza(0, (MediaInfo) null);
            CastDevice castDevice2 = this.zzaj;
            if (castDevice2 != null && !TextUtils.isEmpty(castDevice2.u())) {
                this.zzrg.setMetadata(new MediaMetadataCompat.Builder().putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ARTIST, this.zzhe.getResources().getString(R$string.cast_casting_to_device, new Object[]{this.zzaj.u()})).build());
            }
            this.zzrh = new zzap(this);
            this.zzrg.setCallback(this.zzrh);
            this.zzrg.setActive(true);
            this.zzjd.setMediaSessionCompat(this.zzrg);
            this.zzri = true;
            zzg(false);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzdc() {
        zzh(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00fd, code lost:
        if (r1.intValue() < (r12.C() - 1)) goto L_0x0103;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzg(boolean r12) {
        /*
            r11 = this;
            com.google.android.gms.cast.framework.media.RemoteMediaClient r0 = r11.zzid
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            com.google.android.gms.cast.MediaStatus r0 = r0.f()
            r1 = 0
            if (r0 != 0) goto L_0x000e
            r2 = r1
            goto L_0x0012
        L_0x000e:
            com.google.android.gms.cast.MediaInfo r2 = r0.y()
        L_0x0012:
            if (r2 != 0) goto L_0x0016
            r3 = r1
            goto L_0x001a
        L_0x0016:
            com.google.android.gms.cast.MediaMetadata r3 = r2.y()
        L_0x001a:
            r4 = 6
            r5 = 3
            r6 = 2
            r7 = 0
            r8 = 1
            if (r0 == 0) goto L_0x006a
            if (r2 == 0) goto L_0x006a
            if (r3 != 0) goto L_0x0026
            goto L_0x006a
        L_0x0026:
            com.google.android.gms.cast.framework.media.RemoteMediaClient r3 = r11.zzid
            int r3 = r3.h()
            if (r3 == r8) goto L_0x003e
            if (r3 == r6) goto L_0x003b
            if (r3 == r5) goto L_0x0038
            r0 = 4
            if (r3 == r0) goto L_0x0036
            goto L_0x006a
        L_0x0036:
            r3 = 0
            goto L_0x006c
        L_0x0038:
            r3 = 0
        L_0x0039:
            r4 = 2
            goto L_0x006c
        L_0x003b:
            r3 = 0
            r4 = 3
            goto L_0x006c
        L_0x003e:
            int r3 = r0.w()
            com.google.android.gms.cast.framework.media.RemoteMediaClient r9 = r11.zzid
            boolean r9 = r9.m()
            if (r9 == 0) goto L_0x004e
            if (r3 != r6) goto L_0x004e
            r9 = 1
            goto L_0x004f
        L_0x004e:
            r9 = 0
        L_0x004f:
            int r10 = r0.x()
            if (r10 == 0) goto L_0x005b
            if (r3 == r8) goto L_0x0059
            if (r3 != r5) goto L_0x005b
        L_0x0059:
            r3 = 1
            goto L_0x005c
        L_0x005b:
            r3 = 0
        L_0x005c:
            if (r9 == 0) goto L_0x005f
            goto L_0x0039
        L_0x005f:
            com.google.android.gms.cast.MediaQueueItem r0 = r0.d(r10)
            if (r0 == 0) goto L_0x006b
            com.google.android.gms.cast.MediaInfo r2 = r0.v()
            goto L_0x006c
        L_0x006a:
            r3 = 0
        L_0x006b:
            r4 = 0
        L_0x006c:
            r11.zza((int) r4, (com.google.android.gms.cast.MediaInfo) r2)
            if (r4 != 0) goto L_0x0078
            r11.zzda()
            r11.zzdb()
            return
        L_0x0078:
            com.google.android.gms.cast.framework.CastOptions r0 = r11.zzrb
            com.google.android.gms.cast.framework.media.CastMediaOptions r0 = r0.s()
            com.google.android.gms.cast.framework.media.NotificationOptions r0 = r0.v()
            if (r0 == 0) goto L_0x012b
            com.google.android.gms.cast.framework.media.RemoteMediaClient r0 = r11.zzid
            if (r0 == 0) goto L_0x012b
            android.content.Intent r0 = new android.content.Intent
            android.content.Context r2 = r11.zzhe
            java.lang.Class<com.google.android.gms.cast.framework.media.MediaNotificationService> r4 = com.google.android.gms.cast.framework.media.MediaNotificationService.class
            r0.<init>(r2, r4)
            java.lang.String r2 = "extra_media_notification_force_update"
            r0.putExtra(r2, r12)
            android.content.Context r12 = r11.zzhe
            java.lang.String r12 = r12.getPackageName()
            r0.setPackage(r12)
            java.lang.String r12 = "com.google.android.gms.cast.framework.action.UPDATE_NOTIFICATION"
            r0.setAction(r12)
            com.google.android.gms.cast.framework.media.RemoteMediaClient r12 = r11.zzid
            com.google.android.gms.cast.MediaInfo r12 = r12.e()
            java.lang.String r2 = "extra_media_info"
            r0.putExtra(r2, r12)
            com.google.android.gms.cast.framework.media.RemoteMediaClient r12 = r11.zzid
            int r12 = r12.h()
            java.lang.String r2 = "extra_remote_media_client_player_state"
            r0.putExtra(r2, r12)
            com.google.android.gms.cast.CastDevice r12 = r11.zzaj
            java.lang.String r2 = "extra_cast_device"
            r0.putExtra(r2, r12)
            android.support.v4.media.session.MediaSessionCompat r12 = r11.zzrg
            if (r12 != 0) goto L_0x00c6
            goto L_0x00ca
        L_0x00c6:
            android.support.v4.media.session.MediaSessionCompat$Token r1 = r12.getSessionToken()
        L_0x00ca:
            java.lang.String r12 = "extra_media_session_token"
            r0.putExtra(r12, r1)
            com.google.android.gms.cast.framework.media.RemoteMediaClient r12 = r11.zzid
            com.google.android.gms.cast.MediaStatus r12 = r12.f()
            if (r12 == 0) goto L_0x010e
            int r1 = r12.D()
            if (r1 == r8) goto L_0x0102
            if (r1 == r6) goto L_0x0102
            if (r1 == r5) goto L_0x0102
            int r1 = r12.v()
            java.lang.Integer r1 = r12.b(r1)
            if (r1 == 0) goto L_0x0100
            int r2 = r1.intValue()
            if (r2 <= 0) goto L_0x00f3
            r2 = 1
            goto L_0x00f4
        L_0x00f3:
            r2 = 0
        L_0x00f4:
            int r1 = r1.intValue()
            int r12 = r12.C()
            int r12 = r12 - r8
            if (r1 >= r12) goto L_0x0104
            goto L_0x0103
        L_0x0100:
            r2 = 0
            goto L_0x0104
        L_0x0102:
            r2 = 1
        L_0x0103:
            r7 = 1
        L_0x0104:
            java.lang.String r12 = "extra_can_skip_next"
            r0.putExtra(r12, r7)
            java.lang.String r12 = "extra_can_skip_prev"
            r0.putExtra(r12, r2)
        L_0x010e:
            int r12 = android.os.Build.VERSION.SDK_INT
            r1 = 26
            if (r12 < r1) goto L_0x0126
            android.content.Context r12 = r11.zzhe
            com.google.android.gms.cast.framework.CastContext r12 = com.google.android.gms.cast.framework.CastContext.a((android.content.Context) r12)
            boolean r12 = r12.d()
            if (r12 != 0) goto L_0x0126
            android.content.Context r12 = r11.zzhe
            r12.startForegroundService(r0)
            goto L_0x012b
        L_0x0126:
            android.content.Context r12 = r11.zzhe
            r12.startService(r0)
        L_0x012b:
            if (r3 != 0) goto L_0x0130
            r11.zzh(r8)
        L_0x0130:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzal.zzg(boolean):void");
    }

    public final void zzn(int i) {
        if (this.zzri) {
            this.zzri = false;
            RemoteMediaClient remoteMediaClient = this.zzid;
            if (remoteMediaClient != null) {
                remoteMediaClient.b((RemoteMediaClient.Listener) this);
            }
            if (!PlatformVersion.h()) {
                ((AudioManager) this.zzhe.getSystemService("audio")).abandonAudioFocus((AudioManager.OnAudioFocusChangeListener) null);
            }
            this.zzjd.setMediaSessionCompat((MediaSessionCompat) null);
            zzaa zzaa = this.zzrd;
            if (zzaa != null) {
                zzaa.clear();
            }
            zzaa zzaa2 = this.zzre;
            if (zzaa2 != null) {
                zzaa2.clear();
            }
            MediaSessionCompat mediaSessionCompat = this.zzrg;
            if (mediaSessionCompat != null) {
                mediaSessionCompat.setSessionActivity((PendingIntent) null);
                this.zzrg.setCallback((MediaSessionCompat.Callback) null);
                this.zzrg.setMetadata(new MediaMetadataCompat.Builder().build());
                zza(0, (MediaInfo) null);
                this.zzrg.setActive(false);
                this.zzrg.release();
                this.zzrg = null;
            }
            this.zzid = null;
            this.zzaj = null;
            this.zzrh = null;
            zzda();
            if (i == 0) {
                zzdb();
            }
        }
    }

    private final void zza(int i, MediaInfo mediaInfo) {
        PendingIntent pendingIntent;
        if (i == 0) {
            this.zzrg.setPlaybackState(new PlaybackStateCompat.Builder().setState(0, 0, 1.0f).build());
            this.zzrg.setMetadata(new MediaMetadataCompat.Builder().build());
            return;
        }
        this.zzrg.setPlaybackState(new PlaybackStateCompat.Builder().setState(i, 0, 1.0f).setActions(mediaInfo.A() == 2 ? 5 : 512).build());
        MediaSessionCompat mediaSessionCompat = this.zzrg;
        if (this.zzrc == null) {
            pendingIntent = null;
        } else {
            Intent intent = new Intent();
            intent.setComponent(this.zzrc);
            pendingIntent = PendingIntent.getActivity(this.zzhe, 0, intent, 134217728);
        }
        mediaSessionCompat.setSessionActivity(pendingIntent);
        MediaMetadata y = mediaInfo.y();
        this.zzrg.setMetadata(zzcz().putString(MediaMetadataCompat.METADATA_KEY_TITLE, y.f("com.google.android.gms.cast.metadata.TITLE")).putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, y.f("com.google.android.gms.cast.metadata.TITLE")).putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, y.f("com.google.android.gms.cast.metadata.SUBTITLE")).putLong(MediaMetadataCompat.METADATA_KEY_DURATION, mediaInfo.z()).build());
        Uri zza = zza(y, 0);
        if (zza != null) {
            this.zzrd.zza(zza);
        } else {
            zza((Bitmap) null, 0);
        }
        Uri zza2 = zza(y, 3);
        if (zza2 != null) {
            this.zzre.zza(zza2);
        } else {
            zza((Bitmap) null, 3);
        }
    }

    /* access modifiers changed from: private */
    public final void zza(Bitmap bitmap, int i) {
        if (i == 0) {
            if (bitmap != null) {
                this.zzrg.setMetadata(zzcz().putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, bitmap).build());
                return;
            }
            Bitmap createBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
            createBitmap.eraseColor(0);
            this.zzrg.setMetadata(zzcz().putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, createBitmap).build());
        } else if (i == 3) {
            this.zzrg.setMetadata(zzcz().putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap).build());
        }
    }

    private final Uri zza(MediaMetadata mediaMetadata, int i) {
        WebImage webImage;
        if (this.zzrb.s().t() != null) {
            webImage = this.zzrb.s().t().a(mediaMetadata, i);
        } else {
            webImage = mediaMetadata.v() ? mediaMetadata.t().get(0) : null;
        }
        if (webImage == null) {
            return null;
        }
        return webImage.t();
    }
}
