package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

public final class zzdsm extends zzdqe<String> implements zzdsl, RandomAccess {
    private static final zzdsm zzhof;
    private static final zzdsl zzhog = zzhof;
    private final List<Object> zzhoh;

    static {
        zzdsm zzdsm = new zzdsm();
        zzhof = zzdsm;
        zzdsm.zzaxq();
    }

    public zzdsm() {
        this(10);
    }

    private static String zzap(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzdqk) {
            return ((zzdqk) obj).zzaxt();
        }
        return zzdrv.zzz((byte[]) obj);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        zzaxr();
        this.zzhoh.add(i, (String) obj);
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    public final void clear() {
        zzaxr();
        this.zzhoh.clear();
        this.modCount++;
    }

    public final /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final /* synthetic */ Object get(int i) {
        Object obj = this.zzhoh.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzdqk) {
            zzdqk zzdqk = (zzdqk) obj;
            String zzaxt = zzdqk.zzaxt();
            if (zzdqk.zzaxu()) {
                this.zzhoh.set(i, zzaxt);
            }
            return zzaxt;
        }
        byte[] bArr = (byte[]) obj;
        String zzz = zzdrv.zzz(bArr);
        if (zzdrv.zzy(bArr)) {
            this.zzhoh.set(i, zzz);
        }
        return zzz;
    }

    public final /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    public final /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    public final /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    public final /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        zzaxr();
        return zzap(this.zzhoh.set(i, (String) obj));
    }

    public final int size() {
        return this.zzhoh.size();
    }

    public final /* bridge */ /* synthetic */ boolean zzaxp() {
        return super.zzaxp();
    }

    public final List<?> zzbav() {
        return Collections.unmodifiableList(this.zzhoh);
    }

    public final zzdsl zzbaw() {
        return zzaxp() ? new zzdut(this) : this;
    }

    public final void zzbg(zzdqk zzdqk) {
        zzaxr();
        this.zzhoh.add(zzdqk);
        this.modCount++;
    }

    public final /* synthetic */ zzdsb zzfd(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.zzhoh);
            return new zzdsm((ArrayList<Object>) arrayList);
        }
        throw new IllegalArgumentException();
    }

    public final Object zzgm(int i) {
        return this.zzhoh.get(i);
    }

    public zzdsm(int i) {
        this((ArrayList<Object>) new ArrayList(i));
    }

    public final boolean addAll(int i, Collection<? extends String> collection) {
        zzaxr();
        if (collection instanceof zzdsl) {
            collection = ((zzdsl) collection).zzbav();
        }
        boolean addAll = this.zzhoh.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    public final /* synthetic */ Object remove(int i) {
        zzaxr();
        Object remove = this.zzhoh.remove(i);
        this.modCount++;
        return zzap(remove);
    }

    private zzdsm(ArrayList<Object> arrayList) {
        this.zzhoh = arrayList;
    }

    public final /* bridge */ /* synthetic */ boolean add(Object obj) {
        return super.add(obj);
    }
}
