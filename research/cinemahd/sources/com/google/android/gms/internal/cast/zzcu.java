package com.google.android.gms.internal.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzcu implements Parcelable.Creator<zzct> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a2) != 2) {
                SafeParcelReader.A(parcel, a2);
            } else {
                str = SafeParcelReader.o(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzct(str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzct[i];
    }
}
