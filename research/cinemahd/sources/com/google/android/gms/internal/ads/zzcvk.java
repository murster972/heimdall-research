package com.google.android.gms.internal.ads;

public final class zzcvk implements zzdxg<zzcvi> {
    private final zzdxp<zzauw> zzeqj;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<String> zzfrr;

    public zzcvk(zzdxp<zzauw> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<String> zzdxp3) {
        this.zzeqj = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzfrr = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcvi(this.zzeqj.get(), this.zzfcv.get(), this.zzfrr.get());
    }
}
