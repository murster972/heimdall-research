package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcto implements zzcub<zzctp> {
    private final zzatv zzbng;
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcto(zzatv zzatv, zzdhd zzdhd, Context context) {
        this.zzbng = zzatv;
        this.zzfov = zzdhd;
        this.zzup = context;
    }

    public final zzdhe<zzctp> zzanc() {
        return this.zzfov.zzd(new zzctr(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzctp zzano() throws Exception {
        String str;
        String str2;
        String str3;
        if (!this.zzbng.zzab(this.zzup)) {
            return new zzctp((String) null, (String) null, (String) null, (String) null, (Long) null);
        }
        String zzae = this.zzbng.zzae(this.zzup);
        String str4 = zzae == null ? "" : zzae;
        String zzaf = this.zzbng.zzaf(this.zzup);
        if (zzaf == null) {
            str = "";
        } else {
            str = zzaf;
        }
        String zzag = this.zzbng.zzag(this.zzup);
        if (zzag == null) {
            str2 = "";
        } else {
            str2 = zzag;
        }
        String zzah = this.zzbng.zzah(this.zzup);
        if (zzah == null) {
            str3 = "";
        } else {
            str3 = zzah;
        }
        return new zzctp(str4, str, str2, str3, "TIME_OUT".equals(str) ? (Long) zzve.zzoy().zzd(zzzn.zzcie) : null);
    }
}
