package com.google.android.gms.internal.ads;

import android.os.Bundle;

final /* synthetic */ class zzatw implements zzaul {
    private final String zzcyz;
    private final Bundle zzdpq;

    zzatw(String str, Bundle bundle) {
        this.zzcyz = str;
        this.zzdpq = bundle;
    }

    public final void zza(zzbfq zzbfq) {
        zzbfq.logEvent("am", this.zzcyz, this.zzdpq);
    }
}
