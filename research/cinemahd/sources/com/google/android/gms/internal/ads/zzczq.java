package com.google.android.gms.internal.ads;

import java.util.Map;

public final class zzczq {
    public String name;
    public Map<String, String> zzgmd;

    zzczq(String str, Map<String, String> map) {
        this.name = str;
        this.zzgmd = map;
    }
}
