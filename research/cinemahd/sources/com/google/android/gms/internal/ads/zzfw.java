package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public abstract class zzfw implements Callable {
    private final String TAG = getClass().getSimpleName();
    private final String className;
    private final String zzaac;
    protected Method zzaae;
    private final int zzaai;
    private final int zzaaj;
    protected final zzei zzuv;
    protected final zzbs.zza.zzb zzzt;

    public zzfw(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        this.zzuv = zzei;
        this.className = str;
        this.zzaac = str2;
        this.zzzt = zzb;
        this.zzaai = i;
        this.zzaaj = i2;
    }

    /* access modifiers changed from: protected */
    public abstract void zzcn() throws IllegalAccessException, InvocationTargetException;

    /* renamed from: zzcp */
    public Void call() throws Exception {
        try {
            long nanoTime = System.nanoTime();
            this.zzaae = this.zzuv.zza(this.className, this.zzaac);
            if (this.zzaae == null) {
                return null;
            }
            zzcn();
            zzde zzcc = this.zzuv.zzcc();
            if (!(zzcc == null || this.zzaai == Integer.MIN_VALUE)) {
                zzcc.zza(this.zzaaj, this.zzaai, (System.nanoTime() - nanoTime) / 1000);
            }
            return null;
        } catch (IllegalAccessException | InvocationTargetException unused) {
        }
    }
}
