package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.AdRequest;

final class zzamp implements Runnable {
    private final /* synthetic */ zzamf zzdef;
    private final /* synthetic */ AdRequest.ErrorCode zzdeg;

    zzamp(zzamf zzamf, AdRequest.ErrorCode errorCode) {
        this.zzdef = zzamf;
        this.zzdeg = errorCode;
    }

    public final void run() {
        try {
            this.zzdef.zzdds.onAdFailedToLoad(zzamr.zza(this.zzdeg));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
