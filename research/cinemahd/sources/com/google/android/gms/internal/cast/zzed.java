package com.google.android.gms.internal.cast;

import android.os.Handler;
import android.os.Looper;
import com.facebook.ads.AdError;
import java.util.Locale;

public final class zzed {
    private static final Object zzaat = new Object();
    private static final zzdw zzbf = new zzdw("RequestTracker");
    private final Handler handler = new zzez(Looper.getMainLooper());
    private long zzaar;
    private zzec zzaas;
    private Runnable zzpz;
    private long zzwu = -1;

    public zzed(long j) {
        this.zzaar = j;
    }

    public final boolean test(long j) {
        boolean z;
        synchronized (zzaat) {
            z = this.zzwu != -1 && this.zzwu == j;
        }
        return z;
    }

    public final void zza(long j, zzec zzec) {
        zzec zzec2;
        long j2;
        synchronized (zzaat) {
            zzec2 = this.zzaas;
            j2 = this.zzwu;
            this.zzwu = j;
            this.zzaas = zzec;
        }
        if (zzec2 != null) {
            zzec2.zzd(j2);
        }
        synchronized (zzaat) {
            if (this.zzpz != null) {
                this.handler.removeCallbacks(this.zzpz);
            }
            this.zzpz = new zzee(this);
            this.handler.postDelayed(this.zzpz, this.zzaar);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzc(long r8, int r10, java.lang.Object r11) {
        /*
            r7 = this;
            java.lang.Object r0 = zzaat
            monitor-enter(r0)
            long r1 = r7.zzwu     // Catch:{ all -> 0x002a }
            r3 = -1
            r5 = 0
            int r6 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r6 == 0) goto L_0x0028
            long r1 = r7.zzwu     // Catch:{ all -> 0x002a }
            int r3 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r3 != 0) goto L_0x0028
            java.util.Locale r1 = java.util.Locale.ROOT     // Catch:{ all -> 0x002a }
            java.lang.String r2 = "request %d completed"
            r3 = 1
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x002a }
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x002a }
            r4[r5] = r8     // Catch:{ all -> 0x002a }
            java.lang.String r8 = java.lang.String.format(r1, r2, r4)     // Catch:{ all -> 0x002a }
            r7.zza(r10, r11, r8)     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return r3
        L_0x0028:
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return r5
        L_0x002a:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzed.zzc(long, int, java.lang.Object):boolean");
    }

    public final boolean zzfd() {
        boolean z;
        synchronized (zzaat) {
            z = this.zzwu != -1;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzfe() {
        synchronized (zzaat) {
            if (this.zzwu != -1) {
                zza(15, (Object) null);
            }
        }
    }

    public final boolean zzv(int i) {
        return zza((int) AdError.CACHE_ERROR_CODE, (Object) null);
    }

    private final boolean zza(int i, Object obj) {
        synchronized (zzaat) {
            if (this.zzwu == -1) {
                return false;
            }
            zza(i, (Object) null, String.format(Locale.ROOT, "clearing request %d", new Object[]{Long.valueOf(this.zzwu)}));
            return true;
        }
    }

    private final void zza(int i, Object obj, String str) {
        zzbf.d(str, new Object[0]);
        synchronized (zzaat) {
            if (this.zzaas != null) {
                this.zzaas.zza(this.zzwu, i, obj);
            }
            this.zzwu = -1;
            this.zzaas = null;
            synchronized (zzaat) {
                if (this.zzpz != null) {
                    this.handler.removeCallbacks(this.zzpz);
                    this.zzpz = null;
                }
            }
        }
    }
}
