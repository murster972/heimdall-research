package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzclc implements zzdxg<zzckz> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzbvm> zzfyl;

    public zzclc(zzdxp<Context> zzdxp, zzdxp<zzbvm> zzdxp2, zzdxp<Executor> zzdxp3) {
        this.zzejv = zzdxp;
        this.zzfyl = zzdxp2;
        this.zzfei = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzckz(this.zzejv.get(), this.zzfyl.get(), this.zzfei.get());
    }
}
