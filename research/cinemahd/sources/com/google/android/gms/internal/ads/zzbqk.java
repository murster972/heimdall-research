package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

final /* synthetic */ class zzbqk implements zzbrn {
    static final zzbrn zzfhp = new zzbqk();

    private zzbqk() {
    }

    public final void zzp(Object obj) {
        ((zzo) obj).onResume();
    }
}
