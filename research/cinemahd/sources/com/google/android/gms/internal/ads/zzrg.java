package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzrg extends IInterface {
    void onAppOpenAdFailedToLoad(int i) throws RemoteException;

    void zza(zzrf zzrf) throws RemoteException;
}
