package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import com.facebook.ads.AudienceNetworkActivity;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzi;
import com.google.android.gms.ads.internal.zzq;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbyu {
    private final Executor executor;
    private final zzazb zzbll;
    private final zzaby zzddz;
    private final zzsm zzeeg;
    private final zzdq zzefv;
    private final ScheduledExecutorService zzffx;
    private final zzbyl zzfpi;
    private final zza zzfpj;
    private final zzbzh zzfpk;
    private final Context zzup;

    public zzbyu(Context context, zzbyl zzbyl, zzdq zzdq, zzazb zzazb, zza zza, zzsm zzsm, Executor executor2, zzczu zzczu, zzbzh zzbzh, ScheduledExecutorService scheduledExecutorService) {
        this.zzup = context;
        this.zzfpi = zzbyl;
        this.zzefv = zzdq;
        this.zzbll = zzazb;
        this.zzfpj = zza;
        this.zzeeg = zzsm;
        this.executor = executor2;
        this.zzddz = zzczu.zzddz;
        this.zzfpk = zzbzh;
        this.zzffx = scheduledExecutorService;
    }

    private final zzdhe<List<zzabu>> zza(JSONArray jSONArray, boolean z, boolean z2) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return zzdgs.zzaj(Collections.emptyList());
        }
        ArrayList arrayList = new ArrayList();
        int length = z2 ? jSONArray.length() : 1;
        for (int i = 0; i < length; i++) {
            arrayList.add(zza(jSONArray.optJSONObject(i), z));
        }
        return zzdgs.zzb(zzdgs.zzg(arrayList), zzbyt.zzdoq, this.executor);
    }

    private static Integer zzf(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt("r"), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException unused) {
            return null;
        }
    }

    public static List<zzxy> zzi(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject(AnalyticsEvent.Ad.mute);
        if (optJSONObject == null) {
            return Collections.emptyList();
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("reasons");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < optJSONArray.length(); i++) {
            zzxy zzk = zzk(optJSONArray.optJSONObject(i));
            if (zzk != null) {
                arrayList.add(zzk);
            }
        }
        return arrayList;
    }

    public static zzxy zzj(JSONObject jSONObject) {
        JSONObject optJSONObject;
        JSONObject optJSONObject2 = jSONObject.optJSONObject(AnalyticsEvent.Ad.mute);
        if (optJSONObject2 == null || (optJSONObject = optJSONObject2.optJSONObject("default_reason")) == null) {
            return null;
        }
        return zzk(optJSONObject);
    }

    private static zzxy zzk(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("reason");
        String optString2 = jSONObject.optString("ping_url");
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2)) {
            return null;
        }
        return new zzxy(optString, optString2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzb(String str, Object obj) throws Exception {
        zzq.zzkr();
        zzbdi zza = zzbdr.zza(this.zzup, zzbey.zzabq(), "native-omid", false, false, this.zzefv, this.zzbll, (zzaae) null, (zzi) null, this.zzfpj, this.zzeeg, (zzro) null, false);
        zzazi zzl = zzazi.zzl(zza);
        zza.zzaaa().zza((zzbeu) new zzbzc(zzl));
        zza.loadData(str, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8");
        return zzl;
    }

    public final zzdhe<zzabu> zzc(JSONObject jSONObject, String str) {
        return zza(jSONObject.optJSONObject(str), this.zzddz.zzcvo);
    }

    public final zzdhe<List<zzabu>> zzd(JSONObject jSONObject, String str) {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        zzaby zzaby = this.zzddz;
        return zza(optJSONArray, zzaby.zzcvo, zzaby.zzbjy);
    }

    public final zzdhe<zzabp> zze(JSONObject jSONObject, String str) {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return zzdgs.zzaj(null);
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("images");
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("image");
        if (optJSONArray == null && optJSONObject2 != null) {
            optJSONArray = new JSONArray();
            optJSONArray.put(optJSONObject2);
        }
        return zza(optJSONObject.optBoolean("require"), zzdgs.zzb(zza(optJSONArray, false, true), new zzbyv(this, optJSONObject), this.executor), (Object) null);
    }

    public final zzdhe<zzbdi> zzl(JSONObject jSONObject) {
        JSONObject zza = zzaxs.zza(jSONObject, "html_containers", "instream");
        if (zza == null) {
            JSONObject optJSONObject = jSONObject.optJSONObject(Advertisement.KEY_VIDEO);
            if (optJSONObject == null) {
                return zzdgs.zzaj(null);
            }
            if (TextUtils.isEmpty(optJSONObject.optString("vast_xml"))) {
                zzayu.zzez("Required field 'vast_xml' is missing");
                return zzdgs.zzaj(null);
            }
            return zza(zzdgs.zza(this.zzfpk.zzm(optJSONObject), (long) ((Integer) zzve.zzoy().zzd(zzzn.zzcma)).intValue(), TimeUnit.SECONDS, this.zzffx), (Object) null);
        }
        zzdhe<zzbdi> zzo = this.zzfpk.zzo(zza.optString("base_url"), zza.optString("html"));
        return zzdgs.zzb(zzo, new zzbyx(zzo), (Executor) zzazd.zzdwj);
    }

    private final zzdhe<zzabu> zza(JSONObject jSONObject, boolean z) {
        if (jSONObject == null) {
            return zzdgs.zzaj(null);
        }
        String optString = jSONObject.optString(ReportDBAdapter.ReportColumns.COLUMN_URL);
        if (TextUtils.isEmpty(optString)) {
            return zzdgs.zzaj(null);
        }
        double optDouble = jSONObject.optDouble("scale", 1.0d);
        boolean optBoolean = jSONObject.optBoolean("is_transparent", true);
        int optInt = jSONObject.optInt("width", -1);
        int optInt2 = jSONObject.optInt("height", -1);
        if (z) {
            return zzdgs.zzaj(new zzabu((Drawable) null, Uri.parse(optString), optDouble, optInt, optInt2));
        }
        return zza(jSONObject.optBoolean("require"), zzdgs.zzb(this.zzfpi.zza(optString, optDouble, optBoolean), new zzbyw(optString, optDouble, optInt, optInt2), this.executor), (Object) null);
    }

    private static <T> zzdhe<T> zza(zzdhe<T> zzdhe, T t) {
        return zzdgs.zzb(zzdhe, Exception.class, new zzbza((Object) null), zzazd.zzdwj);
    }

    private static <T> zzdhe<T> zza(boolean z, zzdhe<T> zzdhe, T t) {
        if (z) {
            return zzdgs.zzb(zzdhe, new zzbyz(zzdhe), (Executor) zzazd.zzdwj);
        }
        return zza(zzdhe, (Object) null);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzabp zza(JSONObject jSONObject, List list) {
        Integer num = null;
        if (list == null || list.isEmpty()) {
            return null;
        }
        String optString = jSONObject.optString("text");
        Integer zzf = zzf(jSONObject, "bg_color");
        Integer zzf2 = zzf(jSONObject, "text_color");
        int optInt = jSONObject.optInt("text_size", -1);
        boolean optBoolean = jSONObject.optBoolean("allow_pub_rendering");
        int optInt2 = jSONObject.optInt("animation_ms", 1000);
        int optInt3 = jSONObject.optInt("presentation_ms", 4000);
        if (optInt > 0) {
            num = Integer.valueOf(optInt);
        }
        return new zzabp(optString, list, zzf, zzf2, num, optInt3 + optInt2, this.zzddz.zzbjz, optBoolean);
    }
}
