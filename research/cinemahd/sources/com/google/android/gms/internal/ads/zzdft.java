package com.google.android.gms.internal.ads;

import java.lang.Throwable;

final class zzdft<V, X extends Throwable> extends zzdfr<V, X, zzded<? super X, ? extends V>, V> {
    zzdft(zzdhe<? extends V> zzdhe, Class<X> cls, zzded<? super X, ? extends V> zzded) {
        super(zzdhe, cls, zzded);
    }

    /* access modifiers changed from: package-private */
    public final void setResult(V v) {
        set(v);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(Object obj, Throwable th) throws Exception {
        return ((zzded) obj).apply(th);
    }
}
