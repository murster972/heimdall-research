package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class zzawf extends BroadcastReceiver {
    private zzawf(zzawb zzawb) {
    }

    public final void onReceive(Context context, Intent intent) {
        zzayo.zzxk();
        zzavq.zzan(context);
    }

    /* synthetic */ zzawf(zzawb zzawb, zzawa zzawa) {
        this(zzawb);
    }
}
