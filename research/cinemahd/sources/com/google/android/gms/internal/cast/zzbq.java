package com.google.android.gms.internal.cast;

import android.widget.TextView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbq extends UIController {
    private final TextView zztn;

    public zzbq(TextView textView) {
        this.zztn = textView;
    }

    public final void onMediaStatusUpdated() {
        MediaInfo e;
        MediaMetadata y;
        String zzb;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && (e = remoteMediaClient.e()) != null && (y = e.y()) != null && (zzb = zzaq.zzb(y)) != null) {
            this.zztn.setText(zzb);
        }
    }
}
