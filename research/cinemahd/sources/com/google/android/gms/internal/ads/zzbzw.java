package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzbzw implements zzdxg<zzbzh> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzcbn> zzfog;

    public zzbzw(zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2, zzdxp<Executor> zzdxp3, zzdxp<zzcbn> zzdxp4) {
        this.zzejv = zzdxp;
        this.zzfep = zzdxp2;
        this.zzfei = zzdxp3;
        this.zzfog = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzbzh(this.zzejv.get(), this.zzfep.get(), this.zzfei.get(), this.zzfog.get());
    }
}
