package com.google.android.gms.internal.ads;

final class zzlu implements Runnable {
    private final /* synthetic */ zzlp zzbat;
    private final /* synthetic */ zzlv zzbav;

    zzlu(zzlp zzlp, zzlv zzlv) {
        this.zzbat = zzlp;
        this.zzbav = zzlv;
    }

    public final void run() {
        this.zzbav.release();
        int size = this.zzbat.zzbad.size();
        for (int i = 0; i < size; i++) {
            ((zzmj) this.zzbat.zzbad.valueAt(i)).disable();
        }
    }
}
