package com.google.android.gms.internal.ads;

public final class zzjl implements zzjm {
    private final long zzagj;

    public zzjl(long j) {
        this.zzagj = j;
    }

    public final long getDurationUs() {
        return this.zzagj;
    }

    public final long zzdz(long j) {
        return 0;
    }

    public final boolean zzgh() {
        return false;
    }
}
