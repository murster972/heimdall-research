package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public final class zzim implements zzhm {
    private int zzafo = -1;
    private float zzagc = 1.0f;
    private float zzagd = 1.0f;
    private ByteBuffer zzajh = zzhm.zzaha;
    private int zzakk = -1;
    private ByteBuffer zzako = zzhm.zzaha;
    private boolean zzakp;
    private zzin zzaky;
    private ShortBuffer zzakz = this.zzako.asShortBuffer();
    private long zzala;
    private long zzalb;

    public final void flush() {
        this.zzaky = new zzin(this.zzakk, this.zzafo);
        this.zzaky.setSpeed(this.zzagc);
        this.zzaky.zzc(this.zzagd);
        this.zzajh = zzhm.zzaha;
        this.zzala = 0;
        this.zzalb = 0;
        this.zzakp = false;
    }

    public final boolean isActive() {
        return Math.abs(this.zzagc - 1.0f) >= 0.01f || Math.abs(this.zzagd - 1.0f) >= 0.01f;
    }

    public final void reset() {
        this.zzaky = null;
        this.zzako = zzhm.zzaha;
        this.zzakz = this.zzako.asShortBuffer();
        this.zzajh = zzhm.zzaha;
        this.zzafo = -1;
        this.zzakk = -1;
        this.zzala = 0;
        this.zzalb = 0;
        this.zzakp = false;
    }

    public final float zza(float f) {
        this.zzagc = zzoq.zza(f, 0.1f, 8.0f);
        return this.zzagc;
    }

    public final float zzb(float f) {
        this.zzagd = zzoq.zza(f, 0.1f, 8.0f);
        return f;
    }

    public final boolean zzeu() {
        if (!this.zzakp) {
            return false;
        }
        zzin zzin = this.zzaky;
        return zzin == null || zzin.zzfx() == 0;
    }

    public final int zzez() {
        return this.zzafo;
    }

    public final int zzfa() {
        return 2;
    }

    public final void zzfb() {
        this.zzaky.zzfb();
        this.zzakp = true;
    }

    public final ByteBuffer zzfc() {
        ByteBuffer byteBuffer = this.zzajh;
        this.zzajh = zzhm.zzaha;
        return byteBuffer;
    }

    public final long zzfv() {
        return this.zzala;
    }

    public final long zzfw() {
        return this.zzalb;
    }

    public final void zzi(ByteBuffer byteBuffer) {
        if (byteBuffer.hasRemaining()) {
            ShortBuffer asShortBuffer = byteBuffer.asShortBuffer();
            int remaining = byteBuffer.remaining();
            this.zzala += (long) remaining;
            this.zzaky.zza(asShortBuffer);
            byteBuffer.position(byteBuffer.position() + remaining);
        }
        int zzfx = (this.zzaky.zzfx() * this.zzafo) << 1;
        if (zzfx > 0) {
            if (this.zzako.capacity() < zzfx) {
                this.zzako = ByteBuffer.allocateDirect(zzfx).order(ByteOrder.nativeOrder());
                this.zzakz = this.zzako.asShortBuffer();
            } else {
                this.zzako.clear();
                this.zzakz.clear();
            }
            this.zzaky.zzb(this.zzakz);
            this.zzalb += (long) zzfx;
            this.zzako.limit(zzfx);
            this.zzajh = this.zzako;
        }
    }

    public final boolean zzb(int i, int i2, int i3) throws zzhp {
        if (i3 != 2) {
            throw new zzhp(i, i2, i3);
        } else if (this.zzakk == i && this.zzafo == i2) {
            return false;
        } else {
            this.zzakk = i;
            this.zzafo = i2;
            return true;
        }
    }
}
