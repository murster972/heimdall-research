package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfo extends zzfw {
    private final StackTraceElement[] zzzz;

    public zzfo(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2, StackTraceElement[] stackTraceElementArr) {
        super(zzei, str, str2, zzb, i, 45);
        this.zzzz = stackTraceElementArr;
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        zzcd zzcd;
        Object obj = this.zzzz;
        if (obj != null) {
            zzeg zzeg = new zzeg((String) this.zzaae.invoke((Object) null, new Object[]{obj}));
            synchronized (this.zzzt) {
                this.zzzt.zzbi(zzeg.zzxh.longValue());
                if (zzeg.zzxi.booleanValue()) {
                    zzbs.zza.zzb zzb = this.zzzt;
                    if (zzeg.zzxj.booleanValue()) {
                        zzcd = zzcd.ENUM_FALSE;
                    } else {
                        zzcd = zzcd.ENUM_TRUE;
                    }
                    zzb.zzg(zzcd);
                } else {
                    this.zzzt.zzg(zzcd.ENUM_FAILURE);
                }
            }
        }
    }
}
