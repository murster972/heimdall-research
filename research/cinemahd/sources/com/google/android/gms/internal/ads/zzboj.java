package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzboj implements zzdxg<Bundle> {
    private final zzbod zzfhi;

    private zzboj(zzbod zzbod) {
        this.zzfhi = zzbod;
    }

    public static zzboj zzh(zzbod zzbod) {
        return new zzboj(zzbod);
    }

    public final /* synthetic */ Object get() {
        return this.zzfhi.zzahf();
    }
}
