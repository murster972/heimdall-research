package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;

final /* synthetic */ class zzbtm implements zzbrn {
    static final zzbrn zzfhp = new zzbtm();

    private zzbtm() {
    }

    public final void zzp(Object obj) {
        ((VideoController.VideoLifecycleCallbacks) obj).onVideoPause();
    }
}
