package com.google.android.gms.internal.ads;

final /* synthetic */ class zzdcv implements zzbrn {
    private final zzdca zzgrk;
    private final Throwable zzgrl;

    zzdcv(zzdca zzdca, Throwable th) {
        this.zzgrk = zzdca;
        this.zzgrl = th;
    }

    public final void zzp(Object obj) {
        zzdca zzdca = this.zzgrk;
        ((zzdcx) obj).zza((zzdco) zzdca.zzaqd(), zzdca.zzaqe(), this.zzgrl);
    }
}
