package com.google.android.gms.internal.ads;

public final class zzaay {
    public static zzaan<Boolean> zzctf = zzaan.zzf("gad:force_dynamite_loading_enabled", false);
    public static zzaan<Boolean> zzctg = zzaan.zzf("gads:uri_query_to_map_rewrite:enabled", false);
    public static zzaan<Boolean> zzcth = zzaan.zzf("gads:sdk_csi_write_to_file", false);
}
