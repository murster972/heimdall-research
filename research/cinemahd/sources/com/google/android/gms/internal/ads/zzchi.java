package com.google.android.gms.internal.ads;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

final class zzchi implements zzdgt<ParcelFileDescriptor> {
    private final /* synthetic */ zzaqe zzfwk;

    zzchi(zzcgw zzcgw, zzaqe zzaqe) {
        this.zzfwk = zzaqe;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        try {
            this.zzfwk.zzb((ParcelFileDescriptor) obj);
        } catch (RemoteException e) {
            zzavs.zza("Service can't call client", e);
        }
    }

    public final void zzb(Throwable th) {
        try {
            this.zzfwk.zza(zzaxc.zza(th, zzcfb.zzd(th)));
        } catch (RemoteException e) {
            zzavs.zza("Service can't call client", e);
        }
    }
}
