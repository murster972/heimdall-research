package com.google.android.gms.internal.ads;

import android.util.Log;

final class zzgs {
    public final int index;
    private final zzhf[] zzacq;
    private final zznf zzacr;
    public final zzlz zzadn;
    public final Object zzado;
    public final zzmo[] zzadp;
    private final boolean[] zzadq;
    public final long zzadr;
    public int zzads;
    public long zzadt;
    public boolean zzadu;
    public boolean zzadv;
    public boolean zzadw;
    public zzgs zzadx;
    public zznh zzady;
    private final zzhe[] zzadz;
    private final zzha zzaea;
    private final zzmb zzaeb;
    private zznh zzaec;

    public zzgs(zzhf[] zzhfArr, zzhe[] zzheArr, long j, zznf zznf, zzha zzha, zzmb zzmb, Object obj, int i, int i2, boolean z, long j2) {
        this.zzacq = zzhfArr;
        this.zzadz = zzheArr;
        this.zzadr = j;
        this.zzacr = zznf;
        this.zzaea = zzha;
        this.zzaeb = zzmb;
        this.zzado = zzoc.checkNotNull(obj);
        this.index = i;
        this.zzads = i2;
        this.zzadu = z;
        this.zzadt = j2;
        this.zzadp = new zzmo[zzhfArr.length];
        this.zzadq = new boolean[zzhfArr.length];
        this.zzadn = zzmb.zza(i2, zzha.zzet());
    }

    public final void release() {
        try {
            this.zzaeb.zzb(this.zzadn);
        } catch (RuntimeException e) {
            Log.e("ExoPlayerImplInternal", "Period release failed.", e);
        }
    }

    public final long zza(long j, boolean z, boolean[] zArr) {
        zzng zzng = this.zzady.zzbeg;
        int i = 0;
        while (true) {
            boolean z2 = true;
            if (i >= zzng.length) {
                break;
            }
            boolean[] zArr2 = this.zzadq;
            if (z || !this.zzady.zza(this.zzaec, i)) {
                z2 = false;
            }
            zArr2[i] = z2;
            i++;
        }
        long zza = this.zzadn.zza(zzng.zzie(), this.zzadq, this.zzadp, zArr, j);
        this.zzaec = this.zzady;
        this.zzadw = false;
        int i2 = 0;
        while (true) {
            zzmo[] zzmoArr = this.zzadp;
            if (i2 < zzmoArr.length) {
                if (zzmoArr[i2] != null) {
                    zzoc.checkState(zzng.zzay(i2) != null);
                    this.zzadw = true;
                } else {
                    zzoc.checkState(zzng.zzay(i2) == null);
                }
                i2++;
            } else {
                this.zzaea.zza(this.zzacq, this.zzady.zzbef, zzng);
                return zza;
            }
        }
    }

    public final long zzb(long j, boolean z) {
        return zza(j, false, new boolean[this.zzacq.length]);
    }

    public final void zzc(int i, boolean z) {
        this.zzads = i;
        this.zzadu = z;
    }

    public final long zzef() {
        return this.zzadr - this.zzadt;
    }

    public final boolean zzeg() {
        if (this.zzadv) {
            return !this.zzadw || this.zzadn.zzhj() == Long.MIN_VALUE;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzeh() throws com.google.android.gms.internal.ads.zzgl {
        /*
            r6 = this;
            com.google.android.gms.internal.ads.zznf r0 = r6.zzacr
            com.google.android.gms.internal.ads.zzhe[] r1 = r6.zzadz
            com.google.android.gms.internal.ads.zzlz r2 = r6.zzadn
            com.google.android.gms.internal.ads.zzmr r2 = r2.zzhg()
            com.google.android.gms.internal.ads.zznh r0 = r0.zza(r1, r2)
            com.google.android.gms.internal.ads.zznh r1 = r6.zzaec
            r2 = 1
            r3 = 0
            if (r1 != 0) goto L_0x0016
        L_0x0014:
            r1 = 0
            goto L_0x0028
        L_0x0016:
            r4 = 0
        L_0x0017:
            com.google.android.gms.internal.ads.zzng r5 = r0.zzbeg
            int r5 = r5.length
            if (r4 >= r5) goto L_0x0027
            boolean r5 = r0.zza(r1, r4)
            if (r5 != 0) goto L_0x0024
            goto L_0x0014
        L_0x0024:
            int r4 = r4 + 1
            goto L_0x0017
        L_0x0027:
            r1 = 1
        L_0x0028:
            if (r1 == 0) goto L_0x002b
            return r3
        L_0x002b:
            r6.zzady = r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzgs.zzeh():boolean");
    }
}
