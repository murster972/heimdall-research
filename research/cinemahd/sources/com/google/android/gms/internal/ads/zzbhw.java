package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbhw implements Runnable {
    private final zzbht zzfat;
    private final Runnable zzfau;

    zzbhw(zzbht zzbht, Runnable runnable) {
        this.zzfat = zzbht;
        this.zzfau = runnable;
    }

    public final void run() {
        zzazd.zzdwi.execute(new zzbhv(this.zzfat, this.zzfau));
    }
}
