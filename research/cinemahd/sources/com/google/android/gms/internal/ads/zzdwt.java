package com.google.android.gms.internal.ads;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class zzdwt extends zzdwy {
    private Logger logger;

    public zzdwt(String str) {
        this.logger = Logger.getLogger(str);
    }

    public final void zzhp(String str) {
        this.logger.logp(Level.FINE, "com.googlecode.mp4parser.util.JuliLogger", "logDebug", str);
    }
}
