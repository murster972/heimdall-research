package com.google.android.gms.internal.ads;

import android.util.JsonWriter;
import java.io.IOException;

public interface zzaxx {
    void zza(JsonWriter jsonWriter) throws IOException;
}
