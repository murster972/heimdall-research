package com.google.android.gms.internal.ads;

import android.view.View;
import java.util.Collections;
import java.util.Set;

final class zzcnh extends zzbkn {
    zzcnh(zzcng zzcng, View view, zzbdi zzbdi, zzbme zzbme, zzczk zzczk) {
        super(view, (zzbdi) null, zzbme, zzczk);
    }

    public final zzbpw zza(Set<zzbsu<zzbqb>> set) {
        return new zzbpw(Collections.emptySet());
    }
}
