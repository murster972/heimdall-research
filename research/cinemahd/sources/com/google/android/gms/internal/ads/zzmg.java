package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public final class zzmg implements zzmb {
    private final zzhl zzacw = new zzhl();
    private final zzmb[] zzbbn;
    private final ArrayList<zzmb> zzbbo;
    private zzme zzbbp;
    private zzhg zzbbq;
    private Object zzbbr;
    private int zzbbs = -1;
    private zzmi zzbbt;

    public zzmg(zzmb... zzmbArr) {
        this.zzbbn = zzmbArr;
        this.zzbbo = new ArrayList<>(Arrays.asList(zzmbArr));
    }

    public final void zza(zzgk zzgk, boolean z, zzme zzme) {
        this.zzbbp = zzme;
        int i = 0;
        while (true) {
            zzmb[] zzmbArr = this.zzbbn;
            if (i < zzmbArr.length) {
                zzmbArr[i].zza(zzgk, false, new zzmf(this, i));
                i++;
            } else {
                return;
            }
        }
    }

    public final void zzb(zzlz zzlz) {
        zzmd zzmd = (zzmd) zzlz;
        int i = 0;
        while (true) {
            zzmb[] zzmbArr = this.zzbbn;
            if (i < zzmbArr.length) {
                zzmbArr[i].zzb(zzmd.zzbbg[i]);
                i++;
            } else {
                return;
            }
        }
    }

    public final void zzhr() throws IOException {
        zzmi zzmi = this.zzbbt;
        if (zzmi == null) {
            for (zzmb zzhr : this.zzbbn) {
                zzhr.zzhr();
            }
            return;
        }
        throw zzmi;
    }

    public final void zzhs() {
        for (zzmb zzhs : this.zzbbn) {
            zzhs.zzhs();
        }
    }

    public final zzlz zza(int i, zznj zznj) {
        zzlz[] zzlzArr = new zzlz[this.zzbbn.length];
        for (int i2 = 0; i2 < zzlzArr.length; i2++) {
            zzlzArr[i2] = this.zzbbn[i2].zza(i, zznj);
        }
        return new zzmd(zzlzArr);
    }

    /* access modifiers changed from: private */
    public final void zza(int i, zzhg zzhg, Object obj) {
        zzmi zzmi;
        if (this.zzbbt == null) {
            int zzev = zzhg.zzev();
            int i2 = 0;
            while (true) {
                if (i2 >= zzev) {
                    if (this.zzbbs == -1) {
                        this.zzbbs = zzhg.zzew();
                    } else if (zzhg.zzew() != this.zzbbs) {
                        zzmi = new zzmi(1);
                    }
                    zzmi = null;
                } else if (zzhg.zza(i2, this.zzacw, false).zzagv) {
                    zzmi = new zzmi(0);
                    break;
                } else {
                    i2++;
                }
            }
            this.zzbbt = zzmi;
        }
        if (this.zzbbt == null) {
            this.zzbbo.remove(this.zzbbn[i]);
            if (i == 0) {
                this.zzbbq = zzhg;
                this.zzbbr = obj;
            }
            if (this.zzbbo.isEmpty()) {
                this.zzbbp.zzb(this.zzbbq, this.zzbbr);
            }
        }
    }
}
