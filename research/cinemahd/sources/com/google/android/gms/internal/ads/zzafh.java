package com.google.android.gms.internal.ads;

import java.util.Map;

final class zzafh implements zzafn<zzbdi> {
    zzafh() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzabw zzaal = ((zzbdi) obj).zzaal();
        if (zzaal != null) {
            zzaal.zzrd();
        }
    }
}
