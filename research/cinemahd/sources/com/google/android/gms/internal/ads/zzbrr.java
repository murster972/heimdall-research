package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;
import java.util.Collections;
import java.util.Set;

public final class zzbrr implements zzdxg<Set<zzbsu<zzo>>> {
    private final zzbrm zzfim;

    private zzbrr(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbrr zzh(zzbrm zzbrm) {
        return new zzbrr(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.emptySet(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
