package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaim implements zzbex {
    private final zzaii zzczf;

    private zzaim(zzaii zzaii) {
        this.zzczf = zzaii;
    }

    static zzbex zzb(zzaii zzaii) {
        return new zzaim(zzaii);
    }

    public final void zzsb() {
        this.zzczf.zzsa();
    }
}
