package com.google.android.gms.internal.ads;

import java.io.IOException;

final class zzdqg {
    static int zza(byte[] bArr, int i, zzdqf zzdqf) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return zza((int) b, bArr, i2, zzdqf);
        }
        zzdqf.zzhhq = b;
        return i2;
    }

    static int zzb(byte[] bArr, int i, zzdqf zzdqf) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            zzdqf.zzhhr = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b = bArr[i2];
        long j2 = (j & 127) | (((long) (b & Byte.MAX_VALUE)) << 7);
        int i4 = 7;
        while (b < 0) {
            int i5 = i3 + 1;
            byte b2 = bArr[i3];
            i4 += 7;
            j2 |= ((long) (b2 & Byte.MAX_VALUE)) << i4;
            int i6 = i5;
            b = b2;
            i3 = i6;
        }
        zzdqf.zzhhr = j2;
        return i3;
    }

    static int zzc(byte[] bArr, int i, zzdqf zzdqf) throws zzdse {
        int zza = zza(bArr, i, zzdqf);
        int i2 = zzdqf.zzhhq;
        if (i2 < 0) {
            throw zzdse.zzbak();
        } else if (i2 == 0) {
            zzdqf.zzhhs = "";
            return zza;
        } else {
            zzdqf.zzhhs = new String(bArr, zza, i2, zzdrv.UTF_8);
            return zza + i2;
        }
    }

    static int zzd(byte[] bArr, int i, zzdqf zzdqf) throws zzdse {
        int zza = zza(bArr, i, zzdqf);
        int i2 = zzdqf.zzhhq;
        if (i2 < 0) {
            throw zzdse.zzbak();
        } else if (i2 == 0) {
            zzdqf.zzhhs = "";
            return zza;
        } else {
            zzdqf.zzhhs = zzdva.zzo(bArr, zza, i2);
            return zza + i2;
        }
    }

    static int zze(byte[] bArr, int i, zzdqf zzdqf) throws zzdse {
        int zza = zza(bArr, i, zzdqf);
        int i2 = zzdqf.zzhhq;
        if (i2 < 0) {
            throw zzdse.zzbak();
        } else if (i2 > bArr.length - zza) {
            throw zzdse.zzbaj();
        } else if (i2 == 0) {
            zzdqf.zzhhs = zzdqk.zzhhx;
            return zza;
        } else {
            zzdqf.zzhhs = zzdqk.zzi(bArr, zza, i2);
            return zza + i2;
        }
    }

    static int zzf(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    static long zzg(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    static double zzh(byte[] bArr, int i) {
        return Double.longBitsToDouble(zzg(bArr, i));
    }

    static float zzi(byte[] bArr, int i) {
        return Float.intBitsToFloat(zzf(bArr, i));
    }

    static int zza(int i, byte[] bArr, int i2, zzdqf zzdqf) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            zzdqf.zzhhq = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            zzdqf.zzhhq = i5 | (b2 << 14);
            return i6;
        }
        int i7 = i5 | ((b2 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            zzdqf.zzhhq = i7 | (b3 << 21);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            zzdqf.zzhhq = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                zzdqf.zzhhq = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int zza(com.google.android.gms.internal.ads.zzdua r6, byte[] r7, int r8, int r9, com.google.android.gms.internal.ads.zzdqf r10) throws java.io.IOException {
        /*
            int r0 = r8 + 1
            byte r8 = r7[r8]
            if (r8 >= 0) goto L_0x000c
            int r0 = zza((int) r8, (byte[]) r7, (int) r0, (com.google.android.gms.internal.ads.zzdqf) r10)
            int r8 = r10.zzhhq
        L_0x000c:
            r3 = r0
            if (r8 < 0) goto L_0x0025
            int r9 = r9 - r3
            if (r8 > r9) goto L_0x0025
            java.lang.Object r9 = r6.newInstance()
            int r8 = r8 + r3
            r0 = r6
            r1 = r9
            r2 = r7
            r4 = r8
            r5 = r10
            r0.zza(r1, r2, r3, r4, r5)
            r6.zzan(r9)
            r10.zzhhs = r9
            return r8
        L_0x0025:
            com.google.android.gms.internal.ads.zzdse r6 = com.google.android.gms.internal.ads.zzdse.zzbaj()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqg.zza(com.google.android.gms.internal.ads.zzdua, byte[], int, int, com.google.android.gms.internal.ads.zzdqf):int");
    }

    static int zza(zzdua zzdua, byte[] bArr, int i, int i2, int i3, zzdqf zzdqf) throws IOException {
        zzdti zzdti = (zzdti) zzdua;
        Object newInstance = zzdti.newInstance();
        int zza = zzdti.zza(newInstance, bArr, i, i2, i3, zzdqf);
        zzdti.zzan(newInstance);
        zzdqf.zzhhs = newInstance;
        return zza;
    }

    static int zza(int i, byte[] bArr, int i2, int i3, zzdsb<?> zzdsb, zzdqf zzdqf) {
        zzdrw zzdrw = (zzdrw) zzdsb;
        int zza = zza(bArr, i2, zzdqf);
        zzdrw.zzgl(zzdqf.zzhhq);
        while (zza < i3) {
            int zza2 = zza(bArr, zza, zzdqf);
            if (i != zzdqf.zzhhq) {
                break;
            }
            zza = zza(bArr, zza2, zzdqf);
            zzdrw.zzgl(zzdqf.zzhhq);
        }
        return zza;
    }

    static int zza(byte[] bArr, int i, zzdsb<?> zzdsb, zzdqf zzdqf) throws IOException {
        zzdrw zzdrw = (zzdrw) zzdsb;
        int zza = zza(bArr, i, zzdqf);
        int i2 = zzdqf.zzhhq + zza;
        while (zza < i2) {
            zza = zza(bArr, zza, zzdqf);
            zzdrw.zzgl(zzdqf.zzhhq);
        }
        if (zza == i2) {
            return zza;
        }
        throw zzdse.zzbaj();
    }

    static int zza(zzdua<?> zzdua, int i, byte[] bArr, int i2, int i3, zzdsb<?> zzdsb, zzdqf zzdqf) throws IOException {
        int zza = zza((zzdua) zzdua, bArr, i2, i3, zzdqf);
        zzdsb.add(zzdqf.zzhhs);
        while (zza < i3) {
            int zza2 = zza(bArr, zza, zzdqf);
            if (i != zzdqf.zzhhq) {
                break;
            }
            zza = zza((zzdua) zzdua, bArr, zza2, i3, zzdqf);
            zzdsb.add(zzdqf.zzhhs);
        }
        return zza;
    }

    static int zza(int i, byte[] bArr, int i2, int i3, zzdur zzdur, zzdqf zzdqf) throws zzdse {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int zzb = zzb(bArr, i2, zzdqf);
                zzdur.zzd(i, Long.valueOf(zzdqf.zzhhr));
                return zzb;
            } else if (i4 == 1) {
                zzdur.zzd(i, Long.valueOf(zzg(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int zza = zza(bArr, i2, zzdqf);
                int i5 = zzdqf.zzhhq;
                if (i5 < 0) {
                    throw zzdse.zzbak();
                } else if (i5 <= bArr.length - zza) {
                    if (i5 == 0) {
                        zzdur.zzd(i, zzdqk.zzhhx);
                    } else {
                        zzdur.zzd(i, zzdqk.zzi(bArr, zza, i5));
                    }
                    return zza + i5;
                } else {
                    throw zzdse.zzbaj();
                }
            } else if (i4 == 3) {
                zzdur zzbcg = zzdur.zzbcg();
                int i6 = (i & -8) | 4;
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int zza2 = zza(bArr, i2, zzdqf);
                    int i8 = zzdqf.zzhhq;
                    i7 = i8;
                    if (i8 == i6) {
                        i2 = zza2;
                        break;
                    }
                    int zza3 = zza(i7, bArr, zza2, i3, zzbcg, zzdqf);
                    i7 = i8;
                    i2 = zza3;
                }
                if (i2 > i3 || i7 != i6) {
                    throw zzdse.zzbaq();
                }
                zzdur.zzd(i, zzbcg);
                return i2;
            } else if (i4 == 5) {
                zzdur.zzd(i, Integer.valueOf(zzf(bArr, i2)));
                return i2 + 4;
            } else {
                throw zzdse.zzbam();
            }
        } else {
            throw zzdse.zzbam();
        }
    }

    static int zza(int i, byte[] bArr, int i2, int i3, zzdqf zzdqf) throws zzdse {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return zzb(bArr, i2, zzdqf);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return zza(bArr, i2, zzdqf) + zzdqf.zzhhq;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = zza(bArr, i2, zzdqf);
                    i6 = zzdqf.zzhhq;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = zza(i6, bArr, i2, i3, zzdqf);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw zzdse.zzbaq();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw zzdse.zzbam();
            }
        } else {
            throw zzdse.zzbam();
        }
    }
}
