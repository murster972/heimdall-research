package com.google.android.gms.internal.ads;

import java.io.IOException;

public interface zzdte extends zzdtg {
    byte[] toByteArray();

    zzdqk zzaxk();

    int zzazu();

    zzdtd zzazx();

    zzdtd zzazy();

    void zzb(zzdrb zzdrb) throws IOException;
}
