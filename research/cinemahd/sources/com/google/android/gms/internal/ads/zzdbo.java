package com.google.android.gms.internal.ads;

public final class zzdbo implements Cloneable {
    public boolean zzgpi;
    public int zzgpj;

    /* access modifiers changed from: private */
    /* renamed from: zzapq */
    public final zzdbo clone() {
        try {
            return (zzdbo) super.clone();
        } catch (CloneNotSupportedException unused) {
            throw new AssertionError();
        }
    }
}
