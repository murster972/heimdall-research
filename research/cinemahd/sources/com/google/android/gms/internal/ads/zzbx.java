package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;

final class zzbx implements zzdsa {
    static final zzdsa zzew = new zzbx();

    private zzbx() {
    }

    public final boolean zzf(int i) {
        return zzbs.zza.zzc.zzh(i) != null;
    }
}
