package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzyb extends zzvl {
    final /* synthetic */ zzxz zzcfj;

    private zzyb(zzxz zzxz) {
        this.zzcfj = zzxz;
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return null;
    }

    public final boolean isLoading() throws RemoteException {
        return false;
    }

    public final void zza(zzug zzug, int i) throws RemoteException {
        zzayu.zzex("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzayk.zzyu.post(new zzye(this));
    }

    public final void zzb(zzug zzug) throws RemoteException {
        zza(zzug, 1);
    }

    public final String zzka() throws RemoteException {
        return null;
    }
}
