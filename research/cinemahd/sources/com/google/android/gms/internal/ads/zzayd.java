package com.google.android.gms.internal.ads;

public final class zzayd<T> {
    private T zzduw;

    public final T get() {
        return this.zzduw;
    }

    public final void set(T t) {
        this.zzduw = t;
    }
}
