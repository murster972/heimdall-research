package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzasa implements MediationRewardedVideoAdListener {
    private final zzarz zzdnx;

    public zzasa(zzarz zzarz) {
        this.zzdnx = zzarz;
    }

    public final void onAdClicked(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdClicked.");
        try {
            this.zzdnx.zzak(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onAdClosed(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdClosed.");
        try {
            this.zzdnx.zzaj(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onAdFailedToLoad(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, int i) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdFailedToLoad.");
        try {
            this.zzdnx.zze(ObjectWrapper.a(mediationRewardedVideoAdAdapter), i);
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onAdLeftApplication(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdLeftApplication.");
        try {
            this.zzdnx.zzal(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onAdLoaded(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdLoaded.");
        try {
            this.zzdnx.zzag(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onAdOpened(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdOpened.");
        try {
            this.zzdnx.zzah(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onInitializationFailed(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, int i) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onInitializationFailed.");
        try {
            this.zzdnx.zzd(ObjectWrapper.a(mediationRewardedVideoAdAdapter), i);
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onInitializationSucceeded(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onInitializationSucceeded.");
        try {
            this.zzdnx.zzaf(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onRewarded(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter, RewardItem rewardItem) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onRewarded.");
        if (rewardItem != null) {
            try {
                this.zzdnx.zza(ObjectWrapper.a(mediationRewardedVideoAdAdapter), new zzasd(rewardItem));
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        } else {
            this.zzdnx.zza(ObjectWrapper.a(mediationRewardedVideoAdAdapter), new zzasd("", 1));
        }
    }

    public final void onVideoCompleted(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onVideoCompleted.");
        try {
            this.zzdnx.zzam(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onVideoStarted(MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onVideoStarted.");
        try {
            this.zzdnx.zzai(ObjectWrapper.a(mediationRewardedVideoAdAdapter));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void zzb(Bundle bundle) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdMetadataChanged.");
        try {
            this.zzdnx.zzb(bundle);
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
