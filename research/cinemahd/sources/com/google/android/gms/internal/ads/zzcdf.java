package com.google.android.gms.internal.ads;

import java.util.Map;

public final class zzcdf implements zzdcx {
    private zzsm zzfry;
    private Map<zzdco, zzcdh> zzfsg;

    zzcdf(zzsm zzsm, Map<zzdco, zzcdh> map) {
        this.zzfsg = map;
        this.zzfry = zzsm;
    }

    public final void zza(zzdco zzdco, String str) {
    }

    public final void zza(zzdco zzdco, String str, Throwable th) {
        if (this.zzfsg.containsKey(zzdco)) {
            this.zzfry.zza(this.zzfsg.get(zzdco).zzfsl);
        }
    }

    public final void zzb(zzdco zzdco, String str) {
        if (this.zzfsg.containsKey(zzdco)) {
            this.zzfry.zza(this.zzfsg.get(zzdco).zzfsj);
        }
    }

    public final void zzc(zzdco zzdco, String str) {
        if (this.zzfsg.containsKey(zzdco)) {
            this.zzfry.zza(this.zzfsg.get(zzdco).zzfsk);
        }
    }
}
