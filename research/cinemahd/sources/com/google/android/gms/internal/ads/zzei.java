package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.gass.zzc;
import com.google.android.gms.internal.ads.zzbs;
import com.vungle.warren.AdLoader;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzei {
    private static final String TAG = "zzei";
    protected Context zzup;
    private volatile boolean zzvh = false;
    private ExecutorService zzxk;
    private DexClassLoader zzxl;
    private zzds zzxm;
    private byte[] zzxn;
    private volatile AdvertisingIdClient zzxo = null;
    private Future zzxp = null;
    private boolean zzxq;
    /* access modifiers changed from: private */
    public volatile zzbs.zza zzxr = null;
    private Future zzxs = null;
    private zzde zzxt;
    private boolean zzxu = false;
    private boolean zzxv = false;
    private Map<Pair<String, String>, zzfu> zzxw;
    private boolean zzxx = false;
    /* access modifiers changed from: private */
    public boolean zzxy;
    private boolean zzxz;

    final class zza extends BroadcastReceiver {
        private zza() {
        }

        public final void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                boolean unused = zzei.this.zzxy = true;
            } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                boolean unused2 = zzei.this.zzxy = false;
            }
        }

        /* synthetic */ zza(zzei zzei, zzel zzel) {
            this();
        }
    }

    private zzei(Context context) {
        boolean z = true;
        this.zzxy = true;
        this.zzxz = false;
        Context applicationContext = context.getApplicationContext();
        this.zzxq = applicationContext == null ? false : z;
        this.zzup = this.zzxq ? applicationContext : context;
        this.zzxw = new HashMap();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(19:0|1|2|(1:4)|5|6|7|8|(1:10)(1:11)|12|(1:14)(1:15)|16|17|18|(2:20|(1:22)(2:23|24))|25|26|27|(15:28|29|(2:31|(1:33)(2:34|35))|36|(1:38)|39|40|41|42|43|44|45|(1:47)|48|69)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004d */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0056 A[Catch:{ zzdv -> 0x0150, zzeh -> 0x0157 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0089 A[Catch:{ all -> 0x011f, FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4 A[Catch:{ all -> 0x011f, FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00fa A[Catch:{ zzdv -> 0x0150, zzeh -> 0x0157 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.ads.zzei zza(android.content.Context r9, java.lang.String r10, java.lang.String r11, boolean r12) {
        /*
            java.lang.String r0 = "%s/%s.dex"
            com.google.android.gms.internal.ads.zzei r1 = new com.google.android.gms.internal.ads.zzei
            r1.<init>(r9)
            com.google.android.gms.internal.ads.zzel r9 = new com.google.android.gms.internal.ads.zzel     // Catch:{ zzeh -> 0x0157 }
            r9.<init>()     // Catch:{ zzeh -> 0x0157 }
            java.util.concurrent.ExecutorService r9 = java.util.concurrent.Executors.newCachedThreadPool(r9)     // Catch:{ zzeh -> 0x0157 }
            r1.zzxk = r9     // Catch:{ zzeh -> 0x0157 }
            r1.zzvh = r12     // Catch:{ zzeh -> 0x0157 }
            if (r12 == 0) goto L_0x0023
            java.util.concurrent.ExecutorService r9 = r1.zzxk     // Catch:{ zzeh -> 0x0157 }
            com.google.android.gms.internal.ads.zzek r12 = new com.google.android.gms.internal.ads.zzek     // Catch:{ zzeh -> 0x0157 }
            r12.<init>(r1)     // Catch:{ zzeh -> 0x0157 }
            java.util.concurrent.Future r9 = r9.submit(r12)     // Catch:{ zzeh -> 0x0157 }
            r1.zzxp = r9     // Catch:{ zzeh -> 0x0157 }
        L_0x0023:
            java.util.concurrent.ExecutorService r9 = r1.zzxk     // Catch:{ zzeh -> 0x0157 }
            com.google.android.gms.internal.ads.zzem r12 = new com.google.android.gms.internal.ads.zzem     // Catch:{ zzeh -> 0x0157 }
            r12.<init>(r1)     // Catch:{ zzeh -> 0x0157 }
            r9.execute(r12)     // Catch:{ zzeh -> 0x0157 }
            r9 = 1
            r12 = 0
            com.google.android.gms.common.GoogleApiAvailabilityLight r2 = com.google.android.gms.common.GoogleApiAvailabilityLight.a()     // Catch:{ all -> 0x004d }
            android.content.Context r3 = r1.zzup     // Catch:{ all -> 0x004d }
            int r3 = r2.b((android.content.Context) r3)     // Catch:{ all -> 0x004d }
            if (r3 <= 0) goto L_0x003d
            r3 = 1
            goto L_0x003e
        L_0x003d:
            r3 = 0
        L_0x003e:
            r1.zzxu = r3     // Catch:{ all -> 0x004d }
            android.content.Context r3 = r1.zzup     // Catch:{ all -> 0x004d }
            int r2 = r2.c((android.content.Context) r3)     // Catch:{ all -> 0x004d }
            if (r2 != 0) goto L_0x004a
            r2 = 1
            goto L_0x004b
        L_0x004a:
            r2 = 0
        L_0x004b:
            r1.zzxv = r2     // Catch:{ all -> 0x004d }
        L_0x004d:
            r1.zza((int) r12, (boolean) r9)     // Catch:{ zzeh -> 0x0157 }
            boolean r2 = com.google.android.gms.internal.ads.zzep.isMainThread()     // Catch:{ zzeh -> 0x0157 }
            if (r2 == 0) goto L_0x0071
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r2 = com.google.android.gms.internal.ads.zzzn.zzclk     // Catch:{ zzeh -> 0x0157 }
            com.google.android.gms.internal.ads.zzzj r3 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ zzeh -> 0x0157 }
            java.lang.Object r2 = r3.zzd(r2)     // Catch:{ zzeh -> 0x0157 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ zzeh -> 0x0157 }
            boolean r2 = r2.booleanValue()     // Catch:{ zzeh -> 0x0157 }
            if (r2 != 0) goto L_0x0069
            goto L_0x0071
        L_0x0069:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException     // Catch:{ zzeh -> 0x0157 }
            java.lang.String r10 = "Task Context initialization must not be called from the UI thread."
            r9.<init>(r10)     // Catch:{ zzeh -> 0x0157 }
            throw r9     // Catch:{ zzeh -> 0x0157 }
        L_0x0071:
            com.google.android.gms.internal.ads.zzds r2 = new com.google.android.gms.internal.ads.zzds     // Catch:{ zzeh -> 0x0157 }
            r3 = 0
            r2.<init>(r3)     // Catch:{ zzeh -> 0x0157 }
            r1.zzxm = r2     // Catch:{ zzeh -> 0x0157 }
            com.google.android.gms.internal.ads.zzds r2 = r1.zzxm     // Catch:{ zzdv -> 0x0150 }
            byte[] r10 = r2.zzar(r10)     // Catch:{ zzdv -> 0x0150 }
            r1.zzxn = r10     // Catch:{ zzdv -> 0x0150 }
            android.content.Context r10 = r1.zzup     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.io.File r10 = r10.getCacheDir()     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            if (r10 != 0) goto L_0x009a
            android.content.Context r10 = r1.zzup     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.lang.String r2 = "dex"
            java.io.File r10 = r10.getDir(r2, r12)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            if (r10 == 0) goto L_0x0094
            goto L_0x009a
        L_0x0094:
            com.google.android.gms.internal.ads.zzeh r9 = new com.google.android.gms.internal.ads.zzeh     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r9.<init>()     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            throw r9     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
        L_0x009a:
            java.lang.String r2 = "1570054248636"
            java.io.File r4 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.lang.String r5 = "%s/%s.jar"
            r6 = 2
            java.lang.Object[] r7 = new java.lang.Object[r6]     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r7[r12] = r10     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r7[r9] = r2     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.lang.String r5 = java.lang.String.format(r5, r7)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r4.<init>(r5)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            boolean r5 = r4.exists()     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            if (r5 != 0) goto L_0x00cb
            com.google.android.gms.internal.ads.zzds r5 = r1.zzxm     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            byte[] r7 = r1.zzxn     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            byte[] r11 = r5.zza(r7, r11)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r4.createNewFile()     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r5.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            int r7 = r11.length     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r5.write(r11, r12, r7)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r5.close()     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
        L_0x00cb:
            r1.zzb((java.io.File) r10, (java.lang.String) r2)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            dalvik.system.DexClassLoader r11 = new dalvik.system.DexClassLoader     // Catch:{ all -> 0x011f }
            java.lang.String r5 = r4.getAbsolutePath()     // Catch:{ all -> 0x011f }
            java.lang.String r7 = r10.getAbsolutePath()     // Catch:{ all -> 0x011f }
            android.content.Context r8 = r1.zzup     // Catch:{ all -> 0x011f }
            java.lang.ClassLoader r8 = r8.getClassLoader()     // Catch:{ all -> 0x011f }
            r11.<init>(r5, r7, r3, r8)     // Catch:{ all -> 0x011f }
            r1.zzxl = r11     // Catch:{ all -> 0x011f }
            zzb(r4)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r1.zza((java.io.File) r10, (java.lang.String) r2)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.lang.Object[] r11 = new java.lang.Object[r6]     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r11[r12] = r10     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r11[r9] = r2     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.lang.String r10 = java.lang.String.format(r0, r11)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            zzas(r10)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            boolean r10 = r1.zzxz     // Catch:{ zzeh -> 0x0157 }
            if (r10 != 0) goto L_0x0115
            android.content.IntentFilter r10 = new android.content.IntentFilter     // Catch:{ zzeh -> 0x0157 }
            r10.<init>()     // Catch:{ zzeh -> 0x0157 }
            java.lang.String r11 = "android.intent.action.USER_PRESENT"
            r10.addAction(r11)     // Catch:{ zzeh -> 0x0157 }
            java.lang.String r11 = "android.intent.action.SCREEN_OFF"
            r10.addAction(r11)     // Catch:{ zzeh -> 0x0157 }
            android.content.Context r11 = r1.zzup     // Catch:{ zzeh -> 0x0157 }
            com.google.android.gms.internal.ads.zzei$zza r12 = new com.google.android.gms.internal.ads.zzei$zza     // Catch:{ zzeh -> 0x0157 }
            r12.<init>(r1, r3)     // Catch:{ zzeh -> 0x0157 }
            r11.registerReceiver(r12, r10)     // Catch:{ zzeh -> 0x0157 }
            r1.zzxz = r9     // Catch:{ zzeh -> 0x0157 }
        L_0x0115:
            com.google.android.gms.internal.ads.zzde r10 = new com.google.android.gms.internal.ads.zzde     // Catch:{ zzeh -> 0x0157 }
            r10.<init>(r1)     // Catch:{ zzeh -> 0x0157 }
            r1.zzxt = r10     // Catch:{ zzeh -> 0x0157 }
            r1.zzxx = r9     // Catch:{ zzeh -> 0x0157 }
            goto L_0x0157
        L_0x011f:
            r11 = move-exception
            zzb(r4)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r1.zza((java.io.File) r10, (java.lang.String) r2)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.lang.Object[] r3 = new java.lang.Object[r6]     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r3[r12] = r10     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            r3[r9] = r2     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            java.lang.String r9 = java.lang.String.format(r0, r3)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            zzas(r9)     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
            throw r11     // Catch:{ FileNotFoundException -> 0x0149, IOException -> 0x0142, zzdv -> 0x013b, NullPointerException -> 0x0134 }
        L_0x0134:
            r9 = move-exception
            com.google.android.gms.internal.ads.zzeh r10 = new com.google.android.gms.internal.ads.zzeh     // Catch:{ zzeh -> 0x0157 }
            r10.<init>(r9)     // Catch:{ zzeh -> 0x0157 }
            throw r10     // Catch:{ zzeh -> 0x0157 }
        L_0x013b:
            r9 = move-exception
            com.google.android.gms.internal.ads.zzeh r10 = new com.google.android.gms.internal.ads.zzeh     // Catch:{ zzeh -> 0x0157 }
            r10.<init>(r9)     // Catch:{ zzeh -> 0x0157 }
            throw r10     // Catch:{ zzeh -> 0x0157 }
        L_0x0142:
            r9 = move-exception
            com.google.android.gms.internal.ads.zzeh r10 = new com.google.android.gms.internal.ads.zzeh     // Catch:{ zzeh -> 0x0157 }
            r10.<init>(r9)     // Catch:{ zzeh -> 0x0157 }
            throw r10     // Catch:{ zzeh -> 0x0157 }
        L_0x0149:
            r9 = move-exception
            com.google.android.gms.internal.ads.zzeh r10 = new com.google.android.gms.internal.ads.zzeh     // Catch:{ zzeh -> 0x0157 }
            r10.<init>(r9)     // Catch:{ zzeh -> 0x0157 }
            throw r10     // Catch:{ zzeh -> 0x0157 }
        L_0x0150:
            r9 = move-exception
            com.google.android.gms.internal.ads.zzeh r10 = new com.google.android.gms.internal.ads.zzeh     // Catch:{ zzeh -> 0x0157 }
            r10.<init>(r9)     // Catch:{ zzeh -> 0x0157 }
            throw r10     // Catch:{ zzeh -> 0x0157 }
        L_0x0157:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzei.zza(android.content.Context, java.lang.String, java.lang.String, boolean):com.google.android.gms.internal.ads.zzei");
    }

    private static void zzas(String str) {
        zzb(new File(str));
    }

    private static void zzb(File file) {
        if (!file.exists()) {
            Log.d(TAG, String.format("File %s not found. No need for deletion", new Object[]{file.getAbsolutePath()}));
            return;
        }
        file.delete();
    }

    /* access modifiers changed from: private */
    public final void zzch() {
        try {
            if (this.zzxo == null && this.zzxq) {
                AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(this.zzup);
                advertisingIdClient.start();
                this.zzxo = advertisingIdClient;
            }
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException unused) {
            this.zzxo = null;
        }
    }

    private final zzbs.zza zzci() {
        try {
            return zzc.a(this.zzup, this.zzup.getPackageName(), Integer.toString(this.zzup.getPackageManager().getPackageInfo(this.zzup.getPackageName(), 0).versionCode));
        } catch (Throwable unused) {
            return null;
        }
    }

    public final Context getContext() {
        return this.zzup;
    }

    public final boolean isInitialized() {
        return this.zzxx;
    }

    public final int zzbr() {
        if (this.zzxt != null) {
            return zzde.zzbr();
        }
        return Integer.MIN_VALUE;
    }

    public final ExecutorService zzbx() {
        return this.zzxk;
    }

    public final DexClassLoader zzby() {
        return this.zzxl;
    }

    public final zzds zzbz() {
        return this.zzxm;
    }

    public final byte[] zzca() {
        return this.zzxn;
    }

    public final boolean zzcb() {
        return this.zzxu;
    }

    public final zzde zzcc() {
        return this.zzxt;
    }

    public final boolean zzcd() {
        return this.zzxv;
    }

    public final boolean zzce() {
        return this.zzxy;
    }

    public final zzbs.zza zzcf() {
        return this.zzxr;
    }

    public final Future zzcg() {
        return this.zzxs;
    }

    public final AdvertisingIdClient zzcj() {
        if (!this.zzvh) {
            return null;
        }
        if (this.zzxo != null) {
            return this.zzxo;
        }
        Future future = this.zzxp;
        if (future != null) {
            try {
                future.get(AdLoader.RETRY_DELAY, TimeUnit.MILLISECONDS);
                this.zzxp = null;
            } catch (InterruptedException | ExecutionException unused) {
            } catch (TimeoutException unused2) {
                this.zzxp.cancel(true);
            }
        }
        return this.zzxo;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x00cc */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e2 A[SYNTHETIC, Splitter:B:52:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e9 A[SYNTHETIC, Splitter:B:56:0x00e9] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00f0 A[SYNTHETIC, Splitter:B:64:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f7 A[SYNTHETIC, Splitter:B:68:0x00f7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzb(java.io.File r10, java.lang.String r11) {
        /*
            r9 = this;
            java.io.File r0 = new java.io.File
            r1 = 2
            java.lang.Object[] r2 = new java.lang.Object[r1]
            r3 = 0
            r2[r3] = r10
            r4 = 1
            r2[r4] = r11
            java.lang.String r5 = "%s/%s.tmp"
            java.lang.String r2 = java.lang.String.format(r5, r2)
            r0.<init>(r2)
            boolean r2 = r0.exists()
            if (r2 != 0) goto L_0x001b
            return r3
        L_0x001b:
            java.io.File r2 = new java.io.File
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r3] = r10
            r1[r4] = r11
            java.lang.String r10 = "%s/%s.dex"
            java.lang.String r10 = java.lang.String.format(r10, r1)
            r2.<init>(r10)
            boolean r10 = r2.exists()
            if (r10 == 0) goto L_0x0033
            return r3
        L_0x0033:
            r10 = 0
            long r5 = r0.length()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ed, all -> 0x00de }
            r7 = 0
            int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r1 > 0) goto L_0x0042
            zzb(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ed, all -> 0x00de }
            return r3
        L_0x0042:
            int r1 = (int) r5     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ed, all -> 0x00de }
            byte[] r1 = new byte[r1]     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ed, all -> 0x00de }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ed, all -> 0x00de }
            r5.<init>(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ed, all -> 0x00de }
            int r6 = r5.read(r1)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            if (r6 > 0) goto L_0x005e
            java.lang.String r11 = TAG     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            java.lang.String r1 = "Cannot read the cache data."
            android.util.Log.d(r11, r1)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            zzb(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            r5.close()     // Catch:{ IOException -> 0x005d }
        L_0x005d:
            return r3
        L_0x005e:
            com.google.android.gms.internal.ads.zzdrg r6 = com.google.android.gms.internal.ads.zzdrg.zzazi()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            com.google.android.gms.internal.ads.zzbs$zzc r1 = com.google.android.gms.internal.ads.zzbs.zzc.zzb((byte[]) r1, (com.google.android.gms.internal.ads.zzdrg) r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            java.lang.String r6 = new java.lang.String     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            com.google.android.gms.internal.ads.zzdqk r7 = r1.zzaz()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r7 = r7.toByteArray()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            r6.<init>(r7)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            boolean r11 = r11.equals(r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            if (r11 == 0) goto L_0x00d5
            com.google.android.gms.internal.ads.zzdqk r11 = r1.zzay()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r11 = r11.toByteArray()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            com.google.android.gms.internal.ads.zzdqk r6 = r1.zzax()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r6 = r6.toByteArray()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r6 = com.google.android.gms.internal.ads.zzck.zzb(r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            boolean r11 = java.util.Arrays.equals(r11, r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            if (r11 == 0) goto L_0x00d5
            com.google.android.gms.internal.ads.zzdqk r11 = r1.zzba()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r11 = r11.toByteArray()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            java.lang.String r6 = android.os.Build.VERSION.SDK     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r6 = r6.getBytes()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            boolean r11 = java.util.Arrays.equals(r11, r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            if (r11 != 0) goto L_0x00a8
            goto L_0x00d5
        L_0x00a8:
            com.google.android.gms.internal.ads.zzds r11 = r9.zzxm     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r0 = r9.zzxn     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            java.lang.String r6 = new java.lang.String     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            com.google.android.gms.internal.ads.zzdqk r1 = r1.zzax()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r1 = r1.toByteArray()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            r6.<init>(r1)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            byte[] r11 = r11.zza(r0, r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            r2.createNewFile()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            r0.<init>(r2)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            int r10 = r11.length     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00d3, all -> 0x00d0 }
            r0.write(r11, r3, r10)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00d3, all -> 0x00d0 }
            r5.close()     // Catch:{ IOException -> 0x00cc }
        L_0x00cc:
            r0.close()     // Catch:{ IOException -> 0x00cf }
        L_0x00cf:
            return r4
        L_0x00d0:
            r11 = move-exception
            r10 = r0
            goto L_0x00e0
        L_0x00d3:
            r10 = r0
            goto L_0x00ee
        L_0x00d5:
            zzb(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ee, all -> 0x00dc }
            r5.close()     // Catch:{ IOException -> 0x00db }
        L_0x00db:
            return r3
        L_0x00dc:
            r11 = move-exception
            goto L_0x00e0
        L_0x00de:
            r11 = move-exception
            r5 = r10
        L_0x00e0:
            if (r5 == 0) goto L_0x00e7
            r5.close()     // Catch:{ IOException -> 0x00e6 }
            goto L_0x00e7
        L_0x00e6:
        L_0x00e7:
            if (r10 == 0) goto L_0x00ec
            r10.close()     // Catch:{ IOException -> 0x00ec }
        L_0x00ec:
            throw r11
        L_0x00ed:
            r5 = r10
        L_0x00ee:
            if (r5 == 0) goto L_0x00f5
            r5.close()     // Catch:{ IOException -> 0x00f4 }
            goto L_0x00f5
        L_0x00f4:
        L_0x00f5:
            if (r10 == 0) goto L_0x00fa
            r10.close()     // Catch:{ IOException -> 0x00fa }
        L_0x00fa:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzei.zzb(java.io.File, java.lang.String):boolean");
    }

    /* access modifiers changed from: package-private */
    public final zzbs.zza zzb(int i, boolean z) {
        if (i > 0 && z) {
            try {
                Thread.sleep((long) (i * 1000));
            } catch (InterruptedException unused) {
            }
        }
        return zzci();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x00c0 */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d2 A[SYNTHETIC, Splitter:B:39:0x00d2] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d9 A[SYNTHETIC, Splitter:B:43:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e3 A[SYNTHETIC, Splitter:B:52:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00ea A[SYNTHETIC, Splitter:B:56:0x00ea] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zza(java.io.File r10, java.lang.String r11) {
        /*
            r9 = this;
            java.lang.String r0 = "test"
            java.io.File r1 = new java.io.File
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r4 = 0
            r3[r4] = r10
            r5 = 1
            r3[r5] = r11
            java.lang.String r6 = "%s/%s.tmp"
            java.lang.String r3 = java.lang.String.format(r6, r3)
            r1.<init>(r3)
            boolean r3 = r1.exists()
            if (r3 == 0) goto L_0x001d
            return
        L_0x001d:
            java.io.File r3 = new java.io.File
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r4] = r10
            r2[r5] = r11
            java.lang.String r10 = "%s/%s.dex"
            java.lang.String r10 = java.lang.String.format(r10, r2)
            r3.<init>(r10)
            boolean r10 = r3.exists()
            if (r10 != 0) goto L_0x0035
            return
        L_0x0035:
            long r5 = r3.length()
            r7 = 0
            int r10 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r10 > 0) goto L_0x0040
            return
        L_0x0040:
            int r10 = (int) r5
            byte[] r10 = new byte[r10]
            r2 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e0, all -> 0x00ce }
            r5.<init>(r3)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e0, all -> 0x00ce }
            int r6 = r5.read(r10)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            if (r6 > 0) goto L_0x0056
            r5.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0052:
            zzb(r3)
            return
        L_0x0056:
            java.io.PrintStream r6 = java.lang.System.out     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            r6.print(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            java.io.PrintStream r6 = java.lang.System.out     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            r6.print(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            java.io.PrintStream r6 = java.lang.System.out     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            r6.print(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzbs$zzc$zza r0 = com.google.android.gms.internal.ads.zzbs.zzc.zzbb()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            java.lang.String r6 = android.os.Build.VERSION.SDK     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            byte[] r6 = r6.getBytes()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzdqk r6 = com.google.android.gms.internal.ads.zzdqk.zzu(r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzbs$zzc$zza r0 = r0.zzh(r6)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            byte[] r11 = r11.getBytes()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzdqk r11 = com.google.android.gms.internal.ads.zzdqk.zzu(r11)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzbs$zzc$zza r11 = r0.zzg(r11)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzds r0 = r9.zzxm     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            byte[] r6 = r9.zzxn     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            java.lang.String r10 = r0.zzb(r6, r10)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            byte[] r10 = r10.getBytes()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzdqk r0 = com.google.android.gms.internal.ads.zzdqk.zzu(r10)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzbs$zzc$zza r0 = r11.zze(r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            byte[] r10 = com.google.android.gms.internal.ads.zzck.zzb(r10)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzdqk r10 = com.google.android.gms.internal.ads.zzdqk.zzu(r10)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            r0.zzf(r10)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            r1.createNewFile()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            r10.<init>(r1)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00e1, all -> 0x00cc }
            com.google.android.gms.internal.ads.zzdte r11 = r11.zzbaf()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ca, all -> 0x00c7 }
            com.google.android.gms.internal.ads.zzdrt r11 = (com.google.android.gms.internal.ads.zzdrt) r11     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ca, all -> 0x00c7 }
            com.google.android.gms.internal.ads.zzbs$zzc r11 = (com.google.android.gms.internal.ads.zzbs.zzc) r11     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ca, all -> 0x00c7 }
            byte[] r11 = r11.toByteArray()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ca, all -> 0x00c7 }
            int r0 = r11.length     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ca, all -> 0x00c7 }
            r10.write(r11, r4, r0)     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ca, all -> 0x00c7 }
            r10.close()     // Catch:{ zzdv | IOException | NoSuchAlgorithmException -> 0x00ca, all -> 0x00c7 }
            r5.close()     // Catch:{ IOException -> 0x00c0 }
        L_0x00c0:
            r10.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x00c3:
            zzb(r3)
            return
        L_0x00c7:
            r11 = move-exception
            r2 = r10
            goto L_0x00d0
        L_0x00ca:
            r2 = r10
            goto L_0x00e1
        L_0x00cc:
            r11 = move-exception
            goto L_0x00d0
        L_0x00ce:
            r11 = move-exception
            r5 = r2
        L_0x00d0:
            if (r5 == 0) goto L_0x00d7
            r5.close()     // Catch:{ IOException -> 0x00d6 }
            goto L_0x00d7
        L_0x00d6:
        L_0x00d7:
            if (r2 == 0) goto L_0x00dc
            r2.close()     // Catch:{ IOException -> 0x00dc }
        L_0x00dc:
            zzb(r3)
            throw r11
        L_0x00e0:
            r5 = r2
        L_0x00e1:
            if (r5 == 0) goto L_0x00e8
            r5.close()     // Catch:{ IOException -> 0x00e7 }
            goto L_0x00e8
        L_0x00e7:
        L_0x00e8:
            if (r2 == 0) goto L_0x00ed
            r2.close()     // Catch:{ IOException -> 0x00ed }
        L_0x00ed:
            zzb(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzei.zza(java.io.File, java.lang.String):void");
    }

    public final boolean zza(String str, String str2, Class<?>... clsArr) {
        if (this.zzxw.containsKey(new Pair(str, str2))) {
            return false;
        }
        this.zzxw.put(new Pair(str, str2), new zzfu(this, str, str2, clsArr));
        return true;
    }

    public final Method zza(String str, String str2) {
        zzfu zzfu = this.zzxw.get(new Pair(str, str2));
        if (zzfu == null) {
            return null;
        }
        return zzfu.zzcs();
    }

    /* access modifiers changed from: package-private */
    public final void zza(int i, boolean z) {
        if (this.zzxv) {
            Future<?> submit = this.zzxk.submit(new zzen(this, i, z));
            if (i == 0) {
                this.zzxs = submit;
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean zza(int i, zzbs.zza zza2) {
        if (i >= 4) {
            return false;
        }
        if (zza2 != null && zza2.zzaj() && !zza2.zzag().equals("0000000000000000000000000000000000000000000000000000000000000000") && zza2.zzal() && zza2.zzam().zzbd() && zza2.zzam().zzbe() != -2) {
            return false;
        }
        return true;
    }
}
