package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbhy implements zzdxg<zzbht> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzcka> zzeke;
    private final zzdxp<zzceq> zzekp;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzcis<zzdac, zzcjx>> zzfaw;
    private final zzdxp<zzcob> zzfax;
    private final zzdxp<zzatv> zzfay;

    public zzbhy(zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzcka> zzdxp3, zzdxp<zzcis<zzdac, zzcjx>> zzdxp4, zzdxp<zzcob> zzdxp5, zzdxp<zzceq> zzdxp6, zzdxp<zzatv> zzdxp7) {
        this.zzejv = zzdxp;
        this.zzfav = zzdxp2;
        this.zzeke = zzdxp3;
        this.zzfaw = zzdxp4;
        this.zzfax = zzdxp5;
        this.zzekp = zzdxp6;
        this.zzfay = zzdxp7;
    }

    public final /* synthetic */ Object get() {
        return new zzbht(this.zzejv.get(), this.zzfav.get(), this.zzeke.get(), this.zzfaw.get(), this.zzfax.get(), this.zzekp.get(), this.zzfay.get());
    }
}
