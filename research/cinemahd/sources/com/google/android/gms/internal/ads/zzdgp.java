package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class zzdgp<V> extends zzdgn<V> {
    private final zzdhe<V> zzgwt;

    zzdgp(zzdhe<V> zzdhe) {
        this.zzgwt = (zzdhe) zzdei.checkNotNull(zzdhe);
    }

    public final void addListener(Runnable runnable, Executor executor) {
        this.zzgwt.addListener(runnable, executor);
    }

    public final boolean cancel(boolean z) {
        return this.zzgwt.cancel(z);
    }

    public final V get() throws InterruptedException, ExecutionException {
        return this.zzgwt.get();
    }

    public final boolean isCancelled() {
        return this.zzgwt.isCancelled();
    }

    public final boolean isDone() {
        return this.zzgwt.isDone();
    }

    public final String toString() {
        return this.zzgwt.toString();
    }

    public final V get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.zzgwt.get(j, timeUnit);
    }
}
