package com.google.android.gms.internal.ads;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

final class zzagj extends zzagg {
    private final /* synthetic */ zzazl zzbrs;

    zzagj(zzagk zzagk, zzazl zzazl) {
        this.zzbrs = zzazl;
    }

    public final void zza(ParcelFileDescriptor parcelFileDescriptor) throws RemoteException {
        this.zzbrs.set(parcelFileDescriptor);
    }
}
