package com.google.android.gms.internal.ads;

import android.view.MotionEvent;

final class zzdo implements Runnable {
    private final /* synthetic */ int zzwj;
    private final /* synthetic */ int zzwk;
    private final /* synthetic */ int zzwl;

    zzdo(zzdi zzdi, int i, int i2, int i3) {
        this.zzwj = i;
        this.zzwk = i2;
        this.zzwl = i3;
    }

    public final void run() {
        zzdi.zzvc.zza(MotionEvent.obtain(0, (long) this.zzwj, 0, (float) this.zzwk, (float) this.zzwl, 0));
    }
}
