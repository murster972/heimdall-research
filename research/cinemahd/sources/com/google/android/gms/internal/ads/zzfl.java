package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfl extends zzfw {
    private final boolean zzzx;

    public zzfl(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 61);
        this.zzzx = zzei.zzce();
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        long longValue = ((Long) this.zzaae.invoke((Object) null, new Object[]{this.zzuv.getContext(), Boolean.valueOf(this.zzzx)})).longValue();
        synchronized (this.zzzt) {
            this.zzzt.zzbo(longValue);
        }
    }
}
