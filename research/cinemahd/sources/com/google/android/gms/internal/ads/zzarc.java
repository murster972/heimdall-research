package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.Objects;

public final class zzarc extends zzarh {
    private final String type;
    private final int zzdno;

    public zzarc(String str, int i) {
        this.type = str;
        this.zzdno = i;
    }

    public final boolean equals(Object obj) {
        if (obj != null && (obj instanceof zzarc)) {
            zzarc zzarc = (zzarc) obj;
            if (!Objects.a(this.type, zzarc.type) || !Objects.a(Integer.valueOf(this.zzdno), Integer.valueOf(zzarc.zzdno))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public final int getAmount() {
        return this.zzdno;
    }

    public final String getType() {
        return this.type;
    }
}
