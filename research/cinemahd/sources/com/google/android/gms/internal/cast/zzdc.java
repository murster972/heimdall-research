package com.google.android.gms.internal.cast;

import android.text.TextUtils;

public class zzdc {
    protected final zzdw zzxw;
    private final String zzxx;
    private zzeb zzxy;

    protected zzdc(String str, String str2, String str3) {
        zzdk.zzp(str);
        this.zzxx = str;
        this.zzxw = new zzdw(str2);
        setSessionLabel(str3);
    }

    public final String getNamespace() {
        return this.zzxx;
    }

    public final void setSessionLabel(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.zzxw.zzu(str);
        }
    }

    public void zza(long j, int i) {
    }

    public final void zza(zzeb zzeb) {
        this.zzxy = zzeb;
        if (this.zzxy == null) {
            zzeq();
        }
    }

    public void zzeq() {
    }

    /* access modifiers changed from: protected */
    public final long zzes() {
        return this.zzxy.zzr();
    }

    public void zzo(String str) {
    }

    /* access modifiers changed from: protected */
    public final void zza(String str, long j, String str2) throws IllegalStateException {
        Object[] objArr = {str, null};
        this.zzxy.zza(this.zzxx, str, j, (String) null);
    }
}
