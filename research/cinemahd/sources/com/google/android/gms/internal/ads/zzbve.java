package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;

public final class zzbve implements zzbov, zzbsm {
    private final View view;
    private final zzatv zzbng;
    private final zzats zzffo;
    private final int zzfjj;
    private String zzfjp;
    private final Context zzup;

    public zzbve(zzats zzats, Context context, zzatv zzatv, View view2, int i) {
        this.zzffo = zzats;
        this.zzup = context;
        this.zzbng = zzatv;
        this.view = view2;
        this.zzfjj = i;
    }

    public final void onAdClosed() {
        this.zzffo.zzam(false);
    }

    public final void onAdLeftApplication() {
    }

    public final void onAdOpened() {
        View view2 = this.view;
        if (!(view2 == null || this.zzfjp == null)) {
            this.zzbng.zzg(view2.getContext(), this.zzfjp);
        }
        this.zzffo.zzam(true);
    }

    public final void onRewardedVideoCompleted() {
    }

    public final void onRewardedVideoStarted() {
    }

    public final void zzahx() {
        this.zzfjp = this.zzbng.zzad(this.zzup);
        String valueOf = String.valueOf(this.zzfjp);
        String str = this.zzfjj == 7 ? "/Rewarded" : "/Interstitial";
        this.zzfjp = str.length() != 0 ? valueOf.concat(str) : new String(valueOf);
    }

    public final void zzb(zzare zzare, String str, String str2) {
        if (this.zzbng.zzab(this.zzup)) {
            try {
                this.zzbng.zza(this.zzup, this.zzbng.zzag(this.zzup), this.zzffo.getAdUnitId(), zzare.getType(), zzare.getAmount());
            } catch (RemoteException e) {
                zzayu.zzd("Remote Exception to get reward item.", e);
            }
        }
    }
}
