package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicReference;

public final class zzmy extends zzmz {
    private static final int[] zzbds = new int[0];
    private final zznd zzbdt;
    private final AtomicReference<zzmx> zzbdu;

    public zzmy() {
        this((zznd) null);
    }

    private static int zze(int i, int i2) {
        if (i == -1) {
            return i2 == -1 ? 0 : -1;
        }
        if (i2 == -1) {
            return 1;
        }
        return i - i2;
    }

    private static boolean zze(int i, boolean z) {
        int i2 = i & 3;
        if (i2 != 3) {
            return z && i2 == 2;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x017d, code lost:
        if (r10 <= r11) goto L_0x0182;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x01e1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01a7  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzne[] zza(com.google.android.gms.internal.ads.zzhe[] r37, com.google.android.gms.internal.ads.zzmr[] r38, int[][][] r39) throws com.google.android.gms.internal.ads.zzgl {
        /*
            r36 = this;
            r0 = r37
            int r1 = r0.length
            com.google.android.gms.internal.ads.zzne[] r2 = new com.google.android.gms.internal.ads.zzne[r1]
            r3 = r36
            java.util.concurrent.atomic.AtomicReference<com.google.android.gms.internal.ads.zzmx> r4 = r3.zzbdu
            java.lang.Object r4 = r4.get()
            com.google.android.gms.internal.ads.zzmx r4 = (com.google.android.gms.internal.ads.zzmx) r4
            r6 = 0
            r7 = 0
            r8 = 0
        L_0x0012:
            r9 = 2
            if (r6 >= r1) goto L_0x025e
            r13 = r0[r6]
            int r13 = r13.getTrackType()
            if (r9 != r13) goto L_0x0246
            if (r7 != 0) goto L_0x022d
            r7 = r38[r6]
            r13 = r39[r6]
            int r14 = r4.zzbdm
            int r15 = r4.zzbdn
            int r11 = r4.zzbdo
            int r9 = r4.viewportWidth
            int r5 = r4.viewportHeight
            boolean r10 = r4.zzbdr
            boolean r12 = r4.zzbdp
            boolean r3 = r4.zzbdq
            r21 = r1
            r20 = r4
            r25 = r8
            r0 = 0
            r1 = 0
            r4 = 0
            r22 = 0
            r23 = -1
            r24 = -1
        L_0x0042:
            int r8 = r7.length
            if (r4 >= r8) goto L_0x020b
            com.google.android.gms.internal.ads.zzms r8 = r7.zzav(r4)
            r26 = r7
            java.util.ArrayList r7 = new java.util.ArrayList
            r27 = r2
            int r2 = r8.length
            r7.<init>(r2)
            r28 = r6
            r2 = 0
        L_0x0058:
            int r6 = r8.length
            if (r2 >= r6) goto L_0x0066
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)
            r7.add(r6)
            int r2 = r2 + 1
            goto L_0x0058
        L_0x0066:
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r9 == r2) goto L_0x0139
            if (r5 != r2) goto L_0x006f
            goto L_0x0139
        L_0x006f:
            r29 = r1
            r6 = 0
        L_0x0072:
            int r1 = r8.length
            if (r6 >= r1) goto L_0x0103
            com.google.android.gms.internal.ads.zzgw r1 = r8.zzaw(r6)
            r30 = r0
            int r0 = r1.width
            if (r0 <= 0) goto L_0x00e9
            r31 = r12
            int r12 = r1.height
            if (r12 <= 0) goto L_0x00e0
            if (r10 == 0) goto L_0x009e
            r32 = r10
            if (r0 <= r12) goto L_0x008e
            r10 = 1
            goto L_0x008f
        L_0x008e:
            r10 = 0
        L_0x008f:
            r33 = r5
            if (r9 <= r5) goto L_0x0095
            r5 = 1
            goto L_0x0096
        L_0x0095:
            r5 = 0
        L_0x0096:
            if (r10 == r5) goto L_0x00a2
            r5 = r9
            r34 = r5
            r10 = r33
            goto L_0x00a7
        L_0x009e:
            r33 = r5
            r32 = r10
        L_0x00a2:
            r10 = r9
            r34 = r10
            r5 = r33
        L_0x00a7:
            int r9 = r0 * r5
            r35 = r11
            int r11 = r12 * r10
            if (r9 < r11) goto L_0x00ba
            android.graphics.Point r5 = new android.graphics.Point
            int r0 = com.google.android.gms.internal.ads.zzoq.zzf(r11, r0)
            r5.<init>(r10, r0)
            r0 = r5
            goto L_0x00c3
        L_0x00ba:
            android.graphics.Point r0 = new android.graphics.Point
            int r9 = com.google.android.gms.internal.ads.zzoq.zzf(r9, r12)
            r0.<init>(r9, r5)
        L_0x00c3:
            int r5 = r1.width
            int r1 = r1.height
            int r9 = r5 * r1
            int r10 = r0.x
            float r10 = (float) r10
            r11 = 1065017672(0x3f7ae148, float:0.98)
            float r10 = r10 * r11
            int r10 = (int) r10
            if (r5 < r10) goto L_0x00f3
            int r0 = r0.y
            float r0 = (float) r0
            float r0 = r0 * r11
            int r0 = (int) r0
            if (r1 < r0) goto L_0x00f3
            if (r9 >= r2) goto L_0x00f3
            r2 = r9
            goto L_0x00f3
        L_0x00e0:
            r33 = r5
            r34 = r9
            r32 = r10
            r35 = r11
            goto L_0x00f3
        L_0x00e9:
            r33 = r5
            r34 = r9
            r32 = r10
            r35 = r11
            r31 = r12
        L_0x00f3:
            int r6 = r6 + 1
            r0 = r30
            r12 = r31
            r10 = r32
            r5 = r33
            r9 = r34
            r11 = r35
            goto L_0x0072
        L_0x0103:
            r30 = r0
            r33 = r5
            r34 = r9
            r32 = r10
            r35 = r11
            r31 = r12
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r2 == r0) goto L_0x0147
            int r0 = r7.size()
            r1 = 1
            int r0 = r0 - r1
        L_0x011a:
            if (r0 < 0) goto L_0x0147
            java.lang.Object r1 = r7.get(r0)
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r1 = r1.intValue()
            com.google.android.gms.internal.ads.zzgw r1 = r8.zzaw(r1)
            int r1 = r1.zzep()
            r5 = -1
            if (r1 == r5) goto L_0x0133
            if (r1 <= r2) goto L_0x0136
        L_0x0133:
            r7.remove(r0)
        L_0x0136:
            int r0 = r0 + -1
            goto L_0x011a
        L_0x0139:
            r30 = r0
            r29 = r1
            r33 = r5
            r34 = r9
            r32 = r10
            r35 = r11
            r31 = r12
        L_0x0147:
            r0 = r13[r4]
            r2 = r22
            r5 = r23
            r6 = r24
            r1 = 0
        L_0x0150:
            int r9 = r8.length
            if (r1 >= r9) goto L_0x01eb
            r9 = r0[r1]
            boolean r9 = zze((int) r9, (boolean) r3)
            if (r9 == 0) goto L_0x01db
            com.google.android.gms.internal.ads.zzgw r9 = r8.zzaw(r1)
            java.lang.Integer r10 = java.lang.Integer.valueOf(r1)
            boolean r10 = r7.contains(r10)
            if (r10 == 0) goto L_0x0184
            int r10 = r9.width
            r11 = -1
            if (r10 == r11) goto L_0x0171
            if (r10 > r14) goto L_0x0184
        L_0x0171:
            int r10 = r9.height
            if (r10 == r11) goto L_0x0177
            if (r10 > r15) goto L_0x0184
        L_0x0177:
            int r10 = r9.zzafa
            if (r10 == r11) goto L_0x0180
            r11 = r35
            if (r10 > r11) goto L_0x0186
            goto L_0x0182
        L_0x0180:
            r11 = r35
        L_0x0182:
            r10 = 1
            goto L_0x0187
        L_0x0184:
            r11 = r35
        L_0x0186:
            r10 = 0
        L_0x0187:
            if (r10 != 0) goto L_0x0191
            if (r31 == 0) goto L_0x018c
            goto L_0x0191
        L_0x018c:
            r23 = r0
            r22 = r3
            goto L_0x01e1
        L_0x0191:
            r22 = r3
            if (r10 == 0) goto L_0x0197
            r12 = 2
            goto L_0x0198
        L_0x0197:
            r12 = 1
        L_0x0198:
            r3 = r0[r1]
            r23 = r0
            r0 = 0
            boolean r3 = zze((int) r3, (boolean) r0)
            if (r3 == 0) goto L_0x01a5
            int r12 = r12 + 1000
        L_0x01a5:
            if (r12 <= r2) goto L_0x01a9
            r0 = 1
            goto L_0x01aa
        L_0x01a9:
            r0 = 0
        L_0x01aa:
            if (r12 != r2) goto L_0x01cd
            int r0 = r9.zzep()
            if (r0 == r5) goto L_0x01bb
            int r0 = r9.zzep()
            int r0 = zze((int) r0, (int) r5)
            goto L_0x01c1
        L_0x01bb:
            int r0 = r9.zzafa
            int r0 = zze((int) r0, (int) r6)
        L_0x01c1:
            if (r3 == 0) goto L_0x01c8
            if (r10 == 0) goto L_0x01c8
            if (r0 <= 0) goto L_0x01cc
            goto L_0x01ca
        L_0x01c8:
            if (r0 >= 0) goto L_0x01cc
        L_0x01ca:
            r0 = 1
            goto L_0x01cd
        L_0x01cc:
            r0 = 0
        L_0x01cd:
            if (r0 == 0) goto L_0x01e1
            int r6 = r9.zzafa
            int r5 = r9.zzep()
            r29 = r1
            r30 = r8
            r2 = r12
            goto L_0x01e1
        L_0x01db:
            r23 = r0
            r22 = r3
            r11 = r35
        L_0x01e1:
            int r1 = r1 + 1
            r35 = r11
            r3 = r22
            r0 = r23
            goto L_0x0150
        L_0x01eb:
            r22 = r3
            r11 = r35
            int r4 = r4 + 1
            r23 = r5
            r24 = r6
            r7 = r26
            r6 = r28
            r1 = r29
            r0 = r30
            r12 = r31
            r10 = r32
            r5 = r33
            r9 = r34
            r22 = r2
            r2 = r27
            goto L_0x0042
        L_0x020b:
            r30 = r0
            r29 = r1
            r27 = r2
            r28 = r6
            if (r30 != 0) goto L_0x0218
            r16 = 0
            goto L_0x0223
        L_0x0218:
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r1 = r29
            r0 = r30
            r11.<init>(r0, r1)
            r16 = r11
        L_0x0223:
            r27[r28] = r16
            r0 = r27[r28]
            if (r0 == 0) goto L_0x022b
            r7 = 1
            goto L_0x0237
        L_0x022b:
            r7 = 0
            goto L_0x0237
        L_0x022d:
            r21 = r1
            r27 = r2
            r20 = r4
            r28 = r6
            r25 = r8
        L_0x0237:
            r0 = r38[r28]
            int r0 = r0.length
            if (r0 <= 0) goto L_0x0240
            r19 = 1
            goto L_0x0242
        L_0x0240:
            r19 = 0
        L_0x0242:
            r0 = r25 | r19
            r8 = r0
            goto L_0x0250
        L_0x0246:
            r21 = r1
            r27 = r2
            r20 = r4
            r28 = r6
            r25 = r8
        L_0x0250:
            int r6 = r28 + 1
            r3 = r36
            r0 = r37
            r4 = r20
            r1 = r21
            r2 = r27
            goto L_0x0012
        L_0x025e:
            r27 = r2
            r20 = r4
            r25 = r8
            r0 = r1
            r1 = 0
            r2 = 0
            r3 = 0
        L_0x0268:
            if (r1 >= r0) goto L_0x0451
            r4 = r37[r1]
            int r4 = r4.getTrackType()
            r5 = 3
            r6 = 1
            if (r4 == r6) goto L_0x03aa
            r6 = 2
            if (r4 == r6) goto L_0x03a2
            if (r4 == r5) goto L_0x02f8
            r4 = r37[r1]
            r4.getTrackType()
            r4 = r38[r1]
            r5 = r39[r1]
            r6 = r20
            boolean r7 = r6.zzbdq
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
        L_0x028a:
            int r12 = r4.length
            if (r8 >= r12) goto L_0x02e2
            com.google.android.gms.internal.ads.zzms r12 = r4.zzav(r8)
            r13 = r5[r8]
            r14 = r11
            r11 = r10
            r10 = r9
            r9 = 0
        L_0x0298:
            int r15 = r12.length
            if (r9 >= r15) goto L_0x02d8
            r15 = r13[r9]
            boolean r15 = zze((int) r15, (boolean) r7)
            if (r15 == 0) goto L_0x02cd
            com.google.android.gms.internal.ads.zzgw r15 = r12.zzaw(r9)
            int r15 = r15.zzafu
            r19 = 1
            r15 = r15 & 1
            if (r15 == 0) goto L_0x02b2
            r15 = 1
            goto L_0x02b3
        L_0x02b2:
            r15 = 0
        L_0x02b3:
            r21 = r0
            if (r15 == 0) goto L_0x02b9
            r15 = 2
            goto L_0x02ba
        L_0x02b9:
            r15 = 1
        L_0x02ba:
            r0 = r13[r9]
            r20 = r4
            r4 = 0
            boolean r0 = zze((int) r0, (boolean) r4)
            if (r0 == 0) goto L_0x02c7
            int r15 = r15 + 1000
        L_0x02c7:
            if (r15 <= r14) goto L_0x02d1
            r11 = r9
            r10 = r12
            r14 = r15
            goto L_0x02d1
        L_0x02cd:
            r21 = r0
            r20 = r4
        L_0x02d1:
            int r9 = r9 + 1
            r4 = r20
            r0 = r21
            goto L_0x0298
        L_0x02d8:
            r21 = r0
            r20 = r4
            int r8 = r8 + 1
            r9 = r10
            r10 = r11
            r11 = r14
            goto L_0x028a
        L_0x02e2:
            r21 = r0
            if (r9 != 0) goto L_0x02e8
            r11 = 0
            goto L_0x02ed
        L_0x02e8:
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r11.<init>(r9, r10)
        L_0x02ed:
            r27[r1] = r11
            r18 = r2
            r2 = 0
            r5 = -1
            r15 = 0
            r17 = 2
            goto L_0x0444
        L_0x02f8:
            r21 = r0
            r6 = r20
            if (r3 != 0) goto L_0x03a6
            r0 = r38[r1]
            r3 = r39[r1]
            boolean r4 = r6.zzbdq
            r7 = 0
            r8 = 0
            r10 = 0
            r11 = 0
        L_0x0308:
            int r12 = r0.length
            if (r7 >= r12) goto L_0x0385
            com.google.android.gms.internal.ads.zzms r12 = r0.zzav(r7)
            r13 = r3[r7]
            r14 = r11
            r11 = r10
            r10 = r8
            r8 = 0
        L_0x0316:
            int r15 = r12.length
            if (r8 >= r15) goto L_0x037a
            r15 = r13[r8]
            boolean r15 = zze((int) r15, (boolean) r4)
            if (r15 == 0) goto L_0x0370
            com.google.android.gms.internal.ads.zzgw r15 = r12.zzaw(r8)
            int r5 = r15.zzafu
            r19 = 1
            r5 = r5 & 1
            if (r5 == 0) goto L_0x0330
            r5 = 1
            goto L_0x0331
        L_0x0330:
            r5 = 0
        L_0x0331:
            int r9 = r15.zzafu
            r17 = 2
            r9 = r9 & 2
            r23 = r0
            r0 = 0
            if (r9 == 0) goto L_0x033e
            r9 = 1
            goto L_0x033f
        L_0x033e:
            r9 = 0
        L_0x033f:
            boolean r24 = zza(r15, r0)
            if (r24 == 0) goto L_0x034f
            if (r5 == 0) goto L_0x0349
            r9 = 6
            goto L_0x035f
        L_0x0349:
            if (r9 != 0) goto L_0x034d
            r9 = 5
            goto L_0x035f
        L_0x034d:
            r9 = 4
            goto L_0x035f
        L_0x034f:
            if (r5 == 0) goto L_0x0353
            r9 = 3
            goto L_0x035f
        L_0x0353:
            if (r9 == 0) goto L_0x0374
            r0 = 0
            boolean r5 = zza(r15, r0)
            if (r5 == 0) goto L_0x035e
            r9 = 2
            goto L_0x035f
        L_0x035e:
            r9 = 1
        L_0x035f:
            r0 = r13[r8]
            r5 = 0
            boolean r0 = zze((int) r0, (boolean) r5)
            if (r0 == 0) goto L_0x036a
            int r9 = r9 + 1000
        L_0x036a:
            if (r9 <= r14) goto L_0x0374
            r11 = r8
            r14 = r9
            r10 = r12
            goto L_0x0374
        L_0x0370:
            r23 = r0
            r17 = 2
        L_0x0374:
            int r8 = r8 + 1
            r0 = r23
            r5 = 3
            goto L_0x0316
        L_0x037a:
            r23 = r0
            r17 = 2
            int r7 = r7 + 1
            r8 = r10
            r10 = r11
            r11 = r14
            r5 = 3
            goto L_0x0308
        L_0x0385:
            r17 = 2
            if (r8 != 0) goto L_0x038b
            r11 = 0
            goto L_0x0390
        L_0x038b:
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r11.<init>(r8, r10)
        L_0x0390:
            r27[r1] = r11
            r0 = r27[r1]
            if (r0 == 0) goto L_0x0398
            r0 = 1
            goto L_0x0399
        L_0x0398:
            r0 = 0
        L_0x0399:
            r3 = r0
            r0 = r2
            r2 = 0
            r5 = -1
            r15 = 0
            r19 = 1
            goto L_0x0448
        L_0x03a2:
            r21 = r0
            r6 = r20
        L_0x03a6:
            r17 = 2
            goto L_0x043f
        L_0x03aa:
            r21 = r0
            r6 = r20
            r17 = 2
            if (r2 != 0) goto L_0x043f
            r0 = r38[r1]
            r2 = r39[r1]
            boolean r4 = r6.zzbdq
            r5 = 0
            r7 = -1
            r8 = -1
            r9 = 0
        L_0x03bc:
            int r10 = r0.length
            if (r5 >= r10) goto L_0x0423
            com.google.android.gms.internal.ads.zzms r10 = r0.zzav(r5)
            r11 = r2[r5]
            r12 = r9
            r9 = r8
            r8 = r7
            r7 = 0
        L_0x03ca:
            int r13 = r10.length
            if (r7 >= r13) goto L_0x0415
            r13 = r11[r7]
            boolean r13 = zze((int) r13, (boolean) r4)
            if (r13 == 0) goto L_0x040a
            com.google.android.gms.internal.ads.zzgw r13 = r10.zzaw(r7)
            r14 = r11[r7]
            int r15 = r13.zzafu
            r19 = 1
            r15 = r15 & 1
            r16 = r2
            r2 = 0
            if (r15 == 0) goto L_0x03e9
            r15 = 1
            goto L_0x03ea
        L_0x03e9:
            r15 = 0
        L_0x03ea:
            boolean r13 = zza(r13, r2)
            if (r13 == 0) goto L_0x03f6
            if (r15 == 0) goto L_0x03f4
            r13 = 4
            goto L_0x03fb
        L_0x03f4:
            r13 = 3
            goto L_0x03fb
        L_0x03f6:
            if (r15 == 0) goto L_0x03fa
            r13 = 2
            goto L_0x03fb
        L_0x03fa:
            r13 = 1
        L_0x03fb:
            r15 = 0
            boolean r14 = zze((int) r14, (boolean) r15)
            if (r14 == 0) goto L_0x0404
            int r13 = r13 + 1000
        L_0x0404:
            if (r13 <= r12) goto L_0x0410
            r8 = r5
            r9 = r7
            r12 = r13
            goto L_0x0410
        L_0x040a:
            r16 = r2
            r2 = 0
            r15 = 0
            r19 = 1
        L_0x0410:
            int r7 = r7 + 1
            r2 = r16
            goto L_0x03ca
        L_0x0415:
            r16 = r2
            r2 = 0
            r15 = 0
            r19 = 1
            int r5 = r5 + 1
            r7 = r8
            r8 = r9
            r9 = r12
            r2 = r16
            goto L_0x03bc
        L_0x0423:
            r2 = 0
            r5 = -1
            r15 = 0
            r19 = 1
            if (r7 != r5) goto L_0x042c
            r11 = r2
            goto L_0x0435
        L_0x042c:
            com.google.android.gms.internal.ads.zzms r0 = r0.zzav(r7)
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r11.<init>(r0, r8)
        L_0x0435:
            r27[r1] = r11
            r0 = r27[r1]
            if (r0 == 0) goto L_0x043d
            r0 = 1
            goto L_0x0448
        L_0x043d:
            r0 = 0
            goto L_0x0448
        L_0x043f:
            r18 = r2
            r2 = 0
            r5 = -1
            r15 = 0
        L_0x0444:
            r19 = 1
            r0 = r18
        L_0x0448:
            int r1 = r1 + 1
            r2 = r0
            r20 = r6
            r0 = r21
            goto L_0x0268
        L_0x0451:
            return r27
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzmy.zza(com.google.android.gms.internal.ads.zzhe[], com.google.android.gms.internal.ads.zzmr[], int[][][]):com.google.android.gms.internal.ads.zzne[]");
    }

    private zzmy(zznd zznd) {
        this.zzbdt = null;
        this.zzbdu = new AtomicReference<>(new zzmx());
    }

    private static boolean zza(zzgw zzgw, String str) {
        return str != null && TextUtils.equals(str, zzoq.zzbl(zzgw.zzafv));
    }
}
