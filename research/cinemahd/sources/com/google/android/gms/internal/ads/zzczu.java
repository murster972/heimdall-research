package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import java.util.ArrayList;
import java.util.Set;

public final class zzczu {
    public final zzuj zzblm;
    public final zzaby zzddz;
    public final zzagz zzdkf;
    public final int zzgdu;
    public final zzwi zzgmj;
    public final zzyw zzgmk;
    public final zzug zzgml;
    public final String zzgmm;
    public final ArrayList<String> zzgmn;
    public final ArrayList<String> zzgmo;
    public final zzuo zzgmp;
    public final PublisherAdViewOptions zzgmq;
    public final zzwc zzgmr;
    public final Set<String> zzgms;

    private zzczu(zzczw zzczw) {
        zzyw zzyw;
        this.zzblm = zzczw.zzblm;
        this.zzgmm = zzczw.zzgmm;
        this.zzgmj = zzczw.zzgmj;
        this.zzgml = new zzug(zzczw.zzgml.versionCode, zzczw.zzgml.zzcby, zzczw.zzgml.extras, zzczw.zzgml.zzcbz, zzczw.zzgml.zzcca, zzczw.zzgml.zzccb, zzczw.zzgml.zzabo, zzczw.zzgml.zzbkh || zzczw.zzbkh, zzczw.zzgml.zzccc, zzczw.zzgml.zzccd, zzczw.zzgml.zzmi, zzczw.zzgml.zzcce, zzczw.zzgml.zzccf, zzczw.zzgml.zzccg, zzczw.zzgml.zzcch, zzczw.zzgml.zzcci, zzczw.zzgml.zzccj, zzczw.zzgml.zzcck, zzczw.zzgml.zzccm, zzczw.zzgml.zzabp, zzczw.zzgml.zzabq, zzczw.zzgml.zzccl);
        zzaby zzaby = null;
        if (zzczw.zzgmk != null) {
            zzyw = zzczw.zzgmk;
        } else {
            zzyw = zzczw.zzddz != null ? zzczw.zzddz.zzcvp : null;
        }
        this.zzgmk = zzyw;
        this.zzgmn = zzczw.zzgmn;
        this.zzgmo = zzczw.zzgmo;
        if (zzczw.zzgmn != null) {
            if (zzczw.zzddz == null) {
                zzaby = new zzaby(new NativeAdOptions.Builder().build());
            } else {
                zzaby = zzczw.zzddz;
            }
        }
        this.zzddz = zzaby;
        this.zzgmp = zzczw.zzgmp;
        this.zzgdu = zzczw.zzgdu;
        this.zzgmq = zzczw.zzgmq;
        this.zzgmr = zzczw.zzgmr;
        this.zzdkf = zzczw.zzdkf;
        this.zzgms = zzczw.zzgms;
    }

    public final zzaea zzaoo() {
        PublisherAdViewOptions publisherAdViewOptions = this.zzgmq;
        if (publisherAdViewOptions == null) {
            return null;
        }
        return publisherAdViewOptions.zzjn();
    }
}
