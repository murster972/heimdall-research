package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzboa implements zzded {
    private final Context zzcri;
    private final zzazb zzfek;
    private final zzczu zzfhd;

    zzboa(Context context, zzazb zzazb, zzczu zzczu) {
        this.zzcri = context;
        this.zzfek = zzazb;
        this.zzfhd = zzczu;
    }

    public final Object apply(Object obj) {
        Context context = this.zzcri;
        zzazb zzazb = this.zzfek;
        zzczu zzczu = this.zzfhd;
        zzczl zzczl = (zzczl) obj;
        zzawt zzawt = new zzawt(context);
        zzawt.zzeo(zzczl.zzdkp);
        zzawt.zzep(zzczl.zzglt.toString());
        zzawt.zzx(zzazb.zzbma);
        zzawt.setAdUnitId(zzczu.zzgmm);
        return zzawt;
    }
}
