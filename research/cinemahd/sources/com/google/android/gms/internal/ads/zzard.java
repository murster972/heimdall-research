package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import org.json.JSONObject;

public final class zzard implements zzaqo {
    private zzaju<JSONObject, JSONObject> zzdnk;
    private zzaju<JSONObject, JSONObject> zzdnp;

    public zzard(Context context) {
        zzakc zza = zzq.zzld().zza(context, zzazb.zzxm());
        zzajy<JSONObject> zzajy = zzajx.zzdaq;
        this.zzdnp = zza.zza("google.afma.request.getAdDictionary", zzajy, zzajy);
        zzakc zza2 = zzq.zzld().zza(context, zzazb.zzxm());
        zzajy<JSONObject> zzajy2 = zzajx.zzdaq;
        this.zzdnk = zza2.zza("google.afma.sdkConstants.getSdkConstants", zzajy2, zzajy2);
    }

    public final zzaju<JSONObject, JSONObject> zztz() {
        return this.zzdnk;
    }
}
