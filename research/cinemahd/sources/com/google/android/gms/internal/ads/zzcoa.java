package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcoa implements zzdxg<zzcnw> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbup> zzfyl;

    public zzcoa(zzdxp<Context> zzdxp, zzdxp<zzbup> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfyl = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcnw(this.zzejv.get(), this.zzfyl.get());
    }
}
