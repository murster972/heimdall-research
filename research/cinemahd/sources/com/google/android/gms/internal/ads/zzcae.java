package com.google.android.gms.internal.ads;

public final class zzcae implements zzdxg<zzcab> {
    private final zzdxp<String> zzfhl;
    private final zzdxp<zzbws> zzfkx;
    private final zzdxp<zzbwk> zzfqc;

    private zzcae(zzdxp<String> zzdxp, zzdxp<zzbwk> zzdxp2, zzdxp<zzbws> zzdxp3) {
        this.zzfhl = zzdxp;
        this.zzfqc = zzdxp2;
        this.zzfkx = zzdxp3;
    }

    public static zzcae zzj(zzdxp<String> zzdxp, zzdxp<zzbwk> zzdxp2, zzdxp<zzbws> zzdxp3) {
        return new zzcae(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzcab(this.zzfhl.get(), this.zzfqc.get(), this.zzfkx.get());
    }
}
