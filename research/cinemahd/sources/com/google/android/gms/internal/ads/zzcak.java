package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.facebook.common.util.UriUtil;
import java.util.Map;

public final class zzcak implements zzafn<Object> {
    private final zzdxa<zzcad> zzfln;
    private final zzcaj zzfnx;
    private final zzado zzfqi;

    public zzcak(zzbwz zzbwz, zzbws zzbws, zzcaj zzcaj, zzdxa<zzcad> zzdxa) {
        this.zzfqi = zzbwz.zzga(zzbws.getCustomTemplateId());
        this.zzfnx = zzcaj;
        this.zzfln = zzdxa;
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get(UriUtil.LOCAL_ASSET_SCHEME);
        try {
            this.zzfqi.zza(this.zzfln.get(), str);
        } catch (RemoteException e) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40);
            sb.append("Failed to call onCustomClick for asset ");
            sb.append(str);
            sb.append(".");
            zzayu.zzd(sb.toString(), e);
        }
    }

    public final void zzakr() {
        if (this.zzfqi != null) {
            this.zzfnx.zza("/nativeAdCustomClick", (zzafn<Object>) this);
        }
    }
}
