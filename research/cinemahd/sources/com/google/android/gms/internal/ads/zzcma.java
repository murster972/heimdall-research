package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzcma implements zzcir<zzcbb, zzani, zzcjy> {
    private final zzcbi zzgal;
    private final Context zzup;

    public zzcma(Context context, zzcbi zzcbi) {
        this.zzup = context;
        this.zzgal = zzcbi;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzani, zzcjy> zzcip) throws zzdab {
        try {
            ((zzani) zzcip.zzddn).zzdm(zzczl.zzdem);
            ((zzani) zzcip.zzddn).zza(zzczl.zzeif, zzczl.zzglr.toString(), zzczt.zzgmh.zzfgl.zzgml, ObjectWrapper.a(this.zzup), (zzand) new zzcmb(this, zzcip), (zzali) zzcip.zzfyf);
        } catch (RemoteException e) {
            zzdpt.zzl(e);
        }
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzciq zzciq = new zzciq(zzczl);
        zzcbd zza = this.zzgal.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzcbg(new zzclz(zzcip, zzciq)));
        zzciq.zza(zza.zzadk());
        ((zzcjy) zzcip.zzfyf).zza((zzali) zza.zzaew());
        return zza.zzaeu();
    }
}
