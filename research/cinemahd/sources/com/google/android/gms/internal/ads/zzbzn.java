package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbzn implements zzbeu {
    private final zzbzh zzfpu;
    private final zzbdi zzfpv;
    private final zzazi zzfpw;

    zzbzn(zzbzh zzbzh, zzbdi zzbdi, zzazi zzazi) {
        this.zzfpu = zzbzh;
        this.zzfpv = zzbdi;
        this.zzfpw = zzazi;
    }

    public final void zzak(boolean z) {
        this.zzfpu.zzb(this.zzfpv, this.zzfpw, z);
    }
}
