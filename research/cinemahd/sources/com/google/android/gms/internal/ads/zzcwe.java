package com.google.android.gms.internal.ads;

import java.util.HashSet;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzcwe implements zzdxg<zzcua<JSONObject>> {
    public static zzcua<JSONObject> zza(Object obj, zzcuv zzcuv, zzcvm zzcvm, zzdxa<zzcup> zzdxa, zzdxa<zzcuz> zzdxa2, zzdxa<zzcvd> zzdxa3, zzdxa<zzcvi> zzdxa4, zzdxa<zzcvr> zzdxa5, zzdxa<zzcvv> zzdxa6, zzdxa<zzcwg> zzdxa7, Executor executor) {
        HashSet hashSet = new HashSet();
        hashSet.add((zzcve) obj);
        hashSet.add(zzcuv);
        hashSet.add(zzcvm);
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpf)).booleanValue()) {
            hashSet.add(zzdxa.get());
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpg)).booleanValue()) {
            hashSet.add(zzdxa2.get());
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcph)).booleanValue()) {
            hashSet.add(zzdxa3.get());
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpi)).booleanValue()) {
            hashSet.add(zzdxa4.get());
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpl)).booleanValue()) {
            hashSet.add(zzdxa5.get());
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpn)).booleanValue()) {
            hashSet.add(zzdxa6.get());
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpo)).booleanValue()) {
            hashSet.add(zzdxa7.get());
        }
        return (zzcua) zzdxm.zza(new zzcua(executor, hashSet), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
