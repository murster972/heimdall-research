package com.google.android.gms.internal.ads;

import android.app.Activity;

public interface zzqu {
    void onActivityPaused(Activity activity);

    void onActivityResumed(Activity activity);

    boolean zza(Activity activity);
}
