package com.google.android.gms.internal.ads;

import java.util.Comparator;

final class zzqq implements Comparator<zzqw> {
    zzqq(zzqr zzqr) {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzqw zzqw = (zzqw) obj;
        zzqw zzqw2 = (zzqw) obj2;
        int i = zzqw.zzbqg - zzqw2.zzbqg;
        if (i != 0) {
            return i;
        }
        return (int) (zzqw.value - zzqw2.value);
    }
}
