package com.google.android.gms.internal.cast;

import android.annotation.TargetApi;
import android.hardware.display.VirtualDisplay;
import com.google.android.gms.cast.CastRemoteDisplay$CastRemoteDisplaySessionResult;
import com.google.android.gms.cast.CastRemoteDisplayApi;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;

@Deprecated
public final class zzeh implements CastRemoteDisplayApi {
    /* access modifiers changed from: private */
    public static final zzdw zzbf = new zzdw("CastRemoteDisplayApiImpl");
    /* access modifiers changed from: private */
    public Api<?> zzaax;
    /* access modifiers changed from: private */
    public final zzex zzaay = new zzei(this);
    /* access modifiers changed from: private */
    public VirtualDisplay zzbg;

    public zzeh(Api api) {
        this.zzaax = api;
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    public final void zzc() {
        VirtualDisplay virtualDisplay = this.zzbg;
        if (virtualDisplay != null) {
            if (virtualDisplay.getDisplay() != null) {
                zzdw zzdw = zzbf;
                int displayId = this.zzbg.getDisplay().getDisplayId();
                StringBuilder sb = new StringBuilder(38);
                sb.append("releasing virtual display: ");
                sb.append(displayId);
                zzdw.d(sb.toString(), new Object[0]);
            }
            this.zzbg.release();
            this.zzbg = null;
        }
    }

    public final PendingResult<CastRemoteDisplay$CastRemoteDisplaySessionResult> startRemoteDisplay(GoogleApiClient googleApiClient, String str) {
        zzbf.d("startRemoteDisplay", new Object[0]);
        return googleApiClient.a(new zzej(this, googleApiClient, str));
    }

    public final PendingResult<CastRemoteDisplay$CastRemoteDisplaySessionResult> stopRemoteDisplay(GoogleApiClient googleApiClient) {
        zzbf.d("stopRemoteDisplay", new Object[0]);
        return googleApiClient.a(new zzek(this, googleApiClient));
    }
}
