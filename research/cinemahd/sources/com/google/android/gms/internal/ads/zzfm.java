package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public final class zzfm extends zzfw {
    private List<Long> zzzy = null;

    public zzfm(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 31);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        this.zzzt.zzax(-1);
        this.zzzt.zzay(-1);
        if (this.zzzy == null) {
            this.zzzy = (List) this.zzaae.invoke((Object) null, new Object[]{this.zzuv.getContext()});
        }
        List<Long> list = this.zzzy;
        if (list != null && list.size() == 2) {
            synchronized (this.zzzt) {
                this.zzzt.zzax(this.zzzy.get(0).longValue());
                this.zzzt.zzay(this.zzzy.get(1).longValue());
            }
        }
    }
}
