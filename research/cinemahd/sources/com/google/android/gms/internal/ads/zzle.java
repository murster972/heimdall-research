package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.List;

public final class zzle implements Parcelable {
    public static final Parcelable.Creator<zzle> CREATOR = new zzlg();
    private final zza[] zzazn;

    public interface zza extends Parcelable {
    }

    public zzle(List<? extends zza> list) {
        this.zzazn = new zza[list.size()];
        list.toArray(this.zzazn);
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || zzle.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.zzazn, ((zzle) obj).zzazn);
    }

    public final int hashCode() {
        return Arrays.hashCode(this.zzazn);
    }

    public final int length() {
        return this.zzazn.length;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.zzazn.length);
        for (zza writeParcelable : this.zzazn) {
            parcel.writeParcelable(writeParcelable, 0);
        }
    }

    public final zza zzas(int i) {
        return this.zzazn[i];
    }

    zzle(Parcel parcel) {
        this.zzazn = new zza[parcel.readInt()];
        int i = 0;
        while (true) {
            zza[] zzaArr = this.zzazn;
            if (i < zzaArr.length) {
                zzaArr[i] = (zza) parcel.readParcelable(zza.class.getClassLoader());
                i++;
            } else {
                return;
            }
        }
    }
}
