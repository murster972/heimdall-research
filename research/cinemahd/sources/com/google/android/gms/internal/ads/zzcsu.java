package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.Set;

public final class zzcsu implements zzdxg<zzcss> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdhd> zzgem;
    private final zzdxp<Set<String>> zzgew;

    private zzcsu(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2, zzdxp<Set<String>> zzdxp3) {
        this.zzgem = zzdxp;
        this.zzejv = zzdxp2;
        this.zzgew = zzdxp3;
    }

    public static zzcsu zzo(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2, zzdxp<Set<String>> zzdxp3) {
        return new zzcsu(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzcss(this.zzgem.get(), this.zzejv.get(), this.zzgew.get());
    }
}
