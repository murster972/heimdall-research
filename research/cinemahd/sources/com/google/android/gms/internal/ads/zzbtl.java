package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;

final /* synthetic */ class zzbtl implements zzbrn {
    static final zzbrn zzfhp = new zzbtl();

    private zzbtl() {
    }

    public final void zzp(Object obj) {
        ((VideoController.VideoLifecycleCallbacks) obj).onVideoEnd();
    }
}
