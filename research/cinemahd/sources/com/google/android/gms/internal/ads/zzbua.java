package com.google.android.gms.internal.ads;

import android.view.View;

public final class zzbua implements zzdxg<View> {
    private final zzbtv zzfje;

    private zzbua(zzbtv zzbtv) {
        this.zzfje = zzbtv;
    }

    public static zzbua zza(zzbtv zzbtv) {
        return new zzbua(zzbtv);
    }

    public final /* synthetic */ Object get() {
        return this.zzfje.zzaig();
    }
}
