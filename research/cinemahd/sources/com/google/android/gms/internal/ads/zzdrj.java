package com.google.android.gms.internal.ads;

final class zzdrj {
    private static final zzdri<?> zzhjm = new zzdrk();
    private static final zzdri<?> zzhjn = zzazj();

    private static zzdri<?> zzazj() {
        try {
            return (zzdri) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    static zzdri<?> zzazk() {
        return zzhjm;
    }

    static zzdri<?> zzazl() {
        zzdri<?> zzdri = zzhjn;
        if (zzdri != null) {
            return zzdri;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
