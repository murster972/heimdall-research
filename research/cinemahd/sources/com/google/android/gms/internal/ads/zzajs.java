package com.google.android.gms.internal.ads;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public final class zzajs implements zzaia, zzajp {
    private final zzajq zzdak;
    private final HashSet<AbstractMap.SimpleEntry<String, zzafn<? super zzajq>>> zzdal = new HashSet<>();

    public zzajs(zzajq zzajq) {
        this.zzdak = zzajq;
    }

    public final void zza(String str, zzafn<? super zzajq> zzafn) {
        this.zzdak.zza(str, zzafn);
        this.zzdal.add(new AbstractMap.SimpleEntry(str, zzafn));
    }

    public final void zza(String str, Map map) {
        zzahz.zza((zzaia) this, str, map);
    }

    public final void zza(String str, JSONObject jSONObject) {
        zzahz.zza((zzaia) this, str, jSONObject);
    }

    public final void zzb(String str, zzafn<? super zzajq> zzafn) {
        this.zzdak.zzb(str, zzafn);
        this.zzdal.remove(new AbstractMap.SimpleEntry(str, zzafn));
    }

    public final void zzb(String str, JSONObject jSONObject) {
        zzahz.zzb(this, str, jSONObject);
    }

    public final void zzcy(String str) {
        this.zzdak.zzcy(str);
    }

    public final void zzj(String str, String str2) {
        zzahz.zza((zzaia) this, str, str2);
    }

    public final void zzsg() {
        Iterator<AbstractMap.SimpleEntry<String, zzafn<? super zzajq>>> it2 = this.zzdal.iterator();
        while (it2.hasNext()) {
            AbstractMap.SimpleEntry next = it2.next();
            String valueOf = String.valueOf(((zzafn) next.getValue()).toString());
            zzavs.zzed(valueOf.length() != 0 ? "Unregistering eventhandler: ".concat(valueOf) : new String("Unregistering eventhandler: "));
            this.zzdak.zzb((String) next.getKey(), (zzafn) next.getValue());
        }
        this.zzdal.clear();
    }
}
