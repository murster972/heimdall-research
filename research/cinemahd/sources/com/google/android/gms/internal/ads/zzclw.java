package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

public final class zzclw<AdT> implements zzdxg<zzclu<AdT>> {
    public static <AdT> zzclu<AdT> zza(zzdcr zzdcr, zzclp zzclp, zzbou zzbou, zzdda zzdda, zzbmi<AdT> zzbmi, Executor executor, ScheduledExecutorService scheduledExecutorService) {
        return new zzclu(zzdcr, zzclp, zzbou, zzdda, zzbmi, executor, scheduledExecutorService);
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
