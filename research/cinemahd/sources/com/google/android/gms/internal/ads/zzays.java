package com.google.android.gms.internal.ads;

import android.util.JsonWriter;

final /* synthetic */ class zzays implements zzayv {
    private final String zzcyz;

    zzays(String str) {
        this.zzcyz = str;
    }

    public final void zzb(JsonWriter jsonWriter) {
        zzayo.zza(this.zzcyz, jsonWriter);
    }
}
