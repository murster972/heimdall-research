package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;

final /* synthetic */ class zzbtq implements zzbrn {
    static final zzbrn zzfhp = new zzbtq();

    private zzbtq() {
    }

    public final void zzp(Object obj) {
        ((VideoController.VideoLifecycleCallbacks) obj).onVideoPlay();
    }
}
