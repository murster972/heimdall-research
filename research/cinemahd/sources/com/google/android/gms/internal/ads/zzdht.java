package com.google.android.gms.internal.ads;

public final class zzdht extends RuntimeException {
    protected zzdht() {
    }

    public zzdht(Throwable th) {
        super(th);
    }
}
