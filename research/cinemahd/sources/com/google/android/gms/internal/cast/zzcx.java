package com.google.android.gms.internal.cast;

import android.content.Context;
import com.google.android.gms.cast.zzbx;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.tasks.Task;
import java.util.List;

public final class zzcx extends GoogleApi<Object> {
    private static final Api<Object> API = new Api<>("CastApi.API", zzad, CLIENT_KEY);
    private static final Api.ClientKey<zzdb> CLIENT_KEY = new Api.ClientKey<>();
    private static final Api.AbstractClientBuilder<zzdb, Object> zzad = new zzcy();

    public zzcx(Context context) {
        super(context, API, null, GoogleApi.Settings.c);
    }

    public final Task<Void> zza(String[] strArr, String str, List<zzbx> list) {
        return doWrite(new zzcz(this, strArr, str, (List) null));
    }
}
