package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdjd extends zzdih<zzdlg, zzdlf> {
    private final /* synthetic */ zzdjc zzgzc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdjd(zzdjc zzdjc, Class cls) {
        super(cls);
        this.zzgzc = zzdjc;
    }

    public final /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdlg zzdlg = (zzdlg) zzdte;
        new zzdjg().zzasg().zzc(zzdlg.zzate());
        new zzdkq().zzasg().zzc(zzdlg.zzatf());
        zzdpo.zzez(zzdlg.zzate().getKeySize());
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdlg zzdlg = (zzdlg) zzdte;
        return (zzdlf) zzdlf.zzatc().zzb(new zzdjg().zzasg().zzd(zzdlg.zzate())).zzb(new zzdkq().zzasg().zzd(zzdlg.zzatf())).zzed(0).zzbaf();
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdlg.zzx(zzdqk);
    }
}
