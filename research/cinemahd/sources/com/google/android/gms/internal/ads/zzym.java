package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final /* synthetic */ class zzym implements Runnable {
    private final zzaso zzcfp;

    zzym(zzaso zzaso) {
        this.zzcfp = zzaso;
    }

    public final void run() {
        zzaso zzaso = this.zzcfp;
        if (zzaso != null) {
            try {
                zzaso.onRewardedAdFailedToLoad(1);
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }
}
