package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcjh implements zzdxg<zzcjf> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzblg> zzfyl;

    public zzcjh(zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzblg> zzdxp3, zzdxp<Executor> zzdxp4) {
        this.zzejv = zzdxp;
        this.zzfdb = zzdxp2;
        this.zzfyl = zzdxp3;
        this.zzfei = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcjf(this.zzejv.get(), this.zzfdb.get(), this.zzfyl.get(), this.zzfei.get());
    }
}
