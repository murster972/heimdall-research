package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbal implements Runnable {
    private final zzbag zzdyn;

    private zzbal(zzbag zzbag) {
        this.zzdyn = zzbag;
    }

    static Runnable zza(zzbag zzbag) {
        return new zzbal(zzbag);
    }

    public final void run() {
        this.zzdyn.stop();
    }
}
