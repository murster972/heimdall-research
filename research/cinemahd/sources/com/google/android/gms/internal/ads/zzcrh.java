package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcrh implements zzdxg<zzcrf> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdhd> zzfcv;

    private zzcrh(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzejv = zzdxp2;
    }

    public static zzcrh zzan(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2) {
        return new zzcrh(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcrf(this.zzfcv.get(), this.zzejv.get());
    }
}
