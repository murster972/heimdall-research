package com.google.android.gms.internal.ads;

import com.facebook.common.time.Clock;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public final class zzko implements zzjd, zzjm {
    private static final zzji zzanm = new zzkn();
    private static final int zzawr = zzoq.zzbn("qt  ");
    private long zzagj;
    private final zzoj zzant = new zzoj(zzoi.zzbga);
    private final zzoj zzanu = new zzoj(4);
    private int zzapi;
    private int zzapj;
    private zzjf zzapm;
    private final zzoj zzaws = new zzoj(16);
    private final Stack<zzkc> zzawt = new Stack<>();
    private int zzawu;
    private int zzawv;
    private long zzaww;
    private int zzawx;
    private zzoj zzawy;
    private zzkq[] zzawz;
    private boolean zzaxa;

    private final void zzeb(long j) throws zzhd {
        zzle zzle;
        zzjh zzjh;
        zzks zza;
        while (!this.zzawt.isEmpty() && this.zzawt.peek().zzaum == j) {
            zzkc pop = this.zzawt.pop();
            if (pop.type == zzjz.zzarw) {
                ArrayList arrayList = new ArrayList();
                zzle zzle2 = null;
                zzjh zzjh2 = new zzjh();
                zzkb zzao = pop.zzao(zzjz.zzatv);
                if (!(zzao == null || (zzle2 = zzke.zza(zzao, this.zzaxa)) == null)) {
                    zzjh2.zzb(zzle2);
                }
                long j2 = Long.MAX_VALUE;
                long j3 = -9223372036854775807L;
                int i = 0;
                while (i < pop.zzauo.size()) {
                    zzkc zzkc = pop.zzauo.get(i);
                    if (zzkc.type == zzjz.zzary && (zza = zzke.zza(zzkc, pop.zzao(zzjz.zzarx), -9223372036854775807L, (zziv) null, this.zzaxa)) != null) {
                        zzku zza2 = zzke.zza(zza, zzkc.zzap(zzjz.zzarz).zzap(zzjz.zzasa).zzap(zzjz.zzasb), zzjh2);
                        if (zza2.zzavf != 0) {
                            zzkq zzkq = new zzkq(zza, zza2, this.zzapm.zzc(i, zza.type));
                            zzgw zzp = zza.zzafz.zzp(zza2.zzawo + 30);
                            if (zza.type == 1) {
                                if (zzjh2.zzgk()) {
                                    zzp = zzp.zzb(zzjh2.zzafr, zzjh2.zzafs);
                                }
                                if (zzle2 != null) {
                                    zzp = zzp.zza(zzle2);
                                }
                            }
                            zzkq.zzaxe.zze(zzp);
                            zzle = zzle2;
                            zzjh = zzjh2;
                            j3 = Math.max(j3, zza.zzagj);
                            arrayList.add(zzkq);
                            long j4 = zza2.zzamq[0];
                            if (j4 < j2) {
                                j2 = j4;
                            }
                            i++;
                            zzjh2 = zzjh;
                            zzle2 = zzle;
                        }
                    }
                    zzle = zzle2;
                    zzjh = zzjh2;
                    i++;
                    zzjh2 = zzjh;
                    zzle2 = zzle;
                }
                this.zzagj = j3;
                this.zzawz = (zzkq[]) arrayList.toArray(new zzkq[arrayList.size()]);
                this.zzapm.zzgj();
                this.zzapm.zza(this);
                this.zzawt.clear();
                this.zzawu = 2;
            } else if (!this.zzawt.isEmpty()) {
                this.zzawt.peek().zzauo.add(pop);
            }
        }
        if (this.zzawu != 2) {
            zzgt();
        }
    }

    private final void zzgt() {
        this.zzawu = 0;
        this.zzawx = 0;
    }

    public final long getDurationUs() {
        return this.zzagj;
    }

    public final void release() {
    }

    public final boolean zza(zzjg zzjg) throws IOException, InterruptedException {
        return zzkp.zzd(zzjg);
    }

    public final void zzc(long j, long j2) {
        this.zzawt.clear();
        this.zzawx = 0;
        this.zzapj = 0;
        this.zzapi = 0;
        if (j == 0) {
            zzgt();
            return;
        }
        zzkq[] zzkqArr = this.zzawz;
        if (zzkqArr != null) {
            for (zzkq zzkq : zzkqArr) {
                zzku zzku = zzkq.zzaxd;
                int zzec = zzku.zzec(j2);
                if (zzec == -1) {
                    zzec = zzku.zzed(j2);
                }
                zzkq.zzavh = zzec;
            }
        }
    }

    public final long zzdz(long j) {
        long j2 = Clock.MAX_TIME;
        for (zzkq zzkq : this.zzawz) {
            zzku zzku = zzkq.zzaxd;
            int zzec = zzku.zzec(j);
            if (zzec == -1) {
                zzec = zzku.zzed(j);
            }
            long j3 = zzku.zzamq[zzec];
            if (j3 < j2) {
                j2 = j3;
            }
        }
        return j2;
    }

    public final boolean zzgh() {
        return true;
    }

    public final void zza(zzjf zzjf) {
        this.zzapm = zzjf;
    }

    /* JADX WARNING: Removed duplicated region for block: B:150:0x018e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x029d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0006 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(com.google.android.gms.internal.ads.zzjg r24, com.google.android.gms.internal.ads.zzjj r25) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r23 = this;
            r0 = r23
            r1 = r24
            r2 = r25
        L_0x0006:
            int r3 = r0.zzawu
            r4 = -1
            r5 = 8
            r6 = 1
            r7 = 0
            if (r3 == 0) goto L_0x018f
            r8 = 262144(0x40000, double:1.295163E-318)
            r10 = 2
            if (r3 == r6) goto L_0x010d
            if (r3 != r10) goto L_0x0107
            r12 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r3 = 0
            r5 = -1
        L_0x001e:
            com.google.android.gms.internal.ads.zzkq[] r14 = r0.zzawz
            int r15 = r14.length
            if (r3 >= r15) goto L_0x003a
            r14 = r14[r3]
            int r15 = r14.zzavh
            com.google.android.gms.internal.ads.zzku r14 = r14.zzaxd
            int r11 = r14.zzavf
            if (r15 == r11) goto L_0x0037
            long[] r11 = r14.zzamq
            r14 = r11[r15]
            int r11 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r11 >= 0) goto L_0x0037
            r5 = r3
            r12 = r14
        L_0x0037:
            int r3 = r3 + 1
            goto L_0x001e
        L_0x003a:
            if (r5 != r4) goto L_0x003d
            return r4
        L_0x003d:
            r3 = r14[r5]
            com.google.android.gms.internal.ads.zzjo r4 = r3.zzaxe
            int r5 = r3.zzavh
            com.google.android.gms.internal.ads.zzku r11 = r3.zzaxd
            long[] r12 = r11.zzamq
            r13 = r12[r5]
            int[] r11 = r11.zzamp
            r11 = r11[r5]
            com.google.android.gms.internal.ads.zzks r12 = r3.zzaxc
            int r12 = r12.zzaxj
            if (r12 != r6) goto L_0x0059
            r16 = 8
            long r13 = r13 + r16
            int r11 = r11 + -8
        L_0x0059:
            long r16 = r24.getPosition()
            long r16 = r13 - r16
            int r12 = r0.zzapj
            r18 = r11
            long r10 = (long) r12
            long r10 = r16 + r10
            r16 = 0
            int r12 = (r10 > r16 ? 1 : (r10 == r16 ? 0 : -1))
            if (r12 < 0) goto L_0x0104
            int r12 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r12 < 0) goto L_0x0072
            goto L_0x0104
        L_0x0072:
            int r2 = (int) r10
            r1.zzac(r2)
            com.google.android.gms.internal.ads.zzks r2 = r3.zzaxc
            int r2 = r2.zzaqu
            if (r2 == 0) goto L_0x00cc
            com.google.android.gms.internal.ads.zzoj r8 = r0.zzanu
            byte[] r8 = r8.data
            r8[r7] = r7
            r8[r6] = r7
            r9 = 2
            r8[r9] = r7
            r8 = 4
            int r11 = 4 - r2
            r8 = r18
        L_0x008c:
            int r9 = r0.zzapj
            if (r9 >= r8) goto L_0x00c9
            int r9 = r0.zzapi
            if (r9 != 0) goto L_0x00ba
            com.google.android.gms.internal.ads.zzoj r9 = r0.zzanu
            byte[] r9 = r9.data
            r1.readFully(r9, r11, r2)
            com.google.android.gms.internal.ads.zzoj r9 = r0.zzanu
            r9.zzbe(r7)
            com.google.android.gms.internal.ads.zzoj r9 = r0.zzanu
            int r9 = r9.zzis()
            r0.zzapi = r9
            com.google.android.gms.internal.ads.zzoj r9 = r0.zzant
            r9.zzbe(r7)
            com.google.android.gms.internal.ads.zzoj r9 = r0.zzant
            r10 = 4
            r4.zza(r9, r10)
            int r9 = r0.zzapj
            int r9 = r9 + r10
            r0.zzapj = r9
            int r8 = r8 + r11
            goto L_0x008c
        L_0x00ba:
            int r9 = r4.zza(r1, r9, r7)
            int r10 = r0.zzapj
            int r10 = r10 + r9
            r0.zzapj = r10
            int r10 = r0.zzapi
            int r10 = r10 - r9
            r0.zzapi = r10
            goto L_0x008c
        L_0x00c9:
            r20 = r8
            goto L_0x00e7
        L_0x00cc:
            int r2 = r0.zzapj
            r11 = r18
            if (r2 >= r11) goto L_0x00e5
            int r2 = r11 - r2
            int r2 = r4.zza(r1, r2, r7)
            int r8 = r0.zzapj
            int r8 = r8 + r2
            r0.zzapj = r8
            int r8 = r0.zzapi
            int r8 = r8 - r2
            r0.zzapi = r8
            r18 = r11
            goto L_0x00cc
        L_0x00e5:
            r20 = r11
        L_0x00e7:
            com.google.android.gms.internal.ads.zzku r1 = r3.zzaxd
            long[] r2 = r1.zzaxq
            r17 = r2[r5]
            int[] r1 = r1.zzawq
            r19 = r1[r5]
            r21 = 0
            r22 = 0
            r16 = r4
            r16.zza(r17, r19, r20, r21, r22)
            int r1 = r3.zzavh
            int r1 = r1 + r6
            r3.zzavh = r1
            r0.zzapj = r7
            r0.zzapi = r7
            return r7
        L_0x0104:
            r2.zzamw = r13
            return r6
        L_0x0107:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            r1.<init>()
            throw r1
        L_0x010d:
            long r3 = r0.zzaww
            int r10 = r0.zzawx
            long r10 = (long) r10
            long r3 = r3 - r10
            long r10 = r24.getPosition()
            long r10 = r10 + r3
            com.google.android.gms.internal.ads.zzoj r12 = r0.zzawy
            if (r12 == 0) goto L_0x016f
            byte[] r8 = r12.data
            int r9 = r0.zzawx
            int r4 = (int) r3
            r1.readFully(r8, r9, r4)
            int r3 = r0.zzawv
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaqv
            if (r3 != r4) goto L_0x0150
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzawy
            r3.zzbe(r5)
            int r4 = r3.readInt()
            int r5 = zzawr
            if (r4 != r5) goto L_0x0139
        L_0x0137:
            r3 = 1
            goto L_0x014d
        L_0x0139:
            r4 = 4
            r3.zzbf(r4)
        L_0x013d:
            int r4 = r3.zzin()
            if (r4 <= 0) goto L_0x014c
            int r4 = r3.readInt()
            int r5 = zzawr
            if (r4 != r5) goto L_0x013d
            goto L_0x0137
        L_0x014c:
            r3 = 0
        L_0x014d:
            r0.zzaxa = r3
            goto L_0x0177
        L_0x0150:
            java.util.Stack<com.google.android.gms.internal.ads.zzkc> r3 = r0.zzawt
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x0177
            java.util.Stack<com.google.android.gms.internal.ads.zzkc> r3 = r0.zzawt
            java.lang.Object r3 = r3.peek()
            com.google.android.gms.internal.ads.zzkc r3 = (com.google.android.gms.internal.ads.zzkc) r3
            com.google.android.gms.internal.ads.zzkb r4 = new com.google.android.gms.internal.ads.zzkb
            int r5 = r0.zzawv
            com.google.android.gms.internal.ads.zzoj r8 = r0.zzawy
            r4.<init>(r5, r8)
            java.util.List<com.google.android.gms.internal.ads.zzkb> r3 = r3.zzaun
            r3.add(r4)
            goto L_0x0177
        L_0x016f:
            int r5 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r5 >= 0) goto L_0x0179
            int r4 = (int) r3
            r1.zzac(r4)
        L_0x0177:
            r3 = 0
            goto L_0x0181
        L_0x0179:
            long r8 = r24.getPosition()
            long r8 = r8 + r3
            r2.zzamw = r8
            r3 = 1
        L_0x0181:
            r0.zzeb(r10)
            if (r3 == 0) goto L_0x018c
            int r3 = r0.zzawu
            r4 = 2
            if (r3 == r4) goto L_0x018c
            r7 = 1
        L_0x018c:
            if (r7 == 0) goto L_0x0006
            return r6
        L_0x018f:
            int r3 = r0.zzawx
            if (r3 != 0) goto L_0x01b7
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            byte[] r3 = r3.data
            boolean r3 = r1.zza(r3, r7, r5, r6)
            if (r3 != 0) goto L_0x01a0
            r6 = 0
            goto L_0x029b
        L_0x01a0:
            r0.zzawx = r5
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            r3.zzbe(r7)
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            long r8 = r3.zzip()
            r0.zzaww = r8
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            int r3 = r3.readInt()
            r0.zzawv = r3
        L_0x01b7:
            long r8 = r0.zzaww
            r10 = 1
            int r3 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r3 != 0) goto L_0x01d3
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            byte[] r3 = r3.data
            r1.readFully(r3, r5, r5)
            int r3 = r0.zzawx
            int r3 = r3 + r5
            r0.zzawx = r3
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            long r8 = r3.zzit()
            r0.zzaww = r8
        L_0x01d3:
            int r3 = r0.zzawv
            int r8 = com.google.android.gms.internal.ads.zzjz.zzarw
            if (r3 == r8) goto L_0x01f0
            int r8 = com.google.android.gms.internal.ads.zzjz.zzary
            if (r3 == r8) goto L_0x01f0
            int r8 = com.google.android.gms.internal.ads.zzjz.zzarz
            if (r3 == r8) goto L_0x01f0
            int r8 = com.google.android.gms.internal.ads.zzjz.zzasa
            if (r3 == r8) goto L_0x01f0
            int r8 = com.google.android.gms.internal.ads.zzjz.zzasb
            if (r3 == r8) goto L_0x01f0
            int r8 = com.google.android.gms.internal.ads.zzjz.zzask
            if (r3 != r8) goto L_0x01ee
            goto L_0x01f0
        L_0x01ee:
            r3 = 0
            goto L_0x01f1
        L_0x01f0:
            r3 = 1
        L_0x01f1:
            if (r3 == 0) goto L_0x021d
            long r7 = r24.getPosition()
            long r9 = r0.zzaww
            long r7 = r7 + r9
            int r3 = r0.zzawx
            long r9 = (long) r3
            long r7 = r7 - r9
            java.util.Stack<com.google.android.gms.internal.ads.zzkc> r3 = r0.zzawt
            com.google.android.gms.internal.ads.zzkc r5 = new com.google.android.gms.internal.ads.zzkc
            int r9 = r0.zzawv
            r5.<init>(r9, r7)
            r3.add(r5)
            long r9 = r0.zzaww
            int r3 = r0.zzawx
            long r11 = (long) r3
            int r3 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r3 != 0) goto L_0x0218
            r0.zzeb(r7)
            goto L_0x029b
        L_0x0218:
            r23.zzgt()
            goto L_0x029b
        L_0x021d:
            int r3 = r0.zzawv
            int r8 = com.google.android.gms.internal.ads.zzjz.zzasm
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzarx
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzasn
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzaso
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzath
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzati
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzatj
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzasl
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzatk
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzatl
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzatm
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzatn
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzato
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzasj
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzaqv
            if (r3 == r8) goto L_0x0262
            int r8 = com.google.android.gms.internal.ads.zzjz.zzatv
            if (r3 != r8) goto L_0x0260
            goto L_0x0262
        L_0x0260:
            r3 = 0
            goto L_0x0263
        L_0x0262:
            r3 = 1
        L_0x0263:
            if (r3 == 0) goto L_0x0296
            int r3 = r0.zzawx
            if (r3 != r5) goto L_0x026b
            r3 = 1
            goto L_0x026c
        L_0x026b:
            r3 = 0
        L_0x026c:
            com.google.android.gms.internal.ads.zzoc.checkState(r3)
            long r8 = r0.zzaww
            r10 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r3 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r3 > 0) goto L_0x027a
            r3 = 1
            goto L_0x027b
        L_0x027a:
            r3 = 0
        L_0x027b:
            com.google.android.gms.internal.ads.zzoc.checkState(r3)
            com.google.android.gms.internal.ads.zzoj r3 = new com.google.android.gms.internal.ads.zzoj
            long r8 = r0.zzaww
            int r9 = (int) r8
            r3.<init>((int) r9)
            r0.zzawy = r3
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            byte[] r3 = r3.data
            com.google.android.gms.internal.ads.zzoj r8 = r0.zzawy
            byte[] r8 = r8.data
            java.lang.System.arraycopy(r3, r7, r8, r7, r5)
            r0.zzawu = r6
            goto L_0x029b
        L_0x0296:
            r3 = 0
            r0.zzawy = r3
            r0.zzawu = r6
        L_0x029b:
            if (r6 != 0) goto L_0x0006
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzko.zza(com.google.android.gms.internal.ads.zzjg, com.google.android.gms.internal.ads.zzjj):int");
    }
}
