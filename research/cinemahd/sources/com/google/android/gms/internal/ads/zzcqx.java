package com.google.android.gms.internal.ads;

public final class zzcqx implements zzdxg<zzcqv> {
    private final zzdxp<zzbnk> zzema;
    private final zzdxp<zzdak> zzenk;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<String> zzgfg;
    private final zzdxp<String> zzgfh;

    private zzcqx(zzdxp<String> zzdxp, zzdxp<String> zzdxp2, zzdxp<zzbnk> zzdxp3, zzdxp<zzdak> zzdxp4, zzdxp<zzczu> zzdxp5) {
        this.zzgfg = zzdxp;
        this.zzgfh = zzdxp2;
        this.zzema = zzdxp3;
        this.zzenk = zzdxp4;
        this.zzfep = zzdxp5;
    }

    public static zzcqx zzf(zzdxp<String> zzdxp, zzdxp<String> zzdxp2, zzdxp<zzbnk> zzdxp3, zzdxp<zzdak> zzdxp4, zzdxp<zzczu> zzdxp5) {
        return new zzcqx(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5);
    }

    public final /* synthetic */ Object get() {
        return new zzcqv(this.zzgfg.get(), this.zzgfh.get(), this.zzema.get(), this.zzenk.get(), this.zzfep.get());
    }
}
