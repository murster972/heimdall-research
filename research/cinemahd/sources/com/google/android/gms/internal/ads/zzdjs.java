package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdjs extends zzdii<zzdno> {
    zzdjs() {
        super(zzdno.class, new zzdjr(zzdhx.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.KmsAeadKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.REMOTE;
    }

    public final zzdih<zzdnp, zzdno> zzasg() {
        return new zzdju(this, zzdnp.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdpo.zzx(((zzdno) zzdte).getVersion(), 0);
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdno.zzax(zzdqk);
    }
}
