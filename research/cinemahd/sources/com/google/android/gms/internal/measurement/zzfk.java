package com.google.android.gms.internal.measurement;

import java.util.List;
import java.util.RandomAccess;

public interface zzfk<E> extends List<E>, RandomAccess {
    void j_();

    zzfk<E> zza(int i);

    boolean zza();
}
