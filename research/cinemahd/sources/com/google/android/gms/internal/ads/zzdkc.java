package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdkc extends zzdiu<zzdmk, zzdmn> {
    zzdkc() {
        super(zzdmk.class, zzdmn.class, new zzdkb(zzdib.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.ASYMMETRIC_PRIVATE;
    }

    public final zzdih<zzdmg, zzdmk> zzasg() {
        return new zzdke(this, zzdmg.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdmk zzdmk = (zzdmk) zzdte;
        if (!zzdmk.zzass().isEmpty()) {
            zzdpo.zzx(zzdmk.getVersion(), 0);
            zzdkk.zza(zzdmk.zzaum().zzauf());
            return;
        }
        throw new GeneralSecurityException("invalid ECIES private key");
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdmk.zzal(zzdqk);
    }
}
