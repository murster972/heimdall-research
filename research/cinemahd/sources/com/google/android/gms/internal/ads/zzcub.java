package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzcty;

public interface zzcub<S extends zzcty<?>> {
    zzdhe<S> zzanc();
}
