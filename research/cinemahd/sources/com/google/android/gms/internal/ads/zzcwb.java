package com.google.android.gms.internal.ads;

public final class zzcwb implements zzdxg<Boolean> {
    private final zzcvw zzgih;

    public zzcwb(zzcvw zzcvw) {
        this.zzgih = zzcvw;
    }

    public final /* synthetic */ Object get() {
        return Boolean.valueOf(this.zzgih.zzanw());
    }
}
