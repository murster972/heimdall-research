package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final class zzbcl implements Runnable {
    private final /* synthetic */ zzbci zzeda;

    zzbcl(zzbci zzbci) {
        this.zzeda = zzbci;
    }

    public final void run() {
        zzq.zzlm().zzb(this.zzeda);
    }
}
