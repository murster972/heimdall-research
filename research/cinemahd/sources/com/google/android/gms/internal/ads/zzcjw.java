package com.google.android.gms.internal.ads;

final class zzcjw implements zzdgt<zzbkk> {
    private final /* synthetic */ zzcjr zzfzd;

    zzcjw(zzcjr zzcjr) {
        this.zzfzd = zzcjr;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        ((zzbkk) obj).zzagf();
    }

    public final void zzb(Throwable th) {
        this.zzfzd.zzfik.onAdFailedToLoad(zzcfb.zzd(th));
        zzdad.zzc(th, "DelayedBannerAd.onFailure");
    }
}
