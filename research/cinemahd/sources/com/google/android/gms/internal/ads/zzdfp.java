package com.google.android.gms.internal.ads;

import java.util.Iterator;

public abstract class zzdfp<E> implements Iterator<E> {
    protected zzdfp() {
    }

    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
