package com.google.android.gms.internal.cast;

import android.os.RemoteException;
import android.view.Surface;

class zzel extends zzeu {
    zzel() {
    }

    public void onDisconnected() throws RemoteException {
        throw new UnsupportedOperationException();
    }

    public void onError(int i) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    public void zza(int i, int i2, Surface surface) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    public void zzd() throws RemoteException {
        throw new UnsupportedOperationException();
    }
}
