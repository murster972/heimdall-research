package com.google.android.gms.internal.ads;

public final class zzcza implements zzdxg<zzcyz> {
    private final zzdxp<zzczs> zzezj;
    private final zzdxp<String> zzgja;
    private final zzdxp<zzcyt> zzgjb;
    private final zzdxp<zzcxz> zzgjc;

    public zzcza(zzdxp<String> zzdxp, zzdxp<zzcyt> zzdxp2, zzdxp<zzcxz> zzdxp3, zzdxp<zzczs> zzdxp4) {
        this.zzgja = zzdxp;
        this.zzgjb = zzdxp2;
        this.zzgjc = zzdxp3;
        this.zzezj = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcyz(this.zzgja.get(), this.zzgjb.get(), this.zzgjc.get(), this.zzezj.get());
    }
}
