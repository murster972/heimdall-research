package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.internal.ads.zzbs;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public final class zzfe extends zzfw {
    public zzfe(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 24);
    }

    private final void zzcq() {
        AdvertisingIdClient zzcj = this.zzuv.zzcj();
        if (zzcj != null) {
            try {
                AdvertisingIdClient.Info info2 = zzcj.getInfo();
                String zzat = zzep.zzat(info2.getId());
                if (zzat != null) {
                    synchronized (this.zzzt) {
                        this.zzzt.zzan(zzat);
                        this.zzzt.zzb(info2.isLimitAdTrackingEnabled());
                        this.zzzt.zzb(zzbs.zza.zzc.DEVICE_IDENTIFIER_ANDROID_AD_ID);
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (this.zzuv.zzcb()) {
            zzcq();
            return;
        }
        synchronized (this.zzzt) {
            this.zzzt.zzan((String) this.zzaae.invoke((Object) null, new Object[]{this.zzuv.getContext()}));
        }
    }

    /* renamed from: zzcp */
    public final Void call() throws Exception {
        if (this.zzuv.isInitialized()) {
            return super.call();
        }
        if (!this.zzuv.zzcb()) {
            return null;
        }
        zzcq();
        return null;
    }
}
