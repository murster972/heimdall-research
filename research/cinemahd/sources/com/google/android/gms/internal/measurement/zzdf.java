package com.google.android.gms.internal.measurement;

abstract class zzdf {
    private static final Throwable[] zza = new Throwable[0];

    zzdf() {
    }

    public abstract void zza(Throwable th, Throwable th2);
}
