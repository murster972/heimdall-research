package com.google.android.gms.internal.ads;

public final class zzabg {
    public static zzaan<Boolean> zzcue = zzaan.zzf("gads:rewarded_sku:enabled", true);
    private static zzaan<Boolean> zzcuf = zzaan.zzf("gads:rewarded_sku:override_test:enabled", false);
}
