package com.google.android.gms.internal.ads;

final class zzdsu implements zzdtb {
    zzdsu() {
    }

    public final boolean zze(Class<?> cls) {
        return false;
    }

    public final zzdtc zzf(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
