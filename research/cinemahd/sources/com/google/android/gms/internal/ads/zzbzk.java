package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzbzk implements zzdgf {
    private final JSONObject zzfcs;
    private final zzbzh zzfpu;

    zzbzk(zzbzh zzbzh, JSONObject jSONObject) {
        this.zzfpu = zzbzh;
        this.zzfcs = jSONObject;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfpu.zza(this.zzfcs, (zzbdi) obj);
    }
}
