package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzbzu implements zzafn {
    private final zzbzq zzfpy;

    zzbzu(zzbzq zzbzq) {
        this.zzfpy = zzbzq;
    }

    public final void zza(Object obj, Map map) {
        this.zzfpy.zze((zzbdi) obj, map);
    }
}
