package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdgy;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class zzdgs extends zzdgz {
    public static <O> zzdhe<O> zza(zzdgd<O> zzdgd, Executor executor) {
        zzdhs zzdhs = new zzdhs(zzdgd);
        executor.execute(zzdhs);
        return zzdhs;
    }

    public static <V> zzdhe<V> zzaj(V v) {
        if (v == null) {
            return zzdgy.zzgwx;
        }
        return new zzdgy(v);
    }

    public static <V, X extends Throwable> zzdhe<V> zzb(zzdhe<? extends V> zzdhe, Class<X> cls, zzdgf<? super X, ? extends V> zzdgf, Executor executor) {
        return zzdfr.zza(zzdhe, cls, zzdgf, executor);
    }

    public static <V> V zzc(Future<V> future) {
        zzdei.checkNotNull(future);
        try {
            return zzdhw.zza(future);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof Error) {
                throw new zzdgk((Error) cause);
            }
            throw new zzdht(cause);
        }
    }

    public static <V> zzdhe<List<V>> zzg(Iterable<? extends zzdhe<? extends V>> iterable) {
        return new zzdgh(zzdeu.zzf(iterable), true);
    }

    public static <V> zzdgx<V> zzh(Iterable<? extends zzdhe<? extends V>> iterable) {
        return new zzdgx<>(false, zzdeu.zzf(iterable), (zzdgv) null);
    }

    public static <V> zzdgx<V> zzi(Iterable<? extends zzdhe<? extends V>> iterable) {
        return new zzdgx<>(true, zzdeu.zzf(iterable), (zzdgv) null);
    }

    public static <V> zzdhe<V> zzk(Throwable th) {
        zzdei.checkNotNull(th);
        return new zzdgy.zza(th);
    }

    public static <I, O> zzdhe<O> zzb(zzdhe<I> zzdhe, zzdgf<? super I, ? extends O> zzdgf, Executor executor) {
        return zzdfu.zza(zzdhe, zzdgf, executor);
    }

    public static <V> zzdhe<V> zza(zzdhe<V> zzdhe, long j, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        if (zzdhe.isDone()) {
            return zzdhe;
        }
        return zzdho.zzb(zzdhe, j, timeUnit, scheduledExecutorService);
    }

    public static <I, O> zzdhe<O> zzb(zzdhe<I> zzdhe, zzded<? super I, ? extends O> zzded, Executor executor) {
        return zzdfu.zza(zzdhe, zzded, executor);
    }

    @SafeVarargs
    public static <V> zzdgx<V> zzb(zzdhe<? extends V>... zzdheArr) {
        return new zzdgx<>(true, zzdeu.zzb(zzdheArr), (zzdgv) null);
    }

    @SafeVarargs
    public static <V> zzdgx<V> zza(zzdhe<? extends V>... zzdheArr) {
        return new zzdgx<>(false, zzdeu.zzb(zzdheArr), (zzdgv) null);
    }

    public static <V> V zzb(Future<V> future) throws ExecutionException {
        if (future.isDone()) {
            return zzdhw.zza(future);
        }
        throw new IllegalStateException(zzdek.zzb("Future was expected to be done: %s", future));
    }

    public static <V> void zza(zzdhe<V> zzdhe, zzdgt<? super V> zzdgt, Executor executor) {
        zzdei.checkNotNull(zzdgt);
        zzdhe.addListener(new zzdgu(zzdhe, zzdgt), executor);
    }
}
