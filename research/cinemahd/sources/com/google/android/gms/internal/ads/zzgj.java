package com.google.android.gms.internal.ads;

import com.facebook.common.time.Clock;
import java.io.IOException;

public abstract class zzgj implements zzhe, zzhf {
    private int index;
    private int state;
    private final int zzacf;
    private zzhh zzacg;
    private zzmo zzach;
    private long zzaci;
    private boolean zzacj = true;
    private boolean zzack;

    public zzgj(int i) {
        this.zzacf = i;
    }

    public final void disable() {
        boolean z = true;
        if (this.state != 1) {
            z = false;
        }
        zzoc.checkState(z);
        this.state = 0;
        this.zzach = null;
        this.zzack = false;
        zzdx();
    }

    /* access modifiers changed from: protected */
    public final int getIndex() {
        return this.index;
    }

    public final int getState() {
        return this.state;
    }

    public final int getTrackType() {
        return this.zzacf;
    }

    /* access modifiers changed from: protected */
    public void onStarted() throws zzgl {
    }

    /* access modifiers changed from: protected */
    public void onStopped() throws zzgl {
    }

    public final void setIndex(int i) {
        this.index = i;
    }

    public final void start() throws zzgl {
        boolean z = true;
        if (this.state != 1) {
            z = false;
        }
        zzoc.checkState(z);
        this.state = 2;
        onStarted();
    }

    public final void stop() throws zzgl {
        zzoc.checkState(this.state == 2);
        this.state = 1;
        onStopped();
    }

    public void zza(int i, Object obj) throws zzgl {
    }

    /* access modifiers changed from: protected */
    public void zza(long j, boolean z) throws zzgl {
    }

    public final void zza(zzhh zzhh, zzgw[] zzgwArr, zzmo zzmo, long j, boolean z, long j2) throws zzgl {
        zzoc.checkState(this.state == 0);
        this.zzacg = zzhh;
        this.state = 1;
        zze(z);
        zza(zzgwArr, zzmo, j2);
        zza(j, z);
    }

    /* access modifiers changed from: protected */
    public void zza(zzgw[] zzgwArr, long j) throws zzgl {
    }

    public final void zzdo(long j) throws zzgl {
        this.zzack = false;
        this.zzacj = false;
        zza(j, false);
    }

    public final zzhe zzdp() {
        return this;
    }

    /* access modifiers changed from: protected */
    public final void zzdp(long j) {
        this.zzach.zzeh(j - this.zzaci);
    }

    public zzog zzdq() {
        return null;
    }

    public final zzmo zzdr() {
        return this.zzach;
    }

    public final boolean zzds() {
        return this.zzacj;
    }

    public final void zzdt() {
        this.zzack = true;
    }

    public final boolean zzdu() {
        return this.zzack;
    }

    public final void zzdv() throws IOException {
        this.zzach.zzhk();
    }

    public int zzdw() throws zzgl {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void zzdx() {
    }

    /* access modifiers changed from: protected */
    public final zzhh zzdy() {
        return this.zzacg;
    }

    /* access modifiers changed from: protected */
    public final boolean zzdz() {
        return this.zzacj ? this.zzack : this.zzach.isReady();
    }

    /* access modifiers changed from: protected */
    public void zze(boolean z) throws zzgl {
    }

    public final void zza(zzgw[] zzgwArr, zzmo zzmo, long j) throws zzgl {
        zzoc.checkState(!this.zzack);
        this.zzach = zzmo;
        this.zzacj = false;
        this.zzaci = j;
        zza(zzgwArr, j);
    }

    /* access modifiers changed from: protected */
    public final int zza(zzgy zzgy, zzis zzis, boolean z) {
        int zzb = this.zzach.zzb(zzgy, zzis, z);
        if (zzb == -4) {
            if (zzis.zzgb()) {
                this.zzacj = true;
                if (this.zzack) {
                    return -4;
                }
                return -3;
            }
            zzis.zzamd += this.zzaci;
        } else if (zzb == -5) {
            zzgw zzgw = zzgy.zzafz;
            long j = zzgw.zzaft;
            if (j != Clock.MAX_TIME) {
                zzgy.zzafz = zzgw.zzds(j + this.zzaci);
            }
        }
        return zzb;
    }
}
