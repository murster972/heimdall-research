package com.google.android.gms.internal.ads;

import android.view.ViewGroup;

final /* synthetic */ class zzbxl implements Runnable {
    private final zzbxj zzfnp;
    private final ViewGroup zzfnq;

    zzbxl(zzbxj zzbxj, ViewGroup viewGroup) {
        this.zzfnp = zzbxj;
        this.zzfnq = viewGroup;
    }

    public final void run() {
        this.zzfnp.zzb(this.zzfnq);
    }
}
