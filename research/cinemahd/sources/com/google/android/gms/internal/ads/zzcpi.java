package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbod;
import com.google.android.gms.internal.ads.zzbrm;
import com.google.android.gms.internal.ads.zzcpj;

public final class zzcpi extends zzaur {
    private zzbfx zzgea;

    public zzcpi(zzbfx zzbfx) {
        this.zzgea = zzbfx;
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzauu zzauu, zzaun zzaun) {
        String str = zzauu.zzbqz;
        String str2 = zzauu.zzblx;
        zzuj zzuj = zzauu.zzdpu;
        zzdgs.zza(this.zzgea.zzaco().zzf(new zzbod.zza().zzbz((Context) ObjectWrapper.a(iObjectWrapper)).zza(new zzczw().zzgk(str).zzg(new zzuf().zzok()).zzd(zzuj).zzaos()).zzahh()).zza(new zzcpj(new zzcpj.zza().zzgj(str2))).zzf(new zzbrm.zza().zzahw()).zzaet().zzaey(), new zzcph(this, zzaun), this.zzgea.zzaca());
    }
}
