package com.google.android.gms.internal.ads;

final class zzrs implements zzqj {
    private final /* synthetic */ zzrq zzbrh;

    zzrs(zzrq zzrq) {
        this.zzbrh = zzrq;
    }

    public final void zzp(boolean z) {
        if (z) {
            this.zzbrh.connect();
        } else {
            this.zzbrh.disconnect();
        }
    }
}
