package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzue extends RemoteCreator<zzvv> {
    public zzue() {
        super("com.google.android.gms.ads.AdManagerCreatorImpl");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object getRemoteCreator(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManagerCreator");
        if (queryLocalInterface instanceof zzvv) {
            return (zzvv) queryLocalInterface;
        }
        return new zzvy(iBinder);
    }

    public final zzvu zza(Context context, zzuj zzuj, String str, zzalc zzalc, int i) {
        try {
            IBinder zza = ((zzvv) getRemoteCreatorInstance(context)).zza(ObjectWrapper.a(context), zzuj, str, zzalc, 19649000, i);
            if (zza == null) {
                return null;
            }
            IInterface queryLocalInterface = zza.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzvu) {
                return (zzvu) queryLocalInterface;
            }
            return new zzvw(zza);
        } catch (RemoteException | RemoteCreator.RemoteCreatorException e) {
            zzayu.zzb("Could not create remote AdManager.", e);
            return null;
        }
    }
}
