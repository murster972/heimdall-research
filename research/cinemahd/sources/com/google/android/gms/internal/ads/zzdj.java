package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.ads.AdError;
import com.google.android.gms.internal.ads.zzbo;
import com.google.android.gms.internal.ads.zzbs;
import java.util.Iterator;
import java.util.LinkedList;
import okhttp3.internal.ws.WebSocketProtocol;

public abstract class zzdj implements zzdg {
    protected static volatile zzei zzuv;
    protected MotionEvent zzvm;
    protected LinkedList<MotionEvent> zzvn = new LinkedList<>();
    protected long zzvo = 0;
    protected long zzvp = 0;
    protected long zzvq = 0;
    protected long zzvr = 0;
    protected long zzvs = 0;
    protected long zzvt = 0;
    protected long zzvu = 0;
    protected double zzvv;
    private double zzvw;
    private double zzvx;
    protected float zzvy;
    protected float zzvz;
    protected float zzwa;
    protected float zzwb;
    private boolean zzwc = false;
    protected boolean zzwd = false;
    protected DisplayMetrics zzwe;

    protected zzdj(Context context) {
        try {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzclj)).booleanValue()) {
                zzck.zzbl();
            } else {
                zzeo.zzb(zzuv);
            }
            this.zzwe = context.getResources().getDisplayMetrics();
        } catch (Throwable unused) {
        }
    }

    private final void zzbv() {
        this.zzvs = 0;
        this.zzvo = 0;
        this.zzvp = 0;
        this.zzvq = 0;
        this.zzvr = 0;
        this.zzvt = 0;
        this.zzvu = 0;
        if (this.zzvn.size() > 0) {
            Iterator it2 = this.zzvn.iterator();
            while (it2.hasNext()) {
                ((MotionEvent) it2.next()).recycle();
            }
            this.zzvn.clear();
        } else {
            MotionEvent motionEvent = this.zzvm;
            if (motionEvent != null) {
                motionEvent.recycle();
            }
        }
        this.zzvm = null;
    }

    /* access modifiers changed from: protected */
    public abstract long zza(StackTraceElement[] stackTraceElementArr) throws zzeh;

    /* access modifiers changed from: protected */
    public abstract zzbs.zza.zzb zza(Context context, zzbo.zza zza);

    public String zza(Context context, View view, Activity activity) {
        return zza(context, (String) null, zzee.zzxe, view, activity, (byte[]) null);
    }

    /* access modifiers changed from: protected */
    public abstract zzbs.zza.zzb zzb(Context context, View view, Activity activity);

    /* access modifiers changed from: protected */
    public abstract zzeq zzb(MotionEvent motionEvent) throws zzeh;

    public String zzb(Context context) {
        if (zzep.isMainThread()) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcll)).booleanValue()) {
                throw new IllegalStateException("The caller must not be called from the UI thread.");
            }
        }
        return zza(context, (String) null, zzee.zzxd, (View) null, (Activity) null, (byte[]) null);
    }

    public void zzb(View view) {
    }

    /* access modifiers changed from: protected */
    public abstract zzbs.zza.zzb zzc(Context context, View view, Activity activity);

    public final String zza(Context context, String str, View view) {
        return zza(context, str, view, (Activity) null);
    }

    public String zza(Context context, String str, View view, Activity activity) {
        return zza(context, str, zzee.zzxf, view, activity, (byte[]) null);
    }

    public void zza(MotionEvent motionEvent) {
        boolean z = false;
        if (this.zzwc) {
            zzbv();
            this.zzwc = false;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            this.zzvv = 0.0d;
            this.zzvw = (double) motionEvent.getRawX();
            this.zzvx = (double) motionEvent.getRawY();
        } else if (action == 1 || action == 2) {
            double rawX = (double) motionEvent.getRawX();
            double rawY = (double) motionEvent.getRawY();
            double d = rawX - this.zzvw;
            double d2 = rawY - this.zzvx;
            this.zzvv += Math.sqrt((d * d) + (d2 * d2));
            this.zzvw = rawX;
            this.zzvx = rawY;
        }
        int action2 = motionEvent.getAction();
        if (action2 == 0) {
            this.zzvy = motionEvent.getX();
            this.zzvz = motionEvent.getY();
            this.zzwa = motionEvent.getRawX();
            this.zzwb = motionEvent.getRawY();
            this.zzvo++;
        } else if (action2 == 1) {
            this.zzvm = MotionEvent.obtain(motionEvent);
            this.zzvn.add(this.zzvm);
            if (this.zzvn.size() > 6) {
                this.zzvn.remove().recycle();
            }
            this.zzvq++;
            this.zzvs = zza(new Throwable().getStackTrace());
        } else if (action2 == 2) {
            this.zzvp += (long) (motionEvent.getHistorySize() + 1);
            try {
                zzeq zzb = zzb(motionEvent);
                if ((zzb == null || zzb.zzym == null || zzb.zzyp == null) ? false : true) {
                    this.zzvt += zzb.zzym.longValue() + zzb.zzyp.longValue();
                }
                if (!(this.zzwe == null || zzb == null || zzb.zzyn == null || zzb.zzyq == null)) {
                    z = true;
                }
                if (z) {
                    this.zzvu += zzb.zzyn.longValue() + zzb.zzyq.longValue();
                }
            } catch (zzeh unused) {
            }
        } else if (action2 == 3) {
            this.zzvr++;
        }
        this.zzwd = true;
    }

    public void zza(int i, int i2, int i3) {
        if (this.zzvm != null) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzckw)).booleanValue()) {
                zzbv();
            } else {
                this.zzvm.recycle();
            }
        }
        DisplayMetrics displayMetrics = this.zzwe;
        if (displayMetrics != null) {
            float f = displayMetrics.density;
            this.zzvm = MotionEvent.obtain(0, (long) i3, 1, ((float) i) * f, ((float) i2) * f, 0.0f, 0.0f, 0, 0.0f, 0.0f, 0, 0);
        } else {
            this.zzvm = null;
        }
        this.zzwd = false;
    }

    private final String zza(Context context, String str, int i, View view, Activity activity, byte[] bArr) {
        String str2;
        zzde zzde;
        int i2;
        int i3;
        int i4;
        int i5;
        Context context2 = context;
        int i6 = i;
        View view2 = view;
        Activity activity2 = activity;
        long currentTimeMillis = System.currentTimeMillis();
        boolean booleanValue = ((Boolean) zzve.zzoy().zzd(zzzn.zzcky)).booleanValue();
        zzbs.zza.zzb zzb = null;
        if (booleanValue) {
            zzde = zzuv != null ? zzuv.zzcc() : null;
            str2 = ((Boolean) zzve.zzoy().zzd(zzzn.zzclj)).booleanValue() ? "be" : "te";
        } else {
            zzde = null;
            str2 = null;
        }
        try {
            if (i6 == zzee.zzxf) {
                zzb = zzc(context2, view2, activity2);
                this.zzwc = true;
                i5 = AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE;
            } else if (i6 == zzee.zzxe) {
                zzb = zzb(context2, view2, activity2);
                i5 = 1008;
            } else {
                zzb = zza(context2, (zzbo.zza) null);
                i5 = 1000;
            }
            if (booleanValue && zzde != null) {
                zzde.zza(i5, -1, System.currentTimeMillis() - currentTimeMillis, str2);
            }
        } catch (Exception e) {
            Exception exc = e;
            if (booleanValue && zzde != null) {
                if (i6 == zzee.zzxf) {
                    i4 = 1003;
                } else if (i6 == zzee.zzxe) {
                    i4 = 1009;
                } else {
                    i4 = i6 == zzee.zzxd ? 1001 : -1;
                }
                zzde.zza(i4, -1, System.currentTimeMillis() - currentTimeMillis, str2, exc);
            }
        }
        long currentTimeMillis2 = System.currentTimeMillis();
        if (zzb != null) {
            try {
                if (((zzbs.zza) ((zzdrt) zzb.zzbaf())).zzazu() != 0) {
                    String zzj = zzck.zzj((zzbs.zza) ((zzdrt) zzb.zzbaf()), str);
                    if (!booleanValue || zzde == null) {
                        return zzj;
                    }
                    if (i6 == zzee.zzxf) {
                        i3 = 1006;
                    } else if (i6 == zzee.zzxe) {
                        i3 = 1010;
                    } else {
                        i3 = i6 == zzee.zzxd ? 1004 : -1;
                    }
                    zzde.zza(i3, -1, System.currentTimeMillis() - currentTimeMillis2, str2);
                    return zzj;
                }
            } catch (Exception e2) {
                Exception exc2 = e2;
                String num = Integer.toString(7);
                if (!booleanValue || zzde == null) {
                    return num;
                }
                if (i6 == zzee.zzxf) {
                    i2 = 1007;
                } else if (i6 == zzee.zzxe) {
                    i2 = 1011;
                } else {
                    i2 = i6 == zzee.zzxd ? WebSocketProtocol.CLOSE_NO_STATUS_CODE : -1;
                }
                zzde.zza(i2, -1, System.currentTimeMillis() - currentTimeMillis2, str2, exc2);
                return num;
            }
        }
        return Integer.toString(5);
    }
}
