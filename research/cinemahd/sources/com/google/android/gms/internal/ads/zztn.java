package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final class zztn implements zzdsa {
    static final zzdsa zzew = new zztn();

    private zztn() {
    }

    public final boolean zzf(int i) {
        return zzsy.zzq.zza.zzch(i) != null;
    }
}
