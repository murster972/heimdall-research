package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.internal.featurehighlight.zzh;

final class zzo implements zzh {
    final /* synthetic */ zzn zzjh;

    zzo(zzn zzn) {
        this.zzjh = zzn;
    }

    public final void dismiss() {
        if (this.zzjh.zzjg) {
            IntroductoryOverlay.zza.a(this.zzjh.zzij);
            this.zzjh.zzjf.b((Runnable) new zzq(this));
        }
    }

    public final void zzav() {
        if (this.zzjh.zzjg) {
            IntroductoryOverlay.zza.a(this.zzjh.zzij);
            this.zzjh.zzjf.c((Runnable) new zzp(this));
        }
    }
}
