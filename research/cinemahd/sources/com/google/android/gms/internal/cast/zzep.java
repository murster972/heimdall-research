package com.google.android.gms.internal.cast;

import android.view.Display;
import com.google.android.gms.cast.CastRemoteDisplay$CastRemoteDisplaySessionResult;
import com.google.android.gms.common.api.Status;

final class zzep implements CastRemoteDisplay$CastRemoteDisplaySessionResult {
    private final Display zzbz;
    private final Status zzgq;

    public zzep(Display display) {
        this.zzgq = Status.e;
        this.zzbz = display;
    }

    public final Display getPresentationDisplay() {
        return this.zzbz;
    }

    public final Status getStatus() {
        return this.zzgq;
    }

    public zzep(Status status) {
        this.zzgq = status;
        this.zzbz = null;
    }
}
