package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.Cast;

final class zzbe extends Cast.Listener {
    private final /* synthetic */ zzbd zzss;

    zzbe(zzbd zzbd) {
        this.zzss = zzbd;
    }

    public final void onVolumeChanged() {
        this.zzss.zzdk();
    }
}
