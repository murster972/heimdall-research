package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.concurrent.Callable;

public final class zzdch {
    private final E zzgpx;
    private final /* synthetic */ zzdcd zzgqd;

    private zzdch(zzdcd zzdcd, E e) {
        this.zzgqd = zzdcd;
        this.zzgpx = e;
    }

    private final <O> zzdcj<O> zza(Callable<O> callable, zzdhd zzdhd) {
        return new zzdcj(this.zzgqd, this.zzgpx, (String) null, zzdcd.zzgqa, Collections.emptyList(), zzdhd.zzd(callable), (zzdcc) null);
    }

    public final <O> zzdcj<O> zzc(zzdhe<O> zzdhe) {
        return new zzdcj(this.zzgqd, this.zzgpx, (String) null, zzdcd.zzgqa, Collections.emptyList(), zzdhe, (zzdcc) null);
    }

    public final zzdcj<?> zza(zzdcb zzdcb, zzdhd zzdhd) {
        return zza(new zzdcg(zzdcb), zzdhd);
    }

    public final <O> zzdcj<O> zzc(Callable<O> callable) {
        return zza(callable, this.zzgqd.zzfov);
    }
}
