package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzchh {
    public final zzaqq zzfwi;
    public final JSONObject zzfwj;

    public zzchh(zzaqq zzaqq, JSONObject jSONObject) {
        this.zzfwi = zzaqq;
        this.zzfwj = jSONObject;
    }
}
