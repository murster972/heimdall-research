package com.google.android.gms.internal.ads;

import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import com.google.ar.core.ImageMetadata;

public final class zzbbt implements zzha {
    private int zzbep;
    private final zznp zzeci;
    private long zzecj;
    private long zzeck;
    private long zzecl;
    private long zzecm;
    private boolean zzecn;

    zzbbt() {
        this(15000, HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT, 2500, 5000);
    }

    private final void zzk(boolean z) {
        this.zzbep = 0;
        this.zzecn = false;
        if (z) {
            this.zzeci.reset();
        }
    }

    public final void onStopped() {
        zzk(true);
    }

    public final void zza(zzhf[] zzhfArr, zzmr zzmr, zzng zzng) {
        this.zzbep = 0;
        for (int i = 0; i < zzhfArr.length; i++) {
            if (zzng.zzay(i) != null) {
                this.zzbep += zzoq.zzbl(zzhfArr[i].getTrackType());
            }
        }
        this.zzeci.zzba(this.zzbep);
    }

    public final synchronized boolean zzc(long j, boolean z) {
        boolean z2;
        long j2 = z ? this.zzecm : this.zzecl;
        if (j2 <= 0 || j >= j2) {
            z2 = true;
        } else {
            z2 = false;
        }
        return z2;
    }

    public final synchronized void zzcx(int i) {
        this.zzecl = ((long) i) * 1000;
    }

    public final synchronized void zzcy(int i) {
        this.zzecm = ((long) i) * 1000;
    }

    public final synchronized void zzdc(int i) {
        this.zzecj = ((long) i) * 1000;
    }

    public final synchronized void zzdd(int i) {
        this.zzeck = ((long) i) * 1000;
    }

    public final synchronized boolean zzdt(long j) {
        char c;
        boolean z = false;
        if (j > this.zzeck) {
            c = 0;
        } else {
            c = j < this.zzecj ? (char) 2 : 1;
        }
        boolean z2 = this.zzeci.zzii() >= this.zzbep;
        if (c == 2 || (c == 1 && this.zzecn && !z2)) {
            z = true;
        }
        this.zzecn = z;
        return this.zzecn;
    }

    public final void zzer() {
        zzk(false);
    }

    public final void zzes() {
        zzk(true);
    }

    public final zznj zzet() {
        return this.zzeci;
    }

    private zzbbt(int i, int i2, long j, long j2) {
        this.zzeci = new zznp(true, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE);
        this.zzecj = 15000000;
        this.zzeck = 30000000;
        this.zzecl = 2500000;
        this.zzecm = 5000000;
    }
}
