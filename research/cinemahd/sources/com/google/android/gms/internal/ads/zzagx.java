package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzagx extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzagx> CREATOR = new zzaha();
    public final Bundle extras;
    public final String zzcyg;

    public zzagx(String str, Bundle bundle) {
        this.zzcyg = str;
        this.extras = bundle;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzcyg, false);
        SafeParcelWriter.a(parcel, 2, this.extras, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
