package com.google.android.gms.internal.ads;

public final class zzhi {
    public Object zzado;
    public int zzaev;
    private Object zzagi;
    public long zzagj;
    private boolean zzagk;
    private long zzagl;

    public final zzhi zza(Object obj, Object obj2, int i, long j, long j2, boolean z) {
        this.zzagi = obj;
        this.zzado = obj2;
        this.zzaev = 0;
        this.zzagj = j;
        this.zzagl = j2;
        this.zzagk = false;
        return this;
    }

    public final long zzex() {
        return zzgi.zzdm(this.zzagl);
    }
}
