package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcmz implements zzdcb {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcip zzgau;
    private final zzcna zzgbf;

    zzcmz(zzcna zzcna, zzczt zzczt, zzczl zzczl, zzcip zzcip) {
        this.zzgbf = zzcna;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
        this.zzgau = zzcip;
    }

    public final void run() {
        this.zzgbf.zzd(this.zzfot, this.zzfel, this.zzgau);
    }
}
