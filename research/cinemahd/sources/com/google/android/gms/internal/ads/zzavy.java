package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzavy implements zzavu {
    private zzavu zzdsn;
    private zzavu zzdso;

    public zzavy(zzavu zzavu, zzavu zzavu2) {
        this.zzdsn = zzavu;
        this.zzdso = zzavu2;
    }

    public final void zza(String str, String str2, boolean z) {
        this.zzdsn.zza(str, str2, z);
    }

    public final void zzao(boolean z) {
        this.zzdsn.zzao(z);
    }

    public final void zzap(boolean z) {
        this.zzdsn.zzap(z);
    }

    public final void zzaq(boolean z) {
        this.zzdso.zzaq(z);
    }

    public final void zzb(Runnable runnable) {
        this.zzdsn.zzb(runnable);
    }

    public final void zzcp(int i) {
        this.zzdsn.zzcp(i);
    }

    public final void zzcq(int i) {
        this.zzdso.zzcq(i);
    }

    public final void zzee(String str) {
        this.zzdsn.zzee(str);
    }

    public final void zzef(String str) {
        this.zzdsn.zzef(str);
    }

    public final void zzeg(String str) {
        this.zzdsn.zzeg(str);
    }

    public final void zzeh(String str) {
        this.zzdsn.zzeh(str);
    }

    public final void zzez(long j) {
        this.zzdso.zzez(j);
    }

    public final void zzfa(long j) {
        this.zzdso.zzfa(j);
    }

    public final zzqi zzvt() {
        return this.zzdsn.zzvt();
    }

    public final boolean zzvu() {
        return this.zzdsn.zzvu();
    }

    public final String zzvv() {
        return this.zzdsn.zzvv();
    }

    public final boolean zzvw() {
        return this.zzdsn.zzvw();
    }

    public final String zzvx() {
        return this.zzdsn.zzvx();
    }

    public final boolean zzvy() {
        return this.zzdso.zzvy();
    }

    public final int zzvz() {
        return this.zzdsn.zzvz();
    }

    public final zzavf zzwa() {
        return this.zzdsn.zzwa();
    }

    public final long zzwb() {
        return this.zzdso.zzwb();
    }

    public final int zzwc() {
        return this.zzdso.zzwc();
    }

    public final long zzwd() {
        return this.zzdso.zzwd();
    }

    public final JSONObject zzwe() {
        return this.zzdsn.zzwe();
    }

    public final void zzwf() {
        this.zzdsn.zzwf();
    }

    public final String zzwg() {
        return this.zzdsn.zzwg();
    }
}
