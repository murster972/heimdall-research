package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import java.io.IOException;
import java.util.List;
import java.util.Map;

final class zzdrf implements zzdvl {
    private final zzdrb zzhic;

    private zzdrf(zzdrb zzdrb) {
        this.zzhic = (zzdrb) zzdrv.zza(zzdrb, "output");
        this.zzhic.zzhiv = this;
    }

    public static zzdrf zza(zzdrb zzdrb) {
        zzdrf zzdrf = zzdrb.zzhiv;
        if (zzdrf != null) {
            return zzdrf;
        }
        return new zzdrf(zzdrb);
    }

    public final void zzab(int i, int i2) throws IOException {
        this.zzhic.zzab(i, i2);
    }

    public final void zzac(int i, int i2) throws IOException {
        this.zzhic.zzac(i, i2);
    }

    public final void zzad(int i, int i2) throws IOException {
        this.zzhic.zzad(i, i2);
    }

    public final void zzae(int i, int i2) throws IOException {
        this.zzhic.zzae(i, i2);
    }

    public final void zzal(int i, int i2) throws IOException {
        this.zzhic.zzae(i, i2);
    }

    public final void zzam(int i, int i2) throws IOException {
        this.zzhic.zzab(i, i2);
    }

    public final int zzazg() {
        return zzdrt.zze.zzhnd;
    }

    public final void zzb(int i, double d) throws IOException {
        this.zzhic.zzb(i, d);
    }

    public final void zzc(int i, Object obj) throws IOException {
        if (obj instanceof zzdqk) {
            this.zzhic.zzb(i, (zzdqk) obj);
        } else {
            this.zzhic.zzb(i, (zzdte) obj);
        }
    }

    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzfk(list.get(i4).longValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfg(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzg(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzfm(list.get(i4).longValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfi(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzi(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zzf(int i, String str) throws IOException {
        this.zzhic.zzf(i, str);
    }

    public final void zzg(int i, long j) throws IOException {
        this.zzhic.zzg(i, j);
    }

    public final void zzgi(int i) throws IOException {
        this.zzhic.zzaa(i, 3);
    }

    public final void zzgj(int i) throws IOException {
        this.zzhic.zzaa(i, 4);
    }

    public final void zzh(int i, boolean z) throws IOException {
        this.zzhic.zzh(i, z);
    }

    public final void zzi(int i, long j) throws IOException {
        this.zzhic.zzi(i, j);
    }

    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzgb(list.get(i4).intValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfw(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzac(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzge(list.get(i4).intValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfy(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzae(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzfn(list.get(i4).longValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfi(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzi(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzgc(list.get(i4).intValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfx(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzad(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzfl(list.get(i4).longValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfh(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzh(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zzo(int i, long j) throws IOException {
        this.zzhic.zzg(i, j);
    }

    public final void zzp(int i, long j) throws IOException {
        this.zzhic.zzi(i, j);
    }

    public final void zzb(int i, Object obj, zzdua zzdua) throws IOException {
        zzdrb zzdrb = this.zzhic;
        zzdrb.zzaa(i, 3);
        zzdua.zza((zzdte) obj, zzdrb.zzhiv);
        zzdrb.zzaa(i, 4);
    }

    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzg(list.get(i4).floatValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzf(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zza(i, list.get(i2).floatValue());
            i2++;
        }
    }

    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzc(list.get(i4).doubleValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzb(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzb(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    public final void zzh(int i, long j) throws IOException {
        this.zzhic.zzh(i, j);
    }

    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzbo(list.get(i4).booleanValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzbn(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzh(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    public final void zza(int i, float f) throws IOException {
        this.zzhic.zza(i, f);
    }

    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzgf(list.get(i4).intValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfv(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzab(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zza(int i, zzdqk zzdqk) throws IOException {
        this.zzhic.zza(i, zzdqk);
    }

    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzfj(list.get(i4).longValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfg(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzg(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zza(int i, Object obj, zzdua zzdua) throws IOException {
        this.zzhic.zza(i, (zzdte) obj, zzdua);
    }

    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzga(list.get(i4).intValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfv(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzab(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzhic.zzaa(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzdrb.zzgd(list.get(i4).intValue());
            }
            this.zzhic.zzfw(i3);
            while (i2 < list.size()) {
                this.zzhic.zzfy(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzae(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof zzdsl) {
            zzdsl zzdsl = (zzdsl) list;
            while (i2 < list.size()) {
                Object zzgm = zzdsl.zzgm(i2);
                if (zzgm instanceof String) {
                    this.zzhic.zzf(i, (String) zzgm);
                } else {
                    this.zzhic.zza(i, (zzdqk) zzgm);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzhic.zzf(i, list.get(i2));
            i2++;
        }
    }

    public final void zzb(int i, List<zzdqk> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.zzhic.zza(i, list.get(i2));
        }
    }

    public final void zzb(int i, List<?> list, zzdua zzdua) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zzb(i, (Object) list.get(i2), zzdua);
        }
    }

    public final void zza(int i, List<?> list, zzdua zzdua) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zza(i, (Object) list.get(i2), zzdua);
        }
    }

    public final <K, V> void zza(int i, zzdsv<K, V> zzdsv, Map<K, V> map) throws IOException {
        for (Map.Entry next : map.entrySet()) {
            this.zzhic.zzaa(i, 2);
            this.zzhic.zzfw(zzdsw.zza(zzdsv, next.getKey(), next.getValue()));
            zzdsw.zza(this.zzhic, zzdsv, next.getKey(), next.getValue());
        }
    }
}
