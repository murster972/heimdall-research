package com.google.android.gms.internal.ads;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public abstract class zzdeu<E> extends zzdet<E> implements List<E>, RandomAccess {
    private static final zzdfo<Object> zzgui = new zzdew(zzdfe.zzgut, 0);

    zzdeu() {
    }

    public static <E> zzdeu<E> zzaf(E e) {
        Object[] objArr = {e};
        for (int i = 0; i <= 0; i++) {
            zzdff.zzd(objArr[0], 0);
        }
        return zzb(objArr, 1);
    }

    public static <E> zzdeu<E> zzard() {
        return zzdfe.zzgut;
    }

    public static <E> zzdeu<E> zzb(E[] eArr) {
        if (eArr.length == 0) {
            return zzdfe.zzgut;
        }
        Object[] objArr = (Object[]) eArr.clone();
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            zzdff.zzd(objArr[i], i);
        }
        return zzb(objArr, objArr.length);
    }

    static <E> zzdeu<E> zzc(Object[] objArr) {
        return zzb(objArr, objArr.length);
    }

    public static <E> zzdeu<E> zzf(Iterable<? extends E> iterable) {
        zzdei.checkNotNull(iterable);
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection instanceof zzdet) {
                zzdeu<E> zzarb = ((zzdet) collection).zzarb();
                if (!zzarb.zzarc()) {
                    return zzarb;
                }
                Object[] array = zzarb.toArray();
                return zzb(array, array.length);
            }
            Object[] array2 = collection.toArray();
            int length = array2.length;
            for (int i = 0; i < length; i++) {
                zzdff.zzd(array2[i], i);
            }
            return zzb(array2, array2.length);
        }
        Iterator<? extends E> it2 = iterable.iterator();
        if (!it2.hasNext()) {
            return zzdfe.zzgut;
        }
        Object next = it2.next();
        if (!it2.hasNext()) {
            return zzaf(next);
        }
        zzdex zzdex = (zzdex) ((zzdex) new zzdex().zzae(next)).zza(it2);
        zzdex.zzgug = true;
        return zzb(zzdex.zzguf, zzdex.size);
    }

    @Deprecated
    public final void add(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    public boolean equals(Object obj) {
        if (obj == zzdei.checkNotNull(this)) {
            return true;
        }
        if (obj instanceof List) {
            List list = (List) obj;
            int size = size();
            if (size == list.size()) {
                if (list instanceof RandomAccess) {
                    int i = 0;
                    while (i < size) {
                        if (zzdeg.equal(get(i), list.get(i))) {
                            i++;
                        }
                    }
                    return true;
                }
                int size2 = size();
                Iterator it2 = list.iterator();
                int i2 = 0;
                while (true) {
                    if (i2 < size2) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        Object obj2 = get(i2);
                        i2++;
                        if (!zzdeg.equal(obj2, it2.next())) {
                            break;
                        }
                    } else if (!it2.hasNext()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = ~(~((i * 31) + get(i2).hashCode()));
        }
        return i;
    }

    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        int size = size();
        int i = 0;
        if (obj == null) {
            while (i < size) {
                if (get(i) == null) {
                    return i;
                }
                i++;
            }
        } else {
            while (i < size) {
                if (obj.equals(get(i))) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    public int lastIndexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        if (obj == null) {
            for (int size = size() - 1; size >= 0; size--) {
                if (get(size) == null) {
                    return size;
                }
            }
        } else {
            for (int size2 = size() - 1; size2 >= 0; size2--) {
                if (obj.equals(get(size2))) {
                    return size2;
                }
            }
        }
        return -1;
    }

    public /* synthetic */ ListIterator listIterator(int i) {
        zzdei.zzt(i, size());
        if (isEmpty()) {
            return zzgui;
        }
        return new zzdew(this, i);
    }

    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    public int zza(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    /* renamed from: zzaqx */
    public final zzdfp<E> iterator() {
        return (zzdfo) listIterator();
    }

    public final zzdeu<E> zzarb() {
        return this;
    }

    /* renamed from: zzu */
    public zzdeu<E> subList(int i, int i2) {
        zzdei.zzf(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        if (i3 == 0) {
            return zzdfe.zzgut;
        }
        return new zzdez(this, i, i3);
    }

    public /* synthetic */ ListIterator listIterator() {
        return (zzdfo) listIterator(0);
    }

    static <E> zzdeu<E> zzb(Object[] objArr, int i) {
        if (i == 0) {
            return zzdfe.zzgut;
        }
        return new zzdfe(objArr, i);
    }
}
