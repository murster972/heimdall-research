package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzcby implements zzafn {
    private final zzbdi zzfpv;
    private final zzcbp zzfrj;

    zzcby(zzcbp zzcbp, zzbdi zzbdi) {
        this.zzfrj = zzcbp;
        this.zzfpv = zzbdi;
    }

    public final void zza(Object obj, Map map) {
        this.zzfrj.zza(this.zzfpv, (zzbdi) obj, map);
    }
}
