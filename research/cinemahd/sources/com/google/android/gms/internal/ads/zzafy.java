package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.applovin.sdk.AppLovinEventParameters;
import java.util.Map;

public final class zzafy implements zzafn<Object> {
    private final zzafx zzcxu;

    private zzafy(zzafx zzafx) {
        this.zzcxu = zzafx;
    }

    public static void zza(zzbdi zzbdi, zzafx zzafx) {
        zzbdi.zza("/reward", (zzafn<? super zzbdi>) new zzafy(zzafx));
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get("action");
        if ("grant".equals(str)) {
            zzasd zzasd = null;
            try {
                int parseInt = Integer.parseInt(map.get(AppLovinEventParameters.REVENUE_AMOUNT));
                String str2 = map.get("type");
                if (!TextUtils.isEmpty(str2)) {
                    zzasd = new zzasd(str2, parseInt);
                }
            } catch (NumberFormatException e) {
                zzayu.zzd("Unable to parse reward amount.", e);
            }
            this.zzcxu.zza(zzasd);
        } else if ("video_start".equals(str)) {
            this.zzcxu.zzrs();
        } else if ("video_complete".equals(str)) {
            this.zzcxu.zzrt();
        }
    }
}
