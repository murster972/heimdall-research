package com.google.android.gms.internal.ads;

public final class zzbva implements zzbov {
    private final zzbpd zzfjm;
    private final zzczl zzfjn;

    public zzbva(zzbpd zzbpd, zzczl zzczl) {
        this.zzfjm = zzbpd;
        this.zzfjn = zzczl;
    }

    public final void onAdClosed() {
    }

    public final void onAdLeftApplication() {
    }

    public final void onAdOpened() {
        int i = this.zzfjn.zzglz;
        if (i == 0 || i == 1) {
            this.zzfjm.onAdImpression();
        }
    }

    public final void onRewardedVideoCompleted() {
    }

    public final void onRewardedVideoStarted() {
    }

    public final void zzb(zzare zzare, String str, String str2) {
    }
}
