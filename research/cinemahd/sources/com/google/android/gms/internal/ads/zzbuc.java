package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbuc implements zzdxg<Set<zzbsu<zzbov>>> {
    private final zzdxp<zzbva> zzfdq;
    private final zzbtv zzfje;

    private zzbuc(zzbtv zzbtv, zzdxp<zzbva> zzdxp) {
        this.zzfje = zzbtv;
        this.zzfdq = zzdxp;
    }

    public static zzbuc zza(zzbtv zzbtv, zzdxp<zzbva> zzdxp) {
        return new zzbuc(zzbtv, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfje.zza(this.zzfdq.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
