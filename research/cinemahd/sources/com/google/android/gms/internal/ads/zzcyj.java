package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcyj implements zzcxo {
    private final int zzdvv;

    zzcyj(int i) {
        this.zzdvv = i;
    }

    public final void zzt(Object obj) {
        ((zzaso) obj).onRewardedAdFailedToLoad(this.zzdvv);
    }
}
