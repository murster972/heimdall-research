package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzv extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzv> CREATOR = new zzy();
    public final long zza;
    public final long zzb;
    public final boolean zzc;
    public final String zzd;
    public final String zze;
    public final String zzf;
    public final Bundle zzg;

    public zzv(long j, long j2, boolean z, String str, String str2, String str3, Bundle bundle) {
        this.zza = j;
        this.zzb = j2;
        this.zzc = z;
        this.zzd = str;
        this.zze = str2;
        this.zzf = str3;
        this.zzg = bundle;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zza);
        SafeParcelWriter.a(parcel, 2, this.zzb);
        SafeParcelWriter.a(parcel, 3, this.zzc);
        SafeParcelWriter.a(parcel, 4, this.zzd, false);
        SafeParcelWriter.a(parcel, 5, this.zze, false);
        SafeParcelWriter.a(parcel, 6, this.zzf, false);
        SafeParcelWriter.a(parcel, 7, this.zzg, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
