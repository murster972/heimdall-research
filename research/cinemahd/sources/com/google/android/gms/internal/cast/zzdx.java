package com.google.android.gms.internal.cast;

import android.os.SystemClock;
import com.applovin.sdk.AppLovinEventTypes;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkActivity;
import com.google.android.gms.cast.AdBreakClipInfo;
import com.google.android.gms.cast.AdBreakStatus;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.cast.zzam;
import com.google.android.gms.cast.zzap;
import com.google.android.gms.cast.zzas;
import com.google.android.gms.cast.zzbx;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzdx extends zzcw {
    public static final String NAMESPACE = zzdk.zzq("com.google.cast.media");
    private final zzed zzaaa = new zzed(10000);
    private final zzed zzaab = new zzed(86400000);
    private final zzed zzaac = new zzed(86400000);
    private final zzed zzaad = new zzed(86400000);
    private final zzed zzaae = new zzed(86400000);
    private final zzed zzaaf = new zzed(86400000);
    private final zzed zzaag = new zzed(86400000);
    private final zzed zzaah = new zzed(86400000);
    private final zzed zzaai = new zzed(86400000);
    private final zzed zzaaj = new zzed(86400000);
    private final zzed zzaak = new zzed(86400000);
    private final zzed zzaal = new zzed(86400000);
    private final zzed zzaam = new zzed(86400000);
    private final zzed zzaan = new zzed(86400000);
    private final zzed zzaao = new zzed(86400000);
    private long zzzs;
    private MediaStatus zzzt;
    /* access modifiers changed from: private */
    public Long zzzu;
    private zzdz zzzv;
    private final zzed zzzw = new zzed(86400000);
    private final zzed zzzx = new zzed(86400000);
    private final zzed zzzy = new zzed(86400000);
    private final zzed zzzz = new zzed(86400000);

    public zzdx(String str) {
        super(NAMESPACE, "MediaControlChannel", (String) null);
        zza(this.zzzw);
        zza(this.zzzx);
        zza(this.zzzy);
        zza(this.zzzz);
        zza(this.zzaaa);
        zza(this.zzaab);
        zza(this.zzaac);
        zza(this.zzaad);
        zza(this.zzaae);
        zza(this.zzaaf);
        zza(this.zzaag);
        zza(this.zzaah);
        zza(this.zzaai);
        zza(this.zzaaj);
        zza(this.zzaak);
        zza(this.zzaam);
        zza(this.zzaam);
        zza(this.zzaan);
        zza(this.zzaao);
        zzfc();
    }

    private final void onMetadataUpdated() {
        zzdz zzdz = this.zzzv;
        if (zzdz != null) {
            zzdz.onMetadataUpdated();
        }
    }

    private final void onPreloadStatusUpdated() {
        zzdz zzdz = this.zzzv;
        if (zzdz != null) {
            zzdz.onPreloadStatusUpdated();
        }
    }

    private final void onQueueStatusUpdated() {
        zzdz zzdz = this.zzzv;
        if (zzdz != null) {
            zzdz.onQueueStatusUpdated();
        }
    }

    private final void onStatusUpdated() {
        zzdz zzdz = this.zzzv;
        if (zzdz != null) {
            zzdz.onStatusUpdated();
        }
    }

    private final void zzfc() {
        this.zzzs = 0;
        this.zzzt = null;
        for (zzed zzv : zzer()) {
            zzv.zzv(AdError.CACHE_ERROR_CODE);
        }
    }

    private final long zzp() throws zzea {
        MediaStatus mediaStatus = this.zzzt;
        if (mediaStatus != null) {
            return mediaStatus.J();
        }
        throw new zzea();
    }

    public final long getApproximateAdBreakClipPositionMs() {
        MediaStatus mediaStatus;
        AdBreakStatus t;
        AdBreakClipInfo u;
        if (this.zzzs == 0 || (mediaStatus = this.zzzt) == null || (t = mediaStatus.t()) == null || (u = this.zzzt.u()) == null) {
            return 0;
        }
        double d = 0.0d;
        if (this.zzzt.z() == 0.0d && this.zzzt.A() == 2) {
            d = 1.0d;
        }
        return zza(d, t.u(), u.v());
    }

    public final long getApproximateStreamPosition() {
        MediaInfo mediaInfo = getMediaInfo();
        if (mediaInfo == null) {
            return 0;
        }
        Long l = this.zzzu;
        if (l != null) {
            return l.longValue();
        }
        if (this.zzzs == 0) {
            return 0;
        }
        double z = this.zzzt.z();
        long E = this.zzzt.E();
        int A = this.zzzt.A();
        if (z == 0.0d || A != 2) {
            return E;
        }
        return zza(z, E, mediaInfo.z());
    }

    public final MediaInfo getMediaInfo() {
        MediaStatus mediaStatus = this.zzzt;
        if (mediaStatus == null) {
            return null;
        }
        return mediaStatus.y();
    }

    public final MediaStatus getMediaStatus() {
        return this.zzzt;
    }

    public final long getStreamDuration() {
        MediaInfo mediaInfo = getMediaInfo();
        if (mediaInfo != null) {
            return mediaInfo.z();
        }
        return 0;
    }

    public final void zza(zzdz zzdz) {
        this.zzzv = zzdz;
    }

    public final long zzb(zzec zzec, JSONObject jSONObject) throws IllegalStateException, zzea {
        JSONObject jSONObject2 = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject2.put("requestId", zzes);
            jSONObject2.put("type", "STOP");
            jSONObject2.put("mediaSessionId", zzp());
            if (jSONObject != null) {
                jSONObject2.put("customData", jSONObject);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject2.toString(), zzes, (String) null);
        this.zzzz.zza(zzes, zzec);
        return zzes;
    }

    public final long zzc(zzec zzec, JSONObject jSONObject) throws IllegalStateException, zzea {
        JSONObject jSONObject2 = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject2.put("requestId", zzes);
            jSONObject2.put("type", "PLAY");
            jSONObject2.put("mediaSessionId", zzp());
            if (jSONObject != null) {
                jSONObject2.put("customData", jSONObject);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject2.toString(), zzes, (String) null);
        this.zzzy.zza(zzes, zzec);
        return zzes;
    }

    public final void zzeq() {
        super.zzeq();
        zzfc();
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x020c A[Catch:{ JSONException -> 0x0289 }] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0219 A[Catch:{ JSONException -> 0x0289 }] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0226 A[Catch:{ JSONException -> 0x0289 }] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0230 A[Catch:{ JSONException -> 0x0289 }] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0237 A[Catch:{ JSONException -> 0x0289 }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x023e A[Catch:{ JSONException -> 0x0289 }] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0245 A[Catch:{ JSONException -> 0x0289 }] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0258 A[Catch:{ JSONException -> 0x0289 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzo(java.lang.String r15) {
        /*
            r14 = this;
            com.google.android.gms.internal.cast.zzdw r0 = r14.zzxw
            r1 = 1
            java.lang.Object[] r2 = new java.lang.Object[r1]
            r3 = 0
            r2[r3] = r15
            java.lang.String r4 = "message received: %s"
            r0.d(r4, r2)
            r0 = 2
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0289 }
            r2.<init>(r15)     // Catch:{ JSONException -> 0x0289 }
            java.lang.String r4 = "type"
            java.lang.String r4 = r2.getString(r4)     // Catch:{ JSONException -> 0x0289 }
            java.lang.String r5 = "requestId"
            r6 = -1
            long r5 = r2.optLong(r5, r6)     // Catch:{ JSONException -> 0x0289 }
            int r7 = r4.hashCode()     // Catch:{ JSONException -> 0x0289 }
            r8 = -1
            r9 = 4
            r10 = 3
            switch(r7) {
                case -1830647528: goto L_0x0072;
                case -1790231854: goto L_0x0068;
                case -1125000185: goto L_0x005e;
                case -262628938: goto L_0x0054;
                case 154411710: goto L_0x004a;
                case 431600379: goto L_0x0040;
                case 823510221: goto L_0x0036;
                case 2107149050: goto L_0x002c;
                default: goto L_0x002b;
            }     // Catch:{ JSONException -> 0x0289 }
        L_0x002b:
            goto L_0x007c
        L_0x002c:
            java.lang.String r7 = "QUEUE_ITEM_IDS"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 5
            goto L_0x007d
        L_0x0036:
            java.lang.String r7 = "MEDIA_STATUS"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 0
            goto L_0x007d
        L_0x0040:
            java.lang.String r7 = "INVALID_PLAYER_STATE"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 1
            goto L_0x007d
        L_0x004a:
            java.lang.String r7 = "QUEUE_CHANGE"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 6
            goto L_0x007d
        L_0x0054:
            java.lang.String r7 = "LOAD_FAILED"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 2
            goto L_0x007d
        L_0x005e:
            java.lang.String r7 = "INVALID_REQUEST"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 4
            goto L_0x007d
        L_0x0068:
            java.lang.String r7 = "QUEUE_ITEMS"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 7
            goto L_0x007d
        L_0x0072:
            java.lang.String r7 = "LOAD_CANCELLED"
            boolean r4 = r4.equals(r7)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x007c
            r4 = 3
            goto L_0x007d
        L_0x007c:
            r4 = -1
        L_0x007d:
            java.lang.String r7 = "itemIds"
            r11 = 2100(0x834, float:2.943E-42)
            java.lang.String r12 = "customData"
            r13 = 0
            switch(r4) {
                case 0: goto L_0x01b2;
                case 1: goto L_0x018c;
                case 2: goto L_0x0182;
                case 3: goto L_0x0176;
                case 4: goto L_0x0150;
                case 5: goto L_0x0137;
                case 6: goto L_0x00be;
                case 7: goto L_0x0089;
                default: goto L_0x0087;
            }
        L_0x0087:
            goto L_0x0288
        L_0x0089:
            com.google.android.gms.internal.cast.zzed r4 = r14.zzaal     // Catch:{ JSONException -> 0x0289 }
            r4.zzc(r5, r3, r13)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzdz r4 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0288
            java.lang.String r4 = "items"
            org.json.JSONArray r2 = r2.getJSONArray(r4)     // Catch:{ JSONException -> 0x0289 }
            int r4 = r2.length()     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.cast.MediaQueueItem[] r4 = new com.google.android.gms.cast.MediaQueueItem[r4]     // Catch:{ JSONException -> 0x0289 }
            r5 = 0
        L_0x009f:
            int r6 = r2.length()     // Catch:{ JSONException -> 0x0289 }
            if (r5 >= r6) goto L_0x00b7
            com.google.android.gms.cast.MediaQueueItem$Builder r6 = new com.google.android.gms.cast.MediaQueueItem$Builder     // Catch:{ JSONException -> 0x0289 }
            org.json.JSONObject r7 = r2.getJSONObject(r5)     // Catch:{ JSONException -> 0x0289 }
            r6.<init>((org.json.JSONObject) r7)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.cast.MediaQueueItem r6 = r6.a()     // Catch:{ JSONException -> 0x0289 }
            r4[r5] = r6     // Catch:{ JSONException -> 0x0289 }
            int r5 = r5 + 1
            goto L_0x009f
        L_0x00b7:
            com.google.android.gms.internal.cast.zzdz r2 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            r2.zzb((com.google.android.gms.cast.MediaQueueItem[]) r4)     // Catch:{ JSONException -> 0x0289 }
            goto L_0x0288
        L_0x00be:
            com.google.android.gms.internal.cast.zzed r4 = r14.zzaam     // Catch:{ JSONException -> 0x0289 }
            r4.zzc(r5, r3, r13)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzdz r4 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0136
            java.lang.String r4 = "changeType"
            java.lang.String r4 = r2.getString(r4)     // Catch:{ JSONException -> 0x0289 }
            org.json.JSONArray r5 = r2.getJSONArray(r7)     // Catch:{ JSONException -> 0x0289 }
            int[] r5 = zzb((org.json.JSONArray) r5)     // Catch:{ JSONException -> 0x0289 }
            java.lang.String r6 = "insertBefore"
            int r2 = r2.optInt(r6, r3)     // Catch:{ JSONException -> 0x0289 }
            if (r5 == 0) goto L_0x0136
            int r6 = r4.hashCode()     // Catch:{ JSONException -> 0x0289 }
            switch(r6) {
                case -2130463047: goto L_0x010d;
                case -1881281404: goto L_0x0103;
                case -1785516855: goto L_0x00f9;
                case 1122976047: goto L_0x00ef;
                case 1395699694: goto L_0x00e5;
                default: goto L_0x00e4;
            }     // Catch:{ JSONException -> 0x0289 }
        L_0x00e4:
            goto L_0x0116
        L_0x00e5:
            java.lang.String r6 = "NO_CHANGE"
            boolean r4 = r4.equals(r6)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0116
            r8 = 4
            goto L_0x0116
        L_0x00ef:
            java.lang.String r6 = "ITEMS_CHANGE"
            boolean r4 = r4.equals(r6)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0116
            r8 = 1
            goto L_0x0116
        L_0x00f9:
            java.lang.String r6 = "UPDATE"
            boolean r4 = r4.equals(r6)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0116
            r8 = 3
            goto L_0x0116
        L_0x0103:
            java.lang.String r6 = "REMOVE"
            boolean r4 = r4.equals(r6)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0116
            r8 = 2
            goto L_0x0116
        L_0x010d:
            java.lang.String r6 = "INSERT"
            boolean r4 = r4.equals(r6)     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0116
            r8 = 0
        L_0x0116:
            if (r8 == 0) goto L_0x0131
            if (r8 == r1) goto L_0x012b
            if (r8 == r0) goto L_0x0125
            if (r8 == r10) goto L_0x011f
            goto L_0x0136
        L_0x011f:
            com.google.android.gms.internal.cast.zzdz r2 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            r2.zza(r5)     // Catch:{ JSONException -> 0x0289 }
            goto L_0x0136
        L_0x0125:
            com.google.android.gms.internal.cast.zzdz r2 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            r2.zzc(r5)     // Catch:{ JSONException -> 0x0289 }
            return
        L_0x012b:
            com.google.android.gms.internal.cast.zzdz r2 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            r2.zzb((int[]) r5)     // Catch:{ JSONException -> 0x0289 }
            return
        L_0x0131:
            com.google.android.gms.internal.cast.zzdz r4 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            r4.zza(r5, r2)     // Catch:{ JSONException -> 0x0289 }
        L_0x0136:
            return
        L_0x0137:
            com.google.android.gms.internal.cast.zzed r4 = r14.zzaak     // Catch:{ JSONException -> 0x0289 }
            r4.zzc(r5, r3, r13)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzdz r4 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x014f
            org.json.JSONArray r2 = r2.getJSONArray(r7)     // Catch:{ JSONException -> 0x0289 }
            int[] r2 = zzb((org.json.JSONArray) r2)     // Catch:{ JSONException -> 0x0289 }
            if (r2 == 0) goto L_0x014f
            com.google.android.gms.internal.cast.zzdz r4 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            r4.zza(r2)     // Catch:{ JSONException -> 0x0289 }
        L_0x014f:
            return
        L_0x0150:
            com.google.android.gms.internal.cast.zzdw r4 = r14.zzxw     // Catch:{ JSONException -> 0x0289 }
            java.lang.String r7 = "received unexpected error: Invalid Request."
            java.lang.Object[] r8 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0289 }
            r4.w(r7, r8)     // Catch:{ JSONException -> 0x0289 }
            org.json.JSONObject r2 = r2.optJSONObject(r12)     // Catch:{ JSONException -> 0x0289 }
            java.util.List r4 = r14.zzer()     // Catch:{ JSONException -> 0x0289 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ JSONException -> 0x0289 }
        L_0x0165:
            boolean r7 = r4.hasNext()     // Catch:{ JSONException -> 0x0289 }
            if (r7 == 0) goto L_0x0175
            java.lang.Object r7 = r4.next()     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzed r7 = (com.google.android.gms.internal.cast.zzed) r7     // Catch:{ JSONException -> 0x0289 }
            r7.zzc(r5, r11, r2)     // Catch:{ JSONException -> 0x0289 }
            goto L_0x0165
        L_0x0175:
            return
        L_0x0176:
            org.json.JSONObject r2 = r2.optJSONObject(r12)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzed r4 = r14.zzzw     // Catch:{ JSONException -> 0x0289 }
            r7 = 2101(0x835, float:2.944E-42)
            r4.zzc(r5, r7, r2)     // Catch:{ JSONException -> 0x0289 }
            return
        L_0x0182:
            org.json.JSONObject r2 = r2.optJSONObject(r12)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzed r4 = r14.zzzw     // Catch:{ JSONException -> 0x0289 }
            r4.zzc(r5, r11, r2)     // Catch:{ JSONException -> 0x0289 }
            return
        L_0x018c:
            com.google.android.gms.internal.cast.zzdw r4 = r14.zzxw     // Catch:{ JSONException -> 0x0289 }
            java.lang.String r7 = "received unexpected error: Invalid Player State."
            java.lang.Object[] r8 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0289 }
            r4.w(r7, r8)     // Catch:{ JSONException -> 0x0289 }
            org.json.JSONObject r2 = r2.optJSONObject(r12)     // Catch:{ JSONException -> 0x0289 }
            java.util.List r4 = r14.zzer()     // Catch:{ JSONException -> 0x0289 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ JSONException -> 0x0289 }
        L_0x01a1:
            boolean r7 = r4.hasNext()     // Catch:{ JSONException -> 0x0289 }
            if (r7 == 0) goto L_0x01b1
            java.lang.Object r7 = r4.next()     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzed r7 = (com.google.android.gms.internal.cast.zzed) r7     // Catch:{ JSONException -> 0x0289 }
            r7.zzc(r5, r11, r2)     // Catch:{ JSONException -> 0x0289 }
            goto L_0x01a1
        L_0x01b1:
            return
        L_0x01b2:
            java.lang.String r4 = "status"
            org.json.JSONArray r2 = r2.getJSONArray(r4)     // Catch:{ JSONException -> 0x0289 }
            int r4 = r2.length()     // Catch:{ JSONException -> 0x0289 }
            if (r4 <= 0) goto L_0x0262
            org.json.JSONObject r2 = r2.getJSONObject(r3)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzed r4 = r14.zzzw     // Catch:{ JSONException -> 0x0289 }
            boolean r4 = r4.test(r5)     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzed r7 = r14.zzaab     // Catch:{ JSONException -> 0x0289 }
            boolean r7 = r7.zzfd()     // Catch:{ JSONException -> 0x0289 }
            if (r7 == 0) goto L_0x01d8
            com.google.android.gms.internal.cast.zzed r7 = r14.zzaab     // Catch:{ JSONException -> 0x0289 }
            boolean r7 = r7.test(r5)     // Catch:{ JSONException -> 0x0289 }
            if (r7 == 0) goto L_0x01e8
        L_0x01d8:
            com.google.android.gms.internal.cast.zzed r7 = r14.zzaac     // Catch:{ JSONException -> 0x0289 }
            boolean r7 = r7.zzfd()     // Catch:{ JSONException -> 0x0289 }
            if (r7 == 0) goto L_0x01ea
            com.google.android.gms.internal.cast.zzed r7 = r14.zzaac     // Catch:{ JSONException -> 0x0289 }
            boolean r7 = r7.test(r5)     // Catch:{ JSONException -> 0x0289 }
            if (r7 != 0) goto L_0x01ea
        L_0x01e8:
            r7 = 1
            goto L_0x01eb
        L_0x01ea:
            r7 = 0
        L_0x01eb:
            if (r4 != 0) goto L_0x01f9
            com.google.android.gms.cast.MediaStatus r4 = r14.zzzt     // Catch:{ JSONException -> 0x0289 }
            if (r4 != 0) goto L_0x01f2
            goto L_0x01f9
        L_0x01f2:
            com.google.android.gms.cast.MediaStatus r4 = r14.zzzt     // Catch:{ JSONException -> 0x0289 }
            int r2 = r4.a(r2, r7)     // Catch:{ JSONException -> 0x0289 }
            goto L_0x0208
        L_0x01f9:
            com.google.android.gms.cast.MediaStatus r4 = new com.google.android.gms.cast.MediaStatus     // Catch:{ JSONException -> 0x0289 }
            r4.<init>(r2)     // Catch:{ JSONException -> 0x0289 }
            r14.zzzt = r4     // Catch:{ JSONException -> 0x0289 }
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ JSONException -> 0x0289 }
            r14.zzzs = r7     // Catch:{ JSONException -> 0x0289 }
            r2 = 127(0x7f, float:1.78E-43)
        L_0x0208:
            r4 = r2 & 1
            if (r4 == 0) goto L_0x0215
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ JSONException -> 0x0289 }
            r14.zzzs = r7     // Catch:{ JSONException -> 0x0289 }
            r14.onStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
        L_0x0215:
            r4 = r2 & 2
            if (r4 == 0) goto L_0x0222
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ JSONException -> 0x0289 }
            r14.zzzs = r7     // Catch:{ JSONException -> 0x0289 }
            r14.onStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
        L_0x0222:
            r4 = r2 & 128(0x80, float:1.794E-43)
            if (r4 == 0) goto L_0x022c
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ JSONException -> 0x0289 }
            r14.zzzs = r7     // Catch:{ JSONException -> 0x0289 }
        L_0x022c:
            r4 = r2 & 4
            if (r4 == 0) goto L_0x0233
            r14.onMetadataUpdated()     // Catch:{ JSONException -> 0x0289 }
        L_0x0233:
            r4 = r2 & 8
            if (r4 == 0) goto L_0x023a
            r14.onQueueStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
        L_0x023a:
            r4 = r2 & 16
            if (r4 == 0) goto L_0x0241
            r14.onPreloadStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
        L_0x0241:
            r4 = r2 & 32
            if (r4 == 0) goto L_0x0254
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ JSONException -> 0x0289 }
            r14.zzzs = r7     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzdz r4 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0254
            com.google.android.gms.internal.cast.zzdz r4 = r14.zzzv     // Catch:{ JSONException -> 0x0289 }
            r4.onAdBreakStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
        L_0x0254:
            r2 = r2 & 64
            if (r2 == 0) goto L_0x0270
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ JSONException -> 0x0289 }
            r14.zzzs = r7     // Catch:{ JSONException -> 0x0289 }
            r14.onStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
            goto L_0x0270
        L_0x0262:
            r14.zzzt = r13     // Catch:{ JSONException -> 0x0289 }
            r14.onStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
            r14.onMetadataUpdated()     // Catch:{ JSONException -> 0x0289 }
            r14.onQueueStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
            r14.onPreloadStatusUpdated()     // Catch:{ JSONException -> 0x0289 }
        L_0x0270:
            java.util.List r2 = r14.zzer()     // Catch:{ JSONException -> 0x0289 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ JSONException -> 0x0289 }
        L_0x0278:
            boolean r4 = r2.hasNext()     // Catch:{ JSONException -> 0x0289 }
            if (r4 == 0) goto L_0x0288
            java.lang.Object r4 = r2.next()     // Catch:{ JSONException -> 0x0289 }
            com.google.android.gms.internal.cast.zzed r4 = (com.google.android.gms.internal.cast.zzed) r4     // Catch:{ JSONException -> 0x0289 }
            r4.zzc(r5, r3, r13)     // Catch:{ JSONException -> 0x0289 }
            goto L_0x0278
        L_0x0288:
            return
        L_0x0289:
            r2 = move-exception
            com.google.android.gms.internal.cast.zzdw r4 = r14.zzxw
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r2 = r2.getMessage()
            r0[r3] = r2
            r0[r1] = r15
            java.lang.String r15 = "Message is malformed (%s); ignoring: %s"
            r4.w(r15, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzdx.zzo(java.lang.String):void");
    }

    public final long zza(zzec zzec, MediaInfo mediaInfo, zzam zzam, MediaLoadOptions mediaLoadOptions) throws IllegalStateException, IllegalArgumentException {
        if (mediaInfo == null && zzam == null) {
            throw new IllegalArgumentException("MediaInfo should not be null");
        }
        JSONObject jSONObject = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject.put("requestId", zzes);
            jSONObject.put("type", "LOAD");
            if (mediaInfo != null) {
                jSONObject.put("media", mediaInfo.D());
            }
            if (zzam != null) {
                jSONObject.put("queueData", zzam.a());
            }
            jSONObject.put(AudienceNetworkActivity.AUTOPLAY, mediaLoadOptions.b());
            jSONObject.put("currentTime", ((double) mediaLoadOptions.f()) / 1000.0d);
            jSONObject.put("playbackRate", mediaLoadOptions.g());
            if (mediaLoadOptions.c() != null) {
                jSONObject.put("credentials", mediaLoadOptions.c());
            }
            if (mediaLoadOptions.d() != null) {
                jSONObject.put("credentialsType", mediaLoadOptions.d());
            }
            long[] a2 = mediaLoadOptions.a();
            if (a2 != null) {
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < a2.length; i++) {
                    jSONArray.put(i, a2[i]);
                }
                jSONObject.put("activeTrackIds", jSONArray);
            }
            JSONObject e = mediaLoadOptions.e();
            if (e != null) {
                jSONObject.put("customData", e);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject.toString(), zzes, (String) null);
        this.zzzw.zza(zzes, zzec);
        return zzes;
    }

    public final long zzb(zzec zzec, double d, JSONObject jSONObject) throws IllegalStateException, zzea {
        if (this.zzzt != null) {
            JSONObject jSONObject2 = new JSONObject();
            long zzes = zzes();
            try {
                jSONObject2.put("requestId", zzes);
                jSONObject2.put("type", "SET_PLAYBACK_RATE");
                jSONObject2.put("playbackRate", d);
                jSONObject2.put("mediaSessionId", this.zzzt.J());
                if (jSONObject != null) {
                    jSONObject2.put("customData", jSONObject);
                }
            } catch (JSONException unused) {
            }
            zza(jSONObject2.toString(), zzes, (String) null);
            this.zzaan.zza(zzes, zzec);
            return zzes;
        }
        throw new zzea();
    }

    public final long zzc(zzec zzec) throws zzea, IllegalStateException {
        JSONObject jSONObject = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject.put("requestId", zzes);
            jSONObject.put("type", "QUEUE_GET_ITEM_IDS");
            jSONObject.put("mediaSessionId", zzp());
        } catch (JSONException unused) {
        }
        zza(jSONObject.toString(), zzes, (String) null);
        this.zzaak.zza(zzes, zzec);
        return zzes;
    }

    public final long zzb(zzec zzec) throws IllegalStateException {
        JSONObject jSONObject = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject.put("requestId", zzes);
            jSONObject.put("type", "GET_STATUS");
            if (this.zzzt != null) {
                jSONObject.put("mediaSessionId", this.zzzt.J());
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject.toString(), zzes, (String) null);
        this.zzaad.zza(zzes, zzec);
        return zzes;
    }

    public final long zza(zzec zzec, JSONObject jSONObject) throws IllegalStateException, zzea {
        JSONObject jSONObject2 = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject2.put("requestId", zzes);
            jSONObject2.put("type", "PAUSE");
            jSONObject2.put("mediaSessionId", zzp());
            if (jSONObject != null) {
                jSONObject2.put("customData", jSONObject);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject2.toString(), zzes, (String) null);
        this.zzzx.zza(zzes, zzec);
        return zzes;
    }

    public final long zzb(String str, List<zzbx> list) throws IllegalStateException {
        long zzes = zzes();
        zza(zza(str, list, zzes), zzes, (String) null);
        return zzes;
    }

    private static int[] zzb(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null) {
            return null;
        }
        int[] iArr = new int[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            iArr[i] = jSONArray.getInt(i);
        }
        return iArr;
    }

    public final long zza(zzec zzec, zzas zzas) throws IllegalStateException, zzea {
        JSONObject jSONObject = new JSONObject();
        long zzes = zzes();
        long b = zzas.d() ? 4294967296000L : zzas.b();
        try {
            jSONObject.put("requestId", zzes);
            jSONObject.put("type", "SEEK");
            jSONObject.put("mediaSessionId", zzp());
            jSONObject.put("currentTime", ((double) b) / 1000.0d);
            if (zzas.c() == 1) {
                jSONObject.put("resumeState", "PLAYBACK_START");
            } else if (zzas.c() == 2) {
                jSONObject.put("resumeState", "PLAYBACK_PAUSE");
            }
            if (zzas.a() != null) {
                jSONObject.put("customData", zzas.a());
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject.toString(), zzes, (String) null);
        this.zzzu = Long.valueOf(b);
        this.zzaaa.zza(zzes, (zzec) new zzdy(this, zzec));
        return zzes;
    }

    public final long zza(zzec zzec) throws IllegalStateException, zzea {
        JSONObject jSONObject = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject.put("requestId", zzes);
            jSONObject.put("type", "SKIP_AD");
            jSONObject.put("mediaSessionId", zzp());
        } catch (JSONException e) {
            this.zzxw.w(String.format(Locale.ROOT, "Error creating SkipAd message: %s", new Object[]{e.getMessage()}), new Object[0]);
        }
        zza(jSONObject.toString(), zzes, (String) null);
        this.zzaao.zza(zzes, zzec);
        return zzes;
    }

    public final long zza(zzec zzec, double d, JSONObject jSONObject) throws IllegalStateException, zzea, IllegalArgumentException {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            StringBuilder sb = new StringBuilder(41);
            sb.append("Volume cannot be ");
            sb.append(d);
            throw new IllegalArgumentException(sb.toString());
        }
        JSONObject jSONObject2 = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject2.put("requestId", zzes);
            jSONObject2.put("type", "SET_VOLUME");
            jSONObject2.put("mediaSessionId", zzp());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(AppLovinEventTypes.USER_COMPLETED_LEVEL, d);
            jSONObject2.put("volume", jSONObject3);
            if (jSONObject != null) {
                jSONObject2.put("customData", jSONObject);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject2.toString(), zzes, (String) null);
        this.zzaab.zza(zzes, zzec);
        return zzes;
    }

    public final long zza(zzec zzec, boolean z, JSONObject jSONObject) throws IllegalStateException, zzea {
        JSONObject jSONObject2 = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject2.put("requestId", zzes);
            jSONObject2.put("type", "SET_VOLUME");
            jSONObject2.put("mediaSessionId", zzp());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("muted", z);
            jSONObject2.put("volume", jSONObject3);
            if (jSONObject != null) {
                jSONObject2.put("customData", jSONObject);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject2.toString(), zzes, (String) null);
        this.zzaac.zza(zzes, zzec);
        return zzes;
    }

    public final long zza(zzec zzec, long[] jArr) throws IllegalStateException, zzea {
        if (jArr != null) {
            JSONObject jSONObject = new JSONObject();
            long zzes = zzes();
            try {
                jSONObject.put("requestId", zzes);
                jSONObject.put("type", "EDIT_TRACKS_INFO");
                jSONObject.put("mediaSessionId", zzp());
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < jArr.length; i++) {
                    jSONArray.put(i, jArr[i]);
                }
                jSONObject.put("activeTrackIds", jSONArray);
            } catch (JSONException unused) {
            }
            zza(jSONObject.toString(), zzes, (String) null);
            this.zzaae.zza(zzes, zzec);
            return zzes;
        }
        throw new IllegalArgumentException("trackIds cannot be null");
    }

    public final long zza(zzec zzec, TextTrackStyle textTrackStyle) throws IllegalStateException, zzea {
        if (textTrackStyle != null) {
            JSONObject jSONObject = new JSONObject();
            long zzes = zzes();
            try {
                jSONObject.put("requestId", zzes);
                jSONObject.put("type", "EDIT_TRACKS_INFO");
                if (textTrackStyle != null) {
                    jSONObject.put("textTrackStyle", textTrackStyle.D());
                }
                jSONObject.put("mediaSessionId", zzp());
            } catch (JSONException unused) {
            }
            zza(jSONObject.toString(), zzes, (String) null);
            this.zzaaf.zza(zzes, zzec);
            return zzes;
        }
        throw new IllegalArgumentException("trackStyle cannot be null");
    }

    private final long zza(double d, long j, long j2) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.zzzs;
        if (elapsedRealtime < 0) {
            elapsedRealtime = 0;
        }
        if (elapsedRealtime == 0) {
            return j;
        }
        long j3 = ((long) (((double) elapsedRealtime) * d)) + j;
        if (j2 > 0 && j3 > j2) {
            return j2;
        }
        if (j3 < 0) {
            return 0;
        }
        return j3;
    }

    public final long zza(zzec zzec, MediaQueueItem[] mediaQueueItemArr, zzam zzam, zzap zzap) throws IllegalStateException, IllegalArgumentException {
        if (mediaQueueItemArr == null || mediaQueueItemArr.length == 0) {
            throw new IllegalArgumentException("items must not be null or empty.");
        }
        zzap.a();
        throw null;
    }

    public final long zza(zzec zzec, MediaQueueItem[] mediaQueueItemArr, int i, int i2, int i3, long j, JSONObject jSONObject) throws IllegalStateException, zzea, IllegalArgumentException {
        MediaQueueItem[] mediaQueueItemArr2 = mediaQueueItemArr;
        int i4 = i;
        int i5 = i3;
        long j2 = j;
        JSONObject jSONObject2 = jSONObject;
        if (mediaQueueItemArr2 == null || mediaQueueItemArr2.length == 0) {
            throw new IllegalArgumentException("itemsToInsert must not be null or empty.");
        }
        if (i5 == -1 || (i5 >= 0 && i5 < mediaQueueItemArr2.length)) {
            int i6 = (j2 > -1 ? 1 : (j2 == -1 ? 0 : -1));
            if (i6 == 0 || j2 >= 0) {
                JSONObject jSONObject3 = new JSONObject();
                long zzes = zzes();
                try {
                    jSONObject3.put("requestId", zzes);
                    jSONObject3.put("type", "QUEUE_INSERT");
                    jSONObject3.put("mediaSessionId", zzp());
                    JSONArray jSONArray = new JSONArray();
                    for (int i7 = 0; i7 < mediaQueueItemArr2.length; i7++) {
                        jSONArray.put(i7, mediaQueueItemArr2[i7].z());
                    }
                    jSONObject3.put("items", jSONArray);
                    if (i4 != 0) {
                        jSONObject3.put("insertBefore", i4);
                    }
                    if (i5 != -1) {
                        jSONObject3.put("currentItemIndex", i5);
                    }
                    if (i6 != 0) {
                        jSONObject3.put("currentTime", ((double) j2) / 1000.0d);
                    }
                    if (jSONObject2 != null) {
                        jSONObject3.put("customData", jSONObject2);
                    }
                } catch (JSONException unused) {
                }
                zza(jSONObject3.toString(), zzes, (String) null);
                this.zzaag.zza(zzes, zzec);
                return zzes;
            }
            StringBuilder sb = new StringBuilder(54);
            sb.append("playPosition can not be negative: ");
            sb.append(j2);
            throw new IllegalArgumentException(sb.toString());
        }
        throw new IllegalArgumentException(String.format(Locale.ROOT, "currentItemIndexInItemsToInsert %d out of range [0, %d).", new Object[]{Integer.valueOf(i3), Integer.valueOf(mediaQueueItemArr2.length)}));
    }

    public final long zza(zzec zzec, int i, long j, MediaQueueItem[] mediaQueueItemArr, int i2, Integer num, JSONObject jSONObject) throws IllegalArgumentException, IllegalStateException, zzea {
        int i3 = (j > -1 ? 1 : (j == -1 ? 0 : -1));
        if (i3 == 0 || j >= 0) {
            JSONObject jSONObject2 = new JSONObject();
            long zzes = zzes();
            try {
                jSONObject2.put("requestId", zzes);
                jSONObject2.put("type", "QUEUE_UPDATE");
                jSONObject2.put("mediaSessionId", zzp());
                if (i != 0) {
                    jSONObject2.put("currentItemId", i);
                }
                if (i2 != 0) {
                    jSONObject2.put("jump", i2);
                }
                if (mediaQueueItemArr != null && mediaQueueItemArr.length > 0) {
                    JSONArray jSONArray = new JSONArray();
                    for (int i4 = 0; i4 < mediaQueueItemArr.length; i4++) {
                        jSONArray.put(i4, mediaQueueItemArr[i4].z());
                    }
                    jSONObject2.put("items", jSONArray);
                }
                String zza = zzef.zza(num);
                if (zza != null) {
                    jSONObject2.put("repeatMode", zza);
                }
                if (i3 != 0) {
                    jSONObject2.put("currentTime", ((double) j) / 1000.0d);
                }
                if (jSONObject != null) {
                    jSONObject2.put("customData", jSONObject);
                }
            } catch (JSONException unused) {
            }
            zza(jSONObject2.toString(), zzes, (String) null);
            this.zzaah.zza(zzes, zzec);
            return zzes;
        }
        StringBuilder sb = new StringBuilder(53);
        sb.append("playPosition cannot be negative: ");
        sb.append(j);
        throw new IllegalArgumentException(sb.toString());
    }

    public final long zza(zzec zzec, int[] iArr, JSONObject jSONObject) throws IllegalStateException, zzea, IllegalArgumentException {
        if (iArr == null || iArr.length == 0) {
            throw new IllegalArgumentException("itemIdsToRemove must not be null or empty.");
        }
        JSONObject jSONObject2 = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject2.put("requestId", zzes);
            jSONObject2.put("type", "QUEUE_REMOVE");
            jSONObject2.put("mediaSessionId", zzp());
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < iArr.length; i++) {
                jSONArray.put(i, iArr[i]);
            }
            jSONObject2.put("itemIds", jSONArray);
            if (jSONObject != null) {
                jSONObject2.put("customData", jSONObject);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject2.toString(), zzes, (String) null);
        this.zzaai.zza(zzes, zzec);
        return zzes;
    }

    public final long zza(zzec zzec, int[] iArr, int i, JSONObject jSONObject) throws IllegalStateException, zzea, IllegalArgumentException {
        if (iArr == null || iArr.length == 0) {
            throw new IllegalArgumentException("itemIdsToReorder must not be null or empty.");
        }
        JSONObject jSONObject2 = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject2.put("requestId", zzes);
            jSONObject2.put("type", "QUEUE_REORDER");
            jSONObject2.put("mediaSessionId", zzp());
            JSONArray jSONArray = new JSONArray();
            for (int i2 = 0; i2 < iArr.length; i2++) {
                jSONArray.put(i2, iArr[i2]);
            }
            jSONObject2.put("itemIds", jSONArray);
            if (i != 0) {
                jSONObject2.put("insertBefore", i);
            }
            if (jSONObject != null) {
                jSONObject2.put("customData", jSONObject);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject2.toString(), zzes, (String) null);
        this.zzaaj.zza(zzes, zzec);
        return zzes;
    }

    public final long zza(zzec zzec, int i, int i2, int i3) throws zzea, IllegalArgumentException {
        if ((i2 <= 0 || i3 != 0) && (i2 != 0 || i3 <= 0)) {
            throw new IllegalArgumentException("Exactly one of nextCount and prevCount must be positive and the other must be zero");
        }
        JSONObject jSONObject = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject.put("requestId", zzes);
            jSONObject.put("type", "QUEUE_GET_ITEM_RANGE");
            jSONObject.put("mediaSessionId", zzp());
            jSONObject.put("itemId", i);
            if (i2 > 0) {
                jSONObject.put("nextCount", i2);
            }
            if (i3 > 0) {
                jSONObject.put("prevCount", i3);
            }
        } catch (JSONException unused) {
        }
        zza(jSONObject.toString(), zzes, (String) null);
        this.zzaam.zza(zzes, zzec);
        return zzes;
    }

    public final long zza(zzec zzec, int[] iArr) throws zzea, IllegalArgumentException {
        JSONObject jSONObject = new JSONObject();
        long zzes = zzes();
        try {
            jSONObject.put("requestId", zzes);
            jSONObject.put("type", "QUEUE_GET_ITEMS");
            jSONObject.put("mediaSessionId", zzp());
            JSONArray jSONArray = new JSONArray();
            for (int put : iArr) {
                jSONArray.put(put);
            }
            jSONObject.put("itemIds", jSONArray);
        } catch (JSONException unused) {
        }
        zza(jSONObject.toString(), zzes, (String) null);
        this.zzaal.zza(zzes, zzec);
        return zzes;
    }

    private static String zza(String str, List<zzbx> list, long j) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("requestId", j);
            jSONObject.put("type", "PRECACHE");
            if (str != null) {
                jSONObject.put("precacheData", str);
            }
            if (list != null && !list.isEmpty()) {
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < list.size(); i++) {
                    jSONArray.put(i, list.get(i).s());
                }
                jSONObject.put("requestItems", jSONArray);
            }
        } catch (JSONException unused) {
        }
        return jSONObject.toString();
    }

    public final void zza(long j, int i) {
        for (zzed zzc : zzer()) {
            zzc.zzc(j, i, (Object) null);
        }
    }
}
