package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public abstract class zzdgr<V> extends zzdgo<V> implements zzdhe<V> {
    protected zzdgr() {
    }

    public void addListener(Runnable runnable, Executor executor) {
        zzaru().addListener(runnable, executor);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzarv */
    public abstract zzdhe<? extends V> zzaru();
}
