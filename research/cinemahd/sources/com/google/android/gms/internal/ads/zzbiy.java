package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.Map;
import java.util.concurrent.Executor;

public final class zzbiy {
    /* access modifiers changed from: private */
    public final Executor executor;
    private final String zzblz;
    private final zzakh zzfbx;
    /* access modifiers changed from: private */
    public zzbjd zzfby;
    private final zzafn<Object> zzfbz = new zzbix(this);
    private final zzafn<Object> zzfca = new zzbiz(this);

    public zzbiy(String str, zzakh zzakh, Executor executor2) {
        this.zzblz = str;
        this.zzfbx = zzakh;
        this.executor = executor2;
    }

    /* access modifiers changed from: private */
    public final boolean zzl(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        String str = map.get("hashCode");
        if (TextUtils.isEmpty(str) || !str.equals(this.zzblz)) {
            return false;
        }
        return true;
    }

    public final void zza(zzbjd zzbjd) {
        this.zzfbx.zzc("/updateActiveView", this.zzfbz);
        this.zzfbx.zzc("/untrackActiveViewUnit", this.zzfca);
        this.zzfby = zzbjd;
    }

    public final void zzafm() {
        this.zzfbx.zzd("/updateActiveView", this.zzfbz);
        this.zzfbx.zzd("/untrackActiveViewUnit", this.zzfca);
    }

    public final void zzd(zzbdi zzbdi) {
        zzbdi.zza("/updateActiveView", (zzafn<? super zzbdi>) this.zzfbz);
        zzbdi.zza("/untrackActiveViewUnit", (zzafn<? super zzbdi>) this.zzfca);
    }

    public final void zze(zzbdi zzbdi) {
        zzbdi.zzb("/updateActiveView", (zzafn<? super zzbdi>) this.zzfbz);
        zzbdi.zzb("/untrackActiveViewUnit", (zzafn<? super zzbdi>) this.zzfca);
    }
}
