package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public interface zzdop {
    int zzasr();

    zzdhx zzm(byte[] bArr) throws GeneralSecurityException;
}
