package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.charset.Charset;

class zzdqu extends zzdqr {
    protected final byte[] zzhid;

    zzdqu(byte[] bArr) {
        if (bArr != null) {
            this.zzhid = bArr;
            return;
        }
        throw new NullPointerException();
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzdqk) || size() != ((zzdqk) obj).size()) {
            return false;
        }
        if (size() == 0) {
            return true;
        }
        if (!(obj instanceof zzdqu)) {
            return obj.equals(this);
        }
        zzdqu zzdqu = (zzdqu) obj;
        int zzaxy = zzaxy();
        int zzaxy2 = zzdqu.zzaxy();
        if (zzaxy == 0 || zzaxy2 == 0 || zzaxy == zzaxy2) {
            return zza(zzdqu, 0, size());
        }
        return false;
    }

    public int size() {
        return this.zzhid.length;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdqh zzdqh) throws IOException {
        zzdqh.zzh(this.zzhid, zzaxz(), size());
    }

    public final boolean zzaxu() {
        int zzaxz = zzaxz();
        return zzdva.zzm(this.zzhid, zzaxz, size() + zzaxz);
    }

    public final zzdqw zzaxv() {
        return zzdqw.zzb(this.zzhid, zzaxz(), size(), true);
    }

    /* access modifiers changed from: protected */
    public int zzaxz() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void zzb(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.zzhid, i, bArr, i2, i3);
    }

    public byte zzfe(int i) {
        return this.zzhid[i];
    }

    /* access modifiers changed from: package-private */
    public byte zzff(int i) {
        return this.zzhid[i];
    }

    /* access modifiers changed from: protected */
    public final int zzg(int i, int i2, int i3) {
        int zzaxz = zzaxz() + i2;
        return zzdva.zzb(i, this.zzhid, zzaxz, i3 + zzaxz);
    }

    /* access modifiers changed from: protected */
    public final int zzh(int i, int i2, int i3) {
        return zzdrv.zza(i, this.zzhid, zzaxz() + i2, i3);
    }

    public final zzdqk zzy(int i, int i2) {
        int zzi = zzdqk.zzi(i, i2, size());
        if (zzi == 0) {
            return zzdqk.zzhhx;
        }
        return new zzdqn(this.zzhid, zzaxz() + i, zzi);
    }

    /* access modifiers changed from: protected */
    public final String zza(Charset charset) {
        return new String(this.zzhid, zzaxz(), size(), charset);
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(zzdqk zzdqk, int i, int i2) {
        if (i2 <= zzdqk.size()) {
            int i3 = i + i2;
            if (i3 > zzdqk.size()) {
                int size = zzdqk.size();
                StringBuilder sb = new StringBuilder(59);
                sb.append("Ran off end of other: ");
                sb.append(i);
                sb.append(", ");
                sb.append(i2);
                sb.append(", ");
                sb.append(size);
                throw new IllegalArgumentException(sb.toString());
            } else if (!(zzdqk instanceof zzdqu)) {
                return zzdqk.zzy(i, i3).equals(zzy(0, i2));
            } else {
                zzdqu zzdqu = (zzdqu) zzdqk;
                byte[] bArr = this.zzhid;
                byte[] bArr2 = zzdqu.zzhid;
                int zzaxz = zzaxz() + i2;
                int zzaxz2 = zzaxz();
                int zzaxz3 = zzdqu.zzaxz() + i;
                while (zzaxz2 < zzaxz) {
                    if (bArr[zzaxz2] != bArr2[zzaxz3]) {
                        return false;
                    }
                    zzaxz2++;
                    zzaxz3++;
                }
                return true;
            }
        } else {
            int size2 = size();
            StringBuilder sb2 = new StringBuilder(40);
            sb2.append("Length too large: ");
            sb2.append(i2);
            sb2.append(size2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }
}
