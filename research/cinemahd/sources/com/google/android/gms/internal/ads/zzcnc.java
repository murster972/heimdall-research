package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcnc implements zzdby {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcip zzgau;
    private final zzcna zzgbf;

    zzcnc(zzcna zzcna, zzczt zzczt, zzczl zzczl, zzcip zzcip) {
        this.zzgbf = zzcna;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
        this.zzgau = zzcip;
    }

    public final Object apply(Object obj) {
        return this.zzgbf.zza(this.zzfot, this.zzfel, this.zzgau, (Void) obj);
    }
}
