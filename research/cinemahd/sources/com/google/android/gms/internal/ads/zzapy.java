package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class zzapy implements Parcelable.Creator<zzapz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        boolean z = false;
        ArrayList<String> arrayList = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                z = SafeParcelReader.s(parcel, a2);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                arrayList = SafeParcelReader.q(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzapz(z, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzapz[i];
    }
}
