package com.google.android.gms.internal.ads;

import android.os.IBinder;

final /* synthetic */ class zzaop implements zzayw {
    static final zzayw zzbtz = new zzaop();

    private zzaop() {
    }

    public final Object apply(Object obj) {
        return zzdde.zzap((IBinder) obj);
    }
}
