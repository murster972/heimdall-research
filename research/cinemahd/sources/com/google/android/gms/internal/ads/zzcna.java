package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.zzbpu;
import java.util.Iterator;

public final class zzcna<AdT, AdapterT, ListenerT extends zzbpu> implements zzcio<AdT> {
    private final zzcis<AdapterT, ListenerT> zzfaq;
    private final zzdcr zzfgm;
    private final zzcir<AdT, AdapterT, ListenerT> zzgbg;
    private final zzdhd zzgbh;

    public zzcna(zzdcr zzdcr, zzdhd zzdhd, zzcis<AdapterT, ListenerT> zzcis, zzcir<AdT, AdapterT, ListenerT> zzcir) {
        this.zzfgm = zzdcr;
        this.zzgbh = zzdhd;
        this.zzgbg = zzcir;
        this.zzfaq = zzcis;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return !zzczl.zzglp.isEmpty();
    }

    public final zzdhe<AdT> zzb(zzczt zzczt, zzczl zzczl) {
        zzcip<AdapterT, ListenerT> zzcip;
        Class<AdMobAdapter> cls = AdMobAdapter.class;
        Iterator<String> it2 = zzczl.zzglp.iterator();
        while (true) {
            if (!it2.hasNext()) {
                zzcip = null;
                break;
            }
            try {
                zzcip = this.zzfaq.zzd(it2.next(), zzczl.zzglr);
                break;
            } catch (zzdab unused) {
            }
        }
        if (zzcip == null) {
            return zzdgs.zzk(new zzclf("unable to instantiate mediation adapter class"));
        }
        zzazl zzazl = new zzazl();
        zzcip.zzfyf.zza(new zzcnb(this, zzazl, zzcip));
        if (zzczl.zzdmf) {
            Bundle bundle = zzczt.zzgmh.zzfgl.zzgml.zzccf;
            Bundle bundle2 = bundle.getBundle(cls.getName());
            if (bundle2 == null) {
                bundle2 = new Bundle();
                bundle.putBundle(cls.getName(), bundle2);
            }
            bundle2.putBoolean("render_test_ad_label", true);
        }
        return this.zzfgm.zzu(zzdco.ADAPTER_LOAD_AD_SYN).zza((zzdcb) new zzcmz(this, zzczt, zzczl, zzcip), this.zzgbh).zzw(zzdco.ADAPTER_LOAD_AD_ACK).zzc(zzazl).zzw(zzdco.ADAPTER_WRAP_ADAPTER).zzb(new zzcnc(this, zzczt, zzczl, zzcip)).zzaqg();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws Exception {
        this.zzgbg.zza(zzczt, zzczl, zzcip);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(zzczt zzczt, zzczl zzczl, zzcip zzcip, Void voidR) throws Exception {
        return this.zzgbg.zzb(zzczt, zzczl, zzcip);
    }
}
