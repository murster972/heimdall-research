package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;

final class zzen implements Runnable {
    private final /* synthetic */ zzei zzyc;
    private final /* synthetic */ int zzyf;
    private final /* synthetic */ boolean zzyg;

    zzen(zzei zzei, int i, boolean z) {
        this.zzyc = zzei;
        this.zzyf = i;
        this.zzyg = z;
    }

    public final void run() {
        zzbs.zza zzb = this.zzyc.zzb(this.zzyf, this.zzyg);
        zzbs.zza unused = this.zzyc.zzxr = zzb;
        if (zzei.zza(this.zzyf, zzb)) {
            this.zzyc.zza(this.zzyf + 1, this.zzyg);
        }
    }
}
