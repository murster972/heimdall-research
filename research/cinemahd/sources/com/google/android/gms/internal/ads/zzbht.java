package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public final class zzbht extends zzwj {
    private final zzazb zzbll;
    private final zzatv zzbng;
    private final zzcka zzfap;
    private final zzcis<zzdac, zzcjx> zzfaq;
    private final zzcob zzfar;
    private final zzceq zzfas;
    private final Context zzup;
    private boolean zzxx = false;

    zzbht(Context context, zzazb zzazb, zzcka zzcka, zzcis<zzdac, zzcjx> zzcis, zzcob zzcob, zzceq zzceq, zzatv zzatv) {
        this.zzup = context;
        this.zzbll = zzazb;
        this.zzfap = zzcka;
        this.zzfaq = zzcis;
        this.zzfar = zzcob;
        this.zzfas = zzceq;
        this.zzbng = zzatv;
    }

    private final String zzaez() {
        Context applicationContext = this.zzup.getApplicationContext() == null ? this.zzup : this.zzup.getApplicationContext();
        try {
            String string = Wrappers.a(applicationContext).a(applicationContext.getPackageName(), 128).metaData.getString("com.google.android.gms.ads.APPLICATION_ID");
            if (TextUtils.isEmpty(string)) {
                return "";
            }
            if (string.matches("^ca-app-pub-[0-9]{16}~[0-9]{10}$") || string.matches("^/\\d+~.+$")) {
                return string;
            }
            return "";
        } catch (PackageManager.NameNotFoundException | NullPointerException e) {
            zzavs.zza("Error getting metadata", e);
            return "";
        }
    }

    public final String getVersionString() {
        return this.zzbll.zzbma;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0045, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void initialize() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.zzxx     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x000c
            java.lang.String r0 = "Mobile ads is initialized already."
            com.google.android.gms.internal.ads.zzayu.zzez(r0)     // Catch:{ all -> 0x0046 }
            monitor-exit(r3)
            return
        L_0x000c:
            android.content.Context r0 = r3.zzup     // Catch:{ all -> 0x0046 }
            com.google.android.gms.internal.ads.zzzn.initialize(r0)     // Catch:{ all -> 0x0046 }
            com.google.android.gms.internal.ads.zzave r0 = com.google.android.gms.ads.internal.zzq.zzku()     // Catch:{ all -> 0x0046 }
            android.content.Context r1 = r3.zzup     // Catch:{ all -> 0x0046 }
            com.google.android.gms.internal.ads.zzazb r2 = r3.zzbll     // Catch:{ all -> 0x0046 }
            r0.zzd(r1, r2)     // Catch:{ all -> 0x0046 }
            com.google.android.gms.internal.ads.zzrq r0 = com.google.android.gms.ads.internal.zzq.zzkw()     // Catch:{ all -> 0x0046 }
            android.content.Context r1 = r3.zzup     // Catch:{ all -> 0x0046 }
            r0.initialize(r1)     // Catch:{ all -> 0x0046 }
            r0 = 1
            r3.zzxx = r0     // Catch:{ all -> 0x0046 }
            com.google.android.gms.internal.ads.zzceq r0 = r3.zzfas     // Catch:{ all -> 0x0046 }
            r0.zzall()     // Catch:{ all -> 0x0046 }
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcko     // Catch:{ all -> 0x0046 }
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x0046 }
            java.lang.Object r0 = r1.zzd(r0)     // Catch:{ all -> 0x0046 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x0046 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0044
            com.google.android.gms.internal.ads.zzcob r0 = r3.zzfar     // Catch:{ all -> 0x0046 }
            r0.zzamj()     // Catch:{ all -> 0x0046 }
        L_0x0044:
            monitor-exit(r3)
            return
        L_0x0046:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbht.initialize():void");
    }

    public final synchronized void setAppMuted(boolean z) {
        zzq.zzkv().setAppMuted(z);
    }

    public final synchronized void setAppVolume(float f) {
        zzq.zzkv().setAppVolume(f);
    }

    public final void zza(String str, IObjectWrapper iObjectWrapper) {
        zzzn.initialize(this.zzup);
        String zzaez = ((Boolean) zzve.zzoy().zzd(zzzn.zzcms)).booleanValue() ? zzaez() : "";
        if (!TextUtils.isEmpty(zzaez)) {
            str = zzaez;
        }
        if (!TextUtils.isEmpty(str)) {
            boolean booleanValue = ((Boolean) zzve.zzoy().zzd(zzzn.zzcmr)).booleanValue() | ((Boolean) zzve.zzoy().zzd(zzzn.zzcix)).booleanValue();
            zzbhw zzbhw = null;
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcix)).booleanValue()) {
                booleanValue = true;
                zzbhw = new zzbhw(this, (Runnable) ObjectWrapper.a(iObjectWrapper));
            }
            if (booleanValue) {
                zzq.zzky().zza(this.zzup, this.zzbll, str, (Runnable) zzbhw);
            }
        }
    }

    public final void zzb(IObjectWrapper iObjectWrapper, String str) {
        if (iObjectWrapper == null) {
            zzayu.zzex("Wrapped context is null. Failed to open debug menu.");
            return;
        }
        Context context = (Context) ObjectWrapper.a(iObjectWrapper);
        if (context == null) {
            zzayu.zzex("Context is null. Failed to open debug menu.");
            return;
        }
        zzawt zzawt = new zzawt(context);
        zzawt.setAdUnitId(str);
        zzawt.zzx(this.zzbll.zzbma);
        zzawt.showDialog();
    }

    public final synchronized void zzcd(String str) {
        zzzn.initialize(this.zzup);
        if (!TextUtils.isEmpty(str)) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcmr)).booleanValue()) {
                zzq.zzky().zza(this.zzup, this.zzbll, str, (Runnable) null);
            }
        }
    }

    public final void zzce(String str) {
        this.zzfar.zzgi(str);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(Runnable runnable) {
        Preconditions.a("Adapters must be initialized on the main thread.");
        Map<String, zzakx> zzvn = zzq.zzku().zzvf().zzwa().zzvn();
        if (zzvn != null && !zzvn.isEmpty()) {
            if (runnable != null) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    zzayu.zzd("Could not initialize rewarded ads.", th);
                    return;
                }
            }
            if (this.zzfap.zzamf()) {
                HashMap hashMap = new HashMap();
                for (zzakx zzakx : zzvn.values()) {
                    for (zzaky next : zzakx.zzdbo) {
                        String str = next.zzdct;
                        for (String next2 : next.zzdcl) {
                            if (!hashMap.containsKey(next2)) {
                                hashMap.put(next2, new ArrayList());
                            }
                            if (str != null) {
                                ((Collection) hashMap.get(next2)).add(str);
                            }
                        }
                    }
                }
                JSONObject jSONObject = new JSONObject();
                for (Map.Entry entry : hashMap.entrySet()) {
                    String str2 = (String) entry.getKey();
                    try {
                        zzcip<zzdac, zzcjx> zzd = this.zzfaq.zzd(str2, jSONObject);
                        if (zzd != null) {
                            zzdac zzdac = (zzdac) zzd.zzddn;
                            if (!zzdac.isInitialized()) {
                                if (zzdac.zzsp()) {
                                    zzdac.zza(this.zzup, (zzarz) (zzcjx) zzd.zzfyf, (List<String>) (List) entry.getValue());
                                    String valueOf = String.valueOf(str2);
                                    zzayu.zzea(valueOf.length() != 0 ? "Initialized rewarded video mediation adapter ".concat(valueOf) : new String("Initialized rewarded video mediation adapter "));
                                }
                            }
                        }
                    } catch (zzdab e) {
                        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 56);
                        sb.append("Failed to initialize rewarded video mediation adapter \"");
                        sb.append(str2);
                        sb.append("\"");
                        zzayu.zzd(sb.toString(), e);
                    }
                }
            }
        }
    }

    public final synchronized float zzpe() {
        return zzq.zzkv().zzpe();
    }

    public final synchronized boolean zzpf() {
        return zzq.zzkv().zzpf();
    }

    public final List<zzagn> zzpg() throws RemoteException {
        return this.zzfas.zzalm();
    }

    public final void zza(zzalc zzalc) throws RemoteException {
        this.zzfap.zzb(zzalc);
    }

    public final void zza(zzagu zzagu) throws RemoteException {
        this.zzfas.zzb(zzagu);
    }

    public final void zza(zzyq zzyq) throws RemoteException {
        this.zzbng.zza(this.zzup, zzyq);
    }
}
