package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

public final class zzbsg implements zzdxg<Set<zzbsu<zzbsz>>> {
    private final zzbrm zzfim;

    private zzbsg(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbsg zzx(zzbrm zzbrm) {
        return new zzbsg(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.emptySet(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
