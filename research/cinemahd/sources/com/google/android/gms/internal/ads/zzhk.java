package com.google.android.gms.internal.ads;

import com.facebook.imagepipeline.memory.BitmapCounterConfig;
import com.facebook.imageutils.JfifUtil;
import com.google.android.gms.ads.AdRequest;
import java.nio.ByteBuffer;
import java.util.List;

public final class zzhk {
    private static final int[] zzagm = {1, 2, 3, 6};
    private static final int[] zzagn = {48000, 44100, 32000};
    private static final int[] zzago = {24000, 22050, 16000};
    private static final int[] zzagp = {2, 1, 2, 3, 3, 4, 4, 5};
    private static final int[] zzagq = {32, 40, 48, 56, 64, 80, 96, 112, 128, 160, JfifUtil.MARKER_SOFn, 224, 256, 320, BitmapCounterConfig.DEFAULT_MAX_BITMAP_COUNT, 448, AdRequest.MAX_CONTENT_URL_LENGTH, 576, 640};
    private static final int[] zzagr = {69, 87, 104, 121, 139, 174, JfifUtil.MARKER_RST0, 243, 278, 348, 417, 487, 557, 696, 835, 975, 1114, 1253, 1393};

    public static zzgw zza(zzoj zzoj, String str, String str2, zziv zziv) {
        int i = zzagn[(zzoj.readUnsignedByte() & JfifUtil.MARKER_SOFn) >> 6];
        int readUnsignedByte = zzoj.readUnsignedByte();
        int i2 = zzagp[(readUnsignedByte & 56) >> 3];
        if ((readUnsignedByte & 4) != 0) {
            i2++;
        }
        return zzgw.zza(str, "audio/ac3", (String) null, -1, -1, i2, i, (List<byte[]>) null, (zziv) null, 0, str2);
    }

    public static zzgw zzb(zzoj zzoj, String str, String str2, zziv zziv) {
        zzoj.zzbf(2);
        int i = zzagn[(zzoj.readUnsignedByte() & JfifUtil.MARKER_SOFn) >> 6];
        int readUnsignedByte = zzoj.readUnsignedByte();
        int i2 = zzagp[(readUnsignedByte & 14) >> 1];
        if ((readUnsignedByte & 1) != 0) {
            i2++;
        }
        return zzgw.zza(str, "audio/eac3", (String) null, -1, -1, i2, i, (List<byte[]>) null, (zziv) null, 0, str2);
    }

    public static int zzey() {
        return 1536;
    }

    public static int zzh(ByteBuffer byteBuffer) {
        int i = 6;
        if (((byteBuffer.get(byteBuffer.position() + 4) & 192) >> 6) != 3) {
            i = zzagm[(byteBuffer.get(byteBuffer.position() + 4) & 48) >> 4];
        }
        return i * 256;
    }
}
