package com.google.android.gms.internal.ads;

import java.io.IOException;

public interface zznx {
    void cancelLoad();

    boolean zzhp();

    void zzhq() throws IOException, InterruptedException;
}
