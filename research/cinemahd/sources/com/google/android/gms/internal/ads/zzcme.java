package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcme implements zzdxg<zzcma> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzcbi> zzfyl;

    public zzcme(zzdxp<Context> zzdxp, zzdxp<zzcbi> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfyl = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcma(this.zzejv.get(), this.zzfyl.get());
    }
}
