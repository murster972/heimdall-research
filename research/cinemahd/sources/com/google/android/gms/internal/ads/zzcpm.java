package com.google.android.gms.internal.ads;

public final class zzcpm implements zzdxg<String> {
    private final zzcpj zzged;

    public zzcpm(zzcpj zzcpj) {
        this.zzged = zzcpj;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza(this.zzged.zzana(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
