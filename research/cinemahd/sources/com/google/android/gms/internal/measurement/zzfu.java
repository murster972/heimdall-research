package com.google.android.gms.internal.measurement;

import java.util.List;

public interface zzfu extends List {
    zzfu i_();

    void zza(zzdv zzdv);

    Object zzb(int i);

    List<?> zzb();
}
