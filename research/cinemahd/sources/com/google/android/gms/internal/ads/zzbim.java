package com.google.android.gms.internal.ads;

import java.util.Map;

public final class zzbim implements zzdgf<zzczt, zzczt> {
    private Map<String, zzbil> zzfbi;

    public zzbim(Map<String, zzbil> map) {
        this.zzfbi = map;
    }

    public final /* synthetic */ zzdhe zzf(Object obj) throws Exception {
        zzczt zzczt = (zzczt) obj;
        for (zzczq next : zzczt.zzgmi.zzgmg) {
            if (this.zzfbi.containsKey(next.name)) {
                this.zzfbi.get(next.name).zzk(next.zzgmd);
            }
        }
        return zzdgs.zzaj(zzczt);
    }
}
