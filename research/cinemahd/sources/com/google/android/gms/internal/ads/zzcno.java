package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcno implements zzdcb {
    private final zzaad zzgbp;
    private final zzcnl zzgbx;

    zzcno(zzcnl zzcnl, zzaad zzaad) {
        this.zzgbx = zzcnl;
        this.zzgbp = zzaad;
    }

    public final void run() {
        this.zzgbx.zzb(this.zzgbp);
    }
}
