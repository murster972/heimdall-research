package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

final class zzdgj extends zzdgi<V> {
    private final /* synthetic */ zzdgg zzgwp;
    private final Callable<V> zzgwq;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdgj(zzdgg zzdgg, Callable<V> callable, Executor executor) {
        super(zzdgg, executor);
        this.zzgwp = zzdgg;
        this.zzgwq = (Callable) zzdei.checkNotNull(callable);
    }

    /* access modifiers changed from: package-private */
    public final void setValue(V v) {
        this.zzgwp.set(v);
    }

    /* access modifiers changed from: package-private */
    public final V zzars() throws Exception {
        this.zzgwo = false;
        return this.zzgwq.call();
    }

    /* access modifiers changed from: package-private */
    public final String zzart() {
        return this.zzgwq.toString();
    }
}
