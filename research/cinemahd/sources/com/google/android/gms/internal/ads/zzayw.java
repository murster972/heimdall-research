package com.google.android.gms.internal.ads;

public interface zzayw<D, R> {
    R apply(D d);
}
