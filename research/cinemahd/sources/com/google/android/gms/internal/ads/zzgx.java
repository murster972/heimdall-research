package com.google.android.gms.internal.ads;

public final class zzgx {
    public final zzhg zzade;
    public final Object zzadf;
    public final zzgv zzadj;
    public final int zzafy;

    public zzgx(zzhg zzhg, Object obj, zzgv zzgv, int i) {
        this.zzade = zzhg;
        this.zzadf = obj;
        this.zzadj = zzgv;
        this.zzafy = i;
    }
}
