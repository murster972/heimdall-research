package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

public final class zzcrd implements zzdxg<zzcqz> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<ScheduledExecutorService> zzfpr;

    private zzcrd(zzdxp<Context> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<Executor> zzdxp3) {
        this.zzejv = zzdxp;
        this.zzfpr = zzdxp2;
        this.zzfcv = zzdxp3;
    }

    public static zzcrd zzn(zzdxp<Context> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<Executor> zzdxp3) {
        return new zzcrd(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzcqz(this.zzejv.get(), this.zzfpr.get(), this.zzfcv.get());
    }
}
