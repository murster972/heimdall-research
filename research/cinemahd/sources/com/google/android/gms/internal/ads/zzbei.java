package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;

public interface zzbei {
    Context getContext();

    Activity zzyn();
}
