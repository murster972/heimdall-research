package com.google.android.gms.internal.ads;

import android.widget.ImageView;

public interface zzabv {
    void setImageScaleType(ImageView.ScaleType scaleType);
}
