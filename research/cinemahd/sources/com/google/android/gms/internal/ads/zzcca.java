package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcca implements zzbeu {
    private final zzazl zzbru;

    zzcca(zzazl zzazl) {
        this.zzbru = zzazl;
    }

    public final void zzak(boolean z) {
        zzazl zzazl = this.zzbru;
        if (z) {
            zzazl.set(null);
        } else {
            zzazl.setException(new Exception("Ad Web View failed to load."));
        }
    }
}
