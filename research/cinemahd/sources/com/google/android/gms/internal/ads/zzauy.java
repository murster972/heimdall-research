package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

public final class zzauy implements zzava {
    public final zzdhe<String> zza(String str, PackageInfo packageInfo) {
        return zzdgs.zzaj(str);
    }

    public final zzdhe<AdvertisingIdClient.Info> zzak(Context context) {
        zzazl zzazl = new zzazl();
        zzve.zzou();
        if (zzayk.zzbk(context)) {
            zzazd.zzdwe.execute(new zzavb(this, context, zzazl));
        }
        return zzazl;
    }
}
