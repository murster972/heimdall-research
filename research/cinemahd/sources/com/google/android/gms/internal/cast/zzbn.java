package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.AdBreakInfo;
import java.util.List;

public interface zzbn {
    void zzd(List<AdBreakInfo> list);

    void zzde();

    void zzdf();
}
