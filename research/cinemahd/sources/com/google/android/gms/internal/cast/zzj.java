package com.google.android.gms.internal.cast;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

public interface zzj extends IInterface {
    void zza(Bundle bundle, int i) throws RemoteException;

    void zza(Bundle bundle, zzl zzl) throws RemoteException;

    void zzar() throws RemoteException;

    boolean zzas() throws RemoteException;

    String zzat() throws RemoteException;

    void zzau() throws RemoteException;

    boolean zzb(Bundle bundle, int i) throws RemoteException;

    void zzd(Bundle bundle) throws RemoteException;

    void zzl(String str) throws RemoteException;

    Bundle zzm(String str) throws RemoteException;

    int zzs() throws RemoteException;
}
