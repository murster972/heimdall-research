package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

public final class zzaeu extends zzaee {
    private final UnifiedNativeAd.UnconfirmedClickListener zzcwr;

    public zzaeu(UnifiedNativeAd.UnconfirmedClickListener unconfirmedClickListener) {
        this.zzcwr = unconfirmedClickListener;
    }

    public final void onUnconfirmedClickCancelled() {
        this.zzcwr.onUnconfirmedClickCancelled();
    }

    public final void onUnconfirmedClickReceived(String str) {
        this.zzcwr.onUnconfirmedClickReceived(str);
    }
}
