package com.google.android.gms.internal.ads;

import java.util.ListIterator;

public abstract class zzdfo<E> extends zzdfp<E> implements ListIterator<E> {
    protected zzdfo() {
    }

    @Deprecated
    public final void add(E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void set(E e) {
        throw new UnsupportedOperationException();
    }
}
