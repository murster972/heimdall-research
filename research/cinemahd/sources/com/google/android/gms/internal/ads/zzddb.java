package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzddb implements zzdxg<zzakc> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfhn;
    private final zzdcy zzgrn;

    public zzddb(zzdcy zzdcy, zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2) {
        this.zzgrn = zzdcy;
        this.zzejv = zzdxp;
        this.zzfhn = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return (zzakc) zzdxm.zza(new zzajt().zzb(this.zzejv.get(), this.zzfhn.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
