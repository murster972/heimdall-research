package com.google.android.gms.internal.ads;

public final class zzbgf implements zzdxg<zzdq> {
    private final zzbga zzejr;

    public zzbgf(zzbga zzbga) {
        this.zzejr = zzbga;
    }

    public final /* synthetic */ Object get() {
        return (zzdq) zzdxm.zza(this.zzejr.zzact(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
