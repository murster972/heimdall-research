package com.google.android.gms.internal.ads;

import android.util.JsonWriter;

final /* synthetic */ class zzayt implements zzayv {
    private final byte[] zzdvy;

    zzayt(byte[] bArr) {
        this.zzdvy = bArr;
    }

    public final void zzb(JsonWriter jsonWriter) {
        zzayo.zza(this.zzdvy, jsonWriter);
    }
}
