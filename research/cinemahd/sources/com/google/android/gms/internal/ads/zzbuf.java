package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbuf implements zzdxg<zzats> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzczu> zzfep;
    private final zzbtv zzfje;

    private zzbuf(zzbtv zzbtv, zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2) {
        this.zzfje = zzbtv;
        this.zzejv = zzdxp;
        this.zzfep = zzdxp2;
    }

    public static zzbuf zza(zzbtv zzbtv, zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2) {
        return new zzbuf(zzbtv, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzats) zzdxm.zza(new zzats(this.zzejv.get(), this.zzfep.get().zzgmm), "Cannot return null from a non-@Nullable @Provides method");
    }
}
