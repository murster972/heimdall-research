package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import java.io.StringReader;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzcge {
    private final Executor executor;
    private final zzczu zzfgl;
    private final zzazb zzfvm;
    private final Context zzup;

    public zzcge(Context context, zzazb zzazb, zzczu zzczu, Executor executor2) {
        this.zzup = context;
        this.zzfvm = zzazb;
        this.zzfgl = zzczu;
        this.executor = executor2;
    }

    public final zzdhe<zzczt> zzalt() {
        zzakc zzb = zzq.zzld().zzb(this.zzup, this.zzfvm);
        zzajy<JSONObject> zzajy = zzajx.zzdaq;
        zzaju<I, O> zza = zzb.zza("google.afma.response.normalize", zzajy, zzajy);
        return zzdgs.zzb(zzdgs.zzb(zzdgs.zzb(zzdgs.zzaj(""), new zzcgd(this, this.zzfgl.zzgml.zzccm), this.executor), new zzcgg(zza), this.executor), new zzcgf(this), this.executor);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzn(JSONObject jSONObject) throws Exception {
        return zzdgs.zzaj(new zzczt(new zzczo(this.zzfgl), zzczr.zza(new StringReader(jSONObject.toString()))));
    }
}
