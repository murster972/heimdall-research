package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface zzaca extends IInterface {
    String getText() throws RemoteException;

    List<zzaci> zzqx() throws RemoteException;
}
