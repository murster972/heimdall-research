package com.google.android.gms.internal.ads;

public final class zzbwp implements zzdxg<zzbwq> {
    private final zzdxp<zzbwi> zzfly;

    private zzbwp(zzdxp<zzbwi> zzdxp) {
        this.zzfly = zzdxp;
    }

    public static zzbwp zzx(zzdxp<zzbwi> zzdxp) {
        return new zzbwp(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbwq(this.zzfly.get());
    }
}
