package com.google.android.gms.internal.ads;

public final class zzbvq implements zzdxg<String> {
    private static final zzbvq zzfjz = new zzbvq();

    public static zzbvq zzaim() {
        return zzfjz;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza("native", "Cannot return null from a non-@Nullable @Provides method");
    }
}
