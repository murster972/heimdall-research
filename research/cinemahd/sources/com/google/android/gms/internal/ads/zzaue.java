package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzaue implements Callable {
    private final zzatv zzdps;

    zzaue(zzatv zzatv) {
        this.zzdps = zzatv;
    }

    public final Object call() {
        return this.zzdps.zzur();
    }
}
