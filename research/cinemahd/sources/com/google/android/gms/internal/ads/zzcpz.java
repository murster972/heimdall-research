package com.google.android.gms.internal.ads;

import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.vungle.warren.model.ReportDBAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class zzcpz implements zzcty<Bundle> {
    private final zzczu zzfgl;

    public zzcpz(zzczu zzczu) {
        Preconditions.a(zzczu, (Object) "the targeting must not be null");
        this.zzfgl = zzczu;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        zzczu zzczu = this.zzfgl;
        zzug zzug = zzczu.zzgml;
        bundle.putString("slotname", zzczu.zzgmm);
        boolean z = true;
        if (this.zzfgl.zzgms.contains("new_rewarded")) {
            bundle.putBoolean("is_new_rewarded", true);
        }
        zzdaa.zza(bundle, "cust_age", new SimpleDateFormat("yyyyMMdd", Locale.US).format(new Date(zzug.zzcby)), zzug.zzcby != -1);
        zzdaa.zza(bundle, "extras", zzug.extras);
        zzdaa.zza(bundle, "cust_gender", Integer.valueOf(zzug.zzcbz), zzug.zzcbz != -1);
        zzdaa.zza(bundle, "kw", zzug.zzcca);
        zzdaa.zza(bundle, "tag_for_child_directed_treatment", Integer.valueOf(zzug.zzabo), zzug.zzabo != -1);
        boolean z2 = zzug.zzccb;
        if (z2) {
            bundle.putBoolean("test_request", z2);
        }
        zzdaa.zza(bundle, "d_imp_hdr", (Integer) 1, zzug.versionCode >= 2 && zzug.zzbkh);
        String str = zzug.zzccc;
        zzdaa.zza(bundle, "ppid", str, zzug.versionCode >= 2 && !TextUtils.isEmpty(str));
        Location location = zzug.zzmi;
        if (location != null) {
            Float valueOf = Float.valueOf(location.getAccuracy() * 1000.0f);
            Long valueOf2 = Long.valueOf(location.getTime() * 1000);
            Long valueOf3 = Long.valueOf((long) (location.getLatitude() * 1.0E7d));
            Long valueOf4 = Long.valueOf((long) (location.getLongitude() * 1.0E7d));
            Bundle bundle2 = new Bundle();
            bundle2.putFloat("radius", valueOf.floatValue());
            bundle2.putLong("lat", valueOf3.longValue());
            bundle2.putLong("long", valueOf4.longValue());
            bundle2.putLong("time", valueOf2.longValue());
            bundle.putBundle("uule", bundle2);
        }
        zzdaa.zza(bundle, ReportDBAdapter.ReportColumns.COLUMN_URL, zzug.zzcce);
        zzdaa.zza(bundle, "neighboring_content_urls", zzug.zzccl);
        zzdaa.zza(bundle, "custom_targeting", zzug.zzccg);
        zzdaa.zza(bundle, "category_exclusions", zzug.zzcch);
        zzdaa.zza(bundle, "request_agent", zzug.zzcci);
        zzdaa.zza(bundle, "request_pkg", zzug.zzccj);
        zzdaa.zza(bundle, "is_designed_for_families", Boolean.valueOf(zzug.zzcck), zzug.versionCode >= 7);
        if (zzug.versionCode >= 8) {
            Integer valueOf5 = Integer.valueOf(zzug.zzabp);
            if (zzug.zzabp == -1) {
                z = false;
            }
            zzdaa.zza(bundle, "tag_for_under_age_of_consent", valueOf5, z);
            zzdaa.zza(bundle, "max_ad_content_rating", zzug.zzabq);
        }
    }
}
