package com.google.android.gms.internal.ads;

public final class zztx extends zzvf {
    private final zzty zzcbt;

    public zztx(zzty zzty) {
        this.zzcbt = zzty;
    }

    public final void onAdClicked() {
        this.zzcbt.onAdClicked();
    }
}
