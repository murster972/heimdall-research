package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzbvy {
    private final JSONObject zzfka;
    private final zzcaj zzflb;

    public zzbvy(JSONObject jSONObject, zzcaj zzcaj) {
        this.zzfka = jSONObject;
        this.zzflb = zzcaj;
    }

    public final JSONObject zzais() {
        return this.zzfka;
    }

    public final zzcaj zzait() {
        return this.zzflb;
    }
}
