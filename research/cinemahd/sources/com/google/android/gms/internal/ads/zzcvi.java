package com.google.android.gms.internal.ads;

public final class zzcvi implements zzcub<zzcvj> {
    private String packageName;
    private zzdhd zzfov;
    private zzauw zzghy;

    public zzcvi(zzauw zzauw, zzdhd zzdhd, String str) {
        this.zzghy = zzauw;
        this.zzfov = zzdhd;
        this.packageName = str;
    }

    public final zzdhe<zzcvj> zzanc() {
        new zzazl();
        zzdhe<String> zzaj = zzdgs.zzaj(null);
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpj)).booleanValue()) {
            zzaj = this.zzghy.zzeb(this.packageName);
        }
        zzdhe<String> zzec = this.zzghy.zzec(this.packageName);
        return zzdgs.zzb((zzdhe<? extends V>[]) new zzdhe[]{zzaj, zzec}).zza(new zzcvl(zzaj, zzec), zzazd.zzdwe);
    }
}
