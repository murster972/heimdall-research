package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public final class zzdiy {
    public static final String zzgyr = new zzdjc().getKeyType();
    public static final String zzgys = new zzdjm().getKeyType();
    private static final String zzgyt = new zzdjh().getKeyType();
    private static final String zzgyu = new zzdjs().getKeyType();
    private static final String zzgyv = new zzdjw().getKeyType();
    private static final String zzgyw = new zzdjn().getKeyType();
    private static final String zzgyx = new zzdjx().getKeyType();
    @Deprecated
    private static final zzdny zzgyy;
    @Deprecated
    private static final zzdny zzgyz;
    @Deprecated
    private static final zzdny zzgza = zzgyy;

    static {
        zzdny zzawv = zzdny.zzawv();
        zzgyy = zzawv;
        zzgyz = zzawv;
        try {
            zzasq();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static void zzasq() throws GeneralSecurityException {
        zzdku.zzasq();
        zzdit.zza(new zzdjc(), true);
        zzdit.zza(new zzdjh(), true);
        zzdit.zza(new zzdjm(), true);
        zzdit.zza(new zzdjn(), true);
        zzdit.zza(new zzdjs(), true);
        zzdit.zza(new zzdjw(), true);
        zzdit.zza(new zzdjx(), true);
        zzdit.zza(new zzdjb());
    }
}
