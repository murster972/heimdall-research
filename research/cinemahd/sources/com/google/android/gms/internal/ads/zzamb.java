package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class zzamb extends zzalo {
    private final NativeAppInstallAdMapper zzddx;

    public zzamb(NativeAppInstallAdMapper nativeAppInstallAdMapper) {
        this.zzddx = nativeAppInstallAdMapper;
    }

    public final String getBody() {
        return this.zzddx.getBody();
    }

    public final String getCallToAction() {
        return this.zzddx.getCallToAction();
    }

    public final Bundle getExtras() {
        return this.zzddx.getExtras();
    }

    public final String getHeadline() {
        return this.zzddx.getHeadline();
    }

    public final List getImages() {
        List<NativeAd.Image> images = this.zzddx.getImages();
        if (images == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (NativeAd.Image next : images) {
            arrayList.add(new zzabu(next.getDrawable(), next.getUri(), next.getScale(), next.getWidth(), next.getHeight()));
        }
        return arrayList;
    }

    public final boolean getOverrideClickHandling() {
        return this.zzddx.getOverrideClickHandling();
    }

    public final boolean getOverrideImpressionRecording() {
        return this.zzddx.getOverrideImpressionRecording();
    }

    public final String getPrice() {
        return this.zzddx.getPrice();
    }

    public final double getStarRating() {
        return this.zzddx.getStarRating();
    }

    public final String getStore() {
        return this.zzddx.getStore();
    }

    public final zzxb getVideoController() {
        if (this.zzddx.getVideoController() != null) {
            return this.zzddx.getVideoController().zzdl();
        }
        return null;
    }

    public final void recordImpression() {
        this.zzddx.recordImpression();
    }

    public final void zzc(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) {
        this.zzddx.trackViews((View) ObjectWrapper.a(iObjectWrapper), (HashMap) ObjectWrapper.a(iObjectWrapper2), (HashMap) ObjectWrapper.a(iObjectWrapper3));
    }

    public final zzaci zzrg() {
        NativeAd.Image icon = this.zzddx.getIcon();
        if (icon != null) {
            return new zzabu(icon.getDrawable(), icon.getUri(), icon.getScale(), icon.getWidth(), icon.getHeight());
        }
        return null;
    }

    public final zzaca zzrh() {
        return null;
    }

    public final IObjectWrapper zzri() {
        return null;
    }

    public final IObjectWrapper zzsu() {
        View adChoicesContent = this.zzddx.getAdChoicesContent();
        if (adChoicesContent == null) {
            return null;
        }
        return ObjectWrapper.a(adChoicesContent);
    }

    public final IObjectWrapper zzsv() {
        View zzabz = this.zzddx.zzabz();
        if (zzabz == null) {
            return null;
        }
        return ObjectWrapper.a(zzabz);
    }

    public final void zzu(IObjectWrapper iObjectWrapper) {
        this.zzddx.handleClick((View) ObjectWrapper.a(iObjectWrapper));
    }

    public final void zzv(IObjectWrapper iObjectWrapper) {
        this.zzddx.trackView((View) ObjectWrapper.a(iObjectWrapper));
    }

    public final void zzw(IObjectWrapper iObjectWrapper) {
        this.zzddx.untrackView((View) ObjectWrapper.a(iObjectWrapper));
    }
}
