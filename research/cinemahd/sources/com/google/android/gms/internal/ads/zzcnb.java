package com.google.android.gms.internal.ads;

final class zzcnb implements zzbpx {
    private final /* synthetic */ zzcip zzgax;
    private final /* synthetic */ zzazl zzgbi;

    zzcnb(zzcna zzcna, zzazl zzazl, zzcip zzcip) {
        this.zzgbi = zzazl;
        this.zzgax = zzcip;
    }

    public final synchronized void onAdFailedToLoad(int i) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcot)).booleanValue()) {
            i = 3;
        }
        zzazl zzazl = this.zzgbi;
        String str = this.zzgax.zzfge;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 23);
        sb.append("adapter ");
        sb.append(str);
        sb.append(" failed to load");
        zzazl.setException(new zzclr(sb.toString(), i));
    }

    public final synchronized void onAdLoaded() {
        this.zzgbi.set(null);
    }
}
