package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzbga;
import com.google.android.gms.internal.ads.zzbhq;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

public abstract class zzbfx implements zzbij {
    private static zzbfx zzejp;

    public static zzbfx zza(Context context, zzalc zzalc, int i) {
        zzbfx zzd = zzd(context, i);
        zzd.zzacf().zzb(zzalc);
        return zzd;
    }

    @Deprecated
    public static zzbfx zzd(Context context, int i) {
        synchronized (zzbfx.class) {
            if (zzejp == null) {
                return zza(new zzazb(19649000, i, true, false), context, (zzbhq.zza) new zzbgo());
            }
            zzbfx zzbfx = zzejp;
            return zzbfx;
        }
    }

    /* access modifiers changed from: protected */
    public abstract zzcut zza(zzcvw zzcvw);

    public abstract Executor zzaca();

    public abstract ScheduledExecutorService zzacb();

    public abstract Executor zzacc();

    public abstract zzdhd zzacd();

    public abstract zzbqp zzace();

    public abstract zzcka zzacf();

    public abstract zzbht zzacg();

    public abstract zzblf zzach();

    public abstract zzbjz zzaci();

    public abstract zzcww zzacj();

    public abstract zzbus zzack();

    public abstract zzbvl zzacl();

    public abstract zzcbh zzacm();

    public abstract zzczc zzacn();

    public abstract zzcpf zzaco();

    @Deprecated
    private static zzbfx zza(zzazb zzazb, Context context, zzbhq.zza zza) {
        zzbfx zzbfx;
        synchronized (zzbfx.class) {
            if (zzejp == null) {
                zzejp = new zzbhh().zzc(new zzbga(new zzbga.zza().zza(zzazb).zzbs(context))).zza(new zzbhq(zza)).zzael();
                zzzn.initialize(context);
                zzq.zzku().zzd(context, zzazb);
                zzq.zzkw().initialize(context);
                zzq.zzkq().zzao(context);
                zzq.zzkq().zzap(context);
                zzavq.zzan(context);
                zzq.zzkt().initialize(context);
                zzq.zzll().initialize(context);
                if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue()) {
                    new zzcik(context, zzazb, new zzsm(new zzsr(context)), new zzcht(new zzchr(context), zzejp.zzacd())).zzamc();
                }
            }
            zzbfx = zzejp;
        }
        return zzbfx;
    }

    public final zzcut zza(zzaqk zzaqk) {
        return zza(new zzcvw(zzaqk));
    }
}
