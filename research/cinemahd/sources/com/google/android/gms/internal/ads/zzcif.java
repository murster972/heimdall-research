package com.google.android.gms.internal.ads;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzsy;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.ArrayList;

final /* synthetic */ class zzcif implements zzdby {
    private final boolean zzdym;
    private final zzcig zzfxs;
    private final ArrayList zzfxt;
    private final zzsy.zzh zzfxu;
    private final zzsy.zzj.zzc zzfxv;

    zzcif(zzcig zzcig, boolean z, ArrayList arrayList, zzsy.zzh zzh, zzsy.zzj.zzc zzc) {
        this.zzfxs = zzcig;
        this.zzdym = z;
        this.zzfxt = arrayList;
        this.zzfxu = zzh;
        this.zzfxv = zzc;
    }

    public final Object apply(Object obj) {
        zzcig zzcig = this.zzfxs;
        boolean z = this.zzdym;
        SQLiteDatabase sQLiteDatabase = (SQLiteDatabase) obj;
        byte[] zza = zzcig.zzfxx.zza(z, this.zzfxt, this.zzfxu, this.zzfxv);
        ContentValues contentValues = new ContentValues();
        contentValues.put(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, Long.valueOf(zzq.zzkx().b()));
        contentValues.put("serialized_proto_data", zza);
        sQLiteDatabase.insert("offline_signal_contents", (String) null, contentValues);
        sQLiteDatabase.execSQL(String.format("UPDATE offline_signal_statistics SET value = value+1 WHERE statistic_name = '%s'", new Object[]{"total_requests"}));
        if (!z) {
            sQLiteDatabase.execSQL(String.format("UPDATE offline_signal_statistics SET value = value+1 WHERE statistic_name = '%s'", new Object[]{"failed_requests"}));
        }
        return null;
    }
}
