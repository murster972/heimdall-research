package com.google.android.gms.internal.ads;

public final class zzbvp implements zzdxg<zzdaf<zzcaj>> {
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<zzcat> zzfjy;

    public zzbvp(zzdxp<zzcat> zzdxp, zzdxp<zzdhd> zzdxp2) {
        this.zzfjy = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return (zzdaf) zzdxm.zza(new zzdaf(this.zzfjy.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
