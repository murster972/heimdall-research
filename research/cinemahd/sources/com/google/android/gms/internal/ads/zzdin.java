package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import com.google.android.gms.internal.ads.zzdnk;
import java.security.GeneralSecurityException;

@Deprecated
public final class zzdin {
    @Deprecated
    public static final zzdij zzk(byte[] bArr) throws GeneralSecurityException {
        try {
            zzdnk zzn = zzdnk.zzn(bArr);
            for (zzdnk.zza next : zzn.zzavw()) {
                if (next.zzawa().zzavk() == zzdna.zzb.UNKNOWN_KEYMATERIAL || next.zzawa().zzavk() == zzdna.zzb.SYMMETRIC || next.zzawa().zzavk() == zzdna.zzb.ASYMMETRIC_PRIVATE) {
                    throw new GeneralSecurityException("keyset contains secret key material");
                }
            }
            return zzdij.zza(zzn);
        } catch (zzdse unused) {
            throw new GeneralSecurityException("invalid keyset");
        }
    }
}
