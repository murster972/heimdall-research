package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.internal.zzq;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzcvs implements zzcty<JSONObject> {
    private Bundle zzgie;

    public zzcvs(Bundle bundle) {
        this.zzgie = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (this.zzgie != null) {
            try {
                zzaxs.zzb(zzaxs.zzb(jSONObject, "device"), "play_store").put("parental_controls", zzq.zzkq().zzd(this.zzgie));
            } catch (JSONException unused) {
                zzavs.zzed("Failed putting parental controls bundle.");
            }
        }
    }
}
