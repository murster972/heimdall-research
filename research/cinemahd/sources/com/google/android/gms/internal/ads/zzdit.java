package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdnk;
import java.security.GeneralSecurityException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class zzdit {
    private static final Logger logger = Logger.getLogger(zzdit.class.getName());
    private static final ConcurrentMap<String, zza> zzgyi = new ConcurrentHashMap();
    private static final ConcurrentMap<String, Boolean> zzgyj = new ConcurrentHashMap();
    private static final ConcurrentMap<String, zzdia<?>> zzgyk = new ConcurrentHashMap();
    private static final ConcurrentMap<Class<?>, zzdis<?>> zzgyl = new ConcurrentHashMap();

    interface zza {
        Set<Class<?>> zzase();

        zzdid<?> zzasn();

        Class<?> zzaso();

        Class<?> zzasp();

        <P> zzdid<P> zzb(Class<P> cls) throws GeneralSecurityException;
    }

    private static <T> T checkNotNull(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    private static <KeyProtoT extends zzdte> zza zza(zzdii<KeyProtoT> zzdii) {
        return new zzdiv(zzdii);
    }

    public static synchronized zzdte zzb(zzdng zzdng) throws GeneralSecurityException {
        zzdte zzn;
        synchronized (zzdit.class) {
            zzdid<?> zzgz = zzgz(zzdng.zzavi());
            if (((Boolean) zzgyj.get(zzdng.zzavi())).booleanValue()) {
                zzn = zzgz.zzn(zzdng.zzavj());
            } else {
                String valueOf = String.valueOf(zzdng.zzavi());
                throw new GeneralSecurityException(valueOf.length() != 0 ? "newKey-operation not permitted for key type ".concat(valueOf) : new String("newKey-operation not permitted for key type "));
            }
        }
        return zzn;
    }

    private static synchronized zza zzgx(String str) throws GeneralSecurityException {
        zza zza2;
        synchronized (zzdit.class) {
            if (!zzgyi.containsKey(str)) {
                String valueOf = String.valueOf(str);
                throw new GeneralSecurityException(valueOf.length() != 0 ? "No key manager found for key type ".concat(valueOf) : new String("No key manager found for key type "));
            }
            zza2 = (zza) zzgyi.get(str);
        }
        return zza2;
    }

    @Deprecated
    public static zzdia<?> zzgy(String str) throws GeneralSecurityException {
        if (str != null) {
            zzdia<?> zzdia = (zzdia) zzgyk.get(str.toLowerCase());
            if (zzdia != null) {
                return zzdia;
            }
            String format = String.format("no catalogue found for %s. ", new Object[]{str});
            if (str.toLowerCase().startsWith("tinkaead")) {
                format = String.valueOf(format).concat("Maybe call AeadConfig.register().");
            }
            if (str.toLowerCase().startsWith("tinkdeterministicaead")) {
                format = String.valueOf(format).concat("Maybe call DeterministicAeadConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkstreamingaead")) {
                format = String.valueOf(format).concat("Maybe call StreamingAeadConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkhybriddecrypt") || str.toLowerCase().startsWith("tinkhybridencrypt")) {
                format = String.valueOf(format).concat("Maybe call HybridConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkmac")) {
                format = String.valueOf(format).concat("Maybe call MacConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkpublickeysign") || str.toLowerCase().startsWith("tinkpublickeyverify")) {
                format = String.valueOf(format).concat("Maybe call SignatureConfig.register().");
            } else if (str.toLowerCase().startsWith("tink")) {
                format = String.valueOf(format).concat("Maybe call TinkConfig.register().");
            }
            throw new GeneralSecurityException(format);
        }
        throw new IllegalArgumentException("catalogueName must be non-null.");
    }

    private static zzdid<?> zzgz(String str) throws GeneralSecurityException {
        return zzgx(str).zzasn();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0092, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized <P> void zza(java.lang.String r8, java.lang.Class<?> r9, boolean r10) throws java.security.GeneralSecurityException {
        /*
            java.lang.Class<com.google.android.gms.internal.ads.zzdit> r0 = com.google.android.gms.internal.ads.zzdit.class
            monitor-enter(r0)
            java.util.concurrent.ConcurrentMap<java.lang.String, com.google.android.gms.internal.ads.zzdit$zza> r1 = zzgyi     // Catch:{ all -> 0x0093 }
            boolean r1 = r1.containsKey(r8)     // Catch:{ all -> 0x0093 }
            if (r1 != 0) goto L_0x000d
            monitor-exit(r0)
            return
        L_0x000d:
            java.util.concurrent.ConcurrentMap<java.lang.String, com.google.android.gms.internal.ads.zzdit$zza> r1 = zzgyi     // Catch:{ all -> 0x0093 }
            java.lang.Object r1 = r1.get(r8)     // Catch:{ all -> 0x0093 }
            com.google.android.gms.internal.ads.zzdit$zza r1 = (com.google.android.gms.internal.ads.zzdit.zza) r1     // Catch:{ all -> 0x0093 }
            java.lang.Class r2 = r1.zzaso()     // Catch:{ all -> 0x0093 }
            boolean r2 = r2.equals(r9)     // Catch:{ all -> 0x0093 }
            if (r2 != 0) goto L_0x0065
            java.util.logging.Logger r10 = logger     // Catch:{ all -> 0x0093 }
            java.util.logging.Level r2 = java.util.logging.Level.WARNING     // Catch:{ all -> 0x0093 }
            java.lang.String r3 = "com.google.crypto.tink.Registry"
            java.lang.String r4 = "ensureKeyManagerInsertable"
            java.lang.String r5 = "Attempted overwrite of a registered key manager for key type "
            java.lang.String r6 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0093 }
            int r7 = r6.length()     // Catch:{ all -> 0x0093 }
            if (r7 == 0) goto L_0x0038
            java.lang.String r5 = r5.concat(r6)     // Catch:{ all -> 0x0093 }
            goto L_0x003e
        L_0x0038:
            java.lang.String r6 = new java.lang.String     // Catch:{ all -> 0x0093 }
            r6.<init>(r5)     // Catch:{ all -> 0x0093 }
            r5 = r6
        L_0x003e:
            r10.logp(r2, r3, r4, r5)     // Catch:{ all -> 0x0093 }
            java.security.GeneralSecurityException r10 = new java.security.GeneralSecurityException     // Catch:{ all -> 0x0093 }
            java.lang.String r2 = "typeUrl (%s) is already registered with %s, cannot be re-registered with %s"
            r3 = 3
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0093 }
            r4 = 0
            r3[r4] = r8     // Catch:{ all -> 0x0093 }
            r8 = 1
            java.lang.Class r1 = r1.zzaso()     // Catch:{ all -> 0x0093 }
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x0093 }
            r3[r8] = r1     // Catch:{ all -> 0x0093 }
            r8 = 2
            java.lang.String r9 = r9.getName()     // Catch:{ all -> 0x0093 }
            r3[r8] = r9     // Catch:{ all -> 0x0093 }
            java.lang.String r8 = java.lang.String.format(r2, r3)     // Catch:{ all -> 0x0093 }
            r10.<init>(r8)     // Catch:{ all -> 0x0093 }
            throw r10     // Catch:{ all -> 0x0093 }
        L_0x0065:
            if (r10 == 0) goto L_0x0091
            java.util.concurrent.ConcurrentMap<java.lang.String, java.lang.Boolean> r9 = zzgyj     // Catch:{ all -> 0x0093 }
            java.lang.Object r9 = r9.get(r8)     // Catch:{ all -> 0x0093 }
            java.lang.Boolean r9 = (java.lang.Boolean) r9     // Catch:{ all -> 0x0093 }
            boolean r9 = r9.booleanValue()     // Catch:{ all -> 0x0093 }
            if (r9 != 0) goto L_0x0091
            java.security.GeneralSecurityException r9 = new java.security.GeneralSecurityException     // Catch:{ all -> 0x0093 }
            java.lang.String r10 = "New keys are already disallowed for key type "
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0093 }
            int r1 = r8.length()     // Catch:{ all -> 0x0093 }
            if (r1 == 0) goto L_0x0088
            java.lang.String r8 = r10.concat(r8)     // Catch:{ all -> 0x0093 }
            goto L_0x008d
        L_0x0088:
            java.lang.String r8 = new java.lang.String     // Catch:{ all -> 0x0093 }
            r8.<init>(r10)     // Catch:{ all -> 0x0093 }
        L_0x008d:
            r9.<init>(r8)     // Catch:{ all -> 0x0093 }
            throw r9     // Catch:{ all -> 0x0093 }
        L_0x0091:
            monitor-exit(r0)
            return
        L_0x0093:
            r8 = move-exception
            monitor-exit(r0)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class, boolean):void");
    }

    public static synchronized <P> void zza(zzdid<P> zzdid, boolean z) throws GeneralSecurityException {
        synchronized (zzdit.class) {
            if (zzdid != null) {
                String keyType = zzdid.getKeyType();
                zza(keyType, zzdid.getClass(), z);
                if (!zzgyi.containsKey(keyType)) {
                    zzgyi.put(keyType, new zzdiw(zzdid));
                }
                zzgyj.put(keyType, Boolean.valueOf(z));
            } else {
                throw new IllegalArgumentException("key manager must be non-null.");
            }
        }
    }

    public static synchronized <KeyProtoT extends zzdte> void zza(zzdii<KeyProtoT> zzdii, boolean z) throws GeneralSecurityException {
        synchronized (zzdit.class) {
            String keyType = zzdii.getKeyType();
            zza(keyType, zzdii.getClass(), true);
            if (!zzgyi.containsKey(keyType)) {
                zzgyi.put(keyType, zza(zzdii));
            }
            zzgyj.put(keyType, true);
        }
    }

    public static synchronized <KeyProtoT extends zzdte, PublicKeyProtoT extends zzdte> void zza(zzdiu<KeyProtoT, PublicKeyProtoT> zzdiu, zzdii<PublicKeyProtoT> zzdii, boolean z) throws GeneralSecurityException {
        Class<?> zzasp;
        synchronized (zzdit.class) {
            String keyType = zzdiu.getKeyType();
            String keyType2 = zzdii.getKeyType();
            zza(keyType, zzdiu.getClass(), true);
            zza(keyType2, zzdii.getClass(), false);
            if (!keyType.equals(keyType2)) {
                if (zzgyi.containsKey(keyType) && (zzasp = ((zza) zzgyi.get(keyType)).zzasp()) != null) {
                    if (!zzasp.equals(zzdii.getClass())) {
                        Logger logger2 = logger;
                        Level level = Level.WARNING;
                        StringBuilder sb = new StringBuilder(String.valueOf(keyType).length() + 96 + String.valueOf(keyType2).length());
                        sb.append("Attempted overwrite of a registered key manager for key type ");
                        sb.append(keyType);
                        sb.append(" with inconsistent public key type ");
                        sb.append(keyType2);
                        logger2.logp(level, "com.google.crypto.tink.Registry", "registerAsymmetricKeyManagers", sb.toString());
                        throw new GeneralSecurityException(String.format("public key manager corresponding to %s is already registered with %s, cannot be re-registered with %s", new Object[]{zzdiu.getClass().getName(), zzasp.getName(), zzdii.getClass().getName()}));
                    }
                }
                if (!zzgyi.containsKey(keyType) || ((zza) zzgyi.get(keyType)).zzasp() == null) {
                    zzgyi.put(keyType, new zzdix(zzdiu, zzdii));
                }
                zzgyj.put(keyType, true);
                if (!zzgyi.containsKey(keyType2)) {
                    zzgyi.put(keyType2, zza(zzdii));
                }
                zzgyj.put(keyType2, false);
            } else {
                throw new GeneralSecurityException("Private and public key type must be different.");
            }
        }
    }

    public static synchronized <P> void zza(zzdis<P> zzdis) throws GeneralSecurityException {
        synchronized (zzdit.class) {
            if (zzdis != null) {
                Class<P> zzarz = zzdis.zzarz();
                if (zzgyl.containsKey(zzarz)) {
                    zzdis zzdis2 = (zzdis) zzgyl.get(zzarz);
                    if (!zzdis.getClass().equals(zzdis2.getClass())) {
                        Logger logger2 = logger;
                        Level level = Level.WARNING;
                        String valueOf = String.valueOf(zzarz.toString());
                        logger2.logp(level, "com.google.crypto.tink.Registry", "registerPrimitiveWrapper", valueOf.length() != 0 ? "Attempted overwrite of a registered SetWrapper for type ".concat(valueOf) : new String("Attempted overwrite of a registered SetWrapper for type "));
                        throw new GeneralSecurityException(String.format("SetWrapper for primitive (%s) is already registered to be %s, cannot be re-registered with %s", new Object[]{zzarz.getName(), zzdis2.getClass().getName(), zzdis.getClass().getName()}));
                    }
                }
                zzgyl.put(zzarz, zzdis);
            } else {
                throw new IllegalArgumentException("wrapper must be non-null");
            }
        }
    }

    private static <P> zzdid<P> zza(String str, Class<P> cls) throws GeneralSecurityException {
        zza zzgx = zzgx(str);
        if (cls == null) {
            return zzgx.zzasn();
        }
        if (zzgx.zzase().contains(cls)) {
            return zzgx.zzb(cls);
        }
        String name = cls.getName();
        String valueOf = String.valueOf(zzgx.zzaso());
        Set<Class<?>> zzase = zzgx.zzase();
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Class next : zzase) {
            if (!z) {
                sb.append(", ");
            }
            sb.append(next.getCanonicalName());
            z = false;
        }
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder(String.valueOf(name).length() + 77 + String.valueOf(valueOf).length() + String.valueOf(sb2).length());
        sb3.append("Primitive type ");
        sb3.append(name);
        sb3.append(" not supported by key manager of type ");
        sb3.append(valueOf);
        sb3.append(", supported primitives: ");
        sb3.append(sb2);
        throw new GeneralSecurityException(sb3.toString());
    }

    public static synchronized zzdna zza(zzdng zzdng) throws GeneralSecurityException {
        zzdna zzo;
        synchronized (zzdit.class) {
            zzdid<?> zzgz = zzgz(zzdng.zzavi());
            if (((Boolean) zzgyj.get(zzdng.zzavi())).booleanValue()) {
                zzo = zzgz.zzo(zzdng.zzavj());
            } else {
                String valueOf = String.valueOf(zzdng.zzavi());
                throw new GeneralSecurityException(valueOf.length() != 0 ? "newKey-operation not permitted for key type ".concat(valueOf) : new String("newKey-operation not permitted for key type "));
            }
        }
        return zzo;
    }

    public static <P> P zza(String str, zzdte zzdte, Class<P> cls) throws GeneralSecurityException {
        return zza(str, (Class) checkNotNull(cls)).zza(zzdte);
    }

    private static <P> P zza(String str, zzdqk zzdqk, Class<P> cls) throws GeneralSecurityException {
        return zza(str, cls).zzm(zzdqk);
    }

    public static <P> P zza(String str, byte[] bArr, Class<P> cls) throws GeneralSecurityException {
        return zza(str, zzdqk.zzu(bArr), (Class) checkNotNull(cls));
    }

    public static <P> zzdiq<P> zza(zzdij zzdij, zzdid<P> zzdid, Class<P> cls) throws GeneralSecurityException {
        Class cls2 = (Class) checkNotNull(cls);
        zzdiz.zzc(zzdij.zzash());
        zzdiq<P> zza2 = zzdiq.zza(cls2);
        for (zzdnk.zza next : zzdij.zzash().zzavw()) {
            if (next.zzasj() == zzdne.ENABLED) {
                zzdip<P> zza3 = zza2.zza(zza(next.zzawa().zzavi(), next.zzawa().zzavj(), cls2), next);
                if (next.zzawb() == zzdij.zzash().zzavv()) {
                    zza2.zza(zza3);
                }
            }
        }
        return zza2;
    }

    public static <P> P zza(zzdiq<P> zzdiq) throws GeneralSecurityException {
        zzdis zzdis = (zzdis) zzgyl.get(zzdiq.zzarz());
        if (zzdis != null) {
            return zzdis.zza(zzdiq);
        }
        String valueOf = String.valueOf(zzdiq.zzarz().getName());
        throw new GeneralSecurityException(valueOf.length() != 0 ? "No wrapper found for ".concat(valueOf) : new String("No wrapper found for "));
    }
}
