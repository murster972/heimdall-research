package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map;

final class zzdta implements zzdsx {
    zzdta() {
    }

    public final Map<?, ?> zzaq(Object obj) {
        return (zzdsy) obj;
    }

    public final Map<?, ?> zzar(Object obj) {
        return (zzdsy) obj;
    }

    public final boolean zzas(Object obj) {
        return !((zzdsy) obj).isMutable();
    }

    public final Object zzat(Object obj) {
        ((zzdsy) obj).zzaxq();
        return obj;
    }

    public final Object zzau(Object obj) {
        return zzdsy.zzbba().zzbbb();
    }

    public final zzdsv<?, ?> zzav(Object obj) {
        zzdsw zzdsw = (zzdsw) obj;
        throw new NoSuchMethodError();
    }

    public final int zzb(int i, Object obj, Object obj2) {
        zzdsy zzdsy = (zzdsy) obj;
        zzdsw zzdsw = (zzdsw) obj2;
        if (zzdsy.isEmpty()) {
            return 0;
        }
        Iterator it2 = zzdsy.entrySet().iterator();
        if (!it2.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it2.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }

    public final Object zze(Object obj, Object obj2) {
        zzdsy zzdsy = (zzdsy) obj;
        zzdsy zzdsy2 = (zzdsy) obj2;
        if (!zzdsy2.isEmpty()) {
            if (!zzdsy.isMutable()) {
                zzdsy = zzdsy.zzbbb();
            }
            zzdsy.zza(zzdsy2);
        }
        return zzdsy;
    }
}
