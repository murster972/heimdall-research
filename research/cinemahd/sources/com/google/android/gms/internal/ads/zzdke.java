package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;

final class zzdke extends zzdih<zzdmg, zzdmk> {
    private final /* synthetic */ zzdkc zzgzn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdke(zzdkc zzdkc, Class cls) {
        super(cls);
        this.zzgzn = zzdkc;
    }

    public final /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdkk.zza(((zzdmg) zzdte).zzauf());
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdmg zzdmg = (zzdmg) zzdte;
        KeyPair zza = zzdov.zza(zzdov.zza(zzdkk.zza(zzdmg.zzauf().zzauh().zzauu())));
        ECPoint w = ((ECPublicKey) zza.getPublic()).getW();
        return (zzdmk) zzdmk.zzaun().zzej(0).zzb((zzdmn) zzdmn.zzaur().zzek(0).zzc(zzdmg.zzauf()).zzaq(zzdqk.zzu(w.getAffineX().toByteArray())).zzar(zzdqk.zzu(w.getAffineY().toByteArray())).zzbaf()).zzam(zzdqk.zzu(((ECPrivateKey) zza.getPrivate()).getS().toByteArray())).zzbaf();
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdmg.zzak(zzdqk);
    }
}
