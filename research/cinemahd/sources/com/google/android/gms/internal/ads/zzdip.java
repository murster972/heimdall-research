package com.google.android.gms.internal.ads;

import java.util.Arrays;

public final class zzdip<P> {
    private final P zzgya;
    private final byte[] zzgyb;
    private final zzdne zzgyc;
    private final zzdnw zzgyd;

    public zzdip(P p, byte[] bArr, zzdne zzdne, zzdnw zzdnw) {
        this.zzgya = p;
        this.zzgyb = Arrays.copyOf(bArr, bArr.length);
        this.zzgyc = zzdne;
        this.zzgyd = zzdnw;
    }

    public final P zzasi() {
        return this.zzgya;
    }

    public final zzdne zzasj() {
        return this.zzgyc;
    }

    public final zzdnw zzask() {
        return this.zzgyd;
    }

    public final byte[] zzasl() {
        byte[] bArr = this.zzgyb;
        if (bArr == null) {
            return null;
        }
        return Arrays.copyOf(bArr, bArr.length);
    }
}
