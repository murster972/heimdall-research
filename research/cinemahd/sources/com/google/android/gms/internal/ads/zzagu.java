package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface zzagu extends IInterface {
    void zzc(List<zzagn> list) throws RemoteException;
}
