package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

@Deprecated
public interface zzdia<P> {
    zzdis<P> zzary() throws GeneralSecurityException;

    zzdid<P> zzb(String str, String str2, int i) throws GeneralSecurityException;
}
