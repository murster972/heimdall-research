package com.google.android.gms.internal.ads;

public final class zzcwa implements zzdxg<String> {
    private final zzcvw zzgih;

    public zzcwa(zzcvw zzcvw) {
        this.zzgih = zzcvw;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza(this.zzgih.zzanz(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
