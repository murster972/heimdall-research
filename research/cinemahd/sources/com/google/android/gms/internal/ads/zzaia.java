package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public interface zzaia extends zzahs, zzaip {
    void zzb(String str, JSONObject jSONObject);

    void zzcy(String str);

    void zzj(String str, String str2);
}
