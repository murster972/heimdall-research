package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbqh implements zzdxg<zzbqf> {
    private final zzdxp<Set<zzbsu<zzbqg>>> zzfeo;

    private zzbqh(zzdxp<Set<zzbsu<zzbqg>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbqh zzm(zzdxp<Set<zzbsu<zzbqg>>> zzdxp) {
        return new zzbqh(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbqf(this.zzfeo.get());
    }
}
