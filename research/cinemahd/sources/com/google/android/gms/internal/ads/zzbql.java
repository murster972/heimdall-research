package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

final /* synthetic */ class zzbql implements zzbrn {
    static final zzbrn zzfhp = new zzbql();

    private zzbql() {
    }

    public final void zzp(Object obj) {
        ((zzo) obj).onPause();
    }
}
