package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbsa implements zzdxg<Set<zzbsu<zzbqb>>> {
    private final zzbrm zzfim;

    private zzbsa(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbsa zzq(zzbrm zzbrm) {
        return new zzbsa(zzbrm);
    }

    public static Set<zzbsu<zzbqb>> zzr(zzbrm zzbrm) {
        return (Set) zzdxm.zza(zzbrm.zzahn(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzr(this.zzfim);
    }
}
