package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.text.TextUtils;

public final class zzcui implements zzcty<Bundle> {
    private final String zzdmn;
    private final int zzdmo;
    private final int zzdmp;
    private final int zzdmq;
    private final boolean zzdmr;
    private final int zzdms;

    public zzcui(String str, int i, int i2, int i3, boolean z, int i4) {
        this.zzdmn = str;
        this.zzdmo = i;
        this.zzdmp = i2;
        this.zzdmq = i3;
        this.zzdmr = z;
        this.zzdms = i4;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        String str = this.zzdmn;
        boolean z = true;
        zzdaa.zza(bundle, "carrier", str, !TextUtils.isEmpty(str));
        Integer valueOf = Integer.valueOf(this.zzdmo);
        if (this.zzdmo == -2) {
            z = false;
        }
        zzdaa.zza(bundle, "cnt", valueOf, z);
        bundle.putInt("gnt", this.zzdmp);
        bundle.putInt("pt", this.zzdmq);
        Bundle zza = zzdaa.zza(bundle, "device");
        bundle.putBundle("device", zza);
        Bundle zza2 = zzdaa.zza(zza, "network");
        zza.putBundle("network", zza2);
        zza2.putInt("active_network_state", this.zzdms);
        zza2.putBoolean("active_network_metered", this.zzdmr);
    }
}
