package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.Executor;

public final class zzcua<T> {
    private final Executor executor;
    private final Set<zzcub<? extends zzcty<T>>> zzggz;

    public zzcua(Executor executor2, Set<zzcub<? extends zzcty<T>>> set) {
        this.executor = executor2;
        this.zzggz = set;
    }

    public final zzdhe<T> zzs(T t) {
        ArrayList arrayList = new ArrayList(this.zzggz.size());
        for (zzcub next : this.zzggz) {
            zzdhe zzanc = next.zzanc();
            if (zzabc.zzctx.get().booleanValue()) {
                zzanc.addListener(new zzcud(next, zzq.zzkx().a()), zzazd.zzdwj);
            }
            arrayList.add(zzanc);
        }
        return zzdgs.zzi(arrayList).zza(new zzcuc(arrayList, t), this.executor);
    }
}
