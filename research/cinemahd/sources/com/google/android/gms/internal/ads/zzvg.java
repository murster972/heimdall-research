package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzvg extends IInterface {
    void onAdClicked() throws RemoteException;
}
