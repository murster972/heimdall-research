package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.Callable;

final class zzaqu implements Callable<zzaqt> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ zzaqv zzdnh;

    zzaqu(zzaqv zzaqv, Context context) {
        this.zzdnh = zzaqv;
        this.val$context = context;
    }

    public final /* synthetic */ Object call() throws Exception {
        zzaqt zzaqt;
        zzaqx zzaqx = (zzaqx) this.zzdnh.zzdni.get(this.val$context);
        if (zzaqx != null) {
            if (!(zzaqx.zzdnl + zzaau.zzcsu.get().longValue() < zzq.zzkx().b())) {
                zzaqt = new zzaqs(this.val$context, zzaqx.zzdnm).zzug();
                this.zzdnh.zzdni.put(this.val$context, new zzaqx(this.zzdnh, zzaqt));
                return zzaqt;
            }
        }
        zzaqt = new zzaqs(this.val$context).zzug();
        this.zzdnh.zzdni.put(this.val$context, new zzaqx(this.zzdnh, zzaqt));
        return zzaqt;
    }
}
