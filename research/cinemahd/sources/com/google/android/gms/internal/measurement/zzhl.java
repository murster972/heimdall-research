package com.google.android.gms.internal.measurement;

import java.util.Iterator;

final class zzhl {
    /* access modifiers changed from: private */
    public static final Iterator<Object> zza = new zzhk();
    private static final Iterable<Object> zzb = new zzhn();

    static <T> Iterable<T> zza() {
        return zzb;
    }
}
