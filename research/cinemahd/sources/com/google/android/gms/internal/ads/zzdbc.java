package com.google.android.gms.internal.ads;

final class zzdbc {
    private final zzdbf zzgnw = new zzdbf();
    private int zzgnx;
    private int zzgny;
    private int zzgnz;
    private int zzgoa;
    private int zzgob;

    zzdbc() {
    }

    public final void zzape() {
        this.zzgnz++;
    }

    public final void zzapf() {
        this.zzgoa++;
    }

    public final void zzapg() {
        this.zzgnx++;
        this.zzgnw.zzgoq = true;
    }

    public final void zzaph() {
        this.zzgny++;
        this.zzgnw.zzgor = true;
    }

    public final void zzapi() {
        this.zzgob++;
    }

    public final zzdbf zzapj() {
        zzdbf zzdbf = (zzdbf) this.zzgnw.clone();
        zzdbf zzdbf2 = this.zzgnw;
        zzdbf2.zzgoq = false;
        zzdbf2.zzgor = false;
        return zzdbf;
    }

    public final String zzapk() {
        return "\n\tPool does not exist: " + this.zzgnz + "\n\tNew pools created: " + this.zzgnx + "\n\tPools removed: " + this.zzgny + "\n\tEntries added: " + this.zzgob + "\n\tNo entries retrieved: " + this.zzgoa + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
    }
}
