package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.initialization.AdapterStatus;

public final class zzagv implements AdapterStatus {
    private final String description;
    private final int zzcye;
    private final AdapterStatus.State zzcyf;

    public zzagv(AdapterStatus.State state, String str, int i) {
        this.zzcyf = state;
        this.description = str;
        this.zzcye = i;
    }

    public final String getDescription() {
        return this.description;
    }

    public final AdapterStatus.State getInitializationState() {
        return this.zzcyf;
    }

    public final int getLatency() {
        return this.zzcye;
    }
}
