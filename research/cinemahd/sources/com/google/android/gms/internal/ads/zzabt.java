package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

public interface zzabt {
    void setMediaContent(UnifiedNativeAd.MediaContent mediaContent);
}
