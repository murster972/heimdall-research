package com.google.android.gms.internal.measurement;

final class zzgd implements zzgk {
    zzgd() {
    }

    public final boolean zza(Class<?> cls) {
        return false;
    }

    public final zzgl zzb(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
