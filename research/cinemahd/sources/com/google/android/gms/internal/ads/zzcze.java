package com.google.android.gms.internal.ads;

final class zzcze implements zzcoz<zzcbb> {
    private final /* synthetic */ zzczf zzgla;

    zzcze(zzczf zzczf) {
        this.zzgla = zzczf;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzcbb zzcbb = (zzcbb) obj;
        synchronized (this.zzgla) {
            zzcbb unused = this.zzgla.zzgky = zzcbb;
            this.zzgla.zzgky.zzagf();
        }
    }

    public final void zzamx() {
        synchronized (this.zzgla) {
            zzcbb unused = this.zzgla.zzgky = null;
        }
    }
}
