package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdoc extends zzdrt<zzdoc, zza> implements zzdtg {
    private static volatile zzdtn<zzdoc> zzdz;
    /* access modifiers changed from: private */
    public static final zzdoc zzheu;

    public static final class zza extends zzdrt.zzb<zzdoc, zza> implements zzdtg {
        private zza() {
            super(zzdoc.zzheu);
        }

        /* synthetic */ zza(zzdob zzdob) {
            this();
        }
    }

    static {
        zzdoc zzdoc = new zzdoc();
        zzheu = zzdoc;
        zzdrt.zza(zzdoc.class, zzdoc);
    }

    private zzdoc() {
    }

    public static zzdoc zzbd(zzdqk zzdqk) throws zzdse {
        return (zzdoc) zzdrt.zza(zzheu, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdob.zzdk[i - 1]) {
            case 1:
                return new zzdoc();
            case 2:
                return new zza((zzdob) null);
            case 3:
                return zzdrt.zza((zzdte) zzheu, "\u0000\u0000", (Object[]) null);
            case 4:
                return zzheu;
            case 5:
                zzdtn<zzdoc> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdoc.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzheu);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
