package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbsq extends zzbrl<zzbsn> {
    public zzbsq(Set<zzbsu<zzbsn>> set) {
        super(set);
    }

    public final void onHide() {
        zza(zzbss.zzfhp);
    }

    public final void zzahy() {
        zza(zzbsp.zzfhp);
    }
}
