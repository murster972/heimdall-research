package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.instream.InstreamAd;
import com.google.android.gms.common.internal.Preconditions;

public final class zzahq {
    private final zzvn zzabd;
    private final Context zzup;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public zzahq(Context context, String str) {
        this(context, zzve.zzov().zzb(context, str, new zzakz()));
        Preconditions.a(context, (Object) "context cannot be null");
    }

    public final zzahq zza(InstreamAd.InstreamAdLoadCallback instreamAdLoadCallback) {
        try {
            this.zzabd.zza((zzahh) new zzaho(instreamAdLoadCallback));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        return this;
    }

    public final zzahn zzry() {
        try {
            return new zzahn(this.zzup, this.zzabd.zzpd());
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return null;
        }
    }

    public final zzahq zza(zzahl zzahl) {
        try {
            this.zzabd.zza(new zzagz(zzahl));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        return this;
    }

    private zzahq(Context context, zzvn zzvn) {
        this.zzup = context;
        this.zzabd = zzvn;
    }
}
