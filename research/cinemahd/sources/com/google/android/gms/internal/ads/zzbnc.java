package com.google.android.gms.internal.ads;

public final class zzbnc<T> implements zzdxg<zzbmz<T>> {
    public static <T> zzbmz<T> zza(zzcfx zzcfx, zzcge zzcge, zzdxp<zzdhe<zzaqk>> zzdxp, zzczu zzczu, zzdcr zzdcr, zzbim zzbim, zzclu<T> zzclu, zzbrf zzbrf, zzczt zzczt, zzcgu zzcgu) {
        return new zzbmz(zzcfx, zzcge, zzdxp, zzczu, zzdcr, zzbim, zzclu, zzbrf, zzczt, zzcgu);
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
