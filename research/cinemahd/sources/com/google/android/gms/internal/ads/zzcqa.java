package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzcqa implements zzcub<Object> {
    private final Executor executor;
    private final zzdhe<String> zzgeo;

    public zzcqa(zzdhe<String> zzdhe, Executor executor2) {
        this.zzgeo = zzdhe;
        this.executor = executor2;
    }

    public final zzdhe<Object> zzanc() {
        return zzdgs.zzb(this.zzgeo, zzcqd.zzbkw, this.executor);
    }
}
