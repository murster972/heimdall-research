package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzi;
import com.google.android.gms.ads.internal.zzq;

public final class zzbfe {
    public static zzbdi zza(Context context, zzbey zzbey, String str, boolean z, boolean z2, zzdq zzdq, zzazb zzazb, zzaae zzaae, zzi zzi, zza zza, zzsm zzsm, zzro zzro, boolean z3) throws zzbdv {
        try {
            return (zzbdi) zzayc.zza(new zzbfh(context, zzbey, str, z, z2, zzdq, zzazb, (zzaae) null, zzi, zza, zzsm, zzro, z3));
        } catch (Throwable th) {
            zzq.zzku().zza(th, "AdWebViewFactory.newAdWebView2");
            throw new zzbdv("Webview initialization failed.", th);
        }
    }
}
