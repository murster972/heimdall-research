package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzaqz implements zzded {
    private final zzaqw zzdnn;

    zzaqz(zzaqw zzaqw) {
        this.zzdnn = zzaqw;
    }

    public final Object apply(Object obj) {
        return this.zzdnn.zzf((JSONObject) obj);
    }
}
