package com.google.android.gms.internal.ads;

import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

final class zzaff implements zzafn<zzbdi> {
    zzaff() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        ((zzbdi) obj).zzal(DiskLruCache.VERSION_1.equals(map.get("custom_close")));
    }
}
