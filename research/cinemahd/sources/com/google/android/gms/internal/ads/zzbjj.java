package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import org.json.JSONObject;

public final class zzbjj implements zzdxg<zzpn> {
    private final zzdxp<JSONObject> zzeua;
    private final zzdxp<zzczl> zzfda;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<String> zzfdc;

    private zzbjj(zzdxp<zzczl> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<JSONObject> zzdxp3, zzdxp<String> zzdxp4) {
        this.zzfda = zzdxp;
        this.zzfdb = zzdxp2;
        this.zzeua = zzdxp3;
        this.zzfdc = zzdxp4;
    }

    public static zzbjj zza(zzdxp<zzczl> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<JSONObject> zzdxp3, zzdxp<String> zzdxp4) {
        return new zzbjj(zzdxp, zzdxp2, zzdxp3, zzdxp4);
    }

    public final /* synthetic */ Object get() {
        zzczl zzczl = this.zzfda.get();
        String str = this.zzfdc.get();
        boolean equals = "native".equals(str);
        zzq.zzkq();
        return (zzpn) zzdxm.zza(new zzpn(zzawb.zzwk(), this.zzfdb.get(), str, this.zzeua.get(), false, equals), "Cannot return null from a non-@Nullable @Provides method");
    }
}
