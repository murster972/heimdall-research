package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;

final /* synthetic */ class zzasz implements zzayw {
    static final zzayw zzbtz = new zzasz();

    private zzasz() {
    }

    public final Object apply(Object obj) {
        IBinder iBinder = (IBinder) obj;
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.rewarded.client.IRewardedAdCreator");
        if (queryLocalInterface instanceof zzasm) {
            return (zzasm) queryLocalInterface;
        }
        return new zzasp(iBinder);
    }
}
