package com.google.android.gms.internal.ads;

final class zzaje implements zzazn {
    private final /* synthetic */ zzais zzczu;
    private final /* synthetic */ zzajj zzczy;

    zzaje(zzais zzais, zzajj zzajj) {
        this.zzczu = zzais;
        this.zzczy = zzajj;
    }

    public final void run() {
        synchronized (this.zzczu.lock) {
            int unused = this.zzczu.status = 1;
            zzavs.zzed("Failed loading new engine. Marking new engine destroyable.");
            this.zzczy.zzse();
        }
    }
}
