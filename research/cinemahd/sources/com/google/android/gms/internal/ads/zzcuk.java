package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcuk implements Callable {
    private final zzcul zzghi;

    zzcuk(zzcul zzcul) {
        this.zzghi = zzcul;
    }

    public final Object call() {
        return this.zzghi.zzanr();
    }
}
