package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final class zzafw implements zzafv {
    private final /* synthetic */ zzazl zzcxt;

    zzafw(zzaft zzaft, zzazl zzazl) {
        this.zzcxt = zzazl;
    }

    public final void onFailure(String str) {
        this.zzcxt.setException(new zzajr(str));
    }

    public final void zzc(JSONObject jSONObject) {
        this.zzcxt.set(jSONObject);
    }
}
