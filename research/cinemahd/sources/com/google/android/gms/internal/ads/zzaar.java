package com.google.android.gms.internal.ads;

public final class zzaar {
    public static zzaan<String> zzcsn = zzaan.zzi("gads:afs:csa_webview_custom_domain_param_key", "csa_customDomain");
    public static zzaan<String> zzcso = zzaan.zzi("gads:afs:csa_webview_static_file_path", "/afs/ads/i/webview.html");
}
