package com.google.android.gms.internal.ads;

public enum zzdvm {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(zzdqk.zzhhx),
    ENUM((String) null),
    MESSAGE((String) null);
    
    private final Object zzhny;

    private zzdvm(Object obj) {
        this.zzhny = obj;
    }
}
