package com.google.android.gms.internal.cast;

import android.view.View;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzba extends UIController {
    private final View view;

    public zzba(View view2) {
        this.view = view2;
    }

    private final void zzdk() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k() || remoteMediaClient.l()) {
            this.view.setVisibility(0);
        } else {
            this.view.setVisibility(8);
        }
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onSendingRemoteMediaRequest() {
        this.view.setVisibility(0);
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        zzdk();
    }

    public final void onSessionEnded() {
        this.view.setVisibility(8);
        super.onSessionEnded();
    }
}
