package com.google.android.gms.internal.ads;

public final class zzcar {
    private final zzbpm zzfgg;
    private final zzbqj zzfjf;
    private final zzboq zzfke;
    private final zzbqa zzfqp;
    private final zzbra zzfqq;

    public zzcar(zzboq zzboq, zzbpm zzbpm, zzbqa zzbqa, zzbqj zzbqj, zzbra zzbra) {
        this.zzfke = zzboq;
        this.zzfgg = zzbpm;
        this.zzfqp = zzbqa;
        this.zzfjf = zzbqj;
        this.zzfqq = zzbra;
    }

    public final void zzb(zzcaj zzcaj) {
        zzcas zza = zzcaj.zzfqf;
        zzboq zzboq = this.zzfke;
        zzbqa zzbqa = this.zzfqp;
        zzbqj zzbqj = this.zzfjf;
        zzbra zzbra = this.zzfqq;
        zzbpm zzbpm = this.zzfgg;
        zzbpm.getClass();
        zza.zza(zzboq, zzbqa, zzbqj, zzbra, zzcau.zza(zzbpm));
    }
}
