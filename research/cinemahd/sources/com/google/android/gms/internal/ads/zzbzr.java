package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzbzr implements zzafn {
    private final zzbzq zzfpy;

    zzbzr(zzbzq zzbzq) {
        this.zzfpy = zzbzq;
    }

    public final void zza(Object obj, Map map) {
        this.zzfpy.zzf((zzbdi) obj, map);
    }
}
