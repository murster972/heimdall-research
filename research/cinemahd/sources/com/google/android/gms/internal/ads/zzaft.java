package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzaft implements zzafn<Object> {
    private final Object lock = new Object();
    private final Map<String, zzafv> zzcxs = new HashMap();

    public final void zza(String str, zzafv zzafv) {
        synchronized (this.lock) {
            this.zzcxs.put(str, zzafv);
        }
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str;
        String str2 = map.get("id");
        String str3 = map.get("fail");
        String str4 = map.get("fail_reason");
        String str5 = map.get("fail_stack");
        String str6 = map.get("result");
        if (TextUtils.isEmpty(str5)) {
            str4 = "Unknown Fail Reason.";
        }
        if (TextUtils.isEmpty(str5)) {
            str = "";
        } else {
            String valueOf = String.valueOf(str5);
            str = valueOf.length() != 0 ? ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE.concat(valueOf) : new String(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
        synchronized (this.lock) {
            zzafv remove = this.zzcxs.remove(str2);
            if (remove == null) {
                String valueOf2 = String.valueOf(str2);
                zzayu.zzez(valueOf2.length() != 0 ? "Received result for unexpected method invocation: ".concat(valueOf2) : new String("Received result for unexpected method invocation: "));
            } else if (!TextUtils.isEmpty(str3)) {
                String valueOf3 = String.valueOf(str4);
                String valueOf4 = String.valueOf(str);
                remove.onFailure(valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3));
            } else if (str6 == null) {
                remove.zzc((JSONObject) null);
            } else {
                try {
                    JSONObject jSONObject = new JSONObject(str6);
                    if (zzavs.zzvs()) {
                        String valueOf5 = String.valueOf(jSONObject.toString(2));
                        zzavs.zzed(valueOf5.length() != 0 ? "Result GMSG: ".concat(valueOf5) : new String("Result GMSG: "));
                    }
                    remove.zzc(jSONObject);
                } catch (JSONException e) {
                    remove.onFailure(e.getMessage());
                }
            }
        }
    }

    public final <EngineT extends zzaip> zzdhe<JSONObject> zza(EngineT enginet, String str, JSONObject jSONObject) {
        zzazl zzazl = new zzazl();
        zzq.zzkq();
        String zzwk = zzawb.zzwk();
        zza(zzwk, (zzafv) new zzafw(this, zzazl));
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("id", zzwk);
            jSONObject2.put("args", jSONObject);
            enginet.zza(str, jSONObject2);
        } catch (Exception e) {
            zzazl.setException(e);
        }
        return zzazl;
    }
}
