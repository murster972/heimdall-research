package com.google.android.gms.internal.ads;

import android.view.View;

final class zzaod implements View.OnClickListener {
    private final /* synthetic */ zzaoe zzdfg;

    zzaod(zzaoe zzaoe) {
        this.zzdfg = zzaoe;
    }

    public final void onClick(View view) {
        this.zzdfg.zzac(true);
    }
}
