package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzazl<T> implements zzdhe<T> {
    private final zzdhl<T> zzdwo = zzdhl.zzarx();

    private static boolean zzas(boolean z) {
        if (!z) {
            zzq.zzku().zzb(new IllegalStateException("Provided SettableFuture with multiple values."), "SettableFuture");
        }
        return z;
    }

    public void addListener(Runnable runnable, Executor executor) {
        this.zzdwo.addListener(runnable, executor);
    }

    public boolean cancel(boolean z) {
        return this.zzdwo.cancel(z);
    }

    public T get() throws ExecutionException, InterruptedException {
        return this.zzdwo.get();
    }

    public boolean isCancelled() {
        return this.zzdwo.isCancelled();
    }

    public boolean isDone() {
        return this.zzdwo.isDone();
    }

    public final boolean set(T t) {
        return zzas(this.zzdwo.set(t));
    }

    public final boolean setException(Throwable th) {
        return zzas(this.zzdwo.setException(th));
    }

    public T get(long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        return this.zzdwo.get(j, timeUnit);
    }
}
