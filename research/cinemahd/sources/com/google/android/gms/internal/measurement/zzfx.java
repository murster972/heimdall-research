package com.google.android.gms.internal.measurement;

import java.util.List;

abstract class zzfx {
    private static final zzfx zza = new zzfz();
    private static final zzfx zzb = new zzfy();

    private zzfx() {
    }

    static zzfx zza() {
        return zza;
    }

    static zzfx zzb() {
        return zzb;
    }

    /* access modifiers changed from: package-private */
    public abstract <L> List<L> zza(Object obj, long j);

    /* access modifiers changed from: package-private */
    public abstract <L> void zza(Object obj, Object obj2, long j);

    /* access modifiers changed from: package-private */
    public abstract void zzb(Object obj, long j);
}
