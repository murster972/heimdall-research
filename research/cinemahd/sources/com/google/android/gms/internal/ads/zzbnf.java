package com.google.android.gms.internal.ads;

public final class zzbnf implements zzdxg<zzbsu<zzbqb>> {
    private final zzbnb zzfgs;
    private final zzdxp<zzbnk> zzfgt;

    private zzbnf(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        this.zzfgs = zzbnb;
        this.zzfgt = zzdxp;
    }

    public static zzbnf zzc(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        return new zzbnf(zzbnb, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfgt.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
