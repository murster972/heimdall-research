package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzyq extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzyq> CREATOR = new zzyp();
    private final int zzabo;
    private final int zzabp;

    public zzyq(int i, int i2) {
        this.zzabo = i;
        this.zzabp = i2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzabo);
        SafeParcelWriter.a(parcel, 2, this.zzabp);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzyq(RequestConfiguration requestConfiguration) {
        this.zzabo = requestConfiguration.getTagForChildDirectedTreatment();
        this.zzabp = requestConfiguration.getTagForUnderAgeOfConsent();
    }
}
