package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public final class zzdrv {
    private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final byte[] zzhng;
    private static final ByteBuffer zzhnh;
    private static final zzdqw zzhni;

    static {
        byte[] bArr = new byte[0];
        zzhng = bArr;
        zzhnh = ByteBuffer.wrap(bArr);
        byte[] bArr2 = zzhng;
        zzhni = zzdqw.zzb(bArr2, 0, bArr2.length, false);
    }

    static <T> T checkNotNull(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    public static int hashCode(byte[] bArr) {
        int length = bArr.length;
        int zza = zza(length, bArr, 0, length);
        if (zza == 0) {
            return 1;
        }
        return zza;
    }

    static <T> T zza(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public static int zzbp(boolean z) {
        return z ? 1231 : 1237;
    }

    static Object zzd(Object obj, Object obj2) {
        return ((zzdte) obj).zzazx().zzf((zzdte) obj2).zzbae();
    }

    public static int zzfq(long j) {
        return (int) (j ^ (j >>> 32));
    }

    static boolean zzk(zzdte zzdte) {
        if (!(zzdte instanceof zzdqc)) {
            return false;
        }
        zzdqc zzdqc = (zzdqc) zzdte;
        return false;
    }

    public static boolean zzy(byte[] bArr) {
        return zzdva.zzy(bArr);
    }

    public static String zzz(byte[] bArr) {
        return new String(bArr, UTF_8);
    }

    static int zza(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        for (int i5 = i2; i5 < i2 + i3; i5++) {
            i4 = (i4 * 31) + bArr[i5];
        }
        return i4;
    }
}
