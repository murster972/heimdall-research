package com.google.android.gms.internal.ads;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class zzdww<E> extends AbstractList<E> {
    private static final zzdwy zzcr = zzdwy.zzn(zzdww.class);
    List<E> zzhzn;
    Iterator<E> zzhzo;

    public zzdww(List<E> list, Iterator<E> it2) {
        this.zzhzn = list;
        this.zzhzo = it2;
    }

    public E get(int i) {
        if (this.zzhzn.size() > i) {
            return this.zzhzn.get(i);
        }
        if (this.zzhzo.hasNext()) {
            this.zzhzn.add(this.zzhzo.next());
            return get(i);
        }
        throw new NoSuchElementException();
    }

    public Iterator<E> iterator() {
        return new zzdwv(this);
    }

    public int size() {
        zzcr.zzhp("potentially expensive size() call");
        zzcr.zzhp("blowup running");
        while (this.zzhzo.hasNext()) {
            this.zzhzn.add(this.zzhzo.next());
        }
        return this.zzhzn.size();
    }
}
