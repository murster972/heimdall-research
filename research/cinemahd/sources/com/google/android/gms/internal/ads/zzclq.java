package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbod;

public final class zzclq implements zzdxg<zzcln> {
    private final zzdxp<zzbfx> zzfkr;
    private final zzdxp<zzbod.zza> zzfks;
    private final zzdxp<zzbrm> zzfkt;

    public zzclq(zzdxp<zzbfx> zzdxp, zzdxp<zzbod.zza> zzdxp2, zzdxp<zzbrm> zzdxp3) {
        this.zzfkr = zzdxp;
        this.zzfks = zzdxp2;
        this.zzfkt = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcln(this.zzfkr.get(), this.zzfks.get(), this.zzfkt.get());
    }
}
