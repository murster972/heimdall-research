package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcmp implements zzdxg<zzcmi> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzcbn> zzfog;
    private final zzdxp<zzcbi> zzfyl;

    public zzcmp(zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzczu> zzdxp3, zzdxp<Executor> zzdxp4, zzdxp<zzcbi> zzdxp5, zzdxp<zzcbn> zzdxp6) {
        this.zzejv = zzdxp;
        this.zzfdb = zzdxp2;
        this.zzfep = zzdxp3;
        this.zzfei = zzdxp4;
        this.zzfyl = zzdxp5;
        this.zzfog = zzdxp6;
    }

    public final /* synthetic */ Object get() {
        return new zzcmi(this.zzejv.get(), this.zzfdb.get(), this.zzfep.get(), this.zzfei.get(), this.zzfyl.get(), this.zzfog.get());
    }
}
