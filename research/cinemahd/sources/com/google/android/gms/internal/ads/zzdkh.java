package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.util.logging.Logger;

public class zzdkh implements zzdis<zzdib> {
    private static final Logger logger = Logger.getLogger(zzdkh.class.getName());

    static class zza implements zzdib {
        private final zzdiq<zzdib> zzgzq;

        public zza(zzdiq<zzdib> zzdiq) {
            this.zzgzq = zzdiq;
        }
    }

    zzdkh() {
    }

    public final /* synthetic */ Object zza(zzdiq zzdiq) throws GeneralSecurityException {
        return new zza(zzdiq);
    }

    public final Class<zzdib> zzarz() {
        return zzdib.class;
    }
}
