package com.google.android.gms.internal.cast;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.ImagePicker;
import com.google.android.gms.cast.framework.media.MediaUtils;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import com.google.android.gms.common.images.WebImage;

public final class zzax extends UIController {
    private final ImagePicker zzlp;
    private final zzaa zzma;
    private final ImageHints zzmb;
    /* access modifiers changed from: private */
    public final ImageView zzsi;
    private final Bitmap zzsl;
    /* access modifiers changed from: private */
    public final View zzsm;

    public zzax(ImageView imageView, Context context, ImageHints imageHints, int i, View view) {
        this.zzsi = imageView;
        this.zzmb = imageHints;
        ImagePicker imagePicker = null;
        this.zzsl = i != 0 ? BitmapFactory.decodeResource(context.getResources(), i) : null;
        this.zzsm = view;
        CastContext b = CastContext.b(context);
        if (b != null) {
            CastMediaOptions s = b.a().s();
            this.zzlp = s != null ? s.t() : imagePicker;
        } else {
            this.zzlp = null;
        }
        this.zzma = new zzaa(context.getApplicationContext());
    }

    private final void zzdk() {
        Uri uri;
        WebImage a2;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            zzdl();
            return;
        }
        MediaInfo e = remoteMediaClient.e();
        if (e == null) {
            uri = null;
        } else {
            ImagePicker imagePicker = this.zzlp;
            if (imagePicker == null || (a2 = imagePicker.a(e.y(), this.zzmb)) == null || a2.t() == null) {
                uri = MediaUtils.a(e, 0);
            } else {
                uri = a2.t();
            }
        }
        if (uri == null) {
            zzdl();
        } else {
            this.zzma.zza(uri);
        }
    }

    private final void zzdl() {
        View view = this.zzsm;
        if (view != null) {
            view.setVisibility(0);
            this.zzsi.setVisibility(4);
        }
        Bitmap bitmap = this.zzsl;
        if (bitmap != null) {
            this.zzsi.setImageBitmap(bitmap);
        }
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        this.zzma.zza((zzab) new zzay(this));
        zzdl();
        zzdk();
    }

    public final void onSessionEnded() {
        this.zzma.clear();
        zzdl();
        super.onSessionEnded();
    }
}
