package com.google.android.gms.internal.ads;

public final class zzcsf implements zzcub<zzcsc> {
    private final zzdhd zzfov;

    public zzcsf(zzdhd zzdhd) {
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcsc> zzanc() {
        return this.zzfov.zzd(zzcse.zzgfx);
    }
}
