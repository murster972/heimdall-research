package com.google.android.gms.internal.ads;

public final class zzbvb implements zzdxg<zzbvc> {
    private final zzdxp<zzbpg> zzfjk;

    private zzbvb(zzdxp<zzbpg> zzdxp) {
        this.zzfjk = zzdxp;
    }

    public static zzbvb zzv(zzdxp<zzbpg> zzdxp) {
        return new zzbvb(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbvc(this.zzfjk.get());
    }
}
