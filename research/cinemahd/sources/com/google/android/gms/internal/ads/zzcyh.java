package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcyh implements zzcxo {
    private final int zzdvv;

    zzcyh(int i) {
        this.zzdvv = i;
    }

    public final void zzt(Object obj) {
        ((zzasl) obj).onRewardedAdFailedToShow(this.zzdvv);
    }
}
