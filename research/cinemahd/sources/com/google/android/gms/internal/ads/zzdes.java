package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;

class zzdes<E> extends zzdev<E> {
    int size = 0;
    Object[] zzguf;
    boolean zzgug;

    zzdes(int i) {
        zzdeo.zze(i, "initialCapacity");
        this.zzguf = new Object[i];
    }

    private final void zzdw(int i) {
        Object[] objArr = this.zzguf;
        if (objArr.length < i) {
            int length = objArr.length;
            if (i >= 0) {
                int i2 = length + (length >> 1) + 1;
                if (i2 < i) {
                    i2 = Integer.highestOneBit(i - 1) << 1;
                }
                if (i2 < 0) {
                    i2 = Integer.MAX_VALUE;
                }
                this.zzguf = Arrays.copyOf(objArr, i2);
                this.zzgug = false;
                return;
            }
            throw new AssertionError("cannot store more than MAX_VALUE elements");
        } else if (this.zzgug) {
            this.zzguf = (Object[]) objArr.clone();
            this.zzgug = false;
        }
    }

    /* renamed from: zzad */
    public zzdes<E> zzae(E e) {
        zzdei.checkNotNull(e);
        zzdw(this.size + 1);
        Object[] objArr = this.zzguf;
        int i = this.size;
        this.size = i + 1;
        objArr[i] = e;
        return this;
    }

    public zzdev<E> zze(Iterable<? extends E> iterable) {
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            zzdw(this.size + collection.size());
            if (collection instanceof zzdet) {
                this.size = ((zzdet) collection).zza(this.zzguf, this.size);
                return this;
            }
        }
        super.zze(iterable);
        return this;
    }
}
