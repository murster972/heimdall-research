package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.internal.ads.zzbod;

public final class zzcll extends zzclk<zzbtu> {
    private final zzbrm zzers;
    private final zzcns zzexc;
    private final zzbfx zzfzz;
    private final zzbod.zza zzgaa;

    public zzcll(zzbfx zzbfx, zzbod.zza zza, zzcns zzcns, zzbrm zzbrm) {
        this.zzfzz = zzbfx;
        this.zzgaa = zza;
        this.zzexc = zzcns;
        this.zzers = zzbrm;
    }

    /* access modifiers changed from: protected */
    public final zzdhe<zzbtu> zza(zzczu zzczu, Bundle bundle) {
        return this.zzfzz.zzack().zzd(this.zzgaa.zza(zzczu).zze(bundle).zzahh()).zzd(this.zzers).zzb(this.zzexc).zzaek().zzadc().zzaha();
    }
}
