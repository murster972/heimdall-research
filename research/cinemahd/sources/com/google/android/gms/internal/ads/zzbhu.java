package com.google.android.gms.internal.ads;

public final class zzbhu implements zzdxg<zzatq> {
    private final zzbhq zzfao;

    public zzbhu(zzbhq zzbhq) {
        this.zzfao = zzbhq;
    }

    public final /* synthetic */ Object get() {
        return (zzatq) zzdxm.zza(this.zzfao.zzacz(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
