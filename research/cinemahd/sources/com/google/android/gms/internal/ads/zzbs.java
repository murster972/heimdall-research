package com.google.android.gms.internal.ads;

import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.internal.ads.zzdrt;
import com.google.ar.core.ImageMetadata;
import okhttp3.internal.http2.Http2;
import okhttp3.internal.http2.Http2Connection;

public final class zzbs {

    public static final class zza extends zzdrt<zza, zzb> implements zzdtg {
        private static volatile zzdtn<zza> zzdz;
        /* access modifiers changed from: private */
        public static final zza zzht;
        private int zzdl;
        private String zzdv = "";
        private String zzep = "";
        private String zzer = "";
        private String zzes = "D";
        private String zzet = "D";
        private int zzex;
        private int zzey;
        private String zzez = "";
        private long zzfa;
        private long zzfb;
        private long zzfc;
        private long zzfd;
        private long zzfe;
        private long zzff;
        private long zzfg;
        private long zzfh;
        private long zzfi;
        private long zzfj;
        private String zzfk = "";
        private long zzfl;
        private long zzfm;
        private long zzfn;
        private long zzfo;
        private long zzfp;
        private long zzfq;
        private long zzfr;
        private long zzfs;
        private long zzft;
        private String zzfu = "D";
        private String zzfv = "";
        private long zzfw;
        private long zzfx;
        private long zzfy;
        private long zzfz;
        private long zzga = -1;
        private long zzgb = -1;
        private zzb zzgc;
        private long zzgd = -1;
        private long zzge = -1;
        private long zzgf = -1;
        private long zzgg = -1;
        private long zzgh = -1;
        private long zzgi = -1;
        private long zzgj = -1;
        private int zzgk = 1000;
        private int zzgl = 1000;
        private long zzgm = -1;
        private long zzgn = -1;
        private long zzgo = -1;
        private long zzgp = -1;
        private long zzgq = -1;
        private int zzgr = 1000;
        private zze zzgs;
        private zzdsb<zze> zzgt = zzdrt.zzazw();
        private zzf zzgu;
        private long zzgv = -1;
        private long zzgw = -1;
        private long zzgx = -1;
        private long zzgy = -1;
        private long zzgz = -1;
        private long zzha = -1;
        private long zzhb = -1;
        private long zzhc = -1;
        private String zzhd = "D";
        private long zzhe = -1;
        private int zzhf;
        private int zzhg;
        private int zzhh;
        private zze zzhi;
        private long zzhj = -1;
        private int zzhk = 1000;
        private int zzhl = 1000;
        private long zzhm;
        private String zzhn = "";
        private int zzho = 2;
        private boolean zzhp;
        private String zzhq = "";
        private long zzhr;
        private zzd zzhs;

        /* renamed from: com.google.android.gms.internal.ads.zzbs$zza$zza  reason: collision with other inner class name */
        public enum C0036zza implements zzdry {
            DEBUGGER_STATE_UNSPECIFIED(0),
            DEBUGGER_STATE_NOT_INSTALLED(1),
            DEBUGGER_STATE_INSTALLED(2),
            DEBUGGER_STATE_ACTIVE(3),
            DEBUGGER_STATE_ENVVAR(4),
            DEBUGGER_STATE_MACHPORT(5),
            DEBUGGER_STATE_ENVVAR_MACHPORT(6);
            
            private static final zzdrx<C0036zza> zzen = null;
            private final int value;

            static {
                zzen = new zzbv();
            }

            private C0036zza(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zzbu.zzew;
            }

            public static C0036zza zzg(int i) {
                switch (i) {
                    case 0:
                        return DEBUGGER_STATE_UNSPECIFIED;
                    case 1:
                        return DEBUGGER_STATE_NOT_INSTALLED;
                    case 2:
                        return DEBUGGER_STATE_INSTALLED;
                    case 3:
                        return DEBUGGER_STATE_ACTIVE;
                    case 4:
                        return DEBUGGER_STATE_ENVVAR;
                    case 5:
                        return DEBUGGER_STATE_MACHPORT;
                    case 6:
                        return DEBUGGER_STATE_ENVVAR_MACHPORT;
                    default:
                        return null;
                }
            }

            public final String toString() {
                return "<" + C0036zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        public static final class zzb extends zzdrt.zzb<zza, zzb> implements zzdtg {
            private zzb() {
                super(zza.zzht);
            }

            public final zzb zzaf(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzw(str);
                return this;
            }

            public final zzb zzag(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzx(str);
                return this;
            }

            public final zzb zzah(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzy(str);
                return this;
            }

            public final zzb zzai(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzz(str);
                return this;
            }

            public final zzb zzaj(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzaa(str);
                return this;
            }

            public final zzb zzak(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzab(str);
                return this;
            }

            public final zzb zzal(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zze(j);
                return this;
            }

            public final zzb zzam(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzf(j);
                return this;
            }

            public final zzb zzan(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzg(j);
                return this;
            }

            public final zzb zzao(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzh(j);
                return this;
            }

            public final zzb zzap(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzi(j);
                return this;
            }

            public final zzb zzaq(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzj(j);
                return this;
            }

            public final zzb zzar(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzk(j);
                return this;
            }

            public final zzb zzas(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzl(j);
                return this;
            }

            public final zzb zzat(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzm(j);
                return this;
            }

            public final zzb zzau(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzn(j);
                return this;
            }

            public final zzb zzav(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzo(j);
                return this;
            }

            public final zzb zzaw(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzp(j);
                return this;
            }

            public final zzb zzax(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzq(j);
                return this;
            }

            public final zzb zzay(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzr(j);
                return this;
            }

            public final zzb zzaz(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzs(j);
                return this;
            }

            public final zzb zzb(zzf zzf) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zza(zzf);
                return this;
            }

            public final zzb zzba(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzt(j);
                return this;
            }

            @Deprecated
            public final zzb zzbb(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzu(j);
                return this;
            }

            public final zzb zzbc(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzv(j);
                return this;
            }

            public final zzb zzbd(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzw(j);
                return this;
            }

            public final zzb zzbe(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzx(j);
                return this;
            }

            public final zzb zzbf(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzy(j);
                return this;
            }

            public final zzb zzbg(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzz(j);
                return this;
            }

            public final zzb zzbh(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzaa(j);
                return this;
            }

            public final zzb zzbi(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzab(j);
                return this;
            }

            public final zzb zzbj(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzac(j);
                return this;
            }

            public final zzb zzbk(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzad(j);
                return this;
            }

            public final zzb zzbl(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzae(j);
                return this;
            }

            public final zzb zzbm(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzaf(j);
                return this;
            }

            public final zzb zzbn(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzag(j);
                return this;
            }

            public final zzb zzbo(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzah(j);
                return this;
            }

            public final zzb zzbp(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzai(j);
                return this;
            }

            public final zzb zzbq(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzaj(j);
                return this;
            }

            public final zzb zzbr(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzak(j);
                return this;
            }

            public final zzb zzc(zze zze) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zza(zze);
                return this;
            }

            public final zzb zzd(zze zze) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzb(zze);
                return this;
            }

            public final zzb zze(zzcd zzcd) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zza(zzcd);
                return this;
            }

            public final zzb zzf(zzcd zzcd) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzb(zzcd);
                return this;
            }

            public final zzb zzg(zzcd zzcd) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzc(zzcd);
                return this;
            }

            public final zzb zzh(zzcd zzcd) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzd(zzcd);
                return this;
            }

            /* synthetic */ zzb(zzbt zzbt) {
                this();
            }

            public final zzb zzal(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzac(str);
                return this;
            }

            public final zzb zzam(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzad(str);
                return this;
            }

            public final zzb zzan(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzae(str);
                return this;
            }

            public final zzb zzap() {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzak();
                return this;
            }

            public final zzb zzb(zzc zzc) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zza(zzc);
                return this;
            }

            public final zzb zzb(boolean z) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zza(z);
                return this;
            }
        }

        public enum zzc implements zzdry {
            DEVICE_IDENTIFIER_NO_ID(0),
            DEVICE_IDENTIFIER_APP_SPECIFIC_ID(1),
            DEVICE_IDENTIFIER_GLOBAL_ID(2),
            DEVICE_IDENTIFIER_ADVERTISER_ID(3),
            DEVICE_IDENTIFIER_ADVERTISER_ID_UNHASHED(4),
            DEVICE_IDENTIFIER_ANDROID_AD_ID(5),
            DEVICE_IDENTIFIER_GFIBER_ADVERTISING_ID(6);
            
            private static final zzdrx<zzc> zzen = null;
            private final int value;

            static {
                zzen = new zzbw();
            }

            private zzc(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zzbx.zzew;
            }

            public static zzc zzh(int i) {
                switch (i) {
                    case 0:
                        return DEVICE_IDENTIFIER_NO_ID;
                    case 1:
                        return DEVICE_IDENTIFIER_APP_SPECIFIC_ID;
                    case 2:
                        return DEVICE_IDENTIFIER_GLOBAL_ID;
                    case 3:
                        return DEVICE_IDENTIFIER_ADVERTISER_ID;
                    case 4:
                        return DEVICE_IDENTIFIER_ADVERTISER_ID_UNHASHED;
                    case 5:
                        return DEVICE_IDENTIFIER_ANDROID_AD_ID;
                    case 6:
                        return DEVICE_IDENTIFIER_GFIBER_ADVERTISING_ID;
                    default:
                        return null;
                }
            }

            public final String toString() {
                return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        public enum zzd implements zzdry {
            ERROR_ENCODE_SIZE_FAIL(1),
            ERROR_UNKNOWN(3),
            ERROR_NO_SIGNALS(5),
            ERROR_ENCRYPTION(7),
            ERROR_MEMORY(9),
            ERROR_SIMULATOR(11),
            ERROR_SERVICE(13),
            ERROR_THREAD(15),
            PSN_WEB64_FAIL(2),
            PSN_DECRYPT_SIZE_FAIL(4),
            PSN_MD5_CHECK_FAIL(8),
            PSN_MD5_SIZE_FAIL(16),
            PSN_MD5_FAIL(32),
            PSN_DECODE_FAIL(64),
            PSN_SALT_FAIL(128),
            PSN_BITSLICER_FAIL(256),
            PSN_REQUEST_TYPE_FAIL(AdRequest.MAX_CONTENT_URL_LENGTH),
            PSN_INVALID_ERROR_CODE(ByteConstants.KB),
            PSN_TIMESTAMP_EXPIRED(2048),
            PSN_ENCODE_SIZE_FAIL(4096),
            PSN_BLANK_VALUE(8192),
            PSN_INITIALIZATION_FAIL(Http2.INITIAL_MAX_FRAME_SIZE),
            PSN_GASS_CLIENT_FAIL(32768),
            PSN_SIGNALS_TIMEOUT(ImageMetadata.CONTROL_AE_ANTIBANDING_MODE),
            PSN_TINK_FAIL(131072);
            
            private static final zzdrx<zzd> zzen = null;
            private final int value;

            static {
                zzen = new zzby();
            }

            private zzd(int i) {
                this.value = i;
            }

            public final String toString() {
                return "<" + zzd.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        public static final class zzf extends zzdrt<zzf, C0038zza> implements zzdtg {
            private static volatile zzdtn<zzf> zzdz;
            /* access modifiers changed from: private */
            public static final zzf zzki;
            private int zzdl;
            private long zzgp = -1;
            private long zzgq = -1;
            private long zzke = -1;
            private long zzkf = -1;
            private long zzkg = -1;
            private long zzkh = -1;

            /* renamed from: com.google.android.gms.internal.ads.zzbs$zza$zzf$zza  reason: collision with other inner class name */
            public static final class C0038zza extends zzdrt.zzb<zzf, C0038zza> implements zzdtg {
                private C0038zza() {
                    super(zzf.zzki);
                }

                public final C0038zza zzdc(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzf) this.zzhmp).zzch(j);
                    return this;
                }

                public final C0038zza zzdd(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzf) this.zzhmp).zzci(j);
                    return this;
                }

                public final C0038zza zzde(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzf) this.zzhmp).zzcj(j);
                    return this;
                }

                public final C0038zza zzdf(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzf) this.zzhmp).zzck(j);
                    return this;
                }

                /* synthetic */ C0038zza(zzbt zzbt) {
                    this();
                }
            }

            static {
                zzf zzf = new zzf();
                zzki = zzf;
                zzdrt.zza(zzf.class, zzf);
            }

            private zzf() {
            }

            public static C0038zza zzat() {
                return (C0038zza) zzki.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zzch(long j) {
                this.zzdl |= 4;
                this.zzke = j;
            }

            /* access modifiers changed from: private */
            public final void zzci(long j) {
                this.zzdl |= 8;
                this.zzkf = j;
            }

            /* access modifiers changed from: private */
            public final void zzcj(long j) {
                this.zzdl |= 16;
                this.zzkg = j;
            }

            /* access modifiers changed from: private */
            public final void zzck(long j) {
                this.zzdl |= 32;
                this.zzkh = j;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzbt.zzdk[i - 1]) {
                    case 1:
                        return new zzf();
                    case 2:
                        return new C0038zza((zzbt) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzki, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0002\u0001\u0003\u0002\u0002\u0004\u0002\u0003\u0005\u0002\u0004\u0006\u0002\u0005", new Object[]{"zzdl", "zzgp", "zzgq", "zzke", "zzkf", "zzkg", "zzkh"});
                    case 4:
                        return zzki;
                    case 5:
                        zzdtn<zzf> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzf.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzki);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        static {
            zza zza = new zza();
            zzht = zza;
            zzdrt.zza(zza.class, zza);
        }

        private zza() {
        }

        /* access modifiers changed from: private */
        public final void zza(zzcd zzcd) {
            this.zzgk = zzcd.zzae();
            this.zzex |= ByteConstants.KB;
        }

        /* access modifiers changed from: private */
        public final void zzaa(String str) {
            str.getClass();
            this.zzdl |= 268435456;
            this.zzer = str;
        }

        /* access modifiers changed from: private */
        public final void zzab(long j) {
            this.zzex |= 64;
            this.zzgi = j;
        }

        /* access modifiers changed from: private */
        public final void zzac(String str) {
            str.getClass();
            this.zzex |= 256;
            this.zzet = str;
        }

        /* access modifiers changed from: private */
        public final void zzad(long j) {
            this.zzex |= 8192;
            this.zzgn = j;
        }

        /* access modifiers changed from: private */
        public final void zzae(long j) {
            this.zzex |= Http2.INITIAL_MAX_FRAME_SIZE;
            this.zzgo = j;
        }

        /* access modifiers changed from: private */
        public final void zzaf(long j) {
            this.zzex |= 2097152;
            this.zzgw = j;
        }

        /* access modifiers changed from: private */
        public final void zzah(long j) {
            this.zzex |= 8388608;
            this.zzgy = j;
        }

        /* access modifiers changed from: private */
        public final void zzai(long j) {
            this.zzex |= 67108864;
            this.zzhb = j;
        }

        /* access modifiers changed from: private */
        public final void zzak() {
            this.zzgt = zzdrt.zzazw();
        }

        public static zzb zzan() {
            return (zzb) zzht.zzazt();
        }

        /* access modifiers changed from: private */
        public final void zzb(zzcd zzcd) {
            this.zzgl = zzcd.zzae();
            this.zzex |= 2048;
        }

        /* access modifiers changed from: private */
        public final void zzc(zzcd zzcd) {
            this.zzgr = zzcd.zzae();
            this.zzex |= 131072;
        }

        /* access modifiers changed from: private */
        public final void zzd(zzcd zzcd) {
            this.zzhk = zzcd.zzae();
            this.zzey |= 8;
        }

        /* access modifiers changed from: private */
        public final void zze(long j) {
            this.zzdl |= 4;
            this.zzfa = j;
        }

        /* access modifiers changed from: private */
        public final void zzf(long j) {
            this.zzdl |= 16;
            this.zzfc = j;
        }

        /* access modifiers changed from: private */
        public final void zzg(long j) {
            this.zzdl |= 32;
            this.zzfd = j;
        }

        /* access modifiers changed from: private */
        public final void zzh(long j) {
            this.zzdl |= ByteConstants.KB;
            this.zzfi = j;
        }

        /* access modifiers changed from: private */
        public final void zzi(long j) {
            this.zzdl |= 2048;
            this.zzfj = j;
        }

        /* access modifiers changed from: private */
        public final void zzj(long j) {
            this.zzdl |= 8192;
            this.zzfl = j;
        }

        /* access modifiers changed from: private */
        public final void zzk(long j) {
            this.zzdl |= Http2.INITIAL_MAX_FRAME_SIZE;
            this.zzfm = j;
        }

        /* access modifiers changed from: private */
        public final void zzl(long j) {
            this.zzdl |= 32768;
            this.zzfn = j;
        }

        /* access modifiers changed from: private */
        public final void zzm(long j) {
            this.zzdl |= ImageMetadata.CONTROL_AE_ANTIBANDING_MODE;
            this.zzfo = j;
        }

        /* access modifiers changed from: private */
        public final void zzn(long j) {
            this.zzdl |= ImageMetadata.LENS_APERTURE;
            this.zzfr = j;
        }

        /* access modifiers changed from: private */
        public final void zzo(long j) {
            this.zzdl |= 1048576;
            this.zzfs = j;
        }

        /* access modifiers changed from: private */
        public final void zzp(long j) {
            this.zzdl |= 2097152;
            this.zzft = j;
        }

        /* access modifiers changed from: private */
        public final void zzq(long j) {
            this.zzdl |= 33554432;
            this.zzfw = j;
        }

        /* access modifiers changed from: private */
        public final void zzr(long j) {
            this.zzdl |= 67108864;
            this.zzfx = j;
        }

        /* access modifiers changed from: private */
        public final void zzs(long j) {
            this.zzdl |= 134217728;
            this.zzfy = j;
        }

        /* access modifiers changed from: private */
        public final void zzt(long j) {
            this.zzdl |= 536870912;
            this.zzfz = j;
        }

        /* access modifiers changed from: private */
        public final void zzu(long j) {
            this.zzdl |= 1073741824;
            this.zzga = j;
        }

        /* access modifiers changed from: private */
        public final void zzv(long j) {
            this.zzdl |= Integer.MIN_VALUE;
            this.zzgb = j;
        }

        /* access modifiers changed from: private */
        public final void zzw(String str) {
            str.getClass();
            this.zzdl |= 1;
            this.zzez = str;
        }

        /* access modifiers changed from: private */
        public final void zzx(String str) {
            str.getClass();
            this.zzdl |= 2;
            this.zzdv = str;
        }

        /* access modifiers changed from: private */
        public final void zzy(String str) {
            str.getClass();
            this.zzdl |= 4194304;
            this.zzep = str;
        }

        /* access modifiers changed from: private */
        public final void zzz(String str) {
            str.getClass();
            this.zzdl |= Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE;
            this.zzfv = str;
        }

        public final String zzag() {
            return this.zzep;
        }

        public final boolean zzaj() {
            return (this.zzdl & 4194304) != 0;
        }

        public final boolean zzal() {
            return (this.zzey & 2048) != 0;
        }

        public final zzd zzam() {
            zzd zzd2 = this.zzhs;
            return zzd2 == null ? zzd.zzbf() : zzd2;
        }

        public static final class zze extends zzdrt<zze, C0037zza> implements zzdtg {
            private static volatile zzdtn<zze> zzdz;
            /* access modifiers changed from: private */
            public static final zze zzkd;
            private int zzdl;
            private long zzfl = -1;
            private long zzfm = -1;
            private long zzjk = -1;
            private long zzjl = -1;
            private long zzjm = -1;
            private long zzjn = -1;
            private int zzjo = 1000;
            private long zzjp = -1;
            private long zzjq = -1;
            private long zzjr = -1;
            private int zzjs = 1000;
            private long zzjt = -1;
            private long zzju = -1;
            private long zzjv = -1;
            private long zzjw = -1;
            private long zzjx;
            private long zzjy;
            private long zzjz = -1;
            private long zzka = -1;
            private long zzkb = -1;
            private long zzkc = -1;

            /* renamed from: com.google.android.gms.internal.ads.zzbs$zza$zze$zza  reason: collision with other inner class name */
            public static final class C0037zza extends zzdrt.zzb<zze, C0037zza> implements zzdtg {
                private C0037zza() {
                    super(zze.zzkd);
                }

                public final C0037zza zzav() {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzaq();
                    return this;
                }

                public final C0037zza zzcl(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzj(j);
                    return this;
                }

                public final C0037zza zzcm(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzk(j);
                    return this;
                }

                public final C0037zza zzcn(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbs(j);
                    return this;
                }

                public final C0037zza zzco(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbt(j);
                    return this;
                }

                public final C0037zza zzcp(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbu(j);
                    return this;
                }

                public final C0037zza zzcq(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbv(j);
                    return this;
                }

                public final C0037zza zzcr(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbw(j);
                    return this;
                }

                public final C0037zza zzcs(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbx(j);
                    return this;
                }

                public final C0037zza zzct(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzby(j);
                    return this;
                }

                public final C0037zza zzcu(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbz(j);
                    return this;
                }

                public final C0037zza zzcv(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzca(j);
                    return this;
                }

                public final C0037zza zzcw(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzcb(j);
                    return this;
                }

                public final C0037zza zzcx(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzcc(j);
                    return this;
                }

                public final C0037zza zzcy(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzcd(j);
                    return this;
                }

                public final C0037zza zzcz(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzce(j);
                    return this;
                }

                public final C0037zza zzda(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzcf(j);
                    return this;
                }

                public final C0037zza zzdb(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzcg(j);
                    return this;
                }

                public final C0037zza zzk(zzcd zzcd) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzi(zzcd);
                    return this;
                }

                public final C0037zza zzl(zzcd zzcd) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzj(zzcd);
                    return this;
                }

                /* synthetic */ C0037zza(zzbt zzbt) {
                    this();
                }
            }

            static {
                zze zze = new zze();
                zzkd = zze;
                zzdrt.zza(zze.class, zze);
            }

            private zze() {
            }

            /* access modifiers changed from: private */
            public final void zzaq() {
                this.zzdl &= -9;
                this.zzjl = -1;
            }

            public static C0037zza zzar() {
                return (C0037zza) zzkd.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zzbs(long j) {
                this.zzdl |= 4;
                this.zzjk = j;
            }

            /* access modifiers changed from: private */
            public final void zzbt(long j) {
                this.zzdl |= 8;
                this.zzjl = j;
            }

            /* access modifiers changed from: private */
            public final void zzbu(long j) {
                this.zzdl |= 16;
                this.zzjm = j;
            }

            /* access modifiers changed from: private */
            public final void zzbv(long j) {
                this.zzdl |= 32;
                this.zzjn = j;
            }

            /* access modifiers changed from: private */
            public final void zzbw(long j) {
                this.zzdl |= 128;
                this.zzjp = j;
            }

            /* access modifiers changed from: private */
            public final void zzbx(long j) {
                this.zzdl |= 256;
                this.zzjq = j;
            }

            /* access modifiers changed from: private */
            public final void zzby(long j) {
                this.zzdl |= AdRequest.MAX_CONTENT_URL_LENGTH;
                this.zzjr = j;
            }

            /* access modifiers changed from: private */
            public final void zzbz(long j) {
                this.zzdl |= 2048;
                this.zzjt = j;
            }

            /* access modifiers changed from: private */
            public final void zzca(long j) {
                this.zzdl |= 4096;
                this.zzju = j;
            }

            /* access modifiers changed from: private */
            public final void zzcb(long j) {
                this.zzdl |= 8192;
                this.zzjv = j;
            }

            /* access modifiers changed from: private */
            public final void zzcc(long j) {
                this.zzdl |= Http2.INITIAL_MAX_FRAME_SIZE;
                this.zzjw = j;
            }

            /* access modifiers changed from: private */
            public final void zzcd(long j) {
                this.zzdl |= 32768;
                this.zzjx = j;
            }

            /* access modifiers changed from: private */
            public final void zzce(long j) {
                this.zzdl |= ImageMetadata.CONTROL_AE_ANTIBANDING_MODE;
                this.zzjy = j;
            }

            /* access modifiers changed from: private */
            public final void zzcf(long j) {
                this.zzdl |= 131072;
                this.zzjz = j;
            }

            /* access modifiers changed from: private */
            public final void zzcg(long j) {
                this.zzdl |= 262144;
                this.zzka = j;
            }

            /* access modifiers changed from: private */
            public final void zzi(zzcd zzcd) {
                this.zzjo = zzcd.zzae();
                this.zzdl |= 64;
            }

            /* access modifiers changed from: private */
            public final void zzj(long j) {
                this.zzdl |= 1;
                this.zzfl = j;
            }

            /* access modifiers changed from: private */
            public final void zzk(long j) {
                this.zzdl |= 2;
                this.zzfm = j;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzbt.zzdk[i - 1]) {
                    case 1:
                        return new zze();
                    case 2:
                        return new C0037zza((zzbt) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzkd, "\u0001\u0015\u0000\u0001\u0001\u0015\u0015\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0002\u0001\u0003\u0002\u0002\u0004\u0002\u0003\u0005\u0002\u0004\u0006\u0002\u0005\u0007\f\u0006\b\u0002\u0007\t\u0002\b\n\u0002\t\u000b\f\n\f\u0002\u000b\r\u0002\f\u000e\u0002\r\u000f\u0002\u000e\u0010\u0002\u000f\u0011\u0002\u0010\u0012\u0002\u0011\u0013\u0002\u0012\u0014\u0002\u0013\u0015\u0002\u0014", new Object[]{"zzdl", "zzfl", "zzfm", "zzjk", "zzjl", "zzjm", "zzjn", "zzjo", zzcd.zzaf(), "zzjp", "zzjq", "zzjr", "zzjs", zzcd.zzaf(), "zzjt", "zzju", "zzjv", "zzjw", "zzjx", "zzjy", "zzjz", "zzka", "zzkb", "zzkc"});
                    case 4:
                        return zzkd;
                    case 5:
                        zzdtn<zze> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zze.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzkd);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            /* access modifiers changed from: private */
            public final void zzj(zzcd zzcd) {
                this.zzjs = zzcd.zzae();
                this.zzdl |= ByteConstants.KB;
            }
        }

        /* access modifiers changed from: private */
        public final void zzag(long j) {
            this.zzex |= 4194304;
            this.zzgx = j;
        }

        /* access modifiers changed from: private */
        public final void zzaj(long j) {
            this.zzex |= 134217728;
            this.zzhc = j;
        }

        /* access modifiers changed from: private */
        public final void zzak(long j) {
            this.zzey |= ByteConstants.KB;
            this.zzhr = j;
        }

        /* access modifiers changed from: private */
        public final void zza(zze zze2) {
            zze2.getClass();
            this.zzgs = zze2;
            this.zzex |= 262144;
        }

        /* access modifiers changed from: private */
        public final void zzab(String str) {
            str.getClass();
            this.zzex |= 128;
            this.zzes = str;
        }

        /* access modifiers changed from: private */
        public final void zzad(String str) {
            str.getClass();
            this.zzex |= 268435456;
            this.zzhd = str;
        }

        /* access modifiers changed from: private */
        public final void zzae(String str) {
            str.getClass();
            this.zzey |= 64;
            this.zzhn = str;
        }

        /* access modifiers changed from: private */
        public final void zzb(zze zze2) {
            zze2.getClass();
            if (!this.zzgt.zzaxp()) {
                this.zzgt = zzdrt.zza(this.zzgt);
            }
            this.zzgt.add(zze2);
        }

        /* access modifiers changed from: private */
        public final void zzaa(long j) {
            this.zzex |= 32;
            this.zzgh = j;
        }

        /* access modifiers changed from: private */
        public final void zzac(long j) {
            this.zzex |= 4096;
            this.zzgm = j;
        }

        /* access modifiers changed from: private */
        public final void zzw(long j) {
            this.zzex |= 2;
            this.zzgd = j;
        }

        /* access modifiers changed from: private */
        public final void zzx(long j) {
            this.zzex |= 4;
            this.zzge = j;
        }

        /* access modifiers changed from: private */
        public final void zzy(long j) {
            this.zzex |= 8;
            this.zzgf = j;
        }

        /* access modifiers changed from: private */
        public final void zzz(long j) {
            this.zzex |= 16;
            this.zzgg = j;
        }

        /* access modifiers changed from: private */
        public final void zza(zzf zzf2) {
            zzf2.getClass();
            this.zzgu = zzf2;
            this.zzex |= ImageMetadata.LENS_APERTURE;
        }

        /* access modifiers changed from: private */
        public final void zza(zzc zzc2) {
            this.zzho = zzc2.zzae();
            this.zzey |= 128;
        }

        /* access modifiers changed from: private */
        public final void zza(boolean z) {
            this.zzey |= 256;
            this.zzhp = z;
        }

        public static zza zza(byte[] bArr, zzdrg zzdrg) throws zzdse {
            return (zza) zzdrt.zza(zzht, bArr, zzdrg);
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzbt.zzdk[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new zzb((zzbt) null);
                case 3:
                    return zzdrt.zza((zzdte) zzht, "\u0001M\u0000\u0003\u0001ÉM\u0000\u0001\u0000\u0001\b\u0000\u0002\b\u0001\u0003\u0002\u0002\u0004\u0002\u0003\u0005\u0002\u0004\u0006\u0002\u0005\u0007\u0002\u0006\b\u0002\u0007\t\u0002\b\n\u0002\t\u000b\u0002\n\f\u0002\u000b\r\b\f\u000e\u0002\r\u000f\u0002\u000e\u0010\u0002\u000f\u0011\u0002\u0010\u0012\u0002\u0011\u0013\u0002\u0012\u0014\u0002\u0013\u0015\u0002E\u0016\u0002\u0014\u0017\u0002\u0015\u0018\bF\u0019\u0002J\u001a\fG\u001b\b\u0016\u001c\u0007H\u001d\b\u0018\u001e\bI\u001f\u0002\u0019 \u0002\u001a!\u0002\u001b\"\b\u001c#\u0002\u001d$\u0002\u001e%\u0002\u001f&\t '\u0002!(\u0002\")\u0002#*\u0002$+\u001b,\u0002%-\u0002&.\b'/\b(0\f*1\f+2\t23\u0002,4\u0002-5\u0002.6\u0002/7\u000208\f19\t3:\u00024;\u00025<\u00026=\u00027>\u0002:?\u0002;@\u0002=A\f>B\f?C\b<D\f@E\tAF\u0002BG\u00028H\u00029I\fCJ\u0002)K\b\u0017L\fDÉ\tK", new Object[]{"zzdl", "zzex", "zzey", "zzez", "zzdv", "zzfa", "zzfb", "zzfc", "zzfd", "zzfe", "zzff", "zzfg", "zzfh", "zzfi", "zzfj", "zzfk", "zzfl", "zzfm", "zzfn", "zzfo", "zzfp", "zzfq", "zzfr", "zzhm", "zzfs", "zzft", "zzhn", "zzhr", "zzho", zzc.zzaf(), "zzep", "zzhp", "zzfv", "zzhq", "zzfw", "zzfx", "zzfy", "zzer", "zzfz", "zzga", "zzgb", "zzgc", "zzgd", "zzge", "zzgf", "zzgg", "zzgt", zze.class, "zzgh", "zzgi", "zzes", "zzet", "zzgk", zzcd.zzaf(), "zzgl", zzcd.zzaf(), "zzgs", "zzgm", "zzgn", "zzgo", "zzgp", "zzgq", "zzgr", zzcd.zzaf(), "zzgu", "zzgv", "zzgw", "zzgx", "zzgy", "zzhb", "zzhc", "zzhe", "zzhf", zzbz.zzaf(), "zzhg", zzce.zzaf(), "zzhd", "zzhh", C0036zza.zzaf(), "zzhi", "zzhj", "zzgz", "zzha", "zzhk", zzcd.zzaf(), "zzgj", "zzfu", "zzhl", zzcd.zzaf(), "zzhs"});
                case 4:
                    return zzht;
                case 5:
                    zzdtn<zza> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zza.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzht);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzb extends zzdrt<zzb, zza> implements zzdtg {
        private static volatile zzdtn<zzb> zzdz;
        /* access modifiers changed from: private */
        public static final zzb zzko;
        private int zzdl;
        private long zzkj;
        private int zzkk;
        private boolean zzkl;
        private zzdrz zzkm = zzdrt.zzazv();
        private long zzkn;

        public static final class zza extends zzdrt.zzb<zzb, zza> implements zzdtg {
            private zza() {
                super(zzb.zzko);
            }

            /* synthetic */ zza(zzbt zzbt) {
                this();
            }
        }

        static {
            zzb zzb = new zzb();
            zzko = zzb;
            zzdrt.zza(zzb.class, zzb);
        }

        private zzb() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzbt.zzdk[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza((zzbt) null);
                case 3:
                    return zzdrt.zza((zzdte) zzko, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u0002\u0000\u0002\u0004\u0001\u0003\u0007\u0002\u0004\u0016\u0005\u0003\u0003", new Object[]{"zzdl", "zzkj", "zzkk", "zzkl", "zzkm", "zzkn"});
                case 4:
                    return zzko;
                case 5:
                    zzdtn<zzb> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzb.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzko);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzd extends zzdrt<zzd, zza> implements zzdtg {
        private static volatile zzdtn<zzd> zzdz;
        /* access modifiers changed from: private */
        public static final zzd zzll;
        private int zzdl;
        private long zzkj;
        private String zzlj = "";
        private zzdqk zzlk = zzdqk.zzhhx;

        public static final class zza extends zzdrt.zzb<zzd, zza> implements zzdtg {
            private zza() {
                super(zzd.zzll);
            }

            /* synthetic */ zza(zzbt zzbt) {
                this();
            }
        }

        static {
            zzd zzd = new zzd();
            zzll = zzd;
            zzdrt.zza(zzd.class, zzd);
        }

        private zzd() {
        }

        public static zzd zzbf() {
            return zzll;
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzbt.zzdk[i - 1]) {
                case 1:
                    return new zzd();
                case 2:
                    return new zza((zzbt) null);
                case 3:
                    return zzdrt.zza((zzdte) zzll, "\u0001\u0003\u0000\u0001\u0001\u0004\u0003\u0000\u0000\u0000\u0001\u0002\u0000\u0003\b\u0001\u0004\n\u0002", new Object[]{"zzdl", "zzkj", "zzlj", "zzlk"});
                case 4:
                    return zzll;
                case 5:
                    zzdtn<zzd> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzd.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzll);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final boolean zzbd() {
            return (this.zzdl & 1) != 0;
        }

        public final long zzbe() {
            return this.zzkj;
        }
    }

    public static final class zze extends zzdrt<zze, zza> implements zzdtg {
        private static volatile zzdtn<zze> zzdz;
        /* access modifiers changed from: private */
        public static final zze zzlm;
        private int zzdl;
        private String zzeu = "";

        public static final class zza extends zzdrt.zzb<zze, zza> implements zzdtg {
            private zza() {
                super(zze.zzlm);
            }

            /* synthetic */ zza(zzbt zzbt) {
                this();
            }
        }

        static {
            zze zze = new zze();
            zzlm = zze;
            zzdrt.zza(zze.class, zze);
        }

        private zze() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzbt.zzdk[i - 1]) {
                case 1:
                    return new zze();
                case 2:
                    return new zza((zzbt) null);
                case 3:
                    return zzdrt.zza((zzdte) zzlm, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001\b\u0000", new Object[]{"zzdl", "zzeu"});
                case 4:
                    return zzlm;
                case 5:
                    zzdtn<zze> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zze.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzlm);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzc extends zzdrt<zzc, zza> implements zzdtg {
        private static volatile zzdtn<zzc> zzdz;
        /* access modifiers changed from: private */
        public static final zzc zzkt;
        private int zzdl;
        private zzdqk zzkp;
        private zzdqk zzkq;
        private zzdqk zzkr;
        private zzdqk zzks;

        public static final class zza extends zzdrt.zzb<zzc, zza> implements zzdtg {
            private zza() {
                super(zzc.zzkt);
            }

            public final zza zze(zzdqk zzdqk) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzc) this.zzhmp).zza(zzdqk);
                return this;
            }

            public final zza zzf(zzdqk zzdqk) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzc) this.zzhmp).zzb(zzdqk);
                return this;
            }

            public final zza zzg(zzdqk zzdqk) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzc) this.zzhmp).zzc(zzdqk);
                return this;
            }

            public final zza zzh(zzdqk zzdqk) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzc) this.zzhmp).zzd(zzdqk);
                return this;
            }

            /* synthetic */ zza(zzbt zzbt) {
                this();
            }
        }

        static {
            zzc zzc = new zzc();
            zzkt = zzc;
            zzdrt.zza(zzc.class, zzc);
        }

        private zzc() {
            zzdqk zzdqk = zzdqk.zzhhx;
            this.zzkp = zzdqk;
            this.zzkq = zzdqk;
            this.zzkr = zzdqk;
            this.zzks = zzdqk;
        }

        /* access modifiers changed from: private */
        public final void zza(zzdqk zzdqk) {
            zzdqk.getClass();
            this.zzdl |= 1;
            this.zzkp = zzdqk;
        }

        /* access modifiers changed from: private */
        public final void zzb(zzdqk zzdqk) {
            zzdqk.getClass();
            this.zzdl |= 2;
            this.zzkq = zzdqk;
        }

        public static zza zzbb() {
            return (zza) zzkt.zzazt();
        }

        /* access modifiers changed from: private */
        public final void zzc(zzdqk zzdqk) {
            zzdqk.getClass();
            this.zzdl |= 4;
            this.zzkr = zzdqk;
        }

        /* access modifiers changed from: private */
        public final void zzd(zzdqk zzdqk) {
            zzdqk.getClass();
            this.zzdl |= 8;
            this.zzks = zzdqk;
        }

        public final zzdqk zzax() {
            return this.zzkp;
        }

        public final zzdqk zzay() {
            return this.zzkq;
        }

        public final zzdqk zzaz() {
            return this.zzkr;
        }

        public final zzdqk zzba() {
            return this.zzks;
        }

        public static zzc zzb(byte[] bArr, zzdrg zzdrg) throws zzdse {
            return (zzc) zzdrt.zza(zzkt, bArr, zzdrg);
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzbt.zzdk[i - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new zza((zzbt) null);
                case 3:
                    return zzdrt.zza((zzdte) zzkt, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\n\u0000\u0002\n\u0001\u0003\n\u0002\u0004\n\u0003", new Object[]{"zzdl", "zzkp", "zzkq", "zzkr", "zzks"});
                case 4:
                    return zzkt;
                case 5:
                    zzdtn<zzc> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzc.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzkt);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzf extends zzdrt<zzf, zza> implements zzdtg {
        private static volatile zzdtn<zzf> zzdz;
        /* access modifiers changed from: private */
        public static final zzf zzlo;
        private int zzdl;
        private int zzhf = 1;
        private int zzhg = 1;
        private zzdqk zzkq = zzdqk.zzhhx;
        private zzdsb<zzdqk> zzln = zzdrt.zzazw();

        public static final class zza extends zzdrt.zzb<zzf, zza> implements zzdtg {
            private zza() {
                super(zzf.zzlo);
            }

            public final zza zza(zzbz zzbz) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzf) this.zzhmp).zzb(zzbz);
                return this;
            }

            public final zza zzi(zzdqk zzdqk) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzf) this.zzhmp).zzk(zzdqk);
                return this;
            }

            public final zza zzj(zzdqk zzdqk) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzf) this.zzhmp).zzb(zzdqk);
                return this;
            }

            /* synthetic */ zza(zzbt zzbt) {
                this();
            }
        }

        static {
            zzf zzf = new zzf();
            zzlo = zzf;
            zzdrt.zza(zzf.class, zzf);
        }

        private zzf() {
        }

        /* access modifiers changed from: private */
        public final void zzb(zzdqk zzdqk) {
            zzdqk.getClass();
            this.zzdl |= 1;
            this.zzkq = zzdqk;
        }

        public static zza zzbi() {
            return (zza) zzlo.zzazt();
        }

        /* access modifiers changed from: private */
        public final void zzk(zzdqk zzdqk) {
            zzdqk.getClass();
            if (!this.zzln.zzaxp()) {
                this.zzln = zzdrt.zza(this.zzln);
            }
            this.zzln.add(zzdqk);
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzbt.zzdk[i - 1]) {
                case 1:
                    return new zzf();
                case 2:
                    return new zza((zzbt) null);
                case 3:
                    return zzdrt.zza((zzdte) zzlo, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u001c\u0002\n\u0000\u0003\f\u0001\u0004\f\u0002", new Object[]{"zzdl", "zzln", "zzkq", "zzhg", zzce.zzaf(), "zzhf", zzbz.zzaf()});
                case 4:
                    return zzlo;
                case 5:
                    zzdtn<zzf> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzf.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzlo);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        /* access modifiers changed from: private */
        public final void zzb(zzbz zzbz) {
            this.zzhf = zzbz.zzae();
            this.zzdl |= 4;
        }
    }
}
