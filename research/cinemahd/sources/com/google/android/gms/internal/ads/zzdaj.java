package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;

public final class zzdaj implements zzdxg<Clock> {
    private final zzdag zzgna;

    public zzdaj(zzdag zzdag) {
        this.zzgna = zzdag;
    }

    public final /* synthetic */ Object get() {
        return (Clock) zzdxm.zza(DefaultClock.c(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
