package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;

public final class zzauk {
    private static Uri zza(String str, String str2, String str3) {
        int indexOf = str.indexOf("&adurl");
        if (indexOf == -1) {
            indexOf = str.indexOf("?adurl");
        }
        if (indexOf == -1) {
            return Uri.parse(str).buildUpon().appendQueryParameter(str2, str3).build();
        }
        int i = indexOf + 1;
        return Uri.parse(str.substring(0, i) + str2 + "=" + str3 + "&" + str.substring(i));
    }

    public static String zzb(String str, Context context, boolean z) {
        String zzag;
        if ((((Boolean) zzve.zzoy().zzd(zzzn.zzcik)).booleanValue() && !z) || !zzq.zzlo().zzab(context) || TextUtils.isEmpty(str) || (zzag = zzq.zzlo().zzag(context)) == null) {
            return str;
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcic)).booleanValue()) {
            String str2 = (String) zzve.zzoy().zzd(zzzn.zzcid);
            if (!str.contains(str2)) {
                return str;
            }
            if (zzq.zzkq().zzek(str)) {
                zzq.zzlo().zzh(context, zzag);
                return str.replace(str2, zzag);
            } else if (!zzq.zzkq().zzel(str)) {
                return str;
            } else {
                zzq.zzlo().zzi(context, zzag);
                return str.replace(str2, zzag);
            }
        } else if (str.contains("fbs_aeid")) {
            return str;
        } else {
            if (zzq.zzkq().zzek(str)) {
                zzq.zzlo().zzh(context, zzag);
                return zza(str, "fbs_aeid", zzag).toString();
            } else if (!zzq.zzkq().zzel(str)) {
                return str;
            } else {
                zzq.zzlo().zzi(context, zzag);
                return zza(str, "fbs_aeid", zzag).toString();
            }
        }
    }

    public static String zzb(Uri uri, Context context) {
        if (!zzq.zzlo().zzab(context)) {
            return uri.toString();
        }
        String zzag = zzq.zzlo().zzag(context);
        if (zzag == null) {
            return uri.toString();
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcic)).booleanValue()) {
            String str = (String) zzve.zzoy().zzd(zzzn.zzcid);
            String uri2 = uri.toString();
            if (uri2.contains(str)) {
                zzq.zzlo().zzh(context, zzag);
                return uri2.replace(str, zzag);
            }
        } else if (TextUtils.isEmpty(uri.getQueryParameter("fbs_aeid"))) {
            uri = zza(uri.toString(), "fbs_aeid", zzag);
            zzq.zzlo().zzh(context, zzag);
        }
        return uri.toString();
    }
}
