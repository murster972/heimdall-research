package com.google.android.gms.internal.ads;

public final class zzoz {
    public final int height;
    public final int width;
    public final int zzbio;

    public zzoz(int i, int i2, int i3) {
        this.width = i;
        this.height = i2;
        this.zzbio = i3;
    }
}
