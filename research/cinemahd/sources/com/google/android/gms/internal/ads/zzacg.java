package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzacg extends zzgb implements zzacd {
    public zzacg() {
        super("com.google.android.gms.ads.internal.formats.client.IMediaContent");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 2) {
            float aspectRatio = getAspectRatio();
            parcel2.writeNoException();
            parcel2.writeFloat(aspectRatio);
            return true;
        } else if (i == 3) {
            zzo(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
            parcel2.writeNoException();
            return true;
        } else if (i != 4) {
            return false;
        } else {
            IObjectWrapper zzre = zzre();
            parcel2.writeNoException();
            zzge.zza(parcel2, (IInterface) zzre);
            return true;
        }
    }
}
