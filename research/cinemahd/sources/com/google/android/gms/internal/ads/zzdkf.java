package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public final class zzdkf {
    @Deprecated
    public static final zzdny zzgyy = zzdny.zzawv();
    @Deprecated
    private static final zzdny zzgyz = zzdny.zzawv();
    @Deprecated
    private static final zzdny zzgza = zzdny.zzawv();
    private static final String zzgzo = new zzdkd().getKeyType();
    private static final String zzgzp = new zzdkc().getKeyType();

    static {
        try {
            zzdiy.zzasq();
            zzdit.zza(new zzdkc(), new zzdkd(), true);
            zzdit.zza(new zzdkh());
            zzdit.zza(new zzdki());
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
}
