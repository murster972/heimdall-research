package com.google.android.gms.internal.ads;

import android.os.Bundle;

final /* synthetic */ class zzcqc implements zzcty {
    private final String zzcyz;

    zzcqc(String str) {
        this.zzcyz = str;
    }

    public final void zzr(Object obj) {
        ((Bundle) obj).putString("ms", this.zzcyz);
    }
}
