package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;
import com.google.android.gms.internal.ads.zzsy;

public final class zzccw implements zzbow, zzbpe, zzbqb, zzbqx, zzbri, zzty {
    private final zzsm zzfry;
    private boolean zzfrz = false;
    private boolean zzfsa = false;

    public zzccw(zzsm zzsm, zzcxw zzcxw) {
        this.zzfry = zzsm;
        zzsm.zza(zzso.zza.C0047zza.AD_REQUEST);
        if (zzcxw != null) {
            zzsm.zza(zzso.zza.C0047zza.REQUEST_IS_PREFETCH);
        }
    }

    public final synchronized void onAdClicked() {
        if (!this.zzfsa) {
            this.zzfry.zza(zzso.zza.C0047zza.AD_FIRST_CLICK);
            this.zzfsa = true;
            return;
        }
        this.zzfry.zza(zzso.zza.C0047zza.AD_SUBSEQUENT_CLICK);
    }

    public final void onAdFailedToLoad(int i) {
        switch (i) {
            case 1:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD_INVALID_REQUEST);
                return;
            case 2:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD_NETWORK_ERROR);
                return;
            case 3:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD_NO_FILL);
                return;
            case 4:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD_TIMEOUT);
                return;
            case 5:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD_CANCELLED);
                return;
            case 6:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD_NO_ERROR);
                return;
            case 7:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD_NOT_FOUND);
                return;
            default:
                this.zzfry.zza(zzso.zza.C0047zza.AD_FAILED_TO_LOAD);
                return;
        }
    }

    public final synchronized void onAdImpression() {
        this.zzfry.zza(zzso.zza.C0047zza.AD_IMPRESSION);
    }

    public final void onAdLoaded() {
        this.zzfry.zza(zzso.zza.C0047zza.AD_LOADED);
    }

    public final void zza(zzsy.zza zza) {
        this.zzfry.zza((zzsp) new zzccy(zza));
        this.zzfry.zza(zzso.zza.C0047zza.REQUEST_LOADED_FROM_CACHE);
    }

    public final void zzb(zzaqk zzaqk) {
    }

    public final void zzb(zzczt zzczt) {
        this.zzfry.zza((zzsp) new zzccv(zzczt));
    }

    public final void zzc(zzsy.zza zza) {
        this.zzfry.zza((zzsp) new zzcda(zza));
        this.zzfry.zza(zzso.zza.C0047zza.REQUEST_PREFETCH_INTERCEPTED);
    }

    public final void zzb(zzsy.zza zza) {
        this.zzfry.zza((zzsp) new zzccx(zza));
        this.zzfry.zza(zzso.zza.C0047zza.REQUEST_SAVED_TO_CACHE);
    }
}
