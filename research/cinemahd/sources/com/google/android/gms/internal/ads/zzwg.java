package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzwg extends zzgb implements zzwd {
    public zzwg() {
        super("com.google.android.gms.ads.internal.client.IClientApi");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                zzvu zza = zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), (zzuj) zzge.zza(parcel, zzuj.CREATOR), parcel.readString(), zzalb.zzaa(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zza);
                return true;
            case 2:
                zzvu zzb = zzb(IObjectWrapper.Stub.a(parcel.readStrongBinder()), (zzuj) zzge.zza(parcel, zzuj.CREATOR), parcel.readString(), zzalb.zzaa(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zzb);
                return true;
            case 3:
                zzvn zza2 = zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), parcel.readString(), zzalb.zzaa(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zza2);
                return true;
            case 4:
                zzwk zzc = zzc(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zzc);
                return true;
            case 5:
                zzacm zza3 = zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zza3);
                return true;
            case 6:
                zzarl zza4 = zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), zzalb.zzaa(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zza4);
                return true;
            case 7:
                zzapd zzd = zzd(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zzd);
                return true;
            case 8:
                zzaot zzb2 = zzb(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zzb2);
                return true;
            case 9:
                zzwk zza5 = zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zza5);
                return true;
            case 10:
                zzvu zza6 = zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), (zzuj) zzge.zza(parcel, zzuj.CREATOR), parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zza6);
                return true;
            case 11:
                zzacp zza7 = zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), IObjectWrapper.Stub.a(parcel.readStrongBinder()), IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zza7);
                return true;
            case 12:
                zzasg zzb3 = zzb(IObjectWrapper.Stub.a(parcel.readStrongBinder()), parcel.readString(), zzalb.zzaa(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zzb3);
                return true;
            case 13:
                zzvu zzc2 = zzc(IObjectWrapper.Stub.a(parcel.readStrongBinder()), (zzuj) zzge.zza(parcel, zzuj.CREATOR), parcel.readString(), zzalb.zzaa(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                zzge.zza(parcel2, (IInterface) zzc2);
                return true;
            default:
                return false;
        }
    }
}
