package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzyj extends zzasj {
    public final Bundle getAdMetadata() throws RemoteException {
        return new Bundle();
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return "";
    }

    public final boolean isLoaded() throws RemoteException {
        return false;
    }

    public final void zza(IObjectWrapper iObjectWrapper, boolean z) {
    }

    public final void zza(zzasl zzasl) throws RemoteException {
    }

    public final void zza(zzast zzast) throws RemoteException {
    }

    public final void zza(zzatb zzatb) {
    }

    public final void zza(zzug zzug, zzaso zzaso) throws RemoteException {
        zzayu.zzex("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzayk.zzyu.post(new zzym(zzaso));
    }

    public final void zza(zzwv zzwv) throws RemoteException {
    }

    public final void zzh(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final zzxa zzkb() {
        return null;
    }

    public final zzasf zzpz() {
        return null;
    }
}
