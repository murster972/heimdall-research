package com.google.android.gms.internal.ads;

final class zzbmu implements zzdgt<zzbmd> {
    private final /* synthetic */ zzdgt zzfgc;
    private final /* synthetic */ zzbmo zzfgd;

    zzbmu(zzbmo zzbmo, zzdgt zzdgt) {
        this.zzfgd = zzbmo;
        this.zzfgc = zzdgt;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        this.zzfgd.zzagu();
        this.zzfgc.onSuccess((zzbmd) obj);
    }

    public final void zzb(Throwable th) {
        this.zzfgd.zzagu();
        this.zzfgc.zzb(th);
    }
}
