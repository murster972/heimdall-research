package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbpw extends zzbrl<zzbqb> implements zzbqb {
    public zzbpw(Set<zzbsu<zzbqb>> set) {
        super(set);
    }

    public final void onAdLoaded() {
        zza(zzbpz.zzfhp);
    }
}
