package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.PriorityQueue;

public final class zzqr {
    private final int zzbqg;
    private final int zzbqh;
    private final int zzbqi;
    private final zzqo zzbqj = new zzqv();

    public zzqr(int i) {
        this.zzbqh = i;
        this.zzbqg = 6;
        this.zzbqi = 0;
    }

    private final String zzbw(String str) {
        String[] split = str.split(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        if (split.length == 0) {
            return "";
        }
        zzqt zzqt = new zzqt();
        PriorityQueue priorityQueue = new PriorityQueue(this.zzbqh, new zzqq(this));
        for (String zzd : split) {
            String[] zzd2 = zzqs.zzd(zzd, false);
            if (zzd2.length != 0) {
                zzqx.zza(zzd2, this.zzbqh, this.zzbqg, priorityQueue);
            }
        }
        Iterator it2 = priorityQueue.iterator();
        while (it2.hasNext()) {
            try {
                zzqt.write(this.zzbqj.zzbv(((zzqw) it2.next()).zzbqn));
            } catch (IOException e) {
                zzayu.zzc("Error while writing hash to byteStream", e);
            }
        }
        return zzqt.toString();
    }

    public final String zza(ArrayList<String> arrayList) {
        StringBuilder sb = new StringBuilder();
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            String str = arrayList.get(i);
            i++;
            sb.append(str.toLowerCase(Locale.US));
            sb.append(10);
        }
        return zzbw(sb.toString());
    }
}
