package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzako<I, O> implements zzdgf<I, O> {
    /* access modifiers changed from: private */
    public final zzajw<O> zzdaw;
    private final zzajv<I> zzdax;
    private final String zzday;
    private final zzdhe<zzajq> zzdbd;

    zzako(zzdhe<zzajq> zzdhe, String str, zzajv<I> zzajv, zzajw<O> zzajw) {
        this.zzdbd = zzdhe;
        this.zzday = str;
        this.zzdax = zzajv;
        this.zzdaw = zzajw;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(Object obj, zzajq zzajq) throws Exception {
        zzazl zzazl = new zzazl();
        zzq.zzkq();
        String zzwk = zzawb.zzwk();
        zzafa.zzcxi.zza(zzwk, (zzafv) new zzakq(this, zzazl));
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", zzwk);
        jSONObject.put("args", this.zzdax.zzj(obj));
        zzajq.zza(this.zzday, jSONObject);
        return zzazl;
    }

    public final zzdhe<O> zzf(I i) throws Exception {
        return zzdgs.zzb(this.zzdbd, new zzakn(this, i), (Executor) zzazd.zzdwj);
    }
}
