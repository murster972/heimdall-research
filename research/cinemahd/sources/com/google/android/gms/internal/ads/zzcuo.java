package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

final /* synthetic */ class zzcuo implements zzded {
    static final zzded zzdoq = new zzcuo();

    private zzcuo() {
    }

    public final Object apply(Object obj) {
        AdvertisingIdClient.Info info2 = (AdvertisingIdClient.Info) obj;
        info2.getClass();
        return new zzcum(info2, (String) null);
    }
}
