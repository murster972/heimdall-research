package com.google.android.gms.internal.ads;

import java.io.Closeable;
import java.io.IOException;

public class zzbd extends zzdwq implements Closeable {
    private static zzdwy zzcr = zzdwy.zzn(zzbd.class);

    public zzbd(zzdws zzdws, zzbe zzbe) throws IOException {
        zza(zzdws, zzdws.size(), zzbe);
    }

    public void close() throws IOException {
        this.zzhyv.close();
    }

    public String toString() {
        String obj = this.zzhyv.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 7);
        sb.append("model(");
        sb.append(obj);
        sb.append(")");
        return sb.toString();
    }
}
