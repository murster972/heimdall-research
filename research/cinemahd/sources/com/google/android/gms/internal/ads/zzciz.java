package com.google.android.gms.internal.ads;

final /* synthetic */ class zzciz implements zzbme {
    private final zzbdi zzehp;

    private zzciz(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static zzbme zzp(zzbdi zzbdi) {
        return new zzciz(zzbdi);
    }

    public final zzxb getVideoController() {
        return this.zzehp.zzyl();
    }
}
