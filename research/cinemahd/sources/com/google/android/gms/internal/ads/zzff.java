package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzff extends zzfw {
    public zzff(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 11);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        this.zzzt.zzao(((Long) this.zzaae.invoke((Object) null, new Object[]{this.zzuv.getContext()})).longValue());
    }
}
