package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

final class zzbct implements Runnable {
    private final /* synthetic */ String val$message;
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ String zzedb;
    private final /* synthetic */ zzbcn zzedf;
    private final /* synthetic */ String zzedl;

    zzbct(zzbcn zzbcn, String str, String str2, String str3, String str4) {
        this.zzedf = zzbcn;
        this.zzdug = str;
        this.zzedb = str2;
        this.zzedl = str3;
        this.val$message = str4;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "precacheCanceled");
        hashMap.put("src", this.zzdug);
        if (!TextUtils.isEmpty(this.zzedb)) {
            hashMap.put("cachedSrc", this.zzedb);
        }
        hashMap.put("type", zzbcn.zzfk(this.zzedl));
        hashMap.put("reason", this.zzedl);
        if (!TextUtils.isEmpty(this.val$message)) {
            hashMap.put("message", this.val$message);
        }
        this.zzedf.zza("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
