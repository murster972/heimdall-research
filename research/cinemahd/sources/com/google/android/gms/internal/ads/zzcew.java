package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcew implements Callable {
    private final zzceq zzftj;

    zzcew(zzceq zzceq) {
        this.zzftj = zzceq;
    }

    public final Object call() {
        return this.zzftj.zzalo();
    }
}
