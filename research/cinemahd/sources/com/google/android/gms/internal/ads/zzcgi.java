package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.ConnectionResult;
import java.io.InputStream;

public final class zzcgi extends zzcgk {
    public zzcgi(Context context) {
        this.zzfvt = new zzaps(context, zzq.zzle().zzxb(), this, this);
    }

    public final void onConnected(Bundle bundle) {
        synchronized (this.mLock) {
            if (!this.zzfvr) {
                this.zzfvr = true;
                try {
                    this.zzfvt.zztx().zza(this.zzfvs, (zzaqe) new zzcgj(this));
                } catch (RemoteException | IllegalArgumentException unused) {
                    this.zzdbf.setException(new zzcgr(0));
                } catch (Throwable th) {
                    zzq.zzku().zza(th, "RemoteAdRequestClientTask.onConnected");
                    this.zzdbf.setException(new zzcgr(0));
                }
            }
        }
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        zzayu.zzea("Cannot connect to remote service, fallback to local instance.");
        this.zzdbf.setException(new zzcgr(0));
    }

    public final zzdhe<InputStream> zzf(zzaqk zzaqk) {
        synchronized (this.mLock) {
            if (this.zzfvq) {
                zzazl<InputStream> zzazl = this.zzdbf;
                return zzazl;
            }
            this.zzfvq = true;
            this.zzfvs = zzaqk;
            this.zzfvt.checkAvailabilityAndConnect();
            this.zzdbf.addListener(new zzcgh(this), zzazd.zzdwj);
            zzazl<InputStream> zzazl2 = this.zzdbf;
            return zzazl2;
        }
    }
}
