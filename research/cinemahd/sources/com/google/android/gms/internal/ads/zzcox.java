package com.google.android.gms.internal.ads;

import android.os.RemoteException;

public interface zzcox<AdT> {
    boolean isLoading();

    boolean zza(zzug zzug, String str, zzcpa zzcpa, zzcoz<? super AdT> zzcoz) throws RemoteException;
}
