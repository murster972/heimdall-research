package com.google.android.gms.internal.ads;

public final class zzbkx implements zzdxg<zzbsu<zzbpe>> {
    private final zzdxp<zzblw> zzfdq;
    private final zzbkn zzfen;

    public zzbkx(zzbkn zzbkn, zzdxp<zzblw> zzdxp) {
        this.zzfen = zzbkn;
        this.zzfdq = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), zzazd.zzdwi), "Cannot return null from a non-@Nullable @Provides method");
    }
}
