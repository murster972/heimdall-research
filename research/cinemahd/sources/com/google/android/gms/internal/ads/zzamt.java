package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class zzamt extends zzalu {
    private final UnifiedNativeAdMapper zzdeh;

    public zzamt(UnifiedNativeAdMapper unifiedNativeAdMapper) {
        this.zzdeh = unifiedNativeAdMapper;
    }

    public final String getAdvertiser() {
        return this.zzdeh.getAdvertiser();
    }

    public final String getBody() {
        return this.zzdeh.getBody();
    }

    public final String getCallToAction() {
        return this.zzdeh.getCallToAction();
    }

    public final Bundle getExtras() {
        return this.zzdeh.getExtras();
    }

    public final String getHeadline() {
        return this.zzdeh.getHeadline();
    }

    public final List getImages() {
        List<NativeAd.Image> images = this.zzdeh.getImages();
        ArrayList arrayList = new ArrayList();
        if (images != null) {
            for (NativeAd.Image next : images) {
                arrayList.add(new zzabu(next.getDrawable(), next.getUri(), next.getScale(), next.getWidth(), next.getHeight()));
            }
        }
        return arrayList;
    }

    public final float getMediaContentAspectRatio() {
        return this.zzdeh.getMediaContentAspectRatio();
    }

    public final boolean getOverrideClickHandling() {
        return this.zzdeh.getOverrideClickHandling();
    }

    public final boolean getOverrideImpressionRecording() {
        return this.zzdeh.getOverrideImpressionRecording();
    }

    public final String getPrice() {
        return this.zzdeh.getPrice();
    }

    public final double getStarRating() {
        if (this.zzdeh.getStarRating() != null) {
            return this.zzdeh.getStarRating().doubleValue();
        }
        return -1.0d;
    }

    public final String getStore() {
        return this.zzdeh.getStore();
    }

    public final zzxb getVideoController() {
        if (this.zzdeh.getVideoController() != null) {
            return this.zzdeh.getVideoController().zzdl();
        }
        return null;
    }

    public final void recordImpression() {
        this.zzdeh.recordImpression();
    }

    public final void zzc(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) {
        this.zzdeh.trackViews((View) ObjectWrapper.a(iObjectWrapper), (HashMap) ObjectWrapper.a(iObjectWrapper2), (HashMap) ObjectWrapper.a(iObjectWrapper3));
    }

    public final zzaci zzrg() {
        NativeAd.Image icon = this.zzdeh.getIcon();
        if (icon != null) {
            return new zzabu(icon.getDrawable(), icon.getUri(), icon.getScale(), icon.getWidth(), icon.getHeight());
        }
        return null;
    }

    public final zzaca zzrh() {
        return null;
    }

    public final IObjectWrapper zzri() {
        Object zzjo = this.zzdeh.zzjo();
        if (zzjo == null) {
            return null;
        }
        return ObjectWrapper.a(zzjo);
    }

    public final IObjectWrapper zzsu() {
        View adChoicesContent = this.zzdeh.getAdChoicesContent();
        if (adChoicesContent == null) {
            return null;
        }
        return ObjectWrapper.a(adChoicesContent);
    }

    public final IObjectWrapper zzsv() {
        View zzabz = this.zzdeh.zzabz();
        if (zzabz == null) {
            return null;
        }
        return ObjectWrapper.a(zzabz);
    }

    public final void zzu(IObjectWrapper iObjectWrapper) {
        this.zzdeh.handleClick((View) ObjectWrapper.a(iObjectWrapper));
    }

    public final void zzw(IObjectWrapper iObjectWrapper) {
        this.zzdeh.untrackView((View) ObjectWrapper.a(iObjectWrapper));
    }
}
