package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdln extends zzdrt<zzdln, zza> implements zzdtg {
    private static volatile zzdtn<zzdln> zzdz;
    /* access modifiers changed from: private */
    public static final zzdln zzhat;
    private int zzhas;

    public static final class zza extends zzdrt.zzb<zzdln, zza> implements zzdtg {
        private zza() {
            super(zzdln.zzhat);
        }

        /* synthetic */ zza(zzdlm zzdlm) {
            this();
        }
    }

    static {
        zzdln zzdln = new zzdln();
        zzhat = zzdln;
        zzdrt.zza(zzdln.class, zzdln);
    }

    private zzdln() {
    }

    public static zzdln zzato() {
        return zzhat;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlm.zzdk[i - 1]) {
            case 1:
                return new zzdln();
            case 2:
                return new zza((zzdlm) null);
            case 3:
                return zzdrt.zza((zzdte) zzhat, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u000b", new Object[]{"zzhas"});
            case 4:
                return zzhat;
            case 5:
                zzdtn<zzdln> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdln.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhat);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final int zzatn() {
        return this.zzhas;
    }
}
