package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.Set;
import java.util.concurrent.Executor;

public final class zzbpg extends zzbrl<zzbph> {
    public zzbpg(Set<zzbsu<zzbph>> set) {
        super(set);
    }

    public final void zza(zzbtk zzbtk, Executor executor) {
        zza(zzbsu.zzb(new zzbpk(this, zzbtk), executor));
    }

    public final void zzbv(Context context) {
        zza(new zzbpj(context));
    }

    public final void zzbw(Context context) {
        zza(new zzbpi(context));
    }

    public final void zzbx(Context context) {
        zza(new zzbpl(context));
    }
}
