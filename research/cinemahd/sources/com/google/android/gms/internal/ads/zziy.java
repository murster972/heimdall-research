package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import com.google.android.gms.internal.ads.zziz;

@TargetApi(16)
public interface zziy<T extends zziz> {
    int getState();

    T zzgf();

    zzix zzgg();
}
