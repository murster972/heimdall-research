package com.google.android.gms.internal.ads;

public abstract class zzcfv implements zzdgf<zzaqk, zzczt> {
    /* access modifiers changed from: private */
    public final zzbqs zzfvd;

    public zzcfv(zzbqs zzbqs) {
        this.zzfvd = zzbqs;
    }

    /* access modifiers changed from: protected */
    public abstract zzdhe<zzczt> zze(zzaqk zzaqk) throws zzcfb;

    public final /* synthetic */ zzdhe zzf(Object obj) throws Exception {
        zzaqk zzaqk = (zzaqk) obj;
        this.zzfvd.zzb(zzaqk);
        zzdhe<zzczt> zze = zze(zzaqk);
        zzdgs.zza(zze, new zzcfy(this), zzazd.zzdwj);
        return zze;
    }
}
