package com.google.android.gms.internal.ads;

public interface zzaif extends zzajq {
    void destroy();

    boolean isDestroyed();

    void zza(zzaii zzaii);

    void zzcv(String str);

    void zzcw(String str);

    void zzcx(String str);

    zzajp zzrz();
}
