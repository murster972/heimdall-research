package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class zzdrg {
    private static volatile boolean zzhjg = false;
    private static boolean zzhjh = true;
    private static volatile zzdrg zzhji;
    private static volatile zzdrg zzhjj;
    private static final zzdrg zzhjk = new zzdrg(true);
    private final Map<zza, zzdrt.zzf<?, ?>> zzhjl;

    static final class zza {
        private final int number;
        private final Object object;

        zza(Object obj, int i) {
            this.object = obj;
            this.number = i;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.object == zza.object && this.number == zza.number) {
                return true;
            }
            return false;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.object) * 65535) + this.number;
        }
    }

    zzdrg() {
        this.zzhjl = new HashMap();
    }

    public static zzdrg zzazh() {
        zzdrg zzdrg = zzhji;
        if (zzdrg == null) {
            synchronized (zzdrg.class) {
                zzdrg = zzhji;
                if (zzdrg == null) {
                    zzdrg = zzhjk;
                    zzhji = zzdrg;
                }
            }
        }
        return zzdrg;
    }

    public static zzdrg zzazi() {
        Class<zzdrg> cls = zzdrg.class;
        zzdrg zzdrg = zzhjj;
        if (zzdrg == null) {
            synchronized (cls) {
                zzdrg = zzhjj;
                if (zzdrg == null) {
                    zzdrg = zzdrr.zzc(cls);
                    zzhjj = zzdrg;
                }
            }
        }
        return zzdrg;
    }

    public final <ContainingType extends zzdte> zzdrt.zzf<ContainingType, ?> zza(ContainingType containingtype, int i) {
        return this.zzhjl.get(new zza(containingtype, i));
    }

    private zzdrg(boolean z) {
        this.zzhjl = Collections.emptyMap();
    }
}
