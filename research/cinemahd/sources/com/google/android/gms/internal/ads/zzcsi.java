package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcsi implements Callable {
    private final zzcsj zzgfy;

    zzcsi(zzcsj zzcsj) {
        this.zzgfy = zzcsj;
    }

    public final Object call() {
        return this.zzgfy.zzani();
    }
}
