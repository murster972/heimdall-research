package com.google.android.gms.internal.ads;

public final class zzchy implements zzdxg<zzcht> {
    private final zzdxp<zzchr> zzepa;
    private final zzdxp<zzdhd> zzfxb;

    private zzchy(zzdxp<zzchr> zzdxp, zzdxp<zzdhd> zzdxp2) {
        this.zzepa = zzdxp;
        this.zzfxb = zzdxp2;
    }

    public static zzchy zzaj(zzdxp<zzchr> zzdxp, zzdxp<zzdhd> zzdxp2) {
        return new zzchy(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcht(this.zzepa.get(), this.zzfxb.get());
    }
}
