package com.google.android.gms.internal.ads;

import java.util.ArrayList;

final /* synthetic */ class zzcqq implements zzded {
    static final zzded zzdoq = new zzcqq();

    private zzcqq() {
    }

    public final Object apply(Object obj) {
        ArrayList arrayList = (ArrayList) obj;
        if (arrayList.isEmpty()) {
            return null;
        }
        return new zzcqt(arrayList);
    }
}
