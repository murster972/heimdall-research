package com.google.android.gms.internal.measurement;

import java.util.List;

public final class zzhv extends RuntimeException {
    private final List<String> zza = null;

    public zzhv(zzgn zzgn) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
