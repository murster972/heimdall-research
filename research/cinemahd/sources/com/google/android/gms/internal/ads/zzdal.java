package com.google.android.gms.internal.ads;

public final class zzdal implements zzdxg<zzbsu<zzbow>> {
    private final zzdxp<zzdak> zzfgt;
    private final zzdai zzgnc;

    private zzdal(zzdai zzdai, zzdxp<zzdak> zzdxp) {
        this.zzgnc = zzdai;
        this.zzfgt = zzdxp;
    }

    public static zzdal zza(zzdai zzdai, zzdxp<zzdak> zzdxp) {
        return new zzdal(zzdai, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfgt.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
