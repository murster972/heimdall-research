package com.google.android.gms.internal.measurement;

final class zzdk extends zzdf {
    zzdk() {
    }

    public final void zza(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }
}
