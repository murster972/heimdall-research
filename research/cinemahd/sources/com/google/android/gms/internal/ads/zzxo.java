package com.google.android.gms.internal.ads;

final class zzxo extends zzvd {
    private final /* synthetic */ zzxl zzcew;

    zzxo(zzxl zzxl) {
        this.zzcew = zzxl;
    }

    public final void onAdFailedToLoad(int i) {
        this.zzcew.zzcel.zza(this.zzcew.zzdl());
        super.onAdFailedToLoad(i);
    }

    public final void onAdLoaded() {
        this.zzcew.zzcel.zza(this.zzcew.zzdl());
        super.onAdLoaded();
    }
}
