package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface zzaqe extends IInterface {
    void zza(zzaxc zzaxc) throws RemoteException;

    void zzb(ParcelFileDescriptor parcelFileDescriptor) throws RemoteException;
}
