package com.google.android.gms.internal.ads;

import android.view.View;

public final class zzbjv implements zzdxg<View> {
    private final zzbjw zzfdn;

    public zzbjv(zzbjw zzbjw) {
        this.zzfdn = zzbjw;
    }

    public static View zza(zzbjw zzbjw) {
        return (View) zzdxm.zza(zzbjw.zzafu(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfdn);
    }
}
