package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzceb implements zzdxg<zzcec> {
    private final zzdxp<Clock> zzfcz;

    public zzceb(zzdxp<Clock> zzdxp) {
        this.zzfcz = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcec(this.zzfcz.get());
    }
}
