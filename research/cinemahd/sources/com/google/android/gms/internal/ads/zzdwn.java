package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;

public abstract class zzdwn extends zzdwl implements zzbf {
    private int flags;
    private int version;

    protected zzdwn(String str) {
        super(str);
    }

    public final int getVersion() {
        if (!this.zzhyq) {
            zzbdj();
        }
        return this.version;
    }

    /* access modifiers changed from: protected */
    public final long zzo(ByteBuffer byteBuffer) {
        this.version = zzbg.zza(byteBuffer.get());
        this.flags = (zzbg.zzb(byteBuffer) << 8) + 0 + zzbg.zza(byteBuffer.get());
        return 4;
    }
}
