package com.google.android.gms.internal.ads;

public final class zzdbt implements zzdxg<zzdhd> {
    private static final zzdbt zzgpr = new zzdbt();

    public static zzdbt zzapx() {
        return zzgpr;
    }

    public final /* synthetic */ Object get() {
        return (zzdhd) zzdxm.zza(zzdhg.zza(zzddq.zzaqs().zzdu(zzddv.zzgtv)), "Cannot return null from a non-@Nullable @Provides method");
    }
}
