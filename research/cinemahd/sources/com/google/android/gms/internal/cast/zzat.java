package com.google.android.gms.internal.cast;

import android.content.Context;
import android.view.View;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import java.util.Iterator;
import java.util.List;

public final class zzat extends UIController {
    private final View view;
    private final String zznp;
    private final String zzsh;

    public zzat(View view2, Context context) {
        this.view = view2;
        this.zznp = context.getString(R$string.cast_closed_captions);
        this.zzsh = context.getString(R$string.cast_closed_captions_unavailable);
        this.view.setEnabled(false);
    }

    private final void zzdk() {
        boolean z;
        List<MediaTrack> x;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && remoteMediaClient.k()) {
            MediaInfo e = remoteMediaClient.e();
            if (e != null && (x = e.x()) != null && !x.isEmpty()) {
                Iterator<MediaTrack> it2 = x.iterator();
                int i = 0;
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    MediaTrack next = it2.next();
                    if (next.y() == 2) {
                        i++;
                        if (i > 1) {
                            break;
                        }
                    } else if (next.y() == 1) {
                        break;
                    }
                }
                z = true;
                if (z && !remoteMediaClient.q()) {
                    this.view.setEnabled(true);
                    this.view.setContentDescription(this.zznp);
                    return;
                }
            }
            z = false;
            this.view.setEnabled(true);
            this.view.setContentDescription(this.zznp);
            return;
        }
        this.view.setEnabled(false);
        this.view.setContentDescription(this.zzsh);
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onSendingRemoteMediaRequest() {
        this.view.setEnabled(false);
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        this.view.setEnabled(true);
        zzdk();
    }

    public final void onSessionEnded() {
        this.view.setEnabled(false);
        super.onSessionEnded();
    }
}
