package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdfz;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

final class zzdgg<V> extends zzdfz<Object, V> {
    /* access modifiers changed from: private */
    public zzdgi<?> zzgwm;

    zzdgg(zzdet<? extends zzdhe<?>> zzdet, boolean z, Executor executor, Callable<V> callable) {
        super(zzdet, z, false);
        this.zzgwm = new zzdgj(this, callable, executor);
        zzarn();
    }

    /* access modifiers changed from: protected */
    public final void interruptTask() {
        zzdgi<?> zzdgi = this.zzgwm;
        if (zzdgi != null) {
            zzdgi.interruptTask();
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdfz.zza zza) {
        super.zza(zza);
        if (zza == zzdfz.zza.OUTPUT_FUTURE_DONE) {
            this.zzgwm = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzaro() {
        zzdgi<?> zzdgi = this.zzgwm;
        if (zzdgi != null) {
            zzdgi.execute();
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(int i, Object obj) {
    }
}
