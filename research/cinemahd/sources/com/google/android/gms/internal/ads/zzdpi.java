package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public interface zzdpi {
    byte[] zzp(byte[] bArr) throws GeneralSecurityException;
}
