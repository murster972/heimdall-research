package com.google.android.gms.internal.measurement;

final class zzge<K, V> {
    public final zzil zza;
    public final K zzb;
    public final zzil zzc;
    public final V zzd;
}
