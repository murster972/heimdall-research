package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public abstract class zzcut {
    public abstract zzcua<JSONObject> zzadt();

    public abstract zzcua<JSONObject> zzadu();

    public abstract zzdcr zzadv();
}
