package com.google.android.gms.internal.ads;

import android.os.Bundle;
import okhttp3.internal.cache.DiskLruCache;

public final class zzcry implements zzcty<Bundle> {
    private final String zzdjo;
    private final boolean zzdjq;

    public zzcry(String str, boolean z) {
        this.zzdjo = str;
        this.zzdjq = z;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        bundle.putString("gct", this.zzdjo);
        if (this.zzdjq) {
            bundle.putString("de", DiskLruCache.VERSION_1);
        }
    }
}
