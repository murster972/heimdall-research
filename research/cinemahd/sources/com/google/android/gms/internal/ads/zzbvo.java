package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

public final class zzbvo implements zzdxg<Set<String>> {
    private final zzdxp<zzbwz> zzfeh;

    public zzbvo(zzdxp<zzbwz> zzdxp) {
        this.zzfeh = zzdxp;
    }

    public final /* synthetic */ Object get() {
        Set set;
        if (this.zzfeh.get().zzajs() != null) {
            set = Collections.singleton("banner");
        } else {
            set = Collections.emptySet();
        }
        return (Set) zzdxm.zza(set, "Cannot return null from a non-@Nullable @Provides method");
    }
}
