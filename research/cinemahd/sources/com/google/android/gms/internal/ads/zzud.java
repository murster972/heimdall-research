package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.AdMetadataListener;

public final class zzud extends zzwa {
    private final AdMetadataListener zzcbx;

    public zzud(AdMetadataListener adMetadataListener) {
        this.zzcbx = adMetadataListener;
    }

    public final void onAdMetadataChanged() {
        AdMetadataListener adMetadataListener = this.zzcbx;
        if (adMetadataListener != null) {
            adMetadataListener.onAdMetadataChanged();
        }
    }
}
