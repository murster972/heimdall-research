package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzcmr implements zzbuv {
    private final zzcip zzfyq;

    zzcmr(zzcip zzcip) {
        this.zzfyq = zzcip;
    }

    public final void zza(boolean z, Context context) {
        zzcip zzcip = this.zzfyq;
        try {
            ((zzdac) zzcip.zzddn).setImmersiveMode(z);
            ((zzdac) zzcip.zzddn).showVideo();
        } catch (zzdab e) {
            zzayu.zzd("Cannot show rewarded video.", e);
        }
    }
}
