package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcxj implements zzdgf {
    private final zzboe zzgkb;

    zzcxj(zzboe zzboe) {
        this.zzgkb = zzboe;
    }

    public final zzdhe zzf(Object obj) {
        zzbmd zzbmd = (zzbmd) obj;
        zzbmd.zzagt().zzb(((zzbob) this.zzgkb.zzadg()).zzadz());
        return zzdgs.zzaj(zzbmd);
    }
}
