package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbvc implements zzbsn {
    private final zzbpg zzfjo;

    public zzbvc(zzbpg zzbpg) {
        this.zzfjo = zzbpg;
    }

    public final void onHide() {
        this.zzfjo.zzbx((Context) null);
    }

    public final void zzahy() {
    }
}
