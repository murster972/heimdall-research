package com.google.android.gms.internal.ads;

import android.content.DialogInterface;

final /* synthetic */ class zzawv implements DialogInterface.OnClickListener {
    private final zzawt zzdta;
    private final int zzdtf;
    private final int zzdtg;
    private final int zzdth;

    zzawv(zzawt zzawt, int i, int i2, int i3) {
        this.zzdta = zzawt;
        this.zzdtf = i;
        this.zzdtg = i2;
        this.zzdth = i3;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.zzdta.zza(this.zzdtf, this.zzdtg, this.zzdth, dialogInterface, i);
    }
}
