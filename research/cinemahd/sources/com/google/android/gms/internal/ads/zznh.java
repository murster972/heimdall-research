package com.google.android.gms.internal.ads;

public final class zznh {
    public final zzmr zzbef;
    public final zzng zzbeg;
    public final Object zzbeh;
    public final zzhh[] zzbei;

    public zznh(zzmr zzmr, zzng zzng, Object obj, zzhh[] zzhhArr) {
        this.zzbef = zzmr;
        this.zzbeg = zzng;
        this.zzbeh = obj;
        this.zzbei = zzhhArr;
    }

    public final boolean zza(zznh zznh, int i) {
        if (zznh != null && zzoq.zza(this.zzbeg.zzay(i), zznh.zzbeg.zzay(i)) && zzoq.zza(this.zzbei[i], zznh.zzbei[i])) {
            return true;
        }
        return false;
    }
}
