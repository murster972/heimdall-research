package com.google.android.gms.internal.ads;

public final class zzbgi implements zzdxg<String> {
    private final zzbga zzejr;

    public zzbgi(zzbga zzbga) {
        this.zzejr = zzbga;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza(this.zzejr.zzacs(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
