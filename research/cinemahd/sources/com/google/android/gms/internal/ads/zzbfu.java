package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzbfu extends zzgc implements zzbfs {
    zzbfu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.measurement.IMeasurementManager");
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzbfq zzbfq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, (IInterface) iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, (IInterface) zzbfq);
        zza(2, obtainAndWriteInterfaceToken);
    }
}
