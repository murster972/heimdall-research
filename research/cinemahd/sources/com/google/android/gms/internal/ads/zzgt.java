package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import java.io.IOException;

final class zzgt implements Handler.Callback, zzmc, zzme, zzni {
    private int repeatMode = 0;
    private int state;
    private final zzhf[] zzacq;
    private final zznf zzacr;
    private final zzddu zzact;
    private final zzhl zzacw;
    private final zzhi zzacx;
    private boolean zzacz;
    private boolean zzadd;
    private zzhg zzade;
    private zzhc zzadi;
    private zzgv zzadj;
    private final zzhe[] zzadz;
    private final zzha zzaea;
    private zzmb zzaeb;
    private final zzoo zzaed;
    private final zzddu zzaee;
    private final HandlerThread zzaef;
    private final zzgk zzaeg;
    private zzhf zzaeh;
    private zzog zzaei;
    private zzhf[] zzaej;
    private boolean zzaek;
    private boolean zzael;
    private int zzaem;
    private int zzaen;
    private long zzaeo;
    private int zzaep;
    private zzgu zzaeq;
    private long zzaer;
    private zzgs zzaes;
    private zzgs zzaet;
    private zzgs zzaeu;

    public zzgt(zzhf[] zzhfArr, zznf zznf, zzha zzha, boolean z, int i, zzddu zzddu, zzgv zzgv, zzgk zzgk) {
        this.zzacq = zzhfArr;
        this.zzacr = zznf;
        this.zzaea = zzha;
        this.zzacz = z;
        this.zzact = zzddu;
        this.state = 1;
        this.zzadj = zzgv;
        this.zzaeg = zzgk;
        this.zzadz = new zzhe[zzhfArr.length];
        for (int i2 = 0; i2 < zzhfArr.length; i2++) {
            zzhfArr[i2].setIndex(i2);
            this.zzadz[i2] = zzhfArr[i2].zzdp();
        }
        this.zzaed = new zzoo();
        this.zzaej = new zzhf[0];
        this.zzacw = new zzhl();
        this.zzacx = new zzhi();
        zznf.zza(this);
        this.zzadi = zzhc.zzagb;
        this.zzaef = new HandlerThread("ExoPlayerImplInternal:Handler", -16);
        this.zzaef.start();
        this.zzaee = new zzddu(this.zzaef.getLooper(), this);
    }

    private final void setState(int i) {
        if (this.state != i) {
            this.state = i;
            this.zzact.obtainMessage(1, i, 0).sendToTarget();
        }
    }

    private final void zzdq(long j) throws zzgl {
        long j2;
        zzgs zzgs = this.zzaeu;
        if (zzgs == null) {
            j2 = 60000000;
        } else {
            j2 = zzgs.zzef();
        }
        this.zzaer = j + j2;
        this.zzaed.zzel(this.zzaer);
        for (zzhf zzdo : this.zzaej) {
            zzdo.zzdo(this.zzaer);
        }
    }

    private final boolean zzdr(long j) {
        if (j == -9223372036854775807L || this.zzadj.zzaex < j) {
            return true;
        }
        zzgs zzgs = this.zzaeu.zzadx;
        return zzgs != null && zzgs.zzadv;
    }

    private final void zzej() throws zzgl {
        this.zzael = false;
        this.zzaed.start();
        for (zzhf start : this.zzaej) {
            start.start();
        }
    }

    private final void zzek() throws zzgl {
        this.zzaed.stop();
        for (zzhf zza : this.zzaej) {
            zza(zza);
        }
    }

    private final void zzel() throws zzgl {
        long j;
        zzgs zzgs = this.zzaeu;
        if (zzgs != null) {
            long zzhi = zzgs.zzadn.zzhi();
            if (zzhi != -9223372036854775807L) {
                zzdq(zzhi);
            } else {
                zzhf zzhf = this.zzaeh;
                if (zzhf == null || zzhf.zzeu()) {
                    this.zzaer = this.zzaed.zzfp();
                } else {
                    this.zzaer = this.zzaei.zzfp();
                    this.zzaed.zzel(this.zzaer);
                }
                zzhi = this.zzaer - this.zzaeu.zzef();
            }
            this.zzadj.zzaex = zzhi;
            this.zzaeo = SystemClock.elapsedRealtime() * 1000;
            if (this.zzaej.length == 0) {
                j = Long.MIN_VALUE;
            } else {
                j = this.zzaeu.zzadn.zzhj();
            }
            zzgv zzgv = this.zzadj;
            if (j == Long.MIN_VALUE) {
                j = this.zzade.zza(this.zzaeu.zzads, this.zzacx, false).zzagj;
            }
            zzgv.zzaey = j;
        }
    }

    private final void zzem() {
        zzi(true);
        this.zzaea.onStopped();
        setState(1);
    }

    private final void zzen() throws IOException {
        zzgs zzgs = this.zzaes;
        if (zzgs != null && !zzgs.zzadv) {
            zzgs zzgs2 = this.zzaet;
            if (zzgs2 == null || zzgs2.zzadx == zzgs) {
                zzhf[] zzhfArr = this.zzaej;
                int length = zzhfArr.length;
                int i = 0;
                while (i < length) {
                    if (zzhfArr[i].zzds()) {
                        i++;
                    } else {
                        return;
                    }
                }
                this.zzaes.zzadn.zzhf();
            }
        }
    }

    private final void zzeo() {
        long j;
        zzgs zzgs = this.zzaes;
        if (!zzgs.zzadv) {
            j = 0;
        } else {
            j = zzgs.zzadn.zzhh();
        }
        if (j == Long.MIN_VALUE) {
            zzh(false);
            return;
        }
        long zzef = this.zzaer - this.zzaes.zzef();
        boolean zzdt = this.zzaea.zzdt(j - zzef);
        zzh(zzdt);
        if (zzdt) {
            this.zzaes.zzadn.zzef(zzef);
        }
    }

    private final void zzh(boolean z) {
        if (this.zzadd != z) {
            this.zzadd = z;
            this.zzact.obtainMessage(2, z ? 1 : 0, 0).sendToTarget();
        }
    }

    private final void zzi(boolean z) {
        this.zzaee.removeMessages(2);
        this.zzael = false;
        this.zzaed.stop();
        this.zzaei = null;
        this.zzaeh = null;
        this.zzaer = 60000000;
        for (zzhf zzhf : this.zzaej) {
            try {
                zza(zzhf);
                zzhf.disable();
            } catch (zzgl | RuntimeException e) {
                Log.e("ExoPlayerImplInternal", "Stop failed.", e);
            }
        }
        this.zzaej = new zzhf[0];
        zzgs zzgs = this.zzaeu;
        if (zzgs == null) {
            zzgs = this.zzaes;
        }
        zza(zzgs);
        this.zzaes = null;
        this.zzaet = null;
        this.zzaeu = null;
        zzh(false);
        if (z) {
            zzmb zzmb = this.zzaeb;
            if (zzmb != null) {
                zzmb.zzhs();
                this.zzaeb = null;
            }
            this.zzade = null;
        }
    }

    private final boolean zzo(int i) {
        this.zzade.zza(i, this.zzacx, false);
        if (this.zzade.zza(0, this.zzacw, false).zzagv || this.zzade.zza(i, this.zzacx, this.zzacw, this.repeatMode) != -1) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:504:0x08b9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:505:0x08ba, code lost:
        r1 = r0;
        android.util.Log.e("ExoPlayerImplInternal", "Internal runtime error.", r1);
        r8.zzact.obtainMessage(8, com.google.android.gms.internal.ads.zzgl.zza(r1)).sendToTarget();
        zzem();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:506:0x08d4, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:507:0x08d5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:508:0x08d6, code lost:
        r3 = 8;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:511:0x08f1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:512:0x08f2, code lost:
        r3 = 8;
        r1 = r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x028f A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0292 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0296 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x0358 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x036c A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:307:0x0538 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x053f A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x0559 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:313:0x055c A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x0597 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x05ab A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x05c7 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }, LOOP:8: B:331:0x05c7->B:335:0x05d9, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:405:0x0729 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:444:0x07e2 A[Catch:{ all -> 0x0454, all -> 0x038c, all -> 0x00cc, all -> 0x00d9, all -> 0x00c8, zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:504:0x08b9 A[ExcHandler: RuntimeException (r0v2 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean handleMessage(android.os.Message r35) {
        /*
            r34 = this;
            r8 = r34
            r1 = r35
            r10 = 1
            int r2 = r1.what     // Catch:{ zzgl -> 0x08f1, IOException -> 0x08d5, RuntimeException -> 0x08b9 }
            r11 = 7
            r3 = 0
            r14 = 3
            r5 = -1
            r6 = 0
            r15 = 4
            r12 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r7 = 2
            r9 = 0
            switch(r2) {
                case 0: goto L_0x0878;
                case 1: goto L_0x084b;
                case 2: goto L_0x046e;
                case 3: goto L_0x03b5;
                case 4: goto L_0x0394;
                case 5: goto L_0x0390;
                case 6: goto L_0x0379;
                case 7: goto L_0x021b;
                case 8: goto L_0x01e5;
                case 9: goto L_0x01d2;
                case 10: goto L_0x00dd;
                case 11: goto L_0x009f;
                case 12: goto L_0x0019;
                default: goto L_0x0018;
            }
        L_0x0018:
            return r9
        L_0x0019:
            int r1 = r1.arg1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.repeatMode = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0024
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0026
        L_0x0024:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0026:
            if (r2 == 0) goto L_0x009e
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r3) goto L_0x002e
            r3 = 1
            goto L_0x002f
        L_0x002e:
            r3 = 0
        L_0x002f:
            com.google.android.gms.internal.ads.zzgs r4 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r4) goto L_0x0037
            r4 = r3
            r3 = r2
            r2 = 1
            goto L_0x003a
        L_0x0037:
            r4 = r3
            r3 = r2
            r2 = 0
        L_0x003a:
            com.google.android.gms.internal.ads.zzhg r11 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r12 = r3.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r13 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhl r14 = r8.zzacw     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.zza((int) r12, (com.google.android.gms.internal.ads.zzhi) r13, (com.google.android.gms.internal.ads.zzhl) r14, (int) r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r12 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x0065
            if (r11 == r5) goto L_0x0065
            com.google.android.gms.internal.ads.zzgs r12 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r12 = r12.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 != r11) goto L_0x0065
            com.google.android.gms.internal.ads.zzgs r3 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r11 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != r11) goto L_0x005a
            r11 = 1
            goto L_0x005b
        L_0x005a:
            r11 = 0
        L_0x005b:
            r4 = r4 | r11
            com.google.android.gms.internal.ads.zzgs r11 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != r11) goto L_0x0062
            r11 = 1
            goto L_0x0063
        L_0x0062:
            r11 = 0
        L_0x0063:
            r2 = r2 | r11
            goto L_0x003a
        L_0x0065:
            com.google.android.gms.internal.ads.zzgs r5 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r5 == 0) goto L_0x0070
            com.google.android.gms.internal.ads.zzgs r5 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            zza((com.google.android.gms.internal.ads.zzgs) r5)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzadx = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0070:
            int r5 = r3.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r5 = r8.zzo(r5)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzadu = r5     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x007c
            r8.zzaes = r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x007c:
            if (r4 != 0) goto L_0x0095
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0095
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r3 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r8.zza((int) r2, (long) r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r5 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5.<init>(r2, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r5     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0095:
            int r2 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r15) goto L_0x009e
            if (r1 == 0) goto L_0x009e
            r8.setState(r7)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x009e:
            return r10
        L_0x009f:
            java.lang.Object r1 = r1.obj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgp[] r1 = (com.google.android.gms.internal.ads.zzgp[]) r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r1.length     // Catch:{ all -> 0x00cc }
        L_0x00a4:
            if (r9 >= r2) goto L_0x00b4
            r3 = r1[r9]     // Catch:{ all -> 0x00cc }
            com.google.android.gms.internal.ads.zzgm r4 = r3.zzacm     // Catch:{ all -> 0x00cc }
            int r5 = r3.zzacn     // Catch:{ all -> 0x00cc }
            java.lang.Object r3 = r3.zzaco     // Catch:{ all -> 0x00cc }
            r4.zza(r5, r3)     // Catch:{ all -> 0x00cc }
            int r9 = r9 + 1
            goto L_0x00a4
        L_0x00b4:
            com.google.android.gms.internal.ads.zzmb r1 = r8.zzaeb     // Catch:{ all -> 0x00cc }
            if (r1 == 0) goto L_0x00bd
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzaee     // Catch:{ all -> 0x00cc }
            r1.sendEmptyMessage(r7)     // Catch:{ all -> 0x00cc }
        L_0x00bd:
            monitor-enter(r34)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r8.zzaen     // Catch:{ all -> 0x00c8 }
            int r1 = r1 + r10
            r8.zzaen = r1     // Catch:{ all -> 0x00c8 }
            r34.notifyAll()     // Catch:{ all -> 0x00c8 }
            monitor-exit(r34)     // Catch:{ all -> 0x00c8 }
            return r10
        L_0x00c8:
            r0 = move-exception
            r1 = r0
            monitor-exit(r34)     // Catch:{ all -> 0x00c8 }
            throw r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x00cc:
            r0 = move-exception
            r1 = r0
            monitor-enter(r34)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r8.zzaen     // Catch:{ all -> 0x00d9 }
            int r2 = r2 + r10
            r8.zzaen = r2     // Catch:{ all -> 0x00d9 }
            r34.notifyAll()     // Catch:{ all -> 0x00d9 }
            monitor-exit(r34)     // Catch:{ all -> 0x00d9 }
            throw r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x00d9:
            r0 = move-exception
            r1 = r0
            monitor-exit(r34)     // Catch:{ all -> 0x00d9 }
            throw r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x00dd:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x01d1
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 1
        L_0x00e4:
            if (r1 == 0) goto L_0x01d1
            boolean r3 = r1.zzadv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != 0) goto L_0x00ec
            goto L_0x01d1
        L_0x00ec:
            boolean r3 = r1.zzeh()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != 0) goto L_0x00fa
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r3) goto L_0x00f7
            r2 = 0
        L_0x00f7:
            com.google.android.gms.internal.ads.zzgs r1 = r1.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x00e4
        L_0x00fa:
            if (r2 == 0) goto L_0x0198
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == r3) goto L_0x0104
            r2 = 1
            goto L_0x0105
        L_0x0104:
            r2 = 0
        L_0x0105:
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            zza((com.google.android.gms.internal.ads.zzgs) r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzadx = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaes = r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaet = r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhf[] r3 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean[] r3 = new boolean[r3]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r4 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r5 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r5.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r4.zza(r11, r2, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r2 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r2.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
            if (r2 == 0) goto L_0x0136
            com.google.android.gms.internal.ads.zzgv r2 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zzaex = r4     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzdq(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0136:
            com.google.android.gms.internal.ads.zzhf[] r2 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean[] r2 = new boolean[r2]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4 = 0
            r5 = 0
        L_0x013d:
            com.google.android.gms.internal.ads.zzhf[] r11 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 >= r11) goto L_0x0189
            com.google.android.gms.internal.ads.zzhf[] r11 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11 = r11[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r12 = r11.getState()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x014e
            r12 = 1
            goto L_0x014f
        L_0x014e:
            r12 = 0
        L_0x014f:
            r2[r4] = r12     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r12 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzmo[] r12 = r12.zzadp     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12 = r12[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x015b
            int r5 = r5 + 1
        L_0x015b:
            boolean r13 = r2[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r13 == 0) goto L_0x0186
            com.google.android.gms.internal.ads.zzmo r13 = r11.zzdr()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == r13) goto L_0x017d
            com.google.android.gms.internal.ads.zzhf r13 = r8.zzaeh     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 != r13) goto L_0x0176
            if (r12 != 0) goto L_0x0172
            com.google.android.gms.internal.ads.zzoo r12 = r8.zzaed     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzog r13 = r8.zzaei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.zza(r13)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0172:
            r8.zzaei = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaeh = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0176:
            zza((com.google.android.gms.internal.ads.zzhf) r11)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.disable()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0186
        L_0x017d:
            boolean r12 = r3[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x0186
            long r12 = r8.zzaer     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.zzdo(r12)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0186:
            int r4 = r4 + 1
            goto L_0x013d
        L_0x0189:
            com.google.android.gms.internal.ads.zzddu r3 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zznh r1 = r1.zzady     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r3.obtainMessage(r14, r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zza((boolean[]) r2, (int) r5)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x01c6
        L_0x0198:
            r8.zzaes = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r1.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x019e:
            if (r1 == 0) goto L_0x01a6
            r1.release()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r1.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x019e
        L_0x01a6:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzadx = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzadv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x01c6
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzadt     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r8.zzaer     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r3.zzef()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r4 - r11
            long r1 = java.lang.Math.max(r1, r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzb(r1, r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x01c6:
            r34.zzeo()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzel()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzaee     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r7)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x01d1:
            return r10
        L_0x01d2:
            java.lang.Object r1 = r1.obj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r1 = (com.google.android.gms.internal.ads.zzlz) r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x01e4
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r2 = r2.zzadn     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == r1) goto L_0x01e1
            goto L_0x01e4
        L_0x01e1:
            r34.zzeo()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x01e4:
            return r10
        L_0x01e5:
            java.lang.Object r1 = r1.obj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r1 = (com.google.android.gms.internal.ads.zzlz) r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x021a
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r2 = r2.zzadn     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == r1) goto L_0x01f4
            goto L_0x021a
        L_0x01f4:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzadv = r10     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzeh()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r1.zzadt     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r1.zzb(r2, r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzadt = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x0217
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaet = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzadt     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzdq(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzb((com.google.android.gms.internal.ads.zzgs) r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0217:
            r34.zzeo()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x021a:
            return r10
        L_0x021b:
            java.lang.Object r1 = r1.obj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.util.Pair r1 = (android.util.Pair) r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r3 = r1.first     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r3 = (com.google.android.gms.internal.ads.zzhg) r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzade = r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r1 = r1.second     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x028a
            int r3 = r8.zzaep     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 <= 0) goto L_0x025a
            com.google.android.gms.internal.ads.zzgu r3 = r8.zzaeq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.util.Pair r3 = r8.zza((com.google.android.gms.internal.ads.zzgu) r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r4 = r8.zzaep     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaep = r9     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaeq = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != 0) goto L_0x0242
            r8.zza((java.lang.Object) r1, (int) r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0378
        L_0x0242:
            com.google.android.gms.internal.ads.zzgv r7 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r11 = r3.first     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r11 = (java.lang.Integer) r11     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.intValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r3 = r3.second     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r3.longValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r7.<init>(r11, r14)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r7     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x028b
        L_0x025a:
            com.google.android.gms.internal.ads.zzgv r3 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzadt     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r7 != 0) goto L_0x028a
            com.google.android.gms.internal.ads.zzhg r3 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r3 = r3.isEmpty()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x026f
            r8.zza((java.lang.Object) r1, (int) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0378
        L_0x026f:
            android.util.Pair r3 = r8.zzb((int) r9, (long) r12)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r4 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r7 = r3.first     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r7 = (java.lang.Integer) r7     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.intValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r3 = r3.second     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r3.longValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4.<init>(r7, r14)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r4     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x028a:
            r4 = 0
        L_0x028b:
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x0292
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0294
        L_0x0292:
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0294:
            if (r3 == 0) goto L_0x0375
            com.google.android.gms.internal.ads.zzhg r7 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r11 = r3.zzado     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.zzc(r11)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 != r5) goto L_0x02f8
            int r6 = r3.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r7 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r8.zza((int) r6, (com.google.android.gms.internal.ads.zzhg) r2, (com.google.android.gms.internal.ads.zzhg) r7)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != r5) goto L_0x02af
            r8.zza((java.lang.Object) r1, (int) r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0378
        L_0x02af:
            com.google.android.gms.internal.ads.zzhg r6 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r7 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r6.zza((int) r2, (com.google.android.gms.internal.ads.zzhi) r7, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.util.Pair r2 = r8.zzb((int) r9, (long) r12)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r6 = r2.first     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r6 = r6.intValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r2 = r2.second     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r2.longValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r7 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza((int) r6, (com.google.android.gms.internal.ads.zzhi) r7, (boolean) r10)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r2 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r2 = r2.zzado     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzads = r5     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x02d7:
            com.google.android.gms.internal.ads.zzgs r7 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x02eb
            com.google.android.gms.internal.ads.zzgs r3 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r7 = r3.zzado     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r7 = r7.equals(r2)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x02e7
            r7 = r6
            goto L_0x02e8
        L_0x02e7:
            r7 = -1
        L_0x02e8:
            r3.zzads = r7     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x02d7
        L_0x02eb:
            long r2 = r8.zza((int) r6, (long) r11)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r5 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5.<init>(r6, r2)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r5     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0375
        L_0x02f8:
            boolean r2 = r8.zzo(r7)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzc(r7, r2)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 != r2) goto L_0x0305
            r2 = 1
            goto L_0x0306
        L_0x0305:
            r2 = 0
        L_0x0306:
            com.google.android.gms.internal.ads.zzgv r11 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == r11) goto L_0x031f
            com.google.android.gms.internal.ads.zzgv r11 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r12 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r13 = r11.zzadt     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.<init>(r7, r13)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r13 = r11.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.zzaex = r13     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r13 = r11.zzaey     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r12.zzaey = r13     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r12     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x031f:
            com.google.android.gms.internal.ads.zzgs r11 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 == 0) goto L_0x0375
            com.google.android.gms.internal.ads.zzgs r11 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r12 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r13 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhl r14 = r8.zzacw     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r15 = r8.repeatMode     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r12.zza((int) r7, (com.google.android.gms.internal.ads.zzhi) r13, (com.google.android.gms.internal.ads.zzhl) r14, (int) r15)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == r5) goto L_0x0356
            java.lang.Object r12 = r11.zzado     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r13 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r14 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r13 = r13.zza((int) r7, (com.google.android.gms.internal.ads.zzhi) r14, (boolean) r10)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r13 = r13.zzado     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r12 = r12.equals(r13)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r12 == 0) goto L_0x0356
            boolean r3 = r8.zzo(r7)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.zzc(r7, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 != r3) goto L_0x0352
            r3 = 1
            goto L_0x0353
        L_0x0352:
            r3 = 0
        L_0x0353:
            r2 = r2 | r3
            r3 = r11
            goto L_0x031f
        L_0x0356:
            if (r2 != 0) goto L_0x036c
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r3 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r3.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r8.zza((int) r2, (long) r5)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r3 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.<init>(r2, r5)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0375
        L_0x036c:
            r8.zzaes = r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zzadx = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            zza((com.google.android.gms.internal.ads.zzgs) r11)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0375:
            r8.zzb((java.lang.Object) r1, (int) r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0378:
            return r10
        L_0x0379:
            r8.zzi(r10)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzha r1 = r8.zzaea     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzes()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r10)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            monitor-enter(r34)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaek = r10     // Catch:{ all -> 0x038c }
            r34.notifyAll()     // Catch:{ all -> 0x038c }
            monitor-exit(r34)     // Catch:{ all -> 0x038c }
            return r10
        L_0x038c:
            r0 = move-exception
            r1 = r0
            monitor-exit(r34)     // Catch:{ all -> 0x038c }
            throw r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0390:
            r34.zzem()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            return r10
        L_0x0394:
            java.lang.Object r1 = r1.obj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhc r1 = (com.google.android.gms.internal.ads.zzhc) r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzog r2 = r8.zzaei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x03a3
            com.google.android.gms.internal.ads.zzog r2 = r8.zzaei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhc r1 = r2.zzb(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x03a9
        L_0x03a3:
            com.google.android.gms.internal.ads.zzoo r2 = r8.zzaed     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhc r1 = r2.zzb(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x03a9:
            r8.zzadi = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r2 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r2.obtainMessage(r11, r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            return r10
        L_0x03b5:
            java.lang.Object r1 = r1.obj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgu r1 = (com.google.android.gms.internal.ads.zzgu) r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x03c6
            int r2 = r8.zzaep     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2 + r10
            r8.zzaep = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaeq = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0453
        L_0x03c6:
            android.util.Pair r2 = r8.zza((com.google.android.gms.internal.ads.zzgu) r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x03ec
            com.google.android.gms.internal.ads.zzgv r1 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.<init>(r9, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r2 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r1.obtainMessage(r15, r10, r9, r2)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r1 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.<init>(r9, r12)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r15)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzi(r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0453
        L_0x03ec:
            long r3 = r1.zzaew     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r1 != 0) goto L_0x03f4
            r1 = 1
            goto L_0x03f5
        L_0x03f4:
            r1 = 0
        L_0x03f5:
            java.lang.Object r3 = r2.first     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.intValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r2 = r2.second     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r2.longValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r2 = r8.zzadj     // Catch:{ all -> 0x0454 }
            int r2 = r2.zzads     // Catch:{ all -> 0x0454 }
            if (r3 != r2) goto L_0x0430
            r6 = 1000(0x3e8, double:4.94E-321)
            long r11 = r4 / r6
            com.google.android.gms.internal.ads.zzgv r2 = r8.zzadj     // Catch:{ all -> 0x0454 }
            long r13 = r2.zzaex     // Catch:{ all -> 0x0454 }
            long r13 = r13 / r6
            int r2 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r2 != 0) goto L_0x0430
            com.google.android.gms.internal.ads.zzgv r2 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.<init>(r3, r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r2 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0425
            r1 = 1
            goto L_0x0426
        L_0x0425:
            r1 = 0
        L_0x0426:
            com.google.android.gms.internal.ads.zzgv r3 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r2.obtainMessage(r15, r1, r9, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0453
        L_0x0430:
            long r6 = r8.zza((int) r3, (long) r4)     // Catch:{ all -> 0x0454 }
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x043a
            r2 = 1
            goto L_0x043b
        L_0x043a:
            r2 = 0
        L_0x043b:
            r1 = r1 | r2
            com.google.android.gms.internal.ads.zzgv r2 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.<init>(r3, r6)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r2 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0449
            r1 = 1
            goto L_0x044a
        L_0x0449:
            r1 = 0
        L_0x044a:
            com.google.android.gms.internal.ads.zzgv r3 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r2.obtainMessage(r15, r1, r9, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0453:
            return r10
        L_0x0454:
            r0 = move-exception
            r2 = r0
            com.google.android.gms.internal.ads.zzgv r6 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r6.<init>(r3, r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r6     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r3 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0463
            r1 = 1
            goto L_0x0464
        L_0x0463:
            r1 = 0
        L_0x0464:
            com.google.android.gms.internal.ads.zzgv r4 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r3.obtainMessage(r15, r1, r9, r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            throw r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x046e:
            long r5 = android.os.SystemClock.elapsedRealtime()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r1 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x047e
            com.google.android.gms.internal.ads.zzmb r1 = r8.zzaeb     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzhr()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r14 = r5
            goto L_0x06da
        L_0x047e:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x0487
            com.google.android.gms.internal.ads.zzgv r1 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x04c5
        L_0x0487:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzadu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x04d2
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzeg()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x04d2
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r7 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r2 = r2.zza((int) r1, (com.google.android.gms.internal.ads.zzhi) r7, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r2.zzagj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r2 != 0) goto L_0x04a8
            goto L_0x04d2
        L_0x04a8:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x04b9
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.index     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r7 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.index     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2 - r7
            r7 = 100
            if (r2 == r7) goto L_0x04d2
        L_0x04b9:
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r7 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhl r14 = r8.zzacw     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r15 = r8.repeatMode     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r2.zza((int) r1, (com.google.android.gms.internal.ads.zzhi) r7, (com.google.android.gms.internal.ads.zzhl) r14, (int) r15)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x04c5:
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzew()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 < r2) goto L_0x04d5
            com.google.android.gms.internal.ads.zzmb r1 = r8.zzaeb     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzhr()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x04d2:
            r14 = r5
            goto L_0x05a7
        L_0x04d5:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x04df
            com.google.android.gms.internal.ads.zzgv r2 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r2.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x04dd:
            r14 = r5
            goto L_0x0534
        L_0x04df:
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r7 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza((int) r1, (com.google.android.gms.internal.ads.zzhi) r7, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhl r7 = r8.zzacw     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza((int) r9, (com.google.android.gms.internal.ads.zzhl) r7, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x04f0
            goto L_0x04dd
        L_0x04f0:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzef()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r7 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r14 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r14 = r14.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r15 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r7 = r7.zza((int) r14, (com.google.android.gms.internal.ads.zzhi) r15, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r14 = r7.zzagj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1 + r14
            long r14 = r8.zzaer     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1 - r14
            com.google.android.gms.internal.ads.zzhg r7 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r14 = 0
            r16 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            long r18 = java.lang.Math.max(r3, r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1 = r34
            r2 = r7
            r3 = r14
            r14 = r5
            r4 = r16
            r6 = r18
            android.util.Pair r1 = r1.zza(r2, r3, r4, r6)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05a7
            java.lang.Object r2 = r1.first     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.intValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r1 = r1.second     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r1.longValue()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1 = r2
        L_0x0534:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x053f
            r5 = 60000000(0x3938700, double:2.96439388E-316)
            long r5 = r5 + r3
        L_0x053c:
            r23 = r5
            goto L_0x0555
        L_0x053f:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r2.zzef()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r7 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r7 = r7.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r11 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r2 = r2.zza((int) r7, (com.google.android.gms.internal.ads.zzhi) r11, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r12 = r2.zzagj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r5 = r5 + r12
            goto L_0x053c
        L_0x0555:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x055c
            r29 = 0
            goto L_0x0563
        L_0x055c:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.index     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2 + r10
            r29 = r2
        L_0x0563:
            boolean r31 = r8.zzo(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r5 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza((int) r1, (com.google.android.gms.internal.ads.zzhi) r5, (boolean) r10)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = new com.google.android.gms.internal.ads.zzgs     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhf[] r5 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhe[] r6 = r8.zzadz     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zznf r7 = r8.zzacr     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzha r11 = r8.zzaea     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzmb r12 = r8.zzaeb     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r13 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            java.lang.Object r13 = r13.zzado     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r20 = r2
            r21 = r5
            r22 = r6
            r25 = r7
            r26 = r11
            r27 = r12
            r28 = r13
            r30 = r1
            r32 = r3
            r20.<init>(r21, r22, r23, r25, r26, r27, r28, r29, r30, r31, r32)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x059b
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzadx = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x059b:
            r8.zzaes = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r1 = r1.zzadn     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zza(r8, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzh(r10)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x05a7:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05c0
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzeg()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05b4
            goto L_0x05c0
        L_0x05b4:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x05c3
            boolean r1 = r8.zzadd     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x05c3
            r34.zzeo()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x05c3
        L_0x05c0:
            r8.zzh(r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x05c3:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x06da
        L_0x05c7:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == r2) goto L_0x0604
            long r1 = r8.zzaer     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r3.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzadr     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 < 0) goto L_0x0604
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.release()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r1.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzb((com.google.android.gms.internal.ads.zzgs) r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r1 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzadt     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.<init>(r2, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzel()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 5
            com.google.android.gms.internal.ads.zzgv r3 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            android.os.Message r1 = r1.obtainMessage(r2, r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x05c7
        L_0x0604:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzadu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x062e
            r1 = 0
        L_0x060b:
            com.google.android.gms.internal.ads.zzhf[] r2 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 >= r2) goto L_0x06da
            com.google.android.gms.internal.ads.zzhf[] r2 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = r2[r1]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzmo[] r3 = r3.zzadp     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3 = r3[r1]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x062b
            com.google.android.gms.internal.ads.zzmo r4 = r2.zzdr()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 != r3) goto L_0x062b
            boolean r3 = r2.zzds()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x062b
            r2.zzdt()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x062b:
            int r1 = r1 + 1
            goto L_0x060b
        L_0x062e:
            r1 = 0
        L_0x062f:
            com.google.android.gms.internal.ads.zzhf[] r2 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 >= r2) goto L_0x0651
            com.google.android.gms.internal.ads.zzhf[] r2 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = r2[r1]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzmo[] r3 = r3.zzadp     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3 = r3[r1]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzmo r4 = r2.zzdr()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 != r3) goto L_0x06da
            if (r3 == 0) goto L_0x064e
            boolean r2 = r2.zzds()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x064e
            goto L_0x06da
        L_0x064e:
            int r1 = r1 + 1
            goto L_0x062f
        L_0x0651:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r1.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x06da
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r1.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r1.zzadv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x06da
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zznh r1 = r1.zzady     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r2.zzadx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzaet = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zznh r2 = r2.zzady     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r3 = r3.zzadn     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzhi()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0680
            r3 = 1
            goto L_0x0681
        L_0x0680:
            r3 = 0
        L_0x0681:
            r4 = 0
        L_0x0682:
            com.google.android.gms.internal.ads.zzhf[] r5 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r5 = r5.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r4 >= r5) goto L_0x06da
            com.google.android.gms.internal.ads.zzhf[] r5 = r8.zzacq     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = r5[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzng r6 = r1.zzbeg     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzne r6 = r6.zzay(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r6 == 0) goto L_0x06d7
            if (r3 != 0) goto L_0x06d4
            boolean r6 = r5.zzdu()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r6 != 0) goto L_0x06d7
            com.google.android.gms.internal.ads.zzng r6 = r2.zzbeg     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzne r6 = r6.zzay(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhh[] r7 = r1.zzbei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r7 = r7[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhh[] r11 = r2.zzbei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11 = r11[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r6 == 0) goto L_0x06d4
            boolean r7 = r11.equals(r7)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x06d4
            int r7 = r6.length()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgw[] r7 = new com.google.android.gms.internal.ads.zzgw[r7]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11 = 0
        L_0x06b8:
            int r12 = r7.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r11 >= r12) goto L_0x06c4
            com.google.android.gms.internal.ads.zzgw r12 = r6.zzaw(r11)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r7[r11] = r12     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r11 = r11 + 1
            goto L_0x06b8
        L_0x06c4:
            com.google.android.gms.internal.ads.zzgs r6 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzmo[] r6 = r6.zzadp     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r6 = r6[r4]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r11 = r8.zzaet     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r11.zzef()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5.zza(r7, r6, r11)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x06d7
        L_0x06d4:
            r5.zzdt()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x06d7:
            int r4 = r4 + 1
            goto L_0x0682
        L_0x06da:
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 10
            if (r1 != 0) goto L_0x06e8
            r34.zzen()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zza((long) r14, (long) r2)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x084a
        L_0x06e8:
            java.lang.String r1 = "doSomeWork"
            com.google.android.gms.internal.ads.zzon.beginSection(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzel()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r1 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r1 = r1.zzadn     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgv r4 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r4 = r4.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.zzee(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhf[] r1 = r8.zzaej     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r4 = r1.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = 0
            r6 = 1
            r7 = 1
        L_0x0701:
            if (r5 >= r4) goto L_0x0738
            r11 = r1[r5]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r12 = r8.zzaer     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r8.zzaeo     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r11.zzb(r12, r2)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x0716
            boolean r2 = r11.zzeu()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0716
            r7 = 1
            goto L_0x0717
        L_0x0716:
            r7 = 0
        L_0x0717:
            boolean r2 = r11.isReady()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x0726
            boolean r2 = r11.zzeu()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x0724
            goto L_0x0726
        L_0x0724:
            r2 = 0
            goto L_0x0727
        L_0x0726:
            r2 = 1
        L_0x0727:
            if (r2 != 0) goto L_0x072c
            r11.zzdv()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x072c:
            if (r6 == 0) goto L_0x0732
            if (r2 == 0) goto L_0x0732
            r6 = 1
            goto L_0x0733
        L_0x0732:
            r6 = 0
        L_0x0733:
            int r5 = r5 + 1
            r2 = 10
            goto L_0x0701
        L_0x0738:
            if (r6 != 0) goto L_0x073d
            r34.zzen()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x073d:
            com.google.android.gms.internal.ads.zzog r1 = r8.zzaei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0762
            com.google.android.gms.internal.ads.zzog r1 = r8.zzaei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhc r1 = r1.zzfi()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhc r2 = r8.zzadi     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r1.equals(r2)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x0762
            r8.zzadi = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzoo r2 = r8.zzaed     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzog r3 = r8.zzaei     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r2 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3 = 7
            android.os.Message r1 = r2.obtainMessage(r3, r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendToTarget()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0762:
            com.google.android.gms.internal.ads.zzhg r1 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r2.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r3 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r1 = r1.zza((int) r2, (com.google.android.gms.internal.ads.zzhi) r3, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r1 = r1.zzagj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r7 == 0) goto L_0x0793
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0783
            com.google.android.gms.internal.ads.zzgv r3 = r8.zzadj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r3 = r3.zzaex     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x0793
        L_0x0783:
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaeu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r3 = r3.zzadu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 == 0) goto L_0x0793
            r3 = 4
            r8.setState(r3)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzek()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4 = 2
            goto L_0x0813
        L_0x0793:
            int r3 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r4 = 2
            if (r3 != r4) goto L_0x07f8
            com.google.android.gms.internal.ads.zzhf[] r3 = r8.zzaej     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 <= 0) goto L_0x07e6
            if (r6 == 0) goto L_0x07e4
            boolean r1 = r8.zzael     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzadv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 != 0) goto L_0x07ac
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r2.zzadt     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x07b4
        L_0x07ac:
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzlz r2 = r2.zzadn     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r2.zzhj()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07b4:
            r5 = -9223372036854775808
            int r7 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r7 != 0) goto L_0x07d0
            com.google.android.gms.internal.ads.zzgs r2 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r2 = r2.zzadu     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r2 == 0) goto L_0x07c2
            r1 = 1
            goto L_0x07e0
        L_0x07c2:
            com.google.android.gms.internal.ads.zzhg r2 = r8.zzade     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r3 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.zzads     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r5 = r8.zzacx     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzhi r2 = r2.zza((int) r3, (com.google.android.gms.internal.ads.zzhi) r5, (boolean) r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r2 = r2.zzagj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07d0:
            com.google.android.gms.internal.ads.zzha r5 = r8.zzaea     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgs r6 = r8.zzaes     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r8.zzaer     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r6 = r6.zzef()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            long r11 = r11 - r6
            long r2 = r2 - r11
            boolean r1 = r5.zzc(r2, r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07e0:
            if (r1 == 0) goto L_0x07e4
            r1 = 1
            goto L_0x07ea
        L_0x07e4:
            r1 = 0
            goto L_0x07ea
        L_0x07e6:
            boolean r1 = r8.zzdr(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x07ea:
            if (r1 == 0) goto L_0x0813
            r1 = 3
            r8.setState(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            boolean r1 = r8.zzacz     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0813
            r34.zzej()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0813
        L_0x07f8:
            int r3 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = 3
            if (r3 != r5) goto L_0x0813
            com.google.android.gms.internal.ads.zzhf[] r3 = r8.zzaej     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r3 = r3.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r3 <= 0) goto L_0x0803
            goto L_0x0807
        L_0x0803:
            boolean r6 = r8.zzdr(r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0807:
            if (r6 != 0) goto L_0x0813
            boolean r1 = r8.zzacz     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzael = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzek()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0813:
            int r1 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r4) goto L_0x0824
            com.google.android.gms.internal.ads.zzhf[] r1 = r8.zzaej     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r2 = r1.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x081a:
            if (r9 >= r2) goto L_0x0824
            r3 = r1[r9]     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzdv()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r9 = r9 + 1
            goto L_0x081a
        L_0x0824:
            boolean r1 = r8.zzacz     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x082d
            int r1 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 3
            if (r1 == r2) goto L_0x0831
        L_0x082d:
            int r1 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r4) goto L_0x0837
        L_0x0831:
            r1 = 10
            r8.zza((long) r14, (long) r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0847
        L_0x0837:
            com.google.android.gms.internal.ads.zzhf[] r1 = r8.zzaej     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.length     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0842
            r1 = 1000(0x3e8, double:4.94E-321)
            r8.zza((long) r14, (long) r1)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0847
        L_0x0842:
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzaee     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.removeMessages(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0847:
            com.google.android.gms.internal.ads.zzon.endSection()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x084a:
            return r10
        L_0x084b:
            r4 = 2
            int r1 = r1.arg1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0852
            r1 = 1
            goto L_0x0853
        L_0x0852:
            r1 = 0
        L_0x0853:
            r8.zzael = r9     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzacz = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != 0) goto L_0x0860
            r34.zzek()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r34.zzel()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0877
        L_0x0860:
            int r1 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2 = 3
            if (r1 != r2) goto L_0x086e
            r34.zzej()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzaee     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            goto L_0x0877
        L_0x086e:
            int r1 = r8.state     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 != r4) goto L_0x0877
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzaee     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x0877:
            return r10
        L_0x0878:
            r4 = 2
            java.lang.Object r2 = r1.obj     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzmb r2 = (com.google.android.gms.internal.ads.zzmb) r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            int r1 = r1.arg1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x0883
            r1 = 1
            goto L_0x0884
        L_0x0883:
            r1 = 0
        L_0x0884:
            com.google.android.gms.internal.ads.zzddu r3 = r8.zzact     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.sendEmptyMessage(r9)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzi(r10)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzha r3 = r8.zzaea     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r3.zzer()     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            if (r1 == 0) goto L_0x089f
            com.google.android.gms.internal.ads.zzgv r1 = new com.google.android.gms.internal.ads.zzgv     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r1.<init>(r9, r5)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.zzadj = r1     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
        L_0x089f:
            r8.zzaeb = r2     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzgk r1 = r8.zzaeg     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r2.zza(r1, r10, r8)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r8.setState(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            com.google.android.gms.internal.ads.zzddu r1 = r8.zzaee     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            r1.sendEmptyMessage(r4)     // Catch:{ zzgl -> 0x08b4, IOException -> 0x08af, RuntimeException -> 0x08b9 }
            return r10
        L_0x08af:
            r0 = move-exception
            r1 = r0
            r3 = 8
            goto L_0x08d9
        L_0x08b4:
            r0 = move-exception
            r1 = r0
            r3 = 8
            goto L_0x08f5
        L_0x08b9:
            r0 = move-exception
            r1 = r0
            java.lang.String r2 = "ExoPlayerImplInternal"
            java.lang.String r3 = "Internal runtime error."
            android.util.Log.e(r2, r3, r1)
            com.google.android.gms.internal.ads.zzddu r2 = r8.zzact
            com.google.android.gms.internal.ads.zzgl r1 = com.google.android.gms.internal.ads.zzgl.zza((java.lang.RuntimeException) r1)
            r3 = 8
            android.os.Message r1 = r2.obtainMessage(r3, r1)
            r1.sendToTarget()
            r34.zzem()
            return r10
        L_0x08d5:
            r0 = move-exception
            r3 = 8
            r1 = r0
        L_0x08d9:
            java.lang.String r2 = "ExoPlayerImplInternal"
            java.lang.String r4 = "Source error."
            android.util.Log.e(r2, r4, r1)
            com.google.android.gms.internal.ads.zzddu r2 = r8.zzact
            com.google.android.gms.internal.ads.zzgl r1 = com.google.android.gms.internal.ads.zzgl.zza((java.io.IOException) r1)
            android.os.Message r1 = r2.obtainMessage(r3, r1)
            r1.sendToTarget()
            r34.zzem()
            return r10
        L_0x08f1:
            r0 = move-exception
            r3 = 8
            r1 = r0
        L_0x08f5:
            java.lang.String r2 = "ExoPlayerImplInternal"
            java.lang.String r4 = "Renderer error."
            android.util.Log.e(r2, r4, r1)
            com.google.android.gms.internal.ads.zzddu r2 = r8.zzact
            android.os.Message r1 = r2.obtainMessage(r3, r1)
            r1.sendToTarget()
            r34.zzem()
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzgt.handleMessage(android.os.Message):boolean");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:10|11|12|13|23|20|8) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x000d, code lost:
        continue;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0015 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void release() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.zzaek     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r2)
            return
        L_0x0007:
            com.google.android.gms.internal.ads.zzddu r0 = r2.zzaee     // Catch:{ all -> 0x0024 }
            r1 = 6
            r0.sendEmptyMessage(r1)     // Catch:{ all -> 0x0024 }
        L_0x000d:
            boolean r0 = r2.zzaek     // Catch:{ all -> 0x0024 }
            if (r0 != 0) goto L_0x001d
            r2.wait()     // Catch:{ InterruptedException -> 0x0015 }
            goto L_0x000d
        L_0x0015:
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0024 }
            r0.interrupt()     // Catch:{ all -> 0x0024 }
            goto L_0x000d
        L_0x001d:
            android.os.HandlerThread r0 = r2.zzaef     // Catch:{ all -> 0x0024 }
            r0.quit()     // Catch:{ all -> 0x0024 }
            monitor-exit(r2)
            return
        L_0x0024:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzgt.release():void");
    }

    public final void stop() {
        this.zzaee.sendEmptyMessage(5);
    }

    public final void zza(zzmb zzmb, boolean z) {
        this.zzaee.obtainMessage(0, 1, 0, zzmb).sendToTarget();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:11|12|13|14|23|20|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x001f, code lost:
        continue;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0027 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zzb(com.google.android.gms.internal.ads.zzgp... r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.zzaek     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x000e
            java.lang.String r4 = "ExoPlayerImplInternal"
            java.lang.String r0 = "Ignoring messages sent after release."
            android.util.Log.w(r4, r0)     // Catch:{ all -> 0x0031 }
            monitor-exit(r3)
            return
        L_0x000e:
            int r0 = r3.zzaem     // Catch:{ all -> 0x0031 }
            int r1 = r0 + 1
            r3.zzaem = r1     // Catch:{ all -> 0x0031 }
            com.google.android.gms.internal.ads.zzddu r1 = r3.zzaee     // Catch:{ all -> 0x0031 }
            r2 = 11
            android.os.Message r4 = r1.obtainMessage(r2, r4)     // Catch:{ all -> 0x0031 }
            r4.sendToTarget()     // Catch:{ all -> 0x0031 }
        L_0x001f:
            int r4 = r3.zzaen     // Catch:{ all -> 0x0031 }
            if (r4 > r0) goto L_0x002f
            r3.wait()     // Catch:{ InterruptedException -> 0x0027 }
            goto L_0x001f
        L_0x0027:
            java.lang.Thread r4 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0031 }
            r4.interrupt()     // Catch:{ all -> 0x0031 }
            goto L_0x001f
        L_0x002f:
            monitor-exit(r3)
            return
        L_0x0031:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzgt.zzb(com.google.android.gms.internal.ads.zzgp[]):void");
    }

    public final void zzei() {
        this.zzaee.sendEmptyMessage(10);
    }

    public final void zzf(boolean z) {
        this.zzaee.obtainMessage(1, z ? 1 : 0, 0).sendToTarget();
    }

    public final void zza(zzhg zzhg, int i, long j) {
        this.zzaee.obtainMessage(3, new zzgu(zzhg, i, j)).sendToTarget();
    }

    public final void zza(zzgp... zzgpArr) {
        if (this.zzaek) {
            Log.w("ExoPlayerImplInternal", "Ignoring messages sent after release.");
            return;
        }
        this.zzaem++;
        this.zzaee.obtainMessage(11, zzgpArr).sendToTarget();
    }

    public final void zza(zzlz zzlz) {
        this.zzaee.obtainMessage(8, zzlz).sendToTarget();
    }

    private final void zza(long j, long j2) {
        this.zzaee.removeMessages(2);
        long elapsedRealtime = (j + j2) - SystemClock.elapsedRealtime();
        if (elapsedRealtime <= 0) {
            this.zzaee.sendEmptyMessage(2);
        } else {
            this.zzaee.sendEmptyMessageDelayed(2, elapsedRealtime);
        }
    }

    public final void zzb(zzhg zzhg, Object obj) {
        this.zzaee.obtainMessage(7, Pair.create(zzhg, obj)).sendToTarget();
    }

    private final void zzb(Object obj, int i) {
        this.zzact.obtainMessage(6, new zzgx(this.zzade, obj, this.zzadj, i)).sendToTarget();
    }

    private final Pair<Integer, Long> zzb(int i, long j) {
        return zzb(this.zzade, i, -9223372036854775807L);
    }

    private final long zza(int i, long j) throws zzgl {
        zzgs zzgs;
        zzek();
        this.zzael = false;
        setState(2);
        zzgs zzgs2 = this.zzaeu;
        if (zzgs2 == null) {
            zzgs zzgs3 = this.zzaes;
            if (zzgs3 != null) {
                zzgs3.release();
            }
            zzgs = null;
        } else {
            zzgs = null;
            while (zzgs2 != null) {
                if (zzgs2.zzads != i || !zzgs2.zzadv) {
                    zzgs2.release();
                } else {
                    zzgs = zzgs2;
                }
                zzgs2 = zzgs2.zzadx;
            }
        }
        zzgs zzgs4 = this.zzaeu;
        if (!(zzgs4 == zzgs && zzgs4 == this.zzaet)) {
            for (zzhf disable : this.zzaej) {
                disable.disable();
            }
            this.zzaej = new zzhf[0];
            this.zzaei = null;
            this.zzaeh = null;
            this.zzaeu = null;
        }
        if (zzgs != null) {
            zzgs.zzadx = null;
            this.zzaes = zzgs;
            this.zzaet = zzgs;
            zzb(zzgs);
            zzgs zzgs5 = this.zzaeu;
            if (zzgs5.zzadw) {
                j = zzgs5.zzadn.zzeg(j);
            }
            zzdq(j);
            zzeo();
        } else {
            this.zzaes = null;
            this.zzaet = null;
            this.zzaeu = null;
            zzdq(j);
        }
        this.zzaee.sendEmptyMessage(2);
        return j;
    }

    private final Pair<Integer, Long> zzb(zzhg zzhg, int i, long j) {
        return zza(zzhg, i, j, 0);
    }

    private final void zzb(zzgs zzgs) throws zzgl {
        if (this.zzaeu != zzgs) {
            boolean[] zArr = new boolean[this.zzacq.length];
            int i = 0;
            int i2 = 0;
            while (true) {
                zzhf[] zzhfArr = this.zzacq;
                if (i < zzhfArr.length) {
                    zzhf zzhf = zzhfArr[i];
                    zArr[i] = zzhf.getState() != 0;
                    zzne zzay = zzgs.zzady.zzbeg.zzay(i);
                    if (zzay != null) {
                        i2++;
                    }
                    if (zArr[i] && (zzay == null || (zzhf.zzdu() && zzhf.zzdr() == this.zzaeu.zzadp[i]))) {
                        if (zzhf == this.zzaeh) {
                            this.zzaed.zza(this.zzaei);
                            this.zzaei = null;
                            this.zzaeh = null;
                        }
                        zza(zzhf);
                        zzhf.disable();
                    }
                    i++;
                } else {
                    this.zzaeu = zzgs;
                    this.zzact.obtainMessage(3, zzgs.zzady).sendToTarget();
                    zza(zArr, i2);
                    return;
                }
            }
        }
    }

    private static void zza(zzhf zzhf) throws zzgl {
        if (zzhf.getState() == 2) {
            zzhf.stop();
        }
    }

    private final void zza(Object obj, int i) {
        this.zzadj = new zzgv(0, 0);
        zzb(obj, i);
        this.zzadj = new zzgv(0, -9223372036854775807L);
        setState(4);
        zzi(false);
    }

    private final int zza(int i, zzhg zzhg, zzhg zzhg2) {
        int zzew = zzhg.zzew();
        int i2 = i;
        int i3 = -1;
        for (int i4 = 0; i4 < zzew && i3 == -1; i4++) {
            i2 = zzhg.zza(i2, this.zzacx, this.zzacw, this.repeatMode);
            i3 = zzhg2.zzc(zzhg.zza(i2, this.zzacx, true).zzado);
        }
        return i3;
    }

    private final Pair<Integer, Long> zza(zzgu zzgu) {
        zzhg zzhg = zzgu.zzade;
        if (zzhg.isEmpty()) {
            zzhg = this.zzade;
        }
        try {
            Pair<Integer, Long> zzb = zzb(zzhg, zzgu.zzaev, zzgu.zzaew);
            zzhg zzhg2 = this.zzade;
            if (zzhg2 == zzhg) {
                return zzb;
            }
            int zzc = zzhg2.zzc(zzhg.zza(((Integer) zzb.first).intValue(), this.zzacx, true).zzado);
            if (zzc != -1) {
                return Pair.create(Integer.valueOf(zzc), (Long) zzb.second);
            }
            int zza = zza(((Integer) zzb.first).intValue(), zzhg, this.zzade);
            if (zza == -1) {
                return null;
            }
            this.zzade.zza(zza, this.zzacx, false);
            return zzb(0, -9223372036854775807L);
        } catch (IndexOutOfBoundsException unused) {
            throw new zzhb(this.zzade, zzgu.zzaev, zzgu.zzaew);
        }
    }

    private final Pair<Integer, Long> zza(zzhg zzhg, int i, long j, long j2) {
        zzoc.zzc(i, 0, zzhg.zzev());
        zzhg.zza(i, this.zzacw, false, j2);
        if (j == -9223372036854775807L) {
            j = this.zzacw.zzagy;
            if (j == -9223372036854775807L) {
                return null;
            }
        }
        long j3 = this.zzacw.zzagz + j;
        long j4 = zzhg.zza(0, this.zzacx, false).zzagj;
        if (j4 != -9223372036854775807L) {
            int i2 = (j3 > j4 ? 1 : (j3 == j4 ? 0 : -1));
        }
        return Pair.create(0, Long.valueOf(j3));
    }

    private static void zza(zzgs zzgs) {
        while (zzgs != null) {
            zzgs.release();
            zzgs = zzgs.zzadx;
        }
    }

    private final void zza(boolean[] zArr, int i) throws zzgl {
        this.zzaej = new zzhf[i];
        int i2 = 0;
        int i3 = 0;
        while (true) {
            zzhf[] zzhfArr = this.zzacq;
            if (i2 < zzhfArr.length) {
                zzhf zzhf = zzhfArr[i2];
                zzne zzay = this.zzaeu.zzady.zzbeg.zzay(i2);
                if (zzay != null) {
                    int i4 = i3 + 1;
                    this.zzaej[i3] = zzhf;
                    if (zzhf.getState() == 0) {
                        zzhh zzhh = this.zzaeu.zzady.zzbei[i2];
                        boolean z = this.zzacz && this.state == 3;
                        boolean z2 = !zArr[i2] && z;
                        zzgw[] zzgwArr = new zzgw[zzay.length()];
                        for (int i5 = 0; i5 < zzgwArr.length; i5++) {
                            zzgwArr[i5] = zzay.zzaw(i5);
                        }
                        zzgs zzgs = this.zzaeu;
                        zzhf.zza(zzhh, zzgwArr, zzgs.zzadp[i2], this.zzaer, z2, zzgs.zzef());
                        zzog zzdq = zzhf.zzdq();
                        if (zzdq != null) {
                            if (this.zzaei == null) {
                                this.zzaei = zzdq;
                                this.zzaeh = zzhf;
                                this.zzaei.zzb(this.zzadi);
                            } else {
                                throw zzgl.zza((RuntimeException) new IllegalStateException("Multiple renderer media clocks enabled."));
                            }
                        }
                        if (z) {
                            zzhf.start();
                        }
                    }
                    i3 = i4;
                }
                i2++;
            } else {
                return;
            }
        }
    }

    public final /* synthetic */ void zza(zzmn zzmn) {
        this.zzaee.obtainMessage(9, (zzlz) zzmn).sendToTarget();
    }
}
