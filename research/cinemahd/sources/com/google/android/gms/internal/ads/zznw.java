package com.google.android.gms.internal.ads;

import android.os.Looper;
import android.os.SystemClock;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

public final class zznw {
    /* access modifiers changed from: private */
    public final ExecutorService zzbfp;
    /* access modifiers changed from: private */
    public zzny<? extends zznx> zzbfq;
    /* access modifiers changed from: private */
    public IOException zzbfr;

    public zznw(String str) {
        this.zzbfp = zzoq.zzbk(str);
    }

    public final boolean isLoading() {
        return this.zzbfq != null;
    }

    public final <T extends zznx> long zza(T t, zznv<T> zznv, int i) {
        Looper myLooper = Looper.myLooper();
        zzoc.checkState(myLooper != null);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        new zzny(this, myLooper, t, zznv, i, elapsedRealtime).zzek(0);
        return elapsedRealtime;
    }

    public final void zzbc(int i) throws IOException {
        IOException iOException = this.zzbfr;
        if (iOException == null) {
            zzny<? extends zznx> zzny = this.zzbfq;
            if (zzny != null) {
                zzny.zzbc(zzny.zzbfu);
                return;
            }
            return;
        }
        throw iOException;
    }

    public final void zzil() {
        this.zzbfq.zzl(false);
    }

    public final void zza(Runnable runnable) {
        zzny<? extends zznx> zzny = this.zzbfq;
        if (zzny != null) {
            zzny.zzl(true);
        }
        this.zzbfp.execute(runnable);
        this.zzbfp.shutdown();
    }
}
