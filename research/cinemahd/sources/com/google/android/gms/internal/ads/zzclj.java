package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.ViewGroup;
import com.google.android.gms.internal.ads.zzbod;

public final class zzclj extends zzclk<zzbkk> {
    private final zzbvi zzelu;
    private final zzbrm zzers;
    private final zzcns zzexc;
    private final ViewGroup zzfdu;
    private final zzbqp zzffm;
    private final zzbfx zzfzz;
    private final zzbod.zza zzgaa;

    public zzclj(zzbfx zzbfx, zzbod.zza zza, zzcns zzcns, zzbrm zzbrm, zzbvi zzbvi, zzbqp zzbqp, ViewGroup viewGroup) {
        this.zzfzz = zzbfx;
        this.zzgaa = zza;
        this.zzexc = zzcns;
        this.zzers = zzbrm;
        this.zzelu = zzbvi;
        this.zzffm = zzbqp;
        this.zzfdu = viewGroup;
    }

    /* access modifiers changed from: protected */
    public final zzdhe<zzbkk> zza(zzczu zzczu, Bundle bundle) {
        return this.zzfzz.zzach().zzc(this.zzgaa.zza(zzczu).zze(bundle).zzahh()).zzc(this.zzers).zza(this.zzexc).zzb(this.zzelu).zza(new zzbma(this.zzffm)).zzb(new zzbkf(this.zzfdu)).zzaee().zzadc().zzaha();
    }
}
