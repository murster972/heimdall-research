package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzcgg implements zzdgf {
    private final zzaju zzfvn;

    zzcgg(zzaju zzaju) {
        this.zzfvn = zzaju;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfvn.zzi((JSONObject) obj);
    }
}
