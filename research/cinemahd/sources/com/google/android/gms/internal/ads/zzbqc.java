package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.AdMetadataListener;
import java.util.Set;

public final class zzbqc implements zzdxg<zzbqa> {
    private final zzdxp<Set<zzbsu<AdMetadataListener>>> zzfeo;

    private zzbqc(zzdxp<Set<zzbsu<AdMetadataListener>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbqc zzl(zzdxp<Set<zzbsu<AdMetadataListener>>> zzdxp) {
        return new zzbqc(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbqa(this.zzfeo.get());
    }
}
