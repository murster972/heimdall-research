package com.google.android.gms.internal.ads;

import java.io.IOException;

public interface zzmo {
    boolean isReady();

    int zzb(zzgy zzgy, zzis zzis, boolean z);

    void zzeh(long j);

    void zzhk() throws IOException;
}
