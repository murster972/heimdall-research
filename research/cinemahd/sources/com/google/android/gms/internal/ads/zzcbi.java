package com.google.android.gms.internal.ads;

public abstract class zzcbi implements zzbob<zzcbb> {
    public abstract zzcbd zza(zzbmt zzbmt, zzcbg zzcbg);

    public abstract zzbmz<zzcbb> zzadc();

    public abstract zzbou zzadd();
}
