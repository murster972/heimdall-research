package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdte;

public abstract class zzdiu<KeyProtoT extends zzdte, PublicKeyProtoT extends zzdte> extends zzdii<KeyProtoT> {
    private final Class<PublicKeyProtoT> zzgym;

    @SafeVarargs
    protected zzdiu(Class<KeyProtoT> cls, Class<PublicKeyProtoT> cls2, zzdik<?, KeyProtoT>... zzdikArr) {
        super(cls, zzdikArr);
        this.zzgym = cls2;
    }
}
