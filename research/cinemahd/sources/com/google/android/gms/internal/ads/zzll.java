package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzle;

public abstract class zzll implements zzle.zza {
    public final String zzaez;

    public zzll(String str) {
        this.zzaez = (String) zzoc.checkNotNull(str);
    }

    public int describeContents() {
        return 0;
    }
}
