package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzc;

public final class zzbnr implements zzdxg<zzc> {
    private final zzdxp<Context> zzejv;
    private final zzbns zzfgy;
    private final zzdxp<zzato> zzfgz;

    private zzbnr(zzbns zzbns, zzdxp<Context> zzdxp, zzdxp<zzato> zzdxp2) {
        this.zzfgy = zzbns;
        this.zzejv = zzdxp;
        this.zzfgz = zzdxp2;
    }

    public static zzbnr zza(zzbns zzbns, zzdxp<Context> zzdxp, zzdxp<zzato> zzdxp2) {
        return new zzbnr(zzbns, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzc) zzdxm.zza(new zzc(this.zzejv.get(), this.zzfgz.get(), (zzapz) null), "Cannot return null from a non-@Nullable @Provides method");
    }
}
