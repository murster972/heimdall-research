package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdte;
import java.security.GeneralSecurityException;

public abstract class zzdih<KeyFormatProtoT extends zzdte, KeyT> {
    private final Class<KeyFormatProtoT> zzgxv;

    public zzdih(Class<KeyFormatProtoT> cls) {
        this.zzgxv = cls;
    }

    public final Class<KeyFormatProtoT> zzasb() {
        return this.zzgxv;
    }

    public abstract void zzc(KeyFormatProtoT keyformatprotot) throws GeneralSecurityException;

    public abstract KeyT zzd(KeyFormatProtoT keyformatprotot) throws GeneralSecurityException;

    public abstract KeyFormatProtoT zzq(zzdqk zzdqk) throws zzdse;
}
