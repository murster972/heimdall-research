package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;
import java.util.Set;

public final class zzbtp implements zzdxg<zzbtj> {
    private final zzdxp<Set<zzbsu<VideoController.VideoLifecycleCallbacks>>> zzfeo;

    private zzbtp(zzdxp<Set<zzbsu<VideoController.VideoLifecycleCallbacks>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbtp zzt(zzdxp<Set<zzbsu<VideoController.VideoLifecycleCallbacks>>> zzdxp) {
        return new zzbtp(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbtj(this.zzfeo.get());
    }
}
