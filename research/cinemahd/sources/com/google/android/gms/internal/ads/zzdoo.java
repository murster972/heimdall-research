package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.interfaces.ECPrivateKey;

public final class zzdoo implements zzdib {
    private static final byte[] zzgzh = new byte[0];
    private final ECPrivateKey zzhfl;
    private final zzdoq zzhfm;
    private final String zzhfn;
    private final byte[] zzhfo;
    private final zzdow zzhfp;
    private final zzdop zzhfq;

    public zzdoo(ECPrivateKey eCPrivateKey, byte[] bArr, String str, zzdow zzdow, zzdop zzdop) throws GeneralSecurityException {
        this.zzhfl = eCPrivateKey;
        this.zzhfm = new zzdoq(eCPrivateKey);
        this.zzhfo = bArr;
        this.zzhfn = str;
        this.zzhfp = zzdow;
        this.zzhfq = zzdop;
    }
}
