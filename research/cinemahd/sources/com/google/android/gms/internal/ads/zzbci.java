package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

public final class zzbci extends zzavo {
    final zzbaz zzdxu;
    private final String zzdyg;
    private final String[] zzdyh;
    final zzbcn zzecy;

    zzbci(zzbaz zzbaz, zzbcn zzbcn, String str, String[] strArr) {
        this.zzdxu = zzbaz;
        this.zzecy = zzbcn;
        this.zzdyg = str;
        this.zzdyh = strArr;
        zzq.zzlm().zza(this);
    }

    public final void zztu() {
        try {
            this.zzecy.zze(this.zzdyg, this.zzdyh);
        } finally {
            zzawb.zzdsr.post(new zzbcl(this));
        }
    }
}
