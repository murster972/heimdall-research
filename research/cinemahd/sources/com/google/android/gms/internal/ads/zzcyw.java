package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcyw implements zzdxg<zzcyt> {
    private final zzdxp<zzbfx> zzejt;
    private final zzdxp<zzczs> zzezj;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<Context> zzgiq;
    private final zzdxp<zzcxt<zzcbi, zzcbb>> zzgir;
    private final zzdxp<zzcxz> zzgis;
    private final zzdxp<zzczw> zzgit;

    public zzcyw(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzbfx> zzdxp3, zzdxp<zzcxt<zzcbi, zzcbb>> zzdxp4, zzdxp<zzcxz> zzdxp5, zzdxp<zzczw> zzdxp6, zzdxp<zzczs> zzdxp7) {
        this.zzgiq = zzdxp;
        this.zzfei = zzdxp2;
        this.zzejt = zzdxp3;
        this.zzgir = zzdxp4;
        this.zzgis = zzdxp5;
        this.zzgit = zzdxp6;
        this.zzezj = zzdxp7;
    }

    public final /* synthetic */ Object get() {
        return new zzcyt(this.zzgiq.get(), this.zzfei.get(), this.zzejt.get(), this.zzgir.get(), this.zzgis.get(), this.zzgit.get(), this.zzezj.get());
    }
}
