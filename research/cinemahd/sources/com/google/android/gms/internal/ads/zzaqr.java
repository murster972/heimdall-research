package com.google.android.gms.internal.ads;

import android.content.Context;
import org.json.JSONObject;

public interface zzaqr {
    void zza(Context context, String str, JSONObject jSONObject);
}
