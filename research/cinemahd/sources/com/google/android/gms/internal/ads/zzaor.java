package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzaor extends RemoteCreator<zzaou> {
    public zzaor() {
        super("com.google.android.gms.ads.AdOverlayCreatorImpl");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object getRemoteCreator(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlayCreator");
        if (queryLocalInterface instanceof zzaou) {
            return (zzaou) queryLocalInterface;
        }
        return new zzaox(iBinder);
    }

    public final zzaot zzc(Activity activity) {
        try {
            IBinder zzae = ((zzaou) getRemoteCreatorInstance(activity)).zzae(ObjectWrapper.a(activity));
            if (zzae == null) {
                return null;
            }
            IInterface queryLocalInterface = zzae.queryLocalInterface("com.google.android.gms.ads.internal.overlay.client.IAdOverlay");
            if (queryLocalInterface instanceof zzaot) {
                return (zzaot) queryLocalInterface;
            }
            return new zzaov(zzae);
        } catch (RemoteException e) {
            zzayu.zzd("Could not create remote AdOverlay.", e);
            return null;
        } catch (RemoteCreator.RemoteCreatorException e2) {
            zzayu.zzd("Could not create remote AdOverlay.", e2);
            return null;
        }
    }
}
