package com.google.android.gms.internal.measurement;

interface zzgk {
    boolean zza(Class<?> cls);

    zzgl zzb(Class<?> cls);
}
