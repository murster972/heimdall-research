package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbtz implements zzdxg<zzbsu<zzbqb>> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzczl> zzfda;
    private final zzdxp<zzczu> zzfep;
    private final zzbtv zzfje;

    private zzbtz(zzbtv zzbtv, zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzczu> zzdxp4) {
        this.zzfje = zzbtv;
        this.zzejv = zzdxp;
        this.zzfav = zzdxp2;
        this.zzfda = zzdxp3;
        this.zzfep = zzdxp4;
    }

    public static zzbtz zza(zzbtv zzbtv, zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzczu> zzdxp4) {
        return new zzbtz(zzbtv, zzdxp, zzdxp2, zzdxp3, zzdxp4);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(new zzbty(this.zzejv.get(), this.zzfav.get(), this.zzfda.get(), this.zzfep.get()), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
