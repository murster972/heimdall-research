package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzavh implements Callable {
    private final zzave zzdqx;

    zzavh(zzave zzave) {
        this.zzdqx = zzave;
    }

    public final Object call() {
        return this.zzdqx.zzvi();
    }
}
