package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcre implements Callable {
    private final zzcrf zzgfk;

    zzcre(zzcrf zzcrf) {
        this.zzgfk = zzcrf;
    }

    public final Object call() {
        return this.zzgfk.zzanf();
    }
}
