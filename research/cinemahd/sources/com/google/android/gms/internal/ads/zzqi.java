package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.PlatformVersion;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@TargetApi(14)
public final class zzqi extends Thread {
    private final Object lock;
    private boolean started;
    private final int zzboj;
    private final int zzbol;
    private final boolean zzbom;
    private boolean zzbpj;
    private boolean zzbpk;
    private final zzqf zzbpl;
    private final int zzbpm;
    private final int zzbpn;
    private final int zzbpo;
    private final int zzbpp;
    private final int zzbpq;
    private final int zzbpr;
    private final String zzbps;
    private final boolean zzbpt;
    private final boolean zzbpu;

    public zzqi() {
        this(new zzqf());
    }

    private final zzqm zza(View view, zzqc zzqc) {
        boolean z;
        if (view == null) {
            return new zzqm(this, 0, 0);
        }
        boolean globalVisibleRect = view.getGlobalVisibleRect(new Rect());
        if ((view instanceof TextView) && !(view instanceof EditText)) {
            CharSequence text = ((TextView) view).getText();
            if (TextUtils.isEmpty(text)) {
                return new zzqm(this, 0, 0);
            }
            zzqc.zzb(text.toString(), globalVisibleRect, view.getX(), view.getY(), (float) view.getWidth(), (float) view.getHeight());
            return new zzqm(this, 1, 0);
        } else if ((view instanceof WebView) && !(view instanceof zzbdi)) {
            WebView webView = (WebView) view;
            if (!PlatformVersion.f()) {
                z = false;
            } else {
                zzqc.zzlx();
                webView.post(new zzqk(this, zzqc, webView, globalVisibleRect));
                z = true;
            }
            if (z) {
                return new zzqm(this, 0, 1);
            }
            return new zzqm(this, 0, 0);
        } else if (!(view instanceof ViewGroup)) {
            return new zzqm(this, 0, 0);
        } else {
            ViewGroup viewGroup = (ViewGroup) view;
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                zzqm zza = zza(viewGroup.getChildAt(i3), zzqc);
                i += zza.zzbqb;
                i2 += zza.zzbqc;
            }
            return new zzqm(this, i, i2);
        }
    }

    private static boolean zzmc() {
        boolean z;
        try {
            Context context = zzq.zzkt().getContext();
            if (context == null) {
                return false;
            }
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
            if (activityManager == null) {
                return false;
            }
            if (keyguardManager == null) {
                return false;
            }
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (Process.myPid() == next.pid) {
                    if (next.importance != 100 || keyguardManager.inKeyguardRestrictedInputMode()) {
                        return false;
                    }
                    PowerManager powerManager = (PowerManager) context.getSystemService("power");
                    if (powerManager == null) {
                        z = false;
                    } else {
                        z = powerManager.isScreenOn();
                    }
                    if (z) {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        } catch (Throwable th) {
            zzq.zzku().zza(th, "ContentFetchTask.isInForeground");
            return false;
        }
    }

    private final void zzme() {
        synchronized (this.lock) {
            this.zzbpj = true;
            boolean z = this.zzbpj;
            StringBuilder sb = new StringBuilder(42);
            sb.append("ContentFetchThread: paused, mPause = ");
            sb.append(z);
            zzayu.zzea(sb.toString());
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x0084 */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0084 A[LOOP:1: B:30:0x0084->B:42:0x0084, LOOP_START, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
        L_0x0000:
            boolean r0 = zzmc()     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            if (r0 == 0) goto L_0x005a
            com.google.android.gms.internal.ads.zzqe r0 = com.google.android.gms.ads.internal.zzq.zzkt()     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            android.app.Activity r0 = r0.getActivity()     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            if (r0 != 0) goto L_0x0019
            java.lang.String r0 = "ContentFetchThread: no activity. Sleeping."
            com.google.android.gms.internal.ads.zzayu.zzea(r0)     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            r4.zzme()     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            goto L_0x0062
        L_0x0019:
            if (r0 == 0) goto L_0x0062
            r1 = 0
            android.view.Window r2 = r0.getWindow()     // Catch:{ Exception -> 0x003d }
            if (r2 == 0) goto L_0x004c
            android.view.Window r2 = r0.getWindow()     // Catch:{ Exception -> 0x003d }
            android.view.View r2 = r2.getDecorView()     // Catch:{ Exception -> 0x003d }
            if (r2 == 0) goto L_0x004c
            android.view.Window r0 = r0.getWindow()     // Catch:{ Exception -> 0x003d }
            android.view.View r0 = r0.getDecorView()     // Catch:{ Exception -> 0x003d }
            r2 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r0 = r0.findViewById(r2)     // Catch:{ Exception -> 0x003d }
            r1 = r0
            goto L_0x004c
        L_0x003d:
            r0 = move-exception
            com.google.android.gms.internal.ads.zzave r2 = com.google.android.gms.ads.internal.zzq.zzku()     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            java.lang.String r3 = "ContentFetchTask.extractContent"
            r2.zza(r0, r3)     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            java.lang.String r0 = "Failed getting root view of activity. Content not extracted."
            com.google.android.gms.internal.ads.zzayu.zzea(r0)     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
        L_0x004c:
            if (r1 == 0) goto L_0x0062
            if (r1 != 0) goto L_0x0051
            goto L_0x0062
        L_0x0051:
            com.google.android.gms.internal.ads.zzql r0 = new com.google.android.gms.internal.ads.zzql     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            r0.<init>(r4, r1)     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            r1.post(r0)     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            goto L_0x0062
        L_0x005a:
            java.lang.String r0 = "ContentFetchTask: sleeping"
            com.google.android.gms.internal.ads.zzayu.zzea(r0)     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            r4.zzme()     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
        L_0x0062:
            int r0 = r4.zzbpm     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            int r0 = r0 * 1000
            long r0 = (long) r0     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x007b, Exception -> 0x006b }
            goto L_0x0081
        L_0x006b:
            r0 = move-exception
            java.lang.String r1 = "Error in ContentFetchTask"
            com.google.android.gms.internal.ads.zzayu.zzc(r1, r0)
            com.google.android.gms.internal.ads.zzave r1 = com.google.android.gms.ads.internal.zzq.zzku()
            java.lang.String r2 = "ContentFetchTask.run"
            r1.zza(r0, r2)
            goto L_0x0081
        L_0x007b:
            r0 = move-exception
            java.lang.String r1 = "Error in ContentFetchTask"
            com.google.android.gms.internal.ads.zzayu.zzc(r1, r0)
        L_0x0081:
            java.lang.Object r0 = r4.lock
            monitor-enter(r0)
        L_0x0084:
            boolean r1 = r4.zzbpj     // Catch:{ all -> 0x0096 }
            if (r1 == 0) goto L_0x0093
            java.lang.String r1 = "ContentFetchTask: waiting"
            com.google.android.gms.internal.ads.zzayu.zzea(r1)     // Catch:{ InterruptedException -> 0x0084 }
            java.lang.Object r1 = r4.lock     // Catch:{ InterruptedException -> 0x0084 }
            r1.wait()     // Catch:{ InterruptedException -> 0x0084 }
            goto L_0x0084
        L_0x0093:
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            goto L_0x0000
        L_0x0096:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzqi.run():void");
    }

    public final void wakeup() {
        synchronized (this.lock) {
            this.zzbpj = false;
            this.lock.notifyAll();
            zzayu.zzea("ContentFetchThread: wakeup");
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzi(View view) {
        try {
            zzqc zzqc = new zzqc(this.zzboj, this.zzbpn, this.zzbol, this.zzbpo, this.zzbpp, this.zzbpq, this.zzbpr, this.zzbom);
            Context context = zzq.zzkt().getContext();
            if (context != null && !TextUtils.isEmpty(this.zzbps)) {
                String str = (String) view.getTag(context.getResources().getIdentifier((String) zzve.zzoy().zzd(zzzn.zzchu), "id", context.getPackageName()));
                if (str != null && str.equals(this.zzbps)) {
                    return;
                }
            }
            zzqm zza = zza(view, zzqc);
            zzqc.zzlz();
            if (zza.zzbqb != 0 || zza.zzbqc != 0) {
                if (zza.zzbqc != 0 || zzqc.zzma() != 0) {
                    if (zza.zzbqc != 0 || !this.zzbpl.zza(zzqc)) {
                        this.zzbpl.zzc(zzqc);
                    }
                }
            }
        } catch (Exception e) {
            zzayu.zzc("Exception in fetchContentOnUIThread", e);
            zzq.zzku().zza(e, "ContentFetchTask.fetchContent");
        }
    }

    public final void zzmb() {
        synchronized (this.lock) {
            if (this.started) {
                zzayu.zzea("Content hash thread already started, quiting...");
                return;
            }
            this.started = true;
            start();
        }
    }

    public final zzqc zzmd() {
        return this.zzbpl.zzo(this.zzbpu);
    }

    public final boolean zzmf() {
        return this.zzbpj;
    }

    private zzqi(zzqf zzqf) {
        this.started = false;
        this.zzbpj = false;
        this.zzbpk = false;
        this.zzbpl = zzqf;
        this.lock = new Object();
        this.zzboj = zzaaw.zzcta.get().intValue();
        this.zzbpn = zzaaw.zzcsx.get().intValue();
        this.zzbol = zzaaw.zzctb.get().intValue();
        this.zzbpo = zzaaw.zzcsz.get().intValue();
        this.zzbpp = ((Integer) zzve.zzoy().zzd(zzzn.zzchr)).intValue();
        this.zzbpq = ((Integer) zzve.zzoy().zzd(zzzn.zzchs)).intValue();
        this.zzbpr = ((Integer) zzve.zzoy().zzd(zzzn.zzcht)).intValue();
        this.zzbpm = zzaaw.zzctc.get().intValue();
        this.zzbps = (String) zzve.zzoy().zzd(zzzn.zzchv);
        this.zzbpt = ((Boolean) zzve.zzoy().zzd(zzzn.zzchw)).booleanValue();
        this.zzbom = ((Boolean) zzve.zzoy().zzd(zzzn.zzchx)).booleanValue();
        this.zzbpu = ((Boolean) zzve.zzoy().zzd(zzzn.zzchy)).booleanValue();
        setName("ContentFetchTask");
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzqc zzqc, WebView webView, String str, boolean z) {
        zzqc.zzlw();
        try {
            if (!TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString("text");
                if (this.zzbpt || TextUtils.isEmpty(webView.getTitle())) {
                    zzqc.zza(optString, z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                } else {
                    String title = webView.getTitle();
                    StringBuilder sb = new StringBuilder(String.valueOf(title).length() + 1 + String.valueOf(optString).length());
                    sb.append(title);
                    sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                    sb.append(optString);
                    zzqc.zza(sb.toString(), z, webView.getX(), webView.getY(), (float) webView.getWidth(), (float) webView.getHeight());
                }
            }
            if (zzqc.zzlr()) {
                this.zzbpl.zzb(zzqc);
            }
        } catch (JSONException unused) {
            zzayu.zzea("Json string may be malformed.");
        } catch (Throwable th) {
            zzayu.zzb("Failed to get webview content.", th);
            zzq.zzku().zza(th, "ContentFetchTask.processWebViewContent");
        }
    }
}
