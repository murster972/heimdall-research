package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final class zzta implements zzdsa {
    static final zzdsa zzew = new zzta();

    private zzta() {
    }

    public final boolean zzf(int i) {
        return zzsy.zza.zzc.zzbs(i) != null;
    }
}
