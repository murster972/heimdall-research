package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.ads.internal.zzc;
import java.util.Map;
import java.util.concurrent.Executor;

public final class zzcbp {
    private final zzato zzbkt;
    private final zzdq zzefv;
    private final Executor zzfci;
    /* access modifiers changed from: private */
    public final zzbpm zzfgg;
    private final zzbqj zzfjf;
    private final zzboq zzfke;
    private final zzbjd zzfkf;
    private final zzbst zzfkh;
    private final zzbqa zzfqp;
    private final zzbra zzfqq;
    private final zzc zzfrf;
    private final zzbpg zzfrg;
    /* access modifiers changed from: private */
    public final zzbqw zzfrh;

    public zzcbp(zzboq zzboq, zzbpm zzbpm, zzbqa zzbqa, zzbqj zzbqj, zzbra zzbra, Executor executor, zzbst zzbst, zzbjd zzbjd, zzc zzc, zzbpg zzbpg, zzato zzato, zzdq zzdq, zzbqw zzbqw) {
        this.zzfke = zzboq;
        this.zzfgg = zzbpm;
        this.zzfqp = zzbqa;
        this.zzfjf = zzbqj;
        this.zzfqq = zzbra;
        this.zzfci = executor;
        this.zzfkh = zzbst;
        this.zzfkf = zzbjd;
        this.zzfrf = zzc;
        this.zzfrg = zzbpg;
        this.zzbkt = zzato;
        this.zzefv = zzdq;
        this.zzfrh = zzbqw;
    }

    public static zzdhe<?> zza(zzbdi zzbdi, String str, String str2) {
        zzazl zzazl = new zzazl();
        zzbdi.zzaaa().zza((zzbeu) new zzcca(zzazl));
        zzbdi.zzb(str, str2, (String) null);
        return zzazl;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzad(View view) {
        this.zzfrf.recordClick();
        zzato zzato = this.zzbkt;
        if (zzato != null) {
            zzato.zzum();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzakv() {
        this.zzfgg.onAdLeftApplication();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzakw() {
        this.zzfke.onAdClicked();
    }

    public final void zzb(zzbdi zzbdi, boolean z) {
        zzdg zzbw;
        zzbdi.zzaaa().zza(new zzcbs(this), this.zzfqp, this.zzfjf, new zzcbr(this), new zzcbu(this), z, (zzafq) null, this.zzfrf, new zzcbz(this), this.zzbkt);
        zzbdi.setOnTouchListener(new zzcbt(this));
        zzbdi.setOnClickListener(new zzcbw(this));
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclc)).booleanValue() && (zzbw = this.zzefv.zzbw()) != null) {
            zzbw.zzb(zzbdi.getView());
        }
        this.zzfkh.zza(zzbdi, this.zzfci);
        this.zzfkh.zza(new zzcbv(zzbdi), this.zzfci);
        this.zzfkh.zzq(zzbdi.getView());
        zzbdi.zza("/trackActiveViewUnit", (zzafn<? super zzbdi>) new zzcby(this, zzbdi));
        this.zzfkf.zzo(zzbdi);
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzciv)).booleanValue()) {
            zzbpg zzbpg = this.zzfrg;
            zzbdi.getClass();
            zzbpg.zza(zzcbx.zzn(zzbdi), this.zzfci);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzp(String str, String str2) {
        this.zzfqq.onAppEvent(str, str2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzbdi zzbdi, zzbdi zzbdi2, Map map) {
        this.zzfkf.zzf(zzbdi);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ boolean zza(View view, MotionEvent motionEvent) {
        this.zzfrf.recordClick();
        zzato zzato = this.zzbkt;
        if (zzato == null) {
            return false;
        }
        zzato.zzum();
        return false;
    }
}
