package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.ads.internal.zzq;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

public final class zzagh implements zzn {
    /* access modifiers changed from: private */
    public volatile zzaga zzcxz;
    private final Context zzup;

    public zzagh(Context context) {
        this.zzup = context;
    }

    /* access modifiers changed from: private */
    public final void disconnect() {
        if (this.zzcxz != null) {
            this.zzcxz.disconnect();
            Binder.flushPendingCommands();
        }
    }

    public final zzo zzc(zzq<?> zzq) throws zzae {
        zzafz zzh = zzafz.zzh(zzq);
        long a2 = zzq.zzkx().a();
        try {
            zzazl zzazl = new zzazl();
            this.zzcxz = new zzaga(this.zzup, zzq.zzle().zzxb(), new zzagl(this, zzazl), new zzago(this, zzazl));
            this.zzcxz.checkAvailabilityAndConnect();
            zzdhe zza = zzdgs.zza(zzdgs.zzb(zzazl, new zzagk(this, zzh), (Executor) zzazd.zzdwe), (long) ((Integer) zzve.zzoy().zzd(zzzn.zzcni)).intValue(), TimeUnit.MILLISECONDS, zzazd.zzdwh);
            zza.addListener(new zzagm(this), zzazd.zzdwe);
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) zza.get();
            long a3 = zzq.zzkx().a() - a2;
            StringBuilder sb = new StringBuilder(52);
            sb.append("Http assets remote cache took ");
            sb.append(a3);
            sb.append("ms");
            zzavs.zzed(sb.toString());
            zzagb zzagb = (zzagb) new zzaqj(parcelFileDescriptor).zza(zzagb.CREATOR);
            if (zzagb == null) {
                return null;
            }
            if (zzagb.zzcxx) {
                throw new zzae(zzagb.zzcxy);
            } else if (zzagb.zzcxv.length != zzagb.zzcxw.length) {
                return null;
            } else {
                HashMap hashMap = new HashMap();
                int i = 0;
                while (true) {
                    String[] strArr = zzagb.zzcxv;
                    if (i >= strArr.length) {
                        return new zzo(zzagb.statusCode, zzagb.data, (Map<String, String>) hashMap, zzagb.zzac, zzagb.zzad);
                    }
                    hashMap.put(strArr[i], zzagb.zzcxw[i]);
                    i++;
                }
            }
        } catch (InterruptedException | ExecutionException unused) {
            StringBuilder sb2 = new StringBuilder(52);
            sb2.append("Http assets remote cache took ");
            sb2.append(zzq.zzkx().a() - a2);
            sb2.append("ms");
            zzavs.zzed(sb2.toString());
            return null;
        } catch (Throwable th) {
            long a4 = zzq.zzkx().a() - a2;
            StringBuilder sb3 = new StringBuilder(52);
            sb3.append("Http assets remote cache took ");
            sb3.append(a4);
            sb3.append("ms");
            zzavs.zzed(sb3.toString());
            throw th;
        }
    }
}
