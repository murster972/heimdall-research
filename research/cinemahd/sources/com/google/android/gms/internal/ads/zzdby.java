package com.google.android.gms.internal.ads;

public interface zzdby<I, O> {
    O apply(I i) throws Exception;
}
