package com.google.android.gms.internal.ads;

final class zzzo implements zzabo {
    private final /* synthetic */ zzzj zzcrh;

    zzzo(zzzj zzzj) {
        this.zzcrh = zzzj;
    }

    public final String get(String str, String str2) {
        return this.zzcrh.zzcgc.getString(str, str2);
    }

    public final Long getLong(String str, long j) {
        try {
            return Long.valueOf(this.zzcrh.zzcgc.getLong(str, j));
        } catch (ClassCastException unused) {
            return Long.valueOf((long) this.zzcrh.zzcgc.getInt(str, (int) j));
        }
    }

    public final Double zza(String str, double d) {
        return Double.valueOf((double) this.zzcrh.zzcgc.getFloat(str, (float) d));
    }

    public final Boolean zze(String str, boolean z) {
        return Boolean.valueOf(this.zzcrh.zzcgc.getBoolean(str, z));
    }
}
