package com.google.android.gms.internal.ads;

final class zzpl implements Runnable {
    private final /* synthetic */ zzit zzahl;
    private final /* synthetic */ zzpg zzbjg;

    zzpl(zzpg zzpg, zzit zzit) {
        this.zzbjg = zzpg;
        this.zzahl = zzit;
    }

    public final void run() {
        this.zzahl.zzge();
        this.zzbjg.zzbjh.zzf(this.zzahl);
    }
}
