package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.NativeAppInstallAd;

public final class zzael extends zzadh {
    private final NativeAppInstallAd.OnAppInstallAdLoadedListener zzcwh;

    public zzael(NativeAppInstallAd.OnAppInstallAdLoadedListener onAppInstallAdLoadedListener) {
        this.zzcwh = onAppInstallAdLoadedListener;
    }

    public final void zza(zzacw zzacw) {
        this.zzcwh.onAppInstallAdLoaded(new zzacx(zzacw));
    }
}
