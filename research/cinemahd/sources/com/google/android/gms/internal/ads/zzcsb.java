package com.google.android.gms.internal.ads;

public final class zzcsb implements zzcub<zzcry> {
    private final zzdhd zzfov;

    public zzcsb(zzdhd zzdhd) {
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcry> zzanc() {
        return this.zzfov.zzd(zzcsa.zzgfx);
    }
}
