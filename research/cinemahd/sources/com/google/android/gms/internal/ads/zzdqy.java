package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Arrays;

final class zzdqy extends zzdqw {
    private final byte[] buffer;
    private int limit;
    private int pos;
    private int zzhik;
    private int zzhil;
    private int zzhin;
    private final boolean zzhip;
    private int zzhiq;

    private zzdqy(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.zzhin = Integer.MAX_VALUE;
        this.buffer = bArr;
        this.limit = i2 + i;
        this.pos = i;
        this.zzhiq = this.pos;
        this.zzhip = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
        if (r2[r3] >= 0) goto L_0x0068;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int zzayu() throws java.io.IOException {
        /*
            r5 = this;
            int r0 = r5.pos
            int r1 = r5.limit
            if (r1 == r0) goto L_0x006b
            byte[] r2 = r5.buffer
            int r3 = r0 + 1
            byte r0 = r2[r0]
            if (r0 < 0) goto L_0x0011
            r5.pos = r3
            return r0
        L_0x0011:
            int r1 = r1 - r3
            r4 = 9
            if (r1 < r4) goto L_0x006b
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 7
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x0022
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
            goto L_0x0068
        L_0x0022:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x002f
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
        L_0x002d:
            r1 = r3
            goto L_0x0068
        L_0x002f:
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 21
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x003d
            r2 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r2
            goto L_0x0068
        L_0x003d:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r4 = r1 << 28
            r0 = r0 ^ r4
            r4 = 266354560(0xfe03f80, float:2.2112565E-29)
            r0 = r0 ^ r4
            if (r1 >= 0) goto L_0x002d
            int r1 = r3 + 1
            byte r3 = r2[r3]
            if (r3 >= 0) goto L_0x0068
            int r3 = r1 + 1
            byte r1 = r2[r1]
            if (r1 >= 0) goto L_0x002d
            int r1 = r3 + 1
            byte r3 = r2[r3]
            if (r3 >= 0) goto L_0x0068
            int r3 = r1 + 1
            byte r1 = r2[r1]
            if (r1 >= 0) goto L_0x002d
            int r1 = r3 + 1
            byte r2 = r2[r3]
            if (r2 < 0) goto L_0x006b
        L_0x0068:
            r5.pos = r1
            return r0
        L_0x006b:
            long r0 = r5.zzayr()
            int r1 = (int) r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqy.zzayu():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b0, code lost:
        if (((long) r2[r0]) >= 0) goto L_0x00b4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final long zzayv() throws java.io.IOException {
        /*
            r11 = this;
            int r0 = r11.pos
            int r1 = r11.limit
            if (r1 == r0) goto L_0x00b8
            byte[] r2 = r11.buffer
            int r3 = r0 + 1
            byte r0 = r2[r0]
            if (r0 < 0) goto L_0x0012
            r11.pos = r3
            long r0 = (long) r0
            return r0
        L_0x0012:
            int r1 = r1 - r3
            r4 = 9
            if (r1 < r4) goto L_0x00b8
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 7
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x0025
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
        L_0x0022:
            long r2 = (long) r0
            goto L_0x00b5
        L_0x0025:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0036
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            long r0 = (long) r0
            r9 = r0
            r1 = r3
            r2 = r9
            goto L_0x00b5
        L_0x0036:
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 21
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x0044
            r2 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r2
            goto L_0x0022
        L_0x0044:
            long r3 = (long) r0
            int r0 = r1 + 1
            byte r1 = r2[r1]
            long r5 = (long) r1
            r1 = 28
            long r5 = r5 << r1
            long r3 = r3 ^ r5
            r5 = 0
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 < 0) goto L_0x005b
            r1 = 266354560(0xfe03f80, double:1.315966377E-315)
        L_0x0057:
            long r2 = r3 ^ r1
            r1 = r0
            goto L_0x00b5
        L_0x005b:
            int r1 = r0 + 1
            byte r0 = r2[r0]
            long r7 = (long) r0
            r0 = 35
            long r7 = r7 << r0
            long r3 = r3 ^ r7
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0070
            r5 = -34093383808(0xfffffff80fe03f80, double:NaN)
        L_0x006d:
            long r2 = r3 ^ r5
            goto L_0x00b5
        L_0x0070:
            int r0 = r1 + 1
            byte r1 = r2[r1]
            long r7 = (long) r1
            r1 = 42
            long r7 = r7 << r1
            long r3 = r3 ^ r7
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 < 0) goto L_0x0083
            r1 = 4363953127296(0x3f80fe03f80, double:2.1560793202584E-311)
            goto L_0x0057
        L_0x0083:
            int r1 = r0 + 1
            byte r0 = r2[r0]
            long r7 = (long) r0
            r0 = 49
            long r7 = r7 << r0
            long r3 = r3 ^ r7
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0096
            r5 = -558586000294016(0xfffe03f80fe03f80, double:NaN)
            goto L_0x006d
        L_0x0096:
            int r0 = r1 + 1
            byte r1 = r2[r1]
            long r7 = (long) r1
            r1 = 56
            long r7 = r7 << r1
            long r3 = r3 ^ r7
            r7 = 71499008037633920(0xfe03f80fe03f80, double:6.838959413692434E-304)
            long r3 = r3 ^ r7
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 >= 0) goto L_0x00b3
            int r1 = r0 + 1
            byte r0 = r2[r0]
            long r7 = (long) r0
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x00b8
            goto L_0x00b4
        L_0x00b3:
            r1 = r0
        L_0x00b4:
            r2 = r3
        L_0x00b5:
            r11.pos = r1
            return r2
        L_0x00b8:
            long r0 = r11.zzayr()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqy.zzayv():long");
    }

    private final int zzayw() throws IOException {
        int i = this.pos;
        if (this.limit - i >= 4) {
            byte[] bArr = this.buffer;
            this.pos = i + 4;
            return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
        }
        throw zzdse.zzbaj();
    }

    private final long zzayx() throws IOException {
        int i = this.pos;
        if (this.limit - i >= 8) {
            byte[] bArr = this.buffer;
            this.pos = i + 8;
            return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
        }
        throw zzdse.zzbaj();
    }

    private final void zzayy() {
        this.limit += this.zzhik;
        int i = this.limit;
        int i2 = i - this.zzhiq;
        int i3 = this.zzhin;
        if (i2 > i3) {
            this.zzhik = i2 - i3;
            this.limit = i - this.zzhik;
            return;
        }
        this.zzhik = 0;
    }

    private final byte zzayz() throws IOException {
        int i = this.pos;
        if (i != this.limit) {
            byte[] bArr = this.buffer;
            this.pos = i + 1;
            return bArr[i];
        }
        throw zzdse.zzbaj();
    }

    private final void zzfq(int i) throws IOException {
        if (i >= 0) {
            int i2 = this.limit;
            int i3 = this.pos;
            if (i <= i2 - i3) {
                this.pos = i3 + i;
                return;
            }
        }
        if (i < 0) {
            throw zzdse.zzbak();
        }
        throw zzdse.zzbaj();
    }

    public final double readDouble() throws IOException {
        return Double.longBitsToDouble(zzayx());
    }

    public final float readFloat() throws IOException {
        return Float.intBitsToFloat(zzayw());
    }

    public final String readString() throws IOException {
        int zzayu = zzayu();
        if (zzayu > 0) {
            int i = this.limit;
            int i2 = this.pos;
            if (zzayu <= i - i2) {
                String str = new String(this.buffer, i2, zzayu, zzdrv.UTF_8);
                this.pos += zzayu;
                return str;
            }
        }
        if (zzayu == 0) {
            return "";
        }
        if (zzayu < 0) {
            throw zzdse.zzbak();
        }
        throw zzdse.zzbaj();
    }

    public final int zzayc() throws IOException {
        if (zzays()) {
            this.zzhil = 0;
            return 0;
        }
        this.zzhil = zzayu();
        int i = this.zzhil;
        if ((i >>> 3) != 0) {
            return i;
        }
        throw zzdse.zzbam();
    }

    public final long zzayd() throws IOException {
        return zzayv();
    }

    public final long zzaye() throws IOException {
        return zzayv();
    }

    public final int zzayf() throws IOException {
        return zzayu();
    }

    public final long zzayg() throws IOException {
        return zzayx();
    }

    public final int zzayh() throws IOException {
        return zzayw();
    }

    public final boolean zzayi() throws IOException {
        return zzayv() != 0;
    }

    public final String zzayj() throws IOException {
        int zzayu = zzayu();
        if (zzayu > 0) {
            int i = this.limit;
            int i2 = this.pos;
            if (zzayu <= i - i2) {
                String zzo = zzdva.zzo(this.buffer, i2, zzayu);
                this.pos += zzayu;
                return zzo;
            }
        }
        if (zzayu == 0) {
            return "";
        }
        if (zzayu <= 0) {
            throw zzdse.zzbak();
        }
        throw zzdse.zzbaj();
    }

    public final zzdqk zzayk() throws IOException {
        byte[] bArr;
        int zzayu = zzayu();
        if (zzayu > 0) {
            int i = this.limit;
            int i2 = this.pos;
            if (zzayu <= i - i2) {
                zzdqk zzi = zzdqk.zzi(this.buffer, i2, zzayu);
                this.pos += zzayu;
                return zzi;
            }
        }
        if (zzayu == 0) {
            return zzdqk.zzhhx;
        }
        if (zzayu > 0) {
            int i3 = this.limit;
            int i4 = this.pos;
            if (zzayu <= i3 - i4) {
                this.pos = zzayu + i4;
                bArr = Arrays.copyOfRange(this.buffer, i4, this.pos);
                return zzdqk.zzv(bArr);
            }
        }
        if (zzayu > 0) {
            throw zzdse.zzbaj();
        } else if (zzayu == 0) {
            bArr = zzdrv.zzhng;
            return zzdqk.zzv(bArr);
        } else {
            throw zzdse.zzbak();
        }
    }

    public final int zzayl() throws IOException {
        return zzayu();
    }

    public final int zzaym() throws IOException {
        return zzayu();
    }

    public final int zzayn() throws IOException {
        return zzayw();
    }

    public final long zzayo() throws IOException {
        return zzayx();
    }

    public final int zzayp() throws IOException {
        return zzdqw.zzfl(zzayu());
    }

    public final long zzayq() throws IOException {
        return zzdqw.zzff(zzayv());
    }

    /* access modifiers changed from: package-private */
    public final long zzayr() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte zzayz = zzayz();
            j |= ((long) (zzayz & Byte.MAX_VALUE)) << i;
            if ((zzayz & 128) == 0) {
                return j;
            }
        }
        throw zzdse.zzbal();
    }

    public final boolean zzays() throws IOException {
        return this.pos == this.limit;
    }

    public final int zzayt() {
        return this.pos - this.zzhiq;
    }

    public final void zzfh(int i) throws zzdse {
        if (this.zzhil != i) {
            throw zzdse.zzban();
        }
    }

    public final boolean zzfi(int i) throws IOException {
        int zzayc;
        int i2 = i & 7;
        int i3 = 0;
        if (i2 == 0) {
            if (this.limit - this.pos >= 10) {
                while (i3 < 10) {
                    byte[] bArr = this.buffer;
                    int i4 = this.pos;
                    this.pos = i4 + 1;
                    if (bArr[i4] < 0) {
                        i3++;
                    }
                }
                throw zzdse.zzbal();
            }
            while (i3 < 10) {
                if (zzayz() < 0) {
                    i3++;
                }
            }
            throw zzdse.zzbal();
            return true;
        } else if (i2 == 1) {
            zzfq(8);
            return true;
        } else if (i2 == 2) {
            zzfq(zzayu());
            return true;
        } else if (i2 == 3) {
            do {
                zzayc = zzayc();
                if (zzayc == 0 || !zzfi(zzayc)) {
                    zzfh(((i >>> 3) << 3) | 4);
                }
                zzayc = zzayc();
                break;
            } while (!zzfi(zzayc));
            zzfh(((i >>> 3) << 3) | 4);
            return true;
        } else if (i2 == 4) {
            return false;
        } else {
            if (i2 == 5) {
                zzfq(4);
                return true;
            }
            throw zzdse.zzbao();
        }
    }

    public final int zzfj(int i) throws zzdse {
        if (i >= 0) {
            int zzayt = i + zzayt();
            int i2 = this.zzhin;
            if (zzayt <= i2) {
                this.zzhin = zzayt;
                zzayy();
                return i2;
            }
            throw zzdse.zzbaj();
        }
        throw zzdse.zzbak();
    }

    public final void zzfk(int i) {
        this.zzhin = i;
        zzayy();
    }
}
