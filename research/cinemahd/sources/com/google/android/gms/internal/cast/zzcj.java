package com.google.android.gms.internal.cast;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;

public abstract class zzcj<R extends Result> extends zzcv<R> {
    protected zzec zzwq;

    public zzcj(zzcb zzcb) {
        super(zzcb.zzpb);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        execute();
    }

    public abstract void execute();
}
