package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzanb extends zzgb implements zzanc {
    public zzanb() {
        super("com.google.android.gms.ads.internal.mediation.client.rtb.INativeCallback");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            zza(zzalu.zzac(parcel.readStrongBinder()));
        } else if (i != 2) {
            return false;
        } else {
            zzdl(parcel.readString());
        }
        parcel2.writeNoException();
        return true;
    }
}
