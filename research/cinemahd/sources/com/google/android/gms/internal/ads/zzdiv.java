package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdit;
import java.security.GeneralSecurityException;
import java.util.Set;

final class zzdiv implements zzdit.zza {
    private final /* synthetic */ zzdii zzgyn;

    zzdiv(zzdii zzdii) {
        this.zzgyn = zzdii;
    }

    public final Set<Class<?>> zzase() {
        return this.zzgyn.zzase();
    }

    public final zzdid<?> zzasn() {
        zzdii zzdii = this.zzgyn;
        return new zzdig(zzdii, zzdii.zzasf());
    }

    public final Class<?> zzaso() {
        return this.zzgyn.getClass();
    }

    public final Class<?> zzasp() {
        return null;
    }

    public final <Q> zzdid<Q> zzb(Class<Q> cls) throws GeneralSecurityException {
        try {
            return new zzdig(this.zzgyn, cls);
        } catch (IllegalArgumentException e) {
            throw new GeneralSecurityException("Primitive type not supported", e);
        }
    }
}
