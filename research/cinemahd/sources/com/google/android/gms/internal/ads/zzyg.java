package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzyg implements Runnable {
    private final /* synthetic */ zzyd zzcfm;

    zzyg(zzyd zzyd) {
        this.zzcfm = zzyd;
    }

    public final void run() {
        if (this.zzcfm.zzblq != null) {
            try {
                this.zzcfm.zzblq.onAdFailedToLoad(1);
            } catch (RemoteException e) {
                zzayu.zzd("Could not notify onAdFailedToLoad event.", e);
            }
        }
    }
}
