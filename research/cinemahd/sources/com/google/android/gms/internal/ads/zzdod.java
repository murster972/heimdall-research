package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public final class zzdod implements zzdio {
    private final SecretKey zzhev;
    private final int zzhew;
    private byte[] zzhex;
    private byte[] zzhey;

    public zzdod(byte[] bArr, int i) throws GeneralSecurityException {
        zzdpo.zzez(bArr.length);
        if (i < 10) {
            throw new InvalidAlgorithmParameterException("tag size too small, min is 10 bytes");
        } else if (i <= 16) {
            this.zzhev = new SecretKeySpec(bArr, "AES");
            this.zzhew = i;
            Cipher zzaxa = zzaxa();
            zzaxa.init(1, this.zzhev);
            this.zzhex = zzdoj.zzq(zzaxa.doFinal(new byte[16]));
            this.zzhey = zzdoj.zzq(this.zzhex);
        } else {
            throw new InvalidAlgorithmParameterException("tag size too large, max is 16 bytes");
        }
    }

    private static Cipher zzaxa() throws GeneralSecurityException {
        return zzdoy.zzhgg.zzhd("AES/ECB/NoPadding");
    }

    public final byte[] zzl(byte[] bArr) throws GeneralSecurityException {
        byte[] bArr2;
        Cipher zzaxa = zzaxa();
        zzaxa.init(1, this.zzhev);
        int max = Math.max(1, (int) Math.ceil(((double) bArr.length) / 16.0d));
        if ((max << 4) == bArr.length) {
            bArr2 = zzdoi.zza(bArr, (max - 1) << 4, this.zzhex, 0, 16);
        } else {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, (max - 1) << 4, bArr.length);
            if (copyOfRange.length < 16) {
                byte[] copyOf = Arrays.copyOf(copyOfRange, 16);
                copyOf[copyOfRange.length] = Byte.MIN_VALUE;
                bArr2 = zzdoi.zzd(copyOf, this.zzhey);
            } else {
                throw new IllegalArgumentException("x must be smaller than a block.");
            }
        }
        byte[] bArr3 = new byte[16];
        for (int i = 0; i < max - 1; i++) {
            bArr3 = zzaxa.doFinal(zzdoi.zza(bArr3, 0, bArr, i << 4, 16));
        }
        byte[] zzd = zzdoi.zzd(bArr2, bArr3);
        byte[] bArr4 = new byte[this.zzhew];
        System.arraycopy(zzaxa.doFinal(zzd), 0, bArr4, 0, this.zzhew);
        return bArr4;
    }
}
