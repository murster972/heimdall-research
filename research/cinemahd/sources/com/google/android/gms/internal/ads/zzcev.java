package com.google.android.gms.internal.ads;

import java.util.List;

final /* synthetic */ class zzcev implements Runnable {
    private final String zzefb;
    private final zzceq zzftj;
    private final zzdac zzftw;
    private final zzagp zzftx;
    private final List zzfty;

    zzcev(zzceq zzceq, zzdac zzdac, zzagp zzagp, List list, String str) {
        this.zzftj = zzceq;
        this.zzftw = zzdac;
        this.zzftx = zzagp;
        this.zzfty = list;
        this.zzefb = str;
    }

    public final void run() {
        this.zzftj.zza(this.zzftw, this.zzftx, this.zzfty, this.zzefb);
    }
}
