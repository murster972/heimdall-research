package com.google.android.gms.internal.ads;

import java.util.HashMap;
import java.util.Map;

public final class zzaab {
    private final Map<String, zzaac> zzcru = new HashMap();
    private final zzaae zzcrv;

    public zzaab(zzaae zzaae) {
        this.zzcrv = zzaae;
    }

    public final void zza(String str, zzaac zzaac) {
        this.zzcru.put(str, zzaac);
    }

    public final zzaae zzqp() {
        return this.zzcrv;
    }

    public final void zza(String str, String str2, long j) {
        zzaac zzaac;
        zzaae zzaae = this.zzcrv;
        zzaac zzaac2 = this.zzcru.get(str2);
        String[] strArr = {str};
        if (!(zzaae == null || zzaac2 == null)) {
            zzaae.zza(zzaac2, j, strArr);
        }
        Map<String, zzaac> map = this.zzcru;
        zzaae zzaae2 = this.zzcrv;
        if (zzaae2 == null) {
            zzaac = null;
        } else {
            zzaac = zzaae2.zzex(j);
        }
        map.put(str, zzaac);
    }
}
