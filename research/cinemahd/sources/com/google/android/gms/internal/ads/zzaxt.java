package com.google.android.gms.internal.ads;

final class zzaxt implements zzy {
    private final /* synthetic */ zzazl zzduo;

    zzaxt(zzazl zzazl) {
        this.zzduo = zzazl;
    }

    public final void zzc(zzae zzae) {
        this.zzduo.setException(zzae);
    }
}
