package com.google.android.gms.internal.cast;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.ImagePicker;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzav extends UIController {
    private final ImagePicker zzlp;
    private final zzaa zzma;
    private final ImageHints zzmb;
    /* access modifiers changed from: private */
    public final ImageView zzsi;
    private final Bitmap zzsj;

    public zzav(ImageView imageView, Context context, ImageHints imageHints, int i) {
        this.zzsi = imageView;
        this.zzmb = imageHints;
        this.zzsj = BitmapFactory.decodeResource(context.getResources(), i);
        CastContext b = CastContext.b(context);
        ImagePicker imagePicker = null;
        if (b != null) {
            CastMediaOptions s = b.a().s();
            this.zzlp = s != null ? s.t() : imagePicker;
        } else {
            this.zzlp = null;
        }
        this.zzma = new zzaa(context.getApplicationContext());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0020, code lost:
        r1 = r1.a(r0.y(), r4.zzmb);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzdk() {
        /*
            r4 = this;
            com.google.android.gms.cast.framework.media.RemoteMediaClient r0 = r4.getRemoteMediaClient()
            if (r0 == 0) goto L_0x004c
            boolean r1 = r0.k()
            if (r1 != 0) goto L_0x000d
            goto L_0x004c
        L_0x000d:
            com.google.android.gms.cast.MediaQueueItem r0 = r0.i()
            r1 = 0
            if (r0 != 0) goto L_0x0015
            goto L_0x003c
        L_0x0015:
            com.google.android.gms.cast.MediaInfo r0 = r0.v()
            if (r0 != 0) goto L_0x001c
            goto L_0x003c
        L_0x001c:
            com.google.android.gms.cast.framework.media.ImagePicker r1 = r4.zzlp
            if (r1 == 0) goto L_0x0037
            com.google.android.gms.cast.MediaMetadata r2 = r0.y()
            com.google.android.gms.cast.framework.media.ImageHints r3 = r4.zzmb
            com.google.android.gms.common.images.WebImage r1 = r1.a((com.google.android.gms.cast.MediaMetadata) r2, (com.google.android.gms.cast.framework.media.ImageHints) r3)
            if (r1 == 0) goto L_0x0037
            android.net.Uri r2 = r1.t()
            if (r2 == 0) goto L_0x0037
            android.net.Uri r1 = r1.t()
            goto L_0x003c
        L_0x0037:
            r1 = 0
            android.net.Uri r1 = com.google.android.gms.cast.framework.media.MediaUtils.a(r0, r1)
        L_0x003c:
            if (r1 != 0) goto L_0x0046
            android.widget.ImageView r0 = r4.zzsi
            android.graphics.Bitmap r1 = r4.zzsj
            r0.setImageBitmap(r1)
            return
        L_0x0046:
            com.google.android.gms.internal.cast.zzaa r0 = r4.zzma
            r0.zza((android.net.Uri) r1)
            return
        L_0x004c:
            android.widget.ImageView r0 = r4.zzsi
            android.graphics.Bitmap r1 = r4.zzsj
            r0.setImageBitmap(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzav.zzdk():void");
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        this.zzma.zza((zzab) new zzaw(this));
        this.zzsi.setImageBitmap(this.zzsj);
        zzdk();
    }

    public final void onSessionEnded() {
        this.zzma.clear();
        this.zzsi.setImageBitmap(this.zzsj);
        super.onSessionEnded();
    }
}
