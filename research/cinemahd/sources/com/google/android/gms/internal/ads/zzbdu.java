package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.android.gms.ads.impl.R;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.Predicate;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public final class zzbdu extends FrameLayout implements zzbdi {
    /* access modifiers changed from: private */
    public final zzbdi zzefo;
    private final zzbao zzefp;
    private final AtomicBoolean zzefq = new AtomicBoolean();

    public zzbdu(zzbdi zzbdi) {
        super(zzbdi.getContext());
        this.zzefo = zzbdi;
        this.zzefp = new zzbao(zzbdi.zzzv(), this, this);
        if (!zzaar()) {
            addView(this.zzefo.getView());
        }
    }

    public final void destroy() {
        IObjectWrapper zzaae = zzaae();
        if (zzaae != null) {
            zzawb.zzdsr.post(new zzbdx(zzaae));
            zzawb.zzdsr.postDelayed(new zzbdw(this), (long) ((Integer) zzve.zzoy().zzd(zzzn.zzcod)).intValue());
            return;
        }
        this.zzefo.destroy();
    }

    public final View getView() {
        return this;
    }

    public final WebView getWebView() {
        return this.zzefo.getWebView();
    }

    public final boolean isDestroyed() {
        return this.zzefo.isDestroyed();
    }

    public final void loadData(String str, String str2, String str3) {
        this.zzefo.loadData(str, str2, str3);
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        this.zzefo.loadDataWithBaseURL(str, str2, str3, str4, str5);
    }

    public final void loadUrl(String str) {
        this.zzefo.loadUrl(str);
    }

    public final void onPause() {
        this.zzefp.onPause();
        this.zzefo.onPause();
    }

    public final void onResume() {
        this.zzefo.onResume();
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.zzefo.setOnClickListener(onClickListener);
    }

    public final void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.zzefo.setOnTouchListener(onTouchListener);
    }

    public final void setRequestedOrientation(int i) {
        this.zzefo.setRequestedOrientation(i);
    }

    public final void setWebChromeClient(WebChromeClient webChromeClient) {
        this.zzefo.setWebChromeClient(webChromeClient);
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        this.zzefo.setWebViewClient(webViewClient);
    }

    public final void zza(String str, Map<String, ?> map) {
        this.zzefo.zza(str, map);
    }

    public final zzbev zzaaa() {
        return this.zzefo.zzaaa();
    }

    public final WebViewClient zzaab() {
        return this.zzefo.zzaab();
    }

    public final boolean zzaac() {
        return this.zzefo.zzaac();
    }

    public final zzdq zzaad() {
        return this.zzefo.zzaad();
    }

    public final IObjectWrapper zzaae() {
        return this.zzefo.zzaae();
    }

    public final boolean zzaaf() {
        return this.zzefo.zzaaf();
    }

    public final void zzaag() {
        this.zzefp.onDestroy();
        this.zzefo.zzaag();
    }

    public final boolean zzaah() {
        return this.zzefo.zzaah();
    }

    public final boolean zzaai() {
        return this.zzefo.zzaai();
    }

    public final void zzaaj() {
        this.zzefo.zzaaj();
    }

    public final void zzaak() {
        this.zzefo.zzaak();
    }

    public final zzabw zzaal() {
        return this.zzefo.zzaal();
    }

    public final void zzaam() {
        setBackgroundColor(0);
        this.zzefo.setBackgroundColor(0);
    }

    public final void zzaan() {
        TextView textView = new TextView(getContext());
        Resources resources = zzq.zzku().getResources();
        textView.setText(resources != null ? resources.getString(R.string.s7) : "Test Ad");
        textView.setTextSize(15.0f);
        textView.setTextColor(-1);
        textView.setPadding(5, 0, 5, 0);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(0);
        gradientDrawable.setColor(-12303292);
        gradientDrawable.setCornerRadius(8.0f);
        if (Build.VERSION.SDK_INT >= 16) {
            textView.setBackground(gradientDrawable);
        } else {
            textView.setBackgroundDrawable(gradientDrawable);
        }
        addView(textView, new FrameLayout.LayoutParams(-2, -2, 49));
        bringChildToFront(textView);
    }

    public final zzra zzaao() {
        return this.zzefo.zzaao();
    }

    public final boolean zzaap() {
        return this.zzefq.get();
    }

    public final zzro zzaaq() {
        return this.zzefo.zzaaq();
    }

    public final boolean zzaar() {
        return this.zzefo.zzaar();
    }

    public final void zzal(boolean z) {
        this.zzefo.zzal(z);
    }

    public final void zzan(IObjectWrapper iObjectWrapper) {
        this.zzefo.zzan(iObjectWrapper);
    }

    public final void zzav(boolean z) {
        this.zzefo.zzav(z);
    }

    public final void zzax(boolean z) {
        this.zzefo.zzax(z);
    }

    public final void zzay(boolean z) {
        this.zzefo.zzay(z);
    }

    public final void zzaz(boolean z) {
        this.zzefo.zzaz(z);
    }

    public final void zzb(String str, JSONObject jSONObject) {
        this.zzefo.zzb(str, jSONObject);
    }

    public final void zzba(boolean z) {
        this.zzefo.zzba(z);
    }

    public final void zzbr(Context context) {
        this.zzefo.zzbr(context);
    }

    public final void zzc(boolean z, int i) {
        this.zzefo.zzc(z, i);
    }

    public final void zzcy(String str) {
        this.zzefo.zzcy(str);
    }

    public final void zzde(int i) {
        this.zzefo.zzde(i);
    }

    public final zzbcn zzfe(String str) {
        return this.zzefo.zzfe(str);
    }

    public final void zzjv() {
        this.zzefo.zzjv();
    }

    public final void zzjw() {
        this.zzefo.zzjw();
    }

    public final void zztr() {
        this.zzefo.zztr();
    }

    public final void zzts() {
        this.zzefo.zzts();
    }

    public final zzbao zzyk() {
        return this.zzefp;
    }

    public final zzbed zzyl() {
        return this.zzefo.zzyl();
    }

    public final zzaac zzym() {
        return this.zzefo.zzym();
    }

    public final Activity zzyn() {
        return this.zzefo.zzyn();
    }

    public final zza zzyo() {
        return this.zzefo.zzyo();
    }

    public final String zzyp() {
        return this.zzefo.zzyp();
    }

    public final zzaab zzyq() {
        return this.zzefo.zzyq();
    }

    public final zzazb zzyr() {
        return this.zzefo.zzyr();
    }

    public final int zzys() {
        return getMeasuredHeight();
    }

    public final int zzyt() {
        return getMeasuredWidth();
    }

    public final void zzyu() {
        this.zzefo.zzyu();
    }

    public final void zzzt() {
        this.zzefo.zzzt();
    }

    public final void zzzu() {
        this.zzefo.zzzu();
    }

    public final Context zzzv() {
        return this.zzefo.zzzv();
    }

    public final zzc zzzw() {
        return this.zzefo.zzzw();
    }

    public final zzc zzzx() {
        return this.zzefo.zzzx();
    }

    public final zzbey zzzy() {
        return this.zzefo.zzzy();
    }

    public final String zzzz() {
        return this.zzefo.zzzz();
    }

    public final void zza(String str, zzafn<? super zzbdi> zzafn) {
        this.zzefo.zza(str, zzafn);
    }

    public final void zzb(String str, zzafn<? super zzbdi> zzafn) {
        this.zzefo.zzb(str, zzafn);
    }

    public final void zza(String str, Predicate<zzafn<? super zzbdi>> predicate) {
        this.zzefo.zza(str, predicate);
    }

    public final void zzb(zzc zzc) {
        this.zzefo.zzb(zzc);
    }

    public final void zza(boolean z, long j) {
        this.zzefo.zza(z, j);
    }

    public final void zzb(String str, String str2, String str3) {
        this.zzefo.zzb(str, str2, str3);
    }

    public final void zza(String str, JSONObject jSONObject) {
        this.zzefo.zza(str, jSONObject);
    }

    public final boolean zzb(boolean z, int i) {
        if (!this.zzefq.compareAndSet(false, true)) {
            return true;
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcis)).booleanValue()) {
            return false;
        }
        if (this.zzefo.getParent() instanceof ViewGroup) {
            ((ViewGroup) this.zzefo.getParent()).removeView(this.zzefo.getView());
        }
        return this.zzefo.zzb(z, i);
    }

    public final void zza(zzc zzc) {
        this.zzefo.zza(zzc);
    }

    public final void zza(zzbey zzbey) {
        this.zzefo.zza(zzbey);
    }

    public final void zza(String str, zzbcn zzbcn) {
        this.zzefo.zza(str, zzbcn);
    }

    public final void zza(zzabr zzabr) {
        this.zzefo.zza(zzabr);
    }

    public final void zza(zzra zzra) {
        this.zzefo.zza(zzra);
    }

    public final void zza(zzpt zzpt) {
        this.zzefo.zza(zzpt);
    }

    public final void zza(zzabw zzabw) {
        this.zzefo.zza(zzabw);
    }

    public final void zza(zzbed zzbed) {
        this.zzefo.zza(zzbed);
    }

    public final void zza(zzd zzd) {
        this.zzefo.zza(zzd);
    }

    public final void zza(boolean z, int i, String str) {
        this.zzefo.zza(z, i, str);
    }

    public final void zza(boolean z, int i, String str, String str2) {
        this.zzefo.zza(z, i, str, str2);
    }

    public final void zza(ViewGroup viewGroup, Activity activity, String str, String str2) {
        this.zzefo.zza(this, activity, str, str2);
    }
}
