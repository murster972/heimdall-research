package com.google.android.gms.internal.ads;

import android.location.Location;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.uwetrottmann.trakt5.TraktV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import okhttp3.internal.cache.DiskLruCache;

public final class zzamd implements NativeMediationAdRequest {
    private final String zzabq;
    private final int zzcbz;
    private final boolean zzcck;
    private final int zzddh;
    private final int zzddi;
    private final zzaby zzddz;
    private final List<String> zzdea = new ArrayList();
    private final Map<String, Boolean> zzdeb = new HashMap();
    private final Date zzme;
    private final Set<String> zzmg;
    private final boolean zzmh;
    private final Location zzmi;

    public zzamd(Date date, int i, Set<String> set, Location location, boolean z, int i2, zzaby zzaby, List<String> list, boolean z2, int i3, String str) {
        this.zzme = date;
        this.zzcbz = i;
        this.zzmg = set;
        this.zzmi = location;
        this.zzmh = z;
        this.zzddh = i2;
        this.zzddz = zzaby;
        this.zzcck = z2;
        this.zzddi = i3;
        this.zzabq = str;
        if (list != null) {
            for (String next : list) {
                if (next.startsWith("custom:")) {
                    String[] split = next.split(":", 3);
                    if (split.length == 3) {
                        if ("true".equals(split[2])) {
                            this.zzdeb.put(split[1], true);
                        } else if ("false".equals(split[2])) {
                            this.zzdeb.put(split[1], false);
                        }
                    }
                } else {
                    this.zzdea.add(next);
                }
            }
        }
    }

    public final float getAdVolume() {
        return zzxq.zzpw().zzpe();
    }

    @Deprecated
    public final Date getBirthday() {
        return this.zzme;
    }

    @Deprecated
    public final int getGender() {
        return this.zzcbz;
    }

    public final Set<String> getKeywords() {
        return this.zzmg;
    }

    public final Location getLocation() {
        return this.zzmi;
    }

    public final NativeAdOptions getNativeAdOptions() {
        zzyw zzyw;
        if (this.zzddz == null) {
            return null;
        }
        NativeAdOptions.Builder requestMultipleImages = new NativeAdOptions.Builder().setReturnUrlsForImageAssets(this.zzddz.zzcvo).setImageOrientation(this.zzddz.zzbjw).setRequestMultipleImages(this.zzddz.zzbjy);
        zzaby zzaby = this.zzddz;
        if (zzaby.versionCode >= 2) {
            requestMultipleImages.setAdChoicesPlacement(zzaby.zzbjz);
        }
        zzaby zzaby2 = this.zzddz;
        if (zzaby2.versionCode >= 3 && (zzyw = zzaby2.zzcvp) != null) {
            requestMultipleImages.setVideoOptions(new VideoOptions(zzyw));
        }
        return requestMultipleImages.build();
    }

    public final boolean isAdMuted() {
        return zzxq.zzpw().zzpf();
    }

    public final boolean isAppInstallAdRequested() {
        List<String> list = this.zzdea;
        if (list != null) {
            return list.contains(TraktV2.API_VERSION) || this.zzdea.contains("6");
        }
        return false;
    }

    public final boolean isContentAdRequested() {
        List<String> list = this.zzdea;
        if (list != null) {
            return list.contains(DiskLruCache.VERSION_1) || this.zzdea.contains("6");
        }
        return false;
    }

    @Deprecated
    public final boolean isDesignedForFamilies() {
        return this.zzcck;
    }

    public final boolean isTesting() {
        return this.zzmh;
    }

    public final boolean isUnifiedNativeAdRequested() {
        List<String> list = this.zzdea;
        return list != null && list.contains("6");
    }

    public final int taggedForChildDirectedTreatment() {
        return this.zzddh;
    }

    public final boolean zzsz() {
        List<String> list = this.zzdea;
        return list != null && list.contains("3");
    }

    public final Map<String, Boolean> zzta() {
        return this.zzdeb;
    }
}
