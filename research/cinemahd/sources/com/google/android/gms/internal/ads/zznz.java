package com.google.android.gms.internal.ads;

public interface zznz<S> {
    void zza(S s, zznq zznq);

    void zzc(S s, int i);

    void zze(S s);
}
