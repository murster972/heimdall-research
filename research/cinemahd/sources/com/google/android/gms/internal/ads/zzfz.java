package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzfz extends zzdrt<zzfz, zza> implements zzdtg {
    /* access modifiers changed from: private */
    public static final zzfz zzaat;
    private static volatile zzdtn<zzfz> zzdz;
    private String zzaao = "";
    private String zzaap = "";
    private long zzaaq;
    private long zzaar;
    private long zzaas;
    private int zzdl;

    public static final class zza extends zzdrt.zzb<zzfz, zza> implements zzdtg {
        private zza() {
            super(zzfz.zzaat);
        }

        public final zza zzay(String str) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzfz) this.zzhmp).zzaw(str);
            return this;
        }

        public final zza zzaz(String str) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzfz) this.zzhmp).zzax(str);
            return this;
        }

        public final zza zzdj(long j) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzfz) this.zzhmp).zzdg(j);
            return this;
        }

        public final zza zzdk(long j) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzfz) this.zzhmp).zzdh(j);
            return this;
        }

        public final zza zzdl(long j) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzfz) this.zzhmp).zzdi(j);
            return this;
        }

        /* synthetic */ zza(zzga zzga) {
            this();
        }
    }

    static {
        zzfz zzfz = new zzfz();
        zzaat = zzfz;
        zzdrt.zza(zzfz.class, zzfz);
    }

    private zzfz() {
    }

    /* access modifiers changed from: private */
    public final void zzaw(String str) {
        str.getClass();
        this.zzdl |= 1;
        this.zzaao = str;
    }

    /* access modifiers changed from: private */
    public final void zzax(String str) {
        str.getClass();
        this.zzdl |= 2;
        this.zzaap = str;
    }

    public static zza zzdc() {
        return (zza) zzaat.zzazt();
    }

    public static zzfz zzdd() {
        return zzaat;
    }

    /* access modifiers changed from: private */
    public final void zzdg(long j) {
        this.zzdl |= 4;
        this.zzaaq = j;
    }

    /* access modifiers changed from: private */
    public final void zzdh(long j) {
        this.zzdl |= 8;
        this.zzaar = j;
    }

    /* access modifiers changed from: private */
    public final void zzdi(long j) {
        this.zzdl |= 16;
        this.zzaas = j;
    }

    public static zzfz zzl(zzdqk zzdqk) throws zzdse {
        return (zzfz) zzdrt.zza(zzaat, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzga.zzdk[i - 1]) {
            case 1:
                return new zzfz();
            case 2:
                return new zza((zzga) null);
            case 3:
                return zzdrt.zza((zzdte) zzaat, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\u0003\u0002\u0004\u0003\u0003\u0005\u0003\u0004", new Object[]{"zzdl", "zzaao", "zzaap", "zzaaq", "zzaar", "zzaas"});
            case 4:
                return zzaat;
            case 5:
                zzdtn<zzfz> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzfz.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzaat);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final String zzcx() {
        return this.zzaao;
    }

    public final String zzcy() {
        return this.zzaap;
    }

    public final long zzcz() {
        return this.zzaaq;
    }

    public final long zzda() {
        return this.zzaar;
    }

    public final long zzdb() {
        return this.zzaas;
    }
}
