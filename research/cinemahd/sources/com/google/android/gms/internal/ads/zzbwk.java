package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.Executor;

public final class zzbwk extends zzbmd {
    private final zzazb zzbll;
    private final zzdq zzefv;
    private final Executor zzfci;
    private final zzbwz zzfea;
    private final zzats zzffo;
    private final zzbws zzfkc;
    private final zzbxa zzflg;
    private final zzbxj zzflh;
    private final zzbww zzfli;
    private final zzdxa<zzcab> zzflj;
    private final zzdxa<zzbzz> zzflk;
    private final zzdxa<zzcai> zzfll;
    private final zzdxa<zzbzv> zzflm;
    private final zzdxa<zzcad> zzfln;
    private zzbxz zzflo;
    private boolean zzflp;
    private final zzbwq zzflq;
    private final Context zzup;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzbwk(zzbmg zzbmg, Executor executor, zzbws zzbws, zzbxa zzbxa, zzbxj zzbxj, zzbww zzbww, zzbwz zzbwz, zzdxa<zzcab> zzdxa, zzdxa<zzbzz> zzdxa2, zzdxa<zzcai> zzdxa3, zzdxa<zzbzv> zzdxa4, zzdxa<zzcad> zzdxa5, zzats zzats, zzdq zzdq, zzazb zzazb, Context context, zzbwq zzbwq) {
        super(zzbmg);
        this.zzfci = executor;
        this.zzfkc = zzbws;
        this.zzflg = zzbxa;
        this.zzflh = zzbxj;
        this.zzfli = zzbww;
        this.zzfea = zzbwz;
        this.zzflj = zzdxa;
        this.zzflk = zzdxa2;
        this.zzfll = zzdxa3;
        this.zzflm = zzdxa4;
        this.zzfln = zzdxa5;
        this.zzffo = zzats;
        this.zzefv = zzdq;
        this.zzbll = zzazb;
        this.zzup = context;
        this.zzflq = zzbwq;
    }

    public static boolean zzy(View view) {
        return view.isShown() && view.getGlobalVisibleRect(new Rect(), (Point) null);
    }

    public final synchronized void cancelUnconfirmedClick() {
        this.zzflg.cancelUnconfirmedClick();
    }

    public final synchronized void destroy() {
        this.zzfci.execute(new zzbwl(this));
        super.destroy();
    }

    public final synchronized boolean isCustomClickGestureEnabled() {
        return this.zzflg.isCustomClickGestureEnabled();
    }

    public final synchronized void recordCustomClickGesture() {
        if (this.zzflo == null) {
            zzayu.zzea("Ad should be associated with an ad view before calling recordCustomClickGesture()");
        } else {
            this.zzfci.execute(new zzbwo(this, this.zzflo instanceof zzbxi));
        }
    }

    public final synchronized void setClickConfirmingView(View view) {
        this.zzflg.setClickConfirmingView(view);
    }

    public final synchronized void zza(zzbxz zzbxz) {
        zzdg zzbw;
        this.zzflo = zzbxz;
        this.zzflh.zza(zzbxz);
        this.zzflg.zza(zzbxz.zzaga(), zzbxz.zzaka(), zzbxz.zzakb(), (View.OnTouchListener) zzbxz, (View.OnClickListener) zzbxz);
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclc)).booleanValue() && (zzbw = this.zzefv.zzbw()) != null) {
            zzbw.zzb(zzbxz.zzaga());
        }
        if (zzbxz.zzake() != null) {
            zzbxz.zzake().zza((zzps) this.zzffo);
        }
    }

    public final void zzaa(View view) {
        IObjectWrapper zzajh = this.zzfkc.zzajh();
        if (this.zzfli.zzaiw() && zzajh != null && view != null) {
            zzq.zzlf().zzb(zzajh, view);
        }
    }

    public final void zzagf() {
        this.zzfci.execute(new zzbwj(this));
        if (this.zzfkc.zzaja() != 7) {
            Executor executor = this.zzfci;
            zzbxa zzbxa = this.zzflg;
            zzbxa.getClass();
            executor.execute(zzbwm.zza(zzbxa));
        }
        super.zzagf();
    }

    public final synchronized void zzaio() {
        if (!this.zzflp) {
            this.zzflg.zzaio();
        }
    }

    public final boolean zzaiv() {
        return this.zzfli.zzajn();
    }

    public final boolean zzaiw() {
        return this.zzfli.zzaiw();
    }

    public final zzbwq zzaix() {
        return this.zzflq;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaiy() {
        this.zzflg.destroy();
        this.zzfkc.destroy();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaiz() {
        try {
            int zzaja = this.zzfkc.zzaja();
            if (zzaja != 1) {
                if (zzaja != 2) {
                    if (zzaja != 3) {
                        if (zzaja != 6) {
                            if (zzaja != 7) {
                                zzayu.zzex("Wrong native template id!");
                            } else if (this.zzfea.zzajt() != null) {
                                this.zzfea.zzajt().zza(this.zzflm.get());
                            }
                        } else if (this.zzfea.zzajr() != null) {
                            zzg("Google", true);
                            this.zzfea.zzajr().zza(this.zzfll.get());
                        }
                    } else if (this.zzfea.zzfz(this.zzfkc.getCustomTemplateId()) != null) {
                        if (this.zzfkc.zzajf() != null) {
                            zzg("Google", true);
                        }
                        this.zzfea.zzfz(this.zzfkc.getCustomTemplateId()).zzb(this.zzfln.get());
                    }
                } else if (this.zzfea.zzajq() != null) {
                    zzg("Google", true);
                    this.zzfea.zzajq().zza(this.zzflk.get());
                }
            } else if (this.zzfea.zzajp() != null) {
                zzg("Google", true);
                this.zzfea.zzajp().zza(this.zzflj.get());
            }
        } catch (RemoteException e) {
            zzayu.zzc("RemoteException when notifyAdLoad is called", e);
        }
    }

    public final synchronized void zzb(zzbxz zzbxz) {
        this.zzflg.zza(zzbxz.zzaga(), zzbxz.zzajz());
        if (zzbxz.zzakd() != null) {
            zzbxz.zzakd().setClickable(false);
            zzbxz.zzakd().removeAllViews();
        }
        if (zzbxz.zzake() != null) {
            zzbxz.zzake().zzb(this.zzffo);
        }
        this.zzflo = null;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzbh(boolean z) {
        this.zzflg.zza(this.zzflo.zzaga(), this.zzflo.zzajz(), this.zzflo.zzaka(), z);
    }

    public final synchronized void zzf(Bundle bundle) {
        this.zzflg.zzf(bundle);
    }

    public final synchronized void zzfu(String str) {
        this.zzflg.zzfu(str);
    }

    public final synchronized void zzg(Bundle bundle) {
        this.zzflg.zzg(bundle);
    }

    public final synchronized boolean zzh(Bundle bundle) {
        if (this.zzflp) {
            return true;
        }
        boolean zzh = this.zzflg.zzh(bundle);
        this.zzflp = zzh;
        return zzh;
    }

    public final synchronized void zzrp() {
        this.zzflg.zzrp();
    }

    public final void zzz(View view) {
        IObjectWrapper zzajh = this.zzfkc.zzajh();
        boolean z = this.zzfkc.zzajg() != null;
        if (this.zzfli.zzaiw() && zzajh != null && z && view != null) {
            zzq.zzlf().zza(zzajh, view);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzg(java.lang.String r11, boolean r12) {
        /*
            r10 = this;
            com.google.android.gms.internal.ads.zzbww r0 = r10.zzfli
            boolean r0 = r0.zzaiw()
            if (r0 != 0) goto L_0x0009
            return
        L_0x0009:
            com.google.android.gms.internal.ads.zzbws r0 = r10.zzfkc
            com.google.android.gms.internal.ads.zzbdi r0 = r0.zzajg()
            com.google.android.gms.internal.ads.zzbws r1 = r10.zzfkc
            com.google.android.gms.internal.ads.zzbdi r1 = r1.zzajf()
            if (r0 != 0) goto L_0x001a
            if (r1 != 0) goto L_0x001a
            return
        L_0x001a:
            r2 = 1
            r3 = 0
            if (r0 == 0) goto L_0x0020
            r4 = 1
            goto L_0x0021
        L_0x0020:
            r4 = 0
        L_0x0021:
            if (r1 == 0) goto L_0x0024
            goto L_0x0025
        L_0x0024:
            r2 = 0
        L_0x0025:
            r3 = 0
            if (r4 == 0) goto L_0x002a
        L_0x0028:
            r8 = r3
            goto L_0x0032
        L_0x002a:
            if (r2 == 0) goto L_0x0030
            java.lang.String r3 = "javascript"
            r0 = r1
            goto L_0x0028
        L_0x0030:
            r0 = r3
            r8 = r0
        L_0x0032:
            android.webkit.WebView r3 = r0.getWebView()
            if (r3 != 0) goto L_0x0039
            return
        L_0x0039:
            com.google.android.gms.internal.ads.zzaoq r3 = com.google.android.gms.ads.internal.zzq.zzlf()
            android.content.Context r4 = r10.zzup
            boolean r3 = r3.zzp(r4)
            if (r3 == 0) goto L_0x0095
            com.google.android.gms.internal.ads.zzazb r3 = r10.zzbll
            int r4 = r3.zzdvz
            int r3 = r3.zzdwa
            r5 = 23
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r5)
            r6.append(r4)
            java.lang.String r4 = "."
            r6.append(r4)
            r6.append(r3)
            java.lang.String r4 = r6.toString()
            com.google.android.gms.internal.ads.zzaoq r3 = com.google.android.gms.ads.internal.zzq.zzlf()
            android.webkit.WebView r5 = r0.getWebView()
            java.lang.String r6 = ""
            java.lang.String r7 = "javascript"
            r9 = r11
            com.google.android.gms.dynamic.IObjectWrapper r11 = r3.zza(r4, r5, r6, r7, r8, r9)
            if (r11 != 0) goto L_0x0075
            return
        L_0x0075:
            com.google.android.gms.internal.ads.zzbws r3 = r10.zzfkc
            r3.zzaq(r11)
            r0.zzan(r11)
            if (r2 == 0) goto L_0x008c
            android.view.View r0 = r1.getView()
            if (r0 == 0) goto L_0x008c
            com.google.android.gms.internal.ads.zzaoq r1 = com.google.android.gms.ads.internal.zzq.zzlf()
            r1.zza(r11, r0)
        L_0x008c:
            if (r12 == 0) goto L_0x0095
            com.google.android.gms.internal.ads.zzaoq r12 = com.google.android.gms.ads.internal.zzq.zzlf()
            r12.zzab(r11)
        L_0x0095:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbwk.zzg(java.lang.String, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005b, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zzb(android.view.View r3, java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4, java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r5, boolean r6) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.zzflp     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r2)
            return
        L_0x0007:
            r0 = 1
            if (r6 == 0) goto L_0x0013
            com.google.android.gms.internal.ads.zzbxa r6 = r2.zzflg     // Catch:{ all -> 0x005c }
            r6.zza((android.view.View) r3, (java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>) r4, (java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>) r5)     // Catch:{ all -> 0x005c }
            r2.zzflp = r0     // Catch:{ all -> 0x005c }
            monitor-exit(r2)
            return
        L_0x0013:
            if (r6 != 0) goto L_0x005a
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r6 = com.google.android.gms.internal.ads.zzzn.zzcmi     // Catch:{ all -> 0x005c }
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x005c }
            java.lang.Object r6 = r1.zzd(r6)     // Catch:{ all -> 0x005c }
            java.lang.Boolean r6 = (java.lang.Boolean) r6     // Catch:{ all -> 0x005c }
            boolean r6 = r6.booleanValue()     // Catch:{ all -> 0x005c }
            if (r6 == 0) goto L_0x005a
            if (r4 == 0) goto L_0x005a
            java.util.Set r6 = r4.entrySet()     // Catch:{ all -> 0x005c }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x005c }
        L_0x0031:
            boolean r1 = r6.hasNext()     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x005a
            java.lang.Object r1 = r6.next()     // Catch:{ all -> 0x005c }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ all -> 0x005c }
            java.lang.Object r1 = r1.getValue()     // Catch:{ all -> 0x005c }
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1     // Catch:{ all -> 0x005c }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x005c }
            android.view.View r1 = (android.view.View) r1     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x0031
            boolean r1 = zzy(r1)     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x0031
            com.google.android.gms.internal.ads.zzbxa r6 = r2.zzflg     // Catch:{ all -> 0x005c }
            r6.zza((android.view.View) r3, (java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>) r4, (java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>>) r5)     // Catch:{ all -> 0x005c }
            r2.zzflp = r0     // Catch:{ all -> 0x005c }
            monitor-exit(r2)
            return
        L_0x005a:
            monitor-exit(r2)
            return
        L_0x005c:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbwk.zzb(android.view.View, java.util.Map, java.util.Map, boolean):void");
    }

    public final synchronized void zza(View view, View view2, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, boolean z) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqc)).booleanValue()) {
            this.zzflh.zzc(this.zzflo);
        }
        this.zzflg.zza(view, view2, map, map2, z);
    }

    public final synchronized void zza(View view, MotionEvent motionEvent, View view2) {
        this.zzflg.zza(view, motionEvent, view2);
    }

    public final synchronized void zza(zzaeb zzaeb) {
        this.zzflg.zza(zzaeb);
    }

    public final synchronized void zza(zzwr zzwr) {
        this.zzflg.zza(zzwr);
    }

    public final synchronized void zza(zzwn zzwn) {
        this.zzflg.zza(zzwn);
    }
}
