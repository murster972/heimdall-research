package com.google.android.gms.internal.ads;

final class zzdst implements zzdtb {
    private zzdtb[] zzhop;

    zzdst(zzdtb... zzdtbArr) {
        this.zzhop = zzdtbArr;
    }

    public final boolean zze(Class<?> cls) {
        for (zzdtb zze : this.zzhop) {
            if (zze.zze(cls)) {
                return true;
            }
        }
        return false;
    }

    public final zzdtc zzf(Class<?> cls) {
        for (zzdtb zzdtb : this.zzhop) {
            if (zzdtb.zze(cls)) {
                return zzdtb.zzf(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
