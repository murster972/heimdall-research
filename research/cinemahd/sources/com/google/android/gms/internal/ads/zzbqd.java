package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.AdMetadataListener;

final /* synthetic */ class zzbqd implements zzbrn {
    static final zzbrn zzfhp = new zzbqd();

    private zzbqd() {
    }

    public final void zzp(Object obj) {
        ((AdMetadataListener) obj).onAdMetadataChanged();
    }
}
