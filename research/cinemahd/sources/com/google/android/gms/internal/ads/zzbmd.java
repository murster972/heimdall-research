package com.google.android.gms.internal.ads;

import android.content.Context;

public class zzbmd {
    protected final zzczt zzfbl;
    protected final zzczl zzffc;
    private final zzbpg zzffp;
    private final zzbpw zzffq;
    private final zzcxq zzffr;
    private final zzbom zzffs;

    protected zzbmd(zzbmg zzbmg) {
        this.zzfbl = zzbmg.zzfbl;
        this.zzffc = zzbmg.zzffc;
        this.zzffp = zzbmg.zzffp;
        this.zzffq = zzbmg.zzffq;
        this.zzffr = zzbmg.zzffr;
        this.zzffs = zzbmg.zzffs;
    }

    public void destroy() {
        this.zzffp.zzbx((Context) null);
    }

    public void zzagf() {
        this.zzffq.onAdLoaded();
    }

    public final zzbpg zzagr() {
        return this.zzffp;
    }

    public final zzbom zzags() {
        return this.zzffs;
    }

    public final zzcxq zzagt() {
        return this.zzffr;
    }
}
