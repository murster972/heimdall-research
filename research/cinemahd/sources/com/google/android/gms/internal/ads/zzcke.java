package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcke implements Runnable {
    private final zzccd zzfzn;

    private zzcke(zzccd zzccd) {
        this.zzfzn = zzccd;
    }

    static Runnable zza(zzccd zzccd) {
        return new zzcke(zzccd);
    }

    public final void run() {
        this.zzfzn.zzakx();
    }
}
