package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.common.internal.Preconditions;

public final class zzatd implements MediationRewardedAdCallback {
    private final zzali zzdds;

    public zzatd(zzali zzali) {
        this.zzdds = zzali;
    }

    public final void onAdClosed() {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdClosed.");
        try {
            this.zzdds.onAdClosed();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onAdFailedToShow(String str) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdFailedToShow.");
        String valueOf = String.valueOf(str);
        zzayu.zzez(valueOf.length() != 0 ? "Mediation ad failed to show: ".concat(valueOf) : new String("Mediation ad failed to show: "));
        try {
            this.zzdds.zzco(0);
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onAdOpened() {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onAdOpened.");
        try {
            this.zzdds.onAdOpened();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onUserEarnedReward(RewardItem rewardItem) {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onUserEarnedReward.");
        try {
            this.zzdds.zza((zzasf) new zzatc(rewardItem));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onVideoComplete() {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onVideoComplete.");
        try {
            this.zzdds.zzst();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void onVideoStart() {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called onVideoStart.");
        try {
            this.zzdds.zzss();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void reportAdClicked() {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called reportAdClicked.");
        try {
            this.zzdds.onAdClicked();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void reportAdImpression() {
        Preconditions.a("#008 Must be called on the main UI thread.");
        zzayu.zzea("Adapter called reportAdImpression.");
        try {
            this.zzdds.onAdImpression();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
