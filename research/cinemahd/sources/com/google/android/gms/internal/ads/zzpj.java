package com.google.android.gms.internal.ads;

final class zzpj implements Runnable {
    private final /* synthetic */ zzpg zzbjg;
    private final /* synthetic */ int zzbji;
    private final /* synthetic */ int zzbjj;
    private final /* synthetic */ int zzbjk;
    private final /* synthetic */ float zzbjl;

    zzpj(zzpg zzpg, int i, int i2, int i3, float f) {
        this.zzbjg = zzpg;
        this.zzbji = i;
        this.zzbjj = i2;
        this.zzbjk = i3;
        this.zzbjl = f;
    }

    public final void run() {
        this.zzbjg.zzbjh.zza(this.zzbji, this.zzbjj, this.zzbjk, this.zzbjl);
    }
}
