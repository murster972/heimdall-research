package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaiy implements Runnable {
    private final zzaif zzczv;

    private zzaiy(zzaif zzaif) {
        this.zzczv = zzaif;
    }

    static Runnable zzb(zzaif zzaif) {
        return new zzaiy(zzaif);
    }

    public final void run() {
        this.zzczv.destroy();
    }
}
