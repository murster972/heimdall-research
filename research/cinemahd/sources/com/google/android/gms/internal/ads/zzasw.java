package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzasw {
    public static zzasg zzd(Context context, String str, zzalc zzalc) {
        try {
            IBinder zzd = ((zzasm) zzayx.zza(context, "com.google.android.gms.ads.rewarded.ChimeraRewardedAdCreatorImpl", zzasz.zzbtz)).zzd(ObjectWrapper.a(context), str, zzalc, 19649000);
            if (zzd == null) {
                return null;
            }
            IInterface queryLocalInterface = zzd.queryLocalInterface("com.google.android.gms.ads.internal.rewarded.client.IRewardedAd");
            if (queryLocalInterface instanceof zzasg) {
                return (zzasg) queryLocalInterface;
            }
            return new zzasi(zzd);
        } catch (RemoteException | zzayz e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return null;
        }
    }
}
