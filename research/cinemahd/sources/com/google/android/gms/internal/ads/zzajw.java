package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

public interface zzajw<T> {
    T zzd(JSONObject jSONObject) throws JSONException;
}
