package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdfs;

public final class zzdhl<V> extends zzdfs.zzj<V> {
    private zzdhl() {
    }

    public static <V> zzdhl<V> zzarx() {
        return new zzdhl<>();
    }

    public final boolean set(V v) {
        return super.set(v);
    }

    public final boolean setException(Throwable th) {
        return super.setException(th);
    }
}
