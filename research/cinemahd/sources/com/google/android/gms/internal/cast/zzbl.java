package com.google.android.gms.internal.cast;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.SeekBar;
import com.google.android.gms.cast.AdBreakInfo;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import com.google.android.gms.cast.framework.media.widget.zze;
import java.util.List;

public final class zzbl extends UIController implements RemoteMediaClient.ProgressListener {
    private boolean zzri = true;
    private final SeekBar zzrp;
    private final zzbh zzrw;
    private final long zztc;
    private final zzbn zztj;
    private Boolean zztk;
    private Drawable zztl = null;

    public zzbl(SeekBar seekBar, long j, zzbh zzbh, zzbn zzbn) {
        this.zzrp = seekBar;
        this.zztc = j;
        this.zzrw = zzbh;
        this.zztj = zzbn;
        this.zzrp.setEnabled(false);
        this.zztl = zze.a(seekBar);
    }

    private final void zzdj() {
        zzec();
        if (this.zztj != null) {
            if (getRemoteMediaClient() != null) {
                MediaInfo e = getRemoteMediaClient().e();
                if (getRemoteMediaClient().k() && !getRemoteMediaClient().n() && e != null) {
                    this.zztj.zzd(e.t());
                }
            }
            this.zztj.zzd((List<AdBreakInfo>) null);
        }
        zzbn zzbn = this.zztj;
        if (zzbn != null) {
            zzbn.zzdf();
        }
    }

    private final void zzec() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            this.zzrp.setMax(this.zzrw.zzdm());
            this.zzrp.setProgress(this.zzrw.zzdn());
            this.zzrp.setEnabled(false);
        } else if (this.zzri) {
            this.zzrp.setMax(this.zzrw.zzdm());
            if (!remoteMediaClient.m() || !this.zzrw.zzdq()) {
                this.zzrp.setProgress(this.zzrw.zzdn());
            } else {
                this.zzrp.setProgress(this.zzrw.zzds());
            }
            if (remoteMediaClient.q()) {
                this.zzrp.setEnabled(false);
            } else {
                this.zzrp.setEnabled(true);
            }
            if (getRemoteMediaClient() != null) {
                Boolean bool = this.zztk;
                if (bool == null || bool.booleanValue() != this.zzrw.zzdo()) {
                    this.zztk = Boolean.valueOf(this.zzrw.zzdo());
                    if (!this.zztk.booleanValue()) {
                        this.zzrp.setThumb(new ColorDrawable(0));
                        this.zzrp.setClickable(false);
                        this.zzrp.setOnTouchListener(new zzbm(this));
                        return;
                    }
                    Drawable drawable = this.zztl;
                    if (drawable != null) {
                        this.zzrp.setThumb(drawable);
                    }
                    this.zzrp.setClickable(true);
                    this.zzrp.setOnTouchListener((View.OnTouchListener) null);
                }
            }
        }
    }

    public final void onMediaStatusUpdated() {
        zzdj();
    }

    public final void onProgressUpdated(long j, long j2) {
        zzec();
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this, this.zztc);
        }
        zzdj();
    }

    public final void onSessionEnded() {
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this);
        }
        super.onSessionEnded();
        zzdj();
    }

    public final void zzk(boolean z) {
        this.zzri = z;
    }
}
