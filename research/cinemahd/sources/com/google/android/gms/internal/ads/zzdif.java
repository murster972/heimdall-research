package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdte;
import java.security.GeneralSecurityException;

final class zzdif<KeyFormatProtoT extends zzdte, KeyProtoT extends zzdte> {
    private final zzdih<KeyFormatProtoT, KeyProtoT> zzgxs;

    zzdif(zzdih<KeyFormatProtoT, KeyProtoT> zzdih) {
        this.zzgxs = zzdih;
    }

    /* access modifiers changed from: package-private */
    public final KeyProtoT zzp(zzdqk zzdqk) throws GeneralSecurityException, zzdse {
        KeyFormatProtoT zzq = this.zzgxs.zzq(zzdqk);
        this.zzgxs.zzc(zzq);
        return (zzdte) this.zzgxs.zzd(zzq);
    }
}
