package com.google.android.gms.internal.measurement;

final class zzee implements zzeb {
    private zzee() {
    }

    public final byte[] zza(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    /* synthetic */ zzee(zzdu zzdu) {
        this();
    }
}
