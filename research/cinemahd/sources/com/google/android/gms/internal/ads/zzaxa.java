package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.net.Uri;
import com.google.android.gms.ads.internal.zzq;

final class zzaxa implements DialogInterface.OnClickListener {
    private final /* synthetic */ zzaxb zzdtn;

    zzaxa(zzaxb zzaxb) {
        this.zzdtn = zzaxb;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        zzq.zzkq();
        zzawb.zza(this.zzdtn.val$context, Uri.parse("https://support.google.com/dfp_premium/answer/7160685#push"));
    }
}
