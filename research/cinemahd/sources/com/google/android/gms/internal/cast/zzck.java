package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.games.GameManagerClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

public abstract class zzck extends zzcj<GameManagerClient.GameManagerInstanceResult> {
    final /* synthetic */ zzcb zzwi;
    /* access modifiers changed from: private */
    public GameManagerClient zzwr;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzck(zzcb zzcb, GameManagerClient gameManagerClient) {
        super(zzcb);
        this.zzwi = zzcb;
        this.zzwr = gameManagerClient;
        this.zzwq = new zzcl(this, zzcb);
    }

    public static GameManagerClient.GameManagerInstanceResult zzc(Status status) {
        return new zzcm(status, (GameManagerClient) null);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return zzc(status);
    }
}
