package com.google.android.gms.internal.ads;

import android.annotation.SuppressLint;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

final class zzgr implements zzgk {
    private int repeatMode;
    private final zzhf[] zzacq;
    private final zznf zzacr;
    private final zzng zzacs;
    private final zzddu zzact;
    private final zzgt zzacu;
    private final CopyOnWriteArraySet<zzgn> zzacv;
    private final zzhl zzacw;
    private final zzhi zzacx;
    private boolean zzacy;
    private boolean zzacz;
    private int zzada;
    private int zzadb;
    private int zzadc;
    private boolean zzadd;
    private zzhg zzade;
    private Object zzadf;
    private zzmr zzadg;
    private zzng zzadh;
    private zzhc zzadi;
    private zzgv zzadj;
    private int zzadk;
    private int zzadl;
    private long zzadm;

    @SuppressLint({"HandlerLeak"})
    public zzgr(zzhf[] zzhfArr, zznf zznf, zzha zzha) {
        String str = zzoq.zzbgv;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 26);
        sb.append("Init ExoPlayerLib/2.4.2 [");
        sb.append(str);
        sb.append("]");
        Log.i("ExoPlayerImpl", sb.toString());
        zzoc.checkState(zzhfArr.length > 0);
        this.zzacq = (zzhf[]) zzoc.checkNotNull(zzhfArr);
        this.zzacr = (zznf) zzoc.checkNotNull(zznf);
        this.zzacz = false;
        this.repeatMode = 0;
        this.zzada = 1;
        this.zzacv = new CopyOnWriteArraySet<>();
        this.zzacs = new zzng(new zzne[zzhfArr.length]);
        this.zzade = zzhg.zzagf;
        this.zzacw = new zzhl();
        this.zzacx = new zzhi();
        this.zzadg = zzmr.zzbdd;
        this.zzadh = this.zzacs;
        this.zzadi = zzhc.zzagb;
        this.zzact = new zzgq(this, Looper.myLooper() != null ? Looper.myLooper() : Looper.getMainLooper());
        this.zzadj = new zzgv(0, 0);
        this.zzacu = new zzgt(zzhfArr, zznf, zzha, this.zzacz, 0, this.zzact, this.zzadj, this);
    }

    private final int zzee() {
        if (this.zzade.isEmpty() || this.zzadb > 0) {
            return this.zzadk;
        }
        this.zzade.zza(this.zzadj.zzads, this.zzacx, false);
        return 0;
    }

    public final long getBufferedPosition() {
        if (this.zzade.isEmpty() || this.zzadb > 0) {
            return this.zzadm;
        }
        this.zzade.zza(this.zzadj.zzads, this.zzacx, false);
        return this.zzacx.zzex() + zzgi.zzdm(this.zzadj.zzaey);
    }

    public final long getDuration() {
        if (this.zzade.isEmpty()) {
            return -9223372036854775807L;
        }
        return zzgi.zzdm(this.zzade.zza(zzee(), this.zzacw, false).zzagj);
    }

    public final int getPlaybackState() {
        return this.zzada;
    }

    public final void release() {
        this.zzacu.release();
        this.zzact.removeCallbacksAndMessages((Object) null);
    }

    public final void seekTo(long j) {
        long j2;
        int zzee = zzee();
        if (zzee < 0 || (!this.zzade.isEmpty() && zzee >= this.zzade.zzev())) {
            throw new zzhb(this.zzade, zzee, j);
        }
        this.zzadb++;
        this.zzadk = zzee;
        if (!this.zzade.isEmpty()) {
            this.zzade.zza(zzee, this.zzacw, false);
            if (j == -9223372036854775807L) {
                j2 = this.zzacw.zzagy;
            } else {
                j2 = zzgi.zzdn(j);
            }
            long j3 = this.zzacw.zzagz + j2;
            long j4 = this.zzade.zza(0, this.zzacx, false).zzagj;
            if (j4 != -9223372036854775807L) {
                int i = (j3 > j4 ? 1 : (j3 == j4 ? 0 : -1));
            }
        }
        this.zzadl = 0;
        if (j == -9223372036854775807L) {
            this.zzadm = 0;
            this.zzacu.zza(this.zzade, zzee, -9223372036854775807L);
            return;
        }
        this.zzadm = j;
        this.zzacu.zza(this.zzade, zzee, zzgi.zzdn(j));
        Iterator<zzgn> it2 = this.zzacv.iterator();
        while (it2.hasNext()) {
            it2.next().zzed();
        }
    }

    public final void stop() {
        this.zzacu.stop();
    }

    public final void zza(zzgn zzgn) {
        this.zzacv.add(zzgn);
    }

    public final void zzb(zzgn zzgn) {
        this.zzacv.remove(zzgn);
    }

    public final boolean zzea() {
        return this.zzacz;
    }

    public final int zzeb() {
        return this.zzacq.length;
    }

    public final long zzec() {
        if (this.zzade.isEmpty() || this.zzadb > 0) {
            return this.zzadm;
        }
        this.zzade.zza(this.zzadj.zzads, this.zzacx, false);
        return this.zzacx.zzex() + zzgi.zzdm(this.zzadj.zzaex);
    }

    public final void zzf(boolean z) {
        if (this.zzacz != z) {
            this.zzacz = z;
            this.zzacu.zzf(z);
            Iterator<zzgn> it2 = this.zzacv.iterator();
            while (it2.hasNext()) {
                it2.next().zza(z, this.zzada);
            }
        }
    }

    public final void zza(zzmb zzmb) {
        if (!this.zzade.isEmpty() || this.zzadf != null) {
            this.zzade = zzhg.zzagf;
            this.zzadf = null;
            Iterator<zzgn> it2 = this.zzacv.iterator();
            while (it2.hasNext()) {
                it2.next().zza(this.zzade, this.zzadf);
            }
        }
        if (this.zzacy) {
            this.zzacy = false;
            this.zzadg = zzmr.zzbdd;
            this.zzadh = this.zzacs;
            this.zzacr.zzd((Object) null);
            Iterator<zzgn> it3 = this.zzacv.iterator();
            while (it3.hasNext()) {
                it3.next().zza(this.zzadg, this.zzadh);
            }
        }
        this.zzadc++;
        this.zzacu.zza(zzmb, true);
    }

    public final void zzb(zzgp... zzgpArr) {
        this.zzacu.zzb(zzgpArr);
    }

    public final void zza(zzgp... zzgpArr) {
        this.zzacu.zza(zzgpArr);
    }

    /* access modifiers changed from: package-private */
    public final void zza(Message message) {
        boolean z = true;
        switch (message.what) {
            case 0:
                this.zzadc--;
                return;
            case 1:
                this.zzada = message.arg1;
                Iterator<zzgn> it2 = this.zzacv.iterator();
                while (it2.hasNext()) {
                    it2.next().zza(this.zzacz, this.zzada);
                }
                return;
            case 2:
                if (message.arg1 == 0) {
                    z = false;
                }
                this.zzadd = z;
                Iterator<zzgn> it3 = this.zzacv.iterator();
                while (it3.hasNext()) {
                    it3.next().zzg(this.zzadd);
                }
                return;
            case 3:
                if (this.zzadc == 0) {
                    zznh zznh = (zznh) message.obj;
                    this.zzacy = true;
                    this.zzadg = zznh.zzbef;
                    this.zzadh = zznh.zzbeg;
                    this.zzacr.zzd(zznh.zzbeh);
                    Iterator<zzgn> it4 = this.zzacv.iterator();
                    while (it4.hasNext()) {
                        it4.next().zza(this.zzadg, this.zzadh);
                    }
                    return;
                }
                return;
            case 4:
                int i = this.zzadb - 1;
                this.zzadb = i;
                if (i == 0) {
                    this.zzadj = (zzgv) message.obj;
                    if (message.arg1 != 0) {
                        Iterator<zzgn> it5 = this.zzacv.iterator();
                        while (it5.hasNext()) {
                            it5.next().zzed();
                        }
                        return;
                    }
                    return;
                }
                return;
            case 5:
                if (this.zzadb == 0) {
                    this.zzadj = (zzgv) message.obj;
                    Iterator<zzgn> it6 = this.zzacv.iterator();
                    while (it6.hasNext()) {
                        it6.next().zzed();
                    }
                    return;
                }
                return;
            case 6:
                zzgx zzgx = (zzgx) message.obj;
                this.zzadb -= zzgx.zzafy;
                if (this.zzadc == 0) {
                    this.zzade = zzgx.zzade;
                    this.zzadf = zzgx.zzadf;
                    this.zzadj = zzgx.zzadj;
                    Iterator<zzgn> it7 = this.zzacv.iterator();
                    while (it7.hasNext()) {
                        it7.next().zza(this.zzade, this.zzadf);
                    }
                    return;
                }
                return;
            case 7:
                zzhc zzhc = (zzhc) message.obj;
                if (!this.zzadi.equals(zzhc)) {
                    this.zzadi = zzhc;
                    Iterator<zzgn> it8 = this.zzacv.iterator();
                    while (it8.hasNext()) {
                        it8.next().zza(zzhc);
                    }
                    return;
                }
                return;
            case 8:
                zzgl zzgl = (zzgl) message.obj;
                Iterator<zzgn> it9 = this.zzacv.iterator();
                while (it9.hasNext()) {
                    it9.next().zza(zzgl);
                }
                return;
            default:
                throw new IllegalStateException();
        }
    }
}
