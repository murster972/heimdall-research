package com.google.android.gms.internal.ads;

public class zzaan<T> {
    private final String zzcc;
    private final T zzcfu;
    private final int zzcsg;

    /* JADX WARNING: type inference failed for: r3v0, types: [int, java.lang.Integer] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected zzaan(java.lang.String r1, T r2, java.lang.Integer r3) {
        /*
            r0 = this;
            r0.<init>()
            r0.zzcc = r1
            r0.zzcfu = r2
            r0.zzcsg = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzaan.<init>(java.lang.String, java.lang.Object, int):void");
    }

    public static zzaan<Long> zzb(String str, long j) {
        return new zzaan<>(str, Long.valueOf(j), zzaap.zzcsi);
    }

    public static zzaan<Boolean> zzf(String str, boolean z) {
        return new zzaan<>(str, Boolean.valueOf(z), zzaap.zzcsh);
    }

    public static zzaan<String> zzi(String str, String str2) {
        return new zzaan<>(str, str2, zzaap.zzcsk);
    }

    public T get() {
        zzabo zzqw = zzabn.zzqw();
        if (zzqw != null) {
            int i = zzaaq.zzcsm[this.zzcsg - 1];
            if (i == 1) {
                return zzqw.zze(this.zzcc, ((Boolean) this.zzcfu).booleanValue());
            }
            if (i == 2) {
                return zzqw.getLong(this.zzcc, ((Long) this.zzcfu).longValue());
            }
            if (i == 3) {
                return zzqw.zza(this.zzcc, ((Double) this.zzcfu).doubleValue());
            }
            if (i == 4) {
                return zzqw.get(this.zzcc, (String) this.zzcfu);
            }
            throw new IllegalStateException();
        }
        throw new IllegalStateException("Flag is not initialized.");
    }
}
