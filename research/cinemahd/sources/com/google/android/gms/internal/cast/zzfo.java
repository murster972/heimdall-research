package com.google.android.gms.internal.cast;

public final class zzfo {
    public static <T> T checkNotNull(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }
}
