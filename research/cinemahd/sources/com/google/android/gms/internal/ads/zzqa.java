package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;

final class zzqa implements zzqd {
    private final /* synthetic */ Activity val$activity;

    zzqa(zzpv zzpv, Activity activity) {
        this.val$activity = activity;
    }

    public final void zza(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityDestroyed(this.val$activity);
    }
}
