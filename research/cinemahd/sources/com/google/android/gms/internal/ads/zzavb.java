package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;

final class zzavb implements Runnable {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ zzazl zzdpv;

    zzavb(zzauy zzauy, Context context, zzazl zzazl) {
        this.val$context = context;
        this.zzdpv = zzazl;
    }

    public final void run() {
        try {
            this.zzdpv.set(AdvertisingIdClient.getAdvertisingIdInfo(this.val$context));
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException | IllegalStateException e) {
            this.zzdpv.setException(e);
            zzayu.zzc("Exception while getting advertising Id info", e);
        }
    }
}
