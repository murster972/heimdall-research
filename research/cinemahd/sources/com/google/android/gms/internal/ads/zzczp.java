package com.google.android.gms.internal.ads;

import android.util.JsonReader;
import android.util.JsonWriter;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzczp implements zzaxx {
    public final String zzdhr = this.zzgmc.optString("ad_base_url", (String) null);
    public final String zzdht = this.zzgmc.optString("ad_html", (String) null);
    public final JSONObject zzfka = this.zzgmc.optJSONObject("ad_json");
    private final JSONObject zzgmc;

    zzczp(JsonReader jsonReader) throws IllegalStateException, IOException, JSONException, NumberFormatException {
        this.zzgmc = zzaxs.zzc(jsonReader);
    }

    public final void zza(JsonWriter jsonWriter) throws IOException {
        zzaxs.zza(jsonWriter, this.zzgmc);
    }
}
