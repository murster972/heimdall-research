package com.google.android.gms.internal.ads;

public final class zzbnd implements zzdxg<zzbsu<zzbov>> {
    private final zzbnb zzfgs;
    private final zzdxp<zzbnk> zzfgt;

    private zzbnd(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        this.zzfgs = zzbnb;
        this.zzfgt = zzdxp;
    }

    public static zzbnd zza(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        return new zzbnd(zzbnb, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfgt.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
