package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdng extends zzdrt<zzdng, zza> implements zzdtg {
    private static volatile zzdtn<zzdng> zzdz;
    /* access modifiers changed from: private */
    public static final zzdng zzhdk;
    private String zzhcs = "";
    private zzdqk zzhct = zzdqk.zzhhx;
    private int zzhdj;

    public static final class zza extends zzdrt.zzb<zzdng, zza> implements zzdtg {
        private zza() {
            super(zzdng.zzhdk);
        }

        /* synthetic */ zza(zzdnf zzdnf) {
            this();
        }
    }

    static {
        zzdng zzdng = new zzdng();
        zzhdk = zzdng;
        zzdrt.zza(zzdng.class, zzdng);
    }

    private zzdng() {
    }

    public static zzdng zzavo() {
        return zzhdk;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnf.zzdk[i - 1]) {
            case 1:
                return new zzdng();
            case 2:
                return new zza((zzdnf) null);
            case 3:
                return zzdrt.zza((zzdte) zzhdk, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001Ȉ\u0002\n\u0003\f", new Object[]{"zzhcs", "zzhct", "zzhdj"});
            case 4:
                return zzhdk;
            case 5:
                zzdtn<zzdng> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdng.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhdk);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final String zzavi() {
        return this.zzhcs;
    }

    public final zzdqk zzavj() {
        return this.zzhct;
    }
}
