package com.google.android.gms.internal.ads;

public final class zzcnu implements zzdxg<zzcns> {
    private final zzcns zzgbz;

    private zzcnu(zzcns zzcns) {
        this.zzgbz = zzcns;
    }

    public static zzcnu zzd(zzcns zzcns) {
        return new zzcnu(zzcns);
    }

    public final /* synthetic */ Object get() {
        zzcns zzcns = this.zzgbz;
        if (zzcns != null) {
            return (zzcns) zzdxm.zza(zzcns, "Cannot return null from a non-@Nullable @Provides method");
        }
        throw null;
    }
}
