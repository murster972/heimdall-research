package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

final class zzakq implements zzafv {
    private final zzazl<O> zzdbf;
    private final /* synthetic */ zzako zzdbi;

    public zzakq(zzako zzako, zzazl<O> zzazl) {
        this.zzdbi = zzako;
        this.zzdbf = zzazl;
    }

    public final void onFailure(String str) {
        if (str == null) {
            try {
                this.zzdbf.setException(new zzajr());
            } catch (IllegalStateException unused) {
            }
        } else {
            this.zzdbf.setException(new zzajr(str));
        }
    }

    public final void zzc(JSONObject jSONObject) {
        try {
            this.zzdbf.set(this.zzdbi.zzdaw.zzd(jSONObject));
        } catch (IllegalStateException unused) {
        } catch (JSONException e) {
            this.zzdbf.setException(e);
        }
    }
}
