package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcbx implements zzbtk {
    private final zzbdi zzehp;

    private zzcbx(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static zzbtk zzn(zzbdi zzbdi) {
        return new zzcbx(zzbdi);
    }

    public final void zzaib() {
        this.zzehp.destroy();
    }
}
