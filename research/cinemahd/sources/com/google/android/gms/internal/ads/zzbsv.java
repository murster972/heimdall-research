package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.Set;

public final class zzbsv implements zzdxg<zzbst> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Set<zzbsu<zzps>>> zzfeo;
    private final zzdxp<zzczl> zzffb;

    private zzbsv(zzdxp<Context> zzdxp, zzdxp<Set<zzbsu<zzps>>> zzdxp2, zzdxp<zzczl> zzdxp3) {
        this.zzejv = zzdxp;
        this.zzfeo = zzdxp2;
        this.zzffb = zzdxp3;
    }

    public static zzbsv zzh(zzdxp<Context> zzdxp, zzdxp<Set<zzbsu<zzps>>> zzdxp2, zzdxp<zzczl> zzdxp3) {
        return new zzbsv(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzbst(this.zzejv.get(), this.zzfeo.get(), this.zzffb.get());
    }
}
