package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcrg implements zzcty<Bundle> {
    private final double zzdmt;
    private final boolean zzdmu;

    public zzcrg(double d, boolean z) {
        this.zzdmt = d;
        this.zzdmu = z;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        Bundle zza = zzdaa.zza(bundle, "device");
        bundle.putBundle("device", zza);
        Bundle zza2 = zzdaa.zza(zza, "battery");
        zza.putBundle("battery", zza2);
        zza2.putBoolean("is_charging", this.zzdmu);
        zza2.putDouble("battery_level", this.zzdmt);
    }
}
