package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;

public abstract class zzvt extends zzgb implements zzvu {
    public zzvt() {
        super("com.google.android.gms.ads.internal.client.IAdManager");
    }

    public static zzvu zzc(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
        if (queryLocalInterface instanceof zzvu) {
            return (zzvu) queryLocalInterface;
        }
        return new zzvw(iBinder);
    }

    /* JADX WARNING: type inference failed for: r4v1 */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.google.android.gms.internal.ads.zzvh] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.google.android.gms.internal.ads.zzwc] */
    /* JADX WARNING: type inference failed for: r4v12, types: [com.google.android.gms.internal.ads.zzvg] */
    /* JADX WARNING: type inference failed for: r4v17, types: [com.google.android.gms.internal.ads.zzwi] */
    /* JADX WARNING: type inference failed for: r4v22, types: [com.google.android.gms.internal.ads.zzvx] */
    /* JADX WARNING: type inference failed for: r4v27 */
    /* JADX WARNING: type inference failed for: r4v28 */
    /* JADX WARNING: type inference failed for: r4v29 */
    /* JADX WARNING: type inference failed for: r4v30 */
    /* JADX WARNING: type inference failed for: r4v31 */
    /* JADX WARNING: type inference failed for: r4v32 */
    /* JADX WARNING: type inference failed for: r4v33 */
    /* JADX WARNING: type inference failed for: r4v34 */
    /* JADX WARNING: type inference failed for: r4v35 */
    /* JADX WARNING: type inference failed for: r4v36 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(int r1, android.os.Parcel r2, android.os.Parcel r3, int r4) throws android.os.RemoteException {
        /*
            r0 = this;
            r4 = 0
            switch(r1) {
                case 1: goto L_0x0235;
                case 2: goto L_0x022e;
                case 3: goto L_0x0223;
                case 4: goto L_0x0210;
                case 5: goto L_0x0209;
                case 6: goto L_0x0202;
                case 7: goto L_0x01e1;
                case 8: goto L_0x01c0;
                case 9: goto L_0x01b8;
                case 10: goto L_0x01b0;
                case 11: goto L_0x01a8;
                case 12: goto L_0x019c;
                case 13: goto L_0x018c;
                case 14: goto L_0x017c;
                case 15: goto L_0x0168;
                case 16: goto L_0x0004;
                case 17: goto L_0x0004;
                case 18: goto L_0x015c;
                case 19: goto L_0x014c;
                case 20: goto L_0x012a;
                case 21: goto L_0x0108;
                case 22: goto L_0x00fc;
                case 23: goto L_0x00f0;
                case 24: goto L_0x00e0;
                case 25: goto L_0x00d4;
                case 26: goto L_0x00c8;
                case 27: goto L_0x0004;
                case 28: goto L_0x0004;
                case 29: goto L_0x00b8;
                case 30: goto L_0x00a8;
                case 31: goto L_0x009c;
                case 32: goto L_0x0090;
                case 33: goto L_0x0084;
                case 34: goto L_0x0078;
                case 35: goto L_0x006c;
                case 36: goto L_0x004a;
                case 37: goto L_0x003e;
                case 38: goto L_0x0032;
                case 39: goto L_0x0022;
                case 40: goto L_0x0012;
                case 41: goto L_0x0006;
                default: goto L_0x0004;
            }
        L_0x0004:
            r1 = 0
            return r1
        L_0x0006:
            com.google.android.gms.internal.ads.zzxa r1 = r0.zzkb()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x023f
        L_0x0012:
            android.os.IBinder r1 = r2.readStrongBinder()
            com.google.android.gms.internal.ads.zzrg r1 = com.google.android.gms.internal.ads.zzrj.zzb(r1)
            r0.zza((com.google.android.gms.internal.ads.zzrg) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x0022:
            android.os.Parcelable$Creator<com.google.android.gms.internal.ads.zzuo> r1 = com.google.android.gms.internal.ads.zzuo.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            com.google.android.gms.internal.ads.zzuo r1 = (com.google.android.gms.internal.ads.zzuo) r1
            r0.zza((com.google.android.gms.internal.ads.zzuo) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x0032:
            java.lang.String r1 = r2.readString()
            r0.zzbr(r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x003e:
            android.os.Bundle r1 = r0.getAdMetadata()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zzb(r3, r1)
            goto L_0x023f
        L_0x004a:
            android.os.IBinder r1 = r2.readStrongBinder()
            if (r1 != 0) goto L_0x0051
            goto L_0x0064
        L_0x0051:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IAdMetadataListener"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r4 = r2 instanceof com.google.android.gms.internal.ads.zzvx
            if (r4 == 0) goto L_0x005f
            r4 = r2
            com.google.android.gms.internal.ads.zzvx r4 = (com.google.android.gms.internal.ads.zzvx) r4
            goto L_0x0064
        L_0x005f:
            com.google.android.gms.internal.ads.zzvz r4 = new com.google.android.gms.internal.ads.zzvz
            r4.<init>(r1)
        L_0x0064:
            r0.zza((com.google.android.gms.internal.ads.zzvx) r4)
            r3.writeNoException()
            goto L_0x023f
        L_0x006c:
            java.lang.String r1 = r0.zzka()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x023f
        L_0x0078:
            boolean r1 = com.google.android.gms.internal.ads.zzge.zza(r2)
            r0.setImmersiveMode(r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x0084:
            com.google.android.gms.internal.ads.zzvh r1 = r0.zzkd()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x023f
        L_0x0090:
            com.google.android.gms.internal.ads.zzwc r1 = r0.zzkc()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x023f
        L_0x009c:
            java.lang.String r1 = r0.getAdUnitId()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x023f
        L_0x00a8:
            android.os.Parcelable$Creator<com.google.android.gms.internal.ads.zzxh> r1 = com.google.android.gms.internal.ads.zzxh.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            com.google.android.gms.internal.ads.zzxh r1 = (com.google.android.gms.internal.ads.zzxh) r1
            r0.zza((com.google.android.gms.internal.ads.zzxh) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x00b8:
            android.os.Parcelable$Creator<com.google.android.gms.internal.ads.zzyw> r1 = com.google.android.gms.internal.ads.zzyw.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            com.google.android.gms.internal.ads.zzyw r1 = (com.google.android.gms.internal.ads.zzyw) r1
            r0.zza((com.google.android.gms.internal.ads.zzyw) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x00c8:
            com.google.android.gms.internal.ads.zzxb r1 = r0.getVideoController()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
            goto L_0x023f
        L_0x00d4:
            java.lang.String r1 = r2.readString()
            r0.setUserId(r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x00e0:
            android.os.IBinder r1 = r2.readStrongBinder()
            com.google.android.gms.internal.ads.zzaro r1 = com.google.android.gms.internal.ads.zzarr.zzaj(r1)
            r0.zza((com.google.android.gms.internal.ads.zzaro) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x00f0:
            boolean r1 = r0.isLoading()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.writeBoolean(r3, r1)
            goto L_0x023f
        L_0x00fc:
            boolean r1 = com.google.android.gms.internal.ads.zzge.zza(r2)
            r0.setManualImpressionsEnabled(r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x0108:
            android.os.IBinder r1 = r2.readStrongBinder()
            if (r1 != 0) goto L_0x010f
            goto L_0x0122
        L_0x010f:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.ICorrelationIdProvider"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r4 = r2 instanceof com.google.android.gms.internal.ads.zzwi
            if (r4 == 0) goto L_0x011d
            r4 = r2
            com.google.android.gms.internal.ads.zzwi r4 = (com.google.android.gms.internal.ads.zzwi) r4
            goto L_0x0122
        L_0x011d:
            com.google.android.gms.internal.ads.zzwh r4 = new com.google.android.gms.internal.ads.zzwh
            r4.<init>(r1)
        L_0x0122:
            r0.zza((com.google.android.gms.internal.ads.zzwi) r4)
            r3.writeNoException()
            goto L_0x023f
        L_0x012a:
            android.os.IBinder r1 = r2.readStrongBinder()
            if (r1 != 0) goto L_0x0131
            goto L_0x0144
        L_0x0131:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IAdClickListener"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r4 = r2 instanceof com.google.android.gms.internal.ads.zzvg
            if (r4 == 0) goto L_0x013f
            r4 = r2
            com.google.android.gms.internal.ads.zzvg r4 = (com.google.android.gms.internal.ads.zzvg) r4
            goto L_0x0144
        L_0x013f:
            com.google.android.gms.internal.ads.zzvi r4 = new com.google.android.gms.internal.ads.zzvi
            r4.<init>(r1)
        L_0x0144:
            r0.zza((com.google.android.gms.internal.ads.zzvg) r4)
            r3.writeNoException()
            goto L_0x023f
        L_0x014c:
            android.os.IBinder r1 = r2.readStrongBinder()
            com.google.android.gms.internal.ads.zzaak r1 = com.google.android.gms.internal.ads.zzaaj.zzk(r1)
            r0.zza((com.google.android.gms.internal.ads.zzaak) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x015c:
            java.lang.String r1 = r0.getMediationAdapterClassName()
            r3.writeNoException()
            r3.writeString(r1)
            goto L_0x023f
        L_0x0168:
            android.os.IBinder r1 = r2.readStrongBinder()
            com.google.android.gms.internal.ads.zzape r1 = com.google.android.gms.internal.ads.zzaph.zzah(r1)
            java.lang.String r2 = r2.readString()
            r0.zza(r1, r2)
            r3.writeNoException()
            goto L_0x023f
        L_0x017c:
            android.os.IBinder r1 = r2.readStrongBinder()
            com.google.android.gms.internal.ads.zzaoy r1 = com.google.android.gms.internal.ads.zzapb.zzaf(r1)
            r0.zza((com.google.android.gms.internal.ads.zzaoy) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x018c:
            android.os.Parcelable$Creator<com.google.android.gms.internal.ads.zzuj> r1 = com.google.android.gms.internal.ads.zzuj.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            com.google.android.gms.internal.ads.zzuj r1 = (com.google.android.gms.internal.ads.zzuj) r1
            r0.zza((com.google.android.gms.internal.ads.zzuj) r1)
            r3.writeNoException()
            goto L_0x023f
        L_0x019c:
            com.google.android.gms.internal.ads.zzuj r1 = r0.zzjz()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zzb(r3, r1)
            goto L_0x023f
        L_0x01a8:
            r0.zzjy()
            r3.writeNoException()
            goto L_0x023f
        L_0x01b0:
            r0.stopLoading()
            r3.writeNoException()
            goto L_0x023f
        L_0x01b8:
            r0.showInterstitial()
            r3.writeNoException()
            goto L_0x023f
        L_0x01c0:
            android.os.IBinder r1 = r2.readStrongBinder()
            if (r1 != 0) goto L_0x01c7
            goto L_0x01da
        L_0x01c7:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IAppEventListener"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r4 = r2 instanceof com.google.android.gms.internal.ads.zzwc
            if (r4 == 0) goto L_0x01d5
            r4 = r2
            com.google.android.gms.internal.ads.zzwc r4 = (com.google.android.gms.internal.ads.zzwc) r4
            goto L_0x01da
        L_0x01d5:
            com.google.android.gms.internal.ads.zzwe r4 = new com.google.android.gms.internal.ads.zzwe
            r4.<init>(r1)
        L_0x01da:
            r0.zza((com.google.android.gms.internal.ads.zzwc) r4)
            r3.writeNoException()
            goto L_0x023f
        L_0x01e1:
            android.os.IBinder r1 = r2.readStrongBinder()
            if (r1 != 0) goto L_0x01e8
            goto L_0x01fb
        L_0x01e8:
            java.lang.String r2 = "com.google.android.gms.ads.internal.client.IAdListener"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r4 = r2 instanceof com.google.android.gms.internal.ads.zzvh
            if (r4 == 0) goto L_0x01f6
            r4 = r2
            com.google.android.gms.internal.ads.zzvh r4 = (com.google.android.gms.internal.ads.zzvh) r4
            goto L_0x01fb
        L_0x01f6:
            com.google.android.gms.internal.ads.zzvj r4 = new com.google.android.gms.internal.ads.zzvj
            r4.<init>(r1)
        L_0x01fb:
            r0.zza((com.google.android.gms.internal.ads.zzvh) r4)
            r3.writeNoException()
            goto L_0x023f
        L_0x0202:
            r0.resume()
            r3.writeNoException()
            goto L_0x023f
        L_0x0209:
            r0.pause()
            r3.writeNoException()
            goto L_0x023f
        L_0x0210:
            android.os.Parcelable$Creator<com.google.android.gms.internal.ads.zzug> r1 = com.google.android.gms.internal.ads.zzug.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r2, r1)
            com.google.android.gms.internal.ads.zzug r1 = (com.google.android.gms.internal.ads.zzug) r1
            boolean r1 = r0.zza((com.google.android.gms.internal.ads.zzug) r1)
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.writeBoolean(r3, r1)
            goto L_0x023f
        L_0x0223:
            boolean r1 = r0.isReady()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.writeBoolean(r3, r1)
            goto L_0x023f
        L_0x022e:
            r0.destroy()
            r3.writeNoException()
            goto L_0x023f
        L_0x0235:
            com.google.android.gms.dynamic.IObjectWrapper r1 = r0.zzjx()
            r3.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r3, (android.os.IInterface) r1)
        L_0x023f:
            r1 = 1
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzvt.zza(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
