package com.google.android.gms.internal.ads;

public class zzip {
    private int flags;

    public void clear() {
        this.flags = 0;
    }

    public final void setFlags(int i) {
        this.flags = i;
    }

    public final boolean zzga() {
        return zzx(Integer.MIN_VALUE);
    }

    public final boolean zzgb() {
        return zzx(4);
    }

    public final boolean zzgc() {
        return zzx(1);
    }

    public final void zzw(int i) {
        this.flags |= Integer.MIN_VALUE;
    }

    /* access modifiers changed from: protected */
    public final boolean zzx(int i) {
        return (this.flags & i) == i;
    }
}
