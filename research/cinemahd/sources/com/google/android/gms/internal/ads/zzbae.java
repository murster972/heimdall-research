package com.google.android.gms.internal.ads;

final class zzbae implements Runnable {
    private final /* synthetic */ zzazx zzdxn;

    zzbae(zzazx zzazx) {
        this.zzdxn = zzazx;
    }

    public final void run() {
        if (this.zzdxn.zzdxm != null) {
            this.zzdxn.zzdxm.onPaused();
        }
    }
}
