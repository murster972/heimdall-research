package com.google.android.gms.internal.ads;

public final class zzbop implements zzdxg<zzbom> {
    private final zzdxp<zzczl> zzffb;
    private final zzdxp<String> zzfhl;

    private zzbop(zzdxp<zzczl> zzdxp, zzdxp<String> zzdxp2) {
        this.zzffb = zzdxp;
        this.zzfhl = zzdxp2;
    }

    public static zzbop zzh(zzdxp<zzczl> zzdxp, zzdxp<String> zzdxp2) {
        return new zzbop(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzbom(this.zzffb.get(), this.zzfhl.get());
    }
}
