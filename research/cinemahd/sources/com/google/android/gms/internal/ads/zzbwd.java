package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import org.json.JSONObject;

public final class zzbwd implements zzdxg<zzpn> {
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<String> zzfdc;

    public zzbwd(zzdxp<zzazb> zzdxp, zzdxp<String> zzdxp2) {
        this.zzfdb = zzdxp;
        this.zzfdc = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        zzq.zzkq();
        return (zzpn) zzdxm.zza(new zzpn(zzawb.zzwk(), this.zzfdb.get(), this.zzfdc.get(), new JSONObject(), false, true), "Cannot return null from a non-@Nullable @Provides method");
    }
}
