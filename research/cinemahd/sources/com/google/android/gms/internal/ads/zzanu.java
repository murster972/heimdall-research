package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.rtb.SignalCallbacks;

final class zzanu implements SignalCallbacks {
    private final /* synthetic */ zzanj zzdeu;

    zzanu(zzann zzann, zzanj zzanj) {
        this.zzdeu = zzanj;
    }

    public final void onFailure(String str) {
        try {
            this.zzdeu.onFailure(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final void onSuccess(String str) {
        try {
            this.zzdeu.zzdn(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
