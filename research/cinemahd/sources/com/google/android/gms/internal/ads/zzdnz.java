package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdnz extends zzdrt<zzdnz, zza> implements zzdtg {
    private static volatile zzdtn<zzdnz> zzdz;
    /* access modifiers changed from: private */
    public static final zzdnz zzhet;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;

    public static final class zza extends zzdrt.zzb<zzdnz, zza> implements zzdtg {
        private zza() {
            super(zzdnz.zzhet);
        }

        public final zza zzbc(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdnz) this.zzhmp).zzs(zzdqk);
            return this;
        }

        public final zza zzex(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdnz) this.zzhmp).setVersion(0);
            return this;
        }

        /* synthetic */ zza(zzdoa zzdoa) {
            this();
        }
    }

    static {
        zzdnz zzdnz = new zzdnz();
        zzhet = zzdnz;
        zzdrt.zza(zzdnz.class, zzdnz);
    }

    private zzdnz() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public static zza zzawx() {
        return (zza) zzhet.zzazt();
    }

    public static zzdnz zzbb(zzdqk zzdqk) throws zzdse {
        return (zzdnz) zzdrt.zza(zzhet, zzdqk);
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdoa.zzdk[i - 1]) {
            case 1:
                return new zzdnz();
            case 2:
                return new zza((zzdoa) null);
            case 3:
                return zzdrt.zza((zzdte) zzhet, "\u0000\u0002\u0000\u0000\u0001\u0003\u0002\u0000\u0000\u0000\u0001\u000b\u0003\n", new Object[]{"zzhaa", "zzhab"});
            case 4:
                return zzhet;
            case 5:
                zzdtn<zzdnz> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdnz.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhet);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }
}
