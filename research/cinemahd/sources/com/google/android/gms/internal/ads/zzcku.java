package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import org.json.JSONArray;
import org.json.JSONObject;

public final class zzcku implements zzckr<zzbwk> {
    private final zzdhd zzfov;
    private final zzbvm zzfzt;
    private final zzbyq zzfzu;

    public zzcku(zzbvm zzbvm, zzdhd zzdhd, zzbyq zzbyq) {
        this.zzfzt = zzbvm;
        this.zzfov = zzdhd;
        this.zzfzu = zzbyq;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        zzczp zzczp = zzczl.zzglo;
        return (zzczp == null || zzczp.zzfka == null) ? false : true;
    }

    public final zzdhe<List<zzdhe<zzbwk>>> zzb(zzczt zzczt, zzczl zzczl) {
        zzdhe<zzcaj> zzaou = this.zzfzt.zzade().zzaou();
        this.zzfzt.zzade().zzb(zzaou);
        return zzdgs.zzb(zzdgs.zzb(zzaou, new zzckt(this, zzczl), (Executor) this.zzfov), new zzckw(this, zzczt, zzczl), (Executor) this.zzfov);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbwk zza(zzdhe zzdhe, zzdhe zzdhe2, zzczt zzczt, zzczl zzczl, JSONObject jSONObject) throws Exception {
        zzbws zzbws = (zzbws) zzdhe.get();
        zzcaj zzcaj = (zzcaj) zzdhe2.get();
        zzbwu zza = this.zzfzt.zza(new zzbmt(zzczt, zzczl, (String) null), new zzbxe(zzbws), new zzbvy(jSONObject, zzcaj));
        zza.zzado().zzakr();
        zza.zzadp().zzb(zzcaj);
        zza.zzadq().zzl(zzbws.zzajf());
        return zza.zzadn();
    }

    private final zzdhe<zzbwk> zzb(zzczt zzczt, zzczl zzczl, JSONObject jSONObject) {
        zzdhe<zzcaj> zzaou = this.zzfzt.zzade().zzaou();
        zzdhe<zzbws> zza = this.zzfzu.zza(zzczt, zzczl, jSONObject);
        return zzdgs.zzb((zzdhe<? extends V>[]) new zzdhe[]{zzaou, zza}).zza(new zzckx(this, zza, zzaou, zzczt, zzczl, jSONObject), this.zzfov);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzczt zzczt, zzczl zzczl, JSONArray jSONArray) throws Exception {
        if (jSONArray.length() == 0) {
            return zzdgs.zzk(new zzcfb(3));
        }
        if (zzczt.zzgmh.zzfgl.zzgdu <= 1) {
            return zzdgs.zzb(zzb(zzczt, zzczl, jSONArray.getJSONObject(0)), zzcky.zzdoq, (Executor) this.zzfov);
        }
        int length = jSONArray.length();
        this.zzfzt.zzade().zzdm(Math.min(length, zzczt.zzgmh.zzfgl.zzgdu));
        ArrayList arrayList = new ArrayList(zzczt.zzgmh.zzfgl.zzgdu);
        for (int i = 0; i < zzczt.zzgmh.zzfgl.zzgdu; i++) {
            if (i < length) {
                arrayList.add(zzb(zzczt, zzczl, jSONArray.getJSONObject(i)));
            } else {
                arrayList.add(zzdgs.zzk(new zzcfb(3)));
            }
        }
        return zzdgs.zzaj(arrayList);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzczl zzczl, zzcaj zzcaj) throws Exception {
        JSONObject zza = zzaxs.zza("isNonagon", (Object) true);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("response", zzczl.zzglo.zzfka);
        jSONObject.put("sdk_params", zza);
        return zzdgs.zzb(zzcaj.zzc("google.afma.nativeAds.preProcessJson", jSONObject), zzckv.zzbkw, (Executor) this.zzfov);
    }
}
