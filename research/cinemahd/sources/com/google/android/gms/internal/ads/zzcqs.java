package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzcqs implements zzdxg<zzcqr> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzave> zzgev;

    private zzcqs(zzdxp<Executor> zzdxp, zzdxp<zzave> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzgev = zzdxp2;
    }

    public static zzcqs zzam(zzdxp<Executor> zzdxp, zzdxp<zzave> zzdxp2) {
        return new zzcqs(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcqr(this.zzfcv.get(), this.zzgev.get());
    }
}
