package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zztt extends zzdvq<zztt> {
    public Integer zzcam = null;
    public Integer zzcan = null;
    public Integer zzcao = null;

    public zztt() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        Integer num = this.zzcam;
        if (num != null) {
            zzdvo.zzab(1, num.intValue());
        }
        Integer num2 = this.zzcan;
        if (num2 != null) {
            zzdvo.zzab(2, num2.intValue());
        }
        Integer num3 = this.zzcao;
        if (num3 != null) {
            zzdvo.zzab(3, num3.intValue());
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int zzoi = super.zzoi();
        Integer num = this.zzcam;
        if (num != null) {
            zzoi += zzdvo.zzaf(1, num.intValue());
        }
        Integer num2 = this.zzcan;
        if (num2 != null) {
            zzoi += zzdvo.zzaf(2, num2.intValue());
        }
        Integer num3 = this.zzcao;
        return num3 != null ? zzoi + zzdvo.zzaf(3, num3.intValue()) : zzoi;
    }
}
