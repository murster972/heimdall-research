package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzcgf implements zzdgf {
    private final zzcge zzfvk;

    zzcgf(zzcge zzcge) {
        this.zzfvk = zzcge;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfvk.zzn((JSONObject) obj);
    }
}
