package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzagb extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzagb> CREATOR = new zzage();
    public final byte[] data;
    public final int statusCode;
    public final boolean zzac;
    public final long zzad;
    public final String[] zzcxv;
    public final String[] zzcxw;
    public final boolean zzcxx;
    public final String zzcxy;

    zzagb(boolean z, String str, int i, byte[] bArr, String[] strArr, String[] strArr2, boolean z2, long j) {
        this.zzcxx = z;
        this.zzcxy = str;
        this.statusCode = i;
        this.data = bArr;
        this.zzcxv = strArr;
        this.zzcxw = strArr2;
        this.zzac = z2;
        this.zzad = j;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzcxx);
        SafeParcelWriter.a(parcel, 2, this.zzcxy, false);
        SafeParcelWriter.a(parcel, 3, this.statusCode);
        SafeParcelWriter.a(parcel, 4, this.data, false);
        SafeParcelWriter.a(parcel, 5, this.zzcxv, false);
        SafeParcelWriter.a(parcel, 6, this.zzcxw, false);
        SafeParcelWriter.a(parcel, 7, this.zzac);
        SafeParcelWriter.a(parcel, 8, this.zzad);
        SafeParcelWriter.a(parcel, a2);
    }
}
