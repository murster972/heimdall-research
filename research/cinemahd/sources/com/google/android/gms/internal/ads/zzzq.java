package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzzq implements zzden {
    private final Context zzcri;

    zzzq(Context context) {
        this.zzcri = context;
    }

    public final Object get() {
        return zzve.zzoy().initialize(this.zzcri);
    }
}
