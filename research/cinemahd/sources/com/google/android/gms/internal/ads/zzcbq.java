package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzi;

final class zzcbq implements zzi {
    private final /* synthetic */ zzcbn zzfri;

    zzcbq(zzcbn zzcbn) {
        this.zzfri = zzcbn;
    }

    public final void zzjv() {
        this.zzfri.zzfre.onPause();
    }

    public final void zzjw() {
        this.zzfri.zzfre.onResume();
    }
}
