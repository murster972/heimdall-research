package com.google.android.gms.internal.cast;

import android.view.animation.Interpolator;

public final class zzfl {
    public static Interpolator zzfk() {
        return zzfm.zzabt;
    }

    public static Interpolator zzfl() {
        return zzfm.zzabu;
    }

    public static Interpolator zzfm() {
        return zzfm.zzabv;
    }
}
