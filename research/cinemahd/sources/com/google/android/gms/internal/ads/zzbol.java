package com.google.android.gms.internal.ads;

public final class zzbol implements zzdxg<zzczs> {
    private final zzbod zzfhi;

    private zzbol(zzbod zzbod) {
        this.zzfhi = zzbod;
    }

    public static zzbol zzk(zzbod zzbod) {
        return new zzbol(zzbod);
    }

    public final /* synthetic */ Object get() {
        return this.zzfhi.zzahe();
    }
}
