package com.google.android.gms.internal.ads;

public final class zzaom {
    /* access modifiers changed from: private */
    public boolean zzdfz;
    /* access modifiers changed from: private */
    public boolean zzdga;
    /* access modifiers changed from: private */
    public boolean zzdgb;
    /* access modifiers changed from: private */
    public boolean zzdgc;
    /* access modifiers changed from: private */
    public boolean zzdgd;

    public final zzaom zzad(boolean z) {
        this.zzdfz = z;
        return this;
    }

    public final zzaom zzae(boolean z) {
        this.zzdga = z;
        return this;
    }

    public final zzaom zzaf(boolean z) {
        this.zzdgb = z;
        return this;
    }

    public final zzaom zzag(boolean z) {
        this.zzdgc = z;
        return this;
    }

    public final zzaom zzah(boolean z) {
        this.zzdgd = true;
        return this;
    }
}
