package com.google.android.gms.internal.ads;

final class zzdfl extends zzdeu<Object> {
    private final transient int offset;
    private final transient int size;
    private final transient Object[] zzguw;

    zzdfl(Object[] objArr, int i, int i2) {
        this.zzguw = objArr;
        this.offset = i;
        this.size = i2;
    }

    public final Object get(int i) {
        zzdei.zzs(i, this.size);
        return this.zzguw[(i * 2) + this.offset];
    }

    public final int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return true;
    }
}
