package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcrl implements zzdxg<zzcrj> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdhd> zzfcv;

    public zzcrl(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzejv = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcrj(this.zzfcv.get(), this.zzejv.get());
    }
}
