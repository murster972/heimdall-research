package com.google.android.gms.internal.ads;

final /* synthetic */ class zzahu implements Runnable {
    private final zzahr zzcyq;
    private final String zzcyr;

    zzahu(zzahr zzahr, String str) {
        this.zzcyq = zzahr;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzcyq.zzdb(this.zzcyr);
    }
}
