package com.google.android.gms.internal.ads;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class zzabn {
    private static final AtomicReference<zzabo> zzcuy = new AtomicReference<>();
    static final AtomicBoolean zzcuz = new AtomicBoolean();

    public static void zza(zzabo zzabo) {
        zzcuy.set(zzabo);
    }

    static zzabo zzqw() {
        return zzcuy.get();
    }
}
