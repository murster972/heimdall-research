package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzaby extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaby> CREATOR = new zzabx();
    public final int versionCode;
    public final int zzbjw;
    public final int zzbjx;
    public final boolean zzbjy;
    public final int zzbjz;
    public final boolean zzbkb;
    public final boolean zzcvo;
    public final zzyw zzcvp;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public zzaby(NativeAdOptions nativeAdOptions) {
        this(4, nativeAdOptions.shouldReturnUrlsForImageAssets(), nativeAdOptions.getImageOrientation(), nativeAdOptions.shouldRequestMultipleImages(), nativeAdOptions.getAdChoicesPlacement(), nativeAdOptions.getVideoOptions() != null ? new zzyw(nativeAdOptions.getVideoOptions()) : null, nativeAdOptions.zzjk(), nativeAdOptions.getMediaAspectRatio());
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.versionCode);
        SafeParcelWriter.a(parcel, 2, this.zzcvo);
        SafeParcelWriter.a(parcel, 3, this.zzbjw);
        SafeParcelWriter.a(parcel, 4, this.zzbjy);
        SafeParcelWriter.a(parcel, 5, this.zzbjz);
        SafeParcelWriter.a(parcel, 6, (Parcelable) this.zzcvp, i, false);
        SafeParcelWriter.a(parcel, 7, this.zzbkb);
        SafeParcelWriter.a(parcel, 8, this.zzbjx);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzaby(int i, boolean z, int i2, boolean z2, int i3, zzyw zzyw, boolean z3, int i4) {
        this.versionCode = i;
        this.zzcvo = z;
        this.zzbjw = i2;
        this.zzbjy = z2;
        this.zzbjz = i3;
        this.zzcvp = zzyw;
        this.zzbkb = z3;
        this.zzbjx = i4;
    }
}
