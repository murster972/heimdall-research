package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcqp implements Callable {
    private final zzcqm zzgez;

    zzcqp(zzcqm zzcqm) {
        this.zzgez = zzcqm;
    }

    public final Object call() {
        return this.zzgez.zzane();
    }
}
