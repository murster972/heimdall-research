package com.google.android.gms.internal.ads;

final class zzbgw implements zzbjz {
    private zzbod zzelr;
    private zzczt zzelt;
    private final /* synthetic */ zzbgr zzerr;
    private zzbrm zzers;
    private zzcxw zzert;

    private zzbgw(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    @Deprecated
    public final /* synthetic */ zzbjz zza(zzbkf zzbkf) {
        zzdxm.checkNotNull(zzbkf);
        return this;
    }

    public final /* synthetic */ Object zzadg() {
        zzdxm.zza(this.zzers, zzbrm.class);
        zzdxm.zza(this.zzelr, zzbod.class);
        return new zzbgz(this.zzerr, new zzbnb(), new zzdai(), new zzbny(), new zzcee(), this.zzers, this.zzelr, new zzdaq(), this.zzelt, this.zzert);
    }

    public final /* synthetic */ zzbjz zzb(zzbod zzbod) {
        this.zzelr = (zzbod) zzdxm.checkNotNull(zzbod);
        return this;
    }

    public final /* synthetic */ zzboe zza(zzcxw zzcxw) {
        this.zzert = zzcxw;
        return this;
    }

    public final /* synthetic */ zzbjz zzb(zzbrm zzbrm) {
        this.zzers = (zzbrm) zzdxm.checkNotNull(zzbrm);
        return this;
    }

    public final /* synthetic */ zzboe zza(zzczt zzczt) {
        this.zzelt = zzczt;
        return this;
    }
}
