package com.google.android.gms.internal.ads;

public final class zzqw {
    final long value;
    final int zzbqg;
    final String zzbqn;

    zzqw(long j, String str, int i) {
        this.value = j;
        this.zzbqn = str;
        this.zzbqg = i;
    }

    public final boolean equals(Object obj) {
        if (obj != null && (obj instanceof zzqw)) {
            zzqw zzqw = (zzqw) obj;
            if (zzqw.value == this.value && zzqw.zzbqg == this.zzbqg) {
                return true;
            }
            return false;
        }
        return false;
    }

    public final int hashCode() {
        return (int) this.value;
    }
}
