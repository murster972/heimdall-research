package com.google.android.gms.internal.ads;

import android.content.Context;
import java.io.InputStream;
import java.util.concurrent.Future;

public final class zzsn {
    public static Future<InputStream> zza(Context context, zzry zzry) {
        return new zzse(context).zzb(zzry);
    }
}
