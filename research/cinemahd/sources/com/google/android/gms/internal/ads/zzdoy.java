package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdpb;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;

public final class zzdoy<T_WRAPPER extends zzdpb<T_ENGINE>, T_ENGINE> {
    private static final Logger logger = Logger.getLogger(zzdoy.class.getName());
    private static final List<Provider> zzhgf;
    public static final zzdoy<zzdpa, Cipher> zzhgg = new zzdoy<>(new zzdpa());
    public static final zzdoy<zzdpe, Mac> zzhgh = new zzdoy<>(new zzdpe());
    private static final zzdoy<zzdpg, Signature> zzhgi = new zzdoy<>(new zzdpg());
    private static final zzdoy<zzdph, MessageDigest> zzhgj = new zzdoy<>(new zzdph());
    public static final zzdoy<zzdpd, KeyAgreement> zzhgk = new zzdoy<>(new zzdpd());
    public static final zzdoy<zzdpf, KeyPairGenerator> zzhgl = new zzdoy<>(new zzdpf());
    public static final zzdoy<zzdpc, KeyFactory> zzhgm = new zzdoy<>(new zzdpc());
    private T_WRAPPER zzhgn;
    private List<Provider> zzhgo = zzhgf;
    private boolean zzhgp = true;

    static {
        if (zzdpp.zzaxh()) {
            String[] strArr = {"GmsCore_OpenSSL", "AndroidOpenSSL"};
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 2; i++) {
                String str = strArr[i];
                Provider provider = Security.getProvider(str);
                if (provider != null) {
                    arrayList.add(provider);
                } else {
                    logger.logp(Level.INFO, "com.google.crypto.tink.subtle.EngineFactory", "toProviderList", String.format("Provider %s not available", new Object[]{str}));
                }
            }
            zzhgf = arrayList;
        } else {
            zzhgf = new ArrayList();
        }
    }

    private zzdoy(T_WRAPPER t_wrapper) {
        this.zzhgn = t_wrapper;
    }

    public final T_ENGINE zzhd(String str) throws GeneralSecurityException {
        Exception exc = null;
        for (Provider zza : this.zzhgo) {
            try {
                return this.zzhgn.zza(str, zza);
            } catch (Exception e) {
                if (exc == null) {
                    exc = e;
                }
            }
        }
        if (this.zzhgp) {
            return this.zzhgn.zza(str, (Provider) null);
        }
        throw new GeneralSecurityException("No good Provider found.", exc);
    }
}
