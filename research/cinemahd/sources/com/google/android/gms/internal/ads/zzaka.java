package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzaka implements zzajw {
    static final zzajw zzdas = new zzaka();

    private zzaka() {
    }

    public final Object zzd(JSONObject jSONObject) {
        return zzajx.zze(jSONObject);
    }
}
