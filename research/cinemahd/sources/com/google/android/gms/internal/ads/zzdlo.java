package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdlo extends zzdrt<zzdlo, zza> implements zzdtg {
    private static volatile zzdtn<zzdlo> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlo zzhav;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;
    private zzdls zzhau;

    public static final class zza extends zzdrt.zzb<zzdlo, zza> implements zzdtg {
        private zza() {
            super(zzdlo.zzhav);
        }

        public final zza zzac(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlo) this.zzhmp).zzs(zzdqk);
            return this;
        }

        public final zza zzb(zzdls zzdls) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlo) this.zzhmp).zza(zzdls);
            return this;
        }

        public final zza zzef(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlo) this.zzhmp).setVersion(0);
            return this;
        }

        /* synthetic */ zza(zzdlp zzdlp) {
            this();
        }
    }

    static {
        zzdlo zzdlo = new zzdlo();
        zzhav = zzdlo;
        zzdrt.zza(zzdlo.class, zzdlo);
    }

    private zzdlo() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdls zzdls) {
        zzdls.getClass();
        this.zzhau = zzdls;
    }

    public static zzdlo zzab(zzdqk zzdqk) throws zzdse {
        return (zzdlo) zzdrt.zza(zzhav, zzdqk);
    }

    public static zza zzatr() {
        return (zza) zzhav.zzazt();
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }

    public final zzdls zzatq() {
        zzdls zzdls = this.zzhau;
        return zzdls == null ? zzdls.zzatu() : zzdls;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlp.zzdk[i - 1]) {
            case 1:
                return new zzdlo();
            case 2:
                return new zza((zzdlp) null);
            case 3:
                return zzdrt.zza((zzdte) zzhav, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", new Object[]{"zzhaa", "zzhau", "zzhab"});
            case 4:
                return zzhav;
            case 5:
                zzdtn<zzdlo> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlo.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhav);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
