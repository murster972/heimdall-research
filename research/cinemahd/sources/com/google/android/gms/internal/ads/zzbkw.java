package com.google.android.gms.internal.ads;

public final class zzbkw implements zzdxg<zzbkk> {
    private final zzdxp<zzbkm> zzexx;
    private final zzbkn zzfen;

    public zzbkw(zzbkn zzbkn, zzdxp<zzbkm> zzdxp) {
        this.zzfen = zzbkn;
        this.zzexx = zzdxp;
    }

    public static zzbkk zza(zzbkn zzbkn, Object obj) {
        return (zzbkk) zzdxm.zza((zzbkm) obj, "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfen, this.zzexx.get());
    }
}
