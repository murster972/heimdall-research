package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

public final class zzccu implements zzdxg<String> {
    private static final zzccu zzfrx = new zzccu();

    public static zzccu zzala() {
        return zzfrx;
    }

    public final /* synthetic */ Object get() {
        zzq.zzkq();
        return (String) zzdxm.zza(zzawb.zzwk(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
