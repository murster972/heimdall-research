package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzcdn implements zzdxg<zzbsu<zzbqb>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzcdj> zzfdd;

    private zzcdn(zzdxp<zzcdj> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcdn zzx(zzdxp<zzcdj> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcdn(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
