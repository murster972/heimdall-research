package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.reward.AdMetadataListener;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public final class zzxn {
    private final zzuh zzaba;
    private AppEventListener zzbkj;
    private boolean zzbld;
    private zzvu zzbqy;
    private String zzbqz;
    private final zzakz zzbrb;
    private zzty zzcbt;
    private AdListener zzcbw;
    private AdMetadataListener zzcbx;
    private OnCustomRenderedAdLoadedListener zzcen;
    private RewardedVideoAdListener zzceu;
    private boolean zzcev;
    private final Context zzup;

    public zzxn(Context context) {
        this(context, zzuh.zzccn, (PublisherInterstitialAd) null);
    }

    private final void zzcn(String str) {
        if (this.zzbqy == null) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 63);
            sb.append("The ad unit ID must be set on InterstitialAd before ");
            sb.append(str);
            sb.append(" is called.");
            throw new IllegalStateException(sb.toString());
        }
    }

    public final AdListener getAdListener() {
        return this.zzcbw;
    }

    public final Bundle getAdMetadata() {
        try {
            if (this.zzbqy != null) {
                return this.zzbqy.getAdMetadata();
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
        return new Bundle();
    }

    public final String getAdUnitId() {
        return this.zzbqz;
    }

    public final AppEventListener getAppEventListener() {
        return this.zzbkj;
    }

    public final String getMediationAdapterClassName() {
        try {
            if (this.zzbqy != null) {
                return this.zzbqy.zzka();
            }
            return null;
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
            return null;
        }
    }

    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.zzcen;
    }

    public final boolean isLoaded() {
        try {
            if (this.zzbqy == null) {
                return false;
            }
            return this.zzbqy.isReady();
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
            return false;
        }
    }

    public final boolean isLoading() {
        try {
            if (this.zzbqy == null) {
                return false;
            }
            return this.zzbqy.isLoading();
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
            return false;
        }
    }

    public final void setAdListener(AdListener adListener) {
        try {
            this.zzcbw = adListener;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzvh) adListener != null ? new zzuc(adListener) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void setAdMetadataListener(AdMetadataListener adMetadataListener) {
        try {
            this.zzcbx = adMetadataListener;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzvx) adMetadataListener != null ? new zzud(adMetadataListener) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void setAdUnitId(String str) {
        if (this.zzbqz == null) {
            this.zzbqz = str;
            return;
        }
        throw new IllegalStateException("The ad unit ID can only be set once on InterstitialAd.");
    }

    public final void setAppEventListener(AppEventListener appEventListener) {
        try {
            this.zzbkj = appEventListener;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzwc) appEventListener != null ? new zzul(appEventListener) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void setImmersiveMode(boolean z) {
        try {
            this.zzbld = z;
            if (this.zzbqy != null) {
                this.zzbqy.setImmersiveMode(z);
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        try {
            this.zzcen = onCustomRenderedAdLoadedListener;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzaak) onCustomRenderedAdLoadedListener != null ? new zzaal(onCustomRenderedAdLoadedListener) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void setRewardedVideoAdListener(RewardedVideoAdListener rewardedVideoAdListener) {
        try {
            this.zzceu = rewardedVideoAdListener;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzaro) rewardedVideoAdListener != null ? new zzarv(rewardedVideoAdListener) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void show() {
        try {
            zzcn("show");
            this.zzbqy.showInterstitial();
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void zza(zzxj zzxj) {
        try {
            if (this.zzbqy == null) {
                if (this.zzbqz == null) {
                    zzcn("loadAd");
                }
                zzuj zzol = this.zzcev ? zzuj.zzol() : new zzuj();
                zzup zzov = zzve.zzov();
                Context context = this.zzup;
                this.zzbqy = (zzvu) new zzuv(zzov, context, zzol, this.zzbqz, this.zzbrb).zzd(context, false);
                if (this.zzcbw != null) {
                    this.zzbqy.zza((zzvh) new zzuc(this.zzcbw));
                }
                if (this.zzcbt != null) {
                    this.zzbqy.zza((zzvg) new zztx(this.zzcbt));
                }
                if (this.zzcbx != null) {
                    this.zzbqy.zza((zzvx) new zzud(this.zzcbx));
                }
                if (this.zzbkj != null) {
                    this.zzbqy.zza((zzwc) new zzul(this.zzbkj));
                }
                if (this.zzcen != null) {
                    this.zzbqy.zza((zzaak) new zzaal(this.zzcen));
                }
                if (this.zzceu != null) {
                    this.zzbqy.zza((zzaro) new zzarv(this.zzceu));
                }
                this.zzbqy.setImmersiveMode(this.zzbld);
            }
            if (this.zzbqy.zza(zzuh.zza(this.zzup, zzxj))) {
                this.zzbrb.zzf(zzxj.zzpq());
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }

    public final void zzd(boolean z) {
        this.zzcev = true;
    }

    public zzxn(Context context, PublisherInterstitialAd publisherInterstitialAd) {
        this(context, zzuh.zzccn, publisherInterstitialAd);
    }

    private zzxn(Context context, zzuh zzuh, PublisherInterstitialAd publisherInterstitialAd) {
        this.zzbrb = new zzakz();
        this.zzup = context;
        this.zzaba = zzuh;
    }

    public final void zza(zzty zzty) {
        try {
            this.zzcbt = zzty;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzvg) zzty != null ? new zztx(zzty) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#008 Must be called on the main UI thread.", e);
        }
    }
}
