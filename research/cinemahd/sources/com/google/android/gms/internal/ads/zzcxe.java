package com.google.android.gms.internal.ads;

import java.util.Iterator;

final /* synthetic */ class zzcxe implements zzdgf {
    private final zzcxf zzgjm;
    private final zzdbi zzgjn;
    private final zzbmz zzgjo;

    zzcxe(zzcxf zzcxf, zzdbi zzdbi, zzbmz zzbmz) {
        this.zzgjm = zzcxf;
        this.zzgjn = zzdbi;
        this.zzgjo = zzbmz;
    }

    public final zzdhe zzf(Object obj) {
        zzdbi zzdbi = this.zzgjn;
        zzbmz zzbmz = this.zzgjo;
        zzczt zzczt = (zzczt) obj;
        zzdbi.zzelt = zzczt;
        Iterator<zzczl> it2 = zzczt.zzgmi.zzgme.iterator();
        boolean z = false;
        boolean z2 = false;
        loop0:
        while (true) {
            if (!it2.hasNext()) {
                z = z2;
                break;
            }
            Iterator<String> it3 = it2.next().zzgli.iterator();
            while (true) {
                if (it3.hasNext()) {
                    if (!it3.next().contains("FirstPartyRenderer")) {
                        break loop0;
                    }
                    z2 = true;
                }
            }
        }
        if (!z) {
            return zzdgs.zzaj(null);
        }
        return zzbmz.zza(zzdgs.zzaj(zzczt));
    }
}
