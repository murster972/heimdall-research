package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzcty;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class zzcsx<S extends zzcty<?>> implements zzcub<S> {
    private final ScheduledExecutorService zzfdi;
    private final zzcub<S> zzgfn;
    private final long zzgge;

    public zzcsx(zzcub<S> zzcub, long j, ScheduledExecutorService scheduledExecutorService) {
        this.zzgfn = zzcub;
        this.zzgge = j;
        this.zzfdi = scheduledExecutorService;
    }

    public final zzdhe<S> zzanc() {
        zzdhe<S> zzanc = this.zzgfn.zzanc();
        long j = this.zzgge;
        if (j > 0) {
            zzanc = zzdgs.zza(zzanc, j, TimeUnit.MILLISECONDS, this.zzfdi);
        }
        return zzdgs.zzb(zzanc, Throwable.class, zzcsw.zzbkw, zzazd.zzdwj);
    }
}
