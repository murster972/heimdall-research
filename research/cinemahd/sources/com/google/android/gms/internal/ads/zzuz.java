package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.HashMap;

final class zzuz extends zzvb<zzacp> {
    private final /* synthetic */ zzup zzcdi;
    private final /* synthetic */ View zzcdk;
    private final /* synthetic */ HashMap zzcdl;
    private final /* synthetic */ HashMap zzcdm;

    zzuz(zzup zzup, View view, HashMap hashMap, HashMap hashMap2) {
        this.zzcdi = zzup;
        this.zzcdk = view;
        this.zzcdl = hashMap;
        this.zzcdm = hashMap2;
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.a(this.zzcdk), ObjectWrapper.a(this.zzcdl), ObjectWrapper.a(this.zzcdm));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.zzcdk.getContext(), "native_ad_view_holder_delegate");
        return new zzyk();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzcde.zzb(this.zzcdk, this.zzcdl, this.zzcdm);
    }
}
