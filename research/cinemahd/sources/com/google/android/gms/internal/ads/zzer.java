package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import java.lang.ref.WeakReference;

public final class zzer implements Application.ActivityLifecycleCallbacks, View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener {
    private static final Handler zzyu = new Handler(Looper.getMainLooper());
    private final zzei zzuv;
    private Application zzxa;
    private final Context zzyv;
    private final PowerManager zzyw;
    private final KeyguardManager zzyx;
    private BroadcastReceiver zzyy;
    private WeakReference<ViewTreeObserver> zzyz;
    private WeakReference<View> zzza;
    private zzdx zzzb;
    private byte zzzc = -1;
    private int zzzd = -1;
    private long zzze = -3;

    public zzer(zzei zzei, View view) {
        this.zzuv = zzei;
        this.zzyv = zzei.zzup;
        this.zzyw = (PowerManager) this.zzyv.getSystemService("power");
        this.zzyx = (KeyguardManager) this.zzyv.getSystemService("keyguard");
        Context context = this.zzyv;
        if (context instanceof Application) {
            this.zzxa = (Application) context;
            this.zzzb = new zzdx((Application) context, this);
        }
        zzd(view);
    }

    private final void zza(Activity activity, int i) {
        Window window;
        if (this.zzza != null && (window = activity.getWindow()) != null) {
            View peekDecorView = window.peekDecorView();
            View view = (View) this.zzza.get();
            if (view != null && peekDecorView != null && view.getRootView() == peekDecorView.getRootView()) {
                this.zzzd = i;
            }
        }
    }

    private final void zzck() {
        zzyu.post(new zzeu(this));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0066, code lost:
        if (r7 == false) goto L_0x0069;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzcm() {
        /*
            r9 = this;
            java.lang.ref.WeakReference<android.view.View> r0 = r9.zzza
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            java.lang.Object r0 = r0.get()
            android.view.View r0 = (android.view.View) r0
            r1 = -1
            r2 = -3
            if (r0 != 0) goto L_0x0015
            r9.zzze = r2
            r9.zzzc = r1
            return
        L_0x0015:
            int r4 = r0.getVisibility()
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L_0x001f
            r4 = 1
            goto L_0x0020
        L_0x001f:
            r4 = 0
        L_0x0020:
            boolean r7 = r0.isShown()
            if (r7 != 0) goto L_0x0029
            r4 = r4 | 2
            byte r4 = (byte) r4
        L_0x0029:
            android.os.PowerManager r7 = r9.zzyw
            if (r7 == 0) goto L_0x0036
            boolean r7 = r7.isScreenOn()
            if (r7 != 0) goto L_0x0036
            r4 = r4 | 4
            byte r4 = (byte) r4
        L_0x0036:
            com.google.android.gms.internal.ads.zzei r7 = r9.zzuv
            boolean r7 = r7.zzce()
            if (r7 != 0) goto L_0x006a
            android.app.KeyguardManager r7 = r9.zzyx
            if (r7 == 0) goto L_0x0069
            boolean r7 = r7.inKeyguardRestrictedInputMode()
            if (r7 == 0) goto L_0x0069
            android.app.Activity r7 = com.google.android.gms.internal.ads.zzep.zzc(r0)
            if (r7 == 0) goto L_0x0065
            android.view.Window r7 = r7.getWindow()
            if (r7 != 0) goto L_0x0056
            r7 = 0
            goto L_0x005a
        L_0x0056:
            android.view.WindowManager$LayoutParams r7 = r7.getAttributes()
        L_0x005a:
            if (r7 == 0) goto L_0x0065
            int r7 = r7.flags
            r8 = 524288(0x80000, float:7.34684E-40)
            r7 = r7 & r8
            if (r7 == 0) goto L_0x0065
            r7 = 1
            goto L_0x0066
        L_0x0065:
            r7 = 0
        L_0x0066:
            if (r7 == 0) goto L_0x0069
            goto L_0x006a
        L_0x0069:
            r5 = 0
        L_0x006a:
            if (r5 != 0) goto L_0x006f
            r4 = r4 | 8
            byte r4 = (byte) r4
        L_0x006f:
            android.graphics.Rect r5 = new android.graphics.Rect
            r5.<init>()
            boolean r5 = r0.getGlobalVisibleRect(r5)
            if (r5 != 0) goto L_0x007d
            r4 = r4 | 16
            byte r4 = (byte) r4
        L_0x007d:
            android.graphics.Rect r5 = new android.graphics.Rect
            r5.<init>()
            boolean r5 = r0.getLocalVisibleRect(r5)
            if (r5 != 0) goto L_0x008b
            r4 = r4 | 32
            byte r4 = (byte) r4
        L_0x008b:
            int r0 = r0.getWindowVisibility()
            int r5 = r9.zzzd
            if (r5 == r1) goto L_0x0094
            r0 = r5
        L_0x0094:
            if (r0 == 0) goto L_0x0099
            r0 = r4 | 64
            byte r4 = (byte) r0
        L_0x0099:
            byte r0 = r9.zzzc
            if (r0 == r4) goto L_0x00ad
            r9.zzzc = r4
            byte r0 = r9.zzzc
            if (r0 != 0) goto L_0x00a8
            long r0 = android.os.SystemClock.elapsedRealtime()
            goto L_0x00ab
        L_0x00a8:
            long r0 = (long) r0
            long r0 = r2 - r0
        L_0x00ab:
            r9.zzze = r0
        L_0x00ad:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzer.zzcm():void");
    }

    private final void zze(View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            this.zzyz = new WeakReference<>(viewTreeObserver);
            viewTreeObserver.addOnScrollChangedListener(this);
            viewTreeObserver.addOnGlobalLayoutListener(this);
        }
        if (this.zzyy == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.zzyy = new zzet(this);
            this.zzyv.registerReceiver(this.zzyy, intentFilter);
        }
        Application application = this.zzxa;
        if (application != null) {
            try {
                application.registerActivityLifecycleCallbacks(this.zzzb);
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(11:0|1|2|(3:4|(1:8)|9)|10|11|(1:13)|15|(3:17|18|19)|21|(3:23|24|26)(1:28)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x001d */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027 A[Catch:{ Exception -> 0x002e }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0033 A[SYNTHETIC, Splitter:B:17:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003e A[SYNTHETIC, Splitter:B:23:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzf(android.view.View r4) {
        /*
            r3 = this;
            r0 = 0
            java.lang.ref.WeakReference<android.view.ViewTreeObserver> r1 = r3.zzyz     // Catch:{ Exception -> 0x001d }
            if (r1 == 0) goto L_0x001d
            java.lang.ref.WeakReference<android.view.ViewTreeObserver> r1 = r3.zzyz     // Catch:{ Exception -> 0x001d }
            java.lang.Object r1 = r1.get()     // Catch:{ Exception -> 0x001d }
            android.view.ViewTreeObserver r1 = (android.view.ViewTreeObserver) r1     // Catch:{ Exception -> 0x001d }
            if (r1 == 0) goto L_0x001b
            boolean r2 = r1.isAlive()     // Catch:{ Exception -> 0x001d }
            if (r2 == 0) goto L_0x001b
            r1.removeOnScrollChangedListener(r3)     // Catch:{ Exception -> 0x001d }
            r1.removeGlobalOnLayoutListener(r3)     // Catch:{ Exception -> 0x001d }
        L_0x001b:
            r3.zzyz = r0     // Catch:{ Exception -> 0x001d }
        L_0x001d:
            android.view.ViewTreeObserver r4 = r4.getViewTreeObserver()     // Catch:{ Exception -> 0x002e }
            boolean r1 = r4.isAlive()     // Catch:{ Exception -> 0x002e }
            if (r1 == 0) goto L_0x002f
            r4.removeOnScrollChangedListener(r3)     // Catch:{ Exception -> 0x002e }
            r4.removeGlobalOnLayoutListener(r3)     // Catch:{ Exception -> 0x002e }
            goto L_0x002f
        L_0x002e:
        L_0x002f:
            android.content.BroadcastReceiver r4 = r3.zzyy
            if (r4 == 0) goto L_0x003a
            android.content.Context r1 = r3.zzyv     // Catch:{ Exception -> 0x0038 }
            r1.unregisterReceiver(r4)     // Catch:{ Exception -> 0x0038 }
        L_0x0038:
            r3.zzyy = r0
        L_0x003a:
            android.app.Application r4 = r3.zzxa
            if (r4 == 0) goto L_0x0043
            com.google.android.gms.internal.ads.zzdx r0 = r3.zzzb     // Catch:{ Exception -> 0x0043 }
            r4.unregisterActivityLifecycleCallbacks(r0)     // Catch:{ Exception -> 0x0043 }
        L_0x0043:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzer.zzf(android.view.View):void");
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        zza(activity, 0);
        zzcm();
    }

    public final void onActivityDestroyed(Activity activity) {
        zzcm();
    }

    public final void onActivityPaused(Activity activity) {
        zza(activity, 4);
        zzcm();
    }

    public final void onActivityResumed(Activity activity) {
        zza(activity, 0);
        zzcm();
        zzck();
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        zzcm();
    }

    public final void onActivityStarted(Activity activity) {
        zza(activity, 0);
        zzcm();
    }

    public final void onActivityStopped(Activity activity) {
        zzcm();
    }

    public final void onGlobalLayout() {
        zzcm();
    }

    public final void onScrollChanged() {
        zzcm();
    }

    public final void onViewAttachedToWindow(View view) {
        this.zzzd = -1;
        zze(view);
        zzcm();
    }

    public final void onViewDetachedFromWindow(View view) {
        this.zzzd = -1;
        zzcm();
        zzck();
        zzf(view);
    }

    public final long zzcl() {
        if (this.zzze <= -2 && this.zzza.get() == null) {
            this.zzze = -3;
        }
        return this.zzze;
    }

    /* access modifiers changed from: package-private */
    public final void zzd(View view) {
        WeakReference<View> weakReference = this.zzza;
        View view2 = weakReference != null ? (View) weakReference.get() : null;
        if (view2 != null) {
            view2.removeOnAttachStateChangeListener(this);
            zzf(view2);
        }
        this.zzza = new WeakReference<>(view);
        if (view != null) {
            if ((view.getWindowToken() == null && view.getWindowVisibility() == 8) ? false : true) {
                zze(view);
            }
            view.addOnAttachStateChangeListener(this);
            this.zzze = -2;
            return;
        }
        this.zzze = -3;
    }
}
