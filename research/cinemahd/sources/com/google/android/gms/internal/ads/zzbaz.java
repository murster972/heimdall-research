package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.ads.internal.zza;

public interface zzbaz extends zzahs, zzber, zzbes {
    Context getContext();

    void setBackgroundColor(int i);

    void zza(zzbed zzbed);

    void zza(String str, zzbcn zzbcn);

    void zza(boolean z, long j);

    void zzav(boolean z);

    zzbcn zzfe(String str);

    void zzts();

    zzbao zzyk();

    zzbed zzyl();

    zzaac zzym();

    Activity zzyn();

    zza zzyo();

    String zzyp();

    zzaab zzyq();

    zzazb zzyr();

    int zzys();

    int zzyt();

    void zzyu();
}
