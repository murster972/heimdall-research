package com.google.android.gms.internal.cast;

import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.R$attr;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.cast.framework.R$layout;
import com.google.android.gms.cast.framework.R$style;
import com.google.android.gms.cast.framework.R$styleable;

public final class zzr extends RelativeLayout implements IntroductoryOverlay {
    private Activity zzij;
    private IntroductoryOverlay.OnOverlayDismissedListener zzin;
    private final boolean zzje;
    private boolean zzjg;
    private int zzjj;
    private final zzu zzjk;

    public zzr(IntroductoryOverlay.Builder builder) {
        this(builder, (AttributeSet) null, R$attr.castIntroOverlayStyle);
    }

    /* access modifiers changed from: private */
    public final void zzaw() {
        IntroductoryOverlay.zza.a(this.zzij);
        IntroductoryOverlay.OnOverlayDismissedListener onOverlayDismissedListener = this.zzin;
        if (onOverlayDismissedListener != null) {
            onOverlayDismissedListener.a();
            this.zzin = null;
        }
        remove();
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas2 = new Canvas(createBitmap);
        canvas2.drawColor(this.zzjj);
        zzu zzu = this.zzjk;
        if (zzu != null) {
            canvas2.drawCircle((float) zzu.x, (float) zzu.y, zzu.zzjo, zzu.zzjn);
        }
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, (Paint) null);
        createBitmap.recycle();
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        if (this.zzij != null) {
            this.zzij = null;
        }
        super.onDetachedFromWindow();
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public final void remove() {
        Activity activity = this.zzij;
        if (activity != null) {
            ((ViewGroup) activity.getWindow().getDecorView()).removeView(this);
            this.zzij = null;
        }
        this.zzin = null;
    }

    public final void show() {
        Activity activity = this.zzij;
        if (activity == null || zzn.zzg(activity)) {
            return;
        }
        if (this.zzje && IntroductoryOverlay.zza.b(this.zzij)) {
            this.zzij = null;
            this.zzin = null;
        } else if (!this.zzjg) {
            this.zzjg = true;
            ((ViewGroup) this.zzij.getWindow().getDecorView()).addView(this);
        }
    }

    private zzr(IntroductoryOverlay.Builder builder, AttributeSet attributeSet, int i) {
        super(builder.b(), (AttributeSet) null, i);
        this.zzij = builder.b();
        this.zzje = builder.g();
        this.zzin = builder.e();
        TypedArray obtainStyledAttributes = this.zzij.getTheme().obtainStyledAttributes((AttributeSet) null, R$styleable.CastIntroOverlay, i, R$style.CastIntroOverlay);
        if (builder.d() != null) {
            Rect rect = new Rect();
            builder.d().getGlobalVisibleRect(rect);
            this.zzjk = new zzu((zzs) null);
            this.zzjk.x = rect.centerX();
            this.zzjk.y = rect.centerY();
            zzu zzu = this.zzjk;
            PorterDuffXfermode porterDuffXfermode = new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY);
            Paint paint = new Paint();
            paint.setColor(-1);
            paint.setAlpha(0);
            paint.setXfermode(porterDuffXfermode);
            paint.setAntiAlias(true);
            zzu.zzjn = paint;
            this.zzjk.zzjo = builder.j();
            zzu zzu2 = this.zzjk;
            if (zzu2.zzjo == 0.0f) {
                zzu2.zzjo = obtainStyledAttributes.getDimension(R$styleable.CastIntroOverlay_castFocusRadius, 0.0f);
            }
        } else {
            this.zzjk = null;
        }
        LayoutInflater.from(this.zzij).inflate(R$layout.cast_intro_overlay, this);
        this.zzjj = builder.f();
        if (this.zzjj == 0) {
            this.zzjj = obtainStyledAttributes.getColor(R$styleable.CastIntroOverlay_castBackgroundColor, Color.argb(0, 0, 0, 0));
        }
        TextView textView = (TextView) findViewById(R$id.textTitle);
        if (!TextUtils.isEmpty(builder.h())) {
            textView.setText(builder.h());
            int resourceId = obtainStyledAttributes.getResourceId(R$styleable.CastIntroOverlay_castTitleTextAppearance, 0);
            if (resourceId != 0) {
                textView.setTextAppearance(this.zzij, resourceId);
            }
        }
        String i2 = builder.i();
        i2 = TextUtils.isEmpty(i2) ? obtainStyledAttributes.getString(R$styleable.CastIntroOverlay_castButtonText) : i2;
        int color = obtainStyledAttributes.getColor(R$styleable.CastIntroOverlay_castButtonBackgroundColor, Color.argb(0, 0, 0, 0));
        Button button = (Button) findViewById(R$id.button);
        button.setText(i2);
        button.getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        int resourceId2 = obtainStyledAttributes.getResourceId(R$styleable.CastIntroOverlay_castButtonTextAppearance, 0);
        if (resourceId2 != 0) {
            button.setTextAppearance(this.zzij, resourceId2);
        }
        button.setOnClickListener(new zzs(this));
        obtainStyledAttributes.recycle();
        setFitsSystemWindows(true);
    }
}
