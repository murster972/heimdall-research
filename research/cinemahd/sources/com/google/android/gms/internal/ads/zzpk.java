package com.google.android.gms.internal.ads;

final class zzpk implements Runnable {
    private final /* synthetic */ zzpg zzbjg;
    private final /* synthetic */ int zzbjm;
    private final /* synthetic */ long zzbjn;

    zzpk(zzpg zzpg, int i, long j) {
        this.zzbjg = zzpg;
        this.zzbjm = i;
        this.zzbjn = j;
    }

    public final void run() {
        this.zzbjg.zzbjh.zze(this.zzbjm, this.zzbjn);
    }
}
