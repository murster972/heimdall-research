package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public interface zzdim {
    boolean zzgv(String str);

    zzdhx zzgw(String str) throws GeneralSecurityException;
}
