package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzaqm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaqm> CREATOR = new zzaqp();
    String zzdlz;

    public zzaqm(String str) {
        this.zzdlz = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.zzdlz, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
