package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import java.util.ArrayList;

public final class zzbiw implements zzbov, zzbpe, zzbqb, zzty {
    private final View view;
    private final zzczt zzfbl;
    private final zzdda zzfbm;
    private final zzczl zzfbs;
    private final zzdq zzfbt;
    private boolean zzfbu;
    private boolean zzfbv;
    private final Context zzup;

    public zzbiw(Context context, zzczt zzczt, zzczl zzczl, zzdda zzdda, View view2, zzdq zzdq) {
        this.zzup = context;
        this.zzfbl = zzczt;
        this.zzfbs = zzczl;
        this.zzfbm = zzdda;
        this.zzfbt = zzdq;
        this.view = view2;
    }

    public final void onAdClicked() {
        zzdda zzdda = this.zzfbm;
        zzczt zzczt = this.zzfbl;
        zzczl zzczl = this.zzfbs;
        zzdda.zza(zzczt, zzczl, zzczl.zzdbq);
    }

    public final void onAdClosed() {
    }

    public final synchronized void onAdImpression() {
        if (!this.zzfbv) {
            String str = null;
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcls)).booleanValue()) {
                str = this.zzfbt.zzbw().zza(this.zzup, this.view, (Activity) null);
            }
            this.zzfbm.zza(this.zzfbl, this.zzfbs, false, str, this.zzfbs.zzdbr);
            this.zzfbv = true;
        }
    }

    public final void onAdLeftApplication() {
    }

    public final synchronized void onAdLoaded() {
        if (this.zzfbu) {
            ArrayList arrayList = new ArrayList(this.zzfbs.zzdbr);
            arrayList.addAll(this.zzfbs.zzglk);
            this.zzfbm.zza(this.zzfbl, this.zzfbs, true, (String) null, arrayList);
        } else {
            this.zzfbm.zza(this.zzfbl, this.zzfbs, this.zzfbs.zzglm);
            this.zzfbm.zza(this.zzfbl, this.zzfbs, this.zzfbs.zzglk);
        }
        this.zzfbu = true;
    }

    public final void onAdOpened() {
    }

    public final void onRewardedVideoCompleted() {
        zzdda zzdda = this.zzfbm;
        zzczt zzczt = this.zzfbl;
        zzczl zzczl = this.zzfbs;
        zzdda.zza(zzczt, zzczl, zzczl.zzgll);
    }

    public final void onRewardedVideoStarted() {
        zzdda zzdda = this.zzfbm;
        zzczt zzczt = this.zzfbl;
        zzczl zzczl = this.zzfbs;
        zzdda.zza(zzczt, zzczl, zzczl.zzdkz);
    }

    public final void zzb(zzare zzare, String str, String str2) {
        zzdda zzdda = this.zzfbm;
        zzczt zzczt = this.zzfbl;
        zzczl zzczl = this.zzfbs;
        zzdda.zza(zzczt, zzczl, zzczl.zzdla, zzare);
    }
}
