package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdky extends zzdrt<zzdky, zza> implements zzdtg {
    private static volatile zzdtn<zzdky> zzdz;
    /* access modifiers changed from: private */
    public static final zzdky zzhad;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;
    private zzdlc zzhac;

    public static final class zza extends zzdrt.zzb<zzdky, zza> implements zzdtg {
        private zza() {
            super(zzdky.zzhad);
        }

        public final zza zzd(zzdlc zzdlc) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdky) this.zzhmp).zzc(zzdlc);
            return this;
        }

        public final zza zzec(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdky) this.zzhmp).setVersion(0);
            return this;
        }

        public final zza zzu(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdky) this.zzhmp).zzs(zzdqk);
            return this;
        }

        /* synthetic */ zza(zzdkz zzdkz) {
            this();
        }
    }

    static {
        zzdky zzdky = new zzdky();
        zzhad = zzdky;
        zzdrt.zza(zzdky.class, zzdky);
    }

    private zzdky() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public static zza zzasu() {
        return (zza) zzhad.zzazt();
    }

    /* access modifiers changed from: private */
    public final void zzc(zzdlc zzdlc) {
        zzdlc.getClass();
        this.zzhac = zzdlc;
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public static zzdky zzt(zzdqk zzdqk) throws zzdse {
        return (zzdky) zzdrt.zza(zzhad, zzdqk);
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdkz.zzdk[i - 1]) {
            case 1:
                return new zzdky();
            case 2:
                return new zza((zzdkz) null);
            case 3:
                return zzdrt.zza((zzdte) zzhad, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\n\u0003\t", new Object[]{"zzhaa", "zzhab", "zzhac"});
            case 4:
                return zzhad;
            case 5:
                zzdtn<zzdky> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdky.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhad);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }

    public final zzdlc zzast() {
        zzdlc zzdlc = this.zzhac;
        return zzdlc == null ? zzdlc.zzasy() : zzdlc;
    }
}
