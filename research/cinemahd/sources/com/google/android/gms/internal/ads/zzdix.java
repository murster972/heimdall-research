package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdit;
import java.security.GeneralSecurityException;
import java.util.Set;

final class zzdix implements zzdit.zza {
    private final /* synthetic */ zzdiu zzgyp;
    private final /* synthetic */ zzdii zzgyq;

    zzdix(zzdiu zzdiu, zzdii zzdii) {
        this.zzgyp = zzdiu;
        this.zzgyq = zzdii;
    }

    public final Set<Class<?>> zzase() {
        return this.zzgyp.zzase();
    }

    public final zzdid<?> zzasn() {
        zzdiu zzdiu = this.zzgyp;
        return new zzdir(zzdiu, this.zzgyq, zzdiu.zzasf());
    }

    public final Class<?> zzaso() {
        return this.zzgyp.getClass();
    }

    public final Class<?> zzasp() {
        return this.zzgyq.getClass();
    }

    public final <Q> zzdid<Q> zzb(Class<Q> cls) throws GeneralSecurityException {
        try {
            return new zzdir(this.zzgyp, this.zzgyq, cls);
        } catch (IllegalArgumentException e) {
            throw new GeneralSecurityException("Primitive type not supported", e);
        }
    }
}
