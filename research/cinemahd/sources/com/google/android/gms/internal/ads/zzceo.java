package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.internal.zzq;
import java.util.Collections;
import java.util.List;

public final class zzceo implements AppEventListener, zzbov, zzbow, zzbpe, zzbph, zzbqb, zzbqx, zzdcx, zzty {
    private long startTime;
    private final List<Object> zzdvu;
    private final zzcec zzfti;

    public zzceo(zzcec zzcec, zzbfx zzbfx) {
        this.zzfti = zzcec;
        this.zzdvu = Collections.singletonList(zzbfx);
    }

    public final void onAdClicked() {
        zza((Class<?>) zzty.class, "onAdClicked", new Object[0]);
    }

    public final void onAdClosed() {
        zza((Class<?>) zzbov.class, "onAdClosed", new Object[0]);
    }

    public final void onAdFailedToLoad(int i) {
        zza((Class<?>) zzbow.class, "onAdFailedToLoad", Integer.valueOf(i));
    }

    public final void onAdImpression() {
        zza((Class<?>) zzbpe.class, "onAdImpression", new Object[0]);
    }

    public final void onAdLeftApplication() {
        zza((Class<?>) zzbov.class, "onAdLeftApplication", new Object[0]);
    }

    public final void onAdLoaded() {
        long a2 = zzq.zzkx().a() - this.startTime;
        StringBuilder sb = new StringBuilder(41);
        sb.append("Ad Request Latency : ");
        sb.append(a2);
        zzavs.zzed(sb.toString());
        zza((Class<?>) zzbqb.class, "onAdLoaded", new Object[0]);
    }

    public final void onAdOpened() {
        zza((Class<?>) zzbov.class, "onAdOpened", new Object[0]);
    }

    public final void onAppEvent(String str, String str2) {
        zza((Class<?>) AppEventListener.class, "onAppEvent", str, str2);
    }

    public final void onRewardedVideoCompleted() {
        zza((Class<?>) zzbov.class, "onRewardedVideoCompleted", new Object[0]);
    }

    public final void onRewardedVideoStarted() {
        zza((Class<?>) zzbov.class, "onRewardedVideoStarted", new Object[0]);
    }

    public final void zza(zzdco zzdco, String str) {
        zza((Class<?>) zzdcp.class, "onTaskCreated", str);
    }

    public final void zzb(zzare zzare, String str, String str2) {
        zza((Class<?>) zzbov.class, "onRewarded", zzare, str, str2);
    }

    public final void zzb(zzczt zzczt) {
    }

    public final void zzbv(Context context) {
        zza((Class<?>) zzbph.class, "onPause", context);
    }

    public final void zzbw(Context context) {
        zza((Class<?>) zzbph.class, "onResume", context);
    }

    public final void zzbx(Context context) {
        zza((Class<?>) zzbph.class, "onDestroy", context);
    }

    public final void zzc(zzdco zzdco, String str) {
        zza((Class<?>) zzdcp.class, "onTaskSucceeded", str);
    }

    public final void zza(zzdco zzdco, String str, Throwable th) {
        zza((Class<?>) zzdcp.class, "onTaskFailed", str, th.getClass().getSimpleName());
    }

    public final void zzb(zzdco zzdco, String str) {
        zza((Class<?>) zzdcp.class, "onTaskStarted", str);
    }

    private final void zza(Class<?> cls, String str, Object... objArr) {
        zzcec zzcec = this.zzfti;
        List<Object> list = this.zzdvu;
        String valueOf = String.valueOf(cls.getSimpleName());
        zzcec.zza(list, valueOf.length() != 0 ? "Event-".concat(valueOf) : new String("Event-"), str, objArr);
    }

    public final void zzb(zzaqk zzaqk) {
        this.startTime = zzq.zzkx().a();
        zza((Class<?>) zzbqx.class, "onAdRequest", new Object[0]);
    }
}
