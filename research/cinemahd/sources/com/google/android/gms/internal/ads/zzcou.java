package com.google.android.gms.internal.ads;

final class zzcou implements zzdgt<zzbtu> {
    private final /* synthetic */ zzbup zzgdk;
    private final /* synthetic */ zzcor zzgdl;

    zzcou(zzcor zzcor, zzbup zzbup) {
        this.zzgdl = zzcor;
        this.zzgdk = zzbup;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzbtu zzbtu = (zzbtu) obj;
        synchronized (this.zzgdl) {
            zzdhe unused = this.zzgdl.zzgdb = null;
            zzbtu unused2 = this.zzgdl.zzgdf = zzbtu;
            zzbtu.zzagf();
        }
    }

    public final void zzb(Throwable th) {
        synchronized (this.zzgdl) {
            zzdhe unused = this.zzgdl.zzgdb = null;
            this.zzgdk.zzadd().onAdFailedToLoad(zzcfb.zzd(th));
            zzdad.zzc(th, "InterstitialAdManagerShim.onFailure");
        }
    }
}
