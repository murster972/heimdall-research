package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;

public final class zzahn {
    private final zzuh zzaba;
    private final zzvm zzabb;
    private final Context zzup;

    zzahn(Context context, zzvm zzvm) {
        this(context, zzvm, zzuh.zzccn);
    }

    private final void zza(zzxj zzxj) {
        try {
            this.zzabb.zzb(zzuh.zza(this.zzup, zzxj));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void loadAd(AdRequest adRequest) {
        zza(adRequest.zzdg());
    }

    public final void loadAd(PublisherAdRequest publisherAdRequest) {
        zza(publisherAdRequest.zzdg());
    }

    private zzahn(Context context, zzvm zzvm, zzuh zzuh) {
        this.zzup = context;
        this.zzabb = zzvm;
        this.zzaba = zzuh;
    }
}
