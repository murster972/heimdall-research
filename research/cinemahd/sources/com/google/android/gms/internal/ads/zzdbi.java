package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbmd;

public final class zzdbi<AdT extends zzbmd> {
    public zzczt zzelt;
    public zzbmz<AdT> zzgpc;
    public AdT zzgpd;
    public long zzgpe;
}
