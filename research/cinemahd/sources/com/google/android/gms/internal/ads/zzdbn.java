package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.HashMap;

public final class zzdbn {
    private HashMap<zzdbh, zzdbb> zzgph = new HashMap<>();

    public final zzdbb zzb(zzdbh zzdbh, Context context) {
        zzdbb zzdbb = this.zzgph.get(zzdbh);
        if (zzdbb != null) {
            return zzdbb;
        }
        zzdba zzdba = new zzdba(zzdbe.zza(zzdbh, context));
        this.zzgph.put(zzdbh, zzdba);
        return zzdba;
    }
}
