package com.google.android.gms.internal.ads;

final /* synthetic */ class zzdcu implements zzbrn {
    private final zzdca zzgrk;

    zzdcu(zzdca zzdca) {
        this.zzgrk = zzdca;
    }

    public final void zzp(Object obj) {
        zzdca zzdca = this.zzgrk;
        ((zzdcx) obj).zzc((zzdco) zzdca.zzaqd(), zzdca.zzaqe());
    }
}
