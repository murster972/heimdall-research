package com.google.android.gms.internal.ads;

public final class zzahl {
    private int zzbjx = 2;
    private String zzcyj = "";

    public zzahl(int i) {
        this.zzbjx = i;
    }

    public final int getMediaAspectRatio() {
        return this.zzbjx;
    }

    public final String zzrw() {
        return this.zzcyj;
    }

    public zzahl(String str) {
        this.zzcyj = str;
    }
}
