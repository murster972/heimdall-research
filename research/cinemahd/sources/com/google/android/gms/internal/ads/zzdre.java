package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzdre extends zzdqe<Double> implements zzdsb<Double>, zzdtq, RandomAccess {
    private static final zzdre zzhje;
    private int size;
    private double[] zzhjf;

    static {
        zzdre zzdre = new zzdre(new double[0], 0);
        zzhje = zzdre;
        zzdre.zzaxq();
    }

    zzdre() {
        this(new double[10], 0);
    }

    private final void zzfb(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
    }

    private final String zzfc(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        double doubleValue = ((Double) obj).doubleValue();
        zzaxr();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
        double[] dArr = this.zzhjf;
        if (i2 < dArr.length) {
            System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
        } else {
            double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.zzhjf, i, dArr2, i + 1, this.size - i);
            this.zzhjf = dArr2;
        }
        this.zzhjf[i] = doubleValue;
        this.size++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Double> collection) {
        zzaxr();
        zzdrv.checkNotNull(collection);
        if (!(collection instanceof zzdre)) {
            return super.addAll(collection);
        }
        zzdre zzdre = (zzdre) collection;
        int i = zzdre.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.zzhjf;
            if (i3 > dArr.length) {
                this.zzhjf = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(zzdre.zzhjf, 0, this.zzhjf, this.size, zzdre.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdre)) {
            return super.equals(obj);
        }
        zzdre zzdre = (zzdre) obj;
        if (this.size != zzdre.size) {
            return false;
        }
        double[] dArr = zzdre.zzhjf;
        for (int i = 0; i < this.size; i++) {
            if (Double.doubleToLongBits(this.zzhjf[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        zzfb(i);
        return Double.valueOf(this.zzhjf[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + zzdrv.zzfq(Double.doubleToLongBits(this.zzhjf[i2]));
        }
        return i;
    }

    public final boolean remove(Object obj) {
        zzaxr();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Double.valueOf(this.zzhjf[i]))) {
                double[] dArr = this.zzhjf;
                System.arraycopy(dArr, i + 1, dArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zzaxr();
        if (i2 >= i) {
            double[] dArr = this.zzhjf;
            System.arraycopy(dArr, i2, dArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        zzaxr();
        zzfb(i);
        double[] dArr = this.zzhjf;
        double d = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d);
    }

    public final int size() {
        return this.size;
    }

    public final void zzd(double d) {
        zzaxr();
        int i = this.size;
        double[] dArr = this.zzhjf;
        if (i == dArr.length) {
            double[] dArr2 = new double[(((i * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.zzhjf = dArr2;
        }
        double[] dArr3 = this.zzhjf;
        int i2 = this.size;
        this.size = i2 + 1;
        dArr3[i2] = d;
    }

    public final /* synthetic */ zzdsb zzfd(int i) {
        if (i >= this.size) {
            return new zzdre(Arrays.copyOf(this.zzhjf, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    private zzdre(double[] dArr, int i) {
        this.zzhjf = dArr;
        this.size = i;
    }

    public final /* synthetic */ Object remove(int i) {
        zzaxr();
        zzfb(i);
        double[] dArr = this.zzhjf;
        double d = dArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Double.valueOf(d);
    }

    public final /* synthetic */ boolean add(Object obj) {
        zzd(((Double) obj).doubleValue());
        return true;
    }
}
