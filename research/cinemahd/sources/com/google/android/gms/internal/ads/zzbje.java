package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbje implements zzdxg<zzbjb> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzpn> zzfcn;

    private zzbje(zzdxp<Context> zzdxp, zzdxp<zzpn> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfcn = zzdxp2;
    }

    public static zzbje zza(zzdxp<Context> zzdxp, zzdxp<zzpn> zzdxp2) {
        return new zzbje(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzbjb(this.zzejv.get(), this.zzfcn.get());
    }
}
