package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import java.util.List;

@Deprecated
public final class zzdny extends zzdrt<zzdny, zza> implements zzdtg {
    private static volatile zzdtn<zzdny> zzdz;
    /* access modifiers changed from: private */
    public static final zzdny zzhes;
    private String zzheq = "";
    private zzdsb<zzdnh> zzher = zzdrt.zzazw();

    public static final class zza extends zzdrt.zzb<zzdny, zza> implements zzdtg {
        private zza() {
            super(zzdny.zzhes);
        }

        /* synthetic */ zza(zzdnx zzdnx) {
            this();
        }
    }

    static {
        zzdny zzdny = new zzdny();
        zzhes = zzdny;
        zzdrt.zza(zzdny.class, zzdny);
    }

    private zzdny() {
    }

    public static zzdny zzawv() {
        return zzhes;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnx.zzdk[i - 1]) {
            case 1:
                return new zzdny();
            case 2:
                return new zza((zzdnx) null);
            case 3:
                return zzdrt.zza((zzdte) zzhes, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001Ȉ\u0002\u001b", new Object[]{"zzheq", "zzher", zzdnh.class});
            case 4:
                return zzhes;
            case 5:
                zzdtn<zzdny> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdny.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhes);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final List<zzdnh> zzawu() {
        return this.zzher;
    }
}
