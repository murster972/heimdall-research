package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final /* synthetic */ class zzccx implements zzsp {
    private final zzsy.zza zzfhy;

    zzccx(zzsy.zza zza) {
        this.zzfhy = zza;
    }

    public final void zza(zztu zztu) {
        zztu.zzcbb = this.zzfhy;
    }
}
