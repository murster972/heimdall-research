package com.google.android.gms.internal.ads;

import java.util.Iterator;

public abstract class zzdev<E> {
    zzdev() {
    }

    public zzdev<E> zza(Iterator<? extends E> it2) {
        while (it2.hasNext()) {
            zzae(it2.next());
        }
        return this;
    }

    public abstract zzdev<E> zzae(E e);

    public zzdev<E> zze(Iterable<? extends E> iterable) {
        for (Object zzae : iterable) {
            zzae(zzae);
        }
        return this;
    }
}
