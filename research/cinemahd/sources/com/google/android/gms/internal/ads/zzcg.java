package com.google.android.gms.internal.ads;

final class zzcg implements zzdsa {
    static final zzdsa zzew = new zzcg();

    private zzcg() {
    }

    public final boolean zzf(int i) {
        return zzce.zzk(i) != null;
    }
}
