package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

final class zzft<K> implements Iterator<Map.Entry<K, Object>> {
    private Iterator<Map.Entry<K, Object>> zza;

    public zzft(Iterator<Map.Entry<K, Object>> it2) {
        this.zza = it2;
    }

    public final boolean hasNext() {
        return this.zza.hasNext();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.zza.next();
        return next.getValue() instanceof zzfo ? new zzfq(next) : next;
    }

    public final void remove() {
        this.zza.remove();
    }
}
