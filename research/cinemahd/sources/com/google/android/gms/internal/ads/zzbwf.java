package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.common.util.Clock;
import java.util.concurrent.Executor;

public final class zzbwf implements zzdxg<zzbjq> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<zzpn> zzfld;

    public zzbwf(zzdxp<zzpn> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<Context> zzdxp3, zzdxp<Clock> zzdxp4) {
        this.zzfld = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzejv = zzdxp3;
        this.zzfcz = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return (zzbjq) zzdxm.zza(new zzbjq(this.zzfcv.get(), new zzbjb(this.zzejv.get(), this.zzfld.get()), this.zzfcz.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
