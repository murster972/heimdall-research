package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.List;
import java.util.Map;

final class zzdue extends zzdub<FieldDescriptorType, Object> {
    zzdue(int i) {
        super(i, (zzdue) null);
    }

    public final void zzaxq() {
        if (!isImmutable()) {
            for (int i = 0; i < zzbbs(); i++) {
                Map.Entry zzgw = zzgw(i);
                if (((zzdro) zzgw.getKey()).zzazq()) {
                    zzgw.setValue(Collections.unmodifiableList((List) zzgw.getValue()));
                }
            }
            for (Map.Entry entry : zzbbt()) {
                if (((zzdro) entry.getKey()).zzazq()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.zzaxq();
    }
}
