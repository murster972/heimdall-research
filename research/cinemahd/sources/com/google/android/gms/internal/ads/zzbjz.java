package com.google.android.gms.internal.ads;

public interface zzbjz extends zzboe<zzbka> {
    zzbjz zza(zzbkf zzbkf);

    zzbjz zzb(zzbod zzbod);

    zzbjz zzb(zzbrm zzbrm);
}
