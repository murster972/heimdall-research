package com.google.android.gms.internal.ads;

public interface zzdcp<E> {
    void zza(zzdca<E, ?> zzdca);

    void zza(zzdca<E, ?> zzdca, Throwable th);

    void zzb(zzdca<E, ?> zzdca);

    void zzc(zzdca<E, ?> zzdca);
}
