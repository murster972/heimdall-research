package com.google.android.gms.internal.ads;

import android.view.ViewGroup;

public class zzbkf {
    private final ViewGroup zzfdu;

    public zzbkf(ViewGroup viewGroup) {
        this.zzfdu = viewGroup;
    }

    public final ViewGroup zzagd() {
        return this.zzfdu;
    }
}
