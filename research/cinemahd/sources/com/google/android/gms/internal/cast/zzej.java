package com.google.android.gms.internal.cast;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzej extends zzem {
    private final /* synthetic */ zzeh zzaaz;
    private final /* synthetic */ String zzaba;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzej(zzeh zzeh, GoogleApiClient googleApiClient, String str) {
        super(zzeh, googleApiClient);
        this.zzaaz = zzeh;
        this.zzaba = str;
    }

    /* renamed from: zza */
    public final void doExecute(zzer zzer) throws RemoteException {
        zzer.zza(new zzen(this, zzer), this.zzaaz.zzaay, this.zzaba);
    }
}
