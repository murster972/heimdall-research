package com.google.android.gms.internal.ads;

public final class zzcpn implements zzdxg<Integer> {
    private final zzcpj zzged;

    public zzcpn(zzcpj zzcpj) {
        this.zzged = zzcpj;
    }

    public final /* synthetic */ Object get() {
        return Integer.valueOf(this.zzged.zzanb());
    }
}
