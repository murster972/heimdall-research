package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdlg extends zzdrt<zzdlg, zza> implements zzdtg {
    private static volatile zzdtn<zzdlg> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlg zzhao;
    private zzdlk zzham;
    private zzdmw zzhan;

    public static final class zza extends zzdrt.zzb<zzdlg, zza> implements zzdtg {
        private zza() {
            super(zzdlg.zzhao);
        }

        /* synthetic */ zza(zzdlh zzdlh) {
            this();
        }
    }

    static {
        zzdlg zzdlg = new zzdlg();
        zzhao = zzdlg;
        zzdrt.zza(zzdlg.class, zzdlg);
    }

    private zzdlg() {
    }

    public static zzdlg zzx(zzdqk zzdqk) throws zzdse {
        return (zzdlg) zzdrt.zza(zzhao, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlh.zzdk[i - 1]) {
            case 1:
                return new zzdlg();
            case 2:
                return new zza((zzdlh) null);
            case 3:
                return zzdrt.zza((zzdte) zzhao, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\t", new Object[]{"zzham", "zzhan"});
            case 4:
                return zzhao;
            case 5:
                zzdtn<zzdlg> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlg.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhao);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdlk zzate() {
        zzdlk zzdlk = this.zzham;
        return zzdlk == null ? zzdlk.zzatl() : zzdlk;
    }

    public final zzdmw zzatf() {
        zzdmw zzdmw = this.zzhan;
        return zzdmw == null ? zzdmw.zzavd() : zzdmw;
    }
}
