package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzrf extends IInterface {
    void zza(zzrl zzrl) throws RemoteException;

    zzvu zzdm() throws RemoteException;
}
