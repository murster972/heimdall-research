package com.google.android.gms.internal.ads;

public final class zzhb extends IllegalStateException {
    private final zzhg zzade;
    private final int zzaev;
    private final long zzaga;

    public zzhb(zzhg zzhg, int i, long j) {
        this.zzade = zzhg;
        this.zzaev = i;
        this.zzaga = j;
    }
}
