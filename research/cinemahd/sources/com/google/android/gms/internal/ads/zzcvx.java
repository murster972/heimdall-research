package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.ScheduledExecutorService;

public final class zzcvx implements zzdxg<zzcvv> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzaoz> zzeqj;
    private final zzdxp<ScheduledExecutorService> zzfpr;

    public zzcvx(zzdxp<zzaoz> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<Context> zzdxp3) {
        this.zzeqj = zzdxp;
        this.zzfpr = zzdxp2;
        this.zzejv = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcvv(this.zzeqj.get(), this.zzfpr.get(), this.zzejv.get());
    }
}
