package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.common.util.UriUtil;
import com.google.android.gms.ads.internal.zzq;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzcob {
    private final Executor executor;
    private final Map<String, zzcoi> zzgcf = new ConcurrentHashMap();
    private final Map<String, Map<String, List<zzcoi>>> zzgcg = new ConcurrentHashMap();
    private JSONObject zzgch;

    public zzcob(Executor executor2) {
        this.executor = executor2;
    }

    private static boolean zza(JSONArray jSONArray, String str) {
        if (!(jSONArray == null || str == null)) {
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    if (Pattern.compile(jSONArray.optString(i)).matcher(str).lookingAt()) {
                        return true;
                    }
                    i++;
                } catch (PatternSyntaxException e) {
                    zzq.zzku().zza(e, "RtbAdapterMap.hasAtleastOneRegexMatch");
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzamk */
    public final synchronized void zzamn() {
        JSONArray optJSONArray;
        JSONObject zzvo = zzq.zzku().zzvf().zzwa().zzvo();
        if (zzvo != null) {
            try {
                JSONArray optJSONArray2 = zzvo.optJSONArray("ad_unit_id_settings");
                this.zzgch = zzvo.optJSONObject("ad_unit_patterns");
                if (optJSONArray2 != null) {
                    for (int i = 0; i < optJSONArray2.length(); i++) {
                        JSONObject jSONObject = optJSONArray2.getJSONObject(i);
                        String optString = jSONObject.optString("ad_unit_id", "");
                        String optString2 = jSONObject.optString("format", "");
                        ArrayList arrayList = new ArrayList();
                        JSONObject optJSONObject = jSONObject.optJSONObject("mediation_config");
                        if (!(optJSONObject == null || (optJSONArray = optJSONObject.optJSONArray("ad_networks")) == null)) {
                            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                                JSONObject jSONObject2 = optJSONArray.getJSONObject(i2);
                                ArrayList arrayList2 = new ArrayList();
                                if (jSONObject2 != null) {
                                    JSONObject optJSONObject2 = jSONObject2.optJSONObject(UriUtil.DATA_SCHEME);
                                    Bundle bundle = new Bundle();
                                    if (optJSONObject2 != null) {
                                        Iterator<String> keys = optJSONObject2.keys();
                                        while (keys.hasNext()) {
                                            String next = keys.next();
                                            bundle.putString(next, optJSONObject2.optString(next, ""));
                                        }
                                    }
                                    JSONArray optJSONArray3 = jSONObject2.optJSONArray("rtb_adapters");
                                    if (optJSONArray3 != null) {
                                        ArrayList arrayList3 = new ArrayList();
                                        for (int i3 = 0; i3 < optJSONArray3.length(); i3++) {
                                            String optString3 = optJSONArray3.optString(i3, "");
                                            if (!TextUtils.isEmpty(optString3)) {
                                                arrayList3.add(optString3);
                                            }
                                        }
                                        int size = arrayList3.size();
                                        int i4 = 0;
                                        while (i4 < size) {
                                            Object obj = arrayList3.get(i4);
                                            i4++;
                                            String str = (String) obj;
                                            zzgi(str);
                                            if (this.zzgcf.get(str) != null) {
                                                arrayList2.add(new zzcoi(str, optString2, bundle));
                                            }
                                        }
                                    }
                                }
                                arrayList.addAll(arrayList2);
                            }
                        }
                        if (!TextUtils.isEmpty(optString2) && !TextUtils.isEmpty(optString)) {
                            Map map = this.zzgcg.get(optString2);
                            if (map == null) {
                                map = new ConcurrentHashMap();
                            }
                            this.zzgcg.put(optString2, map);
                            List list = (List) map.get(optString);
                            if (list == null) {
                                list = new ArrayList();
                            }
                            list.addAll(arrayList);
                            map.put(optString, list);
                        }
                    }
                }
            } catch (JSONException e) {
                zzavs.zza("Malformed config loading JSON.", e);
            }
        }
    }

    public final void zzamj() {
        zzq.zzku().zzvf().zzb(new zzcoe(this));
        this.executor.execute(new zzcod(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzamm() {
        this.executor.execute(new zzcog(this));
    }

    public final void zzgi(String str) {
        if (!TextUtils.isEmpty(str) && !this.zzgcf.containsKey(str)) {
            this.zzgcf.put(str, new zzcoi(str, "", new Bundle()));
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.util.List} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.String, java.util.List<android.os.Bundle>> zzr(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            if (r0 != 0) goto L_0x00b2
            boolean r0 = android.text.TextUtils.isEmpty(r8)
            if (r0 == 0) goto L_0x000e
            goto L_0x00b2
        L_0x000e:
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.util.List<com.google.android.gms.internal.ads.zzcoi>>> r0 = r6.zzgcg
            java.lang.Object r0 = r0.get(r7)
            java.util.Map r0 = (java.util.Map) r0
            if (r0 != 0) goto L_0x001d
            java.util.Map r7 = java.util.Collections.emptyMap()
            return r7
        L_0x001d:
            java.lang.Object r1 = r0.get(r8)
            java.util.List r1 = (java.util.List) r1
            if (r1 != 0) goto L_0x0079
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r1 = com.google.android.gms.internal.ads.zzzn.zzckp
            com.google.android.gms.internal.ads.zzzj r2 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r1 = r2.zzd(r1)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x0072
            org.json.JSONObject r1 = r6.zzgch
            if (r1 == 0) goto L_0x0072
            org.json.JSONArray r7 = r1.optJSONArray(r7)
            if (r7 == 0) goto L_0x0072
            r1 = 0
        L_0x0044:
            int r3 = r7.length()
            if (r1 >= r3) goto L_0x0072
            org.json.JSONObject r3 = r7.optJSONObject(r1)
            if (r3 == 0) goto L_0x006f
            java.lang.String r4 = "including"
            org.json.JSONArray r4 = r3.optJSONArray(r4)
            java.lang.String r5 = "excluding"
            org.json.JSONArray r5 = r3.optJSONArray(r5)
            boolean r4 = zza(r4, r8)
            if (r4 == 0) goto L_0x006f
            boolean r4 = zza(r5, r8)
            if (r4 != 0) goto L_0x006f
            java.lang.String r7 = "effective_ad_unit_id"
            java.lang.String r2 = r3.optString(r7, r2)
            goto L_0x0072
        L_0x006f:
            int r1 = r1 + 1
            goto L_0x0044
        L_0x0072:
            java.lang.Object r7 = r0.get(r2)
            r1 = r7
            java.util.List r1 = (java.util.List) r1
        L_0x0079:
            if (r1 != 0) goto L_0x0080
            java.util.Map r7 = java.util.Collections.emptyMap()
            return r7
        L_0x0080:
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.util.Iterator r8 = r1.iterator()
        L_0x0089:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x00b1
            java.lang.Object r0 = r8.next()
            com.google.android.gms.internal.ads.zzcoi r0 = (com.google.android.gms.internal.ads.zzcoi) r0
            java.lang.String r1 = r0.zzfge
            boolean r2 = r7.containsKey(r1)
            if (r2 != 0) goto L_0x00a5
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r7.put(r1, r2)
        L_0x00a5:
            java.lang.Object r1 = r7.get(r1)
            java.util.List r1 = (java.util.List) r1
            android.os.Bundle r0 = r0.zzeig
            r1.add(r0)
            goto L_0x0089
        L_0x00b1:
            return r7
        L_0x00b2:
            java.util.Map r7 = java.util.Collections.emptyMap()
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcob.zzr(java.lang.String, java.lang.String):java.util.Map");
    }
}
