package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final /* synthetic */ class zzbrg implements zzbrn {
    private final zzsy.zza zzfhy;

    zzbrg(zzsy.zza zza) {
        this.zzfhy = zza;
    }

    public final void zzp(Object obj) {
        ((zzbri) obj).zzc(this.zzfhy);
    }
}
