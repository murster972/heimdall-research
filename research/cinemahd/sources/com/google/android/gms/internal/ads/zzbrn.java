package com.google.android.gms.internal.ads;

public interface zzbrn<ListenerT> {
    void zzp(ListenerT listenert) throws Exception;
}
