package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzq;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzcgw extends zzaqa {
    private final Executor zzfci;
    private final zzarb zzfwb;
    private final zzaqy zzfwc;
    private final zzbij zzfwd;
    private final HashMap<String, zzchh> zzfwe;
    private final Context zzup;

    public zzcgw(Context context, Executor executor, zzarb zzarb, zzbij zzbij, zzaqy zzaqy, HashMap<String, zzchh> hashMap) {
        zzzn.initialize(context);
        this.zzup = context;
        this.zzfci = executor;
        this.zzfwb = zzarb;
        this.zzfwc = zzaqy;
        this.zzfwd = zzbij;
        this.zzfwe = hashMap;
    }

    private static zzdhe<JSONObject> zza(zzaqk zzaqk, zzdcr zzdcr, zzcut zzcut) {
        zzcha zzcha = new zzcha(zzcut);
        return zzdcr.zza(zzdco.GMS_SIGNALS, zzdgs.zzaj(zzaqk.zzdlu)).zza(zzcha).zzb(zzcgz.zzfun).zzaqg();
    }

    public final zzapx zza(zzapv zzapv) throws RemoteException {
        return null;
    }

    public final void zza(zzapv zzapv, zzaqc zzaqc) throws RemoteException {
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzalv() {
        zzazh.zza(this.zzfwc.zzuh(), "persistFlags");
    }

    public final void zzb(zzaqk zzaqk, zzaqe zzaqe) {
        zzdhe zzdhe;
        zzakc zza = zzq.zzld().zza(this.zzup, zzazb.zzxm());
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcpp)).booleanValue()) {
            zzdhe = zzdgs.zzk(new Exception("Signal collection disabled."));
        } else {
            zzcut zza2 = this.zzfwd.zza(zzaqk);
            zzcua<JSONObject> zzadu = this.zzfwd.zza(zzaqk).zzadu();
            zzdhe = zza2.zzadv().zza(zzdco.GET_SIGNALS, zzdgs.zzaj(zzaqk.zzdlu)).zza(new zzchd(zzadu)).zzw(zzdco.JS_SIGNALS).zza(zza.zza("google.afma.request.getSignals", zzajx.zzdaq, zzajx.zzdar)).zzaqg();
        }
        zza((zzdhe<InputStream>) zzdhe, zzaqe);
    }

    public final void zzc(zzaqk zzaqk, zzaqe zzaqe) {
        zzdhe zzdhe;
        if (!zzabd.zzcty.get().booleanValue()) {
            zzdhe = zzdgs.zzk(new Exception("Split request is disabled."));
        } else {
            zzdbe zzdbe = zzaqk.zzdlx;
            if (zzdbe == null) {
                zzdhe = zzdgs.zzk(new Exception("Pool configuration missing from request."));
            } else if (zzdbe.zzgoi == 0 || zzdbe.zzgoj == 0) {
                zzdhe = zzdgs.zzk(new Exception("Caching is disabled."));
            } else {
                zzakc zza = zzq.zzld().zza(this.zzup, zzazb.zzxm());
                zzcut zza2 = this.zzfwd.zza(zzaqk);
                zzdcr zzadv = zza2.zzadv();
                zzdhe<JSONObject> zza3 = zza(zzaqk, zzadv, zza2);
                zzdhe<zzaqq> zza4 = zza(zza3, zzadv, zza);
                zzdhe = zzadv.zza(zzdco.GET_URL_AND_CACHE_KEY, (zzdhe<?>[]) new zzdhe[]{zza3, zza4}).zzb(new zzche(this, zza4, zza3)).zzaqg();
            }
        }
        zza((zzdhe<InputStream>) zzdhe, zzaqe);
    }

    public final zzdhe<InputStream> zzh(zzaqk zzaqk) {
        zzakc zza = zzq.zzld().zza(this.zzup, zzazb.zzxm());
        zzcut zza2 = this.zzfwd.zza(zzaqk);
        zzaju<I, O> zza3 = zza.zza("google.afma.response.normalize", zzchk.zzfwm, zzajx.zzdar);
        zzchl zzchl = new zzchl(this.zzup, zzaqk.zzdij.zzbma, this.zzfwb, zzaqk.zzdjj);
        zzdcr zzadv = zza2.zzadv();
        zzchh zzchh = null;
        if (!zzabd.zzcty.get().booleanValue()) {
            String str = zzaqk.zzdly;
            if (str != null && !str.isEmpty()) {
                zzavs.zzed("Request contained a PoolKey but split request is disabled.");
            }
        } else {
            String str2 = zzaqk.zzdly;
            if (str2 != null && !str2.isEmpty() && (zzchh = this.zzfwe.remove(zzaqk.zzdly)) == null) {
                zzavs.zzed("Request contained a PoolKey but no matching parameters were found.");
            }
        }
        if (zzchh == null) {
            zzdhe<JSONObject> zza4 = zza(zzaqk, zzadv, zza2);
            zzdhe<zzaqq> zza5 = zza(zza4, zzadv, zza);
            zzdca zzaqg = zzadv.zza(zzdco.HTTP, (zzdhe<?>[]) new zzdhe[]{zza5, zza4}).zzb(new zzcgv(zza4, zza5)).zzb(zzchl).zzaqg();
            return zzadv.zza(zzdco.PRE_PROCESS, (zzdhe<?>[]) new zzdhe[]{zza4, zza5, zzaqg}).zzb(new zzcgy(zzaqg, zza4, zza5)).zza(zza3).zzaqg();
        }
        zzdca zzaqg2 = zzadv.zza(zzdco.HTTP, zzdgs.zzaj(new zzcho(zzchh.zzfwj, zzchh.zzfwi))).zzb(zzchl).zzaqg();
        zzdhe zzaj = zzdgs.zzaj(zzchh);
        return zzadv.zza(zzdco.PRE_PROCESS, (zzdhe<?>[]) new zzdhe[]{zzaqg2, zzaj}).zzb(new zzcgx(zzaqg2, zzaj)).zza(zza3).zzaqg();
    }

    private static zzdhe<zzaqq> zza(zzdhe<JSONObject> zzdhe, zzdcr zzdcr, zzakc zzakc) {
        return zzdcr.zza(zzdco.BUILD_URL, zzdhe).zza(zzakc.zza("AFMA_getAdDictionary", zzajx.zzdaq, zzchc.zzdas)).zzaqg();
    }

    public final void zza(zzaqk zzaqk, zzaqe zzaqe) {
        zzdhe<InputStream> zzh = zzh(zzaqk);
        zza(zzh, zzaqe);
        zzh.addListener(new zzchb(this), this.zzfci);
    }

    public final void zza(String str, zzaqe zzaqe) {
        zzdhe zzdhe;
        if (!zzabd.zzcty.get().booleanValue()) {
            zzdhe = zzdgs.zzk(new Exception("Split request is disabled."));
        } else {
            zzchf zzchf = new zzchf(this);
            if (this.zzfwe.remove(str) == null) {
                String valueOf = String.valueOf(str);
                zzdhe = zzdgs.zzk(new Exception(valueOf.length() != 0 ? "URL to be removed not found for cache key: ".concat(valueOf) : new String("URL to be removed not found for cache key: ")));
            } else {
                zzdhe = zzdgs.zzaj(zzchf);
            }
        }
        zza((zzdhe<InputStream>) zzdhe, zzaqe);
    }

    private final void zza(zzdhe<InputStream> zzdhe, zzaqe zzaqe) {
        zzdgs.zza(zzdgs.zzb(zzdhe, new zzchg(this), (Executor) zzazd.zzdwe), new zzchi(this, zzaqe), zzazd.zzdwj);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ InputStream zza(zzdhe zzdhe, zzdhe zzdhe2) throws Exception {
        String zzuf = ((zzaqq) zzdhe.get()).zzuf();
        this.zzfwe.put(zzuf, new zzchh((zzaqq) zzdhe.get(), (JSONObject) zzdhe2.get()));
        return new ByteArrayInputStream(zzuf.getBytes(zzdeb.UTF_8));
    }
}
