package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbpn implements zzdxg<zzbpg> {
    private final zzdxp<Set<zzbsu<zzbph>>> zzfeo;

    private zzbpn(zzdxp<Set<zzbsu<zzbph>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbpn zzi(zzdxp<Set<zzbsu<zzbph>>> zzdxp) {
        return new zzbpn(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbpg(this.zzfeo.get());
    }
}
