package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

final class zzdtr implements zzdtc {
    private final int flags;

    /* renamed from: info  reason: collision with root package name */
    private final String f4146info;
    private final Object[] zzhoy;
    private final zzdte zzhpb;

    zzdtr(zzdte zzdte, String str, Object[] objArr) {
        this.zzhpb = zzdte;
        this.f4146info = str;
        this.zzhoy = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.flags = charAt;
            return;
        }
        char c = charAt & 8191;
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= 55296) {
                c |= (charAt2 & 8191) << i;
                i += 13;
                i2 = i3;
            } else {
                this.flags = c | (charAt2 << i);
                return;
            }
        }
    }

    public final int zzbbg() {
        return (this.flags & 1) == 1 ? zzdrt.zze.zzhna : zzdrt.zze.zzhnb;
    }

    public final boolean zzbbh() {
        return (this.flags & 2) == 2;
    }

    public final zzdte zzbbi() {
        return this.zzhpb;
    }

    /* access modifiers changed from: package-private */
    public final String zzbbn() {
        return this.f4146info;
    }

    /* access modifiers changed from: package-private */
    public final Object[] zzbbo() {
        return this.zzhoy;
    }
}
