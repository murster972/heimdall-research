package com.google.android.gms.internal.ads;

import java.util.LinkedHashMap;

public class zzdxc<K, V, V2> {
    final LinkedHashMap<K, zzdxp<V>> zzhzy;

    zzdxc(int i) {
        this.zzhzy = zzdxb.zzhj(i);
    }

    /* access modifiers changed from: package-private */
    public zzdxc<K, V, V2> zza(K k, zzdxp<V> zzdxp) {
        this.zzhzy.put(zzdxm.zza(k, "key"), zzdxm.zza(zzdxp, "provider"));
        return this;
    }
}
