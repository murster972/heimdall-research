package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzckc implements zzcio<zzbtu> {
    private final zzazb zzbli;
    private final Executor zzfci;
    private final zzczu zzfgl;
    private final zzcbn zzfod;
    private final zzbup zzfyt;
    private final Context zzup;

    public zzckc(Context context, zzazb zzazb, zzczu zzczu, Executor executor, zzbup zzbup, zzcbn zzcbn) {
        this.zzup = context;
        this.zzfgl = zzczu;
        this.zzfyt = zzbup;
        this.zzfci = executor;
        this.zzbli = zzazb;
        this.zzfod = zzcbn;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        zzczp zzczp = zzczl.zzglo;
        return (zzczp == null || zzczp.zzdht == null) ? false : true;
    }

    public final zzdhe<zzbtu> zzb(zzczt zzczt, zzczl zzczl) {
        zzccd zzccd = new zzccd();
        zzdhe<zzbtu> zzb = zzdgs.zzb(zzdgs.zzaj(null), new zzckb(this, zzczl, zzccd, zzczt), this.zzfci);
        zzb.addListener(zzcke.zza(zzccd), this.zzfci);
        return zzb;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzczl zzczl, zzccd zzccd, zzczt zzczt, Object obj) throws Exception {
        zzdhe<?> zzdhe;
        zzbdi zza = this.zzfod.zza(this.zzfgl.zzblm, zzczl.zzega);
        zza.zzba(zzczl.zzdll);
        zzccd.zza(this.zzup, zza.getView());
        zzazl zzazl = new zzazl();
        zzbtw zza2 = this.zzfyt.zza(new zzbmt(zzczt, zzczl, (String) null), new zzbtv(new zzcki(this.zzup, this.zzbli, zzazl, zzczl, zza), zza));
        zzazl.set(zza2);
        zza2.zzadk().zza(new zzckd(zza), zzazd.zzdwj);
        zza2.zzadx().zzb(zza, true);
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcqj)).booleanValue() || !zzczl.zzega) {
            zza2.zzadx();
            zzczp zzczp = zzczl.zzglo;
            zzdhe = zzcbp.zza(zza, zzczp.zzdhr, zzczp.zzdht);
        } else {
            zzdhe = zzdgs.zzaj(null);
        }
        return zzdgs.zzb(zzdhe, new zzckg(this, zza, zzczl, zza2), this.zzfci);
    }
}
