package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class zzauv extends zzgc implements zzaut {
    zzauv(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.signals.ISignalGeneratorCreator");
    }

    /* JADX WARNING: type inference failed for: r0v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzauo zzf(com.google.android.gms.dynamic.IObjectWrapper r3, int r4) throws android.os.RemoteException {
        /*
            r2 = this;
            android.os.Parcel r4 = r2.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza((android.os.Parcel) r4, (android.os.IInterface) r3)
            r3 = 12451009(0xbdfcc1, float:1.744758E-38)
            r4.writeInt(r3)
            r3 = 1
            android.os.Parcel r3 = r2.transactAndReadException(r3, r4)
            android.os.IBinder r4 = r3.readStrongBinder()
            if (r4 != 0) goto L_0x001a
            r4 = 0
            goto L_0x002e
        L_0x001a:
            java.lang.String r0 = "com.google.android.gms.ads.internal.signals.ISignalGenerator"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.ads.zzauo
            if (r1 == 0) goto L_0x0028
            r4 = r0
            com.google.android.gms.internal.ads.zzauo r4 = (com.google.android.gms.internal.ads.zzauo) r4
            goto L_0x002e
        L_0x0028:
            com.google.android.gms.internal.ads.zzauq r0 = new com.google.android.gms.internal.ads.zzauq
            r0.<init>(r4)
            r4 = r0
        L_0x002e:
            r3.recycle()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzauv.zzf(com.google.android.gms.dynamic.IObjectWrapper, int):com.google.android.gms.internal.ads.zzauo");
    }
}
