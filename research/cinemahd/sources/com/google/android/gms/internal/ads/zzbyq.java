package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.List;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzbyq {
    private final zzdhd zzfov;
    private final zzbyu zzfow;
    private final zzbze zzfox;

    public zzbyq(zzdhd zzdhd, zzbyu zzbyu, zzbze zzbze) {
        this.zzfov = zzdhd;
        this.zzfow = zzbyu;
        this.zzfox = zzbze;
    }

    public final zzdhe<zzbws> zza(zzczt zzczt, zzczl zzczl, JSONObject jSONObject) {
        zzdhe zzb;
        JSONObject jSONObject2 = jSONObject;
        zzczt zzczt2 = zzczt;
        zzdhe zzd = this.zzfov.zzd(new zzbyp(this, zzczt, zzczl, jSONObject2));
        zzdhe<List<zzabu>> zzd2 = this.zzfow.zzd(jSONObject2, "images");
        zzdhe<zzabu> zzc = this.zzfow.zzc(jSONObject2, "secondary_image");
        zzdhe<zzabu> zzc2 = this.zzfow.zzc(jSONObject2, "app_icon");
        zzdhe<zzabp> zze = this.zzfow.zze(jSONObject2, "attribution");
        zzdhe<zzbdi> zzl = this.zzfow.zzl(jSONObject2);
        zzbyu zzbyu = this.zzfow;
        if (!jSONObject2.optBoolean("enable_omid")) {
            zzb = zzdgs.zzaj(null);
        } else {
            JSONObject optJSONObject = jSONObject2.optJSONObject("omid_settings");
            if (optJSONObject == null) {
                zzb = zzdgs.zzaj(null);
            } else {
                String optString = optJSONObject.optString("omid_html");
                if (TextUtils.isEmpty(optString)) {
                    zzb = zzdgs.zzaj(null);
                } else {
                    zzb = zzdgs.zzb(zzdgs.zzaj(null), new zzbyy(zzbyu, optString), (Executor) zzazd.zzdwi);
                }
            }
        }
        zzdhe zzdhe = zzb;
        zzdhe<List<zzbzf>> zzg = this.zzfox.zzg(jSONObject2, "custom_assets");
        return zzdgs.zza(zzd, zzd2, zzc, zzc2, zze, zzl, zzdhe, zzg).zza(new zzbys(this, zzd, zzd2, zzc2, zzc, zze, jSONObject, zzl, zzdhe, zzg), this.zzfov);
    }
}
