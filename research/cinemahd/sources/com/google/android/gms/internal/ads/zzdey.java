package com.google.android.gms.internal.ads;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public abstract class zzdey<K, V> implements Serializable, Map<K, V> {
    private static final Map.Entry<?, ?>[] zzguk = new Map.Entry[0];
    private transient zzdfb<Map.Entry<K, V>> zzgul;
    private transient zzdfb<K> zzgum;
    private transient zzdet<V> zzgun;

    zzdey() {
    }

    public static <K, V> zzdey<K, V> zza(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        zzdeo.zzb(k, v);
        zzdeo.zzb(k2, v2);
        zzdeo.zzb(k3, v3);
        zzdeo.zzb(k4, v4);
        zzdeo.zzb(k5, v5);
        return zzdfh.zzc(5, new Object[]{k, v, k2, v2, k3, v3, k4, v4, k5, v5});
    }

    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    public boolean containsValue(Object obj) {
        return ((zzdet) values()).contains(obj);
    }

    public /* synthetic */ Set entrySet() {
        zzdfb<Map.Entry<K, V>> zzdfb = this.zzgul;
        if (zzdfb != null) {
            return zzdfb;
        }
        zzdfb<Map.Entry<K, V>> zzare = zzare();
        this.zzgul = zzare;
        return zzare;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    public abstract V get(Object obj);

    public final V getOrDefault(Object obj, V v) {
        V v2 = get(obj);
        return v2 != null ? v2 : v;
    }

    public int hashCode() {
        return zzdfn.zzf((zzdfb) entrySet());
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public /* synthetic */ Set keySet() {
        zzdfb<K> zzdfb = this.zzgum;
        if (zzdfb != null) {
            return zzdfb;
        }
        zzdfb<K> zzarf = zzarf();
        this.zzgum = zzarf;
        return zzarf;
    }

    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public String toString() {
        int size = size();
        zzdeo.zze(size, "size");
        StringBuilder sb = new StringBuilder((int) Math.min(((long) size) << 3, 1073741824));
        sb.append('{');
        boolean z = true;
        for (Map.Entry entry : entrySet()) {
            if (!z) {
                sb.append(", ");
            }
            z = false;
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
        }
        sb.append('}');
        return sb.toString();
    }

    public /* synthetic */ Collection values() {
        zzdet<V> zzdet = this.zzgun;
        if (zzdet != null) {
            return zzdet;
        }
        zzdet<V> zzarg = zzarg();
        this.zzgun = zzarg;
        return zzarg;
    }

    /* access modifiers changed from: package-private */
    public abstract zzdfb<Map.Entry<K, V>> zzare();

    /* access modifiers changed from: package-private */
    public abstract zzdfb<K> zzarf();

    /* access modifiers changed from: package-private */
    public abstract zzdet<V> zzarg();
}
