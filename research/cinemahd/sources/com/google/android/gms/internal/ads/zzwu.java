package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzwu extends zzgb implements zzwr {
    public zzwu() {
        super("com.google.android.gms.ads.internal.client.IMuteThisAdReason");
    }

    public static zzwr zzg(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMuteThisAdReason");
        if (queryLocalInterface instanceof zzwr) {
            return (zzwr) queryLocalInterface;
        }
        return new zzwt(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            String description = getDescription();
            parcel2.writeNoException();
            parcel2.writeString(description);
        } else if (i != 2) {
            return false;
        } else {
            String zzph = zzph();
            parcel2.writeNoException();
            parcel2.writeString(zzph);
        }
        return true;
    }
}
