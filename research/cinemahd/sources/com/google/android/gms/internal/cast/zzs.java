package com.google.android.gms.internal.cast;

import android.animation.ObjectAnimator;
import android.view.View;
import com.google.android.gms.common.util.PlatformVersion;

final class zzs implements View.OnClickListener {
    final /* synthetic */ zzr zzjl;

    zzs(zzr zzr) {
        this.zzjl = zzr;
    }

    public final void onClick(View view) {
        if (PlatformVersion.c()) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "alpha", new float[]{0.0f});
            ofFloat.setDuration(400).addListener(new zzt(this));
            ofFloat.start();
            return;
        }
        this.zzjl.zzaw();
    }
}
