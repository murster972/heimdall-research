package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzblv implements zzdxg<zzblw> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzbdi> zzfef;
    private final zzdxp<zzczl> zzffb;

    public zzblv(zzdxp<Context> zzdxp, zzdxp<zzbdi> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzazb> zzdxp4) {
        this.zzejv = zzdxp;
        this.zzfef = zzdxp2;
        this.zzffb = zzdxp3;
        this.zzfav = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzblw(this.zzejv.get(), this.zzfef.get(), this.zzffb.get(), this.zzfav.get());
    }
}
