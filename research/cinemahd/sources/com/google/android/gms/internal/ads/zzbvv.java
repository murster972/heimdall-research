package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.common.util.Clock;
import org.json.JSONObject;

public final class zzbvv implements zzdxg<zzbvr> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdda> zzepi;
    private final zzdxp<zzboq> zzesg;
    private final zzdxp<zzbpd> zzesj;
    private final zzdxp<JSONObject> zzety;
    private final zzdxp<zzbjd> zzeue;
    private final zzdxp<zzbst> zzeun;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<zzbwz> zzfeh;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzczl> zzffb;
    private final zzdxp<zzcaj> zzfkw;
    private final zzdxp<zzbws> zzfkx;
    private final zzdxp<zzdq> zzfky;
    private final zzdxp<zzbxq> zzfkz;

    public zzbvv(zzdxp<Context> zzdxp, zzdxp<zzbwz> zzdxp2, zzdxp<JSONObject> zzdxp3, zzdxp<zzcaj> zzdxp4, zzdxp<zzbws> zzdxp5, zzdxp<zzdq> zzdxp6, zzdxp<zzbpd> zzdxp7, zzdxp<zzboq> zzdxp8, zzdxp<zzczl> zzdxp9, zzdxp<zzazb> zzdxp10, zzdxp<zzczu> zzdxp11, zzdxp<zzbjd> zzdxp12, zzdxp<zzbxq> zzdxp13, zzdxp<Clock> zzdxp14, zzdxp<zzbst> zzdxp15, zzdxp<zzdda> zzdxp16) {
        this.zzejv = zzdxp;
        this.zzfeh = zzdxp2;
        this.zzety = zzdxp3;
        this.zzfkw = zzdxp4;
        this.zzfkx = zzdxp5;
        this.zzfky = zzdxp6;
        this.zzesj = zzdxp7;
        this.zzesg = zzdxp8;
        this.zzffb = zzdxp9;
        this.zzfav = zzdxp10;
        this.zzfep = zzdxp11;
        this.zzeue = zzdxp12;
        this.zzfkz = zzdxp13;
        this.zzfcz = zzdxp14;
        this.zzeun = zzdxp15;
        this.zzepi = zzdxp16;
    }

    public final /* synthetic */ Object get() {
        return new zzbvr(this.zzejv.get(), this.zzfeh.get(), this.zzety.get(), this.zzfkw.get(), this.zzfkx.get(), this.zzfky.get(), this.zzesj.get(), this.zzesg.get(), this.zzffb.get(), this.zzfav.get(), this.zzfep.get(), this.zzeue.get(), this.zzfkz.get(), this.zzfcz.get(), this.zzeun.get(), this.zzepi.get());
    }
}
