package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;

@TargetApi(17)
public final class zzayf {
    private static zzayf zzdux;
    String zzduy;

    private zzayf() {
    }

    public static zzayf zzxc() {
        if (zzdux == null) {
            zzdux = new zzayf();
        }
        return zzdux;
    }
}
