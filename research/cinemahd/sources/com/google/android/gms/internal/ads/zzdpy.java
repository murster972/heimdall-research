package com.google.android.gms.internal.ads;

import java.io.PrintWriter;

final class zzdpy extends zzdps {
    zzdpy() {
    }

    public final void zza(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }

    public final void zzl(Throwable th) {
        th.printStackTrace();
    }

    public final void zza(Throwable th, PrintWriter printWriter) {
        th.printStackTrace(printWriter);
    }
}
