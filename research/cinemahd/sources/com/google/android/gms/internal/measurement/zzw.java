package com.google.android.gms.internal.measurement;

import android.os.IBinder;

public final class zzw extends zza implements zzt {
    zzw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IStringProvider");
    }
}
