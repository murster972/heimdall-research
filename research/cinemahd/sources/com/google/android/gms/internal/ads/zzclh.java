package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbod;

public final class zzclh implements zzdxg<zzcli> {
    private final zzdxp<zzbfx> zzfkr;
    private final zzdxp<zzbod.zza> zzfks;
    private final zzdxp<zzbrm> zzfkt;
    private final zzdxp<zzbvi> zzfku;

    public zzclh(zzdxp<zzbfx> zzdxp, zzdxp<zzbvi> zzdxp2, zzdxp<zzbod.zza> zzdxp3, zzdxp<zzbrm> zzdxp4) {
        this.zzfkr = zzdxp;
        this.zzfku = zzdxp2;
        this.zzfks = zzdxp3;
        this.zzfkt = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcli(this.zzfkr.get(), this.zzfku.get(), this.zzfks.get(), this.zzfkt.get());
    }
}
