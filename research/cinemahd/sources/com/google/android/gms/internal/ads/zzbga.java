package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzh;
import com.google.android.gms.ads.internal.zzq;
import java.lang.ref.WeakReference;

public class zzbga {
    private final zzazb zzbli;
    private final WeakReference<Context> zzejq;
    private final Context zzyv;

    public static class zza {
        /* access modifiers changed from: private */
        public zzazb zzbli;
        /* access modifiers changed from: private */
        public WeakReference<Context> zzejq;
        /* access modifiers changed from: private */
        public Context zzyv;

        public final zza zza(zzazb zzazb) {
            this.zzbli = zzazb;
            return this;
        }

        public final zza zzbs(Context context) {
            this.zzejq = new WeakReference<>(context);
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.zzyv = context;
            return this;
        }
    }

    private zzbga(zza zza2) {
        this.zzbli = zza2.zzbli;
        this.zzyv = zza2.zzyv;
        this.zzejq = zza2.zzejq;
    }

    /* access modifiers changed from: package-private */
    public final Context zzacp() {
        return this.zzyv;
    }

    /* access modifiers changed from: package-private */
    public final WeakReference<Context> zzacq() {
        return this.zzejq;
    }

    /* access modifiers changed from: package-private */
    public final zzazb zzacr() {
        return this.zzbli;
    }

    /* access modifiers changed from: package-private */
    public final String zzacs() {
        return zzq.zzkq().zzr(this.zzyv, this.zzbli.zzbma);
    }

    public final zzdq zzact() {
        return new zzdq(new zzh(this.zzyv, this.zzbli));
    }
}
