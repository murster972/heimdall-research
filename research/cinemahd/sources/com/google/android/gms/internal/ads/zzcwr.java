package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.overlay.zzy;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public final class zzcwr extends zzvt implements zzy, zzbqg, zzra {
    /* access modifiers changed from: private */
    public final zzazb zzbli;
    private final String zzbqz;
    /* access modifiers changed from: private */
    public final ViewGroup zzfdu;
    private final zzbfx zzfzz;
    private final Context zzgcr;
    private AtomicBoolean zzgiv = new AtomicBoolean();
    private final zzcwl zzgiw;
    /* access modifiers changed from: private */
    public final zzcwz zzgix;
    private zzbju zzgiy;
    protected zzbke zzgiz;

    public zzcwr(zzbfx zzbfx, Context context, String str, zzcwl zzcwl, zzcwz zzcwz, zzazb zzazb) {
        this.zzfdu = new FrameLayout(context);
        this.zzfzz = zzbfx;
        this.zzgcr = context;
        this.zzbqz = str;
        this.zzgiw = zzcwl;
        this.zzgix = zzcwz;
        zzcwz.zza((zzbqg) this);
        this.zzbli = zzazb;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzaoc */
    public final void zzaof() {
        if (this.zzgiv.compareAndSet(false, true)) {
            zzbke zzbke = this.zzgiz;
            if (!(zzbke == null || zzbke.zzagc() == null)) {
                this.zzgix.zzb(this.zzgiz.zzagc());
            }
            this.zzgix.onAdClosed();
            this.zzfdu.removeAllViews();
            zzbju zzbju = this.zzgiy;
            if (zzbju != null) {
                zzq.zzkt().zzb(zzbju);
            }
            destroy();
        }
    }

    /* access modifiers changed from: private */
    public final zzuj zzaod() {
        return zzczy.zza(this.zzgcr, (List<zzczk>) Collections.singletonList(this.zzgiz.zzafz()));
    }

    /* access modifiers changed from: private */
    public static RelativeLayout.LayoutParams zzb(zzbke zzbke) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(zzbke.zzaat() ? 11 : 9);
        return layoutParams;
    }

    /* access modifiers changed from: private */
    public final void zzc(zzbke zzbke) {
        zzbke.zza((zzra) this);
    }

    public final synchronized void destroy() {
        Preconditions.a("destroy must be called on the main UI thread.");
        if (this.zzgiz != null) {
            this.zzgiz.destroy();
        }
    }

    public final Bundle getAdMetadata() {
        return new Bundle();
    }

    public final synchronized String getAdUnitId() {
        return this.zzbqz;
    }

    public final synchronized String getMediationAdapterClassName() {
        return null;
    }

    public final synchronized zzxb getVideoController() {
        return null;
    }

    public final synchronized boolean isLoading() {
        return this.zzgiw.isLoading();
    }

    public final boolean isReady() {
        return false;
    }

    public final synchronized void pause() {
        Preconditions.a("pause must be called on the main UI thread.");
    }

    public final synchronized void resume() {
        Preconditions.a("resume must be called on the main UI thread.");
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final synchronized void setManualImpressionsEnabled(boolean z) {
    }

    public final void setUserId(String str) {
    }

    public final void showInterstitial() {
    }

    public final void stopLoading() {
    }

    public final void zza(zzaoy zzaoy) {
    }

    public final void zza(zzape zzape, String str) {
    }

    public final void zza(zzaro zzaro) {
    }

    public final void zza(zzuo zzuo) {
        this.zzgiw.zza(zzuo);
    }

    public final void zza(zzvg zzvg) {
    }

    public final void zza(zzvh zzvh) {
    }

    public final void zza(zzvx zzvx) {
    }

    public final void zza(zzwc zzwc) {
    }

    public final void zza(zzxh zzxh) {
    }

    public final void zzahi() {
        int zzafw;
        zzbke zzbke = this.zzgiz;
        if (zzbke != null && (zzafw = zzbke.zzafw()) > 0) {
            this.zzgiy = new zzbju(this.zzfzz.zzacb(), zzq.zzkx());
            this.zzgiy.zza(zzafw, new zzcwt(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaoe() {
        this.zzfzz.zzaca().execute(new zzcwq(this));
    }

    public final void zzbr(String str) {
    }

    public final IObjectWrapper zzjx() {
        Preconditions.a("getAdFrame must be called on the main UI thread.");
        return ObjectWrapper.a(this.zzfdu);
    }

    public final synchronized void zzjy() {
    }

    public final synchronized zzuj zzjz() {
        Preconditions.a("getAdSize must be called on the main UI thread.");
        if (this.zzgiz == null) {
            return null;
        }
        return zzczy.zza(this.zzgcr, (List<zzczk>) Collections.singletonList(this.zzgiz.zzafz()));
    }

    public final synchronized String zzka() {
        return null;
    }

    public final synchronized zzxa zzkb() {
        return null;
    }

    public final zzwc zzkc() {
        return null;
    }

    public final zzvh zzkd() {
        return null;
    }

    public final void zzmm() {
        zzaof();
    }

    public final void zztl() {
        zzaof();
    }

    public final synchronized boolean zza(zzug zzug) throws RemoteException {
        Preconditions.a("loadAd must be called on the main UI thread.");
        if (isLoading()) {
            return false;
        }
        this.zzgiv = new AtomicBoolean();
        return this.zzgiw.zza(zzug, this.zzbqz, new zzcws(this), new zzcwv(this));
    }

    /* access modifiers changed from: private */
    public final com.google.android.gms.ads.internal.overlay.zzq zza(zzbke zzbke) {
        boolean zzaat = zzbke.zzaat();
        int intValue = ((Integer) zzve.zzoy().zzd(zzzn.zzcnv)).intValue();
        zzp zzp = new zzp();
        zzp.size = 50;
        zzp.paddingLeft = zzaat ? intValue : 0;
        zzp.paddingRight = zzaat ? 0 : intValue;
        zzp.paddingTop = 0;
        zzp.paddingBottom = intValue;
        return new com.google.android.gms.ads.internal.overlay.zzq(this.zzgcr, zzp, this);
    }

    public final void zza(zzrg zzrg) {
        this.zzgix.zzb(zzrg);
    }

    public final synchronized void zza(zzuj zzuj) {
        Preconditions.a("setAdSize must be called on the main UI thread.");
    }

    public final synchronized void zza(zzwi zzwi) {
    }

    public final synchronized void zza(zzyw zzyw) {
    }

    public final synchronized void zza(zzaak zzaak) {
    }
}
