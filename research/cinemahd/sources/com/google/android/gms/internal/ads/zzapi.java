package com.google.android.gms.internal.ads;

import android.os.IBinder;

final /* synthetic */ class zzapi implements zzayw {
    static final zzayw zzbtz = new zzapi();

    private zzapi() {
    }

    public final Object apply(Object obj) {
        return zzaus.zzan((IBinder) obj);
    }
}
