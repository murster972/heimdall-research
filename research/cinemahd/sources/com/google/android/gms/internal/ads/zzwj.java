package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public abstract class zzwj extends zzgb implements zzwk {
    public zzwj() {
        super("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                initialize();
                parcel2.writeNoException();
                return true;
            case 2:
                setAppVolume(parcel.readFloat());
                parcel2.writeNoException();
                return true;
            case 3:
                zzcd(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 4:
                setAppMuted(zzge.zza(parcel));
                parcel2.writeNoException();
                return true;
            case 5:
                zzb(IObjectWrapper.Stub.a(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                zza(parcel.readString(), IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 7:
                float zzpe = zzpe();
                parcel2.writeNoException();
                parcel2.writeFloat(zzpe);
                return true;
            case 8:
                boolean zzpf = zzpf();
                parcel2.writeNoException();
                zzge.writeBoolean(parcel2, zzpf);
                return true;
            case 9:
                String versionString = getVersionString();
                parcel2.writeNoException();
                parcel2.writeString(versionString);
                return true;
            case 10:
                zzce(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                zza(zzalb.zzaa(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 12:
                zza(zzagt.zzy(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 13:
                List<zzagn> zzpg = zzpg();
                parcel2.writeNoException();
                parcel2.writeTypedList(zzpg);
                return true;
            case 14:
                zza((zzyq) zzge.zza(parcel, zzyq.CREATOR));
                parcel2.writeNoException();
                return true;
            default:
                return false;
        }
    }
}
