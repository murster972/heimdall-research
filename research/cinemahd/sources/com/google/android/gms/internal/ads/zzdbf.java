package com.google.android.gms.internal.ads;

public final class zzdbf implements Cloneable {
    public boolean zzgoq;
    public boolean zzgor;

    /* access modifiers changed from: private */
    /* renamed from: zzapm */
    public final zzdbf clone() {
        try {
            return (zzdbf) super.clone();
        } catch (CloneNotSupportedException unused) {
            throw new AssertionError();
        }
    }
}
