package com.google.android.gms.internal.ads;

public final class zzkm {
    public final int[] zzamp;
    public final long[] zzamq;
    public final int zzawo;
    public final long[] zzawp;
    public final int[] zzawq;

    private zzkm(long[] jArr, int[] iArr, int i, long[] jArr2, int[] iArr2) {
        this.zzamq = jArr;
        this.zzamp = iArr;
        this.zzawo = i;
        this.zzawp = jArr2;
        this.zzawq = iArr2;
    }
}
