package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.AdMetadataListener;
import java.util.Set;

public final class zzbrz implements zzdxg<Set<zzbsu<AdMetadataListener>>> {
    private final zzbrm zzfim;

    private zzbrz(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbrz zzp(zzbrm zzbrm) {
        return new zzbrz(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfim.zzahq(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
