package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.media.AudioTimestamp;
import android.media.AudioTrack;

@TargetApi(19)
final class zzib extends zzhy {
    private final AudioTimestamp zzakb = new AudioTimestamp();
    private long zzakc;
    private long zzakd;
    private long zzake;

    public zzib() {
        super((zzhz) null);
    }

    public final void zza(AudioTrack audioTrack, boolean z) {
        super.zza(audioTrack, z);
        this.zzakc = 0;
        this.zzakd = 0;
        this.zzake = 0;
    }

    public final boolean zzfq() {
        boolean timestamp = this.zzaia.getTimestamp(this.zzakb);
        if (timestamp) {
            long j = this.zzakb.framePosition;
            if (this.zzakd > j) {
                this.zzakc++;
            }
            this.zzakd = j;
            this.zzake = j + (this.zzakc << 32);
        }
        return timestamp;
    }

    public final long zzfr() {
        return this.zzakb.nanoTime;
    }

    public final long zzfs() {
        return this.zzake;
    }
}
