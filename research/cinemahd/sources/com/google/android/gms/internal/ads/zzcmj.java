package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcmj implements zzbpe {
    private final zzbdi zzehp;

    zzcmj(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void onAdImpression() {
        zzbdi zzbdi = this.zzehp;
        if (zzbdi.zzaaa() != null) {
            zzbdi.zzaaa().zzaaz();
        }
    }
}
