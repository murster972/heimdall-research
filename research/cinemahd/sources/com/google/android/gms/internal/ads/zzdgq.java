package com.google.android.gms.internal.ads;

public class zzdgq<V> extends zzdgr<V> {
    private final zzdhe<V> zzgwt;

    protected zzdgq(zzdhe<V> zzdhe) {
        this.zzgwt = (zzdhe) zzdei.checkNotNull(zzdhe);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzarv */
    public final zzdhe<V> zzaru() {
        return this.zzgwt;
    }
}
