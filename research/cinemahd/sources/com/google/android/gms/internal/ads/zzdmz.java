package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdmz extends zzdrt<zzdmz, zza> implements zzdtg {
    private static volatile zzdtn<zzdmz> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmz zzhcr;
    private int zzhah;
    private int zzhcq;

    public static final class zza extends zzdrt.zzb<zzdmz, zza> implements zzdtg {
        private zza() {
            super(zzdmz.zzhcr);
        }

        /* synthetic */ zza(zzdmy zzdmy) {
            this();
        }
    }

    static {
        zzdmz zzdmz = new zzdmz();
        zzhcr = zzdmz;
        zzdrt.zza(zzdmz.class, zzdmz);
    }

    private zzdmz() {
    }

    public static zzdmz zzavg() {
        return zzhcr;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmy.zzdk[i - 1]) {
            case 1:
                return new zzdmz();
            case 2:
                return new zza((zzdmy) null);
            case 3:
                return zzdrt.zza((zzdte) zzhcr, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0002\u000b", new Object[]{"zzhcq", "zzhah"});
            case 4:
                return zzhcr;
            case 5:
                zzdtn<zzdmz> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmz.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhcr);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final int zzasx() {
        return this.zzhah;
    }

    public final zzdmt zzavf() {
        zzdmt zzem = zzdmt.zzem(this.zzhcq);
        return zzem == null ? zzdmt.UNRECOGNIZED : zzem;
    }
}
