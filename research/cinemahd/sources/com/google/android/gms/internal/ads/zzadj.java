package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzadj extends IInterface {
    void zza(zzada zzada) throws RemoteException;
}
