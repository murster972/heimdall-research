package com.google.android.gms.internal.ads;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzso;
import com.google.android.gms.internal.ads.zzsy;
import java.util.ArrayList;

public final class zzcik {
    private zzazb zzdij;
    private zzcht zzfxo;
    private zzsm zzfya;
    private Context zzup;

    public zzcik(Context context, zzazb zzazb, zzsm zzsm, zzcht zzcht) {
        this.zzup = context;
        this.zzdij = zzazb;
        this.zzfya = zzsm;
        this.zzfxo = zzcht;
    }

    public final void zzamc() {
        try {
            this.zzfxo.zza(new zzcij(this));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            zzayu.zzex(valueOf.length() != 0 ? "Error in offline signals database startup: ".concat(valueOf) : new String("Error in offline signals database startup: "));
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Void zzb(SQLiteDatabase sQLiteDatabase) throws Exception {
        SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
        ArrayList<zzsy.zzj.zza> zza = zzcih.zza(sQLiteDatabase);
        int i = 0;
        zzsy.zzj zzj = (zzsy.zzj) zzsy.zzj.zzno().zzca(this.zzup.getPackageName()).zzcb(Build.MODEL).zzcd(zzcih.zza(sQLiteDatabase2, 0)).zzc(zza).zzce(zzcih.zza(sQLiteDatabase2, 1)).zzes(zzq.zzkx().b()).zzet(zzcih.zzb(sQLiteDatabase2, 2)).zzbaf();
        int size = zza.size();
        long j = 0;
        int i2 = 0;
        while (i2 < size) {
            zzsy.zzj.zza zza2 = zza.get(i2);
            i2++;
            zzsy.zzj.zza zza3 = zza2;
            if (zza3.zznq() == zzte.ENUM_TRUE && zza3.getTimestamp() > j) {
                j = zza3.getTimestamp();
            }
        }
        if (j != 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("value", Long.valueOf(j));
            sQLiteDatabase2.update("offline_signal_statistics", contentValues, "statistic_name = 'last_successful_request_time'", (String[]) null);
        }
        this.zzfya.zza((zzsp) new zzcim(zzj));
        zztt zztt = new zztt();
        zztt.zzcam = Integer.valueOf(this.zzdij.zzdvz);
        zztt.zzcan = Integer.valueOf(this.zzdij.zzdwa);
        if (!this.zzdij.zzdwb) {
            i = 2;
        }
        zztt.zzcao = Integer.valueOf(i);
        this.zzfya.zza((zzsp) new zzcil(zztt));
        this.zzfya.zza(zzso.zza.C0047zza.OFFLINE_UPLOAD);
        sQLiteDatabase2.delete("offline_signal_contents", (String) null, (String[]) null);
        ContentValues contentValues2 = new ContentValues();
        contentValues2.put("value", 0);
        sQLiteDatabase2.update("offline_signal_statistics", contentValues2, "statistic_name = ?", new String[]{"failed_requests"});
        ContentValues contentValues3 = new ContentValues();
        contentValues3.put("value", 0);
        sQLiteDatabase2.update("offline_signal_statistics", contentValues3, "statistic_name = ?", new String[]{"total_requests"});
        return null;
    }
}
