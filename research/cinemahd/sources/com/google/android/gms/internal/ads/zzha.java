package com.google.android.gms.internal.ads;

public interface zzha {
    void onStopped();

    void zza(zzhf[] zzhfArr, zzmr zzmr, zzng zzng);

    boolean zzc(long j, boolean z);

    boolean zzdt(long j);

    void zzer();

    void zzes();

    zznj zzet();
}
