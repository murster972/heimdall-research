package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzcmy implements zzcis<zzani, zzcjy> {
    private final zzcnz zzgbe;

    public zzcmy(zzcnz zzcnz) {
        this.zzgbe = zzcnz;
    }

    public final zzcip<zzani, zzcjy> zzd(String str, JSONObject jSONObject) throws zzdab {
        zzani zzgh = this.zzgbe.zzgh(str);
        if (zzgh == null) {
            return null;
        }
        return new zzcip<>(zzgh, new zzcjy(), str);
    }
}
