package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

final class zzdkk {
    public static void zza(zzdmj zzdmj) throws GeneralSecurityException {
        zzdov.zza(zza(zzdmj.zzauh().zzauu()));
        zza(zzdmj.zzauh().zzauv());
        if (zzdmj.zzauj() != zzdmd.UNKNOWN_FORMAT) {
            zzdit.zza(zzdmj.zzaui().zzauc());
            return;
        }
        throw new GeneralSecurityException("unknown EC point format");
    }

    public static String zza(zzdmt zzdmt) throws NoSuchAlgorithmException {
        int i = zzdkn.zzgzw[zzdmt.ordinal()];
        if (i == 1) {
            return "HmacSha1";
        }
        if (i == 2) {
            return "HmacSha256";
        }
        if (i == 3) {
            return "HmacSha512";
        }
        String valueOf = String.valueOf(zzdmt);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
        sb.append("hash unsupported for HMAC: ");
        sb.append(valueOf);
        throw new NoSuchAlgorithmException(sb.toString());
    }

    public static zzdox zza(zzdmr zzdmr) throws GeneralSecurityException {
        int i = zzdkn.zzgzx[zzdmr.ordinal()];
        if (i == 1) {
            return zzdox.NIST_P256;
        }
        if (i == 2) {
            return zzdox.NIST_P384;
        }
        if (i == 3) {
            return zzdox.NIST_P521;
        }
        String valueOf = String.valueOf(zzdmr);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20);
        sb.append("unknown curve type: ");
        sb.append(valueOf);
        throw new GeneralSecurityException(sb.toString());
    }

    public static zzdow zza(zzdmd zzdmd) throws GeneralSecurityException {
        int i = zzdkn.zzgzy[zzdmd.ordinal()];
        if (i == 1) {
            return zzdow.UNCOMPRESSED;
        }
        if (i == 2) {
            return zzdow.DO_NOT_USE_CRUNCHY_UNCOMPRESSED;
        }
        if (i == 3) {
            return zzdow.COMPRESSED;
        }
        String valueOf = String.valueOf(zzdmd);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("unknown point format: ");
        sb.append(valueOf);
        throw new GeneralSecurityException(sb.toString());
    }
}
