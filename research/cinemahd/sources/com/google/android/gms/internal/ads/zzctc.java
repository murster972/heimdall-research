package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzctc implements zzdxg<zzcta> {
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<Bundle> zzggf;

    private zzctc(zzdxp<zzdhd> zzdxp, zzdxp<Bundle> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzggf = zzdxp2;
    }

    public static zzctc zzas(zzdxp<zzdhd> zzdxp, zzdxp<Bundle> zzdxp2) {
        return new zzctc(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcta(this.zzfcv.get(), this.zzggf.get());
    }
}
