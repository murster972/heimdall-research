package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbtt implements Runnable {
    private final zzbdi zzehp;

    private zzbtt(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static Runnable zzh(zzbdi zzbdi) {
        return new zzbtt(zzbdi);
    }

    public final void run() {
        this.zzehp.destroy();
    }
}
