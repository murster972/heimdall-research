package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

final class zzcjl extends zzamv {
    private zzcip<zzani, zzcjy> zzfyr;
    private final /* synthetic */ zzcjk zzfys;

    private zzcjl(zzcjk zzcjk, zzcip<zzani, zzcjy> zzcip) {
        this.zzfys = zzcjk;
        this.zzfyr = zzcip;
    }

    public final void zzdl(String str) throws RemoteException {
        ((zzcjy) this.zzfyr.zzfyf).onAdFailedToLoad(0);
    }

    public final void zzx(IObjectWrapper iObjectWrapper) throws RemoteException {
        View unused = this.zzfys.view = (View) ObjectWrapper.a(iObjectWrapper);
        ((zzcjy) this.zzfyr.zzfyf).onAdLoaded();
    }
}
