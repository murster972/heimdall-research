package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcvb implements zzdxg<zzcuz> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzaqr> zzeqj;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<String> zzfrr;

    public zzcvb(zzdxp<zzaqr> zzdxp, zzdxp<Context> zzdxp2, zzdxp<String> zzdxp3, zzdxp<zzdhd> zzdxp4) {
        this.zzeqj = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfrr = zzdxp3;
        this.zzfcv = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcuz(this.zzeqj.get(), this.zzejv.get(), this.zzfrr.get(), this.zzfcv.get());
    }
}
