package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.ScheduledExecutorService;

public final class zzctm implements zzdxg<zzctj> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzcob> zzeki;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<String> zzfdc;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<ScheduledExecutorService> zzfpr;
    private final zzdxp<zzcnz> zzgbd;

    private zzctm(zzdxp<zzdhd> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<String> zzdxp3, zzdxp<zzcob> zzdxp4, zzdxp<Context> zzdxp5, zzdxp<zzczu> zzdxp6, zzdxp<zzcnz> zzdxp7) {
        this.zzfcv = zzdxp;
        this.zzfpr = zzdxp2;
        this.zzfdc = zzdxp3;
        this.zzeki = zzdxp4;
        this.zzejv = zzdxp5;
        this.zzfep = zzdxp6;
        this.zzgbd = zzdxp7;
    }

    public static zzctm zza(zzdxp<zzdhd> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<String> zzdxp3, zzdxp<zzcob> zzdxp4, zzdxp<Context> zzdxp5, zzdxp<zzczu> zzdxp6, zzdxp<zzcnz> zzdxp7) {
        return new zzctm(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6, zzdxp7);
    }

    public final /* synthetic */ Object get() {
        return new zzctj(this.zzfcv.get(), this.zzfpr.get(), this.zzfdc.get(), this.zzeki.get(), this.zzejv.get(), this.zzfep.get(), this.zzgbd.get());
    }
}
