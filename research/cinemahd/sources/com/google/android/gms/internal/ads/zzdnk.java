package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import java.util.List;

public final class zzdnk extends zzdrt<zzdnk, zzb> implements zzdtg {
    private static volatile zzdtn<zzdnk> zzdz;
    /* access modifiers changed from: private */
    public static final zzdnk zzhds;
    private int zzhdq;
    private zzdsb<zza> zzhdr = zzdrt.zzazw();

    public static final class zza extends zzdrt<zza, C0039zza> implements zzdtg {
        private static volatile zzdtn<zza> zzdz;
        /* access modifiers changed from: private */
        public static final zza zzhdw;
        private int zzhdj;
        private zzdna zzhdt;
        private int zzhdu;
        private int zzhdv;

        /* renamed from: com.google.android.gms.internal.ads.zzdnk$zza$zza  reason: collision with other inner class name */
        public static final class C0039zza extends zzdrt.zzb<zza, C0039zza> implements zzdtg {
            private C0039zza() {
                super(zza.zzhdw);
            }

            /* synthetic */ C0039zza(zzdnj zzdnj) {
                this();
            }
        }

        static {
            zza zza = new zza();
            zzhdw = zza;
            zzdrt.zza(zza.class, zza);
        }

        private zza() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzdnj.zzdk[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0039zza((zzdnj) null);
                case 3:
                    return zzdrt.zza((zzdte) zzhdw, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0002\f\u0003\u000b\u0004\f", new Object[]{"zzhdt", "zzhdu", "zzhdv", "zzhdj"});
                case 4:
                    return zzhdw;
                case 5:
                    zzdtn<zza> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zza.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzhdw);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final zzdne zzasj() {
            zzdne zzep = zzdne.zzep(this.zzhdu);
            return zzep == null ? zzdne.UNRECOGNIZED : zzep;
        }

        public final zzdnw zzask() {
            zzdnw zzew = zzdnw.zzew(this.zzhdj);
            return zzew == null ? zzdnw.UNRECOGNIZED : zzew;
        }

        public final boolean zzavz() {
            return this.zzhdt != null;
        }

        public final zzdna zzawa() {
            zzdna zzdna = this.zzhdt;
            return zzdna == null ? zzdna.zzavm() : zzdna;
        }

        public final int zzawb() {
            return this.zzhdv;
        }
    }

    public static final class zzb extends zzdrt.zzb<zzdnk, zzb> implements zzdtg {
        private zzb() {
            super(zzdnk.zzhds);
        }

        /* synthetic */ zzb(zzdnj zzdnj) {
            this();
        }
    }

    static {
        zzdnk zzdnk = new zzdnk();
        zzhds = zzdnk;
        zzdrt.zza(zzdnk.class, zzdnk);
    }

    private zzdnk() {
    }

    public static zzdnk zzn(byte[] bArr) throws zzdse {
        return (zzdnk) zzdrt.zza(zzhds, bArr);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnj.zzdk[i - 1]) {
            case 1:
                return new zzdnk();
            case 2:
                return new zzb((zzdnj) null);
            case 3:
                return zzdrt.zza((zzdte) zzhds, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u000b\u0002\u001b", new Object[]{"zzhdq", "zzhdr", zza.class});
            case 4:
                return zzhds;
            case 5:
                zzdtn<zzdnk> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdnk.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhds);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final int zzavv() {
        return this.zzhdq;
    }

    public final List<zza> zzavw() {
        return this.zzhdr;
    }

    public final int zzavx() {
        return this.zzhdr.size();
    }
}
