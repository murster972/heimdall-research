package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzctd implements Callable {
    private final zzcta zzggg;

    zzctd(zzcta zzcta) {
        this.zzggg = zzcta;
    }

    public final Object call() {
        return this.zzggg.zzanl();
    }
}
