package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzyh extends zzacl {
    public final void destroy() throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final void zza(zzacd zzacd) {
    }

    public final void zzb(String str, IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final void zzc(IObjectWrapper iObjectWrapper, int i) {
    }

    public final IObjectWrapper zzco(String str) throws RemoteException {
        return null;
    }

    public final void zze(IObjectWrapper iObjectWrapper) {
    }

    public final void zzf(IObjectWrapper iObjectWrapper) {
    }

    public final synchronized void zzg(IObjectWrapper iObjectWrapper) {
    }
}
