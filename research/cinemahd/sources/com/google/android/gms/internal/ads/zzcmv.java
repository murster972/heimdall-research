package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcmv implements zzdxg<zzcms> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzcbi> zzfyl;

    public zzcmv(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzcbi> zzdxp3) {
        this.zzejv = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfyl = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcms(this.zzejv.get(), this.zzfei.get(), this.zzfyl.get());
    }
}
