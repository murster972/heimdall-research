package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;

public final class zzcqe implements zzcty<Bundle> {
    private final String zzabg;
    private final zzuj zzblm;
    private final float zzboh;
    private final boolean zzccq;
    private final int zzdgf;
    private final int zzdgg;
    private final String zzgep;
    private final String zzgeq;

    public zzcqe(zzuj zzuj, String str, boolean z, String str2, float f, int i, int i2, String str3) {
        Preconditions.a(zzuj, (Object) "the adSize must not be null");
        this.zzblm = zzuj;
        this.zzabg = str;
        this.zzccq = z;
        this.zzgep = str2;
        this.zzboh = f;
        this.zzdgf = i;
        this.zzdgg = i2;
        this.zzgeq = str3;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        zzdaa.zza(bundle, "smart_w", "full", this.zzblm.width == -1);
        zzdaa.zza(bundle, "smart_h", "auto", this.zzblm.height == -2);
        zzdaa.zza(bundle, "ene", (Boolean) true, this.zzblm.zzccr);
        zzdaa.zza(bundle, "rafmt", "102", this.zzblm.zzccu);
        zzdaa.zza(bundle, "rafmt", "103", this.zzblm.zzccv);
        zzdaa.zza(bundle, "format", this.zzabg);
        zzdaa.zza(bundle, "fluid", "height", this.zzccq);
        String str = this.zzgep;
        zzdaa.zza(bundle, "sz", str, !TextUtils.isEmpty(str));
        bundle.putFloat("u_sd", this.zzboh);
        bundle.putInt("sw", this.zzdgf);
        bundle.putInt("sh", this.zzdgg);
        String str2 = this.zzgeq;
        zzdaa.zza(bundle, "sc", str2, true ^ TextUtils.isEmpty(str2));
        ArrayList arrayList = new ArrayList();
        zzuj[] zzujArr = this.zzblm.zzccp;
        if (zzujArr == null) {
            Bundle bundle2 = new Bundle();
            bundle2.putInt("height", this.zzblm.height);
            bundle2.putInt("width", this.zzblm.width);
            bundle2.putBoolean("is_fluid_height", this.zzblm.zzccq);
            arrayList.add(bundle2);
        } else {
            for (zzuj zzuj : zzujArr) {
                Bundle bundle3 = new Bundle();
                bundle3.putBoolean("is_fluid_height", zzuj.zzccq);
                bundle3.putInt("height", zzuj.height);
                bundle3.putInt("width", zzuj.width);
                arrayList.add(bundle3);
            }
        }
        bundle.putParcelableArrayList("valid_ad_sizes", arrayList);
    }
}
