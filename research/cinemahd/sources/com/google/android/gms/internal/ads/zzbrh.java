package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final /* synthetic */ class zzbrh implements zzbrn {
    private final zzsy.zza zzfhy;

    zzbrh(zzsy.zza zza) {
        this.zzfhy = zza;
    }

    public final void zzp(Object obj) {
        ((zzbri) obj).zzb(this.zzfhy);
    }
}
