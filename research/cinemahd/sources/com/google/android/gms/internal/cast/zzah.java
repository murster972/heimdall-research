package com.google.android.gms.internal.cast;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;

public interface zzah extends IInterface {

    public static abstract class zza extends zzb implements zzah {
        public static zzah zze(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.internal.IFetchBitmapTask");
            if (queryLocalInterface instanceof zzah) {
                return (zzah) queryLocalInterface;
            }
            return new zzai(iBinder);
        }
    }

    Bitmap zzb(Uri uri) throws RemoteException;
}
