package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;

final /* synthetic */ class zzbty implements zzbqb {
    private final Context zzcri;
    private final zzazb zzfek;
    private final zzczl zzfel;
    private final zzczu zzfem;

    zzbty(Context context, zzazb zzazb, zzczl zzczl, zzczu zzczu) {
        this.zzcri = context;
        this.zzfek = zzazb;
        this.zzfel = zzczl;
        this.zzfem = zzczu;
    }

    public final void onAdLoaded() {
        zzq.zzla().zzb(this.zzcri, this.zzfek.zzbma, this.zzfel.zzglt.toString(), this.zzfem.zzgmm);
    }
}
