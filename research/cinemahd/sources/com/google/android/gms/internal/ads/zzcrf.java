package com.google.android.gms.internal.ads;

import android.content.Context;
import android.media.AudioManager;
import com.google.android.gms.ads.internal.zzq;

public final class zzcrf implements zzcub<zzcrc> {
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcrf(zzdhd zzdhd, Context context) {
        this.zzfov = zzdhd;
        this.zzup = context;
    }

    public final zzdhe<zzcrc> zzanc() {
        return this.zzfov.zzd(new zzcre(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcrc zzanf() throws Exception {
        AudioManager audioManager = (AudioManager) this.zzup.getSystemService("audio");
        return new zzcrc(audioManager.getMode(), audioManager.isMusicActive(), audioManager.isSpeakerphoneOn(), audioManager.getStreamVolume(3), audioManager.getRingerMode(), audioManager.getStreamVolume(2), zzq.zzkv().zzpe(), zzq.zzkv().zzpf());
    }
}
