package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbkv implements zzdxg<zzbsu<zzbqb>> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzczl> zzfda;
    private final zzbkn zzfen;
    private final zzdxp<zzczu> zzfep;

    public zzbkv(zzbkn zzbkn, zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzczu> zzdxp4) {
        this.zzfen = zzbkn;
        this.zzejv = zzdxp;
        this.zzfav = zzdxp2;
        this.zzfda = zzdxp3;
        this.zzfep = zzdxp4;
    }

    public static zzbsu<zzbqb> zza(zzbkn zzbkn, Context context, zzazb zzazb, zzczl zzczl, zzczu zzczu) {
        return (zzbsu) zzdxm.zza(new zzbsu(new zzbkq(context, zzazb, zzczl, zzczu), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfen, this.zzejv.get(), this.zzfav.get(), this.zzfda.get(), this.zzfep.get());
    }
}
