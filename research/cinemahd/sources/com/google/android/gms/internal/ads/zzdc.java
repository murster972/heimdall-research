package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.gass.AdShield2Logger;
import com.google.android.gms.gass.AdShieldVm;
import com.google.android.gms.gass.internal.Program;
import com.google.android.gms.gass.internal.zzk;
import com.google.android.gms.gass.internal.zzo;
import com.google.android.gms.gass.internal.zzp;
import com.google.android.gms.gass.internal.zzq;
import com.google.android.gms.gass.zzd;
import java.util.Map;
import java.util.concurrent.Executor;
import okhttp3.internal.cache.DiskLruCache;

public final class zzdc {
    private final Context zzup;
    private final zzk zzuq;
    private final AdShieldVm zzur;
    private final AdShield2Logger zzus;
    private final Executor zzut;
    private boolean zzuu;

    zzdc(Context context, AdShield2Logger adShield2Logger, zzk zzk, AdShieldVm adShieldVm, Executor executor) {
        this.zzup = context;
        this.zzus = adShield2Logger;
        this.zzuq = zzk;
        this.zzur = adShieldVm;
        this.zzut = executor;
    }

    /* access modifiers changed from: private */
    public final void zzbp() {
        String str;
        String str2;
        Program a2 = this.zzuq.a(zzp.f4056a);
        if (a2 != null) {
            String zzcx = a2.c().zzcx();
            str = a2.c().zzcy();
            str2 = zzcx;
        } else {
            str2 = null;
            str = null;
        }
        try {
            zzo a3 = zzd.a(this.zzup, 1, str2, str, DiskLruCache.VERSION_1, this.zzus);
            if (a3.b == null) {
                return;
            }
            if (a3.b.length != 0) {
                zzfy zza = zzfy.zza(zzdqk.zzu(a3.b), zzdrg.zzazi());
                if ((!zza.zzct().zzcx().isEmpty() && !zza.zzct().zzcy().isEmpty() && zza.zzcv().toByteArray().length != 0) && this.zzuq.a(zza, (zzq) null) && this.zzur.a(this.zzuq.a(zzp.f4056a)) == null) {
                    this.zzuu = true;
                }
            }
        } catch (zzdse e) {
            this.zzus.a(4002, 0, e);
        }
    }

    private final void zzbq() {
        if (!this.zzuu || (this.zzur.b() != null && this.zzur.b().e())) {
            zzbo();
        }
    }

    public final void zza(MotionEvent motionEvent) {
        zzbq();
        if (zzbn()) {
            this.zzur.a((String) null, motionEvent);
        }
    }

    public final String zzb(Context context) {
        zzbq();
        if (!zzbn()) {
            return "";
        }
        long currentTimeMillis = System.currentTimeMillis();
        String a2 = this.zzur.a(context, (String) null);
        this.zzus.a(5001, System.currentTimeMillis() - currentTimeMillis, a2, (Map<String, String>) null);
        return a2;
    }

    public final synchronized boolean zzbn() {
        if (this.zzuu) {
            return true;
        }
        Program a2 = this.zzuq.a(zzp.f4056a);
        if (a2 != null && !a2.f() && this.zzur.a(a2) == null) {
            this.zzuu = true;
        }
        return this.zzuu;
    }

    public final void zzbo() {
        this.zzut.execute(new zzdf(this));
    }

    public final String zza(Context context, String str, View view, Activity activity) {
        zzbq();
        if (!zzbn()) {
            return "";
        }
        long currentTimeMillis = System.currentTimeMillis();
        String a2 = this.zzur.a(context, (String) null, str, view, activity);
        this.zzus.a(5000, System.currentTimeMillis() - currentTimeMillis, a2, (Map<String, String>) null);
        return a2;
    }

    public final String zza(Context context, View view, Activity activity) {
        zzbq();
        if (!zzbn()) {
            return "";
        }
        long currentTimeMillis = System.currentTimeMillis();
        String a2 = this.zzur.a(context, (String) null, view, activity);
        this.zzus.a(5002, System.currentTimeMillis() - currentTimeMillis, a2, (Map<String, String>) null);
        return a2;
    }
}
