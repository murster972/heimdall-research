package com.google.android.gms.internal.ads;

public final class zzbmv implements zzdxg<String> {
    private final zzbmt zzfgf;

    private zzbmv(zzbmt zzbmt) {
        this.zzfgf = zzbmt;
    }

    public static zzbmv zza(zzbmt zzbmt) {
        return new zzbmv(zzbmt);
    }

    public static String zzb(zzbmt zzbmt) {
        return (String) zzdxm.zza(zzbmt.zzagy(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzb(this.zzfgf);
    }
}
