package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzanj extends IInterface {
    void onFailure(String str) throws RemoteException;

    void zzdn(String str) throws RemoteException;
}
