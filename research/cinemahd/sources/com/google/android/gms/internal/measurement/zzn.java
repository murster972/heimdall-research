package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

public interface zzn extends IInterface {
    void zza(Bundle bundle) throws RemoteException;
}
