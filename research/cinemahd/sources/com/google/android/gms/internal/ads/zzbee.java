package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import com.google.android.gms.internal.ads.zzbei;
import com.google.android.gms.internal.ads.zzbeq;
import com.google.android.gms.internal.ads.zzbes;

@TargetApi(17)
public final class zzbee<WebViewT extends zzbei & zzbeq & zzbes> {
    private final zzbej zzehm;
    private final WebViewT zzehn;

    private zzbee(WebViewT webviewt, zzbej zzbej) {
        this.zzehm = zzbej;
        this.zzehn = webviewt;
    }

    public static zzbee<zzbdi> zzc(zzbdi zzbdi) {
        return new zzbee<>(zzbdi, new zzbeh(zzbdi));
    }

    @JavascriptInterface
    public final String getClickSignals(String str) {
        if (TextUtils.isEmpty(str)) {
            zzavs.zzed("Click string is empty, not proceeding.");
            return "";
        }
        zzdq zzaad = ((zzbeq) this.zzehn).zzaad();
        if (zzaad == null) {
            zzavs.zzed("Signal utils is empty, ignoring.");
            return "";
        }
        zzdg zzbw = zzaad.zzbw();
        if (zzbw == null) {
            zzavs.zzed("Signals object is empty, ignoring.");
            return "";
        } else if (this.zzehn.getContext() != null) {
            return zzbw.zza(this.zzehn.getContext(), str, ((zzbes) this.zzehn).getView(), this.zzehn.zzyn());
        } else {
            zzavs.zzed("Context is null, ignoring.");
            return "";
        }
    }

    @JavascriptInterface
    public final void notify(String str) {
        if (TextUtils.isEmpty(str)) {
            zzayu.zzez("URL is empty, ignoring message");
        } else {
            zzawb.zzdsr.post(new zzbeg(this, str));
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzfp(String str) {
        this.zzehm.zzh(Uri.parse(str));
    }
}
