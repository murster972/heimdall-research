package com.google.android.gms.internal.ads;

public interface zzden<T> {
    T get();
}
