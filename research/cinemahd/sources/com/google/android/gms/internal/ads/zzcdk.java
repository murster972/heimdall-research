package com.google.android.gms.internal.ads;

import java.util.Map;

public final class zzcdk implements zzdxg<zzcdf> {
    private final zzdxp<zzsm> zzfsb;
    private final zzdxp<Map<zzdco, zzcdh>> zzfso;

    private zzcdk(zzdxp<zzsm> zzdxp, zzdxp<Map<zzdco, zzcdh>> zzdxp2) {
        this.zzfsb = zzdxp;
        this.zzfso = zzdxp2;
    }

    public static zzcdk zzu(zzdxp<zzsm> zzdxp, zzdxp<Map<zzdco, zzcdh>> zzdxp2) {
        return new zzcdk(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcdf(this.zzfsb.get(), this.zzfso.get());
    }
}
