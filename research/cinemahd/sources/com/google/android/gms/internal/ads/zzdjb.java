package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.util.logging.Logger;

public class zzdjb implements zzdis<zzdhx> {
    private static final Logger logger = Logger.getLogger(zzdjb.class.getName());

    static class zza implements zzdhx {
        private final zzdiq<zzdhx> zzgzb;

        private zza(zzdiq<zzdhx> zzdiq) {
            this.zzgzb = zzdiq;
        }

        public final byte[] zzc(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
            return zzdoi.zza(this.zzgzb.zzasm().zzasl(), this.zzgzb.zzasm().zzasi().zzc(bArr, bArr2));
        }
    }

    zzdjb() {
    }

    public final /* synthetic */ Object zza(zzdiq zzdiq) throws GeneralSecurityException {
        return new zza(zzdiq);
    }

    public final Class<zzdhx> zzarz() {
        return zzdhx.class;
    }
}
