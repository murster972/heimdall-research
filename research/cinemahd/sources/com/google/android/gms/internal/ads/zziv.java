package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.UUID;

public final class zziv implements Parcelable, Comparator<zza> {
    public static final Parcelable.Creator<zziv> CREATOR = new zziu();
    private int zzafx;
    private final zza[] zzamm;
    public final int zzamn;

    public static final class zza implements Parcelable {
        public static final Parcelable.Creator<zza> CREATOR = new zziw();
        private final byte[] data;
        private final String mimeType;
        /* access modifiers changed from: private */
        public final UUID uuid;
        private int zzafx;
        public final boolean zzamo;

        public zza(UUID uuid2, String str, byte[] bArr) {
            this(uuid2, str, bArr, false);
        }

        public final int describeContents() {
            return 0;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof zza)) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            zza zza = (zza) obj;
            return this.mimeType.equals(zza.mimeType) && zzoq.zza(this.uuid, zza.uuid) && Arrays.equals(this.data, zza.data);
        }

        public final int hashCode() {
            if (this.zzafx == 0) {
                this.zzafx = (((this.uuid.hashCode() * 31) + this.mimeType.hashCode()) * 31) + Arrays.hashCode(this.data);
            }
            return this.zzafx;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            parcel.writeLong(this.uuid.getMostSignificantBits());
            parcel.writeLong(this.uuid.getLeastSignificantBits());
            parcel.writeString(this.mimeType);
            parcel.writeByteArray(this.data);
            parcel.writeByte(this.zzamo ? (byte) 1 : 0);
        }

        private zza(UUID uuid2, String str, byte[] bArr, boolean z) {
            this.uuid = (UUID) zzoc.checkNotNull(uuid2);
            this.mimeType = (String) zzoc.checkNotNull(str);
            this.data = (byte[]) zzoc.checkNotNull(bArr);
            this.zzamo = false;
        }

        zza(Parcel parcel) {
            this.uuid = new UUID(parcel.readLong(), parcel.readLong());
            this.mimeType = parcel.readString();
            this.data = parcel.createByteArray();
            this.zzamo = parcel.readByte() != 0;
        }
    }

    public zziv(zza... zzaArr) {
        this(true, zzaArr);
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zza zza2 = (zza) obj;
        zza zza3 = (zza) obj2;
        if (zzgi.zzacb.equals(zza2.uuid)) {
            return zzgi.zzacb.equals(zza3.uuid) ? 0 : 1;
        }
        return zza2.uuid.compareTo(zza3.uuid);
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || zziv.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.zzamm, ((zziv) obj).zzamm);
    }

    public final int hashCode() {
        if (this.zzafx == 0) {
            this.zzafx = Arrays.hashCode(this.zzamm);
        }
        return this.zzafx;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.zzamm, 0);
    }

    public final zza zzaa(int i) {
        return this.zzamm[i];
    }

    private zziv(boolean z, zza... zzaArr) {
        zza[] zzaArr2 = (zza[]) zzaArr.clone();
        Arrays.sort(zzaArr2, this);
        int i = 1;
        while (i < zzaArr2.length) {
            if (!zzaArr2[i - 1].uuid.equals(zzaArr2[i].uuid)) {
                i++;
            } else {
                String valueOf = String.valueOf(zzaArr2[i].uuid);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
                sb.append("Duplicate data for uuid: ");
                sb.append(valueOf);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        this.zzamm = zzaArr2;
        this.zzamn = zzaArr2.length;
    }

    zziv(Parcel parcel) {
        this.zzamm = (zza[]) parcel.createTypedArray(zza.CREATOR);
        this.zzamn = this.zzamm.length;
    }
}
