package com.google.android.gms.internal.ads;

import java.util.Collections;

final /* synthetic */ class zzcky implements zzded {
    static final zzded zzdoq = new zzcky();

    private zzcky() {
    }

    public final Object apply(Object obj) {
        return Collections.singletonList(zzdgs.zzaj((zzbwk) obj));
    }
}
