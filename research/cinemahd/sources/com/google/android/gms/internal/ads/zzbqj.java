package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;
import java.util.Set;

public final class zzbqj extends zzbrl<zzo> implements zzo {
    public zzbqj(Set<zzbsu<zzo>> set) {
        super(set);
    }

    public final synchronized void onPause() {
        zza(zzbql.zzfhp);
    }

    public final synchronized void onResume() {
        zza(zzbqk.zzfhp);
    }

    public final synchronized void zzte() {
        zza(zzbqi.zzfhp);
    }

    public final synchronized void zztf() {
        zza(zzbqn.zzfhp);
    }
}
