package com.google.android.gms.internal.ads;

import java.util.Map;

public final class zzbir implements zzbil {
    private final zzavu zzdrk;

    public zzbir(zzavu zzavu) {
        this.zzdrk = zzavu;
    }

    public final void zzk(Map<String, String> map) {
        String str = map.get("value");
        if ("auto_collect_location".equals(map.get("key"))) {
            this.zzdrk.zzaq(Boolean.parseBoolean(str));
        }
    }
}
