package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzast extends IInterface {
    void zza(zzasf zzasf, String str, String str2) throws RemoteException;
}
