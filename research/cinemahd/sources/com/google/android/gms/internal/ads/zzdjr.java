package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdjr extends zzdik<zzdhx, zzdno> {
    zzdjr(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        String zzawk = ((zzdno) obj).zzawh().zzawk();
        return zzdil.zzgu(zzawk).zzgw(zzawk);
    }
}
