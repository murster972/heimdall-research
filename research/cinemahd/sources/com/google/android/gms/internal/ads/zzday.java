package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.LinkedList;

final class zzday {
    private final int maxEntries;
    private final LinkedList<zzdbi<?>> zzgnp = new LinkedList<>();
    private final int zzgnq;
    private final zzdbp zzgnr;

    public zzday(int i, int i2) {
        this.maxEntries = i;
        this.zzgnq = i2;
        this.zzgnr = new zzdbp();
    }

    private final void zzapc() {
        while (!this.zzgnp.isEmpty()) {
            if (zzq.zzkx().b() - this.zzgnp.getFirst().zzgpe >= ((long) this.zzgnq)) {
                this.zzgnr.zzapt();
                this.zzgnp.remove();
            } else {
                return;
            }
        }
    }

    public final long getCreationTimeMillis() {
        return this.zzgnr.getCreationTimeMillis();
    }

    public final int size() {
        zzapc();
        return this.zzgnp.size();
    }

    public final zzdbi<?> zzaox() {
        this.zzgnr.zzapr();
        zzapc();
        if (this.zzgnp.isEmpty()) {
            return null;
        }
        zzdbi<?> remove = this.zzgnp.remove();
        if (remove != null) {
            this.zzgnr.zzaps();
        }
        return remove;
    }

    public final long zzaoy() {
        return this.zzgnr.zzaoy();
    }

    public final int zzaoz() {
        return this.zzgnr.zzaoz();
    }

    public final String zzapa() {
        return this.zzgnr.zzapk();
    }

    public final zzdbo zzapb() {
        return this.zzgnr.zzapu();
    }

    public final boolean zzb(zzdbi<?> zzdbi) {
        this.zzgnr.zzapr();
        zzapc();
        if (this.zzgnp.size() == this.maxEntries) {
            return false;
        }
        this.zzgnp.add(zzdbi);
        return true;
    }
}
