package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzace implements NativeCustomTemplateAd.DisplayOpenMeasurement {
    private final zzade zzcvr;

    public zzace(zzade zzade) {
        this.zzcvr = zzade;
        try {
            zzade.zzrn();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final void setView(View view) {
        try {
            this.zzcvr.zzq(ObjectWrapper.a(view));
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final boolean start() {
        try {
            return this.zzcvr.zzrm();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return false;
        }
    }
}
