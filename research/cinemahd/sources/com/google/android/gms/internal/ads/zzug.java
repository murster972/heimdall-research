package com.google.android.gms.internal.ads;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.ArrayList;
import java.util.List;

public final class zzug extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzug> CREATOR = new zzui();
    public final Bundle extras;
    public final int versionCode;
    public final int zzabo;
    public final int zzabp;
    public final String zzabq;
    public final boolean zzbkh;
    @Deprecated
    public final long zzcby;
    @Deprecated
    public final int zzcbz;
    public final List<String> zzcca;
    public final boolean zzccb;
    public final String zzccc;
    public final zzys zzccd;
    public final String zzcce;
    public final Bundle zzccf;
    public final Bundle zzccg;
    public final List<String> zzcch;
    public final String zzcci;
    public final String zzccj;
    @Deprecated
    public final boolean zzcck;
    public final List<String> zzccl;
    public final zzua zzccm;
    public final Location zzmi;

    public zzug(int i, long j, Bundle bundle, int i2, List<String> list, boolean z, int i3, boolean z2, String str, zzys zzys, Location location, String str2, Bundle bundle2, Bundle bundle3, List<String> list2, String str3, String str4, boolean z3, zzua zzua, int i4, String str5, List<String> list3) {
        this.versionCode = i;
        this.zzcby = j;
        this.extras = bundle == null ? new Bundle() : bundle;
        this.zzcbz = i2;
        this.zzcca = list;
        this.zzccb = z;
        this.zzabo = i3;
        this.zzbkh = z2;
        this.zzccc = str;
        this.zzccd = zzys;
        this.zzmi = location;
        this.zzcce = str2;
        this.zzccf = bundle2 == null ? new Bundle() : bundle2;
        this.zzccg = bundle3;
        this.zzcch = list2;
        this.zzcci = str3;
        this.zzccj = str4;
        this.zzcck = z3;
        this.zzccm = zzua;
        this.zzabp = i4;
        this.zzabq = str5;
        this.zzccl = list3 == null ? new ArrayList<>() : list3;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzug)) {
            return false;
        }
        zzug zzug = (zzug) obj;
        if (this.versionCode != zzug.versionCode || this.zzcby != zzug.zzcby || !Objects.a(this.extras, zzug.extras) || this.zzcbz != zzug.zzcbz || !Objects.a(this.zzcca, zzug.zzcca) || this.zzccb != zzug.zzccb || this.zzabo != zzug.zzabo || this.zzbkh != zzug.zzbkh || !Objects.a(this.zzccc, zzug.zzccc) || !Objects.a(this.zzccd, zzug.zzccd) || !Objects.a(this.zzmi, zzug.zzmi) || !Objects.a(this.zzcce, zzug.zzcce) || !Objects.a(this.zzccf, zzug.zzccf) || !Objects.a(this.zzccg, zzug.zzccg) || !Objects.a(this.zzcch, zzug.zzcch) || !Objects.a(this.zzcci, zzug.zzcci) || !Objects.a(this.zzccj, zzug.zzccj) || this.zzcck != zzug.zzcck || this.zzabp != zzug.zzabp || !Objects.a(this.zzabq, zzug.zzabq) || !Objects.a(this.zzccl, zzug.zzccl)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Objects.a(Integer.valueOf(this.versionCode), Long.valueOf(this.zzcby), this.extras, Integer.valueOf(this.zzcbz), this.zzcca, Boolean.valueOf(this.zzccb), Integer.valueOf(this.zzabo), Boolean.valueOf(this.zzbkh), this.zzccc, this.zzccd, this.zzmi, this.zzcce, this.zzccf, this.zzccg, this.zzcch, this.zzcci, this.zzccj, Boolean.valueOf(this.zzcck), Integer.valueOf(this.zzabp), this.zzabq, this.zzccl);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.versionCode);
        SafeParcelWriter.a(parcel, 2, this.zzcby);
        SafeParcelWriter.a(parcel, 3, this.extras, false);
        SafeParcelWriter.a(parcel, 4, this.zzcbz);
        SafeParcelWriter.b(parcel, 5, this.zzcca, false);
        SafeParcelWriter.a(parcel, 6, this.zzccb);
        SafeParcelWriter.a(parcel, 7, this.zzabo);
        SafeParcelWriter.a(parcel, 8, this.zzbkh);
        SafeParcelWriter.a(parcel, 9, this.zzccc, false);
        SafeParcelWriter.a(parcel, 10, (Parcelable) this.zzccd, i, false);
        SafeParcelWriter.a(parcel, 11, (Parcelable) this.zzmi, i, false);
        SafeParcelWriter.a(parcel, 12, this.zzcce, false);
        SafeParcelWriter.a(parcel, 13, this.zzccf, false);
        SafeParcelWriter.a(parcel, 14, this.zzccg, false);
        SafeParcelWriter.b(parcel, 15, this.zzcch, false);
        SafeParcelWriter.a(parcel, 16, this.zzcci, false);
        SafeParcelWriter.a(parcel, 17, this.zzccj, false);
        SafeParcelWriter.a(parcel, 18, this.zzcck);
        SafeParcelWriter.a(parcel, 19, (Parcelable) this.zzccm, i, false);
        SafeParcelWriter.a(parcel, 20, this.zzabp);
        SafeParcelWriter.a(parcel, 21, this.zzabq, false);
        SafeParcelWriter.b(parcel, 22, this.zzccl, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
