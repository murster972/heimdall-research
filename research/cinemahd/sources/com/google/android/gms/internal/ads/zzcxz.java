package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.AdMetadataListener;
import java.util.concurrent.atomic.AtomicReference;

public final class zzcxz extends AdMetadataListener implements zzbov, zzbow, zzbpa, zzbqb, zzcxq {
    private final zzdax zzgjf;
    private final AtomicReference<AdMetadataListener> zzgkj = new AtomicReference<>();
    private final AtomicReference<zzaso> zzgkk = new AtomicReference<>();
    private final AtomicReference<zzasl> zzgkl = new AtomicReference<>();
    private final AtomicReference<zzaro> zzgkm = new AtomicReference<>();
    private final AtomicReference<zzast> zzgkn = new AtomicReference<>();
    private final AtomicReference<zzarj> zzgko = new AtomicReference<>();
    private zzcxz zzgkp = null;

    public zzcxz(zzdax zzdax) {
        this.zzgjf = zzdax;
    }

    public static zzcxz zza(zzcxz zzcxz) {
        zzcxz zzcxz2 = new zzcxz(zzcxz.zzgjf);
        zzcxz2.zzb((zzcxq) zzcxz);
        return zzcxz2;
    }

    public final void onAdClosed() {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxz.zzgjf.onAdClosed();
                zzcxp.zza(zzcxz.zzgkl, zzcyn.zzgjk);
                zzcxp.zza(zzcxz.zzgkm, zzcym.zzgjk);
                return;
            }
        }
    }

    public final void onAdFailedToLoad(int i) {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkk, new zzcyj(i));
                zzcxp.zza(zzcxz.zzgkm, new zzcyi(i));
                return;
            }
        }
    }

    public final void onAdLeftApplication() {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkm, zzcyp.zzgjk);
                return;
            }
        }
    }

    public final void onAdLoaded() {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkk, zzcxy.zzgjk);
                zzcxp.zza(zzcxz.zzgkm, zzcyb.zzgjk);
                return;
            }
        }
    }

    public final void onAdMetadataChanged() {
        zzcxz zzcxz = this.zzgkp;
        if (zzcxz != null) {
            zzcxz.onAdMetadataChanged();
        } else {
            zzcxp.zza(this.zzgkj, zzcyg.zzgjk);
        }
    }

    public final void onAdOpened() {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkl, zzcyl.zzgjk);
                zzcxp.zza(zzcxz.zzgkm, zzcyk.zzgjk);
                return;
            }
        }
    }

    public final void onRewardedVideoCompleted() {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkm, zzcye.zzgjk);
                return;
            }
        }
    }

    public final void onRewardedVideoStarted() {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkm, zzcyo.zzgjk);
                return;
            }
        }
    }

    public final void zzb(zzasl zzasl) {
        this.zzgkl.set(zzasl);
    }

    public final void zzco(int i) {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkl, new zzcyh(i));
                return;
            }
        }
    }

    public final void zzb(zzast zzast) {
        this.zzgkn.set(zzast);
    }

    public final void zza(zzaso zzaso) {
        this.zzgkk.set(zzaso);
    }

    @Deprecated
    public final void zzb(zzaro zzaro) {
        this.zzgkm.set(zzaro);
    }

    public final void zza(AdMetadataListener adMetadataListener) {
        this.zzgkj.set(adMetadataListener);
    }

    @Deprecated
    public final void zzb(zzarj zzarj) {
        this.zzgko.set(zzarj);
    }

    public final void zzb(zzare zzare, String str, String str2) {
        zzcxz zzcxz = this;
        while (true) {
            zzcxz zzcxz2 = zzcxz.zzgkp;
            if (zzcxz2 != null) {
                zzcxz = zzcxz2;
            } else {
                zzcxp.zza(zzcxz.zzgkl, new zzcya(zzare));
                zzcxp.zza(zzcxz.zzgkn, new zzcyd(zzare, str, str2));
                zzcxp.zza(zzcxz.zzgkm, new zzcyc(zzare));
                zzcxp.zza(zzcxz.zzgko, new zzcyf(zzare, str, str2));
                return;
            }
        }
    }

    public final void zzb(zzcxq zzcxq) {
        this.zzgkp = (zzcxz) zzcxq;
    }
}
