package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import java.util.Set;

public final class zzcss implements zzcub<zzcst> {
    private final zzdhd zzgej;
    private final Set<String> zzgeu;
    private final Context zzup;

    public zzcss(zzdhd zzdhd, Context context, Set<String> set) {
        this.zzgej = zzdhd;
        this.zzup = context;
        this.zzgeu = set;
    }

    public final zzdhe<zzcst> zzanc() {
        return this.zzgej.zzd(new zzcsv(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcst zzank() throws Exception {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue() || !zzcst.zzd(this.zzgeu)) {
            return new zzcst((String) null);
        }
        return new zzcst(zzq.zzlf().getVersion(this.zzup));
    }
}
