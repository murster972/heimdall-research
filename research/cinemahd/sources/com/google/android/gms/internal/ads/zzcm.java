package com.google.android.gms.internal.ads;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

final class zzcm implements Runnable {
    private zzcm() {
    }

    public final void run() {
        try {
            MessageDigest unused = zzck.zzmr = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException unused2) {
        } finally {
            zzck.zzmu.countDown();
        }
    }
}
