package com.google.android.gms.internal.ads;

import org.json.JSONArray;

final /* synthetic */ class zzckw implements zzdgf {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcku zzfzs;

    zzckw(zzcku zzcku, zzczt zzczt, zzczl zzczl) {
        this.zzfzs = zzcku;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfzs.zza(this.zzfot, this.zzfel, (JSONArray) obj);
    }
}
