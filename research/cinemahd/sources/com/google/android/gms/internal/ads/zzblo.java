package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public final class zzblo extends zzbkk {
    private final Executor zzfci;
    private final zzaea zzfex;
    private final Runnable zzfey;

    public zzblo(zzbmg zzbmg, zzaea zzaea, Runnable runnable, Executor executor) {
        super(zzbmg);
        this.zzfex = zzaea;
        this.zzfey = runnable;
        this.zzfci = executor;
    }

    public final zzxb getVideoController() {
        return null;
    }

    public final void zza(ViewGroup viewGroup, zzuj zzuj) {
    }

    public final zzczk zzafz() {
        return null;
    }

    public final View zzaga() {
        return null;
    }

    public final int zzage() {
        return 0;
    }

    public final void zzagf() {
        this.zzfci.execute(new zzblq(this, new zzbln(new AtomicReference(this.zzfey))));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zze(Runnable runnable) {
        try {
            if (!this.zzfex.zzm(ObjectWrapper.a(runnable))) {
                runnable.run();
            }
        } catch (RemoteException unused) {
            runnable.run();
        }
    }

    public final void zzjy() {
    }
}
