package com.google.android.gms.internal.cast;

import android.graphics.Bitmap;

final class zzaw implements zzab {
    private final /* synthetic */ zzav zzsk;

    zzaw(zzav zzav) {
        this.zzsk = zzav;
    }

    public final void zza(Bitmap bitmap) {
        if (bitmap != null) {
            this.zzsk.zzsi.setImageBitmap(bitmap);
        }
    }
}
