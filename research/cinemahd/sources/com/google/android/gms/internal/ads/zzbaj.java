package com.google.android.gms.internal.ads;

import android.content.Context;

public abstract class zzbaj {
    protected static boolean zzb(zzbaz zzbaz) {
        return zzbaz.zzzy().zzabt();
    }

    public abstract zzbag zza(Context context, zzbaz zzbaz, int i, boolean z, zzaae zzaae, zzbaw zzbaw);
}
