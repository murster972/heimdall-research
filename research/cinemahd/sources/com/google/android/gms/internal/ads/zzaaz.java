package com.google.android.gms.internal.ads;

public final class zzaaz {
    public static zzaan<Boolean> zzcti = zzaan.zzf("gads:js_flags:mf", false);
    public static zzaan<Long> zzctj = zzaan.zzb("gads:js_flags:update_interval", 43200000);
}
