package com.google.android.gms.internal.ads;

final class zzbac implements Runnable {
    private final /* synthetic */ zzazx zzdxn;

    zzbac(zzazx zzazx) {
        this.zzdxn = zzazx;
    }

    public final void run() {
        if (this.zzdxn.zzdxm != null) {
            this.zzdxn.zzdxm.onPaused();
            this.zzdxn.zzdxm.zzxw();
        }
    }
}
