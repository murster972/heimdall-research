package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzaml implements Runnable {
    private final /* synthetic */ zzamf zzdef;

    zzaml(zzamf zzamf) {
        this.zzdef = zzamf;
    }

    public final void run() {
        try {
            this.zzdef.zzdds.onAdLeftApplication();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
