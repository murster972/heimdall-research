package com.google.android.gms.internal.ads;

final class zzcyy implements zzcoz<zzcbb> {
    private final /* synthetic */ zzcyz zzgkv;

    zzcyy(zzcyz zzcyz) {
        this.zzgkv = zzcyz;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzcbb zzcbb = (zzcbb) obj;
        synchronized (this.zzgkv) {
            zzcbb unused = this.zzgkv.zzgky = zzcbb;
            this.zzgkv.zzgky.zzagf();
        }
    }

    public final void zzamx() {
        synchronized (this.zzgkv) {
            zzcbb unused = this.zzgkv.zzgky = null;
        }
    }
}
