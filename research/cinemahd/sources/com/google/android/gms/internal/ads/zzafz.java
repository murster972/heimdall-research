package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.Map;

public final class zzafz extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzafz> CREATOR = new zzagc();
    private final String url;
    private final String[] zzcxv;
    private final String[] zzcxw;

    zzafz(String str, String[] strArr, String[] strArr2) {
        this.url = str;
        this.zzcxv = strArr;
        this.zzcxw = strArr2;
    }

    public static zzafz zzh(zzq<?> zzq) throws zzb {
        Map<String, String> headers = zzq.getHeaders();
        int size = headers.size();
        String[] strArr = new String[size];
        String[] strArr2 = new String[size];
        int i = 0;
        for (Map.Entry next : headers.entrySet()) {
            strArr[i] = (String) next.getKey();
            strArr2[i] = (String) next.getValue();
            i++;
        }
        return new zzafz(zzq.getUrl(), strArr, strArr2);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.url, false);
        SafeParcelWriter.a(parcel, 2, this.zzcxv, false);
        SafeParcelWriter.a(parcel, 3, this.zzcxw, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
