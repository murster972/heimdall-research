package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcun implements zzdxg<zzcul> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdhd> zzfcv;

    private zzcun(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzejv = zzdxp2;
    }

    public static zzcun zzav(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2) {
        return new zzcun(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcul(this.zzfcv.get(), this.zzejv.get());
    }
}
