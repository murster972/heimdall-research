package com.google.android.gms.internal.ads;

public final class zzbok implements zzdxg<zzczu> {
    private final zzbod zzfhi;

    private zzbok(zzbod zzbod) {
        this.zzfhi = zzbod;
    }

    public static zzbok zzi(zzbod zzbod) {
        return new zzbok(zzbod);
    }

    public static zzczu zzj(zzbod zzbod) {
        return (zzczu) zzdxm.zza(zzbod.zzahd(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzj(this.zzfhi);
    }
}
