package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.ads.internal.overlay.zzt;

final class zzcas implements zzo, zzt, zzaew, zzaey, zzty {
    private zzty zzcbt;
    private zzaew zzcwq;
    private zzaey zzcws;
    private zzo zzdhq;
    private zzt zzdhu;

    private zzcas() {
    }

    public final synchronized void onAdClicked() {
        if (this.zzcbt != null) {
            this.zzcbt.onAdClicked();
        }
    }

    public final synchronized void onAppEvent(String str, String str2) {
        if (this.zzcws != null) {
            this.zzcws.onAppEvent(str, str2);
        }
    }

    public final synchronized void onPause() {
        if (this.zzdhq != null) {
            this.zzdhq.onPause();
        }
    }

    public final synchronized void onResume() {
        if (this.zzdhq != null) {
            this.zzdhq.onResume();
        }
    }

    public final synchronized void zza(String str, Bundle bundle) {
        if (this.zzcwq != null) {
            this.zzcwq.zza(str, bundle);
        }
    }

    public final synchronized void zzte() {
        if (this.zzdhq != null) {
            this.zzdhq.zzte();
        }
    }

    public final synchronized void zztf() {
        if (this.zzdhq != null) {
            this.zzdhq.zztf();
        }
    }

    public final synchronized void zztv() {
        if (this.zzdhu != null) {
            this.zzdhu.zztv();
        }
    }

    /* synthetic */ zzcas(zzcao zzcao) {
        this();
    }

    /* access modifiers changed from: private */
    public final synchronized void zza(zzty zzty, zzaew zzaew, zzo zzo, zzaey zzaey, zzt zzt) {
        this.zzcbt = zzty;
        this.zzcwq = zzaew;
        this.zzdhq = zzo;
        this.zzcws = zzaey;
        this.zzdhu = zzt;
    }
}
