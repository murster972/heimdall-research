package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface zzbf {
    String getType();

    void zza(zzbi zzbi);

    void zza(zzdws zzdws, ByteBuffer byteBuffer, long j, zzbe zzbe) throws IOException;
}
