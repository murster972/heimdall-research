package com.google.android.gms.internal.ads;

final class zzajm implements zzazp<zzaif> {
    private final /* synthetic */ zzajf zzdag;

    zzajm(zzajj zzajj, zzajf zzajf) {
        this.zzdag = zzajf;
    }

    public final /* synthetic */ void zzh(Object obj) {
        zzavs.zzed("Getting a new session for JS Engine.");
        this.zzdag.zzm(((zzaif) obj).zzrz());
    }
}
