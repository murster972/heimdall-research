package com.google.android.gms.internal.cast;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.support.v4.media.session.PlaybackStateCompat;

public final class zzac extends AsyncTask<Uri, Long, Bitmap> {
    private static final zzdw zzbf = new zzdw("FetchBitmapTask");
    private final zzah zzqy;
    private final zzae zzqz;

    public zzac(Context context, zzae zzae) {
        this(context, 0, 0, false, PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE, 5, 333, 10000, zzae);
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final Bitmap doInBackground(Uri... uriArr) {
        if (uriArr.length == 1 && uriArr[0] != null) {
            try {
                return this.zzqy.zzb(uriArr[0]);
            } catch (RemoteException e) {
                zzbf.zza(e, "Unable to call %s on %s.", "doFetch", zzah.class.getSimpleName());
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        zzae zzae = this.zzqz;
        if (zzae != null) {
            zzae.onPostExecute(bitmap);
        }
    }

    public zzac(Context context, int i, int i2, boolean z, zzae zzae) {
        this(context, i, i2, false, PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE, 5, 333, 10000, zzae);
    }

    private zzac(Context context, int i, int i2, boolean z, long j, int i3, int i4, int i5, zzae zzae) {
        this.zzqy = zze.zza(context.getApplicationContext(), this, new zzag(this), i, i2, z, PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE, 5, 333, 10000);
        this.zzqz = zzae;
    }
}
