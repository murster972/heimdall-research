package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzaek implements UnifiedNativeAd.MediaContent {
    private final zzacd zzcwg;

    public zzaek(zzacd zzacd) {
        this.zzcwg = zzacd;
    }

    public final float getAspectRatio() {
        try {
            return this.zzcwg.getAspectRatio();
        } catch (RemoteException unused) {
            return 0.0f;
        }
    }

    public final Drawable getMainImage() {
        try {
            IObjectWrapper zzre = this.zzcwg.zzre();
            if (zzre != null) {
                return (Drawable) ObjectWrapper.a(zzre);
            }
            return null;
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final void setMainImage(Drawable drawable) {
        try {
            this.zzcwg.zzo(ObjectWrapper.a(drawable));
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final zzacd zzrr() {
        return this.zzcwg;
    }
}
