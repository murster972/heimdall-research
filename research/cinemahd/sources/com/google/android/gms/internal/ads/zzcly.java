package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzcly implements zzbuv {
    private final zzcip zzfyq;

    zzcly(zzcip zzcip) {
        this.zzfyq = zzcip;
    }

    public final void zza(boolean z, Context context) {
        zzcip zzcip = this.zzfyq;
        try {
            ((zzdac) zzcip.zzddn).setImmersiveMode(z);
            ((zzdac) zzcip.zzddn).zzcb(context);
        } catch (zzdab e) {
            zzayu.zzd("Cannot show rewarded .", e);
        }
    }
}
