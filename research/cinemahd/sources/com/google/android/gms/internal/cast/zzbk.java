package com.google.android.gms.internal.cast;

import android.view.View;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbk extends UIController implements RemoteMediaClient.ProgressListener {
    private final View view;
    private final zzbh zzrw;

    public zzbk(View view2, zzbh zzbh) {
        this.view = view2;
        this.zzrw = zzbh;
        this.view.setEnabled(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        if (r3.zzrw.zzdp() == false) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzdk() {
        /*
            r3 = this;
            com.google.android.gms.cast.framework.media.RemoteMediaClient r0 = r3.getRemoteMediaClient()
            r1 = 1
            if (r0 == 0) goto L_0x0030
            boolean r2 = r0.k()
            if (r2 == 0) goto L_0x0030
            boolean r2 = r0.q()
            if (r2 == 0) goto L_0x0014
            goto L_0x0030
        L_0x0014:
            boolean r0 = r0.m()
            if (r0 != 0) goto L_0x001d
            android.view.View r0 = r3.view
            goto L_0x0033
        L_0x001d:
            android.view.View r0 = r3.view
            com.google.android.gms.internal.cast.zzbh r2 = r3.zzrw
            boolean r2 = r2.zzdo()
            if (r2 == 0) goto L_0x0032
            com.google.android.gms.internal.cast.zzbh r2 = r3.zzrw
            boolean r2 = r2.zzdp()
            if (r2 != 0) goto L_0x0032
            goto L_0x0033
        L_0x0030:
            android.view.View r0 = r3.view
        L_0x0032:
            r1 = 0
        L_0x0033:
            r0.setEnabled(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzbk.zzdk():void");
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onProgressUpdated(long j, long j2) {
        zzdk();
    }

    public final void onSendingRemoteMediaRequest() {
        this.view.setEnabled(false);
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this, 1000);
        }
        zzdk();
    }

    public final void onSessionEnded() {
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this);
        }
        this.view.setEnabled(false);
        super.onSessionEnded();
        zzdk();
    }
}
