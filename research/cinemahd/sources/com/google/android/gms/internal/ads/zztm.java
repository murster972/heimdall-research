package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final class zztm implements zzdsa {
    static final zzdsa zzew = new zztm();

    private zztm() {
    }

    public final boolean zzf(int i) {
        return zzsy.zzj.zzc.zzcg(i) != null;
    }
}
