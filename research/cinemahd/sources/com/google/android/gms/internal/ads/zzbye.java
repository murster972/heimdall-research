package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public final class zzbye implements zzbxa {
    private final zzazb zzbll;
    private final zzczl zzffc;
    private final zzczu zzfgl;
    private final zzbpd zzfkd;
    private final zzboq zzfke;
    private boolean zzfki = false;
    private boolean zzfkl = false;
    private final zzall zzfok;
    private final zzalq zzfol;
    private final zzalr zzfom;
    private final Context zzup;

    public zzbye(zzall zzall, zzalq zzalq, zzalr zzalr, zzbpd zzbpd, zzboq zzboq, Context context, zzczl zzczl, zzazb zzazb, zzczu zzczu) {
        this.zzfok = zzall;
        this.zzfol = zzalq;
        this.zzfom = zzalr;
        this.zzfkd = zzbpd;
        this.zzfke = zzboq;
        this.zzup = context;
        this.zzffc = zzczl;
        this.zzbll = zzazb;
        this.zzfgl = zzczu;
    }

    private final void zzac(View view) {
        try {
            if (this.zzfom != null && !this.zzfom.getOverrideClickHandling()) {
                this.zzfom.zzu(ObjectWrapper.a(view));
                this.zzfke.onAdClicked();
            } else if (this.zzfok != null && !this.zzfok.getOverrideClickHandling()) {
                this.zzfok.zzu(ObjectWrapper.a(view));
                this.zzfke.onAdClicked();
            } else if (this.zzfol != null && !this.zzfol.getOverrideClickHandling()) {
                this.zzfol.zzu(ObjectWrapper.a(view));
                this.zzfke.onAdClicked();
            }
        } catch (RemoteException e) {
            zzayu.zzd("Failed to call handleClick", e);
        }
    }

    private static HashMap<String, View> zzb(Map<String, WeakReference<View>> map) {
        HashMap<String, View> hashMap = new HashMap<>();
        if (map == null) {
            return hashMap;
        }
        synchronized (map) {
            for (Map.Entry next : map.entrySet()) {
                View view = (View) ((WeakReference) next.getValue()).get();
                if (view != null) {
                    hashMap.put((String) next.getKey(), view);
                }
            }
        }
        return hashMap;
    }

    public final void cancelUnconfirmedClick() {
    }

    public final void destroy() {
    }

    public final boolean isCustomClickGestureEnabled() {
        return this.zzffc.zzdcf;
    }

    public final void setClickConfirmingView(View view) {
    }

    public final void zza(View view, MotionEvent motionEvent, View view2) {
    }

    public final void zza(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, View.OnTouchListener onTouchListener, View.OnClickListener onClickListener) {
        try {
            IObjectWrapper a2 = ObjectWrapper.a(view);
            HashMap<String, View> zzb = zzb(map);
            HashMap<String, View> zzb2 = zzb(map2);
            if (this.zzfom != null) {
                this.zzfom.zzc(a2, ObjectWrapper.a(zzb), ObjectWrapper.a(zzb2));
            } else if (this.zzfok != null) {
                this.zzfok.zzc(a2, ObjectWrapper.a(zzb), ObjectWrapper.a(zzb2));
                this.zzfok.zzv(a2);
            } else if (this.zzfol != null) {
                this.zzfol.zzc(a2, ObjectWrapper.a(zzb), ObjectWrapper.a(zzb2));
                this.zzfol.zzv(a2);
            }
        } catch (RemoteException e) {
            zzayu.zzd("Failed to call trackView", e);
        }
    }

    public final void zza(zzaeb zzaeb) {
    }

    public final void zzaio() {
    }

    public final void zzaip() {
        zzayu.zzez("Mute This Ad is not supported for 3rd party ads");
    }

    public final void zzaiq() {
    }

    public final void zzf(Bundle bundle) {
    }

    public final void zzfu(String str) {
    }

    public final void zzg(Bundle bundle) {
    }

    public final boolean zzh(Bundle bundle) {
        return false;
    }

    public final void zzrp() {
        this.zzfkl = true;
    }

    public final void zza(View view, Map<String, WeakReference<View>> map) {
        try {
            IObjectWrapper a2 = ObjectWrapper.a(view);
            if (this.zzfom != null) {
                this.zzfom.zzw(a2);
            } else if (this.zzfok != null) {
                this.zzfok.zzw(a2);
            } else if (this.zzfol != null) {
                this.zzfol.zzw(a2);
            }
        } catch (RemoteException e) {
            zzayu.zzd("Failed to call untrackView", e);
        }
    }

    public final void zza(View view, View view2, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, boolean z) {
        if (!this.zzfkl || !this.zzffc.zzdcf) {
            zzac(view);
        }
    }

    public final void zza(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, boolean z) {
        if (!this.zzfkl) {
            zzayu.zzez("Custom click reporting for 3p ads failed. enableCustomClickGesture is not set.");
        } else if (!this.zzffc.zzdcf) {
            zzayu.zzez("Custom click reporting for 3p ads failed. Ad unit id not whitelisted.");
        } else {
            zzac(view);
        }
    }

    public final void zza(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2) {
        try {
            if (!this.zzfki && this.zzffc.zzglt != null) {
                this.zzfki |= zzq.zzla().zzb(this.zzup, this.zzbll.zzbma, this.zzffc.zzglt.toString(), this.zzfgl.zzgmm);
            }
            if (this.zzfom != null && !this.zzfom.getOverrideImpressionRecording()) {
                this.zzfom.recordImpression();
                this.zzfkd.onAdImpression();
            } else if (this.zzfok != null && !this.zzfok.getOverrideImpressionRecording()) {
                this.zzfok.recordImpression();
                this.zzfkd.onAdImpression();
            } else if (this.zzfol != null && !this.zzfol.getOverrideImpressionRecording()) {
                this.zzfol.recordImpression();
                this.zzfkd.onAdImpression();
            }
        } catch (RemoteException e) {
            zzayu.zzd("Failed to call recordImpression", e);
        }
    }

    public final void zza(zzwr zzwr) {
        zzayu.zzez("Mute This Ad is not supported for 3rd party ads");
    }

    public final void zza(zzwn zzwn) {
        zzayu.zzez("Mute This Ad is not supported for 3rd party ads");
    }
}
