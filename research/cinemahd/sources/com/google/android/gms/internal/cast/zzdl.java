package com.google.android.gms.internal.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.zzad;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzdl extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzdl> CREATOR = new zzdm();
    private double zzet;
    private boolean zzeu;
    private zzad zzyh;
    private int zzyi;
    private int zzyj;
    private ApplicationMetadata zzyt;

    zzdl(double d, boolean z, int i, ApplicationMetadata applicationMetadata, int i2, zzad zzad) {
        this.zzet = d;
        this.zzeu = z;
        this.zzyi = i;
        this.zzyt = applicationMetadata;
        this.zzyj = i2;
        this.zzyh = zzad;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzdl)) {
            return false;
        }
        zzdl zzdl = (zzdl) obj;
        if (this.zzet == zzdl.zzet && this.zzeu == zzdl.zzeu && this.zzyi == zzdl.zzyi && zzdk.zza(this.zzyt, zzdl.zzyt) && this.zzyj == zzdl.zzyj) {
            zzad zzad = this.zzyh;
            if (zzdk.zza(zzad, zzad)) {
                return true;
            }
        }
        return false;
    }

    public final int getActiveInputState() {
        return this.zzyi;
    }

    public final ApplicationMetadata getApplicationMetadata() {
        return this.zzyt;
    }

    public final int getStandbyState() {
        return this.zzyj;
    }

    public final double getVolume() {
        return this.zzet;
    }

    public final int hashCode() {
        return Objects.a(Double.valueOf(this.zzet), Boolean.valueOf(this.zzeu), Integer.valueOf(this.zzyi), this.zzyt, Integer.valueOf(this.zzyj), this.zzyh);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.zzet);
        SafeParcelWriter.a(parcel, 3, this.zzeu);
        SafeParcelWriter.a(parcel, 4, this.zzyi);
        SafeParcelWriter.a(parcel, 5, (Parcelable) this.zzyt, i, false);
        SafeParcelWriter.a(parcel, 6, this.zzyj);
        SafeParcelWriter.a(parcel, 7, (Parcelable) this.zzyh, i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public final boolean zzey() {
        return this.zzeu;
    }

    public final zzad zzez() {
        return this.zzyh;
    }

    public zzdl() {
        this(Double.NaN, false, -1, (ApplicationMetadata) null, -1, (zzad) null);
    }
}
