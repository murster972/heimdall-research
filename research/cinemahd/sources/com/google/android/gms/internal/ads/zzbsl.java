package com.google.android.gms.internal.ads;

public final class zzbsl implements zzbov {
    private boolean zzfio = false;

    public final void onAdClosed() {
        this.zzfio = true;
    }

    public final void onAdLeftApplication() {
    }

    public final void onAdOpened() {
    }

    public final void onRewardedVideoCompleted() {
    }

    public final void onRewardedVideoStarted() {
    }

    public final void zzb(zzare zzare, String str, String str2) {
    }
}
