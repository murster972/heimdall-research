package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;

public final class zzccb implements zzdxg<zzcbn> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzbqp> zzfeq;
    private final zzdxp<zzdq> zzfky;
    private final zzdxp<zzsm> zzfpq;
    private final zzdxp<zzbdr> zzfqw;
    private final zzdxp<zza> zzfrl;
    private final zzdxp<zzbts> zzfrm;

    private zzccb(zzdxp<zzbdr> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzczu> zzdxp3, zzdxp<zzdq> zzdxp4, zzdxp<zzazb> zzdxp5, zzdxp<zza> zzdxp6, zzdxp<zzsm> zzdxp7, zzdxp<zzbqp> zzdxp8, zzdxp<zzbts> zzdxp9) {
        this.zzfqw = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfep = zzdxp3;
        this.zzfky = zzdxp4;
        this.zzfav = zzdxp5;
        this.zzfrl = zzdxp6;
        this.zzfpq = zzdxp7;
        this.zzfeq = zzdxp8;
        this.zzfrm = zzdxp9;
    }

    public static zzccb zzb(zzdxp<zzbdr> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzczu> zzdxp3, zzdxp<zzdq> zzdxp4, zzdxp<zzazb> zzdxp5, zzdxp<zza> zzdxp6, zzdxp<zzsm> zzdxp7, zzdxp<zzbqp> zzdxp8, zzdxp<zzbts> zzdxp9) {
        return new zzccb(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6, zzdxp7, zzdxp8, zzdxp9);
    }

    public final /* synthetic */ Object get() {
        return new zzcbn(this.zzfqw.get(), this.zzejv.get(), this.zzfep.get(), this.zzfky.get(), this.zzfav.get(), this.zzfrl.get(), this.zzfpq.get(), this.zzfeq.get(), this.zzfrm.get());
    }
}
