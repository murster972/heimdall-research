package com.google.android.gms.internal.ads;

public final class zzabb {
    public static zzaan<Boolean> zzctv = zzaan.zzf("gads:ad_key_enabled", false);
    public static zzaan<Boolean> zzctw = zzaan.zzf("gads:adshield:enable_adshield_instrumentation", false);
}
