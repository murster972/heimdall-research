package com.google.android.gms.internal.ads;

import android.os.RemoteException;

public final class zzsv {
    private final byte[] zzbua;
    private int zzbub;
    private int zzbuc;
    private final /* synthetic */ zzsr zzbud;

    private zzsv(zzsr zzsr, byte[] bArr) {
        this.zzbud = zzsr;
        this.zzbua = bArr;
    }

    public final zzsv zzbq(int i) {
        this.zzbub = i;
        return this;
    }

    public final zzsv zzbr(int i) {
        this.zzbuc = i;
        return this;
    }

    public final synchronized void zzdn() {
        try {
            if (this.zzbud.zzbty) {
                this.zzbud.zzbtx.zzc(this.zzbua);
                this.zzbud.zzbtx.zzm(this.zzbub);
                this.zzbud.zzbtx.zzn(this.zzbuc);
                this.zzbud.zzbtx.zza((int[]) null);
                this.zzbud.zzbtx.zzdn();
            }
        } catch (RemoteException e) {
            zzayu.zzb("Clearcut log failed", e);
        }
    }
}
