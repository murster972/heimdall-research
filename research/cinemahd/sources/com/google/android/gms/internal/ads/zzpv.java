package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class zzpv implements Application.ActivityLifecycleCallbacks {
    private final Application zzxa;
    private final WeakReference<Application.ActivityLifecycleCallbacks> zzxb;
    private boolean zzxc = false;

    public zzpv(Application application, Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        this.zzxb = new WeakReference<>(activityLifecycleCallbacks);
        this.zzxa = application;
    }

    private final void zza(zzqd zzqd) {
        try {
            Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = (Application.ActivityLifecycleCallbacks) this.zzxb.get();
            if (activityLifecycleCallbacks != null) {
                zzqd.zza(activityLifecycleCallbacks);
            } else if (!this.zzxc) {
                this.zzxa.unregisterActivityLifecycleCallbacks(this);
                this.zzxc = true;
            }
        } catch (Exception e) {
            zzayu.zzc("Error while dispatching lifecycle callback.", e);
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        zza(new zzpu(this, activity, bundle));
    }

    public final void onActivityDestroyed(Activity activity) {
        zza(new zzqa(this, activity));
    }

    public final void onActivityPaused(Activity activity) {
        zza(new zzpz(this, activity));
    }

    public final void onActivityResumed(Activity activity) {
        zza(new zzpw(this, activity));
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        zza(new zzqb(this, activity, bundle));
    }

    public final void onActivityStarted(Activity activity) {
        zza(new zzpx(this, activity));
    }

    public final void onActivityStopped(Activity activity) {
        zza(new zzpy(this, activity));
    }
}
