package com.google.android.gms.internal.ads;

public final class zzcov {
    private final zzbwz zzgdm;
    private final zzcop zzgdn = new zzcop();
    private final zzbow zzgdo;

    public zzcov(zzbwz zzbwz) {
        this.zzgdm = zzbwz;
        this.zzgdo = new zzcoy(this.zzgdn, this.zzgdm.zzajt());
    }

    public final zzbvi zzamr() {
        return new zzbvi(this.zzgdm, this.zzgdn.zzamo());
    }

    public final zzbov zzams() {
        return this.zzgdn;
    }

    public final zzbqb zzamt() {
        return this.zzgdn;
    }

    public final zzbow zzamu() {
        return this.zzgdo;
    }

    public final zzbpe zzamv() {
        return this.zzgdn;
    }

    public final zzty zzamw() {
        return this.zzgdn;
    }

    public final void zzc(zzvh zzvh) {
        this.zzgdn.zzc(zzvh);
    }
}
