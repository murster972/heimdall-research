package com.google.android.gms.internal.ads;

import java.util.Iterator;

public final class zzdex<E> extends zzdes<E> {
    public zzdex() {
        this(4);
    }

    public final /* synthetic */ zzdev zza(Iterator it2) {
        super.zza(it2);
        return this;
    }

    public final /* synthetic */ zzdes zzad(Object obj) {
        return (zzdex) zzae(obj);
    }

    public final /* synthetic */ zzdev zzae(Object obj) {
        super.zzae(obj);
        return this;
    }

    public final /* synthetic */ zzdev zze(Iterable iterable) {
        super.zze(iterable);
        return this;
    }

    private zzdex(int i) {
        super(4);
    }
}
