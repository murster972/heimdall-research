package com.google.android.gms.internal.ads;

import java.math.BigInteger;

public final class zzavn {
    private String zzdir = "0";
    private BigInteger zzdrn = BigInteger.ONE;

    public final synchronized String zzvp() {
        String bigInteger;
        bigInteger = this.zzdrn.toString();
        this.zzdrn = this.zzdrn.add(BigInteger.ONE);
        this.zzdir = bigInteger;
        return bigInteger;
    }

    public final synchronized String zzvq() {
        return this.zzdir;
    }
}
