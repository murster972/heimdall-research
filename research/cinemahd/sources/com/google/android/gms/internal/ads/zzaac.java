package com.google.android.gms.internal.ads;

public final class zzaac {
    private final long time;
    private final String zzcrw;
    private final zzaac zzcrx;

    public zzaac(long j, String str, zzaac zzaac) {
        this.time = j;
        this.zzcrw = str;
        this.zzcrx = zzaac;
    }

    public final long getTime() {
        return this.time;
    }

    public final String zzqq() {
        return this.zzcrw;
    }

    public final zzaac zzqr() {
        return this.zzcrx;
    }
}
