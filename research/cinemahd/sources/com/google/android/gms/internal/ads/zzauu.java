package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzauu extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzauu> CREATOR = new zzaux();
    public final String zzblx;
    public final String zzbqz;
    public final zzuj zzdpu;

    public zzauu(String str, String str2, zzuj zzuj) {
        this.zzbqz = str;
        this.zzblx = str2;
        this.zzdpu = zzuj;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzbqz, false);
        SafeParcelWriter.a(parcel, 2, this.zzblx, false);
        SafeParcelWriter.a(parcel, 3, (Parcelable) this.zzdpu, i, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
