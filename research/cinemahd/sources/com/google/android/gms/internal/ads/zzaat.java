package com.google.android.gms.internal.ads;

public final class zzaat {
    private static zzaan<Boolean> zzcsp = zzaas.zzf("gads:consent:gmscore:dsid:enabled", true);
    private static zzaan<Boolean> zzcsq = zzaas.zzf("gads:consent:gmscore:lat:enabled", true);
    private static zzaan<String> zzcsr = new zzaas("gads:consent:gmscore:backend_url", "https://adservice.google.com/getconfig/pubvendors", zzaap.zzcsk);
    private static zzaan<Long> zzcss = new zzaas("gads:consent:gmscore:time_out", 10000L, zzaap.zzcsi);
    private static zzaan<Boolean> zzcst = zzaas.zzf("gads:consent:gmscore:enabled", true);

    public static final void zza(zzabq zzabq) {
    }
}
