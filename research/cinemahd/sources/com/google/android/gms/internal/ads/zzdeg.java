package com.google.android.gms.internal.ads;

public final class zzdeg extends zzdea {
    public static boolean equal(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }
}
