package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

final /* synthetic */ class zzclz implements zzbuv {
    private final zzcip zzfyq;
    private final zzciq zzfzr;

    zzclz(zzcip zzcip, zzciq zzciq) {
        this.zzfyq = zzcip;
        this.zzfzr = zzciq;
    }

    public final void zza(boolean z, Context context) {
        zzcip zzcip = this.zzfyq;
        zzciq zzciq = this.zzfzr;
        try {
            if (((zzani) zzcip.zzddn).zzaa(ObjectWrapper.a(context))) {
                zzciq.zzamd();
            } else {
                zzayu.zzez("Can't show rewarded video.");
            }
        } catch (RemoteException e) {
            zzayu.zzd("Can't show rewarded video.", e);
        }
    }
}
