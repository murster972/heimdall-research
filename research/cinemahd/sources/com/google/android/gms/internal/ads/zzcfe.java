package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public final class zzcfe {
    private final zzdhd zzfov;
    private final zzdhd zzfuj;
    private final zzcgi zzfuk;
    private final zzdxa<zzcgw> zzful;

    public zzcfe(zzdhd zzdhd, zzdhd zzdhd2, zzcgi zzcgi, zzdxa<zzcgw> zzdxa) {
        this.zzfuj = zzdhd;
        this.zzfov = zzdhd2;
        this.zzfuk = zzcgi;
        this.zzful = zzdxa;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzaqk zzaqk, zzcgr zzcgr) throws Exception {
        return this.zzful.get().zzh(zzaqk);
    }

    public final zzdhe<InputStream> zzc(zzaqk zzaqk) {
        zzdhe<V> zzdhe;
        String str = zzaqk.packageName;
        zzq.zzkq();
        if (zzawb.zzem(str)) {
            zzdhe = zzdgs.zzk(new zzcgr(0));
        } else {
            zzdhe = zzdgs.zzb(this.zzfuj.zzd(new zzcfd(this, zzaqk)), ExecutionException.class, zzcfg.zzbkw, this.zzfov);
        }
        return zzdgs.zzb(zzdhe, zzcgr.class, new zzcff(this, zzaqk), this.zzfov);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ InputStream zzd(zzaqk zzaqk) throws Exception {
        return this.zzfuk.zzf(zzaqk).get((long) ((Integer) zzve.zzoy().zzd(zzzn.zzcox)).intValue(), TimeUnit.SECONDS);
    }
}
