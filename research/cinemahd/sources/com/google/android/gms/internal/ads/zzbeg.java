package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbeg implements Runnable {
    private final String zzcyr;
    private final zzbee zzeho;

    zzbeg(zzbee zzbee, String str) {
        this.zzeho = zzbee;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzeho.zzfp(this.zzcyr);
    }
}
