package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;

public final class zzcjy extends zzalh implements zzbpu {
    private zzali zzdet;
    private zzbpx zzfzf;

    public final synchronized void onAdClicked() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAdClicked();
        }
    }

    public final synchronized void onAdClosed() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAdClosed();
        }
    }

    public final synchronized void onAdFailedToLoad(int i) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAdFailedToLoad(i);
        }
        if (this.zzfzf != null) {
            this.zzfzf.onAdFailedToLoad(i);
        }
    }

    public final synchronized void onAdImpression() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAdImpression();
        }
    }

    public final synchronized void onAdLeftApplication() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAdLeftApplication();
        }
    }

    public final synchronized void onAdLoaded() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAdLoaded();
        }
        if (this.zzfzf != null) {
            this.zzfzf.onAdLoaded();
        }
    }

    public final synchronized void onAdOpened() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAdOpened();
        }
    }

    public final synchronized void onAppEvent(String str, String str2) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onAppEvent(str, str2);
        }
    }

    public final synchronized void onVideoEnd() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onVideoEnd();
        }
    }

    public final synchronized void onVideoPause() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onVideoPause();
        }
    }

    public final synchronized void onVideoPlay() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.onVideoPlay();
        }
    }

    public final synchronized void zza(zzali zzali) {
        this.zzdet = zzali;
    }

    public final synchronized void zzb(zzasd zzasd) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zzb(zzasd);
        }
    }

    public final synchronized void zzco(int i) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zzco(i);
        }
    }

    public final synchronized void zzdj(String str) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zzdj(str);
        }
    }

    public final synchronized void zzss() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zzss();
        }
    }

    public final synchronized void zzst() throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zzst();
        }
    }

    public final synchronized void zza(zzbpx zzbpx) {
        this.zzfzf = zzbpx;
    }

    public final synchronized void zzb(Bundle bundle) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zzb(bundle);
        }
    }

    public final synchronized void zza(zzalj zzalj) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zza(zzalj);
        }
    }

    public final synchronized void zza(zzade zzade, String str) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zza(zzade, str);
        }
    }

    public final synchronized void zza(zzasf zzasf) throws RemoteException {
        if (this.zzdet != null) {
            this.zzdet.zza(zzasf);
        }
    }
}
