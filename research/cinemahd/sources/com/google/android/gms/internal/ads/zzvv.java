package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzvv extends IInterface {
    IBinder zza(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, zzalc zzalc, int i, int i2) throws RemoteException;
}
