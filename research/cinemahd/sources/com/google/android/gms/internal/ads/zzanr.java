package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationNativeAdCallback;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;

final class zzanr implements MediationAdLoadCallback<UnifiedNativeAdMapper, MediationNativeAdCallback> {
    private final /* synthetic */ zzali zzdeo;
    private final /* synthetic */ zzanc zzder;

    zzanr(zzann zzann, zzanc zzanc, zzali zzali) {
        this.zzder = zzanc;
        this.zzdeo = zzali;
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final MediationNativeAdCallback onSuccess(UnifiedNativeAdMapper unifiedNativeAdMapper) {
        if (unifiedNativeAdMapper == null) {
            zzayu.zzez("Adapter incorrectly returned a null ad. The onFailure() callback should be called if an adapter fails to load an ad.");
            try {
                this.zzder.zzdl("Adapter returned null.");
                return null;
            } catch (RemoteException e) {
                zzayu.zzc("", e);
                return null;
            }
        } else {
            try {
                this.zzder.zza(new zzamt(unifiedNativeAdMapper));
            } catch (RemoteException e2) {
                zzayu.zzc("", e2);
            }
            return new zzant(this.zzdeo);
        }
    }

    public final void onFailure(String str) {
        try {
            this.zzder.zzdl(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
