package com.google.android.gms.internal.ads;

import java.util.Set;
import java.util.concurrent.Executor;

public final class zzcej implements zzdxg<Set<zzbsu<zzty>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzceo> zzfsb;
    private final zzcee zzfth;

    private zzcej(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfth = zzcee;
        this.zzfsb = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcej zze(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcej(zzcee, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(zzcee.zzg(this.zzfsb.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
