package com.google.android.gms.internal.ads;

public interface zzdgt<V> {
    void onSuccess(V v);

    void zzb(Throwable th);
}
