package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzagq implements Parcelable.Creator<zzagn> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        boolean z = false;
        String str = null;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 == 2) {
                z = SafeParcelReader.s(parcel, a2);
            } else if (a3 == 3) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 != 4) {
                SafeParcelReader.A(parcel, a2);
            } else {
                str2 = SafeParcelReader.o(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzagn(str, z, i, str2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzagn[i];
    }
}
