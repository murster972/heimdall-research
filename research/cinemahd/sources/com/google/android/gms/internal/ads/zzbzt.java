package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbzt implements zzdxg<zzbzq> {
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzbjq> zzfoh;
    private final zzdxp<zzbst> zzfpz;

    public zzbzt(zzdxp<Executor> zzdxp, zzdxp<zzbjq> zzdxp2, zzdxp<zzbst> zzdxp3) {
        this.zzfei = zzdxp;
        this.zzfoh = zzdxp2;
        this.zzfpz = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzbzq(this.zzfei.get(), this.zzfoh.get(), this.zzfpz.get());
    }
}
