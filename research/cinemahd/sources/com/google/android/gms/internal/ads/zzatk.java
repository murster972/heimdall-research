package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public final class zzatk implements zzatq {
    public final zzdhe<Map<String, String>> zza(Context context, Set<String> set) {
        return zzdgs.zzaj(Collections.emptyMap());
    }
}
