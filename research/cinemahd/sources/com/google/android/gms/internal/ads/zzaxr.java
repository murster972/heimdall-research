package com.google.android.gms.internal.ads;

final class zzaxr<T> extends zzazl<T> implements zzab<T> {
    private zzaxr() {
    }

    public final void zzb(T t) {
        super.set(t);
    }

    /* synthetic */ zzaxr(zzaxn zzaxn) {
        this();
    }
}
