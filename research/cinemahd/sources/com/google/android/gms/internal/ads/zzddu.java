package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class zzddu extends Handler {
    private static volatile zzddx zzgtt;

    public zzddu() {
    }

    public final void dispatchMessage(Message message) {
        zzb(message);
    }

    /* access modifiers changed from: protected */
    public void zzb(Message message) {
        super.dispatchMessage(message);
    }

    public zzddu(Looper looper) {
        super(looper);
    }

    public zzddu(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
