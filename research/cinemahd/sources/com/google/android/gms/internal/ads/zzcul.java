package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.google.android.gms.ads.internal.zzq;

public final class zzcul implements zzcub<zzcui> {
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcul(zzdhd zzdhd, Context context) {
        this.zzfov = zzdhd;
        this.zzup = context;
    }

    public final zzdhe<zzcui> zzanc() {
        return this.zzfov.zzd(new zzcuk(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcui zzanr() throws Exception {
        int i;
        boolean z;
        int i2;
        int i3;
        TelephonyManager telephonyManager = (TelephonyManager) this.zzup.getSystemService("phone");
        String networkOperator = telephonyManager.getNetworkOperator();
        int networkType = telephonyManager.getNetworkType();
        int phoneType = telephonyManager.getPhoneType();
        zzq.zzkq();
        int i4 = -1;
        if (zzawb.zzq(this.zzup, "android.permission.ACCESS_NETWORK_STATE")) {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.zzup.getSystemService("connectivity");
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                int type = activeNetworkInfo.getType();
                int ordinal = activeNetworkInfo.getDetailedState().ordinal();
                i3 = type;
                i4 = ordinal;
            } else {
                i3 = -1;
            }
            if (Build.VERSION.SDK_INT >= 16) {
                i = i4;
                i2 = i3;
                z = connectivityManager.isActiveNetworkMetered();
            } else {
                i = i4;
                i2 = i3;
                z = false;
            }
        } else {
            i2 = -2;
            z = false;
            i = -1;
        }
        return new zzcui(networkOperator, i2, networkType, phoneType, z, i);
    }
}
