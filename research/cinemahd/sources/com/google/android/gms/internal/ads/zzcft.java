package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcft implements zzdxg<String> {
    private final zzdxp<Context> zzejv;

    private zzcft(zzdxp<Context> zzdxp) {
        this.zzejv = zzdxp;
    }

    public static zzcft zzab(zzdxp<Context> zzdxp) {
        return new zzcft(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza(this.zzejv.get().getPackageName(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
