package com.google.android.gms.internal.cast;

import android.os.Handler;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder;
import java.util.concurrent.atomic.AtomicReference;

final class zzdf extends zzds {
    private final Handler handler;
    private final AtomicReference<zzdd> zzyx;

    public zzdf(zzdd zzdd) {
        this.zzyx = new AtomicReference<>(zzdd);
        this.handler = new zzez(zzdd.getLooper());
    }

    public final boolean isDisposed() {
        return this.zzyx.get() == null;
    }

    public final void onApplicationDisconnected(int i) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            String unused = zzdd.zzyl = null;
            String unused2 = zzdd.zzym = null;
            zzdd.zzr(i);
            if (zzdd.zzak != null) {
                this.handler.post(new zzdg(this, zzdd, i));
            }
        }
    }

    public final void zza(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            ApplicationMetadata unused = zzdd.zzxz = applicationMetadata;
            String unused2 = zzdd.zzyl = applicationMetadata.s();
            String unused3 = zzdd.zzym = str2;
            String unused4 = zzdd.zzyd = str;
            synchronized (zzdd.zzyr) {
                if (zzdd.zzyp != null) {
                    zzdd.zzyp.setResult(new zzde(new Status(0), applicationMetadata, str, str2, z));
                    BaseImplementation$ResultHolder unused5 = zzdd.zzyp = null;
                }
            }
        }
    }

    public final void zzb(zzdl zzdl) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzbf.d("onDeviceStatusChanged", new Object[0]);
            this.handler.post(new zzdh(this, zzdd, zzdl));
        }
    }

    public final zzdd zzex() {
        zzdd andSet = this.zzyx.getAndSet((Object) null);
        if (andSet == null) {
            return null;
        }
        andSet.zzet();
        return andSet;
    }

    public final void zzi(int i) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzq(i);
        }
    }

    public final void zzs(int i) {
        zzdd zzex = zzex();
        if (zzex != null) {
            zzdd.zzbf.d("ICastDeviceControllerListener.onDisconnected: %d", Integer.valueOf(i));
            if (i != 0) {
                zzex.triggerConnectionSuspended(2);
            }
        }
    }

    public final void zzt(int i) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzr(i);
        }
    }

    public final void zzu(int i) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzr(i);
        }
    }

    public final void zzb(zzct zzct) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzbf.d("onApplicationStatusChanged", new Object[0]);
            this.handler.post(new zzdi(this, zzdd, zzct));
        }
    }

    public final void zzb(String str, String str2) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzbf.d("Receive (type=text, ns=%s) %s", str, str2);
            this.handler.post(new zzdj(this, zzdd, str, str2));
        }
    }

    public final void zza(String str, double d, boolean z) {
        zzdd.zzbf.d("Deprecated callback: \"onStatusreceived\"", new Object[0]);
    }

    public final void zza(String str, byte[] bArr) {
        if (this.zzyx.get() != null) {
            zzdd.zzbf.d("IGNORING: Receive (type=binary, ns=%s) <%d bytes>", str, Integer.valueOf(bArr.length));
        }
    }

    public final void zza(String str, long j, int i) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzb(j, i);
        }
    }

    public final void zza(String str, long j) {
        zzdd zzdd = this.zzyx.get();
        if (zzdd != null) {
            zzdd.zzb(j, 0);
        }
    }
}
