package com.google.android.gms.internal.measurement;

interface zzgl {
    int zza();

    boolean zzb();

    zzgn zzc();
}
