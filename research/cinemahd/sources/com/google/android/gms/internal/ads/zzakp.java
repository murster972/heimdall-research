package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.location.Location;

public interface zzakp {
    zzdhe<Location> zza(ApplicationInfo applicationInfo);
}
