package com.google.android.gms.internal.ads;

public final class zzblm implements zzdxg<zzcio<zzbkk>> {
    private final zzdxp<zzdhd> zzfei;
    private final zzdxp<zzdcr> zzfet;
    private final zzdxp<zzcjk> zzfeu;
    private final zzdxp<zzcmy> zzfev;

    public zzblm(zzdxp<zzdcr> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<zzcjk> zzdxp3, zzdxp<zzcmy> zzdxp4) {
        this.zzfet = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfeu = zzdxp3;
        this.zzfev = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return (zzcio) zzdxm.zza(new zzcna(this.zzfet.get(), this.zzfei.get(), this.zzfev.get(), this.zzfeu.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
