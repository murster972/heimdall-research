package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcoj implements zzdxg<zzcok> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzvh> zzgcn;
    private final zzdxp<zzbkk> zzgco;

    public zzcoj(zzdxp<Context> zzdxp, zzdxp<zzvh> zzdxp2, zzdxp<zzczu> zzdxp3, zzdxp<zzbkk> zzdxp4) {
        this.zzejv = zzdxp;
        this.zzgcn = zzdxp2;
        this.zzfep = zzdxp3;
        this.zzgco = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcok(this.zzejv.get(), this.zzgcn.get(), this.zzfep.get(), this.zzgco.get());
    }
}
