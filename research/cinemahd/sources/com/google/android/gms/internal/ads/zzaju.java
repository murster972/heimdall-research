package com.google.android.gms.internal.ads;

public interface zzaju<I, O> extends zzdgf<I, O> {
    zzdhe<O> zzi(I i);
}
