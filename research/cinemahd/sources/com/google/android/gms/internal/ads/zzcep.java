package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcep implements Runnable {
    private final zzceq zzftj;
    private final zzagu zzftk;

    zzcep(zzceq zzceq, zzagu zzagu) {
        this.zzftj = zzceq;
        this.zzftk = zzagu;
    }

    public final void run() {
        this.zzftj.zzc(this.zzftk);
    }
}
