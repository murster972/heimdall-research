package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.instream.InstreamAd;

public final class zzaho extends zzahk {
    private final InstreamAd.InstreamAdLoadCallback zzcyl;

    public zzaho(InstreamAd.InstreamAdLoadCallback instreamAdLoadCallback) {
        this.zzcyl = instreamAdLoadCallback;
    }

    public final void onInstreamAdFailedToLoad(int i) {
        this.zzcyl.onInstreamAdFailedToLoad(i);
    }

    public final void zza(zzahb zzahb) {
        this.zzcyl.onInstreamAdLoaded(new zzahm(zzahb));
    }
}
