package com.google.android.gms.internal.ads;

import com.facebook.react.uimanager.ViewProps;
import com.vungle.warren.model.AdvertisementDBAdapter;
import org.json.JSONException;
import org.json.JSONObject;

public class zzaoo {
    private final zzbdi zzcza;
    private final String zzdgj;

    public zzaoo(zzbdi zzbdi) {
        this(zzbdi, "");
    }

    public final void zza(int i, int i2, int i3, int i4, float f, int i5) {
        try {
            this.zzcza.zzb("onScreenInfoChanged", new JSONObject().put("width", i).put("height", i2).put("maxSizeWidth", i3).put("maxSizeHeight", i4).put("density", (double) f).put(ViewProps.ROTATION, i5));
        } catch (JSONException e) {
            zzayu.zzc("Error occurred while obtaining screen information.", e);
        }
    }

    public final void zzb(int i, int i2, int i3, int i4) {
        try {
            this.zzcza.zzb("onSizeChanged", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            zzayu.zzc("Error occurred while dispatching size change.", e);
        }
    }

    public final void zzc(int i, int i2, int i3, int i4) {
        try {
            this.zzcza.zzb("onDefaultPositionReceived", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            zzayu.zzc("Error occurred while dispatching default position.", e);
        }
    }

    public final void zzds(String str) {
        try {
            JSONObject put = new JSONObject().put("message", str).put("action", this.zzdgj);
            if (this.zzcza != null) {
                this.zzcza.zzb("onError", put);
            }
        } catch (JSONException e) {
            zzayu.zzc("Error occurred while dispatching error event.", e);
        }
    }

    public final void zzdt(String str) {
        try {
            this.zzcza.zzb("onReadyEventReceived", new JSONObject().put("js", str));
        } catch (JSONException e) {
            zzayu.zzc("Error occurred while dispatching ready Event.", e);
        }
    }

    public final void zzdu(String str) {
        try {
            this.zzcza.zzb("onStateChanged", new JSONObject().put(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE, str));
        } catch (JSONException e) {
            zzayu.zzc("Error occurred while dispatching state change.", e);
        }
    }

    public zzaoo(zzbdi zzbdi, String str) {
        this.zzcza = zzbdi;
        this.zzdgj = str;
    }
}
