package com.google.android.gms.internal.ads;

import java.util.Map;

final class zzdfg<K, V> extends zzdfb<Map.Entry<K, V>> {
    /* access modifiers changed from: private */
    public final transient int size;
    private final transient zzdey<K, V> zzguv;
    /* access modifiers changed from: private */
    public final transient Object[] zzguw;
    private final transient int zzgux = 0;

    zzdfg(zzdey<K, V> zzdey, Object[] objArr, int i, int i2) {
        this.zzguv = zzdey;
        this.zzguw = objArr;
        this.size = i2;
    }

    public final boolean contains(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object value = entry.getValue();
            if (value == null || !value.equals(this.zzguv.get(key))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public final int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public final int zza(Object[] objArr, int i) {
        return zzarb().zza(objArr, i);
    }

    /* renamed from: zzaqx */
    public final zzdfp<Map.Entry<K, V>> iterator() {
        return (zzdfp) zzarb().iterator();
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public final zzdeu<Map.Entry<K, V>> zzarj() {
        return new zzdfj(this);
    }
}
