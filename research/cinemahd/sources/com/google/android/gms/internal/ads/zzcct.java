package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;

public final class zzcct implements zzdxg<Set<zzbsu<zzdcx>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzcdf> zzfdd;

    private zzcct(zzdxp<Executor> zzdxp, zzdxp<zzcdf> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzfdd = zzdxp2;
    }

    public static zzcct zzs(zzdxp<Executor> zzdxp, zzdxp<zzcdf> zzdxp2) {
        return new zzcct(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        Set set;
        Executor executor = this.zzfcv.get();
        zzcdf zzcdf = this.zzfdd.get();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnw)).booleanValue()) {
            set = Collections.singleton(new zzbsu(zzcdf, executor));
        } else {
            set = Collections.emptySet();
        }
        return (Set) zzdxm.zza(set, "Cannot return null from a non-@Nullable @Provides method");
    }
}
