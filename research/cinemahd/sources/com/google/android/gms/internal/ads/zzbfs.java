package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzbfs extends IInterface {
    void zza(IObjectWrapper iObjectWrapper, zzbfq zzbfq) throws RemoteException;
}
