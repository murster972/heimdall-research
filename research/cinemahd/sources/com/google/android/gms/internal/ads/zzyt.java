package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;

public final class zzyt extends zzxf {
    private final VideoController.VideoLifecycleCallbacks zzabu;

    public zzyt(VideoController.VideoLifecycleCallbacks videoLifecycleCallbacks) {
        this.zzabu = videoLifecycleCallbacks;
    }

    public final void onVideoEnd() {
        this.zzabu.onVideoEnd();
    }

    public final void onVideoMute(boolean z) {
        this.zzabu.onVideoMute(z);
    }

    public final void onVideoPause() {
        this.zzabu.onVideoPause();
    }

    public final void onVideoPlay() {
        this.zzabu.onVideoPlay();
    }

    public final void onVideoStart() {
        this.zzabu.onVideoStart();
    }
}
