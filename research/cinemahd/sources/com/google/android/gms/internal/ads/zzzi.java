package com.google.android.gms.internal.ads;

import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONObject;

public final class zzzi {
    private final Collection<zzzc<?>> zzcfx = new ArrayList();
    private final Collection<zzzc<String>> zzcfy = new ArrayList();
    private final Collection<zzzc<String>> zzcfz = new ArrayList();

    public final void zza(zzzc zzzc) {
        this.zzcfx.add(zzzc);
    }

    public final void zzb(zzzc<String> zzzc) {
        this.zzcfy.add(zzzc);
    }

    public final void zzc(zzzc<String> zzzc) {
        this.zzcfz.add(zzzc);
    }

    public final List<String> zzqg() {
        ArrayList arrayList = new ArrayList();
        for (zzzc<String> zzd : this.zzcfy) {
            String str = (String) zzve.zzoy().zzd(zzd);
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(str);
            }
        }
        arrayList.addAll(zzzs.zzqm());
        return arrayList;
    }

    public final List<String> zzqh() {
        List<String> zzqg = zzqg();
        for (zzzc<String> zzd : this.zzcfz) {
            String str = (String) zzve.zzoy().zzd(zzd);
            if (!TextUtils.isEmpty(str)) {
                zzqg.add(str);
            }
        }
        zzqg.addAll(zzzs.zzqn());
        return zzqg;
    }

    public final void zza(SharedPreferences.Editor editor, int i, JSONObject jSONObject) {
        for (zzzc next : this.zzcfx) {
            if (next.getSource() == 1) {
                next.zza(editor, next.zza(jSONObject));
            }
        }
        if (jSONObject != null) {
            editor.putString("flag_configuration", jSONObject.toString());
        } else {
            zzayu.zzex("Flag Json is null.");
        }
    }
}
