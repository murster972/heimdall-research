package com.google.android.gms.internal.ads;

import java.util.HashMap;
import java.util.Map;

final class zzbcr implements Runnable {
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ String zzedb;
    private final /* synthetic */ int zzedd;
    private final /* synthetic */ zzbcn zzedf;

    zzbcr(zzbcn zzbcn, String str, String str2, int i) {
        this.zzedf = zzbcn;
        this.zzdug = str;
        this.zzedb = str2;
        this.zzedd = i;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "precacheComplete");
        hashMap.put("src", this.zzdug);
        hashMap.put("cachedSrc", this.zzedb);
        hashMap.put("totalBytes", Integer.toString(this.zzedd));
        this.zzedf.zza("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
