package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzclv implements zzcir<zzcbb, zzdac, zzcjy> {
    private final Executor zzfci;
    private final zzcbi zzgal;
    private final Context zzup;

    public zzclv(Context context, Executor executor, zzcbi zzcbi) {
        this.zzup = context;
        this.zzfci = executor;
        this.zzgal = zzcbi;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzdac, zzcjy> zzcip) throws zzdab {
        try {
            ((zzdac) zzcip.zzddn).zzb(this.zzup, zzczt.zzgmh.zzfgl.zzgml, zzczl.zzglr.toString(), (zzali) zzcip.zzfyf);
        } catch (Exception e) {
            String valueOf = String.valueOf(zzcip.zzfge);
            zzayu.zzd(valueOf.length() != 0 ? "Fail to load ad from adapter ".concat(valueOf) : new String("Fail to load ad from adapter "), e);
        }
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzcbd zza = this.zzgal.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzcbg(new zzcly(zzcip)));
        zza.zzadh().zza(new zzbiu((zzdac) zzcip.zzddn), this.zzfci);
        ((zzcjy) zzcip.zzfyf).zza((zzali) zza.zzaew());
        return zza.zzaeu();
    }
}
