package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzun implements Parcelable.Creator<zzuo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a2) != 2) {
                SafeParcelReader.A(parcel, a2);
            } else {
                i = SafeParcelReader.w(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzuo(i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzuo[i];
    }
}
