package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzya extends RemoteCreator<zzwl> {
    public zzya() {
        super("com.google.android.gms.ads.MobileAdsSettingManagerCreatorImpl");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object getRemoteCreator(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManagerCreator");
        if (queryLocalInterface instanceof zzwl) {
            return (zzwl) queryLocalInterface;
        }
        return new zzwo(iBinder);
    }

    public final zzwk zzi(Context context) {
        try {
            IBinder zzb = ((zzwl) getRemoteCreatorInstance(context)).zzb(ObjectWrapper.a(context), 19649000);
            if (zzb == null) {
                return null;
            }
            IInterface queryLocalInterface = zzb.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            if (queryLocalInterface instanceof zzwk) {
                return (zzwk) queryLocalInterface;
            }
            return new zzwm(zzb);
        } catch (RemoteException | RemoteCreator.RemoteCreatorException e) {
            zzayu.zzd("Could not get remote MobileAdsSettingManager.", e);
            return null;
        }
    }
}
