package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdlv extends zzdrt<zzdlv, zza> implements zzdtg {
    private static volatile zzdtn<zzdlv> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlv zzhay;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;

    public static final class zza extends zzdrt.zzb<zzdlv, zza> implements zzdtg {
        private zza() {
            super(zzdlv.zzhay);
        }

        public final zza zzag(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlv) this.zzhmp).zzs(zzdqk);
            return this;
        }

        public final zza zzeg(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlv) this.zzhmp).setVersion(0);
            return this;
        }

        /* synthetic */ zza(zzdlu zzdlu) {
            this();
        }
    }

    static {
        zzdlv zzdlv = new zzdlv();
        zzhay = zzdlv;
        zzdrt.zza(zzdlv.class, zzdlv);
    }

    private zzdlv() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public static zzdlv zzae(zzdqk zzdqk) throws zzdse {
        return (zzdlv) zzdrt.zza(zzhay, zzdqk);
    }

    public static zza zzatw() {
        return (zza) zzhay.zzazt();
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlu.zzdk[i - 1]) {
            case 1:
                return new zzdlv();
            case 2:
                return new zza((zzdlu) null);
            case 3:
                return zzdrt.zza((zzdte) zzhay, "\u0000\u0002\u0000\u0000\u0001\u0003\u0002\u0000\u0000\u0000\u0001\u000b\u0003\n", new Object[]{"zzhaa", "zzhab"});
            case 4:
                return zzhay;
            case 5:
                zzdtn<zzdlv> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlv.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhay);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }
}
