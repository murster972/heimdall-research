package com.google.android.gms.internal.ads;

import java.util.regex.Matcher;

final class zzcgc implements zzdgt<zzczt> {
    private final /* synthetic */ zzcfx zzfvj;

    zzcgc(zzcfx zzcfx) {
        this.zzfvj = zzcfx;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzczt zzczt = (zzczt) obj;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue()) {
            this.zzfvj.zzfvf.zzdk(zzczt.zzgmi.zzgmf.responseCode);
            this.zzfvj.zzfvf.zzeq(zzczt.zzgmi.zzgmf.zzfwt);
        }
    }

    public final void zzb(Throwable th) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue()) {
            Matcher matcher = zzcfx.zzfvg.matcher(th.getMessage());
            if (matcher.matches()) {
                this.zzfvj.zzfvf.zzdk(Integer.parseInt(matcher.group(1)));
            }
        }
    }
}
