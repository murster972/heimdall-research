package com.google.android.gms.internal.ads;

final class zzdfi<K> extends zzdfb<K> {
    private final transient zzdeu<K> zzguj;
    private final transient zzdey<K, ?> zzguv;

    zzdfi(zzdey<K, ?> zzdey, zzdeu<K> zzdeu) {
        this.zzguv = zzdey;
        this.zzguj = zzdeu;
    }

    public final boolean contains(Object obj) {
        return this.zzguv.get(obj) != null;
    }

    public final int size() {
        return this.zzguv.size();
    }

    /* access modifiers changed from: package-private */
    public final int zza(Object[] objArr, int i) {
        return zzarb().zza(objArr, i);
    }

    /* renamed from: zzaqx */
    public final zzdfp<K> iterator() {
        return (zzdfp) zzarb().iterator();
    }

    public final zzdeu<K> zzarb() {
        return this.zzguj;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return true;
    }
}
