package com.google.android.gms.internal.ads;

final /* synthetic */ class zzajc implements Runnable {
    private final zzaif zzczv;

    private zzajc(zzaif zzaif) {
        this.zzczv = zzaif;
    }

    static Runnable zzb(zzaif zzaif) {
        return new zzajc(zzaif);
    }

    public final void run() {
        this.zzczv.destroy();
    }
}
