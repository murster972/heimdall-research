package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzaza implements Parcelable.Creator<zzazb> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        int i = 0;
        int i2 = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 == 3) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 4) {
                i2 = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 5) {
                z = SafeParcelReader.s(parcel, a2);
            } else if (a3 != 6) {
                SafeParcelReader.A(parcel, a2);
            } else {
                z2 = SafeParcelReader.s(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzazb(str, i, i2, z, z2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzazb[i];
    }
}
