package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzarj extends IInterface {
    void zza(zzare zzare, String str, String str2) throws RemoteException;
}
