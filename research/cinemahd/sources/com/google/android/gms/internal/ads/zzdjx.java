package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdjx extends zzdii<zzdnz> {
    zzdjx() {
        super(zzdnz.class, new zzdka(zzdhx.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.XChaCha20Poly1305Key";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    public final zzdih<zzdoc, zzdnz> zzasg() {
        return new zzdjz(this, zzdoc.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdnz zzdnz = (zzdnz) zzdte;
        zzdpo.zzx(zzdnz.getVersion(), 0);
        if (zzdnz.zzass().size() != 32) {
            throw new GeneralSecurityException("invalid XChaCha20Poly1305Key: incorrect key length");
        }
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdnz.zzbb(zzdqk);
    }
}
