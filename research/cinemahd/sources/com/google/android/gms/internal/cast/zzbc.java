package com.google.android.gms.internal.cast;

import android.widget.TextView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import java.util.ArrayList;
import java.util.List;

public final class zzbc extends UIController {
    private final TextView zzso;
    private final List<String> zzsp = new ArrayList();

    public zzbc(TextView textView, List<String> list) {
        this.zzso = textView;
        this.zzsp.addAll(list);
    }

    public final void onMediaStatusUpdated() {
        MediaInfo y;
        MediaMetadata y2;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null && remoteMediaClient.k() && (y = remoteMediaClient.f().y()) != null && (y2 = y.y()) != null) {
            for (String next : this.zzsp) {
                if (y2.e(next)) {
                    this.zzso.setText(y2.f(next));
                    return;
                }
            }
            this.zzso.setText("");
        }
    }
}
