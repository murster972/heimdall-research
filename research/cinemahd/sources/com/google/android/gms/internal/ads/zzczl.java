package com.google.android.gms.internal.ads;

import java.util.List;
import org.json.JSONObject;

public final class zzczl {
    public final String zzaez;
    public final boolean zzblf;
    public final List<String> zzdbq;
    public final List<String> zzdbr;
    public final String zzdbw;
    public final boolean zzdcd;
    public final boolean zzdce;
    public final boolean zzdcf;
    public final String zzdcm;
    public final String zzdcx;
    public final String zzdcy;
    public final String zzdem;
    public final List<String> zzdkm;
    public final String zzdkp;
    public final String zzdks;
    public final zzasd zzdky;
    public final List<String> zzdkz;
    public final List<String> zzdla;
    public final boolean zzdli;
    public final boolean zzdll;
    public final boolean zzdlm;
    public final boolean zzdmf;
    public final boolean zzega;
    public final String zzeif;
    public final int zzfdp;
    public final String zzfhk;
    public final int zzfjj;
    public final List<String> zzgli;
    public final int zzglj;
    public final List<String> zzglk;
    public final List<String> zzgll;
    public final List<String> zzglm;
    public final List<zzczk> zzgln;
    public final zzczp zzglo;
    public final List<String> zzglp;
    public final List<zzczk> zzglq;
    public final JSONObject zzglr;
    public final zzatn zzgls;
    public final JSONObject zzglt;
    public final JSONObject zzglu;
    public final boolean zzglv;
    public final int zzglw;
    public final int zzglx;
    public final JSONObject zzgly;
    public final int zzglz;
    public final boolean zzgma;

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzczl(android.util.JsonReader r61) throws java.lang.IllegalStateException, java.io.IOException, org.json.JSONException, java.lang.NumberFormatException {
        /*
            r60 = this;
            r0 = r60
            r60.<init>()
            java.util.List r1 = java.util.Collections.emptyList()
            java.util.List r2 = java.util.Collections.emptyList()
            java.util.List r3 = java.util.Collections.emptyList()
            java.util.List r4 = java.util.Collections.emptyList()
            java.util.List r5 = java.util.Collections.emptyList()
            java.util.Collections.emptyList()
            java.util.List r6 = java.util.Collections.emptyList()
            java.util.List r7 = java.util.Collections.emptyList()
            java.util.List r8 = java.util.Collections.emptyList()
            java.util.List r9 = java.util.Collections.emptyList()
            java.util.List r10 = java.util.Collections.emptyList()
            java.util.List r11 = java.util.Collections.emptyList()
            java.util.List r12 = java.util.Collections.emptyList()
            org.json.JSONObject r13 = new org.json.JSONObject
            r13.<init>()
            org.json.JSONObject r14 = new org.json.JSONObject
            r14.<init>()
            org.json.JSONObject r15 = new org.json.JSONObject
            r15.<init>()
            org.json.JSONObject r16 = new org.json.JSONObject
            r16.<init>()
            r61.beginObject()
            r17 = 0
            r18 = -1
            java.lang.String r19 = ""
            r20 = 0
            r22 = r11
            r23 = r12
            r25 = r13
            r31 = r14
            r32 = r15
            r43 = r16
            r12 = r17
            r21 = r12
            r29 = r21
            r11 = r19
            r24 = r11
            r26 = r24
            r27 = r26
            r28 = r27
            r30 = r28
            r42 = r30
            r48 = r42
            r50 = r48
            r33 = 0
            r34 = 0
            r35 = 0
            r36 = 0
            r37 = 0
            r38 = 0
            r39 = -1
            r40 = 0
            r41 = 0
            r44 = 0
            r45 = 0
            r46 = 0
            r47 = 0
            r49 = -1
            r51 = 0
            r13 = r8
            r14 = r9
            r15 = r10
            r10 = r50
            r8 = r6
            r9 = r7
            r6 = 0
            r7 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = 0
        L_0x00a6:
            boolean r16 = r61.hasNext()
            if (r16 == 0) goto L_0x0563
            java.lang.String r16 = r61.nextName()
            if (r16 != 0) goto L_0x00b5
            r17 = r19
            goto L_0x00b7
        L_0x00b5:
            r17 = r16
        L_0x00b7:
            int r16 = r17.hashCode()
            r52 = 6
            r53 = 5
            r54 = 7
            r55 = 4
            r56 = 3
            r57 = 2
            r58 = r15
            switch(r16) {
                case -1980587809: goto L_0x03a2;
                case -1965512151: goto L_0x0393;
                case -1620470467: goto L_0x0384;
                case -1440104884: goto L_0x0375;
                case -1439500848: goto L_0x0366;
                case -1428969291: goto L_0x0357;
                case -1403779768: goto L_0x0348;
                case -1360811658: goto L_0x0338;
                case -1306015996: goto L_0x0328;
                case -1289032093: goto L_0x0318;
                case -1234181075: goto L_0x0308;
                case -1181000426: goto L_0x02f8;
                case -1152230954: goto L_0x02e9;
                case -1146534047: goto L_0x02d9;
                case -1115838944: goto L_0x02c9;
                case -1081936678: goto L_0x02b9;
                case -1078050970: goto L_0x02a9;
                case -1051269058: goto L_0x0299;
                case -982608540: goto L_0x0289;
                case -776859333: goto L_0x027a;
                case -544216775: goto L_0x026a;
                case -437057161: goto L_0x025b;
                case -404326515: goto L_0x024b;
                case -397704715: goto L_0x023b;
                case -213424028: goto L_0x022b;
                case -29338502: goto L_0x021b;
                case 3107: goto L_0x020b;
                case 3355: goto L_0x01fb;
                case 3076010: goto L_0x01eb;
                case 63195984: goto L_0x01db;
                case 107433883: goto L_0x01cb;
                case 230323073: goto L_0x01bc;
                case 418392395: goto L_0x01ac;
                case 597473788: goto L_0x019c;
                case 673261304: goto L_0x018d;
                case 754887508: goto L_0x017d;
                case 791122864: goto L_0x016e;
                case 1010584092: goto L_0x015e;
                case 1100650276: goto L_0x014e;
                case 1321720943: goto L_0x013e;
                case 1637553475: goto L_0x012e;
                case 1638957285: goto L_0x011f;
                case 1688341040: goto L_0x010f;
                case 1799285870: goto L_0x00ff;
                case 1839650832: goto L_0x00f0;
                case 1875425491: goto L_0x00e0;
                case 2072888499: goto L_0x00d0;
                default: goto L_0x00cc;
            }
        L_0x00cc:
            r59 = r14
            goto L_0x03b1
        L_0x00d0:
            java.lang.String r15 = "manual_tracking_urls"
            r59 = r14
            r14 = r17
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 14
            goto L_0x03b2
        L_0x00e0:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "is_analytics_logging_enabled"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 39
            goto L_0x03b2
        L_0x00f0:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "renderers"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 0
            goto L_0x03b2
        L_0x00ff:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "use_third_party_container_height"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 46
            goto L_0x03b2
        L_0x010f:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "video_reward_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 8
            goto L_0x03b2
        L_0x011f:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "video_start_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 6
            goto L_0x03b2
        L_0x012e:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "bid_response"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 37
            goto L_0x03b2
        L_0x013e:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "allow_pub_owned_ad_view"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 29
            goto L_0x03b2
        L_0x014e:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "rewards"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 12
            goto L_0x03b2
        L_0x015e:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "transaction_id"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 10
            goto L_0x03b2
        L_0x016e:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "impression_type"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 5
            goto L_0x03b2
        L_0x017d:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "container_sizes"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 15
            goto L_0x03b2
        L_0x018d:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "reward_granted_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 7
            goto L_0x03b2
        L_0x019c:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "debug_dialog_string"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 25
            goto L_0x03b2
        L_0x01ac:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "is_closable_area_disabled"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 33
            goto L_0x03b2
        L_0x01bc:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "ad_load_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 4
            goto L_0x03b2
        L_0x01cb:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "qdata"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 22
            goto L_0x03b2
        L_0x01db:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "render_test_label"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 31
            goto L_0x03b2
        L_0x01eb:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "data"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 20
            goto L_0x03b2
        L_0x01fb:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "id"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 21
            goto L_0x03b2
        L_0x020b:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "ad"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 16
            goto L_0x03b2
        L_0x021b:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "allow_custom_click_gesture"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 30
            goto L_0x03b2
        L_0x022b:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "watermark"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 44
            goto L_0x03b2
        L_0x023b:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "ad_close_time_ms"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 43
            goto L_0x03b2
        L_0x024b:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "render_timeout_ms"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 35
            goto L_0x03b2
        L_0x025b:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "imp_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 3
            goto L_0x03b2
        L_0x026a:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "safe_browsing"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 24
            goto L_0x03b2
        L_0x027a:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "click_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 2
            goto L_0x03b2
        L_0x0289:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "valid_from_timestamp"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 11
            goto L_0x03b2
        L_0x0299:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "active_view"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 23
            goto L_0x03b2
        L_0x02a9:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "video_complete_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 9
            goto L_0x03b2
        L_0x02b9:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "allocation_id"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 19
            goto L_0x03b2
        L_0x02c9:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "fill_urls"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 13
            goto L_0x03b2
        L_0x02d9:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "is_scroll_aware"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 40
            goto L_0x03b2
        L_0x02e9:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "ad_type"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 1
            goto L_0x03b2
        L_0x02f8:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "is_augmented_reality_ad"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 42
            goto L_0x03b2
        L_0x0308:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "allow_pub_rendered_attribution"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 28
            goto L_0x03b2
        L_0x0318:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "extras"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 27
            goto L_0x03b2
        L_0x0328:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "adapters"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 18
            goto L_0x03b2
        L_0x0338:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "ad_sizes"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 17
            goto L_0x03b2
        L_0x0348:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "showable_impression_type"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 41
            goto L_0x03b2
        L_0x0357:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "enable_omid"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 36
            goto L_0x03b2
        L_0x0366:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "orientation"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 34
            goto L_0x03b2
        L_0x0375:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "is_custom_close_blocked"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 32
            goto L_0x03b2
        L_0x0384:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "backend_query_id"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 45
            goto L_0x03b2
        L_0x0393:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "omid_settings"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 38
            goto L_0x03b2
        L_0x03a2:
            r59 = r14
            r14 = r17
            java.lang.String r15 = "debug_signals"
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x03b1
            r14 = 26
            goto L_0x03b2
        L_0x03b1:
            r14 = -1
        L_0x03b2:
            switch(r14) {
                case 0: goto L_0x0557;
                case 1: goto L_0x051c;
                case 2: goto L_0x0515;
                case 3: goto L_0x050e;
                case 4: goto L_0x0507;
                case 5: goto L_0x04f9;
                case 6: goto L_0x04f1;
                case 7: goto L_0x04ea;
                case 8: goto L_0x04e2;
                case 9: goto L_0x04da;
                case 10: goto L_0x04d2;
                case 11: goto L_0x04ca;
                case 12: goto L_0x04be;
                case 13: goto L_0x04b6;
                case 14: goto L_0x04ac;
                case 15: goto L_0x04a3;
                case 16: goto L_0x0498;
                case 17: goto L_0x0492;
                case 18: goto L_0x048c;
                case 19: goto L_0x0486;
                case 20: goto L_0x0480;
                case 21: goto L_0x047a;
                case 22: goto L_0x0474;
                case 23: goto L_0x046a;
                case 24: goto L_0x0460;
                case 25: goto L_0x045a;
                case 26: goto L_0x0454;
                case 27: goto L_0x044e;
                case 28: goto L_0x0448;
                case 29: goto L_0x0442;
                case 30: goto L_0x043c;
                case 31: goto L_0x0436;
                case 32: goto L_0x0430;
                case 33: goto L_0x042a;
                case 34: goto L_0x0404;
                case 35: goto L_0x03fe;
                case 36: goto L_0x03f8;
                case 37: goto L_0x03f2;
                case 38: goto L_0x03ec;
                case 39: goto L_0x03e6;
                case 40: goto L_0x03e0;
                case 41: goto L_0x03da;
                case 42: goto L_0x03d4;
                case 43: goto L_0x03ce;
                case 44: goto L_0x03c8;
                case 45: goto L_0x03c2;
                case 46: goto L_0x03bc;
                default: goto L_0x03b5;
            }
        L_0x03b5:
            r15 = r61
            r61.skipValue()
            goto L_0x055d
        L_0x03bc:
            boolean r51 = r61.nextBoolean()
            goto L_0x055d
        L_0x03c2:
            java.lang.String r50 = r61.nextString()
            goto L_0x055d
        L_0x03c8:
            java.lang.String r48 = r61.nextString()
            goto L_0x055d
        L_0x03ce:
            int r49 = r61.nextInt()
            goto L_0x055d
        L_0x03d4:
            boolean r47 = r61.nextBoolean()
            goto L_0x055d
        L_0x03da:
            int r46 = r61.nextInt()
            goto L_0x055d
        L_0x03e0:
            boolean r45 = r61.nextBoolean()
            goto L_0x055d
        L_0x03e6:
            boolean r44 = r61.nextBoolean()
            goto L_0x055d
        L_0x03ec:
            org.json.JSONObject r43 = com.google.android.gms.internal.ads.zzaxs.zzc(r61)
            goto L_0x055d
        L_0x03f2:
            java.lang.String r42 = r61.nextString()
            goto L_0x055d
        L_0x03f8:
            boolean r41 = r61.nextBoolean()
            goto L_0x055d
        L_0x03fe:
            int r40 = r61.nextInt()
            goto L_0x055d
        L_0x0404:
            java.lang.String r14 = r61.nextString()
            java.lang.String r15 = "landscape"
            boolean r15 = r15.equalsIgnoreCase(r14)
            if (r15 == 0) goto L_0x0417
            com.google.android.gms.ads.internal.zzq.zzks()
            r39 = 6
            goto L_0x055d
        L_0x0417:
            java.lang.String r15 = "portrait"
            boolean r14 = r15.equalsIgnoreCase(r14)
            if (r14 == 0) goto L_0x0426
            com.google.android.gms.ads.internal.zzq.zzks()
            r39 = 7
            goto L_0x055d
        L_0x0426:
            r39 = -1
            goto L_0x055d
        L_0x042a:
            boolean r38 = r61.nextBoolean()
            goto L_0x055d
        L_0x0430:
            boolean r37 = r61.nextBoolean()
            goto L_0x055d
        L_0x0436:
            boolean r36 = r61.nextBoolean()
            goto L_0x055d
        L_0x043c:
            boolean r35 = r61.nextBoolean()
            goto L_0x055d
        L_0x0442:
            boolean r34 = r61.nextBoolean()
            goto L_0x055d
        L_0x0448:
            boolean r33 = r61.nextBoolean()
            goto L_0x055d
        L_0x044e:
            org.json.JSONObject r32 = com.google.android.gms.internal.ads.zzaxs.zzc(r61)
            goto L_0x055d
        L_0x0454:
            org.json.JSONObject r31 = com.google.android.gms.internal.ads.zzaxs.zzc(r61)
            goto L_0x055d
        L_0x045a:
            java.lang.String r30 = r61.nextString()
            goto L_0x055d
        L_0x0460:
            org.json.JSONObject r14 = com.google.android.gms.internal.ads.zzaxs.zzc(r61)
            com.google.android.gms.internal.ads.zzatn r29 = com.google.android.gms.internal.ads.zzatn.zzg(r14)
            goto L_0x055d
        L_0x046a:
            org.json.JSONObject r14 = com.google.android.gms.internal.ads.zzaxs.zzc(r61)
            java.lang.String r28 = r14.toString()
            goto L_0x055d
        L_0x0474:
            java.lang.String r27 = r61.nextString()
            goto L_0x055d
        L_0x047a:
            java.lang.String r26 = r61.nextString()
            goto L_0x055d
        L_0x0480:
            org.json.JSONObject r25 = com.google.android.gms.internal.ads.zzaxs.zzc(r61)
            goto L_0x055d
        L_0x0486:
            java.lang.String r24 = r61.nextString()
            goto L_0x055d
        L_0x048c:
            java.util.List r22 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x0492:
            java.util.List r23 = com.google.android.gms.internal.ads.zzczk.zze(r61)
            goto L_0x055d
        L_0x0498:
            com.google.android.gms.internal.ads.zzczp r14 = new com.google.android.gms.internal.ads.zzczp
            r15 = r61
            r14.<init>(r15)
            r21 = r14
            goto L_0x055d
        L_0x04a3:
            r15 = r61
            java.util.List r14 = com.google.android.gms.internal.ads.zzczk.zze(r61)
            r15 = r14
            goto L_0x055f
        L_0x04ac:
            r15 = r61
            java.util.List r14 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            r15 = r58
            goto L_0x00a6
        L_0x04b6:
            r15 = r61
            java.util.List r13 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x04be:
            r15 = r61
            org.json.JSONArray r12 = com.google.android.gms.internal.ads.zzaxs.zzd(r61)
            com.google.android.gms.internal.ads.zzasd r12 = com.google.android.gms.internal.ads.zzasd.zza(r12)
            goto L_0x055d
        L_0x04ca:
            r15 = r61
            java.lang.String r11 = r61.nextString()
            goto L_0x055d
        L_0x04d2:
            r15 = r61
            java.lang.String r10 = r61.nextString()
            goto L_0x055d
        L_0x04da:
            r15 = r61
            java.util.List r9 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x04e2:
            r15 = r61
            java.util.List r8 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x04ea:
            r15 = r61
            com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x04f1:
            r15 = r61
            java.util.List r7 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x04f9:
            r15 = r61
            int r6 = r61.nextInt()
            if (r6 == 0) goto L_0x055d
            r14 = 1
            if (r6 != r14) goto L_0x0505
            goto L_0x055d
        L_0x0505:
            r6 = 0
            goto L_0x055d
        L_0x0507:
            r15 = r61
            java.util.List r5 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x050e:
            r15 = r61
            java.util.List r4 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x0515:
            r15 = r61
            java.util.List r3 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
            goto L_0x055d
        L_0x051c:
            r15 = r61
            r14 = 1
            java.lang.String r2 = r61.nextString()
            java.lang.String r14 = "banner"
            boolean r14 = r14.equals(r2)
            if (r14 == 0) goto L_0x052d
            r2 = 1
            goto L_0x055d
        L_0x052d:
            java.lang.String r14 = "interstitial"
            boolean r14 = r14.equals(r2)
            if (r14 == 0) goto L_0x0537
            r2 = 2
            goto L_0x055d
        L_0x0537:
            java.lang.String r14 = "native_express"
            boolean r14 = r14.equals(r2)
            if (r14 == 0) goto L_0x0541
            r2 = 3
            goto L_0x055d
        L_0x0541:
            java.lang.String r14 = "native"
            boolean r14 = r14.equals(r2)
            if (r14 == 0) goto L_0x054b
            r2 = 4
            goto L_0x055d
        L_0x054b:
            java.lang.String r14 = "rewarded"
            boolean r2 = r14.equals(r2)
            if (r2 == 0) goto L_0x0555
            r2 = 5
            goto L_0x055d
        L_0x0555:
            r2 = 0
            goto L_0x055d
        L_0x0557:
            r15 = r61
            java.util.List r1 = com.google.android.gms.internal.ads.zzaxs.zza((android.util.JsonReader) r61)
        L_0x055d:
            r15 = r58
        L_0x055f:
            r14 = r59
            goto L_0x00a6
        L_0x0563:
            r59 = r14
            r58 = r15
            r15 = r61
            r61.endObject()
            r0.zzgli = r1
            r0.zzfjj = r2
            r0.zzdbq = r3
            r0.zzdbr = r4
            r0.zzglk = r5
            r0.zzglj = r6
            r0.zzdkz = r7
            r0.zzdla = r8
            r0.zzgll = r9
            r0.zzdcx = r10
            r0.zzdcy = r11
            r0.zzdky = r12
            r0.zzglm = r13
            r0.zzdkm = r14
            r14 = r58
            r0.zzgln = r14
            r14 = r21
            r0.zzglo = r14
            r11 = r22
            r0.zzglp = r11
            r12 = r23
            r0.zzglq = r12
            r1 = r24
            r0.zzdcm = r1
            r13 = r25
            r0.zzglr = r13
            r1 = r26
            r0.zzaez = r1
            r1 = r27
            r0.zzdbw = r1
            r1 = r28
            r0.zzdks = r1
            r1 = r29
            r0.zzgls = r1
            r1 = r30
            r0.zzdkp = r1
            r14 = r31
            r0.zzglt = r14
            r15 = r32
            r0.zzglu = r15
            r1 = r33
            r0.zzdcd = r1
            r1 = r34
            r0.zzdce = r1
            r1 = r35
            r0.zzdcf = r1
            r1 = r36
            r0.zzdmf = r1
            r1 = r37
            r0.zzglv = r1
            r1 = r38
            r0.zzblf = r1
            r1 = r39
            r0.zzglw = r1
            r1 = r40
            r0.zzglx = r1
            r1 = r41
            r0.zzdli = r1
            r1 = r42
            r0.zzeif = r1
            r1 = r43
            r0.zzgly = r1
            r1 = r44
            r0.zzdll = r1
            r1 = r45
            r0.zzdlm = r1
            r1 = r46
            r0.zzglz = r1
            r1 = r47
            r0.zzega = r1
            r1 = r48
            r0.zzdem = r1
            r1 = r49
            r0.zzfdp = r1
            r1 = r50
            r0.zzfhk = r1
            r1 = r51
            r0.zzgma = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzczl.<init>(android.util.JsonReader):void");
    }
}
