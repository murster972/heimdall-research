package com.google.android.gms.internal.ads;

final /* synthetic */ class zzdct implements zzbrn {
    private final zzdca zzgrk;

    zzdct(zzdca zzdca) {
        this.zzgrk = zzdca;
    }

    public final void zzp(Object obj) {
        zzdca zzdca = this.zzgrk;
        ((zzdcx) obj).zza((zzdco) zzdca.zzaqd(), zzdca.zzaqe());
    }
}
