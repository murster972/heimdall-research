package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.List;
import okhttp3.internal.cache.DiskLruCache;

public final class zzry extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzry> CREATOR = new zzsb();
    public final String url;
    private final long zzbrj;
    private final String zzbrk;
    private final String zzbrl;
    private final String zzbrm;
    private final Bundle zzbrn;
    public final boolean zzbro;
    public long zzbrp;

    zzry(String str, long j, String str2, String str3, String str4, Bundle bundle, boolean z, long j2) {
        this.url = str;
        this.zzbrj = j;
        String str5 = "";
        this.zzbrk = str2 == null ? str5 : str2;
        this.zzbrl = str3 == null ? str5 : str3;
        this.zzbrm = str4 != null ? str4 : str5;
        this.zzbrn = bundle == null ? new Bundle() : bundle;
        this.zzbro = z;
        this.zzbrp = j2;
    }

    public static zzry zzby(String str) {
        return zzd(Uri.parse(str));
    }

    public static zzry zzd(Uri uri) {
        long j;
        try {
            if (!"gcache".equals(uri.getScheme())) {
                return null;
            }
            List<String> pathSegments = uri.getPathSegments();
            if (pathSegments.size() != 2) {
                int size = pathSegments.size();
                StringBuilder sb = new StringBuilder(62);
                sb.append("Expected 2 path parts for namespace and id, found :");
                sb.append(size);
                zzayu.zzez(sb.toString());
                return null;
            }
            String str = pathSegments.get(0);
            String str2 = pathSegments.get(1);
            String host = uri.getHost();
            String queryParameter = uri.getQueryParameter(ReportDBAdapter.ReportColumns.COLUMN_URL);
            boolean equals = DiskLruCache.VERSION_1.equals(uri.getQueryParameter("read_only"));
            String queryParameter2 = uri.getQueryParameter("expiration");
            if (queryParameter2 == null) {
                j = 0;
            } else {
                j = Long.parseLong(queryParameter2);
            }
            long j2 = j;
            Bundle bundle = new Bundle();
            zzq.zzks();
            for (String next : uri.getQueryParameterNames()) {
                if (next.startsWith("tag.")) {
                    bundle.putString(next.substring(4), uri.getQueryParameter(next));
                }
            }
            return new zzry(queryParameter, j2, host, str, str2, bundle, equals, 0);
        } catch (NullPointerException | NumberFormatException e) {
            zzayu.zzd("Unable to parse Uri into cache offering.", e);
            return null;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.url, false);
        SafeParcelWriter.a(parcel, 3, this.zzbrj);
        SafeParcelWriter.a(parcel, 4, this.zzbrk, false);
        SafeParcelWriter.a(parcel, 5, this.zzbrl, false);
        SafeParcelWriter.a(parcel, 6, this.zzbrm, false);
        SafeParcelWriter.a(parcel, 7, this.zzbrn, false);
        SafeParcelWriter.a(parcel, 8, this.zzbro);
        SafeParcelWriter.a(parcel, 9, this.zzbrp);
        SafeParcelWriter.a(parcel, a2);
    }
}
