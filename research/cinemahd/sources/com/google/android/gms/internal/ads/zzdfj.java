package com.google.android.gms.internal.ads;

import java.util.AbstractMap;
import java.util.Map;

final class zzdfj extends zzdeu<Map.Entry<K, V>> {
    private final /* synthetic */ zzdfg zzgva;

    zzdfj(zzdfg zzdfg) {
        this.zzgva = zzdfg;
    }

    public final /* synthetic */ Object get(int i) {
        zzdei.zzs(i, this.zzgva.size);
        int i2 = i * 2;
        return new AbstractMap.SimpleImmutableEntry(this.zzgva.zzguw[i2], this.zzgva.zzguw[i2 + 1]);
    }

    public final int size() {
        return this.zzgva.size;
    }

    public final boolean zzarc() {
        return true;
    }
}
