package com.google.android.gms.internal.cast;

import android.widget.ProgressBar;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbg extends UIController implements RemoteMediaClient.ProgressListener {
    private final ProgressBar zztb;
    private final long zztc;

    public zzbg(ProgressBar progressBar, long j) {
        this.zztb = progressBar;
        this.zztc = j;
    }

    public final void onMediaStatusUpdated() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            this.zztb.setMax(1);
            this.zztb.setProgress(0);
        }
    }

    public final void onProgressUpdated(long j, long j2) {
        this.zztb.setMax((int) j2);
        this.zztb.setProgress((int) j);
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null) {
            remoteMediaClient.a((RemoteMediaClient.ProgressListener) this, this.zztc);
            if (remoteMediaClient.k()) {
                this.zztb.setMax((int) remoteMediaClient.j());
                this.zztb.setProgress((int) remoteMediaClient.b());
                return;
            }
            this.zztb.setMax(1);
            this.zztb.setProgress(0);
        }
    }

    public final void onSessionEnded() {
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this);
        }
        this.zztb.setMax(1);
        this.zztb.setProgress(0);
        super.onSessionEnded();
    }
}
