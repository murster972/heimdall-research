package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzagt extends zzgb implements zzagu {
    public zzagt() {
        super("com.google.android.gms.ads.internal.initialization.IInitializationCallback");
    }

    public static zzagu zzy(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.initialization.IInitializationCallback");
        if (queryLocalInterface instanceof zzagu) {
            return (zzagu) queryLocalInterface;
        }
        return new zzagw(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        zzc(parcel.createTypedArrayList(zzagn.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
