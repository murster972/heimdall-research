package com.google.android.gms.internal.ads;

public final class zzhh {
    public static final zzhh zzagg = new zzhh(0);
    public final int zzagh = 0;

    public zzhh(int i) {
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && zzhh.class == obj.getClass() && this.zzagh == ((zzhh) obj).zzagh;
    }

    public final int hashCode() {
        return this.zzagh;
    }
}
