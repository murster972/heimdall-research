package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdjj extends zzdih<zzdlr, zzdlo> {
    private final /* synthetic */ zzdjh zzgze;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdjj(zzdjh zzdjh, Class cls) {
        super(cls);
        this.zzgze = zzdjh;
    }

    public final /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdlr zzdlr = (zzdlr) zzdte;
        zzdpo.zzez(zzdlr.getKeySize());
        if (zzdlr.zzatq().zzatn() != 12 && zzdlr.zzatq().zzatn() != 16) {
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        }
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdlr zzdlr = (zzdlr) zzdte;
        return (zzdlo) zzdlo.zzatr().zzac(zzdqk.zzu(zzdpn.zzey(zzdlr.getKeySize()))).zzb(zzdlr.zzatq()).zzef(0).zzbaf();
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdlr.zzad(zzdqk);
    }
}
