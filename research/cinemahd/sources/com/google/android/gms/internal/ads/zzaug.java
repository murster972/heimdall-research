package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaug implements zzaui {
    static final zzaui zzdpr = new zzaug();

    private zzaug() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return Long.valueOf(zzbfq.generateEventId());
    }
}
