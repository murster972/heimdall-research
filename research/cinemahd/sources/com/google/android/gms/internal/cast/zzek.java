package com.google.android.gms.internal.cast;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzek extends zzem {
    zzek(zzeh zzeh, GoogleApiClient googleApiClient) {
        super(zzeh, googleApiClient);
    }

    /* renamed from: zza */
    public final void doExecute(zzer zzer) throws RemoteException {
        zzer.zza(new zzeo(this));
    }
}
