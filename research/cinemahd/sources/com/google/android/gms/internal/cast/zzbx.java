package com.google.android.gms.internal.cast;

import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$attr;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.cast.framework.R$style;
import com.google.android.gms.cast.framework.R$styleable;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbx extends UIController implements RemoteMediaClient.ProgressListener {
    private final zzbh zzrw;
    private final TextView zzvp;
    private final ImageView zzvq;
    private final int zzvr;

    public zzbx(View view, zzbh zzbh) {
        this.zzvp = (TextView) view.findViewById(R$id.live_indicator_text);
        this.zzvq = (ImageView) view.findViewById(R$id.live_indicator_dot);
        this.zzrw = zzbh;
        TypedArray obtainStyledAttributes = view.getContext().obtainStyledAttributes((AttributeSet) null, R$styleable.CastExpandedController, R$attr.castExpandedControllerStyle, R$style.CastExpandedController);
        int resourceId = obtainStyledAttributes.getResourceId(R$styleable.CastExpandedController_castLiveIndicatorColor, 0);
        obtainStyledAttributes.recycle();
        this.zzvr = view.getContext().getResources().getColor(resourceId);
        this.zzvq.getDrawable().setColorFilter(this.zzvr, PorterDuff.Mode.SRC_IN);
        zzdj();
    }

    private final void zzdj() {
        boolean z;
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        int i = 8;
        if (remoteMediaClient == null || !remoteMediaClient.k() || !remoteMediaClient.m()) {
            this.zzvp.setVisibility(8);
            this.zzvq.setVisibility(8);
            return;
        }
        if (!this.zzrw.zzdo()) {
            z = remoteMediaClient.p();
        } else {
            z = this.zzrw.zzdq();
        }
        this.zzvp.setVisibility(0);
        ImageView imageView = this.zzvq;
        if (z) {
            i = 0;
        }
        imageView.setVisibility(i);
    }

    public final void onMediaStatusUpdated() {
        zzdj();
    }

    public final void onProgressUpdated(long j, long j2) {
        zzdj();
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this, 1000);
        }
        zzdj();
    }

    public final void onSessionEnded() {
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this);
        }
        super.onSessionEnded();
        zzdj();
    }
}
