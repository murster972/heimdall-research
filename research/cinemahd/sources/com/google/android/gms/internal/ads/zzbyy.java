package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbyy implements zzdgf {
    private final String zzcyr;
    private final zzbyu zzfpl;

    zzbyy(zzbyu zzbyu, String str) {
        this.zzfpl = zzbyu;
        this.zzcyr = str;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfpl.zzb(this.zzcyr, obj);
    }
}
