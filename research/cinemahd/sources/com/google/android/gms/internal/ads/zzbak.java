package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbak implements Runnable {
    private final zzbai zzdyl;
    private final boolean zzdym;

    zzbak(zzbai zzbai, boolean z) {
        this.zzdyl = zzbai;
        this.zzdym = z;
    }

    public final void run() {
        this.zzdyl.zzau(this.zzdym);
    }
}
