package com.google.android.gms.internal.ads;

final class zzrt implements Runnable {
    private final /* synthetic */ zzrq zzbrh;

    zzrt(zzrq zzrq) {
        this.zzbrh = zzrq;
    }

    public final void run() {
        this.zzbrh.disconnect();
    }
}
