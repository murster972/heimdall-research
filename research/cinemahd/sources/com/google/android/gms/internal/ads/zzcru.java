package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcru implements zzcty<Bundle> {
    private final Bundle zzgfv;

    public zzcru(Bundle bundle) {
        this.zzgfv = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        ((Bundle) obj).putBundle("content_info", this.zzgfv);
    }
}
