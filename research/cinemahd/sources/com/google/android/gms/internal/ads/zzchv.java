package com.google.android.gms.internal.ads;

import android.database.sqlite.SQLiteDatabase;

final class zzchv implements zzdgt<SQLiteDatabase> {
    private final /* synthetic */ zzdby zzfww;

    zzchv(zzcht zzcht, zzdby zzdby) {
        this.zzfww = zzdby;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        try {
            this.zzfww.apply((SQLiteDatabase) obj);
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            zzayu.zzex(valueOf.length() != 0 ? "Error executing function on offline signal database: ".concat(valueOf) : new String("Error executing function on offline signal database: "));
        }
    }

    public final void zzb(Throwable th) {
        String valueOf = String.valueOf(th.getMessage());
        zzayu.zzex(valueOf.length() != 0 ? "Failed to get offline signal database: ".concat(valueOf) : new String("Failed to get offline signal database: "));
    }
}
