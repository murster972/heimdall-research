package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbmd;
import com.google.android.gms.internal.ads.zzbob;
import com.google.android.gms.internal.ads.zzsy;
import java.util.LinkedList;
import java.util.concurrent.Executor;

public final class zzcxg<R extends zzbob<AdT>, AdT extends zzbmd> implements zzcxt<R, AdT> {
    private final Executor executor;
    private final zzcxt<R, AdT> zzgio;
    /* access modifiers changed from: private */
    public final zzdbb zzgjq;
    private final zzdax zzgjr;
    /* access modifiers changed from: private */
    public final zzcxt<R, zzdbi<AdT>> zzgjs;
    private final LinkedList<zzcxn<R>> zzgjt;
    /* access modifiers changed from: private */
    public zzcxn<R> zzgju;
    private zzdhe<zzdbi<AdT>> zzgjv;
    /* access modifiers changed from: private */
    public int zzgjw = zzcxm.zzgkd;
    private R zzgjx;
    private final zzdgt<zzdbi<AdT>> zzgjy = new zzcxk(this);

    public zzcxg(zzdbb zzdbb, zzdax zzdax, zzcxt<R, AdT> zzcxt, zzcxt<R, zzdbi<AdT>> zzcxt2, Executor executor2) {
        this.zzgjq = zzdbb;
        this.zzgjr = zzdax;
        this.zzgio = zzcxt;
        this.zzgjs = zzcxt2;
        this.zzgjt = new LinkedList<>();
        this.executor = executor2;
        this.zzgjr.zza(new zzcxl(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: zzaoh */
    public final synchronized R zzaog() {
        return this.zzgjx;
    }

    public final zzdhe<AdT> zza(zzcxs zzcxs, zzcxv<R> zzcxv) {
        zzczu zzady = ((zzbob) zzcxv.zzc(zzcxs).zzadg()).zzady();
        zzcxn zzcxn = new zzcxn(zzcxv, zzcxs, zzady.zzgml, zzady.zzgmm, this.executor, zzady.zzgmp);
        zzdbb zzdbb = this.zzgjq;
        zzdbi<?> zza = zzdbb.zza(zzdbb.zza(zzcxn.zzdio, zzcxn.zzbqz, zzcxn.zzgey));
        if (zza != null) {
            this.zzgjx = null;
            this.zzgjt.add(zzcxn);
            return zza(zza, zzcxs, zzcxv);
        }
        zzdhe<zzdbi<AdT>> zzdhe = this.zzgjv;
        if (zzdhe != null && !zzdhe.isDone()) {
            this.zzgjw = zzcxm.zzgkf;
            zzdbl zza2 = this.zzgjq.zza(zzcxn.zzdio, zzcxn.zzbqz, zzcxn.zzgey);
            zzdbb zzdbb2 = this.zzgjq;
            zzcxn<R> zzcxn2 = this.zzgju;
            if (zza2.equals(zzdbb2.zza(zzcxn2.zzdio, zzcxn2.zzbqz, zzcxn.zzgey))) {
                this.zzgjw = zzcxm.zzgke;
                this.zzgjx = (zzbob) this.zzgjs.zzaog();
                return zzdgs.zzb(this.zzgjv, new zzcxi(this), this.executor);
            }
        }
        this.zzgjt.add(zzcxn);
        zzdhe<AdT> zza3 = this.zzgio.zza(zzcxs, zzcxv);
        this.zzgjx = (zzbob) this.zzgio.zzaog();
        return zza3;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaoi() {
        synchronized (this) {
            zza(this.zzgju);
        }
    }

    private final zzdhe<AdT> zza(zzdbi<AdT> zzdbi, zzcxs zzcxs, zzcxv<R> zzcxv) {
        zzboe<R> zzc = zzcxv.zzc(zzcxs);
        if (zzdbi.zzgpd != null) {
            zzdbi.zzgpd.zzagt().zzb(((zzbob) zzc.zzadg()).zzadz());
            return zzdgs.zzaj(zzdbi.zzgpd);
        }
        zzc.zza(zzdbi.zzelt);
        return zzdgs.zzb(this.zzgio.zza(zzcxs, zzcxv), new zzcxj(zzc), this.executor);
    }

    /* access modifiers changed from: private */
    public final void zza(zzcxn<R> zzcxn) {
        while (true) {
            zzdhe<zzdbi<AdT>> zzdhe = this.zzgjv;
            if (zzdhe == null || zzdhe.isDone()) {
                if (zzcxn != null || !this.zzgjt.isEmpty()) {
                    if (zzcxn == null) {
                        zzcxn = this.zzgjt.remove();
                    }
                    zzdbb zzdbb = this.zzgjq;
                    if (zzdbb.zzb(zzdbb.zza(zzcxn.zzdio, zzcxn.zzbqz, zzcxn.zzgey))) {
                        this.zzgju = zzcxn;
                        this.zzgjv = this.zzgjs.zza(zzcxn.zzgki, zzcxn.zzgkh);
                        zzdgs.zza(this.zzgjv, this.zzgjy, zzcxn.executor);
                        return;
                    }
                    zzcxn = null;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzdbi zzdbi) throws Exception {
        zzdbi.zzgpc.zzahb().zzc((zzsy.zza) zzsy.zza.zzmz().zza(zzsy.zza.C0048zza.zzmx().zzb(zzsy.zza.zzc.IN_MEMORY).zza(zzsy.zza.zzd.zznb())).zzbaf());
        zzcxn<R> zzcxn = this.zzgju;
        return zza(zzdbi, zzcxn.zzgki, zzcxn.zzgkh);
    }
}
