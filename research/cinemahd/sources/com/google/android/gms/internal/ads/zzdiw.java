package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdit;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Set;

final class zzdiw implements zzdit.zza {
    private final /* synthetic */ zzdid zzgyo;

    zzdiw(zzdid zzdid) {
        this.zzgyo = zzdid;
    }

    public final Set<Class<?>> zzase() {
        return Collections.singleton(this.zzgyo.zzarz());
    }

    public final zzdid<?> zzasn() {
        return this.zzgyo;
    }

    public final Class<?> zzaso() {
        return this.zzgyo.getClass();
    }

    public final Class<?> zzasp() {
        return null;
    }

    public final <Q> zzdid<Q> zzb(Class<Q> cls) throws GeneralSecurityException {
        if (this.zzgyo.zzarz().equals(cls)) {
            return this.zzgyo;
        }
        throw new InternalError("This should never be called, as we always first check supportedPrimitives.");
    }
}
