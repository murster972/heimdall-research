package com.google.android.gms.internal.ads;

public final class zzbwh implements zzdxg<zzbwi> {
    private final zzdxp<zzbws> zzfkx;

    private zzbwh(zzdxp<zzbws> zzdxp) {
        this.zzfkx = zzdxp;
    }

    public static zzbwh zzw(zzdxp<zzbws> zzdxp) {
        return new zzbwh(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbwi(this.zzfkx.get());
    }
}
