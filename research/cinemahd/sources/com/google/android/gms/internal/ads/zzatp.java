package com.google.android.gms.internal.ads;

public final class zzatp {
    public static boolean isEnabled() {
        return zzabf.zzcud.get().booleanValue();
    }

    public static void zzea(String str) {
        if (zzabf.zzcud.get().booleanValue()) {
            zzayu.zzea(str);
        }
    }
}
