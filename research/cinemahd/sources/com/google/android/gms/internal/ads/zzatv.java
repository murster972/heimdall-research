package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import java.lang.reflect.Method;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class zzatv {
    private final AtomicReference<ThreadPoolExecutor> zzdpe = new AtomicReference<>((Object) null);
    private final Object zzdpf = new Object();
    private String zzdpg = null;
    private String zzdph = null;
    private final AtomicBoolean zzdpi = new AtomicBoolean(false);
    private final AtomicInteger zzdpj = new AtomicInteger(-1);
    private final AtomicReference<Object> zzdpk = new AtomicReference<>((Object) null);
    private final AtomicReference<Object> zzdpl = new AtomicReference<>((Object) null);
    private final ConcurrentMap<String, Method> zzdpm = new ConcurrentHashMap(9);
    private final AtomicReference<zzbfq> zzdpn = new AtomicReference<>((Object) null);
    private final BlockingQueue<FutureTask<?>> zzdpo = new ArrayBlockingQueue(20);
    private final Object zzdpp = new Object();

    private static boolean zzac(Context context) {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcih)).booleanValue()) {
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcig)).booleanValue()) {
                return false;
            }
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcii)).booleanValue()) {
            try {
                context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
                return false;
            } catch (ClassNotFoundException unused) {
            }
        }
        return true;
    }

    private final Method zzai(Context context) {
        Method method = (Method) this.zzdpm.get("logEventInternal");
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod("logEventInternal", new Class[]{String.class, String.class, Bundle.class});
            this.zzdpm.put("logEventInternal", declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, "logEventInternal", true);
            return null;
        }
    }

    private static Bundle zzl(String str, String str2) {
        Bundle bundle = new Bundle();
        try {
            bundle.putLong("_aeid", Long.parseLong(str));
        } catch (NullPointerException | NumberFormatException e) {
            String valueOf = String.valueOf(str);
            zzayu.zzc(valueOf.length() != 0 ? "Invalid event ID: ".concat(valueOf) : new String("Invalid event ID: "), e);
        }
        if ("_ac".equals(str2)) {
            bundle.putInt("_r", 1);
        }
        return bundle;
    }

    private final Method zzm(Context context, String str) {
        Method method = (Method) this.zzdpm.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, new Class[0]);
            this.zzdpm.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    private final Method zzn(Context context, String str) {
        Method method = (Method) this.zzdpm.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics").getDeclaredMethod(str, new Class[]{Activity.class, String.class, String.class});
            this.zzdpm.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    private final ThreadPoolExecutor zzuq() {
        if (this.zzdpe.get() == null) {
            this.zzdpe.compareAndSet((Object) null, new ThreadPoolExecutor(((Integer) zzve.zzoy().zzd(zzzn.zzcif)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcif)).intValue(), 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), new zzauj(this)));
        }
        return this.zzdpe.get();
    }

    public final void zza(Context context, zzyq zzyq) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcim)).booleanValue() && zzab(context) && zzac(context)) {
            synchronized (this.zzdpp) {
            }
        }
    }

    public final boolean zzab(Context context) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzchz)).booleanValue() && !this.zzdpi.get()) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcij)).booleanValue()) {
                return true;
            }
            if (this.zzdpj.get() == -1) {
                zzve.zzou();
                if (!zzayk.zzc(context, GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE)) {
                    zzve.zzou();
                    if (zzayk.zzbk(context)) {
                        zzayu.zzez("Google Play Service is out of date, the Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires updated Google Play Service.");
                        this.zzdpj.set(0);
                    }
                }
                this.zzdpj.set(1);
            }
            if (this.zzdpj.get() == 1) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String zzad(android.content.Context r7) {
        /*
            r6 = this;
            java.lang.String r0 = "getCurrentScreenName"
            boolean r1 = r6.zzab(r7)
            java.lang.String r2 = ""
            if (r1 != 0) goto L_0x000b
            return r2
        L_0x000b:
            boolean r1 = zzac(r7)
            if (r1 == 0) goto L_0x001c
            com.google.android.gms.internal.ads.zzaui r7 = com.google.android.gms.internal.ads.zzaua.zzdpr
            java.lang.String r0 = "getCurrentScreenNameOrScreenClass"
            java.lang.Object r7 = r6.zza((java.lang.String) r0, r2, r7)
            java.lang.String r7 = (java.lang.String) r7
            return r7
        L_0x001c:
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r1 = r6.zzdpk
            r3 = 1
            java.lang.String r4 = "com.google.android.gms.measurement.AppMeasurement"
            boolean r1 = r6.zza((android.content.Context) r7, (java.lang.String) r4, (java.util.concurrent.atomic.AtomicReference<java.lang.Object>) r1, (boolean) r3)
            if (r1 != 0) goto L_0x0028
            return r2
        L_0x0028:
            r1 = 0
            java.lang.reflect.Method r3 = r6.zzm(r7, r0)     // Catch:{ Exception -> 0x0056 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r4 = r6.zzdpk     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r4 = r4.get()     // Catch:{ Exception -> 0x0056 }
            java.lang.Object[] r5 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r3 = r3.invoke(r4, r5)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0056 }
            if (r3 != 0) goto L_0x0052
            java.lang.String r3 = "getCurrentScreenClass"
            java.lang.reflect.Method r7 = r6.zzm(r7, r3)     // Catch:{ Exception -> 0x0056 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r3 = r6.zzdpk     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r3 = r3.get()     // Catch:{ Exception -> 0x0056 }
            java.lang.Object[] r4 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r7 = r7.invoke(r3, r4)     // Catch:{ Exception -> 0x0056 }
            r3 = r7
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0056 }
        L_0x0052:
            if (r3 == 0) goto L_0x0055
            return r3
        L_0x0055:
            return r2
        L_0x0056:
            r7 = move-exception
            r6.zza((java.lang.Exception) r7, (java.lang.String) r0, (boolean) r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzatv.zzad(android.content.Context):java.lang.String");
    }

    public final String zzae(Context context) {
        if (!zzab(context)) {
            return null;
        }
        synchronized (this.zzdpf) {
            if (this.zzdpg != null) {
                String str = this.zzdpg;
                return str;
            }
            if (zzac(context)) {
                this.zzdpg = (String) zza("getGmpAppId", this.zzdpg, zzauc.zzdpr);
            } else {
                this.zzdpg = (String) zza("getGmpAppId", context);
            }
            String str2 = this.zzdpg;
            return str2;
        }
    }

    public final String zzaf(Context context) {
        if (!zzab(context)) {
            return null;
        }
        long longValue = ((Long) zzve.zzoy().zzd(zzzn.zzcie)).longValue();
        if (zzac(context)) {
            if (longValue >= 0) {
                return (String) zzuq().submit(new zzaue(this)).get(longValue, TimeUnit.MILLISECONDS);
            }
            try {
                return (String) zza("getAppInstanceId", (Object) null, zzauf.zzdpr);
            } catch (TimeoutException unused) {
                return "TIME_OUT";
            } catch (Exception unused2) {
                return null;
            }
        } else if (longValue < 0) {
            return (String) zza("getAppInstanceId", context);
        } else {
            try {
                return (String) zzuq().submit(new zzauh(this, context)).get(longValue, TimeUnit.MILLISECONDS);
            } catch (TimeoutException unused3) {
                return "TIME_OUT";
            } catch (Exception unused4) {
                return null;
            }
        }
    }

    public final String zzag(Context context) {
        if (!zzab(context)) {
            return null;
        }
        if (zzac(context)) {
            Long l = (Long) zza("getAdEventId", (Object) null, zzaug.zzdpr);
            if (l != null) {
                return Long.toString(l.longValue());
            }
            return null;
        }
        Object zza = zza("generateEventId", context);
        if (zza != null) {
            return zza.toString();
        }
        return null;
    }

    public final String zzah(Context context) {
        if (!zzab(context)) {
            return null;
        }
        synchronized (this.zzdpf) {
            if (this.zzdph != null) {
                String str = this.zzdph;
                return str;
            }
            if (zzac(context)) {
                this.zzdph = (String) zza("getAppIdOrigin", this.zzdph, zzatx.zzdpr);
            } else {
                this.zzdph = "fa";
            }
            String str2 = this.zzdph;
            return str2;
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzaj(Context context) throws Exception {
        return (String) zza("getAppInstanceId", context);
    }

    public final void zze(Context context, String str) {
        if (zzab(context)) {
            if (zzac(context)) {
                zza("beginAdUnitExposure", (zzaul) new zzatu(str));
            } else {
                zza(context, str, "beginAdUnitExposure");
            }
        }
    }

    public final void zzf(Context context, String str) {
        if (zzab(context)) {
            if (zzac(context)) {
                zza("endAdUnitExposure", (zzaul) new zzaub(str));
            } else {
                zza(context, str, "endAdUnitExposure");
            }
        }
    }

    public final void zzg(Context context, String str) {
        if (!zzab(context) || !(context instanceof Activity)) {
            return;
        }
        if (zzac(context)) {
            zza("setScreenName", (zzaul) new zzaud(context, str));
        } else if (zza(context, "com.google.firebase.analytics.FirebaseAnalytics", this.zzdpl, false)) {
            Method zzn = zzn(context, "setCurrentScreen");
            try {
                zzn.invoke(this.zzdpl.get(), new Object[]{(Activity) context, str, context.getPackageName()});
            } catch (Exception e) {
                zza(e, "setCurrentScreen", false);
            }
        }
    }

    public final void zzh(Context context, String str) {
        zza(context, "_ac", str, (Bundle) null);
    }

    public final void zzi(Context context, String str) {
        zza(context, "_ai", str, (Bundle) null);
    }

    public final void zzj(Context context, String str) {
        zza(context, "_aq", str, (Bundle) null);
    }

    public final void zzk(Context context, String str) {
        zza(context, "_aa", str, (Bundle) null);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzur() throws Exception {
        return (String) zza("getAppInstanceId", (Object) null, zzaty.zzdpr);
    }

    private final Method zzl(Context context, String str) {
        Method method = (Method) this.zzdpm.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, new Class[]{String.class});
            this.zzdpm.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    public final void zza(Context context, zzug zzug) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcim)).booleanValue() && zzab(context) && zzac(context)) {
            synchronized (this.zzdpp) {
            }
        }
    }

    public final void zza(Context context, String str, String str2, String str3, int i) {
        if (zzab(context)) {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str2);
            bundle.putString("reward_type", str3);
            bundle.putInt("reward_value", i);
            zza(context, "_ar", str, bundle);
            StringBuilder sb = new StringBuilder(String.valueOf(str3).length() + 75);
            sb.append("Log a Firebase reward video event, reward type: ");
            sb.append(str3);
            sb.append(", reward value: ");
            sb.append(i);
            zzavs.zzed(sb.toString());
        }
    }

    private final void zza(Context context, String str, String str2, Bundle bundle) {
        if (zzab(context)) {
            Bundle zzl = zzl(str2, str);
            if (bundle != null) {
                zzl.putAll(bundle);
            }
            if (zzac(context)) {
                zza("logEventInternal", (zzaul) new zzatw(str, zzl));
            } else if (zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdpk, true)) {
                Method zzai = zzai(context);
                try {
                    zzai.invoke(this.zzdpk.get(), new Object[]{"am", str, zzl});
                } catch (Exception e) {
                    zza(e, "logEventInternal", true);
                }
            }
        }
    }

    private final void zza(Context context, String str, String str2) {
        if (zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdpk, true)) {
            Method zzl = zzl(context, str2);
            try {
                zzl.invoke(this.zzdpk.get(), new Object[]{str});
                StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 37 + String.valueOf(str).length());
                sb.append("Invoke Firebase method ");
                sb.append(str2);
                sb.append(", Ad Unit Id: ");
                sb.append(str);
                zzavs.zzed(sb.toString());
            } catch (Exception e) {
                zza(e, str2, false);
            }
        }
    }

    private final Object zza(String str, Context context) {
        if (!zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdpk, true)) {
            return null;
        }
        try {
            return zzm(context, str).invoke(this.zzdpk.get(), new Object[0]);
        } catch (Exception e) {
            zza(e, str, true);
            return null;
        }
    }

    private final void zza(Exception exc, String str, boolean z) {
        if (!this.zzdpi.get()) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 30);
            sb.append("Invoke Firebase method ");
            sb.append(str);
            sb.append(" error.");
            zzayu.zzez(sb.toString());
            if (z) {
                zzayu.zzez("The Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires the latest Firebase SDK jar, but Firebase SDK is either missing or out of date");
                this.zzdpi.set(true);
            }
        }
    }

    private final boolean zza(Context context, String str, AtomicReference<Object> atomicReference, boolean z) {
        if (atomicReference.get() == null) {
            try {
                atomicReference.compareAndSet((Object) null, context.getClassLoader().loadClass(str).getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke((Object) null, new Object[]{context}));
            } catch (Exception e) {
                zza(e, "getInstance", z);
                return false;
            }
        }
        return true;
    }

    private final void zza(String str, zzaul zzaul) {
        synchronized (this.zzdpn) {
            FutureTask futureTask = new FutureTask(new zzatz(this, zzaul, str), (Object) null);
            if (this.zzdpn.get() != null) {
                futureTask.run();
            } else {
                this.zzdpo.offer(futureTask);
            }
        }
    }

    private final <T> T zza(String str, T t, zzaui<T> zzaui) {
        synchronized (this.zzdpn) {
            if (this.zzdpn.get() != null) {
                try {
                    T zzb = zzaui.zzb(this.zzdpn.get());
                    return zzb;
                } catch (Exception e) {
                    zza(e, str, false);
                    return t;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzaul zzaul, String str) {
        if (this.zzdpn.get() != null) {
            try {
                zzaul.zza(this.zzdpn.get());
            } catch (Exception e) {
                zza(e, str, false);
            }
        }
    }
}
