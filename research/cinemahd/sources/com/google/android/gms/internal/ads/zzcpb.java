package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzbod;
import com.google.android.gms.internal.ads.zzbrm;

public final class zzcpb implements zzcox<zzbmd> {
    private final zzbfx zzfzz;
    private final Context zzgcr;
    private final zzczw zzgcs;
    private final zzcov zzgds;
    private zzbmo zzgdt;

    public zzcpb(zzbfx zzbfx, Context context, zzcov zzcov, zzczw zzczw) {
        this.zzfzz = zzbfx;
        this.zzgcr = context;
        this.zzgds = zzcov;
        this.zzgcs = zzczw;
    }

    public final boolean isLoading() {
        zzbmo zzbmo = this.zzgdt;
        return zzbmo != null && zzbmo.isLoading();
    }

    public final boolean zza(zzug zzug, String str, zzcpa zzcpa, zzcoz<? super zzbmd> zzcoz) throws RemoteException {
        if (str == null) {
            zzayu.zzex("Ad unit ID should not be null for NativeAdLoader.");
            this.zzfzz.zzaca().execute(new zzcpe(this));
            return false;
        }
        zzdad.zze(this.zzgcr, zzug.zzccb);
        zzczu zzaos = this.zzgcs.zzg(zzug).zzdl(zzcpa instanceof zzcpc ? ((zzcpc) zzcpa).zzgdu : 1).zzaos();
        zzbvm zzadf = this.zzfzz.zzacl().zza(new zzbod.zza().zzbz(this.zzgcr).zza(zzaos).zzahh()).zza(new zzbrm.zza().zza(this.zzgds.zzamt(), this.zzfzz.zzaca()).zza(this.zzgds.zzamu(), this.zzfzz.zzaca()).zza(this.zzgds.zzamv(), this.zzfzz.zzaca()).zza(this.zzgds.zzamw(), this.zzfzz.zzaca()).zza(this.zzgds.zzams(), this.zzfzz.zzaca()).zza(zzaos.zzgmr, this.zzfzz.zzaca()).zzahw()).zza(this.zzgds.zzamr()).zzadf();
        zzadf.zzade().zzdm(1);
        this.zzgdt = new zzbmo(this.zzfzz.zzacc(), this.zzfzz.zzacb(), zzadf.zzadc().zzaha());
        this.zzgdt.zza((zzdgt<zzbmd>) new zzcpd(this, zzcoz, zzadf));
        return true;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzamy() {
        this.zzgds.zzamu().onAdFailedToLoad(1);
    }
}
