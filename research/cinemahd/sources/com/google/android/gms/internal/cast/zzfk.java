package com.google.android.gms.internal.cast;

import android.annotation.TargetApi;
import android.view.Choreographer;

@TargetApi(16)
final class zzfk extends zzfe {
    private Choreographer zzabs = Choreographer.getInstance();

    public final void zza(zzfg zzfg) {
        this.zzabs.postFrameCallback(zzfg.zzfi());
    }
}
