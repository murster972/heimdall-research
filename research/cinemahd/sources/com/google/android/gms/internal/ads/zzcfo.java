package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcfo implements zzdxg<zzdhe<Bundle>> {
    private final zzdxp<zzcua<Bundle>> zzeop;
    private final zzdxp<zzdcr> zzfet;

    private zzcfo(zzdxp<zzdcr> zzdxp, zzdxp<zzcua<Bundle>> zzdxp2) {
        this.zzfet = zzdxp;
        this.zzeop = zzdxp2;
    }

    public static zzcfo zzab(zzdxp<zzdcr> zzdxp, zzdxp<zzcua<Bundle>> zzdxp2) {
        return new zzcfo(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzdhe) zzdxm.zza(this.zzfet.get().zzu(zzdco.SIGNALS).zzc(this.zzeop.get().zzs(new Bundle())).zzaqg(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
