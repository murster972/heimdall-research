package com.google.android.gms.internal.ads;

public final class zzbwb implements zzdxg<zzcaj> {
    private final zzbvy zzfla;

    public zzbwb(zzbvy zzbvy) {
        this.zzfla = zzbvy;
    }

    public static zzcaj zza(zzbvy zzbvy) {
        return (zzcaj) zzdxm.zza(zzbvy.zzait(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfla);
    }
}
