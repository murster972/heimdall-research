package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

final class zzut extends zzvb<zzvu> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ String zzcdg;
    private final /* synthetic */ zzalc zzcdh;
    private final /* synthetic */ zzup zzcdi;
    private final /* synthetic */ zzuj zzcdj;

    zzut(zzup zzup, Context context, zzuj zzuj, String str, zzalc zzalc) {
        this.zzcdi = zzup;
        this.val$context = context;
        this.zzcdj = zzuj;
        this.zzcdg = str;
        this.zzcdh = zzalc;
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zzc(ObjectWrapper.a(this.val$context), this.zzcdj, this.zzcdg, this.zzcdh, 19649000);
    }

    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, "app_open");
        return new zzyd();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzccx.zza(this.val$context, this.zzcdj, this.zzcdg, this.zzcdh, 4);
    }
}
