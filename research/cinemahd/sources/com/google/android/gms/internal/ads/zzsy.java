package com.google.android.gms.internal.ads;

import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.internal.ads.zzdrt;

public final class zzsy {

    public static final class zza extends zzdrt<zza, zzb> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zza zzbuj;
        private static volatile zzdtn<zza> zzdz;
        private zzdsb<C0048zza> zzbui = zzdrt.zzazw();

        public static final class zzb extends zzdrt.zzb<zza, zzb> implements zzdtg {
            private zzb() {
                super(zza.zzbuj);
            }

            public final zzb zza(C0048zza.C0049zza zza) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zza((C0048zza) zza.zzbaf());
                return this;
            }

            /* synthetic */ zzb(zzsx zzsx) {
                this();
            }
        }

        public enum zzc implements zzdry {
            UNSPECIFIED(0),
            IN_MEMORY(1);
            
            private static final zzdrx<zzc> zzen = null;
            private final int value;

            static {
                zzen = new zzsz();
            }

            private zzc(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zzta.zzew;
            }

            public static zzc zzbs(int i) {
                if (i == 0) {
                    return UNSPECIFIED;
                }
                if (i != 1) {
                    return null;
                }
                return IN_MEMORY;
            }

            public final String toString() {
                return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        public static final class zzd extends zzdrt<zzd, C0050zza> implements zzdtg {
            /* access modifiers changed from: private */
            public static final zzd zzbup;
            private static volatile zzdtn<zzd> zzdz;
            private boolean zzbun;
            private int zzbuo;
            private int zzdl;

            /* renamed from: com.google.android.gms.internal.ads.zzsy$zza$zzd$zza  reason: collision with other inner class name */
            public static final class C0050zza extends zzdrt.zzb<zzd, C0050zza> implements zzdtg {
                private C0050zza() {
                    super(zzd.zzbup);
                }

                public final C0050zza zzbu(int i) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzd) this.zzhmp).zzbt(i);
                    return this;
                }

                public final C0050zza zzt(boolean z) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzd) this.zzhmp).zzq(z);
                    return this;
                }

                /* synthetic */ C0050zza(zzsx zzsx) {
                    this();
                }
            }

            static {
                zzd zzd = new zzd();
                zzbup = zzd;
                zzdrt.zza(zzd.class, zzd);
            }

            private zzd() {
            }

            /* access modifiers changed from: private */
            public final void zzbt(int i) {
                this.zzdl |= 2;
                this.zzbuo = i;
            }

            public static C0050zza zznb() {
                return (C0050zza) zzbup.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zzq(boolean z) {
                this.zzdl |= 1;
                this.zzbun = z;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzsx.zzdk[i - 1]) {
                    case 1:
                        return new zzd();
                    case 2:
                        return new C0050zza((zzsx) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzbup, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0007\u0000\u0002\u000b\u0001", new Object[]{"zzdl", "zzbun", "zzbuo"});
                    case 4:
                        return zzbup;
                    case 5:
                        zzdtn<zzd> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzd.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzbup);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public static final class zze extends zzdrt<zze, C0051zza> implements zzdtg {
            /* access modifiers changed from: private */
            public static final zze zzbus;
            private static volatile zzdtn<zze> zzdz;
            private int zzbuo;
            private boolean zzbuq;
            private boolean zzbur;
            private int zzdl;

            /* renamed from: com.google.android.gms.internal.ads.zzsy$zza$zze$zza  reason: collision with other inner class name */
            public static final class C0051zza extends zzdrt.zzb<zze, C0051zza> implements zzdtg {
                private C0051zza() {
                    super(zze.zzbus);
                }

                public final C0051zza zzbv(int i) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzbt(i);
                    return this;
                }

                public final C0051zza zzu(boolean z) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzr(z);
                    return this;
                }

                public final C0051zza zzv(boolean z) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zze) this.zzhmp).zzs(z);
                    return this;
                }

                /* synthetic */ C0051zza(zzsx zzsx) {
                    this();
                }
            }

            static {
                zze zze = new zze();
                zzbus = zze;
                zzdrt.zza(zze.class, zze);
            }

            private zze() {
            }

            /* access modifiers changed from: private */
            public final void zzbt(int i) {
                this.zzdl |= 4;
                this.zzbuo = i;
            }

            public static C0051zza zznd() {
                return (C0051zza) zzbus.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zzr(boolean z) {
                this.zzdl |= 1;
                this.zzbuq = z;
            }

            /* access modifiers changed from: private */
            public final void zzs(boolean z) {
                this.zzdl |= 2;
                this.zzbur = z;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzsx.zzdk[i - 1]) {
                    case 1:
                        return new zze();
                    case 2:
                        return new C0051zza((zzsx) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzbus, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0007\u0000\u0002\u0007\u0001\u0003\u000b\u0002", new Object[]{"zzdl", "zzbuq", "zzbur", "zzbuo"});
                    case 4:
                        return zzbus;
                    case 5:
                        zzdtn<zze> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zze.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzbus);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        static {
            zza zza = new zza();
            zzbuj = zza;
            zzdrt.zza(zza.class, zza);
        }

        private zza() {
        }

        /* access modifiers changed from: private */
        public final void zza(C0048zza zza) {
            zza.getClass();
            if (!this.zzbui.zzaxp()) {
                this.zzbui = zzdrt.zza(this.zzbui);
            }
            this.zzbui.add(zza);
        }

        public static zzb zzmz() {
            return (zzb) zzbuj.zzazt();
        }

        /* renamed from: com.google.android.gms.internal.ads.zzsy$zza$zza  reason: collision with other inner class name */
        public static final class C0048zza extends zzdrt<C0048zza, C0049zza> implements zzdtg {
            /* access modifiers changed from: private */
            public static final C0048zza zzbuh;
            private static volatile zzdtn<C0048zza> zzdz;
            private int zzbue;
            private zzd zzbuf;
            private zze zzbug;
            private int zzdl;

            /* renamed from: com.google.android.gms.internal.ads.zzsy$zza$zza$zza  reason: collision with other inner class name */
            public static final class C0049zza extends zzdrt.zzb<C0048zza, C0049zza> implements zzdtg {
                private C0049zza() {
                    super(C0048zza.zzbuh);
                }

                public final C0049zza zza(zzd.C0050zza zza) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((C0048zza) this.zzhmp).zza((zzd) zza.zzbaf());
                    return this;
                }

                public final C0049zza zzb(zzc zzc) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((C0048zza) this.zzhmp).zza(zzc);
                    return this;
                }

                /* synthetic */ C0049zza(zzsx zzsx) {
                    this();
                }

                public final C0049zza zza(zze.C0051zza zza) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((C0048zza) this.zzhmp).zza((zze) zza.zzbaf());
                    return this;
                }
            }

            static {
                C0048zza zza = new C0048zza();
                zzbuh = zza;
                zzdrt.zza(C0048zza.class, zza);
            }

            private C0048zza() {
            }

            /* access modifiers changed from: private */
            public final void zza(zzc zzc) {
                this.zzbue = zzc.zzae();
                this.zzdl |= 1;
            }

            public static C0049zza zzmx() {
                return (C0049zza) zzbuh.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zza(zzd zzd) {
                zzd.getClass();
                this.zzbuf = zzd;
                this.zzdl |= 2;
            }

            /* access modifiers changed from: private */
            public final void zza(zze zze) {
                zze.getClass();
                this.zzbug = zze;
                this.zzdl |= 4;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzsx.zzdk[i - 1]) {
                    case 1:
                        return new C0048zza();
                    case 2:
                        return new C0049zza((zzsx) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzbuh, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001\u0003\t\u0002", new Object[]{"zzdl", "zzbue", zzc.zzaf(), "zzbuf", "zzbug"});
                    case 4:
                        return zzbuh;
                    case 5:
                        zzdtn<C0048zza> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (C0048zza.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzbuh);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new zzb((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbuj, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzbui", C0048zza.class});
                case 4:
                    return zzbuj;
                case 5:
                    zzdtn<zza> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zza.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbuj);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzb extends zzdrt<zzb, C0052zzb> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzb zzbuv;
        private static volatile zzdtn<zzb> zzdz;
        private int zzbut;
        private zzm zzbuu;
        private int zzdl;

        public enum zza implements zzdry {
            AD_FORMAT_TYPE_UNSPECIFIED(0),
            BANNER(1),
            INTERSTITIAL(2),
            NATIVE_EXPRESS(3),
            NATIVE_CONTENT(4),
            NATIVE_APP_INSTALL(5),
            NATIVE_CUSTOM_TEMPLATE(6),
            DFP_BANNER(7),
            DFP_INTERSTITIAL(8),
            REWARD_BASED_VIDEO_AD(9),
            BANNER_SEARCH_ADS(10);
            
            private static final zzdrx<zza> zzen = null;
            private final int value;

            static {
                zzen = new zztb();
            }

            private zza(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zztc.zzew;
            }

            public static zza zzbw(int i) {
                switch (i) {
                    case 0:
                        return AD_FORMAT_TYPE_UNSPECIFIED;
                    case 1:
                        return BANNER;
                    case 2:
                        return INTERSTITIAL;
                    case 3:
                        return NATIVE_EXPRESS;
                    case 4:
                        return NATIVE_CONTENT;
                    case 5:
                        return NATIVE_APP_INSTALL;
                    case 6:
                        return NATIVE_CUSTOM_TEMPLATE;
                    case 7:
                        return DFP_BANNER;
                    case 8:
                        return DFP_INTERSTITIAL;
                    case 9:
                        return REWARD_BASED_VIDEO_AD;
                    case 10:
                        return BANNER_SEARCH_ADS;
                    default:
                        return null;
                }
            }

            public final String toString() {
                return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        /* renamed from: com.google.android.gms.internal.ads.zzsy$zzb$zzb  reason: collision with other inner class name */
        public static final class C0052zzb extends zzdrt.zzb<zzb, C0052zzb> implements zzdtg {
            private C0052zzb() {
                super(zzb.zzbuv);
            }

            /* synthetic */ C0052zzb(zzsx zzsx) {
                this();
            }
        }

        static {
            zzb zzb = new zzb();
            zzbuv = zzb;
            zzdrt.zza(zzb.class, zzb);
        }

        private zzb() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new C0052zzb((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbuv, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001", new Object[]{"zzdl", "zzbut", zza.zzaf(), "zzbuu"});
                case 4:
                    return zzbuv;
                case 5:
                    zzdtn<zzb> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzb.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbuv);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzc extends zzdrt<zzc, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzc zzbvl;
        private static volatile zzdtn<zzc> zzdz;
        private String zzbvi = "";
        private zzdsb<zzb> zzbvj = zzdrt.zzazw();
        private int zzbvk;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzc, zza> implements zzdtg {
            private zza() {
                super(zzc.zzbvl);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzc zzc = new zzc();
            zzbvl = zzc;
            zzdrt.zza(zzc.class, zzc);
        }

        private zzc() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbvl, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0001\u0000\u0001\b\u0000\u0002\u001b\u0003\f\u0001", new Object[]{"zzdl", "zzbvi", "zzbvj", zzb.class, "zzbvk", zzte.zzaf()});
                case 4:
                    return zzbvl;
                case 5:
                    zzdtn<zzc> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzc.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbvl);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzd extends zzdrt<zzd, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzd zzbvs;
        private static volatile zzdtn<zzd> zzdz;
        private int zzbvm;
        private zzo zzbvn;
        private zzo zzbvo;
        private zzo zzbvp;
        private zzdsb<zzo> zzbvq = zzdrt.zzazw();
        private int zzbvr;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzd, zza> implements zzdtg {
            private zza() {
                super(zzd.zzbvs);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzd zzd = new zzd();
            zzbvs = zzd;
            zzdrt.zza(zzd.class, zzd);
        }

        private zzd() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzd();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbvs, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0000\u0001\u0004\u0000\u0002\t\u0001\u0003\t\u0002\u0004\t\u0003\u0005\u001b\u0006\u0004\u0004", new Object[]{"zzdl", "zzbvm", "zzbvn", "zzbvo", "zzbvp", "zzbvq", zzo.class, "zzbvr"});
                case 4:
                    return zzbvs;
                case 5:
                    zzdtn<zzd> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzd.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbvs);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zze extends zzdrt<zze, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zze zzbwb;
        private static volatile zzdtn<zze> zzdz;
        private String zzbvx = "";
        private int zzbvy;
        private zzdrz zzbvz = zzdrt.zzazv();
        private zzo zzbwa;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zze, zza> implements zzdtg {
            private zza() {
                super(zze.zzbwb);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zze zze = new zze();
            zzbwb = zze;
            zzdrt.zza(zze.class, zze);
        }

        private zze() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zze();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbwb, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\b\u0000\u0002\f\u0001\u0003\u0016\u0004\t\u0002", new Object[]{"zzdl", "zzbvx", "zzbvy", zzte.zzaf(), "zzbvz", "zzbwa"});
                case 4:
                    return zzbwb;
                case 5:
                    zzdtn<zze> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zze.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbwb);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzf extends zzdrt<zzf, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzf zzbwd;
        private static volatile zzdtn<zzf> zzdz;
        private zzdrz zzbvz = zzdrt.zzazv();
        private int zzbwc;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzf, zza> implements zzdtg {
            private zza() {
                super(zzf.zzbwd);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzf zzf = new zzf();
            zzbwd = zzf;
            zzdrt.zza(zzf.class, zzf);
        }

        private zzf() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzf();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbwd, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\f\u0000\u0002\u0016", new Object[]{"zzdl", "zzbwc", zzte.zzaf(), "zzbvz"});
                case 4:
                    return zzbwd;
                case 5:
                    zzdtn<zzf> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzf.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbwd);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzg extends zzdrt<zzg, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzg zzbwg;
        private static volatile zzdtn<zzg> zzdz;
        private zzo zzbwa;
        private int zzbwc;
        private zze zzbwe;
        private zzdsb<zzn> zzbwf = zzdrt.zzazw();
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzg, zza> implements zzdtg {
            private zza() {
                super(zzg.zzbwg);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzg zzg = new zzg();
            zzbwg = zzg;
            zzdrt.zza(zzg.class, zzg);
        }

        private zzg() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzg();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbwg, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\t\u0000\u0002\u001b\u0003\f\u0001\u0004\t\u0002", new Object[]{"zzdl", "zzbwe", "zzbwf", zzn.class, "zzbwc", zzte.zzaf(), "zzbwa"});
                case 4:
                    return zzbwg;
                case 5:
                    zzdtn<zzg> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzg.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbwg);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzi extends zzdrt<zzi, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzi zzbwu;
        private static volatile zzdtn<zzi> zzdz;
        private int zzbws;
        private zzo zzbwt;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzi, zza> implements zzdtg {
            private zza() {
                super(zzi.zzbwu);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzi zzi = new zzi();
            zzbwu = zzi;
            zzdrt.zza(zzi.class, zzi);
        }

        private zzi() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzi();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbwu, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001", new Object[]{"zzdl", "zzbws", zzte.zzaf(), "zzbwt"});
                case 4:
                    return zzbwu;
                case 5:
                    zzdtn<zzi> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzi.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbwu);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzj extends zzdrt<zzj, zzb> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzj zzbxa;
        private static volatile zzdtn<zzj> zzdz;
        private zzdsb<zza> zzbui = zzdrt.zzazw();
        private int zzbwv;
        private int zzbww;
        private long zzbwx;
        private String zzbwy = "";
        private long zzbwz;
        private int zzdl;
        private String zzdm = "";

        public static final class zzb extends zzdrt.zzb<zzj, zzb> implements zzdtg {
            private zzb() {
                super(zzj.zzbxa);
            }

            public final zzb zzc(Iterable<? extends zza> iterable) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzj) this.zzhmp).zza(iterable);
                return this;
            }

            public final zzb zzca(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzj) this.zzhmp).zzn(str);
                return this;
            }

            public final zzb zzcb(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzj) this.zzhmp).zzbz(str);
                return this;
            }

            public final zzb zzcd(int i) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzj) this.zzhmp).zzca(i);
                return this;
            }

            public final zzb zzce(int i) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzj) this.zzhmp).zzcb(i);
                return this;
            }

            public final zzb zzes(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzj) this.zzhmp).zzeo(j);
                return this;
            }

            public final zzb zzet(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzj) this.zzhmp).zzep(j);
                return this;
            }

            /* synthetic */ zzb(zzsx zzsx) {
                this();
            }
        }

        public enum zzc implements zzdry {
            UNSPECIFIED(0),
            CONNECTING(1),
            CONNECTED(2),
            DISCONNECTING(3),
            DISCONNECTED(4),
            SUSPENDED(5);
            
            private static final zzdrx<zzc> zzen = null;
            private final int value;

            static {
                zzen = new zztl();
            }

            private zzc(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zztm.zzew;
            }

            public static zzc zzcg(int i) {
                if (i == 0) {
                    return UNSPECIFIED;
                }
                if (i == 1) {
                    return CONNECTING;
                }
                if (i == 2) {
                    return CONNECTED;
                }
                if (i == 3) {
                    return DISCONNECTING;
                }
                if (i == 4) {
                    return DISCONNECTED;
                }
                if (i != 5) {
                    return null;
                }
                return SUSPENDED;
            }

            public final String toString() {
                return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        static {
            zzj zzj = new zzj();
            zzbxa = zzj;
            zzdrt.zza(zzj.class, zzj);
        }

        private zzj() {
        }

        /* access modifiers changed from: private */
        public final void zza(Iterable<? extends zza> iterable) {
            if (!this.zzbui.zzaxp()) {
                this.zzbui = zzdrt.zza(this.zzbui);
            }
            zzdqa.zza(iterable, this.zzbui);
        }

        /* access modifiers changed from: private */
        public final void zzbz(String str) {
            str.getClass();
            this.zzdl |= 16;
            this.zzbwy = str;
        }

        /* access modifiers changed from: private */
        public final void zzca(int i) {
            this.zzdl |= 1;
            this.zzbwv = i;
        }

        /* access modifiers changed from: private */
        public final void zzcb(int i) {
            this.zzdl |= 2;
            this.zzbww = i;
        }

        /* access modifiers changed from: private */
        public final void zzeo(long j) {
            this.zzdl |= 4;
            this.zzbwx = j;
        }

        /* access modifiers changed from: private */
        public final void zzep(long j) {
            this.zzdl |= 32;
            this.zzbwz = j;
        }

        /* access modifiers changed from: private */
        public final void zzn(String str) {
            str.getClass();
            this.zzdl |= 8;
            this.zzdm = str;
        }

        public static zzb zzno() {
            return (zzb) zzbxa.zzazt();
        }

        public static final class zza extends zzdrt<zza, C0053zza> implements zzdtg {
            private static final zzdsc<Integer, zzb.zza> zzbxg = new zztk();
            /* access modifiers changed from: private */
            public static final zza zzbxo;
            private static volatile zzdtn<zza> zzdz;
            private long zzbxb;
            private int zzbxc;
            private long zzbxd;
            private long zzbxe;
            private zzdrz zzbxf = zzdrt.zzazv();
            private zzh zzbxh;
            private int zzbxi;
            private int zzbxj;
            private int zzbxk;
            private int zzbxl;
            private int zzbxm;
            private int zzbxn;
            private int zzdl;

            /* renamed from: com.google.android.gms.internal.ads.zzsy$zzj$zza$zza  reason: collision with other inner class name */
            public static final class C0053zza extends zzdrt.zzb<zza, C0053zza> implements zzdtg {
                private C0053zza() {
                    super(zza.zzbxo);
                }

                public final C0053zza zzb(zzh zzh) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zza(zzh);
                    return this;
                }

                public final C0053zza zzcf(int i) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zzcc(i);
                    return this;
                }

                public final C0053zza zzd(Iterable<? extends zzb.zza> iterable) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zzb(iterable);
                    return this;
                }

                public final C0053zza zzeu(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).setTimestamp(j);
                    return this;
                }

                public final C0053zza zzev(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zzeq(j);
                    return this;
                }

                public final C0053zza zzew(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zzer(j);
                    return this;
                }

                public final C0053zza zzf(zzte zzte) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zza(zzte);
                    return this;
                }

                public final C0053zza zzg(zzte zzte) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zzb(zzte);
                    return this;
                }

                public final C0053zza zzh(zzte zzte) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zzc(zzte);
                    return this;
                }

                public final C0053zza zzi(zzte zzte) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zzd(zzte);
                    return this;
                }

                public final C0053zza zzj(zzte zzte) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zze(zzte);
                    return this;
                }

                /* synthetic */ C0053zza(zzsx zzsx) {
                    this();
                }

                public final C0053zza zzb(zzc zzc) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zza) this.zzhmp).zza(zzc);
                    return this;
                }
            }

            static {
                zza zza = new zza();
                zzbxo = zza;
                zzdrt.zza(zza.class, zza);
            }

            private zza() {
            }

            /* access modifiers changed from: private */
            public final void setTimestamp(long j) {
                this.zzdl |= 1;
                this.zzbxb = j;
            }

            /* access modifiers changed from: private */
            public final void zza(zzte zzte) {
                this.zzbxc = zzte.zzae();
                this.zzdl |= 2;
            }

            /* access modifiers changed from: private */
            public final void zzb(Iterable<? extends zzb.zza> iterable) {
                if (!this.zzbxf.zzaxp()) {
                    this.zzbxf = zzdrt.zza(this.zzbxf);
                }
                for (zzb.zza zzae : iterable) {
                    this.zzbxf.zzgl(zzae.zzae());
                }
            }

            /* access modifiers changed from: private */
            public final void zzc(zzte zzte) {
                this.zzbxj = zzte.zzae();
                this.zzdl |= 64;
            }

            /* access modifiers changed from: private */
            public final void zzcc(int i) {
                this.zzdl |= 256;
                this.zzbxl = i;
            }

            /* access modifiers changed from: private */
            public final void zzd(zzte zzte) {
                this.zzbxk = zzte.zzae();
                this.zzdl |= 128;
            }

            /* access modifiers changed from: private */
            public final void zze(zzte zzte) {
                this.zzbxm = zzte.zzae();
                this.zzdl |= AdRequest.MAX_CONTENT_URL_LENGTH;
            }

            /* access modifiers changed from: private */
            public final void zzeq(long j) {
                this.zzdl |= 4;
                this.zzbxd = j;
            }

            /* access modifiers changed from: private */
            public final void zzer(long j) {
                this.zzdl |= 8;
                this.zzbxe = j;
            }

            public static zza zzg(byte[] bArr) throws zzdse {
                return (zza) zzdrt.zza(zzbxo, bArr);
            }

            public static C0053zza zznr() {
                return (C0053zza) zzbxo.zzazt();
            }

            public final long getTimestamp() {
                return this.zzbxb;
            }

            public final zzte zznq() {
                zzte zzbx = zzte.zzbx(this.zzbxc);
                return zzbx == null ? zzte.ENUM_FALSE : zzbx;
            }

            /* access modifiers changed from: private */
            public final void zza(zzh zzh) {
                zzh.getClass();
                this.zzbxh = zzh;
                this.zzdl |= 16;
            }

            /* access modifiers changed from: private */
            public final void zza(zzc zzc) {
                this.zzbxn = zzc.zzae();
                this.zzdl |= ByteConstants.KB;
            }

            /* access modifiers changed from: private */
            public final void zzb(zzte zzte) {
                this.zzbxi = zzte.zzae();
                this.zzdl |= 32;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzsx.zzdk[i - 1]) {
                    case 1:
                        return new zza();
                    case 2:
                        return new C0053zza((zzsx) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzbxo, "\u0001\f\u0000\u0001\u0001\f\f\u0000\u0001\u0000\u0001\u0002\u0000\u0002\f\u0001\u0003\u0002\u0002\u0004\u0002\u0003\u0005\u001e\u0006\t\u0004\u0007\f\u0005\b\f\u0006\t\f\u0007\n\u0004\b\u000b\f\t\f\f\n", new Object[]{"zzdl", "zzbxb", "zzbxc", zzte.zzaf(), "zzbxd", "zzbxe", "zzbxf", zzb.zza.zzaf(), "zzbxh", "zzbxi", zzte.zzaf(), "zzbxj", zzte.zzaf(), "zzbxk", zzte.zzaf(), "zzbxl", "zzbxm", zzte.zzaf(), "zzbxn", zzc.zzaf()});
                    case 4:
                        return zzbxo;
                    case 5:
                        zzdtn<zza> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zza.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzbxo);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzj();
                case 2:
                    return new zzb((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbxa, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0001\u0000\u0001\u001b\u0002\u0004\u0000\u0003\u0004\u0001\u0004\u0002\u0002\u0005\b\u0003\u0006\b\u0004\u0007\u0002\u0005", new Object[]{"zzdl", "zzbui", zza.class, "zzbwv", "zzbww", "zzbwx", "zzdm", "zzbwy", "zzbwz"});
                case 4:
                    return zzbxa;
                case 5:
                    zzdtn<zzj> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzj.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbxa);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzk extends zzdrt<zzk, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzk zzbyh;
        private static volatile zzdtn<zzk> zzdz;
        private int zzbxw = 1000;
        private int zzbxx = 1000;
        private int zzbxy;
        private int zzbxz;
        private int zzbya;
        private int zzbyb;
        private int zzbyc;
        private int zzbyd;
        private int zzbye;
        private int zzbyf;
        private zzl zzbyg;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzk, zza> implements zzdtg {
            private zza() {
                super(zzk.zzbyh);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzk zzk = new zzk();
            zzbyh = zzk;
            zzdrt.zza(zzk.class, zzk);
        }

        private zzk() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzk();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbyh, "\u0001\u000b\u0000\u0001\u0001\u000b\u000b\u0000\u0000\u0000\u0001\f\u0000\u0002\f\u0001\u0003\u0004\u0002\u0004\u0004\u0003\u0005\u0004\u0004\u0006\u0004\u0005\u0007\u0004\u0006\b\u0004\u0007\t\u0004\b\n\u0004\t\u000b\t\n", new Object[]{"zzdl", "zzbxw", zzte.zzaf(), "zzbxx", zzte.zzaf(), "zzbxy", "zzbxz", "zzbya", "zzbyb", "zzbyc", "zzbyd", "zzbye", "zzbyf", "zzbyg"});
                case 4:
                    return zzbyh;
                case 5:
                    zzdtn<zzk> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzk.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbyh);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzl extends zzdrt<zzl, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzl zzbyk;
        private static volatile zzdtn<zzl> zzdz;
        private int zzbyi;
        private int zzbyj;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzl, zza> implements zzdtg {
            private zza() {
                super(zzl.zzbyk);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzl zzl = new zzl();
            zzbyk = zzl;
            zzdrt.zza(zzl.class, zzl);
        }

        private zzl() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzl();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbyk, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0004\u0001", new Object[]{"zzdl", "zzbyi", "zzbyj"});
                case 4:
                    return zzbyk;
                case 5:
                    zzdtn<zzl> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzl.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbyk);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzm extends zzdrt<zzm, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzm zzbyn;
        private static volatile zzdtn<zzm> zzdz;
        private int zzbyl;
        private int zzbym;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzm, zza> implements zzdtg {
            private zza() {
                super(zzm.zzbyn);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzm zzm = new zzm();
            zzbyn = zzm;
            zzdrt.zza(zzm.class, zzm);
        }

        private zzm() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzm();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbyn, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0004\u0001", new Object[]{"zzdl", "zzbyl", "zzbym"});
                case 4:
                    return zzbyn;
                case 5:
                    zzdtn<zzm> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzm.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbyn);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzn extends zzdrt<zzn, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzn zzbyo;
        private static volatile zzdtn<zzn> zzdz;
        private String zzbvx = "";
        private int zzbvy;
        private zzo zzbwa;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzn, zza> implements zzdtg {
            private zza() {
                super(zzn.zzbyo);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzn zzn = new zzn();
            zzbyo = zzn;
            zzdrt.zza(zzn.class, zzn);
        }

        private zzn() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzn();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbyo, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\b\u0000\u0002\f\u0001\u0003\t\u0002", new Object[]{"zzdl", "zzbvx", "zzbvy", zzte.zzaf(), "zzbwa"});
                case 4:
                    return zzbyo;
                case 5:
                    zzdtn<zzn> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzn.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbyo);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzo extends zzdrt<zzo, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzo zzbyr;
        private static volatile zzdtn<zzo> zzdz;
        private int zzbyp;
        private int zzbyq;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzo, zza> implements zzdtg {
            private zza() {
                super(zzo.zzbyr);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzo zzo = new zzo();
            zzbyr = zzo;
            zzdrt.zza(zzo.class, zzo);
        }

        private zzo() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzo();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbyr, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0004\u0001", new Object[]{"zzdl", "zzbyp", "zzbyq"});
                case 4:
                    return zzbyr;
                case 5:
                    zzdtn<zzo> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzo.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbyr);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzp extends zzdrt<zzp, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzp zzbyu;
        private static volatile zzdtn<zzp> zzdz;
        private int zzbwc = 1000;
        private zzq zzbys;
        private zzo zzbyt;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzp, zza> implements zzdtg {
            private zza() {
                super(zzp.zzbyu);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzp zzp = new zzp();
            zzbyu = zzp;
            zzdrt.zza(zzp.class, zzp);
        }

        private zzp() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzp();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbyu, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001\u0003\t\u0002", new Object[]{"zzdl", "zzbwc", zzte.zzaf(), "zzbys", "zzbyt"});
                case 4:
                    return zzbyu;
                case 5:
                    zzdtn<zzp> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzp.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbyu);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzq extends zzdrt<zzq, zzb> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzq zzbyw;
        private static volatile zzdtn<zzq> zzdz;
        private int zzbyv;
        private int zzdl;

        public enum zza implements zzdry {
            VIDEO_ERROR_CODE_UNSPECIFIED(0),
            OPENGL_RENDERING_FAILED(1),
            CACHE_LOAD_FAILED(2),
            ANDROID_TARGET_API_TOO_LOW(3);
            
            private static final zzdrx<zza> zzen = null;
            private final int value;

            static {
                zzen = new zzto();
            }

            private zza(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zztn.zzew;
            }

            public static zza zzch(int i) {
                if (i == 0) {
                    return VIDEO_ERROR_CODE_UNSPECIFIED;
                }
                if (i == 1) {
                    return OPENGL_RENDERING_FAILED;
                }
                if (i == 2) {
                    return CACHE_LOAD_FAILED;
                }
                if (i != 3) {
                    return null;
                }
                return ANDROID_TARGET_API_TOO_LOW;
            }

            public final String toString() {
                return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        public static final class zzb extends zzdrt.zzb<zzq, zzb> implements zzdtg {
            private zzb() {
                super(zzq.zzbyw);
            }

            /* synthetic */ zzb(zzsx zzsx) {
                this();
            }
        }

        static {
            zzq zzq = new zzq();
            zzbyw = zzq;
            zzdrt.zza(zzq.class, zzq);
        }

        private zzq() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzq();
                case 2:
                    return new zzb((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbyw, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001\f\u0000", new Object[]{"zzdl", "zzbyv", zza.zzaf()});
                case 4:
                    return zzbyw;
                case 5:
                    zzdtn<zzq> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzq.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbyw);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzr extends zzdrt<zzr, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzr zzbzf;
        private static volatile zzdtn<zzr> zzdz;
        private int zzbwc = 1000;
        private zzq zzbys;
        private int zzbzc;
        private int zzbzd;
        private int zzbze;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzr, zza> implements zzdtg {
            private zza() {
                super(zzr.zzbzf);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzr zzr = new zzr();
            zzbzf = zzr;
            zzdrt.zza(zzr.class, zzr);
        }

        private zzr() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzr();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbzf, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001\u0003\u0004\u0002\u0004\u0004\u0003\u0005\u0004\u0004", new Object[]{"zzdl", "zzbwc", zzte.zzaf(), "zzbys", "zzbzc", "zzbzd", "zzbze"});
                case 4:
                    return zzbzf;
                case 5:
                    zzdtn<zzr> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzr.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbzf);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzs extends zzdrt<zzs, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzs zzbzg;
        private static volatile zzdtn<zzs> zzdz;
        private int zzbwc = 1000;
        private zzq zzbys;
        private zzo zzbyt;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzs, zza> implements zzdtg {
            private zza() {
                super(zzs.zzbzg);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzs zzs = new zzs();
            zzbzg = zzs;
            zzdrt.zza(zzs.class, zzs);
        }

        private zzs() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzs();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbzg, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001\u0003\t\u0002", new Object[]{"zzdl", "zzbwc", zzte.zzaf(), "zzbys", "zzbyt"});
                case 4:
                    return zzbzg;
                case 5:
                    zzdtn<zzs> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzs.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbzg);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzt extends zzdrt<zzt, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzt zzbzi;
        private static volatile zzdtn<zzt> zzdz;
        private int zzbwc = 1000;
        private zzq zzbys;
        private int zzbzc;
        private int zzbzd;
        private int zzbze;
        private long zzbzh;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzt, zza> implements zzdtg {
            private zza() {
                super(zzt.zzbzi);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzt zzt = new zzt();
            zzbzi = zzt;
            zzdrt.zza(zzt.class, zzt);
        }

        private zzt() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzt();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbzi, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001\u0003\u0004\u0002\u0004\u0004\u0003\u0005\u0004\u0004\u0006\u0003\u0005", new Object[]{"zzdl", "zzbwc", zzte.zzaf(), "zzbys", "zzbzc", "zzbzd", "zzbze", "zzbzh"});
                case 4:
                    return zzbzi;
                case 5:
                    zzdtn<zzt> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzt.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbzi);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzu extends zzdrt<zzu, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzu zzbzj;
        private static volatile zzdtn<zzu> zzdz;
        private int zzbwc = 1000;
        private zzq zzbys;
        private zzo zzbyt;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzu, zza> implements zzdtg {
            private zza() {
                super(zzu.zzbzj);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzu zzu = new zzu();
            zzbzj = zzu;
            zzdrt.zza(zzu.class, zzu);
        }

        private zzu() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzu();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbzj, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001\u0003\t\u0002", new Object[]{"zzdl", "zzbwc", zzte.zzaf(), "zzbys", "zzbyt"});
                case 4:
                    return zzbzj;
                case 5:
                    zzdtn<zzu> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzu.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbzj);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzv extends zzdrt<zzv, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzv zzbzk;
        private static volatile zzdtn<zzv> zzdz;
        private int zzbwc = 1000;
        private zzq zzbys;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzv, zza> implements zzdtg {
            private zza() {
                super(zzv.zzbzk);
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzv zzv = new zzv();
            zzbzk = zzv;
            zzdrt.zza(zzv.class, zzv);
        }

        private zzv() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzv();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbzk, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0000\u0002\t\u0001", new Object[]{"zzdl", "zzbwc", zzte.zzaf(), "zzbys"});
                case 4:
                    return zzbzk;
                case 5:
                    zzdtn<zzv> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzv.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbzk);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzw extends zzdrt<zzw, zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzw zzbzn;
        private static volatile zzdtn<zzw> zzdz;
        private boolean zzbzl;
        private int zzbzm;
        private int zzdl;

        public static final class zza extends zzdrt.zzb<zzw, zza> implements zzdtg {
            private zza() {
                super(zzw.zzbzn);
            }

            public final zza zzci(int i) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzw) this.zzhmp).zzcj(i);
                return this;
            }

            public final boolean zzof() {
                return ((zzw) this.zzhmp).zzof();
            }

            public final zza zzw(boolean z) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzw) this.zzhmp).zzx(z);
                return this;
            }

            /* synthetic */ zza(zzsx zzsx) {
                this();
            }
        }

        static {
            zzw zzw = new zzw();
            zzbzn = zzw;
            zzdrt.zza(zzw.class, zzw);
        }

        private zzw() {
        }

        /* access modifiers changed from: private */
        public final void zzcj(int i) {
            this.zzdl |= 2;
            this.zzbzm = i;
        }

        public static zza zzog() {
            return (zza) zzbzn.zzazt();
        }

        /* access modifiers changed from: private */
        public final void zzx(boolean z) {
            this.zzdl |= 1;
            this.zzbzl = z;
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzw();
                case 2:
                    return new zza((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbzn, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0007\u0000\u0002\u0004\u0001", new Object[]{"zzdl", "zzbzl", "zzbzm"});
                case 4:
                    return zzbzn;
                case 5:
                    zzdtn<zzw> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzw.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbzn);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final boolean zzof() {
            return this.zzbzl;
        }
    }

    public static final class zzh extends zzdrt<zzh, zzb> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zzh zzbwi;
        private static volatile zzdtn<zzh> zzdz;
        private int zzbut;
        private int zzbwh;
        private int zzdl;

        public enum zza implements zzdry {
            CELLULAR_NETWORK_TYPE_UNSPECIFIED(0),
            TWO_G(1),
            THREE_G(2),
            LTE(4);
            
            private static final zzdrx<zza> zzen = null;
            private final int value;

            static {
                zzen = new zzth();
            }

            private zza(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zztg.zzew;
            }

            public static zza zzby(int i) {
                if (i == 0) {
                    return CELLULAR_NETWORK_TYPE_UNSPECIFIED;
                }
                if (i == 1) {
                    return TWO_G;
                }
                if (i == 2) {
                    return THREE_G;
                }
                if (i != 4) {
                    return null;
                }
                return LTE;
            }

            public final String toString() {
                return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        public static final class zzb extends zzdrt.zzb<zzh, zzb> implements zzdtg {
            private zzb() {
                super(zzh.zzbwi);
            }

            public final zzb zzb(zzc zzc) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzh) this.zzhmp).zza(zzc);
                return this;
            }

            /* synthetic */ zzb(zzsx zzsx) {
                this();
            }

            public final zzb zzb(zza zza) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zzh) this.zzhmp).zza(zza);
                return this;
            }
        }

        public enum zzc implements zzdry {
            NETWORKTYPE_UNSPECIFIED(0),
            CELL(1),
            WIFI(2);
            
            private static final zzdrx<zzc> zzen = null;
            private final int value;

            static {
                zzen = new zzti();
            }

            private zzc(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zztj.zzew;
            }

            public static zzc zzbz(int i) {
                if (i == 0) {
                    return NETWORKTYPE_UNSPECIFIED;
                }
                if (i == 1) {
                    return CELL;
                }
                if (i != 2) {
                    return null;
                }
                return WIFI;
            }

            public final String toString() {
                return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        static {
            zzh zzh = new zzh();
            zzbwi = zzh;
            zzdrt.zza(zzh.class, zzh);
        }

        private zzh() {
        }

        /* access modifiers changed from: private */
        public final void zza(zzc zzc2) {
            this.zzbut = zzc2.zzae();
            this.zzdl |= 1;
        }

        public static zzb zznl() {
            return (zzb) zzbwi.zzazt();
        }

        /* access modifiers changed from: private */
        public final void zza(zza zza2) {
            this.zzbwh = zza2.zzae();
            this.zzdl |= 2;
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzsx.zzdk[i - 1]) {
                case 1:
                    return new zzh();
                case 2:
                    return new zzb((zzsx) null);
                case 3:
                    return zzdrt.zza((zzdte) zzbwi, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0000\u0002\f\u0001", new Object[]{"zzdl", "zzbut", zzc.zzaf(), "zzbwh", zza.zzaf()});
                case 4:
                    return zzbwi;
                case 5:
                    zzdtn<zzh> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzh.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzbwi);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
