package com.google.android.gms.internal.ads;

public final class zzbuq implements zzdxg<zzbun> {
    private final zzdxp<zzbqj> zzesu;
    private final zzdxp<zzbsq> zzeys;

    private zzbuq(zzdxp<zzbqj> zzdxp, zzdxp<zzbsq> zzdxp2) {
        this.zzesu = zzdxp;
        this.zzeys = zzdxp2;
    }

    public static zzbuq zzj(zzdxp<zzbqj> zzdxp, zzdxp<zzbsq> zzdxp2) {
        return new zzbuq(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzbun(this.zzesu.get(), this.zzeys.get());
    }
}
