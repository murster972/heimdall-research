package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutorService;

public interface zzddr {
    ExecutorService zzdt(int i);

    ExecutorService zzdu(int i);
}
