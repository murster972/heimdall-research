package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;

final /* synthetic */ class zzbtn implements zzbrn {
    static final zzbrn zzfhp = new zzbtn();

    private zzbtn() {
    }

    public final void zzp(Object obj) {
        ((VideoController.VideoLifecycleCallbacks) obj).onVideoStart();
    }
}
