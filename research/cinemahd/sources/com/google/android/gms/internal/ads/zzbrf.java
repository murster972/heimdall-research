package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;
import java.util.Set;

public final class zzbrf extends zzbrl<zzbri> implements zzbri {
    public zzbrf(Set<zzbsu<zzbri>> set) {
        super(set);
    }

    public final void zza(zzsy.zza zza) {
        zza(new zzbre(zza));
    }

    public final void zzb(zzsy.zza zza) {
        zza(new zzbrh(zza));
    }

    public final void zzc(zzsy.zza zza) {
        zza(new zzbrg(zza));
    }
}
