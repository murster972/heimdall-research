package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class zzdgo<V> extends zzder implements Future<V> {
    protected zzdgo() {
    }

    public boolean cancel(boolean z) {
        return zzaqw().cancel(z);
    }

    public V get() throws InterruptedException, ExecutionException {
        return zzaqw().get();
    }

    public boolean isCancelled() {
        return zzaqw().isCancelled();
    }

    public boolean isDone() {
        return zzaqw().isDone();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzaru */
    public abstract Future<? extends V> zzaqw();

    public V get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return zzaqw().get(j, timeUnit);
    }
}
