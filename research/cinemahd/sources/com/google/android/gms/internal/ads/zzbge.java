package com.google.android.gms.internal.ads;

public final class zzbge implements zzdxg<zzcis<zzdac, zzcjy>> {
    private final zzbga zzejr;
    private final zzdxp<zzcka> zzejs;

    public zzbge(zzbga zzbga, zzdxp<zzcka> zzdxp) {
        this.zzejr = zzbga;
        this.zzejs = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzcis) zzdxm.zza(new zzcks(this.zzejs.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
