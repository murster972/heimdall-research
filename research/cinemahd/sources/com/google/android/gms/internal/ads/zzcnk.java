package com.google.android.gms.internal.ads;

import android.view.View;
import com.google.android.gms.ads.internal.zze;
import java.util.concurrent.atomic.AtomicBoolean;

public final class zzcnk implements zze {
    private final zzbsy zzfeb;
    private final zzbpd zzfkd;
    private final zzboq zzfke;
    private final zzbjd zzfkf;
    private final zzbst zzfkh;
    private AtomicBoolean zzgbq = new AtomicBoolean(false);

    zzcnk(zzboq zzboq, zzbpd zzbpd, zzbsy zzbsy, zzbst zzbst, zzbjd zzbjd) {
        this.zzfke = zzboq;
        this.zzfkd = zzbpd;
        this.zzfeb = zzbsy;
        this.zzfkh = zzbst;
        this.zzfkf = zzbjd;
    }

    public final synchronized void zzg(View view) {
        if (this.zzgbq.compareAndSet(false, true)) {
            this.zzfkf.onAdImpression();
            this.zzfkh.zzq(view);
        }
    }

    public final void zzjr() {
        if (this.zzgbq.get()) {
            this.zzfke.onAdClicked();
        }
    }

    public final void zzjs() {
        if (this.zzgbq.get()) {
            this.zzfkd.onAdImpression();
            this.zzfeb.zzaia();
        }
    }
}
