package com.google.android.gms.internal.ads;

public final class zzchx implements zzbow, zzbqb {
    private static final Object zzfwy = new Object();
    private static int zzfwz;
    private final zzcid zzfxa;

    public zzchx(zzcid zzcid) {
        this.zzfxa = zzcid;
    }

    private static void zzalw() {
        synchronized (zzfwy) {
            zzfwz++;
        }
    }

    private static boolean zzalx() {
        boolean z;
        synchronized (zzfwy) {
            z = zzfwz < ((Integer) zzve.zzoy().zzd(zzzn.zzcqg)).intValue();
        }
        return z;
    }

    public final void onAdFailedToLoad(int i) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzalx()) {
            this.zzfxa.zzbj(false);
            zzalw();
        }
    }

    public final void onAdLoaded() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzalx()) {
            this.zzfxa.zzbj(true);
            zzalw();
        }
    }
}
