package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.AdSize;
import java.util.ArrayList;
import java.util.List;

public final class zzczy {
    public static zzczk zza(List<zzczk> list, zzczk zzczk) {
        return list.get(0);
    }

    public static zzczk zze(zzuj zzuj) {
        if (zzuj.zzccq) {
            return new zzczk(-3, 0, true);
        }
        return new zzczk(zzuj.width, zzuj.height, false);
    }

    public static zzuj zza(Context context, List<zzczk> list) {
        ArrayList arrayList = new ArrayList();
        for (zzczk next : list) {
            if (next.zzglh) {
                arrayList.add(AdSize.FLUID);
            } else {
                arrayList.add(new AdSize(next.width, next.height));
            }
        }
        return new zzuj(context, (AdSize[]) arrayList.toArray(new AdSize[arrayList.size()]));
    }
}
