package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdjg extends zzdii<zzdlj> {
    zzdjg() {
        super(zzdlj.class, new zzdjf(zzdpi.class));
    }

    /* access modifiers changed from: private */
    public static void zza(zzdln zzdln) throws GeneralSecurityException {
        if (zzdln.zzatn() < 12 || zzdln.zzatn() > 16) {
            throw new GeneralSecurityException("invalid IV size");
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesCtrKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    public final zzdih<zzdlk, zzdlj> zzasg() {
        return new zzdji(this, zzdlk.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdlj zzdlj = (zzdlj) zzdte;
        zzdpo.zzx(zzdlj.getVersion(), 0);
        zzdpo.zzez(zzdlj.zzass().size());
        zza(zzdlj.zzath());
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdlj.zzy(zzdqk);
    }
}
