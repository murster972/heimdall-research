package com.google.android.gms.internal.ads;

final class zzbad implements Runnable {
    private final /* synthetic */ zzazx zzdxn;
    private final /* synthetic */ int zzdxq;
    private final /* synthetic */ int zzdxr;

    zzbad(zzazx zzazx, int i, int i2) {
        this.zzdxn = zzazx;
        this.zzdxq = i;
        this.zzdxr = i2;
    }

    public final void run() {
        if (this.zzdxn.zzdxm != null) {
            this.zzdxn.zzdxm.zzk(this.zzdxq, this.zzdxr);
        }
    }
}
