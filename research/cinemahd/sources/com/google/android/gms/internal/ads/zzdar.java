package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzdar implements zzdxg<zzdam> {
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<Context> zzfhb;
    private final zzdxp<zzave> zzgev;

    public zzdar(zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzave> zzdxp3) {
        this.zzfhb = zzdxp;
        this.zzfdb = zzdxp2;
        this.zzgev = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzdam(this.zzfhb.get(), this.zzfdb.get(), this.zzgev.get());
    }
}
