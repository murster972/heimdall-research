package com.google.android.gms.internal.ads;

import java.util.Arrays;

public class zzmt implements zzne {
    private final int length;
    private int zzafx;
    private final zzgw[] zzbbx;
    private final zzms zzbdf;
    private final int[] zzbdg;
    private final long[] zzbdh;

    public zzmt(zzms zzms, int... iArr) {
        int i = 0;
        zzoc.checkState(iArr.length > 0);
        this.zzbdf = (zzms) zzoc.checkNotNull(zzms);
        this.length = iArr.length;
        this.zzbbx = new zzgw[this.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            this.zzbbx[i2] = zzms.zzaw(iArr[i2]);
        }
        Arrays.sort(this.zzbbx, new zzmv());
        this.zzbdg = new int[this.length];
        while (true) {
            int i3 = this.length;
            if (i < i3) {
                this.zzbdg[i] = zzms.zzh(this.zzbbx[i]);
                i++;
            } else {
                this.zzbdh = new long[i3];
                return;
            }
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzmt zzmt = (zzmt) obj;
            return this.zzbdf == zzmt.zzbdf && Arrays.equals(this.zzbdg, zzmt.zzbdg);
        }
    }

    public int hashCode() {
        if (this.zzafx == 0) {
            this.zzafx = (System.identityHashCode(this.zzbdf) * 31) + Arrays.hashCode(this.zzbdg);
        }
        return this.zzafx;
    }

    public final int length() {
        return this.zzbdg.length;
    }

    public final zzgw zzaw(int i) {
        return this.zzbbx[i];
    }

    public final int zzax(int i) {
        return this.zzbdg[0];
    }

    public final zzms zzid() {
        return this.zzbdf;
    }
}
