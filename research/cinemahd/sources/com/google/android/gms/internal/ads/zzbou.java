package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbou extends zzbrl<zzbow> implements zzbow {
    public zzbou(Set<zzbsu<zzbow>> set) {
        super(set);
    }

    public final void onAdFailedToLoad(int i) {
        zza(new zzbox(i));
    }
}
