package com.google.android.gms.internal.ads;

public final class zzbku implements zzdxg<zzczk> {
    private final zzbkn zzfen;

    public zzbku(zzbkn zzbkn) {
        this.zzfen = zzbkn;
    }

    public static zzczk zzc(zzbkn zzbkn) {
        return (zzczk) zzdxm.zza(zzbkn.zzagi(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzc(this.zzfen);
    }
}
