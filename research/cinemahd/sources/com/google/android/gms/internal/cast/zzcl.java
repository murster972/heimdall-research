package com.google.android.gms.internal.cast;

import android.app.PendingIntent;
import com.google.android.gms.common.api.Status;
import java.util.Locale;

final class zzcl implements zzec {
    private final /* synthetic */ zzcb zzwo;
    private final /* synthetic */ zzck zzws;

    zzcl(zzck zzck, zzcb zzcb) {
        this.zzws = zzck;
        this.zzwo = zzcb;
    }

    public final void zza(long j, int i, Object obj) {
        if (obj == null) {
            try {
                this.zzws.setResult(new zzcm(new Status(i, (String) null, (PendingIntent) null), this.zzws.zzwr));
            } catch (ClassCastException unused) {
                this.zzws.setResult(zzck.zzc(new Status(13)));
            }
        } else {
            zzcp zzcp = (zzcp) obj;
            zzco zzco = zzcp.zzxk;
            if (zzco == null || zzdk.zza("1.0.0", zzco.getVersion())) {
                this.zzws.setResult(new zzcm(new Status(i, zzcp.zzxa, (PendingIntent) null), this.zzws.zzwr));
                return;
            }
            zzco unused2 = this.zzws.zzwi.zzvy = null;
            this.zzws.setResult(zzck.zzc(new Status(2150, String.format(Locale.ROOT, "Incorrect Game Manager SDK version. Receiver: %s Sender: %s", new Object[]{zzco.getVersion(), "1.0.0"}))));
        }
    }

    public final void zzd(long j) {
        this.zzws.setResult(zzck.zzc(new Status(2103)));
    }
}
