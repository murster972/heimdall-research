package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcrc implements zzcty<Bundle> {
    private final float zzdje;
    private final int zzdmh;
    private final boolean zzdmi;
    private final boolean zzdmj;
    private final int zzdmk;
    private final int zzdml;
    private final int zzdmm;
    private final boolean zzgfj;

    public zzcrc(int i, boolean z, boolean z2, int i2, int i3, int i4, float f, boolean z3) {
        this.zzdmh = i;
        this.zzdmi = z;
        this.zzdmj = z2;
        this.zzdmk = i2;
        this.zzdml = i3;
        this.zzdmm = i4;
        this.zzdje = f;
        this.zzgfj = z3;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        bundle.putInt("am", this.zzdmh);
        bundle.putBoolean("ma", this.zzdmi);
        bundle.putBoolean("sp", this.zzdmj);
        bundle.putInt("muv", this.zzdmk);
        bundle.putInt("rm", this.zzdml);
        bundle.putInt("riv", this.zzdmm);
        bundle.putFloat("android_app_volume", this.zzdje);
        bundle.putBoolean("android_app_muted", this.zzgfj);
    }
}
