package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzbxx implements zzbeu {
    private final Map zzdvw;
    private final zzbxr zzfof;

    zzbxx(zzbxr zzbxr, Map map) {
        this.zzfof = zzbxr;
        this.zzdvw = map;
    }

    public final void zzak(boolean z) {
        this.zzfof.zza(this.zzdvw, z);
    }
}
