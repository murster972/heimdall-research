package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public class zzbww {
    protected final zzczl zzfmp;

    public zzbww(zzczl zzczl) {
        this.zzfmp = zzczl;
    }

    public boolean zzaiw() {
        return this.zzfmp.zzdli;
    }

    public JSONObject zzajl() {
        return null;
    }

    public boolean zzajm() {
        return false;
    }

    public boolean zzajn() {
        return this.zzfmp.zzdce;
    }

    public boolean zzajo() {
        return this.zzfmp.zzdcd;
    }
}
