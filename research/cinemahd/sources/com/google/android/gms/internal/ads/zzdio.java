package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public interface zzdio {
    byte[] zzl(byte[] bArr) throws GeneralSecurityException;
}
