package com.google.android.gms.internal.ads;

import android.os.ParcelFileDescriptor;

public final class zzcgj extends zzaqh {
    private final /* synthetic */ zzcgk zzfvp;

    protected zzcgj(zzcgk zzcgk) {
        this.zzfvp = zzcgk;
    }

    public final void zza(zzaxc zzaxc) {
        this.zzfvp.zzdbf.setException(new zzaxf(zzaxc.zzdtr, zzaxc.errorCode));
    }

    public final void zzb(ParcelFileDescriptor parcelFileDescriptor) {
        this.zzfvp.zzdbf.set(new ParcelFileDescriptor.AutoCloseInputStream(parcelFileDescriptor));
    }
}
