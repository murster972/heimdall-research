package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzvm extends IInterface {
    String getMediationAdapterClassName() throws RemoteException;

    boolean isLoading() throws RemoteException;

    void zza(zzug zzug, int i) throws RemoteException;

    void zzb(zzug zzug) throws RemoteException;

    String zzka() throws RemoteException;
}
