package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfc extends zzfw {
    private static volatile Long zzzr;
    private static final Object zzzs = new Object();

    public zzfc(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 44);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (zzzr == null) {
            synchronized (zzzs) {
                if (zzzr == null) {
                    zzzr = (Long) this.zzaae.invoke((Object) null, new Object[0]);
                }
            }
        }
        synchronized (this.zzzt) {
            this.zzzt.zzbh(zzzr.longValue());
        }
    }
}
