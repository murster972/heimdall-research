package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcmq implements zzbex {
    private final zzbdi zzehp;

    private zzcmq(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static zzbex zzq(zzbdi zzbdi) {
        return new zzcmq(zzbdi);
    }

    public final void zzsb() {
        this.zzehp.zztr();
    }
}
