package com.google.android.gms.internal.ads;

public final class zzcdj implements zzbow, zzbqb, zzbqx {
    private final zzcds zzfsm;
    private final zzcdv zzfsn;

    public zzcdj(zzcds zzcds, zzcdv zzcdv) {
        this.zzfsm = zzcds;
        this.zzfsn = zzcdv;
    }

    public final void onAdFailedToLoad(int i) {
        this.zzfsn.zzm(this.zzfsm.zzqu());
    }

    public final void onAdLoaded() {
        this.zzfsn.zzm(this.zzfsm.zzqu());
    }

    public final void zzb(zzaqk zzaqk) {
        this.zzfsm.zzi(zzaqk.zzdlu);
    }

    public final void zzb(zzczt zzczt) {
        this.zzfsm.zzc(zzczt);
    }
}
