package com.google.android.gms.internal.ads;

import java.util.Set;
import java.util.concurrent.Executor;

public final class zzcei implements zzdxg<Set<zzbsu<zzbqb>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzceo> zzfsb;
    private final zzcee zzfth;

    private zzcei(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfth = zzcee;
        this.zzfsb = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static Set<zzbsu<zzbqb>> zzb(zzcee zzcee, zzceo zzceo, Executor executor) {
        return (Set) zzdxm.zza(zzcee.zzc(zzceo, executor), "Cannot return null from a non-@Nullable @Provides method");
    }

    public static zzcei zzd(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcei(zzcee, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return zzb(this.zzfth, this.zzfsb.get(), this.zzfcv.get());
    }
}
