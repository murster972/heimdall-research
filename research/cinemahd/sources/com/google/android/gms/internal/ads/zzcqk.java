package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzcqk implements zzdxg<zzcqi> {
    private final zzdxp<Set<String>> zzgew;

    private zzcqk(zzdxp<Set<String>> zzdxp) {
        this.zzgew = zzdxp;
    }

    public static zzcqk zzai(zzdxp<Set<String>> zzdxp) {
        return new zzcqk(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcqi(this.zzgew.get());
    }
}
