package com.google.android.gms.internal.ads;

import com.facebook.react.uimanager.ViewProps;
import java.util.Map;

final class zzafm implements zzafn<zzbdi> {
    zzafm() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (map.keySet().contains(ViewProps.START)) {
            zzbdi.zzaaa().zzaba();
        } else if (map.keySet().contains("stop")) {
            zzbdi.zzaaa().zzabb();
        } else if (map.keySet().contains("cancel")) {
            zzbdi.zzaaa().zzabc();
        }
    }
}
