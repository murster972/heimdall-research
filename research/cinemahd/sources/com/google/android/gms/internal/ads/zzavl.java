package com.google.android.gms.internal.ads;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.gms.ads.AdActivity;

public final class zzavl {
    private final Object lock = new Object();
    private long zzdre = -1;
    private long zzdrf = -1;
    private int zzdrg = -1;
    int zzdrh = -1;
    private long zzdri = 0;
    private final String zzdrj;
    private final zzavu zzdrk;
    private int zzdrl = 0;
    private int zzdrm = 0;

    public zzavl(String str, zzavu zzavu) {
        this.zzdrj = str;
        this.zzdrk = zzavu;
    }

    private static boolean zzam(Context context) {
        Context zzaa = zzarf.zzaa(context);
        int identifier = zzaa.getResources().getIdentifier("Theme.Translucent", "style", "android");
        if (identifier == 0) {
            zzayu.zzey("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        }
        try {
            if (identifier == zzaa.getPackageManager().getActivityInfo(new ComponentName(zzaa.getPackageName(), AdActivity.CLASS_NAME), 0).theme) {
                return true;
            }
            zzayu.zzey("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            zzayu.zzez("Fail to fetch AdActivity theme");
            zzayu.zzey("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(com.google.android.gms.internal.ads.zzug r11, long r12) {
        /*
            r10 = this;
            java.lang.Object r0 = r10.lock
            monitor-enter(r0)
            com.google.android.gms.internal.ads.zzavu r1 = r10.zzdrk     // Catch:{ all -> 0x007b }
            long r1 = r1.zzwb()     // Catch:{ all -> 0x007b }
            com.google.android.gms.common.util.Clock r3 = com.google.android.gms.ads.internal.zzq.zzkx()     // Catch:{ all -> 0x007b }
            long r3 = r3.b()     // Catch:{ all -> 0x007b }
            long r5 = r10.zzdrf     // Catch:{ all -> 0x007b }
            r7 = -1
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 != 0) goto L_0x0042
            long r1 = r3 - r1
            com.google.android.gms.internal.ads.zzzc<java.lang.Long> r5 = com.google.android.gms.internal.ads.zzzn.zzcjc     // Catch:{ all -> 0x007b }
            com.google.android.gms.internal.ads.zzzj r6 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x007b }
            java.lang.Object r5 = r6.zzd(r5)     // Catch:{ all -> 0x007b }
            java.lang.Long r5 = (java.lang.Long) r5     // Catch:{ all -> 0x007b }
            long r5 = r5.longValue()     // Catch:{ all -> 0x007b }
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x0033
            r1 = -1
            r10.zzdrh = r1     // Catch:{ all -> 0x007b }
            goto L_0x003b
        L_0x0033:
            com.google.android.gms.internal.ads.zzavu r1 = r10.zzdrk     // Catch:{ all -> 0x007b }
            int r1 = r1.zzwc()     // Catch:{ all -> 0x007b }
            r10.zzdrh = r1     // Catch:{ all -> 0x007b }
        L_0x003b:
            r10.zzdrf = r12     // Catch:{ all -> 0x007b }
            long r12 = r10.zzdrf     // Catch:{ all -> 0x007b }
            r10.zzdre = r12     // Catch:{ all -> 0x007b }
            goto L_0x0044
        L_0x0042:
            r10.zzdre = r12     // Catch:{ all -> 0x007b }
        L_0x0044:
            r12 = 1
            if (r11 == 0) goto L_0x0058
            android.os.Bundle r13 = r11.extras     // Catch:{ all -> 0x007b }
            if (r13 == 0) goto L_0x0058
            android.os.Bundle r11 = r11.extras     // Catch:{ all -> 0x007b }
            java.lang.String r13 = "gw"
            r1 = 2
            int r11 = r11.getInt(r13, r1)     // Catch:{ all -> 0x007b }
            if (r11 != r12) goto L_0x0058
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            return
        L_0x0058:
            int r11 = r10.zzdrg     // Catch:{ all -> 0x007b }
            int r11 = r11 + r12
            r10.zzdrg = r11     // Catch:{ all -> 0x007b }
            int r11 = r10.zzdrh     // Catch:{ all -> 0x007b }
            int r11 = r11 + r12
            r10.zzdrh = r11     // Catch:{ all -> 0x007b }
            int r11 = r10.zzdrh     // Catch:{ all -> 0x007b }
            if (r11 != 0) goto L_0x0070
            r11 = 0
            r10.zzdri = r11     // Catch:{ all -> 0x007b }
            com.google.android.gms.internal.ads.zzavu r11 = r10.zzdrk     // Catch:{ all -> 0x007b }
            r11.zzfa(r3)     // Catch:{ all -> 0x007b }
            goto L_0x0079
        L_0x0070:
            com.google.android.gms.internal.ads.zzavu r11 = r10.zzdrk     // Catch:{ all -> 0x007b }
            long r11 = r11.zzwd()     // Catch:{ all -> 0x007b }
            long r3 = r3 - r11
            r10.zzdri = r3     // Catch:{ all -> 0x007b }
        L_0x0079:
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            return
        L_0x007b:
            r11 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzavl.zza(com.google.android.gms.internal.ads.zzug, long):void");
    }

    public final Bundle zzo(Context context, String str) {
        Bundle bundle;
        synchronized (this.lock) {
            bundle = new Bundle();
            bundle.putString("session_id", this.zzdrj);
            bundle.putLong("basets", this.zzdrf);
            bundle.putLong("currts", this.zzdre);
            bundle.putString("seq_num", str);
            bundle.putInt("preqs", this.zzdrg);
            bundle.putInt("preqs_in_session", this.zzdrh);
            bundle.putLong("time_in_session", this.zzdri);
            bundle.putInt("pclick", this.zzdrl);
            bundle.putInt("pimp", this.zzdrm);
            bundle.putBoolean("support_transparent_background", zzam(context));
        }
        return bundle;
    }

    public final void zzuv() {
        synchronized (this.lock) {
            this.zzdrm++;
        }
    }

    public final void zzuw() {
        synchronized (this.lock) {
            this.zzdrl++;
        }
    }
}
