package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.HashMap;
import java.util.concurrent.Executor;

public final class zzchm implements zzdxg<zzcgw> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzarb> zzfwo;
    private final zzdxp<zzbij> zzfwp;
    private final zzdxp<zzaqy> zzfwq;
    private final zzdxp<HashMap<String, zzchh>> zzfwr;

    private zzchm(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzarb> zzdxp3, zzdxp<zzbij> zzdxp4, zzdxp<zzaqy> zzdxp5, zzdxp<HashMap<String, zzchh>> zzdxp6) {
        this.zzejv = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfwo = zzdxp3;
        this.zzfwp = zzdxp4;
        this.zzfwq = zzdxp5;
        this.zzfwr = zzdxp6;
    }

    public static zzchm zzc(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzarb> zzdxp3, zzdxp<zzbij> zzdxp4, zzdxp<zzaqy> zzdxp5, zzdxp<HashMap<String, zzchh>> zzdxp6) {
        return new zzchm(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6);
    }

    public final /* synthetic */ Object get() {
        return new zzcgw(this.zzejv.get(), this.zzfei.get(), this.zzfwo.get(), this.zzfwp.get(), this.zzfwq.get(), this.zzfwr.get());
    }
}
