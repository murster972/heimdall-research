package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.io.InputStream;

final class zzdtx extends InputStream {
    private int mark;
    private final /* synthetic */ zzdtt zzhqd;
    private zzdty zzhqe;
    private zzdqr zzhqf;
    private int zzhqg;
    private int zzhqh;
    private int zzhqi;

    public zzdtx(zzdtt zzdtt) {
        this.zzhqd = zzdtt;
        initialize();
    }

    private final void initialize() {
        this.zzhqe = new zzdty(this.zzhqd, (zzdtw) null);
        this.zzhqf = (zzdqr) this.zzhqe.next();
        this.zzhqg = this.zzhqf.size();
        this.zzhqh = 0;
        this.zzhqi = 0;
    }

    private final void zzbbr() {
        int i;
        if (this.zzhqf != null && this.zzhqh == (i = this.zzhqg)) {
            this.zzhqi += i;
            this.zzhqh = 0;
            if (this.zzhqe.hasNext()) {
                this.zzhqf = (zzdqr) this.zzhqe.next();
                this.zzhqg = this.zzhqf.size();
                return;
            }
            this.zzhqf = null;
            this.zzhqg = 0;
        }
    }

    private final int zzl(byte[] bArr, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        while (true) {
            if (i4 <= 0) {
                break;
            }
            zzbbr();
            if (this.zzhqf != null) {
                int min = Math.min(this.zzhqg - this.zzhqh, i4);
                if (bArr != null) {
                    this.zzhqf.zza(bArr, this.zzhqh, i3, min);
                    i3 += min;
                }
                this.zzhqh += min;
                i4 -= min;
            } else if (i4 == i2) {
                return -1;
            }
        }
        return i2 - i4;
    }

    public final int available() throws IOException {
        return this.zzhqd.size() - (this.zzhqi + this.zzhqh);
    }

    public final void mark(int i) {
        this.mark = this.zzhqi + this.zzhqh;
    }

    public final boolean markSupported() {
        return true;
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new NullPointerException();
        } else if (i >= 0 && i2 >= 0 && i2 <= bArr.length - i) {
            return zzl(bArr, i, i2);
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public final synchronized void reset() {
        initialize();
        zzl((byte[]) null, 0, this.mark);
    }

    public final long skip(long j) {
        if (j >= 0) {
            if (j > 2147483647L) {
                j = 2147483647L;
            }
            return (long) zzl((byte[]) null, 0, (int) j);
        }
        throw new IndexOutOfBoundsException();
    }

    public final int read() throws IOException {
        zzbbr();
        zzdqr zzdqr = this.zzhqf;
        if (zzdqr == null) {
            return -1;
        }
        int i = this.zzhqh;
        this.zzhqh = i + 1;
        return zzdqr.zzfe(i) & 255;
    }
}
