package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Callable;

final /* synthetic */ class zzauh implements Callable {
    private final Context zzcey;
    private final zzatv zzdps;

    zzauh(zzatv zzatv, Context context) {
        this.zzdps = zzatv;
        this.zzcey = context;
    }

    public final Object call() {
        return this.zzdps.zzaj(this.zzcey);
    }
}
