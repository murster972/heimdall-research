package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzbec implements Runnable {
    private final Map zzdvw;
    private final zzbed zzehe;

    zzbec(zzbed zzbed, Map map) {
        this.zzehe = zzbed;
        this.zzdvw = map;
    }

    public final void run() {
        this.zzehe.zzj(this.zzdvw);
    }
}
