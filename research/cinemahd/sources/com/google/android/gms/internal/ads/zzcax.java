package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;

public final class zzcax extends VideoController.VideoLifecycleCallbacks {
    private final zzbws zzfkc;

    public zzcax(zzbws zzbws) {
        this.zzfkc = zzbws;
    }

    private static zzxg zza(zzbws zzbws) {
        zzxb videoController = zzbws.getVideoController();
        if (videoController == null) {
            return null;
        }
        try {
            return videoController.zzpm();
        } catch (RemoteException unused) {
            return null;
        }
    }

    public final void onVideoEnd() {
        zzxg zza = zza(this.zzfkc);
        if (zza != null) {
            try {
                zza.onVideoEnd();
            } catch (RemoteException e) {
                zzayu.zzd("Unable to call onVideoEnd()", e);
            }
        }
    }

    public final void onVideoPause() {
        zzxg zza = zza(this.zzfkc);
        if (zza != null) {
            try {
                zza.onVideoPause();
            } catch (RemoteException e) {
                zzayu.zzd("Unable to call onVideoEnd()", e);
            }
        }
    }

    public final void onVideoStart() {
        zzxg zza = zza(this.zzfkc);
        if (zza != null) {
            try {
                zza.onVideoStart();
            } catch (RemoteException e) {
                zzayu.zzd("Unable to call onVideoEnd()", e);
            }
        }
    }
}
