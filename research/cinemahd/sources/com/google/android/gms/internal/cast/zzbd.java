package com.google.android.gms.internal.cast;

import android.content.Context;
import android.widget.ImageView;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbd extends UIController {
    private Cast.Listener zzak;
    private final Context zzhe;
    private final ImageView zzsi;
    private final String zzsq = this.zzhe.getString(R$string.cast_mute);
    private final String zzsr = this.zzhe.getString(R$string.cast_unmute);

    public zzbd(ImageView imageView, Context context) {
        this.zzsi = imageView;
        this.zzhe = context.getApplicationContext();
        this.zzsi.setEnabled(false);
        this.zzak = null;
    }

    private final void zzi(boolean z) {
        this.zzsi.setSelected(z);
        this.zzsi.setContentDescription(z ? this.zzsq : this.zzsr);
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onSendingRemoteMediaRequest() {
        this.zzsi.setEnabled(false);
    }

    public final void onSessionConnected(CastSession castSession) {
        if (this.zzak == null) {
            this.zzak = new zzbe(this);
        }
        super.onSessionConnected(castSession);
        castSession.a(this.zzak);
        zzdk();
    }

    public final void onSessionEnded() {
        Cast.Listener listener;
        this.zzsi.setEnabled(false);
        CastSession a2 = CastContext.a(this.zzhe).c().a();
        if (!(a2 == null || (listener = this.zzak) == null)) {
            a2.b(listener);
        }
        super.onSessionEnded();
    }

    /* access modifiers changed from: protected */
    public final void zzdk() {
        CastSession a2 = CastContext.a(this.zzhe).c().a();
        if (a2 == null || !a2.b()) {
            this.zzsi.setEnabled(false);
            return;
        }
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            this.zzsi.setEnabled(false);
        } else {
            this.zzsi.setEnabled(true);
        }
        if (a2.j()) {
            zzi(true);
        } else {
            zzi(false);
        }
    }
}
