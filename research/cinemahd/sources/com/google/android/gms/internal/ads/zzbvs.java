package com.google.android.gms.internal.ads;

import android.view.ViewGroup;
import com.google.android.gms.internal.ads.zzbod;

public final class zzbvs implements zzdxg<zzbmi<zzbkk>> {
    private final zzdxp<zzbqp> zzfhe;
    private final zzdxp<zzbfx> zzfkr;
    private final zzdxp<zzbod.zza> zzfks;
    private final zzdxp<zzbrm> zzfkt;
    private final zzdxp<zzbvi> zzfku;

    public zzbvs(zzdxp<zzbfx> zzdxp, zzdxp<zzbod.zza> zzdxp2, zzdxp<zzbrm> zzdxp3, zzdxp<zzbvi> zzdxp4, zzdxp<zzbqp> zzdxp5) {
        this.zzfkr = zzdxp;
        this.zzfks = zzdxp2;
        this.zzfkt = zzdxp3;
        this.zzfku = zzdxp4;
        this.zzfhe = zzdxp5;
    }

    public final /* synthetic */ Object get() {
        return (zzbmi) zzdxm.zza(this.zzfkr.get().zzach().zzc(this.zzfks.get().zzahh()).zzc(this.zzfkt.get()).zzb(this.zzfku.get()).zza(new zzcns((zzaak) null)).zza(new zzbma(this.zzfhe.get())).zzb(new zzbkf((ViewGroup) null)).zzaee().zzaed(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
