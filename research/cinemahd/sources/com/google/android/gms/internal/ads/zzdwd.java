package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;

final class zzdwd implements zzdsa {
    static final zzdsa zzew = new zzdwd();

    private zzdwd() {
    }

    public final boolean zzf(int i) {
        return zzdvx.zzb.zzf.C0045zzb.zzhe(i) != null;
    }
}
