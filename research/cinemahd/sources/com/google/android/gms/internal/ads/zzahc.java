package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzahc implements Parcelable.Creator<zzagz> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                i2 = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 2) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 == 3) {
                i3 = SafeParcelReader.w(parcel, a2);
            } else if (a3 != 1000) {
                SafeParcelReader.A(parcel, a2);
            } else {
                i = SafeParcelReader.w(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzagz(i, i2, str, i3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzagz[i];
    }
}
