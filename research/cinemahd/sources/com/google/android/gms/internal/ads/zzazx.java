package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.view.Surface;
import android.view.TextureView;
import com.google.android.gms.ads.internal.zzq;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@TargetApi(14)
public final class zzazx extends zzbag implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, TextureView.SurfaceTextureListener {
    private static final Map<Integer, String> zzdwx = new HashMap();
    private final zzbay zzdwy;
    private final boolean zzdwz;
    private int zzdxa = 0;
    private int zzdxb = 0;
    private MediaPlayer zzdxc;
    private Uri zzdxd;
    private int zzdxe;
    private int zzdxf;
    private int zzdxg;
    private int zzdxh;
    private int zzdxi;
    private zzbax zzdxj;
    private boolean zzdxk;
    private int zzdxl;
    /* access modifiers changed from: private */
    public zzbah zzdxm;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            zzdwx.put(-1004, "MEDIA_ERROR_IO");
            zzdwx.put(-1007, "MEDIA_ERROR_MALFORMED");
            zzdwx.put(-1010, "MEDIA_ERROR_UNSUPPORTED");
            zzdwx.put(-110, "MEDIA_ERROR_TIMED_OUT");
            zzdwx.put(3, "MEDIA_INFO_VIDEO_RENDERING_START");
        }
        zzdwx.put(100, "MEDIA_ERROR_SERVER_DIED");
        zzdwx.put(1, "MEDIA_ERROR_UNKNOWN");
        zzdwx.put(1, "MEDIA_INFO_UNKNOWN");
        zzdwx.put(700, "MEDIA_INFO_VIDEO_TRACK_LAGGING");
        zzdwx.put(701, "MEDIA_INFO_BUFFERING_START");
        zzdwx.put(702, "MEDIA_INFO_BUFFERING_END");
        zzdwx.put(800, "MEDIA_INFO_BAD_INTERLEAVING");
        zzdwx.put(801, "MEDIA_INFO_NOT_SEEKABLE");
        zzdwx.put(802, "MEDIA_INFO_METADATA_UPDATE");
        if (Build.VERSION.SDK_INT >= 19) {
            zzdwx.put(901, "MEDIA_INFO_UNSUPPORTED_SUBTITLE");
            zzdwx.put(902, "MEDIA_INFO_SUBTITLE_TIMED_OUT");
        }
    }

    public zzazx(Context context, boolean z, boolean z2, zzbaw zzbaw, zzbay zzbay) {
        super(context);
        setSurfaceTextureListener(this);
        this.zzdwy = zzbay;
        this.zzdxk = z;
        this.zzdwz = z2;
        this.zzdwy.zzb(this);
    }

    private final void zzat(boolean z) {
        zzavs.zzed("AdMediaPlayerView release");
        zzbax zzbax = this.zzdxj;
        if (zzbax != null) {
            zzbax.zzyf();
            this.zzdxj = null;
        }
        MediaPlayer mediaPlayer = this.zzdxc;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            this.zzdxc.release();
            this.zzdxc = null;
            zzct(0);
            if (z) {
                this.zzdxb = 0;
                this.zzdxb = 0;
            }
        }
    }

    private final void zzct(int i) {
        if (i == 3) {
            this.zzdwy.zzyi();
            this.zzdxt.zzyi();
        } else if (this.zzdxa == 3) {
            this.zzdwy.zzyj();
            this.zzdxt.zzyj();
        }
        this.zzdxa = i;
    }

    private final void zzd(float f) {
        MediaPlayer mediaPlayer = this.zzdxc;
        if (mediaPlayer != null) {
            try {
                mediaPlayer.setVolume(f, f);
            } catch (IllegalStateException unused) {
            }
        } else {
            zzayu.zzez("AdMediaPlayerView setMediaPlayerVolume() called before onPrepared().");
        }
    }

    private final void zzxp() {
        zzavs.zzed("AdMediaPlayerView init MediaPlayer");
        SurfaceTexture surfaceTexture = getSurfaceTexture();
        if (this.zzdxd != null && surfaceTexture != null) {
            zzat(false);
            try {
                zzq.zzlg();
                this.zzdxc = new MediaPlayer();
                this.zzdxc.setOnBufferingUpdateListener(this);
                this.zzdxc.setOnCompletionListener(this);
                this.zzdxc.setOnErrorListener(this);
                this.zzdxc.setOnInfoListener(this);
                this.zzdxc.setOnPreparedListener(this);
                this.zzdxc.setOnVideoSizeChangedListener(this);
                this.zzdxg = 0;
                if (this.zzdxk) {
                    this.zzdxj = new zzbax(getContext());
                    this.zzdxj.zza(surfaceTexture, getWidth(), getHeight());
                    this.zzdxj.start();
                    SurfaceTexture zzyg = this.zzdxj.zzyg();
                    if (zzyg != null) {
                        surfaceTexture = zzyg;
                    } else {
                        this.zzdxj.zzyf();
                        this.zzdxj = null;
                    }
                }
                this.zzdxc.setDataSource(getContext(), this.zzdxd);
                zzq.zzlh();
                this.zzdxc.setSurface(new Surface(surfaceTexture));
                this.zzdxc.setAudioStreamType(3);
                this.zzdxc.setScreenOnWhilePlaying(true);
                this.zzdxc.prepareAsync();
                zzct(1);
            } catch (IOException | IllegalArgumentException | IllegalStateException e) {
                String valueOf = String.valueOf(this.zzdxd);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 36);
                sb.append("Failed to initialize MediaPlayer at ");
                sb.append(valueOf);
                zzayu.zzd(sb.toString(), e);
                onError(this.zzdxc, 1, 0);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034 A[LOOP:0: B:10:0x0034->B:15:0x004f, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzxq() {
        /*
            r8 = this;
            boolean r0 = r8.zzdwz
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            boolean r0 = r8.zzxr()
            if (r0 == 0) goto L_0x0059
            android.media.MediaPlayer r0 = r8.zzdxc
            int r0 = r0.getCurrentPosition()
            if (r0 <= 0) goto L_0x0059
            int r0 = r8.zzdxb
            r1 = 3
            if (r0 == r1) goto L_0x0059
            java.lang.String r0 = "AdMediaPlayerView nudging MediaPlayer"
            com.google.android.gms.internal.ads.zzavs.zzed(r0)
            r0 = 0
            r8.zzd(r0)
            android.media.MediaPlayer r0 = r8.zzdxc
            r0.start()
            android.media.MediaPlayer r0 = r8.zzdxc
            int r0 = r0.getCurrentPosition()
            com.google.android.gms.common.util.Clock r1 = com.google.android.gms.ads.internal.zzq.zzkx()
            long r1 = r1.b()
        L_0x0034:
            boolean r3 = r8.zzxr()
            if (r3 == 0) goto L_0x0051
            android.media.MediaPlayer r3 = r8.zzdxc
            int r3 = r3.getCurrentPosition()
            if (r3 != r0) goto L_0x0051
            com.google.android.gms.common.util.Clock r3 = com.google.android.gms.ads.internal.zzq.zzkx()
            long r3 = r3.b()
            long r3 = r3 - r1
            r5 = 250(0xfa, double:1.235E-321)
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x0034
        L_0x0051:
            android.media.MediaPlayer r0 = r8.zzdxc
            r0.pause()
            r8.zzxs()
        L_0x0059:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzazx.zzxq():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r2.zzdxa;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzxr() {
        /*
            r2 = this;
            android.media.MediaPlayer r0 = r2.zzdxc
            if (r0 == 0) goto L_0x000f
            int r0 = r2.zzdxa
            r1 = -1
            if (r0 == r1) goto L_0x000f
            if (r0 == 0) goto L_0x000f
            r1 = 1
            if (r0 == r1) goto L_0x000f
            return r1
        L_0x000f:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzazx.zzxr():boolean");
    }

    public final int getCurrentPosition() {
        if (zzxr()) {
            return this.zzdxc.getCurrentPosition();
        }
        return 0;
    }

    public final int getDuration() {
        if (zzxr()) {
            return this.zzdxc.getDuration();
        }
        return -1;
    }

    public final int getVideoHeight() {
        MediaPlayer mediaPlayer = this.zzdxc;
        if (mediaPlayer != null) {
            return mediaPlayer.getVideoHeight();
        }
        return 0;
    }

    public final int getVideoWidth() {
        MediaPlayer mediaPlayer = this.zzdxc;
        if (mediaPlayer != null) {
            return mediaPlayer.getVideoWidth();
        }
        return 0;
    }

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        this.zzdxg = i;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        zzavs.zzed("AdMediaPlayerView completion");
        zzct(5);
        this.zzdxb = 5;
        zzawb.zzdsr.post(new zzazy(this));
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        String str = zzdwx.get(Integer.valueOf(i));
        String str2 = zzdwx.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 38 + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer error: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzayu.zzez(sb.toString());
        zzct(-1);
        this.zzdxb = -1;
        zzawb.zzdsr.post(new zzbab(this, str, str2));
        return true;
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
        String str = zzdwx.get(Integer.valueOf(i));
        String str2 = zzdwx.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 37 + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer info: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzavs.zzed(sb.toString());
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r6, int r7) {
        /*
            r5 = this;
            int r0 = r5.zzdxe
            int r0 = android.view.TextureView.getDefaultSize(r0, r6)
            int r1 = r5.zzdxf
            int r1 = android.view.TextureView.getDefaultSize(r1, r7)
            int r2 = r5.zzdxe
            if (r2 <= 0) goto L_0x0088
            int r2 = r5.zzdxf
            if (r2 <= 0) goto L_0x0088
            com.google.android.gms.internal.ads.zzbax r2 = r5.zzdxj
            if (r2 != 0) goto L_0x0088
            int r0 = android.view.View.MeasureSpec.getMode(r6)
            int r6 = android.view.View.MeasureSpec.getSize(r6)
            int r1 = android.view.View.MeasureSpec.getMode(r7)
            int r7 = android.view.View.MeasureSpec.getSize(r7)
            r2 = 1073741824(0x40000000, float:2.0)
            if (r0 != r2) goto L_0x0048
            if (r1 != r2) goto L_0x0048
            int r0 = r5.zzdxe
            int r1 = r0 * r7
            int r2 = r5.zzdxf
            int r3 = r6 * r2
            if (r1 >= r3) goto L_0x003d
            int r0 = r0 * r7
            int r0 = r0 / r2
            r1 = r7
            goto L_0x0088
        L_0x003d:
            int r1 = r0 * r7
            int r3 = r6 * r2
            if (r1 <= r3) goto L_0x0069
            int r2 = r2 * r6
            int r1 = r2 / r0
            goto L_0x0089
        L_0x0048:
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r2) goto L_0x005a
            int r0 = r5.zzdxf
            int r0 = r0 * r6
            int r2 = r5.zzdxe
            int r0 = r0 / r2
            if (r1 != r3) goto L_0x0058
            if (r0 <= r7) goto L_0x0058
            goto L_0x0067
        L_0x0058:
            r1 = r0
            goto L_0x0089
        L_0x005a:
            if (r1 != r2) goto L_0x006b
            int r1 = r5.zzdxe
            int r1 = r1 * r7
            int r2 = r5.zzdxf
            int r1 = r1 / r2
            if (r0 != r3) goto L_0x0068
            if (r1 <= r6) goto L_0x0068
        L_0x0067:
            goto L_0x0069
        L_0x0068:
            r6 = r1
        L_0x0069:
            r1 = r7
            goto L_0x0089
        L_0x006b:
            int r2 = r5.zzdxe
            int r4 = r5.zzdxf
            if (r1 != r3) goto L_0x0078
            if (r4 <= r7) goto L_0x0078
            int r2 = r2 * r7
            int r2 = r2 / r4
            r1 = r7
            goto L_0x0079
        L_0x0078:
            r1 = r4
        L_0x0079:
            if (r0 != r3) goto L_0x0086
            if (r2 <= r6) goto L_0x0086
            int r7 = r5.zzdxf
            int r7 = r7 * r6
            int r0 = r5.zzdxe
            int r1 = r7 / r0
            goto L_0x0089
        L_0x0086:
            r6 = r2
            goto L_0x0089
        L_0x0088:
            r6 = r0
        L_0x0089:
            r5.setMeasuredDimension(r6, r1)
            com.google.android.gms.internal.ads.zzbax r7 = r5.zzdxj
            if (r7 == 0) goto L_0x0093
            r7.zzm(r6, r1)
        L_0x0093:
            int r7 = android.os.Build.VERSION.SDK_INT
            r0 = 16
            if (r7 != r0) goto L_0x00ac
            int r7 = r5.zzdxh
            if (r7 <= 0) goto L_0x009f
            if (r7 != r6) goto L_0x00a5
        L_0x009f:
            int r7 = r5.zzdxi
            if (r7 <= 0) goto L_0x00a8
            if (r7 == r1) goto L_0x00a8
        L_0x00a5:
            r5.zzxq()
        L_0x00a8:
            r5.zzdxh = r6
            r5.zzdxi = r1
        L_0x00ac:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzazx.onMeasure(int, int):void");
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        zzavs.zzed("AdMediaPlayerView prepared");
        zzct(2);
        this.zzdwy.zzer();
        zzawb.zzdsr.post(new zzazz(this));
        this.zzdxe = mediaPlayer.getVideoWidth();
        this.zzdxf = mediaPlayer.getVideoHeight();
        int i = this.zzdxl;
        if (i != 0) {
            seekTo(i);
        }
        zzxq();
        int i2 = this.zzdxe;
        int i3 = this.zzdxf;
        StringBuilder sb = new StringBuilder(62);
        sb.append("AdMediaPlayerView stream dimensions: ");
        sb.append(i2);
        sb.append(" x ");
        sb.append(i3);
        zzayu.zzey(sb.toString());
        if (this.zzdxb == 3) {
            play();
        }
        zzxs();
    }

    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        zzavs.zzed("AdMediaPlayerView surface created");
        zzxp();
        zzawb.zzdsr.post(new zzbaa(this));
    }

    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        zzavs.zzed("AdMediaPlayerView surface destroyed");
        MediaPlayer mediaPlayer = this.zzdxc;
        if (mediaPlayer != null && this.zzdxl == 0) {
            this.zzdxl = mediaPlayer.getCurrentPosition();
        }
        zzbax zzbax = this.zzdxj;
        if (zzbax != null) {
            zzbax.zzyf();
        }
        zzawb.zzdsr.post(new zzbac(this));
        zzat(true);
        return true;
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        zzavs.zzed("AdMediaPlayerView surface changed");
        boolean z = true;
        boolean z2 = this.zzdxb == 3;
        if (!(this.zzdxe == i && this.zzdxf == i2)) {
            z = false;
        }
        if (this.zzdxc != null && z2 && z) {
            int i3 = this.zzdxl;
            if (i3 != 0) {
                seekTo(i3);
            }
            play();
        }
        zzbax zzbax = this.zzdxj;
        if (zzbax != null) {
            zzbax.zzm(i, i2);
        }
        zzawb.zzdsr.post(new zzbad(this, i, i2));
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.zzdwy.zzc(this);
        this.zzdxs.zza(surfaceTexture, this.zzdxm);
    }

    public final void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        StringBuilder sb = new StringBuilder(57);
        sb.append("AdMediaPlayerView size changed: ");
        sb.append(i);
        sb.append(" x ");
        sb.append(i2);
        zzavs.zzed(sb.toString());
        this.zzdxe = mediaPlayer.getVideoWidth();
        this.zzdxf = mediaPlayer.getVideoHeight();
        if (this.zzdxe != 0 && this.zzdxf != 0) {
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i) {
        StringBuilder sb = new StringBuilder(58);
        sb.append("AdMediaPlayerView window visibility changed to ");
        sb.append(i);
        zzavs.zzed(sb.toString());
        zzawb.zzdsr.post(new zzazw(this, i));
        super.onWindowVisibilityChanged(i);
    }

    public final void pause() {
        zzavs.zzed("AdMediaPlayerView pause");
        if (zzxr() && this.zzdxc.isPlaying()) {
            this.zzdxc.pause();
            zzct(4);
            zzawb.zzdsr.post(new zzbae(this));
        }
        this.zzdxb = 4;
    }

    public final void play() {
        zzavs.zzed("AdMediaPlayerView play");
        if (zzxr()) {
            this.zzdxc.start();
            zzct(3);
            this.zzdxs.zzxu();
            zzawb.zzdsr.post(new zzbaf(this));
        }
        this.zzdxb = 3;
    }

    public final void seekTo(int i) {
        StringBuilder sb = new StringBuilder(34);
        sb.append("AdMediaPlayerView seek ");
        sb.append(i);
        zzavs.zzed(sb.toString());
        if (zzxr()) {
            this.zzdxc.seekTo(i);
            this.zzdxl = 0;
            return;
        }
        this.zzdxl = i;
    }

    public final void setVideoPath(String str) {
        Uri parse = Uri.parse(str);
        zzry zzd = zzry.zzd(parse);
        if (zzd == null || zzd.url != null) {
            if (zzd != null) {
                parse = Uri.parse(zzd.url);
            }
            this.zzdxd = parse;
            this.zzdxl = 0;
            zzxp();
            requestLayout();
            invalidate();
        }
    }

    public final void stop() {
        zzavs.zzed("AdMediaPlayerView stop");
        MediaPlayer mediaPlayer = this.zzdxc;
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            this.zzdxc.release();
            this.zzdxc = null;
            zzct(0);
            this.zzdxb = 0;
        }
        this.zzdwy.onStop();
    }

    public final String toString() {
        String name = zzazx.class.getName();
        String hexString = Integer.toHexString(hashCode());
        StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 1 + String.valueOf(hexString).length());
        sb.append(name);
        sb.append("@");
        sb.append(hexString);
        return sb.toString();
    }

    public final void zza(zzbah zzbah) {
        this.zzdxm = zzbah;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzcu(int i) {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.onWindowVisibilityChanged(i);
        }
    }

    public final String zzxo() {
        String str = this.zzdxk ? " spherical" : "";
        return str.length() != 0 ? "MediaPlayer".concat(str) : new String("MediaPlayer");
    }

    public final void zzxs() {
        zzd(this.zzdxt.getVolume());
    }

    public final void zza(float f, float f2) {
        zzbax zzbax = this.zzdxj;
        if (zzbax != null) {
            zzbax.zzb(f, f2);
        }
    }
}
