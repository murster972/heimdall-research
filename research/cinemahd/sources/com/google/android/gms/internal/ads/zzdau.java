package com.google.android.gms.internal.ads;

public final class zzdau implements zzdxg<zzavp> {
    private final zzdaq zzgne;
    private final zzdxp<zzdao> zzgng;

    private zzdau(zzdaq zzdaq, zzdxp<zzdao> zzdxp) {
        this.zzgne = zzdaq;
        this.zzgng = zzdxp;
    }

    public static zzdau zzb(zzdaq zzdaq, zzdxp<zzdao> zzdxp) {
        return new zzdau(zzdaq, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzavp) zzdxm.zza(this.zzgng.get().zzdpz, "Cannot return null from a non-@Nullable @Provides method");
    }
}
