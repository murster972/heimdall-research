package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;

final class zzcgp implements zzbqx {
    private final zzatv zzbng;
    private final Context zzup;

    zzcgp(Context context, zzatv zzatv) {
        this.zzup = context;
        this.zzbng = zzatv;
    }

    public final void zzb(zzaqk zzaqk) {
    }

    public final void zzb(zzczt zzczt) {
        if (!TextUtils.isEmpty(zzczt.zzgmi.zzgmf.zzdlk)) {
            this.zzbng.zza(this.zzup, zzczt.zzgmh.zzfgl.zzgml);
            this.zzbng.zzj(this.zzup, zzczt.zzgmi.zzgmf.zzdlk);
        }
    }
}
