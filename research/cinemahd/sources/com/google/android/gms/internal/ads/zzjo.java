package com.google.android.gms.internal.ads;

import java.io.IOException;

public interface zzjo {
    int zza(zzjg zzjg, int i, boolean z) throws IOException, InterruptedException;

    void zza(long j, int i, int i2, int i3, zzjn zzjn);

    void zza(zzoj zzoj, int i);

    void zze(zzgw zzgw);
}
