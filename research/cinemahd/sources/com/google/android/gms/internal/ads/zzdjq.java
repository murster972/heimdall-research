package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdjq extends zzdik<zzdhx, zzdlz> {
    zzdjq(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        return new zzdon(((zzdlz) obj).zzass().toByteArray());
    }
}
