package com.google.android.gms.internal.cast;

import android.os.Build;
import android.os.Looper;

final class zzff extends ThreadLocal<zzfe> {
    zzff() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object initialValue() {
        if (Build.VERSION.SDK_INT >= 16) {
            return new zzfk();
        }
        Looper myLooper = Looper.myLooper();
        if (myLooper != null) {
            return new zzfj(myLooper);
        }
        throw new IllegalStateException("The current thread must have a looper!");
    }
}
