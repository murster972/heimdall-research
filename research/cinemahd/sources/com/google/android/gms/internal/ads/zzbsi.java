package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzbsi implements zzdxg<zzclp> {
    private final zzdxp<Clock> zzfcz;
    private final zzbrm zzfim;

    private zzbsi(zzbrm zzbrm, zzdxp<Clock> zzdxp) {
        this.zzfim = zzbrm;
        this.zzfcz = zzdxp;
    }

    public static zzbsi zzb(zzbrm zzbrm, zzdxp<Clock> zzdxp) {
        return new zzbsi(zzbrm, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzclp) zzdxm.zza(this.zzfim.zza(this.zzfcz.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
