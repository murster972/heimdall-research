package com.google.android.gms.internal.ads;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;

final /* synthetic */ class zzbyw implements zzded {
    private final String zzcyz;
    private final int zzdtg;
    private final int zzdth;
    private final double zzfpm;

    zzbyw(String str, double d, int i, int i2) {
        this.zzcyz = str;
        this.zzfpm = d;
        this.zzdtg = i;
        this.zzdth = i2;
    }

    public final Object apply(Object obj) {
        String str = this.zzcyz;
        return new zzabu(new BitmapDrawable(Resources.getSystem(), (Bitmap) obj), Uri.parse(str), this.zzfpm, this.zzdtg, this.zzdth);
    }
}
