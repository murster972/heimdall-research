package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final class zzaqx {
    public final long zzdnl = zzq.zzkx().b();
    public final zzaqt zzdnm;

    public zzaqx(zzaqv zzaqv, zzaqt zzaqt) {
        this.zzdnm = zzaqt;
    }
}
