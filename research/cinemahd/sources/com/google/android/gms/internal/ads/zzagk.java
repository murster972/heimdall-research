package com.google.android.gms.internal.ads;

import android.os.ParcelFileDescriptor;

final class zzagk implements zzdgf<zzagf, ParcelFileDescriptor> {
    private final /* synthetic */ zzafz zzcya;

    zzagk(zzagh zzagh, zzafz zzafz) {
        this.zzcya = zzafz;
    }

    public final /* synthetic */ zzdhe zzf(Object obj) throws Exception {
        zzazl zzazl = new zzazl();
        ((zzagf) obj).zza(this.zzcya, new zzagj(this, zzazl));
        return zzazl;
    }
}
