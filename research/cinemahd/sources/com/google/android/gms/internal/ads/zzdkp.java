package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdkp extends zzdii<zzdky> {
    zzdkp() {
        super(zzdky.class, new zzdko(zzdio.class));
    }

    /* access modifiers changed from: private */
    public static void zza(zzdlc zzdlc) throws GeneralSecurityException {
        if (zzdlc.zzasx() < 10) {
            throw new GeneralSecurityException("tag size too short");
        } else if (zzdlc.zzasx() > 16) {
            throw new GeneralSecurityException("tag size too long");
        }
    }

    /* access modifiers changed from: private */
    public static void zzea(int i) throws GeneralSecurityException {
        if (i != 32) {
            throw new GeneralSecurityException("AesCmacKey size wrong, must be 16 bytes");
        }
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesCmacKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    public final zzdih<zzdlb, zzdky> zzasg() {
        return new zzdkr(this, zzdlb.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdky zzdky = (zzdky) zzdte;
        zzdpo.zzx(zzdky.getVersion(), 0);
        zzea(zzdky.zzass().size());
        zza(zzdky.zzast());
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdky.zzt(zzdqk);
    }
}
