package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.RunnableFuture;

final class zzdhs<V> extends zzdgm<V> implements RunnableFuture<V> {
    private volatile zzdha<?> zzgxp;

    private zzdhs(Callable<V> callable) {
        this.zzgxp = new zzdhu(this, callable);
    }

    static <V> zzdhs<V> zza(Runnable runnable, V v) {
        return new zzdhs<>(Executors.callable(runnable, v));
    }

    static <V> zzdhs<V> zze(Callable<V> callable) {
        return new zzdhs<>(callable);
    }

    /* access modifiers changed from: protected */
    public final void afterDone() {
        zzdha<?> zzdha;
        super.afterDone();
        if (wasInterrupted() && (zzdha = this.zzgxp) != null) {
            zzdha.interruptTask();
        }
        this.zzgxp = null;
    }

    /* access modifiers changed from: protected */
    public final String pendingToString() {
        zzdha<?> zzdha = this.zzgxp;
        if (zzdha == null) {
            return super.pendingToString();
        }
        String valueOf = String.valueOf(zzdha);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 7);
        sb.append("task=[");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    public final void run() {
        zzdha<?> zzdha = this.zzgxp;
        if (zzdha != null) {
            zzdha.run();
        }
        this.zzgxp = null;
    }

    zzdhs(zzdgd<V> zzdgd) {
        this.zzgxp = new zzdhr(this, zzdgd);
    }
}
