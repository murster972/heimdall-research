package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Callable;

public final class zzdcf {
    private final E zzgpx;
    private final List<zzdhe<?>> zzgqc;
    private final /* synthetic */ zzdcd zzgqd;

    private zzdcf(zzdcd zzdcd, E e, List<zzdhe<?>> list) {
        this.zzgqd = zzdcd;
        this.zzgpx = e;
        this.zzgqc = list;
    }

    public final <O> zzdcj<O> zzb(Callable<O> callable) {
        zzdgx<V> zzi = zzdgs.zzi(this.zzgqc);
        zzdhe<C> zza = zzi.zza(zzdce.zzgfx, zzazd.zzdwj);
        zzdcd zzdcd = this.zzgqd;
        return new zzdcj(zzdcd, this.zzgpx, (String) null, zza, this.zzgqc, zzi.zza(callable, zzdcd.zzfov), (zzdcc) null);
    }
}
