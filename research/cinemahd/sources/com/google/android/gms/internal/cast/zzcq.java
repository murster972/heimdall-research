package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.games.GameManagerState;
import com.google.android.gms.cast.games.PlayerInfo;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.util.JsonUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public final class zzcq implements GameManagerState {
    private final String zzww;
    private final int zzwx;
    private final int zzxl;
    private final int zzxm;
    private final String zzxn;
    private final JSONObject zzxo;
    private final Map<String, PlayerInfo> zzxp;

    public zzcq(int i, int i2, String str, JSONObject jSONObject, Collection<PlayerInfo> collection, String str2, int i3) {
        this.zzxl = i;
        this.zzxm = i2;
        this.zzxn = str;
        this.zzxo = jSONObject;
        this.zzww = str2;
        this.zzwx = i3;
        this.zzxp = new HashMap(collection.size());
        for (PlayerInfo next : collection) {
            this.zzxp.put(next.getPlayerId(), next);
        }
    }

    public final boolean equals(Object obj) {
        if (obj != null && (obj instanceof GameManagerState)) {
            GameManagerState gameManagerState = (GameManagerState) obj;
            if (getPlayers().size() != gameManagerState.getPlayers().size()) {
                return false;
            }
            for (PlayerInfo next : getPlayers()) {
                boolean z = false;
                for (PlayerInfo next2 : gameManagerState.getPlayers()) {
                    if (zzdk.zza(next.getPlayerId(), next2.getPlayerId())) {
                        if (!zzdk.zza(next, next2)) {
                            return false;
                        }
                        z = true;
                    }
                }
                if (!z) {
                    return false;
                }
            }
            if (this.zzxl != gameManagerState.getLobbyState() || this.zzxm != gameManagerState.getGameplayState() || this.zzwx != gameManagerState.getMaxPlayers() || !zzdk.zza(this.zzww, gameManagerState.getApplicationName()) || !zzdk.zza(this.zzxn, gameManagerState.getGameStatusText()) || !JsonUtils.a(this.zzxo, gameManagerState.getGameData())) {
                return false;
            }
            return true;
        }
        return false;
    }

    public final CharSequence getApplicationName() {
        return this.zzww;
    }

    public final List<PlayerInfo> getConnectedControllablePlayers() {
        ArrayList arrayList = new ArrayList();
        for (PlayerInfo next : getPlayers()) {
            if (next.isConnected() && next.isControllable()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final List<PlayerInfo> getConnectedPlayers() {
        ArrayList arrayList = new ArrayList();
        for (PlayerInfo next : getPlayers()) {
            if (next.isConnected()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final List<PlayerInfo> getControllablePlayers() {
        ArrayList arrayList = new ArrayList();
        for (PlayerInfo next : getPlayers()) {
            if (next.isControllable()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final JSONObject getGameData() {
        return this.zzxo;
    }

    public final CharSequence getGameStatusText() {
        return this.zzxn;
    }

    public final int getGameplayState() {
        return this.zzxm;
    }

    public final Collection<String> getListOfChangedPlayers(GameManagerState gameManagerState) {
        HashSet hashSet = new HashSet();
        for (PlayerInfo next : getPlayers()) {
            PlayerInfo player = gameManagerState.getPlayer(next.getPlayerId());
            if (player == null || !next.equals(player)) {
                hashSet.add(next.getPlayerId());
            }
        }
        for (PlayerInfo next2 : gameManagerState.getPlayers()) {
            if (getPlayer(next2.getPlayerId()) == null) {
                hashSet.add(next2.getPlayerId());
            }
        }
        return hashSet;
    }

    public final int getLobbyState() {
        return this.zzxl;
    }

    public final int getMaxPlayers() {
        return this.zzwx;
    }

    public final PlayerInfo getPlayer(String str) {
        if (str == null) {
            return null;
        }
        return this.zzxp.get(str);
    }

    public final Collection<PlayerInfo> getPlayers() {
        return Collections.unmodifiableCollection(this.zzxp.values());
    }

    public final List<PlayerInfo> getPlayersInState(int i) {
        ArrayList arrayList = new ArrayList();
        for (PlayerInfo next : getPlayers()) {
            if (next.getPlayerState() == i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final boolean hasGameDataChanged(GameManagerState gameManagerState) {
        return !JsonUtils.a(this.zzxo, gameManagerState.getGameData());
    }

    public final boolean hasGameStatusTextChanged(GameManagerState gameManagerState) {
        return !zzdk.zza(this.zzxn, gameManagerState.getGameStatusText());
    }

    public final boolean hasGameplayStateChanged(GameManagerState gameManagerState) {
        return this.zzxm != gameManagerState.getGameplayState();
    }

    public final boolean hasLobbyStateChanged(GameManagerState gameManagerState) {
        return this.zzxl != gameManagerState.getLobbyState();
    }

    public final boolean hasPlayerChanged(String str, GameManagerState gameManagerState) {
        return !zzdk.zza(getPlayer(str), gameManagerState.getPlayer(str));
    }

    public final boolean hasPlayerDataChanged(String str, GameManagerState gameManagerState) {
        PlayerInfo player = getPlayer(str);
        PlayerInfo player2 = gameManagerState.getPlayer(str);
        if (player == null && player2 == null) {
            return false;
        }
        return player == null || player2 == null || !JsonUtils.a(player.getPlayerData(), player2.getPlayerData());
    }

    public final boolean hasPlayerStateChanged(String str, GameManagerState gameManagerState) {
        PlayerInfo player = getPlayer(str);
        PlayerInfo player2 = gameManagerState.getPlayer(str);
        if (player == null && player2 == null) {
            return false;
        }
        return player == null || player2 == null || player.getPlayerState() != player2.getPlayerState();
    }

    public final int hashCode() {
        return Objects.a(Integer.valueOf(this.zzxl), Integer.valueOf(this.zzxm), this.zzxp, this.zzxn, this.zzxo, this.zzww, Integer.valueOf(this.zzwx));
    }
}
