package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzary extends zzgb implements zzarz {
    public zzary() {
        super("com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
    }

    public static zzarz zzak(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.mediation.client.IMediationRewardedVideoAdListener");
        if (queryLocalInterface instanceof zzarz) {
            return (zzarz) queryLocalInterface;
        }
        return new zzasb(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                zzaf(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 2:
                zzd(IObjectWrapper.Stub.a(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 3:
                zzag(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 4:
                zzah(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 5:
                zzai(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 6:
                zzaj(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 7:
                zza(IObjectWrapper.Stub.a(parcel.readStrongBinder()), (zzasd) zzge.zza(parcel, zzasd.CREATOR));
                break;
            case 8:
                zzak(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 9:
                zze(IObjectWrapper.Stub.a(parcel.readStrongBinder()), parcel.readInt());
                break;
            case 10:
                zzal(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 11:
                zzam(IObjectWrapper.Stub.a(parcel.readStrongBinder()));
                break;
            case 12:
                zzb((Bundle) zzge.zza(parcel, Bundle.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
