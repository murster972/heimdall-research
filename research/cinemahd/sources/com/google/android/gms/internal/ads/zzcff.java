package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcff implements zzdgf {
    private final zzcfe zzfuh;
    private final zzaqk zzfui;

    zzcff(zzcfe zzcfe, zzaqk zzaqk) {
        this.zzfuh = zzcfe;
        this.zzfui = zzaqk;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfuh.zza(this.zzfui, (zzcgr) obj);
    }
}
