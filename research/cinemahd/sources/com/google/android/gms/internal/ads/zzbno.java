package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbno implements zzdxg<zzbsu<zzbov>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbiw> zzfdq;

    private zzbno(zzdxp<zzbiw> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdq = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzbno zzf(zzdxp<zzbiw> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzbno(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
