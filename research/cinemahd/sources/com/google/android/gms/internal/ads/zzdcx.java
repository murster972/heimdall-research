package com.google.android.gms.internal.ads;

public interface zzdcx {
    void zza(zzdco zzdco, String str);

    void zza(zzdco zzdco, String str, Throwable th);

    void zzb(zzdco zzdco, String str);

    void zzc(zzdco zzdco, String str);
}
