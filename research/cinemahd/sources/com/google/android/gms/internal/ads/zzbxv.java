package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzbxv implements zzafn {
    private final zzbxr zzfof;

    zzbxv(zzbxr zzbxr) {
        this.zzfof = zzbxr;
    }

    public final void zza(Object obj, Map map) {
        this.zzfof.zzb((zzbdi) obj, map);
    }
}
