package com.google.android.gms.internal.ads;

public final class zzblz implements zzdxg<zzbqp> {
    private final zzbma zzffl;

    public zzblz(zzbma zzbma) {
        this.zzffl = zzbma;
    }

    public final /* synthetic */ Object get() {
        return (zzbqp) zzdxm.zza(this.zzffl.zzagq(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
