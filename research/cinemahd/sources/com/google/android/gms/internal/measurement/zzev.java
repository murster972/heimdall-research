package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzev;

public interface zzev<T extends zzev<T>> extends Comparable<T> {
    int zza();

    zzgm zza(zzgm zzgm, zzgn zzgn);

    zzgs zza(zzgs zzgs, zzgs zzgs2);

    zzil zzb();

    zzio zzc();

    boolean zzd();

    boolean zze();
}
