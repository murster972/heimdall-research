package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzccn implements zzdxg<zzbsu<zzbri>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzccw> zzfdd;

    private zzccn(zzdxp<zzccw> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzccn zzo(zzdxp<zzccw> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzccn(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
