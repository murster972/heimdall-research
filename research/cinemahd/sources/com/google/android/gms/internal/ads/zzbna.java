package com.google.android.gms.internal.ads;

public final class zzbna implements zzdxg<zzbmx> {
    private final zzdxp<zzbpm> zzesd;

    private zzbna(zzdxp<zzbpm> zzdxp) {
        this.zzesd = zzdxp;
    }

    public static zzbna zze(zzdxp<zzbpm> zzdxp) {
        return new zzbna(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbmx(this.zzesd.get());
    }
}
