package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcjg implements zzdxg<zzcix> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzcbn> zzfog;
    private final zzdxp<zzblg> zzfyl;
    private final zzdxp<zzded<zzczl, zzawt>> zzfyo;

    public zzcjg(zzdxp<zzblg> zzdxp, zzdxp<Context> zzdxp2, zzdxp<Executor> zzdxp3, zzdxp<zzcbn> zzdxp4, zzdxp<zzczu> zzdxp5, zzdxp<zzded<zzczl, zzawt>> zzdxp6) {
        this.zzfyl = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfei = zzdxp3;
        this.zzfog = zzdxp4;
        this.zzfep = zzdxp5;
        this.zzfyo = zzdxp6;
    }

    public final /* synthetic */ Object get() {
        return new zzcix(this.zzfyl.get(), this.zzejv.get(), this.zzfei.get(), this.zzfog.get(), this.zzfep.get(), this.zzfyo.get());
    }
}
