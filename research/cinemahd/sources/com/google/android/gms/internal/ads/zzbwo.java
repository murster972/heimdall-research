package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbwo implements Runnable {
    private final boolean zzdym;
    private final zzbwk zzflf;

    zzbwo(zzbwk zzbwk, boolean z) {
        this.zzflf = zzbwk;
        this.zzdym = z;
    }

    public final void run() {
        this.zzflf.zzbh(this.zzdym);
    }
}
