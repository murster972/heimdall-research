package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class zzdwl implements zzbf {
    private static zzdwy zzcr = zzdwy.zzn(zzdwl.class);
    private String type;
    private long zzauq;
    private zzbi zzhyp;
    boolean zzhyq;
    private boolean zzhyr;
    private ByteBuffer zzhys;
    private long zzhyt;
    private long zzhyu = -1;
    private zzdws zzhyv;
    private ByteBuffer zzhyw = null;

    protected zzdwl(String str) {
        this.type = str;
        this.zzhyr = true;
        this.zzhyq = true;
    }

    private final synchronized void zzbdi() {
        if (!this.zzhyr) {
            try {
                zzdwy zzdwy = zzcr;
                String valueOf = String.valueOf(this.type);
                zzdwy.zzhp(valueOf.length() != 0 ? "mem mapping ".concat(valueOf) : new String("mem mapping "));
                this.zzhys = this.zzhyv.zzh(this.zzhyt, this.zzhyu);
                this.zzhyr = true;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public final String getType() {
        return this.type;
    }

    public final void zza(zzdws zzdws, ByteBuffer byteBuffer, long j, zzbe zzbe) throws IOException {
        this.zzhyt = zzdws.position();
        this.zzauq = this.zzhyt - ((long) byteBuffer.remaining());
        this.zzhyu = j;
        this.zzhyv = zzdws;
        zzdws.zzfc(zzdws.position() + j);
        this.zzhyr = false;
        this.zzhyq = false;
        zzbdj();
    }

    public final synchronized void zzbdj() {
        zzbdi();
        zzdwy zzdwy = zzcr;
        String valueOf = String.valueOf(this.type);
        zzdwy.zzhp(valueOf.length() != 0 ? "parsing details of ".concat(valueOf) : new String("parsing details of "));
        if (this.zzhys != null) {
            ByteBuffer byteBuffer = this.zzhys;
            this.zzhyq = true;
            byteBuffer.rewind();
            zzg(byteBuffer);
            if (byteBuffer.remaining() > 0) {
                this.zzhyw = byteBuffer.slice();
            }
            this.zzhys = null;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zzg(ByteBuffer byteBuffer);

    public final void zza(zzbi zzbi) {
        this.zzhyp = zzbi;
    }
}
