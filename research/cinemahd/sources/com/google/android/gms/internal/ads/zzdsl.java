package com.google.android.gms.internal.ads;

import java.util.List;

public interface zzdsl extends List {
    List<?> zzbav();

    zzdsl zzbaw();

    void zzbg(zzdqk zzdqk);

    Object zzgm(int i);
}
