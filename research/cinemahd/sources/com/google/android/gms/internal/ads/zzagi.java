package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzagi extends zzgc implements zzagf {
    zzagi(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.httpcache.IHttpAssetsCacheService");
    }

    public final void zza(zzafz zzafz, zzagd zzagd) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, (Parcelable) zzafz);
        zzge.zza(obtainAndWriteInterfaceToken, (IInterface) zzagd);
        zzb(2, obtainAndWriteInterfaceToken);
    }
}
