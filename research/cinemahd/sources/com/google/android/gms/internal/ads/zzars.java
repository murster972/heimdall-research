package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzars extends RemoteCreator<zzarm> {
    public zzars() {
        super("com.google.android.gms.ads.reward.RewardedVideoAdCreatorImpl");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object getRemoteCreator(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAdCreator");
        if (queryLocalInterface instanceof zzarm) {
            return (zzarm) queryLocalInterface;
        }
        return new zzarp(iBinder);
    }

    public final zzarl zza(Context context, zzalc zzalc) {
        try {
            IBinder zzb = ((zzarm) getRemoteCreatorInstance(context)).zzb(ObjectWrapper.a(context), zzalc, 19649000);
            if (zzb == null) {
                return null;
            }
            IInterface queryLocalInterface = zzb.queryLocalInterface("com.google.android.gms.ads.internal.reward.client.IRewardedVideoAd");
            if (queryLocalInterface instanceof zzarl) {
                return (zzarl) queryLocalInterface;
            }
            return new zzarn(zzb);
        } catch (RemoteException | RemoteCreator.RemoteCreatorException e) {
            zzayu.zzd("Could not get remote RewardedVideoAd.", e);
            return null;
        }
    }
}
