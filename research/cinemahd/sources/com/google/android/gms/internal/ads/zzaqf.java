package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class zzaqf extends zzgc implements zzaqc {
    zzaqf(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.request.IAdResponseListener");
    }
}
