package com.google.android.gms.internal.ads;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;

final class zzcls implements zzdgt<T> {
    private final /* synthetic */ String zzgae;
    private final /* synthetic */ long zzgaf;
    private final /* synthetic */ zzclp zzgag;

    zzcls(zzclp zzclp, String str, long j) {
        this.zzgag = zzclp;
        this.zzgae = str;
        this.zzgaf = j;
    }

    public final void onSuccess(T t) {
        this.zzgag.zza(this.zzgae, 0, this.zzgag.zzbmq.a() - this.zzgaf);
    }

    public final void zzb(Throwable th) {
        long a2 = this.zzgag.zzbmq.a();
        int i = 3;
        if (th instanceof TimeoutException) {
            i = 2;
        } else if (!(th instanceof zzclf)) {
            i = th instanceof CancellationException ? 4 : (!(th instanceof zzcfb) || ((zzcfb) th).getErrorCode() != 3) ? 6 : 1;
        }
        this.zzgag.zza(this.zzgae, i, a2 - this.zzgaf);
    }
}
