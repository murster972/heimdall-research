package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.HashMap;

public final class zzbuy implements zzo, zzbqb {
    private final zzazb zzbll;
    private final zzbdi zzcza;
    private final zzczl zzffc;
    private IObjectWrapper zzffd;
    private final int zzfjj;
    private final Context zzup;

    public zzbuy(Context context, zzbdi zzbdi, zzczl zzczl, zzazb zzazb, int i) {
        this.zzup = context;
        this.zzcza = zzbdi;
        this.zzffc = zzczl;
        this.zzbll = zzazb;
        this.zzfjj = i;
    }

    public final void onAdLoaded() {
        int i = this.zzfjj;
        if ((i == 7 || i == 3) && this.zzffc.zzdli && this.zzcza != null && zzq.zzlf().zzp(this.zzup)) {
            zzazb zzazb = this.zzbll;
            int i2 = zzazb.zzdvz;
            int i3 = zzazb.zzdwa;
            StringBuilder sb = new StringBuilder(23);
            sb.append(i2);
            sb.append(".");
            sb.append(i3);
            this.zzffd = zzq.zzlf().zza(sb.toString(), this.zzcza.getWebView(), "", "javascript", this.zzffc.zzgly.optInt("media_type", -1) == 0 ? null : "javascript");
            if (this.zzffd != null && this.zzcza.getView() != null) {
                zzq.zzlf().zza(this.zzffd, this.zzcza.getView());
                this.zzcza.zzan(this.zzffd);
                zzq.zzlf().zzab(this.zzffd);
            }
        }
    }

    public final void onPause() {
    }

    public final void onResume() {
    }

    public final void zzte() {
        this.zzffd = null;
    }

    public final void zztf() {
        zzbdi zzbdi;
        if (this.zzffd != null && (zzbdi = this.zzcza) != null) {
            zzbdi.zza("onSdkImpression", new HashMap());
        }
    }
}
