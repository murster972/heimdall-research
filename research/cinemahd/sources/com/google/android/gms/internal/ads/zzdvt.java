package com.google.android.gms.internal.ads;

import java.io.IOException;

public class zzdvt {
    protected volatile int zzhhn = -1;

    public static final byte[] zza(zzdvt zzdvt) {
        byte[] bArr = new byte[zzdvt.zzazu()];
        try {
            zzdvo zzq = zzdvo.zzq(bArr, 0, bArr.length);
            zzdvt.zza(zzq);
            zzq.zzazd();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public String toString() {
        return zzdvw.zzb(this);
    }

    public void zza(zzdvo zzdvo) throws IOException {
    }

    public final int zzazu() {
        int zzoi = zzoi();
        this.zzhhn = zzoi;
        return zzoi;
    }

    /* renamed from: zzbcr */
    public zzdvt clone() throws CloneNotSupportedException {
        return (zzdvt) super.clone();
    }

    /* access modifiers changed from: protected */
    public int zzoi() {
        return 0;
    }
}
