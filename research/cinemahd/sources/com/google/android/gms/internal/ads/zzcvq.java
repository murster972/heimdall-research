package com.google.android.gms.internal.ads;

import android.location.Location;

final /* synthetic */ class zzcvq implements zzded {
    static final zzded zzdoq = new zzcvq();

    private zzcvq() {
    }

    public final Object apply(Object obj) {
        return new zzcvo((Location) obj);
    }
}
