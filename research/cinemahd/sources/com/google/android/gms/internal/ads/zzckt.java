package com.google.android.gms.internal.ads;

final /* synthetic */ class zzckt implements zzdgf {
    private final zzczl zzfzk;
    private final zzcku zzfzs;

    zzckt(zzcku zzcku, zzczl zzczl) {
        this.zzfzs = zzcku;
        this.zzfzk = zzczl;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfzs.zza(this.zzfzk, (zzcaj) obj);
    }
}
