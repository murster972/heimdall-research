package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;

final /* synthetic */ class zzbto implements zzbrn {
    static final zzbrn zzfhp = new zzbto();

    private zzbto() {
    }

    public final void zzp(Object obj) {
        ((VideoController.VideoLifecycleCallbacks) obj).onVideoStart();
    }
}
