package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzi;
import java.util.concurrent.Executor;

public final class zzbdr {
    public static zzdhe<zzbdi> zza(Context context, zzazb zzazb, String str, zzdq zzdq, zza zza) {
        return zzdgs.zzb(zzdgs.zzaj(null), new zzbdq(context, zzdq, zzazb, zza, str), (Executor) zzazd.zzdwi);
    }

    public static zzbdi zza(Context context, zzbey zzbey, String str, boolean z, boolean z2, zzdq zzdq, zzazb zzazb, zzaae zzaae, zzi zzi, zza zza, zzsm zzsm, zzro zzro, boolean z3) throws zzbdv {
        zzzn.initialize(context);
        if (zzabl.zzcuv.get().booleanValue()) {
            return zzbfe.zza(context, zzbey, str, z, z2, zzdq, zzazb, (zzaae) null, zzi, zza, zzsm, zzro, z3);
        }
        try {
            return (zzbdi) zzayc.zza(new zzbdt(context, zzbey, str, z, z2, zzdq, zzazb, (zzaae) null, zzi, zza, zzsm, zzro, z3));
        } catch (Throwable th) {
            throw new zzbdv("Webview initialization failed.", th);
        }
    }
}
