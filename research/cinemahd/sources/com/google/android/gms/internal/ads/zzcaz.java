package com.google.android.gms.internal.ads;

public final class zzcaz implements zzafx {
    private final zzbpm zzfgg;
    private final zzasd zzfqx;
    private final String zzfqy;
    private final String zzfqz;

    public zzcaz(zzbpm zzbpm, zzczl zzczl) {
        this.zzfgg = zzbpm;
        this.zzfqx = zzczl.zzdky;
        this.zzfqy = zzczl.zzdcx;
        this.zzfqz = zzczl.zzdcy;
    }

    public final void zza(zzasd zzasd) {
        int i;
        String str;
        zzasd zzasd2 = this.zzfqx;
        if (zzasd2 != null) {
            zzasd = zzasd2;
        }
        if (zzasd != null) {
            str = zzasd.type;
            i = zzasd.zzdno;
        } else {
            str = "";
            i = 1;
        }
        this.zzfgg.zzb(new zzarc(str, i), this.zzfqy, this.zzfqz);
    }

    public final void zzrs() {
        this.zzfgg.onRewardedVideoStarted();
    }

    public final void zzrt() {
        this.zzfgg.onRewardedVideoCompleted();
    }
}
