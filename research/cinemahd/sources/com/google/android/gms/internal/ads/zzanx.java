package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

final class zzanx implements zzo {
    private final /* synthetic */ zzany zzdew;

    zzanx(zzany zzany) {
        this.zzdew = zzany;
    }

    public final void onPause() {
        zzayu.zzea("AdMobCustomTabsAdapter overlay is paused.");
    }

    public final void onResume() {
        zzayu.zzea("AdMobCustomTabsAdapter overlay is resumed.");
    }

    public final void zzte() {
        zzayu.zzea("AdMobCustomTabsAdapter overlay is closed.");
        this.zzdew.zzdey.onAdClosed(this.zzdew);
    }

    public final void zztf() {
        zzayu.zzea("Opening AdMobCustomTabsAdapter overlay.");
        this.zzdew.zzdey.onAdOpened(this.zzdew);
    }
}
