package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcpx implements Callable {
    private final zzcpu zzgen;

    zzcpx(zzcpu zzcpu) {
        this.zzgen = zzcpu;
    }

    public final Object call() {
        return this.zzgen.zzand();
    }
}
