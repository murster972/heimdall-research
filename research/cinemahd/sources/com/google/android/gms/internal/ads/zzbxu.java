package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzbxu implements zzafn {
    private final zzbxr zzfof;

    zzbxu(zzbxr zzbxr) {
        this.zzfof = zzbxr;
    }

    public final void zza(Object obj, Map map) {
        this.zzfof.zzd((zzbdi) obj, map);
    }
}
