package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public final class zzdoz implements zzdhx {
    private final zzdpi zzhgq;
    private final zzdio zzhgr;
    private final int zzhgs;

    public zzdoz(zzdpi zzdpi, zzdio zzdio, int i) {
        this.zzhgq = zzdpi;
        this.zzhgr = zzdio;
        this.zzhgs = i;
    }

    public final byte[] zzc(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        byte[] zzp = this.zzhgq.zzp(bArr);
        if (bArr2 == null) {
            bArr2 = new byte[0];
        }
        byte[] copyOf = Arrays.copyOf(ByteBuffer.allocate(8).putLong(((long) bArr2.length) * 8).array(), 8);
        return zzdoi.zza(zzp, this.zzhgr.zzl(zzdoi.zza(bArr2, zzp, copyOf)));
    }
}
