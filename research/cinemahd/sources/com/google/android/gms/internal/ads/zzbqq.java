package com.google.android.gms.internal.ads;

import java.lang.ref.WeakReference;

final class zzbqq implements Runnable {
    private final WeakReference<zzbqp> zzfhv;

    private zzbqq(zzbqp zzbqp) {
        this.zzfhv = new WeakReference<>(zzbqp);
    }

    public final void run() {
        zzbqp zzbqp = (zzbqp) this.zzfhv.get();
        if (zzbqp != null) {
            zzbqp.zzahk();
        }
    }
}
