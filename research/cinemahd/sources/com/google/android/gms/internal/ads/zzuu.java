package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

final class zzuu extends zzvb<zzaot> {
    private final /* synthetic */ Activity val$activity;
    private final /* synthetic */ zzup zzcdi;

    zzuu(zzup zzup, Activity activity) {
        this.zzcdi = zzup;
        this.val$activity = activity;
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zzb(ObjectWrapper.a(this.val$activity));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$activity, "ad_overlay");
        return null;
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzcdd.zzc(this.val$activity);
    }
}
