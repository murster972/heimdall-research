package com.google.android.gms.internal.ads;

import java.lang.ref.WeakReference;
import java.util.Map;

final class zzcaw<T> implements zzafn<Object> {
    private final WeakReference<T> zzfqs;
    private final String zzfqt;
    private final zzafn<T> zzfqu;
    private final /* synthetic */ zzcaj zzfqv;

    private zzcaw(zzcaj zzcaj, WeakReference<T> weakReference, String str, zzafn<T> zzafn) {
        this.zzfqv = zzcaj;
        this.zzfqs = weakReference;
        this.zzfqt = str;
        this.zzfqu = zzafn;
    }

    public final void zza(Object obj, Map<String, String> map) {
        Object obj2 = this.zzfqs.get();
        if (obj2 == null) {
            this.zzfqv.zzb(this.zzfqt, this);
        } else {
            this.zzfqu.zza(obj2, map);
        }
    }

    /* synthetic */ zzcaw(zzcaj zzcaj, WeakReference weakReference, String str, zzafn zzafn, zzcao zzcao) {
        this(zzcaj, weakReference, str, zzafn);
    }
}
