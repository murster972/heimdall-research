package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.appopen.AppOpenAd;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzuo extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzuo> CREATOR = new zzun();
    @AppOpenAd.AppOpenAdOrientation
    public final int orientation;

    public zzuo(@AppOpenAd.AppOpenAdOrientation int i) {
        this.orientation = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.orientation);
        SafeParcelWriter.a(parcel, a2);
    }
}
