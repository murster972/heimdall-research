package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

public final class zzbzb implements zzdxg<zzbyu> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbyl> zzepw;
    private final zzdxp<zzbzh> zzepy;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzdq> zzfky;
    private final zzdxp<zza> zzfpp;
    private final zzdxp<zzsm> zzfpq;
    private final zzdxp<ScheduledExecutorService> zzfpr;

    public zzbzb(zzdxp<Context> zzdxp, zzdxp<zzbyl> zzdxp2, zzdxp<zzdq> zzdxp3, zzdxp<zzazb> zzdxp4, zzdxp<zza> zzdxp5, zzdxp<zzsm> zzdxp6, zzdxp<Executor> zzdxp7, zzdxp<zzczu> zzdxp8, zzdxp<zzbzh> zzdxp9, zzdxp<ScheduledExecutorService> zzdxp10) {
        this.zzejv = zzdxp;
        this.zzepw = zzdxp2;
        this.zzfky = zzdxp3;
        this.zzfav = zzdxp4;
        this.zzfpp = zzdxp5;
        this.zzfpq = zzdxp6;
        this.zzfcv = zzdxp7;
        this.zzfep = zzdxp8;
        this.zzepy = zzdxp9;
        this.zzfpr = zzdxp10;
    }

    public final /* synthetic */ Object get() {
        return new zzbyu(this.zzejv.get(), this.zzepw.get(), this.zzfky.get(), this.zzfav.get(), this.zzfpp.get(), this.zzfpq.get(), this.zzfcv.get(), this.zzfep.get(), this.zzepy.get(), this.zzfpr.get());
    }
}
