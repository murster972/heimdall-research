package com.google.android.gms.internal.measurement;

import java.util.Iterator;

final class zzhn implements Iterable<Object> {
    zzhn() {
    }

    public final Iterator<Object> iterator() {
        return zzhl.zza;
    }
}
