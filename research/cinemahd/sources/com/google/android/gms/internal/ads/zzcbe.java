package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcbe implements Runnable {
    private final zzbdi zzehp;

    private zzcbe(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static Runnable zzh(zzbdi zzbdi) {
        return new zzcbe(zzbdi);
    }

    public final void run() {
        this.zzehp.destroy();
    }
}
