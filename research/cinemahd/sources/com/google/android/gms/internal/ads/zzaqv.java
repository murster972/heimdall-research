package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.WeakHashMap;
import java.util.concurrent.Future;

public final class zzaqv {
    /* access modifiers changed from: private */
    public WeakHashMap<Context, zzaqx> zzdni = new WeakHashMap<>();

    public final Future<zzaqt> zzx(Context context) {
        return zzazd.zzdwe.zzd(new zzaqu(this, context));
    }
}
