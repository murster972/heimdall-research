package com.google.android.gms.internal.cast;

import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class zzw extends zzk {
    private final MediaRouter zzcc;
    private final Map<MediaRouteSelector, Set<MediaRouter.Callback>> zzjq = new HashMap();

    public zzw(MediaRouter mediaRouter) {
        this.zzcc = mediaRouter;
    }

    public final void setMediaSessionCompat(MediaSessionCompat mediaSessionCompat) {
        this.zzcc.a(mediaSessionCompat);
    }

    public final void zza(Bundle bundle, zzl zzl) {
        MediaRouteSelector a2 = MediaRouteSelector.a(bundle);
        if (!this.zzjq.containsKey(a2)) {
            this.zzjq.put(a2, new HashSet());
        }
        this.zzjq.get(a2).add(new zzv(zzl));
    }

    public final void zzar() {
        MediaRouter mediaRouter = this.zzcc;
        mediaRouter.a(mediaRouter.a());
    }

    public final boolean zzas() {
        return this.zzcc.d().h().equals(this.zzcc.a().h());
    }

    public final String zzat() {
        return this.zzcc.d().h();
    }

    public final void zzau() {
        for (Set<MediaRouter.Callback> it2 : this.zzjq.values()) {
            for (MediaRouter.Callback a2 : it2) {
                this.zzcc.a(a2);
            }
        }
        this.zzjq.clear();
    }

    public final boolean zzb(Bundle bundle, int i) {
        return this.zzcc.a(MediaRouteSelector.a(bundle), i);
    }

    public final void zzd(Bundle bundle) {
        for (MediaRouter.Callback a2 : this.zzjq.get(MediaRouteSelector.a(bundle))) {
            this.zzcc.a(a2);
        }
    }

    public final void zzl(String str) {
        for (MediaRouter.RouteInfo next : this.zzcc.c()) {
            if (next.h().equals(str)) {
                this.zzcc.a(next);
                return;
            }
        }
    }

    public final Bundle zzm(String str) {
        for (MediaRouter.RouteInfo next : this.zzcc.c()) {
            if (next.h().equals(str)) {
                return next.f();
            }
        }
        return null;
    }

    public final int zzs() {
        return 12451009;
    }

    public final void zza(Bundle bundle, int i) {
        MediaRouteSelector a2 = MediaRouteSelector.a(bundle);
        for (MediaRouter.Callback a3 : this.zzjq.get(a2)) {
            this.zzcc.a(a2, a3, i);
        }
    }
}
