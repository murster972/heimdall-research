package com.google.android.gms.internal.cast;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbf extends UIController {
    private final ImageView zzsi;
    private final View zzst;
    private final boolean zzsu;
    private final Drawable zzsv;
    private final String zzsw;
    private final Drawable zzsx;
    private final String zzsy;
    private final Drawable zzsz;
    private final String zzta;

    public zzbf(ImageView imageView, Context context, Drawable drawable, Drawable drawable2, Drawable drawable3, View view, boolean z) {
        this.zzsi = imageView;
        this.zzsv = drawable;
        this.zzsx = drawable2;
        this.zzsz = drawable3 != null ? drawable3 : drawable2;
        this.zzsw = context.getString(R$string.cast_play);
        this.zzsy = context.getString(R$string.cast_pause);
        this.zzta = context.getString(R$string.cast_stop);
        this.zzst = view;
        this.zzsu = z;
        this.zzsi.setEnabled(false);
    }

    private final void zza(Drawable drawable, String str) {
        this.zzsi.setImageDrawable(drawable);
        this.zzsi.setContentDescription(str);
        this.zzsi.setVisibility(0);
        this.zzsi.setEnabled(true);
        View view = this.zzst;
        if (view != null) {
            view.setVisibility(8);
        }
    }

    private final void zzdk() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            this.zzsi.setEnabled(false);
        } else if (remoteMediaClient.o()) {
            zza(this.zzsv, this.zzsw);
        } else if (remoteMediaClient.p()) {
            if (remoteMediaClient.m()) {
                zza(this.zzsz, this.zzta);
            } else {
                zza(this.zzsx, this.zzsy);
            }
        } else if (remoteMediaClient.l()) {
            zzj(false);
        } else if (remoteMediaClient.n()) {
            zzj(true);
        }
    }

    private final void zzj(boolean z) {
        View view = this.zzst;
        int i = 0;
        if (view != null) {
            view.setVisibility(0);
        }
        ImageView imageView = this.zzsi;
        if (this.zzsu) {
            i = 4;
        }
        imageView.setVisibility(i);
        this.zzsi.setEnabled(!z);
    }

    public final void onMediaStatusUpdated() {
        zzdk();
    }

    public final void onSendingRemoteMediaRequest() {
        zzj(true);
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        zzdk();
    }

    public final void onSessionEnded() {
        this.zzsi.setEnabled(false);
        super.onSessionEnded();
    }
}
