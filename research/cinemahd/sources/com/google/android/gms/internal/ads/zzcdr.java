package com.google.android.gms.internal.ads;

public final class zzcdr implements zzdxg<zzcds> {
    private final zzdxp<zzcdv> zzekn;

    private zzcdr(zzdxp<zzcdv> zzdxp) {
        this.zzekn = zzdxp;
    }

    public static zzcdr zzz(zzdxp<zzcdv> zzdxp) {
        return new zzcdr(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcds(this.zzekn.get());
    }
}
