package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzbnk implements zzbov, zzbpe, zzbqb, zzbqx, zzty {
    private final Clock zzbmq;
    private final zzavd zzfgv;

    public zzbnk(Clock clock, zzavd zzavd) {
        this.zzbmq = clock;
        this.zzfgv = zzavd;
    }

    public final void onAdClicked() {
        this.zzfgv.zzuw();
    }

    public final void onAdClosed() {
        this.zzfgv.zzux();
    }

    public final void onAdImpression() {
        this.zzfgv.zzuv();
    }

    public final void onAdLeftApplication() {
    }

    public final void onAdLoaded() {
        this.zzfgv.zzan(true);
    }

    public final void onAdOpened() {
    }

    public final void onRewardedVideoCompleted() {
    }

    public final void onRewardedVideoStarted() {
    }

    public final void zzb(zzaqk zzaqk) {
    }

    public final void zzb(zzare zzare, String str, String str2) {
    }

    public final void zzb(zzczt zzczt) {
        this.zzfgv.zzey(this.zzbmq.a());
    }

    public final void zzf(zzug zzug) {
        this.zzfgv.zze(zzug);
    }

    public final String zzuy() {
        return this.zzfgv.zzuy();
    }
}
