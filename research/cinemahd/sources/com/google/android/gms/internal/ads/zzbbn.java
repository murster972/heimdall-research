package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbbn implements Runnable {
    private final int zzdtf;
    private final zzbbc zzebm;

    zzbbn(zzbbc zzbbc, int i) {
        this.zzebm = zzbbc;
        this.zzdtf = i;
    }

    public final void run() {
        this.zzebm.zzdb(this.zzdtf);
    }
}
