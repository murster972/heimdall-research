package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final class zzcez extends zzags {
    private final /* synthetic */ zzceq zzftz;
    private final /* synthetic */ Object zzfua;
    private final /* synthetic */ String zzfub;
    private final /* synthetic */ long zzfuc;
    private final /* synthetic */ zzazl zzfud;

    zzcez(zzceq zzceq, Object obj, String str, long j, zzazl zzazl) {
        this.zzftz = zzceq;
        this.zzfua = obj;
        this.zzfub = str;
        this.zzfuc = j;
        this.zzfud = zzazl;
    }

    public final void onInitializationFailed(String str) {
        synchronized (this.zzfua) {
            this.zzftz.zza(this.zzfub, false, str, (int) (zzq.zzkx().a() - this.zzfuc));
            this.zzftz.zzftr.zzq(this.zzfub, "error");
            this.zzfud.set(false);
        }
    }

    public final void onInitializationSucceeded() {
        synchronized (this.zzfua) {
            this.zzftz.zza(this.zzfub, true, "", (int) (zzq.zzkx().a() - this.zzfuc));
            this.zzftz.zzftr.zzge(this.zzfub);
            this.zzfud.set(true);
        }
    }
}
