package com.google.android.gms.internal.ads;

public class zzbvi {
    private final zzbwz zzfea;
    private final zzvh zzfjw;

    public zzbvi(zzbwz zzbwz, zzvh zzvh) {
        this.zzfea = zzbwz;
        this.zzfjw = zzvh;
    }

    public final zzbwz zzaij() {
        return this.zzfea;
    }

    public final zzvh zzaik() {
        return this.zzfjw;
    }
}
