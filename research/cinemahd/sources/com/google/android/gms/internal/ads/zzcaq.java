package com.google.android.gms.internal.ads;

final class zzcaq implements zzdgt<zzbdi> {
    private final /* synthetic */ String zzfql;
    private final /* synthetic */ zzafn zzfqm;

    zzcaq(zzcaj zzcaj, String str, zzafn zzafn) {
        this.zzfql = str;
        this.zzfqm = zzafn;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        ((zzbdi) obj).zzb(this.zzfql, (zzafn<? super zzbdi>) this.zzfqm);
    }

    public final void zzb(Throwable th) {
    }
}
