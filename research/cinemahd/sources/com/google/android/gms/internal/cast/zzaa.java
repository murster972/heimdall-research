package com.google.android.gms.internal.cast;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import com.google.android.gms.cast.framework.media.ImageHints;

public final class zzaa implements zzae {
    private final Context zzhe;
    private final ImageHints zzmb;
    private Uri zzqs;
    private zzac zzqt;
    private zzaf zzqu;
    private Bitmap zzqv;
    private boolean zzqw;
    private zzab zzqx;

    public zzaa(Context context) {
        this(context, new ImageHints(-1, 0, 0));
    }

    private final void reset() {
        zzac zzac = this.zzqt;
        if (zzac != null) {
            zzac.cancel(true);
            this.zzqt = null;
        }
        this.zzqs = null;
        this.zzqv = null;
        this.zzqw = false;
    }

    public final void clear() {
        reset();
        this.zzqx = null;
    }

    public final void onPostExecute(Bitmap bitmap) {
        this.zzqv = bitmap;
        this.zzqw = true;
        zzab zzab = this.zzqx;
        if (zzab != null) {
            zzab.zza(this.zzqv);
        }
        this.zzqt = null;
    }

    public final void zza(zzab zzab) {
        this.zzqx = zzab;
    }

    public zzaa(Context context, ImageHints imageHints) {
        this.zzhe = context;
        this.zzmb = imageHints;
        this.zzqu = new zzaf();
        reset();
    }

    public final boolean zza(Uri uri) {
        if (uri == null) {
            reset();
            return true;
        } else if (!uri.equals(this.zzqs)) {
            reset();
            this.zzqs = uri;
            if (this.zzmb.u() == 0 || this.zzmb.s() == 0) {
                this.zzqt = new zzac(this.zzhe, this);
            } else {
                this.zzqt = new zzac(this.zzhe, this.zzmb.u(), this.zzmb.s(), false, this);
            }
            zzac zzac = this.zzqt;
            Uri uri2 = this.zzqs;
            zzac.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Uri[]{uri2});
            return false;
        } else if (this.zzqw) {
            return true;
        } else {
            return false;
        }
    }
}
