package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.AdFormat;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationBannerAdConfiguration;
import com.google.android.gms.ads.mediation.MediationConfiguration;
import com.google.android.gms.ads.mediation.MediationInterstitialAd;
import com.google.android.gms.ads.mediation.MediationInterstitialAdConfiguration;
import com.google.android.gms.ads.mediation.MediationNativeAdConfiguration;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.google.android.gms.ads.mediation.rtb.RtbAdapter;
import com.google.android.gms.ads.mediation.rtb.RtbSignalData;
import com.google.android.gms.ads.mediation.zza;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzann extends zzanh {
    /* access modifiers changed from: private */
    public MediationRewardedAd zzddr;
    private final RtbAdapter zzdek;
    /* access modifiers changed from: private */
    public MediationInterstitialAd zzdel;
    private String zzdem = "";

    public zzann(RtbAdapter rtbAdapter) {
        this.zzdek = rtbAdapter;
    }

    private static boolean zzc(zzug zzug) {
        if (zzug.zzccb) {
            return true;
        }
        zzve.zzou();
        return zzayk.zzxd();
    }

    private final Bundle zzd(zzug zzug) {
        Bundle bundle;
        Bundle bundle2 = zzug.zzccf;
        if (bundle2 == null || (bundle = bundle2.getBundle(this.zzdek.getClass().getName())) == null) {
            return new Bundle();
        }
        return bundle;
    }

    private static Bundle zzdo(String str) throws RemoteException {
        String valueOf = String.valueOf(str);
        zzayu.zzez(valueOf.length() != 0 ? "Server parameters: ".concat(valueOf) : new String("Server parameters: "));
        try {
            Bundle bundle = new Bundle();
            if (str == null) {
                return bundle;
            }
            JSONObject jSONObject = new JSONObject(str);
            Bundle bundle2 = new Bundle();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                bundle2.putString(next, jSONObject.getString(next));
            }
            return bundle2;
        } catch (JSONException e) {
            zzayu.zzc("", e);
            throw new RemoteException();
        }
    }

    public final zzxb getVideoController() {
        RtbAdapter rtbAdapter = this.zzdek;
        if (!(rtbAdapter instanceof zza)) {
            return null;
        }
        try {
            return ((zza) rtbAdapter).getVideoController();
        } catch (Throwable th) {
            zzayu.zzc("", th);
            return null;
        }
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzamw zzamw, zzali zzali, zzuj zzuj) throws RemoteException {
        zzug zzug2 = zzug;
        zzuj zzuj2 = zzuj;
        try {
            zzanq zzanq = new zzanq(this, zzamw, zzali);
            RtbAdapter rtbAdapter = this.zzdek;
            Bundle zzdo = zzdo(str2);
            Bundle zzd = zzd(zzug2);
            boolean zzc = zzc(zzug);
            Location location = zzug2.zzmi;
            int i = zzug2.zzabo;
            int i2 = zzug2.zzabp;
            String zza = zza(str2, zzug);
            AdSize zza2 = zzb.zza(zzuj2.width, zzuj2.height, zzuj2.zzabg);
            String str3 = this.zzdem;
            MediationBannerAdConfiguration mediationBannerAdConfiguration = r5;
            MediationBannerAdConfiguration mediationBannerAdConfiguration2 = new MediationBannerAdConfiguration((Context) ObjectWrapper.a(iObjectWrapper), str, zzdo, zzd, zzc, location, i, i2, zza, zza2, str3);
            rtbAdapter.loadBannerAd(mediationBannerAdConfiguration, zzanq);
        } catch (Throwable th) {
            zzayu.zzc("Adapter failed to render banner ad.", th);
            throw new RemoteException();
        }
    }

    public final void zza(String[] strArr, Bundle[] bundleArr) {
    }

    public final boolean zzaa(IObjectWrapper iObjectWrapper) throws RemoteException {
        MediationRewardedAd mediationRewardedAd = this.zzddr;
        if (mediationRewardedAd == null) {
            return false;
        }
        try {
            mediationRewardedAd.showAd((Context) ObjectWrapper.a(iObjectWrapper));
            return true;
        } catch (Throwable th) {
            zzayu.zzc("", th);
            return true;
        }
    }

    public final void zzdm(String str) {
        this.zzdem = str;
    }

    public final zzanw zztc() throws RemoteException {
        return zzanw.zza(this.zzdek.getVersionInfo());
    }

    public final zzanw zztd() throws RemoteException {
        return zzanw.zza(this.zzdek.getSDKVersionInfo());
    }

    public final void zzy(IObjectWrapper iObjectWrapper) {
    }

    public final boolean zzz(IObjectWrapper iObjectWrapper) throws RemoteException {
        MediationInterstitialAd mediationInterstitialAd = this.zzdel;
        if (mediationInterstitialAd == null) {
            return false;
        }
        try {
            mediationInterstitialAd.showAd((Context) ObjectWrapper.a(iObjectWrapper));
            return true;
        } catch (Throwable th) {
            zzayu.zzc("", th);
            return true;
        }
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzamx zzamx, zzali zzali) throws RemoteException {
        zzug zzug2 = zzug;
        try {
            this.zzdek.loadInterstitialAd(new MediationInterstitialAdConfiguration((Context) ObjectWrapper.a(iObjectWrapper), str, zzdo(str2), zzd(zzug2), zzc(zzug), zzug2.zzmi, zzug2.zzabo, zzug2.zzabp, zza(str2, zzug), this.zzdem), new zzanp(this, zzamx, zzali));
        } catch (Throwable th) {
            zzayu.zzc("Adapter failed to render interstitial ad.", th);
            throw new RemoteException();
        }
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzand zzand, zzali zzali) throws RemoteException {
        zzug zzug2 = zzug;
        try {
            this.zzdek.loadRewardedAd(new MediationRewardedAdConfiguration((Context) ObjectWrapper.a(iObjectWrapper), str, zzdo(str2), zzd(zzug2), zzc(zzug), zzug2.zzmi, zzug2.zzabo, zzug2.zzabp, zza(str2, zzug), this.zzdem), new zzans(this, zzand, zzali));
        } catch (Throwable th) {
            zzayu.zzc("Adapter failed to render rewarded ad.", th);
            throw new RemoteException();
        }
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzanc zzanc, zzali zzali) throws RemoteException {
        zzug zzug2 = zzug;
        try {
            this.zzdek.loadNativeAd(new MediationNativeAdConfiguration((Context) ObjectWrapper.a(iObjectWrapper), str, zzdo(str2), zzd(zzug2), zzc(zzug), zzug2.zzmi, zzug2.zzabo, zzug2.zzabp, zza(str2, zzug), this.zzdem), new zzanr(this, zzanc, zzali));
        } catch (Throwable th) {
            zzayu.zzc("Adapter failed to render rewarded ad.", th);
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, String str, Bundle bundle, Bundle bundle2, zzuj zzuj, zzanj zzanj) throws RemoteException {
        AdFormat adFormat;
        try {
            zzanu zzanu = new zzanu(this, zzanj);
            RtbAdapter rtbAdapter = this.zzdek;
            char c = 65535;
            switch (str.hashCode()) {
                case -1396342996:
                    if (str.equals("banner")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1052618729:
                    if (str.equals("native")) {
                        c = 3;
                        break;
                    }
                    break;
                case -239580146:
                    if (str.equals("rewarded")) {
                        c = 2;
                        break;
                    }
                    break;
                case 604727084:
                    if (str.equals("interstitial")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                adFormat = AdFormat.BANNER;
            } else if (c == 1) {
                adFormat = AdFormat.INTERSTITIAL;
            } else if (c == 2) {
                adFormat = AdFormat.REWARDED;
            } else if (c == 3) {
                adFormat = AdFormat.NATIVE;
            } else {
                throw new IllegalArgumentException("Internal Error");
            }
            MediationConfiguration mediationConfiguration = new MediationConfiguration(adFormat, bundle2);
            ArrayList arrayList = new ArrayList();
            arrayList.add(mediationConfiguration);
            rtbAdapter.collectSignals(new RtbSignalData((Context) ObjectWrapper.a(iObjectWrapper), arrayList, bundle, zzb.zza(zzuj.width, zzuj.height, zzuj.zzabg)), zzanu);
        } catch (Throwable th) {
            zzayu.zzc("Error generating signals for RTB", th);
            throw new RemoteException();
        }
    }

    private static String zza(String str, zzug zzug) {
        try {
            return new JSONObject(str).getString("max_ad_content_rating");
        } catch (JSONException unused) {
            return zzug.zzabq;
        }
    }
}
