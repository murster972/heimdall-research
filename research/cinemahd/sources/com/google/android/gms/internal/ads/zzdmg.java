package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdmg extends zzdrt<zzdmg, zza> implements zzdtg {
    private static volatile zzdtn<zzdmg> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmg zzhbl;
    private zzdmj zzhbk;

    public static final class zza extends zzdrt.zzb<zzdmg, zza> implements zzdtg {
        private zza() {
            super(zzdmg.zzhbl);
        }

        /* synthetic */ zza(zzdmh zzdmh) {
            this();
        }
    }

    static {
        zzdmg zzdmg = new zzdmg();
        zzhbl = zzdmg;
        zzdrt.zza(zzdmg.class, zzdmg);
    }

    private zzdmg() {
    }

    public static zzdmg zzak(zzdqk zzdqk) throws zzdse {
        return (zzdmg) zzdrt.zza(zzhbl, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmh.zzdk[i - 1]) {
            case 1:
                return new zzdmg();
            case 2:
                return new zza((zzdmh) null);
            case 3:
                return zzdrt.zza((zzdte) zzhbl, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t", new Object[]{"zzhbk"});
            case 4:
                return zzhbl;
            case 5:
                zzdtn<zzdmg> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmg.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbl);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdmj zzauf() {
        zzdmj zzdmj = this.zzhbk;
        return zzdmj == null ? zzdmj.zzauk() : zzdmj;
    }
}
