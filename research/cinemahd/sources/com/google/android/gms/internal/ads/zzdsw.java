package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzdsw<K, V> {
    static <K, V> void zza(zzdrb zzdrb, zzdsv<K, V> zzdsv, K k, V v) throws IOException {
        zzdrm.zza(zzdrb, zzdsv.zzhoq, 1, k);
        zzdrm.zza(zzdrb, zzdsv.zzhos, 2, v);
    }

    static <K, V> int zza(zzdsv<K, V> zzdsv, K k, V v) {
        return zzdrm.zza(zzdsv.zzhoq, 1, k) + zzdrm.zza(zzdsv.zzhos, 2, v);
    }
}
