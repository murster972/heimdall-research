package com.google.android.gms.internal.ads;

import java.io.IOException;

public interface zzbe {
    zzbf zza(zzdws zzdws, zzbi zzbi) throws IOException;
}
