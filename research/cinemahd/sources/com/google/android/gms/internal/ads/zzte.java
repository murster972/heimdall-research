package com.google.android.gms.internal.ads;

public enum zzte implements zzdry {
    ENUM_FALSE(0),
    ENUM_TRUE(1),
    ENUM_UNKNOWN(1000);
    
    private static final zzdrx<zzte> zzen = null;
    private final int value;

    static {
        zzen = new zztd();
    }

    private zzte(int i) {
        this.value = i;
    }

    public static zzdsa zzaf() {
        return zztf.zzew;
    }

    public static zzte zzbx(int i) {
        if (i == 0) {
            return ENUM_FALSE;
        }
        if (i == 1) {
            return ENUM_TRUE;
        }
        if (i != 1000) {
            return null;
        }
        return ENUM_UNKNOWN;
    }

    public final String toString() {
        return "<" + zzte.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
    }

    public final int zzae() {
        return this.value;
    }
}
