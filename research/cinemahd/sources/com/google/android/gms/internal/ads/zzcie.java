package com.google.android.gms.internal.ads;

public final class zzcie implements zzdxg<zzcib> {
    private final zzdxp<zzchz> zzfxr;

    private zzcie(zzdxp<zzchz> zzdxp) {
        this.zzfxr = zzdxp;
    }

    public static zzcie zzae(zzdxp<zzchz> zzdxp) {
        return new zzcie(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcib(this.zzfxr.get());
    }
}
