package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzddh extends zzdrt<zzddh, zzb> implements zzdtg {
    private static volatile zzdtn<zzddh> zzdz;
    private static final zzdsc<Integer, zza> zzgsj = new zzddj();
    /* access modifiers changed from: private */
    public static final zzddh zzgsn;
    private int zzdl;
    private zzdrz zzgsi = zzdrt.zzazv();
    private String zzgsk = "";
    private String zzgsl = "";
    private String zzgsm = "";

    public enum zza implements zzdry {
        BLOCKED_REASON_UNKNOWN(1),
        BLOCKED_REASON_BACKGROUND(2);
        
        private static final zzdrx<zza> zzen = null;
        private final int value;

        static {
            zzen = new zzddk();
        }

        private zza(int i) {
            this.value = i;
        }

        public static zzdsa zzaf() {
            return zzddl.zzew;
        }

        public static zza zzdp(int i) {
            if (i == 1) {
                return BLOCKED_REASON_UNKNOWN;
            }
            if (i != 2) {
                return null;
            }
            return BLOCKED_REASON_BACKGROUND;
        }

        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
        }

        public final int zzae() {
            return this.value;
        }
    }

    public static final class zzb extends zzdrt.zzb<zzddh, zzb> implements zzdtg {
        private zzb() {
            super(zzddh.zzgsn);
        }

        public final zzb zzb(zza zza) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzddh) this.zzhmp).zza(zza);
            return this;
        }

        public final zzb zzgs(String str) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzddh) this.zzhmp).zzgr(str);
            return this;
        }

        /* synthetic */ zzb(zzddj zzddj) {
            this();
        }
    }

    static {
        zzddh zzddh = new zzddh();
        zzgsn = zzddh;
        zzdrt.zza(zzddh.class, zzddh);
    }

    private zzddh() {
    }

    /* access modifiers changed from: private */
    public final void zza(zza zza2) {
        zza2.getClass();
        if (!this.zzgsi.zzaxp()) {
            this.zzgsi = zzdrt.zza(this.zzgsi);
        }
        this.zzgsi.zzgl(zza2.zzae());
    }

    public static zzb zzaqk() {
        return (zzb) zzgsn.zzazt();
    }

    /* access modifiers changed from: private */
    public final void zzgr(String str) {
        str.getClass();
        this.zzdl |= 1;
        this.zzgsk = str;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzddi.zzdk[i - 1]) {
            case 1:
                return new zzddh();
            case 2:
                return new zzb((zzddj) null);
            case 3:
                return zzdrt.zza((zzdte) zzgsn, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u001e\u0002\b\u0000\u0003\b\u0001\u0004\b\u0002", new Object[]{"zzdl", "zzgsi", zza.zzaf(), "zzgsk", "zzgsl", "zzgsm"});
            case 4:
                return zzgsn;
            case 5:
                zzdtn<zzddh> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzddh.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzgsn);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
