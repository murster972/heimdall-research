package com.google.android.gms.internal.ads;

import java.util.Map;

public final class zzbvf implements zzdxg<zzbvg> {
    private final zzdxp<zzbwz> zzfeh;
    private final zzdxp<Map<String, zzcio<zzbmj>>> zzffv;
    private final zzdxp<Map<String, zzcio<zzbwk>>> zzfjq;
    private final zzdxp<Map<String, zzckr<zzbwk>>> zzfjr;
    private final zzdxp<zzbmi<zzbkk>> zzfjs;

    public zzbvf(zzdxp<Map<String, zzcio<zzbmj>>> zzdxp, zzdxp<Map<String, zzcio<zzbwk>>> zzdxp2, zzdxp<Map<String, zzckr<zzbwk>>> zzdxp3, zzdxp<zzbmi<zzbkk>> zzdxp4, zzdxp<zzbwz> zzdxp5) {
        this.zzffv = zzdxp;
        this.zzfjq = zzdxp2;
        this.zzfjr = zzdxp3;
        this.zzfjs = zzdxp4;
        this.zzfeh = zzdxp5;
    }

    public final /* synthetic */ Object get() {
        return new zzbvg(this.zzffv.get(), this.zzfjq.get(), this.zzfjr.get(), this.zzfjs, this.zzfeh.get());
    }
}
