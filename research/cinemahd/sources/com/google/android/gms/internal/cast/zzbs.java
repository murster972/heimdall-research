package com.google.android.gms.internal.cast;

import android.text.format.DateUtils;
import android.widget.TextView;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

public final class zzbs extends zzbt implements RemoteMediaClient.ProgressListener {
    private boolean zzri = true;
    private final long zztc;
    private final TextView zzto;
    private final String zztr;

    public zzbs(TextView textView, long j, String str) {
        this.zzto = textView;
        this.zztc = j;
        this.zztr = str;
    }

    public final void onProgressUpdated(long j, long j2) {
        if (zzee()) {
            TextView textView = this.zzto;
            if (j == -1000) {
                j = j2;
            }
            textView.setText(DateUtils.formatElapsedTime(j / 1000));
        }
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient != null) {
            remoteMediaClient.a((RemoteMediaClient.ProgressListener) this, this.zztc);
            if (remoteMediaClient.k()) {
                this.zzto.setText(DateUtils.formatElapsedTime(remoteMediaClient.b() / 1000));
            } else {
                this.zzto.setText(this.zztr);
            }
        }
    }

    public final void onSessionEnded() {
        this.zzto.setText(this.zztr);
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this);
        }
        super.onSessionEnded();
    }

    public final boolean zzee() {
        return this.zzri;
    }

    public final void zzg(long j) {
        this.zzto.setText(DateUtils.formatElapsedTime(j / 1000));
    }

    public final void zzk(boolean z) {
        this.zzri = z;
    }
}
