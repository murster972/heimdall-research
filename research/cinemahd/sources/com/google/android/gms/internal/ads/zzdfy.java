package com.google.android.gms.internal.ads;

final class zzdfy implements Runnable {
    private final /* synthetic */ zzdhe zzgvy;
    private final /* synthetic */ int zzgvz;
    private final /* synthetic */ zzdfz zzgwa;

    zzdfy(zzdfz zzdfz, zzdhe zzdhe, int i) {
        this.zzgwa = zzdfz;
        this.zzgvy = zzdhe;
        this.zzgvz = i;
    }

    public final void run() {
        try {
            if (this.zzgvy.isCancelled()) {
                zzdet unused = this.zzgwa.zzgwb = null;
                this.zzgwa.cancel(false);
            } else {
                this.zzgwa.zza(this.zzgvz, this.zzgvy);
            }
        } finally {
            this.zzgwa.zza((zzdet) null);
        }
    }
}
