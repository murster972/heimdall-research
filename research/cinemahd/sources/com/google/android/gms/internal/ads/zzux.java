package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

final class zzux extends zzvb<zzwk> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ zzup zzcdi;

    zzux(zzup zzup, Context context) {
        this.zzcdi = zzup;
        this.val$context = context;
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.a(this.val$context), 19649000);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, "mobile_ads_settings");
        return new zzyf();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzccz.zzi(this.val$context);
    }
}
