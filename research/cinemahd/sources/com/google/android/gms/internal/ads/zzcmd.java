package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;

public final class zzcmd extends zzcnd {
    private zzbte zzgam;
    private zzboz zzgan;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzcmd(zzboq zzboq, zzbpd zzbpd, zzbpm zzbpm, zzbpw zzbpw, zzboz zzboz, zzbra zzbra, zzbtj zzbtj, zzbqj zzbqj, zzbte zzbte) {
        super(zzboq, zzbpd, zzbpm, zzbpw, zzbra, zzbqj, zzbtj);
        this.zzgam = zzbte;
        this.zzgan = zzboz;
    }

    public final void onVideoEnd() {
        this.zzgam.zzrt();
    }

    public final void zza(zzasf zzasf) throws RemoteException {
        this.zzgam.zza(new zzasd(zzasf.getType(), zzasf.getAmount()));
    }

    public final void zzb(Bundle bundle) throws RemoteException {
    }

    public final void zzb(zzasd zzasd) {
        this.zzgam.zza(zzasd);
    }

    public final void zzco(int i) throws RemoteException {
        this.zzgan.zzco(i);
    }

    public final void zzss() {
        this.zzgam.zzrs();
    }

    public final void zzst() throws RemoteException {
        this.zzgam.zzrt();
    }
}
