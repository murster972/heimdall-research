package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import java.util.Set;

public final class zzbsb implements zzdxg<Set<zzbsu<AppEventListener>>> {
    private final zzbrm zzfim;

    private zzbsb(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbsb zzs(zzbrm zzbrm) {
        return new zzbsb(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfim.zzahr(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
