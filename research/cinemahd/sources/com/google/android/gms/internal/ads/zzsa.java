package com.google.android.gms.internal.ads;

import android.content.Context;
import org.json.JSONObject;

public interface zzsa {
    JSONObject zzf(Context context);
}
