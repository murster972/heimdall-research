package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcaf implements zzdxg<zzcad> {
    private final zzdxp<zzbxj> zzetm;
    private final zzdxp<Context> zzfhb;
    private final zzdxp<zzbws> zzfkx;
    private final zzdxp<zzbwk> zzfqc;

    private zzcaf(zzdxp<Context> zzdxp, zzdxp<zzbws> zzdxp2, zzdxp<zzbxj> zzdxp3, zzdxp<zzbwk> zzdxp4) {
        this.zzfhb = zzdxp;
        this.zzfkx = zzdxp2;
        this.zzetm = zzdxp3;
        this.zzfqc = zzdxp4;
    }

    public static zzcaf zzb(zzdxp<Context> zzdxp, zzdxp<zzbws> zzdxp2, zzdxp<zzbxj> zzdxp3, zzdxp<zzbwk> zzdxp4) {
        return new zzcaf(zzdxp, zzdxp2, zzdxp3, zzdxp4);
    }

    public final /* synthetic */ Object get() {
        return new zzcad(this.zzfhb.get(), this.zzfkx.get(), this.zzetm.get(), this.zzfqc.get());
    }
}
