package com.google.android.gms.internal.ads;

public abstract class zzder {
    protected zzder() {
    }

    public String toString() {
        return zzaqw().toString();
    }

    /* access modifiers changed from: protected */
    public abstract Object zzaqw();
}
