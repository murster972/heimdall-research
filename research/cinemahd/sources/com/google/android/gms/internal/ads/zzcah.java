package com.google.android.gms.internal.ads;

public final class zzcah implements zzdxg<zzcai> {
    private final zzdxp<String> zzfhl;
    private final zzdxp<zzbws> zzfkx;
    private final zzdxp<zzbwk> zzfqc;

    private zzcah(zzdxp<String> zzdxp, zzdxp<zzbwk> zzdxp2, zzdxp<zzbws> zzdxp3) {
        this.zzfhl = zzdxp;
        this.zzfqc = zzdxp2;
        this.zzfkx = zzdxp3;
    }

    public static zzcah zzk(zzdxp<String> zzdxp, zzdxp<zzbwk> zzdxp2, zzdxp<zzbws> zzdxp3) {
        return new zzcah(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzcai(this.zzfhl.get(), this.zzfqc.get(), this.zzfkx.get());
    }
}
