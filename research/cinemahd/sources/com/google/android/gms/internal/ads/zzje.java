package com.google.android.gms.internal.ads;

import com.google.ar.core.ImageMetadata;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

public final class zzje implements zzjg {
    private static final byte[] zzamt = new byte[4096];
    private final zznl zzamu;
    private final long zzamv;
    private long zzamw;
    private byte[] zzamx = new byte[ImageMetadata.CONTROL_AE_ANTIBANDING_MODE];
    private int zzamy;
    private int zzamz;

    public zzje(zznl zznl, long j, long j2) {
        this.zzamu = zznl;
        this.zzamw = j;
        this.zzamv = j2;
    }

    private final int zzae(int i) {
        int min = Math.min(this.zzamz, i);
        zzaf(min);
        return min;
    }

    private final void zzaf(int i) {
        this.zzamz -= i;
        this.zzamy = 0;
        byte[] bArr = this.zzamx;
        int i2 = this.zzamz;
        if (i2 < bArr.length - ImageMetadata.LENS_APERTURE) {
            bArr = new byte[(i2 + ImageMetadata.CONTROL_AE_ANTIBANDING_MODE)];
        }
        System.arraycopy(this.zzamx, i, bArr, 0, this.zzamz);
        this.zzamx = bArr;
    }

    private final void zzag(int i) {
        if (i != -1) {
            this.zzamw += (long) i;
        }
    }

    private final int zzb(byte[] bArr, int i, int i2) {
        int i3 = this.zzamz;
        if (i3 == 0) {
            return 0;
        }
        int min = Math.min(i3, i2);
        System.arraycopy(this.zzamx, 0, bArr, i, min);
        zzaf(min);
        return min;
    }

    private final boolean zzd(int i, boolean z) throws IOException, InterruptedException {
        int i2 = this.zzamy + i;
        byte[] bArr = this.zzamx;
        if (i2 > bArr.length) {
            this.zzamx = Arrays.copyOf(this.zzamx, zzoq.zzd(bArr.length << 1, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE + i2, i2 + ImageMetadata.LENS_APERTURE));
        }
        int min = Math.min(this.zzamz - this.zzamy, i);
        while (min < i) {
            min = zza(this.zzamx, this.zzamy, i, min, false);
            if (min == -1) {
                return false;
            }
        }
        this.zzamy += i;
        this.zzamz = Math.max(this.zzamz, this.zzamy);
        return true;
    }

    public final long getLength() {
        return this.zzamv;
    }

    public final long getPosition() {
        return this.zzamw;
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException, InterruptedException {
        int zzb = zzb(bArr, i, i2);
        if (zzb == 0) {
            zzb = zza(bArr, i, i2, 0, true);
        }
        zzag(zzb);
        return zzb;
    }

    public final void readFully(byte[] bArr, int i, int i2) throws IOException, InterruptedException {
        zza(bArr, i, i2, false);
    }

    public final boolean zza(byte[] bArr, int i, int i2, boolean z) throws IOException, InterruptedException {
        int zzb = zzb(bArr, i, i2);
        while (zzb < i2 && zzb != -1) {
            zzb = zza(bArr, i, i2, zzb, z);
        }
        zzag(zzb);
        return zzb != -1;
    }

    public final int zzab(int i) throws IOException, InterruptedException {
        int zzae = zzae(i);
        if (zzae == 0) {
            byte[] bArr = zzamt;
            zzae = zza(bArr, 0, Math.min(i, bArr.length), 0, true);
        }
        zzag(zzae);
        return zzae;
    }

    public final void zzac(int i) throws IOException, InterruptedException {
        int zzae = zzae(i);
        while (zzae < i && zzae != -1) {
            byte[] bArr = zzamt;
            zzae = zza(bArr, -zzae, Math.min(i, bArr.length + zzae), zzae, false);
        }
        zzag(zzae);
    }

    public final void zzad(int i) throws IOException, InterruptedException {
        zzd(i, false);
    }

    public final void zzgi() {
        this.zzamy = 0;
    }

    public final void zza(byte[] bArr, int i, int i2) throws IOException, InterruptedException {
        if (zzd(i2, false)) {
            System.arraycopy(this.zzamx, this.zzamy - i2, bArr, i, i2);
        }
    }

    private final int zza(byte[] bArr, int i, int i2, int i3, boolean z) throws InterruptedException, IOException {
        if (!Thread.interrupted()) {
            int read = this.zzamu.read(bArr, i + i3, i2 - i3);
            if (read != -1) {
                return i3 + read;
            }
            if (i3 == 0 && z) {
                return -1;
            }
            throw new EOFException();
        }
        throw new InterruptedException();
    }
}
