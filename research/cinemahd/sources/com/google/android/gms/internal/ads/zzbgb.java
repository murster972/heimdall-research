package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

public final class zzbgb implements zzdxg<zzave> {
    private final zzbga zzejr;

    public zzbgb(zzbga zzbga) {
        this.zzejr = zzbga;
    }

    public final /* synthetic */ Object get() {
        return (zzave) zzdxm.zza(zzq.zzku(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
