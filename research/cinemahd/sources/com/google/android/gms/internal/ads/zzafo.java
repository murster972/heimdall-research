package com.google.android.gms.internal.ads;

import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzq;
import java.util.HashMap;
import java.util.Map;

final class zzafo implements zzafn<zzbdi> {
    zzafo() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        zzq.zzkq();
        DisplayMetrics zza = zzawb.zza((WindowManager) zzbdi.getContext().getSystemService("window"));
        int i = zza.widthPixels;
        int i2 = zza.heightPixels;
        int[] iArr = new int[2];
        HashMap hashMap = new HashMap();
        ((View) zzbdi).getLocationInWindow(iArr);
        hashMap.put("xInPixels", Integer.valueOf(iArr[0]));
        hashMap.put("yInPixels", Integer.valueOf(iArr[1]));
        hashMap.put("windowWidthInPixels", Integer.valueOf(i));
        hashMap.put("windowHeightInPixels", Integer.valueOf(i2));
        zzbdi.zza("locationReady", hashMap);
        zzayu.zzez("GET LOCATION COMPILED");
    }
}
