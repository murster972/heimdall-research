package com.google.android.gms.internal.ads;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

abstract class zzdgi<T> extends zzdha<T> {
    private final Executor zzgwn;
    boolean zzgwo = true;
    private final /* synthetic */ zzdgg zzgwp;

    zzdgi(zzdgg zzdgg, Executor executor) {
        this.zzgwp = zzdgg;
        this.zzgwn = (Executor) zzdei.checkNotNull(executor);
    }

    /* access modifiers changed from: package-private */
    public final void execute() {
        try {
            this.zzgwn.execute(this);
        } catch (RejectedExecutionException e) {
            if (this.zzgwo) {
                this.zzgwp.setException(e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean isDone() {
        return this.zzgwp.isDone();
    }

    /* access modifiers changed from: package-private */
    public abstract void setValue(T t);

    /* access modifiers changed from: package-private */
    public final void zzb(T t, Throwable th) {
        zzdgi unused = this.zzgwp.zzgwm = null;
        if (th == null) {
            setValue(t);
        } else if (th instanceof ExecutionException) {
            this.zzgwp.setException(th.getCause());
        } else if (th instanceof CancellationException) {
            this.zzgwp.cancel(false);
        } else {
            this.zzgwp.setException(th);
        }
    }
}
