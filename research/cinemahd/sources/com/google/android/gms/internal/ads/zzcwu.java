package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcwu implements zzdxg<zzcwr> {
    private final zzdxp<zzbfx> zzejt;
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<String> zzgja;
    private final zzdxp<zzcwl> zzgjb;
    private final zzdxp<zzcwz> zzgjc;

    public zzcwu(zzdxp<zzbfx> zzdxp, zzdxp<Context> zzdxp2, zzdxp<String> zzdxp3, zzdxp<zzcwl> zzdxp4, zzdxp<zzcwz> zzdxp5, zzdxp<zzazb> zzdxp6) {
        this.zzejt = zzdxp;
        this.zzejv = zzdxp2;
        this.zzgja = zzdxp3;
        this.zzgjb = zzdxp4;
        this.zzgjc = zzdxp5;
        this.zzfdb = zzdxp6;
    }

    public final /* synthetic */ Object get() {
        return new zzcwr(this.zzejt.get(), this.zzejv.get(), this.zzgja.get(), this.zzgjb.get(), this.zzgjc.get(), this.zzfdb.get());
    }
}
