package com.google.android.gms.internal.ads;

public final class zzdat implements zzdxg<zzavu> {
    private final zzdaq zzgne;
    private final zzdxp<zzdao> zzgng;

    private zzdat(zzdaq zzdaq, zzdxp<zzdao> zzdxp) {
        this.zzgne = zzdaq;
        this.zzgng = zzdxp;
    }

    public static zzdat zza(zzdaq zzdaq, zzdxp<zzdao> zzdxp) {
        return new zzdat(zzdaq, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzgne, this.zzgng.get());
    }

    public static zzavu zza(zzdaq zzdaq, zzdao zzdao) {
        return (zzavu) zzdxm.zza(zzdao.zzdrk, "Cannot return null from a non-@Nullable @Provides method");
    }
}
