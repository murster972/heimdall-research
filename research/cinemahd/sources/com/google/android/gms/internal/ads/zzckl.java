package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzckl implements zzcir<zzbtu, zzani, zzcjy> {
    private final zzbup zzfyt;
    private final Context zzup;

    public zzckl(Context context, zzbup zzbup) {
        this.zzup = context;
        this.zzfyt = zzbup;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzani, zzcjy> zzcip) throws zzdab {
        try {
            ((zzani) zzcip.zzddn).zzdm(zzczl.zzdem);
            ((zzani) zzcip.zzddn).zza(zzczl.zzeif, zzczl.zzglr.toString(), zzczt.zzgmh.zzfgl.zzgml, ObjectWrapper.a(this.zzup), (zzamx) new zzckq(this, zzcip), (zzali) zzcip.zzfyf);
        } catch (RemoteException e) {
            throw new zzdab(e);
        }
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzciq zzciq = new zzciq(zzczl);
        zzbtw zza = this.zzfyt.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzbtv(new zzcko(zzcip, zzciq)));
        zzciq.zza(zza.zzadk());
        ((zzcjy) zzcip.zzfyf).zza((zzali) zza.zzadm());
        return zza.zzaem();
    }
}
