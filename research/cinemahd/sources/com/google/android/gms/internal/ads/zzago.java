package com.google.android.gms.internal.ads;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzago implements BaseGmsClient.BaseOnConnectionFailedListener {
    private final /* synthetic */ zzazl zzbrs;

    zzago(zzagh zzagh, zzazl zzazl) {
        this.zzbrs = zzazl;
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.zzbrs.setException(new RuntimeException("Connection failed."));
    }
}
