package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzctx implements zzcub<zzcty<Bundle>> {
    private final boolean zzggy;

    zzctx(zzcxw zzcxw) {
        if (zzcxw != null) {
            this.zzggy = true;
        } else {
            this.zzggy = false;
        }
    }

    public final zzdhe<zzcty<Bundle>> zzanc() {
        return zzdgs.zzaj(this.zzggy ? zzctw.zzggx : null);
    }
}
