package com.google.android.gms.internal.ads;

final class zzdcm implements zzdgt<O> {
    private final /* synthetic */ zzdca zzgqi;
    private final /* synthetic */ zzdcj zzgqj;

    zzdcm(zzdcj zzdcj, zzdca zzdca) {
        this.zzgqj = zzdcj;
        this.zzgqi = zzdca;
    }

    public final void onSuccess(O o) {
        this.zzgqj.zzgqd.zzgqb.zzc(this.zzgqi);
    }

    public final void zzb(Throwable th) {
        this.zzgqj.zzgqd.zzgqb.zza(this.zzgqi, th);
    }
}
