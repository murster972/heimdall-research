package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.instream.InstreamAd;
import com.google.android.gms.ads.instream.InstreamAdView;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzahm extends InstreamAd {
    private final VideoController zzcel = zzrx();
    private final zzahb zzcyk;

    public zzahm(zzahb zzahb) {
        this.zzcyk = zzahb;
    }

    private final VideoController zzrx() {
        VideoController videoController = new VideoController();
        try {
            videoController.zza(this.zzcyk.getVideoController());
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        return videoController;
    }

    public final void destroy() {
        try {
            this.zzcyk.destroy();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final float getAspectRatio() {
        VideoController videoController = this.zzcel;
        if (videoController == null) {
            return 0.0f;
        }
        return videoController.getAspectRatio();
    }

    public final VideoController getVideoController() {
        return this.zzcel;
    }

    public final float getVideoCurrentTime() {
        VideoController videoController = this.zzcel;
        if (videoController == null) {
            return 0.0f;
        }
        return videoController.getVideoCurrentTime();
    }

    public final float getVideoDuration() {
        VideoController videoController = this.zzcel;
        if (videoController == null) {
            return 0.0f;
        }
        return videoController.getVideoDuration();
    }

    public final void zza(InstreamAdView instreamAdView) {
        if (instreamAdView == null) {
            zzayu.zzex("showInView: parameter view must not be null.");
            return;
        }
        try {
            this.zzcyk.zzr(ObjectWrapper.a(instreamAdView));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
