package com.google.android.gms.internal.ads;

public final class zzbug implements zzdxg<zzbsu<zzafx>> {
    private final zzdxp<zzcaz> zzfdq;
    private final zzbtv zzfje;

    public zzbug(zzbtv zzbtv, zzdxp<zzcaz> zzdxp) {
        this.zzfje = zzbtv;
        this.zzfdq = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
