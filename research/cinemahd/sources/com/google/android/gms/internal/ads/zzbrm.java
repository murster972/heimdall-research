package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.reward.AdMetadataListener;
import com.google.android.gms.common.util.Clock;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;

public class zzbrm {
    private final zzcxq zzffr;
    private final Set<zzbsu<zzty>> zzfib;
    private final Set<zzbsu<zzbov>> zzfic;
    private final Set<zzbsu<zzbpe>> zzfid;
    private final Set<zzbsu<zzbqg>> zzfie;
    private final Set<zzbsu<zzbqb>> zzfif;
    private final Set<zzbsu<zzbow>> zzfig;
    private final Set<zzbsu<zzbpa>> zzfih;
    private final Set<zzbsu<AdMetadataListener>> zzfii;
    private final Set<zzbsu<AppEventListener>> zzfij;
    private zzbou zzfik;
    private zzclp zzfil;

    public static class zza {
        /* access modifiers changed from: private */
        public zzcxq zzffr;
        /* access modifiers changed from: private */
        public Set<zzbsu<zzty>> zzfib = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<zzbov>> zzfic = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<zzbpe>> zzfid = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<zzbqg>> zzfie = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<zzbqb>> zzfif = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<zzbow>> zzfig = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<zzbpa>> zzfih = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<AdMetadataListener>> zzfii = new HashSet();
        /* access modifiers changed from: private */
        public Set<zzbsu<AppEventListener>> zzfij = new HashSet();

        public final zza zza(zzbov zzbov, Executor executor) {
            this.zzfic.add(new zzbsu(zzbov, executor));
            return this;
        }

        public final zzbrm zzahw() {
            return new zzbrm(this);
        }

        public final zza zza(zzbqb zzbqb, Executor executor) {
            this.zzfif.add(new zzbsu(zzbqb, executor));
            return this;
        }

        public final zza zza(zzbow zzbow, Executor executor) {
            this.zzfig.add(new zzbsu(zzbow, executor));
            return this;
        }

        public final zza zza(zzbpa zzbpa, Executor executor) {
            this.zzfih.add(new zzbsu(zzbpa, executor));
            return this;
        }

        public final zza zza(AdMetadataListener adMetadataListener, Executor executor) {
            this.zzfii.add(new zzbsu(adMetadataListener, executor));
            return this;
        }

        public final zza zza(AppEventListener appEventListener, Executor executor) {
            this.zzfij.add(new zzbsu(appEventListener, executor));
            return this;
        }

        public final zza zza(zzwc zzwc, Executor executor) {
            if (this.zzfij != null) {
                zzcos zzcos = new zzcos();
                zzcos.zzb(zzwc);
                this.zzfij.add(new zzbsu(zzcos, executor));
            }
            return this;
        }

        public final zza zza(zzty zzty, Executor executor) {
            this.zzfib.add(new zzbsu(zzty, executor));
            return this;
        }

        public final zza zza(zzbpe zzbpe, Executor executor) {
            this.zzfid.add(new zzbsu(zzbpe, executor));
            return this;
        }

        public final zza zza(zzbqg zzbqg, Executor executor) {
            this.zzfie.add(new zzbsu(zzbqg, executor));
            return this;
        }

        public final zza zza(zzcxq zzcxq) {
            this.zzffr = zzcxq;
            return this;
        }
    }

    private zzbrm(zza zza2) {
        this.zzfib = zza2.zzfib;
        this.zzfid = zza2.zzfid;
        this.zzfie = zza2.zzfie;
        this.zzfic = zza2.zzfic;
        this.zzfif = zza2.zzfif;
        this.zzfig = zza2.zzfig;
        this.zzfih = zza2.zzfih;
        this.zzfii = zza2.zzfii;
        this.zzfij = zza2.zzfij;
        this.zzffr = zza2.zzffr;
    }

    public final zzclp zza(Clock clock) {
        if (this.zzfil == null) {
            this.zzfil = new zzclp(clock);
        }
        return this.zzfil;
    }

    public final Set<zzbsu<zzbov>> zzahm() {
        return this.zzfic;
    }

    public final Set<zzbsu<zzbqb>> zzahn() {
        return this.zzfif;
    }

    public final Set<zzbsu<zzbow>> zzaho() {
        return this.zzfig;
    }

    public final Set<zzbsu<zzbpa>> zzahp() {
        return this.zzfih;
    }

    public final Set<zzbsu<AdMetadataListener>> zzahq() {
        return this.zzfii;
    }

    public final Set<zzbsu<AppEventListener>> zzahr() {
        return this.zzfij;
    }

    public final Set<zzbsu<zzty>> zzahs() {
        return this.zzfib;
    }

    public final Set<zzbsu<zzbpe>> zzaht() {
        return this.zzfid;
    }

    public final Set<zzbsu<zzbqg>> zzahu() {
        return this.zzfie;
    }

    public final zzcxq zzahv() {
        return this.zzffr;
    }

    public final zzbou zzc(Set<zzbsu<zzbow>> set) {
        if (this.zzfik == null) {
            this.zzfik = new zzbou(set);
        }
        return this.zzfik;
    }
}
