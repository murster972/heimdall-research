package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;

public class zzbod {
    private final zzczu zzfgl;
    private Bundle zzfhf;
    private final String zzfhg;
    private final zzczs zzfhh;
    private final Context zzup;

    public static class zza {
        /* access modifiers changed from: private */
        public zzczu zzfgl;
        /* access modifiers changed from: private */
        public Bundle zzfhf;
        /* access modifiers changed from: private */
        public String zzfhg;
        /* access modifiers changed from: private */
        public zzczs zzfhh;
        /* access modifiers changed from: private */
        public Context zzup;

        public final zza zza(zzczu zzczu) {
            this.zzfgl = zzczu;
            return this;
        }

        public final zzbod zzahh() {
            return new zzbod(this);
        }

        public final zza zzbz(Context context) {
            this.zzup = context;
            return this;
        }

        public final zza zze(Bundle bundle) {
            this.zzfhf = bundle;
            return this;
        }

        public final zza zzfs(String str) {
            this.zzfhg = str;
            return this;
        }

        public final zza zza(zzczs zzczs) {
            this.zzfhh = zzczs;
            return this;
        }
    }

    private zzbod(zza zza2) {
        this.zzup = zza2.zzup;
        this.zzfgl = zza2.zzfgl;
        this.zzfhf = zza2.zzfhf;
        this.zzfhg = zza2.zzfhg;
        this.zzfhh = zza2.zzfhh;
    }

    /* access modifiers changed from: package-private */
    public final zza zzahc() {
        return new zza().zzbz(this.zzup).zza(this.zzfgl).zzfs(this.zzfhg).zze(this.zzfhf);
    }

    /* access modifiers changed from: package-private */
    public final zzczu zzahd() {
        return this.zzfgl;
    }

    /* access modifiers changed from: package-private */
    public final zzczs zzahe() {
        return this.zzfhh;
    }

    /* access modifiers changed from: package-private */
    public final Bundle zzahf() {
        return this.zzfhf;
    }

    /* access modifiers changed from: package-private */
    public final String zzahg() {
        return this.zzfhg;
    }

    /* access modifiers changed from: package-private */
    public final Context zzby(Context context) {
        if (this.zzfhg != null) {
            return context;
        }
        return this.zzup;
    }
}
