package com.google.android.gms.internal.ads;

public final class zzbsf implements zzdxg<zzcxq> {
    private final zzbrm zzfim;

    private zzbsf(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbsf zzw(zzbrm zzbrm) {
        return new zzbsf(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return this.zzfim.zzahv();
    }
}
