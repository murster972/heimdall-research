package com.google.android.gms.internal.ads;

import android.content.Context;

final class zzawd implements Runnable {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ zzawb zzdsw;

    zzawd(zzawb zzawb, Context context) {
        this.zzdsw = zzawb;
        this.val$context = context;
    }

    public final void run() {
        synchronized (this.zzdsw.zzdss) {
            String unused = this.zzdsw.zzbfb = zzawb.zzaq(this.val$context);
            this.zzdsw.zzdss.notifyAll();
        }
    }
}
