package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzboz extends zzbrl<zzbpa> implements zzbpa {
    public zzboz(Set<zzbsu<zzbpa>> set) {
        super(set);
    }

    public final void zzco(int i) {
        zza(new zzboy(i));
    }
}
