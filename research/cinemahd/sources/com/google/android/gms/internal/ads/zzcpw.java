package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.ViewGroup;

public final class zzcpw implements zzdxg<zzcpu> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<ViewGroup> zzgac;
    private final zzdxp<zzdhd> zzgem;

    public zzcpw(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzczu> zzdxp3, zzdxp<ViewGroup> zzdxp4) {
        this.zzgem = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfep = zzdxp3;
        this.zzgac = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcpu(this.zzgem.get(), this.zzejv.get(), this.zzfep.get(), this.zzgac.get());
    }
}
