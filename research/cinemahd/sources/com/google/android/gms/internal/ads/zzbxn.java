package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzbxn implements zzdxg<zzbxj> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzavu> zzemi;
    private final zzdxp<zzbww> zzeti;
    private final zzdxp<zzbxr> zzetj;
    private final zzdxp<zzbwq> zzetl;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzbws> zzfkx;

    private zzbxn(zzdxp<Context> zzdxp, zzdxp<zzavu> zzdxp2, zzdxp<zzczu> zzdxp3, zzdxp<zzbww> zzdxp4, zzdxp<zzbws> zzdxp5, zzdxp<zzbxr> zzdxp6, zzdxp<Executor> zzdxp7, zzdxp<Executor> zzdxp8, zzdxp<zzbwq> zzdxp9) {
        this.zzejv = zzdxp;
        this.zzemi = zzdxp2;
        this.zzfep = zzdxp3;
        this.zzeti = zzdxp4;
        this.zzfkx = zzdxp5;
        this.zzetj = zzdxp6;
        this.zzfei = zzdxp7;
        this.zzfcv = zzdxp8;
        this.zzetl = zzdxp9;
    }

    public static zzbxn zza(zzdxp<Context> zzdxp, zzdxp<zzavu> zzdxp2, zzdxp<zzczu> zzdxp3, zzdxp<zzbww> zzdxp4, zzdxp<zzbws> zzdxp5, zzdxp<zzbxr> zzdxp6, zzdxp<Executor> zzdxp7, zzdxp<Executor> zzdxp8, zzdxp<zzbwq> zzdxp9) {
        return new zzbxn(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6, zzdxp7, zzdxp8, zzdxp9);
    }

    public final /* synthetic */ Object get() {
        return new zzbxj(this.zzejv.get(), this.zzemi.get(), this.zzfep.get(), this.zzeti.get(), this.zzfkx.get(), this.zzetj.get(), this.zzfei.get(), this.zzfcv.get(), this.zzetl.get());
    }
}
