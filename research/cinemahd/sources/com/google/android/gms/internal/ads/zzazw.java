package com.google.android.gms.internal.ads;

final /* synthetic */ class zzazw implements Runnable {
    private final int zzdtf;
    private final zzazx zzdww;

    zzazw(zzazx zzazx, int i) {
        this.zzdww = zzazx;
        this.zzdtf = i;
    }

    public final void run() {
        this.zzdww.zzcu(this.zzdtf);
    }
}
