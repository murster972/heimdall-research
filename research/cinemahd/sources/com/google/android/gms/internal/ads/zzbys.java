package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Callable;
import org.json.JSONObject;

final /* synthetic */ class zzbys implements Callable {
    private final zzdhe zzfgb;
    private final zzbyq zzfos;
    private final zzdhe zzfpa;
    private final zzdhe zzfpb;
    private final zzdhe zzfpc;
    private final zzdhe zzfpd;
    private final JSONObject zzfpe;
    private final zzdhe zzfpf;
    private final zzdhe zzfpg;
    private final zzdhe zzfph;

    zzbys(zzbyq zzbyq, zzdhe zzdhe, zzdhe zzdhe2, zzdhe zzdhe3, zzdhe zzdhe4, zzdhe zzdhe5, JSONObject jSONObject, zzdhe zzdhe6, zzdhe zzdhe7, zzdhe zzdhe8) {
        this.zzfos = zzbyq;
        this.zzfpa = zzdhe;
        this.zzfgb = zzdhe2;
        this.zzfpb = zzdhe3;
        this.zzfpc = zzdhe4;
        this.zzfpd = zzdhe5;
        this.zzfpe = jSONObject;
        this.zzfpf = zzdhe6;
        this.zzfpg = zzdhe7;
        this.zzfph = zzdhe8;
    }

    public final Object call() {
        zzdhe zzdhe = this.zzfpa;
        zzdhe zzdhe2 = this.zzfgb;
        zzdhe zzdhe3 = this.zzfpb;
        zzdhe zzdhe4 = this.zzfpc;
        zzdhe zzdhe5 = this.zzfpd;
        JSONObject jSONObject = this.zzfpe;
        zzdhe zzdhe6 = this.zzfpf;
        zzdhe zzdhe7 = this.zzfpg;
        zzdhe zzdhe8 = this.zzfph;
        zzbws zzbws = (zzbws) zzdhe.get();
        zzbws.setImages((List) zzdhe2.get());
        zzbws.zza((zzaci) zzdhe3.get());
        zzbws.zzb((zzaci) zzdhe4.get());
        zzbws.zza((zzaca) zzdhe5.get());
        zzbws.zzf(zzbyu.zzi(jSONObject));
        zzbws.zza(zzbyu.zzj(jSONObject));
        zzbdi zzbdi = (zzbdi) zzdhe6.get();
        if (zzbdi != null) {
            zzbws.zzi(zzbdi);
            zzbws.zzab(zzbdi.getView());
            zzbws.zzb((zzxb) zzbdi.zzyl());
        }
        zzbdi zzbdi2 = (zzbdi) zzdhe7.get();
        if (zzbdi2 != null) {
            zzbws.zzj(zzbdi2);
        }
        for (zzbzf zzbzf : (List) zzdhe8.get()) {
            int i = zzbzf.type;
            if (i == 1) {
                zzbws.zzn(zzbzf.zzcc, zzbzf.zzfps);
            } else if (i == 2) {
                zzbws.zza(zzbzf.zzcc, zzbzf.zzfpt);
            }
        }
        return zzbws;
    }
}
