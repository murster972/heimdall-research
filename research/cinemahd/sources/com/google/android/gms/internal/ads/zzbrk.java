package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final /* synthetic */ class zzbrk implements Runnable {
    private final Object zzdbh;
    private final zzbrn zzfhz;

    zzbrk(zzbrn zzbrn, Object obj) {
        this.zzfhz = zzbrn;
        this.zzdbh = obj;
    }

    public final void run() {
        try {
            this.zzfhz.zzp(this.zzdbh);
        } catch (Throwable th) {
            zzq.zzku().zzb(th, "EventEmitter.notify");
            zzavs.zza("Event emitter exception.", th);
        }
    }
}
