package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.Collections;
import java.util.List;

public final class zzyf extends zzwj {
    private zzagu zzcfl;

    public final String getVersionString() {
        return "";
    }

    public final void initialize() throws RemoteException {
        zzayu.zzex("The initialization is not processed because MobileAdsSettingsManager is not created successfully.");
        zzayk.zzyu.post(new zzyi(this));
    }

    public final void setAppMuted(boolean z) throws RemoteException {
    }

    public final void setAppVolume(float f) throws RemoteException {
    }

    public final void zza(zzagu zzagu) throws RemoteException {
        this.zzcfl = zzagu;
    }

    public final void zza(zzalc zzalc) throws RemoteException {
    }

    public final void zza(zzyq zzyq) throws RemoteException {
    }

    public final void zza(String str, IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final void zzb(IObjectWrapper iObjectWrapper, String str) throws RemoteException {
    }

    public final void zzcd(String str) throws RemoteException {
    }

    public final void zzce(String str) throws RemoteException {
    }

    public final float zzpe() throws RemoteException {
        return 1.0f;
    }

    public final boolean zzpf() throws RemoteException {
        return false;
    }

    public final List<zzagn> zzpg() throws RemoteException {
        return Collections.emptyList();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzpy() {
        zzagu zzagu = this.zzcfl;
        if (zzagu != null) {
            try {
                zzagu.zzc(Collections.emptyList());
            } catch (RemoteException e) {
                zzayu.zzd("Could not notify onComplete event.", e);
            }
        }
    }
}
