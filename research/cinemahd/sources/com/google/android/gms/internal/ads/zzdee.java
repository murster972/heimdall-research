package com.google.android.gms.internal.ads;

import java.util.Arrays;

public final class zzdee {
    private final String className;
    private final zzdeh zzgty;
    private zzdeh zzgtz;
    private boolean zzgua;

    private zzdee(String str) {
        this.zzgty = new zzdeh();
        this.zzgtz = this.zzgty;
        this.zzgua = false;
        this.className = (String) zzdei.checkNotNull(str);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.className);
        sb.append('{');
        zzdeh zzdeh = this.zzgty.zzgub;
        String str = "";
        while (zzdeh != null) {
            Object obj = zzdeh.value;
            sb.append(str);
            if (obj == null || !obj.getClass().isArray()) {
                sb.append(obj);
            } else {
                String deepToString = Arrays.deepToString(new Object[]{obj});
                sb.append(deepToString, 1, deepToString.length() - 1);
            }
            zzdeh = zzdeh.zzgub;
            str = ", ";
        }
        sb.append('}');
        return sb.toString();
    }

    public final zzdee zzaa(Object obj) {
        zzdeh zzdeh = new zzdeh();
        this.zzgtz.zzgub = zzdeh;
        this.zzgtz = zzdeh;
        zzdeh.value = obj;
        return this;
    }
}
