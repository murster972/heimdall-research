package com.google.android.gms.internal.ads;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

abstract class zzdfu<I, O, F, T> extends zzdgm<O> implements Runnable {
    private zzdhe<? extends I> zzgvg;
    private F zzgvx;

    zzdfu(zzdhe<? extends I> zzdhe, F f) {
        this.zzgvg = (zzdhe) zzdei.checkNotNull(zzdhe);
        this.zzgvx = zzdei.checkNotNull(f);
    }

    static <I, O> zzdhe<O> zza(zzdhe<I> zzdhe, zzdgf<? super I, ? extends O> zzdgf, Executor executor) {
        zzdei.checkNotNull(executor);
        zzdfx zzdfx = new zzdfx(zzdhe, zzdgf);
        zzdhe.addListener(zzdfx, zzdhg.zza(executor, zzdfx));
        return zzdfx;
    }

    /* access modifiers changed from: protected */
    public final void afterDone() {
        maybePropagateCancellationTo(this.zzgvg);
        this.zzgvg = null;
        this.zzgvx = null;
    }

    /* access modifiers changed from: protected */
    public final String pendingToString() {
        String str;
        zzdhe<? extends I> zzdhe = this.zzgvg;
        F f = this.zzgvx;
        String pendingToString = super.pendingToString();
        if (zzdhe != null) {
            String valueOf = String.valueOf(zzdhe);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 16);
            sb.append("inputFuture=[");
            sb.append(valueOf);
            sb.append("], ");
            str = sb.toString();
        } else {
            str = "";
        }
        if (f != null) {
            String valueOf2 = String.valueOf(f);
            StringBuilder sb2 = new StringBuilder(String.valueOf(str).length() + 11 + String.valueOf(valueOf2).length());
            sb2.append(str);
            sb2.append("function=[");
            sb2.append(valueOf2);
            sb2.append("]");
            return sb2.toString();
        } else if (pendingToString == null) {
            return null;
        } else {
            String valueOf3 = String.valueOf(str);
            String valueOf4 = String.valueOf(pendingToString);
            return valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
        }
    }

    public final void run() {
        zzdhe<? extends I> zzdhe = this.zzgvg;
        F f = this.zzgvx;
        boolean z = true;
        boolean isCancelled = isCancelled() | (zzdhe == null);
        if (f != null) {
            z = false;
        }
        if (!isCancelled && !z) {
            this.zzgvg = null;
            if (zzdhe.isCancelled()) {
                setFuture(zzdhe);
                return;
            }
            try {
                try {
                    Object zzc = zzc(f, zzdgs.zzb(zzdhe));
                    this.zzgvx = null;
                    setResult(zzc);
                } catch (Throwable th) {
                    this.zzgvx = null;
                    throw th;
                }
            } catch (CancellationException unused) {
                cancel(false);
            } catch (ExecutionException e) {
                setException(e.getCause());
            } catch (RuntimeException e2) {
                setException(e2);
            } catch (Error e3) {
                setException(e3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public abstract void setResult(T t);

    /* access modifiers changed from: package-private */
    public abstract T zzc(F f, I i) throws Exception;

    static <I, O> zzdhe<O> zza(zzdhe<I> zzdhe, zzded<? super I, ? extends O> zzded, Executor executor) {
        zzdei.checkNotNull(zzded);
        zzdfw zzdfw = new zzdfw(zzdhe, zzded);
        zzdhe.addListener(zzdfw, zzdhg.zza(executor, zzdfw));
        return zzdfw;
    }
}
