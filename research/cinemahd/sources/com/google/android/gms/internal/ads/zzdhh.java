package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

class zzdhh extends zzdfv {
    private final ExecutorService zzgxe;

    zzdhh(ExecutorService executorService) {
        this.zzgxe = (ExecutorService) zzdei.checkNotNull(executorService);
    }

    public final boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        return this.zzgxe.awaitTermination(j, timeUnit);
    }

    public final void execute(Runnable runnable) {
        this.zzgxe.execute(runnable);
    }

    public final boolean isShutdown() {
        return this.zzgxe.isShutdown();
    }

    public final boolean isTerminated() {
        return this.zzgxe.isTerminated();
    }

    public final void shutdown() {
        this.zzgxe.shutdown();
    }

    public final List<Runnable> shutdownNow() {
        return this.zzgxe.shutdownNow();
    }
}
