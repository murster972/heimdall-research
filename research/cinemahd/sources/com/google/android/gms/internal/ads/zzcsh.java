package com.google.android.gms.internal.ads;

public final class zzcsh implements zzdxg<zzcsf> {
    private final zzdxp<zzdhd> zzfcv;

    private zzcsh(zzdxp<zzdhd> zzdxp) {
        this.zzfcv = zzdxp;
    }

    public static zzcsh zzak(zzdxp<zzdhd> zzdxp) {
        return new zzcsh(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcsf(this.zzfcv.get());
    }
}
