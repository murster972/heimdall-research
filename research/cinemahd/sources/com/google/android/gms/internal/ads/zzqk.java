package com.google.android.gms.internal.ads;

import android.webkit.ValueCallback;
import android.webkit.WebView;

final class zzqk implements Runnable {
    private ValueCallback<String> zzbpv = new zzqn(this);
    final /* synthetic */ zzqc zzbpw;
    final /* synthetic */ WebView zzbpx;
    final /* synthetic */ boolean zzbpy;
    final /* synthetic */ zzqi zzbpz;

    zzqk(zzqi zzqi, zzqc zzqc, WebView webView, boolean z) {
        this.zzbpz = zzqi;
        this.zzbpw = zzqc;
        this.zzbpx = webView;
        this.zzbpy = z;
    }

    public final void run() {
        if (this.zzbpx.getSettings().getJavaScriptEnabled()) {
            try {
                this.zzbpx.evaluateJavascript("(function() { return  {text:document.body.innerText}})();", this.zzbpv);
            } catch (Throwable unused) {
                this.zzbpv.onReceiveValue("");
            }
        }
    }
}
