package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzbxs implements zzdxg<zzbxq> {
    private final zzdxp<zzcaj> zzeuj;
    private final zzdxp<Clock> zzfcz;

    public zzbxs(zzdxp<zzcaj> zzdxp, zzdxp<Clock> zzdxp2) {
        this.zzeuj = zzdxp;
        this.zzfcz = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzbxq(this.zzeuj.get(), this.zzfcz.get());
    }
}
