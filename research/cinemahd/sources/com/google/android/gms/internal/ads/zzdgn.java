package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class zzdgn<V> extends zzdgw<V> {
    zzdgn() {
    }

    public static <V> zzdgn<V> zze(zzdhe<V> zzdhe) {
        if (zzdhe instanceof zzdgn) {
            return (zzdgn) zzdhe;
        }
        return new zzdgp(zzdhe);
    }

    public final <X extends Throwable> zzdgn<V> zza(Class<X> cls, zzded<? super X, ? extends V> zzded, Executor executor) {
        zzdft zzdft = new zzdft(this, cls, zzded);
        addListener(zzdft, zzdhg.zza(executor, zzdft));
        return zzdft;
    }

    public final zzdgn<V> zza(long j, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        return (zzdgn) zzdgs.zza(this, j, timeUnit, scheduledExecutorService);
    }

    public final <T> zzdgn<T> zza(zzded<? super V, T> zzded, Executor executor) {
        zzdei.checkNotNull(zzded);
        zzdfw zzdfw = new zzdfw(this, zzded);
        addListener(zzdfw, zzdhg.zza(executor, zzdfw));
        return zzdfw;
    }
}
