package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcet implements Runnable {
    private final Object zzdbh;
    private final String zzfru;
    private final zzceq zzftj;
    private final zzazl zzftu;
    private final long zzftv;

    zzcet(zzceq zzceq, Object obj, zzazl zzazl, String str, long j) {
        this.zzftj = zzceq;
        this.zzdbh = obj;
        this.zzftu = zzazl;
        this.zzfru = str;
        this.zzftv = j;
    }

    public final void run() {
        this.zzftj.zza(this.zzdbh, this.zzftu, this.zzfru, this.zzftv);
    }
}
