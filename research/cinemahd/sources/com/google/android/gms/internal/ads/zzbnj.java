package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzbnj implements zzdxg<zzbnk> {
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<zzavd> zzfgu;

    private zzbnj(zzdxp<Clock> zzdxp, zzdxp<zzavd> zzdxp2) {
        this.zzfcz = zzdxp;
        this.zzfgu = zzdxp2;
    }

    public static zzbnj zzb(zzdxp<Clock> zzdxp, zzdxp<zzavd> zzdxp2) {
        return new zzbnj(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzbnk(this.zzfcz.get(), this.zzfgu.get());
    }
}
