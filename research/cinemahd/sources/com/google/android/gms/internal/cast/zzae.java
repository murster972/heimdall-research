package com.google.android.gms.internal.cast;

import android.graphics.Bitmap;

public interface zzae {
    void onPostExecute(Bitmap bitmap);
}
