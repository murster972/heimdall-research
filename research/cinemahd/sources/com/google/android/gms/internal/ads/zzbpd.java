package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbpd extends zzbrl<zzbpe> {
    private boolean zzfhq = false;

    public zzbpd(Set<zzbsu<zzbpe>> set) {
        super(set);
    }

    public final synchronized void onAdImpression() {
        if (!this.zzfhq) {
            zza(zzbpc.zzfhp);
            this.zzfhq = true;
        }
    }
}
