package com.google.android.gms.internal.ads;

public final class zzblh implements zzdxg<zzcio<zzbkk>> {
    private final zzdxp<zzcna<zzbkk, zzdac, zzcjy>> zzepq;
    private final zzdxp<Boolean> zzewt;
    private final zzdxp<zzclj> zzewv;

    public zzblh(zzdxp<Boolean> zzdxp, zzdxp<zzclj> zzdxp2, zzdxp<zzcna<zzbkk, zzdac, zzcjy>> zzdxp3) {
        this.zzewt = zzdxp;
        this.zzewv = zzdxp2;
        this.zzepq = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        boolean booleanValue = this.zzewt.get().booleanValue();
        zzcio zzcio = (zzclj) this.zzewv.get();
        zzcio zzcio2 = (zzcna) this.zzepq.get();
        if (!booleanValue) {
            zzcio = zzcio2;
        }
        return (zzcio) zzdxm.zza(zzcio, "Cannot return null from a non-@Nullable @Provides method");
    }
}
