package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final class zzcex implements zzdgt<String> {
    final /* synthetic */ zzceq zzftz;

    zzcex(zzceq zzceq) {
        this.zzftz = zzceq;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        String str = (String) obj;
        synchronized (this) {
            boolean unused = this.zzftz.zzftm = true;
            this.zzftz.zza("com.google.android.gms.ads.MobileAds", true, "", (int) (zzq.zzkx().a() - this.zzftz.zzftn));
            this.zzftz.executor.execute(new zzcfa(this, str));
        }
    }

    public final void zzb(Throwable th) {
        synchronized (this) {
            boolean unused = this.zzftz.zzftm = true;
            this.zzftz.zza("com.google.android.gms.ads.MobileAds", false, "Internal Error.", (int) (zzq.zzkx().a() - this.zzftz.zzftn));
            this.zzftz.zzfto.setException(new Exception());
        }
    }
}
