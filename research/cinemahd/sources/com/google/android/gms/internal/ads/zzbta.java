package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbta implements zzdxg<zzbsy> {
    private final zzdxp<Set<zzbsu<zzbsz>>> zzfeo;

    private zzbta(zzdxp<Set<zzbsu<zzbsz>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbta zzs(zzdxp<Set<zzbsu<zzbsz>>> zzdxp) {
        return new zzbta(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbsy(this.zzfeo.get());
    }
}
