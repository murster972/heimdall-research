package com.google.android.gms.internal.ads;

import android.view.ViewGroup;
import com.google.android.gms.internal.ads.zzbod;

public final class zzclm implements zzdxg<zzclj> {
    private final zzdxp<zzbqp> zzfhe;
    private final zzdxp<zzbfx> zzfkr;
    private final zzdxp<zzbod.zza> zzfks;
    private final zzdxp<zzbrm> zzfkt;
    private final zzdxp<zzbvi> zzfku;
    private final zzdxp<zzcns> zzgab;
    private final zzdxp<ViewGroup> zzgac;

    public zzclm(zzdxp<zzbfx> zzdxp, zzdxp<zzbod.zza> zzdxp2, zzdxp<zzcns> zzdxp3, zzdxp<zzbrm> zzdxp4, zzdxp<zzbvi> zzdxp5, zzdxp<zzbqp> zzdxp6, zzdxp<ViewGroup> zzdxp7) {
        this.zzfkr = zzdxp;
        this.zzfks = zzdxp2;
        this.zzgab = zzdxp3;
        this.zzfkt = zzdxp4;
        this.zzfku = zzdxp5;
        this.zzfhe = zzdxp6;
        this.zzgac = zzdxp7;
    }

    public final /* synthetic */ Object get() {
        return new zzclj(this.zzfkr.get(), this.zzfks.get(), this.zzgab.get(), this.zzfkt.get(), this.zzfku.get(), this.zzfhe.get(), this.zzgac.get());
    }
}
