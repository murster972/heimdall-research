package com.google.android.gms.internal.ads;

public final class zzdas implements zzdxg<zzdao> {
    private final zzdxp<zzdam> zzfcw;
    private final zzdaq zzgne;
    private final zzdxp<String> zzgnf;

    private zzdas(zzdaq zzdaq, zzdxp<zzdam> zzdxp, zzdxp<String> zzdxp2) {
        this.zzgne = zzdaq;
        this.zzfcw = zzdxp;
        this.zzgnf = zzdxp2;
    }

    public static zzdas zza(zzdaq zzdaq, zzdxp<zzdam> zzdxp, zzdxp<String> zzdxp2) {
        return new zzdas(zzdaq, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzdao) zzdxm.zza(this.zzfcw.get().zzgl(this.zzgnf.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
