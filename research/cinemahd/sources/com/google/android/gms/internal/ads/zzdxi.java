package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public final class zzdxi<K, V> extends zzdwz<K, V, V> {
    private static final zzdxp<Map<Object, Object>> zziad = zzdxf.zzbe(Collections.emptyMap());

    private zzdxi(Map<K, zzdxp<V>> map) {
        super(map);
    }

    public static <K, V> zzdxk<K, V> zzhl(int i) {
        return new zzdxk<>(i);
    }

    public final /* synthetic */ Object get() {
        LinkedHashMap zzhj = zzdxb.zzhj(zzbdn().size());
        for (Map.Entry entry : zzbdn().entrySet()) {
            zzhj.put(entry.getKey(), ((zzdxp) entry.getValue()).get());
        }
        return Collections.unmodifiableMap(zzhj);
    }
}
