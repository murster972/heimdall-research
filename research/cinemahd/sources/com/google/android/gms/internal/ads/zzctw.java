package com.google.android.gms.internal.ads;

import android.os.Bundle;

final /* synthetic */ class zzctw implements zzcty {
    static final zzcty zzggx = new zzctw();

    private zzctw() {
    }

    public final void zzr(Object obj) {
        ((Bundle) obj).putBoolean("sdk_prefetch", true);
    }
}
