package com.google.android.gms.internal.cast;

import android.text.TextUtils;
import com.google.android.gms.common.images.WebImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public final class zzeg {
    private static final String[] zzaav = {"Z", "+hh", "+hhmm", "+hh:mm"};
    private static final String zzaaw;
    private static final zzdw zzbf = new zzdw("MetadataUtils");

    static {
        String valueOf = String.valueOf(zzaav[0]);
        zzaaw = valueOf.length() != 0 ? "yyyyMMdd'T'HHmmss".concat(valueOf) : new String("yyyyMMdd'T'HHmmss");
    }

    public static void zza(List<WebImage> list, JSONArray jSONArray) {
        try {
            list.clear();
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    list.add(new WebImage(jSONArray.getJSONObject(i)));
                } catch (IllegalArgumentException unused) {
                }
            }
        } catch (JSONException unused2) {
        }
    }

    public static JSONArray zze(List<WebImage> list) {
        if (list == null && list.isEmpty()) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (WebImage v : list) {
            jSONArray.put(v.v());
        }
        return jSONArray;
    }

    public static Calendar zzv(String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            zzbf.d("Input string is empty or null", new Object[0]);
            return null;
        }
        String zzw = zzw(str);
        if (TextUtils.isEmpty(zzw)) {
            zzbf.d("Invalid date format", new Object[0]);
            return null;
        }
        String zzx = zzx(str);
        if (!TextUtils.isEmpty(zzx)) {
            String valueOf = String.valueOf(zzw);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(zzx).length());
            sb.append(valueOf);
            sb.append("T");
            sb.append(zzx);
            zzw = sb.toString();
            str2 = zzx.length() == 6 ? "yyyyMMdd'T'HHmmss" : zzaaw;
        } else {
            str2 = "yyyyMMdd";
        }
        Calendar instance = GregorianCalendar.getInstance();
        try {
            instance.setTime(new SimpleDateFormat(str2).parse(zzw));
            return instance;
        } catch (ParseException e) {
            zzbf.d("Error parsing string: %s", e.getMessage());
            return null;
        }
    }

    private static String zzw(String str) {
        if (TextUtils.isEmpty(str)) {
            zzbf.d("Input string is empty or null", new Object[0]);
            return null;
        }
        try {
            return str.substring(0, 8);
        } catch (IndexOutOfBoundsException e) {
            zzbf.i("Error extracting the date: %s", e.getMessage());
            return null;
        }
    }

    private static String zzx(String str) {
        if (TextUtils.isEmpty(str)) {
            zzbf.d("string is empty or null", new Object[0]);
            return null;
        }
        int indexOf = str.indexOf(84);
        int i = indexOf + 1;
        if (indexOf != 8) {
            zzbf.d("T delimeter is not found", new Object[0]);
            return null;
        }
        boolean z = true;
        try {
            String substring = str.substring(i);
            if (substring.length() == 6) {
                return substring;
            }
            char charAt = substring.charAt(6);
            if (charAt == '+' || charAt == '-') {
                int length = substring.length();
                if (!(length == zzaav[1].length() + 6 || length == zzaav[2].length() + 6 || length == zzaav[3].length() + 6)) {
                    z = false;
                }
                if (z) {
                    return substring.replaceAll("([\\+\\-]\\d\\d):(\\d\\d)", "$1$2");
                }
            } else if (charAt != 'Z' || substring.length() != zzaav[0].length() + 6) {
                return null;
            } else {
                String valueOf = String.valueOf(substring.substring(0, substring.length() - 1));
                return "+0000".length() != 0 ? valueOf.concat("+0000") : new String(valueOf);
            }
            return null;
        } catch (IndexOutOfBoundsException e) {
            zzbf.d("Error extracting the time substring: %s", e.getMessage());
            return null;
        }
    }

    public static String zza(Calendar calendar) {
        if (calendar == null) {
            zzbf.d("Calendar object cannot be null", new Object[0]);
            return null;
        }
        String str = zzaaw;
        if (calendar.get(11) == 0 && calendar.get(12) == 0 && calendar.get(13) == 0) {
            str = "yyyyMMdd";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str);
        simpleDateFormat.setTimeZone(calendar.getTimeZone());
        String format = simpleDateFormat.format(calendar.getTime());
        return format.endsWith("+0000") ? format.replace("+0000", zzaav[0]) : format;
    }
}
