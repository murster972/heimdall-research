package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.util.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import okhttp3.internal.cache.DiskLruCache;

public final class zzdda {
    private final Executor executor;
    private final String zzbma;
    private final Clock zzbmq;
    private final String zzcdu;
    private final String zzdir;
    private final zzdq zzfbt;
    private final zzczs zzfhh;
    private final zzclp zzfil;
    private final zzayy zzgrm;
    private final Context zzup;

    public zzdda(Executor executor2, zzayy zzayy, zzclp zzclp, zzazb zzazb, String str, String str2, Context context, zzczs zzczs, Clock clock, zzdq zzdq) {
        this.executor = executor2;
        this.zzgrm = zzayy;
        this.zzfil = zzclp;
        this.zzbma = zzazb.zzbma;
        this.zzdir = str;
        this.zzcdu = str2;
        this.zzup = context;
        this.zzfhh = zzczs;
        this.zzbmq = clock;
        this.zzfbt = zzdq;
    }

    private static String zzc(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str3)) {
            str3 = "";
        }
        return str.replaceAll(str2, str3);
    }

    private static String zzgo(String str) {
        return (TextUtils.isEmpty(str) || !zzayo.isEnabled()) ? str : "fakeForAdDebugLog";
    }

    public final void zza(zzczt zzczt, zzczl zzczl, List<String> list) {
        zza(zzczt, zzczl, false, "", list);
    }

    public final void zzen(String str) {
        this.executor.execute(new zzddd(this, str));
    }

    public final void zzg(List<String> list) {
        for (String zzen : list) {
            zzen(zzen);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzgp(String str) {
        this.zzgrm.zzen(str);
    }

    public final void zza(zzczt zzczt, zzczl zzczl, boolean z, String str, List<String> list) {
        ArrayList arrayList = new ArrayList();
        String str2 = z ? DiskLruCache.VERSION_1 : "0";
        for (String zzc : list) {
            String zzc2 = zzc(zzc(zzc(zzc, "@gw_adlocid@", zzczt.zzgmh.zzfgl.zzgmm), "@gw_adnetrefresh@", str2), "@gw_sdkver@", this.zzbma);
            if (zzczl != null) {
                zzc2 = zzauk.zzb(zzc(zzc(zzc(zzc2, "@gw_qdata@", zzczl.zzdbw), "@gw_adnetid@", zzczl.zzaez), "@gw_allocid@", zzczl.zzdcm), this.zzup, zzczl.zzdll);
            }
            String zzc3 = zzc(zzc(zzc(zzc2, "@gw_adnetstatus@", this.zzfil.zzamh()), "@gw_seqnum@", this.zzdir), "@gw_sessid@", this.zzcdu);
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcls)).booleanValue() && !TextUtils.isEmpty(str)) {
                if (this.zzfbt.zzb(Uri.parse(zzc3))) {
                    zzc3 = Uri.parse(zzc3).buildUpon().appendQueryParameter("ms", str).build().toString();
                }
            }
            arrayList.add(zzc3);
        }
        zzg(arrayList);
    }

    public final void zza(zzczt zzczt, zzczl zzczl, List<String> list, zzare zzare) {
        long b = this.zzbmq.b();
        try {
            String type = zzare.getType();
            String num = Integer.toString(zzare.getAmount());
            ArrayList arrayList = new ArrayList();
            zzczs zzczs = this.zzfhh;
            String str = "";
            String zzgo = zzczs == null ? str : zzgo(zzczs.zzdnv);
            zzczs zzczs2 = this.zzfhh;
            if (zzczs2 != null) {
                str = zzgo(zzczs2.zzdnw);
            }
            for (String zzc : list) {
                arrayList.add(zzauk.zzb(zzc(zzc(zzc(zzc(zzc(zzc(zzc, "@gw_rwd_userid@", Uri.encode(zzgo)), "@gw_rwd_custom_data@", Uri.encode(str)), "@gw_tmstmp@", Long.toString(b)), "@gw_rwd_itm@", Uri.encode(type)), "@gw_rwd_amt@", num), "@gw_sdkver@", this.zzbma), this.zzup, zzczl.zzdll));
            }
            zzg(arrayList);
        } catch (RemoteException unused) {
        }
    }
}
