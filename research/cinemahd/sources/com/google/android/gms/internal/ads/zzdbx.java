package com.google.android.gms.internal.ads;

public final class zzdbx implements zzdxg<zzdhd> {
    private static final zzdbx zzgpv = new zzdbx();

    public static zzdhd zzaqb() {
        return (zzdhd) zzdxm.zza(zzazd.zzdwf, "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzaqb();
    }
}
