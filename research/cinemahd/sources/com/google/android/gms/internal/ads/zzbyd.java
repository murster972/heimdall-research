package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbyd implements zzdxg<zzbye> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzboq> zzesg;
    private final zzdxp<zzbpd> zzesj;
    private final zzdxp<zzalr> zzetf;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzczl> zzffb;
    private final zzdxp<zzall> zzfoi;
    private final zzdxp<zzalq> zzfoj;

    public zzbyd(zzdxp<zzall> zzdxp, zzdxp<zzalq> zzdxp2, zzdxp<zzalr> zzdxp3, zzdxp<zzbpd> zzdxp4, zzdxp<zzboq> zzdxp5, zzdxp<Context> zzdxp6, zzdxp<zzczl> zzdxp7, zzdxp<zzazb> zzdxp8, zzdxp<zzczu> zzdxp9) {
        this.zzfoi = zzdxp;
        this.zzfoj = zzdxp2;
        this.zzetf = zzdxp3;
        this.zzesj = zzdxp4;
        this.zzesg = zzdxp5;
        this.zzejv = zzdxp6;
        this.zzffb = zzdxp7;
        this.zzfav = zzdxp8;
        this.zzfep = zzdxp9;
    }

    public final /* synthetic */ Object get() {
        return new zzbye(this.zzfoi.get(), this.zzfoj.get(), this.zzetf.get(), this.zzesj.get(), this.zzesg.get(), this.zzejv.get(), this.zzffb.get(), this.zzfav.get(), this.zzfep.get());
    }
}
