package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzbjl implements zzdxg<Set<zzbsu<zzbph>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbjd> zzfdd;
    private final zzdxp<JSONObject> zzfde;

    private zzbjl(zzdxp<zzbjd> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<JSONObject> zzdxp3) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzfde = zzdxp3;
    }

    public static zzbjl zzc(zzdxp<zzbjd> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<JSONObject> zzdxp3) {
        return new zzbjl(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        Set set;
        zzbjd zzbjd = this.zzfdd.get();
        Executor executor = this.zzfcv.get();
        if (this.zzfde.get() == null) {
            set = Collections.emptySet();
        } else {
            set = Collections.singleton(new zzbsu(zzbjd, executor));
        }
        return (Set) zzdxm.zza(set, "Cannot return null from a non-@Nullable @Provides method");
    }
}
