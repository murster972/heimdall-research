package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcsg implements zzcty<Bundle> {
    private final zzczj zzfbj;

    public zzcsg(zzczj zzczj) {
        this.zzfbj = zzczj;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        zzczj zzczj = this.zzfbj;
        if (zzczj != null) {
            bundle.putBoolean("render_in_browser", zzczj.zzaom());
            bundle.putBoolean("disable_ml", this.zzfbj.zzaon());
        }
    }
}
