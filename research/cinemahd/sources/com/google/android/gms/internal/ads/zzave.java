package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.wrappers.Wrappers;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzave {
    /* access modifiers changed from: private */
    public final Object lock = new Object();
    /* access modifiers changed from: private */
    public zzazb zzbll;
    private final zzavp zzdpz = new zzavp(zzve.zzoz(), this.zzdqi);
    private zzpp zzdqh;
    private final zzavx zzdqi = new zzavx();
    /* access modifiers changed from: private */
    public zzzu zzdqj = null;
    private Boolean zzdqk = null;
    private final AtomicInteger zzdql = new AtomicInteger(0);
    private final zzavj zzdqm = new zzavj((zzavg) null);
    private final Object zzdqn = new Object();
    private zzdhe<ArrayList<String>> zzdqo;
    /* access modifiers changed from: private */
    public Context zzup;
    private boolean zzxx = false;

    @TargetApi(16)
    private static ArrayList<String> zzal(Context context) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            PackageInfo b = Wrappers.a(context).b(context.getApplicationInfo().packageName, 4096);
            if (b.requestedPermissions != null && b.requestedPermissionsFlags != null) {
                int i = 0;
                while (true) {
                    String[] strArr = b.requestedPermissions;
                    if (i >= strArr.length) {
                        break;
                    }
                    if ((b.requestedPermissionsFlags[i] & 2) != 0) {
                        arrayList.add(strArr[i]);
                    }
                    i++;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return arrayList;
    }

    public final Context getApplicationContext() {
        return this.zzup;
    }

    public final Resources getResources() {
        if (this.zzbll.zzdwb) {
            return this.zzup.getResources();
        }
        try {
            zzayx.zzbp(this.zzup).getResources();
            return null;
        } catch (zzayz e) {
            zzayu.zzd("Cannot load resource from dynamite apk or local jar", e);
            return null;
        }
    }

    public final void zza(Boolean bool) {
        synchronized (this.lock) {
            this.zzdqk = bool;
        }
    }

    public final void zzb(Throwable th, String str) {
        zzapn.zzc(this.zzup, this.zzbll).zza(th, str, zzabi.zzcup.get().floatValue());
    }

    @TargetApi(23)
    public final void zzd(Context context, zzazb zzazb) {
        synchronized (this.lock) {
            if (!this.zzxx) {
                this.zzup = context.getApplicationContext();
                this.zzbll = zzazb;
                zzq.zzkt().zza(this.zzdpz);
                zzzu zzzu = null;
                this.zzdqi.zza(this.zzup, (String) null, true);
                zzapn.zzc(this.zzup, this.zzbll);
                this.zzdqh = new zzpp(context.getApplicationContext(), this.zzbll);
                zzq.zzkz();
                if (!zzaav.zzcsw.get().booleanValue()) {
                    zzavs.zzed("CsiReporterFactory: CSI is not enabled. No CSI reporter created.");
                } else {
                    zzzu = new zzzu();
                }
                this.zzdqj = zzzu;
                if (this.zzdqj != null) {
                    zzazh.zza(new zzavg(this).zzvr(), "AppState.registerCsiReporter");
                }
                this.zzxx = true;
                zzvg();
            }
        }
        zzq.zzkq().zzr(context, zzazb.zzbma);
    }

    public final zzzu zzuz() {
        zzzu zzzu;
        synchronized (this.lock) {
            zzzu = this.zzdqj;
        }
        return zzzu;
    }

    public final Boolean zzva() {
        Boolean bool;
        synchronized (this.lock) {
            bool = this.zzdqk;
        }
        return bool;
    }

    public final void zzvb() {
        this.zzdqm.zzvb();
    }

    public final void zzvc() {
        this.zzdql.incrementAndGet();
    }

    public final void zzvd() {
        this.zzdql.decrementAndGet();
    }

    public final int zzve() {
        return this.zzdql.get();
    }

    public final zzavu zzvf() {
        zzavx zzavx;
        synchronized (this.lock) {
            zzavx = this.zzdqi;
        }
        return zzavx;
    }

    public final zzdhe<ArrayList<String>> zzvg() {
        if (PlatformVersion.c() && this.zzup != null) {
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzclf)).booleanValue()) {
                synchronized (this.zzdqn) {
                    if (this.zzdqo != null) {
                        zzdhe<ArrayList<String>> zzdhe = this.zzdqo;
                        return zzdhe;
                    }
                    zzdhe<ArrayList<String>> zzd = zzazd.zzdwe.zzd(new zzavh(this));
                    this.zzdqo = zzd;
                    return zzd;
                }
            }
        }
        return zzdgs.zzaj(new ArrayList());
    }

    public final zzavp zzvh() {
        return this.zzdpz;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ ArrayList zzvi() throws Exception {
        return zzal(zzarf.zzaa(this.zzup));
    }

    public final void zza(Throwable th, String str) {
        zzapn.zzc(this.zzup, this.zzbll).zza(th, str);
    }
}
