package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;

final /* synthetic */ class zzbrd implements zzbrn {
    private final String zzcyr;
    private final String zzcyz;

    zzbrd(String str, String str2) {
        this.zzcyz = str;
        this.zzcyr = str2;
    }

    public final void zzp(Object obj) {
        ((AppEventListener) obj).onAppEvent(this.zzcyz, this.zzcyr);
    }
}
