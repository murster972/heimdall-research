package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdlz extends zzdrt<zzdlz, zza> implements zzdtg {
    private static volatile zzdtn<zzdlz> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlz zzhba;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;

    public static final class zza extends zzdrt.zzb<zzdlz, zza> implements zzdtg {
        private zza() {
            super(zzdlz.zzhba);
        }

        public final zza zzaj(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlz) this.zzhmp).zzs(zzdqk);
            return this;
        }

        public final zza zzeh(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlz) this.zzhmp).setVersion(0);
            return this;
        }

        /* synthetic */ zza(zzdly zzdly) {
            this();
        }
    }

    static {
        zzdlz zzdlz = new zzdlz();
        zzhba = zzdlz;
        zzdrt.zza(zzdlz.class, zzdlz);
    }

    private zzdlz() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public static zzdlz zzah(zzdqk zzdqk) throws zzdse {
        return (zzdlz) zzdrt.zza(zzhba, zzdqk);
    }

    public static zza zzatz() {
        return (zza) zzhba.zzazt();
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdly.zzdk[i - 1]) {
            case 1:
                return new zzdlz();
            case 2:
                return new zza((zzdly) null);
            case 3:
                return zzdrt.zza((zzdte) zzhba, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u000b\u0002\n", new Object[]{"zzhaa", "zzhab"});
            case 4:
                return zzhba;
            case 5:
                zzdtn<zzdlz> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlz.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhba);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }
}
