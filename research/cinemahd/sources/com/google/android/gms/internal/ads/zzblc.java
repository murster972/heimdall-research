package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzblc implements zzdxg<zzats> {
    private final zzdxp<Context> zzejv;
    private final zzbkn zzfen;
    private final zzdxp<zzczu> zzfep;

    public zzblc(zzbkn zzbkn, zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2) {
        this.zzfen = zzbkn;
        this.zzejv = zzdxp;
        this.zzfep = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return (zzats) zzdxm.zza(new zzats(this.zzejv.get(), this.zzfep.get().zzgmm), "Cannot return null from a non-@Nullable @Provides method");
    }
}
