package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

public abstract class zzdfb<E> extends zzdet<E> implements Set<E> {
    private transient zzdeu<E> zzguq;

    zzdfb() {
    }

    @SafeVarargs
    public static <E> zzdfb<E> zza(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        zzdei.checkArgument(eArr.length <= 2147483641, "the total number of elements must fit in an int");
        Object[] objArr = new Object[(eArr.length + 6)];
        objArr[0] = e;
        objArr[1] = e2;
        objArr[2] = e3;
        objArr[3] = e4;
        objArr[4] = e5;
        objArr[5] = e6;
        System.arraycopy(eArr, 0, objArr, 6, eArr.length);
        return zza(objArr.length, objArr);
    }

    public static <E> zzdfb<E> zzag(E e) {
        return new zzdfm(e);
    }

    static int zzdx(int i) {
        int max = Math.max(i, 2);
        boolean z = true;
        if (max < 751619276) {
            int highestOneBit = Integer.highestOneBit(max - 1) << 1;
            while (((double) highestOneBit) * 0.7d < ((double) max)) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        if (max >= 1073741824) {
            z = false;
        }
        zzdei.checkArgument(z, "collection too large");
        return 1073741824;
    }

    public static <E> zzdfa<E> zzdy(int i) {
        zzdeo.zze(i, "expectedSize");
        return new zzdfa<>(i);
    }

    /* access modifiers changed from: private */
    public static boolean zzv(int i, int i2) {
        return i < (i2 >> 1) + (i2 >> 2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzdfb) || !zzari() || !((zzdfb) obj).zzari() || hashCode() == obj.hashCode()) {
            return zzdfn.zza(this, obj);
        }
        return false;
    }

    public int hashCode() {
        return zzdfn.zzf(this);
    }

    public /* synthetic */ Iterator iterator() {
        return iterator();
    }

    public zzdeu<E> zzarb() {
        zzdeu<E> zzdeu = this.zzguq;
        if (zzdeu != null) {
            return zzdeu;
        }
        zzdeu<E> zzarj = zzarj();
        this.zzguq = zzarj;
        return zzarj;
    }

    /* access modifiers changed from: package-private */
    public boolean zzari() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public zzdeu<E> zzarj() {
        return zzdeu.zzc(toArray());
    }

    /* access modifiers changed from: private */
    public static <E> zzdfb<E> zza(int i, Object... objArr) {
        while (i != 0) {
            if (i == 1) {
                return zzag(objArr[0]);
            }
            int zzdx = zzdx(i);
            Object[] objArr2 = new Object[zzdx];
            int i2 = zzdx - 1;
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                Object zzd = zzdff.zzd(objArr[i5], i5);
                int hashCode = zzd.hashCode();
                int zzdv = zzdeq.zzdv(hashCode);
                while (true) {
                    int i6 = zzdv & i2;
                    Object obj = objArr2[i6];
                    if (obj != null) {
                        if (obj.equals(zzd)) {
                            break;
                        }
                        zzdv++;
                    } else {
                        objArr[i4] = zzd;
                        objArr2[i6] = zzd;
                        i3 += hashCode;
                        i4++;
                        break;
                    }
                }
            }
            Arrays.fill(objArr, i4, i, (Object) null);
            if (i4 == 1) {
                return new zzdfm(objArr[0], i3);
            }
            if (zzdx(i4) < zzdx / 2) {
                i = i4;
            } else {
                if (zzv(i4, objArr.length)) {
                    objArr = Arrays.copyOf(objArr, i4);
                }
                return new zzdfk(objArr, i3, objArr2, i2, i4);
            }
        }
        return zzdfk.zzgvb;
    }
}
