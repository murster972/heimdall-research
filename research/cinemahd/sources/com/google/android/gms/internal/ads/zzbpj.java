package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzbpj implements zzbrn {
    private final Context zzcri;

    zzbpj(Context context) {
        this.zzcri = context;
    }

    public final void zzp(Object obj) {
        ((zzbph) obj).zzbv(this.zzcri);
    }
}
