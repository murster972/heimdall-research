package com.google.android.gms.internal.ads;

import android.content.DialogInterface;

final class zzaob implements DialogInterface.OnClickListener {
    private final /* synthetic */ zzanz zzdff;

    zzaob(zzanz zzanz) {
        this.zzdff = zzanz;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.zzdff.zzds("Operation denied by user.");
    }
}
