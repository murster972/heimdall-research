package com.google.android.gms.internal.cast;

import android.view.ViewGroup;

final class zzq implements Runnable {
    private final /* synthetic */ zzo zzji;

    zzq(zzo zzo) {
        this.zzji = zzo;
    }

    public final void run() {
        if (this.zzji.zzjh.zzjg) {
            ((ViewGroup) this.zzji.zzjh.zzij.getWindow().getDecorView()).removeView(this.zzji.zzjh);
            if (this.zzji.zzjh.zzin != null) {
                this.zzji.zzjh.zzin.a();
            }
            this.zzji.zzjh.reset();
        }
    }
}
