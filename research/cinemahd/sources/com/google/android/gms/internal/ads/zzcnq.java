package com.google.android.gms.internal.ads;

import android.view.View;

public interface zzcnq<AdT> {
    AdT zza(zzczt zzczt, zzczl zzczl, View view, zzcnt zzcnt);
}
