package com.google.android.gms.internal.ads;

final /* synthetic */ class zzckb implements zzdgf {
    private final zzckc zzfzj;
    private final zzczl zzfzk;
    private final zzccd zzfzl;
    private final zzczt zzfzm;

    zzckb(zzckc zzckc, zzczl zzczl, zzccd zzccd, zzczt zzczt) {
        this.zzfzj = zzckc;
        this.zzfzk = zzczl;
        this.zzfzl = zzccd;
        this.zzfzm = zzczt;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfzj.zza(this.zzfzk, this.zzfzl, this.zzfzm, obj);
    }
}
