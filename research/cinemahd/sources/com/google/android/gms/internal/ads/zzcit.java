package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcit implements zzdgf {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzciu zzfyg;

    zzcit(zzciu zzciu, zzczt zzczt, zzczl zzczl) {
        this.zzfyg = zzciu;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfyg.zza(this.zzfot, this.zzfel, obj);
    }
}
