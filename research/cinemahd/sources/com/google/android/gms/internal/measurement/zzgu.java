package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzfd;

final class zzgu implements zzgv {
    zzgu() {
    }

    public final Object zza(Object obj) {
        return ((zzfd) obj).zza(zzfd.zzd.zzd, (Object) null, (Object) null);
    }
}
