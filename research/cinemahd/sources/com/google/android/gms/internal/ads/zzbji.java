package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import java.util.concurrent.Executor;

public final class zzbji implements zzdxg<zzbjd> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzakh> zzfcw;
    private final zzdxp<zzbjb> zzfcx;
    private final zzdxp<zzbiy> zzfcy;
    private final zzdxp<Clock> zzfcz;

    private zzbji(zzdxp<zzakh> zzdxp, zzdxp<zzbjb> zzdxp2, zzdxp<Executor> zzdxp3, zzdxp<zzbiy> zzdxp4, zzdxp<Clock> zzdxp5) {
        this.zzfcw = zzdxp;
        this.zzfcx = zzdxp2;
        this.zzfcv = zzdxp3;
        this.zzfcy = zzdxp4;
        this.zzfcz = zzdxp5;
    }

    public static zzbji zza(zzdxp<zzakh> zzdxp, zzdxp<zzbjb> zzdxp2, zzdxp<Executor> zzdxp3, zzdxp<zzbiy> zzdxp4, zzdxp<Clock> zzdxp5) {
        return new zzbji(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5);
    }

    public final /* synthetic */ Object get() {
        return new zzbjd(this.zzfcw.get(), this.zzfcx.get(), this.zzfcv.get(), this.zzfcy.get(), this.zzfcz.get());
    }
}
