package com.google.android.gms.internal.ads;

public final class zzbyb implements zzdxg<zzbyc> {
    private final zzdxp<zzbww> zzeti;
    private final zzdxp<zzbws> zzfkx;

    public zzbyb(zzdxp<zzbws> zzdxp, zzdxp<zzbww> zzdxp2) {
        this.zzfkx = zzdxp;
        this.zzeti = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzbyc(this.zzfkx.get(), this.zzeti.get());
    }
}
