package com.google.android.gms.internal.ads;

import android.os.Bundle;

final class zzavc {
    private long zzdpw = -1;
    private long zzdpx = -1;
    private final /* synthetic */ zzavd zzdpy;

    public zzavc(zzavd zzavd) {
        this.zzdpy = zzavd;
    }

    public final Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putLong("topen", this.zzdpw);
        bundle.putLong("tclose", this.zzdpx);
        return bundle;
    }

    public final long zzus() {
        return this.zzdpx;
    }

    public final void zzut() {
        this.zzdpx = this.zzdpy.zzbmq.a();
    }

    public final void zzuu() {
        this.zzdpw = this.zzdpy.zzbmq.a();
    }
}
