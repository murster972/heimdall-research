package com.google.android.gms.internal.ads;

import java.util.NoSuchElementException;

final class zzdtw extends zzdql {
    private final zzdty zzhqb = new zzdty(this.zzhqd, (zzdtw) null);
    private zzdqp zzhqc = zzbbq();
    private final /* synthetic */ zzdtt zzhqd;

    zzdtw(zzdtt zzdtt) {
        this.zzhqd = zzdtt;
    }

    private final zzdqp zzbbq() {
        if (this.zzhqb.hasNext()) {
            return (zzdqp) ((zzdqr) this.zzhqb.next()).iterator();
        }
        return null;
    }

    public final boolean hasNext() {
        return this.zzhqc != null;
    }

    public final byte nextByte() {
        zzdqp zzdqp = this.zzhqc;
        if (zzdqp != null) {
            byte nextByte = zzdqp.nextByte();
            if (!this.zzhqc.hasNext()) {
                this.zzhqc = zzbbq();
            }
            return nextByte;
        }
        throw new NoSuchElementException();
    }
}
