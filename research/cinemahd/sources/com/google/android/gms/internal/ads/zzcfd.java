package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcfd implements Callable {
    private final zzcfe zzfuh;
    private final zzaqk zzfui;

    zzcfd(zzcfe zzcfe, zzaqk zzaqk) {
        this.zzfuh = zzcfe;
        this.zzfui = zzaqk;
    }

    public final Object call() {
        return this.zzfuh.zzd(this.zzfui);
    }
}
