package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfp extends zzfw {
    private static volatile Long zzaaa;
    private static final Object zzzs = new Object();

    public zzfp(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 33);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (zzaaa == null) {
            synchronized (zzzs) {
                if (zzaaa == null) {
                    zzaaa = (Long) this.zzaae.invoke((Object) null, new Object[0]);
                }
            }
        }
        synchronized (this.zzzt) {
            this.zzzt.zzaz(zzaaa.longValue());
        }
    }
}
