package com.google.android.gms.internal.ads;

final class zzbch extends zzbc {
    static final zzbch zzecx = new zzbch();

    zzbch() {
    }

    public final zzbf zza(String str, byte[] bArr, String str2) {
        if ("moov".equals(str)) {
            return new zzbh();
        }
        if ("mvhd".equals(str)) {
            return new zzbk();
        }
        return new zzbj(str);
    }
}
