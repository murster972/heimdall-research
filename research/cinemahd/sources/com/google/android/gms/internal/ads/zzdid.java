package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public interface zzdid<P> {
    String getKeyType();

    P zza(zzdte zzdte) throws GeneralSecurityException;

    Class<P> zzarz();

    P zzm(zzdqk zzdqk) throws GeneralSecurityException;

    zzdte zzn(zzdqk zzdqk) throws GeneralSecurityException;

    zzdna zzo(zzdqk zzdqk) throws GeneralSecurityException;
}
