package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;

public final class zzdwx {
    public static final zzdwx zzhzt = new zzdwx(1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzdwx zzhzu = new zzdwx(0.0d, 1.0d, -1.0d, 0.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzdwx zzhzv = new zzdwx(-1.0d, 0.0d, 0.0d, -1.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);
    private static final zzdwx zzhzw = new zzdwx(0.0d, -1.0d, 1.0d, 0.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d);

    /* renamed from: a  reason: collision with root package name */
    private final double f4147a;
    private final double b;
    private final double c;
    private final double d;
    private final double w;
    private final double zzhzp;
    private final double zzhzq;
    private final double zzhzr;
    private final double zzhzs;

    private zzdwx(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10) {
        this.zzhzp = d6;
        this.zzhzq = d7;
        this.w = d8;
        this.f4147a = d2;
        this.b = d3;
        this.c = d4;
        this.d = d5;
        this.zzhzr = d9;
        this.zzhzs = d10;
    }

    public static zzdwx zzp(ByteBuffer byteBuffer) {
        double zzd = zzbg.zzd(byteBuffer);
        double zzd2 = zzbg.zzd(byteBuffer);
        double zze = zzbg.zze(byteBuffer);
        return new zzdwx(zzd, zzd2, zzbg.zzd(byteBuffer), zzbg.zzd(byteBuffer), zze, zzbg.zze(byteBuffer), zzbg.zze(byteBuffer), zzbg.zzd(byteBuffer), zzbg.zzd(byteBuffer));
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || zzdwx.class != obj.getClass()) {
            return false;
        }
        zzdwx zzdwx = (zzdwx) obj;
        return Double.compare(zzdwx.f4147a, this.f4147a) == 0 && Double.compare(zzdwx.b, this.b) == 0 && Double.compare(zzdwx.c, this.c) == 0 && Double.compare(zzdwx.d, this.d) == 0 && Double.compare(zzdwx.zzhzr, this.zzhzr) == 0 && Double.compare(zzdwx.zzhzs, this.zzhzs) == 0 && Double.compare(zzdwx.zzhzp, this.zzhzp) == 0 && Double.compare(zzdwx.zzhzq, this.zzhzq) == 0 && Double.compare(zzdwx.w, this.w) == 0;
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.zzhzp);
        long doubleToLongBits2 = Double.doubleToLongBits(this.zzhzq);
        long doubleToLongBits3 = Double.doubleToLongBits(this.w);
        long doubleToLongBits4 = Double.doubleToLongBits(this.f4147a);
        long doubleToLongBits5 = Double.doubleToLongBits(this.b);
        long doubleToLongBits6 = Double.doubleToLongBits(this.c);
        long doubleToLongBits7 = Double.doubleToLongBits(this.d);
        long doubleToLongBits8 = Double.doubleToLongBits(this.zzhzr);
        long doubleToLongBits9 = Double.doubleToLongBits(this.zzhzs);
        return (((((((((((((((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)))) * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)))) * 31) + ((int) (doubleToLongBits4 ^ (doubleToLongBits4 >>> 32)))) * 31) + ((int) (doubleToLongBits5 ^ (doubleToLongBits5 >>> 32)))) * 31) + ((int) (doubleToLongBits6 ^ (doubleToLongBits6 >>> 32)))) * 31) + ((int) (doubleToLongBits7 ^ (doubleToLongBits7 >>> 32)))) * 31) + ((int) (doubleToLongBits8 ^ (doubleToLongBits8 >>> 32)))) * 31) + ((int) (doubleToLongBits9 ^ (doubleToLongBits9 >>> 32)));
    }

    public final String toString() {
        if (equals(zzhzt)) {
            return "Rotate 0°";
        }
        if (equals(zzhzu)) {
            return "Rotate 90°";
        }
        if (equals(zzhzv)) {
            return "Rotate 180°";
        }
        if (equals(zzhzw)) {
            return "Rotate 270°";
        }
        double d2 = this.zzhzp;
        double d3 = this.zzhzq;
        double d4 = this.w;
        double d5 = this.f4147a;
        double d6 = this.b;
        double d7 = this.c;
        double d8 = this.d;
        double d9 = this.zzhzr;
        double d10 = this.zzhzs;
        double d11 = d8;
        StringBuilder sb = new StringBuilder(260);
        sb.append("Matrix{u=");
        sb.append(d2);
        sb.append(", v=");
        sb.append(d3);
        sb.append(", w=");
        sb.append(d4);
        sb.append(", a=");
        sb.append(d5);
        sb.append(", b=");
        sb.append(d6);
        sb.append(", c=");
        sb.append(d7);
        sb.append(", d=");
        sb.append(d11);
        sb.append(", tx=");
        sb.append(d9);
        sb.append(", ty=");
        sb.append(d10);
        sb.append("}");
        return sb.toString();
    }
}
