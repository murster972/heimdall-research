package com.google.android.gms.internal.cast;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;

final class zzes extends zzey {
    private final /* synthetic */ zzex zzabf;
    private final /* synthetic */ zzer zzabg;

    zzes(zzer zzer, zzex zzex) {
        this.zzabg = zzer;
        this.zzabf = zzex;
    }

    public final void zzw(int i) throws RemoteException {
        zzer.zzbf.d("onRemoteDisplayEnded", new Object[0]);
        zzex zzex = this.zzabf;
        if (zzex != null) {
            zzex.zzw(i);
        }
        if (this.zzabg.zzabd != null) {
            this.zzabg.zzabd.a(new Status(i));
        }
    }
}
