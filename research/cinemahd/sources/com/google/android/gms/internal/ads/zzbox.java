package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbox implements zzbrn {
    private final int zzdvv;

    zzbox(int i) {
        this.zzdvv = i;
    }

    public final void zzp(Object obj) {
        ((zzbow) obj).onAdFailedToLoad(this.zzdvv);
    }
}
