package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;
import java.io.IOException;

public final class zzdwj extends zzdvq<zzdwj> {
    public String mimeType = null;
    public zzdvx.zzb.zzf.C0045zzb zzhyi = null;
    public byte[] zzhyj = null;

    public zzdwj() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        zzdvx.zzb.zzf.C0045zzb zzb = this.zzhyi;
        if (!(zzb == null || zzb == null)) {
            zzdvo.zzab(1, zzb.zzae());
        }
        String str = this.mimeType;
        if (str != null) {
            zzdvo.zzf(2, str);
        }
        byte[] bArr = this.zzhyj;
        if (bArr != null) {
            zzdvo.zza(3, bArr);
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int zzoi = super.zzoi();
        zzdvx.zzb.zzf.C0045zzb zzb = this.zzhyi;
        if (!(zzb == null || zzb == null)) {
            zzoi += zzdvo.zzaf(1, zzb.zzae());
        }
        String str = this.mimeType;
        if (str != null) {
            zzoi += zzdvo.zzg(2, str);
        }
        byte[] bArr = this.zzhyj;
        return bArr != null ? zzoi + zzdvo.zzfz(3) + zzdvo.zzgh(bArr.length) + bArr.length : zzoi;
    }
}
