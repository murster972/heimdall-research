package com.google.android.gms.internal.ads;

public final class zzbhh {
    private zzbga zzejy;
    private zzbhq zzeyc;
    private zzdcy zzeyd;
    private zzbhx zzeye;
    private zzdag zzeyf;

    private zzbhh() {
    }

    public final zzbhh zza(zzbhq zzbhq) {
        this.zzeyc = (zzbhq) zzdxm.checkNotNull(zzbhq);
        return this;
    }

    public final zzbfx zzael() {
        zzdxm.zza(this.zzejy, zzbga.class);
        zzdxm.zza(this.zzeyc, zzbhq.class);
        if (this.zzeyd == null) {
            this.zzeyd = new zzdcy();
        }
        if (this.zzeye == null) {
            this.zzeye = new zzbhx();
        }
        if (this.zzeyf == null) {
            this.zzeyf = new zzdag();
        }
        return new zzbgr(this.zzejy, this.zzeyc, this.zzeyd, this.zzeye, this.zzeyf);
    }

    public final zzbhh zzc(zzbga zzbga) {
        this.zzejy = (zzbga) zzdxm.checkNotNull(zzbga);
        return this;
    }
}
