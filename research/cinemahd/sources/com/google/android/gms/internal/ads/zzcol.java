package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;

public final class zzcol extends zzvl {
    private final zzcot zzgcq;

    public zzcol(Context context, zzbfx zzbfx, zzczw zzczw, zzbwz zzbwz, zzvh zzvh) {
        zzcov zzcov = new zzcov(zzbwz);
        zzcov.zzc(zzvh);
        this.zzgcq = new zzcot(new zzcpb(zzbfx, context, zzcov, zzczw), zzczw.zzaor());
    }

    public final synchronized String getMediationAdapterClassName() {
        return this.zzgcq.getMediationAdapterClassName();
    }

    public final synchronized boolean isLoading() throws RemoteException {
        return this.zzgcq.isLoading();
    }

    public final synchronized void zza(zzug zzug, int i) throws RemoteException {
        this.zzgcq.zza(zzug, i);
    }

    public final void zzb(zzug zzug) throws RemoteException {
        this.zzgcq.zza(zzug, 1);
    }

    public final synchronized String zzka() {
        return this.zzgcq.zzka();
    }
}
