package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.CalendarContract;
import android.text.TextUtils;
import com.google.android.gms.ads.impl.R;
import com.google.android.gms.ads.internal.zzq;
import com.uwetrottmann.thetvdb.TheTvdb;
import java.util.Map;

public final class zzanz extends zzaoo {
    private final Map<String, String> zzcsd;
    private String zzdez = zzdq("description");
    private long zzdfa = zzdr("start_ticks");
    private long zzdfb = zzdr("end_ticks");
    private String zzdfc = zzdq("summary");
    private String zzdfd = zzdq("location");
    /* access modifiers changed from: private */
    public final Context zzup;

    public zzanz(zzbdi zzbdi, Map<String, String> map) {
        super(zzbdi, "createCalendarEvent");
        this.zzcsd = map;
        this.zzup = zzbdi.zzyn();
    }

    private final String zzdq(String str) {
        return TextUtils.isEmpty(this.zzcsd.get(str)) ? "" : this.zzcsd.get(str);
    }

    private final long zzdr(String str) {
        String str2 = this.zzcsd.get(str);
        if (str2 == null) {
            return -1;
        }
        try {
            return Long.parseLong(str2);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(14)
    public final Intent createIntent() {
        Intent data = new Intent("android.intent.action.EDIT").setData(CalendarContract.Events.CONTENT_URI);
        data.putExtra("title", this.zzdez);
        data.putExtra("eventLocation", this.zzdfd);
        data.putExtra("description", this.zzdfc);
        long j = this.zzdfa;
        if (j > -1) {
            data.putExtra("beginTime", j);
        }
        long j2 = this.zzdfb;
        if (j2 > -1) {
            data.putExtra("endTime", j2);
        }
        data.setFlags(268435456);
        return data;
    }

    public final void execute() {
        if (this.zzup == null) {
            zzds("Activity context is not available.");
            return;
        }
        zzq.zzkq();
        if (!zzawb.zzas(this.zzup).zzqe()) {
            zzds("This feature is not available on the device.");
            return;
        }
        zzq.zzkq();
        AlertDialog.Builder zzar = zzawb.zzar(this.zzup);
        Resources resources = zzq.zzku().getResources();
        zzar.setTitle(resources != null ? resources.getString(R.string.s5) : "Create calendar event");
        zzar.setMessage(resources != null ? resources.getString(R.string.s6) : "Allow Ad to create a calendar event?");
        zzar.setPositiveButton(resources != null ? resources.getString(R.string.s3) : TheTvdb.HEADER_ACCEPT, new zzaoc(this));
        zzar.setNegativeButton(resources != null ? resources.getString(R.string.s4) : "Decline", new zzaob(this));
        zzar.create().show();
    }
}
