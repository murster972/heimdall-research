package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzcrm implements zzdxg<zzcrk<zzcrg>> {
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<zzcrj> zzgfp;

    public zzcrm(zzdxp<zzcrj> zzdxp, zzdxp<Clock> zzdxp2) {
        this.zzgfp = zzdxp;
        this.zzfcz = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return (zzcrk) zzdxm.zza(new zzcrk(this.zzgfp.get(), 10000, this.zzfcz.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
