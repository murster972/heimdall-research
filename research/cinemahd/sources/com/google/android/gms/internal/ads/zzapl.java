package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.query.QueryData;
import com.google.android.gms.ads.query.QueryDataGenerationCallback;

final class zzapl extends zzaum {
    private final /* synthetic */ QueryDataGenerationCallback zzdid;

    zzapl(zzapj zzapj, QueryDataGenerationCallback queryDataGenerationCallback) {
        this.zzdid = queryDataGenerationCallback;
    }

    public final void onError(String str) throws RemoteException {
        this.zzdid.onFailure(str);
    }

    public final void zzk(String str, String str2) throws RemoteException {
        QueryData queryData = new QueryData(new zzxx(str));
        zzve.zzpc().put(queryData, str2);
        this.zzdid.onSuccess(queryData);
    }
}
