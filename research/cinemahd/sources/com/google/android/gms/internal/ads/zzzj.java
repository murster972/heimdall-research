package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.ConditionVariable;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.wrappers.Wrappers;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzzj implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final Object lock = new Object();
    private Bundle metaData = new Bundle();
    private final ConditionVariable zzcga = new ConditionVariable();
    private volatile boolean zzcgb = false;
    /* access modifiers changed from: private */
    public SharedPreferences zzcgc = null;
    private Context zzcgd;
    private JSONObject zzcge = new JSONObject();
    private volatile boolean zzxx = false;

    private final void zzqi() {
        if (this.zzcgc != null) {
            try {
                this.zzcge = new JSONObject((String) zzayc.zza(new zzzl(this)));
            } catch (JSONException unused) {
            }
        }
    }

    public final void initialize(Context context) {
        if (!this.zzxx) {
            synchronized (this.lock) {
                if (!this.zzxx) {
                    if (!this.zzcgb) {
                        this.zzcgb = true;
                    }
                    this.zzcgd = context.getApplicationContext() == null ? context : context.getApplicationContext();
                    try {
                        this.metaData = Wrappers.a(this.zzcgd).a(this.zzcgd.getPackageName(), 128).metaData;
                    } catch (PackageManager.NameNotFoundException | NullPointerException unused) {
                    }
                    try {
                        Context remoteContext = GooglePlayServicesUtilLight.getRemoteContext(context);
                        if (remoteContext == null && context != null && (remoteContext = context.getApplicationContext()) == null) {
                            remoteContext = context;
                        }
                        if (remoteContext != null) {
                            zzve.zzow();
                            this.zzcgc = remoteContext.getSharedPreferences("google_ads_flags", 0);
                            if (this.zzcgc != null) {
                                this.zzcgc.registerOnSharedPreferenceChangeListener(this);
                            }
                            zzabn.zza(new zzzo(this));
                            zzqi();
                            this.zzxx = true;
                            this.zzcgb = false;
                            this.zzcga.open();
                        }
                    } finally {
                        this.zzcgb = false;
                        this.zzcga.open();
                    }
                }
            }
        }
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if ("flag_configuration".equals(str)) {
            zzqi();
        }
    }

    public final <T> T zzd(zzzc<T> zzzc) {
        if (!this.zzcga.block(5000)) {
            synchronized (this.lock) {
                if (!this.zzcgb) {
                    throw new IllegalStateException("Flags.initialize() was not called!");
                }
            }
        }
        if (!this.zzxx || this.zzcgc == null) {
            synchronized (this.lock) {
                if (this.zzxx) {
                    if (this.zzcgc == null) {
                    }
                }
                T zzqf = zzzc.zzqf();
                return zzqf;
            }
        }
        if (zzzc.getSource() == 2) {
            Bundle bundle = this.metaData;
            if (bundle == null) {
                return zzzc.zzqf();
            }
            return zzzc.zza(bundle);
        } else if (zzzc.getSource() != 1 || !this.zzcge.has(zzzc.getKey())) {
            return zzayc.zza(new zzzm(this, zzzc));
        } else {
            return zzzc.zza(this.zzcge);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zze(zzzc zzzc) {
        return zzzc.zza(this.zzcgc);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzqj() {
        return this.zzcgc.getString("flag_configuration", "{}");
    }
}
