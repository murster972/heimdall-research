package com.google.android.gms.internal.ads;

import java.util.Set;

final class zzbhe implements zzblp {
    private zzdxp<Set<zzbsu<zzbph>>> zzerv;
    private zzdxp<zzbpg> zzerw;
    private zzdxp<zzczt> zzerx;
    private zzdxp<zzczl> zzery;
    private zzdxp<Set<zzbsu<zzbqb>>> zzesp;
    private zzdxp<zzbpw> zzesq;
    private zzdxp<String> zzeta;
    private zzdxp<zzbom> zzetb;
    private zzdxp<zzbmg> zzetc;
    private final zzbls zzexf;
    private zzdxp<zzaea> zzexg;
    private zzdxp<Runnable> zzexh;
    private zzdxp<zzblo> zzexi;
    private final /* synthetic */ zzbhc zzexj;

    private zzbhe(zzbhc zzbhc, zzbmt zzbmt, zzbls zzbls) {
        this.zzexj = zzbhc;
        this.zzexf = zzbls;
        this.zzerx = zzbmy.zze(zzbmt);
        this.zzery = zzbmw.zzc(zzbmt);
        this.zzerv = zzdxl.zzar(0, 2).zzaq(this.zzexj.zzeql).zzaq(this.zzexj.zzeqm).zzbdp();
        this.zzerw = zzdxd.zzan(zzbpn.zzi(this.zzerv));
        this.zzesp = zzdxl.zzar(4, 3).zzap(this.zzexj.zzeqz).zzap(this.zzexj.zzera).zzap(this.zzexj.zzerb).zzaq(this.zzexj.zzerc).zzaq(this.zzexj.zzerd).zzaq(this.zzexj.zzere).zzap(this.zzexj.zzerf).zzbdp();
        this.zzesq = zzdxd.zzan(zzbpy.zzk(this.zzesp));
        this.zzeta = zzbmv.zza(zzbmt);
        this.zzetb = zzbop.zzh(this.zzery, this.zzeta);
        this.zzetc = zzbnp.zzb(this.zzerx, this.zzery, this.zzerw, this.zzesq, this.zzexj.zzerm, this.zzetb);
        this.zzexg = new zzblu(zzbls);
        this.zzexh = new zzblr(zzbls);
        this.zzexi = zzdxd.zzan(new zzblt(this.zzetc, this.zzexg, this.zzexh, this.zzexj.zzerr.zzejz));
    }

    public final zzbkk zzaef() {
        return (zzbkk) zzdxm.zza(this.zzexi.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
