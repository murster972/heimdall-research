package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class zzwh extends zzgc implements zzwi {
    zzwh(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
    }
}
