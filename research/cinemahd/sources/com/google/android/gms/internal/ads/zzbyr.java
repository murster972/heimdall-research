package com.google.android.gms.internal.ads;

public final class zzbyr implements zzdxg<zzbyq> {
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<zzbyu> zzfoy;
    private final zzdxp<zzbze> zzfoz;

    public zzbyr(zzdxp<zzdhd> zzdxp, zzdxp<zzbyu> zzdxp2, zzdxp<zzbze> zzdxp3) {
        this.zzfcv = zzdxp;
        this.zzfoy = zzdxp2;
        this.zzfoz = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzbyq(this.zzfcv.get(), this.zzfoy.get(), this.zzfoz.get());
    }
}
