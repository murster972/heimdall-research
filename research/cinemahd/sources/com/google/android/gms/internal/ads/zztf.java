package com.google.android.gms.internal.ads;

final class zztf implements zzdsa {
    static final zzdsa zzew = new zztf();

    private zztf() {
    }

    public final boolean zzf(int i) {
        return zzte.zzbx(i) != null;
    }
}
