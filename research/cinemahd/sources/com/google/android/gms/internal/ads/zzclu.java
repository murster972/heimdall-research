package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class zzclu<AdT> implements zzdgf<zzczt, AdT> {
    private final Executor executor;
    private final zzdda zzfbm;
    private final ScheduledExecutorService zzfdi;
    private final zzdcr zzfgm;
    private final zzbou zzfik;
    private final zzbmi<AdT> zzgaj;
    private final zzclp zzgak;

    public zzclu(zzdcr zzdcr, zzclp zzclp, zzbou zzbou, zzdda zzdda, zzbmi<AdT> zzbmi, Executor executor2, ScheduledExecutorService scheduledExecutorService) {
        this.zzfgm = zzdcr;
        this.zzgak = zzclp;
        this.zzfik = zzbou;
        this.zzfbm = zzdda;
        this.zzgaj = zzbmi;
        this.executor = executor2;
        this.zzfdi = scheduledExecutorService;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzczl zzczl, zzcio zzcio, zzczt zzczt, Throwable th) throws Exception {
        return this.zzgak.zza(zzczl, zzdgs.zza(zzcio.zzb(zzczt, zzczl), (long) zzczl.zzglx, TimeUnit.MILLISECONDS, this.zzfdi));
    }

    public final /* synthetic */ zzdhe zzf(Object obj) throws Exception {
        zzczt zzczt = (zzczt) obj;
        zzdca<E, I> zzaqg = this.zzfgm.zzu(zzdco.RENDER_CONFIG_INIT).zzc(zzdgs.zzk(new zzclr("No ad configs", 3))).zzaqg();
        this.zzfik.zza(new zzbit(zzczt, this.zzfbm), this.executor);
        int i = 0;
        for (zzczl next : zzczt.zzgmi.zzgme) {
            Iterator<String> it2 = next.zzgli.iterator();
            while (true) {
                if (it2.hasNext()) {
                    String next2 = it2.next();
                    zzcio<AdT> zzd = this.zzgaj.zzd(next.zzfjj, next2);
                    if (zzd != null && zzd.zza(zzczt, next)) {
                        zzdcj<I> zza = this.zzfgm.zza(zzdco.RENDER_CONFIG_WATERFALL, zzaqg);
                        StringBuilder sb = new StringBuilder(String.valueOf(next2).length() + 26);
                        sb.append("render-config-");
                        sb.append(i);
                        sb.append("-");
                        sb.append(next2);
                        zzaqg = zza.zzgn(sb.toString()).zza(Throwable.class, new zzclt(this, next, zzd, zzczt)).zzaqg();
                        break;
                    }
                } else {
                    break;
                }
            }
            i++;
        }
        return zzaqg;
    }
}
