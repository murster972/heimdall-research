package com.google.android.gms.internal.ads;

import android.util.JsonReader;
import java.io.InputStream;
import java.io.InputStreamReader;

final /* synthetic */ class zzcpq implements zzdgf {
    private final zzaqk zzfhx;

    zzcpq(zzaqk zzaqk) {
        this.zzfhx = zzaqk;
    }

    public final zzdhe zzf(Object obj) {
        return zzdgs.zzaj(new zzcps(new JsonReader(new InputStreamReader((InputStream) obj))).zzn(this.zzfhx.zzdlu));
    }
}
