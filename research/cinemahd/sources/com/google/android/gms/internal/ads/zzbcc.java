package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.ByteBuffer;

final class zzbcc implements zzdws {
    private final ByteBuffer zzako;

    zzbcc(ByteBuffer byteBuffer) {
        this.zzako = byteBuffer.duplicate();
    }

    public final void close() throws IOException {
    }

    public final long position() throws IOException {
        return (long) this.zzako.position();
    }

    public final int read(ByteBuffer byteBuffer) throws IOException {
        if (this.zzako.remaining() == 0 && byteBuffer.remaining() > 0) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), this.zzako.remaining());
        byte[] bArr = new byte[min];
        this.zzako.get(bArr);
        byteBuffer.put(bArr);
        return min;
    }

    public final long size() throws IOException {
        return (long) this.zzako.limit();
    }

    public final void zzfc(long j) throws IOException {
        this.zzako.position((int) j);
    }

    public final ByteBuffer zzh(long j, long j2) throws IOException {
        int position = this.zzako.position();
        this.zzako.position((int) j);
        ByteBuffer slice = this.zzako.slice();
        slice.limit((int) j2);
        this.zzako.position(position);
        return slice;
    }
}
