package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzadu extends IInterface {
    void zza(zzvu zzvu, IObjectWrapper iObjectWrapper) throws RemoteException;
}
