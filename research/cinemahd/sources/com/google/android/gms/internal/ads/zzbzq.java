package com.google.android.gms.internal.ads;

import java.util.Map;
import java.util.concurrent.Executor;

public final class zzbzq {
    private final Executor zzfci;
    private final zzbst zzfkh;
    private final zzbjq zzfpx;

    zzbzq(Executor executor, zzbjq zzbjq, zzbst zzbst) {
        this.zzfci = executor;
        this.zzfkh = zzbst;
        this.zzfpx = zzbjq;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zze(zzbdi zzbdi, Map map) {
        this.zzfpx.disable();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzf(zzbdi zzbdi, Map map) {
        this.zzfpx.enable();
    }

    public final void zzl(zzbdi zzbdi) {
        if (zzbdi != null) {
            this.zzfkh.zzq(zzbdi.getView());
            this.zzfkh.zza(new zzbzp(zzbdi), this.zzfci);
            this.zzfkh.zza(new zzbzs(zzbdi), this.zzfci);
            this.zzfkh.zza(this.zzfpx, this.zzfci);
            this.zzfpx.zzg(zzbdi);
            zzbdi.zza("/trackActiveViewUnit", (zzafn<? super zzbdi>) new zzbzr(this));
            zzbdi.zza("/untrackActiveViewUnit", (zzafn<? super zzbdi>) new zzbzu(this));
        }
    }
}
