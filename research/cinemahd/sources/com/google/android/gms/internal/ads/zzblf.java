package com.google.android.gms.internal.ads;

public interface zzblf extends zzboe<zzblg> {
    zzblf zza(zzbma zzbma);

    zzblf zza(zzcns zzcns);

    zzblg zzaee();

    zzblf zzb(zzbkf zzbkf);

    zzblf zzb(zzbvi zzbvi);

    zzblf zzc(zzbod zzbod);

    zzblf zzc(zzbrm zzbrm);
}
