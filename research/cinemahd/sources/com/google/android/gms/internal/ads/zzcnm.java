package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcnm implements zzdxg<zzcng> {
    private final zzdxp<zzdhd> zzfei;
    private final zzdxp<zzdcr> zzfet;
    private final zzdxp<zzblg> zzfyl;
    private final zzdxp<Context> zzgbs;
    private final zzdxp<zzaak> zzgbt;

    public zzcnm(zzdxp<Context> zzdxp, zzdxp<zzblg> zzdxp2, zzdxp<zzdcr> zzdxp3, zzdxp<zzdhd> zzdxp4, zzdxp<zzaak> zzdxp5) {
        this.zzgbs = zzdxp;
        this.zzfyl = zzdxp2;
        this.zzfet = zzdxp3;
        this.zzfei = zzdxp4;
        this.zzgbt = zzdxp5;
    }

    public final /* synthetic */ Object get() {
        return new zzcng(this.zzgbs.get(), this.zzfyl.get(), this.zzfet.get(), this.zzfei.get(), this.zzgbt.get());
    }
}
