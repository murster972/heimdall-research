package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzcph implements zzdgt<zzcps> {
    private final /* synthetic */ zzaun zzgdz;

    zzcph(zzcpi zzcpi, zzaun zzaun) {
        this.zzgdz = zzaun;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzcps zzcps = (zzcps) obj;
        try {
            this.zzgdz.zzk(zzcps.zzgeg, zzcps.zzgeh);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final void zzb(Throwable th) {
        try {
            this.zzgdz.onError("Internal error.");
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
