package com.google.android.gms.internal.ads;

import java.util.Comparator;

public final class zzrb implements Comparator<zzqp> {
    public zzrb(zzqy zzqy) {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzqp zzqp = (zzqp) obj;
        zzqp zzqp2 = (zzqp) obj2;
        if (zzqp.zzmi() < zzqp2.zzmi()) {
            return -1;
        }
        if (zzqp.zzmi() > zzqp2.zzmi()) {
            return 1;
        }
        if (zzqp.zzmh() < zzqp2.zzmh()) {
            return -1;
        }
        if (zzqp.zzmh() > zzqp2.zzmh()) {
            return 1;
        }
        float zzmk = (zzqp.zzmk() - zzqp.zzmi()) * (zzqp.zzmj() - zzqp.zzmh());
        float zzmk2 = (zzqp2.zzmk() - zzqp2.zzmi()) * (zzqp2.zzmj() - zzqp2.zzmh());
        if (zzmk > zzmk2) {
            return -1;
        }
        if (zzmk < zzmk2) {
            return 1;
        }
        return 0;
    }
}
