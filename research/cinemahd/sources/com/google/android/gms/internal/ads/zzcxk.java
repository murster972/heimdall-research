package com.google.android.gms.internal.ads;

final class zzcxk implements zzdgt<zzdbi<AdT>> {
    private final /* synthetic */ zzcxg zzgkc;

    zzcxk(zzcxg zzcxg) {
        this.zzgkc = zzcxg;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzdbi zzdbi = (zzdbi) obj;
        synchronized (this.zzgkc) {
            zzdbi.zzgpc = ((zzbob) this.zzgkc.zzgjs.zzaog()).zzadc();
            if (this.zzgkc.zzgjw != zzcxm.zzgke) {
                this.zzgkc.zzgjq.zza(this.zzgkc.zzgjq.zza(this.zzgkc.zzgju.zzdio, this.zzgkc.zzgju.zzbqz, this.zzgkc.zzgju.zzgey), zzdbi);
            }
            if (this.zzgkc.zzgjw == zzcxm.zzgkd) {
                this.zzgkc.zza(this.zzgkc.zzgju);
            }
            int unused = this.zzgkc.zzgjw = zzcxm.zzgkd;
        }
    }

    public final void zzb(Throwable th) {
    }
}
