package com.google.android.gms.internal.ads;

public final class zzbvz implements zzdxg<zzbww> {
    private final zzdxp<zzbwv> zzetz;
    private final zzbvy zzfla;

    public zzbvz(zzbvy zzbvy, zzdxp<zzbwv> zzdxp) {
        this.zzfla = zzbvy;
        this.zzetz = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbww) zzdxm.zza(this.zzetz.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
