package com.google.android.gms.internal.ads;

import java.util.Map;

final class zzafj implements zzafn<zzbdi> {
    zzafj() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        String str = (String) map.get("action");
        if ("pause".equals(str)) {
            zzbdi.zzjv();
        } else if ("resume".equals(str)) {
            zzbdi.zzjw();
        }
    }
}
