package com.google.android.gms.internal.ads;

final /* synthetic */ class zzdcs implements zzbrn {
    private final zzdca zzgrk;

    zzdcs(zzdca zzdca) {
        this.zzgrk = zzdca;
    }

    public final void zzp(Object obj) {
        zzdca zzdca = this.zzgrk;
        ((zzdcx) obj).zzb((zzdco) zzdca.zzaqd(), zzdca.zzaqe());
    }
}
