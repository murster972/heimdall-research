package com.google.android.gms.internal.ads;

import java.util.Map;

final class zzcap implements zzdgt<zzbdi> {
    private final /* synthetic */ String zzfqn;
    private final /* synthetic */ Map zzfqo;

    zzcap(zzcaj zzcaj, String str, Map map) {
        this.zzfqn = str;
        this.zzfqo = map;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        ((zzbdi) obj).zza(this.zzfqn, this.zzfqo);
    }

    public final void zzb(Throwable th) {
    }
}
