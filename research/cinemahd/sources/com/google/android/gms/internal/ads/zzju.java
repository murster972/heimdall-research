package com.google.android.gms.internal.ads;

import java.io.IOException;

interface zzju {
    void zza(int i, double d) throws zzhd;

    void zza(int i, int i2, zzjg zzjg) throws IOException, InterruptedException;

    void zza(int i, String str) throws zzhd;

    int zzah(int i);

    boolean zzai(int i);

    void zzaj(int i) throws zzhd;

    void zzc(int i, long j) throws zzhd;

    void zzd(int i, long j, long j2) throws zzhd;
}
