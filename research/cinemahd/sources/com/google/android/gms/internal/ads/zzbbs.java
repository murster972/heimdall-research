package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.view.Surface;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Set;

public final class zzbbs implements zzgn, zzma, zznz<zznl>, zzpd {
    private static int zzebx;
    private static int zzeby;
    private int bytesTransferred;
    private final zzbaw zzebe;
    private final zzbbt zzebz;
    private final zzhf zzeca;
    private final zzhf zzecb;
    private final zzmz zzecc;
    private zzgk zzecd;
    private ByteBuffer zzece;
    private boolean zzecf;
    private zzbca zzecg;
    private Set<WeakReference<zzbbp>> zzech = new HashSet();
    private final Context zzup;

    public zzbbs(Context context, zzbaw zzbaw) {
        this.zzup = context;
        this.zzebe = zzbaw;
        this.zzebz = new zzbbt();
        this.zzeca = new zzox(this.zzup, zzky.zzazf, 0, zzawb.zzdsr, this, -1);
        this.zzecb = new zzij(zzky.zzazf);
        this.zzecc = new zzmy();
        if (zzavs.zzvs()) {
            String valueOf = String.valueOf(this);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 28);
            sb.append("ExoPlayerAdapter initialize ");
            sb.append(valueOf);
            zzavs.zzed(sb.toString());
        }
        zzebx++;
        this.zzecd = zzgo.zza(new zzhf[]{this.zzecb, this.zzeca}, this.zzecc, this.zzebz);
        this.zzecd.zza((zzgn) this);
    }

    public static int zzzn() {
        return zzebx;
    }

    public static int zzzo() {
        return zzeby;
    }

    public final void finalize() throws Throwable {
        zzebx--;
        if (zzavs.zzvs()) {
            String valueOf = String.valueOf(this);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 26);
            sb.append("ExoPlayerAdapter finalize ");
            sb.append(valueOf);
            zzavs.zzed(sb.toString());
        }
    }

    public final long getBytesTransferred() {
        return (long) this.bytesTransferred;
    }

    public final void release() {
        zzgk zzgk = this.zzecd;
        if (zzgk != null) {
            zzgk.zzb((zzgn) this);
            this.zzecd.release();
            this.zzecd = null;
            zzeby--;
        }
    }

    public final void zza(Surface surface) {
    }

    public final void zza(zzbca zzbca) {
        this.zzecg = zzbca;
    }

    public final void zza(zzhc zzhc) {
    }

    public final void zza(zzhg zzhg, Object obj) {
    }

    public final void zza(zzmr zzmr, zzng zzng) {
    }

    /* access modifiers changed from: package-private */
    public final void zzaw(boolean z) {
        if (this.zzecd != null) {
            for (int i = 0; i < this.zzecd.zzeb(); i++) {
                this.zzecc.zzf(i, !z);
            }
        }
    }

    public final void zzb(IOException iOException) {
        zzbca zzbca = this.zzecg;
        if (zzbca != null) {
            zzbca.zza("onLoadError", iOException);
        }
    }

    public final /* synthetic */ void zzc(Object obj, int i) {
        this.bytesTransferred += i;
    }

    public final void zzcz(int i) {
        for (WeakReference<zzbbp> weakReference : this.zzech) {
            zzbbp zzbbp = (zzbbp) weakReference.get();
            if (zzbbp != null) {
                zzbbp.setReceiveBufferSize(i);
            }
        }
    }

    public final void zzd(String str, long j, long j2) {
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(boolean z, long j) {
        zzbca zzbca = this.zzecg;
        if (zzbca != null) {
            zzbca.zzb(z, j);
        }
    }

    public final void zze(int i, long j) {
    }

    public final void zze(zzit zzit) {
    }

    public final /* bridge */ /* synthetic */ void zze(Object obj) {
    }

    public final void zzed() {
    }

    public final void zzf(zzit zzit) {
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zznl zzfg(String str) {
        zzbbs zzbbs = this.zzebe.zzdzn ? null : this;
        zzbaw zzbaw = this.zzebe;
        return new zzns(str, (zzol<String>) null, zzbbs, zzbaw.zzdzh, zzbaw.zzdzj, true, (zznt) null);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zznl zzfh(String str) {
        zzbbs zzbbs = this.zzebe.zzdzn ? null : this;
        zzbaw zzbaw = this.zzebe;
        zzbbp zzbbp = new zzbbp(str, zzbbs, zzbaw.zzdzh, zzbaw.zzdzj, zzbaw.zzdzm);
        this.zzech.add(new WeakReference(zzbbp));
        return zzbbp;
    }

    public final void zzg(boolean z) {
    }

    public final void zzk(zzgw zzgw) {
    }

    public final zzgk zzzm() {
        return this.zzecd;
    }

    public final zzbbt zzzp() {
        return this.zzebz;
    }

    public final void zza(Uri[] uriArr, String str) {
        zza(uriArr, str, ByteBuffer.allocate(0), false);
    }

    public final void zza(Uri[] uriArr, String str, ByteBuffer byteBuffer, boolean z) {
        zzmb zzmb;
        if (this.zzecd != null) {
            this.zzece = byteBuffer;
            this.zzecf = z;
            if (uriArr.length == 1) {
                zzmb = zzb(uriArr[0], str);
            } else {
                zzmb[] zzmbArr = new zzmb[uriArr.length];
                for (int i = 0; i < uriArr.length; i++) {
                    zzmbArr[i] = zzb(uriArr[i], str);
                }
                zzmb = new zzmg(zzmbArr);
            }
            this.zzecd.zza(zzmb);
            zzeby++;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(float f, boolean z) {
        if (this.zzecd != null) {
            zzgp zzgp = new zzgp(this.zzecb, 2, Float.valueOf(f));
            if (z) {
                this.zzecd.zzb(zzgp);
                return;
            }
            this.zzecd.zza(zzgp);
        }
    }

    private final zzmb zzb(Uri uri, String str) {
        zzbbv zzbbv;
        zzno zzno;
        if (!this.zzecf || this.zzece.limit() <= 0) {
            if (this.zzebe.zzdzm > 0) {
                zzno = new zzbbu(this, str);
            } else {
                zzno = new zzbbx(this, str);
            }
            zzbbz zzbbw = this.zzebe.zzdzn ? new zzbbw(this, zzno) : zzno;
            if (this.zzece.limit() > 0) {
                byte[] bArr = new byte[this.zzece.limit()];
                this.zzece.get(bArr);
                zzbbw = new zzbbz(zzbbw, bArr);
            }
            zzbbv = zzbbw;
        } else {
            byte[] bArr2 = new byte[this.zzece.limit()];
            this.zzece.get(bArr2);
            zzbbv = new zzbbv(bArr2);
        }
        zzji zzji = zzbby.zzecq;
        zzbaw zzbaw = this.zzebe;
        return new zzlx(uri, zzbbv, zzji, zzbaw.zzdzo, zzawb.zzdsr, this, (String) null, zzbaw.zzdzk);
    }

    public final void zza(int i, int i2, int i3, float f) {
        zzbca zzbca = this.zzecg;
        if (zzbca != null) {
            zzbca.zzn(i, i2);
        }
    }

    public final void zza(boolean z, int i) {
        zzbca zzbca = this.zzecg;
        if (zzbca != null) {
            zzbca.zzda(i);
        }
    }

    public final void zza(zzgl zzgl) {
        zzbca zzbca = this.zzecg;
        if (zzbca != null) {
            zzbca.zza("onPlayerError", zzgl);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(Surface surface, boolean z) {
        if (this.zzecd != null) {
            zzgp zzgp = new zzgp(this.zzeca, 1, surface);
            if (z) {
                this.zzecd.zzb(zzgp);
                return;
            }
            this.zzecd.zza(zzgp);
        }
    }

    public final /* synthetic */ void zza(Object obj, zznq zznq) {
        this.bytesTransferred = 0;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zznl zza(zzno zzno) {
        return new zzbbr(this.zzup, zzno.zzih(), this, new zzbcb(this));
    }
}
