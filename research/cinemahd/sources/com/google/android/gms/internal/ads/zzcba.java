package com.google.android.gms.internal.ads;

public final class zzcba implements zzdxg<zzcax> {
    private final zzdxp<zzbws> zzfkx;

    public zzcba(zzdxp<zzbws> zzdxp) {
        this.zzfkx = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcax(this.zzfkx.get());
    }
}
