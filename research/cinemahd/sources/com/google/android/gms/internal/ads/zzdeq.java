package com.google.android.gms.internal.ads;

final class zzdeq {
    static int zzdv(int i) {
        return (int) (((long) Integer.rotateLeft((int) (((long) i) * -862048943), 15)) * 461845907);
    }
}
