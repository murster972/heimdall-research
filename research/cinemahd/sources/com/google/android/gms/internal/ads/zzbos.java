package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbos implements zzdxg<zzboq> {
    private final zzdxp<Set<zzbsu<zzty>>> zzfeo;

    private zzbos(zzdxp<Set<zzbsu<zzty>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbos zzg(zzdxp<Set<zzbsu<zzty>>> zzdxp) {
        return new zzbos(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzboq(this.zzfeo.get());
    }
}
