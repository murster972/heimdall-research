package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdmw extends zzdrt<zzdmw, zza> implements zzdtg {
    private static volatile zzdtn<zzdmw> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmw zzhcp;
    private int zzhaf;
    private zzdmz zzhcn;

    public static final class zza extends zzdrt.zzb<zzdmw, zza> implements zzdtg {
        private zza() {
            super(zzdmw.zzhcp);
        }

        /* synthetic */ zza(zzdmx zzdmx) {
            this();
        }
    }

    static {
        zzdmw zzdmw = new zzdmw();
        zzhcp = zzdmw;
        zzdrt.zza(zzdmw.class, zzdmw);
    }

    private zzdmw() {
    }

    public static zzdmw zzat(zzdqk zzdqk) throws zzdse {
        return (zzdmw) zzdrt.zza(zzhcp, zzdqk);
    }

    public static zzdmw zzavd() {
        return zzhcp;
    }

    public final int getKeySize() {
        return this.zzhaf;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmx.zzdk[i - 1]) {
            case 1:
                return new zzdmw();
            case 2:
                return new zza((zzdmx) null);
            case 3:
                return zzdrt.zza((zzdte) zzhcp, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u000b", new Object[]{"zzhcn", "zzhaf"});
            case 4:
                return zzhcp;
            case 5:
                zzdtn<zzdmw> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmw.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhcp);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdmz zzauz() {
        zzdmz zzdmz = this.zzhcn;
        return zzdmz == null ? zzdmz.zzavg() : zzdmz;
    }
}
