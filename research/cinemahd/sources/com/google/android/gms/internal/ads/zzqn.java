package com.google.android.gms.internal.ads;

import android.webkit.ValueCallback;

final class zzqn implements ValueCallback<String> {
    private final /* synthetic */ zzqk zzbqd;

    zzqn(zzqk zzqk) {
        this.zzbqd = zzqk;
    }

    public final /* synthetic */ void onReceiveValue(Object obj) {
        zzqk zzqk = this.zzbqd;
        zzqk.zzbpz.zza(zzqk.zzbpw, zzqk.zzbpx, (String) obj, zzqk.zzbpy);
    }
}
