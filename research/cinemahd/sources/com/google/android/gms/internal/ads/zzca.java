package com.google.android.gms.internal.ads;

final class zzca implements zzdsa {
    static final zzdsa zzew = new zzca();

    private zzca() {
    }

    public final boolean zzf(int i) {
        return zzbz.zzi(i) != null;
    }
}
