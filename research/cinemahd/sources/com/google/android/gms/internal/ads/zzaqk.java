package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.List;

public final class zzaqk extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaqk> CREATOR = new zzaqn();
    public final ApplicationInfo applicationInfo;
    public final String packageName;
    public final zzazb zzdij;
    public final PackageInfo zzdip;
    public final List<String> zzdiz;
    public final String zzdjj;
    public final Bundle zzdlu;
    public final boolean zzdlv;
    public final String zzdlw;
    public zzdbe zzdlx;
    public String zzdly;

    public zzaqk(Bundle bundle, zzazb zzazb, ApplicationInfo applicationInfo2, String str, List<String> list, PackageInfo packageInfo, String str2, boolean z, String str3, zzdbe zzdbe, String str4) {
        this.zzdlu = bundle;
        this.zzdij = zzazb;
        this.packageName = str;
        this.applicationInfo = applicationInfo2;
        this.zzdiz = list;
        this.zzdip = packageInfo;
        this.zzdjj = str2;
        this.zzdlv = z;
        this.zzdlw = str3;
        this.zzdlx = zzdbe;
        this.zzdly = str4;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzdlu, false);
        SafeParcelWriter.a(parcel, 2, (Parcelable) this.zzdij, i, false);
        SafeParcelWriter.a(parcel, 3, (Parcelable) this.applicationInfo, i, false);
        SafeParcelWriter.a(parcel, 4, this.packageName, false);
        SafeParcelWriter.b(parcel, 5, this.zzdiz, false);
        SafeParcelWriter.a(parcel, 6, (Parcelable) this.zzdip, i, false);
        SafeParcelWriter.a(parcel, 7, this.zzdjj, false);
        SafeParcelWriter.a(parcel, 8, this.zzdlv);
        SafeParcelWriter.a(parcel, 9, this.zzdlw, false);
        SafeParcelWriter.a(parcel, 10, (Parcelable) this.zzdlx, i, false);
        SafeParcelWriter.a(parcel, 11, this.zzdly, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
