package com.google.android.gms.internal.cast;

import com.unity3d.ads.metadata.MediationMetaData;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzco {
    private final String version;
    private final String zzww;
    private final int zzwx;

    public zzco(JSONObject jSONObject) throws JSONException {
        this(jSONObject.optString("applicationName"), jSONObject.optInt("maxPlayers"), jSONObject.optString(MediationMetaData.KEY_VERSION));
    }

    public final int getMaxPlayers() {
        return this.zzwx;
    }

    public final String getVersion() {
        return this.version;
    }

    public final String zzeo() {
        return this.zzww;
    }

    private zzco(String str, int i, String str2) {
        this.zzww = str;
        this.zzwx = i;
        this.version = str2;
    }
}
