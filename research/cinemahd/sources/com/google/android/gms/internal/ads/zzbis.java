package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.zzq;
import com.vungle.warren.model.CookieDBAdapter;
import java.util.Map;

public final class zzbis implements zzbil {
    private final Context zzup;

    public zzbis(Context context) {
        this.zzup = context;
    }

    public final void zzk(Map<String, String> map) {
        CookieManager zzbd;
        String str = map.get(CookieDBAdapter.CookieColumns.TABLE_NAME);
        if (!TextUtils.isEmpty(str) && (zzbd = zzq.zzks().zzbd(this.zzup)) != null) {
            zzbd.setCookie("googleads.g.doubleclick.net", str);
        }
    }
}
