package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import org.json.JSONObject;

final class zzbgx extends zzcut {
    private zzdxp<String> zzemd;
    private zzdxp<zzcdh> zzemt;
    private zzdxp<Map<zzdco, zzcdh>> zzemv;
    private zzdxp<Set<zzbsu<zzdcx>>> zzemx;
    private zzdxp<Set<zzbsu<zzdcx>>> zzene;
    private zzdxp zzenf;
    private zzdxp<zzdcr> zzeng;
    private zzdxp<ApplicationInfo> zzenh;
    private final /* synthetic */ zzbgr zzerr;
    private final zzcvw zzeuu;
    private zzdxp<zzcup> zzeuv;
    private zzdxp<String> zzeuw;
    private zzdxp<zzcuz> zzeux;
    private zzdxp<zzcvd> zzeuy;
    private zzdxp<zzcvi> zzeuz;
    private zzdxp<Boolean> zzeva;
    private zzdxp<zzcvr> zzevb;
    private zzdxp<zzcvv> zzevc;
    private zzdxp<zzcwg> zzevd;
    private zzdxp<zzcdh> zzeve;
    private zzdxp<zzcdh> zzevf;
    private zzdxp<zzcdh> zzevg;

    private zzbgx(zzbgr zzbgr, zzcvw zzcvw) {
        this.zzerr = zzbgr;
        this.zzeuu = zzcvw;
        this.zzeuv = new zzcuq(zzbih.zzafi(), this.zzerr.zzekf, this.zzerr.zzekb, zzdbv.zzapz());
        this.zzeuw = new zzcvz(zzcvw);
        this.zzeux = new zzcvb(zzbgk.zzacu(), this.zzerr.zzekf, this.zzeuw, zzdbv.zzapz());
        this.zzeuy = new zzcvf(zzbid.zzafb(), zzdbv.zzapz(), this.zzerr.zzekf);
        this.zzeuz = new zzcvk(zzbig.zzafg(), zzdbv.zzapz(), this.zzeuw);
        this.zzeva = new zzcwb(zzcvw);
        this.zzenh = new zzcvy(zzcvw);
        this.zzevb = new zzcvt(zzbif.zzafe(), this.zzerr.zzekb, this.zzeva, this.zzenh);
        this.zzevc = new zzcvx(zzbii.zzafk(), this.zzerr.zzekb, this.zzerr.zzekf);
        this.zzevd = new zzcwi(zzdbv.zzapz());
        this.zzemd = new zzcwa(zzcvw);
        this.zzemt = zzdxd.zzan(zzcdb.zzalb());
        this.zzeve = zzdxd.zzan(zzcdc.zzalc());
        this.zzevf = zzdxd.zzan(zzcde.zzale());
        this.zzevg = zzdxd.zzan(zzcdd.zzald());
        this.zzemv = ((zzdxk) ((zzdxk) ((zzdxk) ((zzdxk) zzdxi.zzhl(4).zza(zzdco.GMS_SIGNALS, this.zzemt)).zza(zzdco.BUILD_URL, this.zzeve)).zza(zzdco.HTTP, this.zzevf)).zza(zzdco.PRE_PROCESS, this.zzevg)).zzbdo();
        this.zzemx = zzdxd.zzan(new zzcdg(this.zzemd, this.zzerr.zzekf, zzdbv.zzapz(), this.zzemv));
        this.zzene = zzdxl.zzar(0, 1).zzaq(this.zzemx).zzbdp();
        this.zzenf = zzdcz.zzam(this.zzene);
        this.zzeng = zzdxd.zzan(zzdcw.zzr(zzdbv.zzapz(), this.zzerr.zzekb, this.zzenf));
    }

    private final zzcvm zzadr() {
        return new zzcvm(zzbie.zzafd(), zzdbv.zzaqa(), (List) zzdxm.zza(this.zzeuu.zzanx(), "Cannot return null from a non-@Nullable @Provides method"));
    }

    private final zzcuv zzads() {
        return new zzcuv(zzbih.zzafj(), zzdbv.zzaqa(), (String) zzdxm.zza(this.zzeuu.zzanu(), "Cannot return null from a non-@Nullable @Provides method"), this.zzeuu.zzanv());
    }

    public final zzcua<JSONObject> zzadt() {
        zzdhd zzaqa = zzdbv.zzaqa();
        long longValue = ((Long) zzve.zzoy().zzd(zzzn.zzcmn)).longValue();
        long longValue2 = ((Long) zzve.zzoy().zzd(zzzn.zzcmu)).longValue();
        return new zzcua<>(zzaqa, zzdfb.zza((zzcub) zzdxm.zza(new zzcsx(new zzcvi(zzbig.zzafh(), zzdbv.zzaqa(), zzcvz.zzc(this.zzeuu)), 0, (ScheduledExecutorService) this.zzerr.zzekb.get()), "Cannot return null from a non-@Nullable @Provides method"), (zzcub) zzdxm.zza(new zzcsx(new zzcvr(zzbif.zzaff(), (ScheduledExecutorService) this.zzerr.zzekb.get(), this.zzeuu.zzanw(), zzcvy.zzb(this.zzeuu)), longValue, (ScheduledExecutorService) this.zzerr.zzekb.get()), "Cannot return null from a non-@Nullable @Provides method"), (zzcub) zzdxm.zza(new zzcsx(new zzcvv(zzbii.zzafl(), (ScheduledExecutorService) this.zzerr.zzekb.get(), zzbgd.zza(this.zzerr.zzejy)), longValue2, (ScheduledExecutorService) this.zzerr.zzekb.get()), "Cannot return null from a non-@Nullable @Provides method"), (zzcub) zzdxm.zza(new zzcsx(new zzcup(zzbih.zzafj(), zzbgd.zza(this.zzerr.zzejy), (ScheduledExecutorService) this.zzerr.zzekb.get(), zzdbv.zzaqa()), 0, (ScheduledExecutorService) this.zzerr.zzekb.get()), "Cannot return null from a non-@Nullable @Provides method"), (zzcub) zzdxm.zza(new zzcsx(new zzcwg(zzdbv.zzaqa()), 0, (ScheduledExecutorService) this.zzerr.zzekb.get()), "Cannot return null from a non-@Nullable @Provides method"), zzcwf.zzaoa(), new zzcuz((zzaqr) null, zzbgd.zza(this.zzerr.zzejy), zzcvz.zzc(this.zzeuu), zzdbv.zzaqa()), new zzcvd(zzbid.zzafc(), zzdbv.zzaqa(), zzbgd.zza(this.zzerr.zzejy)), zzadr(), zzads(), (zzcub) this.zzerr.zzekt.get()));
    }

    public final zzcua<JSONObject> zzadu() {
        return zzcwe.zza(this.zzerr.zzekt.get(), zzads(), zzadr(), zzdxd.zzao(this.zzeuv), zzdxd.zzao(this.zzeux), zzdxd.zzao(this.zzeuy), zzdxd.zzao(this.zzeuz), zzdxd.zzao(this.zzevb), zzdxd.zzao(this.zzevc), zzdxd.zzao(this.zzevd), zzdbv.zzaqa());
    }

    public final zzdcr zzadv() {
        return this.zzeng.get();
    }
}
