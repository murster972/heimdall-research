package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.reward.AdMetadataListener;
import com.google.android.gms.internal.ads.zzbod;
import com.google.android.gms.internal.ads.zzbrm;
import java.util.concurrent.Executor;

public final class zzcyt implements zzcox<zzcbb> {
    private final Executor zzfci;
    private final zzczs zzfhh;
    private final zzczw zzgcs;
    private final zzbfx zzgea;
    private final Context zzgim;
    /* access modifiers changed from: private */
    public final zzcxt<zzcbi, zzcbb> zzgio;
    /* access modifiers changed from: private */
    public final zzcxz zzgks;
    private zzdhe<zzcbb> zzgkt;

    public zzcyt(Context context, Executor executor, zzbfx zzbfx, zzcxt<zzcbi, zzcbb> zzcxt, zzcxz zzcxz, zzczw zzczw, zzczs zzczs) {
        this.zzgim = context;
        this.zzfci = executor;
        this.zzgea = zzbfx;
        this.zzgio = zzcxt;
        this.zzgks = zzcxz;
        this.zzgcs = zzczw;
        this.zzfhh = zzczs;
    }

    public final boolean isLoading() {
        zzdhe<zzcbb> zzdhe = this.zzgkt;
        return zzdhe != null && !zzdhe.isDone();
    }

    public final boolean zza(zzug zzug, String str, zzcpa zzcpa, zzcoz<? super zzcbb> zzcoz) throws RemoteException {
        zzaru zzaru = new zzaru(zzug, str);
        String str2 = zzcpa instanceof zzcyq ? ((zzcyq) zzcpa).zzgkq : null;
        if (zzaru.zzbqz == null) {
            zzayu.zzex("Ad unit ID should not be null for rewarded video ad.");
            this.zzfci.execute(new zzcys(this));
            return false;
        }
        zzdhe<zzcbb> zzdhe = this.zzgkt;
        if (zzdhe != null && !zzdhe.isDone()) {
            return false;
        }
        zzdad.zze(this.zzgim, zzaru.zzdio.zzccb);
        zzczu zzaos = this.zzgcs.zzgk(zzaru.zzbqz).zzd(zzuj.zzol()).zzg(zzaru.zzdio).zzaos();
        zzcyx zzcyx = new zzcyx((zzcyu) null);
        zzcyx.zzfgl = zzaos;
        zzcyx.zzgkq = str2;
        this.zzgkt = this.zzgio.zza(zzcyx, new zzcyv(this));
        zzdgs.zza(this.zzgkt, new zzcyu(this, zzcoz), this.zzfci);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void zzaoj() {
        this.zzgcs.zzgms.add("new_rewarded");
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaok() {
        this.zzgks.onAdFailedToLoad(1);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcbh zzd(zzcxs zzcxs) {
        zzcyx zzcyx = (zzcyx) zzcxs;
        zzcxz zza = zzcxz.zza(this.zzgks);
        zzbrm.zza zza2 = new zzbrm.zza();
        zza2.zza((zzbov) zza, this.zzfci);
        zza2.zza((zzbqb) zza, this.zzfci);
        zza2.zza((zzbow) zza, this.zzfci);
        zza2.zza((AdMetadataListener) zza, this.zzfci);
        zza2.zza((zzbpa) zza, this.zzfci);
        zza2.zza((zzcxq) zza);
        return this.zzgea.zzacm().zze(new zzbod.zza().zzbz(this.zzgim).zza(zzcyx.zzfgl).zzfs(zzcyx.zzgkq).zza(this.zzfhh).zzahh()).zze(zza2.zzahw());
    }
}
