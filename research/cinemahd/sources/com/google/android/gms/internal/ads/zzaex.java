package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.Map;

public final class zzaex implements zzafn<zzbdi> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        String str = (String) map.get("action");
        if ("tick".equals(str)) {
            String str2 = (String) map.get("label");
            String str3 = (String) map.get("start_label");
            String str4 = (String) map.get(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP);
            if (TextUtils.isEmpty(str2)) {
                zzayu.zzez("No label given for CSI tick.");
            } else if (TextUtils.isEmpty(str4)) {
                zzayu.zzez("No timestamp given for CSI tick.");
            } else {
                try {
                    long a2 = zzq.zzkx().a() + (Long.parseLong(str4) - zzq.zzkx().b());
                    if (TextUtils.isEmpty(str3)) {
                        str3 = "native:view_load";
                    }
                    zzbdi.zzyq().zza(str2, str3, a2);
                } catch (NumberFormatException e) {
                    zzayu.zzd("Malformed timestamp for CSI tick.", e);
                }
            }
        } else if ("experiment".equals(str)) {
            String str5 = (String) map.get("value");
            if (TextUtils.isEmpty(str5)) {
                zzayu.zzez("No value given for CSI experiment.");
                return;
            }
            zzaae zzqp = zzbdi.zzyq().zzqp();
            if (zzqp == null) {
                zzayu.zzez("No ticker for WebView, dropping experiment ID.");
            } else {
                zzqp.zzh("e", str5);
            }
        } else if ("extra".equals(str)) {
            String str6 = (String) map.get(MediationMetaData.KEY_NAME);
            String str7 = (String) map.get("value");
            if (TextUtils.isEmpty(str7)) {
                zzayu.zzez("No value given for CSI extra.");
            } else if (TextUtils.isEmpty(str6)) {
                zzayu.zzez("No name given for CSI extra.");
            } else {
                zzaae zzqp2 = zzbdi.zzyq().zzqp();
                if (zzqp2 == null) {
                    zzayu.zzez("No ticker for WebView, dropping extra parameter.");
                } else {
                    zzqp2.zzh(str6, str7);
                }
            }
        }
    }
}
