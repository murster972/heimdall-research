package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

public final class zzbru implements zzdxg<Set<zzbsu<zzbrb>>> {
    private final zzbrm zzfim;

    private zzbru(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbru zzl(zzbrm zzbrm) {
        return new zzbru(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.emptySet(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
