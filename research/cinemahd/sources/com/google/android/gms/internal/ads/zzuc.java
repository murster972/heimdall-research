package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.AdListener;

public final class zzuc extends zzvk {
    private final AdListener zzcbw;

    public zzuc(AdListener adListener) {
        this.zzcbw = adListener;
    }

    public final AdListener getAdListener() {
        return this.zzcbw;
    }

    public final void onAdClicked() {
        this.zzcbw.onAdClicked();
    }

    public final void onAdClosed() {
        this.zzcbw.onAdClosed();
    }

    public final void onAdFailedToLoad(int i) {
        this.zzcbw.onAdFailedToLoad(i);
    }

    public final void onAdImpression() {
        this.zzcbw.onAdImpression();
    }

    public final void onAdLeftApplication() {
        this.zzcbw.onAdLeftApplication();
    }

    public final void onAdLoaded() {
        this.zzcbw.onAdLoaded();
    }

    public final void onAdOpened() {
        this.zzcbw.onAdOpened();
    }
}
