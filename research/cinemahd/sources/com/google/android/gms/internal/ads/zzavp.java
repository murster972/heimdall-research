package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.Clock;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public final class zzavp implements zzqj {
    private final Object lock = new Object();
    private final zzavu zzdrk;
    private final zzavn zzdrq;
    private final zzavl zzdrr;
    private final HashSet<zzavd> zzdrs = new HashSet<>();
    private final HashSet<zzavm> zzdrt = new HashSet<>();

    public zzavp(String str, zzavu zzavu) {
        this.zzdrr = new zzavl(str, zzavu);
        this.zzdrk = zzavu;
        this.zzdrq = new zzavn();
    }

    public final Bundle zza(Context context, zzavk zzavk) {
        HashSet hashSet = new HashSet();
        synchronized (this.lock) {
            hashSet.addAll(this.zzdrs);
            this.zzdrs.clear();
        }
        Bundle bundle = new Bundle();
        bundle.putBundle("app", this.zzdrr.zzo(context, this.zzdrq.zzvq()));
        Bundle bundle2 = new Bundle();
        Iterator<zzavm> it2 = this.zzdrt.iterator();
        if (!it2.hasNext()) {
            bundle.putBundle("slots", bundle2);
            ArrayList arrayList = new ArrayList();
            Iterator it3 = hashSet.iterator();
            while (it3.hasNext()) {
                arrayList.add(((zzavd) it3.next()).toBundle());
            }
            bundle.putParcelableArrayList("ads", arrayList);
            zzavk.zza(hashSet);
            return bundle;
        }
        zzavm next = it2.next();
        throw new NoSuchMethodError();
    }

    public final void zzb(zzavd zzavd) {
        synchronized (this.lock) {
            this.zzdrs.add(zzavd);
        }
    }

    public final void zzp(boolean z) {
        long b = zzq.zzkx().b();
        if (z) {
            if (b - this.zzdrk.zzwb() > ((Long) zzve.zzoy().zzd(zzzn.zzcjc)).longValue()) {
                this.zzdrr.zzdrh = -1;
                return;
            }
            this.zzdrr.zzdrh = this.zzdrk.zzwc();
            return;
        }
        this.zzdrk.zzez(b);
        this.zzdrk.zzcq(this.zzdrr.zzdrh);
    }

    public final void zzuv() {
        synchronized (this.lock) {
            this.zzdrr.zzuv();
        }
    }

    public final void zzuw() {
        synchronized (this.lock) {
            this.zzdrr.zzuw();
        }
    }

    public final void zzb(HashSet<zzavd> hashSet) {
        synchronized (this.lock) {
            this.zzdrs.addAll(hashSet);
        }
    }

    public final void zza(zzug zzug, long j) {
        synchronized (this.lock) {
            this.zzdrr.zza(zzug, j);
        }
    }

    public final zzavd zza(Clock clock, String str) {
        return new zzavd(clock, this, this.zzdrq.zzvp(), str);
    }
}
