package com.google.android.gms.internal.measurement;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class zzdh extends WeakReference<Throwable> {
    private final int zza;

    public zzdh(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.zza = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == zzdh.class) {
            if (this == obj) {
                return true;
            }
            zzdh zzdh = (zzdh) obj;
            return this.zza == zzdh.zza && get() == zzdh.get();
        }
    }

    public final int hashCode() {
        return this.zza;
    }
}
