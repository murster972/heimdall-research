package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.IOException;

final class zzbcd implements zznl {
    private Uri uri;
    private final zznl zzecs;
    private final long zzect;
    private final zznl zzecu;
    private long zzecv;

    zzbcd(zznl zznl, int i, zznl zznl2) {
        this.zzecs = zznl;
        this.zzect = (long) i;
        this.zzecu = zznl2;
    }

    public final void close() throws IOException {
        this.zzecs.close();
        this.zzecu.close();
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        long j = this.zzecv;
        long j2 = this.zzect;
        if (j < j2) {
            i3 = this.zzecs.read(bArr, i, (int) Math.min((long) i2, j2 - j));
            this.zzecv += (long) i3;
        } else {
            i3 = 0;
        }
        if (this.zzecv < this.zzect) {
            return i3;
        }
        int read = this.zzecu.read(bArr, i + i3, i2 - i3);
        int i4 = i3 + read;
        this.zzecv += (long) read;
        return i4;
    }

    public final long zza(zznq zznq) throws IOException {
        zznq zznq2;
        zznq zznq3;
        zznq zznq4 = zznq;
        this.uri = zznq4.uri;
        long j = zznq4.zzamw;
        long j2 = this.zzect;
        if (j >= j2) {
            zznq2 = null;
        } else {
            long j3 = zznq4.zzce;
            zznq2 = new zznq(zznq4.uri, j, j3 != -1 ? Math.min(j3, j2 - j) : j2 - j, (String) null);
        }
        long j4 = zznq4.zzce;
        if (j4 == -1 || zznq4.zzamw + j4 > this.zzect) {
            long max = Math.max(this.zzect, zznq4.zzamw);
            long j5 = zznq4.zzce;
            zznq3 = new zznq(zznq4.uri, max, j5 != -1 ? Math.min(j5, (zznq4.zzamw + j5) - this.zzect) : -1, (String) null);
        } else {
            zznq3 = null;
        }
        long j6 = 0;
        long zza = zznq2 != null ? this.zzecs.zza(zznq2) : 0;
        if (zznq3 != null) {
            j6 = this.zzecu.zza(zznq3);
        }
        this.zzecv = zznq4.zzamw;
        if (zza == -1 || j6 == -1) {
            return -1;
        }
        return zza + j6;
    }
}
