package com.google.android.gms.internal.ads;

import android.annotation.SuppressLint;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.internal.ads.zznx;
import java.io.IOException;

@SuppressLint({"HandlerLeak"})
final class zzny<T extends zznx> extends zzddu implements Runnable {
    private volatile boolean zzaek;
    private final T zzbfs;
    private final zznv<T> zzbft;
    public final int zzbfu;
    private final long zzbfv;
    private IOException zzbfw;
    private int zzbfx;
    private volatile Thread zzbfy;
    private final /* synthetic */ zznw zzbfz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzny(zznw zznw, Looper looper, T t, zznv<T> zznv, int i, long j) {
        super(looper);
        this.zzbfz = zznw;
        this.zzbfs = t;
        this.zzbft = zznv;
        this.zzbfu = i;
        this.zzbfv = j;
    }

    private final void execute() {
        this.zzbfw = null;
        this.zzbfz.zzbfp.execute(this.zzbfz.zzbfq);
    }

    private final void finish() {
        zzny unused = this.zzbfz.zzbfq = null;
    }

    public final void handleMessage(Message message) {
        int i;
        if (!this.zzaek) {
            int i2 = message.what;
            if (i2 == 0) {
                execute();
            } else if (i2 != 4) {
                finish();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                long j = elapsedRealtime - this.zzbfv;
                if (this.zzbfs.zzhp()) {
                    this.zzbft.zza(this.zzbfs, elapsedRealtime, j, false);
                    return;
                }
                int i3 = message.what;
                if (i3 == 1) {
                    this.zzbft.zza(this.zzbfs, elapsedRealtime, j, false);
                } else if (i3 == 2) {
                    this.zzbft.zza(this.zzbfs, elapsedRealtime, j);
                } else if (i3 == 3) {
                    this.zzbfw = (IOException) message.obj;
                    int zza = this.zzbft.zza(this.zzbfs, elapsedRealtime, j, this.zzbfw);
                    if (zza == 3) {
                        IOException unused = this.zzbfz.zzbfr = this.zzbfw;
                    } else if (zza != 2) {
                        if (zza == 1) {
                            i = 1;
                        } else {
                            i = this.zzbfx + 1;
                        }
                        this.zzbfx = i;
                        zzek((long) Math.min((this.zzbfx - 1) * 1000, 5000));
                    }
                }
            } else {
                throw ((Error) message.obj);
            }
        }
    }

    public final void run() {
        try {
            this.zzbfy = Thread.currentThread();
            if (!this.zzbfs.zzhp()) {
                String valueOf = String.valueOf(this.zzbfs.getClass().getSimpleName());
                zzon.beginSection(valueOf.length() != 0 ? "load:".concat(valueOf) : new String("load:"));
                this.zzbfs.zzhq();
                zzon.endSection();
            }
            if (!this.zzaek) {
                sendEmptyMessage(2);
            }
        } catch (IOException e) {
            if (!this.zzaek) {
                obtainMessage(3, e).sendToTarget();
            }
        } catch (InterruptedException unused) {
            zzoc.checkState(this.zzbfs.zzhp());
            if (!this.zzaek) {
                sendEmptyMessage(2);
            }
        } catch (Exception e2) {
            Log.e("LoadTask", "Unexpected exception loading stream", e2);
            if (!this.zzaek) {
                obtainMessage(3, new zzoa(e2)).sendToTarget();
            }
        } catch (OutOfMemoryError e3) {
            Log.e("LoadTask", "OutOfMemory error loading stream", e3);
            if (!this.zzaek) {
                obtainMessage(3, new zzoa(e3)).sendToTarget();
            }
        } catch (Error e4) {
            Log.e("LoadTask", "Unexpected error loading stream", e4);
            if (!this.zzaek) {
                obtainMessage(4, e4).sendToTarget();
            }
            throw e4;
        } catch (Throwable th) {
            zzon.endSection();
            throw th;
        }
    }

    public final void zzbc(int i) throws IOException {
        IOException iOException = this.zzbfw;
        if (iOException != null && this.zzbfx > i) {
            throw iOException;
        }
    }

    public final void zzek(long j) {
        zzoc.checkState(this.zzbfz.zzbfq == null);
        zzny unused = this.zzbfz.zzbfq = this;
        if (j > 0) {
            sendEmptyMessageDelayed(0, j);
        } else {
            execute();
        }
    }

    public final void zzl(boolean z) {
        this.zzaek = z;
        this.zzbfw = null;
        if (hasMessages(0)) {
            removeMessages(0);
            if (!z) {
                sendEmptyMessage(1);
            }
        } else {
            this.zzbfs.cancelLoad();
            if (this.zzbfy != null) {
                this.zzbfy.interrupt();
            }
        }
        if (z) {
            finish();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.zzbft.zza(this.zzbfs, elapsedRealtime, elapsedRealtime - this.zzbfv, true);
        }
    }
}
