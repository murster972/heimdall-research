package com.google.android.gms.internal.ads;

final /* synthetic */ class zzauf implements zzaui {
    static final zzaui zzdpr = new zzauf();

    private zzauf() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return zzbfq.getAppInstanceId();
    }
}
