package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

public interface zzava {
    zzdhe<String> zza(String str, PackageInfo packageInfo);

    zzdhe<AdvertisingIdClient.Info> zzak(Context context);
}
