package com.google.android.gms.internal.ads;

import java.util.Map;

final class zzafk implements zzafn<zzbdi> {
    zzafk() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        ((zzbdi) obj).zzay(!Boolean.parseBoolean((String) map.get("disabled")));
    }
}
