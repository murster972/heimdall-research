package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzbjg implements Runnable {
    private final zzbdi zzehp;
    private final JSONObject zzfcs;

    zzbjg(zzbdi zzbdi, JSONObject jSONObject) {
        this.zzehp = zzbdi;
        this.zzfcs = jSONObject;
    }

    public final void run() {
        this.zzehp.zza("AFMA_updateActiveView", this.zzfcs);
    }
}
