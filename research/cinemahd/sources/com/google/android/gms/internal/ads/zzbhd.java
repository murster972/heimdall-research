package com.google.android.gms.internal.ads;

final class zzbhd implements zzblf {
    private zzbod zzelr;
    private zzczt zzelt;
    private zzbvi zzelu;
    private final /* synthetic */ zzbgr zzerr;
    private zzbrm zzers;
    private zzcxw zzert;
    private zzcns zzexc;
    private zzbma zzexd;
    private zzbkf zzexe;

    private zzbhd(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    public final /* synthetic */ zzblf zza(zzbma zzbma) {
        this.zzexd = (zzbma) zzdxm.checkNotNull(zzbma);
        return this;
    }

    /* renamed from: zzaee */
    public final zzblg zzadg() {
        zzdxm.zza(this.zzers, zzbrm.class);
        zzdxm.zza(this.zzelr, zzbod.class);
        zzdxm.zza(this.zzexc, zzcns.class);
        zzdxm.zza(this.zzexd, zzbma.class);
        zzdxm.zza(this.zzexe, zzbkf.class);
        zzdxm.zza(this.zzelu, zzbvi.class);
        return new zzbhc(this.zzerr, this.zzexe, this.zzelu, new zzbnb(), new zzdai(), new zzbny(), new zzcee(), this.zzers, this.zzelr, new zzdaq(), this.zzexc, this.zzexd, this.zzelt, this.zzert);
    }

    public final /* synthetic */ zzblf zzb(zzbvi zzbvi) {
        this.zzelu = (zzbvi) zzdxm.checkNotNull(zzbvi);
        return this;
    }

    public final /* synthetic */ zzblf zzc(zzbod zzbod) {
        this.zzelr = (zzbod) zzdxm.checkNotNull(zzbod);
        return this;
    }

    public final /* synthetic */ zzblf zza(zzcns zzcns) {
        this.zzexc = (zzcns) zzdxm.checkNotNull(zzcns);
        return this;
    }

    public final /* synthetic */ zzblf zzb(zzbkf zzbkf) {
        this.zzexe = (zzbkf) zzdxm.checkNotNull(zzbkf);
        return this;
    }

    public final /* synthetic */ zzblf zzc(zzbrm zzbrm) {
        this.zzers = (zzbrm) zzdxm.checkNotNull(zzbrm);
        return this;
    }

    public final /* synthetic */ zzboe zza(zzcxw zzcxw) {
        this.zzert = zzcxw;
        return this;
    }

    public final /* synthetic */ zzboe zza(zzczt zzczt) {
        this.zzelt = zzczt;
        return this;
    }
}
