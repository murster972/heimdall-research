package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

public final class zzdhg {
    public static zzdhd zza(ExecutorService executorService) {
        if (executorService instanceof zzdhd) {
            return (zzdhd) executorService;
        }
        if (executorService instanceof ScheduledExecutorService) {
            return new zzdhk((ScheduledExecutorService) executorService);
        }
        return new zzdhh(executorService);
    }

    public static Executor zzarw() {
        return zzdgl.INSTANCE;
    }

    static Executor zza(Executor executor, zzdfs<?> zzdfs) {
        zzdei.checkNotNull(executor);
        zzdei.checkNotNull(zzdfs);
        if (executor == zzdgl.INSTANCE) {
            return executor;
        }
        return new zzdhf(executor, zzdfs);
    }
}
