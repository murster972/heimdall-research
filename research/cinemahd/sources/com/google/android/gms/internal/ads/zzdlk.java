package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdlk extends zzdrt<zzdlk, zza> implements zzdtg {
    private static volatile zzdtn<zzdlk> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlk zzhar;
    private int zzhaf;
    private zzdln zzhap;

    public static final class zza extends zzdrt.zzb<zzdlk, zza> implements zzdtg {
        private zza() {
            super(zzdlk.zzhar);
        }

        /* synthetic */ zza(zzdll zzdll) {
            this();
        }
    }

    static {
        zzdlk zzdlk = new zzdlk();
        zzhar = zzdlk;
        zzdrt.zza(zzdlk.class, zzdlk);
    }

    private zzdlk() {
    }

    public static zzdlk zzatl() {
        return zzhar;
    }

    public static zzdlk zzz(zzdqk zzdqk) throws zzdse {
        return (zzdlk) zzdrt.zza(zzhar, zzdqk);
    }

    public final int getKeySize() {
        return this.zzhaf;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdll.zzdk[i - 1]) {
            case 1:
                return new zzdlk();
            case 2:
                return new zza((zzdll) null);
            case 3:
                return zzdrt.zza((zzdte) zzhar, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u000b", new Object[]{"zzhap", "zzhaf"});
            case 4:
                return zzhar;
            case 5:
                zzdtn<zzdlk> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlk.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhar);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdln zzath() {
        zzdln zzdln = this.zzhap;
        return zzdln == null ? zzdln.zzato() : zzdln;
    }
}
