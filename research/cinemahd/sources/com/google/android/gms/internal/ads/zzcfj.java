package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Callable;

final /* synthetic */ class zzcfj implements Callable {
    private final Context zzcey;
    private final zzdq zzfum;

    zzcfj(zzdq zzdq, Context context) {
        this.zzfum = zzdq;
        this.zzcey = context;
    }

    public final Object call() {
        zzdq zzdq = this.zzfum;
        return zzdq.zzbw().zzb(this.zzcey);
    }
}
