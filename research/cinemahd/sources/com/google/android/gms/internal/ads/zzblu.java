package com.google.android.gms.internal.ads;

public final class zzblu implements zzdxg<zzaea> {
    private final zzbls zzffa;

    public zzblu(zzbls zzbls) {
        this.zzffa = zzbls;
    }

    public final /* synthetic */ Object get() {
        return (zzaea) zzdxm.zza(this.zzffa.zzagm(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
