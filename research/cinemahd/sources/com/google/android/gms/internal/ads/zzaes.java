package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.PublisherAdView;

final class zzaes implements Runnable {
    private final /* synthetic */ PublisherAdView zzcwn;
    private final /* synthetic */ zzvu zzcwo;
    private final /* synthetic */ zzaep zzcwp;

    zzaes(zzaep zzaep, PublisherAdView publisherAdView, zzvu zzvu) {
        this.zzcwp = zzaep;
        this.zzcwn = publisherAdView;
        this.zzcwo = zzvu;
    }

    public final void run() {
        if (this.zzcwn.zza(this.zzcwo)) {
            this.zzcwp.zzcwk.onPublisherAdViewLoaded(this.zzcwn);
        } else {
            zzayu.zzez("Could not bind.");
        }
    }
}
