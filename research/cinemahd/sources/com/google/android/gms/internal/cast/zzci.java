package com.google.android.gms.internal.cast;

import android.app.PendingIntent;
import com.google.android.gms.common.api.Status;
import org.json.JSONObject;

final class zzci implements zzec {
    private final /* synthetic */ zzcb zzwo;
    private final /* synthetic */ zzch zzwp;

    zzci(zzch zzch, zzcb zzcb) {
        this.zzwp = zzch;
        this.zzwo = zzcb;
    }

    public final void zza(long j, int i, Object obj) {
        if (obj == null) {
            try {
                this.zzwp.setResult(new zzcn(new Status(i, (String) null, (PendingIntent) null), (String) null, j, (JSONObject) null));
            } catch (ClassCastException unused) {
                this.zzwp.setResult(zzch.zzb(new Status(13)));
            }
        } else {
            zzcp zzcp = (zzcp) obj;
            String str = zzcp.zzxh;
            if (i == 0 && str != null) {
                String unused2 = this.zzwp.zzwi.zzwh = str;
            }
            this.zzwp.setResult(new zzcn(new Status(i, zzcp.zzxa, (PendingIntent) null), str, zzcp.zzxi, zzcp.zzxb));
        }
    }

    public final void zzd(long j) {
        this.zzwp.setResult(zzch.zzb(new Status(2103)));
    }
}
