package com.google.android.gms.internal.ads;

public final class zzbli implements zzdxg<zzcio<zzbkk>> {
    private final zzdxp<zzcix> zzewq;
    private final zzdxp<zzcjr> zzewr;
    private final zzdxp<zzczu> zzfep;

    public zzbli(zzdxp<zzczu> zzdxp, zzdxp<zzcjr> zzdxp2, zzdxp<zzcix> zzdxp3) {
        this.zzfep = zzdxp;
        this.zzewr = zzdxp2;
        this.zzewq = zzdxp3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x002e, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.ads.zzve.zzoy().zzd(com.google.android.gms.internal.ads.zzzn.zzcjp)).booleanValue() != false) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object get() {
        /*
            r4 = this;
            com.google.android.gms.internal.ads.zzdxp<com.google.android.gms.internal.ads.zzczu> r0 = r4.zzfep
            java.lang.Object r0 = r0.get()
            com.google.android.gms.internal.ads.zzczu r0 = (com.google.android.gms.internal.ads.zzczu) r0
            com.google.android.gms.internal.ads.zzdxp<com.google.android.gms.internal.ads.zzcjr> r1 = r4.zzewr
            java.lang.Object r1 = r1.get()
            com.google.android.gms.internal.ads.zzcjr r1 = (com.google.android.gms.internal.ads.zzcjr) r1
            com.google.android.gms.internal.ads.zzdxp<com.google.android.gms.internal.ads.zzcix> r2 = r4.zzewq
            java.lang.Object r2 = r2.get()
            com.google.android.gms.internal.ads.zzcix r2 = (com.google.android.gms.internal.ads.zzcix) r2
            com.google.android.gms.internal.ads.zzaea r0 = r0.zzaoo()
            if (r0 == 0) goto L_0x0031
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcjp
            com.google.android.gms.internal.ads.zzzj r3 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r3.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0031
            goto L_0x0032
        L_0x0031:
            r1 = r2
        L_0x0032:
            java.lang.String r0 = "Cannot return null from a non-@Nullable @Provides method"
            java.lang.Object r0 = com.google.android.gms.internal.ads.zzdxm.zza(r1, (java.lang.String) r0)
            com.google.android.gms.internal.ads.zzcio r0 = (com.google.android.gms.internal.ads.zzcio) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbli.get():java.lang.Object");
    }
}
