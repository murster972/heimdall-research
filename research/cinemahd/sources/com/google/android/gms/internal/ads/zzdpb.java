package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.Provider;

public interface zzdpb<T> {
    T zza(String str, Provider provider) throws GeneralSecurityException;
}
