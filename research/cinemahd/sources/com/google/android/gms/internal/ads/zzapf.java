package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class zzapf extends zzgc implements zzapd {
    zzapf(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.purchase.client.IInAppPurchaseManager");
    }
}
