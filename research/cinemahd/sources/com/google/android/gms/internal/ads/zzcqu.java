package com.google.android.gms.internal.ads;

import android.os.Bundle;

final /* synthetic */ class zzcqu implements zzcty {
    private final Bundle zzdpq;
    private final zzcqv zzgfa;

    zzcqu(zzcqv zzcqv, Bundle bundle) {
        this.zzgfa = zzcqv;
        this.zzdpq = bundle;
    }

    public final void zzr(Object obj) {
        this.zzgfa.zzb(this.zzdpq, (Bundle) obj);
    }
}
