package com.google.android.gms.internal.cast;

import android.os.RemoteException;
import com.google.android.gms.cast.CastRemoteDisplay$CastRemoteDisplaySessionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl;

class zzem extends BaseImplementation$ApiMethodImpl<CastRemoteDisplay$CastRemoteDisplaySessionResult, zzer> {
    final /* synthetic */ zzeh zzaaz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzem(zzeh zzeh, GoogleApiClient googleApiClient) {
        super((Api<?>) zzeh.zzaax, googleApiClient);
        this.zzaaz = zzeh;
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzep(status);
    }

    /* renamed from: zza */
    public void doExecute(zzer zzer) throws RemoteException {
    }
}
