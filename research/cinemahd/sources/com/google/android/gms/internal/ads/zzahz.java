package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final /* synthetic */ class zzahz {
    public static void zza(zzaia zzaia, String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        zzaia.zzj(str, jSONObject.toString());
    }

    public static void zzb(zzaia zzaia, String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("(window.AFMA_ReceiveMessage || function() {})('");
        sb.append(str);
        sb.append("'");
        sb.append(",");
        sb.append(jSONObject2);
        sb.append(");");
        String valueOf = String.valueOf(sb.toString());
        zzayu.zzea(valueOf.length() != 0 ? "Dispatching AFMA event: ".concat(valueOf) : new String("Dispatching AFMA event: "));
        zzaia.zzcy(sb.toString());
    }

    public static void zza(zzaia zzaia, String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("(");
        sb.append(str2);
        sb.append(");");
        zzaia.zzcy(sb.toString());
    }

    public static void zza(zzaia zzaia, String str, Map map) {
        try {
            zzaia.zzb(str, zzq.zzkq().zzi((Map<String, ?>) map));
        } catch (JSONException unused) {
            zzayu.zzez("Could not convert parameters to JSON.");
        }
    }
}
