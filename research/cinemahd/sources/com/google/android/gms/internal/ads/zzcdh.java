package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

public final class zzcdh {
    public final zzso.zza.C0047zza zzfsj;
    public final zzso.zza.C0047zza zzfsk;
    public final zzso.zza.C0047zza zzfsl;

    public zzcdh(zzso.zza.C0047zza zza, zzso.zza.C0047zza zza2, zzso.zza.C0047zza zza3) {
        this.zzfsj = zza;
        this.zzfsl = zza3;
        this.zzfsk = zza2;
    }
}
