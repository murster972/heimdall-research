package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdjm extends zzdii<zzdlv> {
    zzdjm() {
        super(zzdlv.class, new zzdjl(zzdhx.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesGcmKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    public final zzdih<zzdlw, zzdlv> zzasg() {
        return new zzdjo(this, zzdlw.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdlv zzdlv = (zzdlv) zzdte;
        zzdpo.zzx(zzdlv.getVersion(), 0);
        zzdpo.zzez(zzdlv.zzass().size());
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdlv.zzae(zzdqk);
    }
}
