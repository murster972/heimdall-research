package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import java.util.Set;

public final class zzbra extends zzbrl<AppEventListener> implements zzaey {
    public zzbra(Set<zzbsu<AppEventListener>> set) {
        super(set);
    }

    public final synchronized void onAppEvent(String str, String str2) {
        zza(new zzbrd(str, str2));
    }
}
