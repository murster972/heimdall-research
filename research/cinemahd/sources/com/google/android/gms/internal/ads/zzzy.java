package com.google.android.gms.internal.ads;

public abstract class zzzy {
    public static final zzzy zzcrr = new zzzx();
    public static final zzzy zzcrs = new zzaaa();
    public static final zzzy zzcrt = new zzzz();

    public abstract String zzg(String str, String str2);
}
