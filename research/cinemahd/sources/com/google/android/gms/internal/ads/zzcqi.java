package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Set;

final class zzcqi implements zzcub<zzcty<Bundle>> {
    private final Set<String> zzgeu;

    zzcqi(Set<String> set) {
        this.zzgeu = set;
    }

    public final zzdhe<zzcty<Bundle>> zzanc() {
        ArrayList arrayList = new ArrayList();
        for (String add : this.zzgeu) {
            arrayList.add(add);
        }
        return zzdgs.zzaj(new zzcql(arrayList));
    }
}
