package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdvx {

    public static final class zza extends zzdrt<zza, zzb> implements zzdtg {
        private static volatile zzdtn<zza> zzdz;
        /* access modifiers changed from: private */
        public static final zza zzhum;
        private int zzdl;
        private int zzhuf;
        private C0040zza zzhug;
        private zzdqk zzhuh;
        private zzdqk zzhui;
        private boolean zzhuj;
        private boolean zzhuk;
        private byte zzhul = 2;

        /* renamed from: com.google.android.gms.internal.ads.zzdvx$zza$zza  reason: collision with other inner class name */
        public static final class C0040zza extends zzdrt<C0040zza, C0041zza> implements zzdtg {
            private static volatile zzdtn<C0040zza> zzdz;
            /* access modifiers changed from: private */
            public static final C0040zza zzhur;
            private int zzdl;
            private String zzhun = "";
            private String zzhuo = "";
            private String zzhup = "";
            private int zzhuq;

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zza$zza$zza  reason: collision with other inner class name */
            public static final class C0041zza extends zzdrt.zzb<C0040zza, C0041zza> implements zzdtg {
                private C0041zza() {
                    super(C0040zza.zzhur);
                }

                /* synthetic */ C0041zza(zzdvz zzdvz) {
                    this();
                }
            }

            static {
                C0040zza zza = new C0040zza();
                zzhur = zza;
                zzdrt.zza(C0040zza.class, zza);
            }

            private C0040zza() {
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new C0040zza();
                    case 2:
                        return new C0041zza((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhur, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\b\u0002\u0004\u0004\u0003", new Object[]{"zzdl", "zzhun", "zzhuo", "zzhup", "zzhuq"});
                    case 4:
                        return zzhur;
                    case 5:
                        zzdtn<C0040zza> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (C0040zza.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhur);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public static final class zzb extends zzdrt.zzb<zza, zzb> implements zzdtg {
            private zzb() {
                super(zza.zzhum);
            }

            /* synthetic */ zzb(zzdvz zzdvz) {
                this();
            }
        }

        public enum zzc implements zzdry {
            SAFE(0),
            DANGEROUS(1),
            UNKNOWN(2),
            POTENTIALLY_UNWANTED(3),
            DANGEROUS_HOST(4);
            
            private static final zzdrx<zzc> zzen = null;
            private final int value;

            static {
                zzen = new zzdwb();
            }

            private zzc(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zzdwa.zzew;
            }

            public static zzc zzhd(int i) {
                if (i == 0) {
                    return SAFE;
                }
                if (i == 1) {
                    return DANGEROUS;
                }
                if (i == 2) {
                    return UNKNOWN;
                }
                if (i == 3) {
                    return POTENTIALLY_UNWANTED;
                }
                if (i != 4) {
                    return null;
                }
                return DANGEROUS_HOST;
            }

            public final String toString() {
                return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        static {
            zza zza = new zza();
            zzhum = zza;
            zzdrt.zza(zza.class, zza);
        }

        private zza() {
            zzdqk zzdqk = zzdqk.zzhhx;
            this.zzhuh = zzdqk;
            this.zzhui = zzdqk;
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            int i2 = 0;
            switch (zzdvz.zzdk[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new zzb((zzdvz) null);
                case 3:
                    return zzdrt.zza((zzdte) zzhum, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0001\u0001Ԍ\u0000\u0002\t\u0001\u0003\n\u0002\u0004\n\u0003\u0005\u0007\u0004\u0006\u0007\u0005", new Object[]{"zzdl", "zzhuf", zzc.zzaf(), "zzhug", "zzhuh", "zzhui", "zzhuj", "zzhuk"});
                case 4:
                    return zzhum;
                case 5:
                    zzdtn<zza> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zza.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzhum);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return Byte.valueOf(this.zzhul);
                case 7:
                    if (obj != null) {
                        i2 = 1;
                    }
                    this.zzhul = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    public static final class zzb extends zzdrt<zzb, zza> implements zzdtg {
        private static volatile zzdtn<zzb> zzdz;
        /* access modifiers changed from: private */
        public static final zzb zzhvn;
        private int zzbut;
        private int zzdl;
        private zzdqk zzhuh = zzdqk.zzhhx;
        private byte zzhul = 2;
        private String zzhuo = "";
        private int zzhuy;
        private String zzhuz = "";
        private String zzhva = "";
        private C0042zzb zzhvb;
        private zzdsb<zzh> zzhvc = zzdrt.zzazw();
        private String zzhvd = "";
        private zzf zzhve;
        private boolean zzhvf;
        private zzdsb<String> zzhvg = zzdrt.zzazw();
        private String zzhvh = "";
        private boolean zzhvi;
        private boolean zzhvj;
        private zzi zzhvk;
        private zzdsb<String> zzhvl = zzdrt.zzazw();
        private zzdsb<String> zzhvm = zzdrt.zzazw();

        public static final class zza extends zzdrt.zzb<zzb, zza> implements zzdtg {
            private zza() {
                super(zzb.zzhvn);
            }

            /* synthetic */ zza(zzdvz zzdvz) {
                this();
            }
        }

        /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzb  reason: collision with other inner class name */
        public static final class C0042zzb extends zzdrt<C0042zzb, zza> implements zzdtg {
            private static volatile zzdtn<C0042zzb> zzdz;
            /* access modifiers changed from: private */
            public static final C0042zzb zzhvp;
            private int zzdl;
            private String zzhvo = "";

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzb$zza */
            public static final class zza extends zzdrt.zzb<C0042zzb, zza> implements zzdtg {
                private zza() {
                    super(C0042zzb.zzhvp);
                }

                public final zza zzhl(String str) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((C0042zzb) this.zzhmp).zzhm(str);
                    return this;
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            static {
                C0042zzb zzb = new C0042zzb();
                zzhvp = zzb;
                zzdrt.zza(C0042zzb.class, zzb);
            }

            private C0042zzb() {
            }

            public static zza zzbcw() {
                return (zza) zzhvp.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zzhm(String str) {
                str.getClass();
                this.zzdl |= 1;
                this.zzhvo = str;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new C0042zzb();
                    case 2:
                        return new zza((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhvp, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001\b\u0000", new Object[]{"zzdl", "zzhvo"});
                    case 4:
                        return zzhvp;
                    case 5:
                        zzdtn<C0042zzb> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (C0042zzb.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhvp);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public static final class zzc extends zzdrt<zzc, zza> implements zzdtg {
            private static volatile zzdtn<zzc> zzdz;
            /* access modifiers changed from: private */
            public static final zzc zzhvr;
            private int zzdl;
            private zzdqk zzhct;
            private byte zzhul = 2;
            private zzdqk zzhvq;

            public static final class zza extends zzdrt.zzb<zzc, zza> implements zzdtg {
                private zza() {
                    super(zzc.zzhvr);
                }

                public final zza zzbk(zzdqk zzdqk) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzc) this.zzhmp).zzbm(zzdqk);
                    return this;
                }

                public final zza zzbl(zzdqk zzdqk) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzc) this.zzhmp).zzav(zzdqk);
                    return this;
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            static {
                zzc zzc = new zzc();
                zzhvr = zzc;
                zzdrt.zza(zzc.class, zzc);
            }

            private zzc() {
                zzdqk zzdqk = zzdqk.zzhhx;
                this.zzhvq = zzdqk;
                this.zzhct = zzdqk;
            }

            /* access modifiers changed from: private */
            public final void zzav(zzdqk zzdqk) {
                zzdqk.getClass();
                this.zzdl |= 2;
                this.zzhct = zzdqk;
            }

            public static zza zzbcy() {
                return (zza) zzhvr.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zzbm(zzdqk zzdqk) {
                zzdqk.getClass();
                this.zzdl |= 1;
                this.zzhvq = zzdqk;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzc();
                    case 2:
                        return new zza((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhvr, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0001\u0001Ԋ\u0000\u0002\n\u0001", new Object[]{"zzdl", "zzhvq", "zzhct"});
                    case 4:
                        return zzhvr;
                    case 5:
                        zzdtn<zzc> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzc.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhvr);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public static final class zzd extends zzdrt<zzd, zza> implements zzdtg {
            private static volatile zzdtn<zzd> zzdz;
            /* access modifiers changed from: private */
            public static final zzd zzhvx;
            private int zzdl;
            private byte zzhul = 2;
            private C0043zzb zzhvs;
            private zzdsb<zzc> zzhvt = zzdrt.zzazw();
            private zzdqk zzhvu;
            private zzdqk zzhvv;
            private int zzhvw;

            public static final class zza extends zzdrt.zzb<zzd, zza> implements zzdtg {
                private zza() {
                    super(zzd.zzhvx);
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzd$zzb  reason: collision with other inner class name */
            public static final class C0043zzb extends zzdrt<C0043zzb, zza> implements zzdtg {
                private static volatile zzdtn<C0043zzb> zzdz;
                /* access modifiers changed from: private */
                public static final C0043zzb zzhwb;
                private int zzdl;
                private zzdqk zzhvy;
                private zzdqk zzhvz;
                private zzdqk zzhwa;

                /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzd$zzb$zza */
                public static final class zza extends zzdrt.zzb<C0043zzb, zza> implements zzdtg {
                    private zza() {
                        super(C0043zzb.zzhwb);
                    }

                    /* synthetic */ zza(zzdvz zzdvz) {
                        this();
                    }
                }

                static {
                    C0043zzb zzb = new C0043zzb();
                    zzhwb = zzb;
                    zzdrt.zza(C0043zzb.class, zzb);
                }

                private C0043zzb() {
                    zzdqk zzdqk = zzdqk.zzhhx;
                    this.zzhvy = zzdqk;
                    this.zzhvz = zzdqk;
                    this.zzhwa = zzdqk;
                }

                /* access modifiers changed from: protected */
                public final Object zza(int i, Object obj, Object obj2) {
                    switch (zzdvz.zzdk[i - 1]) {
                        case 1:
                            return new C0043zzb();
                        case 2:
                            return new zza((zzdvz) null);
                        case 3:
                            return zzdrt.zza((zzdte) zzhwb, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0000\u0002\n\u0001\u0003\n\u0002", new Object[]{"zzdl", "zzhvy", "zzhvz", "zzhwa"});
                        case 4:
                            return zzhwb;
                        case 5:
                            zzdtn<C0043zzb> zzdtn = zzdz;
                            if (zzdtn == null) {
                                synchronized (C0043zzb.class) {
                                    zzdtn = zzdz;
                                    if (zzdtn == null) {
                                        zzdtn = new zzdrt.zza<>(zzhwb);
                                        zzdz = zzdtn;
                                    }
                                }
                            }
                            return zzdtn;
                        case 6:
                            return (byte) 1;
                        case 7:
                            return null;
                        default:
                            throw new UnsupportedOperationException();
                    }
                }
            }

            static {
                zzd zzd = new zzd();
                zzhvx = zzd;
                zzdrt.zza(zzd.class, zzd);
            }

            private zzd() {
                zzdqk zzdqk = zzdqk.zzhhx;
                this.zzhvu = zzdqk;
                this.zzhvv = zzdqk;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzd();
                    case 2:
                        return new zza((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhvx, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0001\u0001\t\u0000\u0002Л\u0003\n\u0001\u0004\n\u0002\u0005\u0004\u0003", new Object[]{"zzdl", "zzhvs", "zzhvt", zzc.class, "zzhvu", "zzhvv", "zzhvw"});
                    case 4:
                        return zzhvx;
                    case 5:
                        zzdtn<zzd> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzd.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhvx);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public static final class zze extends zzdrt<zze, zza> implements zzdtg {
            private static volatile zzdtn<zze> zzdz;
            /* access modifiers changed from: private */
            public static final zze zzhwe;
            private int zzdl;
            private byte zzhul = 2;
            private zzdsb<zzc> zzhvt = zzdrt.zzazw();
            private zzdqk zzhvu;
            private zzdqk zzhvv;
            private int zzhvw;
            private C0044zzb zzhwc;
            private zzdqk zzhwd;

            public static final class zza extends zzdrt.zzb<zze, zza> implements zzdtg {
                private zza() {
                    super(zze.zzhwe);
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zze$zzb  reason: collision with other inner class name */
            public static final class C0044zzb extends zzdrt<C0044zzb, zza> implements zzdtg {
                private static volatile zzdtn<C0044zzb> zzdz;
                /* access modifiers changed from: private */
                public static final C0044zzb zzhwh;
                private int zzdl;
                private zzdqk zzhwa;
                private int zzhwf;
                private zzdqk zzhwg;

                /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zze$zzb$zza */
                public static final class zza extends zzdrt.zzb<C0044zzb, zza> implements zzdtg {
                    private zza() {
                        super(C0044zzb.zzhwh);
                    }

                    /* synthetic */ zza(zzdvz zzdvz) {
                        this();
                    }
                }

                static {
                    C0044zzb zzb = new C0044zzb();
                    zzhwh = zzb;
                    zzdrt.zza(C0044zzb.class, zzb);
                }

                private C0044zzb() {
                    zzdqk zzdqk = zzdqk.zzhhx;
                    this.zzhwg = zzdqk;
                    this.zzhwa = zzdqk;
                }

                /* access modifiers changed from: protected */
                public final Object zza(int i, Object obj, Object obj2) {
                    switch (zzdvz.zzdk[i - 1]) {
                        case 1:
                            return new C0044zzb();
                        case 2:
                            return new zza((zzdvz) null);
                        case 3:
                            return zzdrt.zza((zzdte) zzhwh, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0004\u0000\u0002\n\u0001\u0003\n\u0002", new Object[]{"zzdl", "zzhwf", "zzhwg", "zzhwa"});
                        case 4:
                            return zzhwh;
                        case 5:
                            zzdtn<C0044zzb> zzdtn = zzdz;
                            if (zzdtn == null) {
                                synchronized (C0044zzb.class) {
                                    zzdtn = zzdz;
                                    if (zzdtn == null) {
                                        zzdtn = new zzdrt.zza<>(zzhwh);
                                        zzdz = zzdtn;
                                    }
                                }
                            }
                            return zzdtn;
                        case 6:
                            return (byte) 1;
                        case 7:
                            return null;
                        default:
                            throw new UnsupportedOperationException();
                    }
                }
            }

            static {
                zze zze = new zze();
                zzhwe = zze;
                zzdrt.zza(zze.class, zze);
            }

            private zze() {
                zzdqk zzdqk = zzdqk.zzhhx;
                this.zzhvu = zzdqk;
                this.zzhvv = zzdqk;
                this.zzhwd = zzdqk;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zze();
                    case 2:
                        return new zza((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhwe, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0001\u0001\t\u0000\u0002Л\u0003\n\u0001\u0004\n\u0002\u0005\u0004\u0003\u0006\n\u0004", new Object[]{"zzdl", "zzhwc", "zzhvt", zzc.class, "zzhvu", "zzhvv", "zzhvw", "zzhwd"});
                    case 4:
                        return zzhwe;
                    case 5:
                        zzdtn<zze> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zze.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhwe);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public static final class zzf extends zzdrt<zzf, zza> implements zzdtg {
            private static volatile zzdtn<zzf> zzdz;
            /* access modifiers changed from: private */
            public static final zzf zzhwk;
            private int zzbut;
            private int zzdl;
            private String zzhwi = "";
            private zzdqk zzhwj = zzdqk.zzhhx;

            public static final class zza extends zzdrt.zzb<zzf, zza> implements zzdtg {
                private zza() {
                    super(zzf.zzhwk);
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzf$zzb  reason: collision with other inner class name */
            public enum C0045zzb implements zzdry {
                TYPE_UNKNOWN(0),
                TYPE_CREATIVE(1);
                
                private static final zzdrx<C0045zzb> zzen = null;
                private final int value;

                static {
                    zzen = new zzdwc();
                }

                private C0045zzb(int i) {
                    this.value = i;
                }

                public static zzdsa zzaf() {
                    return zzdwd.zzew;
                }

                public static C0045zzb zzhe(int i) {
                    if (i == 0) {
                        return TYPE_UNKNOWN;
                    }
                    if (i != 1) {
                        return null;
                    }
                    return TYPE_CREATIVE;
                }

                public final String toString() {
                    return "<" + C0045zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
                }

                public final int zzae() {
                    return this.value;
                }
            }

            static {
                zzf zzf = new zzf();
                zzhwk = zzf;
                zzdrt.zza(zzf.class, zzf);
            }

            private zzf() {
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzf();
                    case 2:
                        return new zza((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhwk, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\f\u0000\u0002\b\u0001\u0003\n\u0002", new Object[]{"zzdl", "zzbut", C0045zzb.zzaf(), "zzhwi", "zzhwj"});
                    case 4:
                        return zzhwk;
                    case 5:
                        zzdtn<zzf> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzf.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhwk);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public enum zzg implements zzdry {
            UNKNOWN(0),
            URL_PHISHING(1),
            URL_MALWARE(2),
            URL_UNWANTED(3),
            CLIENT_SIDE_PHISHING_URL(4),
            CLIENT_SIDE_MALWARE_URL(5),
            DANGEROUS_DOWNLOAD_RECOVERY(6),
            DANGEROUS_DOWNLOAD_WARNING(7),
            OCTAGON_AD(8),
            OCTAGON_AD_SB_MATCH(9);
            
            private static final zzdrx<zzg> zzen = null;
            private final int value;

            static {
                zzen = new zzdwf();
            }

            private zzg(int i) {
                this.value = i;
            }

            public static zzdsa zzaf() {
                return zzdwe.zzew;
            }

            public static zzg zzhf(int i) {
                switch (i) {
                    case 0:
                        return UNKNOWN;
                    case 1:
                        return URL_PHISHING;
                    case 2:
                        return URL_MALWARE;
                    case 3:
                        return URL_UNWANTED;
                    case 4:
                        return CLIENT_SIDE_PHISHING_URL;
                    case 5:
                        return CLIENT_SIDE_MALWARE_URL;
                    case 6:
                        return DANGEROUS_DOWNLOAD_RECOVERY;
                    case 7:
                        return DANGEROUS_DOWNLOAD_WARNING;
                    case 8:
                        return OCTAGON_AD;
                    case 9:
                        return OCTAGON_AD_SB_MATCH;
                    default:
                        return null;
                }
            }

            public final String toString() {
                return "<" + zzg.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            public final int zzae() {
                return this.value;
            }
        }

        public static final class zzh extends zzdrt<zzh, C0046zzb> implements zzdtg {
            private static volatile zzdtn<zzh> zzdz;
            /* access modifiers changed from: private */
            public static final zzh zzhxm;
            private int zzdl;
            private byte zzhul = 2;
            private String zzhuo = "";
            private int zzhxe;
            private zzd zzhxf;
            private zze zzhxg;
            private int zzhxh;
            private zzdrz zzhxi = zzdrt.zzazv();
            private String zzhxj = "";
            private int zzhxk;
            private zzdsb<String> zzhxl = zzdrt.zzazw();

            public enum zza implements zzdry {
                AD_RESOURCE_UNKNOWN(0),
                AD_RESOURCE_CREATIVE(1),
                AD_RESOURCE_POST_CLICK(2),
                AD_RESOURCE_AUTO_CLICK_DESTINATION(3);
                
                private static final zzdrx<zza> zzen = null;
                private final int value;

                static {
                    zzen = new zzdwh();
                }

                private zza(int i) {
                    this.value = i;
                }

                public static zzdsa zzaf() {
                    return zzdwg.zzew;
                }

                public static zza zzhg(int i) {
                    if (i == 0) {
                        return AD_RESOURCE_UNKNOWN;
                    }
                    if (i == 1) {
                        return AD_RESOURCE_CREATIVE;
                    }
                    if (i == 2) {
                        return AD_RESOURCE_POST_CLICK;
                    }
                    if (i != 3) {
                        return null;
                    }
                    return AD_RESOURCE_AUTO_CLICK_DESTINATION;
                }

                public final String toString() {
                    return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
                }

                public final int zzae() {
                    return this.value;
                }
            }

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzh$zzb  reason: collision with other inner class name */
            public static final class C0046zzb extends zzdrt.zzb<zzh, C0046zzb> implements zzdtg {
                private C0046zzb() {
                    super(zzh.zzhxm);
                }

                /* synthetic */ C0046zzb(zzdvz zzdvz) {
                    this();
                }
            }

            static {
                zzh zzh = new zzh();
                zzhxm = zzh;
                zzdrt.zza(zzh.class, zzh);
            }

            private zzh() {
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzh();
                    case 2:
                        return new C0046zzb((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhxm, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0002\u0003\u0001Ԅ\u0000\u0002\b\u0001\u0003Љ\u0002\u0004Љ\u0003\u0005\u0004\u0004\u0006\u0016\u0007\b\u0005\b\f\u0006\t\u001a", new Object[]{"zzdl", "zzhxe", "zzhuo", "zzhxf", "zzhxg", "zzhxh", "zzhxi", "zzhxj", "zzhxk", zza.zzaf(), "zzhxl"});
                    case 4:
                        return zzhxm;
                    case 5:
                        zzdtn<zzh> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzh.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhxm);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        public static final class zzi extends zzdrt<zzi, zza> implements zzdtg {
            private static volatile zzdtn<zzi> zzdz;
            /* access modifiers changed from: private */
            public static final zzi zzhxq;
            private int zzdl;
            private String zzhxn = "";
            private long zzhxo;
            private boolean zzhxp;

            public static final class zza extends zzdrt.zzb<zzi, zza> implements zzdtg {
                private zza() {
                    super(zzi.zzhxq);
                }

                public final zza zzbs(boolean z) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzi) this.zzhmp).zzbr(z);
                    return this;
                }

                public final zza zzfu(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzi) this.zzhmp).zzft(j);
                    return this;
                }

                public final zza zzho(String str) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzi) this.zzhmp).zzhn(str);
                    return this;
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            static {
                zzi zzi = new zzi();
                zzhxq = zzi;
                zzdrt.zza(zzi.class, zzi);
            }

            private zzi() {
            }

            public static zza zzbdg() {
                return (zza) zzhxq.zzazt();
            }

            /* access modifiers changed from: private */
            public final void zzbr(boolean z) {
                this.zzdl |= 4;
                this.zzhxp = z;
            }

            /* access modifiers changed from: private */
            public final void zzft(long j) {
                this.zzdl |= 2;
                this.zzhxo = j;
            }

            /* access modifiers changed from: private */
            public final void zzhn(String str) {
                str.getClass();
                this.zzdl |= 1;
                this.zzhxn = str;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzi();
                    case 2:
                        return new zza((zzdvz) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzhxq, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001\u0003\u0007\u0002", new Object[]{"zzdl", "zzhxn", "zzhxo", "zzhxp"});
                    case 4:
                        return zzhxq;
                    case 5:
                        zzdtn<zzi> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzi.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhxq);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }
        }

        static {
            zzb zzb = new zzb();
            zzhvn = zzb;
            zzdrt.zza(zzb.class, zzb);
        }

        private zzb() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            int i2 = 0;
            switch (zzdvz.zzdk[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza((zzdvz) null);
                case 3:
                    return zzdrt.zza((zzdte) zzhvn, "\u0001\u0012\u0000\u0001\u0001\u0015\u0012\u0000\u0004\u0001\u0001\b\u0002\u0002\b\u0003\u0003\b\u0004\u0004Л\u0005\u0007\b\u0006\u001a\u0007\b\t\b\u0007\n\t\u0007\u000b\n\f\u0000\u000b\f\u0001\f\t\u0005\r\b\u0006\u000e\t\u0007\u000f\n\f\u0011\t\r\u0014\u001a\u0015\u001a", new Object[]{"zzdl", "zzhuo", "zzhuz", "zzhva", "zzhvc", zzh.class, "zzhvf", "zzhvg", "zzhvh", "zzhvi", "zzhvj", "zzbut", zzg.zzaf(), "zzhuy", zza.zzc.zzaf(), "zzhvb", "zzhvd", "zzhve", "zzhuh", "zzhvk", "zzhvl", "zzhvm"});
                case 4:
                    return zzhvn;
                case 5:
                    zzdtn<zzb> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzb.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzhvn);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return Byte.valueOf(this.zzhul);
                case 7:
                    if (obj != null) {
                        i2 = 1;
                    }
                    this.zzhul = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
