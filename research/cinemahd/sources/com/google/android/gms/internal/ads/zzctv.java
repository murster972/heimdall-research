package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzctv implements Callable {
    private final zzcts zzggw;

    zzctv(zzcts zzcts) {
        this.zzggw = zzcts;
    }

    public final Object call() {
        return this.zzggw.zzanp();
    }
}
