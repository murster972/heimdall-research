package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdkr extends zzdih<zzdlb, zzdky> {
    zzdkr(zzdkp zzdkp, Class cls) {
        super(cls);
    }

    public final /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdlb zzdlb = (zzdlb) zzdte;
        zzdkp.zza(zzdlb.zzast());
        zzdkp.zzea(zzdlb.getKeySize());
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdlb zzdlb = (zzdlb) zzdte;
        return (zzdky) zzdky.zzasu().zzec(0).zzu(zzdqk.zzu(zzdpn.zzey(zzdlb.getKeySize()))).zzd(zzdlb.zzast()).zzbaf();
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdlb.zzv(zzdqk);
    }
}
