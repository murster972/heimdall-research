package com.google.android.gms.internal.ads;

import java.io.Serializable;

public abstract class zzdej<T> implements Serializable {
    zzdej() {
    }

    public static <T> zzdej<T> zzab(T t) {
        if (t == null) {
            return zzddy.zzgtx;
        }
        return new zzdel(t);
    }

    public abstract T zzaqt();
}
