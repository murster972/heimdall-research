package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final /* synthetic */ class zzcud implements Runnable {
    private final zzcub zzgha;
    private final long zzghb;

    zzcud(zzcub zzcub, long j) {
        this.zzgha = zzcub;
        this.zzghb = j;
    }

    public final void run() {
        zzcub zzcub = this.zzgha;
        long j = this.zzghb;
        String canonicalName = zzcub.getClass().getCanonicalName();
        long a2 = zzq.zzkx().a() - j;
        StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 40);
        sb.append("Signal runtime : ");
        sb.append(canonicalName);
        sb.append(" = ");
        sb.append(a2);
        zzavs.zzed(sb.toString());
    }
}
