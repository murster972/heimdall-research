package com.google.android.gms.internal.ads;

final class zzbbb implements Runnable {
    private boolean zzbpe = false;
    private zzbai zzdyq;

    zzbbb(zzbai zzbai) {
        this.zzdyq = zzbai;
    }

    private final void zzyw() {
        zzawb.zzdsr.removeCallbacks(this);
        zzawb.zzdsr.postDelayed(this, 250);
    }

    public final void pause() {
        this.zzbpe = true;
        this.zzdyq.zzyb();
    }

    public final void resume() {
        this.zzbpe = false;
        zzyw();
    }

    public final void run() {
        if (!this.zzbpe) {
            this.zzdyq.zzyb();
            zzyw();
        }
    }
}
