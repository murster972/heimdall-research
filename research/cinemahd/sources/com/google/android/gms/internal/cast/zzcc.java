package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.games.GameManagerClient;
import java.io.IOException;
import org.json.JSONObject;

final class zzcc extends zzck {
    final /* synthetic */ zzcb zzwi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcc(zzcb zzcb, GameManagerClient gameManagerClient) {
        super(zzcb, gameManagerClient);
        this.zzwi = zzcb;
    }

    public final void execute() {
        try {
            this.zzwi.zzhz.a(this.zzwi.zzpb, this.zzwi.getNamespace(), (Cast.MessageReceivedCallback) new zzcd(this));
            this.zzwi.zzem();
            this.zzwi.zzel();
            this.zzwi.zza((String) null, 1100, (JSONObject) null, this.zzwq);
        } catch (IOException | IllegalStateException unused) {
            this.zzwq.zza(-1, 8, (Object) null);
        }
    }
}
