package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map;

final class zzdug extends zzdum {
    private final /* synthetic */ zzdub zzhqw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzdug(zzdub zzdub) {
        super(zzdub, (zzdue) null);
        this.zzhqw = zzdub;
    }

    public final Iterator<Map.Entry<K, V>> iterator() {
        return new zzdud(this.zzhqw, (zzdue) null);
    }

    /* synthetic */ zzdug(zzdub zzdub, zzdue zzdue) {
        this(zzdub);
    }
}
