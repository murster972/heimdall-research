package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcsv implements Callable {
    private final zzcss zzggd;

    zzcsv(zzcss zzcss) {
        this.zzggd = zzcss;
    }

    public final Object call() {
        return this.zzggd.zzank();
    }
}
