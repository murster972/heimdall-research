package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzgl extends Exception {
    private final int type;
    private final int zzacl;

    private zzgl(int i, String str, Throwable th, int i2) {
        super((String) null, th);
        this.type = i;
        this.zzacl = i2;
    }

    public static zzgl zza(Exception exc, int i) {
        return new zzgl(1, (String) null, exc, i);
    }

    public static zzgl zza(IOException iOException) {
        return new zzgl(0, (String) null, iOException, -1);
    }

    static zzgl zza(RuntimeException runtimeException) {
        return new zzgl(2, (String) null, runtimeException, -1);
    }
}
