package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzadv extends IInterface {
    void zza(zzaeg zzaeg) throws RemoteException;
}
