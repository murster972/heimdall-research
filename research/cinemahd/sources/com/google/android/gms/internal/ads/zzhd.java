package com.google.android.gms.internal.ads;

import java.io.IOException;

public class zzhd extends IOException {
    public zzhd() {
    }

    public zzhd(String str) {
        super(str);
    }

    public zzhd(String str, Throwable th) {
        super(str, th);
    }
}
