package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdmo extends zzdrt<zzdmo, zza> implements zzdtg {
    private static volatile zzdtn<zzdmo> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmo zzhby;
    private int zzhbv;
    private int zzhbw;
    private zzdqk zzhbx = zzdqk.zzhhx;

    public static final class zza extends zzdrt.zzb<zzdmo, zza> implements zzdtg {
        private zza() {
            super(zzdmo.zzhby);
        }

        /* synthetic */ zza(zzdmp zzdmp) {
            this();
        }
    }

    static {
        zzdmo zzdmo = new zzdmo();
        zzhby = zzdmo;
        zzdrt.zza(zzdmo.class, zzdmo);
    }

    private zzdmo() {
    }

    public static zzdmo zzaux() {
        return zzhby;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmp.zzdk[i - 1]) {
            case 1:
                return new zzdmo();
            case 2:
                return new zza((zzdmp) null);
            case 3:
                return zzdrt.zza((zzdte) zzhby, "\u0000\u0003\u0000\u0000\u0001\u000b\u0003\u0000\u0000\u0000\u0001\f\u0002\f\u000b\n", new Object[]{"zzhbv", "zzhbw", "zzhbx"});
            case 4:
                return zzhby;
            case 5:
                zzdtn<zzdmo> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmo.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhby);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdmr zzauu() {
        zzdmr zzel = zzdmr.zzel(this.zzhbv);
        return zzel == null ? zzdmr.UNRECOGNIZED : zzel;
    }

    public final zzdmt zzauv() {
        zzdmt zzem = zzdmt.zzem(this.zzhbw);
        return zzem == null ? zzdmt.UNRECOGNIZED : zzem;
    }

    public final zzdqk zzauw() {
        return this.zzhbx;
    }
}
