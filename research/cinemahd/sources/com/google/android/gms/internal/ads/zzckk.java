package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzckk implements zzcir<zzbtu, zzdac, zzcjy> {
    private final zzazb zzbli;
    private final Executor zzfci;
    private final zzbup zzfyt;
    private final Context zzup;

    public zzckk(Context context, zzazb zzazb, zzbup zzbup, Executor executor) {
        this.zzup = context;
        this.zzbli = zzazb;
        this.zzfyt = zzbup;
        this.zzfci = executor;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzdac, zzcjy> zzcip) throws zzdab {
        if (this.zzbli.zzdwa < 4100000) {
            ((zzdac) zzcip.zzddn).zza(this.zzup, zzczt.zzgmh.zzfgl.zzgml, zzczl.zzglr.toString(), (zzali) zzcip.zzfyf);
        } else {
            ((zzdac) zzcip.zzddn).zza(this.zzup, zzczt.zzgmh.zzfgl.zzgml, zzczl.zzglr.toString(), zzaxs.zza((zzaxx) zzczl.zzglo), (zzali) zzcip.zzfyf);
        }
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzbtw zza = this.zzfyt.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzbtv(new zzckj(zzcip)));
        zza.zzadh().zza(new zzbiu((zzdac) zzcip.zzddn), this.zzfci);
        ((zzcjy) zzcip.zzfyf).zza((zzali) zza.zzadm());
        return zza.zzaem();
    }
}
