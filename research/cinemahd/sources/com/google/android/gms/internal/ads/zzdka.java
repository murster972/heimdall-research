package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdka extends zzdik<zzdhx, zzdnz> {
    zzdka(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        return new zzdpq(((zzdnz) obj).zzass().toByteArray());
    }
}
