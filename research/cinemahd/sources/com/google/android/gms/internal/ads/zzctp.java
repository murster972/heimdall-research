package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzctp implements zzcty<Bundle> {
    private final String zzdpg;
    private final String zzggo;
    private final String zzggp;
    private final String zzggq;
    private final Long zzggr;

    public zzctp(String str, String str2, String str3, String str4, Long l) {
        this.zzdpg = str;
        this.zzggo = str2;
        this.zzggp = str3;
        this.zzggq = str4;
        this.zzggr = l;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        zzdaa.zza(bundle, "gmp_app_id", this.zzdpg);
        zzdaa.zza(bundle, "fbs_aiid", this.zzggo);
        zzdaa.zza(bundle, "fbs_aeid", this.zzggp);
        zzdaa.zza(bundle, "apm_id_origin", this.zzggq);
        Long l = this.zzggr;
        if (l != null) {
            bundle.putLong("sai_timeout", l.longValue());
        }
    }
}
