package com.google.android.gms.internal.ads;

import android.os.IBinder;

final /* synthetic */ class zzst implements zzayw {
    static final zzayw zzbtz = new zzst();

    private zzst() {
    }

    public final Object apply(Object obj) {
        return zzgh.zza((IBinder) obj);
    }
}
