package com.google.android.gms.internal.ads;

public interface zzhe {
    int getTrackType();

    int zza(zzgw zzgw) throws zzgl;

    int zzdw() throws zzgl;
}
