package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

final class zzsl extends PushbackInputStream {
    private final /* synthetic */ zzsg zzbsa;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzsl(zzsg zzsg, InputStream inputStream, int i) {
        super(inputStream, 1);
        this.zzbsa = zzsg;
    }

    public final synchronized void close() throws IOException {
        this.zzbsa.zzbrt.disconnect();
        super.close();
    }
}
