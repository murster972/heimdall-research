package com.google.android.gms.internal.ads;

final class zzgu {
    public final zzhg zzade;
    public final int zzaev;
    public final long zzaew;

    public zzgu(zzhg zzhg, int i, long j) {
        this.zzade = zzhg;
        this.zzaev = i;
        this.zzaew = j;
    }
}
