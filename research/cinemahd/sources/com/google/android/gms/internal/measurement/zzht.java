package com.google.android.gms.internal.measurement;

final class zzht implements zzhs {
    private final /* synthetic */ zzdv zza;

    zzht(zzdv zzdv) {
        this.zza = zzdv;
    }

    public final int zza() {
        return this.zza.zza();
    }

    public final byte zza(int i) {
        return this.zza.zza(i);
    }
}
