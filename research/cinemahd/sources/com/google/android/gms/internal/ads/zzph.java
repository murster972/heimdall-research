package com.google.android.gms.internal.ads;

final class zzph implements Runnable {
    private final /* synthetic */ zzgw zzahh;
    private final /* synthetic */ zzpg zzbjg;

    zzph(zzpg zzpg, zzgw zzgw) {
        this.zzbjg = zzpg;
        this.zzahh = zzgw;
    }

    public final void run() {
        this.zzbjg.zzbjh.zzk(this.zzahh);
    }
}
