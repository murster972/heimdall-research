package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbxd implements zzdxg<zzats> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzczu> zzfep;
    private final zzbxe zzfmy;

    private zzbxd(zzbxe zzbxe, zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2) {
        this.zzfmy = zzbxe;
        this.zzejv = zzdxp;
        this.zzfep = zzdxp2;
    }

    public static zzbxd zza(zzbxe zzbxe, zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2) {
        return new zzbxd(zzbxe, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzats) zzdxm.zza(new zzats(this.zzejv.get(), this.zzfep.get().zzgmm), "Cannot return null from a non-@Nullable @Provides method");
    }
}
