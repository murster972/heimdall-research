package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

final class zzbff implements zzo {
    private zzo zzdhq;
    private zzbdi zzeey;

    public zzbff(zzbdi zzbdi, zzo zzo) {
        this.zzeey = zzbdi;
        this.zzdhq = zzo;
    }

    public final void onPause() {
    }

    public final void onResume() {
    }

    public final void zzte() {
        this.zzdhq.zzte();
        this.zzeey.zzzt();
    }

    public final void zztf() {
        this.zzdhq.zztf();
        this.zzeey.zztr();
    }
}
