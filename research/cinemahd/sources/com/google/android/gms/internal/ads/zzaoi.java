package com.google.android.gms.internal.ads;

import android.app.DownloadManager;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Environment;
import com.google.android.gms.ads.internal.zzq;
import com.vungle.warren.ui.JavascriptBridge;

final class zzaoi implements DialogInterface.OnClickListener {
    private final /* synthetic */ zzaof zzdfw;
    private final /* synthetic */ String zzdfx;
    private final /* synthetic */ String zzdfy;

    zzaoi(zzaof zzaof, String str, String str2) {
        this.zzdfw = zzaof;
        this.zzdfx = str;
        this.zzdfy = str2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        DownloadManager downloadManager = (DownloadManager) this.zzdfw.zzup.getSystemService(JavascriptBridge.MraidHandler.DOWNLOAD_ACTION);
        try {
            String str = this.zzdfx;
            String str2 = this.zzdfy;
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(str));
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, str2);
            zzq.zzks();
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(1);
            downloadManager.enqueue(request);
        } catch (IllegalStateException unused) {
            this.zzdfw.zzds("Could not store picture.");
        }
    }
}
