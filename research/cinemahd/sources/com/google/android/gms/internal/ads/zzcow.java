package com.google.android.gms.internal.ads;

final class zzcow implements zzcoz<zzbmd> {
    private final /* synthetic */ zzcot zzgdp;

    zzcow(zzcot zzcot) {
        this.zzgdp = zzcot;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzbmd zzbmd = (zzbmd) obj;
        synchronized (this.zzgdp) {
            boolean unused = this.zzgdp.zzadd = false;
            zzxa unused2 = this.zzgdp.zzgdj = zzbmd.zzags();
            zzbmd.zzagf();
        }
    }

    public final void zzamx() {
        synchronized (this.zzgdp) {
            boolean unused = this.zzgdp.zzadd = false;
        }
    }
}
