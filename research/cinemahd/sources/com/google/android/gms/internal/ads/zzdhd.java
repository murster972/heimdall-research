package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

public interface zzdhd extends ExecutorService {
    <T> zzdhe<T> zzd(Callable<T> callable);

    zzdhe<?> zzf(Runnable runnable);
}
