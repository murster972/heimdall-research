package com.google.android.gms.internal.ads;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class zzel implements ThreadFactory {
    private final ThreadFactory zzyd = Executors.defaultThreadFactory();
    private final AtomicInteger zzye = new AtomicInteger(1);

    zzel() {
    }

    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.zzyd.newThread(runnable);
        int andIncrement = this.zzye.getAndIncrement();
        StringBuilder sb = new StringBuilder(16);
        sb.append("gads-");
        sb.append(andIncrement);
        newThread.setName(sb.toString());
        return newThread;
    }
}
