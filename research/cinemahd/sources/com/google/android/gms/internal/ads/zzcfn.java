package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcfn implements zzdxg<zzdhe<String>> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdq> zzfbr;
    private final zzdxp<zzdhd> zzfva;

    private zzcfn(zzdxp<zzdq> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzdhd> zzdxp3) {
        this.zzfbr = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfva = zzdxp3;
    }

    public static zzcfn zzm(zzdxp<zzdq> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzdhd> zzdxp3) {
        return new zzcfn(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return (zzdhe) zzdxm.zza(this.zzfva.get().zzd(new zzcfj(this.zzfbr.get(), this.zzejv.get())), "Cannot return null from a non-@Nullable @Provides method");
    }
}
