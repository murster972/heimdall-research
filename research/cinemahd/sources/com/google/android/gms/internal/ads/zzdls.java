package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdls extends zzdrt<zzdls, zza> implements zzdtg {
    private static volatile zzdtn<zzdls> zzdz;
    /* access modifiers changed from: private */
    public static final zzdls zzhax;
    private int zzhas;

    public static final class zza extends zzdrt.zzb<zzdls, zza> implements zzdtg {
        private zza() {
            super(zzdls.zzhax);
        }

        /* synthetic */ zza(zzdlt zzdlt) {
            this();
        }
    }

    static {
        zzdls zzdls = new zzdls();
        zzhax = zzdls;
        zzdrt.zza(zzdls.class, zzdls);
    }

    private zzdls() {
    }

    public static zzdls zzatu() {
        return zzhax;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlt.zzdk[i - 1]) {
            case 1:
                return new zzdls();
            case 2:
                return new zza((zzdlt) null);
            case 3:
                return zzdrt.zza((zzdte) zzhax, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u000b", new Object[]{"zzhas"});
            case 4:
                return zzhax;
            case 5:
                zzdtn<zzdls> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdls.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhax);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final int zzatn() {
        return this.zzhas;
    }
}
