package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzauq extends zzgc implements zzauo {
    zzauq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.signals.ISignalGenerator");
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzauu zzauu, zzaun zzaun) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, (IInterface) iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, (Parcelable) zzauu);
        zzge.zza(obtainAndWriteInterfaceToken, (IInterface) zzaun);
        zza(1, obtainAndWriteInterfaceToken);
    }
}
