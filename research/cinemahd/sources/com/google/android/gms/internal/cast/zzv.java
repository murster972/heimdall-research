package com.google.android.gms.internal.cast;

import android.os.RemoteException;
import androidx.mediarouter.media.MediaRouter;
import com.google.android.gms.common.internal.Preconditions;

public final class zzv extends MediaRouter.Callback {
    private static final zzdw zzbf = new zzdw("MediaRouterCallback");
    private final zzl zzjp;

    public zzv(zzl zzl) {
        Preconditions.a(zzl);
        this.zzjp = zzl;
    }

    public final void onRouteAdded(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.zzjp.zza(routeInfo.h(), routeInfo.f());
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "onRouteAdded", zzl.class.getSimpleName());
        }
    }

    public final void onRouteChanged(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.zzjp.zzb(routeInfo.h(), routeInfo.f());
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "onRouteChanged", zzl.class.getSimpleName());
        }
    }

    public final void onRouteRemoved(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.zzjp.zzc(routeInfo.h(), routeInfo.f());
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "onRouteRemoved", zzl.class.getSimpleName());
        }
    }

    public final void onRouteSelected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo) {
        try {
            this.zzjp.zzd(routeInfo.h(), routeInfo.f());
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "onRouteSelected", zzl.class.getSimpleName());
        }
    }

    public final void onRouteUnselected(MediaRouter mediaRouter, MediaRouter.RouteInfo routeInfo, int i) {
        try {
            this.zzjp.zza(routeInfo.h(), routeInfo.f(), i);
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "onRouteUnselected", zzl.class.getSimpleName());
        }
    }
}
