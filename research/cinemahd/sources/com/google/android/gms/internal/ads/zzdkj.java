package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

@Deprecated
public final class zzdkj {
    @Deprecated
    public static zzdie zza(zzdij zzdij, zzdid<zzdie> zzdid) throws GeneralSecurityException {
        zzdit.zza(new zzdki());
        return (zzdie) zzdit.zza(zzdit.zza(zzdij, (zzdid) null, zzdie.class));
    }
}
