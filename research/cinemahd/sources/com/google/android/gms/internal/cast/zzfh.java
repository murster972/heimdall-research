package com.google.android.gms.internal.cast;

import android.view.Choreographer;

final class zzfh implements Choreographer.FrameCallback {
    private final /* synthetic */ zzfg zzabr;

    zzfh(zzfg zzfg) {
        this.zzabr = zzfg;
    }

    public final void doFrame(long j) {
        this.zzabr.doFrame(j);
    }
}
