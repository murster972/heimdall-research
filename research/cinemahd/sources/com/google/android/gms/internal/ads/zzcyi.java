package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcyi implements zzcxo {
    private final int zzdvv;

    zzcyi(int i) {
        this.zzdvv = i;
    }

    public final void zzt(Object obj) {
        ((zzaro) obj).onRewardedVideoAdFailedToLoad(this.zzdvv);
    }
}
