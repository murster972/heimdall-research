package com.google.android.gms.internal.ads;

final class zzil implements zzic {
    private final /* synthetic */ zzij zzakx;

    private zzil(zzij zzij) {
        this.zzakx = zzij;
    }

    public final void zzc(int i, long j, long j2) {
        this.zzakx.zzakq.zzb(i, j, j2);
        zzij.zza(i, j, j2);
    }

    public final void zzed() {
        zzij.zzft();
        boolean unused = this.zzakx.zzakw = true;
    }

    public final void zzr(int i) {
        this.zzakx.zzakq.zzs(i);
        zzij.zzr(i);
    }
}
