package com.google.android.gms.internal.ads;

public final class zzble implements zzdxg<zzbsu<zzbrb>> {
    private final zzbkn zzfen;
    private final zzdxp<zzbqp> zzfeq;

    public zzble(zzbkn zzbkn, zzdxp<zzbqp> zzdxp) {
        this.zzfen = zzbkn;
        this.zzfeq = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(new zzbkp(this.zzfeq.get()), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
