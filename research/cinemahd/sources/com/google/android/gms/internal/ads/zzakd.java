package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import org.json.JSONObject;

public final class zzakd<I, O> implements zzaju<I, O> {
    private final zzais zzdav;
    /* access modifiers changed from: private */
    public final zzajw<O> zzdaw;
    private final zzajv<I> zzdax;
    private final String zzday;

    zzakd(zzais zzais, String str, zzajv<I> zzajv, zzajw<O> zzajw) {
        this.zzdav = zzais;
        this.zzday = str;
        this.zzdax = zzajv;
        this.zzdaw = zzajw;
    }

    /* access modifiers changed from: private */
    public final void zza(zzajf zzajf, zzajq zzajq, I i, zzazl<O> zzazl) {
        try {
            zzq.zzkq();
            String zzwk = zzawb.zzwk();
            zzafa.zzcxi.zza(zzwk, (zzafv) new zzaki(this, zzajf, zzazl));
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", zzwk);
            jSONObject.put("args", this.zzdax.zzj(i));
            zzajq.zza(this.zzday, jSONObject);
        } catch (Exception e) {
            zzazl.setException(e);
            zzayu.zzc("Unable to invokeJavascript", e);
            zzajf.release();
        } catch (Throwable th) {
            zzajf.release();
            throw th;
        }
    }

    public final zzdhe<O> zzf(I i) throws Exception {
        return zzi(i);
    }

    public final zzdhe<O> zzi(I i) {
        zzazl zzazl = new zzazl();
        zzajf zzb = this.zzdav.zzb((zzdq) null);
        zzb.zza(new zzakg(this, zzb, i, zzazl), new zzakf(this, zzazl, zzb));
        return zzazl;
    }
}
