package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final /* synthetic */ class zzbdn implements Runnable {
    private final String zzcyz;

    zzbdn(String str) {
        this.zzcyz = str;
    }

    public final void run() {
        zzq.zzku().zzuz().zzcr(this.zzcyz.substring(1));
    }
}
