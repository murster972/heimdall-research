package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzclg implements zzdxg<zzclb> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbvm> zzfyl;

    public zzclg(zzdxp<Context> zzdxp, zzdxp<zzbvm> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfyl = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzclb(this.zzejv.get(), this.zzfyl.get());
    }
}
