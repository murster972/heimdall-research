package com.google.android.gms.internal.ads;

import android.view.View;

public final class zzbjw {
    private final View view;
    private final zzbdi zzcza;
    private final zzczk zzfdo;
    private final int zzfdp;

    public zzbjw(View view2, zzbdi zzbdi, zzczk zzczk, int i) {
        this.view = view2;
        this.zzcza = zzbdi;
        this.zzfdo = zzczk;
        this.zzfdp = i;
    }

    public final zzbdi zzaft() {
        return this.zzcza;
    }

    public final View zzafu() {
        return this.view;
    }

    public final zzczk zzafv() {
        return this.zzfdo;
    }

    public final int zzafw() {
        return this.zzfdp;
    }
}
