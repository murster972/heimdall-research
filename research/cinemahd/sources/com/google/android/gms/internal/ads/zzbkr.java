package com.google.android.gms.internal.ads;

import android.view.View;

public final class zzbkr implements zzdxg<View> {
    private final zzbkn zzfen;

    public zzbkr(zzbkn zzbkn) {
        this.zzfen = zzbkn;
    }

    public static View zza(zzbkn zzbkn) {
        return (View) zzdxm.zza(zzbkn.zzaga(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfen);
    }
}
