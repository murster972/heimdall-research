package com.google.android.gms.internal.ads;

final class zzbr implements zzdsa {
    static final zzdsa zzew = new zzbr();

    private zzbr() {
    }

    public final boolean zzf(int i) {
        return zzbq.zze(i) != null;
    }
}
