package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzaru extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaru> CREATOR = new zzarx();
    public final String zzbqz;
    public final zzug zzdio;

    public zzaru(zzug zzug, String str) {
        this.zzdio = zzug;
        this.zzbqz = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, (Parcelable) this.zzdio, i, false);
        SafeParcelWriter.a(parcel, 3, this.zzbqz, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
