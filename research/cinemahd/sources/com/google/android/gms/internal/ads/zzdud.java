package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class zzdud implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private Iterator<Map.Entry<K, V>> zzhqv;
    private final /* synthetic */ zzdub zzhqw;

    private zzdud(zzdub zzdub) {
        this.zzhqw = zzdub;
        this.pos = this.zzhqw.zzhqm.size();
    }

    private final Iterator<Map.Entry<K, V>> zzbcc() {
        if (this.zzhqv == null) {
            this.zzhqv = this.zzhqw.zzhqp.entrySet().iterator();
        }
        return this.zzhqv;
    }

    public final boolean hasNext() {
        int i = this.pos;
        return (i > 0 && i <= this.zzhqw.zzhqm.size()) || zzbcc().hasNext();
    }

    public final /* synthetic */ Object next() {
        if (zzbcc().hasNext()) {
            return (Map.Entry) zzbcc().next();
        }
        List zzb = this.zzhqw.zzhqm;
        int i = this.pos - 1;
        this.pos = i;
        return (Map.Entry) zzb.get(i);
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* synthetic */ zzdud(zzdub zzdub, zzdue zzdue) {
        this(zzdub);
    }
}
