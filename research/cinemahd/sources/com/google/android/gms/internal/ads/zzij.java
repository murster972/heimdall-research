package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.view.Surface;
import java.nio.ByteBuffer;

@TargetApi(16)
public final class zzij extends zzkw implements zzog {
    private int zzafo;
    private int zzafq;
    /* access modifiers changed from: private */
    public final zzhr zzakq;
    private final zzhw zzakr;
    private boolean zzaks;
    private boolean zzakt;
    private MediaFormat zzaku;
    private long zzakv;
    /* access modifiers changed from: private */
    public boolean zzakw;

    public zzij(zzky zzky) {
        this(zzky, (zzja<zzjc>) null, true);
    }

    protected static void zza(int i, long j, long j2) {
    }

    private final boolean zzbc(String str) {
        return this.zzakr.zzba(str);
    }

    protected static void zzft() {
    }

    protected static void zzr(int i) {
    }

    public final boolean isReady() {
        return this.zzakr.zzfh() || super.isReady();
    }

    /* access modifiers changed from: protected */
    public final void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) throws zzgl {
        int[] iArr;
        int i;
        boolean z = this.zzaku != null;
        String string = z ? this.zzaku.getString("mime") : "audio/raw";
        if (z) {
            mediaFormat = this.zzaku;
        }
        int integer = mediaFormat.getInteger("channel-count");
        int integer2 = mediaFormat.getInteger("sample-rate");
        if (!this.zzakt || integer != 6 || (i = this.zzafo) >= 6) {
            iArr = null;
        } else {
            iArr = new int[i];
            for (int i2 = 0; i2 < this.zzafo; i2++) {
                iArr[i2] = i2;
            }
        }
        try {
            this.zzakr.zza(string, integer, integer2, this.zzafq, 0, iArr);
        } catch (zzia e) {
            throw zzgl.zza(e, getIndex());
        }
    }

    /* access modifiers changed from: protected */
    public final void onStarted() {
        super.onStarted();
        this.zzakr.play();
    }

    /* access modifiers changed from: protected */
    public final void onStopped() {
        this.zzakr.pause();
        super.onStopped();
    }

    /* access modifiers changed from: protected */
    public final int zza(zzky zzky, zzgw zzgw) throws zzlb {
        int i;
        int i2;
        String str = zzgw.zzafe;
        if (!zzof.zzbh(str)) {
            return 0;
        }
        int i3 = zzoq.SDK_INT >= 21 ? 16 : 0;
        int i4 = 3;
        if (zzbc(str) && zzky.zzhb() != null) {
            return i3 | 4 | 3;
        }
        zzkt zzb = zzky.zzb(str, false);
        boolean z = true;
        if (zzb == null) {
            return 1;
        }
        if (zzoq.SDK_INT >= 21 && (((i = zzgw.zzafp) != -1 && !zzb.zzaq(i)) || ((i2 = zzgw.zzafo) != -1 && !zzb.zzar(i2)))) {
            z = false;
        }
        if (!z) {
            i4 = 2;
        }
        return i3 | 4 | i4;
    }

    public final zzhc zzb(zzhc zzhc) {
        return this.zzakr.zzb(zzhc);
    }

    /* access modifiers changed from: protected */
    public final void zzc(String str, long j, long j2) {
        this.zzakq.zzb(str, j, j2);
    }

    /* access modifiers changed from: protected */
    public final void zzd(zzgw zzgw) throws zzgl {
        super.zzd(zzgw);
        this.zzakq.zzc(zzgw);
        this.zzafq = "audio/raw".equals(zzgw.zzafe) ? zzgw.zzafq : 2;
        this.zzafo = zzgw.zzafo;
    }

    public final zzog zzdq() {
        return this;
    }

    /* access modifiers changed from: protected */
    public final void zzdx() {
        try {
            this.zzakr.release();
            try {
                super.zzdx();
            } finally {
                this.zzaze.zzge();
                this.zzakq.zzd(this.zzaze);
            }
        } catch (Throwable th) {
            super.zzdx();
            throw th;
        } finally {
            this.zzaze.zzge();
            this.zzakq.zzd(this.zzaze);
        }
    }

    /* access modifiers changed from: protected */
    public final void zze(boolean z) throws zzgl {
        super.zze(z);
        this.zzakq.zzc(this.zzaze);
        int i = zzdy().zzagh;
        if (i != 0) {
            this.zzakr.zzt(i);
        } else {
            this.zzakr.zzfj();
        }
    }

    public final boolean zzeu() {
        return super.zzeu() && this.zzakr.zzeu();
    }

    public final zzhc zzfi() {
        return this.zzakr.zzfi();
    }

    public final long zzfp() {
        long zzj = this.zzakr.zzj(zzeu());
        if (zzj != Long.MIN_VALUE) {
            if (!this.zzakw) {
                zzj = Math.max(this.zzakv, zzj);
            }
            this.zzakv = zzj;
            this.zzakw = false;
        }
        return this.zzakv;
    }

    /* access modifiers changed from: protected */
    public final void zzfu() throws zzgl {
        try {
            this.zzakr.zzff();
        } catch (zzie e) {
            throw zzgl.zza(e, getIndex());
        }
    }

    private zzij(zzky zzky, zzja<zzjc> zzja, boolean z) {
        this(zzky, (zzja<zzjc>) null, true, (zzddu) null, (zzho) null);
    }

    private zzij(zzky zzky, zzja<zzjc> zzja, boolean z, zzddu zzddu, zzho zzho) {
        this(zzky, (zzja<zzjc>) null, true, (zzddu) null, (zzho) null, (zzhn) null, new zzhm[0]);
    }

    private zzij(zzky zzky, zzja<zzjc> zzja, boolean z, zzddu zzddu, zzho zzho, zzhn zzhn, zzhm... zzhmArr) {
        super(1, zzky, zzja, z);
        this.zzakr = new zzhw((zzhn) null, zzhmArr, new zzil(this));
        this.zzakq = new zzhr((zzddu) null, (zzho) null);
    }

    /* access modifiers changed from: protected */
    public final zzkt zza(zzky zzky, zzgw zzgw, boolean z) throws zzlb {
        zzkt zzhb;
        if (!zzbc(zzgw.zzafe) || (zzhb = zzky.zzhb()) == null) {
            this.zzaks = false;
            return super.zza(zzky, zzgw, z);
        }
        this.zzaks = true;
        return zzhb;
    }

    /* access modifiers changed from: protected */
    public final void zza(zzkt zzkt, MediaCodec mediaCodec, zzgw zzgw, MediaCrypto mediaCrypto) {
        this.zzakt = zzoq.SDK_INT < 24 && "OMX.SEC.aac.dec".equals(zzkt.name) && "samsung".equals(zzoq.MANUFACTURER) && (zzoq.DEVICE.startsWith("zeroflte") || zzoq.DEVICE.startsWith("herolte") || zzoq.DEVICE.startsWith("heroqlte"));
        if (this.zzaks) {
            this.zzaku = zzgw.zzeq();
            this.zzaku.setString("mime", "audio/raw");
            mediaCodec.configure(this.zzaku, (Surface) null, (MediaCrypto) null, 0);
            this.zzaku.setString("mime", zzgw.zzafe);
            return;
        }
        mediaCodec.configure(zzgw.zzeq(), (Surface) null, (MediaCrypto) null, 0);
        this.zzaku = null;
    }

    /* access modifiers changed from: protected */
    public final void zza(long j, boolean z) throws zzgl {
        super.zza(j, z);
        this.zzakr.reset();
        this.zzakv = j;
        this.zzakw = true;
    }

    /* access modifiers changed from: protected */
    public final boolean zza(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j3, boolean z) throws zzgl {
        if (this.zzaks && (i2 & 2) != 0) {
            mediaCodec.releaseOutputBuffer(i, false);
            return true;
        } else if (z) {
            mediaCodec.releaseOutputBuffer(i, false);
            this.zzaze.zzamj++;
            this.zzakr.zzfe();
            return true;
        } else {
            try {
                if (!this.zzakr.zza(byteBuffer, j3)) {
                    return false;
                }
                mediaCodec.releaseOutputBuffer(i, false);
                this.zzaze.zzami++;
                return true;
            } catch (zzid | zzie e) {
                throw zzgl.zza(e, getIndex());
            }
        }
    }

    public final void zza(int i, Object obj) throws zzgl {
        if (i == 2) {
            this.zzakr.setVolume(((Float) obj).floatValue());
        } else if (i != 3) {
            super.zza(i, obj);
        } else {
            this.zzakr.setStreamType(((Integer) obj).intValue());
        }
    }
}
