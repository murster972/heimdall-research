package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzcwh implements zzcty<JSONObject> {
    private final Map<String, Object> zzgik;

    public zzcwh(Map<String, Object> map) {
        this.zzgik = map;
    }

    public final /* synthetic */ void zzr(Object obj) {
        try {
            ((JSONObject) obj).put("video_decoders", zzq.zzkq().zzi((Map<String, ?>) this.zzgik));
        } catch (JSONException e) {
            String valueOf = String.valueOf(e.getMessage());
            zzavs.zzed(valueOf.length() != 0 ? "Could not encode video decoder properties: ".concat(valueOf) : new String("Could not encode video decoder properties: "));
        }
    }
}
