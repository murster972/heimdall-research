package com.google.android.gms.internal.ads;

import java.util.Set;

final class zzdcq extends zzbrl<zzdcx> implements zzdcp<zzdco> {
    zzdcq(Set<zzbsu<zzdcx>> set) {
        super(set);
    }

    public final void zza(zzdca<zzdco, ?> zzdca) {
        zza(new zzdct(zzdca));
    }

    public final void zzb(zzdca<zzdco, ?> zzdca) {
        zza(new zzdcs(zzdca));
    }

    public final void zzc(zzdca<zzdco, ?> zzdca) {
        zza(new zzdcu(zzdca));
    }

    public final void zza(zzdca<zzdco, ?> zzdca, Throwable th) {
        zza(new zzdcv(zzdca, th));
    }
}
