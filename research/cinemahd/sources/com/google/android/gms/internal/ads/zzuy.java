package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

final class zzuy extends zzvb<zzvn> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ String zzcdg;
    private final /* synthetic */ zzalc zzcdh;
    private final /* synthetic */ zzup zzcdi;

    zzuy(zzup zzup, Context context, String str, zzalc zzalc) {
        this.zzcdi = zzup;
        this.val$context = context;
        this.zzcdg = str;
        this.zzcdh = zzalc;
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.a(this.val$context), this.zzcdg, this.zzcdh, 19649000);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, "native_ad");
        return new zzxz();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzccy.zza(this.val$context, this.zzcdg, this.zzcdh);
    }
}
