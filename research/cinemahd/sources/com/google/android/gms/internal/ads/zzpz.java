package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;

final class zzpz implements zzqd {
    private final /* synthetic */ Activity val$activity;

    zzpz(zzpv zzpv, Activity activity) {
        this.val$activity = activity;
    }

    public final void zza(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityPaused(this.val$activity);
    }
}
