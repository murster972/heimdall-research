package com.google.android.gms.internal.ads;

public abstract class zzblg {
    public abstract zzbkj zza(zzbmt zzbmt, zzbkn zzbkn);

    public abstract zzblp zza(zzbmt zzbmt, zzbls zzbls);

    public abstract zzbmz<zzbkk> zzadc();

    public abstract zzbou zzadd();

    public abstract zzbmi<zzbkk> zzaed();
}
