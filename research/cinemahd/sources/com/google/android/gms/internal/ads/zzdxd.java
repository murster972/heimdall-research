package com.google.android.gms.internal.ads;

public final class zzdxd<T> implements zzdxa<T>, zzdxp<T> {
    private static final Object zzhzz = new Object();
    private volatile Object zzduw = zzhzz;
    private volatile zzdxp<T> zziaa;

    private zzdxd(zzdxp<T> zzdxp) {
        this.zziaa = zzdxp;
    }

    public static <P extends zzdxp<T>, T> zzdxp<T> zzan(P p) {
        zzdxm.checkNotNull(p);
        if (p instanceof zzdxd) {
            return p;
        }
        return new zzdxd(p);
    }

    public static <P extends zzdxp<T>, T> zzdxa<T> zzao(P p) {
        if (p instanceof zzdxa) {
            return (zzdxa) p;
        }
        return new zzdxd((zzdxp) zzdxm.checkNotNull(p));
    }

    public final T get() {
        T t = this.zzduw;
        if (t == zzhzz) {
            synchronized (this) {
                t = this.zzduw;
                if (t == zzhzz) {
                    t = this.zziaa.get();
                    T t2 = this.zzduw;
                    if (t2 != zzhzz && !(t2 instanceof zzdxj)) {
                        if (t2 != t) {
                            String valueOf = String.valueOf(t2);
                            String valueOf2 = String.valueOf(t);
                            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 118 + String.valueOf(valueOf2).length());
                            sb.append("Scoped provider was invoked recursively returning different results: ");
                            sb.append(valueOf);
                            sb.append(" & ");
                            sb.append(valueOf2);
                            sb.append(". This is likely due to a circular dependency.");
                            throw new IllegalStateException(sb.toString());
                        }
                    }
                    this.zzduw = t;
                    this.zziaa = null;
                }
            }
        }
        return t;
    }
}
