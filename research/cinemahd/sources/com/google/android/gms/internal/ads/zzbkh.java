package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import java.util.concurrent.ScheduledExecutorService;

public final class zzbkh implements zzdxg<zzbqp> {
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<ScheduledExecutorService> zzfdw;

    public zzbkh(zzdxp<ScheduledExecutorService> zzdxp, zzdxp<Clock> zzdxp2) {
        this.zzfdw = zzdxp;
        this.zzfcz = zzdxp2;
    }

    public static zzbqp zza(ScheduledExecutorService scheduledExecutorService, Clock clock) {
        return (zzbqp) zzdxm.zza(new zzbqp(scheduledExecutorService, clock), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfdw.get(), this.zzfcz.get());
    }
}
