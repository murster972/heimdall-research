package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdfs;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.logging.Level;
import java.util.logging.Logger;

abstract class zzdgc<OutputT> extends zzdfs.zzj<OutputT> {
    private static final Logger zzgvj = Logger.getLogger(zzdgc.class.getName());
    private static final zzb zzgwi;
    private volatile int remaining;
    /* access modifiers changed from: private */
    public volatile Set<Throwable> seenExceptions = null;

    static final class zza extends zzb {
        private final AtomicReferenceFieldUpdater<zzdgc, Set<Throwable>> zzgwj;
        private final AtomicIntegerFieldUpdater<zzdgc> zzgwk;

        zza(AtomicReferenceFieldUpdater atomicReferenceFieldUpdater, AtomicIntegerFieldUpdater atomicIntegerFieldUpdater) {
            super();
            this.zzgwj = atomicReferenceFieldUpdater;
            this.zzgwk = atomicIntegerFieldUpdater;
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzdgc zzdgc, Set<Throwable> set, Set<Throwable> set2) {
            this.zzgwj.compareAndSet(zzdgc, (Object) null, set2);
        }

        /* access modifiers changed from: package-private */
        public final int zzc(zzdgc zzdgc) {
            return this.zzgwk.decrementAndGet(zzdgc);
        }
    }

    static abstract class zzb {
        private zzb() {
        }

        /* access modifiers changed from: package-private */
        public abstract void zza(zzdgc zzdgc, Set<Throwable> set, Set<Throwable> set2);

        /* access modifiers changed from: package-private */
        public abstract int zzc(zzdgc zzdgc);
    }

    static final class zzc extends zzb {
        private zzc() {
            super();
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzdgc zzdgc, Set<Throwable> set, Set<Throwable> set2) {
            synchronized (zzdgc) {
                if (zzdgc.seenExceptions == null) {
                    Set unused = zzdgc.seenExceptions = set2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final int zzc(zzdgc zzdgc) {
            int zzb;
            synchronized (zzdgc) {
                zzb = zzdgc.zzb(zzdgc);
            }
            return zzb;
        }
    }

    static {
        Throwable th;
        zzb zzb2;
        try {
            zzb2 = new zza(AtomicReferenceFieldUpdater.newUpdater(zzdgc.class, Set.class, "seenExceptions"), AtomicIntegerFieldUpdater.newUpdater(zzdgc.class, "remaining"));
            th = null;
        } catch (Throwable th2) {
            th = th2;
            zzb2 = new zzc();
        }
        zzgwi = zzb2;
        if (th != null) {
            zzgvj.logp(Level.SEVERE, "com.google.common.util.concurrent.AggregateFutureState", "<clinit>", "SafeAtomicHelper is broken!", th);
        }
    }

    zzdgc(int i) {
        this.remaining = i;
    }

    static /* synthetic */ int zzb(zzdgc zzdgc) {
        int i = zzdgc.remaining - 1;
        zzdgc.remaining = i;
        return i;
    }

    /* access modifiers changed from: package-private */
    public final Set<Throwable> zzarp() {
        Set<Throwable> set = this.seenExceptions;
        if (set != null) {
            return set;
        }
        Set newSetFromMap = Collections.newSetFromMap(new ConcurrentHashMap());
        zzg(newSetFromMap);
        zzgwi.zza(this, (Set<Throwable>) null, newSetFromMap);
        return this.seenExceptions;
    }

    /* access modifiers changed from: package-private */
    public final int zzarq() {
        return zzgwi.zzc(this);
    }

    /* access modifiers changed from: package-private */
    public final void zzarr() {
        this.seenExceptions = null;
    }

    /* access modifiers changed from: package-private */
    public abstract void zzg(Set<Throwable> set);
}
