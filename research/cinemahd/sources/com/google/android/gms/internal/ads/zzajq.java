package com.google.android.gms.internal.ads;

public interface zzajq extends zzahs, zzaip {
    void zza(String str, zzafn<? super zzajq> zzafn);

    void zzb(String str, zzafn<? super zzajq> zzafn);
}
