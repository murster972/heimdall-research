package com.google.android.gms.internal.ads;

final class zzcyu implements zzdgt<zzcbb> {
    private final /* synthetic */ zzcoz zzgdv;
    private final /* synthetic */ zzcyt zzgku;

    zzcyu(zzcyt zzcyt, zzcoz zzcoz) {
        this.zzgku = zzcyt;
        this.zzgdv = zzcoz;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzcbb zzcbb = (zzcbb) obj;
        synchronized (this.zzgku) {
            this.zzgdv.onSuccess(zzcbb);
            this.zzgku.zzgks.onAdMetadataChanged();
        }
    }

    public final void zzb(Throwable th) {
        synchronized (this.zzgku) {
            ((zzcbi) this.zzgku.zzgio.zzaog()).zzadd().onAdFailedToLoad(zzcfb.zzd(th));
            zzdad.zzc(th, "RewardedAdLoader.onFailure");
            this.zzgdv.zzamx();
        }
    }
}
