package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManagerListener;

final class zzz implements SessionManagerListener<CastSession> {
    private final /* synthetic */ zzx zzjv;

    private zzz(zzx zzx) {
        this.zzjv = zzx;
    }

    public final /* synthetic */ void onSessionEnded(Session session, int i) {
        zzx zzx = this.zzjv;
        zzx.zzb(zzx.zzg((CastSession) session));
    }

    public final /* synthetic */ void onSessionEnding(Session session) {
        zzx zzx = this.zzjv;
        zzx.zzb(zzx.zzg((CastSession) session));
    }

    public final /* bridge */ /* synthetic */ void onSessionResumeFailed(Session session, int i) {
    }

    public final /* synthetic */ void onSessionResumed(Session session, boolean z) {
        zzx zzx = this.zzjv;
        zzx.zza(zzx.zzg((CastSession) session), false);
    }

    public final /* bridge */ /* synthetic */ void onSessionResuming(Session session, String str) {
    }

    public final /* bridge */ /* synthetic */ void onSessionStartFailed(Session session, int i) {
    }

    public final /* synthetic */ void onSessionStarted(Session session, String str) {
        zzx zzx = this.zzjv;
        zzx.zza(zzx.zzg((CastSession) session), false);
    }

    public final /* bridge */ /* synthetic */ void onSessionStarting(Session session) {
    }

    public final /* synthetic */ void onSessionSuspended(Session session, int i) {
        zzx zzx = this.zzjv;
        zzx.zza(zzx.zzg((CastSession) session));
    }
}
