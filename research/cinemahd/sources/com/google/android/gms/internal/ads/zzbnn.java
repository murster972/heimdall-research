package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbnn implements zzdxg<zzbsu<zzbpe>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbiw> zzfdq;

    private zzbnn(zzdxp<zzbiw> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdq = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzbnn zze(zzdxp<zzbiw> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzbnn(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
