package com.google.android.gms.internal.ads;

public final class zzbvk implements zzdxg<zzbvi> {
    private final zzbvi zzfjv;

    private zzbvk(zzbvi zzbvi) {
        this.zzfjv = zzbvi;
    }

    public static zzbvk zze(zzbvi zzbvi) {
        return new zzbvk(zzbvi);
    }

    public final /* synthetic */ Object get() {
        zzbvi zzbvi = this.zzfjv;
        if (zzbvi != null) {
            return (zzbvi) zzdxm.zza(zzbvi, "Cannot return null from a non-@Nullable @Provides method");
        }
        throw null;
    }
}
