package com.google.android.gms.internal.ads;

import android.view.Surface;

public final class zzpg {
    private final zzddu zzaee;
    /* access modifiers changed from: private */
    public final zzpd zzbjh;

    public zzpg(zzddu zzddu, zzpd zzpd) {
        this.zzaee = zzpd != null ? (zzddu) zzoc.checkNotNull(zzddu) : null;
        this.zzbjh = zzpd;
    }

    public final void zzb(String str, long j, long j2) {
        if (this.zzbjh != null) {
            this.zzaee.post(new zzpi(this, str, j, j2));
        }
    }

    public final void zzc(zzit zzit) {
        if (this.zzbjh != null) {
            this.zzaee.post(new zzpf(this, zzit));
        }
    }

    public final void zzd(zzit zzit) {
        if (this.zzbjh != null) {
            this.zzaee.post(new zzpl(this, zzit));
        }
    }

    public final void zzf(int i, long j) {
        if (this.zzbjh != null) {
            this.zzaee.post(new zzpk(this, i, j));
        }
    }

    public final void zzb(int i, int i2, int i3, float f) {
        if (this.zzbjh != null) {
            this.zzaee.post(new zzpj(this, i, i2, i3, f));
        }
    }

    public final void zzc(zzgw zzgw) {
        if (this.zzbjh != null) {
            this.zzaee.post(new zzph(this, zzgw));
        }
    }

    public final void zzb(Surface surface) {
        if (this.zzbjh != null) {
            this.zzaee.post(new zzpm(this, surface));
        }
    }
}
