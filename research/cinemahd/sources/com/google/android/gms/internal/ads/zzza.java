package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.List;

public final class zzza implements zzyz {
    public final List<String> zzd(List<String> list) {
        return list == null ? Collections.emptyList() : list;
    }
}
