package com.google.android.gms.internal.ads;

import android.os.Bundle;

final class zzcrs implements zzcty<Bundle> {
    private final String zzdke;
    private final String zzgft;
    private final Bundle zzgfu;

    private zzcrs(String str, String str2, Bundle bundle) {
        this.zzdke = str;
        this.zzgft = str2;
        this.zzgfu = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        bundle.putString("consent_string", this.zzdke);
        bundle.putString("fc_consent", this.zzgft);
        bundle.putBundle("iab_consent_info", this.zzgfu);
    }
}
