package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbpy implements zzdxg<zzbpw> {
    private final zzdxp<Set<zzbsu<zzbqb>>> zzfeo;

    private zzbpy(zzdxp<Set<zzbsu<zzbqb>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbpy zzk(zzdxp<Set<zzbsu<zzbqb>>> zzdxp) {
        return new zzbpy(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbpw(this.zzfeo.get());
    }
}
