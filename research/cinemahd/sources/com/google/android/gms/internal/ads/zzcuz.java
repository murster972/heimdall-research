package com.google.android.gms.internal.ads;

import android.content.Context;
import org.json.JSONObject;

public final class zzcuz implements zzcub<zzcuw> {
    private final String packageName;
    private final zzdhd zzfov;
    private final zzaqr zzghs;
    private final Context zzup;

    public zzcuz(zzaqr zzaqr, Context context, String str, zzdhd zzdhd) {
        this.zzghs = zzaqr;
        this.zzup = context;
        this.packageName = str;
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcuw> zzanc() {
        return this.zzfov.zzd(new zzcuy(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcuw zzans() throws Exception {
        JSONObject jSONObject = new JSONObject();
        zzaqr zzaqr = this.zzghs;
        if (zzaqr != null) {
            zzaqr.zza(this.zzup, this.packageName, jSONObject);
        }
        return new zzcuw(jSONObject);
    }
}
