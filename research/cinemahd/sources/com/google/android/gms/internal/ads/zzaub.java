package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaub implements zzaul {
    private final String zzcyz;

    zzaub(String str) {
        this.zzcyz = str;
    }

    public final void zza(zzbfq zzbfq) {
        zzbfq.endAdUnitExposure(this.zzcyz);
    }
}
