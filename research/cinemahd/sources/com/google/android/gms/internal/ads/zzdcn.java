package com.google.android.gms.internal.ads;

final /* synthetic */ class zzdcn implements Runnable {
    private final zzdcj zzgqk;
    private final zzdca zzgql;

    zzdcn(zzdcj zzdcj, zzdca zzdca) {
        this.zzgqk = zzdcj;
        this.zzgql = zzdca;
    }

    public final void run() {
        zzdcj zzdcj = this.zzgqk;
        zzdcj.zzgqd.zzgqb.zzb(this.zzgql);
    }
}
