package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import java.util.List;

public final class zzcfl implements zzdxg<zzdhe<zzaqk>> {
    private final zzdxp<zzavu> zzemi;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<zzdcr> zzfet;
    private final zzdxp<String> zzfrr;
    private final zzdxp<zzdhe<Bundle>> zzfuo;
    private final zzdxp<ApplicationInfo> zzfup;
    private final zzdxp<List<String>> zzfuq;
    private final zzdxp<PackageInfo> zzfur;
    private final zzdxp<zzdhe<String>> zzfus;
    private final zzdxp<String> zzfut;

    private zzcfl(zzdxp<zzdcr> zzdxp, zzdxp<zzdhe<Bundle>> zzdxp2, zzdxp<zzazb> zzdxp3, zzdxp<ApplicationInfo> zzdxp4, zzdxp<String> zzdxp5, zzdxp<List<String>> zzdxp6, zzdxp<PackageInfo> zzdxp7, zzdxp<zzdhe<String>> zzdxp8, zzdxp<zzavu> zzdxp9, zzdxp<String> zzdxp10) {
        this.zzfet = zzdxp;
        this.zzfuo = zzdxp2;
        this.zzfdb = zzdxp3;
        this.zzfup = zzdxp4;
        this.zzfrr = zzdxp5;
        this.zzfuq = zzdxp6;
        this.zzfur = zzdxp7;
        this.zzfus = zzdxp8;
        this.zzemi = zzdxp9;
        this.zzfut = zzdxp10;
    }

    public static zzcfl zza(zzdxp<zzdcr> zzdxp, zzdxp<zzdhe<Bundle>> zzdxp2, zzdxp<zzazb> zzdxp3, zzdxp<ApplicationInfo> zzdxp4, zzdxp<String> zzdxp5, zzdxp<List<String>> zzdxp6, zzdxp<PackageInfo> zzdxp7, zzdxp<zzdhe<String>> zzdxp8, zzdxp<zzavu> zzdxp9, zzdxp<String> zzdxp10) {
        return new zzcfl(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6, zzdxp7, zzdxp8, zzdxp9, zzdxp10);
    }

    public final /* synthetic */ Object get() {
        zzdhe zzdhe = this.zzfuo.get();
        zzdhe zzdhe2 = this.zzfus.get();
        return (zzdhe) zzdxm.zza(this.zzfet.get().zza(zzdco.REQUEST_PARCEL, (zzdhe<?>[]) new zzdhe[]{zzdhe, zzdhe2}).zzb(new zzcfm(zzdhe, this.zzfdb.get(), this.zzfup.get(), this.zzfrr.get(), this.zzfuq.get(), this.zzfur.get(), zzdhe2, this.zzemi.get(), this.zzfut.get())).zzaqg(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
