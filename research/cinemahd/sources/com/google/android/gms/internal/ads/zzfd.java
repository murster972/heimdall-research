package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.util.concurrent.Callable;

public final class zzfd implements Callable {
    private final zzei zzuv;
    private final zzbs.zza.zzb zzzt;

    public zzfd(zzei zzei, zzbs.zza.zzb zzb) {
        this.zzuv = zzei;
        this.zzzt = zzb;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzcp */
    public final Void call() throws Exception {
        if (this.zzuv.zzcg() != null) {
            this.zzuv.zzcg().get();
        }
        zzbs.zza zzcf = this.zzuv.zzcf();
        if (zzcf == null) {
            return null;
        }
        try {
            synchronized (this.zzzt) {
                zzbs.zza.zzb zzb = this.zzzt;
                byte[] byteArray = zzcf.toByteArray();
                zzb.zza(byteArray, 0, byteArray.length, zzdrg.zzazi());
            }
            return null;
        } catch (zzdse unused) {
            return null;
        }
    }
}
