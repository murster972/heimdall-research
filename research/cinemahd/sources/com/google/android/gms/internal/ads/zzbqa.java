package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.reward.AdMetadataListener;
import java.util.Set;

public final class zzbqa extends zzbrl<AdMetadataListener> implements zzaew {
    private Bundle zzfht = new Bundle();

    public zzbqa(Set<zzbsu<AdMetadataListener>> set) {
        super(set);
    }

    public final synchronized Bundle getAdMetadata() {
        return new Bundle(this.zzfht);
    }

    public final synchronized void zza(String str, Bundle bundle) {
        this.zzfht.putAll(bundle);
        zza(zzbqd.zzfhp);
    }
}
