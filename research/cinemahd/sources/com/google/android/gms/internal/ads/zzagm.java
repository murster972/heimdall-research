package com.google.android.gms.internal.ads;

final class zzagm implements Runnable {
    private final /* synthetic */ zzagh zzcyb;

    zzagm(zzagh zzagh) {
        this.zzcyb = zzagh;
    }

    public final void run() {
        this.zzcyb.disconnect();
    }
}
