package com.google.android.gms.internal.ads;

import android.os.Build;
import android.os.ConditionVariable;
import com.google.android.gms.internal.ads.zzbm;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class zzde {
    /* access modifiers changed from: private */
    public static final ConditionVariable zzuw = new ConditionVariable();
    protected static volatile zzsr zzux = null;
    private static volatile Random zzuz = null;
    /* access modifiers changed from: private */
    public zzei zzuv;
    protected volatile Boolean zzuy;

    public zzde(zzei zzei) {
        this.zzuv = zzei;
        zzei.zzbx().execute(new zzdh(this));
    }

    public static int zzbr() {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                return ThreadLocalRandom.current().nextInt();
            }
            return zzbs().nextInt();
        } catch (RuntimeException unused) {
            return zzbs().nextInt();
        }
    }

    private static Random zzbs() {
        if (zzuz == null) {
            synchronized (zzde.class) {
                if (zzuz == null) {
                    zzuz = new Random();
                }
            }
        }
        return zzuz;
    }

    public final void zza(int i, int i2, long j) {
        zza(i, i2, j, (String) null, (Exception) null);
    }

    public final void zza(int i, int i2, long j, String str) {
        zza(i, -1, j, str, (Exception) null);
    }

    public final void zza(int i, int i2, long j, String str, Exception exc) {
        try {
            zzuw.block();
            if (this.zzuy.booleanValue() && zzux != null) {
                zzbm.zza.C0033zza zzc = zzbm.zza.zzs().zzi(this.zzuv.zzup.getPackageName()).zzc(j);
                if (str != null) {
                    zzc.zzl(str);
                }
                if (exc != null) {
                    StringWriter stringWriter = new StringWriter();
                    zzdpt.zza((Throwable) exc, new PrintWriter(stringWriter));
                    zzc.zzj(stringWriter.toString()).zzk(exc.getClass().getName());
                }
                zzsv zzf = zzux.zzf(((zzbm.zza) ((zzdrt) zzc.zzbaf())).toByteArray());
                zzf.zzbr(i);
                if (i2 != -1) {
                    zzf.zzbq(i2);
                }
                zzf.zzdn();
            }
        } catch (Exception unused) {
        }
    }
}
