package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzi;

final /* synthetic */ class zzbfh implements zzden {
    private final Context zzcri;
    private final String zzdbl;
    private final zzbey zzefd;
    private final boolean zzefe;
    private final boolean zzeff;
    private final zzdq zzefg;
    private final zzazb zzefh;
    private final zzaae zzefi;
    private final zzi zzefj;
    private final zza zzefk;
    private final zzsm zzefl;
    private final zzro zzefm;
    private final boolean zzefn;

    zzbfh(Context context, zzbey zzbey, String str, boolean z, boolean z2, zzdq zzdq, zzazb zzazb, zzaae zzaae, zzi zzi, zza zza, zzsm zzsm, zzro zzro, boolean z3) {
        this.zzcri = context;
        this.zzefd = zzbey;
        this.zzdbl = str;
        this.zzefe = z;
        this.zzeff = z2;
        this.zzefg = zzdq;
        this.zzefh = zzazb;
        this.zzefi = zzaae;
        this.zzefj = zzi;
        this.zzefk = zza;
        this.zzefl = zzsm;
        this.zzefm = zzro;
        this.zzefn = z3;
    }

    public final Object get() {
        Context context = this.zzcri;
        zzbey zzbey = this.zzefd;
        String str = this.zzdbl;
        boolean z = this.zzefe;
        boolean z2 = this.zzeff;
        zzdq zzdq = this.zzefg;
        zzazb zzazb = this.zzefh;
        zzaae zzaae = this.zzefi;
        zzi zzi = this.zzefj;
        zza zza = this.zzefk;
        zzsm zzsm = this.zzefl;
        zzro zzro = this.zzefm;
        boolean z3 = this.zzefn;
        zzbfb zzbfb = new zzbfb();
        zzro zzro2 = zzro;
        zzbfg zzbfg = new zzbfg(new zzbez(context), zzbfb, zzbey, str, z, z2, zzdq, zzazb, zzaae, zzi, zza, zzsm, zzro2, z3);
        zzbdu zzbdu = new zzbdu(zzbfg);
        zzbfg.setWebChromeClient(new zzbda(zzbdu));
        zzbfb.zza((zzbdi) zzbdu, z2);
        return zzbdu;
    }
}
