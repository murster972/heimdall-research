package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbjh implements zzdxg<zzbiy> {
    private final zzdxp<zzpn> zzfct;
    private final zzdxp<zzakh> zzfcu;
    private final zzdxp<Executor> zzfcv;

    private zzbjh(zzdxp<zzpn> zzdxp, zzdxp<zzakh> zzdxp2, zzdxp<Executor> zzdxp3) {
        this.zzfct = zzdxp;
        this.zzfcu = zzdxp2;
        this.zzfcv = zzdxp3;
    }

    public static zzbjh zza(zzdxp<zzpn> zzdxp, zzdxp<zzakh> zzdxp2, zzdxp<Executor> zzdxp3) {
        return new zzbjh(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return (zzbiy) zzdxm.zza(new zzbiy(this.zzfct.get().zzkm(), this.zzfcu.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
