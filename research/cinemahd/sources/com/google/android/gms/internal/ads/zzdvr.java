package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

final class zzdvr implements Cloneable {
    private Object value;
    private zzdvp<?, ?> zzhtn;
    private List<zzdvv> zzhto = new ArrayList();

    zzdvr() {
    }

    private final byte[] toByteArray() throws IOException {
        byte[] bArr = new byte[zzoi()];
        zza(zzdvo.zzaa(bArr));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzbcs */
    public final zzdvr clone() {
        zzdvr zzdvr = new zzdvr();
        try {
            zzdvr.zzhtn = this.zzhtn;
            if (this.zzhto == null) {
                zzdvr.zzhto = null;
            } else {
                zzdvr.zzhto.addAll(this.zzhto);
            }
            if (this.value != null) {
                if (this.value instanceof zzdvt) {
                    zzdvr.value = (zzdvt) ((zzdvt) this.value).clone();
                } else if (this.value instanceof byte[]) {
                    zzdvr.value = ((byte[]) this.value).clone();
                } else {
                    int i = 0;
                    if (this.value instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.value;
                        byte[][] bArr2 = new byte[bArr.length][];
                        zzdvr.value = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.value instanceof boolean[]) {
                        zzdvr.value = ((boolean[]) this.value).clone();
                    } else if (this.value instanceof int[]) {
                        zzdvr.value = ((int[]) this.value).clone();
                    } else if (this.value instanceof long[]) {
                        zzdvr.value = ((long[]) this.value).clone();
                    } else if (this.value instanceof float[]) {
                        zzdvr.value = ((float[]) this.value).clone();
                    } else if (this.value instanceof double[]) {
                        zzdvr.value = ((double[]) this.value).clone();
                    } else if (this.value instanceof zzdvt[]) {
                        zzdvt[] zzdvtArr = (zzdvt[]) this.value;
                        zzdvt[] zzdvtArr2 = new zzdvt[zzdvtArr.length];
                        zzdvr.value = zzdvtArr2;
                        while (i < zzdvtArr.length) {
                            zzdvtArr2[i] = (zzdvt) zzdvtArr[i].clone();
                            i++;
                        }
                    }
                }
            }
            return zzdvr;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        List<zzdvv> list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzdvr)) {
            return false;
        }
        zzdvr zzdvr = (zzdvr) obj;
        if (this.value == null || zzdvr.value == null) {
            List<zzdvv> list2 = this.zzhto;
            if (list2 != null && (list = zzdvr.zzhto) != null) {
                return list2.equals(list);
            }
            try {
                return Arrays.equals(toByteArray(), zzdvr.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            zzdvp<?, ?> zzdvp = this.zzhtn;
            if (zzdvp != zzdvr.zzhtn) {
                return false;
            }
            if (!zzdvp.zzgxv.isArray()) {
                return this.value.equals(zzdvr.value);
            }
            Object obj2 = this.value;
            if (obj2 instanceof byte[]) {
                return Arrays.equals((byte[]) obj2, (byte[]) zzdvr.value);
            }
            if (obj2 instanceof int[]) {
                return Arrays.equals((int[]) obj2, (int[]) zzdvr.value);
            }
            if (obj2 instanceof long[]) {
                return Arrays.equals((long[]) obj2, (long[]) zzdvr.value);
            }
            if (obj2 instanceof float[]) {
                return Arrays.equals((float[]) obj2, (float[]) zzdvr.value);
            }
            if (obj2 instanceof double[]) {
                return Arrays.equals((double[]) obj2, (double[]) zzdvr.value);
            }
            if (obj2 instanceof boolean[]) {
                return Arrays.equals((boolean[]) obj2, (boolean[]) zzdvr.value);
            }
            return Arrays.deepEquals((Object[]) obj2, (Object[]) zzdvr.value);
        }
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(toByteArray()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdvo zzdvo) throws IOException {
        if (this.value == null) {
            Iterator<zzdvv> it2 = this.zzhto.iterator();
            if (it2.hasNext()) {
                zzdvv next = it2.next();
                throw new NoSuchMethodError();
            }
            return;
        }
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final int zzoi() {
        if (this.value == null) {
            Iterator<zzdvv> it2 = this.zzhto.iterator();
            if (!it2.hasNext()) {
                return 0;
            }
            zzdvv next = it2.next();
            throw new NoSuchMethodError();
        }
        throw new NoSuchMethodError();
    }
}
