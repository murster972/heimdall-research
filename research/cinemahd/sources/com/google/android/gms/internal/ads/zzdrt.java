package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import com.google.android.gms.internal.ads.zzdrt.zzb;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class zzdrt<MessageType extends zzdrt<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzdqa<MessageType, BuilderType> {
    private static Map<Object, zzdrt<?, ?>> zzhmm = new ConcurrentHashMap();
    protected zzdur zzhmk = zzdur.zzbcf();
    private int zzhml = -1;

    public static class zza<T extends zzdrt<T, ?>> extends zzdqb<T> {
        private final T zzhmo;

        public zza(T t) {
            this.zzhmo = t;
        }
    }

    static final class zzc implements zzdro<zzc> {
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }

        public final zzdtd zza(zzdtd zzdtd, zzdte zzdte) {
            throw new NoSuchMethodError();
        }

        public final int zzae() {
            throw new NoSuchMethodError();
        }

        public final zzdvf zzazo() {
            throw new NoSuchMethodError();
        }

        public final zzdvm zzazp() {
            throw new NoSuchMethodError();
        }

        public final boolean zzazq() {
            throw new NoSuchMethodError();
        }

        public final boolean zzazr() {
            throw new NoSuchMethodError();
        }

        public final zzdtj zza(zzdtj zzdtj, zzdtj zzdtj2) {
            throw new NoSuchMethodError();
        }
    }

    public static abstract class zzd<MessageType extends zzd<MessageType, BuilderType>, BuilderType> extends zzdrt<MessageType, BuilderType> implements zzdtg {
        protected zzdrm<zzc> zzhmr = zzdrm.zzazm();

        /* access modifiers changed from: package-private */
        public final zzdrm<zzc> zzbag() {
            if (this.zzhmr.isImmutable()) {
                this.zzhmr = (zzdrm) this.zzhmr.clone();
            }
            return this.zzhmr;
        }
    }

    /* 'enum' modifier removed */
    public static final class zze {
        public static final int zzhms = 1;
        public static final int zzhmt = 2;
        public static final int zzhmu = 3;
        public static final int zzhmv = 4;
        public static final int zzhmw = 5;
        public static final int zzhmx = 6;
        public static final int zzhmy = 7;
        private static final /* synthetic */ int[] zzhmz = {zzhms, zzhmt, zzhmu, zzhmv, zzhmw, zzhmx, zzhmy};
        public static final int zzhna = 1;
        public static final int zzhnb = 2;
        private static final /* synthetic */ int[] zzhnc = {zzhna, zzhnb};
        public static final int zzhnd = 1;
        public static final int zzhne = 2;
        private static final /* synthetic */ int[] zzhnf = {zzhnd, zzhne};

        public static int[] zzbah() {
            return (int[]) zzhmz.clone();
        }
    }

    public static class zzf<ContainingType extends zzdte, Type> extends zzdrh<ContainingType, Type> {
    }

    protected static <T extends zzdrt<?, ?>> void zza(Class<T> cls, T t) {
        zzhmm.put(cls, t);
    }

    protected static zzdrz zzazv() {
        return zzdrw.zzbai();
    }

    protected static <E> zzdsb<E> zzazw() {
        return zzdts.zzbbp();
    }

    static <T extends zzdrt<?, ?>> T zzd(Class<T> cls) {
        T t = (zzdrt) zzhmm.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzdrt) zzhmm.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzdrt) ((zzdrt) zzduy.zzj(cls)).zza(zze.zzhmx, (Object) null, (Object) null);
            if (t != null) {
                zzhmm.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzdrt) zza(zze.zzhmx, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return zzdtp.zzbbm().zzba(this).equals(this, (zzdrt) obj);
    }

    public int hashCode() {
        int i = this.zzhhk;
        if (i != 0) {
            return i;
        }
        this.zzhhk = zzdtp.zzbbm().zzba(this).hashCode(this);
        return this.zzhhk;
    }

    public final boolean isInitialized() {
        return zza(this, Boolean.TRUE.booleanValue());
    }

    public String toString() {
        return zzdtf.zza(this, super.toString());
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    /* access modifiers changed from: package-private */
    public final int zzaxl() {
        return this.zzhml;
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzdrt<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> BuilderType zzazt() {
        return (zzb) zza(zze.zzhmw, (Object) null, (Object) null);
    }

    public final int zzazu() {
        if (this.zzhml == -1) {
            this.zzhml = zzdtp.zzbbm().zzba(this).zzax(this);
        }
        return this.zzhml;
    }

    public final /* synthetic */ zzdtd zzazx() {
        zzb zzb2 = (zzb) zza(zze.zzhmw, (Object) null, (Object) null);
        zzb2.zza(this);
        return zzb2;
    }

    public final /* synthetic */ zzdtd zzazy() {
        return (zzb) zza(zze.zzhmw, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzdte zzazz() {
        return (zzdrt) zza(zze.zzhmx, (Object) null, (Object) null);
    }

    public final void zzb(zzdrb zzdrb) throws IOException {
        zzdtp.zzbbm().zzba(this).zza(this, zzdrf.zza(zzdrb));
    }

    /* access modifiers changed from: package-private */
    public final void zzfa(int i) {
        this.zzhml = i;
    }

    protected static Object zza(zzdte zzdte, String str, Object[] objArr) {
        return new zzdtr(zzdte, str, objArr);
    }

    public static abstract class zzb<MessageType extends zzdrt<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzdpz<MessageType, BuilderType> {
        private final MessageType zzhmo;
        protected MessageType zzhmp;
        protected boolean zzhmq = false;

        protected zzb(MessageType messagetype) {
            this.zzhmo = messagetype;
            this.zzhmp = (zzdrt) messagetype.zza(zze.zzhmv, (Object) null, (Object) null);
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzdtp.zzbbm().zzba(messagetype).zzf(messagetype, messagetype2);
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zzb zzb = (zzb) this.zzhmo.zza(zze.zzhmw, (Object) null, (Object) null);
            zzb.zza((zzdrt) zzbae());
            return zzb;
        }

        public final boolean isInitialized() {
            return zzdrt.zza(this.zzhmp, false);
        }

        public final /* synthetic */ zzdpz zzaxj() {
            return (zzb) clone();
        }

        public final /* synthetic */ zzdte zzazz() {
            return this.zzhmo;
        }

        /* renamed from: zzb */
        public final BuilderType zza(MessageType messagetype) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            zza(this.zzhmp, messagetype);
            return this;
        }

        /* access modifiers changed from: protected */
        public void zzbab() {
            MessageType messagetype = (zzdrt) this.zzhmp.zza(zze.zzhmv, (Object) null, (Object) null);
            zza(messagetype, this.zzhmp);
            this.zzhmp = messagetype;
        }

        /* renamed from: zzbac */
        public MessageType zzbae() {
            if (this.zzhmq) {
                return this.zzhmp;
            }
            MessageType messagetype = this.zzhmp;
            zzdtp.zzbbm().zzba(messagetype).zzan(messagetype);
            this.zzhmq = true;
            return this.zzhmp;
        }

        /* renamed from: zzbad */
        public final MessageType zzbaf() {
            MessageType messagetype = (zzdrt) zzbae();
            if (messagetype.isInitialized()) {
                return messagetype;
            }
            throw new zzdup(messagetype);
        }

        public final /* synthetic */ zzdpz zza(byte[] bArr, int i, int i2, zzdrg zzdrg) throws zzdse {
            return zzb(bArr, 0, i2, zzdrg);
        }

        private final BuilderType zzb(byte[] bArr, int i, int i2, zzdrg zzdrg) throws zzdse {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            try {
                zzdtp.zzbbm().zzba(this.zzhmp).zza(this.zzhmp, bArr, 0, i2 + 0, new zzdqf(zzdrg));
                return this;
            } catch (zzdse e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw zzdse.zzbaj();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: zzb */
        public final BuilderType zza(zzdqw zzdqw, zzdrg zzdrg) throws IOException {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            try {
                zzdtp.zzbbm().zzba(this.zzhmp).zza(this.zzhmp, zzdqz.zza(zzdqw), zzdrg);
                return this;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw ((IOException) e.getCause());
                }
                throw e;
            }
        }
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    private static <T extends zzdrt<T, ?>> T zzb(T t, zzdqk zzdqk, zzdrg zzdrg) throws zzdse {
        T zza2;
        try {
            zzdqw zzaxv = zzdqk.zzaxv();
            zza2 = zza(t, zzaxv, zzdrg);
            zzaxv.zzfh(0);
            return zza2;
        } catch (zzdse e) {
            throw e.zzl(zza2);
        } catch (zzdse e2) {
            throw e2;
        }
    }

    protected static final <T extends zzdrt<T, ?>> boolean zza(T t, boolean z) {
        byte byteValue = ((Byte) t.zza(zze.zzhms, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzaz = zzdtp.zzbbm().zzba(t).zzaz(t);
        if (z) {
            t.zza(zze.zzhmt, (Object) zzaz ? t : null, (Object) null);
        }
        return zzaz;
    }

    protected static zzdrz zza(zzdrz zzdrz) {
        int size = zzdrz.size();
        return zzdrz.zzgk(size == 0 ? 10 : size << 1);
    }

    protected static <E> zzdsb<E> zza(zzdsb<E> zzdsb) {
        int size = zzdsb.size();
        return zzdsb.zzfd(size == 0 ? 10 : size << 1);
    }

    private static <T extends zzdrt<T, ?>> T zza(T t, zzdqw zzdqw, zzdrg zzdrg) throws zzdse {
        T t2 = (zzdrt) t.zza(zze.zzhmv, (Object) null, (Object) null);
        try {
            zzdua zzba = zzdtp.zzbbm().zzba(t2);
            zzba.zza(t2, zzdqz.zza(zzdqw), zzdrg);
            zzba.zzan(t2);
            return t2;
        } catch (IOException e) {
            if (e.getCause() instanceof zzdse) {
                throw ((zzdse) e.getCause());
            }
            throw new zzdse(e.getMessage()).zzl(t2);
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof zzdse) {
                throw ((zzdse) e2.getCause());
            }
            throw e2;
        }
    }

    private static <T extends zzdrt<T, ?>> T zza(T t, byte[] bArr, int i, int i2, zzdrg zzdrg) throws zzdse {
        T t2 = (zzdrt) t.zza(zze.zzhmv, (Object) null, (Object) null);
        try {
            zzdua zzba = zzdtp.zzbbm().zzba(t2);
            zzba.zza(t2, bArr, 0, i2, new zzdqf(zzdrg));
            zzba.zzan(t2);
            if (t2.zzhhk == 0) {
                return t2;
            }
            throw new RuntimeException();
        } catch (IOException e) {
            if (e.getCause() instanceof zzdse) {
                throw ((zzdse) e.getCause());
            }
            throw new zzdse(e.getMessage()).zzl(t2);
        } catch (IndexOutOfBoundsException unused) {
            throw zzdse.zzbaj().zzl(t2);
        }
    }

    private static <T extends zzdrt<T, ?>> T zza(T t) throws zzdse {
        if (t == null || t.isInitialized()) {
            return t;
        }
        throw new zzdse(new zzdup(t).getMessage()).zzl(t);
    }

    protected static <T extends zzdrt<T, ?>> T zza(T t, zzdqk zzdqk) throws zzdse {
        return zza(zza(zzb(t, zzdqk, zzdrg.zzazh())));
    }

    protected static <T extends zzdrt<T, ?>> T zza(T t, zzdqk zzdqk, zzdrg zzdrg) throws zzdse {
        return zza(zzb(t, zzdqk, zzdrg));
    }

    protected static <T extends zzdrt<T, ?>> T zza(T t, byte[] bArr) throws zzdse {
        return zza(zza(t, bArr, 0, bArr.length, zzdrg.zzazh()));
    }

    protected static <T extends zzdrt<T, ?>> T zza(T t, byte[] bArr, zzdrg zzdrg) throws zzdse {
        return zza(zza(t, bArr, 0, bArr.length, zzdrg));
    }
}
