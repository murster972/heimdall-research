package com.google.android.gms.internal.cast;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public final class zzai extends zza implements zzah {
    zzai(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.media.internal.IFetchBitmapTask");
    }

    public final Bitmap zzb(Uri uri) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (Parcelable) uri);
        Parcel zza2 = zza(1, zza);
        Bitmap bitmap = (Bitmap) zzc.zza(zza2, Bitmap.CREATOR);
        zza2.recycle();
        return bitmap;
    }
}
