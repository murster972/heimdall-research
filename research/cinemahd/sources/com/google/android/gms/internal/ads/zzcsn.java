package com.google.android.gms.internal.ads;

import android.content.pm.PackageInfo;
import java.util.ArrayList;

public final class zzcsn implements zzcub<zzcsk> {
    private final PackageInfo zzdip;
    private final zzavu zzdrk;
    private final zzczu zzfgl;
    private final zzdhd zzfov;

    public zzcsn(zzdhd zzdhd, zzczu zzczu, PackageInfo packageInfo, zzavu zzavu) {
        this.zzfov = zzdhd;
        this.zzfgl = zzczu;
        this.zzdip = packageInfo;
        this.zzdrk = zzavu;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f5, code lost:
        if (r9 == 3) goto L_0x011a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zza(java.util.ArrayList r9, android.os.Bundle r10) {
        /*
            r8 = this;
            r0 = 3
            java.lang.String r1 = "native_version"
            r10.putInt(r1, r0)
            java.lang.String r1 = "native_templates"
            r10.putStringArrayList(r1, r9)
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            java.util.ArrayList<java.lang.String> r9 = r9.zzgmo
            java.lang.String r1 = "native_custom_templates"
            r10.putStringArrayList(r1, r9)
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r9 = com.google.android.gms.internal.ads.zzzn.zzcmc
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r9 = r1.zzd(r9)
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            java.lang.String r1 = "landscape"
            java.lang.String r2 = "portrait"
            java.lang.String r3 = "any"
            java.lang.String r4 = "unknown"
            r5 = 2
            r6 = 1
            if (r9 == 0) goto L_0x0061
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            com.google.android.gms.internal.ads.zzaby r9 = r9.zzddz
            int r9 = r9.versionCode
            if (r9 <= r0) goto L_0x0061
            java.lang.String r9 = "enable_native_media_orientation"
            r10.putBoolean(r9, r6)
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            com.google.android.gms.internal.ads.zzaby r9 = r9.zzddz
            int r9 = r9.zzbjx
            if (r9 == r6) goto L_0x0055
            if (r9 == r5) goto L_0x0053
            if (r9 == r0) goto L_0x0051
            r7 = 4
            if (r9 == r7) goto L_0x004e
            r9 = r4
            goto L_0x0056
        L_0x004e:
            java.lang.String r9 = "square"
            goto L_0x0056
        L_0x0051:
            r9 = r2
            goto L_0x0056
        L_0x0053:
            r9 = r1
            goto L_0x0056
        L_0x0055:
            r9 = r3
        L_0x0056:
            boolean r7 = r4.equals(r9)
            if (r7 != 0) goto L_0x0061
            java.lang.String r7 = "native_media_orientation"
            r10.putString(r7, r9)
        L_0x0061:
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            com.google.android.gms.internal.ads.zzaby r9 = r9.zzddz
            int r9 = r9.zzbjw
            if (r9 == 0) goto L_0x0071
            if (r9 == r6) goto L_0x006f
            if (r9 == r5) goto L_0x0072
            r1 = r4
            goto L_0x0072
        L_0x006f:
            r1 = r2
            goto L_0x0072
        L_0x0071:
            r1 = r3
        L_0x0072:
            boolean r9 = r4.equals(r1)
            if (r9 != 0) goto L_0x007d
            java.lang.String r9 = "native_image_orientation"
            r10.putString(r9, r1)
        L_0x007d:
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            com.google.android.gms.internal.ads.zzaby r9 = r9.zzddz
            boolean r9 = r9.zzbjy
            java.lang.String r1 = "native_multiple_images"
            r10.putBoolean(r1, r9)
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            com.google.android.gms.internal.ads.zzaby r9 = r9.zzddz
            boolean r9 = r9.zzbkb
            java.lang.String r1 = "use_custom_mute"
            r10.putBoolean(r1, r9)
            android.content.pm.PackageInfo r9 = r8.zzdip
            if (r9 != 0) goto L_0x0099
            r9 = 0
            goto L_0x009b
        L_0x0099:
            int r9 = r9.versionCode
        L_0x009b:
            com.google.android.gms.internal.ads.zzavu r1 = r8.zzdrk
            int r1 = r1.zzvz()
            if (r9 <= r1) goto L_0x00ad
            com.google.android.gms.internal.ads.zzavu r1 = r8.zzdrk
            r1.zzwf()
            com.google.android.gms.internal.ads.zzavu r1 = r8.zzdrk
            r1.zzcp(r9)
        L_0x00ad:
            com.google.android.gms.internal.ads.zzavu r9 = r8.zzdrk
            org.json.JSONObject r9 = r9.zzwe()
            if (r9 == 0) goto L_0x00c4
            com.google.android.gms.internal.ads.zzczu r1 = r8.zzfgl
            java.lang.String r1 = r1.zzgmm
            org.json.JSONArray r9 = r9.optJSONArray(r1)
            if (r9 == 0) goto L_0x00c4
            java.lang.String r9 = r9.toString()
            goto L_0x00c5
        L_0x00c4:
            r9 = 0
        L_0x00c5:
            boolean r1 = android.text.TextUtils.isEmpty(r9)
            if (r1 != 0) goto L_0x00d0
            java.lang.String r1 = "native_advanced_settings"
            r10.putString(r1, r9)
        L_0x00d0:
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            int r9 = r9.zzgdu
            if (r9 <= r6) goto L_0x00db
            java.lang.String r1 = "max_num_ads"
            r10.putInt(r1, r9)
        L_0x00db:
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            com.google.android.gms.internal.ads.zzagz r9 = r9.zzdkf
            if (r9 == 0) goto L_0x012d
            java.lang.String r1 = r9.zzcyj
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 == 0) goto L_0x0121
            int r1 = r9.versionCode
            java.lang.String r2 = "p"
            java.lang.String r3 = "l"
            if (r1 < r5) goto L_0x00f8
            int r9 = r9.zzbjx
            if (r9 == r5) goto L_0x011b
            if (r9 == r0) goto L_0x011a
            goto L_0x011b
        L_0x00f8:
            int r9 = r9.zzcyi
            if (r9 == r6) goto L_0x011b
            if (r9 == r5) goto L_0x011a
            r0 = 52
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Instream ad video aspect ratio "
            r1.append(r0)
            r1.append(r9)
            java.lang.String r9 = " is wrong."
            r1.append(r9)
            java.lang.String r9 = r1.toString()
            com.google.android.gms.internal.ads.zzayu.zzex(r9)
            goto L_0x011b
        L_0x011a:
            r3 = r2
        L_0x011b:
            java.lang.String r9 = "ia_var"
            r10.putString(r9, r3)
            goto L_0x0128
        L_0x0121:
            java.lang.String r9 = r9.zzcyj
            java.lang.String r0 = "ad_tag"
            r10.putString(r0, r9)
        L_0x0128:
            java.lang.String r9 = "instr"
            r10.putBoolean(r9, r6)
        L_0x012d:
            com.google.android.gms.internal.ads.zzczu r9 = r8.zzfgl
            com.google.android.gms.internal.ads.zzaea r9 = r9.zzaoo()
            if (r9 == 0) goto L_0x013a
            java.lang.String r9 = "has_delayed_banner_listener"
            r10.putBoolean(r9, r6)
        L_0x013a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcsn.zza(java.util.ArrayList, android.os.Bundle):void");
    }

    public final zzdhe<zzcsk> zzanc() {
        return this.zzfov.zzd(new zzcsm(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcsk zzanj() throws Exception {
        ArrayList<String> arrayList = this.zzfgl.zzgmn;
        if (arrayList == null) {
            return zzcsp.zzgga;
        }
        if (arrayList.isEmpty()) {
            return zzcso.zzgga;
        }
        return new zzcsr(this, arrayList);
    }
}
