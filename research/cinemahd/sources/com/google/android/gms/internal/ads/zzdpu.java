package com.google.android.gms.internal.ads;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class zzdpu extends WeakReference<Throwable> {
    private final int zzhhd;

    public zzdpu(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.zzhhd = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == zzdpu.class) {
            if (this == obj) {
                return true;
            }
            zzdpu zzdpu = (zzdpu) obj;
            return this.zzhhd == zzdpu.zzhhd && get() == zzdpu.get();
        }
    }

    public final int hashCode() {
        return this.zzhhd;
    }
}
