package com.google.android.gms.internal.ads;

import com.facebook.imageutils.JfifUtil;
import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import libcore.io.Memory;
import sun.misc.Unsafe;

final class zzduy {
    private static final Logger logger = Logger.getLogger(zzduy.class.getName());
    private static final Unsafe zzgvq = zzbcl();
    private static final Class<?> zzhho = zzdqd.zzaxo();
    private static final boolean zzhiu = zzbcm();
    private static final boolean zzhrj = zzm(Long.TYPE);
    private static final boolean zzhrk = zzm(Integer.TYPE);
    private static final zzc zzhrl;
    private static final boolean zzhrm = zzbcn();
    static final long zzhrn = ((long) zzk(byte[].class));
    private static final long zzhro;
    private static final long zzhrp;
    private static final long zzhrq;
    private static final long zzhrr;
    private static final long zzhrs;
    private static final long zzhrt;
    private static final long zzhru;
    private static final long zzhrv;
    private static final long zzhrw;
    private static final long zzhrx;
    private static final long zzhry;
    private static final long zzhrz;
    private static final long zzhsa;
    private static final int zzhsb = ((int) (zzhrn & 7));
    static final boolean zzhsc = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    static final class zza extends zzc {
        zza(Unsafe unsafe) {
            super(unsafe);
        }

        public final void zza(long j, byte b) {
            Memory.pokeByte(j, b);
        }

        public final void zze(Object obj, long j, byte b) {
            if (zzduy.zzhsc) {
                zzduy.zza(obj, j, b);
            } else {
                zzduy.zzb(obj, j, b);
            }
        }

        public final boolean zzm(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzs(obj, j);
            }
            return zzduy.zzt(obj, j);
        }

        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        public final byte zzy(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzq(obj, j);
            }
            return zzduy.zzr(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            if (zzduy.zzhsc) {
                zzduy.zzb(obj, j, z);
            } else {
                zzduy.zzc(obj, j, z);
            }
        }

        public final void zza(Object obj, long j, float f) {
            zzb(obj, j, Float.floatToIntBits(f));
        }

        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }

        public final void zza(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray(j2, bArr, (int) j, (int) j3);
        }
    }

    static final class zzb extends zzc {
        zzb(Unsafe unsafe) {
            super(unsafe);
        }

        public final void zza(long j, byte b) {
            Memory.pokeByte((int) (j & -1), b);
        }

        public final void zze(Object obj, long j, byte b) {
            if (zzduy.zzhsc) {
                zzduy.zza(obj, j, b);
            } else {
                zzduy.zzb(obj, j, b);
            }
        }

        public final boolean zzm(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzs(obj, j);
            }
            return zzduy.zzt(obj, j);
        }

        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        public final byte zzy(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzq(obj, j);
            }
            return zzduy.zzr(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            if (zzduy.zzhsc) {
                zzduy.zzb(obj, j, z);
            } else {
                zzduy.zzc(obj, j, z);
            }
        }

        public final void zza(Object obj, long j, float f) {
            zzb(obj, j, Float.floatToIntBits(f));
        }

        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }

        public final void zza(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray((int) (j2 & -1), bArr, (int) j, (int) j3);
        }
    }

    static abstract class zzc {
        Unsafe zzhsd;

        zzc(Unsafe unsafe) {
            this.zzhsd = unsafe;
        }

        public abstract void zza(long j, byte b);

        public abstract void zza(Object obj, long j, double d);

        public abstract void zza(Object obj, long j, float f);

        public final void zza(Object obj, long j, long j2) {
            this.zzhsd.putLong(obj, j, j2);
        }

        public abstract void zza(Object obj, long j, boolean z);

        public abstract void zza(byte[] bArr, long j, long j2, long j3);

        public final void zzb(Object obj, long j, int i) {
            this.zzhsd.putInt(obj, j, i);
        }

        public abstract void zze(Object obj, long j, byte b);

        public final int zzk(Object obj, long j) {
            return this.zzhsd.getInt(obj, j);
        }

        public final long zzl(Object obj, long j) {
            return this.zzhsd.getLong(obj, j);
        }

        public abstract boolean zzm(Object obj, long j);

        public abstract float zzn(Object obj, long j);

        public abstract double zzo(Object obj, long j);

        public abstract byte zzy(Object obj, long j);
    }

    static final class zzd extends zzc {
        zzd(Unsafe unsafe) {
            super(unsafe);
        }

        public final void zza(long j, byte b) {
            this.zzhsd.putByte(j, b);
        }

        public final void zze(Object obj, long j, byte b) {
            this.zzhsd.putByte(obj, j, b);
        }

        public final boolean zzm(Object obj, long j) {
            return this.zzhsd.getBoolean(obj, j);
        }

        public final float zzn(Object obj, long j) {
            return this.zzhsd.getFloat(obj, j);
        }

        public final double zzo(Object obj, long j) {
            return this.zzhsd.getDouble(obj, j);
        }

        public final byte zzy(Object obj, long j) {
            return this.zzhsd.getByte(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            this.zzhsd.putBoolean(obj, j, z);
        }

        public final void zza(Object obj, long j, float f) {
            this.zzhsd.putFloat(obj, j, f);
        }

        public final void zza(Object obj, long j, double d) {
            this.zzhsd.putDouble(obj, j, d);
        }

        public final void zza(byte[] bArr, long j, long j2, long j3) {
            this.zzhsd.copyMemory(bArr, zzduy.zzhrn + j, (Object) null, j2, j3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00d0, code lost:
        r1 = zzhrl;
     */
    static {
        /*
            java.lang.Class<java.lang.Object[]> r0 = java.lang.Object[].class
            java.lang.Class<double[]> r1 = double[].class
            java.lang.Class<float[]> r2 = float[].class
            java.lang.Class<long[]> r3 = long[].class
            java.lang.Class<int[]> r4 = int[].class
            java.lang.Class<boolean[]> r5 = boolean[].class
            java.lang.Class<com.google.android.gms.internal.ads.zzduy> r6 = com.google.android.gms.internal.ads.zzduy.class
            java.lang.String r6 = r6.getName()
            java.util.logging.Logger r6 = java.util.logging.Logger.getLogger(r6)
            logger = r6
            sun.misc.Unsafe r6 = zzbcl()
            zzgvq = r6
            java.lang.Class r6 = com.google.android.gms.internal.ads.zzdqd.zzaxo()
            zzhho = r6
            java.lang.Class r6 = java.lang.Long.TYPE
            boolean r6 = zzm(r6)
            zzhrj = r6
            java.lang.Class r6 = java.lang.Integer.TYPE
            boolean r6 = zzm(r6)
            zzhrk = r6
            sun.misc.Unsafe r6 = zzgvq
            r7 = 0
            if (r6 != 0) goto L_0x003a
            goto L_0x005f
        L_0x003a:
            boolean r6 = com.google.android.gms.internal.ads.zzdqd.zzaxn()
            if (r6 == 0) goto L_0x0058
            boolean r6 = zzhrj
            if (r6 == 0) goto L_0x004c
            com.google.android.gms.internal.ads.zzduy$zza r7 = new com.google.android.gms.internal.ads.zzduy$zza
            sun.misc.Unsafe r6 = zzgvq
            r7.<init>(r6)
            goto L_0x005f
        L_0x004c:
            boolean r6 = zzhrk
            if (r6 == 0) goto L_0x005f
            com.google.android.gms.internal.ads.zzduy$zzb r7 = new com.google.android.gms.internal.ads.zzduy$zzb
            sun.misc.Unsafe r6 = zzgvq
            r7.<init>(r6)
            goto L_0x005f
        L_0x0058:
            com.google.android.gms.internal.ads.zzduy$zzd r7 = new com.google.android.gms.internal.ads.zzduy$zzd
            sun.misc.Unsafe r6 = zzgvq
            r7.<init>(r6)
        L_0x005f:
            zzhrl = r7
            boolean r6 = zzbcn()
            zzhrm = r6
            boolean r6 = zzbcm()
            zzhiu = r6
            java.lang.Class<byte[]> r6 = byte[].class
            int r6 = zzk(r6)
            long r6 = (long) r6
            zzhrn = r6
            int r6 = zzk(r5)
            long r6 = (long) r6
            zzhro = r6
            int r5 = zzl(r5)
            long r5 = (long) r5
            zzhrp = r5
            int r5 = zzk(r4)
            long r5 = (long) r5
            zzhrq = r5
            int r4 = zzl(r4)
            long r4 = (long) r4
            zzhrr = r4
            int r4 = zzk(r3)
            long r4 = (long) r4
            zzhrs = r4
            int r3 = zzl(r3)
            long r3 = (long) r3
            zzhrt = r3
            int r3 = zzk(r2)
            long r3 = (long) r3
            zzhru = r3
            int r2 = zzl(r2)
            long r2 = (long) r2
            zzhrv = r2
            int r2 = zzk(r1)
            long r2 = (long) r2
            zzhrw = r2
            int r1 = zzl(r1)
            long r1 = (long) r1
            zzhrx = r1
            int r1 = zzk(r0)
            long r1 = (long) r1
            zzhry = r1
            int r0 = zzl(r0)
            long r0 = (long) r0
            zzhrz = r0
            java.lang.reflect.Field r0 = zzbco()
            if (r0 == 0) goto L_0x00dc
            com.google.android.gms.internal.ads.zzduy$zzc r1 = zzhrl
            if (r1 != 0) goto L_0x00d5
            goto L_0x00dc
        L_0x00d5:
            sun.misc.Unsafe r1 = r1.zzhsd
            long r0 = r1.objectFieldOffset(r0)
            goto L_0x00de
        L_0x00dc:
            r0 = -1
        L_0x00de:
            zzhsa = r0
            long r0 = zzhrn
            r2 = 7
            long r0 = r0 & r2
            int r1 = (int) r0
            zzhsb = r1
            java.nio.ByteOrder r0 = java.nio.ByteOrder.nativeOrder()
            java.nio.ByteOrder r1 = java.nio.ByteOrder.BIG_ENDIAN
            if (r0 != r1) goto L_0x00f2
            r0 = 1
            goto L_0x00f3
        L_0x00f2:
            r0 = 0
        L_0x00f3:
            zzhsc = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzduy.<clinit>():void");
    }

    private zzduy() {
    }

    static void zza(Object obj, long j, long j2) {
        zzhrl.zza(obj, j, j2);
    }

    static void zzb(Object obj, long j, int i) {
        zzhrl.zzb(obj, j, i);
    }

    static boolean zzbcj() {
        return zzhiu;
    }

    static boolean zzbck() {
        return zzhrm;
    }

    static Unsafe zzbcl() {
        try {
            return (Unsafe) AccessController.doPrivileged(new zzdux());
        } catch (Throwable unused) {
            return null;
        }
    }

    private static boolean zzbcm() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = zzgvq;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls2.getMethod("arrayBaseOffset", new Class[]{Class.class});
            cls2.getMethod("arrayIndexScale", new Class[]{Class.class});
            cls2.getMethod("getInt", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putInt", new Class[]{cls, Long.TYPE, Integer.TYPE});
            cls2.getMethod("getLong", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putLong", new Class[]{cls, Long.TYPE, Long.TYPE});
            cls2.getMethod("getObject", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putObject", new Class[]{cls, Long.TYPE, cls});
            if (zzdqd.zzaxn()) {
                return true;
            }
            cls2.getMethod("getByte", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putByte", new Class[]{cls, Long.TYPE, Byte.TYPE});
            cls2.getMethod("getBoolean", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putBoolean", new Class[]{cls, Long.TYPE, Boolean.TYPE});
            cls2.getMethod("getFloat", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putFloat", new Class[]{cls, Long.TYPE, Float.TYPE});
            cls2.getMethod("getDouble", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putDouble", new Class[]{cls, Long.TYPE, Double.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzbcn() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = zzgvq;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls2.getMethod("getLong", new Class[]{cls, Long.TYPE});
            if (zzbco() == null) {
                return false;
            }
            if (zzdqd.zzaxn()) {
                return true;
            }
            cls2.getMethod("getByte", new Class[]{Long.TYPE});
            cls2.getMethod("putByte", new Class[]{Long.TYPE, Byte.TYPE});
            cls2.getMethod("getInt", new Class[]{Long.TYPE});
            cls2.getMethod("putInt", new Class[]{Long.TYPE, Integer.TYPE});
            cls2.getMethod("getLong", new Class[]{Long.TYPE});
            cls2.getMethod("putLong", new Class[]{Long.TYPE, Long.TYPE});
            cls2.getMethod("copyMemory", new Class[]{Long.TYPE, Long.TYPE, Long.TYPE});
            cls2.getMethod("copyMemory", new Class[]{cls, Long.TYPE, cls, Long.TYPE, Long.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    private static Field zzbco() {
        Field zzb2;
        if (zzdqd.zzaxn() && (zzb2 = zzb(Buffer.class, "effectiveDirectAddress")) != null) {
            return zzb2;
        }
        Field zzb3 = zzb(Buffer.class, "address");
        if (zzb3 == null || zzb3.getType() != Long.TYPE) {
            return null;
        }
        return zzb3;
    }

    /* access modifiers changed from: private */
    public static void zzc(Object obj, long j, boolean z) {
        zzb(obj, j, z ? (byte) 1 : 0);
    }

    static <T> T zzj(Class<T> cls) {
        try {
            return zzgvq.allocateInstance(cls);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        }
    }

    private static int zzk(Class<?> cls) {
        if (zzhiu) {
            return zzhrl.zzhsd.arrayBaseOffset(cls);
        }
        return -1;
    }

    private static int zzl(Class<?> cls) {
        if (zzhiu) {
            return zzhrl.zzhsd.arrayIndexScale(cls);
        }
        return -1;
    }

    static boolean zzm(Object obj, long j) {
        return zzhrl.zzm(obj, j);
    }

    static float zzn(Object obj, long j) {
        return zzhrl.zzn(obj, j);
    }

    static double zzo(Object obj, long j) {
        return zzhrl.zzo(obj, j);
    }

    static Object zzp(Object obj, long j) {
        return zzhrl.zzhsd.getObject(obj, j);
    }

    /* access modifiers changed from: private */
    public static byte zzq(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) (((~j) & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static byte zzr(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) ((j & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static boolean zzs(Object obj, long j) {
        return zzq(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static boolean zzt(Object obj, long j) {
        return zzr(obj, j) != 0;
    }

    static void zza(Object obj, long j, boolean z) {
        zzhrl.zza(obj, j, z);
    }

    private static Field zzb(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    private static boolean zzm(Class<?> cls) {
        Class<byte[]> cls2 = byte[].class;
        if (!zzdqd.zzaxn()) {
            return false;
        }
        try {
            Class<?> cls3 = zzhho;
            cls3.getMethod("peekLong", new Class[]{cls, Boolean.TYPE});
            cls3.getMethod("pokeLong", new Class[]{cls, Long.TYPE, Boolean.TYPE});
            cls3.getMethod("pokeInt", new Class[]{cls, Integer.TYPE, Boolean.TYPE});
            cls3.getMethod("peekInt", new Class[]{cls, Boolean.TYPE});
            cls3.getMethod("pokeByte", new Class[]{cls, Byte.TYPE});
            cls3.getMethod("peekByte", new Class[]{cls});
            cls3.getMethod("pokeByteArray", new Class[]{cls, cls2, Integer.TYPE, Integer.TYPE});
            cls3.getMethod("peekByteArray", new Class[]{cls, cls2, Integer.TYPE, Integer.TYPE});
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    static long zzn(ByteBuffer byteBuffer) {
        return zzhrl.zzl(byteBuffer, zzhsa);
    }

    static void zza(Object obj, long j, float f) {
        zzhrl.zza(obj, j, f);
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = (((int) j) & 3) << 3;
        zzb(obj, j2, ((255 & b) << i) | (zzk(obj, j2) & (~(JfifUtil.MARKER_FIRST_BYTE << i))));
    }

    static int zzk(Object obj, long j) {
        return zzhrl.zzk(obj, j);
    }

    static long zzl(Object obj, long j) {
        return zzhrl.zzl(obj, j);
    }

    static void zza(Object obj, long j, double d) {
        zzhrl.zza(obj, j, d);
    }

    static void zza(Object obj, long j, Object obj2) {
        zzhrl.zzhsd.putObject(obj, j, obj2);
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, boolean z) {
        zza(obj, j, z ? (byte) 1 : 0);
    }

    static byte zza(byte[] bArr, long j) {
        return zzhrl.zzy(bArr, zzhrn + j);
    }

    static void zza(byte[] bArr, long j, byte b) {
        zzhrl.zze(bArr, zzhrn + j, b);
    }

    static void zza(byte[] bArr, long j, long j2, long j3) {
        zzhrl.zza(bArr, j, j2, j3);
    }

    static void zza(long j, byte b) {
        zzhrl.zza(j, b);
    }

    /* access modifiers changed from: private */
    public static void zza(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int zzk = zzk(obj, j2);
        int i = ((~((int) j)) & 3) << 3;
        zzb(obj, j2, ((255 & b) << i) | (zzk & (~(JfifUtil.MARKER_FIRST_BYTE << i))));
    }
}
