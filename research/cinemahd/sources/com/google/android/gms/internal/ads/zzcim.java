package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final /* synthetic */ class zzcim implements zzsp {
    private final zzsy.zzj zzfyc;

    zzcim(zzsy.zzj zzj) {
        this.zzfyc = zzj;
    }

    public final void zza(zztu zztu) {
        zztu.zzcaz = this.zzfyc;
    }
}
