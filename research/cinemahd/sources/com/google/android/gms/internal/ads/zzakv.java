package com.google.android.gms.internal.ads;

import android.os.IBinder;

final /* synthetic */ class zzakv implements zzayw {
    static final zzayw zzbtz = new zzakv();

    private zzakv() {
    }

    public final Object apply(Object obj) {
        return zzbfv.zzao((IBinder) obj);
    }
}
