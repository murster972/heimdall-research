package com.google.android.gms.internal.ads;

import com.google.ar.core.ImageMetadata;
import java.util.Arrays;

public final class zznp implements zznj {
    private final boolean zzbel;
    private final int zzbem;
    private final byte[] zzben;
    private final zznk[] zzbeo;
    private int zzbep;
    private int zzbeq;
    private int zzber;
    private zznk[] zzbes;

    public zznp(boolean z, int i) {
        this(true, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE, 0);
    }

    public final synchronized void reset() {
        if (this.zzbel) {
            zzba(0);
        }
    }

    public final synchronized void zza(zznk zznk) {
        this.zzbeo[0] = zznk;
        zza(this.zzbeo);
    }

    public final synchronized void zzba(int i) {
        boolean z = i < this.zzbep;
        this.zzbep = i;
        if (z) {
            zzm();
        }
    }

    public final synchronized zznk zzif() {
        zznk zznk;
        this.zzbeq++;
        if (this.zzber > 0) {
            zznk[] zznkArr = this.zzbes;
            int i = this.zzber - 1;
            this.zzber = i;
            zznk = zznkArr[i];
            this.zzbes[this.zzber] = null;
        } else {
            zznk = new zznk(new byte[this.zzbem], 0);
        }
        return zznk;
    }

    public final int zzig() {
        return this.zzbem;
    }

    public final synchronized int zzii() {
        return this.zzbeq * this.zzbem;
    }

    public final synchronized void zzm() {
        int max = Math.max(0, zzoq.zzf(this.zzbep, this.zzbem) - this.zzbeq);
        if (max < this.zzber) {
            Arrays.fill(this.zzbes, max, this.zzber, (Object) null);
            this.zzber = max;
        }
    }

    private zznp(boolean z, int i, int i2) {
        zzoc.checkArgument(true);
        zzoc.checkArgument(true);
        this.zzbel = true;
        this.zzbem = ImageMetadata.CONTROL_AE_ANTIBANDING_MODE;
        this.zzber = 0;
        this.zzbes = new zznk[100];
        this.zzben = null;
        this.zzbeo = new zznk[1];
    }

    public final synchronized void zza(zznk[] zznkArr) {
        boolean z;
        if (this.zzber + zznkArr.length >= this.zzbes.length) {
            this.zzbes = (zznk[]) Arrays.copyOf(this.zzbes, Math.max(this.zzbes.length << 1, this.zzber + zznkArr.length));
        }
        for (zznk zznk : zznkArr) {
            if (zznk.data != null) {
                if (zznk.data.length != this.zzbem) {
                    z = false;
                    zzoc.checkArgument(z);
                    zznk[] zznkArr2 = this.zzbes;
                    int i = this.zzber;
                    this.zzber = i + 1;
                    zznkArr2[i] = zznk;
                }
            }
            z = true;
            zzoc.checkArgument(z);
            zznk[] zznkArr22 = this.zzbes;
            int i2 = this.zzber;
            this.zzber = i2 + 1;
            zznkArr22[i2] = zznk;
        }
        this.zzbeq -= zznkArr.length;
        notifyAll();
    }
}
