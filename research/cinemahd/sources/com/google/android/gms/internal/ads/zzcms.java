package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcms implements zzcir<zzcbb, zzdac, zzcjx> {
    /* access modifiers changed from: private */
    public final Executor zzfci;
    private final zzcbi zzgal;
    private final Context zzup;

    public zzcms(Context context, Executor executor, zzcbi zzcbi) {
        this.zzup = context;
        this.zzfci = executor;
        this.zzgal = zzcbi;
    }

    /* access modifiers changed from: private */
    public static void zzc(zzczt zzczt, zzczl zzczl, zzcip<zzdac, zzcjx> zzcip) {
        try {
            ((zzdac) zzcip.zzddn).zza(zzczt.zzgmh.zzfgl.zzgml, zzczl.zzglr.toString());
        } catch (Exception e) {
            String valueOf = String.valueOf(zzcip.zzfge);
            zzayu.zzd(valueOf.length() != 0 ? "Fail to load ad from adapter ".concat(valueOf) : new String("Fail to load ad from adapter "), e);
        }
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzdac, zzcjx> zzcip) throws zzdab {
        if (!((zzdac) zzcip.zzddn).isInitialized()) {
            ((zzcjx) zzcip.zzfyf).zza((zzbth) new zzcmu(this, zzczt, zzczl, zzcip));
            ((zzdac) zzcip.zzddn).zza(this.zzup, zzczt.zzgmh.zzfgl.zzgml, (String) null, (zzarz) zzcip.zzfyf, zzczl.zzglr.toString());
            return;
        }
        zzc(zzczt, zzczl, zzcip);
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzcbd zza = this.zzgal.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzcbg(new zzcmr(zzcip)));
        zza.zzadh().zza(new zzbiu((zzdac) zzcip.zzddn), this.zzfci);
        zzbpm zzadi = zza.zzadi();
        zzboq zzadj = zza.zzadj();
        ((zzcjx) zzcip.zzfyf).zza((zzarz) new zzcmw(this, zza.zzaen(), zzadj, zzadi, zza.zzaev()));
        return zza.zzaeu();
    }
}
