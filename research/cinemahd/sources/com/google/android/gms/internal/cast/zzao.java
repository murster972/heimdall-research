package com.google.android.gms.internal.cast;

import android.graphics.Bitmap;

final class zzao implements zzab {
    private final /* synthetic */ zzal zzrk;

    zzao(zzal zzal) {
        this.zzrk = zzal;
    }

    public final void zza(Bitmap bitmap) {
        this.zzrk.zza(bitmap, 3);
    }
}
