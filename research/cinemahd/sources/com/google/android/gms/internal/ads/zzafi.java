package com.google.android.gms.internal.ads;

import java.util.Map;

final class zzafi implements zzafn<Object> {
    zzafi() {
    }

    public final void zza(Object obj, Map<String, String> map) {
        String valueOf = String.valueOf(map.get("string"));
        zzayu.zzey(valueOf.length() != 0 ? "Received log message: ".concat(valueOf) : new String("Received log message: "));
    }
}
