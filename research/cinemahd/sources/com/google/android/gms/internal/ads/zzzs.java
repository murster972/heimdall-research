package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public final class zzzs {
    private static void zza(List<String> list, zzaan<String> zzaan) {
        String str = zzaan.get();
        if (!TextUtils.isEmpty(str)) {
            list.add(str);
        }
    }

    static List<String> zzqm() {
        ArrayList arrayList = new ArrayList();
        zza(arrayList, zzaan.zzi("gad:dynamite_module:experiment_id", ""));
        zza(arrayList, zzaba.zzctk);
        zza(arrayList, zzaba.zzctl);
        zza(arrayList, zzaba.zzctm);
        zza(arrayList, zzaba.zzctn);
        zza(arrayList, zzaba.zzcto);
        zza(arrayList, zzaba.zzctu);
        zza(arrayList, zzaba.zzctp);
        zza(arrayList, zzaba.zzctq);
        zza(arrayList, zzaba.zzctr);
        zza(arrayList, zzaba.zzcts);
        zza(arrayList, zzaba.zzctt);
        return arrayList;
    }

    static List<String> zzqn() {
        ArrayList arrayList = new ArrayList();
        zza(arrayList, zzabk.zzcut);
        return arrayList;
    }
}
