package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import androidx.core.view.ViewCompat;
import com.facebook.common.util.UriUtil;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.zzc;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.Predicate;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

public final class zzbfb extends zzbfk implements zzbev {
    private final Object lock = new Object();
    private volatile boolean zzbmb;
    private zzty zzcbt;
    private zzaew zzcwq;
    private zzaey zzcws;
    private zzc zzcxo;
    private zzaoe zzcxp;
    private zzo zzdhq;
    private zzt zzdhu;
    private boolean zzdll;
    protected zzbdi zzeef;
    private zzbeu zzeei;
    private zzbex zzeej;
    private zzbew zzeek;
    private boolean zzeel = false;
    private boolean zzeem;
    private boolean zzeen;
    private boolean zzeeo;
    private zzaol zzeep;
    private zzato zzeeq;
    private boolean zzeer;
    private boolean zzees;
    private int zzeet;
    private View.OnAttachStateChangeListener zzeeu;
    private final zzaie<zzbdi> zzehu = new zzaie<>();

    private final void zzaay() {
        if (this.zzeeu != null) {
            this.zzeef.getView().removeOnAttachStateChangeListener(this.zzeeu);
        }
    }

    private final void zzabd() {
        if (this.zzeei != null && ((this.zzeer && this.zzeet <= 0) || this.zzees)) {
            this.zzeei.zzak(!this.zzees);
            this.zzeei = null;
        }
        this.zzeef.zzaak();
    }

    private static WebResourceResponse zzabe() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcip)).booleanValue()) {
            return new WebResourceResponse("", "", new ByteArrayInputStream(new byte[0]));
        }
        return null;
    }

    private final WebResourceResponse zze(zzbfn zzbfn) throws IOException {
        HttpURLConnection httpURLConnection;
        URL url = new URL(zzbfn.url);
        int i = 0;
        while (true) {
            i++;
            if (i <= 20) {
                URLConnection openConnection = url.openConnection();
                openConnection.setConnectTimeout(10000);
                openConnection.setReadTimeout(10000);
                for (Map.Entry next : zzbfn.zzab.entrySet()) {
                    openConnection.addRequestProperty((String) next.getKey(), (String) next.getValue());
                }
                if (openConnection instanceof HttpURLConnection) {
                    httpURLConnection = (HttpURLConnection) openConnection;
                    zzq.zzkq().zza(this.zzeef.getContext(), this.zzeef.zzyr().zzbma, false, httpURLConnection);
                    zzayo zzayo = new zzayo();
                    zzayo.zza(httpURLConnection, (byte[]) null);
                    int responseCode = httpURLConnection.getResponseCode();
                    zzayo.zza(httpURLConnection, responseCode);
                    if (responseCode < 300 || responseCode >= 400) {
                        zzq.zzkq();
                    } else {
                        String headerField = httpURLConnection.getHeaderField("Location");
                        if (headerField == null) {
                            throw new IOException("Missing Location header in redirect");
                        } else if (headerField.startsWith("tel:")) {
                            return null;
                        } else {
                            URL url2 = new URL(url, headerField);
                            String protocol = url2.getProtocol();
                            if (protocol == null) {
                                zzayu.zzez("Protocol is null");
                                return zzabe();
                            } else if (protocol.equals(UriUtil.HTTP_SCHEME) || protocol.equals(UriUtil.HTTPS_SCHEME)) {
                                String valueOf = String.valueOf(headerField);
                                zzayu.zzea(valueOf.length() != 0 ? "Redirecting to ".concat(valueOf) : new String("Redirecting to "));
                                httpURLConnection.disconnect();
                                url = url2;
                            } else {
                                String valueOf2 = String.valueOf(protocol);
                                zzayu.zzez(valueOf2.length() != 0 ? "Unsupported scheme: ".concat(valueOf2) : new String("Unsupported scheme: "));
                                return zzabe();
                            }
                        }
                    }
                } else {
                    throw new IOException("Invalid protocol.");
                }
            } else {
                StringBuilder sb = new StringBuilder(32);
                sb.append("Too many redirects (20)");
                throw new IOException(sb.toString());
            }
        }
        zzq.zzkq();
        return zzawb.zzd(httpURLConnection);
    }

    public final void destroy() {
        zzato zzato = this.zzeeq;
        if (zzato != null) {
            zzato.zzun();
            this.zzeeq = null;
        }
        zzaay();
        this.zzehu.reset();
        this.zzehu.zzg(null);
        synchronized (this.lock) {
            this.zzcbt = null;
            this.zzdhq = null;
            this.zzeei = null;
            this.zzeej = null;
            this.zzcwq = null;
            this.zzcws = null;
            this.zzdhu = null;
            this.zzeek = null;
            if (this.zzcxp != null) {
                this.zzcxp.zzac(true);
                this.zzcxp = null;
            }
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        zzro zzaaq = this.zzeef.zzaaq();
        if (zzaaq != null && webView == zzaaq.getWebView()) {
            zzaaq.onPageStarted(webView, str, bitmap);
        }
        super.onPageStarted(webView, str, bitmap);
    }

    @TargetApi(26)
    public final boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
        return this.zzeef.zzb(renderProcessGoneDetail.didCrash(), renderProcessGoneDetail.rendererPriorityAtExit());
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzbdi zzbdi, boolean z) {
        zzaol zzaol = new zzaol(zzbdi, zzbdi.zzzv(), new zzyy(zzbdi.getContext()));
        this.zzeef = zzbdi;
        this.zzbmb = z;
        this.zzeep = zzaol;
        this.zzcxp = null;
        this.zzehu.zzg(zzbdi);
    }

    public final zzc zzaas() {
        return this.zzcxo;
    }

    public final boolean zzaat() {
        return this.zzbmb;
    }

    public final boolean zzaau() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzeem;
        }
        return z;
    }

    public final boolean zzaav() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzeen;
        }
        return z;
    }

    public final ViewTreeObserver.OnGlobalLayoutListener zzaaw() {
        synchronized (this.lock) {
        }
        return null;
    }

    public final ViewTreeObserver.OnScrollChangedListener zzaax() {
        synchronized (this.lock) {
        }
        return null;
    }

    public final void zzaaz() {
        zzato zzato = this.zzeeq;
        if (zzato != null) {
            WebView webView = this.zzeef.getWebView();
            if (ViewCompat.D(webView)) {
                zza((View) webView, zzato, 10);
                return;
            }
            zzaay();
            this.zzeeu = new zzbfc(this, zzato);
            this.zzeef.getView().addOnAttachStateChangeListener(this.zzeeu);
        }
    }

    public final void zzaba() {
        synchronized (this.lock) {
            this.zzeeo = true;
        }
        this.zzeet++;
        zzabd();
    }

    public final void zzabb() {
        this.zzeet--;
        zzabd();
    }

    public final void zzabc() {
        this.zzees = true;
        zzabd();
    }

    public final zzato zzabf() {
        return this.zzeeq;
    }

    public final void zzav(boolean z) {
        this.zzeel = z;
    }

    public final void zzb(String str, zzafn<? super zzbdi> zzafn) {
        this.zzehu.zzb(str, zzafn);
    }

    public final void zzba(boolean z) {
        this.zzdll = z;
    }

    public final void zzbb(boolean z) {
        synchronized (this.lock) {
            this.zzeem = true;
        }
    }

    public final void zzbc(boolean z) {
        synchronized (this.lock) {
            this.zzeen = z;
        }
    }

    public final void zzc(boolean z, int i) {
        zzty zzty = (!this.zzeef.zzaaf() || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        zzo zzo = this.zzdhq;
        zzt zzt = this.zzdhu;
        zzbdi zzbdi = this.zzeef;
        zza(new AdOverlayInfoParcel(zzty, zzo, zzt, zzbdi, z, i, zzbdi.zzyr()));
    }

    public final WebResourceResponse zzd(zzbfn zzbfn) {
        WebResourceResponse webResourceResponse;
        zzrx zza;
        String str;
        zzato zzato = this.zzeeq;
        if (zzato != null) {
            zzato.zza(zzbfn.url, zzbfn.zzab, 1);
        }
        if (!"mraid.js".equalsIgnoreCase(new File(zzbfn.url).getName())) {
            webResourceResponse = null;
        } else {
            zztn();
            if (this.zzeef.zzzy().zzabt()) {
                str = (String) zzve.zzoy().zzd(zzzn.zzchn);
            } else if (this.zzeef.zzaaf()) {
                str = (String) zzve.zzoy().zzd(zzzn.zzchm);
            } else {
                str = (String) zzve.zzoy().zzd(zzzn.zzchl);
            }
            zzq.zzkq();
            webResourceResponse = zzawb.zzd(this.zzeef.getContext(), this.zzeef.zzyr().zzbma, str);
        }
        if (webResourceResponse != null) {
            return webResourceResponse;
        }
        try {
            if (!zzauk.zzb(zzbfn.url, this.zzeef.getContext(), this.zzdll).equals(zzbfn.url)) {
                return zze(zzbfn);
            }
            zzry zzby = zzry.zzby(zzbfn.url);
            if (zzby != null && (zza = zzq.zzkw().zza(zzby)) != null && zza.zzmp()) {
                return new WebResourceResponse("", "", zza.zzmq());
            }
            if (!zzayo.isEnabled() || !zzaax.zzcte.get().booleanValue()) {
                return null;
            }
            return zze(zzbfn);
        } catch (Exception | NoClassDefFoundError e) {
            zzq.zzku().zza(e, "AdWebViewClient.interceptRequest");
            return zzabe();
        }
    }

    public final void zzh(Uri uri) {
        this.zzehu.zzh(uri);
    }

    public final void zzi(int i, int i2) {
        zzaoe zzaoe = this.zzcxp;
        if (zzaoe != null) {
            zzaoe.zzi(i, i2);
        }
    }

    public final void zztn() {
        synchronized (this.lock) {
            this.zzeel = false;
            this.zzbmb = true;
            zzazd.zzdwi.execute(new zzbfa(this));
        }
    }

    public final void zzb(zzbfn zzbfn) {
        this.zzehu.zzg(zzbfn.uri);
    }

    public final boolean zzc(zzbfn zzbfn) {
        String valueOf = String.valueOf(zzbfn.url);
        zzavs.zzed(valueOf.length() != 0 ? "AdWebView shouldOverrideUrlLoading: ".concat(valueOf) : new String("AdWebView shouldOverrideUrlLoading: "));
        Uri uri = zzbfn.uri;
        if (this.zzehu.zzg(uri)) {
            return true;
        }
        if (this.zzeel) {
            String scheme = uri.getScheme();
            if (UriUtil.HTTP_SCHEME.equalsIgnoreCase(scheme) || UriUtil.HTTPS_SCHEME.equalsIgnoreCase(scheme)) {
                zzty zzty = this.zzcbt;
                if (zzty != null) {
                    zzty.onAdClicked();
                    zzato zzato = this.zzeeq;
                    if (zzato != null) {
                        zzato.zzdv(zzbfn.url);
                    }
                    this.zzcbt = null;
                }
                return false;
            }
        }
        if (!this.zzeef.getWebView().willNotDraw()) {
            try {
                zzdq zzaad = this.zzeef.zzaad();
                if (zzaad != null && zzaad.zzb(uri)) {
                    uri = zzaad.zza(uri, this.zzeef.getContext(), this.zzeef.getView(), this.zzeef.zzyn());
                }
            } catch (zzdt unused) {
                String valueOf2 = String.valueOf(zzbfn.url);
                zzayu.zzez(valueOf2.length() != 0 ? "Unable to append parameter to URL: ".concat(valueOf2) : new String("Unable to append parameter to URL: "));
            }
            zzc zzc = this.zzcxo;
            if (zzc == null || zzc.zzjq()) {
                zza(new zzd("android.intent.action.VIEW", uri.toString(), (String) null, (String) null, (String) null, (String) null, (String) null));
            } else {
                this.zzcxo.zzbq(zzbfn.url);
            }
        } else {
            String valueOf3 = String.valueOf(zzbfn.url);
            zzayu.zzez(valueOf3.length() != 0 ? "AdWebView unable to handle URL: ".concat(valueOf3) : new String("AdWebView unable to handle URL: "));
        }
        return true;
    }

    public final void zza(int i, int i2, boolean z) {
        this.zzeep.zzj(i, i2);
        zzaoe zzaoe = this.zzcxp;
        if (zzaoe != null) {
            zzaoe.zza(i, i2, false);
        }
    }

    public final void zza(String str, zzafn<? super zzbdi> zzafn) {
        this.zzehu.zza(str, zzafn);
    }

    public final void zza(String str, Predicate<zzafn<? super zzbdi>> predicate) {
        this.zzehu.zza(str, predicate);
    }

    public final void zza(zzty zzty, zzaew zzaew, zzo zzo, zzaey zzaey, zzt zzt, boolean z, zzafq zzafq, zzc zzc, zzaon zzaon, zzato zzato) {
        if (zzc == null) {
            zzc = new zzc(this.zzeef.getContext(), zzato, (zzapz) null);
        }
        this.zzcxp = new zzaoe(this.zzeef, zzaon);
        this.zzeeq = zzato;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzciz)).booleanValue()) {
            zza("/adMetadata", (zzafn<? super zzbdi>) new zzaet(zzaew));
        }
        zza("/appEvent", (zzafn<? super zzbdi>) new zzaev(zzaey));
        zza("/backButton", (zzafn<? super zzbdi>) zzafa.zzcxd);
        zza("/refresh", (zzafn<? super zzbdi>) zzafa.zzcxe);
        zza("/canOpenURLs", (zzafn<? super zzbdi>) zzafa.zzcwu);
        zza("/canOpenIntents", (zzafn<? super zzbdi>) zzafa.zzcwv);
        zza("/click", (zzafn<? super zzbdi>) zzafa.zzcww);
        zza("/close", (zzafn<? super zzbdi>) zzafa.zzcwx);
        zza("/customClose", (zzafn<? super zzbdi>) zzafa.zzcwy);
        zza("/instrument", (zzafn<? super zzbdi>) zzafa.zzcxh);
        zza("/delayPageLoaded", (zzafn<? super zzbdi>) zzafa.zzcxj);
        zza("/delayPageClosed", (zzafn<? super zzbdi>) zzafa.zzcxk);
        zza("/getLocationInfo", (zzafn<? super zzbdi>) zzafa.zzcxl);
        zza("/httpTrack", (zzafn<? super zzbdi>) zzafa.zzcwz);
        zza("/log", (zzafn<? super zzbdi>) zzafa.zzcxa);
        zza("/mraid", (zzafn<? super zzbdi>) new zzafs(zzc, this.zzcxp, zzaon));
        zza("/mraidLoaded", (zzafn<? super zzbdi>) this.zzeep);
        zza("/open", (zzafn<? super zzbdi>) new zzafr(zzc, this.zzcxp));
        zza("/precache", (zzafn<? super zzbdi>) new zzbcs());
        zza("/touch", (zzafn<? super zzbdi>) zzafa.zzcxc);
        zza("/video", (zzafn<? super zzbdi>) zzafa.zzcxf);
        zza("/videoMeta", (zzafn<? super zzbdi>) zzafa.zzcxg);
        if (zzq.zzlo().zzab(this.zzeef.getContext())) {
            zza("/logScionEvent", (zzafn<? super zzbdi>) new zzafp(this.zzeef.getContext()));
        }
        this.zzcbt = zzty;
        this.zzdhq = zzo;
        this.zzcwq = zzaew;
        this.zzcws = zzaey;
        this.zzdhu = zzt;
        this.zzcxo = zzc;
        this.zzeel = z;
    }

    public final void zza(zzbfn zzbfn) {
        this.zzeer = true;
        zzbex zzbex = this.zzeej;
        if (zzbex != null) {
            zzbex.zzsb();
            this.zzeej = null;
        }
        zzabd();
    }

    /* access modifiers changed from: private */
    public final void zza(View view, zzato zzato, int i) {
        if (zzato.zzul() && i > 0) {
            zzato.zzj(view);
            if (zzato.zzul()) {
                zzawb.zzdsr.postDelayed(new zzbfd(this, view, zzato, i), 100);
            }
        }
    }

    public final void zza(zzd zzd) {
        zzo zzo;
        boolean zzaaf = this.zzeef.zzaaf();
        zzty zzty = (!zzaaf || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        if (zzaaf) {
            zzo = null;
        } else {
            zzo = this.zzdhq;
        }
        zza(new AdOverlayInfoParcel(zzd, zzty, zzo, this.zzdhu, this.zzeef.zzyr()));
    }

    public final void zza(boolean z, int i, String str) {
        zzbff zzbff;
        boolean zzaaf = this.zzeef.zzaaf();
        zzty zzty = (!zzaaf || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        if (zzaaf) {
            zzbff = null;
        } else {
            zzbff = new zzbff(this.zzeef, this.zzdhq);
        }
        zzaew zzaew = this.zzcwq;
        zzaey zzaey = this.zzcws;
        zzt zzt = this.zzdhu;
        zzbdi zzbdi = this.zzeef;
        zza(new AdOverlayInfoParcel(zzty, (zzo) zzbff, zzaew, zzaey, zzt, zzbdi, z, i, str, zzbdi.zzyr()));
    }

    public final void zza(boolean z, int i, String str, String str2) {
        zzbff zzbff;
        boolean zzaaf = this.zzeef.zzaaf();
        zzty zzty = (!zzaaf || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        if (zzaaf) {
            zzbff = null;
        } else {
            zzbff = new zzbff(this.zzeef, this.zzdhq);
        }
        zzaew zzaew = this.zzcwq;
        zzaey zzaey = this.zzcws;
        zzt zzt = this.zzdhu;
        zzbdi zzbdi = this.zzeef;
        zza(new AdOverlayInfoParcel(zzty, zzbff, zzaew, zzaey, zzt, zzbdi, z, i, str, str2, zzbdi.zzyr()));
    }

    private final void zza(AdOverlayInfoParcel adOverlayInfoParcel) {
        zzd zzd;
        zzaoe zzaoe = this.zzcxp;
        boolean zztg = zzaoe != null ? zzaoe.zztg() : false;
        zzq.zzkp();
        zzn.zza(this.zzeef.getContext(), adOverlayInfoParcel, !zztg);
        if (this.zzeeq != null) {
            String str = adOverlayInfoParcel.url;
            if (str == null && (zzd = adOverlayInfoParcel.zzdhp) != null) {
                str = zzd.url;
            }
            this.zzeeq.zzdv(str);
        }
    }

    public final void zza(zzbeu zzbeu) {
        this.zzeei = zzbeu;
    }

    public final void zza(zzbex zzbex) {
        this.zzeej = zzbex;
    }
}
