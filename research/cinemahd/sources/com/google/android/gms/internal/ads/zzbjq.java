package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import java.util.concurrent.Executor;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbjq implements zzps {
    private final Clock zzbmq;
    private boolean zzbsd = false;
    private zzbdi zzcza;
    private final zzbjb zzfcf;
    private final Executor zzfci;
    private zzbjf zzfck = new zzbjf();
    private boolean zzfdh = false;

    public zzbjq(Executor executor, zzbjb zzbjb, Clock clock) {
        this.zzfci = executor;
        this.zzfcf = zzbjb;
        this.zzbmq = clock;
    }

    private final void zzafq() {
        try {
            JSONObject zza = this.zzfcf.zzj(this.zzfck);
            if (this.zzcza != null) {
                this.zzfci.execute(new zzbjp(this, zza));
            }
        } catch (JSONException e) {
            zzavs.zza("Failed to call video active view js", e);
        }
    }

    public final void disable() {
        this.zzbsd = false;
    }

    public final void enable() {
        this.zzbsd = true;
        zzafq();
    }

    public final void zza(zzpt zzpt) {
        this.zzfck.zzbnq = this.zzfdh ? false : zzpt.zzbnq;
        this.zzfck.timestamp = this.zzbmq.a();
        this.zzfck.zzfcr = zzpt;
        if (this.zzbsd) {
            zzafq();
        }
    }

    public final void zzbf(boolean z) {
        this.zzfdh = z;
    }

    public final void zzg(zzbdi zzbdi) {
        this.zzcza = zzbdi;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzh(JSONObject jSONObject) {
        this.zzcza.zza("AFMA_updateActiveView", jSONObject);
    }
}
