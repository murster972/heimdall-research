package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;
import java.util.concurrent.atomic.AtomicBoolean;

public final class zzbmx implements zzo {
    private final zzbpm zzfgg;
    private AtomicBoolean zzfgh = new AtomicBoolean(false);

    public zzbmx(zzbpm zzbpm) {
        this.zzfgg = zzbpm;
    }

    public final boolean isClosed() {
        return this.zzfgh.get();
    }

    public final void onPause() {
    }

    public final void onResume() {
    }

    public final void zzte() {
        this.zzfgh.set(true);
        this.zzfgg.onAdClosed();
    }

    public final void zztf() {
        this.zzfgg.onAdOpened();
    }
}
