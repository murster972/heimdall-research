package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.concurrent.atomic.AtomicBoolean;

public final class zzxl {
    private final zzuh zzaba;
    private VideoOptions zzbka;
    private boolean zzbkh;
    private AppEventListener zzbkj;
    private zzvu zzbqy;
    private String zzbqz;
    private final zzakz zzbrb;
    private zzty zzcbt;
    private AdListener zzcbw;
    private AdSize[] zzcdf;
    private final AtomicBoolean zzcek;
    /* access modifiers changed from: private */
    public final VideoController zzcel;
    private final zzvd zzcem;
    private OnCustomRenderedAdLoadedListener zzcen;
    private ViewGroup zzceo;
    private int zzcep;

    public zzxl(ViewGroup viewGroup) {
        this(viewGroup, (AttributeSet) null, false, zzuh.zzccn, 0);
    }

    private static boolean zzck(int i) {
        return i == 1;
    }

    public final void destroy() {
        try {
            if (this.zzbqy != null) {
                this.zzbqy.destroy();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final AdListener getAdListener() {
        return this.zzcbw;
    }

    public final AdSize getAdSize() {
        zzuj zzjz;
        try {
            if (!(this.zzbqy == null || (zzjz = this.zzbqy.zzjz()) == null)) {
                return zzjz.zzoo();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        AdSize[] adSizeArr = this.zzcdf;
        if (adSizeArr != null) {
            return adSizeArr[0];
        }
        return null;
    }

    public final AdSize[] getAdSizes() {
        return this.zzcdf;
    }

    public final String getAdUnitId() {
        zzvu zzvu;
        if (this.zzbqz == null && (zzvu = this.zzbqy) != null) {
            try {
                this.zzbqz = zzvu.getAdUnitId();
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
        return this.zzbqz;
    }

    public final AppEventListener getAppEventListener() {
        return this.zzbkj;
    }

    public final String getMediationAdapterClassName() {
        try {
            if (this.zzbqy != null) {
                return this.zzbqy.zzka();
            }
            return null;
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return null;
        }
    }

    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.zzcen;
    }

    public final VideoController getVideoController() {
        return this.zzcel;
    }

    public final VideoOptions getVideoOptions() {
        return this.zzbka;
    }

    public final boolean isLoading() {
        try {
            if (this.zzbqy != null) {
                return this.zzbqy.isLoading();
            }
            return false;
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return false;
        }
    }

    public final void pause() {
        try {
            if (this.zzbqy != null) {
                this.zzbqy.pause();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void recordManualImpression() {
        if (!this.zzcek.getAndSet(true)) {
            try {
                if (this.zzbqy != null) {
                    this.zzbqy.zzjy();
                }
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }

    public final void resume() {
        try {
            if (this.zzbqy != null) {
                this.zzbqy.resume();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void setAdListener(AdListener adListener) {
        this.zzcbw = adListener;
        this.zzcem.zza(adListener);
    }

    public final void setAdSizes(AdSize... adSizeArr) {
        if (this.zzcdf == null) {
            zza(adSizeArr);
            return;
        }
        throw new IllegalStateException("The ad size can only be set once on AdView.");
    }

    public final void setAdUnitId(String str) {
        if (this.zzbqz == null) {
            this.zzbqz = str;
            return;
        }
        throw new IllegalStateException("The ad unit ID can only be set once on AdView.");
    }

    public final void setAppEventListener(AppEventListener appEventListener) {
        try {
            this.zzbkj = appEventListener;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzwc) appEventListener != null ? new zzul(appEventListener) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void setManualImpressionsEnabled(boolean z) {
        this.zzbkh = z;
        try {
            if (this.zzbqy != null) {
                this.zzbqy.setManualImpressionsEnabled(this.zzbkh);
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.zzcen = onCustomRenderedAdLoadedListener;
        try {
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzaak) onCustomRenderedAdLoadedListener != null ? new zzaal(onCustomRenderedAdLoadedListener) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void setVideoOptions(VideoOptions videoOptions) {
        zzyw zzyw;
        this.zzbka = videoOptions;
        try {
            if (this.zzbqy != null) {
                zzvu zzvu = this.zzbqy;
                if (videoOptions == null) {
                    zzyw = null;
                } else {
                    zzyw = new zzyw(videoOptions);
                }
                zzvu.zza(zzyw);
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void zza(zzxj zzxj) {
        zzvu zzvu;
        try {
            if (this.zzbqy == null) {
                if ((this.zzcdf == null || this.zzbqz == null) && this.zzbqy == null) {
                    throw new IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.");
                }
                Context context = this.zzceo.getContext();
                zzuj zza = zza(context, this.zzcdf, this.zzcep);
                if ("search_v2".equals(zza.zzabg)) {
                    zzvu = (zzvu) new zzuw(zzve.zzov(), context, zza, this.zzbqz).zzd(context, false);
                } else {
                    zzvu = (zzvu) new zzus(zzve.zzov(), context, zza, this.zzbqz, this.zzbrb).zzd(context, false);
                }
                this.zzbqy = zzvu;
                this.zzbqy.zza((zzvh) new zzuc(this.zzcem));
                if (this.zzcbt != null) {
                    this.zzbqy.zza((zzvg) new zztx(this.zzcbt));
                }
                if (this.zzbkj != null) {
                    this.zzbqy.zza((zzwc) new zzul(this.zzbkj));
                }
                if (this.zzcen != null) {
                    this.zzbqy.zza((zzaak) new zzaal(this.zzcen));
                }
                if (this.zzbka != null) {
                    this.zzbqy.zza(new zzyw(this.zzbka));
                }
                this.zzbqy.setManualImpressionsEnabled(this.zzbkh);
                try {
                    IObjectWrapper zzjx = this.zzbqy.zzjx();
                    if (zzjx != null) {
                        this.zzceo.addView((View) ObjectWrapper.a(zzjx));
                    }
                } catch (RemoteException e) {
                    zzayu.zze("#007 Could not call remote method.", e);
                }
            }
            if (this.zzbqy.zza(zzuh.zza(this.zzceo.getContext(), zzxj))) {
                this.zzbrb.zzf(zzxj.zzpq());
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final zzxb zzdl() {
        zzvu zzvu = this.zzbqy;
        if (zzvu == null) {
            return null;
        }
        try {
            return zzvu.getVideoController();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return null;
        }
    }

    public zzxl(ViewGroup viewGroup, int i) {
        this(viewGroup, (AttributeSet) null, false, zzuh.zzccn, i);
    }

    public zzxl(ViewGroup viewGroup, AttributeSet attributeSet, boolean z) {
        this(viewGroup, attributeSet, z, zzuh.zzccn, 0);
    }

    public zzxl(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, int i) {
        this(viewGroup, attributeSet, false, zzuh.zzccn, i);
    }

    private zzxl(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, zzuh zzuh, zzvu zzvu, int i) {
        zzuj zzuj;
        this.zzbrb = new zzakz();
        this.zzcel = new VideoController();
        this.zzcem = new zzxo(this);
        this.zzceo = viewGroup;
        this.zzaba = zzuh;
        this.zzbqy = null;
        this.zzcek = new AtomicBoolean(false);
        this.zzcep = i;
        if (attributeSet != null) {
            Context context = viewGroup.getContext();
            try {
                zzuq zzuq = new zzuq(context, attributeSet);
                this.zzcdf = zzuq.zzy(z);
                this.zzbqz = zzuq.getAdUnitId();
                if (viewGroup.isInEditMode()) {
                    zzayk zzou = zzve.zzou();
                    AdSize adSize = this.zzcdf[0];
                    int i2 = this.zzcep;
                    if (adSize.equals(AdSize.INVALID)) {
                        zzuj = zzuj.zzon();
                    } else {
                        zzuj zzuj2 = new zzuj(context, adSize);
                        zzuj2.zzccr = zzck(i2);
                        zzuj = zzuj2;
                    }
                    zzou.zza(viewGroup, zzuj, "Ads by Google");
                }
            } catch (IllegalArgumentException e) {
                zzve.zzou().zza(viewGroup, new zzuj(context, AdSize.BANNER), e.getMessage(), e.getMessage());
            }
        }
    }

    public final void zza(zzty zzty) {
        try {
            this.zzcbt = zzty;
            if (this.zzbqy != null) {
                this.zzbqy.zza((zzvg) zzty != null ? new zztx(zzty) : null);
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    private zzxl(ViewGroup viewGroup, AttributeSet attributeSet, boolean z, zzuh zzuh, int i) {
        this(viewGroup, attributeSet, z, zzuh, (zzvu) null, i);
    }

    public final void zza(AdSize... adSizeArr) {
        this.zzcdf = adSizeArr;
        try {
            if (this.zzbqy != null) {
                this.zzbqy.zza(zza(this.zzceo.getContext(), this.zzcdf, this.zzcep));
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        this.zzceo.requestLayout();
    }

    public final boolean zza(zzvu zzvu) {
        if (zzvu == null) {
            return false;
        }
        try {
            IObjectWrapper zzjx = zzvu.zzjx();
            if (zzjx == null || ((View) ObjectWrapper.a(zzjx)).getParent() != null) {
                return false;
            }
            this.zzceo.addView((View) ObjectWrapper.a(zzjx));
            this.zzbqy = zzvu;
            return true;
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return false;
        }
    }

    private static zzuj zza(Context context, AdSize[] adSizeArr, int i) {
        for (AdSize equals : adSizeArr) {
            if (equals.equals(AdSize.INVALID)) {
                return zzuj.zzon();
            }
        }
        zzuj zzuj = new zzuj(context, adSizeArr);
        zzuj.zzccr = zzck(i);
        return zzuj;
    }
}
