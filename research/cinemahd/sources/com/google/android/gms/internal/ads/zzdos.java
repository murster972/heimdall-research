package com.google.android.gms.internal.ads;

public final class zzdos {
    private final zzdpj zzhfs;
    private final zzdpj zzhft;

    public zzdos(byte[] bArr, byte[] bArr2) {
        this.zzhfs = zzdpj.zzs(bArr);
        this.zzhft = zzdpj.zzs(bArr2);
    }

    public final byte[] zzaxd() {
        zzdpj zzdpj = this.zzhfs;
        if (zzdpj == null) {
            return null;
        }
        return zzdpj.getBytes();
    }

    public final byte[] zzaxe() {
        zzdpj zzdpj = this.zzhft;
        if (zzdpj == null) {
            return null;
        }
        return zzdpj.getBytes();
    }
}
