package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class zzes extends zzcj<Integer, Long> {
    public Long zzzf;
    public Long zzzg;
    public Long zzzh;
    public Long zzzi;

    public zzes() {
    }

    /* access modifiers changed from: protected */
    public final void zzap(String str) {
        HashMap zzaq = zzcj.zzaq(str);
        if (zzaq != null) {
            this.zzzf = (Long) zzaq.get(0);
            this.zzzg = (Long) zzaq.get(1);
            this.zzzh = (Long) zzaq.get(2);
            this.zzzi = (Long) zzaq.get(3);
        }
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> zzbk() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, this.zzzf);
        hashMap.put(1, this.zzzg);
        hashMap.put(2, this.zzzh);
        hashMap.put(3, this.zzzi);
        return hashMap;
    }

    public zzes(String str) {
        zzap(str);
    }
}
