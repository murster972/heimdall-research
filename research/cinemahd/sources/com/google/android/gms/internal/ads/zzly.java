package com.google.android.gms.internal.ads;

import java.io.IOException;

final class zzly implements zzmo {
    /* access modifiers changed from: private */
    public final int track;
    private final /* synthetic */ zzlp zzbat;

    public zzly(zzlp zzlp, int i) {
        this.zzbat = zzlp;
        this.track = i;
    }

    public final boolean isReady() {
        return this.zzbat.zzat(this.track);
    }

    public final int zzb(zzgy zzgy, zzis zzis, boolean z) {
        return this.zzbat.zza(this.track, zzgy, zzis, z);
    }

    public final void zzeh(long j) {
        this.zzbat.zzd(this.track, j);
    }

    public final void zzhk() throws IOException {
        this.zzbat.zzhk();
    }
}
