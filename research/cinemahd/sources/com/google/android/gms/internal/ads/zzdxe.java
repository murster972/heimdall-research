package com.google.android.gms.internal.ads;

public final class zzdxe<T> implements zzdxg<T> {
    private zzdxp<T> zziab;

    public static <T> void zzax(zzdxp<T> zzdxp, zzdxp<T> zzdxp2) {
        zzdxm.checkNotNull(zzdxp2);
        zzdxe zzdxe = (zzdxe) zzdxp;
        if (zzdxe.zziab == null) {
            zzdxe.zziab = zzdxp2;
            return;
        }
        throw new IllegalStateException();
    }

    public final T get() {
        zzdxp<T> zzdxp = this.zziab;
        if (zzdxp != null) {
            return zzdxp.get();
        }
        throw new IllegalStateException();
    }
}
