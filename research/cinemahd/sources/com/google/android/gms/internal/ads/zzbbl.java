package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbbl implements Runnable {
    private final int zzdtf;
    private final int zzdtg;
    private final zzbbc zzebm;

    zzbbl(zzbbc zzbbc, int i, int i2) {
        this.zzebm = zzbbc;
        this.zzdtf = i;
        this.zzdtg = i2;
    }

    public final void run() {
        this.zzebm.zzp(this.zzdtf, this.zzdtg);
    }
}
