package com.google.android.gms.internal.ads;

public final class zzctz implements zzdxg<zzctx> {
    private final zzdxp<zzcxw> zzfjl;

    private zzctz(zzdxp<zzcxw> zzdxp) {
        this.zzfjl = zzdxp;
    }

    public static zzctz zzal(zzdxp<zzcxw> zzdxp) {
        return new zzctz(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzctx(this.zzfjl.get());
    }
}
