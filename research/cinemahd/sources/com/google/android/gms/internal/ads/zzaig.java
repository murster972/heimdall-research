package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

final /* synthetic */ class zzaig implements Runnable {
    private final String zzcyz;

    zzaig(String str) {
        this.zzcyz = str;
    }

    public final void run() {
        zzq.zzku().zzuz().zzcr(this.zzcyz.substring(1));
    }
}
