package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzy implements Parcelable.Creator<zzv> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 1:
                    j = SafeParcelReader.y(parcel2, a2);
                    break;
                case 2:
                    j2 = SafeParcelReader.y(parcel2, a2);
                    break;
                case 3:
                    z = SafeParcelReader.s(parcel2, a2);
                    break;
                case 4:
                    str = SafeParcelReader.o(parcel2, a2);
                    break;
                case 5:
                    str2 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 6:
                    str3 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 7:
                    bundle = SafeParcelReader.f(parcel2, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel2, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel2, b);
        return new zzv(j, j2, z, str, str2, str3, bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzv[i];
    }
}
