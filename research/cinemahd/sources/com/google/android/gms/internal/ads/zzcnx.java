package com.google.android.gms.internal.ads;

import android.view.View;
import com.google.android.gms.ads.internal.zze;

final class zzcnx implements zze {
    private final /* synthetic */ zzbtw zzgcd;

    zzcnx(zzcnw zzcnw, zzbtw zzbtw) {
        this.zzgcd = zzbtw;
    }

    public final void zzg(View view) {
    }

    public final void zzjr() {
        this.zzgcd.zzadj().onAdClicked();
    }

    public final void zzjs() {
        this.zzgcd.zzadk().onAdImpression();
        this.zzgcd.zzadl().zzaia();
    }
}
