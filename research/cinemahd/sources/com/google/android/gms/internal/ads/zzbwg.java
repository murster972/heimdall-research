package com.google.android.gms.internal.ads;

public final class zzbwg implements zzdxg<zzbsu<zzbpe>> {
    private final zzdxp<zzbyc> zzfdq;
    private final zzbvy zzfla;

    public zzbwg(zzbvy zzbvy, zzdxp<zzbyc> zzdxp) {
        this.zzfla = zzbvy;
        this.zzfdq = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
