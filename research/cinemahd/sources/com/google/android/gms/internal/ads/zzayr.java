package com.google.android.gms.internal.ads;

import android.util.JsonWriter;
import java.util.Map;

final /* synthetic */ class zzayr implements zzayv {
    private final String zzcyr;
    private final Map zzcyw;
    private final String zzcyz;
    private final byte[] zzdvx;

    zzayr(String str, String str2, Map map, byte[] bArr) {
        this.zzcyz = str;
        this.zzcyr = str2;
        this.zzcyw = map;
        this.zzdvx = bArr;
    }

    public final void zzb(JsonWriter jsonWriter) {
        zzayo.zza(this.zzcyz, this.zzcyr, this.zzcyw, this.zzdvx, jsonWriter);
    }
}
