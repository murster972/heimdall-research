package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdmv extends zzdrt<zzdmv, zza> implements zzdtg {
    private static volatile zzdtn<zzdmv> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmv zzhco;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;
    private zzdmz zzhcn;

    public static final class zza extends zzdrt.zzb<zzdmv, zza> implements zzdtg {
        private zza() {
            super(zzdmv.zzhco);
        }

        public final zza zzau(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmv) this.zzhmp).zzs(zzdqk);
            return this;
        }

        public final zza zzd(zzdmz zzdmz) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmv) this.zzhmp).zzc(zzdmz);
            return this;
        }

        public final zza zzen(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmv) this.zzhmp).setVersion(0);
            return this;
        }

        /* synthetic */ zza(zzdmu zzdmu) {
            this();
        }
    }

    static {
        zzdmv zzdmv = new zzdmv();
        zzhco = zzdmv;
        zzdrt.zza(zzdmv.class, zzdmv);
    }

    private zzdmv() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public static zzdmv zzas(zzdqk zzdqk) throws zzdse {
        return (zzdmv) zzdrt.zza(zzhco, zzdqk);
    }

    public static zza zzava() {
        return (zza) zzhco.zzazt();
    }

    public static zzdmv zzavb() {
        return zzhco;
    }

    /* access modifiers changed from: private */
    public final void zzc(zzdmz zzdmz) {
        zzdmz.getClass();
        this.zzhcn = zzdmz;
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmu.zzdk[i - 1]) {
            case 1:
                return new zzdmv();
            case 2:
                return new zza((zzdmu) null);
            case 3:
                return zzdrt.zza((zzdte) zzhco, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", new Object[]{"zzhaa", "zzhcn", "zzhab"});
            case 4:
                return zzhco;
            case 5:
                zzdtn<zzdmv> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmv.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhco);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }

    public final zzdmz zzauz() {
        zzdmz zzdmz = this.zzhcn;
        return zzdmz == null ? zzdmz.zzavg() : zzdmz;
    }
}
