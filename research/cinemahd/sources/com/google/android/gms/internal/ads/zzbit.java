package com.google.android.gms.internal.ads;

public final class zzbit implements zzbow {
    private final zzczn zzfbk;
    private final zzczt zzfbl;
    private final zzdda zzfbm;

    public zzbit(zzczt zzczt, zzdda zzdda) {
        this.zzfbl = zzczt;
        this.zzfbm = zzdda;
        this.zzfbk = zzczt.zzgmi.zzgmf;
    }

    public final void onAdFailedToLoad(int i) {
        this.zzfbm.zza(this.zzfbl, (zzczl) null, this.zzfbk.zzdbt);
    }
}
