package com.google.android.gms.internal.cast;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import com.google.android.gms.cast.AdBreakInfo;
import com.google.android.gms.cast.framework.R$attr;
import com.google.android.gms.cast.framework.R$style;
import com.google.android.gms.cast.framework.R$styleable;
import com.google.android.gms.cast.framework.media.widget.zze;
import com.google.android.gms.common.internal.Objects;
import java.util.ArrayList;
import java.util.List;

public final class zzas extends View implements zzbn {
    private List<AdBreakInfo> zzdj = new ArrayList();
    private final SeekBar zzrp;
    private final int zzrr;
    private final int zzrs;
    private final int zzrt;
    private boolean zzru;
    private final Matrix zzrv = new Matrix();
    private final zzbh zzrw;
    private Paint zzrx;
    private Paint zzry;
    private Bitmap zzrz;

    @SuppressLint({"CustomViewStyleable"})
    public zzas(Context context, SeekBar seekBar, zzbh zzbh) {
        super(context);
        long j;
        this.zzrp = seekBar;
        this.zzrw = zzbh;
        Context context2 = getContext();
        if (context2 == null) {
            j = Math.round(3.0d);
        } else {
            j = Math.round(((double) context2.getResources().getDisplayMetrics().density) * 3.0d);
        }
        this.zzrr = (int) j;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, R$styleable.CastExpandedController, R$attr.castExpandedControllerStyle, R$style.CastExpandedController);
        this.zzrs = obtainStyledAttributes.getResourceId(R$styleable.CastExpandedController_castAdBreakMarkerColor, 0);
        this.zzrt = context.getResources().getColor(this.zzrs);
        obtainStyledAttributes.recycle();
        this.zzru = zze.a();
        if (this.zzru) {
            this.zzrp.setAlpha(0.01f);
            invalidate();
        }
    }

    private final void zzdg() {
        if (this.zzry == null) {
            this.zzry = new Paint(1);
            this.zzry.setColor(-8421505);
            this.zzry.setStyle(Paint.Style.FILL);
            this.zzry.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void onDraw(Canvas canvas) {
        Drawable a2;
        int i;
        super.onDraw(canvas);
        if (!(this.zzrz != null && this.zzrz.getWidth() == getMeasuredWidth() && this.zzrz.getHeight() == getMeasuredHeight())) {
            this.zzrz = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        }
        this.zzrz.eraseColor(0);
        Canvas canvas2 = new Canvas(this.zzrz);
        if (this.zzru) {
            Drawable progressDrawable = this.zzrp.getProgressDrawable();
            int save = canvas2.save();
            canvas2.translate((float) getPaddingLeft(), (float) getPaddingTop());
            progressDrawable.draw(canvas2);
            canvas2.restoreToCount(save);
        }
        if (this.zzrw.zzdo()) {
            zzdg();
            int save2 = canvas2.save();
            canvas2.translate((float) getPaddingLeft(), (float) getPaddingTop());
            int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            int measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
            double zzdr = ((double) this.zzrw.zzdr()) / ((double) this.zzrw.zzdm());
            double zzds = ((double) this.zzrw.zzds()) / ((double) this.zzrw.zzdm());
            double d = (double) measuredWidth;
            int floor = (int) Math.floor(zzdr * d);
            int ceil = (int) Math.ceil(zzds * d);
            if (floor > 0) {
                canvas2.drawRect(0.0f, 0.0f, (float) floor, (float) measuredHeight, this.zzry);
            }
            if (ceil < measuredWidth) {
                canvas2.drawRect((float) ceil, 0.0f, (float) measuredWidth, (float) measuredHeight, this.zzry);
            }
            canvas2.restoreToCount(save2);
        } else if (!this.zzrw.zzdo()) {
            int zzdt = (int) (0 - this.zzrw.zzdt());
            zzdg();
            int save3 = canvas2.save();
            canvas2.translate((float) getPaddingLeft(), (float) getPaddingTop());
            int measuredWidth2 = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            int measuredHeight2 = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
            int floor2 = (int) Math.floor((((double) zzdt) / ((double) this.zzrw.zzdm())) * ((double) measuredWidth2));
            if (floor2 > 0) {
                canvas2.drawRect(0.0f, 0.0f, (float) floor2, (float) measuredHeight2, this.zzry);
            }
            canvas2.restoreToCount(save3);
        }
        if (!this.zzdj.isEmpty()) {
            if (this.zzrx == null) {
                this.zzrx = new Paint(1);
                this.zzrx.setColor(this.zzrt);
                this.zzrx.setStyle(Paint.Style.FILL);
            }
            int max = this.zzrp.getMax();
            int round = Math.round(((float) getMeasuredHeight()) / 2.0f);
            int measuredWidth3 = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            for (AdBreakInfo next : this.zzdj) {
                if (next != null) {
                    long v = next.v();
                    if (v == -1000) {
                        i = max;
                    } else {
                        i = Math.min((int) (v - this.zzrw.zzdt()), max);
                    }
                    if (i >= 0) {
                        canvas2.drawCircle((float) (getPaddingLeft() + ((int) ((((double) i) * ((double) measuredWidth3)) / ((double) max)))), (float) round, (float) this.zzrr, this.zzrx);
                    }
                }
            }
        }
        if (this.zzru && (a2 = zze.a(this.zzrp)) != null) {
            int save4 = canvas2.save();
            canvas2.translate((float) (getPaddingLeft() - this.zzrp.getThumbOffset()), (float) getPaddingTop());
            a2.draw(canvas2);
            canvas2.restoreToCount(save4);
        }
        canvas.drawBitmap(this.zzrz, this.zzrv, (Paint) null);
    }

    public final synchronized void zzd(List<AdBreakInfo> list) {
        if (!Objects.a(this.zzdj, list)) {
            this.zzdj = list == null ? new ArrayList() : new ArrayList(list);
            postInvalidate();
        }
    }

    public final synchronized void zzde() {
        postInvalidate();
    }

    public final synchronized void zzdf() {
        postInvalidate();
    }
}
