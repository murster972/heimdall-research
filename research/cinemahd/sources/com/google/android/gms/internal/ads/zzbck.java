package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class zzbck implements Iterable<zzbci> {
    private final List<zzbci> zzecz = new ArrayList();

    public static boolean zzc(zzbaz zzbaz) {
        zzbci zzd = zzd(zzbaz);
        if (zzd == null) {
            return false;
        }
        zzd.zzecy.abort();
        return true;
    }

    static zzbci zzd(zzbaz zzbaz) {
        Iterator<zzbci> it2 = zzq.zzlm().iterator();
        while (it2.hasNext()) {
            zzbci next = it2.next();
            if (next.zzdxu == zzbaz) {
                return next;
            }
        }
        return null;
    }

    public final Iterator<zzbci> iterator() {
        return this.zzecz.iterator();
    }

    public final void zza(zzbci zzbci) {
        this.zzecz.add(zzbci);
    }

    public final void zzb(zzbci zzbci) {
        this.zzecz.remove(zzbci);
    }
}
