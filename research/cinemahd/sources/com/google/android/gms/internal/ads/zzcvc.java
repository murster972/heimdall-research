package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcvc implements Callable {
    private final zzcvd zzghu;

    zzcvc(zzcvd zzcvd) {
        this.zzghu = zzcvd;
    }

    public final Object call() {
        zzcvd zzcvd = this.zzghu;
        return new zzcva(zzcvd.zzghv.zzf(zzcvd.zzup));
    }
}
