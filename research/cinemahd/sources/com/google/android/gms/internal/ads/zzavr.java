package com.google.android.gms.internal.ads;

final class zzavr implements Runnable {
    private final /* synthetic */ zzavo zzdru;

    zzavr(zzavo zzavo) {
        this.zzdru = zzavo;
    }

    public final void run() {
        Thread unused = this.zzdru.thread = Thread.currentThread();
        this.zzdru.zztu();
    }
}
