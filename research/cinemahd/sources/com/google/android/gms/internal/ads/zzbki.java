package com.google.android.gms.internal.ads;

import android.view.ViewGroup;

public final class zzbki implements zzdxg<ViewGroup> {
    private final zzbkf zzfdx;

    public zzbki(zzbkf zzbkf) {
        this.zzfdx = zzbkf;
    }

    public final /* synthetic */ Object get() {
        return this.zzfdx.zzagd();
    }
}
