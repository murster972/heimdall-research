package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

public final class zzbud implements zzdxg<zzbsu<zzo>> {
    private final zzdxp<zzbuy> zzfdq;
    private final zzbtv zzfje;

    private zzbud(zzbtv zzbtv, zzdxp<zzbuy> zzdxp) {
        this.zzfje = zzbtv;
        this.zzfdq = zzdxp;
    }

    public static zzbud zzb(zzbtv zzbtv, zzdxp<zzbuy> zzdxp) {
        return new zzbud(zzbtv, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), zzazd.zzdwi), "Cannot return null from a non-@Nullable @Provides method");
    }
}
