package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zznx;
import java.io.IOException;

public interface zznv<T extends zznx> {
    int zza(T t, long j, long j2, IOException iOException);

    void zza(T t, long j, long j2);

    void zza(T t, long j, long j2, boolean z);
}
