package com.google.android.gms.internal.cast;

import com.facebook.ads.AdError;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl;

public abstract class zzcv<R extends Result> extends BaseImplementation$ApiMethodImpl<R, zzdd> {
    public zzcv(GoogleApiClient googleApiClient) {
        super((Api<?>) Cast.b, googleApiClient);
    }

    public final void zzp(int i) {
        setResult(createFailedResult(new Status(AdError.INTERNAL_ERROR_CODE)));
    }
}
