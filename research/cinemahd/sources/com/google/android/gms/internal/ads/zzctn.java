package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Callable;
import org.json.JSONArray;
import org.json.JSONObject;

final /* synthetic */ class zzctn implements Callable {
    private final List zzggn;

    zzctn(List list) {
        this.zzggn = list;
    }

    public final Object call() {
        List<zzdhe> list = this.zzggn;
        JSONArray jSONArray = new JSONArray();
        for (zzdhe zzdhe : list) {
            if (((JSONObject) zzdhe.get()) != null) {
                jSONArray.put(zzdhe.get());
            }
        }
        if (jSONArray.length() == 0) {
            return null;
        }
        return new zzctg(jSONArray.toString());
    }
}
