package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzez extends zzfw {
    public zzez(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 5);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        this.zzzt.zzam(-1);
        this.zzzt.zzan(-1);
        int[] iArr = (int[]) this.zzaae.invoke((Object) null, new Object[]{this.zzuv.getContext()});
        synchronized (this.zzzt) {
            this.zzzt.zzam((long) iArr[0]);
            this.zzzt.zzan((long) iArr[1]);
            if (iArr[2] != Integer.MIN_VALUE) {
                this.zzzt.zzbm((long) iArr[2]);
            }
        }
    }
}
