package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

public final class zzcdg implements zzdxg<Set<zzbsu<zzdcx>>> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<String> zzfsh;
    private final zzdxp<Map<zzdco, zzcdh>> zzfsi;

    public zzcdg(zzdxp<String> zzdxp, zzdxp<Context> zzdxp2, zzdxp<Executor> zzdxp3, zzdxp<Map<zzdco, zzcdh>> zzdxp4) {
        this.zzfsh = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfcv = zzdxp3;
        this.zzfsi = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        Set set;
        String str = this.zzfsh.get();
        Context context = this.zzejv.get();
        Executor executor = this.zzfcv.get();
        Map map = this.zzfsi.get();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnw)).booleanValue()) {
            zzsm zzsm = new zzsm(new zzsr(context));
            zzsm.zza((zzsp) new zzcdi(str));
            set = Collections.singleton(new zzbsu(new zzcdf(zzsm, map), executor));
        } else {
            set = Collections.emptySet();
        }
        return (Set) zzdxm.zza(set, "Cannot return null from a non-@Nullable @Provides method");
    }
}
