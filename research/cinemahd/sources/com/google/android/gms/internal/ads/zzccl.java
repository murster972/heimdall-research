package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzccl implements zzdxg<zzbsu<zzbow>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzccw> zzfdd;

    private zzccl(zzdxp<zzccw> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzccl zzm(zzdxp<zzccw> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzccl(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
