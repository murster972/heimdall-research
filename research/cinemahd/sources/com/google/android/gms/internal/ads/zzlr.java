package com.google.android.gms.internal.ads;

final class zzlr implements Runnable {
    private final /* synthetic */ zzlp zzbat;

    zzlr(zzlp zzlp) {
        this.zzbat = zzlp;
    }

    public final void run() {
        if (!this.zzbat.zzaek) {
            this.zzbat.zzbae.zza(this.zzbat);
        }
    }
}
