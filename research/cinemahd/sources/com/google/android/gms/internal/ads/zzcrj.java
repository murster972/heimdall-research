package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.sdk.AppLovinEventTypes;
import com.vungle.warren.model.ReportDBAdapter;

public final class zzcrj implements zzcub<zzcrg> {
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcrj(zzdhd zzdhd, Context context) {
        this.zzfov = zzdhd;
        this.zzup = context;
    }

    public final zzdhe<zzcrg> zzanc() {
        return this.zzfov.zzd(new zzcri(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcrg zzang() throws Exception {
        double d;
        Intent registerReceiver = this.zzup.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean z = false;
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, -1);
            d = ((double) registerReceiver.getIntExtra(AppLovinEventTypes.USER_COMPLETED_LEVEL, -1)) / ((double) registerReceiver.getIntExtra("scale", -1));
            if (intExtra == 2 || intExtra == 5) {
                z = true;
            }
        } else {
            d = -1.0d;
        }
        return new zzcrg(d, z);
    }
}
