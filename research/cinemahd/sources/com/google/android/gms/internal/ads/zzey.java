package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicReference;

public final class zzey extends zzfw {
    private static zzfv<String> zzzp = new zzfv<>();
    private final Context zzzn;

    public zzey(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2, Context context) {
        super(zzei, str, str2, zzb, i, 29);
        this.zzzn = context;
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        this.zzzt.zzai("E");
        AtomicReference<String> zzav = zzzp.zzav(this.zzzn.getPackageName());
        if (zzav.get() == null) {
            synchronized (zzav) {
                if (zzav.get() == null) {
                    zzav.set((String) this.zzaae.invoke((Object) null, new Object[]{this.zzzn}));
                }
            }
        }
        String str = zzav.get();
        synchronized (this.zzzt) {
            this.zzzt.zzai(zzci.zza(str.getBytes(), true));
        }
    }
}
