package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.NativeContentAd;

public final class zzaeo extends zzadm {
    private final NativeContentAd.OnContentAdLoadedListener zzcwj;

    public zzaeo(NativeContentAd.OnContentAdLoadedListener onContentAdLoadedListener) {
        this.zzcwj = onContentAdLoadedListener;
    }

    public final void zza(zzada zzada) {
        this.zzcwj.onContentAdLoaded(new zzadb(zzada));
    }
}
