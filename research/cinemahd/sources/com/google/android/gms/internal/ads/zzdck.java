package com.google.android.gms.internal.ads;

final /* synthetic */ class zzdck implements zzdgf {
    private final zzdby zzgqf;

    zzdck(zzdby zzdby) {
        this.zzgqf = zzdby;
    }

    public final zzdhe zzf(Object obj) {
        return zzdgs.zzaj(this.zzgqf.apply((Throwable) obj));
    }
}
