package com.google.android.gms.internal.ads;

final class zzbgt implements zzbvl {
    private zzbod zzelr;
    private zzczt zzelt;
    private zzbvi zzelu;
    private final /* synthetic */ zzbgr zzerr;
    private zzbrm zzers;
    private zzcxw zzert;

    private zzbgt(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    public final /* synthetic */ zzbvl zza(zzbvi zzbvi) {
        this.zzelu = (zzbvi) zzdxm.checkNotNull(zzbvi);
        return this;
    }

    /* renamed from: zzadf */
    public final zzbvm zzadg() {
        zzdxm.zza(this.zzers, zzbrm.class);
        zzdxm.zza(this.zzelr, zzbod.class);
        zzdxm.zza(this.zzelu, zzbvi.class);
        return new zzbgs(this.zzerr, this.zzelu, new zzbnb(), new zzdai(), new zzbny(), new zzcee(), this.zzers, this.zzelr, new zzdaq(), this.zzelt, this.zzert);
    }

    public final /* synthetic */ zzbvl zza(zzbod zzbod) {
        this.zzelr = (zzbod) zzdxm.checkNotNull(zzbod);
        return this;
    }

    public final /* synthetic */ zzbvl zza(zzbrm zzbrm) {
        this.zzers = (zzbrm) zzdxm.checkNotNull(zzbrm);
        return this;
    }

    public final /* synthetic */ zzboe zza(zzcxw zzcxw) {
        this.zzert = zzcxw;
        return this;
    }

    public final /* synthetic */ zzboe zza(zzczt zzczt) {
        this.zzelt = zzczt;
        return this;
    }
}
