package com.google.android.gms.internal.ads;

import java.util.List;

abstract class zzdso {
    private static final zzdso zzhoi = new zzdsq();
    private static final zzdso zzhoj = new zzdsp();

    private zzdso() {
    }

    static zzdso zzbax() {
        return zzhoi;
    }

    static zzdso zzbay() {
        return zzhoj;
    }

    /* access modifiers changed from: package-private */
    public abstract <L> List<L> zza(Object obj, long j);

    /* access modifiers changed from: package-private */
    public abstract <L> void zza(Object obj, Object obj2, long j);

    /* access modifiers changed from: package-private */
    public abstract void zzb(Object obj, long j);
}
