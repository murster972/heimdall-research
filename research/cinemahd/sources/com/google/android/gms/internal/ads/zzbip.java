package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.Map;

public final class zzbip implements zzbil {
    private final zzczj zzfbj;

    public zzbip(zzczj zzczj) {
        this.zzfbj = zzczj;
    }

    public final void zzk(Map<String, String> map) {
        String str = map.get("render_in_browser");
        if (!TextUtils.isEmpty(str)) {
            try {
                this.zzfbj.zzbl(Boolean.parseBoolean(str));
            } catch (Exception unused) {
                throw new IllegalStateException("Invalid render_in_browser state");
            }
        }
    }
}
