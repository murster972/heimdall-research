package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;

public abstract class zzbfv extends zzgb implements zzbfs {
    public static zzbfs zzao(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.measurement.IMeasurementManager");
        if (queryLocalInterface instanceof zzbfs) {
            return (zzbfs) queryLocalInterface;
        }
        return new zzbfu(iBinder);
    }
}
