package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.Set;

public final class zzcst implements zzcty<Bundle> {
    private final String zzggc;

    public zzcst(String str) {
        this.zzggc = str;
    }

    /* access modifiers changed from: private */
    public static boolean zzd(Set<String> set) {
        return set.contains("rewarded") || set.contains("interstitial") || set.contains("native") || set.contains("banner");
    }

    public final /* synthetic */ void zzr(Object obj) {
        zzdaa.zza((Bundle) obj, "omid_v", this.zzggc);
    }
}
