package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzcgz implements zzdby {
    static final zzdby zzfun = new zzcgz();

    private zzcgz() {
    }

    public final Object apply(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        zzavs.zzed("Ad request signals:");
        zzavs.zzed(jSONObject.toString(2));
        return jSONObject;
    }
}
