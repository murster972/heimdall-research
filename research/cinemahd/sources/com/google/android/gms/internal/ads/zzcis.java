package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;
import org.json.JSONObject;

public interface zzcis<AdapterT, ListenerT extends zzbpu> {
    zzcip<AdapterT, ListenerT> zzd(String str, JSONObject jSONObject) throws zzdab;
}
