package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzamx extends IInterface {
    void zzdl(String str) throws RemoteException;

    void zztb() throws RemoteException;
}
