package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;

public final class zzcng implements zzcio<zzbkk> {
    private final zzdcr zzfgm;
    private final zzblg zzfyj;
    private final zzdhd zzgbh;
    private final Context zzgbm;
    private final zzaak zzgbn;

    public zzcng(Context context, zzblg zzblg, zzdcr zzdcr, zzdhd zzdhd, zzaak zzaak) {
        this.zzgbm = context;
        this.zzfyj = zzblg;
        this.zzfgm = zzdcr;
        this.zzgbh = zzdhd;
        this.zzgbn = zzaak;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r2.zzglo;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(com.google.android.gms.internal.ads.zzczt r1, com.google.android.gms.internal.ads.zzczl r2) {
        /*
            r0 = this;
            com.google.android.gms.internal.ads.zzaak r1 = r0.zzgbn
            if (r1 == 0) goto L_0x000e
            com.google.android.gms.internal.ads.zzczp r1 = r2.zzglo
            if (r1 == 0) goto L_0x000e
            java.lang.String r1 = r1.zzdht
            if (r1 == 0) goto L_0x000e
            r1 = 1
            return r1
        L_0x000e:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcng.zza(com.google.android.gms.internal.ads.zzczt, com.google.android.gms.internal.ads.zzczl):boolean");
    }

    public final zzdhe<zzbkk> zzb(zzczt zzczt, zzczl zzczl) {
        zzbkj zza = this.zzfyj.zza(new zzbmt(zzczt, zzczl, (String) null), (zzbkn) new zzcnh(this, new View(this.zzgbm), (zzbdi) null, zzcnf.zzgbl, zzczl.zzglq.get(0)));
        zzcnk zzaej = zza.zzaej();
        zzczp zzczp = zzczl.zzglo;
        return this.zzfgm.zzu(zzdco.CUSTOM_RENDER_SYN).zza((zzdcb) new zzcni(this, new zzaad(zzaej, zzczp.zzdhr, zzczp.zzdht)), this.zzgbh).zzw(zzdco.CUSTOM_RENDER_ACK).zzc(zzdgs.zzaj(zza.zzaeh())).zzaqg();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzaad zzaad) throws Exception {
        this.zzgbn.zza(zzaad);
    }
}
