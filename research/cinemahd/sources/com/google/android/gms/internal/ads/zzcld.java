package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzcld extends zzanb {
    private zzcip<zzani, zzcjy> zzfyr;
    private final /* synthetic */ zzclb zzfzy;

    private zzcld(zzclb zzclb, zzcip<zzani, zzcjy> zzcip) {
        this.zzfzy = zzclb;
        this.zzfyr = zzcip;
    }

    public final void zza(zzalr zzalr) throws RemoteException {
        zzalr unused = this.zzfzy.zzfzx = zzalr;
        ((zzcjy) this.zzfyr.zzfyf).onAdLoaded();
    }

    public final void zzdl(String str) throws RemoteException {
        ((zzcjy) this.zzfyr.zzfyf).onAdFailedToLoad(0);
    }
}
