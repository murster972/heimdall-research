package com.google.android.gms.internal.ads;

public abstract class zzdwy {
    public static zzdwy zzn(Class cls) {
        if (System.getProperty("java.vm.name").equalsIgnoreCase("Dalvik")) {
            return new zzdwr(cls.getSimpleName());
        }
        return new zzdwt(cls.getSimpleName());
    }

    public abstract void zzhp(String str);
}
