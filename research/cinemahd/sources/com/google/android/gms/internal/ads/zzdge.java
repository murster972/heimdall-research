package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdfz;
import java.util.List;

abstract class zzdge<V, C> extends zzdfz<V, C> {
    private List<zzdej<V>> zzgwl;

    zzdge(zzdet<? extends zzdhe<? extends V>> zzdet, boolean z) {
        super(zzdet, true, true);
        List<zzdej<V>> list;
        if (zzdet.isEmpty()) {
            list = zzdeu.zzard();
        } else {
            list = zzdfc.zzdz(zzdet.size());
        }
        this.zzgwl = list;
        for (int i = 0; i < zzdet.size(); i++) {
            this.zzgwl.add((Object) null);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdfz.zza zza) {
        super.zza(zza);
        this.zzgwl = null;
    }

    /* access modifiers changed from: package-private */
    public final void zzaro() {
        List<zzdej<V>> list = this.zzgwl;
        if (list != null) {
            set(zzh(list));
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(int i, V v) {
        List<zzdej<V>> list = this.zzgwl;
        if (list != null) {
            list.set(i, zzdej.zzab(v));
        }
    }

    /* access modifiers changed from: package-private */
    public abstract C zzh(List<zzdej<V>> list);
}
