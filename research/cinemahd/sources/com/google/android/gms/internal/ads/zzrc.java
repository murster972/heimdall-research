package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.appopen.AppOpenAdPresentationCallback;

public final class zzrc extends zzrk {
    private final AppOpenAdPresentationCallback zzbqv;

    public zzrc(AppOpenAdPresentationCallback appOpenAdPresentationCallback) {
        this.zzbqv = appOpenAdPresentationCallback;
    }

    public final void onAppOpenAdClosed() {
        this.zzbqv.onAppOpenAdClosed();
    }
}
