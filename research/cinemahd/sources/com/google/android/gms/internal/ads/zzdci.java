package com.google.android.gms.internal.ads;

final /* synthetic */ class zzdci implements zzdgf {
    private final zzdby zzgqf;

    zzdci(zzdby zzdby) {
        this.zzgqf = zzdby;
    }

    public final zzdhe zzf(Object obj) {
        return zzdgs.zzaj(this.zzgqf.apply(obj));
    }
}
