package com.google.android.gms.internal.ads;

final class zzbhg implements zzbus {
    private zzbod zzelr;
    private zzczt zzelt;
    private final /* synthetic */ zzbgr zzerr;
    private zzbrm zzers;
    private zzcxw zzert;
    private zzcns zzexc;

    private zzbhg(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    public final /* synthetic */ zzboe zza(zzcxw zzcxw) {
        this.zzert = zzcxw;
        return this;
    }

    /* renamed from: zzaek */
    public final zzbup zzadg() {
        zzdxm.zza(this.zzers, zzbrm.class);
        zzdxm.zza(this.zzelr, zzbod.class);
        zzdxm.zza(this.zzexc, zzcns.class);
        return new zzbhj(this.zzerr, new zzbnb(), new zzdai(), new zzbny(), new zzcee(), this.zzers, this.zzelr, new zzdaq(), this.zzexc, this.zzelt, this.zzert);
    }

    public final /* synthetic */ zzbus zzb(zzcns zzcns) {
        this.zzexc = (zzcns) zzdxm.checkNotNull(zzcns);
        return this;
    }

    public final /* synthetic */ zzbus zzd(zzbod zzbod) {
        this.zzelr = (zzbod) zzdxm.checkNotNull(zzbod);
        return this;
    }

    public final /* synthetic */ zzboe zza(zzczt zzczt) {
        this.zzelt = zzczt;
        return this;
    }

    public final /* synthetic */ zzbus zzd(zzbrm zzbrm) {
        this.zzers = (zzbrm) zzdxm.checkNotNull(zzbrm);
        return this;
    }
}
