package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class zzapa extends zzgc implements zzaoy {
    zzapa(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.purchase.client.IInAppPurchaseListener");
    }
}
