package com.google.android.gms.internal.ads;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public final class zzdut extends AbstractList<String> implements zzdsl, RandomAccess {
    /* access modifiers changed from: private */
    public final zzdsl zzhrf;

    public zzdut(zzdsl zzdsl) {
        this.zzhrf = zzdsl;
    }

    public final /* synthetic */ Object get(int i) {
        return (String) this.zzhrf.get(i);
    }

    public final Iterator<String> iterator() {
        return new zzduv(this);
    }

    public final ListIterator<String> listIterator(int i) {
        return new zzduw(this, i);
    }

    public final int size() {
        return this.zzhrf.size();
    }

    public final List<?> zzbav() {
        return this.zzhrf.zzbav();
    }

    public final zzdsl zzbaw() {
        return this;
    }

    public final void zzbg(zzdqk zzdqk) {
        throw new UnsupportedOperationException();
    }

    public final Object zzgm(int i) {
        return this.zzhrf.zzgm(i);
    }
}
