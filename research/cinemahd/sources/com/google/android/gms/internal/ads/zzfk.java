package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfk extends zzfw {
    public zzfk(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 3);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        boolean booleanValue = ((Boolean) zzve.zzoy().zzd(zzzn.zzcle)).booleanValue();
        zzdu zzdu = new zzdu((String) this.zzaae.invoke((Object) null, new Object[]{this.zzuv.getContext(), Boolean.valueOf(booleanValue)}));
        synchronized (this.zzzt) {
            this.zzzt.zzal(zzdu.zzwx);
            this.zzzt.zzbn(zzdu.zzwy);
        }
    }
}
