package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdlf;
import com.google.android.gms.internal.ads.zzdlj;
import com.google.android.gms.internal.ads.zzdlv;
import com.google.android.gms.internal.ads.zzdmv;
import java.security.GeneralSecurityException;
import java.util.Arrays;

final class zzdkm implements zzdop {
    private final String zzgzr;
    private final int zzgzs;
    private zzdlv zzgzt;
    private zzdlf zzgzu;
    private int zzgzv;

    zzdkm(zzdng zzdng) throws GeneralSecurityException {
        this.zzgzr = zzdng.zzavi();
        if (this.zzgzr.equals(zzdiy.zzgys)) {
            try {
                zzdlw zzaf = zzdlw.zzaf(zzdng.zzavj());
                this.zzgzt = (zzdlv) zzdit.zzb(zzdng);
                this.zzgzs = zzaf.getKeySize();
            } catch (zzdse e) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesGcmKeyFormat", e);
            }
        } else if (this.zzgzr.equals(zzdiy.zzgyr)) {
            try {
                zzdlg zzx = zzdlg.zzx(zzdng.zzavj());
                this.zzgzu = (zzdlf) zzdit.zzb(zzdng);
                this.zzgzv = zzx.zzate().getKeySize();
                this.zzgzs = this.zzgzv + zzx.zzatf().getKeySize();
            } catch (zzdse e2) {
                throw new GeneralSecurityException("invalid KeyFormat protobuf, expected AesCtrHmacAeadKeyFormat", e2);
            }
        } else {
            String valueOf = String.valueOf(this.zzgzr);
            throw new GeneralSecurityException(valueOf.length() != 0 ? "unsupported AEAD DEM key type: ".concat(valueOf) : new String("unsupported AEAD DEM key type: "));
        }
    }

    public final int zzasr() {
        return this.zzgzs;
    }

    public final zzdhx zzm(byte[] bArr) throws GeneralSecurityException {
        Class<zzdhx> cls = zzdhx.class;
        if (bArr.length != this.zzgzs) {
            throw new GeneralSecurityException("Symmetric key has incorrect length");
        } else if (this.zzgzr.equals(zzdiy.zzgys)) {
            return (zzdhx) zzdit.zza(this.zzgzr, (zzdte) (zzdlv) ((zzdlv.zza) zzdlv.zzatw().zza(this.zzgzt)).zzag(zzdqk.zzi(bArr, 0, this.zzgzs)).zzbaf(), cls);
        } else if (this.zzgzr.equals(zzdiy.zzgyr)) {
            byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, this.zzgzv);
            byte[] copyOfRange2 = Arrays.copyOfRange(bArr, this.zzgzv, this.zzgzs);
            zzdlf.zza zzb = zzdlf.zzatc().zzed(this.zzgzu.getVersion()).zzb((zzdlj) ((zzdlj.zza) zzdlj.zzati().zza(this.zzgzu.zzata())).zzaa(zzdqk.zzu(copyOfRange)).zzbaf());
            return (zzdhx) zzdit.zza(this.zzgzr, (zzdte) (zzdlf) zzb.zzb((zzdmv) ((zzdmv.zza) zzdmv.zzava().zza(this.zzgzu.zzatb())).zzau(zzdqk.zzu(copyOfRange2)).zzbaf()).zzbaf(), cls);
        } else {
            throw new GeneralSecurityException("unknown DEM key type");
        }
    }
}
