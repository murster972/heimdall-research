package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzcwo implements zzdxg<zzcwl> {
    private final zzdxp<zzbfx> zzejt;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<Context> zzgiq;
    private final zzdxp<zzcxt<zzbka, zzbke>> zzgir;
    private final zzdxp<zzcwz> zzgis;
    private final zzdxp<zzczw> zzgit;

    public zzcwo(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzbfx> zzdxp3, zzdxp<zzcxt<zzbka, zzbke>> zzdxp4, zzdxp<zzcwz> zzdxp5, zzdxp<zzczw> zzdxp6) {
        this.zzgiq = zzdxp;
        this.zzfei = zzdxp2;
        this.zzejt = zzdxp3;
        this.zzgir = zzdxp4;
        this.zzgis = zzdxp5;
        this.zzgit = zzdxp6;
    }

    public final /* synthetic */ Object get() {
        return new zzcwl(this.zzgiq.get(), this.zzfei.get(), this.zzejt.get(), this.zzgir.get(), this.zzgis.get(), this.zzgit.get());
    }
}
