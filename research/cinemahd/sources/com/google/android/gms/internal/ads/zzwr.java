package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzwr extends IInterface {
    String getDescription() throws RemoteException;

    String zzph() throws RemoteException;
}
