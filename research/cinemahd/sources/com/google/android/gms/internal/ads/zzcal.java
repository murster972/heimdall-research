package com.google.android.gms.internal.ads;

import org.json.JSONObject;

final /* synthetic */ class zzcal implements zzdgf {
    private final String zzcyr;
    private final zzcaj zzfqj;
    private final JSONObject zzfqk;

    zzcal(zzcaj zzcaj, String str, JSONObject jSONObject) {
        this.zzfqj = zzcaj;
        this.zzcyr = str;
        this.zzfqk = jSONObject;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfqj.zza(this.zzcyr, this.zzfqk, (zzbdi) obj);
    }
}
