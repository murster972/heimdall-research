package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.internal.ads.zzsy;
import java.util.ArrayList;

final class zzcig implements zzdgt<Bundle> {
    private final /* synthetic */ boolean zzfxw;
    final /* synthetic */ zzcid zzfxx;

    zzcig(zzcid zzcid, boolean z) {
        this.zzfxx = zzcid;
        this.zzfxw = z;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        Bundle bundle = (Bundle) obj;
        ArrayList zza = zzcid.zzl(bundle);
        zzsy.zzj.zzc zzb = zzcid.zzk(bundle);
        this.zzfxx.zzfxo.zza(new zzcif(this, this.zzfxw, zza, this.zzfxx.zzj(bundle), zzb));
    }

    public final void zzb(Throwable th) {
        zzayu.zzex("Failed to get signals bundle");
    }
}
