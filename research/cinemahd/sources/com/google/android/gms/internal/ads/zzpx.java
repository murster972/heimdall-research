package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;

final class zzpx implements zzqd {
    private final /* synthetic */ Activity val$activity;

    zzpx(zzpv zzpv, Activity activity) {
        this.val$activity = activity;
    }

    public final void zza(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        activityLifecycleCallbacks.onActivityStarted(this.val$activity);
    }
}
