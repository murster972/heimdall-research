package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.search.SearchAdRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

public final class zzuh {
    public static final zzuh zzccn = new zzuh();

    protected zzuh() {
    }

    public static zzug zza(Context context, zzxj zzxj) {
        List list;
        Context context2;
        zzua zzua;
        String str;
        zzxj zzxj2 = zzxj;
        Date birthday = zzxj.getBirthday();
        long time = birthday != null ? birthday.getTime() : -1;
        String contentUrl = zzxj.getContentUrl();
        int gender = zzxj.getGender();
        Set<String> keywords = zzxj.getKeywords();
        if (!keywords.isEmpty()) {
            list = Collections.unmodifiableList(new ArrayList(keywords));
            context2 = context;
        } else {
            context2 = context;
            list = null;
        }
        boolean isTestDevice = zzxj2.isTestDevice(context2);
        Location location = zzxj.getLocation();
        Bundle networkExtrasBundle = zzxj2.getNetworkExtrasBundle(AdMobAdapter.class);
        if (zzxj.zzpu() != null) {
            zzua = new zzua(zzxj.zzpu().getAdString(), zzve.zzpc().containsKey(zzxj.zzpu().getQueryData()) ? zzve.zzpc().get(zzxj.zzpu().getQueryData()) : "");
        } else {
            zzua = null;
        }
        boolean manualImpressionsEnabled = zzxj.getManualImpressionsEnabled();
        String publisherProvidedId = zzxj.getPublisherProvidedId();
        SearchAdRequest zzpp = zzxj.zzpp();
        zzys zzys = zzpp != null ? new zzys(zzpp) : null;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            String packageName = applicationContext.getPackageName();
            zzve.zzou();
            str = zzayk.zza(Thread.currentThread().getStackTrace(), packageName);
        } else {
            str = null;
        }
        boolean isDesignedForFamilies = zzxj.isDesignedForFamilies();
        RequestConfiguration requestConfiguration = zzxq.zzpw().getRequestConfiguration();
        return new zzug(8, time, networkExtrasBundle, gender, list, isTestDevice, Math.max(zzxj.zzps(), requestConfiguration.getTagForChildDirectedTreatment()), manualImpressionsEnabled, publisherProvidedId, zzys, location, contentUrl, zzxj.zzpr(), zzxj.getCustomTargeting(), Collections.unmodifiableList(new ArrayList(zzxj.zzpt())), zzxj.zzpo(), str, isDesignedForFamilies, zzua, Math.max(zzxj.zzpv(), requestConfiguration.getTagForUnderAgeOfConsent()), (String) Collections.max(Arrays.asList(new String[]{zzxj.getMaxAdContentRating(), requestConfiguration.getMaxAdContentRating()}), zzuk.zzccw), zzxj.zzpn());
    }

    static final /* synthetic */ int zzd(String str, String str2) {
        return RequestConfiguration.zzabs.indexOf(str) - RequestConfiguration.zzabs.indexOf(str2);
    }

    public static zzaru zza(Context context, zzxj zzxj, String str) {
        return new zzaru(zza(context, zzxj), str);
    }
}
