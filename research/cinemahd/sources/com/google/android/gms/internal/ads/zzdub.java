package com.google.android.gms.internal.ads;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class zzdub<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zzhjq;
    private final int zzhql;
    /* access modifiers changed from: private */
    public List<zzduk> zzhqm;
    /* access modifiers changed from: private */
    public Map<K, V> zzhqn;
    private volatile zzdum zzhqo;
    /* access modifiers changed from: private */
    public Map<K, V> zzhqp;
    private volatile zzdug zzhqq;

    private zzdub(int i) {
        this.zzhql = i;
        this.zzhqm = Collections.emptyList();
        this.zzhqn = Collections.emptyMap();
        this.zzhqp = Collections.emptyMap();
    }

    /* access modifiers changed from: private */
    public final void zzbbv() {
        if (this.zzhjq) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzbbw() {
        zzbbv();
        if (this.zzhqn.isEmpty() && !(this.zzhqn instanceof TreeMap)) {
            this.zzhqn = new TreeMap();
            this.zzhqp = ((TreeMap) this.zzhqn).descendingMap();
        }
        return (SortedMap) this.zzhqn;
    }

    static <FieldDescriptorType extends zzdro<FieldDescriptorType>> zzdub<FieldDescriptorType, Object> zzgv(int i) {
        return new zzdue(i);
    }

    /* access modifiers changed from: private */
    public final V zzgx(int i) {
        zzbbv();
        V value = this.zzhqm.remove(i).getValue();
        if (!this.zzhqn.isEmpty()) {
            Iterator it2 = zzbbw().entrySet().iterator();
            this.zzhqm.add(new zzduk(this, (Map.Entry) it2.next()));
            it2.remove();
        }
        return value;
    }

    public void clear() {
        zzbbv();
        if (!this.zzhqm.isEmpty()) {
            this.zzhqm.clear();
        }
        if (!this.zzhqn.isEmpty()) {
            this.zzhqn.clear();
        }
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzhqn.containsKey(comparable);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzhqo == null) {
            this.zzhqo = new zzdum(this, (zzdue) null);
        }
        return this.zzhqo;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdub)) {
            return super.equals(obj);
        }
        zzdub zzdub = (zzdub) obj;
        int size = size();
        if (size != zzdub.size()) {
            return false;
        }
        int zzbbs = zzbbs();
        if (zzbbs != zzdub.zzbbs()) {
            return entrySet().equals(zzdub.entrySet());
        }
        for (int i = 0; i < zzbbs; i++) {
            if (!zzgw(i).equals(zzdub.zzgw(i))) {
                return false;
            }
        }
        if (zzbbs != size) {
            return this.zzhqn.equals(zzdub.zzhqn);
        }
        return true;
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzhqm.get(zza).getValue();
        }
        return this.zzhqn.get(comparable);
    }

    public int hashCode() {
        int zzbbs = zzbbs();
        int i = 0;
        for (int i2 = 0; i2 < zzbbs; i2++) {
            i += this.zzhqm.get(i2).hashCode();
        }
        return this.zzhqn.size() > 0 ? i + this.zzhqn.hashCode() : i;
    }

    public final boolean isImmutable() {
        return this.zzhjq;
    }

    public V remove(Object obj) {
        zzbbv();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return zzgx(zza);
        }
        if (this.zzhqn.isEmpty()) {
            return null;
        }
        return this.zzhqn.remove(comparable);
    }

    public int size() {
        return this.zzhqm.size() + this.zzhqn.size();
    }

    /* renamed from: zza */
    public final V put(K k, V v) {
        zzbbv();
        int zza = zza(k);
        if (zza >= 0) {
            return this.zzhqm.get(zza).setValue(v);
        }
        zzbbv();
        if (this.zzhqm.isEmpty() && !(this.zzhqm instanceof ArrayList)) {
            this.zzhqm = new ArrayList(this.zzhql);
        }
        int i = -(zza + 1);
        if (i >= this.zzhql) {
            return zzbbw().put(k, v);
        }
        int size = this.zzhqm.size();
        int i2 = this.zzhql;
        if (size == i2) {
            zzduk remove = this.zzhqm.remove(i2 - 1);
            zzbbw().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.zzhqm.add(i, new zzduk(this, k, v));
        return null;
    }

    public void zzaxq() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.zzhjq) {
            if (this.zzhqn.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.zzhqn);
            }
            this.zzhqn = map;
            if (this.zzhqp.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.zzhqp);
            }
            this.zzhqp = map2;
            this.zzhjq = true;
        }
    }

    public final int zzbbs() {
        return this.zzhqm.size();
    }

    public final Iterable<Map.Entry<K, V>> zzbbt() {
        if (this.zzhqn.isEmpty()) {
            return zzduf.zzbcd();
        }
        return this.zzhqn.entrySet();
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> zzbbu() {
        if (this.zzhqq == null) {
            this.zzhqq = new zzdug(this, (zzdue) null);
        }
        return this.zzhqq;
    }

    public final Map.Entry<K, V> zzgw(int i) {
        return this.zzhqm.get(i);
    }

    /* synthetic */ zzdub(int i, zzdue zzdue) {
        this(i);
    }

    private final int zza(K k) {
        int size = this.zzhqm.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) this.zzhqm.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo((Comparable) this.zzhqm.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }
}
