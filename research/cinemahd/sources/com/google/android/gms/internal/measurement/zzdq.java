package com.google.android.gms.internal.measurement;

final class zzdq {
    public int zza;
    public long zzb;
    public Object zzc;
    public final zzeq zzd;

    zzdq() {
        this.zzd = zzeq.zza();
    }

    zzdq(zzeq zzeq) {
        if (zzeq != null) {
            this.zzd = zzeq;
            return;
        }
        throw new NullPointerException();
    }
}
