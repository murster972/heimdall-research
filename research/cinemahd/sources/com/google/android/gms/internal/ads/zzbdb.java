package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbdb implements Runnable {
    private final boolean zzdym;
    private final long zzebn;
    private final zzbaz zzedz;

    zzbdb(zzbaz zzbaz, boolean z, long j) {
        this.zzedz = zzbaz;
        this.zzdym = z;
        this.zzebn = j;
    }

    public final void run() {
        this.zzedz.zza(this.zzdym, this.zzebn);
    }
}
