package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.zzad;
import com.google.android.gms.common.internal.Preconditions;
import java.util.HashSet;
import java.util.Set;

public abstract class zzx<T> {
    private T zzjr;
    private boolean zzjs;
    private final Set<zzad> zzjt;
    private SessionManagerListener<CastSession> zzju;

    public zzx() {
        this(true);
    }

    private final void zzc(T t) {
        T t2 = this.zzjr;
        if (t2 != null && t2 == t) {
            zzaz();
            this.zzjr = null;
        }
    }

    private final void zzj(int i) {
        if (zzax() != i) {
            for (zzad a2 : this.zzjt) {
                a2.a();
            }
        }
    }

    public final void zza(zzad zzad) {
        Preconditions.a("Must be called from the main thread.");
        this.zzjt.add(zzad);
    }

    public final int zzax() {
        Preconditions.a("Must be called from the main thread.");
        if (this.zzjr == null) {
            return 3;
        }
        return this.zzjs ? 2 : 1;
    }

    /* access modifiers changed from: protected */
    public void zzay() {
    }

    /* access modifiers changed from: protected */
    public void zzaz() {
    }

    /* access modifiers changed from: package-private */
    public final void zzb(T t) {
        int zzax = zzax();
        zzc(t);
        zzj(zzax);
    }

    public final T zzba() {
        return this.zzjr;
    }

    /* access modifiers changed from: protected */
    public abstract T zzg(CastSession castSession);

    private zzx(boolean z) {
        this.zzjt = new HashSet();
        this.zzju = new zzz(this);
        SessionManager c = CastContext.g().c();
        c.a(this.zzju, CastSession.class);
        CastSession a2 = c.a();
        if (a2 == null) {
            return;
        }
        if (a2.b() || a2.e()) {
            zza(zzg(a2), a2.e());
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(T t, boolean z) {
        int zzax = zzax();
        T t2 = this.zzjr;
        if (t2 == t) {
            this.zzjs = z;
            zzj(zzax);
            return;
        }
        zzc(t2);
        this.zzjr = t;
        this.zzjs = z;
        zzay();
        zzj(zzax);
    }

    /* access modifiers changed from: package-private */
    public final void zza(T t) {
        if (this.zzjr == t) {
            int zzax = zzax();
            this.zzjs = true;
            zzj(zzax);
        }
    }
}
