package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

public interface zzald extends IInterface {
    void destroy() throws RemoteException;

    Bundle getInterstitialAdapterInfo() throws RemoteException;

    zzxb getVideoController() throws RemoteException;

    boolean isInitialized() throws RemoteException;

    void pause() throws RemoteException;

    void resume() throws RemoteException;

    void setImmersiveMode(boolean z) throws RemoteException;

    void showInterstitial() throws RemoteException;

    void showVideo() throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzagp zzagp, List<zzagx> list) throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzarz zzarz, List<String> list) throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzarz zzarz, String str2) throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali) throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali, zzaby zzaby, List<String> list) throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, zzali zzali) throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, String str2, zzali zzali) throws RemoteException;

    void zza(zzug zzug, String str) throws RemoteException;

    void zza(zzug zzug, String str, String str2) throws RemoteException;

    void zzb(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException;

    void zzs(IObjectWrapper iObjectWrapper) throws RemoteException;

    IObjectWrapper zzsk() throws RemoteException;

    zzall zzsl() throws RemoteException;

    zzalq zzsm() throws RemoteException;

    Bundle zzsn() throws RemoteException;

    Bundle zzso() throws RemoteException;

    boolean zzsp() throws RemoteException;

    zzade zzsq() throws RemoteException;

    zzalr zzsr() throws RemoteException;

    void zzt(IObjectWrapper iObjectWrapper) throws RemoteException;
}
