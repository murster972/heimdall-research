package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbsu<T> {
    public Executor executor;
    public T zzfir;

    public zzbsu(T t, Executor executor2) {
        this.zzfir = t;
        this.executor = executor2;
    }

    public static <T> zzbsu<T> zzb(T t, Executor executor2) {
        return new zzbsu<>(t, executor2);
    }
}
