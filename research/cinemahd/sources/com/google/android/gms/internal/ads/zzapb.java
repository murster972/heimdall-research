package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;

public final class zzapb extends zzgb implements zzaoy {
    public static zzaoy zzaf(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.purchase.client.IInAppPurchaseListener");
        if (queryLocalInterface instanceof zzaoy) {
            return (zzaoy) queryLocalInterface;
        }
        return new zzapa(iBinder);
    }
}
