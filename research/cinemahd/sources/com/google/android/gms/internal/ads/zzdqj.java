package com.google.android.gms.internal.ads;

import java.util.NoSuchElementException;

final class zzdqj extends zzdql {
    private final int limit = this.zzhhw.size();
    private int position = 0;
    private final /* synthetic */ zzdqk zzhhw;

    zzdqj(zzdqk zzdqk) {
        this.zzhhw = zzdqk;
    }

    public final boolean hasNext() {
        return this.position < this.limit;
    }

    public final byte nextByte() {
        int i = this.position;
        if (i < this.limit) {
            this.position = i + 1;
            return this.zzhhw.zzff(i);
        }
        throw new NoSuchElementException();
    }
}
