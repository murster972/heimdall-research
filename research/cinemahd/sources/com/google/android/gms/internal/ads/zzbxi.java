package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdAssetNames;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public final class zzbxi extends zzacs implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener, zzbxz {
    private zzbwk zzfnf;
    private zzpo zzfng;
    private final WeakReference<View> zzfnj;
    private final Map<String, WeakReference<View>> zzfnk = new HashMap();
    private final Map<String, WeakReference<View>> zzfnl = new HashMap();
    private final Map<String, WeakReference<View>> zzfnm = new HashMap();

    public zzbxi(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        view.setOnTouchListener(this);
        view.setOnClickListener(this);
        zzq.zzln();
        zzazt.zza(view, (ViewTreeObserver.OnGlobalLayoutListener) this);
        zzq.zzln();
        zzazt.zza(view, (ViewTreeObserver.OnScrollChangedListener) this);
        this.zzfnj = new WeakReference<>(view);
        for (Map.Entry next : hashMap.entrySet()) {
            String str = (String) next.getKey();
            View view2 = (View) next.getValue();
            if (view2 != null) {
                this.zzfnk.put(str, new WeakReference(view2));
                if (!NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str) && !UnifiedNativeAdAssetNames.ASSET_ADCHOICES_CONTAINER_VIEW.equals(str)) {
                    view2.setOnTouchListener(this);
                    view2.setClickable(true);
                    view2.setOnClickListener(this);
                }
            }
        }
        this.zzfnm.putAll(this.zzfnk);
        for (Map.Entry next2 : hashMap2.entrySet()) {
            View view3 = (View) next2.getValue();
            if (view3 != null) {
                this.zzfnl.put((String) next2.getKey(), new WeakReference(view3));
                view3.setOnTouchListener(this);
                view3.setClickable(false);
            }
        }
        this.zzfnm.putAll(this.zzfnl);
        this.zzfng = new zzpo(view.getContext(), view);
    }

    public final synchronized void onClick(View view) {
        if (this.zzfnf != null) {
            this.zzfnf.zza(view, zzaga(), zzajz(), zzaka(), true);
        }
    }

    public final synchronized void onGlobalLayout() {
        if (this.zzfnf != null) {
            this.zzfnf.zzb(zzaga(), zzajz(), zzaka(), zzbwk.zzy(zzaga()));
        }
    }

    public final synchronized void onScrollChanged() {
        if (this.zzfnf != null) {
            this.zzfnf.zzb(zzaga(), zzajz(), zzaka(), zzbwk.zzy(zzaga()));
        }
    }

    public final synchronized boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.zzfnf != null) {
            this.zzfnf.zza(view, motionEvent, zzaga());
        }
        return false;
    }

    public final synchronized void unregisterNativeAd() {
        if (this.zzfnf != null) {
            this.zzfnf.zzb(this);
            this.zzfnf = null;
        }
    }

    public final synchronized void zza(IObjectWrapper iObjectWrapper) {
        Object a2 = ObjectWrapper.a(iObjectWrapper);
        if (!(a2 instanceof zzbwk)) {
            zzayu.zzez("Not an instance of InternalNativeAd. This is most likely a transient error");
            return;
        }
        if (this.zzfnf != null) {
            this.zzfnf.zzb(this);
        }
        if (((zzbwk) a2).zzaiv()) {
            this.zzfnf = (zzbwk) a2;
            this.zzfnf.zza((zzbxz) this);
            this.zzfnf.zzz(zzaga());
            return;
        }
        zzayu.zzex("Your account must be enabled to use this feature. Talk to your account manager to request this feature for your account.");
    }

    public final View zzaga() {
        return (View) this.zzfnj.get();
    }

    public final synchronized Map<String, WeakReference<View>> zzajz() {
        return this.zzfnm;
    }

    public final synchronized Map<String, WeakReference<View>> zzaka() {
        return this.zzfnk;
    }

    public final synchronized Map<String, WeakReference<View>> zzakb() {
        return this.zzfnl;
    }

    public final synchronized String zzakc() {
        return NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE;
    }

    public final FrameLayout zzakd() {
        return null;
    }

    public final zzpo zzake() {
        return this.zzfng;
    }

    public final synchronized IObjectWrapper zzakf() {
        return null;
    }

    public final synchronized void zze(IObjectWrapper iObjectWrapper) {
        if (this.zzfnf != null) {
            Object a2 = ObjectWrapper.a(iObjectWrapper);
            if (!(a2 instanceof View)) {
                zzayu.zzez("Calling NativeAdViewHolderNonagonDelegate.setClickConfirmingView with wrong wrapped object");
            }
            this.zzfnf.setClickConfirmingView((View) a2);
        }
    }

    public final synchronized View zzgb(String str) {
        WeakReference weakReference = this.zzfnm.get(str);
        if (weakReference == null) {
            return null;
        }
        return (View) weakReference.get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0046, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zza(java.lang.String r2, android.view.View r3, boolean r4) {
        /*
            r1 = this;
            monitor-enter(r1)
            if (r3 != 0) goto L_0x0014
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r1.zzfnm     // Catch:{ all -> 0x0047 }
            r3.remove(r2)     // Catch:{ all -> 0x0047 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r1.zzfnk     // Catch:{ all -> 0x0047 }
            r3.remove(r2)     // Catch:{ all -> 0x0047 }
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r1.zzfnl     // Catch:{ all -> 0x0047 }
            r3.remove(r2)     // Catch:{ all -> 0x0047 }
            monitor-exit(r1)
            return
        L_0x0014:
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r1.zzfnm     // Catch:{ all -> 0x0047 }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0047 }
            r0.<init>(r3)     // Catch:{ all -> 0x0047 }
            r4.put(r2, r0)     // Catch:{ all -> 0x0047 }
            java.lang.String r4 = "1098"
            boolean r4 = r4.equals(r2)     // Catch:{ all -> 0x0047 }
            if (r4 != 0) goto L_0x0045
            java.lang.String r4 = "3011"
            boolean r4 = r4.equals(r2)     // Catch:{ all -> 0x0047 }
            if (r4 == 0) goto L_0x002f
            goto L_0x0045
        L_0x002f:
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r1.zzfnk     // Catch:{ all -> 0x0047 }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0047 }
            r0.<init>(r3)     // Catch:{ all -> 0x0047 }
            r4.put(r2, r0)     // Catch:{ all -> 0x0047 }
            r2 = 1
            r3.setClickable(r2)     // Catch:{ all -> 0x0047 }
            r3.setOnClickListener(r1)     // Catch:{ all -> 0x0047 }
            r3.setOnTouchListener(r1)     // Catch:{ all -> 0x0047 }
            monitor-exit(r1)
            return
        L_0x0045:
            monitor-exit(r1)
            return
        L_0x0047:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbxi.zza(java.lang.String, android.view.View, boolean):void");
    }
}
