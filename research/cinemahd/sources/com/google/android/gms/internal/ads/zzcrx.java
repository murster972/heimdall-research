package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcrx implements zzcub<zzcru> {
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcrx(Context context, zzdhd zzdhd) {
        this.zzup = context;
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcru> zzanc() {
        return this.zzfov.zzd(new zzcrw(this));
    }
}
