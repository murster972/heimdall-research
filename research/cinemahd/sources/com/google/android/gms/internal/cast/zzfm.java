package com.google.android.gms.internal.cast;

import android.view.animation.Interpolator;
import androidx.core.view.animation.PathInterpolatorCompat;

final class zzfm {
    /* access modifiers changed from: private */
    public static final Interpolator zzabt = PathInterpolatorCompat.a(0.0f, 0.0f, 0.2f, 1.0f);
    /* access modifiers changed from: private */
    public static final Interpolator zzabu = PathInterpolatorCompat.a(0.4f, 0.0f, 1.0f, 1.0f);
    /* access modifiers changed from: private */
    public static final Interpolator zzabv = PathInterpolatorCompat.a(0.4f, 0.0f, 0.2f, 1.0f);
}
