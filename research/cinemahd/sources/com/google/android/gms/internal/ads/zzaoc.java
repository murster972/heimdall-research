package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.content.Intent;
import com.google.android.gms.ads.internal.zzq;

final class zzaoc implements DialogInterface.OnClickListener {
    private final /* synthetic */ zzanz zzdff;

    zzaoc(zzanz zzanz) {
        this.zzdff = zzanz;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent createIntent = this.zzdff.createIntent();
        zzq.zzkq();
        zzawb.zza(this.zzdff.zzup, createIntent);
    }
}
