package com.google.android.gms.internal.ads;

import android.database.sqlite.SQLiteDatabase;

final /* synthetic */ class zzcij implements zzdby {
    private final zzcik zzfxz;

    zzcij(zzcik zzcik) {
        this.zzfxz = zzcik;
    }

    public final Object apply(Object obj) {
        return this.zzfxz.zzb((SQLiteDatabase) obj);
    }
}
