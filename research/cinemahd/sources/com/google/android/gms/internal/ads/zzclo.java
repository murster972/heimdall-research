package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbod;

public final class zzclo implements zzdxg<zzcll> {
    private final zzdxp<zzbfx> zzfkr;
    private final zzdxp<zzbod.zza> zzfks;
    private final zzdxp<zzbrm> zzfkt;
    private final zzdxp<zzcns> zzgab;

    public zzclo(zzdxp<zzbfx> zzdxp, zzdxp<zzbod.zza> zzdxp2, zzdxp<zzcns> zzdxp3, zzdxp<zzbrm> zzdxp4) {
        this.zzfkr = zzdxp;
        this.zzfks = zzdxp2;
        this.zzgab = zzdxp3;
        this.zzfkt = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcll(this.zzfkr.get(), this.zzfks.get(), this.zzgab.get(), this.zzfkt.get());
    }
}
