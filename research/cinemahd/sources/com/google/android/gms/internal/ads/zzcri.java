package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcri implements Callable {
    private final zzcrj zzgfl;

    zzcri(zzcrj zzcrj) {
        this.zzgfl = zzcrj;
    }

    public final Object call() {
        return this.zzgfl.zzang();
    }
}
