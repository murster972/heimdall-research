package com.google.android.gms.internal.ads;

final /* synthetic */ class zzccv implements zzsp {
    private final zzczt zzfhw;

    zzccv(zzczt zzczt) {
        this.zzfhw = zzczt;
    }

    public final void zza(zztu zztu) {
        zzczt zzczt = this.zzfhw;
        zztu.zzcay.zzbzw.zzbzo = zzczt.zzgmi.zzgmf.zzbzo;
    }
}
