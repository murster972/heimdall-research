package com.google.android.gms.internal.ads;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzru implements BaseGmsClient.BaseOnConnectionFailedListener {
    private final /* synthetic */ zzrq zzbrh;

    zzru(zzrq zzrq) {
        this.zzbrh = zzrq;
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        synchronized (this.zzbrh.lock) {
            zzsd unused = this.zzbrh.zzbre = null;
            if (this.zzbrh.zzbrd != null) {
                zzrz unused2 = this.zzbrh.zzbrd = null;
            }
            this.zzbrh.lock.notifyAll();
        }
    }
}
