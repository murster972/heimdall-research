package com.google.android.gms.internal.ads;

final /* synthetic */ class zzauc implements zzaui {
    static final zzaui zzdpr = new zzauc();

    private zzauc() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return zzbfq.getGmpAppId();
    }
}
