package com.google.android.gms.internal.cast;

import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.zzab;
import com.google.android.gms.cast.framework.zzh;
import com.google.android.gms.cast.framework.zzj;
import com.google.android.gms.cast.framework.zzl;
import com.google.android.gms.cast.framework.zzr;
import com.google.android.gms.cast.framework.zzt;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.DynamiteModule;
import java.util.Map;

public final class zze {
    private static final zzdw zzbf = new zzdw("CastDynamiteModule");

    public static zzj zza(Context context, CastOptions castOptions, zzj zzj, Map<String, IBinder> map) {
        try {
            return zzf(context).zza(ObjectWrapper.a(context.getApplicationContext()), castOptions, zzj, map);
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "newCastContextImpl", zzh.class.getSimpleName());
            return null;
        }
    }

    private static zzh zzf(Context context) {
        try {
            IBinder a2 = DynamiteModule.a(context, DynamiteModule.i, "com.google.android.gms.cast.framework.dynamite").a("com.google.android.gms.cast.framework.internal.CastDynamiteModuleImpl");
            if (a2 == null) {
                return null;
            }
            IInterface queryLocalInterface = a2.queryLocalInterface("com.google.android.gms.cast.framework.internal.ICastDynamiteModule");
            if (queryLocalInterface instanceof zzh) {
                return (zzh) queryLocalInterface;
            }
            return new zzi(a2);
        } catch (DynamiteModule.LoadingException e) {
            throw new RuntimeException(e);
        }
    }

    public static zzt zza(Context context, String str, String str2, zzab zzab) {
        try {
            return zzf(context).zza(str, str2, zzab);
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "newSessionImpl", zzh.class.getSimpleName());
            return null;
        }
    }

    public static zzl zza(Context context, CastOptions castOptions, IObjectWrapper iObjectWrapper, zzh zzh) {
        try {
            return zzf(context).zza(castOptions, iObjectWrapper, zzh);
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "newCastSessionImpl", zzh.class.getSimpleName());
            return null;
        }
    }

    public static zzr zza(Service service, IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) {
        try {
            return zzf(service.getApplicationContext()).zza(ObjectWrapper.a(service), iObjectWrapper, iObjectWrapper2);
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "newReconnectionServiceImpl", zzh.class.getSimpleName());
            return null;
        }
    }

    public static zzah zza(Context context, AsyncTask<Uri, Long, Bitmap> asyncTask, zzaj zzaj, int i, int i2, boolean z, long j, int i3, int i4, int i5) {
        try {
            return zzf(context.getApplicationContext()).zza(ObjectWrapper.a(asyncTask), zzaj, i, i2, z, PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE, 5, 333, 10000);
        } catch (RemoteException e) {
            zzbf.zza(e, "Unable to call %s on %s.", "newFetchBitmapTaskImpl", zzh.class.getSimpleName());
            return null;
        }
    }
}
