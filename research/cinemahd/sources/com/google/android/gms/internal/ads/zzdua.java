package com.google.android.gms.internal.ads;

import java.io.IOException;

interface zzdua<T> {
    boolean equals(T t, T t2);

    int hashCode(T t);

    T newInstance();

    void zza(T t, zzdtu zzdtu, zzdrg zzdrg) throws IOException;

    void zza(T t, zzdvl zzdvl) throws IOException;

    void zza(T t, byte[] bArr, int i, int i2, zzdqf zzdqf) throws IOException;

    void zzan(T t);

    int zzax(T t);

    boolean zzaz(T t);

    void zzf(T t, T t2);
}
