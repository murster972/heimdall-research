package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.mediation.Adapter;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzamc extends zzalh {
    private final zzarz zzddp;
    private final Adapter zzddy;

    zzamc(Adapter adapter, zzarz zzarz) {
        this.zzddy = adapter;
        this.zzddp = zzarz;
    }

    public final void onAdClicked() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzak(ObjectWrapper.a(this.zzddy));
        }
    }

    public final void onAdClosed() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzaj(ObjectWrapper.a(this.zzddy));
        }
    }

    public final void onAdFailedToLoad(int i) throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zze(ObjectWrapper.a(this.zzddy), i);
        }
    }

    public final void onAdImpression() throws RemoteException {
    }

    public final void onAdLeftApplication() throws RemoteException {
    }

    public final void onAdLoaded() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzag(ObjectWrapper.a(this.zzddy));
        }
    }

    public final void onAdOpened() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzah(ObjectWrapper.a(this.zzddy));
        }
    }

    public final void onAppEvent(String str, String str2) throws RemoteException {
    }

    public final void onVideoEnd() throws RemoteException {
    }

    public final void onVideoPause() throws RemoteException {
    }

    public final void onVideoPlay() throws RemoteException {
    }

    public final void zza(zzade zzade, String str) throws RemoteException {
    }

    public final void zza(zzalj zzalj) throws RemoteException {
    }

    public final void zza(zzasf zzasf) throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zza(ObjectWrapper.a(this.zzddy), new zzasd(zzasf.getType(), zzasf.getAmount()));
        }
    }

    public final void zzb(Bundle bundle) throws RemoteException {
    }

    public final void zzb(zzasd zzasd) throws RemoteException {
    }

    public final void zzco(int i) throws RemoteException {
    }

    public final void zzdj(String str) throws RemoteException {
    }

    public final void zzss() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzai(ObjectWrapper.a(this.zzddy));
        }
    }

    public final void zzst() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzam(ObjectWrapper.a(this.zzddy));
        }
    }
}
