package com.google.android.gms.internal.cast;

import android.graphics.Bitmap;

final class zzay implements zzab {
    private final /* synthetic */ zzax zzsn;

    zzay(zzax zzax) {
        this.zzsn = zzax;
    }

    public final void zza(Bitmap bitmap) {
        if (bitmap != null) {
            if (this.zzsn.zzsm != null) {
                this.zzsn.zzsm.setVisibility(4);
            }
            this.zzsn.zzsi.setVisibility(0);
            this.zzsn.zzsi.setImageBitmap(bitmap);
        }
    }
}
