package com.google.android.gms.internal.ads;

import java.lang.Thread;

final class zzapp implements Thread.UncaughtExceptionHandler {
    private final /* synthetic */ zzapn zzdil;
    private final /* synthetic */ Thread.UncaughtExceptionHandler zzdim;

    zzapp(zzapn zzapn, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.zzdil = zzapn;
        this.zzdim = uncaughtExceptionHandler;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            this.zzdil.zza(thread, th);
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.zzdim;
            if (uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, th);
            }
        } catch (Throwable th2) {
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler2 = this.zzdim;
            if (uncaughtExceptionHandler2 != null) {
                uncaughtExceptionHandler2.uncaughtException(thread, th);
            }
            throw th2;
        }
    }
}
