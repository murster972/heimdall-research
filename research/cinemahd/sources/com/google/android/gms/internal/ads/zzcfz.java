package com.google.android.gms.internal.ads;

import java.util.concurrent.TimeoutException;

final /* synthetic */ class zzcfz implements zzdgf {
    static final zzdgf zzbkw = new zzcfz();

    private zzcfz() {
    }

    public final zzdhe zzf(Object obj) {
        TimeoutException timeoutException = (TimeoutException) obj;
        return zzdgs.zzk(new zzcgt("Timed out waiting for ad response.", 2));
    }
}
