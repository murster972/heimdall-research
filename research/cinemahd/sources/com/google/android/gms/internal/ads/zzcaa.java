package com.google.android.gms.internal.ads;

public final class zzcaa implements zzdxg<zzbzv> {
    private final zzdxp<zzbws> zzfkx;
    private final zzdxp<zzbwk> zzfqc;

    private zzcaa(zzdxp<zzbwk> zzdxp, zzdxp<zzbws> zzdxp2) {
        this.zzfqc = zzdxp;
        this.zzfkx = zzdxp2;
    }

    public static zzcaa zzl(zzdxp<zzbwk> zzdxp, zzdxp<zzbws> zzdxp2) {
        return new zzcaa(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzbzv(this.zzfqc.get(), this.zzfkx.get());
    }
}
