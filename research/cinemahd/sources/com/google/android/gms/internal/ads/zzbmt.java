package com.google.android.gms.internal.ads;

public final class zzbmt {
    private final zzczt zzelt;
    private final zzczl zzfbs;
    private final String zzfge;

    public zzbmt(zzczt zzczt, zzczl zzczl, String str) {
        this.zzelt = zzczt;
        this.zzfbs = zzczl;
        this.zzfge = str == null ? "com.google.ads.mediation.admob.AdMobAdapter" : str;
    }

    public final zzczt zzagw() {
        return this.zzelt;
    }

    public final zzczl zzagx() {
        return this.zzfbs;
    }

    public final String zzagy() {
        return this.zzfge;
    }
}
