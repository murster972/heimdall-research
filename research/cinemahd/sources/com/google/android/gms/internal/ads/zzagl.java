package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzagl implements BaseGmsClient.BaseConnectionCallbacks {
    private final /* synthetic */ zzazl zzbrs;
    private final /* synthetic */ zzagh zzcyb;

    zzagl(zzagh zzagh, zzazl zzazl) {
        this.zzcyb = zzagh;
        this.zzbrs = zzazl;
    }

    public final void onConnected(Bundle bundle) {
        try {
            this.zzbrs.set(this.zzcyb.zzcxz.zzru());
        } catch (DeadObjectException e) {
            this.zzbrs.setException(e);
        }
    }

    public final void onConnectionSuspended(int i) {
        zzazl zzazl = this.zzbrs;
        StringBuilder sb = new StringBuilder(34);
        sb.append("onConnectionSuspended: ");
        sb.append(i);
        zzazl.setException(new RuntimeException(sb.toString()));
    }
}
