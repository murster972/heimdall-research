package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzsb implements Parcelable.Creator<zzry> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        Bundle bundle = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    str = SafeParcelReader.o(parcel2, a2);
                    break;
                case 3:
                    j = SafeParcelReader.y(parcel2, a2);
                    break;
                case 4:
                    str2 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 5:
                    str3 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 6:
                    str4 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 7:
                    bundle = SafeParcelReader.f(parcel2, a2);
                    break;
                case 8:
                    z = SafeParcelReader.s(parcel2, a2);
                    break;
                case 9:
                    j2 = SafeParcelReader.y(parcel2, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel2, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel2, b);
        return new zzry(str, j, str2, str3, str4, bundle, z, j2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzry[i];
    }
}
