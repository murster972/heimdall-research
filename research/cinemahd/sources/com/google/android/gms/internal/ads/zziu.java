package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

final class zziu implements Parcelable.Creator<zziv> {
    zziu() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zziv(parcel);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zziv[i];
    }
}
