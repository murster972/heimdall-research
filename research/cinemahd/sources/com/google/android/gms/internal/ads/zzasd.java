package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import org.json.JSONArray;
import org.json.JSONException;

public final class zzasd extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzasd> CREATOR = new zzasc();
    public final String type;
    public final int zzdno;

    public zzasd(RewardItem rewardItem) {
        this(rewardItem.getType(), rewardItem.getAmount());
    }

    public static zzasd zza(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null || jSONArray.length() == 0) {
            return null;
        }
        return new zzasd(jSONArray.getJSONObject(0).optString("rb_type"), jSONArray.getJSONObject(0).optInt("rb_amount"));
    }

    public final boolean equals(Object obj) {
        if (obj != null && (obj instanceof zzasd)) {
            zzasd zzasd = (zzasd) obj;
            if (!Objects.a(this.type, zzasd.type) || !Objects.a(Integer.valueOf(this.zzdno), Integer.valueOf(zzasd.zzdno))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return Objects.a(this.type, Integer.valueOf(this.zzdno));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.type, false);
        SafeParcelWriter.a(parcel, 3, this.zzdno);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzasd(String str, int i) {
        this.type = str;
        this.zzdno = i;
    }
}
