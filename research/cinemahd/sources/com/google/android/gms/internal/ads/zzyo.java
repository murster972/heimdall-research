package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzyo implements Runnable {
    private final /* synthetic */ zzyl zzcfr;

    zzyo(zzyl zzyl) {
        this.zzcfr = zzyl;
    }

    public final void run() {
        if (this.zzcfr.zzcfo != null) {
            try {
                this.zzcfr.zzcfo.onRewardedVideoAdFailedToLoad(1);
            } catch (RemoteException e) {
                zzayu.zzd("Could not notify onRewardedVideoAdFailedToLoad event.", e);
            }
        }
    }
}
