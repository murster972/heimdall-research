package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzvs extends IInterface {
    IBinder zzc(IObjectWrapper iObjectWrapper, String str, zzalc zzalc, int i) throws RemoteException;
}
