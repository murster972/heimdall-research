package com.google.android.gms.internal.ads;

final class zzpf implements Runnable {
    private final /* synthetic */ zzit zzahe;
    private final /* synthetic */ zzpg zzbjg;

    zzpf(zzpg zzpg, zzit zzit) {
        this.zzbjg = zzpg;
        this.zzahe = zzit;
    }

    public final void run() {
        this.zzbjg.zzbjh.zze(this.zzahe);
    }
}
