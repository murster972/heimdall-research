package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzctr implements Callable {
    private final zzcto zzggs;

    zzctr(zzcto zzcto) {
        this.zzggs = zzcto;
    }

    public final Object call() {
        return this.zzggs.zzano();
    }
}
