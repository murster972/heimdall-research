package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.AdMetadataListener;

final class zzczh extends AdMetadataListener {
    private final /* synthetic */ zzczf zzgla;
    private final /* synthetic */ zzvx zzglb;

    zzczh(zzczf zzczf, zzvx zzvx) {
        this.zzgla = zzczf;
        this.zzglb = zzvx;
    }

    public final void onAdMetadataChanged() {
        if (this.zzgla.zzgky != null) {
            try {
                this.zzglb.onAdMetadataChanged();
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }
}
