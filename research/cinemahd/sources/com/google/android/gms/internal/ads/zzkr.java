package com.google.android.gms.internal.ads;

public final class zzkr {
    private final boolean zzaxf;
    private final int zzaxg;
    private final byte[] zzaxh;

    public zzkr(boolean z, int i, byte[] bArr) {
        this.zzaxf = z;
        this.zzaxg = i;
        this.zzaxh = bArr;
    }
}
