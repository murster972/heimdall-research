package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

public final class zzcat implements Callable<zzcaj> {
    /* access modifiers changed from: private */
    public final zzbdr zzbmj;
    /* access modifiers changed from: private */
    public final zzazb zzdij;
    /* access modifiers changed from: private */
    public final zzdq zzefv;
    /* access modifiers changed from: private */
    public final Executor zzfci;
    /* access modifiers changed from: private */
    public final zza zzfpj;
    /* access modifiers changed from: private */
    public final Context zzup;

    public zzcat(Context context, Executor executor, zzdq zzdq, zzazb zzazb, zza zza, zzbdr zzbdr) {
        this.zzup = context;
        this.zzfci = executor;
        this.zzefv = zzdq;
        this.zzdij = zzazb;
        this.zzfpj = zza;
        this.zzbmj = zzbdr;
    }

    public final /* synthetic */ Object call() throws Exception {
        zzcaj zzcaj = new zzcaj(this);
        zzcaj.zzakq();
        return zzcaj;
    }
}
