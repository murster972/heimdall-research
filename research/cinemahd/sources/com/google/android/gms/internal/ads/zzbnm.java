package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbnm implements zzdxg<zzbsu<zzbsz>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbtc> zzfdq;

    private zzbnm(zzdxp<zzbtc> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdq = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzbnm zzd(zzdxp<zzbtc> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzbnm(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
