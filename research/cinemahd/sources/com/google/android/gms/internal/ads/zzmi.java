package com.google.android.gms.internal.ads;

import java.io.IOException;

public final class zzmi extends IOException {
    private final int zzbcg;

    public zzmi(int i) {
        this.zzbcg = i;
    }
}
