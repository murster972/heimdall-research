package com.google.android.gms.internal.cast;

final class zzdy implements zzec {
    private final /* synthetic */ zzec zzaap;
    private final /* synthetic */ zzdx zzaaq;

    zzdy(zzdx zzdx, zzec zzec) {
        this.zzaaq = zzdx;
        this.zzaap = zzec;
    }

    public final void zza(long j, int i, Object obj) {
        Long unused = this.zzaaq.zzzu = null;
        zzec zzec = this.zzaap;
        if (zzec != null) {
            zzec.zza(j, i, obj);
        }
    }

    public final void zzd(long j) {
        zzec zzec = this.zzaap;
        if (zzec != null) {
            zzec.zzd(j);
        }
    }
}
