package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcmk implements Runnable {
    private final zzccd zzfzn;

    private zzcmk(zzccd zzccd) {
        this.zzfzn = zzccd;
    }

    static Runnable zza(zzccd zzccd) {
        return new zzcmk(zzccd);
    }

    public final void run() {
        this.zzfzn.zzakx();
    }
}
