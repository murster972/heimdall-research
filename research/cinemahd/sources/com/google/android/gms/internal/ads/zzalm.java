package com.google.android.gms.internal.ads;

import android.os.IBinder;

public final class zzalm extends zzgc implements zzalj {
    zzalm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.IMediationResponseMetadata");
    }
}
