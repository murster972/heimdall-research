package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbpv implements zzdxg<zzbpm> {
    private final zzdxp<Set<zzbsu<zzbov>>> zzfeo;

    private zzbpv(zzdxp<Set<zzbsu<zzbov>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbpv zzj(zzdxp<Set<zzbsu<zzbov>>> zzdxp) {
        return new zzbpv(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbpm(this.zzfeo.get());
    }
}
