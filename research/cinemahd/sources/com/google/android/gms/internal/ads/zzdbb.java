package com.google.android.gms.internal.ads;

public interface zzdbb {
    zzdbi<?> zza(zzdbl zzdbl);

    zzdbl zza(zzug zzug, String str, zzuo zzuo);

    boolean zza(zzdbl zzdbl, zzdbi<?> zzdbi);

    boolean zzb(zzdbl zzdbl);
}
