package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzcpr implements zzdgf<zzaqk, zzcps> {
    private Executor executor;
    private zzcgm zzgef;

    public zzcpr(Executor executor2, zzcgm zzcgm) {
        this.executor = executor2;
        this.zzgef = zzcgm;
    }

    public final /* synthetic */ zzdhe zzf(Object obj) throws Exception {
        zzaqk zzaqk = (zzaqk) obj;
        return zzdgs.zzb(this.zzgef.zzg(zzaqk), new zzcpq(zzaqk), this.executor);
    }
}
