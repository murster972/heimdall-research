package com.google.android.gms.internal.ads;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class zzdtp {
    private static final zzdtp zzhpq = new zzdtp();
    private final zzdtz zzhpr = new zzdsr();
    private final ConcurrentMap<Class<?>, zzdua<?>> zzhps = new ConcurrentHashMap();

    private zzdtp() {
    }

    public static zzdtp zzbbm() {
        return zzhpq;
    }

    public final <T> zzdua<T> zzba(T t) {
        return zzh(t.getClass());
    }

    public final <T> zzdua<T> zzh(Class<T> cls) {
        zzdrv.zza(cls, "messageType");
        zzdua<T> zzdua = (zzdua) this.zzhps.get(cls);
        if (zzdua != null) {
            return zzdua;
        }
        zzdua<T> zzg = this.zzhpr.zzg(cls);
        zzdrv.zza(cls, "messageType");
        zzdrv.zza(zzg, "schema");
        zzdua<T> putIfAbsent = this.zzhps.putIfAbsent(cls, zzg);
        return putIfAbsent != null ? putIfAbsent : zzg;
    }
}
