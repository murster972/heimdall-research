package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.facebook.ads.AudienceNetworkActivity;
import java.util.Map;

final /* synthetic */ class zzbxw implements zzafn {
    private final zzbxr zzfof;

    zzbxw(zzbxr zzbxr) {
        this.zzfof = zzbxr;
    }

    public final void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        zzbdi.zzaaa().zza((zzbeu) new zzbxx(this.zzfof, map));
        String str = (String) map.get("overlayHtml");
        String str2 = (String) map.get("baseUrl");
        if (TextUtils.isEmpty(str2)) {
            zzbdi.loadData(str, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8");
        } else {
            zzbdi.loadDataWithBaseURL(str2, str, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8", (String) null);
        }
    }
}
