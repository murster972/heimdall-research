package com.google.android.gms.internal.ads;

final class zzbmr implements zzdgt<zzbmj> {
    private final /* synthetic */ zzdgt zzfgc;
    private final /* synthetic */ zzbmo zzfgd;

    zzbmr(zzbmo zzbmo, zzdgt zzdgt) {
        this.zzfgd = zzbmo;
        this.zzfgc = zzdgt;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        this.zzfgd.zza(((zzbmj) obj).zzffu, this.zzfgc);
    }

    public final void zzb(Throwable th) {
        this.zzfgc.zzb(th);
        this.zzfgd.zzagu();
    }
}
