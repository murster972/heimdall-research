package com.google.android.gms.internal.ads;

import android.util.Base64OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

final class zzqt {
    private ByteArrayOutputStream zzbqk = new ByteArrayOutputStream(4096);
    private Base64OutputStream zzbql = new Base64OutputStream(this.zzbqk, 10);

    public final String toString() {
        try {
            this.zzbql.close();
        } catch (IOException e) {
            zzayu.zzc("HashManager: Unable to convert to Base64.", e);
        }
        try {
            this.zzbqk.close();
            return this.zzbqk.toString();
        } catch (IOException e2) {
            zzayu.zzc("HashManager: Unable to convert to Base64.", e2);
            return "";
        } finally {
            this.zzbqk = null;
            this.zzbql = null;
        }
    }

    public final void write(byte[] bArr) throws IOException {
        this.zzbql.write(bArr);
    }
}
