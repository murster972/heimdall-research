package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class zzqf {
    private final Object lock = new Object();
    private int zzbpb;
    private List<zzqc> zzbpc = new LinkedList();

    public final boolean zza(zzqc zzqc) {
        synchronized (this.lock) {
            if (this.zzbpc.contains(zzqc)) {
                return true;
            }
            return false;
        }
    }

    public final boolean zzb(zzqc zzqc) {
        synchronized (this.lock) {
            Iterator<zzqc> it2 = this.zzbpc.iterator();
            while (it2.hasNext()) {
                zzqc next = it2.next();
                if (!zzq.zzku().zzvf().zzvu()) {
                    if (zzqc != next && next.zzls().equals(zzqc.zzls())) {
                        it2.remove();
                        return true;
                    }
                } else if (!zzq.zzku().zzvf().zzvw() && zzqc != next && next.zzlu().equals(zzqc.zzlu())) {
                    it2.remove();
                    return true;
                }
            }
            return false;
        }
    }

    public final void zzc(zzqc zzqc) {
        synchronized (this.lock) {
            if (this.zzbpc.size() >= 10) {
                int size = this.zzbpc.size();
                StringBuilder sb = new StringBuilder(41);
                sb.append("Queue is full, current size = ");
                sb.append(size);
                zzayu.zzea(sb.toString());
                this.zzbpc.remove(0);
            }
            int i = this.zzbpb;
            this.zzbpb = i + 1;
            zzqc.zzbp(i);
            zzqc.zzly();
            this.zzbpc.add(zzqc);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzqc zzo(boolean r8) {
        /*
            r7 = this;
            java.lang.Object r0 = r7.lock
            monitor-enter(r0)
            java.util.List<com.google.android.gms.internal.ads.zzqc> r1 = r7.zzbpc     // Catch:{ all -> 0x005a }
            int r1 = r1.size()     // Catch:{ all -> 0x005a }
            r2 = 0
            if (r1 != 0) goto L_0x0013
            java.lang.String r8 = "Queue empty"
            com.google.android.gms.internal.ads.zzayu.zzea(r8)     // Catch:{ all -> 0x005a }
            monitor-exit(r0)     // Catch:{ all -> 0x005a }
            return r2
        L_0x0013:
            java.util.List<com.google.android.gms.internal.ads.zzqc> r1 = r7.zzbpc     // Catch:{ all -> 0x005a }
            int r1 = r1.size()     // Catch:{ all -> 0x005a }
            r3 = 2
            r4 = 0
            if (r1 < r3) goto L_0x0045
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            java.util.List<com.google.android.gms.internal.ads.zzqc> r1 = r7.zzbpc     // Catch:{ all -> 0x005a }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x005a }
            r3 = 0
        L_0x0026:
            boolean r5 = r1.hasNext()     // Catch:{ all -> 0x005a }
            if (r5 == 0) goto L_0x003e
            java.lang.Object r5 = r1.next()     // Catch:{ all -> 0x005a }
            com.google.android.gms.internal.ads.zzqc r5 = (com.google.android.gms.internal.ads.zzqc) r5     // Catch:{ all -> 0x005a }
            int r6 = r5.getScore()     // Catch:{ all -> 0x005a }
            if (r6 <= r8) goto L_0x003b
            r4 = r3
            r2 = r5
            r8 = r6
        L_0x003b:
            int r3 = r3 + 1
            goto L_0x0026
        L_0x003e:
            java.util.List<com.google.android.gms.internal.ads.zzqc> r8 = r7.zzbpc     // Catch:{ all -> 0x005a }
            r8.remove(r4)     // Catch:{ all -> 0x005a }
            monitor-exit(r0)     // Catch:{ all -> 0x005a }
            return r2
        L_0x0045:
            java.util.List<com.google.android.gms.internal.ads.zzqc> r1 = r7.zzbpc     // Catch:{ all -> 0x005a }
            java.lang.Object r1 = r1.get(r4)     // Catch:{ all -> 0x005a }
            com.google.android.gms.internal.ads.zzqc r1 = (com.google.android.gms.internal.ads.zzqc) r1     // Catch:{ all -> 0x005a }
            if (r8 == 0) goto L_0x0055
            java.util.List<com.google.android.gms.internal.ads.zzqc> r8 = r7.zzbpc     // Catch:{ all -> 0x005a }
            r8.remove(r4)     // Catch:{ all -> 0x005a }
            goto L_0x0058
        L_0x0055:
            r1.zzlv()     // Catch:{ all -> 0x005a }
        L_0x0058:
            monitor-exit(r0)     // Catch:{ all -> 0x005a }
            return r1
        L_0x005a:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x005a }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzqf.zzo(boolean):com.google.android.gms.internal.ads.zzqc");
    }
}
