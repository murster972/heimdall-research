package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class zzdhk extends zzdhh implements zzdhd, ScheduledExecutorService {
    private final ScheduledExecutorService zzgxi;

    zzdhk(ScheduledExecutorService scheduledExecutorService) {
        super(scheduledExecutorService);
        this.zzgxi = (ScheduledExecutorService) zzdei.checkNotNull(scheduledExecutorService);
    }

    public final /* synthetic */ ScheduledFuture schedule(Callable callable, long j, TimeUnit timeUnit) {
        zzdhs zze = zzdhs.zze(callable);
        return new zzdhj(zze, this.zzgxi.schedule(zze, j, timeUnit));
    }

    public final /* synthetic */ ScheduledFuture scheduleAtFixedRate(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        zzdhm zzdhm = new zzdhm(runnable);
        return new zzdhj(zzdhm, this.zzgxi.scheduleAtFixedRate(zzdhm, j, j2, timeUnit));
    }

    public final /* synthetic */ ScheduledFuture scheduleWithFixedDelay(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        zzdhm zzdhm = new zzdhm(runnable);
        return new zzdhj(zzdhm, this.zzgxi.scheduleWithFixedDelay(zzdhm, j, j2, timeUnit));
    }

    public final /* synthetic */ ScheduledFuture schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        zzdhs zza = zzdhs.zza(runnable, null);
        return new zzdhj(zza, this.zzgxi.schedule(zza, j, timeUnit));
    }
}
