package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcrv implements zzdxg<zzcrr> {
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<Context> zzfhb;

    private zzcrv(zzdxp<Context> zzdxp, zzdxp<zzdhd> zzdxp2) {
        this.zzfhb = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcrv zzao(zzdxp<Context> zzdxp, zzdxp<zzdhd> zzdxp2) {
        return new zzcrv(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcrr(this.zzfhb.get(), this.zzfcv.get());
    }
}
