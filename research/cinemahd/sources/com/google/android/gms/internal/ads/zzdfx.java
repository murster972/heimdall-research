package com.google.android.gms.internal.ads;

final class zzdfx<I, O> extends zzdfu<I, O, zzdgf<? super I, ? extends O>, zzdhe<? extends O>> {
    zzdfx(zzdhe<? extends I> zzdhe, zzdgf<? super I, ? extends O> zzdgf) {
        super(zzdhe, zzdgf);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void setResult(Object obj) {
        setFuture((zzdhe) obj);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzc(Object obj, Object obj2) throws Exception {
        zzdgf zzdgf = (zzdgf) obj;
        zzdhe zzf = zzdgf.zzf(obj2);
        zzdei.zza(zzf, "AsyncFunction.apply returned null instead of a Future. Did you mean to return immediateFuture(null)? %s", zzdgf);
        return zzf;
    }
}
