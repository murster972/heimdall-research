package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbtx implements zzbrb {
    private final zzbdi zzehp;

    zzbtx(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void zzagj() {
        zzbdi zzbdi = this.zzehp;
        if (zzbdi.zzzw() != null) {
            zzbdi.zzzw().close();
        }
    }
}
