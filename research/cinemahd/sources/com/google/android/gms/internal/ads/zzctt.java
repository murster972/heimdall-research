package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzctt implements zzcty<Bundle> {
    private final String zzbma;
    private final int zzdjf;
    private final boolean zzdkc;
    private final boolean zzdwc;
    private final boolean zzggt;
    private final int zzggu;
    private final int zzggv;

    public zzctt(boolean z, boolean z2, String str, boolean z3, int i, int i2, int i3) {
        this.zzggt = z;
        this.zzdwc = z2;
        this.zzbma = str;
        this.zzdkc = z3;
        this.zzdjf = i;
        this.zzggu = i2;
        this.zzggv = i3;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        bundle.putString("js", this.zzbma);
        bundle.putBoolean("is_nonagon", true);
        bundle.putString("extra_caps", (String) zzve.zzoy().zzd(zzzn.zzclv));
        bundle.putInt("target_api", this.zzdjf);
        bundle.putInt("dv", this.zzggu);
        bundle.putInt("lv", this.zzggv);
        Bundle zza = zzdaa.zza(bundle, "sdk_env");
        zza.putBoolean("mf", zzaaz.zzcti.get().booleanValue());
        zza.putBoolean("instant_app", this.zzggt);
        zza.putBoolean("lite", this.zzdwc);
        zza.putBoolean("is_privileged_process", this.zzdkc);
        bundle.putBundle("sdk_env", zza);
        Bundle zza2 = zzdaa.zza(zza, "build_meta");
        zza2.putString("cl", "278033407");
        zza2.putString("rapid_rc", "dev");
        zza2.putString("rapid_rollup", "HEAD");
        zza.putBundle("build_meta", zza2);
    }
}
