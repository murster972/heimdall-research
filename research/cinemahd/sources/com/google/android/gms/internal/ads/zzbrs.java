package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

public final class zzbrs implements zzdxg<Set<zzbsu<zzbqb>>> {
    private final zzbrm zzfim;

    private zzbrs(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbrs zzi(zzbrm zzbrm) {
        return new zzbrs(zzbrm);
    }

    public static Set<zzbsu<zzbqb>> zzj(zzbrm zzbrm) {
        return (Set) zzdxm.zza(Collections.emptySet(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzj(this.zzfim);
    }
}
