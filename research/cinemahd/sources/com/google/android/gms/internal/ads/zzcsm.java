package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcsm implements Callable {
    private final zzcsn zzgfz;

    zzcsm(zzcsn zzcsn) {
        this.zzgfz = zzcsn;
    }

    public final Object call() {
        return this.zzgfz.zzanj();
    }
}
