package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class zzeg extends zzcj<Integer, Object> {
    public Long zzxh;
    public Boolean zzxi;
    public Boolean zzxj;

    public zzeg() {
    }

    /* access modifiers changed from: protected */
    public final void zzap(String str) {
        HashMap zzaq = zzcj.zzaq(str);
        if (zzaq != null) {
            this.zzxh = (Long) zzaq.get(0);
            this.zzxi = (Boolean) zzaq.get(1);
            this.zzxj = (Boolean) zzaq.get(2);
        }
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Object> zzbk() {
        HashMap<Integer, Object> hashMap = new HashMap<>();
        hashMap.put(0, this.zzxh);
        hashMap.put(1, this.zzxi);
        hashMap.put(2, this.zzxj);
        return hashMap;
    }

    public zzeg(String str) {
        zzap(str);
    }
}
