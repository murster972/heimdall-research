package com.google.android.gms.internal.ads;

import android.view.Surface;

public interface zzpd {
    void zza(int i, int i2, int i3, float f);

    void zza(Surface surface);

    void zzd(String str, long j, long j2);

    void zze(int i, long j);

    void zze(zzit zzit);

    void zzf(zzit zzit);

    void zzk(zzgw zzgw);
}
