package com.google.android.gms.internal.ads;

public interface zzcio<AdT> {
    boolean zza(zzczt zzczt, zzczl zzczl);

    zzdhe<AdT> zzb(zzczt zzczt, zzczl zzczl);
}
