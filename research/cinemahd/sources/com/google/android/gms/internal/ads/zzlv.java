package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.EOFException;
import java.io.IOException;

final class zzlv {
    private final zzjf zzapm;
    private final zzjd[] zzbaw;
    private zzjd zzbax;

    public zzlv(zzjd[] zzjdArr, zzjf zzjf) {
        this.zzbaw = zzjdArr;
        this.zzapm = zzjf;
    }

    public final void release() {
        zzjd zzjd = this.zzbax;
        if (zzjd != null) {
            zzjd.release();
            this.zzbax = null;
        }
    }

    public final zzjd zza(zzjg zzjg, Uri uri) throws IOException, InterruptedException {
        zzjd zzjd = this.zzbax;
        if (zzjd != null) {
            return zzjd;
        }
        zzjd[] zzjdArr = this.zzbaw;
        int length = zzjdArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            zzjd zzjd2 = zzjdArr[i];
            try {
                if (zzjd2.zza(zzjg)) {
                    this.zzbax = zzjd2;
                    zzjg.zzgi();
                    break;
                }
                i++;
            } catch (EOFException unused) {
            } finally {
                zzjg.zzgi();
            }
        }
        zzjd zzjd3 = this.zzbax;
        if (zzjd3 != null) {
            zzjd3.zza(this.zzapm);
            return this.zzbax;
        }
        String zza = zzoq.zza((Object[]) this.zzbaw);
        StringBuilder sb = new StringBuilder(String.valueOf(zza).length() + 58);
        sb.append("None of the available extractors (");
        sb.append(zza);
        sb.append(") could read the stream.");
        throw new zzmu(sb.toString(), uri);
    }
}
