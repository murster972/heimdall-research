package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.PackageManager;
import java.util.HashMap;
import java.util.Map;

public final class zzdam {
    private final zzazb zzbli;
    private final zzave zzbmm;
    private final Map<String, zzdao> zzgnd = new HashMap();
    private final Context zzyv;

    public zzdam(Context context, zzazb zzazb, zzave zzave) {
        this.zzyv = context;
        this.zzbli = zzazb;
        this.zzbmm = zzave;
    }

    private final zzdao zzaow() {
        return new zzdao(this.zzyv, this.zzbmm.zzvf(), this.zzbmm.zzvh());
    }

    private final zzdao zzgm(String str) {
        zzarf zzz = zzarf.zzz(this.zzyv);
        try {
            zzz.setAppPackageName(str);
            zzavx zzavx = new zzavx();
            zzavx.zza(this.zzyv, str, false);
            zzavy zzavy = new zzavy(this.zzbmm.zzvf(), zzavx);
            return new zzdao(zzz, zzavy, new zzavp(zzayk.zzxf(), zzavy));
        } catch (PackageManager.NameNotFoundException unused) {
            return zzaow();
        }
    }

    public final zzdao zzgl(String str) {
        if (str == null) {
            return zzaow();
        }
        if (this.zzgnd.containsKey(str)) {
            return this.zzgnd.get(str);
        }
        zzdao zzgm = zzgm(str);
        this.zzgnd.put(str, zzgm);
        return zzgm;
    }
}
