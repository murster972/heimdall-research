package com.google.android.gms.internal.ads;

public final class zzcwi implements zzdxg<zzcwg> {
    private final zzdxp<zzdhd> zzfcv;

    public zzcwi(zzdxp<zzdhd> zzdxp) {
        this.zzfcv = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcwg(this.zzfcv.get());
    }
}
