package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.mediation.VersionInfo;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzanw extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzanw> CREATOR = new zzanv();
    private final int major;
    private final int minor;
    private final int zzdev;

    zzanw(int i, int i2, int i3) {
        this.major = i;
        this.minor = i2;
        this.zzdev = i3;
    }

    public static zzanw zza(VersionInfo versionInfo) {
        return new zzanw(versionInfo.getMajorVersion(), versionInfo.getMinorVersion(), versionInfo.getMicroVersion());
    }

    public final String toString() {
        int i = this.major;
        int i2 = this.minor;
        int i3 = this.zzdev;
        StringBuilder sb = new StringBuilder(35);
        sb.append(i);
        sb.append(".");
        sb.append(i2);
        sb.append(".");
        sb.append(i3);
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.major);
        SafeParcelWriter.a(parcel, 2, this.minor);
        SafeParcelWriter.a(parcel, 3, this.zzdev);
        SafeParcelWriter.a(parcel, a2);
    }
}
