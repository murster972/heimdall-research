package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdjo extends zzdih<zzdlw, zzdlv> {
    private final /* synthetic */ zzdjm zzgzf;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdjo(zzdjm zzdjm, Class cls) {
        super(cls);
        this.zzgzf = zzdjm;
    }

    public final /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdpo.zzez(((zzdlw) zzdte).getKeySize());
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        return (zzdlv) zzdlv.zzatw().zzag(zzdqk.zzu(zzdpn.zzey(((zzdlw) zzdte).getKeySize()))).zzeg(0).zzbaf();
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdlw.zzaf(zzdqk);
    }
}
