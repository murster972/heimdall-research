package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdkg extends zzdik<zzdie, zzdmn> {
    zzdkg(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdmn zzdmn = (zzdmn) obj;
        zzdmj zzauf = zzdmn.zzauf();
        zzdmo zzauh = zzauf.zzauh();
        return new zzdor(zzdov.zza(zzdkk.zza(zzauh.zzauu()), zzdmn.zzaup().toByteArray(), zzdmn.zzauq().toByteArray()), zzauh.zzauw().toByteArray(), zzdkk.zza(zzauh.zzauv()), zzdkk.zza(zzauf.zzauj()), new zzdkm(zzauf.zzaui().zzauc()));
    }
}
