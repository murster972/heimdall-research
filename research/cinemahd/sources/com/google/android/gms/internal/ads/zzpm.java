package com.google.android.gms.internal.ads;

import android.view.Surface;

final class zzpm implements Runnable {
    private final /* synthetic */ zzpg zzbjg;
    private final /* synthetic */ Surface zzbjo;

    zzpm(zzpg zzpg, Surface surface) {
        this.zzbjg = zzpg;
        this.zzbjo = surface;
    }

    public final void run() {
        this.zzbjg.zzbjh.zza(this.zzbjo);
    }
}
