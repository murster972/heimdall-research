package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaiv implements Runnable {
    private final zzais zzczi;
    private final zzajj zzczq;
    private final zzaif zzczr;

    zzaiv(zzais zzais, zzajj zzajj, zzaif zzaif) {
        this.zzczi = zzais;
        this.zzczq = zzajj;
        this.zzczr = zzaif;
    }

    public final void run() {
        this.zzczi.zza(this.zzczq, this.zzczr);
    }
}
