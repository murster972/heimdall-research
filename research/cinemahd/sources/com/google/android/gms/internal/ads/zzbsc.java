package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbsc implements zzdxg<Set<zzbsu<zzbqg>>> {
    private final zzbrm zzfim;

    private zzbsc(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbsc zzt(zzbrm zzbrm) {
        return new zzbsc(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfim.zzahu(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
