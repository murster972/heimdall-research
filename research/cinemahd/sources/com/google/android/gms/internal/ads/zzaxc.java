package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzaxc extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaxc> CREATOR = new zzaxe();
    public final int errorCode;
    public final String zzdtr;

    zzaxc(String str, int i) {
        this.zzdtr = str == null ? "" : str;
        this.errorCode = i;
    }

    public static zzaxc zza(Throwable th, int i) {
        return new zzaxc(th.getMessage(), i);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzdtr, false);
        SafeParcelWriter.a(parcel, 2, this.errorCode);
        SafeParcelWriter.a(parcel, a2);
    }
}
