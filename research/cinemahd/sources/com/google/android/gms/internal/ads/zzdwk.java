package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;
import java.io.IOException;

public final class zzdwk extends zzdvq<zzdwk> {
    private zzdvx.zzb.zzd.C0043zzb zzhyk = null;
    public zzdvx.zzb.zzc[] zzhyl = new zzdvx.zzb.zzc[0];
    private byte[] zzhym = null;
    private byte[] zzhyn = null;
    private Integer zzhyo = null;

    public zzdwk() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        zzdvx.zzb.zzc[] zzcArr = this.zzhyl;
        if (zzcArr != null && zzcArr.length > 0) {
            int i = 0;
            while (true) {
                zzdvx.zzb.zzc[] zzcArr2 = this.zzhyl;
                if (i >= zzcArr2.length) {
                    break;
                }
                zzdvx.zzb.zzc zzc = zzcArr2[i];
                if (zzc != null) {
                    zzdvo.zze(2, zzc);
                }
                i++;
            }
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int zzoi = super.zzoi();
        zzdvx.zzb.zzc[] zzcArr = this.zzhyl;
        if (zzcArr != null && zzcArr.length > 0) {
            int i = 0;
            while (true) {
                zzdvx.zzb.zzc[] zzcArr2 = this.zzhyl;
                if (i >= zzcArr2.length) {
                    break;
                }
                zzdvx.zzb.zzc zzc = zzcArr2[i];
                if (zzc != null) {
                    zzoi += zzdrb.zzc(2, (zzdte) zzc);
                }
                i++;
            }
        }
        return zzoi;
    }
}
