package com.google.android.gms.internal.ads;

public interface zzbvl extends zzboe<zzbvm> {
    zzbvl zza(zzbod zzbod);

    zzbvl zza(zzbrm zzbrm);

    zzbvl zza(zzbvi zzbvi);

    zzbvm zzadf();
}
