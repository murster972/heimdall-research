package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbsr implements zzdxg<zzbsq> {
    private final zzdxp<Set<zzbsu<zzbsn>>> zzfeo;

    private zzbsr(zzdxp<Set<zzbsu<zzbsn>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbsr zzr(zzdxp<Set<zzbsu<zzbsn>>> zzdxp) {
        return new zzbsr(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbsq(this.zzfeo.get());
    }
}
