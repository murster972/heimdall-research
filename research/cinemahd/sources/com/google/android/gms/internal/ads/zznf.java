package com.google.android.gms.internal.ads;

public abstract class zznf {
    private zzni zzbed;

    /* access modifiers changed from: protected */
    public final void invalidate() {
        zzni zzni = this.zzbed;
        if (zzni != null) {
            zzni.zzei();
        }
    }

    public abstract zznh zza(zzhe[] zzheArr, zzmr zzmr) throws zzgl;

    public final void zza(zzni zzni) {
        this.zzbed = zzni;
    }

    public abstract void zzd(Object obj);
}
