package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzrl extends IInterface {
    void onAppOpenAdClosed() throws RemoteException;
}
