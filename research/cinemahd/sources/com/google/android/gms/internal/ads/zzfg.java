package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfg extends zzfw {
    private static final Object zzzs = new Object();
    private static volatile Long zzzu;

    public zzfg(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 22);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (zzzu == null) {
            synchronized (zzzs) {
                if (zzzu == null) {
                    zzzu = (Long) this.zzaae.invoke((Object) null, new Object[0]);
                }
            }
        }
        synchronized (this.zzzt) {
            this.zzzt.zzav(zzzu.longValue());
        }
    }
}
