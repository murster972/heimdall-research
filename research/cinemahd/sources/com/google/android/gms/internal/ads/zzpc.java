package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.media.MediaCodec;

@TargetApi(23)
final class zzpc implements MediaCodec.OnFrameRenderedListener {
    private final /* synthetic */ zzox zzbja;

    private zzpc(zzox zzox, MediaCodec mediaCodec) {
        this.zzbja = zzox;
        mediaCodec.setOnFrameRenderedListener(this, new zzddu());
    }

    public final void onFrameRendered(MediaCodec mediaCodec, long j, long j2) {
        zzox zzox = this.zzbja;
        if (this == zzox.zzbil) {
            zzox.zzjb();
        }
    }
}
