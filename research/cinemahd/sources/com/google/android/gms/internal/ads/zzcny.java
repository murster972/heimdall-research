package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

final class zzcny extends zzbtv {
    zzcny(zzcnw zzcnw, zzbuv zzbuv) {
        super(zzbuv);
    }

    public final Set<zzbsu<zzbov>> zza(zzbva zzbva) {
        return Collections.emptySet();
    }
}
