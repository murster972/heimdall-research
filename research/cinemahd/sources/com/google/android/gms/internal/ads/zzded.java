package com.google.android.gms.internal.ads;

public interface zzded<F, T> {
    T apply(F f);
}
