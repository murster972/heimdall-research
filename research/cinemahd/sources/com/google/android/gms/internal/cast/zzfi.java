package com.google.android.gms.internal.cast;

final class zzfi implements Runnable {
    private final /* synthetic */ zzfg zzabr;

    zzfi(zzfg zzfg) {
        this.zzabr = zzfg;
    }

    public final void run() {
        this.zzabr.doFrame(System.nanoTime());
    }
}
