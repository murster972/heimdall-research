package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcer implements Runnable {
    private final zzceq zzftj;
    private final zzazl zzftt;

    zzcer(zzceq zzceq, zzazl zzazl) {
        this.zzftj = zzceq;
        this.zzftt = zzazl;
    }

    public final void run() {
        this.zzftj.zza(this.zzftt);
    }
}
