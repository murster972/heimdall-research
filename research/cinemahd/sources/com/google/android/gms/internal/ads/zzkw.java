package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

@TargetApi(19)
public abstract class zzkw extends zzgj {
    private static final byte[] zzaxu = zzoq.zzbo("0000016742C00BDA259000000168CE0F13200000016588840DCE7118A0002FBF1C31C3275D78");
    private zzgw zzafz;
    private ByteBuffer[] zzajf;
    private final zzky zzaxv;
    private final zzja<zzjc> zzaxw;
    private final boolean zzaxx;
    private final zzis zzaxy;
    private final zzis zzaxz;
    private final zzgy zzaya;
    private final List<Long> zzayb;
    private final MediaCodec.BufferInfo zzayc;
    private zziy<zzjc> zzayd;
    private zziy<zzjc> zzaye;
    private MediaCodec zzayf;
    private zzkt zzayg;
    private boolean zzayh;
    private boolean zzayi;
    private boolean zzayj;
    private boolean zzayk;
    private boolean zzayl;
    private boolean zzaym;
    private boolean zzayn;
    private boolean zzayo;
    private boolean zzayp;
    private ByteBuffer[] zzayq;
    private long zzayr;
    private int zzays;
    private int zzayt;
    private boolean zzayu;
    private boolean zzayv;
    private int zzayw;
    private int zzayx;
    private boolean zzayy;
    private boolean zzayz;
    private boolean zzaza;
    private boolean zzazb;
    private boolean zzazc;
    private boolean zzazd;
    protected zzit zzaze;

    public zzkw(int i, zzky zzky, zzja<zzjc> zzja, boolean z) {
        super(i);
        zzoc.checkState(zzoq.SDK_INT >= 16);
        this.zzaxv = (zzky) zzoc.checkNotNull(zzky);
        this.zzaxw = zzja;
        this.zzaxx = z;
        this.zzaxy = new zzis(0);
        this.zzaxz = new zzis(0);
        this.zzaya = new zzgy();
        this.zzayb = new ArrayList();
        this.zzayc = new MediaCodec.BufferInfo();
        this.zzayw = 0;
        this.zzayx = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:84:0x014b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x014c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzgz() throws com.google.android.gms.internal.ads.zzgl {
        /*
            r14 = this;
            android.media.MediaCodec r0 = r14.zzayf
            r1 = 0
            if (r0 == 0) goto L_0x01d6
            int r2 = r14.zzayx
            r3 = 2
            if (r2 == r3) goto L_0x01d6
            boolean r2 = r14.zzaza
            if (r2 == 0) goto L_0x0010
            goto L_0x01d6
        L_0x0010:
            int r2 = r14.zzays
            if (r2 >= 0) goto L_0x002c
            r4 = 0
            int r0 = r0.dequeueInputBuffer(r4)
            r14.zzays = r0
            int r0 = r14.zzays
            if (r0 >= 0) goto L_0x0021
            return r1
        L_0x0021:
            com.google.android.gms.internal.ads.zzis r2 = r14.zzaxy
            java.nio.ByteBuffer[] r4 = r14.zzayq
            r0 = r4[r0]
            r2.zzcs = r0
            r2.clear()
        L_0x002c:
            int r0 = r14.zzayx
            r2 = -1
            r4 = 1
            if (r0 != r4) goto L_0x0049
            boolean r0 = r14.zzayk
            if (r0 != 0) goto L_0x0046
            r14.zzayz = r4
            android.media.MediaCodec r5 = r14.zzayf
            int r6 = r14.zzays
            r7 = 0
            r8 = 0
            r9 = 0
            r11 = 4
            r5.queueInputBuffer(r6, r7, r8, r9, r11)
            r14.zzays = r2
        L_0x0046:
            r14.zzayx = r3
            return r1
        L_0x0049:
            boolean r0 = r14.zzayo
            if (r0 == 0) goto L_0x006b
            r14.zzayo = r1
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            java.nio.ByteBuffer r0 = r0.zzcs
            byte[] r1 = zzaxu
            r0.put(r1)
            android.media.MediaCodec r5 = r14.zzayf
            int r6 = r14.zzays
            r7 = 0
            byte[] r0 = zzaxu
            int r8 = r0.length
            r9 = 0
            r11 = 0
            r5.queueInputBuffer(r6, r7, r8, r9, r11)
            r14.zzays = r2
            r14.zzayy = r4
            return r4
        L_0x006b:
            boolean r0 = r14.zzazc
            if (r0 == 0) goto L_0x0072
            r0 = -4
            r5 = 0
            goto L_0x00aa
        L_0x0072:
            int r0 = r14.zzayw
            if (r0 != r4) goto L_0x0097
            r0 = 0
        L_0x0077:
            com.google.android.gms.internal.ads.zzgw r5 = r14.zzafz
            java.util.List<byte[]> r5 = r5.zzafg
            int r5 = r5.size()
            if (r0 >= r5) goto L_0x0095
            com.google.android.gms.internal.ads.zzgw r5 = r14.zzafz
            java.util.List<byte[]> r5 = r5.zzafg
            java.lang.Object r5 = r5.get(r0)
            byte[] r5 = (byte[]) r5
            com.google.android.gms.internal.ads.zzis r6 = r14.zzaxy
            java.nio.ByteBuffer r6 = r6.zzcs
            r6.put(r5)
            int r0 = r0 + 1
            goto L_0x0077
        L_0x0095:
            r14.zzayw = r3
        L_0x0097:
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            java.nio.ByteBuffer r0 = r0.zzcs
            int r0 = r0.position()
            com.google.android.gms.internal.ads.zzgy r5 = r14.zzaya
            com.google.android.gms.internal.ads.zzis r6 = r14.zzaxy
            int r5 = r14.zza((com.google.android.gms.internal.ads.zzgy) r5, (com.google.android.gms.internal.ads.zzis) r6, (boolean) r1)
            r13 = r5
            r5 = r0
            r0 = r13
        L_0x00aa:
            r6 = -3
            if (r0 != r6) goto L_0x00ae
            return r1
        L_0x00ae:
            r6 = -5
            if (r0 != r6) goto L_0x00c4
            int r0 = r14.zzayw
            if (r0 != r3) goto L_0x00bc
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            r0.clear()
            r14.zzayw = r4
        L_0x00bc:
            com.google.android.gms.internal.ads.zzgy r0 = r14.zzaya
            com.google.android.gms.internal.ads.zzgw r0 = r0.zzafz
            r14.zzd(r0)
            return r4
        L_0x00c4:
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            boolean r0 = r0.zzgb()
            if (r0 == 0) goto L_0x0100
            int r0 = r14.zzayw
            if (r0 != r3) goto L_0x00d7
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            r0.clear()
            r14.zzayw = r4
        L_0x00d7:
            r14.zzaza = r4
            boolean r0 = r14.zzayy
            if (r0 != 0) goto L_0x00e1
            r14.zzha()
            return r1
        L_0x00e1:
            boolean r0 = r14.zzayk     // Catch:{ CryptoException -> 0x00f6 }
            if (r0 != 0) goto L_0x00f5
            r14.zzayz = r4     // Catch:{ CryptoException -> 0x00f6 }
            android.media.MediaCodec r5 = r14.zzayf     // Catch:{ CryptoException -> 0x00f6 }
            int r6 = r14.zzays     // Catch:{ CryptoException -> 0x00f6 }
            r7 = 0
            r8 = 0
            r9 = 0
            r11 = 4
            r5.queueInputBuffer(r6, r7, r8, r9, r11)     // Catch:{ CryptoException -> 0x00f6 }
            r14.zzays = r2     // Catch:{ CryptoException -> 0x00f6 }
        L_0x00f5:
            return r1
        L_0x00f6:
            r0 = move-exception
            int r1 = r14.getIndex()
            com.google.android.gms.internal.ads.zzgl r0 = com.google.android.gms.internal.ads.zzgl.zza(r0, r1)
            throw r0
        L_0x0100:
            boolean r0 = r14.zzazd
            if (r0 == 0) goto L_0x0118
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            boolean r0 = r0.zzgc()
            if (r0 != 0) goto L_0x0118
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            r0.clear()
            int r0 = r14.zzayw
            if (r0 != r3) goto L_0x0117
            r14.zzayw = r4
        L_0x0117:
            return r4
        L_0x0118:
            r14.zzazd = r1
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy
            boolean r0 = r0.zzgd()
            com.google.android.gms.internal.ads.zziy<com.google.android.gms.internal.ads.zzjc> r3 = r14.zzayd
            if (r3 == 0) goto L_0x0144
            int r3 = r3.getState()
            if (r3 == 0) goto L_0x0135
            r6 = 4
            if (r3 == r6) goto L_0x0144
            if (r0 != 0) goto L_0x0133
            boolean r3 = r14.zzaxx
            if (r3 != 0) goto L_0x0144
        L_0x0133:
            r3 = 1
            goto L_0x0145
        L_0x0135:
            com.google.android.gms.internal.ads.zziy<com.google.android.gms.internal.ads.zzjc> r0 = r14.zzayd
            com.google.android.gms.internal.ads.zzix r0 = r0.zzgg()
            int r1 = r14.getIndex()
            com.google.android.gms.internal.ads.zzgl r0 = com.google.android.gms.internal.ads.zzgl.zza(r0, r1)
            throw r0
        L_0x0144:
            r3 = 0
        L_0x0145:
            r14.zzazc = r3
            boolean r3 = r14.zzazc
            if (r3 == 0) goto L_0x014c
            return r1
        L_0x014c:
            boolean r3 = r14.zzayh
            if (r3 == 0) goto L_0x0166
            if (r0 != 0) goto L_0x0166
            com.google.android.gms.internal.ads.zzis r3 = r14.zzaxy
            java.nio.ByteBuffer r3 = r3.zzcs
            com.google.android.gms.internal.ads.zzoi.zzk(r3)
            com.google.android.gms.internal.ads.zzis r3 = r14.zzaxy
            java.nio.ByteBuffer r3 = r3.zzcs
            int r3 = r3.position()
            if (r3 != 0) goto L_0x0164
            return r4
        L_0x0164:
            r14.zzayh = r1
        L_0x0166:
            com.google.android.gms.internal.ads.zzis r3 = r14.zzaxy     // Catch:{ CryptoException -> 0x01cc }
            long r10 = r3.zzamd     // Catch:{ CryptoException -> 0x01cc }
            com.google.android.gms.internal.ads.zzis r3 = r14.zzaxy     // Catch:{ CryptoException -> 0x01cc }
            boolean r3 = r3.zzga()     // Catch:{ CryptoException -> 0x01cc }
            if (r3 == 0) goto L_0x017b
            java.util.List<java.lang.Long> r3 = r14.zzayb     // Catch:{ CryptoException -> 0x01cc }
            java.lang.Long r6 = java.lang.Long.valueOf(r10)     // Catch:{ CryptoException -> 0x01cc }
            r3.add(r6)     // Catch:{ CryptoException -> 0x01cc }
        L_0x017b:
            com.google.android.gms.internal.ads.zzis r3 = r14.zzaxy     // Catch:{ CryptoException -> 0x01cc }
            java.nio.ByteBuffer r3 = r3.zzcs     // Catch:{ CryptoException -> 0x01cc }
            r3.flip()     // Catch:{ CryptoException -> 0x01cc }
            com.google.android.gms.internal.ads.zzis r3 = r14.zzaxy     // Catch:{ CryptoException -> 0x01cc }
            r14.zza((com.google.android.gms.internal.ads.zzis) r3)     // Catch:{ CryptoException -> 0x01cc }
            if (r0 == 0) goto L_0x01ad
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy     // Catch:{ CryptoException -> 0x01cc }
            com.google.android.gms.internal.ads.zzio r0 = r0.zzamc     // Catch:{ CryptoException -> 0x01cc }
            android.media.MediaCodec$CryptoInfo r9 = r0.zzfz()     // Catch:{ CryptoException -> 0x01cc }
            if (r5 != 0) goto L_0x0194
            goto L_0x01a3
        L_0x0194:
            int[] r0 = r9.numBytesOfClearData     // Catch:{ CryptoException -> 0x01cc }
            if (r0 != 0) goto L_0x019c
            int[] r0 = new int[r4]     // Catch:{ CryptoException -> 0x01cc }
            r9.numBytesOfClearData = r0     // Catch:{ CryptoException -> 0x01cc }
        L_0x019c:
            int[] r0 = r9.numBytesOfClearData     // Catch:{ CryptoException -> 0x01cc }
            r3 = r0[r1]     // Catch:{ CryptoException -> 0x01cc }
            int r3 = r3 + r5
            r0[r1] = r3     // Catch:{ CryptoException -> 0x01cc }
        L_0x01a3:
            android.media.MediaCodec r6 = r14.zzayf     // Catch:{ CryptoException -> 0x01cc }
            int r7 = r14.zzays     // Catch:{ CryptoException -> 0x01cc }
            r8 = 0
            r12 = 0
            r6.queueSecureInputBuffer(r7, r8, r9, r10, r12)     // Catch:{ CryptoException -> 0x01cc }
            goto L_0x01be
        L_0x01ad:
            android.media.MediaCodec r6 = r14.zzayf     // Catch:{ CryptoException -> 0x01cc }
            int r7 = r14.zzays     // Catch:{ CryptoException -> 0x01cc }
            r8 = 0
            com.google.android.gms.internal.ads.zzis r0 = r14.zzaxy     // Catch:{ CryptoException -> 0x01cc }
            java.nio.ByteBuffer r0 = r0.zzcs     // Catch:{ CryptoException -> 0x01cc }
            int r9 = r0.limit()     // Catch:{ CryptoException -> 0x01cc }
            r12 = 0
            r6.queueInputBuffer(r7, r8, r9, r10, r12)     // Catch:{ CryptoException -> 0x01cc }
        L_0x01be:
            r14.zzays = r2     // Catch:{ CryptoException -> 0x01cc }
            r14.zzayy = r4     // Catch:{ CryptoException -> 0x01cc }
            r14.zzayw = r1     // Catch:{ CryptoException -> 0x01cc }
            com.google.android.gms.internal.ads.zzit r0 = r14.zzaze     // Catch:{ CryptoException -> 0x01cc }
            int r1 = r0.zzamh     // Catch:{ CryptoException -> 0x01cc }
            int r1 = r1 + r4
            r0.zzamh = r1     // Catch:{ CryptoException -> 0x01cc }
            return r4
        L_0x01cc:
            r0 = move-exception
            int r1 = r14.getIndex()
            com.google.android.gms.internal.ads.zzgl r0 = com.google.android.gms.internal.ads.zzgl.zza(r0, r1)
            throw r0
        L_0x01d6:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzkw.zzgz():boolean");
    }

    private final void zzha() throws zzgl {
        if (this.zzayx == 2) {
            zzgy();
            zzgv();
            return;
        }
        this.zzazb = true;
        zzfu();
    }

    public boolean isReady() {
        if (this.zzafz == null || this.zzazc) {
            return false;
        }
        if (zzdz() || this.zzayt >= 0) {
            return true;
        }
        return this.zzayr != -9223372036854775807L && SystemClock.elapsedRealtime() < this.zzayr;
    }

    /* access modifiers changed from: protected */
    public void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) throws zzgl {
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
    }

    /* access modifiers changed from: protected */
    public void onStopped() {
    }

    public final int zza(zzgw zzgw) throws zzgl {
        try {
            return zza(this.zzaxv, zzgw);
        } catch (zzlb e) {
            throw zzgl.zza(e, getIndex());
        }
    }

    /* access modifiers changed from: protected */
    public abstract int zza(zzky zzky, zzgw zzgw) throws zzlb;

    /* access modifiers changed from: protected */
    public void zza(zzis zzis) {
    }

    /* access modifiers changed from: protected */
    public abstract void zza(zzkt zzkt, MediaCodec mediaCodec, zzgw zzgw, MediaCrypto mediaCrypto) throws zzlb;

    /* access modifiers changed from: protected */
    public abstract boolean zza(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j3, boolean z) throws zzgl;

    /* access modifiers changed from: protected */
    public boolean zza(MediaCodec mediaCodec, boolean z, zzgw zzgw, zzgw zzgw2) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean zza(zzkt zzkt) {
        return true;
    }

    public final void zzb(long j, long j2) throws zzgl {
        if (this.zzazb) {
            zzfu();
            return;
        }
        if (this.zzafz == null) {
            this.zzaxz.clear();
            int zza = zza(this.zzaya, this.zzaxz, true);
            if (zza == -5) {
                zzd(this.zzaya.zzafz);
            } else if (zza == -4) {
                zzoc.checkState(this.zzaxz.zzgb());
                this.zzaza = true;
                zzha();
                return;
            } else {
                return;
            }
        }
        zzgv();
        if (this.zzayf != null) {
            zzon.beginSection("drainAndFeed");
            do {
            } while (zzd(j, j2));
            do {
            } while (zzgz());
            zzon.endSection();
        } else {
            zzdp(j);
            this.zzaxz.clear();
            int zza2 = zza(this.zzaya, this.zzaxz, false);
            if (zza2 == -5) {
                zzd(this.zzaya.zzafz);
            } else if (zza2 == -4) {
                zzoc.checkState(this.zzaxz.zzgb());
                this.zzaza = true;
                zzha();
            }
        }
        this.zzaze.zzge();
    }

    /* access modifiers changed from: protected */
    public void zzc(String str, long j, long j2) {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0077, code lost:
        if (r5.height == r0.height) goto L_0x007b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zzd(com.google.android.gms.internal.ads.zzgw r5) throws com.google.android.gms.internal.ads.zzgl {
        /*
            r4 = this;
            com.google.android.gms.internal.ads.zzgw r0 = r4.zzafz
            r4.zzafz = r5
            com.google.android.gms.internal.ads.zzgw r5 = r4.zzafz
            com.google.android.gms.internal.ads.zziv r5 = r5.zzafh
            r1 = 0
            if (r0 != 0) goto L_0x000d
            r2 = r1
            goto L_0x000f
        L_0x000d:
            com.google.android.gms.internal.ads.zziv r2 = r0.zzafh
        L_0x000f:
            boolean r5 = com.google.android.gms.internal.ads.zzoq.zza(r5, r2)
            r2 = 1
            r5 = r5 ^ r2
            if (r5 == 0) goto L_0x004d
            com.google.android.gms.internal.ads.zzgw r5 = r4.zzafz
            com.google.android.gms.internal.ads.zziv r5 = r5.zzafh
            if (r5 == 0) goto L_0x004b
            com.google.android.gms.internal.ads.zzja<com.google.android.gms.internal.ads.zzjc> r5 = r4.zzaxw
            if (r5 == 0) goto L_0x003b
            android.os.Looper r1 = android.os.Looper.myLooper()
            com.google.android.gms.internal.ads.zzgw r3 = r4.zzafz
            com.google.android.gms.internal.ads.zziv r3 = r3.zzafh
            com.google.android.gms.internal.ads.zziy r5 = r5.zza(r1, r3)
            r4.zzaye = r5
            com.google.android.gms.internal.ads.zziy<com.google.android.gms.internal.ads.zzjc> r5 = r4.zzaye
            com.google.android.gms.internal.ads.zziy<com.google.android.gms.internal.ads.zzjc> r1 = r4.zzayd
            if (r5 != r1) goto L_0x004d
            com.google.android.gms.internal.ads.zzja<com.google.android.gms.internal.ads.zzjc> r1 = r4.zzaxw
            r1.zza(r5)
            goto L_0x004d
        L_0x003b:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "Media requires a DrmSessionManager"
            r5.<init>(r0)
            int r0 = r4.getIndex()
            com.google.android.gms.internal.ads.zzgl r5 = com.google.android.gms.internal.ads.zzgl.zza(r5, r0)
            throw r5
        L_0x004b:
            r4.zzaye = r1
        L_0x004d:
            com.google.android.gms.internal.ads.zziy<com.google.android.gms.internal.ads.zzjc> r5 = r4.zzaye
            com.google.android.gms.internal.ads.zziy<com.google.android.gms.internal.ads.zzjc> r1 = r4.zzayd
            if (r5 != r1) goto L_0x007e
            android.media.MediaCodec r5 = r4.zzayf
            if (r5 == 0) goto L_0x007e
            com.google.android.gms.internal.ads.zzkt r1 = r4.zzayg
            boolean r1 = r1.zzaxn
            com.google.android.gms.internal.ads.zzgw r3 = r4.zzafz
            boolean r5 = r4.zza((android.media.MediaCodec) r5, (boolean) r1, (com.google.android.gms.internal.ads.zzgw) r0, (com.google.android.gms.internal.ads.zzgw) r3)
            if (r5 == 0) goto L_0x007e
            r4.zzayv = r2
            r4.zzayw = r2
            boolean r5 = r4.zzayj
            if (r5 == 0) goto L_0x007a
            com.google.android.gms.internal.ads.zzgw r5 = r4.zzafz
            int r1 = r5.width
            int r3 = r0.width
            if (r1 != r3) goto L_0x007a
            int r5 = r5.height
            int r0 = r0.height
            if (r5 != r0) goto L_0x007a
            goto L_0x007b
        L_0x007a:
            r2 = 0
        L_0x007b:
            r4.zzayo = r2
            return
        L_0x007e:
            boolean r5 = r4.zzayy
            if (r5 == 0) goto L_0x0085
            r4.zzayx = r2
            return
        L_0x0085:
            r4.zzgy()
            r4.zzgv()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzkw.zzd(com.google.android.gms.internal.ads.zzgw):void");
    }

    public final int zzdw() {
        return 4;
    }

    /* access modifiers changed from: protected */
    public void zzdx() {
        this.zzafz = null;
        try {
            zzgy();
            try {
                if (this.zzayd != null) {
                    this.zzaxw.zza(this.zzayd);
                }
                try {
                    if (!(this.zzaye == null || this.zzaye == this.zzayd)) {
                        this.zzaxw.zza(this.zzaye);
                    }
                } finally {
                    this.zzayd = null;
                    this.zzaye = null;
                }
            } catch (Throwable th) {
                if (!(this.zzaye == null || this.zzaye == this.zzayd)) {
                    this.zzaxw.zza(this.zzaye);
                }
                throw th;
            } finally {
                this.zzayd = null;
                this.zzaye = null;
            }
        } catch (Throwable th2) {
            try {
                if (!(this.zzaye == null || this.zzaye == this.zzayd)) {
                    this.zzaxw.zza(this.zzaye);
                }
                throw th2;
            } finally {
                this.zzayd = null;
                this.zzaye = null;
            }
        } finally {
        }
    }

    /* access modifiers changed from: protected */
    public void zze(boolean z) throws zzgl {
        this.zzaze = new zzit();
    }

    public boolean zzeu() {
        return this.zzazb;
    }

    /* access modifiers changed from: protected */
    public void zzfu() throws zzgl {
    }

    /* access modifiers changed from: protected */
    public final void zzgv() throws zzgl {
        zzgw zzgw;
        if (this.zzayf == null && (zzgw = this.zzafz) != null) {
            this.zzayd = this.zzaye;
            String str = zzgw.zzafe;
            zziy<zzjc> zziy = this.zzayd;
            if (zziy != null) {
                int state = zziy.getState();
                if (state == 0) {
                    throw zzgl.zza(this.zzayd.zzgg(), getIndex());
                } else if (state == 3 || state == 4) {
                    zzjc zzgf = this.zzayd.zzgf();
                    throw new NoSuchMethodError();
                }
            } else {
                if (this.zzayg == null) {
                    try {
                        this.zzayg = zza(this.zzaxv, zzgw, false);
                        zzkt zzkt = this.zzayg;
                    } catch (zzlb e) {
                        zza(new zzkv(this.zzafz, (Throwable) e, false, -49998));
                    }
                    if (this.zzayg == null) {
                        zza(new zzkv(this.zzafz, (Throwable) null, false, -49999));
                    }
                }
                if (zza(this.zzayg)) {
                    String str2 = this.zzayg.name;
                    this.zzayh = zzoq.SDK_INT < 21 && this.zzafz.zzafg.isEmpty() && "OMX.MTK.VIDEO.DECODER.AVC".equals(str2);
                    int i = zzoq.SDK_INT;
                    this.zzayi = i < 18 || (i == 18 && ("OMX.SEC.avc.dec".equals(str2) || "OMX.SEC.avc.dec.secure".equals(str2))) || (zzoq.SDK_INT == 19 && zzoq.MODEL.startsWith("SM-G800") && ("OMX.Exynos.avc.dec".equals(str2) || "OMX.Exynos.avc.dec.secure".equals(str2)));
                    this.zzayj = zzoq.SDK_INT < 24 && ("OMX.Nvidia.h264.decode".equals(str2) || "OMX.Nvidia.h264.decode.secure".equals(str2)) && ("flounder".equals(zzoq.DEVICE) || "flounder_lte".equals(zzoq.DEVICE) || "grouper".equals(zzoq.DEVICE) || "tilapia".equals(zzoq.DEVICE));
                    this.zzayk = zzoq.SDK_INT <= 17 && ("OMX.rk.video_decoder.avc".equals(str2) || "OMX.allwinner.video.decoder.avc".equals(str2));
                    this.zzayl = (zzoq.SDK_INT <= 23 && "OMX.google.vorbis.decoder".equals(str2)) || (zzoq.SDK_INT <= 19 && "hb2000".equals(zzoq.DEVICE) && ("OMX.amlogic.avc.decoder.awesome".equals(str2) || "OMX.amlogic.avc.decoder.awesome.secure".equals(str2)));
                    this.zzaym = zzoq.SDK_INT == 21 && "OMX.google.aac.decoder".equals(str2);
                    this.zzayn = zzoq.SDK_INT <= 18 && this.zzafz.zzafo == 1 && "OMX.MTK.AUDIO.DECODER.MP3".equals(str2);
                    try {
                        long elapsedRealtime = SystemClock.elapsedRealtime();
                        String valueOf = String.valueOf(str2);
                        zzon.beginSection(valueOf.length() != 0 ? "createCodec:".concat(valueOf) : new String("createCodec:"));
                        this.zzayf = MediaCodec.createByCodecName(str2);
                        zzon.endSection();
                        zzon.beginSection("configureCodec");
                        zza(this.zzayg, this.zzayf, this.zzafz, (MediaCrypto) null);
                        zzon.endSection();
                        zzon.beginSection("startCodec");
                        this.zzayf.start();
                        zzon.endSection();
                        long elapsedRealtime2 = SystemClock.elapsedRealtime();
                        zzc(str2, elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
                        this.zzayq = this.zzayf.getInputBuffers();
                        this.zzajf = this.zzayf.getOutputBuffers();
                    } catch (Exception e2) {
                        zza(new zzkv(this.zzafz, (Throwable) e2, false, str2));
                    }
                    this.zzayr = getState() == 2 ? SystemClock.elapsedRealtime() + 1000 : -9223372036854775807L;
                    this.zzays = -1;
                    this.zzayt = -1;
                    this.zzazd = true;
                    this.zzaze.zzamf++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final MediaCodec zzgw() {
        return this.zzayf;
    }

    /* access modifiers changed from: protected */
    public final zzkt zzgx() {
        return this.zzayg;
    }

    /* access modifiers changed from: protected */
    public void zzgy() {
        this.zzayr = -9223372036854775807L;
        this.zzays = -1;
        this.zzayt = -1;
        this.zzazc = false;
        this.zzayu = false;
        this.zzayb.clear();
        this.zzayq = null;
        this.zzajf = null;
        this.zzayg = null;
        this.zzayv = false;
        this.zzayy = false;
        this.zzayh = false;
        this.zzayi = false;
        this.zzayj = false;
        this.zzayk = false;
        this.zzayl = false;
        this.zzayn = false;
        this.zzayo = false;
        this.zzayp = false;
        this.zzayz = false;
        this.zzayw = 0;
        this.zzayx = 0;
        this.zzaxy.zzcs = null;
        MediaCodec mediaCodec = this.zzayf;
        if (mediaCodec != null) {
            this.zzaze.zzamg++;
            try {
                mediaCodec.stop();
                try {
                    this.zzayf.release();
                    this.zzayf = null;
                    zziy<zzjc> zziy = this.zzayd;
                    if (zziy != null && this.zzaye != zziy) {
                        try {
                            this.zzaxw.zza(zziy);
                        } finally {
                            this.zzayd = null;
                        }
                    }
                } catch (Throwable th) {
                    this.zzayf = null;
                    zziy<zzjc> zziy2 = this.zzayd;
                    if (!(zziy2 == null || this.zzaye == zziy2)) {
                        this.zzaxw.zza(zziy2);
                    }
                    throw th;
                } finally {
                    this.zzayd = null;
                }
            } catch (Throwable th2) {
                this.zzayf = null;
                zziy<zzjc> zziy3 = this.zzayd;
                if (!(zziy3 == null || this.zzaye == zziy3)) {
                    try {
                        this.zzaxw.zza(zziy3);
                    } finally {
                        this.zzayd = null;
                    }
                }
                throw th2;
            } finally {
            }
        }
    }

    /* access modifiers changed from: protected */
    public zzkt zza(zzky zzky, zzgw zzgw, boolean z) throws zzlb {
        return zzky.zzb(zzgw.zzafe, z);
    }

    private final void zza(zzkv zzkv) throws zzgl {
        throw zzgl.zza(zzkv, getIndex());
    }

    /* access modifiers changed from: protected */
    public void zza(long j, boolean z) throws zzgl {
        this.zzaza = false;
        this.zzazb = false;
        if (this.zzayf != null) {
            this.zzayr = -9223372036854775807L;
            this.zzays = -1;
            this.zzayt = -1;
            this.zzazd = true;
            this.zzazc = false;
            this.zzayu = false;
            this.zzayb.clear();
            this.zzayo = false;
            this.zzayp = false;
            if (this.zzayi || (this.zzayl && this.zzayz)) {
                zzgy();
                zzgv();
            } else if (this.zzayx != 0) {
                zzgy();
                zzgv();
            } else {
                this.zzayf.flush();
                this.zzayy = false;
            }
            if (this.zzayv && this.zzafz != null) {
                this.zzayw = 1;
            }
        }
    }

    private final boolean zzd(long j, long j2) throws zzgl {
        boolean z;
        boolean z2;
        if (this.zzayt < 0) {
            if (!this.zzaym || !this.zzayz) {
                this.zzayt = this.zzayf.dequeueOutputBuffer(this.zzayc, 0);
            } else {
                try {
                    this.zzayt = this.zzayf.dequeueOutputBuffer(this.zzayc, 0);
                } catch (IllegalStateException unused) {
                    zzha();
                    if (this.zzazb) {
                        zzgy();
                    }
                    return false;
                }
            }
            int i = this.zzayt;
            if (i >= 0) {
                if (this.zzayp) {
                    this.zzayp = false;
                    this.zzayf.releaseOutputBuffer(i, false);
                    this.zzayt = -1;
                    return true;
                }
                MediaCodec.BufferInfo bufferInfo = this.zzayc;
                if ((bufferInfo.flags & 4) != 0) {
                    zzha();
                    this.zzayt = -1;
                    return false;
                }
                ByteBuffer byteBuffer = this.zzajf[i];
                if (byteBuffer != null) {
                    byteBuffer.position(bufferInfo.offset);
                    MediaCodec.BufferInfo bufferInfo2 = this.zzayc;
                    byteBuffer.limit(bufferInfo2.offset + bufferInfo2.size);
                }
                long j3 = this.zzayc.presentationTimeUs;
                int size = this.zzayb.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        z2 = false;
                        break;
                    } else if (this.zzayb.get(i2).longValue() == j3) {
                        this.zzayb.remove(i2);
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                this.zzayu = z2;
            } else if (i == -2) {
                MediaFormat outputFormat = this.zzayf.getOutputFormat();
                if (this.zzayj && outputFormat.getInteger("width") == 32 && outputFormat.getInteger("height") == 32) {
                    this.zzayp = true;
                } else {
                    if (this.zzayn) {
                        outputFormat.setInteger("channel-count", 1);
                    }
                    onOutputFormatChanged(this.zzayf, outputFormat);
                }
                return true;
            } else if (i == -3) {
                this.zzajf = this.zzayf.getOutputBuffers();
                return true;
            } else {
                if (this.zzayk && (this.zzaza || this.zzayx == 2)) {
                    zzha();
                }
                return false;
            }
        }
        if (!this.zzaym || !this.zzayz) {
            MediaCodec mediaCodec = this.zzayf;
            ByteBuffer[] byteBufferArr = this.zzajf;
            int i3 = this.zzayt;
            ByteBuffer byteBuffer2 = byteBufferArr[i3];
            MediaCodec.BufferInfo bufferInfo3 = this.zzayc;
            z = zza(j, j2, mediaCodec, byteBuffer2, i3, bufferInfo3.flags, bufferInfo3.presentationTimeUs, this.zzayu);
        } else {
            try {
                z = zza(j, j2, this.zzayf, this.zzajf[this.zzayt], this.zzayt, this.zzayc.flags, this.zzayc.presentationTimeUs, this.zzayu);
            } catch (IllegalStateException unused2) {
                zzha();
                if (this.zzazb) {
                    zzgy();
                }
                return false;
            }
        }
        if (!z) {
            return false;
        }
        long j4 = this.zzayc.presentationTimeUs;
        this.zzayt = -1;
        return true;
    }
}
