package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

final /* synthetic */ class zzbyt implements zzded {
    static final zzded zzdoq = new zzbyt();

    private zzbyt() {
    }

    public final Object apply(Object obj) {
        ArrayList arrayList = new ArrayList();
        for (zzabu zzabu : (List) obj) {
            if (zzabu != null) {
                arrayList.add(zzabu);
            }
        }
        return arrayList;
    }
}
