package com.google.android.gms.internal.measurement;

enum zzfa {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);
    
    private final boolean zze;

    private zzfa(boolean z) {
        this.zze = z;
    }
}
