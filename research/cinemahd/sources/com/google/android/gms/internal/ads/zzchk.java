package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzchk {
    public static final zzajv<zzchk> zzfwm = new zzchj();
    public final zzaqq zzfwi;
    public final JSONObject zzfwj;
    public final zzchn zzfwl;

    public zzchk(zzchn zzchn, JSONObject jSONObject, zzaqq zzaqq) {
        this.zzfwl = zzchn;
        this.zzfwj = jSONObject;
        this.zzfwi = zzaqq;
    }
}
