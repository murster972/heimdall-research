package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.Executor;

final class zzazf implements Executor {
    private final Handler zzdwl = new zzavv(Looper.getMainLooper());

    zzazf() {
    }

    public final void execute(Runnable runnable) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            try {
                runnable.run();
            } catch (Throwable th) {
                zzq.zzkq();
                zzawb.zza(zzq.zzku().getApplicationContext(), th);
                throw th;
            }
        } else {
            this.zzdwl.post(runnable);
        }
    }
}
