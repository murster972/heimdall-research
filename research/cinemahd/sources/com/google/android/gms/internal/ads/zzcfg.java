package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;

final /* synthetic */ class zzcfg implements zzdgf {
    static final zzdgf zzbkw = new zzcfg();

    private zzcfg() {
    }

    public final zzdhe zzf(Object obj) {
        return zzdgs.zzk(((ExecutionException) obj).getCause());
    }
}
