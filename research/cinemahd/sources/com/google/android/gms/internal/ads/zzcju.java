package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcju implements Callable {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcjr zzfzb;

    zzcju(zzcjr zzcjr, zzczt zzczt, zzczl zzczl) {
        this.zzfzb = zzcjr;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
    }

    public final Object call() {
        return this.zzfzb.zzc(this.zzfot, this.zzfel);
    }
}
