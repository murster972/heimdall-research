package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;

final class zzdn implements Runnable {
    private final /* synthetic */ Context zzwf;
    private final /* synthetic */ View zzwh;
    private final /* synthetic */ Activity zzwi;

    zzdn(zzdi zzdi, Context context, View view, Activity activity) {
        this.zzwf = context;
        this.zzwh = view;
        this.zzwi = activity;
    }

    public final void run() {
        zzdi.zzvc.zza(this.zzwf, this.zzwh, this.zzwi);
    }
}
