package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.common.util.Clock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class zzclp {
    /* access modifiers changed from: private */
    public final Clock zzbmq;
    private final List<String> zzgad = Collections.synchronizedList(new ArrayList());

    public zzclp(Clock clock) {
        this.zzbmq = clock;
    }

    public final <T> zzdhe<T> zza(zzczl zzczl, zzdhe<T> zzdhe) {
        long a2 = this.zzbmq.a();
        String str = zzczl.zzdcm;
        if (str != null) {
            zzdgs.zza(zzdhe, new zzcls(this, str, a2), zzazd.zzdwj);
        }
        return zzdhe;
    }

    public final String zzamh() {
        return TextUtils.join("_", this.zzgad);
    }

    /* access modifiers changed from: private */
    public final void zza(String str, int i, long j) {
        List<String> list = this.zzgad;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33);
        sb.append(str);
        sb.append(".");
        sb.append(i);
        sb.append(".");
        sb.append(j);
        list.add(sb.toString());
    }
}
