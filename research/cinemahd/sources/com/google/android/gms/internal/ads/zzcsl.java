package com.google.android.gms.internal.ads;

public final class zzcsl implements zzdxg<zzcsj> {
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<zzczj> zzfza;

    private zzcsl(zzdxp<zzdhd> zzdxp, zzdxp<zzczj> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzfza = zzdxp2;
    }

    public static zzcsl zzaq(zzdxp<zzdhd> zzdxp, zzdxp<zzczj> zzdxp2) {
        return new zzcsl(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcsj(this.zzfcv.get(), this.zzfza.get());
    }
}
