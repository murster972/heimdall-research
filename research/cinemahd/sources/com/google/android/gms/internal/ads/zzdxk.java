package com.google.android.gms.internal.ads;

public final class zzdxk<K, V> extends zzdxc<K, V, V> {
    private zzdxk(int i) {
        super(i);
    }

    public final /* synthetic */ zzdxc zza(Object obj, zzdxp zzdxp) {
        super.zza(obj, zzdxp);
        return this;
    }

    public final zzdxi<K, V> zzbdo() {
        return new zzdxi<>(this.zzhzy);
    }
}
