package com.google.android.gms.internal.cast;

final class zzag extends zzak {
    private final /* synthetic */ zzac zzra;

    private zzag(zzac zzac) {
        this.zzra = zzac;
    }

    public final void zza(long j, long j2) {
        this.zzra.publishProgress(new Long[]{Long.valueOf(j), Long.valueOf(j2)});
    }

    public final int zzs() {
        return 12451009;
    }
}
