package com.google.android.gms.internal.ads;

public final class zzbgh implements zzdxg<zzcis<zzdac, zzcjx>> {
    private final zzbga zzejr;
    private final zzdxp<zzcka> zzejs;

    public zzbgh(zzbga zzbga, zzdxp<zzcka> zzdxp) {
        this.zzejr = zzbga;
        this.zzejs = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzcis) zzdxm.zza(new zzcmf(this.zzejs.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
