package com.google.android.gms.internal.ads;

public final class zzlb extends Exception {
    private zzlb(Throwable th) {
        super("Failed to query underlying media codecs", th);
    }
}
