package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

public final class zzdgx<V> {
    private final boolean zzgwc;
    private final zzdeu<zzdhe<? extends V>> zzgww;

    private zzdgx(boolean z, zzdeu<zzdhe<? extends V>> zzdeu) {
        this.zzgwc = z;
        this.zzgww = zzdeu;
    }

    public final <C> zzdhe<C> zza(Callable<C> callable, Executor executor) {
        return new zzdgg(this.zzgww, this.zzgwc, executor, callable);
    }

    /* synthetic */ zzdgx(boolean z, zzdeu zzdeu, zzdgv zzdgv) {
        this(z, zzdeu);
    }
}
