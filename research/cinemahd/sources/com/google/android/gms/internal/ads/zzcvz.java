package com.google.android.gms.internal.ads;

public final class zzcvz implements zzdxg<String> {
    private final zzcvw zzgih;

    public zzcvz(zzcvw zzcvw) {
        this.zzgih = zzcvw;
    }

    public static String zzc(zzcvw zzcvw) {
        return (String) zzdxm.zza(zzcvw.zzant(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzc(this.zzgih);
    }
}
