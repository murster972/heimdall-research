package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Collections;
import java.util.List;

public final class zzcok extends zzvt {
    private final ViewGroup zzfdu;
    private final zzczu zzfgl;
    private final zzvh zzfjw;
    private final zzbkk zzgcp;
    private final Context zzup;

    public zzcok(Context context, zzvh zzvh, zzczu zzczu, zzbkk zzbkk) {
        this.zzup = context;
        this.zzfjw = zzvh;
        this.zzfgl = zzczu;
        this.zzgcp = zzbkk;
        FrameLayout frameLayout = new FrameLayout(this.zzup);
        frameLayout.removeAllViews();
        frameLayout.addView(this.zzgcp.zzaga(), zzq.zzks().zzwp());
        frameLayout.setMinimumHeight(zzjz().heightPixels);
        frameLayout.setMinimumWidth(zzjz().widthPixels);
        this.zzfdu = frameLayout;
    }

    public final void destroy() throws RemoteException {
        Preconditions.a("destroy must be called on the main UI thread.");
        this.zzgcp.destroy();
    }

    public final Bundle getAdMetadata() throws RemoteException {
        zzayu.zzey("getAdMetadata is not supported in Publisher AdView returned by AdLoader.");
        return new Bundle();
    }

    public final String getAdUnitId() throws RemoteException {
        return this.zzfgl.zzgmm;
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        if (this.zzgcp.zzags() != null) {
            return this.zzgcp.zzags().getMediationAdapterClassName();
        }
        return null;
    }

    public final zzxb getVideoController() throws RemoteException {
        return this.zzgcp.getVideoController();
    }

    public final boolean isLoading() throws RemoteException {
        return false;
    }

    public final boolean isReady() throws RemoteException {
        return false;
    }

    public final void pause() throws RemoteException {
        Preconditions.a("destroy must be called on the main UI thread.");
        this.zzgcp.zzagr().zzbv((Context) null);
    }

    public final void resume() throws RemoteException {
        Preconditions.a("destroy must be called on the main UI thread.");
        this.zzgcp.zzagr().zzbw((Context) null);
    }

    public final void setImmersiveMode(boolean z) throws RemoteException {
    }

    public final void setManualImpressionsEnabled(boolean z) throws RemoteException {
        zzayu.zzey("setManualImpressionsEnabled is not supported in Publisher AdView returned by AdLoader.");
    }

    public final void setUserId(String str) throws RemoteException {
    }

    public final void showInterstitial() throws RemoteException {
    }

    public final void stopLoading() throws RemoteException {
    }

    public final void zza(zzaoy zzaoy) throws RemoteException {
    }

    public final void zza(zzape zzape, String str) throws RemoteException {
    }

    public final void zza(zzaro zzaro) throws RemoteException {
    }

    public final void zza(zzrg zzrg) throws RemoteException {
    }

    public final void zza(zzuo zzuo) throws RemoteException {
    }

    public final void zza(zzxh zzxh) throws RemoteException {
    }

    public final boolean zza(zzug zzug) throws RemoteException {
        zzayu.zzey("loadAd is not supported for a Publisher AdView returned from AdLoader.");
        return false;
    }

    public final void zzbr(String str) throws RemoteException {
    }

    public final IObjectWrapper zzjx() throws RemoteException {
        return ObjectWrapper.a(this.zzfdu);
    }

    public final void zzjy() throws RemoteException {
        this.zzgcp.zzjy();
    }

    public final zzuj zzjz() {
        Preconditions.a("getAdSize must be called on the main UI thread.");
        return zzczy.zza(this.zzup, (List<zzczk>) Collections.singletonList(this.zzgcp.zzafz()));
    }

    public final String zzka() throws RemoteException {
        if (this.zzgcp.zzags() != null) {
            return this.zzgcp.zzags().getMediationAdapterClassName();
        }
        return null;
    }

    public final zzxa zzkb() {
        return this.zzgcp.zzags();
    }

    public final zzwc zzkc() throws RemoteException {
        return this.zzfgl.zzgmr;
    }

    public final zzvh zzkd() throws RemoteException {
        return this.zzfjw;
    }

    public final void zza(zzyw zzyw) throws RemoteException {
        zzayu.zzey("setVideoOptions is not supported in Publisher AdView returned by AdLoader.");
    }

    public final void zza(zzuj zzuj) throws RemoteException {
        Preconditions.a("setAdSize must be called on the main UI thread.");
        zzbkk zzbkk = this.zzgcp;
        if (zzbkk != null) {
            zzbkk.zza(this.zzfdu, zzuj);
        }
    }

    public final void zza(zzaak zzaak) throws RemoteException {
        zzayu.zzey("setOnCustomRenderedAdLoadedListener is not supported in Publisher AdView returned by AdLoader.");
    }

    public final void zza(zzvg zzvg) throws RemoteException {
        zzayu.zzey("setAdClickListener is not supported in Publisher AdView returned by AdLoader.");
    }

    public final void zza(zzwi zzwi) throws RemoteException {
        zzayu.zzey("setCorrelationIdProvider is not supported in Publisher AdView returned by AdLoader.");
    }

    public final void zza(zzvh zzvh) throws RemoteException {
        zzayu.zzey("setAdListener is not supported in Publisher AdView returned by AdLoader.");
    }

    public final void zza(zzwc zzwc) throws RemoteException {
        zzayu.zzey("setAppEventListener is not supported in Publisher AdView returned by AdLoader.");
    }

    public final void zza(zzvx zzvx) throws RemoteException {
        zzayu.zzey("setAdMetadataListener is not supported in Publisher AdView returned by AdLoader.");
    }
}
