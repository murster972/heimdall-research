package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import java.util.concurrent.Executor;

public final class zzbko implements zzdxg<zzbkm> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbsy> zzesn;
    private final zzdxp<zzbmg> zzetc;
    private final zzdxp<zzcok> zzexw;
    private final zzdxp<View> zzfbq;
    private final zzdxp<zzczk> zzfee;
    private final zzdxp<zzbdi> zzfef;
    private final zzdxp<zzbme> zzfeg;
    private final zzdxp<zzbwz> zzfeh;
    private final zzdxp<Executor> zzfei;

    public zzbko(zzdxp<zzbmg> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzczk> zzdxp3, zzdxp<View> zzdxp4, zzdxp<zzbdi> zzdxp5, zzdxp<zzbme> zzdxp6, zzdxp<zzbwz> zzdxp7, zzdxp<zzbsy> zzdxp8, zzdxp<zzcok> zzdxp9, zzdxp<Executor> zzdxp10) {
        this.zzetc = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfee = zzdxp3;
        this.zzfbq = zzdxp4;
        this.zzfef = zzdxp5;
        this.zzfeg = zzdxp6;
        this.zzfeh = zzdxp7;
        this.zzesn = zzdxp8;
        this.zzexw = zzdxp9;
        this.zzfei = zzdxp10;
    }

    public static zzbkm zza(zzbmg zzbmg, Context context, zzczk zzczk, View view, zzbdi zzbdi, zzbme zzbme, zzbwz zzbwz, zzbsy zzbsy, zzdxa<zzcok> zzdxa, Executor executor) {
        return new zzbkm(zzbmg, context, zzczk, view, zzbdi, zzbme, zzbwz, zzbsy, zzdxa, executor);
    }

    public final /* synthetic */ Object get() {
        return new zzbkm(this.zzetc.get(), this.zzejv.get(), this.zzfee.get(), this.zzfbq.get(), this.zzfef.get(), this.zzfeg.get(), this.zzfeh.get(), this.zzesn.get(), zzdxd.zzao(this.zzexw), this.zzfei.get());
    }
}
