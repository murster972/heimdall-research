package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdnk;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class zzdiq<P> {
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private final Class<P> zzgxu;
    private ConcurrentMap<String, List<zzdip<P>>> zzgye = new ConcurrentHashMap();
    private zzdip<P> zzgyf;

    private zzdiq(Class<P> cls) {
        this.zzgxu = cls;
    }

    public static <P> zzdiq<P> zza(Class<P> cls) {
        return new zzdiq<>(cls);
    }

    public final Class<P> zzarz() {
        return this.zzgxu;
    }

    public final zzdip<P> zzasm() {
        return this.zzgyf;
    }

    public final void zza(zzdip<P> zzdip) {
        if (zzdip == null) {
            throw new IllegalArgumentException("the primary entry must be non-null");
        } else if (zzdip.zzasj() == zzdne.ENABLED) {
            List list = (List) this.zzgye.get(new String(zzdip.zzasl(), UTF_8));
            if (list == null) {
                list = Collections.emptyList();
            }
            if (!list.isEmpty()) {
                this.zzgyf = zzdip;
                return;
            }
            throw new IllegalArgumentException("the primary entry cannot be set to an entry which is not held by this primitive set");
        } else {
            throw new IllegalArgumentException("the primary entry has to be ENABLED");
        }
    }

    public final zzdip<P> zza(P p, zzdnk.zza zza) throws GeneralSecurityException {
        byte[] bArr;
        if (zza.zzasj() == zzdne.ENABLED) {
            int i = zzdic.zzgxr[zza.zzask().ordinal()];
            if (i == 1 || i == 2) {
                bArr = ByteBuffer.allocate(5).put((byte) 0).putInt(zza.zzawb()).array();
            } else if (i == 3) {
                bArr = ByteBuffer.allocate(5).put((byte) 1).putInt(zza.zzawb()).array();
            } else if (i == 4) {
                bArr = zzdhz.zzgxq;
            } else {
                throw new GeneralSecurityException("unknown output prefix type");
            }
            zzdip<P> zzdip = new zzdip<>(p, bArr, zza.zzasj(), zza.zzask());
            ArrayList arrayList = new ArrayList();
            arrayList.add(zzdip);
            String str = new String(zzdip.zzasl(), UTF_8);
            List list = (List) this.zzgye.put(str, Collections.unmodifiableList(arrayList));
            if (list != null) {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(list);
                arrayList2.add(zzdip);
                this.zzgye.put(str, Collections.unmodifiableList(arrayList2));
            }
            return zzdip;
        }
        throw new GeneralSecurityException("only ENABLED key is allowed");
    }
}
