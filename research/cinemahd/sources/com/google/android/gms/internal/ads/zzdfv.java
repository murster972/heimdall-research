package com.google.android.gms.internal.ads;

import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;

public abstract class zzdfv extends AbstractExecutorService implements zzdhd {
    /* access modifiers changed from: protected */
    public final <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return zzdhs.zza(runnable, t);
    }

    /* renamed from: zzd */
    public final <T> zzdhe<T> submit(Callable<T> callable) {
        return (zzdhe) super.submit(callable);
    }

    /* renamed from: zzf */
    public final zzdhe<?> submit(Runnable runnable) {
        return (zzdhe) super.submit(runnable);
    }

    /* access modifiers changed from: protected */
    public final <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return zzdhs.zze(callable);
    }

    public /* synthetic */ Future submit(Runnable runnable, Object obj) {
        return (zzdhe) super.submit(runnable, obj);
    }
}
