package com.google.android.gms.internal.ads;

final /* synthetic */ class zzctk implements zzded {
    private final String zzcyz;

    zzctk(String str) {
        this.zzcyz = str;
    }

    public final Object apply(Object obj) {
        Throwable th = (Throwable) obj;
        String valueOf = String.valueOf(this.zzcyz);
        zzayu.zzex(valueOf.length() != 0 ? "Error calling adapter: ".concat(valueOf) : new String("Error calling adapter: "));
        return null;
    }
}
