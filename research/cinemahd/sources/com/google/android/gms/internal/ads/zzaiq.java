package com.google.android.gms.internal.ads;

import java.util.Map;

final class zzaiq implements zzafn<zzbdi> {
    private final /* synthetic */ zzaih zzcze;
    /* access modifiers changed from: private */
    public final zzafn<? super zzajq> zzczh;

    public zzaiq(zzaih zzaih, zzafn<? super zzajq> zzafn) {
        this.zzcze = zzaih;
        this.zzczh = zzafn;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        this.zzczh.zza(this.zzcze, map);
    }
}
