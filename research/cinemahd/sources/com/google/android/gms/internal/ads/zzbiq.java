package com.google.android.gms.internal.ads;

import java.util.Map;

public final class zzbiq implements zzbil {
    private zzavu zzdrk;

    public zzbiq(zzavu zzavu) {
        this.zzdrk = zzavu;
    }

    public final void zzk(Map<String, String> map) {
        this.zzdrk.zzap(Boolean.parseBoolean(map.get("content_vertical_opted_out")));
    }
}
