package com.google.android.gms.internal.ads;

public final class zzajf extends zzazo<zzajq> {
    private final Object lock = new Object();
    /* access modifiers changed from: private */
    public final zzajj zzdab;
    private boolean zzdac;

    public zzajf(zzajj zzajj) {
        this.zzdab = zzajj;
    }

    public final void release() {
        synchronized (this.lock) {
            if (!this.zzdac) {
                this.zzdac = true;
                zza(new zzaji(this), new zzazm());
                zza(new zzajh(this), new zzajk(this));
            }
        }
    }
}
