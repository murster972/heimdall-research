package com.google.android.gms.internal.ads;

import java.util.List;

final class zzdsp extends zzdso {
    private zzdsp() {
        super();
    }

    private static <E> zzdsb<E> zzc(Object obj, long j) {
        return (zzdsb) zzduy.zzp(obj, j);
    }

    /* access modifiers changed from: package-private */
    public final <L> List<L> zza(Object obj, long j) {
        zzdsb zzc = zzc(obj, j);
        if (zzc.zzaxp()) {
            return zzc;
        }
        int size = zzc.size();
        zzdsb zzfd = zzc.zzfd(size == 0 ? 10 : size << 1);
        zzduy.zza(obj, j, (Object) zzfd);
        return zzfd;
    }

    /* access modifiers changed from: package-private */
    public final void zzb(Object obj, long j) {
        zzc(obj, j).zzaxq();
    }

    /* access modifiers changed from: package-private */
    public final <E> void zza(Object obj, Object obj2, long j) {
        zzdsb zzc = zzc(obj, j);
        zzdsb zzc2 = zzc(obj2, j);
        int size = zzc.size();
        int size2 = zzc2.size();
        if (size > 0 && size2 > 0) {
            if (!zzc.zzaxp()) {
                zzc = zzc.zzfd(size2 + size);
            }
            zzc.addAll(zzc2);
        }
        if (size > 0) {
            zzc2 = zzc;
        }
        zzduy.zza(obj, j, (Object) zzc2);
    }
}
