package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationBannerAd;
import com.google.android.gms.ads.mediation.MediationBannerAdCallback;
import com.google.android.gms.dynamic.ObjectWrapper;

final class zzanq implements MediationAdLoadCallback<MediationBannerAd, MediationBannerAdCallback> {
    private final /* synthetic */ zzali zzdeo;
    private final /* synthetic */ zzamw zzdeq;

    zzanq(zzann zzann, zzamw zzamw, zzali zzali) {
        this.zzdeq = zzamw;
        this.zzdeo = zzali;
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final MediationBannerAdCallback onSuccess(MediationBannerAd mediationBannerAd) {
        if (mediationBannerAd == null) {
            zzayu.zzez("Adapter incorrectly returned a null ad. The onFailure() callback should be called if an adapter fails to load an ad.");
            try {
                this.zzdeq.zzdl("Adapter returned null.");
                return null;
            } catch (RemoteException e) {
                zzayu.zzc("", e);
                return null;
            }
        } else {
            try {
                this.zzdeq.zzx(ObjectWrapper.a(mediationBannerAd.getView()));
            } catch (RemoteException e2) {
                zzayu.zzc("", e2);
            }
            return new zzant(this.zzdeo);
        }
    }

    public final void onFailure(String str) {
        try {
            this.zzdeq.zzdl(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
