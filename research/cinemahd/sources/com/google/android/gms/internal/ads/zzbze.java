package com.google.android.gms.internal.ads;

import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import org.json.JSONArray;
import org.json.JSONObject;

public final class zzbze {
    private final Executor executor;
    private final zzbyu zzfow;

    public zzbze(Executor executor2, zzbyu zzbyu) {
        this.executor = executor2;
        this.zzfow = zzbyu;
    }

    public final zzdhe<List<zzbzf>> zzg(JSONObject jSONObject, String str) {
        zzdhe<O> zzdhe;
        String optString;
        char c;
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray == null) {
            return zzdgs.zzaj(Collections.emptyList());
        }
        ArrayList arrayList = new ArrayList();
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (!(optJSONObject == null || (optString = optJSONObject.optString(MediationMetaData.KEY_NAME)) == null)) {
                String optString2 = optJSONObject.optString("type");
                if ("string".equals(optString2)) {
                    c = 1;
                } else {
                    c = "image".equals(optString2) ? (char) 2 : 0;
                }
                if (c == 1) {
                    zzdhe = zzdgs.zzaj(new zzbzf(optString, optJSONObject.optString("string_value")));
                } else if (c == 2) {
                    zzdhe = zzdgs.zzb(this.zzfow.zzc(optJSONObject, "image_value"), new zzbzg(optString), this.executor);
                }
                arrayList.add(zzdhe);
            }
            zzdhe = zzdgs.zzaj(null);
            arrayList.add(zzdhe);
        }
        return zzdgs.zzb(zzdgs.zzg(arrayList), zzbzd.zzdoq, this.executor);
    }
}
