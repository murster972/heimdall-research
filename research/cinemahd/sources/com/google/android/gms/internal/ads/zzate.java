package com.google.android.gms.internal.ads;

import java.util.Map;

final /* synthetic */ class zzate implements zzdgf {
    private final zzatf zzdoc;

    zzate(zzatf zzatf) {
        this.zzdoc = zzatf;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzdoc.zzh((Map) obj);
    }
}
