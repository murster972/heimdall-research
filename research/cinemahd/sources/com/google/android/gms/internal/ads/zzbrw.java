package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbrw implements zzdxg<Set<zzbsu<zzbow>>> {
    private final zzbrm zzfim;

    private zzbrw(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbrw zzm(zzbrm zzbrm) {
        return new zzbrw(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfim.zzaho(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
