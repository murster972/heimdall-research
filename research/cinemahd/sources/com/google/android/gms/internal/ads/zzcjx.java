package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzcjx extends zzary implements zzbpu {
    private zzarz zzfze;
    private zzbpx zzfzf;
    private zzbth zzfzg;

    public final synchronized void zza(zzarz zzarz) {
        this.zzfze = zzarz;
    }

    public final synchronized void zzaf(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzaf(iObjectWrapper);
        }
        if (this.zzfzg != null) {
            this.zzfzg.onInitializationSucceeded();
        }
    }

    public final synchronized void zzag(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzag(iObjectWrapper);
        }
        if (this.zzfzf != null) {
            this.zzfzf.onAdLoaded();
        }
    }

    public final synchronized void zzah(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzah(iObjectWrapper);
        }
    }

    public final synchronized void zzai(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzai(iObjectWrapper);
        }
    }

    public final synchronized void zzaj(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzaj(iObjectWrapper);
        }
    }

    public final synchronized void zzak(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzak(iObjectWrapper);
        }
    }

    public final synchronized void zzal(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzal(iObjectWrapper);
        }
    }

    public final synchronized void zzam(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzam(iObjectWrapper);
        }
    }

    public final synchronized void zzb(Bundle bundle) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzb(bundle);
        }
    }

    public final synchronized void zzd(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zzd(iObjectWrapper, i);
        }
        if (this.zzfzg != null) {
            this.zzfzg.zzdh(i);
        }
    }

    public final synchronized void zze(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zze(iObjectWrapper, i);
        }
        if (this.zzfzf != null) {
            this.zzfzf.onAdFailedToLoad(i);
        }
    }

    public final synchronized void zza(zzbpx zzbpx) {
        this.zzfzf = zzbpx;
    }

    public final synchronized void zza(zzbth zzbth) {
        this.zzfzg = zzbth;
    }

    public final synchronized void zza(IObjectWrapper iObjectWrapper, zzasd zzasd) throws RemoteException {
        if (this.zzfze != null) {
            this.zzfze.zza(iObjectWrapper, zzasd);
        }
    }
}
