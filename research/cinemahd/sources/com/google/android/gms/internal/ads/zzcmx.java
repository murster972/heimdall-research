package com.google.android.gms.internal.ads;

public final class zzcmx implements zzdxg<zzcmy> {
    private final zzdxp<zzcnz> zzgbd;

    private zzcmx(zzdxp<zzcnz> zzdxp) {
        this.zzgbd = zzdxp;
    }

    public static zzcmx zzaf(zzdxp<zzcnz> zzdxp) {
        return new zzcmx(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcmy(this.zzgbd.get());
    }
}
