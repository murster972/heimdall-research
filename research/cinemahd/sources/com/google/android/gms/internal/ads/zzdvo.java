package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class zzdvo {
    private final ByteBuffer zzako;
    private zzdrb zzhtk;
    private int zzhtl;

    private zzdvo(byte[] bArr, int i, int i2) {
        this(ByteBuffer.wrap(bArr, i, i2));
    }

    public static zzdvo zzaa(byte[] bArr) {
        return zzq(bArr, 0, bArr.length);
    }

    public static int zzaf(int i, int i2) {
        return zzfz(i) + zzga(i2);
    }

    public static int zzb(int i, zzdvt zzdvt) {
        int zzfz = zzfz(i);
        int zzazu = zzdvt.zzazu();
        return zzfz + zzgh(zzazu) + zzazu;
    }

    private static void zzd(CharSequence charSequence, ByteBuffer byteBuffer) {
        int i;
        int i2;
        char charAt;
        if (!byteBuffer.isReadOnly()) {
            int i3 = 0;
            if (byteBuffer.hasArray()) {
                try {
                    byte[] array = byteBuffer.array();
                    int arrayOffset = byteBuffer.arrayOffset() + byteBuffer.position();
                    int remaining = byteBuffer.remaining();
                    int length = charSequence.length();
                    int i4 = remaining + arrayOffset;
                    while (i3 < length) {
                        int i5 = i3 + arrayOffset;
                        if (i5 >= i4 || (charAt = charSequence.charAt(i3)) >= 128) {
                            break;
                        }
                        array[i5] = (byte) charAt;
                        i3++;
                    }
                    if (i3 == length) {
                        i = arrayOffset + length;
                    } else {
                        i = arrayOffset + i3;
                        while (i3 < length) {
                            char charAt2 = charSequence.charAt(i3);
                            if (charAt2 < 128 && i < i4) {
                                i2 = i + 1;
                                array[i] = (byte) charAt2;
                            } else if (charAt2 < 2048 && i <= i4 - 2) {
                                int i6 = i + 1;
                                array[i] = (byte) ((charAt2 >>> 6) | 960);
                                i = i6 + 1;
                                array[i6] = (byte) ((charAt2 & '?') | 128);
                                i3++;
                            } else if ((charAt2 < 55296 || 57343 < charAt2) && i <= i4 - 3) {
                                int i7 = i + 1;
                                array[i] = (byte) ((charAt2 >>> 12) | 480);
                                int i8 = i7 + 1;
                                array[i7] = (byte) (((charAt2 >>> 6) & 63) | 128);
                                i2 = i8 + 1;
                                array[i8] = (byte) ((charAt2 & '?') | 128);
                            } else if (i <= i4 - 4) {
                                int i9 = i3 + 1;
                                if (i9 != charSequence.length()) {
                                    char charAt3 = charSequence.charAt(i9);
                                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                                        int i10 = i + 1;
                                        array[i] = (byte) ((codePoint >>> 18) | 240);
                                        int i11 = i10 + 1;
                                        array[i10] = (byte) (((codePoint >>> 12) & 63) | 128);
                                        int i12 = i11 + 1;
                                        array[i11] = (byte) (((codePoint >>> 6) & 63) | 128);
                                        i = i12 + 1;
                                        array[i12] = (byte) ((codePoint & 63) | 128);
                                        i3 = i9;
                                        i3++;
                                    } else {
                                        i3 = i9;
                                    }
                                }
                                StringBuilder sb = new StringBuilder(39);
                                sb.append("Unpaired surrogate at index ");
                                sb.append(i3 - 1);
                                throw new IllegalArgumentException(sb.toString());
                            } else {
                                StringBuilder sb2 = new StringBuilder(37);
                                sb2.append("Failed writing ");
                                sb2.append(charAt2);
                                sb2.append(" at index ");
                                sb2.append(i);
                                throw new ArrayIndexOutOfBoundsException(sb2.toString());
                            }
                            i = i2;
                            i3++;
                        }
                    }
                    byteBuffer.position(i - byteBuffer.arrayOffset());
                } catch (ArrayIndexOutOfBoundsException e) {
                    BufferOverflowException bufferOverflowException = new BufferOverflowException();
                    bufferOverflowException.initCause(e);
                    throw bufferOverflowException;
                }
            } else {
                int length2 = charSequence.length();
                while (i3 < length2) {
                    char charAt4 = charSequence.charAt(i3);
                    if (charAt4 < 128) {
                        byteBuffer.put((byte) charAt4);
                    } else if (charAt4 < 2048) {
                        byteBuffer.put((byte) ((charAt4 >>> 6) | 960));
                        byteBuffer.put((byte) ((charAt4 & '?') | 128));
                    } else if (charAt4 < 55296 || 57343 < charAt4) {
                        byteBuffer.put((byte) ((charAt4 >>> 12) | 480));
                        byteBuffer.put((byte) (((charAt4 >>> 6) & 63) | 128));
                        byteBuffer.put((byte) ((charAt4 & '?') | 128));
                    } else {
                        int i13 = i3 + 1;
                        if (i13 != charSequence.length()) {
                            char charAt5 = charSequence.charAt(i13);
                            if (Character.isSurrogatePair(charAt4, charAt5)) {
                                int codePoint2 = Character.toCodePoint(charAt4, charAt5);
                                byteBuffer.put((byte) ((codePoint2 >>> 18) | 240));
                                byteBuffer.put((byte) (((codePoint2 >>> 12) & 63) | 128));
                                byteBuffer.put((byte) (((codePoint2 >>> 6) & 63) | 128));
                                byteBuffer.put((byte) ((codePoint2 & 63) | 128));
                                i3 = i13;
                            } else {
                                i3 = i13;
                            }
                        }
                        StringBuilder sb3 = new StringBuilder(39);
                        sb3.append("Unpaired surrogate at index ");
                        sb3.append(i3 - 1);
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    i3++;
                }
            }
        } else {
            throw new ReadOnlyBufferException();
        }
    }

    public static int zzfz(int i) {
        return zzgh(i << 3);
    }

    public static int zzg(int i, String str) {
        return zzfz(i) + zzhh(str);
    }

    public static int zzga(int i) {
        if (i >= 0) {
            return zzgh(i);
        }
        return 10;
    }

    public static int zzgh(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    private final void zzha(int i) throws IOException {
        byte b = (byte) i;
        if (this.zzako.hasRemaining()) {
            this.zzako.put(b);
            return;
        }
        throw new zzdvn(this.zzako.position(), this.zzako.limit());
    }

    private final void zzhb(int i) throws IOException {
        while ((i & -128) != 0) {
            zzha((i & 127) | 128);
            i >>>= 7;
        }
        zzha(i);
    }

    public static int zzhh(String str) {
        int zza = zza(str);
        return zzgh(zza) + zza;
    }

    public static zzdvo zzq(byte[] bArr, int i, int i2) {
        return new zzdvo(bArr, 0, i2);
    }

    public final void zza(int i, zzdvt zzdvt) throws IOException {
        zzaa(i, 2);
        if (zzdvt.zzhhn < 0) {
            zzdvt.zzazu();
        }
        zzhb(zzdvt.zzhhn);
        zzdvt.zza(this);
    }

    public final void zzab(int i, int i2) throws IOException {
        zzaa(i, 0);
        if (i2 >= 0) {
            zzhb(i2);
        } else {
            zzfs((long) i2);
        }
    }

    public final void zzazd() {
        if (this.zzako.remaining() != 0) {
            throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", new Object[]{Integer.valueOf(this.zzako.remaining())}));
        }
    }

    public final void zze(int i, zzdte zzdte) throws IOException {
        if (this.zzhtk == null) {
            this.zzhtk = zzdrb.zzm(this.zzako);
            this.zzhtl = this.zzako.position();
        } else if (this.zzhtl != this.zzako.position()) {
            this.zzhtk.write(this.zzako.array(), this.zzhtl, this.zzako.position() - this.zzhtl);
            this.zzhtl = this.zzako.position();
        }
        zzdrb zzdrb = this.zzhtk;
        zzdrb.zza(i, zzdte);
        zzdrb.flush();
        this.zzhtl = this.zzako.position();
    }

    public final void zzf(int i, String str) throws IOException {
        zzaa(i, 2);
        try {
            int zzgh = zzgh(str.length());
            if (zzgh == zzgh(str.length() * 3)) {
                int position = this.zzako.position();
                if (this.zzako.remaining() >= zzgh) {
                    this.zzako.position(position + zzgh);
                    zzd(str, this.zzako);
                    int position2 = this.zzako.position();
                    this.zzako.position(position);
                    zzhb((position2 - position) - zzgh);
                    this.zzako.position(position2);
                    return;
                }
                throw new zzdvn(position + zzgh, this.zzako.limit());
            }
            zzhb(zza(str));
            zzd(str, this.zzako);
        } catch (BufferOverflowException e) {
            zzdvn zzdvn = new zzdvn(this.zzako.position(), this.zzako.limit());
            zzdvn.initCause(e);
            throw zzdvn;
        }
    }

    public final void zzfs(long j) throws IOException {
        while ((-128 & j) != 0) {
            zzha((((int) j) & 127) | 128);
            j >>>= 7;
        }
        zzha((int) j);
    }

    private zzdvo(ByteBuffer byteBuffer) {
        this.zzako = byteBuffer;
        this.zzako.order(ByteOrder.LITTLE_ENDIAN);
    }

    public final void zzaa(int i, int i2) throws IOException {
        zzhb((i << 3) | i2);
    }

    public final void zza(int i, byte[] bArr) throws IOException {
        zzaa(3, 2);
        zzhb(bArr.length);
        int length = bArr.length;
        if (this.zzako.remaining() >= length) {
            this.zzako.put(bArr, 0, length);
            return;
        }
        throw new zzdvn(this.zzako.position(), this.zzako.limit());
    }

    private static int zza(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < 128) {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < 2048) {
                i3 += (127 - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < 2048) {
                        i += (127 - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i2) >= 65536) {
                                i2++;
                            } else {
                                StringBuilder sb = new StringBuilder(39);
                                sb.append("Unpaired surrogate at index ");
                                sb.append(i2);
                                throw new IllegalArgumentException(sb.toString());
                            }
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        StringBuilder sb2 = new StringBuilder(54);
        sb2.append("UTF-8 length does not fit in int: ");
        sb2.append(((long) i3) + 4294967296L);
        throw new IllegalArgumentException(sb2.toString());
    }
}
