package com.google.android.gms.internal.ads;

public final class zzbmy implements zzdxg<zzczt> {
    private final zzbmt zzfgf;

    private zzbmy(zzbmt zzbmt) {
        this.zzfgf = zzbmt;
    }

    public static zzbmy zze(zzbmt zzbmt) {
        return new zzbmy(zzbmt);
    }

    public static zzczt zzf(zzbmt zzbmt) {
        return (zzczt) zzdxm.zza(zzbmt.zzagw(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzf(this.zzfgf);
    }
}
