package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.security.NetworkSecurityPolicy;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.PlatformVersion;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzavx implements zzavu {
    private final Object lock = new Object();
    private SharedPreferences zzcgc;
    private boolean zzdiv = true;
    private boolean zzdji = false;
    private String zzdjl = "";
    private boolean zzdku = true;
    private boolean zzdlf = true;
    private boolean zzdrw;
    private final List<Runnable> zzdrx = new ArrayList();
    private zzdhe<?> zzdry;
    private zzqi zzdrz = null;
    private SharedPreferences.Editor zzdsa;
    private boolean zzdsb = false;
    private String zzdsc;
    private String zzdsd;
    private long zzdse = 0;
    private long zzdsf = 0;
    private long zzdsg = 0;
    private int zzdsh = -1;
    private int zzdsi = 0;
    private Set<String> zzdsj = Collections.emptySet();
    private JSONObject zzdsk = new JSONObject();
    private String zzdsl = null;
    private int zzdsm = -1;

    private final void zzc(Bundle bundle) {
        zzazd.zzdwe.execute(new zzavz(this));
    }

    private final void zzwh() {
        zzdhe<?> zzdhe = this.zzdry;
        if (zzdhe != null && !zzdhe.isDone()) {
            try {
                this.zzdry.get(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                zzayu.zzd("Interrupted while waiting for preferences loaded.", e);
            } catch (CancellationException | ExecutionException | TimeoutException e2) {
                zzayu.zzc("Fail to initialize AdSharedPreferenceManager.", e2);
            }
        }
    }

    private final Bundle zzwi() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("listener_registration_bundle", true);
        synchronized (this.lock) {
            bundle.putBoolean("use_https", this.zzdiv);
            bundle.putBoolean("content_url_opted_out", this.zzdku);
            bundle.putBoolean("content_vertical_opted_out", this.zzdlf);
            bundle.putBoolean("auto_collect_location", this.zzdji);
            bundle.putInt("version_code", this.zzdsi);
            bundle.putStringArray("never_pool_slots", (String[]) this.zzdsj.toArray(new String[0]));
            bundle.putString("app_settings_json", this.zzdjl);
            bundle.putLong("app_settings_last_update_ms", this.zzdse);
            bundle.putLong("app_last_background_time_ms", this.zzdsf);
            bundle.putInt("request_in_session_count", this.zzdsh);
            bundle.putLong("first_ad_req_time_ms", this.zzdsg);
            bundle.putString("native_advanced_settings", this.zzdsk.toString());
            bundle.putString("display_cutout", this.zzdsl);
            bundle.putInt("app_measurement_npa", this.zzdsm);
            if (this.zzdsc != null) {
                bundle.putString("content_url_hashes", this.zzdsc);
            }
            if (this.zzdsd != null) {
                bundle.putString("content_vertical_hashes", this.zzdsd);
            }
        }
        return bundle;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x000f, code lost:
        r4 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        if (r4.length() == 0) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        r4 = "admob__".concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        r4 = new java.lang.String("admob__");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        r2.zzdry = com.google.android.gms.internal.ads.zzazd.zzdwe.zzf(new com.google.android.gms.internal.ads.zzavw(r2, r3, r4));
        r2.zzdrw = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000a, code lost:
        if (r4 != null) goto L_0x000f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000c, code lost:
        r4 = com.applovin.sdk.AppLovinMediationProvider.ADMOB;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(android.content.Context r3, java.lang.String r4, boolean r5) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.lock
            monitor-enter(r0)
            android.content.SharedPreferences r1 = r2.zzcgc     // Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            return
        L_0x0009:
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            if (r4 != 0) goto L_0x000f
            java.lang.String r4 = "admob"
            goto L_0x0025
        L_0x000f:
            java.lang.String r0 = "admob__"
            java.lang.String r4 = java.lang.String.valueOf(r4)
            int r1 = r4.length()
            if (r1 == 0) goto L_0x0020
            java.lang.String r4 = r0.concat(r4)
            goto L_0x0025
        L_0x0020:
            java.lang.String r4 = new java.lang.String
            r4.<init>(r0)
        L_0x0025:
            com.google.android.gms.internal.ads.zzdhd r0 = com.google.android.gms.internal.ads.zzazd.zzdwe
            com.google.android.gms.internal.ads.zzavw r1 = new com.google.android.gms.internal.ads.zzavw
            r1.<init>(r2, r3, r4)
            com.google.android.gms.internal.ads.zzdhe r3 = r0.zzf(r1)
            r2.zzdry = r3
            r2.zzdrw = r5
            return
        L_0x0035:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzavx.zza(android.content.Context, java.lang.String, boolean):void");
    }

    public final void zzao(boolean z) {
        zzwh();
        synchronized (this.lock) {
            if (this.zzdku != z) {
                this.zzdku = z;
                if (this.zzdsa != null) {
                    this.zzdsa.putBoolean("content_url_opted_out", z);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putBoolean("content_url_opted_out", this.zzdku);
                bundle.putBoolean("content_vertical_opted_out", this.zzdlf);
                zzc(bundle);
            }
        }
    }

    public final void zzap(boolean z) {
        zzwh();
        synchronized (this.lock) {
            if (this.zzdlf != z) {
                this.zzdlf = z;
                if (this.zzdsa != null) {
                    this.zzdsa.putBoolean("content_vertical_opted_out", z);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putBoolean("content_url_opted_out", this.zzdku);
                bundle.putBoolean("content_vertical_opted_out", this.zzdlf);
                zzc(bundle);
            }
        }
    }

    public final void zzaq(boolean z) {
        zzwh();
        synchronized (this.lock) {
            if (this.zzdji != z) {
                this.zzdji = z;
                if (this.zzdsa != null) {
                    this.zzdsa.putBoolean("auto_collect_location", z);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putBoolean("auto_collect_location", z);
                zzc(bundle);
            }
        }
    }

    public final void zzb(Runnable runnable) {
        this.zzdrx.add(runnable);
    }

    public final void zzcp(int i) {
        zzwh();
        synchronized (this.lock) {
            if (this.zzdsi != i) {
                this.zzdsi = i;
                if (this.zzdsa != null) {
                    this.zzdsa.putInt("version_code", i);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putInt("version_code", i);
                zzc(bundle);
            }
        }
    }

    public final void zzcq(int i) {
        zzwh();
        synchronized (this.lock) {
            if (this.zzdsh != i) {
                this.zzdsh = i;
                if (this.zzdsa != null) {
                    this.zzdsa.putInt("request_in_session_count", i);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putInt("request_in_session_count", i);
                zzc(bundle);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzee(java.lang.String r4) {
        /*
            r3 = this;
            r3.zzwh()
            java.lang.Object r0 = r3.lock
            monitor-enter(r0)
            if (r4 == 0) goto L_0x0032
            java.lang.String r1 = r3.zzdsc     // Catch:{ all -> 0x0034 }
            boolean r1 = r4.equals(r1)     // Catch:{ all -> 0x0034 }
            if (r1 == 0) goto L_0x0011
            goto L_0x0032
        L_0x0011:
            r3.zzdsc = r4     // Catch:{ all -> 0x0034 }
            android.content.SharedPreferences$Editor r1 = r3.zzdsa     // Catch:{ all -> 0x0034 }
            if (r1 == 0) goto L_0x0023
            android.content.SharedPreferences$Editor r1 = r3.zzdsa     // Catch:{ all -> 0x0034 }
            java.lang.String r2 = "content_url_hashes"
            r1.putString(r2, r4)     // Catch:{ all -> 0x0034 }
            android.content.SharedPreferences$Editor r1 = r3.zzdsa     // Catch:{ all -> 0x0034 }
            r1.apply()     // Catch:{ all -> 0x0034 }
        L_0x0023:
            android.os.Bundle r1 = new android.os.Bundle     // Catch:{ all -> 0x0034 }
            r1.<init>()     // Catch:{ all -> 0x0034 }
            java.lang.String r2 = "content_url_hashes"
            r1.putString(r2, r4)     // Catch:{ all -> 0x0034 }
            r3.zzc(r1)     // Catch:{ all -> 0x0034 }
            monitor-exit(r0)     // Catch:{ all -> 0x0034 }
            return
        L_0x0032:
            monitor-exit(r0)     // Catch:{ all -> 0x0034 }
            return
        L_0x0034:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0034 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzavx.zzee(java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzef(java.lang.String r4) {
        /*
            r3 = this;
            r3.zzwh()
            java.lang.Object r0 = r3.lock
            monitor-enter(r0)
            if (r4 == 0) goto L_0x0032
            java.lang.String r1 = r3.zzdsd     // Catch:{ all -> 0x0034 }
            boolean r1 = r4.equals(r1)     // Catch:{ all -> 0x0034 }
            if (r1 == 0) goto L_0x0011
            goto L_0x0032
        L_0x0011:
            r3.zzdsd = r4     // Catch:{ all -> 0x0034 }
            android.content.SharedPreferences$Editor r1 = r3.zzdsa     // Catch:{ all -> 0x0034 }
            if (r1 == 0) goto L_0x0023
            android.content.SharedPreferences$Editor r1 = r3.zzdsa     // Catch:{ all -> 0x0034 }
            java.lang.String r2 = "content_vertical_hashes"
            r1.putString(r2, r4)     // Catch:{ all -> 0x0034 }
            android.content.SharedPreferences$Editor r1 = r3.zzdsa     // Catch:{ all -> 0x0034 }
            r1.apply()     // Catch:{ all -> 0x0034 }
        L_0x0023:
            android.os.Bundle r1 = new android.os.Bundle     // Catch:{ all -> 0x0034 }
            r1.<init>()     // Catch:{ all -> 0x0034 }
            java.lang.String r2 = "content_vertical_hashes"
            r1.putString(r2, r4)     // Catch:{ all -> 0x0034 }
            r3.zzc(r1)     // Catch:{ all -> 0x0034 }
            monitor-exit(r0)     // Catch:{ all -> 0x0034 }
            return
        L_0x0032:
            monitor-exit(r0)     // Catch:{ all -> 0x0034 }
            return
        L_0x0034:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0034 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzavx.zzef(java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzeg(java.lang.String r6) {
        /*
            r5 = this;
            r5.zzwh()
            java.lang.Object r0 = r5.lock
            monitor-enter(r0)
            com.google.android.gms.common.util.Clock r1 = com.google.android.gms.ads.internal.zzq.zzkx()     // Catch:{ all -> 0x0060 }
            long r1 = r1.b()     // Catch:{ all -> 0x0060 }
            r5.zzdse = r1     // Catch:{ all -> 0x0060 }
            if (r6 == 0) goto L_0x005e
            java.lang.String r3 = r5.zzdjl     // Catch:{ all -> 0x0060 }
            boolean r3 = r6.equals(r3)     // Catch:{ all -> 0x0060 }
            if (r3 == 0) goto L_0x001b
            goto L_0x005e
        L_0x001b:
            r5.zzdjl = r6     // Catch:{ all -> 0x0060 }
            android.content.SharedPreferences$Editor r3 = r5.zzdsa     // Catch:{ all -> 0x0060 }
            if (r3 == 0) goto L_0x0034
            android.content.SharedPreferences$Editor r3 = r5.zzdsa     // Catch:{ all -> 0x0060 }
            java.lang.String r4 = "app_settings_json"
            r3.putString(r4, r6)     // Catch:{ all -> 0x0060 }
            android.content.SharedPreferences$Editor r3 = r5.zzdsa     // Catch:{ all -> 0x0060 }
            java.lang.String r4 = "app_settings_last_update_ms"
            r3.putLong(r4, r1)     // Catch:{ all -> 0x0060 }
            android.content.SharedPreferences$Editor r3 = r5.zzdsa     // Catch:{ all -> 0x0060 }
            r3.apply()     // Catch:{ all -> 0x0060 }
        L_0x0034:
            android.os.Bundle r3 = new android.os.Bundle     // Catch:{ all -> 0x0060 }
            r3.<init>()     // Catch:{ all -> 0x0060 }
            java.lang.String r4 = "app_settings_json"
            r3.putString(r4, r6)     // Catch:{ all -> 0x0060 }
            java.lang.String r6 = "app_settings_last_update_ms"
            r3.putLong(r6, r1)     // Catch:{ all -> 0x0060 }
            r5.zzc(r3)     // Catch:{ all -> 0x0060 }
            java.util.List<java.lang.Runnable> r6 = r5.zzdrx     // Catch:{ all -> 0x0060 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x0060 }
        L_0x004c:
            boolean r1 = r6.hasNext()     // Catch:{ all -> 0x0060 }
            if (r1 == 0) goto L_0x005c
            java.lang.Object r1 = r6.next()     // Catch:{ all -> 0x0060 }
            java.lang.Runnable r1 = (java.lang.Runnable) r1     // Catch:{ all -> 0x0060 }
            r1.run()     // Catch:{ all -> 0x0060 }
            goto L_0x004c
        L_0x005c:
            monitor-exit(r0)     // Catch:{ all -> 0x0060 }
            return
        L_0x005e:
            monitor-exit(r0)     // Catch:{ all -> 0x0060 }
            return
        L_0x0060:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0060 }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzavx.zzeg(java.lang.String):void");
    }

    public final void zzeh(String str) {
        zzwh();
        synchronized (this.lock) {
            if (!TextUtils.equals(this.zzdsl, str)) {
                this.zzdsl = str;
                if (this.zzdsa != null) {
                    this.zzdsa.putString("display_cutout", str);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putString("display_cutout", str);
                zzc(bundle);
            }
        }
    }

    public final void zzez(long j) {
        zzwh();
        synchronized (this.lock) {
            if (this.zzdsf != j) {
                this.zzdsf = j;
                if (this.zzdsa != null) {
                    this.zzdsa.putLong("app_last_background_time_ms", j);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putLong("app_last_background_time_ms", j);
                zzc(bundle);
            }
        }
    }

    public final void zzfa(long j) {
        zzwh();
        synchronized (this.lock) {
            if (this.zzdsg != j) {
                this.zzdsg = j;
                if (this.zzdsa != null) {
                    this.zzdsa.putLong("first_ad_req_time_ms", j);
                    this.zzdsa.apply();
                }
                Bundle bundle = new Bundle();
                bundle.putLong("first_ad_req_time_ms", j);
                zzc(bundle);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzp(Context context, String str) {
        boolean z = false;
        SharedPreferences sharedPreferences = context.getSharedPreferences(str, 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        synchronized (this.lock) {
            this.zzcgc = sharedPreferences;
            this.zzdsa = edit;
            if (PlatformVersion.i() && !NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted()) {
                z = true;
            }
            this.zzdsb = z;
            this.zzdiv = this.zzcgc.getBoolean("use_https", this.zzdiv);
            this.zzdku = this.zzcgc.getBoolean("content_url_opted_out", this.zzdku);
            this.zzdsc = this.zzcgc.getString("content_url_hashes", this.zzdsc);
            this.zzdji = this.zzcgc.getBoolean("auto_collect_location", this.zzdji);
            this.zzdlf = this.zzcgc.getBoolean("content_vertical_opted_out", this.zzdlf);
            this.zzdsd = this.zzcgc.getString("content_vertical_hashes", this.zzdsd);
            this.zzdsi = this.zzcgc.getInt("version_code", this.zzdsi);
            this.zzdjl = this.zzcgc.getString("app_settings_json", this.zzdjl);
            this.zzdse = this.zzcgc.getLong("app_settings_last_update_ms", this.zzdse);
            this.zzdsf = this.zzcgc.getLong("app_last_background_time_ms", this.zzdsf);
            this.zzdsh = this.zzcgc.getInt("request_in_session_count", this.zzdsh);
            this.zzdsg = this.zzcgc.getLong("first_ad_req_time_ms", this.zzdsg);
            this.zzdsj = this.zzcgc.getStringSet("never_pool_slots", this.zzdsj);
            this.zzdsl = this.zzcgc.getString("display_cutout", this.zzdsl);
            this.zzdsm = this.zzcgc.getInt("app_measurement_npa", this.zzdsm);
            try {
                this.zzdsk = new JSONObject(this.zzcgc.getString("native_advanced_settings", "{}"));
            } catch (JSONException e) {
                zzayu.zzd("Could not convert native advanced settings to json object", e);
            }
            zzc(zzwi());
        }
    }

    public final zzqi zzvt() {
        if (!this.zzdrw) {
            return null;
        }
        if ((zzvu() && zzvw()) || !zzaaw.zzcsy.get().booleanValue()) {
            return null;
        }
        synchronized (this.lock) {
            if (Looper.getMainLooper() == null) {
                return null;
            }
            if (this.zzdrz == null) {
                this.zzdrz = new zzqi();
            }
            this.zzdrz.zzmb();
            zzayu.zzey("start fetching content...");
            zzqi zzqi = this.zzdrz;
            return zzqi;
        }
    }

    public final boolean zzvu() {
        boolean z;
        zzwh();
        synchronized (this.lock) {
            z = this.zzdku;
        }
        return z;
    }

    public final String zzvv() {
        String str;
        zzwh();
        synchronized (this.lock) {
            str = this.zzdsc;
        }
        return str;
    }

    public final boolean zzvw() {
        boolean z;
        zzwh();
        synchronized (this.lock) {
            z = this.zzdlf;
        }
        return z;
    }

    public final String zzvx() {
        String str;
        zzwh();
        synchronized (this.lock) {
            str = this.zzdsd;
        }
        return str;
    }

    public final boolean zzvy() {
        boolean z;
        zzwh();
        synchronized (this.lock) {
            z = this.zzdji;
        }
        return z;
    }

    public final int zzvz() {
        int i;
        zzwh();
        synchronized (this.lock) {
            i = this.zzdsi;
        }
        return i;
    }

    public final zzavf zzwa() {
        zzavf zzavf;
        zzwh();
        synchronized (this.lock) {
            zzavf = new zzavf(this.zzdjl, this.zzdse);
        }
        return zzavf;
    }

    public final long zzwb() {
        long j;
        zzwh();
        synchronized (this.lock) {
            j = this.zzdsf;
        }
        return j;
    }

    public final int zzwc() {
        int i;
        zzwh();
        synchronized (this.lock) {
            i = this.zzdsh;
        }
        return i;
    }

    public final long zzwd() {
        long j;
        zzwh();
        synchronized (this.lock) {
            j = this.zzdsg;
        }
        return j;
    }

    public final JSONObject zzwe() {
        JSONObject jSONObject;
        zzwh();
        synchronized (this.lock) {
            jSONObject = this.zzdsk;
        }
        return jSONObject;
    }

    public final void zzwf() {
        zzwh();
        synchronized (this.lock) {
            this.zzdsk = new JSONObject();
            if (this.zzdsa != null) {
                this.zzdsa.remove("native_advanced_settings");
                this.zzdsa.apply();
            }
            Bundle bundle = new Bundle();
            bundle.putString("native_advanced_settings", "{}");
            zzc(bundle);
        }
    }

    public final String zzwg() {
        String str;
        zzwh();
        synchronized (this.lock) {
            str = this.zzdsl;
        }
        return str;
    }

    public final void zza(String str, String str2, boolean z) {
        zzwh();
        synchronized (this.lock) {
            JSONArray optJSONArray = this.zzdsk.optJSONArray(str);
            if (optJSONArray == null) {
                optJSONArray = new JSONArray();
            }
            int length = optJSONArray.length();
            int i = 0;
            while (true) {
                if (i < optJSONArray.length()) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        if (!str2.equals(optJSONObject.optString(ReportDBAdapter.ReportColumns.COLUMN_TEMPATE_ID))) {
                            i++;
                        } else if (!z || !optJSONObject.optBoolean("uses_media_view", false)) {
                            length = i;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(ReportDBAdapter.ReportColumns.COLUMN_TEMPATE_ID, str2);
                jSONObject.put("uses_media_view", z);
                jSONObject.put("timestamp_ms", zzq.zzkx().b());
                optJSONArray.put(length, jSONObject);
                this.zzdsk.put(str, optJSONArray);
            } catch (JSONException e) {
                zzayu.zzd("Could not update native advanced settings", e);
            }
            if (this.zzdsa != null) {
                this.zzdsa.putString("native_advanced_settings", this.zzdsk.toString());
                this.zzdsa.apply();
            }
            Bundle bundle = new Bundle();
            bundle.putString("native_advanced_settings", this.zzdsk.toString());
            zzc(bundle);
        }
    }
}
