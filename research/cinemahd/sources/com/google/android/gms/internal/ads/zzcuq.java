package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

public final class zzcuq implements zzdxg<zzcup> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzava> zzeqj;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<ScheduledExecutorService> zzfpr;

    public zzcuq(zzdxp<zzava> zzdxp, zzdxp<Context> zzdxp2, zzdxp<ScheduledExecutorService> zzdxp3, zzdxp<Executor> zzdxp4) {
        this.zzeqj = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfpr = zzdxp3;
        this.zzfcv = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcup(this.zzeqj.get(), this.zzejv.get(), this.zzfpr.get(), this.zzfcv.get());
    }
}
