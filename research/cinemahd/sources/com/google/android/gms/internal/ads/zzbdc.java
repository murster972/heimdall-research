package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.webkit.JsResult;

final class zzbdc implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsResult zzeea;

    zzbdc(JsResult jsResult) {
        this.zzeea = jsResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.zzeea.cancel();
    }
}
