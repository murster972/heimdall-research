package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;
import java.io.IOException;

public final class zzdwi extends zzdvq<zzdwi> {
    public String url = null;
    public zzdvx.zzb.zzg zzhxr = null;
    private zzdvx.zza.zzc zzhxs = null;
    public String zzhxt = null;
    private String zzhxu = null;
    public zzdvx.zzb.C0042zzb zzhxv = null;
    public zzdwm[] zzhxw = zzdwm.zzbdk();
    public String zzhxx = null;
    public zzdwj zzhxy = null;
    private Boolean zzhxz = null;
    private String[] zzhya;
    private String zzhyb;
    private Boolean zzhyc;
    private Boolean zzhyd;
    private byte[] zzhye;
    public zzdvx.zzb.zzi zzhyf;
    public String[] zzhyg;
    public String[] zzhyh;

    public zzdwi() {
        String[] strArr = zzdvy.zzhuc;
        this.zzhya = strArr;
        this.zzhyb = null;
        this.zzhyc = null;
        this.zzhyd = null;
        this.zzhye = null;
        this.zzhyf = null;
        this.zzhyg = strArr;
        this.zzhyh = strArr;
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        String str = this.url;
        if (str != null) {
            zzdvo.zzf(1, str);
        }
        String str2 = this.zzhxt;
        if (str2 != null) {
            zzdvo.zzf(2, str2);
        }
        zzdwm[] zzdwmArr = this.zzhxw;
        int i = 0;
        if (zzdwmArr != null && zzdwmArr.length > 0) {
            int i2 = 0;
            while (true) {
                zzdwm[] zzdwmArr2 = this.zzhxw;
                if (i2 >= zzdwmArr2.length) {
                    break;
                }
                zzdwm zzdwm = zzdwmArr2[i2];
                if (zzdwm != null) {
                    zzdvo.zza(4, (zzdvt) zzdwm);
                }
                i2++;
            }
        }
        String[] strArr = this.zzhya;
        if (strArr != null && strArr.length > 0) {
            int i3 = 0;
            while (true) {
                String[] strArr2 = this.zzhya;
                if (i3 >= strArr2.length) {
                    break;
                }
                String str3 = strArr2[i3];
                if (str3 != null) {
                    zzdvo.zzf(6, str3);
                }
                i3++;
            }
        }
        zzdvx.zzb.zzg zzg = this.zzhxr;
        if (!(zzg == null || zzg == null)) {
            zzdvo.zzab(10, zzg.zzae());
        }
        zzdvx.zzb.C0042zzb zzb = this.zzhxv;
        if (zzb != null) {
            zzdvo.zze(12, zzb);
        }
        String str4 = this.zzhxx;
        if (str4 != null) {
            zzdvo.zzf(13, str4);
        }
        zzdwj zzdwj = this.zzhxy;
        if (zzdwj != null) {
            zzdvo.zza(14, (zzdvt) zzdwj);
        }
        zzdvx.zzb.zzi zzi = this.zzhyf;
        if (zzi != null) {
            zzdvo.zze(17, zzi);
        }
        String[] strArr3 = this.zzhyg;
        if (strArr3 != null && strArr3.length > 0) {
            int i4 = 0;
            while (true) {
                String[] strArr4 = this.zzhyg;
                if (i4 >= strArr4.length) {
                    break;
                }
                String str5 = strArr4[i4];
                if (str5 != null) {
                    zzdvo.zzf(20, str5);
                }
                i4++;
            }
        }
        String[] strArr5 = this.zzhyh;
        if (strArr5 != null && strArr5.length > 0) {
            while (true) {
                String[] strArr6 = this.zzhyh;
                if (i >= strArr6.length) {
                    break;
                }
                String str6 = strArr6[i];
                if (str6 != null) {
                    zzdvo.zzf(21, str6);
                }
                i++;
            }
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int zzoi = super.zzoi();
        String str = this.url;
        if (str != null) {
            zzoi += zzdvo.zzg(1, str);
        }
        String str2 = this.zzhxt;
        if (str2 != null) {
            zzoi += zzdvo.zzg(2, str2);
        }
        zzdwm[] zzdwmArr = this.zzhxw;
        int i = 0;
        if (zzdwmArr != null && zzdwmArr.length > 0) {
            int i2 = zzoi;
            int i3 = 0;
            while (true) {
                zzdwm[] zzdwmArr2 = this.zzhxw;
                if (i3 >= zzdwmArr2.length) {
                    break;
                }
                zzdwm zzdwm = zzdwmArr2[i3];
                if (zzdwm != null) {
                    i2 += zzdvo.zzb(4, zzdwm);
                }
                i3++;
            }
            zzoi = i2;
        }
        String[] strArr = this.zzhya;
        if (strArr != null && strArr.length > 0) {
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (true) {
                String[] strArr2 = this.zzhya;
                if (i4 >= strArr2.length) {
                    break;
                }
                String str3 = strArr2[i4];
                if (str3 != null) {
                    i6++;
                    i5 += zzdvo.zzhh(str3);
                }
                i4++;
            }
            zzoi = zzoi + i5 + (i6 * 1);
        }
        zzdvx.zzb.zzg zzg = this.zzhxr;
        if (!(zzg == null || zzg == null)) {
            zzoi += zzdvo.zzaf(10, zzg.zzae());
        }
        zzdvx.zzb.C0042zzb zzb = this.zzhxv;
        if (zzb != null) {
            zzoi += zzdrb.zzc(12, (zzdte) zzb);
        }
        String str4 = this.zzhxx;
        if (str4 != null) {
            zzoi += zzdvo.zzg(13, str4);
        }
        zzdwj zzdwj = this.zzhxy;
        if (zzdwj != null) {
            zzoi += zzdvo.zzb(14, zzdwj);
        }
        zzdvx.zzb.zzi zzi = this.zzhyf;
        if (zzi != null) {
            zzoi += zzdrb.zzc(17, (zzdte) zzi);
        }
        String[] strArr3 = this.zzhyg;
        if (strArr3 != null && strArr3.length > 0) {
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            while (true) {
                String[] strArr4 = this.zzhyg;
                if (i7 >= strArr4.length) {
                    break;
                }
                String str5 = strArr4[i7];
                if (str5 != null) {
                    i9++;
                    i8 += zzdvo.zzhh(str5);
                }
                i7++;
            }
            zzoi = zzoi + i8 + (i9 * 2);
        }
        String[] strArr5 = this.zzhyh;
        if (strArr5 == null || strArr5.length <= 0) {
            return zzoi;
        }
        int i10 = 0;
        int i11 = 0;
        while (true) {
            String[] strArr6 = this.zzhyh;
            if (i >= strArr6.length) {
                return zzoi + i10 + (i11 * 2);
            }
            String str6 = strArr6[i];
            if (str6 != null) {
                i11++;
                i10 += zzdvo.zzhh(str6);
            }
            i++;
        }
    }
}
