package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcxb implements zzcxo {
    private final int zzdvv;

    zzcxb(int i) {
        this.zzdvv = i;
    }

    public final void zzt(Object obj) {
        ((zzrg) obj).onAppOpenAdFailedToLoad(this.zzdvv);
    }
}
