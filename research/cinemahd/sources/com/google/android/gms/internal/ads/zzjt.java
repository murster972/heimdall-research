package com.google.android.gms.internal.ads;

import android.util.SparseArray;
import com.facebook.imageutils.JfifUtil;
import com.google.android.gms.internal.ads.zziv;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

public final class zzjt implements zzjd {
    private static final zzji zzanm = new zzjw();
    private static final byte[] zzann = {49, 10, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 10};
    private static final byte[] zzano = {32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32};
    /* access modifiers changed from: private */
    public static final UUID zzanp = new UUID(72057594037932032L, -9223371306706625679L);
    private long zzagj;
    private final zzka zzang;
    private final zzjr zzanq;
    private final SparseArray<zzjy> zzanr;
    private final boolean zzans;
    private final zzoj zzant;
    private final zzoj zzanu;
    private final zzoj zzanv;
    private final zzoj zzanw;
    private final zzoj zzanx;
    private final zzoj zzany;
    private final zzoj zzanz;
    private final zzoj zzaoa;
    private final zzoj zzaob;
    private ByteBuffer zzaoc;
    private long zzaod;
    private long zzaoe;
    private long zzaof;
    private long zzaog;
    private zzjy zzaoh;
    private boolean zzaoi;
    private int zzaoj;
    private long zzaok;
    private boolean zzaol;
    private long zzaom;
    private long zzaon;
    private long zzaoo;
    private zzod zzaop;
    private zzod zzaoq;
    private boolean zzaor;
    private int zzaos;
    private long zzaot;
    private long zzaou;
    private int zzaov;
    private int zzaow;
    private int[] zzaox;
    private int zzaoy;
    private int zzaoz;
    private int zzapa;
    private int zzapb;
    private boolean zzapc;
    private boolean zzapd;
    private boolean zzape;
    private boolean zzapf;
    private byte zzapg;
    private int zzaph;
    private int zzapi;
    private int zzapj;
    private boolean zzapk;
    private boolean zzapl;
    private zzjf zzapm;

    public zzjt() {
        this(0);
    }

    static int zzah(int i) {
        switch (i) {
            case 131:
            case 136:
            case 155:
            case 159:
            case 176:
            case 179:
            case 186:
            case JfifUtil.MARKER_RST7:
            case 231:
            case 241:
            case 251:
            case 16980:
            case 17029:
            case 17143:
            case 18401:
            case 18408:
            case 20529:
            case 20530:
            case 21420:
            case 21432:
            case 21680:
            case 21682:
            case 21690:
            case 21930:
            case 21945:
            case 21946:
            case 21947:
            case 21948:
            case 21949:
            case 22186:
            case 22203:
            case 25188:
            case 2352003:
            case 2807729:
                return 2;
            case 134:
            case 17026:
            case 2274716:
                return 3;
            case 160:
            case 174:
            case 183:
            case 187:
            case 224:
            case JfifUtil.MARKER_APP1:
            case 18407:
            case 19899:
            case 20532:
            case 20533:
            case 21936:
            case 21968:
            case 25152:
            case 28032:
            case 30320:
            case 290298740:
            case 357149030:
            case 374648427:
            case 408125543:
            case 440786851:
            case 475249515:
            case 524531317:
                return 1;
            case 161:
            case 163:
            case 16981:
            case 18402:
            case 21419:
            case 25506:
            case 30322:
                return 4;
            case 181:
            case 17545:
            case 21969:
            case 21970:
            case 21971:
            case 21972:
            case 21973:
            case 21974:
            case 21975:
            case 21976:
            case 21977:
            case 21978:
                return 5;
            default:
                return 0;
        }
    }

    static boolean zzai(int i) {
        return i == 357149030 || i == 524531317 || i == 475249515 || i == 374648427;
    }

    private final void zzb(zzjg zzjg, int i) throws IOException, InterruptedException {
        if (this.zzanv.limit() < i) {
            if (this.zzanv.capacity() < i) {
                zzoj zzoj = this.zzanv;
                byte[] bArr = zzoj.data;
                zzoj.zzb(Arrays.copyOf(bArr, Math.max(bArr.length << 1, i)), this.zzanv.limit());
            }
            zzoj zzoj2 = this.zzanv;
            zzjg.readFully(zzoj2.data, zzoj2.limit(), i - this.zzanv.limit());
            this.zzanv.zzbd(i);
        }
    }

    private final long zzea(long j) throws zzhd {
        long j2 = this.zzaof;
        if (j2 != -9223372036854775807L) {
            return zzoq.zza(j, j2, 1000);
        }
        throw new zzhd("Can't scale timecode prior to timecodeScale being set.");
    }

    private final void zzgm() {
        this.zzapb = 0;
        this.zzapj = 0;
        this.zzapi = 0;
        this.zzapc = false;
        this.zzapd = false;
        this.zzapf = false;
        this.zzaph = 0;
        this.zzapg = 0;
        this.zzape = false;
        this.zzany.reset();
    }

    public final void release() {
    }

    public final boolean zza(zzjg zzjg) throws IOException, InterruptedException {
        return new zzjx().zza(zzjg);
    }

    /* access modifiers changed from: package-private */
    public final void zzaj(int i) throws zzhd {
        zzjm zzjm;
        zzod zzod;
        zzod zzod2;
        int i2;
        int i3 = 0;
        if (i != 160) {
            if (i == 174) {
                String str = this.zzaoh.zzapp;
                if ("V_VP8".equals(str) || "V_VP9".equals(str) || "V_MPEG2".equals(str) || "V_MPEG4/ISO/SP".equals(str) || "V_MPEG4/ISO/ASP".equals(str) || "V_MPEG4/ISO/AP".equals(str) || "V_MPEG4/ISO/AVC".equals(str) || "V_MPEGH/ISO/HEVC".equals(str) || "V_MS/VFW/FOURCC".equals(str) || "V_THEORA".equals(str) || "A_OPUS".equals(str) || "A_VORBIS".equals(str) || "A_AAC".equals(str) || "A_MPEG/L2".equals(str) || "A_MPEG/L3".equals(str) || "A_AC3".equals(str) || "A_EAC3".equals(str) || "A_TRUEHD".equals(str) || "A_DTS".equals(str) || "A_DTS/EXPRESS".equals(str) || "A_DTS/LOSSLESS".equals(str) || "A_FLAC".equals(str) || "A_MS/ACM".equals(str) || "A_PCM/INT/LIT".equals(str) || "S_TEXT/UTF8".equals(str) || "S_VOBSUB".equals(str) || "S_HDMV/PGS".equals(str) || "S_DVBSUB".equals(str)) {
                    i3 = 1;
                }
                if (i3 != 0) {
                    zzjy zzjy = this.zzaoh;
                    zzjy.zza(this.zzapm, zzjy.number);
                    SparseArray<zzjy> sparseArray = this.zzanr;
                    zzjy zzjy2 = this.zzaoh;
                    sparseArray.put(zzjy2.number, zzjy2);
                }
                this.zzaoh = null;
            } else if (i == 19899) {
                int i4 = this.zzaoj;
                if (i4 != -1) {
                    long j = this.zzaok;
                    if (j != -1) {
                        if (i4 == 475249515) {
                            this.zzaom = j;
                            return;
                        }
                        return;
                    }
                }
                throw new zzhd("Mandatory element SeekID or SeekPosition not found");
            } else if (i == 25152) {
                zzjy zzjy3 = this.zzaoh;
                if (zzjy3.zzapr) {
                    zzjn zzjn = zzjy3.zzapt;
                    if (zzjn != null) {
                        zzjy3.zzafh = new zziv(new zziv.zza(zzgi.zzacb, "video/webm", zzjn.zzand));
                        return;
                    }
                    throw new zzhd("Encrypted Track found but ContentEncKeyID was not found");
                }
            } else if (i == 28032) {
                zzjy zzjy4 = this.zzaoh;
                if (zzjy4.zzapr && zzjy4.zzaps != null) {
                    throw new zzhd("Combining encryption and compression is not supported");
                }
            } else if (i == 357149030) {
                if (this.zzaof == -9223372036854775807L) {
                    this.zzaof = 1000000;
                }
                long j2 = this.zzaog;
                if (j2 != -9223372036854775807L) {
                    this.zzagj = zzea(j2);
                }
            } else if (i != 374648427) {
                if (i == 475249515 && !this.zzaoi) {
                    zzjf zzjf = this.zzapm;
                    if (this.zzaoe == -1 || this.zzagj == -9223372036854775807L || (zzod = this.zzaop) == null || zzod.size() == 0 || (zzod2 = this.zzaoq) == null || zzod2.size() != this.zzaop.size()) {
                        this.zzaop = null;
                        this.zzaoq = null;
                        zzjm = new zzjl(this.zzagj);
                    } else {
                        int size = this.zzaop.size();
                        int[] iArr = new int[size];
                        long[] jArr = new long[size];
                        long[] jArr2 = new long[size];
                        long[] jArr3 = new long[size];
                        for (int i5 = 0; i5 < size; i5++) {
                            jArr3[i5] = this.zzaop.get(i5);
                            jArr[i5] = this.zzaoe + this.zzaoq.get(i5);
                        }
                        while (true) {
                            i2 = size - 1;
                            if (i3 >= i2) {
                                break;
                            }
                            int i6 = i3 + 1;
                            iArr[i3] = (int) (jArr[i6] - jArr[i3]);
                            jArr2[i3] = jArr3[i6] - jArr3[i3];
                            i3 = i6;
                        }
                        iArr[i2] = (int) ((this.zzaoe + this.zzaod) - jArr[i2]);
                        jArr2[i2] = this.zzagj - jArr3[i2];
                        this.zzaop = null;
                        this.zzaoq = null;
                        zzjm = new zzjb(iArr, jArr, jArr2, jArr3);
                    }
                    zzjf.zza(zzjm);
                    this.zzaoi = true;
                }
            } else if (this.zzanr.size() != 0) {
                this.zzapm.zzgj();
            } else {
                throw new zzhd("No valid tracks were found");
            }
        } else if (this.zzaos == 2) {
            if (!this.zzapl) {
                this.zzapa |= 1;
            }
            zza(this.zzanr.get(this.zzaoy), this.zzaot);
            this.zzaos = 0;
        }
    }

    public final void zzc(long j, long j2) {
        this.zzaoo = -9223372036854775807L;
        this.zzaos = 0;
        this.zzanq.reset();
        this.zzang.reset();
        zzgm();
    }

    /* access modifiers changed from: package-private */
    public final void zzd(int i, long j, long j2) throws zzhd {
        if (i == 160) {
            this.zzapl = false;
        } else if (i == 174) {
            this.zzaoh = new zzjy((zzjw) null);
        } else if (i == 187) {
            this.zzaor = false;
        } else if (i == 19899) {
            this.zzaoj = -1;
            this.zzaok = -1;
        } else if (i == 20533) {
            this.zzaoh.zzapr = true;
        } else if (i == 21968) {
            this.zzaoh.zzapy = true;
        } else if (i == 25152) {
        } else {
            if (i == 408125543) {
                long j3 = this.zzaoe;
                if (j3 == -1 || j3 == j) {
                    this.zzaoe = j;
                    this.zzaod = j2;
                    return;
                }
                throw new zzhd("Multiple Segment elements not supported");
            } else if (i == 475249515) {
                this.zzaop = new zzod();
                this.zzaoq = new zzod();
            } else if (i != 524531317 || this.zzaoi) {
            } else {
                if (!this.zzans || this.zzaom == -1) {
                    this.zzapm.zza(new zzjl(this.zzagj));
                    this.zzaoi = true;
                    return;
                }
                this.zzaol = true;
            }
        }
    }

    private zzjt(int i) {
        this(new zzjq(), 0);
    }

    public final void zza(zzjf zzjf) {
        this.zzapm = zzjf;
    }

    private zzjt(zzjr zzjr, int i) {
        this.zzaoe = -1;
        this.zzaof = -9223372036854775807L;
        this.zzaog = -9223372036854775807L;
        this.zzagj = -9223372036854775807L;
        this.zzaom = -1;
        this.zzaon = -1;
        this.zzaoo = -9223372036854775807L;
        this.zzanq = zzjr;
        this.zzanq.zza(new zzjv(this, (zzjw) null));
        this.zzans = true;
        this.zzang = new zzka();
        this.zzanr = new SparseArray<>();
        this.zzanv = new zzoj(4);
        this.zzanw = new zzoj(ByteBuffer.allocate(4).putInt(-1).array());
        this.zzanx = new zzoj(4);
        this.zzant = new zzoj(zzoi.zzbga);
        this.zzanu = new zzoj(4);
        this.zzany = new zzoj();
        this.zzanz = new zzoj();
        this.zzaoa = new zzoj(8);
        this.zzaob = new zzoj();
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0039 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0005 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(com.google.android.gms.internal.ads.zzjg r9, com.google.android.gms.internal.ads.zzjj r10) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r8 = this;
            r0 = 0
            r8.zzapk = r0
            r1 = 1
            r2 = 1
        L_0x0005:
            if (r2 == 0) goto L_0x003a
            boolean r3 = r8.zzapk
            if (r3 != 0) goto L_0x003a
            com.google.android.gms.internal.ads.zzjr r2 = r8.zzanq
            boolean r2 = r2.zzb(r9)
            if (r2 == 0) goto L_0x0005
            long r3 = r9.getPosition()
            boolean r5 = r8.zzaol
            if (r5 == 0) goto L_0x0025
            r8.zzaon = r3
            long r3 = r8.zzaom
            r10.zzamw = r3
            r8.zzaol = r0
        L_0x0023:
            r3 = 1
            goto L_0x0037
        L_0x0025:
            boolean r3 = r8.zzaoi
            if (r3 == 0) goto L_0x0036
            long r3 = r8.zzaon
            r5 = -1
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0036
            r10.zzamw = r3
            r8.zzaon = r5
            goto L_0x0023
        L_0x0036:
            r3 = 0
        L_0x0037:
            if (r3 == 0) goto L_0x0005
            return r1
        L_0x003a:
            if (r2 == 0) goto L_0x003d
            return r0
        L_0x003d:
            r9 = -1
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzjt.zza(com.google.android.gms.internal.ads.zzjg, com.google.android.gms.internal.ads.zzjj):int");
    }

    /* access modifiers changed from: package-private */
    public final void zzc(int i, long j) throws zzhd {
        if (i != 20529) {
            if (i != 20530) {
                boolean z = false;
                switch (i) {
                    case 131:
                        this.zzaoh.type = (int) j;
                        return;
                    case 136:
                        zzjy zzjy = this.zzaoh;
                        if (j == 1) {
                            z = true;
                        }
                        zzjy.zzaqr = z;
                        return;
                    case 155:
                        this.zzaou = zzea(j);
                        return;
                    case 159:
                        this.zzaoh.zzafo = (int) j;
                        return;
                    case 176:
                        this.zzaoh.width = (int) j;
                        return;
                    case 179:
                        this.zzaop.add(zzea(j));
                        return;
                    case 186:
                        this.zzaoh.height = (int) j;
                        return;
                    case JfifUtil.MARKER_RST7:
                        this.zzaoh.number = (int) j;
                        return;
                    case 231:
                        this.zzaoo = zzea(j);
                        return;
                    case 241:
                        if (!this.zzaor) {
                            this.zzaoq.add(j);
                            this.zzaor = true;
                            return;
                        }
                        return;
                    case 251:
                        this.zzapl = true;
                        return;
                    case 16980:
                        if (j != 3) {
                            StringBuilder sb = new StringBuilder(50);
                            sb.append("ContentCompAlgo ");
                            sb.append(j);
                            sb.append(" not supported");
                            throw new zzhd(sb.toString());
                        }
                        return;
                    case 17029:
                        if (j < 1 || j > 2) {
                            StringBuilder sb2 = new StringBuilder(53);
                            sb2.append("DocTypeReadVersion ");
                            sb2.append(j);
                            sb2.append(" not supported");
                            throw new zzhd(sb2.toString());
                        }
                        return;
                    case 17143:
                        if (j != 1) {
                            StringBuilder sb3 = new StringBuilder(50);
                            sb3.append("EBMLReadVersion ");
                            sb3.append(j);
                            sb3.append(" not supported");
                            throw new zzhd(sb3.toString());
                        }
                        return;
                    case 18401:
                        if (j != 5) {
                            StringBuilder sb4 = new StringBuilder(49);
                            sb4.append("ContentEncAlgo ");
                            sb4.append(j);
                            sb4.append(" not supported");
                            throw new zzhd(sb4.toString());
                        }
                        return;
                    case 18408:
                        if (j != 1) {
                            StringBuilder sb5 = new StringBuilder(56);
                            sb5.append("AESSettingsCipherMode ");
                            sb5.append(j);
                            sb5.append(" not supported");
                            throw new zzhd(sb5.toString());
                        }
                        return;
                    case 21420:
                        this.zzaok = j + this.zzaoe;
                        return;
                    case 21432:
                        int i2 = (int) j;
                        if (i2 == 0) {
                            this.zzaoh.zzafl = 0;
                            return;
                        } else if (i2 == 1) {
                            this.zzaoh.zzafl = 2;
                            return;
                        } else if (i2 == 3) {
                            this.zzaoh.zzafl = 1;
                            return;
                        } else if (i2 == 15) {
                            this.zzaoh.zzafl = 3;
                            return;
                        } else {
                            return;
                        }
                    case 21680:
                        this.zzaoh.zzapv = (int) j;
                        return;
                    case 21682:
                        this.zzaoh.zzapx = (int) j;
                        return;
                    case 21690:
                        this.zzaoh.zzapw = (int) j;
                        return;
                    case 21930:
                        zzjy zzjy2 = this.zzaoh;
                        if (j == 1) {
                            z = true;
                        }
                        zzjy2.zzaqs = z;
                        return;
                    case 22186:
                        this.zzaoh.zzaqp = j;
                        return;
                    case 22203:
                        this.zzaoh.zzaqq = j;
                        return;
                    case 25188:
                        this.zzaoh.zzaqo = (int) j;
                        return;
                    case 2352003:
                        this.zzaoh.zzapq = (int) j;
                        return;
                    case 2807729:
                        this.zzaof = j;
                        return;
                    default:
                        switch (i) {
                            case 21945:
                                int i3 = (int) j;
                                if (i3 == 1) {
                                    this.zzaoh.zzaqb = 2;
                                    return;
                                } else if (i3 == 2) {
                                    this.zzaoh.zzaqb = 1;
                                    return;
                                } else {
                                    return;
                                }
                            case 21946:
                                int i4 = (int) j;
                                if (i4 != 1) {
                                    if (i4 == 16) {
                                        this.zzaoh.zzaqa = 6;
                                        return;
                                    } else if (i4 == 18) {
                                        this.zzaoh.zzaqa = 7;
                                        return;
                                    } else if (!(i4 == 6 || i4 == 7)) {
                                        return;
                                    }
                                }
                                this.zzaoh.zzaqa = 3;
                                return;
                            case 21947:
                                zzjy zzjy3 = this.zzaoh;
                                zzjy3.zzapy = true;
                                int i5 = (int) j;
                                if (i5 == 1) {
                                    zzjy3.zzapz = 1;
                                    return;
                                } else if (i5 == 9) {
                                    zzjy3.zzapz = 6;
                                    return;
                                } else if (i5 == 4 || i5 == 5 || i5 == 6 || i5 == 7) {
                                    this.zzaoh.zzapz = 2;
                                    return;
                                } else {
                                    return;
                                }
                            case 21948:
                                this.zzaoh.zzaqc = (int) j;
                                return;
                            case 21949:
                                this.zzaoh.zzaqd = (int) j;
                                return;
                            default:
                                return;
                        }
                }
            } else if (j != 1) {
                StringBuilder sb6 = new StringBuilder(55);
                sb6.append("ContentEncodingScope ");
                sb6.append(j);
                sb6.append(" not supported");
                throw new zzhd(sb6.toString());
            }
        } else if (j != 0) {
            StringBuilder sb7 = new StringBuilder(55);
            sb7.append("ContentEncodingOrder ");
            sb7.append(j);
            sb7.append(" not supported");
            throw new zzhd(sb7.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(int i, double d) {
        if (i == 181) {
            this.zzaoh.zzafp = (int) d;
        } else if (i != 17545) {
            switch (i) {
                case 21969:
                    this.zzaoh.zzaqe = (float) d;
                    return;
                case 21970:
                    this.zzaoh.zzaqf = (float) d;
                    return;
                case 21971:
                    this.zzaoh.zzaqg = (float) d;
                    return;
                case 21972:
                    this.zzaoh.zzaqh = (float) d;
                    return;
                case 21973:
                    this.zzaoh.zzaqi = (float) d;
                    return;
                case 21974:
                    this.zzaoh.zzaqj = (float) d;
                    return;
                case 21975:
                    this.zzaoh.zzaqk = (float) d;
                    return;
                case 21976:
                    this.zzaoh.zzaql = (float) d;
                    return;
                case 21977:
                    this.zzaoh.zzaqm = (float) d;
                    return;
                case 21978:
                    this.zzaoh.zzaqn = (float) d;
                    return;
                default:
                    return;
            }
        } else {
            this.zzaog = (long) d;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(int i, String str) throws zzhd {
        if (i == 134) {
            this.zzaoh.zzapp = str;
        } else if (i != 17026) {
            if (i == 2274716) {
                String unused = this.zzaoh.zzafv = str;
            }
        } else if (!"webm".equals(str) && !"matroska".equals(str)) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 22);
            sb.append("DocType ");
            sb.append(str);
            sb.append(" not supported");
            throw new zzhd(sb.toString());
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01fa, code lost:
        throw new com.google.android.gms.internal.ads.zzhd("EBML lacing sample size out of range.");
     */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0231  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0233  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0248  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0250  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(int r20, int r21, com.google.android.gms.internal.ads.zzjg r22) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r22
            r4 = 161(0xa1, float:2.26E-43)
            r5 = 163(0xa3, float:2.28E-43)
            r6 = 4
            r7 = 0
            r8 = 1
            if (r1 == r4) goto L_0x0091
            if (r1 == r5) goto L_0x0091
            r4 = 16981(0x4255, float:2.3795E-41)
            if (r1 == r4) goto L_0x0085
            r4 = 18402(0x47e2, float:2.5787E-41)
            if (r1 == r4) goto L_0x0076
            r4 = 21419(0x53ab, float:3.0014E-41)
            if (r1 == r4) goto L_0x0058
            r4 = 25506(0x63a2, float:3.5742E-41)
            if (r1 == r4) goto L_0x004c
            r4 = 30322(0x7672, float:4.249E-41)
            if (r1 != r4) goto L_0x0033
            com.google.android.gms.internal.ads.zzjy r1 = r0.zzaoh
            byte[] r4 = new byte[r2]
            r1.zzafm = r4
            byte[] r1 = r1.zzafm
            r3.readFully(r1, r7, r2)
            return
        L_0x0033:
            com.google.android.gms.internal.ads.zzhd r2 = new com.google.android.gms.internal.ads.zzhd
            r3 = 26
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r3)
            java.lang.String r3 = "Unexpected id: "
            r4.append(r3)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.<init>(r1)
            throw r2
        L_0x004c:
            com.google.android.gms.internal.ads.zzjy r1 = r0.zzaoh
            byte[] r4 = new byte[r2]
            r1.zzapu = r4
            byte[] r1 = r1.zzapu
            r3.readFully(r1, r7, r2)
            return
        L_0x0058:
            com.google.android.gms.internal.ads.zzoj r1 = r0.zzanx
            byte[] r1 = r1.data
            java.util.Arrays.fill(r1, r7)
            com.google.android.gms.internal.ads.zzoj r1 = r0.zzanx
            byte[] r1 = r1.data
            int r6 = r6 - r2
            r3.readFully(r1, r6, r2)
            com.google.android.gms.internal.ads.zzoj r1 = r0.zzanx
            r1.zzbe(r7)
            com.google.android.gms.internal.ads.zzoj r1 = r0.zzanx
            long r1 = r1.zzip()
            int r2 = (int) r1
            r0.zzaoj = r2
            return
        L_0x0076:
            byte[] r1 = new byte[r2]
            r3.readFully(r1, r7, r2)
            com.google.android.gms.internal.ads.zzjy r2 = r0.zzaoh
            com.google.android.gms.internal.ads.zzjn r3 = new com.google.android.gms.internal.ads.zzjn
            r3.<init>(r8, r1)
            r2.zzapt = r3
            return
        L_0x0085:
            com.google.android.gms.internal.ads.zzjy r1 = r0.zzaoh
            byte[] r4 = new byte[r2]
            r1.zzaps = r4
            byte[] r1 = r1.zzaps
            r3.readFully(r1, r7, r2)
            return
        L_0x0091:
            int r4 = r0.zzaos
            r9 = 8
            if (r4 != 0) goto L_0x00b6
            com.google.android.gms.internal.ads.zzka r4 = r0.zzang
            long r10 = r4.zza(r3, r7, r8, r9)
            int r4 = (int) r10
            r0.zzaoy = r4
            com.google.android.gms.internal.ads.zzka r4 = r0.zzang
            int r4 = r4.zzgo()
            r0.zzaoz = r4
            r10 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r0.zzaou = r10
            r0.zzaos = r8
            com.google.android.gms.internal.ads.zzoj r4 = r0.zzanv
            r4.reset()
        L_0x00b6:
            android.util.SparseArray<com.google.android.gms.internal.ads.zzjy> r4 = r0.zzanr
            int r10 = r0.zzaoy
            java.lang.Object r4 = r4.get(r10)
            com.google.android.gms.internal.ads.zzjy r4 = (com.google.android.gms.internal.ads.zzjy) r4
            if (r4 != 0) goto L_0x00cc
            int r1 = r0.zzaoz
            int r1 = r2 - r1
            r3.zzac(r1)
            r0.zzaos = r7
            return
        L_0x00cc:
            int r10 = r0.zzaos
            if (r10 != r8) goto L_0x027d
            r10 = 3
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzoj r11 = r0.zzanv
            byte[] r11 = r11.data
            r12 = 2
            byte r11 = r11[r12]
            r11 = r11 & 6
            int r11 = r11 >> r8
            r13 = 255(0xff, float:3.57E-43)
            if (r11 != 0) goto L_0x00f7
            r0.zzaow = r8
            int[] r6 = r0.zzaox
            int[] r6 = zza((int[]) r6, (int) r8)
            r0.zzaox = r6
            int[] r6 = r0.zzaox
            int r11 = r0.zzaoz
            int r2 = r2 - r11
            int r2 = r2 - r10
            r6[r7] = r2
        L_0x00f4:
            r6 = 1
            goto L_0x020e
        L_0x00f7:
            if (r1 != r5) goto L_0x0275
            r0.zzb(r3, r6)
            com.google.android.gms.internal.ads.zzoj r14 = r0.zzanv
            byte[] r14 = r14.data
            byte r14 = r14[r10]
            r14 = r14 & r13
            int r14 = r14 + r8
            r0.zzaow = r14
            int[] r14 = r0.zzaox
            int r15 = r0.zzaow
            int[] r14 = zza((int[]) r14, (int) r15)
            r0.zzaox = r14
            if (r11 != r12) goto L_0x011f
            int r10 = r0.zzaoz
            int r2 = r2 - r10
            int r2 = r2 - r6
            int r6 = r0.zzaow
            int r2 = r2 / r6
            int[] r10 = r0.zzaox
            java.util.Arrays.fill(r10, r7, r6, r2)
            goto L_0x00f4
        L_0x011f:
            if (r11 != r8) goto L_0x0156
            r6 = 0
            r10 = 4
            r11 = 0
        L_0x0124:
            int r14 = r0.zzaow
            int r15 = r14 + -1
            if (r6 >= r15) goto L_0x014b
            int[] r14 = r0.zzaox
            r14[r6] = r7
        L_0x012e:
            int r10 = r10 + r8
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzoj r14 = r0.zzanv
            byte[] r14 = r14.data
            int r15 = r10 + -1
            byte r14 = r14[r15]
            r14 = r14 & r13
            int[] r15 = r0.zzaox
            r16 = r15[r6]
            int r16 = r16 + r14
            r15[r6] = r16
            if (r14 == r13) goto L_0x012e
            r14 = r15[r6]
            int r11 = r11 + r14
            int r6 = r6 + 1
            goto L_0x0124
        L_0x014b:
            int[] r6 = r0.zzaox
            int r14 = r14 - r8
            int r15 = r0.zzaoz
            int r2 = r2 - r15
            int r2 = r2 - r10
            int r2 = r2 - r11
            r6[r14] = r2
            goto L_0x00f4
        L_0x0156:
            if (r11 != r10) goto L_0x025c
            r6 = 0
            r10 = 4
            r11 = 0
        L_0x015b:
            int r14 = r0.zzaow
            int r15 = r14 + -1
            if (r6 >= r15) goto L_0x0203
            int[] r14 = r0.zzaox
            r14[r6] = r7
            int r10 = r10 + 1
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzoj r14 = r0.zzanv
            byte[] r14 = r14.data
            int r15 = r10 + -1
            byte r14 = r14[r15]
            if (r14 == 0) goto L_0x01fb
            r16 = 0
            r14 = 0
        L_0x0177:
            if (r14 >= r9) goto L_0x01c6
            int r18 = 7 - r14
            int r5 = r8 << r18
            com.google.android.gms.internal.ads.zzoj r12 = r0.zzanv
            byte[] r12 = r12.data
            byte r12 = r12[r15]
            r12 = r12 & r5
            if (r12 == 0) goto L_0x01bc
            int r10 = r10 + r14
            r0.zzb(r3, r10)
            com.google.android.gms.internal.ads.zzoj r12 = r0.zzanv
            byte[] r12 = r12.data
            int r16 = r15 + 1
            byte r12 = r12[r15]
            r12 = r12 & r13
            int r5 = ~r5
            r5 = r5 & r12
            long r7 = (long) r5
            r5 = r16
        L_0x0198:
            r16 = r7
            if (r5 >= r10) goto L_0x01ae
            long r7 = r16 << r9
            com.google.android.gms.internal.ads.zzoj r15 = r0.zzanv
            byte[] r15 = r15.data
            int r16 = r5 + 1
            byte r5 = r15[r5]
            r5 = r5 & r13
            long r12 = (long) r5
            long r7 = r7 | r12
            r5 = r16
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x0198
        L_0x01ae:
            if (r6 <= 0) goto L_0x01c6
            int r14 = r14 * 7
            int r14 = r14 + 6
            r7 = 1
            long r12 = r7 << r14
            long r12 = r12 - r7
            long r16 = r16 - r12
            goto L_0x01c6
        L_0x01bc:
            int r14 = r14 + 1
            r5 = 163(0xa3, float:2.28E-43)
            r7 = 0
            r8 = 1
            r12 = 2
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x0177
        L_0x01c6:
            r7 = r16
            r12 = -2147483648(0xffffffff80000000, double:NaN)
            int r5 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r5 < 0) goto L_0x01f3
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r5 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r5 > 0) goto L_0x01f3
            int r5 = (int) r7
            int[] r7 = r0.zzaox
            if (r6 != 0) goto L_0x01dc
            goto L_0x01e1
        L_0x01dc:
            int r8 = r6 + -1
            r8 = r7[r8]
            int r5 = r5 + r8
        L_0x01e1:
            r7[r6] = r5
            int[] r5 = r0.zzaox
            r5 = r5[r6]
            int r11 = r11 + r5
            int r6 = r6 + 1
            r5 = 163(0xa3, float:2.28E-43)
            r7 = 0
            r8 = 1
            r12 = 2
            r13 = 255(0xff, float:3.57E-43)
            goto L_0x015b
        L_0x01f3:
            com.google.android.gms.internal.ads.zzhd r1 = new com.google.android.gms.internal.ads.zzhd
            java.lang.String r2 = "EBML lacing sample size out of range."
            r1.<init>(r2)
            throw r1
        L_0x01fb:
            com.google.android.gms.internal.ads.zzhd r1 = new com.google.android.gms.internal.ads.zzhd
            java.lang.String r2 = "No valid varint length mask found"
            r1.<init>(r2)
            throw r1
        L_0x0203:
            int[] r5 = r0.zzaox
            r6 = 1
            int r14 = r14 - r6
            int r7 = r0.zzaoz
            int r2 = r2 - r7
            int r2 = r2 - r10
            int r2 = r2 - r11
            r5[r14] = r2
        L_0x020e:
            com.google.android.gms.internal.ads.zzoj r2 = r0.zzanv
            byte[] r2 = r2.data
            r5 = 0
            byte r7 = r2[r5]
            int r5 = r7 << 8
            byte r2 = r2[r6]
            r6 = 255(0xff, float:3.57E-43)
            r2 = r2 & r6
            r2 = r2 | r5
            long r5 = r0.zzaoo
            long r7 = (long) r2
            long r7 = r0.zzea(r7)
            long r5 = r5 + r7
            r0.zzaot = r5
            com.google.android.gms.internal.ads.zzoj r2 = r0.zzanv
            byte[] r2 = r2.data
            r5 = 2
            byte r2 = r2[r5]
            r2 = r2 & r9
            if (r2 != r9) goto L_0x0233
            r2 = 1
            goto L_0x0234
        L_0x0233:
            r2 = 0
        L_0x0234:
            int r6 = r4.type
            if (r6 == r5) goto L_0x024a
            r6 = 163(0xa3, float:2.28E-43)
            if (r1 != r6) goto L_0x0248
            com.google.android.gms.internal.ads.zzoj r6 = r0.zzanv
            byte[] r6 = r6.data
            byte r6 = r6[r5]
            r5 = 128(0x80, float:1.794E-43)
            r6 = r6 & r5
            if (r6 != r5) goto L_0x0248
            goto L_0x024a
        L_0x0248:
            r5 = 0
            goto L_0x024b
        L_0x024a:
            r5 = 1
        L_0x024b:
            if (r2 == 0) goto L_0x0250
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x0251
        L_0x0250:
            r7 = 0
        L_0x0251:
            r2 = r5 | r7
            r0.zzapa = r2
            r2 = 2
            r0.zzaos = r2
            r2 = 0
            r0.zzaov = r2
            goto L_0x027d
        L_0x025c:
            com.google.android.gms.internal.ads.zzhd r1 = new com.google.android.gms.internal.ads.zzhd
            r2 = 36
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Unexpected lacing value: "
            r3.append(r2)
            r3.append(r11)
            java.lang.String r2 = r3.toString()
            r1.<init>(r2)
            throw r1
        L_0x0275:
            com.google.android.gms.internal.ads.zzhd r1 = new com.google.android.gms.internal.ads.zzhd
            java.lang.String r2 = "Lacing only supported in SimpleBlocks."
            r1.<init>(r2)
            throw r1
        L_0x027d:
            r2 = 163(0xa3, float:2.28E-43)
            if (r1 != r2) goto L_0x02a8
        L_0x0281:
            int r1 = r0.zzaov
            int r2 = r0.zzaow
            if (r1 >= r2) goto L_0x02a4
            int[] r2 = r0.zzaox
            r1 = r2[r1]
            r0.zza((com.google.android.gms.internal.ads.zzjg) r3, (com.google.android.gms.internal.ads.zzjy) r4, (int) r1)
            long r1 = r0.zzaot
            int r5 = r0.zzaov
            int r6 = r4.zzapq
            int r5 = r5 * r6
            int r5 = r5 / 1000
            long r5 = (long) r5
            long r1 = r1 + r5
            r0.zza((com.google.android.gms.internal.ads.zzjy) r4, (long) r1)
            int r1 = r0.zzaov
            r2 = 1
            int r1 = r1 + r2
            r0.zzaov = r1
            goto L_0x0281
        L_0x02a4:
            r1 = 0
            r0.zzaos = r1
            return
        L_0x02a8:
            r1 = 0
            int[] r2 = r0.zzaox
            r1 = r2[r1]
            r0.zza((com.google.android.gms.internal.ads.zzjg) r3, (com.google.android.gms.internal.ads.zzjy) r4, (int) r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzjt.zza(int, int, com.google.android.gms.internal.ads.zzjg):void");
    }

    private final void zza(zzjy zzjy, long j) {
        byte[] bArr;
        if ("S_TEXT/UTF8".equals(zzjy.zzapp)) {
            byte[] bArr2 = this.zzanz.data;
            long j2 = this.zzaou;
            if (j2 == -9223372036854775807L) {
                bArr = zzano;
            } else {
                int i = (int) (j2 / 3600000000L);
                long j3 = j2 - (((long) i) * 3600000000L);
                int i2 = (int) (j3 / 60000000);
                long j4 = j3 - ((long) (60000000 * i2));
                int i3 = (int) (j4 / 1000000);
                bArr = zzoq.zzbm(String.format(Locale.US, "%02d:%02d:%02d,%03d", new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf((int) ((j4 - ((long) (1000000 * i3))) / 1000))}));
            }
            System.arraycopy(bArr, 0, bArr2, 19, 12);
            zzjo zzjo = zzjy.zzaqt;
            zzoj zzoj = this.zzanz;
            zzjo.zza(zzoj, zzoj.limit());
            this.zzapj += this.zzanz.limit();
        }
        zzjy.zzaqt.zza(j, this.zzapa, this.zzapj, 0, zzjy.zzapt);
        this.zzapk = true;
        zzgm();
    }

    private final void zza(zzjg zzjg, zzjy zzjy, int i) throws IOException, InterruptedException {
        int i2;
        if ("S_TEXT/UTF8".equals(zzjy.zzapp)) {
            int length = zzann.length + i;
            if (this.zzanz.capacity() < length) {
                this.zzanz.data = Arrays.copyOf(zzann, length + i);
            }
            zzjg.readFully(this.zzanz.data, zzann.length, i);
            this.zzanz.zzbe(0);
            this.zzanz.zzbd(length);
            return;
        }
        zzjo zzjo = zzjy.zzaqt;
        if (!this.zzapc) {
            if (zzjy.zzapr) {
                this.zzapa &= -1073741825;
                int i3 = 128;
                if (!this.zzapd) {
                    zzjg.readFully(this.zzanv.data, 0, 1);
                    this.zzapb++;
                    byte[] bArr = this.zzanv.data;
                    if ((bArr[0] & 128) != 128) {
                        this.zzapg = bArr[0];
                        this.zzapd = true;
                    } else {
                        throw new zzhd("Extension bit is set in signal byte");
                    }
                }
                byte b = this.zzapg;
                if ((b & 1) == 1) {
                    boolean z = (b & 2) == 2;
                    this.zzapa |= 1073741824;
                    if (!this.zzape) {
                        zzjg.readFully(this.zzaoa.data, 0, 8);
                        this.zzapb += 8;
                        this.zzape = true;
                        byte[] bArr2 = this.zzanv.data;
                        if (!z) {
                            i3 = 0;
                        }
                        bArr2[0] = (byte) (i3 | 8);
                        this.zzanv.zzbe(0);
                        zzjo.zza(this.zzanv, 1);
                        this.zzapj++;
                        this.zzaoa.zzbe(0);
                        zzjo.zza(this.zzaoa, 8);
                        this.zzapj += 8;
                    }
                    if (z) {
                        if (!this.zzapf) {
                            zzjg.readFully(this.zzanv.data, 0, 1);
                            this.zzapb++;
                            this.zzanv.zzbe(0);
                            this.zzaph = this.zzanv.readUnsignedByte();
                            this.zzapf = true;
                        }
                        int i4 = this.zzaph << 2;
                        this.zzanv.reset(i4);
                        zzjg.readFully(this.zzanv.data, 0, i4);
                        this.zzapb += i4;
                        short s = (short) ((this.zzaph / 2) + 1);
                        int i5 = (s * 6) + 2;
                        ByteBuffer byteBuffer = this.zzaoc;
                        if (byteBuffer == null || byteBuffer.capacity() < i5) {
                            this.zzaoc = ByteBuffer.allocate(i5);
                        }
                        this.zzaoc.position(0);
                        this.zzaoc.putShort(s);
                        int i6 = 0;
                        int i7 = 0;
                        while (true) {
                            i2 = this.zzaph;
                            if (i6 >= i2) {
                                break;
                            }
                            int zzis = this.zzanv.zzis();
                            if (i6 % 2 == 0) {
                                this.zzaoc.putShort((short) (zzis - i7));
                            } else {
                                this.zzaoc.putInt(zzis - i7);
                            }
                            i6++;
                            i7 = zzis;
                        }
                        int i8 = (i - this.zzapb) - i7;
                        if (i2 % 2 == 1) {
                            this.zzaoc.putInt(i8);
                        } else {
                            this.zzaoc.putShort((short) i8);
                            this.zzaoc.putInt(0);
                        }
                        this.zzaob.zzb(this.zzaoc.array(), i5);
                        zzjo.zza(this.zzaob, i5);
                        this.zzapj += i5;
                    }
                }
            } else {
                byte[] bArr3 = zzjy.zzaps;
                if (bArr3 != null) {
                    this.zzany.zzb(bArr3, bArr3.length);
                }
            }
            this.zzapc = true;
        }
        int limit = i + this.zzany.limit();
        if (!"V_MPEG4/ISO/AVC".equals(zzjy.zzapp) && !"V_MPEGH/ISO/HEVC".equals(zzjy.zzapp)) {
            while (true) {
                int i9 = this.zzapb;
                if (i9 >= limit) {
                    break;
                }
                zza(zzjg, zzjo, limit - i9);
            }
        } else {
            byte[] bArr4 = this.zzanu.data;
            bArr4[0] = 0;
            bArr4[1] = 0;
            bArr4[2] = 0;
            int i10 = zzjy.zzaqu;
            int i11 = 4 - i10;
            while (this.zzapb < limit) {
                int i12 = this.zzapi;
                if (i12 == 0) {
                    int min = Math.min(i10, this.zzany.zzin());
                    zzjg.readFully(bArr4, i11 + min, i10 - min);
                    if (min > 0) {
                        this.zzany.zze(bArr4, i11, min);
                    }
                    this.zzapb += i10;
                    this.zzanu.zzbe(0);
                    this.zzapi = this.zzanu.zzis();
                    this.zzant.zzbe(0);
                    zzjo.zza(this.zzant, 4);
                    this.zzapj += 4;
                } else {
                    this.zzapi = i12 - zza(zzjg, zzjo, i12);
                }
            }
        }
        if ("A_VORBIS".equals(zzjy.zzapp)) {
            this.zzanw.zzbe(0);
            zzjo.zza(this.zzanw, 4);
            this.zzapj += 4;
        }
    }

    private final int zza(zzjg zzjg, zzjo zzjo, int i) throws IOException, InterruptedException {
        int i2;
        int zzin = this.zzany.zzin();
        if (zzin > 0) {
            i2 = Math.min(i, zzin);
            zzjo.zza(this.zzany, i2);
        } else {
            i2 = zzjo.zza(zzjg, i, false);
        }
        this.zzapb += i2;
        this.zzapj += i2;
        return i2;
    }

    private static int[] zza(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        if (iArr.length >= i) {
            return iArr;
        }
        return new int[Math.max(iArr.length << 1, i)];
    }
}
