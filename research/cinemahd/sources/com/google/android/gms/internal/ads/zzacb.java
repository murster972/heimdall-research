package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.ArrayList;
import java.util.List;

public final class zzacb extends NativeAd.AdChoicesInfo {
    private String text;
    private final List<NativeAd.Image> zzcvf = new ArrayList();
    private final zzaca zzcvq;

    public zzacb(zzaca zzaca) {
        zzaci zzaci;
        IBinder iBinder;
        this.zzcvq = zzaca;
        try {
            this.text = this.zzcvq.getText();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            this.text = "";
        }
        try {
            for (zzaci next : zzaca.zzqx()) {
                if (!(next instanceof IBinder) || (iBinder = (IBinder) next) == null) {
                    zzaci = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                    zzaci = queryLocalInterface instanceof zzaci ? (zzaci) queryLocalInterface : new zzack(iBinder);
                }
                if (zzaci != null) {
                    this.zzcvf.add(new zzacj(zzaci));
                }
            }
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final List<NativeAd.Image> getImages() {
        return this.zzcvf;
    }

    public final CharSequence getText() {
        return this.text;
    }
}
