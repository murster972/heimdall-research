package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzahh extends IInterface {
    void onInstreamAdFailedToLoad(int i) throws RemoteException;

    void zza(zzahb zzahb) throws RemoteException;
}
