package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final class zztj implements zzdsa {
    static final zzdsa zzew = new zztj();

    private zztj() {
    }

    public final boolean zzf(int i) {
        return zzsy.zzh.zzc.zzbz(i) != null;
    }
}
