package com.google.android.gms.internal.ads;

public class zzcns {
    private final zzaak zzgca;

    public zzcns(zzaak zzaak) {
        this.zzgca = zzaak;
    }

    /* access modifiers changed from: package-private */
    public final zzaak zzami() {
        return this.zzgca;
    }
}
