package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final /* synthetic */ class zzcda implements zzsp {
    private final zzsy.zza zzfhy;

    zzcda(zzsy.zza zza) {
        this.zzfhy = zza;
    }

    public final void zza(zztu zztu) {
        zztu.zzcbb = this.zzfhy;
    }
}
