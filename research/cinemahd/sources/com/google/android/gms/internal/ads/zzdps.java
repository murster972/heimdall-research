package com.google.android.gms.internal.ads;

import java.io.PrintWriter;

abstract class zzdps {
    private static final Throwable[] zzhha = new Throwable[0];

    zzdps() {
    }

    public abstract void zza(Throwable th, PrintWriter printWriter);

    public abstract void zza(Throwable th, Throwable th2);

    public abstract void zzl(Throwable th);
}
