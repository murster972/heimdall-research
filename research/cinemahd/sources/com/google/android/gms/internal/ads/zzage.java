package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzage implements Parcelable.Creator<zzagb> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        String str = null;
        byte[] bArr = null;
        String[] strArr = null;
        String[] strArr2 = null;
        long j = 0;
        boolean z = false;
        int i = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 1:
                    z = SafeParcelReader.s(parcel2, a2);
                    break;
                case 2:
                    str = SafeParcelReader.o(parcel2, a2);
                    break;
                case 3:
                    i = SafeParcelReader.w(parcel2, a2);
                    break;
                case 4:
                    bArr = SafeParcelReader.g(parcel2, a2);
                    break;
                case 5:
                    strArr = SafeParcelReader.p(parcel2, a2);
                    break;
                case 6:
                    strArr2 = SafeParcelReader.p(parcel2, a2);
                    break;
                case 7:
                    z2 = SafeParcelReader.s(parcel2, a2);
                    break;
                case 8:
                    j = SafeParcelReader.y(parcel2, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel2, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel2, b);
        return new zzagb(z, str, i, bArr, strArr, strArr2, z2, j);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzagb[i];
    }
}
