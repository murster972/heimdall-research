package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zzsg implements BaseGmsClient.BaseConnectionCallbacks {
    private final /* synthetic */ zzry zzbrr;
    private final /* synthetic */ zzazl zzbrs;
    final /* synthetic */ zzse zzbrt;

    zzsg(zzse zzse, zzry zzry, zzazl zzazl) {
        this.zzbrt = zzse;
        this.zzbrr = zzry;
        this.zzbrs = zzazl;
    }

    public final void onConnected(Bundle bundle) {
        synchronized (this.zzbrt.lock) {
            if (!this.zzbrt.zzbrq) {
                boolean unused = this.zzbrt.zzbrq = true;
                zzrz zzd = this.zzbrt.zzbrd;
                if (zzd != null) {
                    this.zzbrs.addListener(new zzsi(this.zzbrs, zzazd.zzdwe.zzf(new zzsj(this, zzd, this.zzbrr, this.zzbrs))), zzazd.zzdwj);
                }
            }
        }
    }

    public final void onConnectionSuspended(int i) {
    }
}
