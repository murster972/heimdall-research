package com.google.android.gms.internal.ads;

final class zzddy<T> extends zzdej<T> {
    static final zzddy<Object> zzgtx = new zzddy<>();

    private zzddy() {
    }

    public final boolean equals(Object obj) {
        return obj == this;
    }

    public final int hashCode() {
        return 2040732332;
    }

    public final String toString() {
        return "Optional.absent()";
    }

    public final T zzaqt() {
        return null;
    }
}
