package com.google.android.gms.internal.ads;

public final class zzbtb implements zzdxg<zzbtc> {
    private final zzdxp<zzdda> zzepi;
    private final zzdxp<zzczl> zzfbp;

    private zzbtb(zzdxp<zzczl> zzdxp, zzdxp<zzdda> zzdxp2) {
        this.zzfbp = zzdxp;
        this.zzepi = zzdxp2;
    }

    public static zzbtb zzi(zzdxp<zzczl> zzdxp, zzdxp<zzdda> zzdxp2) {
        return new zzbtb(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzbtc(this.zzfbp.get(), this.zzepi.get());
    }
}
