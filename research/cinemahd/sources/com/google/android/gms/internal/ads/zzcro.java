package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzcro implements zzdxg<zzczj> {
    private final zzdxp<Clock> zzfcz;

    public zzcro(zzdxp<Clock> zzdxp) {
        this.zzfcz = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzczj) zzdxm.zza(new zzczj(this.zzfcz.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
