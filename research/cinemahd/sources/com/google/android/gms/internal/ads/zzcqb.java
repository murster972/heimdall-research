package com.google.android.gms.internal.ads;

public final class zzcqb implements zzdxg<zzcpy> {
    private final zzdxp<zzczu> zzfep;

    private zzcqb(zzdxp<zzczu> zzdxp) {
        this.zzfep = zzdxp;
    }

    public static zzcqb zzah(zzdxp<zzczu> zzdxp) {
        return new zzcqb(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcpy(this.zzfep.get());
    }
}
