package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdks extends zzdih<zzdmw, zzdmv> {
    zzdks(zzdkq zzdkq, Class cls) {
        super(cls);
    }

    public final /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdmw zzdmw = (zzdmw) zzdte;
        if (zzdmw.getKeySize() >= 16) {
            zzdkq.zza(zzdmw.zzauz());
            return;
        }
        throw new GeneralSecurityException("key too short");
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdmw zzdmw = (zzdmw) zzdte;
        return (zzdmv) zzdmv.zzava().zzen(0).zzd(zzdmw.zzauz()).zzau(zzdqk.zzu(zzdpn.zzey(zzdmw.getKeySize()))).zzbaf();
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdmw.zzat(zzdqk);
    }
}
