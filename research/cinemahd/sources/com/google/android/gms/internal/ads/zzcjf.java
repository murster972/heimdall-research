package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import com.google.android.gms.ads.zzb;
import java.util.concurrent.Executor;

public final class zzcjf implements zzcir<zzbkk, zzdac, zzcjy> {
    private final zzazb zzbli;
    private final Executor zzfci;
    private final zzblg zzfyj;
    private final Context zzup;

    public zzcjf(Context context, zzazb zzazb, zzblg zzblg, Executor executor) {
        this.zzup = context;
        this.zzbli = zzazb;
        this.zzfyj = zzblg;
        this.zzfci = executor;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzdac, zzcjy> zzcip) throws zzdab {
        zzuj zzuj;
        zzuj zzuj2 = zzczt.zzgmh.zzfgl.zzblm;
        if (zzuj2.zzccv) {
            zzuj = new zzuj(this.zzup, zzb.zza(zzuj2.width, zzuj2.height));
        } else {
            zzuj = zzczy.zza(this.zzup, zzczl.zzglq);
        }
        zzuj zzuj3 = zzuj;
        if (this.zzbli.zzdwa < 4100000) {
            ((zzdac) zzcip.zzddn).zza(this.zzup, zzuj3, zzczt.zzgmh.zzfgl.zzgml, zzczl.zzglr.toString(), (zzali) zzcip.zzfyf);
        } else {
            ((zzdac) zzcip.zzddn).zza(this.zzup, zzuj3, zzczt.zzgmh.zzfgl.zzgml, zzczl.zzglr.toString(), zzaxs.zza((zzaxx) zzczl.zzglo), (zzali) zzcip.zzfyf);
        }
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzblg zzblg = this.zzfyj;
        zzbmt zzbmt = new zzbmt(zzczt, zzczl, zzcip.zzfge);
        View view = ((zzdac) zzcip.zzddn).getView();
        zzdac zzdac = (zzdac) zzcip.zzddn;
        zzdac.getClass();
        zzbkj zza = zzblg.zza(zzbmt, new zzbkn(view, (zzbdi) null, zzcji.zza(zzdac), zzczl.zzglq.get(0)));
        zza.zzaei().zzq(((zzdac) zzcip.zzddn).getView());
        zza.zzadh().zza(new zzbiu((zzdac) zzcip.zzddn), this.zzfci);
        ((zzcjy) zzcip.zzfyf).zza((zzali) zza.zzadm());
        return zza.zzaeh();
    }
}
