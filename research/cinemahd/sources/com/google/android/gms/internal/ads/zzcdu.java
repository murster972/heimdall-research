package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class zzcdu implements zzdcx {
    private final Clock zzbmq;
    private final Map<zzdco, Long> zzfst = new HashMap();
    private final zzcds zzfsu;
    private final Map<zzdco, zzcdt> zzfsv = new HashMap();

    public zzcdu(zzcds zzcds, Set<zzcdt> set, Clock clock) {
        this.zzfsu = zzcds;
        for (zzcdt next : set) {
            this.zzfsv.put(next.zzfss, next);
        }
        this.zzbmq = clock;
    }

    public final void zza(zzdco zzdco, String str) {
    }

    public final void zza(zzdco zzdco, String str, Throwable th) {
        if (this.zzfst.containsKey(zzdco)) {
            long a2 = this.zzbmq.a() - this.zzfst.get(zzdco).longValue();
            Map<String, String> zzqu = this.zzfsu.zzqu();
            String valueOf = String.valueOf(str);
            String concat = valueOf.length() != 0 ? "task.".concat(valueOf) : new String("task.");
            String valueOf2 = String.valueOf(Long.toString(a2));
            zzqu.put(concat, valueOf2.length() != 0 ? "f.".concat(valueOf2) : new String("f."));
        }
        if (this.zzfsv.containsKey(zzdco)) {
            zza(zzdco, false);
        }
    }

    public final void zzb(zzdco zzdco, String str) {
        this.zzfst.put(zzdco, Long.valueOf(this.zzbmq.a()));
    }

    public final void zzc(zzdco zzdco, String str) {
        if (this.zzfst.containsKey(zzdco)) {
            long a2 = this.zzbmq.a() - this.zzfst.get(zzdco).longValue();
            Map<String, String> zzqu = this.zzfsu.zzqu();
            String valueOf = String.valueOf(str);
            String concat = valueOf.length() != 0 ? "task.".concat(valueOf) : new String("task.");
            String valueOf2 = String.valueOf(Long.toString(a2));
            zzqu.put(concat, valueOf2.length() != 0 ? "s.".concat(valueOf2) : new String("s."));
        }
        if (this.zzfsv.containsKey(zzdco)) {
            zza(zzdco, true);
        }
    }

    private final void zza(zzdco zzdco, boolean z) {
        zzdco zzb = this.zzfsv.get(zzdco).zzfsr;
        String str = z ? "s." : "f.";
        if (this.zzfst.containsKey(zzb)) {
            long a2 = this.zzbmq.a() - this.zzfst.get(zzb).longValue();
            Map<String, String> zzqu = this.zzfsu.zzqu();
            String valueOf = String.valueOf(this.zzfsv.get(zzdco).label);
            String concat = valueOf.length() != 0 ? "label.".concat(valueOf) : new String("label.");
            String valueOf2 = String.valueOf(Long.toString(a2));
            zzqu.put(concat, valueOf2.length() != 0 ? str.concat(valueOf2) : new String(str));
        }
    }
}
