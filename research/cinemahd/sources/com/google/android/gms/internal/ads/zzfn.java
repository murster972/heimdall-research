package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

public final class zzfn extends zzfw {
    public zzfn(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 51);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        synchronized (this.zzzt) {
            zzej zzej = new zzej((String) this.zzaae.invoke((Object) null, new Object[0]));
            this.zzzt.zzbj(zzej.zzya.longValue());
            this.zzzt.zzbk(zzej.zzyb.longValue());
        }
    }
}
