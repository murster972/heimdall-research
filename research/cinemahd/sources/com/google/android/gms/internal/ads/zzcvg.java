package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcvg implements zzdxg<zzcve> {
    private final zzdxp<Context> zzfhb;

    public zzcvg(zzdxp<Context> zzdxp) {
        this.zzfhb = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcve(this.zzfhb.get());
    }
}
