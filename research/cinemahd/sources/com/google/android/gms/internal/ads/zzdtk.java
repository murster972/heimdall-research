package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

final class zzdtk<T> implements zzdua<T> {
    private final zzdte zzhpb;
    private final boolean zzhpc;
    private final zzdus<?, ?> zzhpl;
    private final zzdri<?> zzhpm;

    private zzdtk(zzdus<?, ?> zzdus, zzdri<?> zzdri, zzdte zzdte) {
        this.zzhpl = zzdus;
        this.zzhpc = zzdri.zzj(zzdte);
        this.zzhpm = zzdri;
        this.zzhpb = zzdte;
    }

    static <T> zzdtk<T> zza(zzdus<?, ?> zzdus, zzdri<?> zzdri, zzdte zzdte) {
        return new zzdtk<>(zzdus, zzdri, zzdte);
    }

    public final boolean equals(T t, T t2) {
        if (!this.zzhpl.zzbb(t).equals(this.zzhpl.zzbb(t2))) {
            return false;
        }
        if (this.zzhpc) {
            return this.zzhpm.zzal(t).equals(this.zzhpm.zzal(t2));
        }
        return true;
    }

    public final int hashCode(T t) {
        int hashCode = this.zzhpl.zzbb(t).hashCode();
        return this.zzhpc ? (hashCode * 53) + this.zzhpm.zzal(t).hashCode() : hashCode;
    }

    public final T newInstance() {
        return this.zzhpb.zzazy().zzbae();
    }

    public final void zzan(T t) {
        this.zzhpl.zzan(t);
        this.zzhpm.zzan(t);
    }

    public final int zzax(T t) {
        zzdus<?, ?> zzdus = this.zzhpl;
        int zzbd = zzdus.zzbd(zzdus.zzbb(t)) + 0;
        return this.zzhpc ? zzbd + this.zzhpm.zzal(t).zzazn() : zzbd;
    }

    public final boolean zzaz(T t) {
        return this.zzhpm.zzal(t).isInitialized();
    }

    public final void zzf(T t, T t2) {
        zzduc.zza(this.zzhpl, t, t2);
        if (this.zzhpc) {
            zzduc.zza(this.zzhpm, t, t2);
        }
    }

    public final void zza(T t, zzdvl zzdvl) throws IOException {
        Iterator<Map.Entry<?, Object>> it2 = this.zzhpm.zzal(t).iterator();
        while (it2.hasNext()) {
            Map.Entry next = it2.next();
            zzdro zzdro = (zzdro) next.getKey();
            if (zzdro.zzazp() != zzdvm.MESSAGE || zzdro.zzazq() || zzdro.zzazr()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzdsh) {
                zzdvl.zzc(zzdro.zzae(), ((zzdsh) next).zzbau().zzaxk());
            } else {
                zzdvl.zzc(zzdro.zzae(), next.getValue());
            }
        }
        zzdus<?, ?> zzdus = this.zzhpl;
        zzdus.zzc(zzdus.zzbb(t), zzdvl);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.google.android.gms.internal.ads.zzdrt$zzf} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r10, byte[] r11, int r12, int r13, com.google.android.gms.internal.ads.zzdqf r14) throws java.io.IOException {
        /*
            r9 = this;
            r0 = r10
            com.google.android.gms.internal.ads.zzdrt r0 = (com.google.android.gms.internal.ads.zzdrt) r0
            com.google.android.gms.internal.ads.zzdur r1 = r0.zzhmk
            com.google.android.gms.internal.ads.zzdur r2 = com.google.android.gms.internal.ads.zzdur.zzbcf()
            if (r1 != r2) goto L_0x0011
            com.google.android.gms.internal.ads.zzdur r1 = com.google.android.gms.internal.ads.zzdur.zzbcg()
            r0.zzhmk = r1
        L_0x0011:
            com.google.android.gms.internal.ads.zzdrt$zzd r10 = (com.google.android.gms.internal.ads.zzdrt.zzd) r10
            r10.zzbag()
            r10 = 0
            r0 = r10
        L_0x0018:
            if (r12 >= r13) goto L_0x00a4
            int r4 = com.google.android.gms.internal.ads.zzdqg.zza(r11, r12, r14)
            int r2 = r14.zzhhq
            r12 = 11
            r3 = 2
            if (r2 == r12) goto L_0x0051
            r12 = r2 & 7
            if (r12 != r3) goto L_0x004c
            com.google.android.gms.internal.ads.zzdri<?> r12 = r9.zzhpm
            com.google.android.gms.internal.ads.zzdrg r0 = r14.zzhht
            com.google.android.gms.internal.ads.zzdte r3 = r9.zzhpb
            int r5 = r2 >>> 3
            java.lang.Object r12 = r12.zza(r0, r3, r5)
            r0 = r12
            com.google.android.gms.internal.ads.zzdrt$zzf r0 = (com.google.android.gms.internal.ads.zzdrt.zzf) r0
            if (r0 != 0) goto L_0x0043
            r3 = r11
            r5 = r13
            r6 = r1
            r7 = r14
            int r12 = com.google.android.gms.internal.ads.zzdqg.zza((int) r2, (byte[]) r3, (int) r4, (int) r5, (com.google.android.gms.internal.ads.zzdur) r6, (com.google.android.gms.internal.ads.zzdqf) r7)
            goto L_0x0018
        L_0x0043:
            com.google.android.gms.internal.ads.zzdtp.zzbbm()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x004c:
            int r12 = com.google.android.gms.internal.ads.zzdqg.zza((int) r2, (byte[]) r11, (int) r4, (int) r13, (com.google.android.gms.internal.ads.zzdqf) r14)
            goto L_0x0018
        L_0x0051:
            r12 = 0
            r2 = r10
        L_0x0053:
            if (r4 >= r13) goto L_0x0099
            int r4 = com.google.android.gms.internal.ads.zzdqg.zza(r11, r4, r14)
            int r5 = r14.zzhhq
            int r6 = r5 >>> 3
            r7 = r5 & 7
            if (r6 == r3) goto L_0x007b
            r8 = 3
            if (r6 == r8) goto L_0x0065
            goto L_0x0090
        L_0x0065:
            if (r0 != 0) goto L_0x0072
            if (r7 != r3) goto L_0x0090
            int r4 = com.google.android.gms.internal.ads.zzdqg.zze(r11, r4, r14)
            java.lang.Object r2 = r14.zzhhs
            com.google.android.gms.internal.ads.zzdqk r2 = (com.google.android.gms.internal.ads.zzdqk) r2
            goto L_0x0053
        L_0x0072:
            com.google.android.gms.internal.ads.zzdtp.zzbbm()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x007b:
            if (r7 != 0) goto L_0x0090
            int r4 = com.google.android.gms.internal.ads.zzdqg.zza(r11, r4, r14)
            int r12 = r14.zzhhq
            com.google.android.gms.internal.ads.zzdri<?> r0 = r9.zzhpm
            com.google.android.gms.internal.ads.zzdrg r5 = r14.zzhht
            com.google.android.gms.internal.ads.zzdte r6 = r9.zzhpb
            java.lang.Object r0 = r0.zza(r5, r6, r12)
            com.google.android.gms.internal.ads.zzdrt$zzf r0 = (com.google.android.gms.internal.ads.zzdrt.zzf) r0
            goto L_0x0053
        L_0x0090:
            r6 = 12
            if (r5 == r6) goto L_0x0099
            int r4 = com.google.android.gms.internal.ads.zzdqg.zza((int) r5, (byte[]) r11, (int) r4, (int) r13, (com.google.android.gms.internal.ads.zzdqf) r14)
            goto L_0x0053
        L_0x0099:
            if (r2 == 0) goto L_0x00a1
            int r12 = r12 << 3
            r12 = r12 | r3
            r1.zzd(r12, r2)
        L_0x00a1:
            r12 = r4
            goto L_0x0018
        L_0x00a4:
            if (r12 != r13) goto L_0x00a7
            return
        L_0x00a7:
            com.google.android.gms.internal.ads.zzdse r10 = com.google.android.gms.internal.ads.zzdse.zzbaq()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdtk.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.ads.zzdqf):void");
    }

    public final void zza(T t, zzdtu zzdtu, zzdrg zzdrg) throws IOException {
        boolean z;
        zzdus<?, ?> zzdus = this.zzhpl;
        zzdri<?> zzdri = this.zzhpm;
        Object zzbc = zzdus.zzbc(t);
        zzdrm<?> zzam = zzdri.zzam(t);
        do {
            try {
                if (zzdtu.zzaza() == Integer.MAX_VALUE) {
                    zzdus.zzi(t, zzbc);
                    return;
                }
                int tag = zzdtu.getTag();
                if (tag == 11) {
                    int i = 0;
                    Object obj = null;
                    zzdqk zzdqk = null;
                    while (zzdtu.zzaza() != Integer.MAX_VALUE) {
                        int tag2 = zzdtu.getTag();
                        if (tag2 == 16) {
                            i = zzdtu.zzayl();
                            obj = zzdri.zza(zzdrg, this.zzhpb, i);
                        } else if (tag2 == 26) {
                            if (obj != null) {
                                zzdri.zza(zzdtu, obj, zzdrg, zzam);
                            } else {
                                zzdqk = zzdtu.zzayk();
                            }
                        } else if (!zzdtu.zzazb()) {
                            break;
                        }
                    }
                    if (zzdtu.getTag() != 12) {
                        throw zzdse.zzban();
                    } else if (zzdqk != null) {
                        if (obj != null) {
                            zzdri.zza(zzdqk, obj, zzdrg, zzam);
                        } else {
                            zzdus.zza(zzbc, i, zzdqk);
                        }
                    }
                } else if ((tag & 7) == 2) {
                    Object zza = zzdri.zza(zzdrg, this.zzhpb, tag >>> 3);
                    if (zza != null) {
                        zzdri.zza(zzdtu, zza, zzdrg, zzam);
                    } else {
                        z = zzdus.zza(zzbc, zzdtu);
                        continue;
                    }
                } else {
                    z = zzdtu.zzazb();
                    continue;
                }
                z = true;
                continue;
            } finally {
                zzdus.zzi(t, zzbc);
            }
        } while (z);
    }
}
