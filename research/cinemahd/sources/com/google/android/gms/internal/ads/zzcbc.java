package com.google.android.gms.internal.ads;

public final class zzcbc implements zzdxg<zzcaz> {
    private final zzdxp<zzbpm> zzesd;
    private final zzdxp<zzczl> zzfbp;

    public zzcbc(zzdxp<zzbpm> zzdxp, zzdxp<zzczl> zzdxp2) {
        this.zzesd = zzdxp;
        this.zzfbp = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcaz(this.zzesd.get(), this.zzfbp.get());
    }
}
