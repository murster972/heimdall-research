package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbpm extends zzbrl<zzbov> {
    public zzbpm(Set<zzbsu<zzbov>> set) {
        super(set);
    }

    public final void onAdClosed() {
        zza(zzbpp.zzfhp);
    }

    public final void onAdLeftApplication() {
        zza(zzbpo.zzfhp);
    }

    public final void onAdOpened() {
        zza(zzbpr.zzfhp);
    }

    public final void onRewardedVideoCompleted() {
        zza(zzbps.zzfhp);
    }

    public final void onRewardedVideoStarted() {
        zza(zzbpq.zzfhp);
    }

    public final void zzb(zzare zzare, String str, String str2) {
        zza(new zzbpt(zzare, str, str2));
    }
}
