package com.google.android.gms.internal.ads;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

public final class zzcfx extends zzcfv {
    /* access modifiers changed from: private */
    public static final Pattern zzfvg = Pattern.compile("Received error HTTP response code: (.*)");
    private final ScheduledExecutorService zzffx;
    private final zzczu zzfgl;
    private final zzdhd zzfov;
    private final zzcfe zzfve;
    /* access modifiers changed from: private */
    public final zzchz zzfvf;

    zzcfx(zzbqs zzbqs, zzczu zzczu, zzcfe zzcfe, zzdhd zzdhd, ScheduledExecutorService scheduledExecutorService, zzchz zzchz) {
        super(zzbqs);
        this.zzfgl = zzczu;
        this.zzfve = zzcfe;
        this.zzfov = zzdhd;
        this.zzffx = scheduledExecutorService;
        this.zzfvf = zzchz;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzd(InputStream inputStream) throws Exception {
        return zzdgs.zzaj(new zzczt(new zzczo(this.zzfgl), zzczr.zza(new InputStreamReader(inputStream))));
    }

    public final zzdhe<zzczt> zze(zzaqk zzaqk) throws zzcfb {
        zzdhe<zzczt> zzb = zzdgs.zzb(this.zzfve.zzc(zzaqk), new zzcga(this), (Executor) this.zzfov);
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcow)).booleanValue()) {
            zzb = zzdgs.zzb(zzdgs.zza(zzb, (long) ((Integer) zzve.zzoy().zzd(zzzn.zzcox)).intValue(), TimeUnit.SECONDS, this.zzffx), TimeoutException.class, zzcfz.zzbkw, zzazd.zzdwj);
        }
        zzdgs.zza(zzb, new zzcgc(this), zzazd.zzdwj);
        return zzb;
    }
}
