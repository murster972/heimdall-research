package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaty implements zzaui {
    static final zzaui zzdpr = new zzaty();

    private zzaty() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return zzbfq.getAppInstanceId();
    }
}
