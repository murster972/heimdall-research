package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

final class zzgz implements Parcelable.Creator<zzgw> {
    zzgz() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzgw(parcel);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgw[i];
    }
}
