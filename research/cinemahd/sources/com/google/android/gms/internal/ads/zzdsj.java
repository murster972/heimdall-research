package com.google.android.gms.internal.ads;

public class zzdsj {
    private static final zzdrg zzhhl = zzdrg.zzazh();
    private zzdqk zzhob;
    private volatile zzdte zzhoc;
    private volatile zzdqk zzhod;

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.ads.zzdte zzm(com.google.android.gms.internal.ads.zzdte r2) {
        /*
            r1 = this;
            com.google.android.gms.internal.ads.zzdte r0 = r1.zzhoc
            if (r0 != 0) goto L_0x001d
            monitor-enter(r1)
            com.google.android.gms.internal.ads.zzdte r0 = r1.zzhoc     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x000b:
            r1.zzhoc = r2     // Catch:{ zzdse -> 0x0012 }
            com.google.android.gms.internal.ads.zzdqk r0 = com.google.android.gms.internal.ads.zzdqk.zzhhx     // Catch:{ zzdse -> 0x0012 }
            r1.zzhod = r0     // Catch:{ zzdse -> 0x0012 }
            goto L_0x0018
        L_0x0012:
            r1.zzhoc = r2     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.ads.zzdqk r2 = com.google.android.gms.internal.ads.zzdqk.zzhhx     // Catch:{ all -> 0x001a }
            r1.zzhod = r2     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x001a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r2
        L_0x001d:
            com.google.android.gms.internal.ads.zzdte r2 = r1.zzhoc
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdsj.zzm(com.google.android.gms.internal.ads.zzdte):com.google.android.gms.internal.ads.zzdte");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdsj)) {
            return false;
        }
        zzdsj zzdsj = (zzdsj) obj;
        zzdte zzdte = this.zzhoc;
        zzdte zzdte2 = zzdsj.zzhoc;
        if (zzdte == null && zzdte2 == null) {
            return zzaxk().equals(zzdsj.zzaxk());
        }
        if (zzdte != null && zzdte2 != null) {
            return zzdte.equals(zzdte2);
        }
        if (zzdte != null) {
            return zzdte.equals(zzdsj.zzm(zzdte.zzazz()));
        }
        return zzm(zzdte2.zzazz()).equals(zzdte2);
    }

    public int hashCode() {
        return 1;
    }

    public final zzdqk zzaxk() {
        if (this.zzhod != null) {
            return this.zzhod;
        }
        synchronized (this) {
            if (this.zzhod != null) {
                zzdqk zzdqk = this.zzhod;
                return zzdqk;
            }
            if (this.zzhoc == null) {
                this.zzhod = zzdqk.zzhhx;
            } else {
                this.zzhod = this.zzhoc.zzaxk();
            }
            zzdqk zzdqk2 = this.zzhod;
            return zzdqk2;
        }
    }

    public final int zzazu() {
        if (this.zzhod != null) {
            return this.zzhod.size();
        }
        if (this.zzhoc != null) {
            return this.zzhoc.zzazu();
        }
        return 0;
    }

    public final zzdte zzn(zzdte zzdte) {
        zzdte zzdte2 = this.zzhoc;
        this.zzhob = null;
        this.zzhod = null;
        this.zzhoc = zzdte;
        return zzdte2;
    }
}
