package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

final class zzdgu<V> implements Runnable {
    private final Future<V> zzgwu;
    private final zzdgt<? super V> zzgwv;

    zzdgu(Future<V> future, zzdgt<? super V> zzdgt) {
        this.zzgwu = future;
        this.zzgwv = zzdgt;
    }

    public final void run() {
        try {
            this.zzgwv.onSuccess(zzdgs.zzb(this.zzgwu));
        } catch (ExecutionException e) {
            this.zzgwv.zzb(e.getCause());
        } catch (Error | RuntimeException e2) {
            this.zzgwv.zzb(e2);
        }
    }

    public final String toString() {
        return zzdec.zzz(this).zzaa(this.zzgwv).toString();
    }
}
