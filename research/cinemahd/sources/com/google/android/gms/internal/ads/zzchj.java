package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class zzchj implements zzajv<zzchk> {
    zzchj() {
    }

    public final /* synthetic */ JSONObject zzj(Object obj) throws JSONException {
        zzchk zzchk = (zzchk) obj;
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        jSONObject2.put("base_url", zzchk.zzfwi.zzub());
        jSONObject2.put("signals", zzchk.zzfwj);
        jSONObject3.put("body", zzchk.zzfwl.zzdki);
        jSONObject3.put("headers", zzq.zzkq().zzi((Map<String, ?>) zzchk.zzfwl.zzab));
        jSONObject3.put("response_code", zzchk.zzfwl.zzfws);
        jSONObject3.put("latency", zzchk.zzfwl.zzfwt);
        jSONObject.put("request", jSONObject2);
        jSONObject.put("response", jSONObject3);
        jSONObject.put("flags", zzchk.zzfwi.zzue());
        return jSONObject;
    }
}
