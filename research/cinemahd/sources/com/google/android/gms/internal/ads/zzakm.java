package com.google.android.gms.internal.ads;

final /* synthetic */ class zzakm implements zzdgf {
    private final zzafn zzcyv;
    private final String zzcyz;

    zzakm(String str, zzafn zzafn) {
        this.zzcyz = str;
        this.zzcyv = zzafn;
    }

    public final zzdhe zzf(Object obj) {
        zzajq zzajq = (zzajq) obj;
        zzajq.zza(this.zzcyz, this.zzcyv);
        return zzdgs.zzaj(zzajq);
    }
}
