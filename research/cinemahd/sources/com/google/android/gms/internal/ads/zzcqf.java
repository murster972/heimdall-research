package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzcqf implements zzdxg<zzcqa> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzdhe<String>> zzger;

    private zzcqf(zzdxp<zzdhe<String>> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzger = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcqf zzak(zzdxp<zzdhe<String>> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcqf(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcqa(this.zzger.get(), this.zzfcv.get());
    }
}
