package com.google.android.gms.internal.ads;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.gms.common.internal.Preconditions;

public final class zzaxw {
    private Handler handler = null;
    private final Object lock = new Object();
    private HandlerThread zzdup = null;
    private int zzduq = 0;

    public final Handler getHandler() {
        return this.handler;
    }

    public final Looper zzxb() {
        Looper looper;
        synchronized (this.lock) {
            if (this.zzduq != 0) {
                Preconditions.a(this.zzdup, (Object) "Invalid state: mHandlerThread should already been initialized.");
            } else if (this.zzdup == null) {
                zzavs.zzed("Starting the looper thread.");
                this.zzdup = new HandlerThread("LooperProvider");
                this.zzdup.start();
                this.handler = new zzddu(this.zzdup.getLooper());
                zzavs.zzed("Looper thread started.");
            } else {
                zzavs.zzed("Resuming the looper thread");
                this.lock.notifyAll();
            }
            this.zzduq++;
            looper = this.zzdup.getLooper();
        }
        return looper;
    }
}
