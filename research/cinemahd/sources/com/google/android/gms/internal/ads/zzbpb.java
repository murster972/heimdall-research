package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbpb implements zzdxg<zzboz> {
    private final zzdxp<Set<zzbsu<zzbpa>>> zzfeo;

    public zzbpb(zzdxp<Set<zzbsu<zzbpa>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzboz(this.zzfeo.get());
    }
}
