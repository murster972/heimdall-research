package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.dynamic.IObjectWrapper;

public final class zzyd extends zzvt {
    /* access modifiers changed from: private */
    public zzvh zzblq;

    public final void destroy() {
    }

    public final Bundle getAdMetadata() {
        return new Bundle();
    }

    public final String getAdUnitId() {
        return null;
    }

    public final String getMediationAdapterClassName() {
        return null;
    }

    public final zzxb getVideoController() {
        return null;
    }

    public final boolean isLoading() {
        return false;
    }

    public final boolean isReady() {
        return false;
    }

    public final void pause() {
    }

    public final void resume() {
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final void setManualImpressionsEnabled(boolean z) {
    }

    public final void setUserId(String str) {
    }

    public final void showInterstitial() {
    }

    public final void stopLoading() {
    }

    public final void zza(zzaak zzaak) {
    }

    public final void zza(zzaoy zzaoy) {
    }

    public final void zza(zzape zzape, String str) {
    }

    public final void zza(zzaro zzaro) {
    }

    public final void zza(zzrg zzrg) {
    }

    public final void zza(zzuj zzuj) {
    }

    public final void zza(zzuo zzuo) {
    }

    public final void zza(zzvg zzvg) {
    }

    public final void zza(zzvx zzvx) {
    }

    public final void zza(zzwc zzwc) {
    }

    public final void zza(zzwi zzwi) {
    }

    public final void zza(zzxh zzxh) {
    }

    public final void zza(zzyw zzyw) {
    }

    public final boolean zza(zzug zzug) {
        zzayu.zzex("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzayk.zzyu.post(new zzyg(this));
        return false;
    }

    public final void zzbr(String str) {
    }

    public final IObjectWrapper zzjx() {
        return null;
    }

    public final void zzjy() {
    }

    public final zzuj zzjz() {
        return null;
    }

    public final String zzka() {
        return null;
    }

    public final zzxa zzkb() {
        return null;
    }

    public final zzwc zzkc() {
        return null;
    }

    public final zzvh zzkd() {
        return null;
    }

    public final void zza(zzvh zzvh) {
        this.zzblq = zzvh;
    }
}
