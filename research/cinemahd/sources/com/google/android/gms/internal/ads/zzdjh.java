package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

public final class zzdjh extends zzdii<zzdlo> {
    zzdjh() {
        super(zzdlo.class, new zzdjk(zzdhx.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesEaxKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    public final zzdih<zzdlr, zzdlo> zzasg() {
        return new zzdjj(this, zzdlr.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdlo zzdlo = (zzdlo) zzdte;
        zzdpo.zzx(zzdlo.getVersion(), 0);
        zzdpo.zzez(zzdlo.zzass().size());
        if (zzdlo.zzatq().zzatn() != 12 && zzdlo.zzatq().zzatn() != 16) {
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        }
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdlo.zzab(zzdqk);
    }
}
