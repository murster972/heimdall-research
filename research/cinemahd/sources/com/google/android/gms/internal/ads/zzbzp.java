package com.google.android.gms.internal.ads;

import android.graphics.Rect;

final /* synthetic */ class zzbzp implements zzps {
    private final zzbdi zzehp;

    zzbzp(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void zza(zzpt zzpt) {
        zzbev zzaaa = this.zzehp.zzaaa();
        Rect rect = zzpt.zzbob;
        zzaaa.zza(rect.left, rect.top, false);
    }
}
