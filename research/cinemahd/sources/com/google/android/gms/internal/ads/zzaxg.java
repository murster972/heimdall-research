package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

public final class zzaxg {
    private final String[] zzdts;
    private final double[] zzdtt;
    private final double[] zzdtu;
    private final int[] zzdtv;
    private int zzdtw;

    private zzaxg(zzaxl zzaxl) {
        int size = zzaxl.zzdue.size();
        this.zzdts = (String[]) zzaxl.zzdud.toArray(new String[size]);
        this.zzdtt = zze(zzaxl.zzdue);
        this.zzdtu = zze(zzaxl.zzduf);
        this.zzdtv = new int[size];
        this.zzdtw = 0;
    }

    private static double[] zze(List<Double> list) {
        double[] dArr = new double[list.size()];
        for (int i = 0; i < dArr.length; i++) {
            dArr[i] = list.get(i).doubleValue();
        }
        return dArr;
    }

    public final void zza(double d) {
        this.zzdtw++;
        int i = 0;
        while (true) {
            double[] dArr = this.zzdtu;
            if (i < dArr.length) {
                if (dArr[i] <= d && d < this.zzdtt[i]) {
                    int[] iArr = this.zzdtv;
                    iArr[i] = iArr[i] + 1;
                }
                if (d >= this.zzdtu[i]) {
                    i++;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public final List<zzaxi> zzwz() {
        ArrayList arrayList = new ArrayList(this.zzdts.length);
        int i = 0;
        while (true) {
            String[] strArr = this.zzdts;
            if (i >= strArr.length) {
                return arrayList;
            }
            String str = strArr[i];
            double d = this.zzdtu[i];
            double d2 = this.zzdtt[i];
            int[] iArr = this.zzdtv;
            arrayList.add(new zzaxi(str, d, d2, ((double) iArr[i]) / ((double) this.zzdtw), iArr[i]));
            i++;
        }
    }
}
