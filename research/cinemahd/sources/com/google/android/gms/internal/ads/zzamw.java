package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzamw extends IInterface {
    void zzdl(String str) throws RemoteException;

    void zzx(IObjectWrapper iObjectWrapper) throws RemoteException;
}
