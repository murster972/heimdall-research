package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbbx implements zzno {
    private final String zzcyr;
    private final zzbbs zzeco;

    zzbbx(zzbbs zzbbs, String str) {
        this.zzeco = zzbbs;
        this.zzcyr = str;
    }

    public final zznl zzih() {
        return this.zzeco.zzfg(this.zzcyr);
    }
}
