package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.gms.ads.internal.zzq;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbjb implements zzajv<zzbjf> {
    private final zzpn zzfcc;
    private final Context zzup;
    private final PowerManager zzyw;

    public zzbjb(Context context, zzpn zzpn) {
        this.zzup = context;
        this.zzfcc = zzpn;
        this.zzyw = (PowerManager) context.getSystemService("power");
    }

    /* renamed from: zza */
    public final JSONObject zzj(zzbjf zzbjf) throws JSONException {
        JSONObject jSONObject;
        boolean z;
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        zzpt zzpt = zzbjf.zzfcr;
        if (zzpt == null) {
            jSONObject = new JSONObject();
        } else if (this.zzfcc.zzkl() != null) {
            boolean z2 = zzpt.zzbnz;
            JSONObject jSONObject3 = new JSONObject();
            JSONObject put = jSONObject3.put("afmaVersion", this.zzfcc.zzkk()).put("activeViewJSON", this.zzfcc.zzkl()).put(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, zzbjf.timestamp).put("adFormat", this.zzfcc.zzkj()).put("hashCode", this.zzfcc.zzkm()).put("isMraid", false).put("isStopped", false).put("isPaused", zzbjf.zzfco).put("isNative", this.zzfcc.zzkn());
            if (Build.VERSION.SDK_INT >= 20) {
                z = this.zzyw.isInteractive();
            } else {
                z = this.zzyw.isScreenOn();
            }
            put.put("isScreenOn", z).put("appMuted", zzq.zzkv().zzpf()).put("appVolume", (double) zzq.zzkv().zzpe()).put("deviceVolume", (double) zzawq.zzbe(this.zzup.getApplicationContext()));
            Rect rect = new Rect();
            Display defaultDisplay = ((WindowManager) this.zzup.getSystemService("window")).getDefaultDisplay();
            rect.right = defaultDisplay.getWidth();
            rect.bottom = defaultDisplay.getHeight();
            jSONObject3.put("windowVisibility", zzpt.zzzd).put("isAttachedToWindow", z2).put("viewBox", new JSONObject().put(ViewProps.TOP, zzpt.zzboa.top).put(ViewProps.BOTTOM, zzpt.zzboa.bottom).put(ViewProps.LEFT, zzpt.zzboa.left).put(ViewProps.RIGHT, zzpt.zzboa.right)).put("adBox", new JSONObject().put(ViewProps.TOP, zzpt.zzbob.top).put(ViewProps.BOTTOM, zzpt.zzbob.bottom).put(ViewProps.LEFT, zzpt.zzbob.left).put(ViewProps.RIGHT, zzpt.zzbob.right)).put("globalVisibleBox", new JSONObject().put(ViewProps.TOP, zzpt.zzboc.top).put(ViewProps.BOTTOM, zzpt.zzboc.bottom).put(ViewProps.LEFT, zzpt.zzboc.left).put(ViewProps.RIGHT, zzpt.zzboc.right)).put("globalVisibleBoxVisible", zzpt.zzbod).put("localVisibleBox", new JSONObject().put(ViewProps.TOP, zzpt.zzboe.top).put(ViewProps.BOTTOM, zzpt.zzboe.bottom).put(ViewProps.LEFT, zzpt.zzboe.left).put(ViewProps.RIGHT, zzpt.zzboe.right)).put("localVisibleBoxVisible", zzpt.zzbof).put("hitBox", new JSONObject().put(ViewProps.TOP, zzpt.zzbog.top).put(ViewProps.BOTTOM, zzpt.zzbog.bottom).put(ViewProps.LEFT, zzpt.zzbog.left).put(ViewProps.RIGHT, zzpt.zzbog.right)).put("screenDensity", (double) this.zzup.getResources().getDisplayMetrics().density);
            jSONObject3.put("isVisible", zzbjf.zzbnq);
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzckk)).booleanValue()) {
                JSONArray jSONArray2 = new JSONArray();
                List<Rect> list = zzpt.zzboi;
                if (list != null) {
                    for (Rect next : list) {
                        jSONArray2.put(new JSONObject().put(ViewProps.TOP, next.top).put(ViewProps.BOTTOM, next.bottom).put(ViewProps.LEFT, next.left).put(ViewProps.RIGHT, next.right));
                    }
                }
                jSONObject3.put("scrollableContainerBoxes", jSONArray2);
            }
            if (!TextUtils.isEmpty(zzbjf.zzfcq)) {
                jSONObject3.put("doneReasonCode", "u");
            }
            jSONObject = jSONObject3;
        } else {
            throw new JSONException("Active view Info cannot be null.");
        }
        jSONArray.put(jSONObject);
        jSONObject2.put("units", jSONArray);
        return jSONObject2;
    }
}
