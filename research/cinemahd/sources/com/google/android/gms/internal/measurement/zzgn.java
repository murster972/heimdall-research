package com.google.android.gms.internal.measurement;

import java.io.IOException;

public interface zzgn extends zzgp {
    void zza(zzek zzek) throws IOException;

    zzdv zzbg();

    int zzbl();

    zzgm zzbp();

    zzgm zzbq();
}
