package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.AdMetadataListener;

final /* synthetic */ class zzcyg implements zzcxo {
    static final zzcxo zzgjk = new zzcyg();

    private zzcyg() {
    }

    public final void zzt(Object obj) {
        ((AdMetadataListener) obj).onAdMetadataChanged();
    }
}
