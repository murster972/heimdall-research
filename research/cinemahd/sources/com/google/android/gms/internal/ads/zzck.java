package com.google.android.gms.internal.ads;

import com.facebook.imageutils.JfifUtil;
import com.google.android.gms.internal.ads.zzbs;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

final class zzck {
    private static boolean zzmq = false;
    /* access modifiers changed from: private */
    public static MessageDigest zzmr;
    private static final Object zzms = new Object();
    private static final Object zzmt = new Object();
    static CountDownLatch zzmu = new CountDownLatch(1);

    private static Vector<byte[]> zza(byte[] bArr, int i) {
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        int length = ((bArr.length + JfifUtil.MARKER_FIRST_BYTE) - 1) / JfifUtil.MARKER_FIRST_BYTE;
        Vector<byte[]> vector = new Vector<>();
        int i2 = 0;
        while (i2 < length) {
            int i3 = i2 * JfifUtil.MARKER_FIRST_BYTE;
            try {
                vector.add(Arrays.copyOfRange(bArr, i3, bArr.length - i3 > 255 ? i3 + JfifUtil.MARKER_FIRST_BYTE : bArr.length));
                i2++;
            } catch (IndexOutOfBoundsException unused) {
                return null;
            }
        }
        return vector;
    }

    public static byte[] zzb(byte[] bArr) throws NoSuchAlgorithmException {
        byte[] digest;
        synchronized (zzms) {
            MessageDigest zzbm = zzbm();
            if (zzbm != null) {
                zzbm.reset();
                zzbm.update(bArr);
                digest = zzmr.digest();
            } else {
                throw new NoSuchAlgorithmException("Cannot compute hash");
            }
        }
        return digest;
    }

    static void zzbl() {
        synchronized (zzmt) {
            if (!zzmq) {
                zzmq = true;
                new Thread(new zzcm()).start();
            }
        }
    }

    private static MessageDigest zzbm() {
        boolean z;
        MessageDigest messageDigest;
        zzbl();
        try {
            z = zzmu.await(2, TimeUnit.SECONDS);
        } catch (InterruptedException unused) {
            z = false;
        }
        if (z && (messageDigest = zzmr) != null) {
            return messageDigest;
        }
        return null;
    }

    static String zzj(zzbs.zza zza, String str) throws GeneralSecurityException, UnsupportedEncodingException {
        byte[] bArr;
        byte[] byteArray = zza.toByteArray();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclj)).booleanValue()) {
            Vector<byte[]> zza2 = zza(byteArray, JfifUtil.MARKER_FIRST_BYTE);
            if (zza2 == null || zza2.size() == 0) {
                bArr = zza(zza(zzbs.zza.zzd.PSN_ENCODE_SIZE_FAIL).toByteArray(), str, true);
            } else {
                zzbs.zzf.zza zzbi = zzbs.zzf.zzbi();
                Iterator<byte[]> it2 = zza2.iterator();
                while (it2.hasNext()) {
                    zzbi.zzi(zzdqk.zzu(zza(it2.next(), str, false)));
                }
                zzbi.zzj(zzdqk.zzu(zzb(byteArray)));
                bArr = ((zzbs.zzf) zzbi.zzbaf()).toByteArray();
            }
        } else if (zzeo.zzyh != null) {
            bArr = ((zzbs.zzf) zzbs.zzf.zzbi().zzi(zzdqk.zzu(zzeo.zzyh.zzc(byteArray, str != null ? str.getBytes() : new byte[0]))).zza(zzbz.TINK_HYBRID).zzbaf()).toByteArray();
        } else {
            throw new GeneralSecurityException();
        }
        return zzci.zza(bArr, true);
    }

    private static zzbs.zza zza(zzbs.zza.zzd zzd) {
        zzbs.zza.zzb zzan = zzbs.zza.zzan();
        zzan.zzau((long) zzd.zzae());
        return (zzbs.zza) zzan.zzbaf();
    }

    private static byte[] zza(byte[] bArr, String str, boolean z) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] bArr2;
        int i = z ? 239 : JfifUtil.MARKER_FIRST_BYTE;
        if (bArr.length > i) {
            bArr = zza(zzbs.zza.zzd.PSN_ENCODE_SIZE_FAIL).toByteArray();
        }
        if (bArr.length < i) {
            byte[] bArr3 = new byte[(i - bArr.length)];
            new SecureRandom().nextBytes(bArr3);
            bArr2 = ByteBuffer.allocate(i + 1).put((byte) bArr.length).put(bArr).put(bArr3).array();
        } else {
            bArr2 = ByteBuffer.allocate(i + 1).put((byte) bArr.length).put(bArr).array();
        }
        if (z) {
            bArr2 = ByteBuffer.allocate(256).put(zzb(bArr2)).put(bArr2).array();
        }
        byte[] bArr4 = new byte[256];
        for (zzcr zza : new zzcp().zzun) {
            zza.zza(bArr2, bArr4);
        }
        if (str != null && str.length() > 0) {
            if (str.length() > 32) {
                str = str.substring(0, 32);
            }
            new zzdpx(str.getBytes("UTF-8")).zzt(bArr4);
        }
        return bArr4;
    }
}
