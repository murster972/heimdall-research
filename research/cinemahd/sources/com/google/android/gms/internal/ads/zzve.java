package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.query.QueryData;
import java.util.Random;
import java.util.WeakHashMap;

public final class zzve {
    private static zzve zzcdr = new zzve();
    private final zzayk zzcds;
    private final zzup zzcdt;
    private final String zzcdu;
    private final zzzi zzcdv;
    private final zzzk zzcdw;
    private final zzzj zzcdx;
    private final zzazb zzcdy;
    private final Random zzcdz;
    private final WeakHashMap<QueryData, String> zzcea;

    protected zzve() {
        this(new zzayk(), new zzup(new zzue(), new zzub(), new zzya(), new zzaej(), new zzars(), new zzasw(), new zzaor(), new zzaem()), new zzzi(), new zzzk(), new zzzj(), zzayk.zzxf(), new zzazb(0, 19649000, true), new Random(), new WeakHashMap());
    }

    public static zzayk zzou() {
        return zzcdr.zzcds;
    }

    public static zzup zzov() {
        return zzcdr.zzcdt;
    }

    public static zzzk zzow() {
        return zzcdr.zzcdw;
    }

    public static zzzi zzox() {
        return zzcdr.zzcdv;
    }

    public static zzzj zzoy() {
        return zzcdr.zzcdx;
    }

    public static String zzoz() {
        return zzcdr.zzcdu;
    }

    public static zzazb zzpa() {
        return zzcdr.zzcdy;
    }

    public static Random zzpb() {
        return zzcdr.zzcdz;
    }

    public static WeakHashMap<QueryData, String> zzpc() {
        return zzcdr.zzcea;
    }

    private zzve(zzayk zzayk, zzup zzup, zzzi zzzi, zzzk zzzk, zzzj zzzj, String str, zzazb zzazb, Random random, WeakHashMap<QueryData, String> weakHashMap) {
        this.zzcds = zzayk;
        this.zzcdt = zzup;
        this.zzcdv = zzzi;
        this.zzcdw = zzzk;
        this.zzcdx = zzzj;
        this.zzcdu = str;
        this.zzcdy = zzazb;
        this.zzcdz = random;
        this.zzcea = weakHashMap;
    }
}
