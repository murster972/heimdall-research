package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

public final class zzccr implements zzdxg<zzcdh> {
    private static final zzccr zzfrv = new zzccr();

    public static zzccr zzaky() {
        return zzfrv;
    }

    public final /* synthetic */ Object get() {
        return (zzcdh) zzdxm.zza(new zzcdh(zzso.zza.C0047zza.REQUEST_WILL_RENDER, zzso.zza.C0047zza.REQUEST_DID_RENDER, zzso.zza.C0047zza.REQUEST_FAILED_TO_RENDER), "Cannot return null from a non-@Nullable @Provides method");
    }
}
