package com.google.android.gms.internal.ads;

public final class zzbne implements zzdxg<zzbsu<zzty>> {
    private final zzbnb zzfgs;
    private final zzdxp<zzbnk> zzfgt;

    private zzbne(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        this.zzfgs = zzbnb;
        this.zzfgt = zzdxp;
    }

    public static zzbne zzb(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        return new zzbne(zzbnb, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfgt.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
