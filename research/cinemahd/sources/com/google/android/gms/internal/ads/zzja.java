package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.os.Looper;
import com.google.android.gms.internal.ads.zziz;

@TargetApi(16)
public interface zzja<T extends zziz> {
    zziy<T> zza(Looper looper, zziv zziv);

    void zza(zziy<T> zziy);
}
