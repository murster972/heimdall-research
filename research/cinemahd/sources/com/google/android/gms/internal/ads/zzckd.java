package com.google.android.gms.internal.ads;

final /* synthetic */ class zzckd implements zzbpe {
    private final zzbdi zzehp;

    zzckd(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void onAdImpression() {
        zzbdi zzbdi = this.zzehp;
        if (zzbdi.zzaaa() != null) {
            zzbdi.zzaaa().zzaaz();
        }
    }
}
