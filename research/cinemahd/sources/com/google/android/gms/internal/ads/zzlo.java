package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

public final class zzlo extends zzll {
    public static final Parcelable.Creator<zzlo> CREATOR = new zzln();
    private final String description;
    private final String value;

    public zzlo(String str, String str2, String str3) {
        super(str);
        this.description = null;
        this.value = str3;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && zzlo.class == obj.getClass()) {
            zzlo zzlo = (zzlo) obj;
            return this.zzaez.equals(zzlo.zzaez) && zzoq.zza(this.description, zzlo.description) && zzoq.zza(this.value, zzlo.value);
        }
    }

    public final int hashCode() {
        int hashCode = (this.zzaez.hashCode() + 527) * 31;
        String str = this.description;
        int i = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.value;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.zzaez);
        parcel.writeString(this.description);
        parcel.writeString(this.value);
    }

    zzlo(Parcel parcel) {
        super(parcel.readString());
        this.description = parcel.readString();
        this.value = parcel.readString();
    }
}
