package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzbpl implements zzbrn {
    private final Context zzcri;

    zzbpl(Context context) {
        this.zzcri = context;
    }

    public final void zzp(Object obj) {
        ((zzbph) obj).zzbx(this.zzcri);
    }
}
