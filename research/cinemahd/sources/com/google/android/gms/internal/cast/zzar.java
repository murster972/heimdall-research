package com.google.android.gms.internal.cast;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import androidx.appcompat.R$attr;
import androidx.core.graphics.drawable.DrawableCompat;
import com.google.android.gms.cast.framework.R$color;
import com.google.android.gms.cast.framework.R$dimen;
import com.google.android.gms.cast.framework.R$drawable;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.R$style;
import com.google.android.gms.cast.framework.R$styleable;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.common.util.PlatformVersion;

public final class zzar {
    private final Activity zzij;
    private final int zzrl;
    private final int zzrm;
    private final int zzrn;
    private final int zzro;
    private final SeekBar zzrp;
    private final zzas zzrq;

    public zzar(Activity activity, UIMediaController uIMediaController, SeekBar seekBar) {
        this.zzij = activity;
        this.zzrp = seekBar;
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(new int[]{R$attr.colorControlActivated});
        this.zzro = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        ColorStateList colorStateList = null;
        TypedArray obtainStyledAttributes2 = activity.obtainStyledAttributes((AttributeSet) null, R$styleable.CastExpandedController, com.google.android.gms.cast.framework.R$attr.castExpandedControllerStyle, R$style.CastExpandedController);
        this.zzrl = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castSeekBarProgressDrawable, 0);
        this.zzrm = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castSeekBarThumbDrawable, 0);
        this.zzrn = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castSeekBarProgressAndThumbColor, this.zzro);
        obtainStyledAttributes2.recycle();
        Drawable drawable = this.zzij.getResources().getDrawable(this.zzrl);
        if (drawable != null) {
            if (this.zzrl == R$drawable.cast_expanded_controller_seekbar_track) {
                colorStateList = zzdd();
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                Drawable i = DrawableCompat.i(layerDrawable.findDrawableByLayerId(16908301));
                DrawableCompat.a(i, colorStateList);
                layerDrawable.setDrawableByLayerId(16908301, i);
                layerDrawable.findDrawableByLayerId(16908288).setColorFilter(this.zzij.getResources().getColor(R$color.cast_expanded_controller_seek_bar_progress_background_tint_color), PorterDuff.Mode.SRC_IN);
            }
            this.zzrp.setProgressDrawable(drawable);
        }
        Drawable drawable2 = this.zzij.getResources().getDrawable(this.zzrm);
        if (drawable2 != null) {
            if (this.zzrm == R$drawable.cast_expanded_controller_seekbar_thumb) {
                colorStateList = colorStateList == null ? zzdd() : colorStateList;
                drawable2 = DrawableCompat.i(drawable2);
                DrawableCompat.a(drawable2, colorStateList);
            }
            this.zzrp.setThumb(drawable2);
        }
        if (PlatformVersion.h()) {
            this.zzrp.setSplitTrack(false);
        }
        RelativeLayout relativeLayout = (RelativeLayout) activity.findViewById(R$id.controllers);
        zzas zzas = new zzas(this.zzij, this.zzrp, uIMediaController.d());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(0, R$id.end_text);
        layoutParams.addRule(1, R$id.start_text);
        layoutParams.addRule(6, R$id.seek_bar);
        layoutParams.addRule(7, R$id.seek_bar);
        layoutParams.addRule(5, R$id.seek_bar);
        layoutParams.addRule(8, R$id.seek_bar);
        zzas.setLayoutParams(layoutParams);
        if (PlatformVersion.d()) {
            zzas.setPaddingRelative(this.zzrp.getPaddingStart(), this.zzrp.getPaddingTop(), this.zzrp.getPaddingEnd(), this.zzrp.getPaddingBottom());
        } else {
            zzas.setPadding(this.zzrp.getPaddingLeft(), this.zzrp.getPaddingTop(), this.zzrp.getPaddingRight(), this.zzrp.getPaddingBottom());
        }
        zzas.setContentDescription(this.zzij.getResources().getString(R$string.cast_seek_bar));
        zzas.setBackgroundColor(0);
        relativeLayout.addView(zzas);
        this.zzrq = zzas;
        uIMediaController.a(seekBar, 1000, (zzbn) this.zzrq);
    }

    private final ColorStateList zzdd() {
        int color = this.zzij.getResources().getColor(this.zzrn);
        TypedValue typedValue = new TypedValue();
        this.zzij.getResources().getValue(R$dimen.cast_expanded_controller_seekbar_disabled_alpha, typedValue, true);
        int[] iArr = {color, Color.argb((int) (typedValue.getFloat() * ((float) Color.alpha(color))), Color.red(color), Color.green(color), Color.blue(color))};
        return new ColorStateList(new int[][]{new int[]{16842910}, new int[]{-16842910}}, iArr);
    }
}
