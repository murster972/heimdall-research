package com.google.android.gms.internal.ads;

public final class zzbls {
    private final zzaea zzfex;
    private final Runnable zzfey;

    public zzbls(zzaea zzaea, Runnable runnable) {
        this.zzfex = zzaea;
        this.zzfey = runnable;
    }

    public final zzaea zzagm() {
        return this.zzfex;
    }

    public final Runnable zzagn() {
        return this.zzfey;
    }
}
