package com.google.android.gms.internal.ads;

import java.util.Comparator;

final class zzmv implements Comparator<zzgw> {
    private zzmv() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return ((zzgw) obj2).zzafa - ((zzgw) obj).zzafa;
    }
}
