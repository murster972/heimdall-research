package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class zzawe extends BroadcastReceiver {
    private final /* synthetic */ zzawb zzdsw;

    private zzawe(zzawb zzawb) {
        this.zzdsw = zzawb;
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            boolean unused = this.zzdsw.zzxy = true;
        } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            boolean unused2 = this.zzdsw.zzxy = false;
        }
    }

    /* synthetic */ zzawe(zzawb zzawb, zzawa zzawa) {
        this(zzawb);
    }
}
