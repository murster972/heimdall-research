package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import javax.net.ssl.SSLSocketFactory;

final class zzbbo extends SSLSocketFactory {
    private SSLSocketFactory zzebo = ((SSLSocketFactory) SSLSocketFactory.getDefault());
    private final /* synthetic */ zzbbp zzebp;

    zzbbo(zzbbp zzbbp) {
        this.zzebp = zzbbp;
    }

    private final Socket zza(Socket socket) throws SocketException {
        if (this.zzebp.zzebr > 0) {
            socket.setReceiveBufferSize(this.zzebp.zzebr);
        }
        this.zzebp.zzb(socket);
        return socket;
    }

    public final Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return zza(this.zzebo.createSocket(socket, str, i, z));
    }

    public final String[] getDefaultCipherSuites() {
        return this.zzebo.getDefaultCipherSuites();
    }

    public final String[] getSupportedCipherSuites() {
        return this.zzebo.getSupportedCipherSuites();
    }

    public final Socket createSocket(String str, int i) throws IOException {
        return zza(this.zzebo.createSocket(str, i));
    }

    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return zza(this.zzebo.createSocket(str, i, inetAddress, i2));
    }

    public final Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return zza(this.zzebo.createSocket(inetAddress, i));
    }

    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return zza(this.zzebo.createSocket(inetAddress, i, inetAddress2, i2));
    }
}
