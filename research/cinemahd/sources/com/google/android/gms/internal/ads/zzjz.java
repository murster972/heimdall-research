package com.google.android.gms.internal.ads;

import com.facebook.common.util.UriUtil;
import com.facebook.imageutils.JfifUtil;
import com.unity3d.ads.metadata.MediationMetaData;

class zzjz {
    public static final int zzaqv = zzoq.zzbn("ftyp");
    public static final int zzaqw = zzoq.zzbn("avc1");
    public static final int zzaqx = zzoq.zzbn("avc3");
    public static final int zzaqy = zzoq.zzbn("hvc1");
    public static final int zzaqz = zzoq.zzbn("hev1");
    public static final int zzara = zzoq.zzbn("s263");
    public static final int zzarb = zzoq.zzbn("d263");
    private static final int zzarc = zzoq.zzbn("mdat");
    public static final int zzard = zzoq.zzbn("mp4a");
    public static final int zzare = zzoq.zzbn(".mp3");
    public static final int zzarf = zzoq.zzbn("wave");
    public static final int zzarg = zzoq.zzbn("lpcm");
    public static final int zzarh = zzoq.zzbn("sowt");
    public static final int zzari = zzoq.zzbn("ac-3");
    public static final int zzarj = zzoq.zzbn("dac3");
    public static final int zzark = zzoq.zzbn("ec-3");
    public static final int zzarl = zzoq.zzbn("dec3");
    public static final int zzarm = zzoq.zzbn("dtsc");
    public static final int zzarn = zzoq.zzbn("dtsh");
    public static final int zzaro = zzoq.zzbn("dtsl");
    public static final int zzarp = zzoq.zzbn("dtse");
    public static final int zzarq = zzoq.zzbn("ddts");
    private static final int zzarr = zzoq.zzbn("tfdt");
    private static final int zzars = zzoq.zzbn("tfhd");
    private static final int zzart = zzoq.zzbn("trex");
    private static final int zzaru = zzoq.zzbn("trun");
    private static final int zzarv = zzoq.zzbn("sidx");
    public static final int zzarw = zzoq.zzbn("moov");
    public static final int zzarx = zzoq.zzbn("mvhd");
    public static final int zzary = zzoq.zzbn("trak");
    public static final int zzarz = zzoq.zzbn("mdia");
    public static final int zzasa = zzoq.zzbn("minf");
    public static final int zzasb = zzoq.zzbn("stbl");
    public static final int zzasc = zzoq.zzbn("avcC");
    public static final int zzasd = zzoq.zzbn("hvcC");
    public static final int zzase = zzoq.zzbn("esds");
    public static final int zzasf = zzoq.zzbn("moof");
    private static final int zzasg = zzoq.zzbn("traf");
    public static final int zzash = zzoq.zzbn("mvex");
    private static final int zzasi = zzoq.zzbn("mehd");
    public static final int zzasj = zzoq.zzbn("tkhd");
    public static final int zzask = zzoq.zzbn("edts");
    public static final int zzasl = zzoq.zzbn("elst");
    public static final int zzasm = zzoq.zzbn("mdhd");
    public static final int zzasn = zzoq.zzbn("hdlr");
    public static final int zzaso = zzoq.zzbn("stsd");
    private static final int zzasp = zzoq.zzbn("pssh");
    public static final int zzasq = zzoq.zzbn("sinf");
    public static final int zzasr = zzoq.zzbn("schm");
    public static final int zzass = zzoq.zzbn("schi");
    public static final int zzast = zzoq.zzbn("tenc");
    public static final int zzasu = zzoq.zzbn("encv");
    public static final int zzasv = zzoq.zzbn("enca");
    public static final int zzasw = zzoq.zzbn("frma");
    private static final int zzasx = zzoq.zzbn("saiz");
    private static final int zzasy = zzoq.zzbn("saio");
    private static final int zzasz = zzoq.zzbn("sbgp");
    private static final int zzata = zzoq.zzbn("sgpd");
    private static final int zzatb = zzoq.zzbn("uuid");
    private static final int zzatc = zzoq.zzbn("senc");
    public static final int zzatd = zzoq.zzbn("pasp");
    public static final int zzate = zzoq.zzbn("TTML");
    private static final int zzatf = zzoq.zzbn("vmhd");
    public static final int zzatg = zzoq.zzbn("mp4v");
    public static final int zzath = zzoq.zzbn("stts");
    public static final int zzati = zzoq.zzbn("stss");
    public static final int zzatj = zzoq.zzbn("ctts");
    public static final int zzatk = zzoq.zzbn("stsc");
    public static final int zzatl = zzoq.zzbn("stsz");
    public static final int zzatm = zzoq.zzbn("stz2");
    public static final int zzatn = zzoq.zzbn("stco");
    public static final int zzato = zzoq.zzbn("co64");
    public static final int zzatp = zzoq.zzbn("tx3g");
    public static final int zzatq = zzoq.zzbn("wvtt");
    public static final int zzatr = zzoq.zzbn("stpp");
    public static final int zzats = zzoq.zzbn("c608");
    public static final int zzatt = zzoq.zzbn("samr");
    public static final int zzatu = zzoq.zzbn("sawb");
    public static final int zzatv = zzoq.zzbn("udta");
    public static final int zzatw = zzoq.zzbn("meta");
    public static final int zzatx = zzoq.zzbn("ilst");
    public static final int zzaty = zzoq.zzbn("mean");
    public static final int zzatz = zzoq.zzbn(MediationMetaData.KEY_NAME);
    public static final int zzaua = zzoq.zzbn(UriUtil.DATA_SCHEME);
    private static final int zzaub = zzoq.zzbn("emsg");
    public static final int zzauc = zzoq.zzbn("st3d");
    public static final int zzaud = zzoq.zzbn("sv3d");
    public static final int zzaue = zzoq.zzbn("proj");
    public static final int zzauf = zzoq.zzbn("vp08");
    public static final int zzaug = zzoq.zzbn("vp09");
    public static final int zzauh = zzoq.zzbn("vpcC");
    public static final int zzaui = zzoq.zzbn("camm");
    public static final int zzauj = zzoq.zzbn("alac");
    public final int type;

    public zzjz(int i) {
        this.type = i;
    }

    public static int zzak(int i) {
        return (i >> 24) & JfifUtil.MARKER_FIRST_BYTE;
    }

    public static int zzal(int i) {
        return i & 16777215;
    }

    public static String zzam(int i) {
        StringBuilder sb = new StringBuilder(4);
        sb.append((char) (i >>> 24));
        sb.append((char) ((i >> 16) & JfifUtil.MARKER_FIRST_BYTE));
        sb.append((char) ((i >> 8) & JfifUtil.MARKER_FIRST_BYTE));
        sb.append((char) (i & JfifUtil.MARKER_FIRST_BYTE));
        return sb.toString();
    }

    public String toString() {
        return zzam(this.type);
    }
}
