package com.google.android.gms.internal.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzct extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzct> CREATOR = new zzcu();
    private String zzxs;

    zzct(String str) {
        this.zzxs = str;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzct)) {
            return false;
        }
        return zzdk.zza(this.zzxs, ((zzct) obj).zzxs);
    }

    public final int hashCode() {
        return Objects.a(this.zzxs);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.zzxs, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public final String zzep() {
        return this.zzxs;
    }

    public zzct() {
        this((String) null);
    }
}
