package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcgo implements zzdxg<zzcgm> {
    private final zzdxp<Context> zzejv;

    public zzcgo(zzdxp<Context> zzdxp) {
        this.zzejv = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcgm(this.zzejv.get());
    }
}
