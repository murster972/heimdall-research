package com.google.android.gms.internal.ads;

import java.util.Arrays;

final class zzdqo implements zzdqq {
    private zzdqo() {
    }

    public final byte[] zzj(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }

    /* synthetic */ zzdqo(zzdqj zzdqj) {
        this();
    }
}
