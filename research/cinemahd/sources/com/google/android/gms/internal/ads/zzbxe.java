package com.google.android.gms.internal.ads;

public final class zzbxe {
    private final zzbws zzfkc;

    public zzbxe(zzbws zzbws) {
        this.zzfkc = zzbws;
    }

    public final zzbws zzajx() {
        return this.zzfkc;
    }
}
