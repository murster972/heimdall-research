package com.google.android.gms.internal.cast;

import android.widget.TextView;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public final class zzbw extends UIController implements RemoteMediaClient.ProgressListener {
    private final zzbh zzrw;
    private final TextView zzvo;

    public zzbw(TextView textView, zzbh zzbh) {
        this.zzvo = textView;
        this.zzrw = zzbh;
        zzdj();
    }

    private final void zzdj() {
        RemoteMediaClient remoteMediaClient = getRemoteMediaClient();
        if (remoteMediaClient == null || !remoteMediaClient.k()) {
            TextView textView = this.zzvo;
            textView.setText(textView.getContext().getString(R$string.cast_invalid_stream_duration_text));
        } else if (!remoteMediaClient.m() || this.zzrw.zzdw() != null) {
            this.zzvo.setVisibility(0);
            TextView textView2 = this.zzvo;
            zzbh zzbh = this.zzrw;
            textView2.setText(zzbh.zze(zzbh.zzdu()));
        } else {
            this.zzvo.setVisibility(8);
        }
    }

    public final void onMediaStatusUpdated() {
        zzdj();
    }

    public final void onProgressUpdated(long j, long j2) {
        zzdj();
    }

    public final void onSessionConnected(CastSession castSession) {
        super.onSessionConnected(castSession);
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this, 1000);
        }
        zzdj();
    }

    public final void onSessionEnded() {
        if (getRemoteMediaClient() != null) {
            getRemoteMediaClient().a((RemoteMediaClient.ProgressListener) this);
        }
        super.onSessionEnded();
        zzdj();
    }
}
