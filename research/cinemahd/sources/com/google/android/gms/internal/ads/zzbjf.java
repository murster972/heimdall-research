package com.google.android.gms.internal.ads;

public final class zzbjf {
    public long timestamp = 0;
    public boolean zzbnq = false;
    public boolean zzfco = false;
    private boolean zzfcp = false;
    public String zzfcq;
    public zzpt zzfcr = null;

    protected zzbjf() {
    }
}
