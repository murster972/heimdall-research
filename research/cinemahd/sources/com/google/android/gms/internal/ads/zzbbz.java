package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbbz implements zzno {
    private final byte[] zzdlq;
    private final zzno zzecr;

    zzbbz(zzno zzno, byte[] bArr) {
        this.zzecr = zzno;
        this.zzdlq = bArr;
    }

    public final zznl zzih() {
        zzno zzno = this.zzecr;
        byte[] bArr = this.zzdlq;
        return new zzbcd(new zznm(bArr), bArr.length, zzno.zzih());
    }
}
