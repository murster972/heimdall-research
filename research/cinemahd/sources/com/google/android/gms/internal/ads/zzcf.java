package com.google.android.gms.internal.ads;

final class zzcf implements zzdsa {
    static final zzdsa zzew = new zzcf();

    private zzcf() {
    }

    public final boolean zzf(int i) {
        return zzcd.zzj(i) != null;
    }
}
