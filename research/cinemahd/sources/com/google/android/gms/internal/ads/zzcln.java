package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.internal.ads.zzbod;

public final class zzcln extends zzclk<zzcbb> {
    private final zzbrm zzers;
    private final zzbfx zzfzz;
    private final zzbod.zza zzgaa;

    public zzcln(zzbfx zzbfx, zzbod.zza zza, zzbrm zzbrm) {
        this.zzfzz = zzbfx;
        this.zzgaa = zza;
        this.zzers = zzbrm;
    }

    /* access modifiers changed from: protected */
    public final zzdhe<zzcbb> zza(zzczu zzczu, Bundle bundle) {
        return this.zzfzz.zzacm().zze(this.zzgaa.zza(zzczu).zze(bundle).zzahh()).zze(this.zzers).zzaes().zzadc().zzaha();
    }
}
