package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzdbj implements Parcelable.Creator<zzdbe> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 1:
                    i = SafeParcelReader.w(parcel, a2);
                    break;
                case 2:
                    i2 = SafeParcelReader.w(parcel, a2);
                    break;
                case 3:
                    i3 = SafeParcelReader.w(parcel, a2);
                    break;
                case 4:
                    i4 = SafeParcelReader.w(parcel, a2);
                    break;
                case 5:
                    str = SafeParcelReader.o(parcel, a2);
                    break;
                case 6:
                    i5 = SafeParcelReader.w(parcel, a2);
                    break;
                case 7:
                    i6 = SafeParcelReader.w(parcel, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzdbe(i, i2, i3, i4, str, i5, i6);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzdbe[i];
    }
}
