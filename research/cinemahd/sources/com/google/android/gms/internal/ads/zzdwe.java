package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;

final class zzdwe implements zzdsa {
    static final zzdsa zzew = new zzdwe();

    private zzdwe() {
    }

    public final boolean zzf(int i) {
        return zzdvx.zzb.zzg.zzhf(i) != null;
    }
}
