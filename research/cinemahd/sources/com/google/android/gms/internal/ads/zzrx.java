package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.io.InputStream;

public final class zzrx extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzrx> CREATOR = new zzrw();
    private ParcelFileDescriptor zzbri;

    public zzrx() {
        this((ParcelFileDescriptor) null);
    }

    private final synchronized ParcelFileDescriptor zzmr() {
        return this.zzbri;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, (Parcelable) zzmr(), i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public final synchronized boolean zzmp() {
        return this.zzbri != null;
    }

    public final synchronized InputStream zzmq() {
        if (this.zzbri == null) {
            return null;
        }
        ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream(this.zzbri);
        this.zzbri = null;
        return autoCloseInputStream;
    }

    public zzrx(ParcelFileDescriptor parcelFileDescriptor) {
        this.zzbri = parcelFileDescriptor;
    }
}
