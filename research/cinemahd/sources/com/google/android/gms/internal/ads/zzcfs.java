package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;

public final class zzcfs implements zzdxg<ApplicationInfo> {
    private final zzdxp<Context> zzejv;

    private zzcfs(zzdxp<Context> zzdxp) {
        this.zzejv = zzdxp;
    }

    public static zzcfs zzaa(zzdxp<Context> zzdxp) {
        return new zzcfs(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (ApplicationInfo) zzdxm.zza(this.zzejv.get().getApplicationInfo(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
