package com.google.android.gms.internal.ads;

public final class zzbmc implements zzps {
    private final zzats zzffo;

    public zzbmc(zzats zzats) {
        this.zzffo = zzats;
    }

    public final void zza(zzpt zzpt) {
        this.zzffo.zza(zzpt);
    }
}
