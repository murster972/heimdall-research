package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

final class zzdhf implements Executor {
    boolean zzgxb = true;
    private final /* synthetic */ Executor zzgxc;
    private final /* synthetic */ zzdfs zzgxd;

    zzdhf(Executor executor, zzdfs zzdfs) {
        this.zzgxc = executor;
        this.zzgxd = zzdfs;
    }

    public final void execute(Runnable runnable) {
        try {
            this.zzgxc.execute(new zzdhi(this, runnable));
        } catch (RejectedExecutionException e) {
            if (this.zzgxb) {
                this.zzgxd.setException(e);
            }
        }
    }
}
