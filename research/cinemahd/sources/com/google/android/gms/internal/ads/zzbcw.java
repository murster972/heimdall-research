package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class zzbcw {
    private final ArrayList<zzns> zzedp = new ArrayList<>();
    private long zzedq;

    zzbcw() {
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzns zzns) {
        this.zzedp.add(zzns);
    }

    /* access modifiers changed from: package-private */
    public final long zzzq() {
        Iterator<zzns> it2 = this.zzedp.iterator();
        while (it2.hasNext()) {
            Map<String, List<String>> responseHeaders = it2.next().getResponseHeaders();
            if (responseHeaders != null) {
                for (Map.Entry next : responseHeaders.entrySet()) {
                    try {
                        if ("content-length".equalsIgnoreCase((String) next.getKey())) {
                            this.zzedq = Math.max(this.zzedq, Long.parseLong((String) ((List) next.getValue()).get(0)));
                        }
                    } catch (RuntimeException unused) {
                    }
                }
                it2.remove();
            }
        }
        return this.zzedq;
    }
}
