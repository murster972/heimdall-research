package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.MuteThisAdListener;

public final class zzws extends zzwq {
    private final MuteThisAdListener zzceb;

    public zzws(MuteThisAdListener muteThisAdListener) {
        this.zzceb = muteThisAdListener;
    }

    public final void onAdMuted() {
        this.zzceb.onAdMuted();
    }
}
