package com.google.android.gms.internal.ads;

public final class zznc {
    private final int length;
    private final int[] zzbdy;
    private final zzmr[] zzbdz;
    private final int[] zzbea;
    private final int[][][] zzbeb;
    private final zzmr zzbec;

    zznc(int[] iArr, zzmr[] zzmrArr, int[] iArr2, int[][][] iArr3, zzmr zzmr) {
        this.zzbdy = iArr;
        this.zzbdz = zzmrArr;
        this.zzbeb = iArr3;
        this.zzbea = iArr2;
        this.zzbec = zzmr;
        this.length = zzmrArr.length;
    }
}
