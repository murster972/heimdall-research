package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class zzcup implements zzcub<zzcum> {
    private final Executor executor;
    private final ScheduledExecutorService zzffx;
    private final zzava zzghl;
    private final Context zzup;

    public zzcup(zzava zzava, Context context, ScheduledExecutorService scheduledExecutorService, Executor executor2) {
        this.zzghl = zzava;
        this.zzup = context;
        this.zzffx = scheduledExecutorService;
        this.executor = executor2;
    }

    public final zzdhe<zzcum> zzanc() {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcjd)).booleanValue()) {
            return zzdgs.zzk(new Exception("Did not ad Ad ID into query param."));
        }
        return zzdgn.zze(this.zzghl.zzak(this.zzup)).zza(zzcuo.zzdoq, this.executor).zza(((Long) zzve.zzoy().zzd(zzzn.zzcje)).longValue(), TimeUnit.MILLISECONDS, this.zzffx).zza(Throwable.class, new zzcur(this), this.executor);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcum zze(Throwable th) {
        zzve.zzou();
        return new zzcum((AdvertisingIdClient.Info) null, zzayk.zzbj(this.zzup));
    }
}
