package com.google.android.gms.internal.ads;

import java.io.InputStream;

final /* synthetic */ class zzchg implements zzdgf {
    private final zzcgw zzfwg;

    zzchg(zzcgw zzcgw) {
        this.zzfwg = zzcgw;
    }

    public final zzdhe zzf(Object obj) {
        return zzdgs.zzaj(zzdae.zze((InputStream) obj));
    }
}
