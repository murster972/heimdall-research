package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzctq implements zzdxg<zzcto> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzatv> zzfay;
    private final zzdxp<zzdhd> zzfcv;

    private zzctq(zzdxp<zzatv> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<Context> zzdxp3) {
        this.zzfay = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzejv = zzdxp3;
    }

    public static zzctq zzp(zzdxp<zzatv> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<Context> zzdxp3) {
        return new zzctq(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzcto(this.zzfay.get(), this.zzfcv.get(), this.zzejv.get());
    }
}
