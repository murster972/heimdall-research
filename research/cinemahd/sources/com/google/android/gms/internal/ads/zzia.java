package com.google.android.gms.internal.ads;

public final class zzia extends Exception {
    public zzia(Throwable th) {
        super(th);
    }

    public zzia(String str) {
        super(str);
    }
}
