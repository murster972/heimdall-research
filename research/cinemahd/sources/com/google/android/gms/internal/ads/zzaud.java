package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.dynamic.ObjectWrapper;

final /* synthetic */ class zzaud implements zzaul {
    private final Context zzcri;
    private final String zzcyr;

    zzaud(Context context, String str) {
        this.zzcri = context;
        this.zzcyr = str;
    }

    public final void zza(zzbfq zzbfq) {
        Context context = this.zzcri;
        zzbfq.zzb(ObjectWrapper.a(context), this.zzcyr, context.getPackageName());
    }
}
