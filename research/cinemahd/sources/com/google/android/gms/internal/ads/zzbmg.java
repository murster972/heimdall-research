package com.google.android.gms.internal.ads;

public final class zzbmg {
    /* access modifiers changed from: private */
    public final zzczt zzfbl;
    /* access modifiers changed from: private */
    public final zzczl zzffc;
    /* access modifiers changed from: private */
    public final zzbpg zzffp;
    /* access modifiers changed from: private */
    public final zzbpw zzffq;
    /* access modifiers changed from: private */
    public final zzcxq zzffr;
    /* access modifiers changed from: private */
    public final zzbom zzffs;

    public zzbmg(zzczt zzczt, zzczl zzczl, zzbpg zzbpg, zzbpw zzbpw, zzcxq zzcxq, zzbom zzbom) {
        this.zzfbl = zzczt;
        this.zzffc = zzczl;
        this.zzffp = zzbpg;
        this.zzffq = zzbpw;
        this.zzffr = zzcxq;
        this.zzffs = zzbom;
    }
}
