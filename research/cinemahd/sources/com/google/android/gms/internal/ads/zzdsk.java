package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map;

final class zzdsk<K> implements Iterator<Map.Entry<K, Object>> {
    private Iterator<Map.Entry<K, Object>> zzhoe;

    public zzdsk(Iterator<Map.Entry<K, Object>> it2) {
        this.zzhoe = it2;
    }

    public final boolean hasNext() {
        return this.zzhoe.hasNext();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.zzhoe.next();
        return next.getValue() instanceof zzdsf ? new zzdsh(next) : next;
    }

    public final void remove() {
        this.zzhoe.remove();
    }
}
