package com.google.android.gms.internal.ads;

import com.facebook.ads.AudienceNetworkActivity;

final class zzaio implements Runnable {
    private final /* synthetic */ String zzczd;
    private final /* synthetic */ zzaih zzcze;

    zzaio(zzaih zzaih, String str) {
        this.zzcze = zzaih;
        this.zzczd = str;
    }

    public final void run() {
        this.zzcze.zzcza.loadData(this.zzczd, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8");
    }
}
