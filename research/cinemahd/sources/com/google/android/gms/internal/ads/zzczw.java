package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public final class zzczw {
    /* access modifiers changed from: private */
    public boolean zzbkh;
    /* access modifiers changed from: private */
    public zzuj zzblm;
    /* access modifiers changed from: private */
    public zzaby zzddz;
    /* access modifiers changed from: private */
    public zzagz zzdkf;
    /* access modifiers changed from: private */
    public int zzgdu = 1;
    /* access modifiers changed from: private */
    public zzwi zzgmj;
    /* access modifiers changed from: private */
    public zzyw zzgmk;
    /* access modifiers changed from: private */
    public zzug zzgml;
    /* access modifiers changed from: private */
    public String zzgmm;
    /* access modifiers changed from: private */
    public ArrayList<String> zzgmn;
    /* access modifiers changed from: private */
    public ArrayList<String> zzgmo;
    /* access modifiers changed from: private */
    public zzuo zzgmp;
    /* access modifiers changed from: private */
    public PublisherAdViewOptions zzgmq;
    /* access modifiers changed from: private */
    public zzwc zzgmr;
    public final Set<String> zzgms = new HashSet();

    public final zzug zzaoq() {
        return this.zzgml;
    }

    public final String zzaor() {
        return this.zzgmm;
    }

    public final zzczu zzaos() {
        Preconditions.a(this.zzgmm, (Object) "ad unit must not be null");
        Preconditions.a(this.zzblm, (Object) "ad size must not be null");
        Preconditions.a(this.zzgml, (Object) "ad request must not be null");
        return new zzczu(this);
    }

    public final zzczw zzb(ArrayList<String> arrayList) {
        this.zzgmn = arrayList;
        return this;
    }

    public final zzczw zzbm(boolean z) {
        this.zzbkh = z;
        return this;
    }

    public final zzczw zzc(zzwi zzwi) {
        this.zzgmj = zzwi;
        return this;
    }

    public final zzczw zzd(zzuj zzuj) {
        this.zzblm = zzuj;
        return this;
    }

    public final zzczw zzdl(int i) {
        this.zzgdu = i;
        return this;
    }

    public final zzczw zzg(zzug zzug) {
        this.zzgml = zzug;
        return this;
    }

    public final zzczw zzgk(String str) {
        this.zzgmm = str;
        return this;
    }

    public final zzuj zzjz() {
        return this.zzblm;
    }

    public final zzczw zzb(zzaby zzaby) {
        this.zzddz = zzaby;
        return this;
    }

    public final zzczw zzc(zzyw zzyw) {
        this.zzgmk = zzyw;
        return this;
    }

    public final zzczw zzb(zzuo zzuo) {
        this.zzgmp = zzuo;
        return this;
    }

    public final zzczw zzc(ArrayList<String> arrayList) {
        this.zzgmo = arrayList;
        return this;
    }

    public final zzczw zzb(zzagz zzagz) {
        this.zzdkf = zzagz;
        this.zzgmk = new zzyw(false, true, false);
        return this;
    }

    public final zzczw zzb(PublisherAdViewOptions publisherAdViewOptions) {
        this.zzgmq = publisherAdViewOptions;
        if (publisherAdViewOptions != null) {
            this.zzbkh = publisherAdViewOptions.getManualImpressionsEnabled();
            this.zzgmr = publisherAdViewOptions.zzjm();
        }
        return this;
    }
}
