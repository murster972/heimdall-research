package com.google.android.gms.internal.ads;

final class zzdsv<K, V> {
    public final V zzcfu;
    public final zzdvf zzhoq;
    public final K zzhor;
    public final zzdvf zzhos;
}
