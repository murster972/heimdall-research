package com.google.android.gms.internal.ads;

import java.util.concurrent.atomic.AtomicReference;

public final class zzcwz implements zzbow, zzbqg, zzcxq {
    private final zzdax zzgjf;
    private final AtomicReference<zzrg> zzgjg = new AtomicReference<>();
    private final AtomicReference<zzrl> zzgjh = new AtomicReference<>();
    private final AtomicReference<zzbqg> zzgji = new AtomicReference<>();
    private zzcwz zzgjj = null;

    public zzcwz(zzdax zzdax) {
        this.zzgjf = zzdax;
    }

    public static zzcwz zza(zzcwz zzcwz) {
        zzcwz zzcwz2 = new zzcwz(zzcwz.zzgjf);
        zzcwz2.zzb((zzcxq) zzcwz);
        return zzcwz2;
    }

    public final void onAdClosed() {
        zzcwz zzcwz = this;
        while (true) {
            zzcwz zzcwz2 = zzcwz.zzgjj;
            if (zzcwz2 != null) {
                zzcwz = zzcwz2;
            } else {
                zzcwz.zzgjf.onAdClosed();
                zzcxp.zza(zzcwz.zzgjh, zzcxa.zzgjk);
                return;
            }
        }
    }

    public final void onAdFailedToLoad(int i) {
        zzcwz zzcwz = this;
        while (true) {
            zzcwz zzcwz2 = zzcwz.zzgjj;
            if (zzcwz2 != null) {
                zzcwz = zzcwz2;
            } else {
                zzcxp.zza(zzcwz.zzgjg, new zzcxb(i));
                return;
            }
        }
    }

    public final void zzahi() {
        zzcwz zzcwz = this;
        while (true) {
            zzcwz zzcwz2 = zzcwz.zzgjj;
            if (zzcwz2 != null) {
                zzcwz = zzcwz2;
            } else {
                zzcxp.zza(zzcwz.zzgji, zzcxd.zzgjk);
                return;
            }
        }
    }

    public final void zzb(zzrg zzrg) {
        this.zzgjg.set(zzrg);
    }

    public final void zzb(zzrl zzrl) {
        this.zzgjh.set(zzrl);
    }

    public final void zza(zzbqg zzbqg) {
        this.zzgji.set(zzbqg);
    }

    public final void zzb(zzrf zzrf) {
        zzcwz zzcwz = this;
        while (true) {
            zzcwz zzcwz2 = zzcwz.zzgjj;
            if (zzcwz2 != null) {
                zzcwz = zzcwz2;
            } else {
                zzcxp.zza(zzcwz.zzgjg, new zzcwy(zzrf));
                return;
            }
        }
    }

    public final void zzb(zzcxq zzcxq) {
        this.zzgjj = (zzcwz) zzcxq;
    }
}
