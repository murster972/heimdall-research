package com.google.android.gms.internal.ads;

final class zzcon implements zzdgt<zzbkk> {
    private final /* synthetic */ zzblg zzgcu;
    private final /* synthetic */ zzcoo zzgcv;

    zzcon(zzcoo zzcoo, zzblg zzblg) {
        this.zzgcv = zzcoo;
        this.zzgcu = zzblg;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzbkk zzbkk = (zzbkk) obj;
        synchronized (this.zzgcv) {
            zzdhe unused = this.zzgcv.zzgdb = null;
            if (this.zzgcv.zzgcp != null) {
                this.zzgcv.zzgcp.destroy();
            }
            zzbkk unused2 = this.zzgcv.zzgcp = zzbkk;
            this.zzgcv.zzfdu.removeAllViews();
            this.zzgcv.zzfdu.addView(zzbkk.zzaga());
            zzbkk.zzagf();
            this.zzgcv.zzgcz.zzdg(zzbkk.zzage());
        }
    }

    public final void zzb(Throwable th) {
        synchronized (this.zzgcv) {
            zzdhe unused = this.zzgcv.zzgdb = null;
            this.zzgcu.zzadd().onAdFailedToLoad(zzcfb.zzd(th));
            this.zzgcv.zzgcz.zzdg(60);
            zzdad.zzc(th, "BannerAdManagerShim.onFailure");
        }
    }
}
