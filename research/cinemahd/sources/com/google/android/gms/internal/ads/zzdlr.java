package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdlr extends zzdrt<zzdlr, zza> implements zzdtg {
    private static volatile zzdtn<zzdlr> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlr zzhaw;
    private int zzhaf;
    private zzdls zzhau;

    public static final class zza extends zzdrt.zzb<zzdlr, zza> implements zzdtg {
        private zza() {
            super(zzdlr.zzhaw);
        }

        /* synthetic */ zza(zzdlq zzdlq) {
            this();
        }
    }

    static {
        zzdlr zzdlr = new zzdlr();
        zzhaw = zzdlr;
        zzdrt.zza(zzdlr.class, zzdlr);
    }

    private zzdlr() {
    }

    public static zzdlr zzad(zzdqk zzdqk) throws zzdse {
        return (zzdlr) zzdrt.zza(zzhaw, zzdqk);
    }

    public final int getKeySize() {
        return this.zzhaf;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlq.zzdk[i - 1]) {
            case 1:
                return new zzdlr();
            case 2:
                return new zza((zzdlq) null);
            case 3:
                return zzdrt.zza((zzdte) zzhaw, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u000b", new Object[]{"zzhau", "zzhaf"});
            case 4:
                return zzhaw;
            case 5:
                zzdtn<zzdlr> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlr.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhaw);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final zzdls zzatq() {
        zzdls zzdls = this.zzhau;
        return zzdls == null ? zzdls.zzatu() : zzdls;
    }
}
