package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

public final class zzcib implements zzdcx {
    private final zzchz zzfxk;

    zzcib(zzchz zzchz) {
        this.zzfxk = zzchz;
    }

    public final void zza(zzdco zzdco, String str) {
    }

    public final void zza(zzdco zzdco, String str, Throwable th) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzdco.RENDERER == zzdco && this.zzfxk.zzalz() != 0) {
            this.zzfxk.zzer(zzq.zzkx().a() - this.zzfxk.zzalz());
        }
    }

    public final void zzb(zzdco zzdco, String str) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzdco.RENDERER == zzdco) {
            this.zzfxk.zzfe(zzq.zzkx().a());
        }
    }

    public final void zzc(zzdco zzdco, String str) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzdco.RENDERER == zzdco && this.zzfxk.zzalz() != 0) {
            this.zzfxk.zzer(zzq.zzkx().a() - this.zzfxk.zzalz());
        }
    }
}
