package com.google.android.gms.internal.ads;

public final class zzbyg {
    private final zzall zzfok;
    private final zzalq zzfol;
    private final zzalr zzfom;

    public zzbyg(zzalq zzalq, zzall zzall, zzalr zzalr) {
        this.zzfol = zzalq;
        this.zzfok = zzall;
        this.zzfom = zzalr;
    }

    public final zzalq zzakk() {
        return this.zzfol;
    }

    public final zzall zzakl() {
        return this.zzfok;
    }

    public final zzalr zzakm() {
        return this.zzfom;
    }
}
