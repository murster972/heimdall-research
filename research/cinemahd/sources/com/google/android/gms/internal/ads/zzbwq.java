package com.google.android.gms.internal.ads;

public final class zzbwq {
    private zzacd zzcwg;

    public zzbwq(zzbwi zzbwi) {
        this.zzcwg = zzbwi;
    }

    public final synchronized void zza(zzacd zzacd) {
        this.zzcwg = zzacd;
    }

    public final synchronized zzacd zzrq() {
        return this.zzcwg;
    }
}
