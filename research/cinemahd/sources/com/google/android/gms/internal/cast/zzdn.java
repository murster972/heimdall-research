package com.google.android.gms.internal.cast;

import android.content.Context;
import com.google.android.gms.flags.Flag;
import com.google.android.gms.flags.FlagRegistry;
import com.google.android.gms.flags.Singletons;

public final class zzdn {
    public static final Flag<Boolean> zzze = Flag.a(0, "gms:cast:remote_display_enabled", false);

    public static final void initialize(Context context) {
        Singletons.a();
        FlagRegistry.a(context);
    }
}
