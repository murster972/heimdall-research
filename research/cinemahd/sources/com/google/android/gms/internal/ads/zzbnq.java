package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbnq implements zzdxg<zzbsu<zzbqb>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbiw> zzfdq;

    private zzbnq(zzdxp<zzbiw> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdq = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzbsu<zzbqb> zza(zzbiw zzbiw, Executor executor) {
        return (zzbsu) zzdxm.zza(new zzbsu(zzbiw, executor), "Cannot return null from a non-@Nullable @Provides method");
    }

    public static zzbnq zzg(zzdxp<zzbiw> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzbnq(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfdq.get(), this.zzfcv.get());
    }
}
