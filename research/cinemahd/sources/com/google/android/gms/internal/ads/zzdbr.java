package com.google.android.gms.internal.ads;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class zzdbr implements ThreadFactory {
    private final AtomicInteger zzye = new AtomicInteger(1);

    zzdbr() {
    }

    public final Thread newThread(Runnable runnable) {
        int andIncrement = this.zzye.getAndIncrement();
        StringBuilder sb = new StringBuilder(25);
        sb.append("AdWorker(NG) #");
        sb.append(andIncrement);
        return new Thread(runnable, sb.toString());
    }
}
