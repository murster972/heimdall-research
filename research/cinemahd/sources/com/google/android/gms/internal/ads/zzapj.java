package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.query.BannerQueryDataConfiguration;
import com.google.android.gms.ads.query.InterstitialQueryDataConfiguration;
import com.google.android.gms.ads.query.NativeQueryDataConfiguration;
import com.google.android.gms.ads.query.QueryDataConfiguration;
import com.google.android.gms.ads.query.QueryDataGenerationCallback;
import com.google.android.gms.ads.query.RewardedQueryDataConfiguration;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.startapp.sdk.adsbase.model.AdPreferences;

public final class zzapj {
    private final QueryDataConfiguration zzdic;

    public zzapj(QueryDataConfiguration queryDataConfiguration) {
        this.zzdic = queryDataConfiguration;
    }

    public final void zza(QueryDataGenerationCallback queryDataGenerationCallback) {
        String str;
        zzuj zzuj;
        Context context = this.zzdic.getContext();
        IObjectWrapper a2 = ObjectWrapper.a(context);
        try {
            zzauo zzf = ((zzaut) zzayx.zza(context, "com.google.android.gms.ads.DynamiteSignalGeneratorCreatorImpl", zzapi.zzbtz)).zzf(a2, 12451009);
            String adUnitId = this.zzdic.getAdUnitId();
            QueryDataConfiguration queryDataConfiguration = this.zzdic;
            if (queryDataConfiguration instanceof BannerQueryDataConfiguration) {
                str = AdPreferences.TYPE_BANNER;
            } else if (queryDataConfiguration instanceof InterstitialQueryDataConfiguration) {
                str = "INTERSTITIAL";
            } else if (queryDataConfiguration instanceof NativeQueryDataConfiguration) {
                str = "NATIVE";
            } else {
                str = queryDataConfiguration instanceof RewardedQueryDataConfiguration ? "REWARDED" : "UNKNOWN";
            }
            QueryDataConfiguration queryDataConfiguration2 = this.zzdic;
            if (queryDataConfiguration2 instanceof BannerQueryDataConfiguration) {
                zzuj = new zzuj(queryDataConfiguration2.getContext(), ((BannerQueryDataConfiguration) queryDataConfiguration2).getAdSize());
            } else if (queryDataConfiguration2 instanceof InterstitialQueryDataConfiguration) {
                zzuj = new zzuj();
            } else if (queryDataConfiguration2 instanceof NativeQueryDataConfiguration) {
                zzuj = zzuj.zzg(queryDataConfiguration2.getContext());
            } else if (queryDataConfiguration2 instanceof RewardedQueryDataConfiguration) {
                zzuj = zzuj.zzol();
            } else {
                zzuj = new zzuj();
            }
            try {
                zzf.zza(a2, new zzauu(adUnitId, str, zzuj), new zzapl(this, queryDataGenerationCallback));
            } catch (RemoteException unused) {
                queryDataGenerationCallback.onFailure("Internal Error.");
            }
        } catch (RemoteException | zzayz | NullPointerException unused2) {
            queryDataGenerationCallback.onFailure("Internal Error.");
        }
    }
}
