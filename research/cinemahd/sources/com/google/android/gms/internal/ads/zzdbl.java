package com.google.android.gms.internal.ads;

public interface zzdbl {
    boolean equals(Object obj);

    int hashCode();
}
