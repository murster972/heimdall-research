package com.google.android.gms.internal.ads;

final class zzdun implements zzduq {
    private final /* synthetic */ zzdqk zzhrb;

    zzdun(zzdqk zzdqk) {
        this.zzhrb = zzdqk;
    }

    public final int size() {
        return this.zzhrb.size();
    }

    public final byte zzfe(int i) {
        return this.zzhrb.zzfe(i);
    }
}
