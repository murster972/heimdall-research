package com.google.android.gms.internal.ads;

import com.facebook.common.util.ByteConstants;
import com.google.android.gms.internal.ads.zzdrt;

public final class zzbm {

    public static final class zza extends zzdrt<zza, C0033zza> implements zzdtg {
        /* access modifiers changed from: private */
        public static final zza zzdy;
        private static volatile zzdtn<zza> zzdz;
        private int zzdl;
        private String zzdm = "";
        private long zzdn;
        private String zzdo = "";
        private String zzdp = "";
        private String zzdq = "";
        private long zzdr;
        private long zzds;
        private String zzdt = "";
        private long zzdu;
        private String zzdv = "";
        private String zzdw = "";
        private zzdsb<zzb> zzdx = zzdrt.zzazw();

        /* renamed from: com.google.android.gms.internal.ads.zzbm$zza$zza  reason: collision with other inner class name */
        public static final class C0033zza extends zzdrt.zzb<zza, C0033zza> implements zzdtg {
            private C0033zza() {
                super(zza.zzdy);
            }

            public final C0033zza zza(zzb.C0034zza zza) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zza((zzb) zza.zzbaf());
                return this;
            }

            public final C0033zza zzc(long j) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzd(j);
                return this;
            }

            public final C0033zza zzi(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzn(str);
                return this;
            }

            public final C0033zza zzj(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzo(str);
                return this;
            }

            public final C0033zza zzk(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzp(str);
                return this;
            }

            public final C0033zza zzl(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzq(str);
                return this;
            }

            public final C0033zza zzm(String str) {
                if (this.zzhmq) {
                    zzbab();
                    this.zzhmq = false;
                }
                ((zza) this.zzhmp).zzr(str);
                return this;
            }

            /* synthetic */ C0033zza(zzbl zzbl) {
                this();
            }
        }

        static {
            zza zza = new zza();
            zzdy = zza;
            zzdrt.zza(zza.class, zza);
        }

        private zza() {
        }

        /* access modifiers changed from: private */
        public final void zza(zzb zzb2) {
            zzb2.getClass();
            if (!this.zzdx.zzaxp()) {
                this.zzdx = zzdrt.zza(this.zzdx);
            }
            this.zzdx.add(zzb2);
        }

        /* access modifiers changed from: private */
        public final void zzd(long j) {
            this.zzdl |= 2;
            this.zzdn = j;
        }

        /* access modifiers changed from: private */
        public final void zzn(String str) {
            str.getClass();
            this.zzdl |= 1;
            this.zzdm = str;
        }

        /* access modifiers changed from: private */
        public final void zzo(String str) {
            str.getClass();
            this.zzdl |= 4;
            this.zzdo = str;
        }

        /* access modifiers changed from: private */
        public final void zzp(String str) {
            str.getClass();
            this.zzdl |= 8;
            this.zzdp = str;
        }

        /* access modifiers changed from: private */
        public final void zzq(String str) {
            str.getClass();
            this.zzdl |= 16;
            this.zzdq = str;
        }

        /* access modifiers changed from: private */
        public final void zzr(String str) {
            str.getClass();
            this.zzdl |= ByteConstants.KB;
            this.zzdw = str;
        }

        public static C0033zza zzs() {
            return (C0033zza) zzdy.zzazt();
        }

        public static final class zzb extends zzdrt<zzb, C0034zza> implements zzdtg {
            private static volatile zzdtn<zzb> zzdz;
            /* access modifiers changed from: private */
            public static final zzb zzec;
            private int zzdl;
            private String zzea = "";
            private String zzeb = "";

            /* renamed from: com.google.android.gms.internal.ads.zzbm$zza$zzb$zza  reason: collision with other inner class name */
            public static final class C0034zza extends zzdrt.zzb<zzb, C0034zza> implements zzdtg {
                private C0034zza() {
                    super(zzb.zzec);
                }

                public final C0034zza zzs(String str) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzb) this.zzhmp).zzu(str);
                    return this;
                }

                public final C0034zza zzt(String str) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzb) this.zzhmp).zzv(str);
                    return this;
                }

                /* synthetic */ C0034zza(zzbl zzbl) {
                    this();
                }
            }

            static {
                zzb zzb = new zzb();
                zzec = zzb;
                zzdrt.zza(zzb.class, zzb);
            }

            private zzb() {
            }

            /* access modifiers changed from: private */
            public final void zzu(String str) {
                str.getClass();
                this.zzdl |= 1;
                this.zzea = str;
            }

            /* access modifiers changed from: private */
            public final void zzv(String str) {
                str.getClass();
                this.zzdl |= 2;
                this.zzeb = str;
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzbl.zzdk[i - 1]) {
                    case 1:
                        return new zzb();
                    case 2:
                        return new C0034zza((zzbl) null);
                    case 3:
                        return zzdrt.zza((zzdte) zzec, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001", new Object[]{"zzdl", "zzea", "zzeb"});
                    case 4:
                        return zzec;
                    case 5:
                        zzdtn<zzb> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzb.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzec);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            public static C0034zza zzu() {
                return (C0034zza) zzec.zzazt();
            }
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            switch (zzbl.zzdk[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0033zza((zzbl) null);
                case 3:
                    return zzdrt.zza((zzdte) zzdy, "\u0001\f\u0000\u0001\u0001\f\f\u0000\u0001\u0000\u0001\b\u0000\u0002\u0002\u0001\u0003\b\u0002\u0004\b\u0003\u0005\b\u0004\u0006\u0002\u0005\u0007\u0002\u0006\b\b\u0007\t\u0002\b\n\b\t\u000b\b\n\f\u001b", new Object[]{"zzdl", "zzdm", "zzdn", "zzdo", "zzdp", "zzdq", "zzdr", "zzds", "zzdt", "zzdu", "zzdv", "zzdw", "zzdx", zzb.class});
                case 4:
                    return zzdy;
                case 5:
                    zzdtn<zza> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zza.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzdy);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
