package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbrt implements zzdxg<Set<zzbsu<zzty>>> {
    private final zzbrm zzfim;

    private zzbrt(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbrt zzk(zzbrm zzbrm) {
        return new zzbrt(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfim.zzahs(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
