package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzbjn implements zzdxg<Set<zzbsu<zzo>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbjd> zzfdd;
    private final zzdxp<JSONObject> zzfde;

    private zzbjn(zzdxp<zzbjd> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<JSONObject> zzdxp3) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzfde = zzdxp3;
    }

    public static zzbjn zzd(zzdxp<zzbjd> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<JSONObject> zzdxp3) {
        return new zzbjn(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        Set set;
        zzbjd zzbjd = this.zzfdd.get();
        Executor executor = this.zzfcv.get();
        if (this.zzfde.get() == null) {
            set = Collections.emptySet();
        } else {
            set = Collections.singleton(new zzbsu(zzbjd, executor));
        }
        return (Set) zzdxm.zza(set, "Cannot return null from a non-@Nullable @Provides method");
    }
}
