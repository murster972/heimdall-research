package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzbwn implements zzdxg<zzbwk> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbmg> zzetc;
    private final zzdxp<zzbxa> zzeth;
    private final zzdxp<zzbww> zzeti;
    private final zzdxp<zzbwq> zzetl;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbwz> zzfeh;
    private final zzdxp<zzats> zzffn;
    private final zzdxp<zzbws> zzfkx;
    private final zzdxp<zzdq> zzfky;
    private final zzdxp<zzbxj> zzfls;
    private final zzdxp<zzcab> zzflt;
    private final zzdxp<zzbzz> zzflu;
    private final zzdxp<zzcai> zzflv;
    private final zzdxp<zzbzv> zzflw;
    private final zzdxp<zzcad> zzflx;

    private zzbwn(zzdxp<zzbmg> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzbws> zzdxp3, zzdxp<zzbxa> zzdxp4, zzdxp<zzbxj> zzdxp5, zzdxp<zzbww> zzdxp6, zzdxp<zzbwz> zzdxp7, zzdxp<zzcab> zzdxp8, zzdxp<zzbzz> zzdxp9, zzdxp<zzcai> zzdxp10, zzdxp<zzbzv> zzdxp11, zzdxp<zzcad> zzdxp12, zzdxp<zzats> zzdxp13, zzdxp<zzdq> zzdxp14, zzdxp<zzazb> zzdxp15, zzdxp<Context> zzdxp16, zzdxp<zzbwq> zzdxp17) {
        this.zzetc = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzfkx = zzdxp3;
        this.zzeth = zzdxp4;
        this.zzfls = zzdxp5;
        this.zzeti = zzdxp6;
        this.zzfeh = zzdxp7;
        this.zzflt = zzdxp8;
        this.zzflu = zzdxp9;
        this.zzflv = zzdxp10;
        this.zzflw = zzdxp11;
        this.zzflx = zzdxp12;
        this.zzffn = zzdxp13;
        this.zzfky = zzdxp14;
        this.zzfav = zzdxp15;
        this.zzejv = zzdxp16;
        this.zzetl = zzdxp17;
    }

    public static zzbwn zza(zzdxp<zzbmg> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzbws> zzdxp3, zzdxp<zzbxa> zzdxp4, zzdxp<zzbxj> zzdxp5, zzdxp<zzbww> zzdxp6, zzdxp<zzbwz> zzdxp7, zzdxp<zzcab> zzdxp8, zzdxp<zzbzz> zzdxp9, zzdxp<zzcai> zzdxp10, zzdxp<zzbzv> zzdxp11, zzdxp<zzcad> zzdxp12, zzdxp<zzats> zzdxp13, zzdxp<zzdq> zzdxp14, zzdxp<zzazb> zzdxp15, zzdxp<Context> zzdxp16, zzdxp<zzbwq> zzdxp17) {
        return new zzbwn(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6, zzdxp7, zzdxp8, zzdxp9, zzdxp10, zzdxp11, zzdxp12, zzdxp13, zzdxp14, zzdxp15, zzdxp16, zzdxp17);
    }

    public final /* synthetic */ Object get() {
        return new zzbwk(this.zzetc.get(), this.zzfcv.get(), this.zzfkx.get(), this.zzeth.get(), this.zzfls.get(), this.zzeti.get(), this.zzfeh.get(), zzdxd.zzao(this.zzflt), zzdxd.zzao(this.zzflu), zzdxd.zzao(this.zzflv), zzdxd.zzao(this.zzflw), zzdxd.zzao(this.zzflx), this.zzffn.get(), this.zzfky.get(), this.zzfav.get(), this.zzejv.get(), this.zzetl.get());
    }
}
