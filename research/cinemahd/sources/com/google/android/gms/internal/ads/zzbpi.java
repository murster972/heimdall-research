package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzbpi implements zzbrn {
    private final Context zzcri;

    zzbpi(Context context) {
        this.zzcri = context;
    }

    public final void zzp(Object obj) {
        ((zzbph) obj).zzbw(this.zzcri);
    }
}
