package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

final class zzlj implements Parcelable.Creator<zzlk> {
    zzlj() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzlk(parcel);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzlk[i];
    }
}
