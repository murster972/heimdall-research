package com.google.android.gms.internal.ads;

final class zzazz implements Runnable {
    private final /* synthetic */ zzazx zzdxn;

    zzazz(zzazx zzazx) {
        this.zzdxn = zzazx;
    }

    public final void run() {
        if (this.zzdxn.zzdxm != null) {
            this.zzdxn.zzdxm.zzer();
        }
    }
}
