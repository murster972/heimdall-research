package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.RandomAccess;

public interface zzdsb<E> extends List<E>, RandomAccess {
    boolean zzaxp();

    void zzaxq();

    zzdsb<E> zzfd(int i);
}
