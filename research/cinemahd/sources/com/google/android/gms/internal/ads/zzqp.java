package com.google.android.gms.internal.ads;

public final class zzqp {
    private final float bottom;
    private final float left;
    private final float right;
    private final float top;
    private final int zzbqf;

    public zzqp(float f, float f2, float f3, float f4, int i) {
        this.left = f;
        this.top = f2;
        this.right = f + f3;
        this.bottom = f2 + f4;
        this.zzbqf = i;
    }

    /* access modifiers changed from: package-private */
    public final float zzmh() {
        return this.left;
    }

    /* access modifiers changed from: package-private */
    public final float zzmi() {
        return this.top;
    }

    /* access modifiers changed from: package-private */
    public final float zzmj() {
        return this.right;
    }

    /* access modifiers changed from: package-private */
    public final float zzmk() {
        return this.bottom;
    }

    /* access modifiers changed from: package-private */
    public final int zzml() {
        return this.zzbqf;
    }
}
