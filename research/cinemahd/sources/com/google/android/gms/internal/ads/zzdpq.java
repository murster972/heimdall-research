package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;

public final class zzdpq extends zzdom {
    public zzdpq(byte[] bArr) throws InvalidKeyException {
        super(bArr);
    }

    public final /* bridge */ /* synthetic */ byte[] zzc(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        return super.zzc(bArr, bArr2);
    }

    /* access modifiers changed from: package-private */
    public final zzdok zzd(byte[] bArr, int i) throws InvalidKeyException {
        return new zzdpr(bArr, i);
    }
}
