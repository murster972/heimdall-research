package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzagf extends IInterface {
    void zza(zzafz zzafz, zzagd zzagd) throws RemoteException;
}
