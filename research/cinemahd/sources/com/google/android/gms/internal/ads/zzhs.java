package com.google.android.gms.internal.ads;

final class zzhs implements Runnable {
    private final /* synthetic */ zzhr zzahf;
    private final /* synthetic */ zzgw zzahh;

    zzhs(zzhr zzhr, zzgw zzgw) {
        this.zzahf = zzhr;
        this.zzahh = zzgw;
    }

    public final void run() {
        this.zzahf.zzahg.zzb(this.zzahh);
    }
}
