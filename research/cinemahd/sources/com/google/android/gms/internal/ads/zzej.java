package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class zzej extends zzcj<Integer, Long> {
    public Long zzya;
    public Long zzyb;

    public zzej() {
    }

    /* access modifiers changed from: protected */
    public final void zzap(String str) {
        HashMap zzaq = zzcj.zzaq(str);
        if (zzaq != null) {
            this.zzya = (Long) zzaq.get(0);
            this.zzyb = (Long) zzaq.get(1);
        }
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> zzbk() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, this.zzya);
        hashMap.put(1, this.zzyb);
        return hashMap;
    }

    public zzej(String str) {
        zzap(str);
    }
}
