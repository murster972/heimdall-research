package com.google.android.gms.internal.ads;

import android.content.Context;

final class zzbhl implements zzczc {
    private final /* synthetic */ zzbgr zzerr;
    private Context zzewj;
    private String zzewk;

    private zzbhl(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    public final zzczd zzaer() {
        zzdxm.zza(this.zzewj, Context.class);
        return new zzbhk(this.zzerr, this.zzewj, this.zzewk);
    }

    public final /* synthetic */ zzczc zzbu(Context context) {
        this.zzewj = (Context) zzdxm.checkNotNull(context);
        return this;
    }

    public final /* synthetic */ zzczc zzfr(String str) {
        this.zzewk = str;
        return this;
    }
}
