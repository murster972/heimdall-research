package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcmt implements Runnable {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcmu zzgat;
    private final zzcip zzgau;

    zzcmt(zzcmu zzcmu, zzczt zzczt, zzczl zzczl, zzcip zzcip) {
        this.zzgat = zzcmu;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
        this.zzgau = zzcip;
    }

    public final void run() {
        zzcmu zzcmu = this.zzgat;
        zzcms.zzc(this.zzfot, this.zzfel, this.zzgau);
    }
}
