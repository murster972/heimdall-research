package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbry implements zzdxg<Set<zzbsu<zzbpe>>> {
    private final zzbrm zzfim;

    private zzbry(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbry zzo(zzbrm zzbrm) {
        return new zzbry(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfim.zzaht(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
