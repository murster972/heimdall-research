package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import java.util.concurrent.TimeUnit;

@TargetApi(14)
public final class zzbaq {
    private final long zzdys = TimeUnit.MILLISECONDS.toNanos(((Long) zzve.zzoy().zzd(zzzn.zzchd)).longValue());
    private long zzdyt;
    private boolean zzdyu = true;

    zzbaq() {
    }

    public final void zza(SurfaceTexture surfaceTexture, zzbah zzbah) {
        if (zzbah != null) {
            long timestamp = surfaceTexture.getTimestamp();
            if (this.zzdyu || Math.abs(timestamp - this.zzdyt) >= this.zzdys) {
                this.zzdyu = false;
                this.zzdyt = timestamp;
                zzawb.zzdsr.post(new zzbat(this, zzbah));
            }
        }
    }

    public final void zzxu() {
        this.zzdyu = true;
    }
}
