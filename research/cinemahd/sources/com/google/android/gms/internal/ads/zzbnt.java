package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

public final class zzbnt implements zzdxg<zzbsu<zzo>> {
    private final zzdxp<zzbmx> zzfdq;
    private final zzbnu zzfha;

    private zzbnt(zzbnu zzbnu, zzdxp<zzbmx> zzdxp) {
        this.zzfha = zzbnu;
        this.zzfdq = zzdxp;
    }

    public static zzbnt zza(zzbnu zzbnu, zzdxp<zzbmx> zzdxp) {
        return new zzbnt(zzbnu, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
