package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.ViewGroup;
import com.google.android.gms.common.internal.Preconditions;

public final class zzbao {
    private final Context zzcgd;
    private final zzbaz zzdxu;
    private final ViewGroup zzdyp;
    private zzbai zzdyq;

    public zzbao(Context context, ViewGroup viewGroup, zzbdi zzbdi) {
        this(context, viewGroup, zzbdi, (zzbai) null);
    }

    public final void onDestroy() {
        Preconditions.a("onDestroy must be called from the UI thread.");
        zzbai zzbai = this.zzdyq;
        if (zzbai != null) {
            zzbai.destroy();
            this.zzdyp.removeView(this.zzdyq);
            this.zzdyq = null;
        }
    }

    public final void onPause() {
        Preconditions.a("onPause must be called from the UI thread.");
        zzbai zzbai = this.zzdyq;
        if (zzbai != null) {
            zzbai.pause();
        }
    }

    public final void zza(int i, int i2, int i3, int i4, int i5, boolean z, zzbaw zzbaw) {
        if (this.zzdyq == null) {
            zzzv.zza(this.zzdxu.zzyq().zzqp(), this.zzdxu.zzym(), "vpr2");
            Context context = this.zzcgd;
            zzbaz zzbaz = this.zzdxu;
            this.zzdyq = new zzbai(context, zzbaz, i5, z, zzbaz.zzyq().zzqp(), zzbaw);
            this.zzdyp.addView(this.zzdyq, 0, new ViewGroup.LayoutParams(-1, -1));
            int i6 = i;
            int i7 = i2;
            int i8 = i3;
            int i9 = i4;
            this.zzdyq.zzd(i, i2, i3, i4);
            this.zzdxu.zzav(false);
        }
    }

    public final void zze(int i, int i2, int i3, int i4) {
        Preconditions.a("The underlay may only be modified from the UI thread.");
        zzbai zzbai = this.zzdyq;
        if (zzbai != null) {
            zzbai.zzd(i, i2, i3, i4);
        }
    }

    public final zzbai zzye() {
        Preconditions.a("getAdVideoUnderlay must be called from the UI thread.");
        return this.zzdyq;
    }

    private zzbao(Context context, ViewGroup viewGroup, zzbaz zzbaz, zzbai zzbai) {
        this.zzcgd = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.zzdyp = viewGroup;
        this.zzdxu = zzbaz;
        this.zzdyq = null;
    }
}
