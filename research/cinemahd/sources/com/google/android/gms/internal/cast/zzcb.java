package com.google.android.gms.internal.cast;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.facebook.ads.AdError;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.games.GameManagerClient;
import com.google.android.gms.cast.games.GameManagerState;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzcb extends zzcw {
    private static final String NAMESPACE = zzdk.zzq("com.google.cast.games");
    /* access modifiers changed from: private */
    public static final zzdw zzbf = new zzdw("GameManagerChannel");
    /* access modifiers changed from: private */
    public final Cast.CastApi zzhz;
    /* access modifiers changed from: private */
    public final GoogleApiClient zzpb;
    private final Map<String, String> zzvv = new ConcurrentHashMap();
    private final SharedPreferences zzvw;
    private final String zzvx;
    /* access modifiers changed from: private */
    public zzco zzvy;
    private boolean zzvz = false;
    private GameManagerState zzwa;
    private GameManagerState zzwb;
    private String zzwc;
    private JSONObject zzwd;
    private long zzwe = 0;
    private GameManagerClient.Listener zzwf;
    private final Clock zzwg;
    /* access modifiers changed from: private */
    public String zzwh;

    public zzcb(GoogleApiClient googleApiClient, String str, Cast.CastApi castApi) throws IllegalArgumentException, IllegalStateException {
        super(NAMESPACE, "CastGameManagerChannel", (String) null);
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("castSessionId cannot be null.");
        } else if (googleApiClient == null || !googleApiClient.e() || !googleApiClient.a((Api<?>) Cast.b)) {
            throw new IllegalArgumentException("googleApiClient needs to be connected and contain the Cast.API API.");
        } else {
            this.zzwg = DefaultClock.c();
            this.zzvx = str;
            this.zzhz = castApi;
            this.zzpb = googleApiClient;
            Context applicationContext = googleApiClient.c().getApplicationContext();
            this.zzvw = applicationContext.getApplicationContext().getSharedPreferences(String.format(Locale.ROOT, "%s.%s", new Object[]{applicationContext.getPackageName(), "game_manager_channel_data"}), 0);
            this.zzwb = null;
            this.zzwa = new zzcq(0, 0, "", (JSONObject) null, new ArrayList(), "", -1);
        }
    }

    private final synchronized boolean isInitialized() {
        return this.zzvy != null;
    }

    private final void zzb(long j, int i, Object obj) {
        List<zzed> zzer = zzer();
        synchronized (zzer) {
            Iterator<zzed> it2 = zzer.iterator();
            while (it2.hasNext()) {
                if (it2.next().zzc(j, i, obj)) {
                    it2.remove();
                }
            }
        }
    }

    private final synchronized void zzek() throws IllegalStateException {
        if (!isInitialized()) {
            throw new IllegalStateException("Attempted to perform an operation on the GameManagerChannel before it is initialized.");
        } else if (isDisposed()) {
            throw new IllegalStateException("Attempted to perform an operation on the GameManagerChannel after it has been disposed.");
        }
    }

    /* access modifiers changed from: private */
    public final synchronized void zzel() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("castSessionId", this.zzvx);
            jSONObject.put("playerTokenMap", new JSONObject(this.zzvv));
            this.zzvw.edit().putString("save_data", jSONObject.toString()).commit();
        } catch (JSONException e) {
            zzbf.w("Error while saving data: %s", e.getMessage());
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0046, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zzem() {
        /*
            r5 = this;
            monitor-enter(r5)
            android.content.SharedPreferences r0 = r5.zzvw     // Catch:{ all -> 0x005b }
            java.lang.String r1 = "save_data"
            r2 = 0
            java.lang.String r0 = r0.getString(r1, r2)     // Catch:{ all -> 0x005b }
            if (r0 != 0) goto L_0x000e
            monitor-exit(r5)
            return
        L_0x000e:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0047 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0047 }
            java.lang.String r0 = "castSessionId"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ JSONException -> 0x0047 }
            java.lang.String r2 = r5.zzvx     // Catch:{ JSONException -> 0x0047 }
            boolean r0 = r2.equals(r0)     // Catch:{ JSONException -> 0x0047 }
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = "playerTokenMap"
            org.json.JSONObject r0 = r1.getJSONObject(r0)     // Catch:{ JSONException -> 0x0047 }
            java.util.Iterator r1 = r0.keys()     // Catch:{ JSONException -> 0x0047 }
        L_0x002b:
            boolean r2 = r1.hasNext()     // Catch:{ JSONException -> 0x0047 }
            if (r2 == 0) goto L_0x0041
            java.lang.Object r2 = r1.next()     // Catch:{ JSONException -> 0x0047 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ JSONException -> 0x0047 }
            java.util.Map<java.lang.String, java.lang.String> r3 = r5.zzvv     // Catch:{ JSONException -> 0x0047 }
            java.lang.String r4 = r0.getString(r2)     // Catch:{ JSONException -> 0x0047 }
            r3.put(r2, r4)     // Catch:{ JSONException -> 0x0047 }
            goto L_0x002b
        L_0x0041:
            r0 = 0
            r5.zzwe = r0     // Catch:{ JSONException -> 0x0047 }
        L_0x0045:
            monitor-exit(r5)
            return
        L_0x0047:
            r0 = move-exception
            com.google.android.gms.internal.cast.zzdw r1 = zzbf     // Catch:{ all -> 0x005b }
            java.lang.String r2 = "Error while loading data: %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x005b }
            r4 = 0
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x005b }
            r3[r4] = r0     // Catch:{ all -> 0x005b }
            r1.w(r2, r3)     // Catch:{ all -> 0x005b }
            monitor-exit(r5)
            return
        L_0x005b:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzcb.zzem():void");
    }

    private final synchronized String zzn(String str) throws IllegalStateException {
        if (str == null) {
            return null;
        }
        return this.zzvv.get(str);
    }

    public final synchronized void dispose() throws IllegalStateException {
        if (!this.zzvz) {
            this.zzwa = null;
            this.zzwb = null;
            this.zzwc = null;
            this.zzwd = null;
            this.zzvz = true;
            try {
                this.zzhz.a(this.zzpb, getNamespace());
            } catch (IOException e) {
                zzbf.w("Exception while detaching game manager channel.", e);
            }
        }
    }

    public final synchronized GameManagerState getCurrentState() throws IllegalStateException {
        zzek();
        return this.zzwa;
    }

    public final synchronized String getLastUsedPlayerId() throws IllegalStateException {
        zzek();
        return this.zzwh;
    }

    public final synchronized boolean isDisposed() {
        return this.zzvz;
    }

    public final synchronized void sendGameMessage(String str, JSONObject jSONObject) throws IllegalStateException {
        zzek();
        long j = this.zzwe + 1;
        this.zzwe = j;
        JSONObject zza = zza(j, str, 7, jSONObject);
        if (zza != null) {
            this.zzhz.b(this.zzpb, getNamespace(), zza.toString());
        }
    }

    public final synchronized PendingResult<GameManagerClient.GameManagerResult> sendGameRequest(String str, JSONObject jSONObject) throws IllegalStateException {
        zzek();
        return this.zzpb.a(new zzcf(this, str, jSONObject));
    }

    public final synchronized void setListener(GameManagerClient.Listener listener) {
        this.zzwf = listener;
    }

    public final synchronized PendingResult<GameManagerClient.GameManagerInstanceResult> zza(GameManagerClient gameManagerClient) throws IllegalArgumentException {
        return this.zzpb.a(new zzcc(this, gameManagerClient));
    }

    public final void zzo(String str) {
        String str2;
        int i = 0;
        zzbf.d("message received: %s", str);
        try {
            zzcp zzj = zzcp.zzj(new JSONObject(str));
            if (zzj == null) {
                zzbf.w("Could not parse game manager message from string: %s", str);
            } else if ((isInitialized() || zzj.zzxk != null) && !isDisposed()) {
                boolean z = zzj.zzwy == 1;
                if (z && !TextUtils.isEmpty(zzj.zzxj)) {
                    this.zzvv.put(zzj.zzxh, zzj.zzxj);
                    zzel();
                }
                int i2 = zzj.zzwz;
                if (i2 == 0) {
                    zza(zzj);
                } else {
                    zzbf.w("Not updating from game message because the message contains error code: %d", Integer.valueOf(i2));
                }
                int i3 = zzj.zzwz;
                if (i3 != 0) {
                    if (i3 == 1) {
                        i = AdError.INTERNAL_ERROR_CODE;
                    } else if (i3 == 2) {
                        i = 2003;
                    } else if (i3 == 3) {
                        i = 2150;
                    } else if (i3 != 4) {
                        zzdw zzdw = zzbf;
                        StringBuilder sb = new StringBuilder(53);
                        sb.append("Unknown GameManager protocol status code: ");
                        sb.append(i3);
                        zzdw.w(sb.toString(), new Object[0]);
                        i = 13;
                    } else {
                        i = 2151;
                    }
                }
                if (z) {
                    zzb(zzj.zzxi, i, zzj);
                }
                if (isInitialized() && i == 0) {
                    if (this.zzwf != null) {
                        GameManagerState gameManagerState = this.zzwb;
                        if (gameManagerState != null && !this.zzwa.equals(gameManagerState)) {
                            this.zzwf.a(this.zzwa, this.zzwb);
                        }
                        JSONObject jSONObject = this.zzwd;
                        if (!(jSONObject == null || (str2 = this.zzwc) == null)) {
                            this.zzwf.a(str2, jSONObject);
                        }
                    }
                    this.zzwb = null;
                    this.zzwc = null;
                    this.zzwd = null;
                }
            }
        } catch (JSONException e) {
            zzbf.w("Message is malformed (%s); ignoring: %s", e.getMessage(), str);
        }
    }

    public final synchronized PendingResult<GameManagerClient.GameManagerResult> zza(String str, int i, JSONObject jSONObject) throws IllegalStateException {
        zzek();
        return this.zzpb.a(new zzce(this, i, str, jSONObject));
    }

    public final void zza(long j, int i) {
        zzb(j, i, (Object) null);
    }

    /* access modifiers changed from: private */
    public final void zza(String str, int i, JSONObject jSONObject, zzec zzec) {
        long j = this.zzwe + 1;
        this.zzwe = j;
        JSONObject zza = zza(j, str, i, jSONObject);
        if (zza == null) {
            zzec.zza(-1, AdError.INTERNAL_ERROR_CODE, (Object) null);
            zzbf.w("Not sending request because it was invalid.", new Object[0]);
            return;
        }
        zzed zzed = new zzed(30000);
        zzed.zza(j, zzec);
        zza(zzed);
        this.zzhz.b(this.zzpb, getNamespace(), zza.toString()).setResultCallback(new zzcg(this, j));
    }

    private final JSONObject zza(long j, String str, int i, JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("requestId", j);
            jSONObject2.put("type", i);
            jSONObject2.put("extraMessageData", jSONObject);
            jSONObject2.put("playerId", str);
            jSONObject2.put("playerToken", zzn(str));
            return jSONObject2;
        } catch (JSONException e) {
            zzbf.w("JSONException when trying to create a message: %s", e.getMessage());
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final synchronized void zza(com.google.android.gms.internal.cast.zzcp r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            int r0 = r10.zzwy     // Catch:{ all -> 0x008b }
            r1 = 1
            if (r0 != r1) goto L_0x0007
            goto L_0x0008
        L_0x0007:
            r1 = 0
        L_0x0008:
            com.google.android.gms.cast.games.GameManagerState r0 = r9.zzwa     // Catch:{ all -> 0x008b }
            r9.zzwb = r0     // Catch:{ all -> 0x008b }
            if (r1 == 0) goto L_0x0016
            com.google.android.gms.internal.cast.zzco r0 = r10.zzxk     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x0016
            com.google.android.gms.internal.cast.zzco r0 = r10.zzxk     // Catch:{ all -> 0x008b }
            r9.zzvy = r0     // Catch:{ all -> 0x008b }
        L_0x0016:
            boolean r0 = r9.isInitialized()     // Catch:{ all -> 0x008b }
            if (r0 != 0) goto L_0x001e
            monitor-exit(r9)
            return
        L_0x001e:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x008b }
            r6.<init>()     // Catch:{ all -> 0x008b }
            java.util.List<com.google.android.gms.internal.cast.zzcs> r0 = r10.zzxe     // Catch:{ all -> 0x008b }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x008b }
        L_0x0029:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x008b }
            if (r1 == 0) goto L_0x0050
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x008b }
            com.google.android.gms.internal.cast.zzcs r1 = (com.google.android.gms.internal.cast.zzcs) r1     // Catch:{ all -> 0x008b }
            java.lang.String r2 = r1.getPlayerId()     // Catch:{ all -> 0x008b }
            com.google.android.gms.internal.cast.zzcr r3 = new com.google.android.gms.internal.cast.zzcr     // Catch:{ all -> 0x008b }
            int r4 = r1.getPlayerState()     // Catch:{ all -> 0x008b }
            org.json.JSONObject r1 = r1.getPlayerData()     // Catch:{ all -> 0x008b }
            java.util.Map<java.lang.String, java.lang.String> r5 = r9.zzvv     // Catch:{ all -> 0x008b }
            boolean r5 = r5.containsKey(r2)     // Catch:{ all -> 0x008b }
            r3.<init>(r2, r4, r1, r5)     // Catch:{ all -> 0x008b }
            r6.add(r3)     // Catch:{ all -> 0x008b }
            goto L_0x0029
        L_0x0050:
            com.google.android.gms.internal.cast.zzcq r0 = new com.google.android.gms.internal.cast.zzcq     // Catch:{ all -> 0x008b }
            int r2 = r10.zzxd     // Catch:{ all -> 0x008b }
            int r3 = r10.zzxc     // Catch:{ all -> 0x008b }
            java.lang.String r4 = r10.zzxg     // Catch:{ all -> 0x008b }
            org.json.JSONObject r5 = r10.zzxf     // Catch:{ all -> 0x008b }
            com.google.android.gms.internal.cast.zzco r1 = r9.zzvy     // Catch:{ all -> 0x008b }
            java.lang.String r7 = r1.zzeo()     // Catch:{ all -> 0x008b }
            com.google.android.gms.internal.cast.zzco r1 = r9.zzvy     // Catch:{ all -> 0x008b }
            int r8 = r1.getMaxPlayers()     // Catch:{ all -> 0x008b }
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x008b }
            r9.zzwa = r0     // Catch:{ all -> 0x008b }
            com.google.android.gms.cast.games.GameManagerState r0 = r9.zzwa     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r10.zzxh     // Catch:{ all -> 0x008b }
            com.google.android.gms.cast.games.PlayerInfo r0 = r0.getPlayer(r1)     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x0089
            boolean r0 = r0.isControllable()     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x0089
            int r0 = r10.zzwy     // Catch:{ all -> 0x008b }
            r1 = 2
            if (r0 != r1) goto L_0x0089
            java.lang.String r0 = r10.zzxh     // Catch:{ all -> 0x008b }
            r9.zzwc = r0     // Catch:{ all -> 0x008b }
            org.json.JSONObject r10 = r10.zzxb     // Catch:{ all -> 0x008b }
            r9.zzwd = r10     // Catch:{ all -> 0x008b }
        L_0x0089:
            monitor-exit(r9)
            return
        L_0x008b:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzcb.zza(com.google.android.gms.internal.cast.zzcp):void");
    }
}
