package com.google.android.gms.internal.ads;

import java.util.HashMap;
import java.util.Map;

final class zzbcq implements Runnable {
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ String zzedb;
    private final /* synthetic */ zzbcn zzedf;
    private final /* synthetic */ long zzedi;

    zzbcq(zzbcn zzbcn, String str, String str2, long j) {
        this.zzedf = zzbcn;
        this.zzdug = str;
        this.zzedb = str2;
        this.zzedi = j;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "precacheComplete");
        hashMap.put("src", this.zzdug);
        hashMap.put("cachedSrc", this.zzedb);
        hashMap.put("totalDuration", Long.toString(this.zzedi));
        this.zzedf.zza("onPrecacheEvent", (Map<String, String>) hashMap);
    }
}
