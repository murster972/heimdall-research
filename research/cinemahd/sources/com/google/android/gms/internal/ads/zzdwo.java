package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.ByteBuffer;

public class zzdwo extends zzdwq implements zzbf {
    private String type;
    private long zzauq;
    private zzbi zzhyp;
    private boolean zzhzg;

    public zzdwo(String str) {
        this.type = str;
    }

    public final String getType() {
        return this.type;
    }

    public final void zza(zzbi zzbi) {
        this.zzhyp = zzbi;
    }

    public final void zza(zzdws zzdws, ByteBuffer byteBuffer, long j, zzbe zzbe) throws IOException {
        this.zzauq = zzdws.position() - ((long) byteBuffer.remaining());
        this.zzhzg = byteBuffer.remaining() == 16;
        zza(zzdws, j, zzbe);
    }

    public final void zza(zzdws zzdws, long j, zzbe zzbe) throws IOException {
        this.zzhyv = zzdws;
        this.zzhzk = zzdws.position();
        this.zzbcu = this.zzhzk - ((long) ((this.zzhzg || 8 + j >= 4294967296L) ? 16 : 8));
        zzdws.zzfc(zzdws.position() + j);
        this.zzaum = zzdws.position();
        this.zzhzi = zzbe;
    }
}
