package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcxh implements zzded {
    private final zzdbi zzgjz;

    zzcxh(zzdbi zzdbi) {
        this.zzgjz = zzdbi;
    }

    public final Object apply(Object obj) {
        zzdbi zzdbi = this.zzgjz;
        zzdbi.zzgpd = (zzbmd) obj;
        return zzdbi;
    }
}
