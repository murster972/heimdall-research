package com.google.android.gms.internal.ads;

import android.util.JsonWriter;
import java.util.Map;

final /* synthetic */ class zzayq implements zzayv {
    private final int zzdvv;
    private final Map zzdvw;

    zzayq(int i, Map map) {
        this.zzdvv = i;
        this.zzdvw = map;
    }

    public final void zzb(JsonWriter jsonWriter) {
        zzayo.zza(this.zzdvv, this.zzdvw, jsonWriter);
    }
}
