package com.google.android.gms.internal.ads;

import android.view.View;

final class zzbfd implements Runnable {
    private final /* synthetic */ View val$view;
    private final /* synthetic */ zzato zzeev;
    private final /* synthetic */ int zzeew;
    private final /* synthetic */ zzbfb zzehv;

    zzbfd(zzbfb zzbfb, View view, zzato zzato, int i) {
        this.zzehv = zzbfb;
        this.val$view = view;
        this.zzeev = zzato;
        this.zzeew = i;
    }

    public final void run() {
        this.zzehv.zza(this.val$view, this.zzeev, this.zzeew - 1);
    }
}
