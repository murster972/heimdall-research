package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzbwa implements zzdxg<JSONObject> {
    private final zzbvy zzfla;

    public zzbwa(zzbvy zzbvy) {
        this.zzfla = zzbvy;
    }

    public final /* synthetic */ Object get() {
        return (JSONObject) zzdxm.zza(this.zzfla.zzais(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
