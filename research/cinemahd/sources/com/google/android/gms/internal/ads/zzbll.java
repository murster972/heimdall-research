package com.google.android.gms.internal.ads;

public final class zzbll implements zzdxg<Boolean> {
    private final zzdxp<zzczu> zzfep;

    public zzbll(zzdxp<zzczu> zzdxp) {
        this.zzfep = zzdxp;
    }

    public final /* synthetic */ Object get() {
        boolean z;
        if (this.zzfep.get().zzaoo() != null) {
            z = ((Boolean) zzve.zzoy().zzd(zzzn.zzcjp)).booleanValue();
        } else {
            z = ((Boolean) zzve.zzoy().zzd(zzzn.zzcoy)).booleanValue();
        }
        return Boolean.valueOf(z);
    }
}
