package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzdbe extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzdbe> CREATOR = new zzdbj();
    private final zzdbh[] zzgod;
    private final int[] zzgoe;
    private final int[] zzgof;
    private final int zzgog;
    public final zzdbh zzgoh;
    public final int zzgoi;
    public final int zzgoj;
    public final int zzgok;
    public final String zzgol;
    private final int zzgom;
    public final int zzgon;
    private final int zzgoo;
    private final int zzgop;
    public final Context zzup;

    private zzdbe(Context context, zzdbh zzdbh, int i, int i2, int i3, String str, String str2, String str3) {
        int i4;
        this.zzgod = zzdbh.values();
        this.zzgoe = zzdbg.zzapn();
        this.zzgof = zzdbg.zzapo();
        this.zzup = context;
        this.zzgog = zzdbh.ordinal();
        this.zzgoh = zzdbh;
        this.zzgoi = i;
        this.zzgoj = i2;
        this.zzgok = i3;
        this.zzgol = str;
        if ("oldest".equals(str2)) {
            i4 = zzdbg.zzgos;
        } else if ("lru".equals(str2) || !"lfu".equals(str2)) {
            i4 = zzdbg.zzgot;
        } else {
            i4 = zzdbg.zzgou;
        }
        this.zzgon = i4;
        this.zzgom = this.zzgon - 1;
        "onAdClosed".equals(str3);
        this.zzgop = zzdbg.zzgow;
        this.zzgoo = this.zzgop - 1;
    }

    public static zzdbe zza(zzdbh zzdbh, Context context) {
        if (zzdbh == zzdbh.Rewarded) {
            return new zzdbe(context, zzdbh, ((Integer) zzve.zzoy().zzd(zzzn.zzcql)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcqr)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcqt)).intValue(), (String) zzve.zzoy().zzd(zzzn.zzcqv), (String) zzve.zzoy().zzd(zzzn.zzcqn), (String) zzve.zzoy().zzd(zzzn.zzcqp));
        } else if (zzdbh == zzdbh.Interstitial) {
            return new zzdbe(context, zzdbh, ((Integer) zzve.zzoy().zzd(zzzn.zzcqm)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcqs)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcqu)).intValue(), (String) zzve.zzoy().zzd(zzzn.zzcqw), (String) zzve.zzoy().zzd(zzzn.zzcqo), (String) zzve.zzoy().zzd(zzzn.zzcqq));
        } else if (zzdbh != zzdbh.AppOpen) {
            return null;
        } else {
            return new zzdbe(context, zzdbh, ((Integer) zzve.zzoy().zzd(zzzn.zzcqz)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcrb)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcrc)).intValue(), (String) zzve.zzoy().zzd(zzzn.zzcqx), (String) zzve.zzoy().zzd(zzzn.zzcqy), (String) zzve.zzoy().zzd(zzzn.zzcra));
        }
    }

    public static boolean zzapl() {
        return ((Boolean) zzve.zzoy().zzd(zzzn.zzcqk)).booleanValue();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.zzgog);
        SafeParcelWriter.a(parcel, 2, this.zzgoi);
        SafeParcelWriter.a(parcel, 3, this.zzgoj);
        SafeParcelWriter.a(parcel, 4, this.zzgok);
        SafeParcelWriter.a(parcel, 5, this.zzgol, false);
        SafeParcelWriter.a(parcel, 6, this.zzgom);
        SafeParcelWriter.a(parcel, 7, this.zzgoo);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzdbe(int i, int i2, int i3, int i4, String str, int i5, int i6) {
        this.zzgod = zzdbh.values();
        this.zzgoe = zzdbg.zzapn();
        this.zzgof = zzdbg.zzapo();
        this.zzup = null;
        this.zzgog = i;
        this.zzgoh = this.zzgod[i];
        this.zzgoi = i2;
        this.zzgoj = i3;
        this.zzgok = i4;
        this.zzgol = str;
        this.zzgom = i5;
        this.zzgon = this.zzgoe[i5];
        this.zzgoo = i6;
        this.zzgop = this.zzgof[i6];
    }
}
