package com.google.android.gms.internal.ads;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzcuw implements zzcty<JSONObject> {
    private final JSONObject zzghp;

    public zzcuw(JSONObject jSONObject) {
        this.zzghp = jSONObject;
    }

    public final /* synthetic */ void zzr(Object obj) {
        try {
            JSONObject zzb = zzaxs.zzb((JSONObject) obj, "content_info");
            JSONObject jSONObject = this.zzghp;
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                zzb.put(next, jSONObject.get(next));
            }
        } catch (JSONException unused) {
            zzavs.zzed("Failed putting app indexing json.");
        }
    }
}
