package com.google.android.gms.internal.ads;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

public class zzbrl<ListenerT> {
    private final Map<ListenerT, Executor> zzfia = new HashMap();

    protected zzbrl(Set<zzbsu<ListenerT>> set) {
        zzb(set);
    }

    private final synchronized void zzb(Set<zzbsu<ListenerT>> set) {
        for (zzbsu<ListenerT> zza : set) {
            zza(zza);
        }
    }

    public final synchronized void zza(zzbsu<ListenerT> zzbsu) {
        zza(zzbsu.zzfir, zzbsu.executor);
    }

    public final synchronized void zza(ListenerT listenert, Executor executor) {
        this.zzfia.put(listenert, executor);
    }

    /* access modifiers changed from: protected */
    public final synchronized void zza(zzbrn<ListenerT> zzbrn) {
        for (Map.Entry next : this.zzfia.entrySet()) {
            ((Executor) next.getValue()).execute(new zzbrk(zzbrn, next.getKey()));
        }
    }
}
