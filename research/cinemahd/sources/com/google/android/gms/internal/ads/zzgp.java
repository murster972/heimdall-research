package com.google.android.gms.internal.ads;

public final class zzgp {
    public final zzgm zzacm;
    public final int zzacn;
    public final Object zzaco;

    public zzgp(zzgm zzgm, int i, Object obj) {
        this.zzacm = zzgm;
        this.zzacn = i;
        this.zzaco = obj;
    }
}
