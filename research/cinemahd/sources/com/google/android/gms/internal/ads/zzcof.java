package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzcof implements zzdxg<zzcob> {
    private final zzdxp<Executor> zzfcv;

    public zzcof(zzdxp<Executor> zzdxp) {
        this.zzfcv = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcob(this.zzfcv.get());
    }
}
