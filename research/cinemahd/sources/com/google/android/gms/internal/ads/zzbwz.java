package com.google.android.gms.internal.ads;

import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;

public final class zzbwz {
    public static final zzbwz zzfmx = new zzbxb().zzajw();
    private final zzadj zzfmq;
    private final zzadi zzfmr;
    private final zzadv zzfms;
    private final zzadu zzfmt;
    private final zzahh zzfmu;
    private final SimpleArrayMap<String, zzadp> zzfmv;
    private final SimpleArrayMap<String, zzado> zzfmw;

    private zzbwz(zzbxb zzbxb) {
        this.zzfmq = zzbxb.zzfmq;
        this.zzfmr = zzbxb.zzfmr;
        this.zzfms = zzbxb.zzfms;
        this.zzfmv = new SimpleArrayMap<>(zzbxb.zzfmv);
        this.zzfmw = new SimpleArrayMap<>(zzbxb.zzfmw);
        this.zzfmt = zzbxb.zzfmt;
        this.zzfmu = zzbxb.zzfmu;
    }

    public final zzadj zzajp() {
        return this.zzfmq;
    }

    public final zzadi zzajq() {
        return this.zzfmr;
    }

    public final zzadv zzajr() {
        return this.zzfms;
    }

    public final zzadu zzajs() {
        return this.zzfmt;
    }

    public final zzahh zzajt() {
        return this.zzfmu;
    }

    public final ArrayList<String> zzaju() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (this.zzfms != null) {
            arrayList.add(Integer.toString(6));
        }
        if (this.zzfmq != null) {
            arrayList.add(Integer.toString(1));
        }
        if (this.zzfmr != null) {
            arrayList.add(Integer.toString(2));
        }
        if (this.zzfmv.size() > 0) {
            arrayList.add(Integer.toString(3));
        }
        if (this.zzfmu != null) {
            arrayList.add(Integer.toString(7));
        }
        return arrayList;
    }

    public final ArrayList<String> zzajv() {
        ArrayList<String> arrayList = new ArrayList<>(this.zzfmv.size());
        for (int i = 0; i < this.zzfmv.size(); i++) {
            arrayList.add(this.zzfmv.b(i));
        }
        return arrayList;
    }

    public final zzadp zzfz(String str) {
        return this.zzfmv.get(str);
    }

    public final zzado zzga(String str) {
        return this.zzfmw.get(str);
    }
}
