package com.google.android.gms.internal.ads;

final class zzmh {
    private int length;
    private int[] zzamp;
    private long[] zzamq;
    private long[] zzams;
    private int[] zzawq;
    private int zzbbu = 1000;
    private int[] zzbbv;
    private zzjn[] zzbbw;
    private zzgw[] zzbbx;
    private int zzbby;
    private int zzbbz;
    private int zzbca;
    private long zzbcb;
    private long zzbcc;
    private boolean zzbcd;
    private boolean zzbce;
    private zzgw zzbcf;

    public zzmh() {
        int i = this.zzbbu;
        this.zzbbv = new int[i];
        this.zzamq = new long[i];
        this.zzams = new long[i];
        this.zzawq = new int[i];
        this.zzamp = new int[i];
        this.zzbbw = new zzjn[i];
        this.zzbbx = new zzgw[i];
        this.zzbcb = Long.MIN_VALUE;
        this.zzbcc = Long.MIN_VALUE;
        this.zzbce = true;
        this.zzbcd = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0023, code lost:
        return -3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int zza(com.google.android.gms.internal.ads.zzgy r5, com.google.android.gms.internal.ads.zzis r6, boolean r7, boolean r8, com.google.android.gms.internal.ads.zzgw r9, com.google.android.gms.internal.ads.zzmk r10) {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.zzhw()     // Catch:{ all -> 0x00a6 }
            r1 = -5
            r2 = -3
            r3 = -4
            if (r0 != 0) goto L_0x0024
            if (r8 == 0) goto L_0x0012
            r5 = 4
            r6.setFlags(r5)     // Catch:{ all -> 0x00a6 }
            monitor-exit(r4)
            return r3
        L_0x0012:
            com.google.android.gms.internal.ads.zzgw r6 = r4.zzbcf     // Catch:{ all -> 0x00a6 }
            if (r6 == 0) goto L_0x0022
            if (r7 != 0) goto L_0x001c
            com.google.android.gms.internal.ads.zzgw r6 = r4.zzbcf     // Catch:{ all -> 0x00a6 }
            if (r6 == r9) goto L_0x0022
        L_0x001c:
            com.google.android.gms.internal.ads.zzgw r6 = r4.zzbcf     // Catch:{ all -> 0x00a6 }
            r5.zzafz = r6     // Catch:{ all -> 0x00a6 }
            monitor-exit(r4)
            return r1
        L_0x0022:
            monitor-exit(r4)
            return r2
        L_0x0024:
            if (r7 != 0) goto L_0x009c
            com.google.android.gms.internal.ads.zzgw[] r7 = r4.zzbbx     // Catch:{ all -> 0x00a6 }
            int r8 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r7 = r7[r8]     // Catch:{ all -> 0x00a6 }
            if (r7 == r9) goto L_0x002f
            goto L_0x009c
        L_0x002f:
            java.nio.ByteBuffer r5 = r6.zzcs     // Catch:{ all -> 0x00a6 }
            r7 = 0
            r8 = 1
            if (r5 != 0) goto L_0x0037
            r5 = 1
            goto L_0x0038
        L_0x0037:
            r5 = 0
        L_0x0038:
            if (r5 == 0) goto L_0x003c
            monitor-exit(r4)
            return r2
        L_0x003c:
            long[] r5 = r4.zzams     // Catch:{ all -> 0x00a6 }
            int r9 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r0 = r5[r9]     // Catch:{ all -> 0x00a6 }
            r6.zzamd = r0     // Catch:{ all -> 0x00a6 }
            int[] r5 = r4.zzawq     // Catch:{ all -> 0x00a6 }
            int r9 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r5 = r5[r9]     // Catch:{ all -> 0x00a6 }
            r6.setFlags(r5)     // Catch:{ all -> 0x00a6 }
            int[] r5 = r4.zzamp     // Catch:{ all -> 0x00a6 }
            int r9 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r5 = r5[r9]     // Catch:{ all -> 0x00a6 }
            r10.size = r5     // Catch:{ all -> 0x00a6 }
            long[] r5 = r4.zzamq     // Catch:{ all -> 0x00a6 }
            int r9 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r0 = r5[r9]     // Catch:{ all -> 0x00a6 }
            r10.zzauq = r0     // Catch:{ all -> 0x00a6 }
            com.google.android.gms.internal.ads.zzjn[] r5 = r4.zzbbw     // Catch:{ all -> 0x00a6 }
            int r9 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r5 = r5[r9]     // Catch:{ all -> 0x00a6 }
            r10.zzapt = r5     // Catch:{ all -> 0x00a6 }
            long r0 = r4.zzbcb     // Catch:{ all -> 0x00a6 }
            long r5 = r6.zzamd     // Catch:{ all -> 0x00a6 }
            long r5 = java.lang.Math.max(r0, r5)     // Catch:{ all -> 0x00a6 }
            r4.zzbcb = r5     // Catch:{ all -> 0x00a6 }
            int r5 = r4.length     // Catch:{ all -> 0x00a6 }
            int r5 = r5 - r8
            r4.length = r5     // Catch:{ all -> 0x00a6 }
            int r5 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            int r5 = r5 + r8
            r4.zzbbz = r5     // Catch:{ all -> 0x00a6 }
            int r5 = r4.zzbby     // Catch:{ all -> 0x00a6 }
            int r5 = r5 + r8
            r4.zzbby = r5     // Catch:{ all -> 0x00a6 }
            int r5 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            int r6 = r4.zzbbu     // Catch:{ all -> 0x00a6 }
            if (r5 != r6) goto L_0x0086
            r4.zzbbz = r7     // Catch:{ all -> 0x00a6 }
        L_0x0086:
            int r5 = r4.length     // Catch:{ all -> 0x00a6 }
            if (r5 <= 0) goto L_0x0091
            long[] r5 = r4.zzamq     // Catch:{ all -> 0x00a6 }
            int r6 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r6 = r5[r6]     // Catch:{ all -> 0x00a6 }
            goto L_0x0098
        L_0x0091:
            long r5 = r10.zzauq     // Catch:{ all -> 0x00a6 }
            int r7 = r10.size     // Catch:{ all -> 0x00a6 }
            long r7 = (long) r7     // Catch:{ all -> 0x00a6 }
            long r6 = r5 + r7
        L_0x0098:
            r10.zzbct = r6     // Catch:{ all -> 0x00a6 }
            monitor-exit(r4)
            return r3
        L_0x009c:
            com.google.android.gms.internal.ads.zzgw[] r6 = r4.zzbbx     // Catch:{ all -> 0x00a6 }
            int r7 = r4.zzbbz     // Catch:{ all -> 0x00a6 }
            r6 = r6[r7]     // Catch:{ all -> 0x00a6 }
            r5.zzafz = r6     // Catch:{ all -> 0x00a6 }
            monitor-exit(r4)
            return r1
        L_0x00a6:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzmh.zza(com.google.android.gms.internal.ads.zzgy, com.google.android.gms.internal.ads.zzis, boolean, boolean, com.google.android.gms.internal.ads.zzgw, com.google.android.gms.internal.ads.zzmk):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0060, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long zzd(long r9, boolean r11) {
        /*
            r8 = this;
            monitor-enter(r8)
            boolean r0 = r8.zzhw()     // Catch:{ all -> 0x0061 }
            r1 = -1
            if (r0 == 0) goto L_0x005f
            long[] r0 = r8.zzams     // Catch:{ all -> 0x0061 }
            int r3 = r8.zzbbz     // Catch:{ all -> 0x0061 }
            r3 = r0[r3]     // Catch:{ all -> 0x0061 }
            int r0 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x0014
            goto L_0x005f
        L_0x0014:
            long r3 = r8.zzbcc     // Catch:{ all -> 0x0061 }
            int r0 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x001e
            if (r11 != 0) goto L_0x001e
            monitor-exit(r8)
            return r1
        L_0x001e:
            r11 = 0
            int r0 = r8.zzbbz     // Catch:{ all -> 0x0061 }
            r3 = -1
            r11 = -1
            r4 = 0
        L_0x0024:
            int r5 = r8.zzbca     // Catch:{ all -> 0x0061 }
            if (r0 == r5) goto L_0x0041
            long[] r5 = r8.zzams     // Catch:{ all -> 0x0061 }
            r6 = r5[r0]     // Catch:{ all -> 0x0061 }
            int r5 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r5 > 0) goto L_0x0041
            int[] r5 = r8.zzawq     // Catch:{ all -> 0x0061 }
            r5 = r5[r0]     // Catch:{ all -> 0x0061 }
            r5 = r5 & 1
            if (r5 == 0) goto L_0x0039
            r11 = r4
        L_0x0039:
            int r0 = r0 + 1
            int r5 = r8.zzbbu     // Catch:{ all -> 0x0061 }
            int r0 = r0 % r5
            int r4 = r4 + 1
            goto L_0x0024
        L_0x0041:
            if (r11 != r3) goto L_0x0045
            monitor-exit(r8)
            return r1
        L_0x0045:
            int r9 = r8.zzbbz     // Catch:{ all -> 0x0061 }
            int r9 = r9 + r11
            int r10 = r8.zzbbu     // Catch:{ all -> 0x0061 }
            int r9 = r9 % r10
            r8.zzbbz = r9     // Catch:{ all -> 0x0061 }
            int r9 = r8.zzbby     // Catch:{ all -> 0x0061 }
            int r9 = r9 + r11
            r8.zzbby = r9     // Catch:{ all -> 0x0061 }
            int r9 = r8.length     // Catch:{ all -> 0x0061 }
            int r9 = r9 - r11
            r8.length = r9     // Catch:{ all -> 0x0061 }
            long[] r9 = r8.zzamq     // Catch:{ all -> 0x0061 }
            int r10 = r8.zzbbz     // Catch:{ all -> 0x0061 }
            r10 = r9[r10]     // Catch:{ all -> 0x0061 }
            monitor-exit(r8)
            return r10
        L_0x005f:
            monitor-exit(r8)
            return r1
        L_0x0061:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzmh.zzd(long, boolean):long");
    }

    public final synchronized void zzei(long j) {
        this.zzbcc = Math.max(this.zzbcc, j);
    }

    public final synchronized boolean zzg(zzgw zzgw) {
        if (zzgw == null) {
            this.zzbce = true;
            return false;
        }
        this.zzbce = false;
        if (zzoq.zza(zzgw, this.zzbcf)) {
            return false;
        }
        this.zzbcf = zzgw;
        return true;
    }

    public final synchronized long zzhn() {
        return Math.max(this.zzbcb, this.zzbcc);
    }

    public final void zzht() {
        this.zzbby = 0;
        this.zzbbz = 0;
        this.zzbca = 0;
        this.length = 0;
        this.zzbcd = true;
    }

    public final void zzhu() {
        this.zzbcb = Long.MIN_VALUE;
        this.zzbcc = Long.MIN_VALUE;
    }

    public final int zzhv() {
        return this.zzbby + this.length;
    }

    public final synchronized boolean zzhw() {
        return this.length != 0;
    }

    public final synchronized zzgw zzhx() {
        if (this.zzbce) {
            return null;
        }
        return this.zzbcf;
    }

    public final synchronized long zzhy() {
        if (!zzhw()) {
            return -1;
        }
        int i = ((this.zzbbz + this.length) - 1) % this.zzbbu;
        this.zzbbz = (this.zzbbz + this.length) % this.zzbbu;
        this.zzbby += this.length;
        this.length = 0;
        return this.zzamq[i] + ((long) this.zzamp[i]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ea, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zza(long r6, int r8, long r9, int r11, com.google.android.gms.internal.ads.zzjn r12) {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = r5.zzbcd     // Catch:{ all -> 0x00eb }
            r1 = 0
            if (r0 == 0) goto L_0x000e
            r0 = r8 & 1
            if (r0 != 0) goto L_0x000c
            monitor-exit(r5)
            return
        L_0x000c:
            r5.zzbcd = r1     // Catch:{ all -> 0x00eb }
        L_0x000e:
            boolean r0 = r5.zzbce     // Catch:{ all -> 0x00eb }
            r2 = 1
            if (r0 != 0) goto L_0x0015
            r0 = 1
            goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            com.google.android.gms.internal.ads.zzoc.checkState(r0)     // Catch:{ all -> 0x00eb }
            r5.zzei(r6)     // Catch:{ all -> 0x00eb }
            long[] r0 = r5.zzams     // Catch:{ all -> 0x00eb }
            int r3 = r5.zzbca     // Catch:{ all -> 0x00eb }
            r0[r3] = r6     // Catch:{ all -> 0x00eb }
            long[] r6 = r5.zzamq     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbca     // Catch:{ all -> 0x00eb }
            r6[r7] = r9     // Catch:{ all -> 0x00eb }
            int[] r6 = r5.zzamp     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbca     // Catch:{ all -> 0x00eb }
            r6[r7] = r11     // Catch:{ all -> 0x00eb }
            int[] r6 = r5.zzawq     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbca     // Catch:{ all -> 0x00eb }
            r6[r7] = r8     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzjn[] r6 = r5.zzbbw     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbca     // Catch:{ all -> 0x00eb }
            r6[r7] = r12     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzgw[] r6 = r5.zzbbx     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbca     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzgw r8 = r5.zzbcf     // Catch:{ all -> 0x00eb }
            r6[r7] = r8     // Catch:{ all -> 0x00eb }
            int[] r6 = r5.zzbbv     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbca     // Catch:{ all -> 0x00eb }
            r6[r7] = r1     // Catch:{ all -> 0x00eb }
            int r6 = r5.length     // Catch:{ all -> 0x00eb }
            int r6 = r6 + r2
            r5.length = r6     // Catch:{ all -> 0x00eb }
            int r6 = r5.length     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbbu     // Catch:{ all -> 0x00eb }
            if (r6 != r7) goto L_0x00dc
            int r6 = r5.zzbbu     // Catch:{ all -> 0x00eb }
            int r6 = r6 + 1000
            int[] r7 = new int[r6]     // Catch:{ all -> 0x00eb }
            long[] r8 = new long[r6]     // Catch:{ all -> 0x00eb }
            long[] r9 = new long[r6]     // Catch:{ all -> 0x00eb }
            int[] r10 = new int[r6]     // Catch:{ all -> 0x00eb }
            int[] r11 = new int[r6]     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzjn[] r12 = new com.google.android.gms.internal.ads.zzjn[r6]     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzgw[] r0 = new com.google.android.gms.internal.ads.zzgw[r6]     // Catch:{ all -> 0x00eb }
            int r2 = r5.zzbbu     // Catch:{ all -> 0x00eb }
            int r3 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            int r2 = r2 - r3
            long[] r3 = r5.zzamq     // Catch:{ all -> 0x00eb }
            int r4 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r3, r4, r8, r1, r2)     // Catch:{ all -> 0x00eb }
            long[] r3 = r5.zzams     // Catch:{ all -> 0x00eb }
            int r4 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r3, r4, r9, r1, r2)     // Catch:{ all -> 0x00eb }
            int[] r3 = r5.zzawq     // Catch:{ all -> 0x00eb }
            int r4 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r3, r4, r10, r1, r2)     // Catch:{ all -> 0x00eb }
            int[] r3 = r5.zzamp     // Catch:{ all -> 0x00eb }
            int r4 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r3, r4, r11, r1, r2)     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzjn[] r3 = r5.zzbbw     // Catch:{ all -> 0x00eb }
            int r4 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r3, r4, r12, r1, r2)     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzgw[] r3 = r5.zzbbx     // Catch:{ all -> 0x00eb }
            int r4 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r3, r4, r0, r1, r2)     // Catch:{ all -> 0x00eb }
            int[] r3 = r5.zzbbv     // Catch:{ all -> 0x00eb }
            int r4 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r3, r4, r7, r1, r2)     // Catch:{ all -> 0x00eb }
            int r3 = r5.zzbbz     // Catch:{ all -> 0x00eb }
            long[] r4 = r5.zzamq     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r4, r1, r8, r2, r3)     // Catch:{ all -> 0x00eb }
            long[] r4 = r5.zzams     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r4, r1, r9, r2, r3)     // Catch:{ all -> 0x00eb }
            int[] r4 = r5.zzawq     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r4, r1, r10, r2, r3)     // Catch:{ all -> 0x00eb }
            int[] r4 = r5.zzamp     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r4, r1, r11, r2, r3)     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzjn[] r4 = r5.zzbbw     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r4, r1, r12, r2, r3)     // Catch:{ all -> 0x00eb }
            com.google.android.gms.internal.ads.zzgw[] r4 = r5.zzbbx     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r4, r1, r0, r2, r3)     // Catch:{ all -> 0x00eb }
            int[] r4 = r5.zzbbv     // Catch:{ all -> 0x00eb }
            java.lang.System.arraycopy(r4, r1, r7, r2, r3)     // Catch:{ all -> 0x00eb }
            r5.zzamq = r8     // Catch:{ all -> 0x00eb }
            r5.zzams = r9     // Catch:{ all -> 0x00eb }
            r5.zzawq = r10     // Catch:{ all -> 0x00eb }
            r5.zzamp = r11     // Catch:{ all -> 0x00eb }
            r5.zzbbw = r12     // Catch:{ all -> 0x00eb }
            r5.zzbbx = r0     // Catch:{ all -> 0x00eb }
            r5.zzbbv = r7     // Catch:{ all -> 0x00eb }
            r5.zzbbz = r1     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbbu     // Catch:{ all -> 0x00eb }
            r5.zzbca = r7     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbbu     // Catch:{ all -> 0x00eb }
            r5.length = r7     // Catch:{ all -> 0x00eb }
            r5.zzbbu = r6     // Catch:{ all -> 0x00eb }
            monitor-exit(r5)
            return
        L_0x00dc:
            int r6 = r5.zzbca     // Catch:{ all -> 0x00eb }
            int r6 = r6 + r2
            r5.zzbca = r6     // Catch:{ all -> 0x00eb }
            int r6 = r5.zzbca     // Catch:{ all -> 0x00eb }
            int r7 = r5.zzbbu     // Catch:{ all -> 0x00eb }
            if (r6 != r7) goto L_0x00e9
            r5.zzbca = r1     // Catch:{ all -> 0x00eb }
        L_0x00e9:
            monitor-exit(r5)
            return
        L_0x00eb:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzmh.zza(long, int, long, int, com.google.android.gms.internal.ads.zzjn):void");
    }
}
