package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdju extends zzdih<zzdnp, zzdno> {
    private final /* synthetic */ zzdjs zzgzk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdju(zzdjs zzdjs, Class cls) {
        super(cls);
        this.zzgzk = zzdjs;
    }

    public final /* bridge */ /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdnp zzdnp = (zzdnp) zzdte;
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        return (zzdno) zzdno.zzawi().zzb((zzdnp) zzdte).zzeu(0).zzbaf();
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdnp.zzay(zzdqk);
    }
}
