package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class zzdgh<V> extends zzdge<V, List<V>> {
    zzdgh(zzdet<? extends zzdhe<? extends V>> zzdet, boolean z) {
        super(zzdet, true);
        zzarn();
    }

    public final /* synthetic */ Object zzh(List list) {
        ArrayList zzdz = zzdfc.zzdz(list.size());
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            zzdej zzdej = (zzdej) it2.next();
            zzdz.add(zzdej != null ? zzdej.zzaqt() : null);
        }
        return Collections.unmodifiableList(zzdz);
    }
}
