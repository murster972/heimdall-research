package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzadi extends IInterface {
    void zza(zzacw zzacw) throws RemoteException;
}
