package com.google.android.gms.internal.ads;

final class zzakf implements zzazn {
    private final /* synthetic */ zzazl zzcxt;
    private final /* synthetic */ zzajf zzdaz;

    zzakf(zzakd zzakd, zzazl zzazl, zzajf zzajf) {
        this.zzcxt = zzazl;
        this.zzdaz = zzajf;
    }

    public final void run() {
        this.zzcxt.setException(new zzajr("Unable to obtain a JavascriptEngine."));
        this.zzdaz.release();
    }
}
