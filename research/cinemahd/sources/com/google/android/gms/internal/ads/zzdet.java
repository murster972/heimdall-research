package com.google.android.gms.internal.ads;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;

public abstract class zzdet<E> extends AbstractCollection<E> implements Serializable {
    private static final Object[] zzguh = new Object[0];

    zzdet() {
    }

    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public abstract boolean contains(Object obj);

    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public final Object[] toArray() {
        return toArray(zzguh);
    }

    /* access modifiers changed from: package-private */
    public int zza(Object[] objArr, int i) {
        zzdfp zzdfp = (zzdfp) iterator();
        while (zzdfp.hasNext()) {
            objArr[i] = zzdfp.next();
            i++;
        }
        return i;
    }

    /* renamed from: zzaqx */
    public abstract zzdfp<E> iterator();

    /* access modifiers changed from: package-private */
    public Object[] zzaqy() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public int zzaqz() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    public int zzara() {
        throw new UnsupportedOperationException();
    }

    public zzdeu<E> zzarb() {
        return isEmpty() ? zzdeu.zzard() : zzdeu.zzc(toArray());
    }

    /* access modifiers changed from: package-private */
    public abstract boolean zzarc();

    public final <T> T[] toArray(T[] tArr) {
        zzdei.checkNotNull(tArr);
        int size = size();
        if (tArr.length < size) {
            Object[] zzaqy = zzaqy();
            if (zzaqy != null) {
                return Arrays.copyOfRange(zzaqy, zzaqz(), zzara(), tArr.getClass());
            }
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), size);
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        zza(tArr, 0);
        return tArr;
    }
}
