package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzanc extends IInterface {
    void zza(zzalr zzalr) throws RemoteException;

    void zzdl(String str) throws RemoteException;
}
