package com.google.android.gms.internal.ads;

public final class zzbue implements zzdxg<zzbsu<zzbqb>> {
    private final zzdxp<zzbuy> zzfdq;
    private final zzbtv zzfje;

    private zzbue(zzbtv zzbtv, zzdxp<zzbuy> zzdxp) {
        this.zzfje = zzbtv;
        this.zzfdq = zzdxp;
    }

    public static zzbue zzc(zzbtv zzbtv, zzdxp<zzbuy> zzdxp) {
        return new zzbue(zzbtv, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), zzazd.zzdwi), "Cannot return null from a non-@Nullable @Provides method");
    }
}
