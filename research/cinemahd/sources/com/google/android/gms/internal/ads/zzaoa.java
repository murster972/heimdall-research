package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.zzq;

final class zzaoa implements Runnable {
    private final /* synthetic */ zzany zzdew;
    private final /* synthetic */ AdOverlayInfoParcel zzdfe;

    zzaoa(zzany zzany, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.zzdew = zzany;
        this.zzdfe = adOverlayInfoParcel;
    }

    public final void run() {
        zzq.zzkp();
        zzn.zza(this.zzdew.zzdex, this.zzdfe, true);
    }
}
