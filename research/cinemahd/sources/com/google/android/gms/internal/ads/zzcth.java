package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcth implements zzdxg<zzctf> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<String> zzelv;

    private zzcth(zzdxp<Context> zzdxp, zzdxp<String> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzelv = zzdxp2;
    }

    public static zzcth zzat(zzdxp<Context> zzdxp, zzdxp<String> zzdxp2) {
        return new zzcth(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzctf(this.zzejv.get(), this.zzelv.get());
    }
}
