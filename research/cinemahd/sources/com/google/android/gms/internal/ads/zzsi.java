package com.google.android.gms.internal.ads;

import java.util.concurrent.Future;

final /* synthetic */ class zzsi implements Runnable {
    private final zzazl zzbru;
    private final Future zzbrv;

    zzsi(zzazl zzazl, Future future) {
        this.zzbru = zzazl;
        this.zzbrv = future;
    }

    public final void run() {
        zzazl zzazl = this.zzbru;
        Future future = this.zzbrv;
        if (zzazl.isCancelled()) {
            future.cancel(true);
        }
    }
}
