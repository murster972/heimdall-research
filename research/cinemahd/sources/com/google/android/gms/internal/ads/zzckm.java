package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

public final class zzckm implements zzdxg<zzckk> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzbup> zzfyl;

    public zzckm(zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzbup> zzdxp3, zzdxp<Executor> zzdxp4) {
        this.zzejv = zzdxp;
        this.zzfdb = zzdxp2;
        this.zzfyl = zzdxp3;
        this.zzfei = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzckk(this.zzejv.get(), this.zzfdb.get(), this.zzfyl.get(), this.zzfei.get());
    }
}
