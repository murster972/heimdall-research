package com.google.android.gms.internal.ads;

import java.util.Map;

interface zzdsx {
    Map<?, ?> zzaq(Object obj);

    Map<?, ?> zzar(Object obj);

    boolean zzas(Object obj);

    Object zzat(Object obj);

    Object zzau(Object obj);

    zzdsv<?, ?> zzav(Object obj);

    int zzb(int i, Object obj, Object obj2);

    Object zze(Object obj, Object obj2);
}
