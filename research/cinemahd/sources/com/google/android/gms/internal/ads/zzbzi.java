package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbzi implements zzdxg<zzbze> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbyu> zzfoy;

    public zzbzi(zzdxp<Executor> zzdxp, zzdxp<zzbyu> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzfoy = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzbze(this.zzfcv.get(), this.zzfoy.get());
    }
}
