package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

public final class zzbuj implements zzdxg<zzbsu<zzbrb>> {
    private final zzdxp<Executor> zzfcv;
    private final zzbtv zzfje;

    private zzbuj(zzbtv zzbtv, zzdxp<Executor> zzdxp) {
        this.zzfje = zzbtv;
        this.zzfcv = zzdxp;
    }

    public static zzbuj zzd(zzbtv zzbtv, zzdxp<Executor> zzdxp) {
        return new zzbuj(zzbtv, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(this.zzfje.zzb(this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
