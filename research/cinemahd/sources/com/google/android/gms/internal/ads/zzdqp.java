package com.google.android.gms.internal.ads;

import java.util.Iterator;

public interface zzdqp extends Iterator<Byte> {
    byte nextByte();
}
