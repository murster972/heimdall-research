package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.Map;

public final class zzaog {
    private final zzbdi zzcza;
    private final boolean zzdfu;
    private final String zzdfv;

    public zzaog(zzbdi zzbdi, Map<String, String> map) {
        this.zzcza = zzbdi;
        this.zzdfv = map.get("forceOrientation");
        if (map.containsKey("allowOrientationChange")) {
            this.zzdfu = Boolean.parseBoolean(map.get("allowOrientationChange"));
        } else {
            this.zzdfu = true;
        }
    }

    public final void execute() {
        int i;
        if (this.zzcza == null) {
            zzayu.zzez("AdWebView is null");
            return;
        }
        if ("portrait".equalsIgnoreCase(this.zzdfv)) {
            zzq.zzks();
            i = 7;
        } else if ("landscape".equalsIgnoreCase(this.zzdfv)) {
            zzq.zzks();
            i = 6;
        } else if (this.zzdfu) {
            i = -1;
        } else {
            i = zzq.zzks().zzwo();
        }
        this.zzcza.setRequestedOrientation(i);
    }
}
