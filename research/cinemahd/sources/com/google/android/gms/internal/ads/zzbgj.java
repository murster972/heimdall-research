package com.google.android.gms.internal.ads;

public final class zzbgj implements zzdxg<String> {
    private final zzbga zzejr;

    public zzbgj(zzbga zzbga) {
        this.zzejr = zzbga;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza(zzve.zzoz(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
