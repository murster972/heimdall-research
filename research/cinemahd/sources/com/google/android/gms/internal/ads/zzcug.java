package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

final /* synthetic */ class zzcug implements Callable {
    private final zzcuh zzghh;

    zzcug(zzcuh zzcuh) {
        this.zzghh = zzcuh;
    }

    public final Object call() {
        return this.zzghh.zzanq();
    }
}
