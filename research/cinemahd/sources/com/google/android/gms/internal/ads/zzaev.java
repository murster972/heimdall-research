package com.google.android.gms.internal.ads;

import com.unity3d.ads.metadata.MediationMetaData;
import java.util.Map;

public final class zzaev implements zzafn<Object> {
    private final zzaey zzcws;

    public zzaev(zzaey zzaey) {
        this.zzcws = zzaey;
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get(MediationMetaData.KEY_NAME);
        if (str == null) {
            zzayu.zzez("App event with no name parameter.");
        } else {
            this.zzcws.onAppEvent(str, map.get("info"));
        }
    }
}
