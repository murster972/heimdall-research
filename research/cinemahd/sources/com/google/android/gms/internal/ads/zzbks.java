package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzbks implements zzdxg<zzbpw> {
    private final zzbkn zzfen;
    private final zzdxp<Set<zzbsu<zzbqb>>> zzfeo;

    public zzbks(zzbkn zzbkn, zzdxp<Set<zzbsu<zzbqb>>> zzdxp) {
        this.zzfen = zzbkn;
        this.zzfeo = zzdxp;
    }

    public static zzbpw zza(zzbkn zzbkn, Set<zzbsu<zzbqb>> set) {
        return (zzbpw) zzdxm.zza(zzbkn.zza(set), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfen, this.zzfeo.get());
    }
}
