package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.telephony.TelephonyManager;
import com.google.android.gms.ads.internal.zzq;

@TargetApi(26)
public class zzawp extends zzawm {
    public final zzte zza(Context context, TelephonyManager telephonyManager) {
        zzq.zzkq();
        if (zzawb.zzq(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return telephonyManager.isDataEnabled() ? zzte.ENUM_TRUE : zzte.ENUM_FALSE;
        }
        return zzte.ENUM_FALSE;
    }
}
