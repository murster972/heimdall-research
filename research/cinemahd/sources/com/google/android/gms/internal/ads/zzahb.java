package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzahb extends IInterface {
    void destroy() throws RemoteException;

    zzxb getVideoController() throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzahg zzahg) throws RemoteException;

    void zzr(IObjectWrapper iObjectWrapper) throws RemoteException;
}
