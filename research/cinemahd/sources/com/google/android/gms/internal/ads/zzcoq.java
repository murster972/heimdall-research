package com.google.android.gms.internal.ads;

import android.os.RemoteException;

public final class zzcoq implements zzty {
    private zzvg zzgdd;

    public final synchronized void onAdClicked() {
        if (this.zzgdd != null) {
            try {
                this.zzgdd.onAdClicked();
            } catch (RemoteException e) {
                zzayu.zzd("Remote Exception at onAdClicked.", e);
            }
        }
    }

    public final synchronized void zzb(zzvg zzvg) {
        this.zzgdd = zzvg;
    }
}
