package com.google.android.gms.internal.ads;

public final class zzbur implements zzdxg<zzbsu<zzbov>> {
    private final zzdxp<zzbsl> zzfdd;

    public zzbur(zzdxp<zzbsl> zzdxp) {
        this.zzfdd = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
