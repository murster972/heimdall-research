package com.google.android.gms.internal.ads;

public final class zzabl {
    public static zzaan<Boolean> zzcuu = zzaan.zzf("gads:webview:permission:disabled", false);
    public static zzaan<Boolean> zzcuv = zzaan.zzf("gads:corewebview:adwebview_factory:enable", false);
    public static zzaan<Boolean> zzcuw = zzaan.zzf("gads:corewebview:javascript_engine", false);
}
