package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

public final class zzbun implements zzo {
    private final zzbqj zzfjf;
    private final zzbsq zzfjg;

    public zzbun(zzbqj zzbqj, zzbsq zzbsq) {
        this.zzfjf = zzbqj;
        this.zzfjg = zzbsq;
    }

    public final void onPause() {
        this.zzfjf.onPause();
    }

    public final void onResume() {
        this.zzfjf.onResume();
    }

    public final void zzte() {
        this.zzfjf.zzte();
        this.zzfjg.onHide();
    }

    public final void zztf() {
        this.zzfjf.zztf();
        this.zzfjg.zzahy();
    }
}
