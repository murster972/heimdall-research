package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;

public final class zzcrr implements zzcub<zzcrs> {
    private final zzdhd zzfov;
    private final Context zzyv;

    zzcrr(Context context, zzdhd zzdhd) {
        this.zzyv = context;
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcrs> zzanc() {
        return this.zzfov.zzd(new zzcrq(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcrs zzanh() throws Exception {
        zzq.zzkq();
        String zzaz = zzawb.zzaz(this.zzyv);
        String str = "";
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpy)).booleanValue()) {
            str = this.zzyv.getSharedPreferences("mobileads_consent", 0).getString("fc_consent", str);
        }
        zzq.zzkq();
        return new zzcrs(zzaz, str, zzawb.zzba(this.zzyv));
    }
}
