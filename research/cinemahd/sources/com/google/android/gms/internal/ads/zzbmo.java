package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class zzbmo {
    private final Executor executor;
    private volatile boolean zzadd = true;
    private final ScheduledExecutorService zzffx;
    private final zzdhe<zzbmj> zzffy;

    public zzbmo(Executor executor2, ScheduledExecutorService scheduledExecutorService, zzdhe<zzbmj> zzdhe) {
        this.executor = executor2;
        this.zzffx = scheduledExecutorService;
        this.zzffy = zzdhe;
    }

    /* access modifiers changed from: private */
    public final void zzagu() {
        zzazd.zzdwi.execute(new zzbms(this));
    }

    public final boolean isLoading() {
        return this.zzadd;
    }

    public final void zza(zzdgt<zzbmd> zzdgt) {
        zzdgs.zza(this.zzffy, new zzbmr(this, zzdgt), this.executor);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzagv() {
        this.zzadd = false;
    }

    /* access modifiers changed from: private */
    public final void zza(List<? extends zzdhe<? extends zzbmd>> list, zzdgt<zzbmd> zzdgt) {
        if (list == null || list.isEmpty()) {
            this.executor.execute(new zzbmn(zzdgt));
            return;
        }
        zzdhe<O> zzaj = zzdgs.zzaj(null);
        for (zzdhe zzbmp : list) {
            zzaj = zzdgs.zzb(zzdgs.zzb(zzaj, Throwable.class, new zzbmq(zzdgt), this.executor), new zzbmp(this, zzdgt, zzbmp), this.executor);
        }
        zzdgs.zza(zzaj, new zzbmu(this, zzdgt), this.executor);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzdgt zzdgt, zzdhe zzdhe, zzbmd zzbmd) throws Exception {
        if (zzbmd != null) {
            zzdgt.onSuccess(zzbmd);
        }
        return zzdgs.zza(zzdhe, zzabj.zzcur.get().longValue(), TimeUnit.MILLISECONDS, this.zzffx);
    }
}
