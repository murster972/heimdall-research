package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class zzamg<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends zzalg {
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zzded;
    private final NETWORK_EXTRAS zzdee;

    public zzamg(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter, NETWORK_EXTRAS network_extras) {
        this.zzded = mediationAdapter;
        this.zzdee = network_extras;
    }

    private static boolean zzc(zzug zzug) {
        if (zzug.zzccb) {
            return true;
        }
        zzve.zzou();
        return zzayk.zzxd();
    }

    private final SERVER_PARAMETERS zzdk(String str) throws RemoteException {
        HashMap hashMap;
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                hashMap = new HashMap(jSONObject.length());
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    hashMap.put(next, jSONObject.getString(next));
                }
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else {
            hashMap = new HashMap(0);
        }
        Class<SERVER_PARAMETERS> serverParametersType = this.zzded.getServerParametersType();
        if (serverParametersType == null) {
            return null;
        }
        SERVER_PARAMETERS server_parameters = (MediationServerParameters) serverParametersType.newInstance();
        server_parameters.load(hashMap);
        return server_parameters;
    }

    public final void destroy() throws RemoteException {
        try {
            this.zzded.destroy();
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final Bundle getInterstitialAdapterInfo() {
        return new Bundle();
    }

    public final zzxb getVideoController() {
        return null;
    }

    public final boolean isInitialized() {
        return true;
    }

    public final void pause() throws RemoteException {
        throw new RemoteException();
    }

    public final void resume() throws RemoteException {
        throw new RemoteException();
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final void showInterstitial() throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationInterstitialAdapter: ".concat(valueOf) : new String("Not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzayu.zzea("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzded).showInterstitial();
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final void showVideo() {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzagp zzagp, List<zzagx> list) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzarz zzarz, List<String> list) {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzarz zzarz, String str2) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali, zzaby zzaby, List<String> list) {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, zzali zzali) throws RemoteException {
        zza(iObjectWrapper, zzuj, zzug, str, (String) null, zzali);
    }

    public final void zza(zzug zzug, String str) {
    }

    public final void zza(zzug zzug, String str, String str2) {
    }

    public final void zzb(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException {
    }

    public final void zzs(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final IObjectWrapper zzsk() throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationBannerAdapter: ".concat(valueOf) : new String("Not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        try {
            return ObjectWrapper.a(((MediationBannerAdapter) mediationAdapter).getBannerView());
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final zzall zzsl() {
        return null;
    }

    public final zzalq zzsm() {
        return null;
    }

    public final Bundle zzsn() {
        return new Bundle();
    }

    public final Bundle zzso() {
        return new Bundle();
    }

    public final boolean zzsp() {
        return false;
    }

    public final zzade zzsq() {
        return null;
    }

    public final zzalr zzsr() {
        return null;
    }

    public final void zzt(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, String str2, zzali zzali) throws RemoteException {
        AdSize adSize;
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationBannerAdapter: ".concat(valueOf) : new String("Not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        zzayu.zzea("Requesting banner ad from adapter.");
        try {
            MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter) this.zzded;
            zzamf zzamf = new zzamf(zzali);
            Activity activity = (Activity) ObjectWrapper.a(iObjectWrapper);
            MediationServerParameters zzdk = zzdk(str);
            int i = 0;
            AdSize[] adSizeArr = {AdSize.SMART_BANNER, AdSize.BANNER, AdSize.IAB_MRECT, AdSize.IAB_BANNER, AdSize.IAB_LEADERBOARD, AdSize.IAB_WIDE_SKYSCRAPER};
            while (true) {
                if (i < 6) {
                    if (adSizeArr[i].getWidth() == zzuj.width && adSizeArr[i].getHeight() == zzuj.height) {
                        adSize = adSizeArr[i];
                        break;
                    }
                    i++;
                } else {
                    adSize = new AdSize(zzb.zza(zzuj.width, zzuj.height, zzuj.zzabg));
                    break;
                }
            }
            mediationBannerAdapter.requestBannerAd(zzamf, activity, zzdk, adSize, zzamr.zza(zzug, zzc(zzug)), this.zzdee);
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException {
        zza(iObjectWrapper, zzug, str, (String) null, zzali);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali) throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationInterstitialAdapter: ".concat(valueOf) : new String("Not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzayu.zzea("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzded).requestInterstitialAd(new zzamf(zzali), (Activity) ObjectWrapper.a(iObjectWrapper), zzdk(str), zzamr.zza(zzug, zzc(zzug)), this.zzdee);
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }
}
