package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdna extends zzdrt<zzdna, zza> implements zzdtg {
    private static volatile zzdtn<zzdna> zzdz;
    /* access modifiers changed from: private */
    public static final zzdna zzhcv;
    private String zzhcs = "";
    private zzdqk zzhct = zzdqk.zzhhx;
    private int zzhcu;

    public static final class zza extends zzdrt.zzb<zzdna, zza> implements zzdtg {
        private zza() {
            super(zzdna.zzhcv);
        }

        public final zza zzaw(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdna) this.zzhmp).zzav(zzdqk);
            return this;
        }

        public final zza zzb(zzb zzb) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdna) this.zzhmp).zza(zzb);
            return this;
        }

        public final zza zzhb(String str) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdna) this.zzhmp).zzha(str);
            return this;
        }

        /* synthetic */ zza(zzdnb zzdnb) {
            this();
        }
    }

    public enum zzb implements zzdry {
        UNKNOWN_KEYMATERIAL(0),
        SYMMETRIC(1),
        ASYMMETRIC_PRIVATE(2),
        ASYMMETRIC_PUBLIC(3),
        REMOTE(4),
        UNRECOGNIZED(-1);
        
        private static final zzdrx<zzb> zzen = null;
        private final int value;

        static {
            zzen = new zzdnc();
        }

        private zzb(int i) {
            this.value = i;
        }

        public static zzb zzeo(int i) {
            if (i == 0) {
                return UNKNOWN_KEYMATERIAL;
            }
            if (i == 1) {
                return SYMMETRIC;
            }
            if (i == 2) {
                return ASYMMETRIC_PRIVATE;
            }
            if (i == 3) {
                return ASYMMETRIC_PUBLIC;
            }
            if (i != 4) {
                return null;
            }
            return REMOTE;
        }

        public final String toString() {
            StringBuilder sb = new StringBuilder("<");
            sb.append(zzb.class.getName());
            sb.append('@');
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            if (this != UNRECOGNIZED) {
                sb.append(" number=");
                sb.append(zzae());
            }
            sb.append(" name=");
            sb.append(name());
            sb.append('>');
            return sb.toString();
        }

        public final int zzae() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
    }

    static {
        zzdna zzdna = new zzdna();
        zzhcv = zzdna;
        zzdrt.zza(zzdna.class, zzdna);
    }

    private zzdna() {
    }

    /* access modifiers changed from: private */
    public final void zza(zzb zzb2) {
        this.zzhcu = zzb2.zzae();
    }

    /* access modifiers changed from: private */
    public final void zzav(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhct = zzdqk;
    }

    public static zza zzavl() {
        return (zza) zzhcv.zzazt();
    }

    public static zzdna zzavm() {
        return zzhcv;
    }

    /* access modifiers changed from: private */
    public final void zzha(String str) {
        str.getClass();
        this.zzhcs = str;
    }

    public final String zzavi() {
        return this.zzhcs;
    }

    public final zzdqk zzavj() {
        return this.zzhct;
    }

    public final zzb zzavk() {
        zzb zzeo = zzb.zzeo(this.zzhcu);
        return zzeo == null ? zzb.UNRECOGNIZED : zzeo;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnb.zzdk[i - 1]) {
            case 1:
                return new zzdna();
            case 2:
                return new zza((zzdnb) null);
            case 3:
                return zzdrt.zza((zzdte) zzhcv, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001Ȉ\u0002\n\u0003\f", new Object[]{"zzhcs", "zzhct", "zzhcu"});
            case 4:
                return zzhcv;
            case 5:
                zzdtn<zzdna> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdna.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhcv);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
