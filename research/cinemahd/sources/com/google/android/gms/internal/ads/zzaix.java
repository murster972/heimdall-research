package com.google.android.gms.internal.ads;

final class zzaix implements zzafn<zzajq> {
    private final /* synthetic */ zzajj zzczs;
    private final /* synthetic */ zzaif zzczt;
    private final /* synthetic */ zzais zzczu;

    zzaix(zzais zzais, zzajj zzajj, zzaif zzaif) {
        this.zzczu = zzais;
        this.zzczs = zzajj;
        this.zzczt = zzaif;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0043, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zza(java.lang.Object r2, java.util.Map r3) {
        /*
            r1 = this;
            com.google.android.gms.internal.ads.zzajq r2 = (com.google.android.gms.internal.ads.zzajq) r2
            com.google.android.gms.internal.ads.zzais r2 = r1.zzczu
            java.lang.Object r2 = r2.lock
            monitor-enter(r2)
            com.google.android.gms.internal.ads.zzajj r3 = r1.zzczs     // Catch:{ all -> 0x0044 }
            int r3 = r3.getStatus()     // Catch:{ all -> 0x0044 }
            r0 = -1
            if (r3 == r0) goto L_0x0042
            com.google.android.gms.internal.ads.zzajj r3 = r1.zzczs     // Catch:{ all -> 0x0044 }
            int r3 = r3.getStatus()     // Catch:{ all -> 0x0044 }
            r0 = 1
            if (r3 != r0) goto L_0x001c
            goto L_0x0042
        L_0x001c:
            com.google.android.gms.internal.ads.zzais r3 = r1.zzczu     // Catch:{ all -> 0x0044 }
            r0 = 0
            int unused = r3.status = r0     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzais r3 = r1.zzczu     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzaxh r3 = r3.zzczm     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzaif r0 = r1.zzczt     // Catch:{ all -> 0x0044 }
            r3.zzh(r0)     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzajj r3 = r1.zzczs     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzaif r0 = r1.zzczt     // Catch:{ all -> 0x0044 }
            r3.zzm(r0)     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzais r3 = r1.zzczu     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzajj r0 = r1.zzczs     // Catch:{ all -> 0x0044 }
            com.google.android.gms.internal.ads.zzajj unused = r3.zzczo = r0     // Catch:{ all -> 0x0044 }
            java.lang.String r3 = "Successfully loaded JS Engine."
            com.google.android.gms.internal.ads.zzavs.zzed(r3)     // Catch:{ all -> 0x0044 }
            monitor-exit(r2)     // Catch:{ all -> 0x0044 }
            return
        L_0x0042:
            monitor-exit(r2)     // Catch:{ all -> 0x0044 }
            return
        L_0x0044:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0044 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzaix.zza(java.lang.Object, java.util.Map):void");
    }
}
