package com.google.android.gms.internal.ads;

public interface zzbus extends zzboe<zzbup> {
    zzbup zzaek();

    zzbus zzb(zzcns zzcns);

    zzbus zzd(zzbod zzbod);

    zzbus zzd(zzbrm zzbrm);
}
