package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.ads.internal.zzq;

final class zzcki implements zzbuv {
    private final zzazb zzbli;
    private final zzczl zzfmp;
    private final zzdhe<zzbtw> zzfzp;
    private final zzbdi zzfzq;
    private final Context zzup;

    private zzcki(Context context, zzazb zzazb, zzdhe<zzbtw> zzdhe, zzczl zzczl, zzbdi zzbdi) {
        this.zzup = context;
        this.zzbli = zzazb;
        this.zzfzp = zzdhe;
        this.zzfmp = zzczl;
        this.zzfzq = zzbdi;
    }

    public final void zza(boolean z, Context context) {
        this.zzfzq.zzax(true);
        zzq.zzkq();
        zzg zzg = new zzg(false, zzawb.zzbb(this.zzup), false, 0.0f, -1, z, this.zzfmp.zzglv, false);
        zzq.zzkp();
        zzbun zzaeo = ((zzbtw) zzdgs.zzc(this.zzfzp)).zzaeo();
        zzbdi zzbdi = this.zzfzq;
        zzczl zzczl = this.zzfmp;
        int i = zzczl.zzglw;
        zzazb zzazb = this.zzbli;
        String str = zzczl.zzdkp;
        zzczp zzczp = zzczl.zzglo;
        Context context2 = context;
        zzn.zza(context2, new AdOverlayInfoParcel((zzty) null, (zzo) zzaeo, (zzt) null, zzbdi, i, zzazb, str, zzg, zzczp.zzdhr, zzczp.zzdht), true);
    }
}
