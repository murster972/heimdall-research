package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbnv implements zzdxg<zzbnw> {
    private final zzdxp<zzceq> zzekp;
    private final zzdxp<zzavu> zzemi;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<Context> zzfhb;

    private zzbnv(zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2, zzdxp<zzazb> zzdxp3, zzdxp<zzavu> zzdxp4, zzdxp<zzceq> zzdxp5) {
        this.zzfhb = zzdxp;
        this.zzfep = zzdxp2;
        this.zzfdb = zzdxp3;
        this.zzemi = zzdxp4;
        this.zzekp = zzdxp5;
    }

    public static zzbnv zzb(zzdxp<Context> zzdxp, zzdxp<zzczu> zzdxp2, zzdxp<zzazb> zzdxp3, zzdxp<zzavu> zzdxp4, zzdxp<zzceq> zzdxp5) {
        return new zzbnv(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5);
    }

    public final /* synthetic */ Object get() {
        return new zzbnw(this.zzfhb.get(), this.zzfep.get(), this.zzfdb.get(), this.zzemi.get(), this.zzekp.get());
    }
}
