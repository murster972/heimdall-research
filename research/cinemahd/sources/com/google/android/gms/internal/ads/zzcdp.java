package com.google.android.gms.internal.ads;

public final class zzcdp implements zzdxg<zzcdt> {
    private static final zzcdp zzfsp = new zzcdp();

    public static zzcdp zzalf() {
        return zzfsp;
    }

    public final /* synthetic */ Object get() {
        return (zzcdt) zzdxm.zza(new zzcdt("ttc", zzdco.SIGNALS, zzdco.RENDERER), "Cannot return null from a non-@Nullable @Provides method");
    }
}
