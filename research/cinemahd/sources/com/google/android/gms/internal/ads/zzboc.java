package com.google.android.gms.internal.ads;

public final class zzboc implements zzdxg<zzbqp> {
    private final zzdxp<zzbqp> zzfhe;

    private zzboc(zzdxp<zzbqp> zzdxp) {
        this.zzfhe = zzdxp;
    }

    public static zzboc zzf(zzdxp<zzbqp> zzdxp) {
        return new zzboc(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbqp) zzdxm.zza(this.zzfhe.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
