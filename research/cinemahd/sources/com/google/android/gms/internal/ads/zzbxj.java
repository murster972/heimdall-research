package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.uwetrottmann.trakt5.TraktV2;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.Executor;
import okhttp3.internal.cache.DiskLruCache;

public final class zzbxj {
    private final Executor executor;
    private final zzaby zzddz;
    private final zzavu zzdrk;
    private final Executor zzfci;
    private final zzczu zzfgl;
    private final zzbws zzfkc;
    private final zzbww zzfli;
    private final zzbwq zzflq;
    private final zzbxr zzfnn;
    private final Context zzup;

    public zzbxj(Context context, zzavu zzavu, zzczu zzczu, zzbww zzbww, zzbws zzbws, zzbxr zzbxr, Executor executor2, Executor executor3, zzbwq zzbwq) {
        this.zzup = context;
        this.zzdrk = zzavu;
        this.zzfgl = zzczu;
        this.zzddz = zzczu.zzddz;
        this.zzfli = zzbww;
        this.zzfkc = zzbws;
        this.zzfnn = zzbxr;
        this.zzfci = executor2;
        this.executor = executor3;
        this.zzflq = zzbwq;
    }

    public final void zza(zzbxz zzbxz) {
        this.zzfci.execute(new zzbxm(this, zzbxz));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(ViewGroup viewGroup) {
        boolean z = viewGroup != null;
        if (this.zzfkc.zzaje() == null) {
            return;
        }
        if (2 == this.zzfkc.zzaja() || 1 == this.zzfkc.zzaja()) {
            this.zzdrk.zza(this.zzfgl.zzgmm, String.valueOf(this.zzfkc.zzaja()), z);
        } else if (6 == this.zzfkc.zzaja()) {
            this.zzdrk.zza(this.zzfgl.zzgmm, TraktV2.API_VERSION, z);
            this.zzdrk.zza(this.zzfgl.zzgmm, DiskLruCache.VERSION_1, z);
        }
    }

    public final void zzc(zzbxz zzbxz) {
        if (zzbxz != null && this.zzfnn != null && zzbxz.zzakd() != null) {
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcqd)).booleanValue() || this.zzfli.zzajm()) {
                try {
                    zzbxz.zzakd().addView(this.zzfnn.zzakj());
                } catch (zzbdv e) {
                    zzavs.zza("web view can not be obtained", e);
                }
            }
        }
    }

    /* JADX WARNING: type inference failed for: r5v4, types: [android.view.View] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:102:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0105 A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zzd(com.google.android.gms.internal.ads.zzbxz r10) {
        /*
            r9 = this;
            com.google.android.gms.internal.ads.zzbww r0 = r9.zzfli
            boolean r0 = r0.zzajo()
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0015
            com.google.android.gms.internal.ads.zzbww r0 = r9.zzfli
            boolean r0 = r0.zzajn()
            if (r0 == 0) goto L_0x0013
            goto L_0x0015
        L_0x0013:
            r0 = 0
            goto L_0x0016
        L_0x0015:
            r0 = 1
        L_0x0016:
            r3 = 0
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = "1098"
            java.lang.String r4 = "3011"
            java.lang.String[] r0 = new java.lang.String[]{r0, r4}
            r4 = 0
        L_0x0022:
            r5 = 2
            if (r4 >= r5) goto L_0x0037
            r5 = r0[r4]
            android.view.View r5 = r10.zzgb(r5)
            if (r5 == 0) goto L_0x0034
            boolean r6 = r5 instanceof android.view.ViewGroup
            if (r6 == 0) goto L_0x0034
            android.view.ViewGroup r5 = (android.view.ViewGroup) r5
            goto L_0x0038
        L_0x0034:
            int r4 = r4 + 1
            goto L_0x0022
        L_0x0037:
            r5 = r3
        L_0x0038:
            if (r5 == 0) goto L_0x003c
            r0 = 1
            goto L_0x003d
        L_0x003c:
            r0 = 0
        L_0x003d:
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams
            r6 = -2
            r4.<init>(r6, r6)
            com.google.android.gms.internal.ads.zzbws r6 = r9.zzfkc
            android.view.View r6 = r6.zzajb()
            if (r6 == 0) goto L_0x0061
            com.google.android.gms.internal.ads.zzbws r6 = r9.zzfkc
            android.view.View r6 = r6.zzajb()
            com.google.android.gms.internal.ads.zzaby r7 = r9.zzddz
            if (r7 != 0) goto L_0x0056
            goto L_0x0095
        L_0x0056:
            if (r0 != 0) goto L_0x0095
            int r7 = r7.zzbjz
            zza((android.widget.RelativeLayout.LayoutParams) r4, (int) r7)
            r6.setLayoutParams(r4)
            goto L_0x0095
        L_0x0061:
            com.google.android.gms.internal.ads.zzbws r6 = r9.zzfkc
            com.google.android.gms.internal.ads.zzaca r6 = r6.zzrh()
            boolean r6 = r6 instanceof com.google.android.gms.internal.ads.zzabp
            if (r6 != 0) goto L_0x006d
            r6 = r3
            goto L_0x0095
        L_0x006d:
            com.google.android.gms.internal.ads.zzbws r6 = r9.zzfkc
            com.google.android.gms.internal.ads.zzaca r6 = r6.zzrh()
            com.google.android.gms.internal.ads.zzabp r6 = (com.google.android.gms.internal.ads.zzabp) r6
            if (r0 != 0) goto L_0x007e
            int r7 = r6.zzra()
            zza((android.widget.RelativeLayout.LayoutParams) r4, (int) r7)
        L_0x007e:
            com.google.android.gms.internal.ads.zzabs r7 = new com.google.android.gms.internal.ads.zzabs
            android.content.Context r8 = r9.zzup
            r7.<init>(r8, r6, r4)
            com.google.android.gms.internal.ads.zzzc<java.lang.String> r4 = com.google.android.gms.internal.ads.zzzn.zzcme
            com.google.android.gms.internal.ads.zzzj r6 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r4 = r6.zzd(r4)
            java.lang.CharSequence r4 = (java.lang.CharSequence) r4
            r7.setContentDescription(r4)
            r6 = r7
        L_0x0095:
            r4 = -1
            if (r6 == 0) goto L_0x00da
            android.view.ViewParent r7 = r6.getParent()
            boolean r7 = r7 instanceof android.view.ViewGroup
            if (r7 == 0) goto L_0x00a9
            android.view.ViewParent r7 = r6.getParent()
            android.view.ViewGroup r7 = (android.view.ViewGroup) r7
            r7.removeView(r6)
        L_0x00a9:
            if (r0 == 0) goto L_0x00b2
            r5.removeAllViews()
            r5.addView(r6)
            goto L_0x00d3
        L_0x00b2:
            com.google.android.gms.ads.formats.AdChoicesView r0 = new com.google.android.gms.ads.formats.AdChoicesView
            android.view.View r5 = r10.zzaga()
            android.content.Context r5 = r5.getContext()
            r0.<init>(r5)
            android.widget.FrameLayout$LayoutParams r5 = new android.widget.FrameLayout$LayoutParams
            r5.<init>(r4, r4)
            r0.setLayoutParams(r5)
            r0.addView(r6)
            android.widget.FrameLayout r5 = r10.zzakd()
            if (r5 == 0) goto L_0x00d3
            r5.addView(r0)
        L_0x00d3:
            java.lang.String r0 = r10.zzakc()
            r10.zza(r0, r6, r2)
        L_0x00da:
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcqc
            com.google.android.gms.internal.ads.zzzj r2 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r2.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x00ef
            r9.zzc(r10)
        L_0x00ef:
            java.lang.String[] r0 = com.google.android.gms.internal.ads.zzbxh.zzfna
            int r2 = r0.length
        L_0x00f2:
            if (r1 >= r2) goto L_0x0105
            r5 = r0[r1]
            android.view.View r5 = r10.zzgb(r5)
            boolean r6 = r5 instanceof android.view.ViewGroup
            if (r6 == 0) goto L_0x0102
            r0 = r5
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            goto L_0x0106
        L_0x0102:
            int r1 = r1 + 1
            goto L_0x00f2
        L_0x0105:
            r0 = r3
        L_0x0106:
            java.util.concurrent.Executor r1 = r9.executor
            com.google.android.gms.internal.ads.zzbxl r2 = new com.google.android.gms.internal.ads.zzbxl
            r2.<init>(r9, r0)
            r1.execute(r2)
            if (r0 == 0) goto L_0x01be
            boolean r1 = r9.zza((android.view.ViewGroup) r0)
            if (r1 == 0) goto L_0x012f
            com.google.android.gms.internal.ads.zzbws r1 = r9.zzfkc
            com.google.android.gms.internal.ads.zzbdi r1 = r1.zzajf()
            if (r1 == 0) goto L_0x01be
            com.google.android.gms.internal.ads.zzbws r1 = r9.zzfkc
            com.google.android.gms.internal.ads.zzbdi r1 = r1.zzajf()
            com.google.android.gms.internal.ads.zzbxo r2 = new com.google.android.gms.internal.ads.zzbxo
            r2.<init>(r9, r10, r0)
            r1.zza((com.google.android.gms.internal.ads.zzabw) r2)
            return
        L_0x012f:
            r0.removeAllViews()
            android.view.View r1 = r10.zzaga()
            if (r1 == 0) goto L_0x013d
            android.content.Context r1 = r1.getContext()
            goto L_0x013e
        L_0x013d:
            r1 = r3
        L_0x013e:
            if (r1 == 0) goto L_0x01be
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r2 = com.google.android.gms.internal.ads.zzzn.zzcmd
            com.google.android.gms.internal.ads.zzzj r5 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r2 = r5.zzd(r2)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x0165
            com.google.android.gms.internal.ads.zzbwq r2 = r9.zzflq
            com.google.android.gms.internal.ads.zzacd r2 = r2.zzrq()
            if (r2 == 0) goto L_0x01be
            com.google.android.gms.dynamic.IObjectWrapper r2 = r2.zzre()     // Catch:{ RemoteException -> 0x015f }
            goto L_0x0171
        L_0x015f:
            java.lang.String r10 = "Could not get main image drawable"
            com.google.android.gms.internal.ads.zzayu.zzez(r10)
            return
        L_0x0165:
            com.google.android.gms.internal.ads.zzbws r2 = r9.zzfkc
            com.google.android.gms.internal.ads.zzaci r2 = r2.zzajc()
            if (r2 == 0) goto L_0x01be
            com.google.android.gms.dynamic.IObjectWrapper r2 = r2.zzrc()     // Catch:{ RemoteException -> 0x01b9 }
        L_0x0171:
            if (r2 == 0) goto L_0x01be
            java.lang.Object r2 = com.google.android.gms.dynamic.ObjectWrapper.a((com.google.android.gms.dynamic.IObjectWrapper) r2)
            android.graphics.drawable.Drawable r2 = (android.graphics.drawable.Drawable) r2
            if (r2 == 0) goto L_0x01be
            android.widget.ImageView r5 = new android.widget.ImageView
            r5.<init>(r1)
            r5.setImageDrawable(r2)
            if (r10 == 0) goto L_0x0189
            com.google.android.gms.dynamic.IObjectWrapper r3 = r10.zzakf()
        L_0x0189:
            if (r3 == 0) goto L_0x01a8
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r10 = com.google.android.gms.internal.ads.zzzn.zzcqe
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r10 = r1.zzd(r10)
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r10 = r10.booleanValue()
            if (r10 != 0) goto L_0x019e
            goto L_0x01a8
        L_0x019e:
            java.lang.Object r10 = com.google.android.gms.dynamic.ObjectWrapper.a((com.google.android.gms.dynamic.IObjectWrapper) r3)
            android.widget.ImageView$ScaleType r10 = (android.widget.ImageView.ScaleType) r10
            r5.setScaleType(r10)
            goto L_0x01ad
        L_0x01a8:
            android.widget.ImageView$ScaleType r10 = android.widget.ImageView.ScaleType.CENTER_INSIDE
            r5.setScaleType(r10)
        L_0x01ad:
            android.widget.FrameLayout$LayoutParams r10 = new android.widget.FrameLayout$LayoutParams
            r10.<init>(r4, r4)
            r5.setLayoutParams(r10)
            r0.addView(r5)
            goto L_0x01be
        L_0x01b9:
            java.lang.String r10 = "Could not get drawable from image"
            com.google.android.gms.internal.ads.zzayu.zzez(r10)
        L_0x01be:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbxj.zzd(com.google.android.gms.internal.ads.zzbxz):void");
    }

    private static void zza(RelativeLayout.LayoutParams layoutParams, int i) {
        if (i == 0) {
            layoutParams.addRule(10);
            layoutParams.addRule(9);
        } else if (i == 2) {
            layoutParams.addRule(12);
            layoutParams.addRule(11);
        } else if (i != 3) {
            layoutParams.addRule(10);
            layoutParams.addRule(11);
        } else {
            layoutParams.addRule(12);
            layoutParams.addRule(9);
        }
    }

    /* access modifiers changed from: private */
    public static boolean zza(zzbxz zzbxz, String[] strArr) {
        Map<String, WeakReference<View>> zzaka = zzbxz.zzaka();
        if (zzaka == null) {
            return false;
        }
        for (String str : strArr) {
            if (zzaka.get(str) != null) {
                return true;
            }
        }
        return false;
    }

    public final boolean zza(ViewGroup viewGroup) {
        FrameLayout.LayoutParams layoutParams;
        View zzaje = this.zzfkc.zzaje();
        if (zzaje == null) {
            return false;
        }
        viewGroup.removeAllViews();
        if (zzaje.getParent() instanceof ViewGroup) {
            ((ViewGroup) zzaje.getParent()).removeView(zzaje);
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcmh)).booleanValue()) {
            layoutParams = new FrameLayout.LayoutParams(-1, -1, 17);
        } else {
            layoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
        }
        viewGroup.addView(zzaje, layoutParams);
        return true;
    }
}
