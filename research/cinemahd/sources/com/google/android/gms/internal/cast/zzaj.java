package com.google.android.gms.internal.cast;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzaj extends IInterface {
    void zza(long j, long j2) throws RemoteException;

    int zzs() throws RemoteException;
}
