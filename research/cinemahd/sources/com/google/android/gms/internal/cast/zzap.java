package com.google.android.gms.internal.cast;

import android.content.Intent;
import android.support.v4.media.session.MediaSessionCompat;
import android.view.KeyEvent;

final class zzap extends MediaSessionCompat.Callback {
    private final /* synthetic */ zzal zzrk;

    zzap(zzal zzal) {
        this.zzrk = zzal;
    }

    public final boolean onMediaButtonEvent(Intent intent) {
        KeyEvent keyEvent = (KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
        if (keyEvent == null) {
            return true;
        }
        if (keyEvent.getKeyCode() != 127 && keyEvent.getKeyCode() != 126) {
            return true;
        }
        this.zzrk.zzid.v();
        return true;
    }

    public final void onPause() {
        this.zzrk.zzid.v();
    }

    public final void onPlay() {
        this.zzrk.zzid.v();
    }

    public final void onStop() {
        if (this.zzrk.zzid.m()) {
            this.zzrk.zzid.v();
        }
    }
}
