package com.google.android.gms.internal.ads;

import android.util.Log;
import android.util.Pair;
import com.facebook.imageutils.JfifUtil;
import com.google.android.gms.internal.ads.zzle;
import java.util.ArrayList;
import java.util.List;

final class zzke {
    private static final int zzatw = zzoq.zzbn("meta");
    private static final int zzauw = zzoq.zzbn("vide");
    private static final int zzaux = zzoq.zzbn("soun");
    private static final int zzauy = zzoq.zzbn("text");
    private static final int zzauz = zzoq.zzbn("sbtl");
    private static final int zzava = zzoq.zzbn("subt");
    private static final int zzavb = zzoq.zzbn("clcp");
    private static final int zzavc = zzoq.zzbn("cenc");

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v46, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r34v10, resolved type: byte[]} */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a3, code lost:
        if (r14 == 0) goto L_0x0093;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0447  */
    /* JADX WARNING: Removed duplicated region for block: B:401:0x0477 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.ads.zzks zza(com.google.android.gms.internal.ads.zzkc r43, com.google.android.gms.internal.ads.zzkb r44, long r45, com.google.android.gms.internal.ads.zziv r47, boolean r48) throws com.google.android.gms.internal.ads.zzhd {
        /*
            r0 = r43
            int r1 = com.google.android.gms.internal.ads.zzjz.zzarz
            com.google.android.gms.internal.ads.zzkc r1 = r0.zzap(r1)
            int r2 = com.google.android.gms.internal.ads.zzjz.zzasn
            com.google.android.gms.internal.ads.zzkb r2 = r1.zzao(r2)
            com.google.android.gms.internal.ads.zzoj r2 = r2.zzaul
            r3 = 16
            r2.zzbe(r3)
            int r2 = r2.readInt()
            int r4 = zzaux
            r5 = 4
            r6 = 3
            r8 = -1
            if (r2 != r4) goto L_0x0022
            r12 = 1
            goto L_0x0042
        L_0x0022:
            int r4 = zzauw
            if (r2 != r4) goto L_0x0028
            r12 = 2
            goto L_0x0042
        L_0x0028:
            int r4 = zzauy
            if (r2 == r4) goto L_0x0041
            int r4 = zzauz
            if (r2 == r4) goto L_0x0041
            int r4 = zzava
            if (r2 == r4) goto L_0x0041
            int r4 = zzavb
            if (r2 != r4) goto L_0x0039
            goto L_0x0041
        L_0x0039:
            int r4 = zzatw
            if (r2 != r4) goto L_0x003f
            r12 = 4
            goto L_0x0042
        L_0x003f:
            r12 = -1
            goto L_0x0042
        L_0x0041:
            r12 = 3
        L_0x0042:
            r2 = 0
            if (r12 != r8) goto L_0x0046
            return r2
        L_0x0046:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzasj
            com.google.android.gms.internal.ads.zzkb r4 = r0.zzao(r4)
            com.google.android.gms.internal.ads.zzoj r4 = r4.zzaul
            r10 = 8
            r4.zzbe(r10)
            int r11 = r4.readInt()
            int r11 = com.google.android.gms.internal.ads.zzjz.zzak(r11)
            if (r11 != 0) goto L_0x0060
            r13 = 8
            goto L_0x0062
        L_0x0060:
            r13 = 16
        L_0x0062:
            r4.zzbf(r13)
            int r13 = r4.readInt()
            r4.zzbf(r5)
            int r14 = r4.getPosition()
            if (r11 != 0) goto L_0x0074
            r15 = 4
            goto L_0x0076
        L_0x0074:
            r15 = 8
        L_0x0076:
            r9 = 0
        L_0x0077:
            if (r9 >= r15) goto L_0x0086
            byte[] r7 = r4.data
            int r16 = r14 + r9
            byte r7 = r7[r16]
            if (r7 == r8) goto L_0x0083
            r7 = 0
            goto L_0x0087
        L_0x0083:
            int r9 = r9 + 1
            goto L_0x0077
        L_0x0086:
            r7 = 1
        L_0x0087:
            r16 = 0
            r18 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r7 == 0) goto L_0x0096
            r4.zzbf(r15)
        L_0x0093:
            r14 = r18
            goto L_0x00a6
        L_0x0096:
            if (r11 != 0) goto L_0x009d
            long r14 = r4.zzip()
            goto L_0x00a1
        L_0x009d:
            long r14 = r4.zzit()
        L_0x00a1:
            int r7 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r7 != 0) goto L_0x00a6
            goto L_0x0093
        L_0x00a6:
            r4.zzbf(r3)
            int r7 = r4.readInt()
            int r9 = r4.readInt()
            r4.zzbf(r5)
            int r11 = r4.readInt()
            int r4 = r4.readInt()
            r5 = -65536(0xffffffffffff0000, float:NaN)
            if (r7 != 0) goto L_0x00cb
            r3 = 65536(0x10000, float:9.18355E-41)
            if (r9 != r3) goto L_0x00cb
            if (r11 != r5) goto L_0x00cb
            if (r4 != 0) goto L_0x00cb
            r7 = 90
            goto L_0x00e4
        L_0x00cb:
            if (r7 != 0) goto L_0x00d8
            if (r9 != r5) goto L_0x00d8
            r3 = 65536(0x10000, float:9.18355E-41)
            if (r11 != r3) goto L_0x00d8
            if (r4 != 0) goto L_0x00d8
            r7 = 270(0x10e, float:3.78E-43)
            goto L_0x00e4
        L_0x00d8:
            if (r7 != r5) goto L_0x00e3
            if (r9 != 0) goto L_0x00e3
            if (r11 != 0) goto L_0x00e3
            if (r4 != r5) goto L_0x00e3
            r7 = 180(0xb4, float:2.52E-43)
            goto L_0x00e4
        L_0x00e3:
            r7 = 0
        L_0x00e4:
            com.google.android.gms.internal.ads.zzkk r3 = new com.google.android.gms.internal.ads.zzkk
            r3.<init>(r13, r14, r7)
            long r22 = r3.zzcw
            r4 = r44
            com.google.android.gms.internal.ads.zzoj r4 = r4.zzaul
            r4.zzbe(r10)
            int r5 = r4.readInt()
            int r5 = com.google.android.gms.internal.ads.zzjz.zzak(r5)
            if (r5 != 0) goto L_0x0101
            r5 = 8
            goto L_0x0103
        L_0x0101:
            r5 = 16
        L_0x0103:
            r4.zzbf(r5)
            long r4 = r4.zzip()
            int r7 = (r22 > r18 ? 1 : (r22 == r18 ? 0 : -1))
            if (r7 != 0) goto L_0x010f
            goto L_0x011a
        L_0x010f:
            r24 = 1000000(0xf4240, double:4.940656E-318)
            r26 = r4
            long r13 = com.google.android.gms.internal.ads.zzoq.zza((long) r22, (long) r24, (long) r26)
            r18 = r13
        L_0x011a:
            int r7 = com.google.android.gms.internal.ads.zzjz.zzasa
            com.google.android.gms.internal.ads.zzkc r7 = r1.zzap(r7)
            int r9 = com.google.android.gms.internal.ads.zzjz.zzasb
            com.google.android.gms.internal.ads.zzkc r7 = r7.zzap(r9)
            int r9 = com.google.android.gms.internal.ads.zzjz.zzasm
            com.google.android.gms.internal.ads.zzkb r1 = r1.zzao(r9)
            com.google.android.gms.internal.ads.zzoj r1 = r1.zzaul
            r1.zzbe(r10)
            int r9 = r1.readInt()
            int r9 = com.google.android.gms.internal.ads.zzjz.zzak(r9)
            if (r9 != 0) goto L_0x013e
            r11 = 8
            goto L_0x0140
        L_0x013e:
            r11 = 16
        L_0x0140:
            r1.zzbf(r11)
            long r13 = r1.zzip()
            if (r9 != 0) goto L_0x014b
            r9 = 4
            goto L_0x014d
        L_0x014b:
            r9 = 8
        L_0x014d:
            r1.zzbf(r9)
            int r1 = r1.readUnsignedShort()
            int r9 = r1 >> 10
            r9 = r9 & 31
            int r9 = r9 + 96
            char r9 = (char) r9
            int r11 = r1 >> 5
            r11 = r11 & 31
            int r11 = r11 + 96
            char r11 = (char) r11
            r1 = r1 & 31
            int r1 = r1 + 96
            char r1 = (char) r1
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>(r6)
            r15.append(r9)
            r15.append(r11)
            r15.append(r1)
            java.lang.String r1 = r15.toString()
            java.lang.Long r9 = java.lang.Long.valueOf(r13)
            android.util.Pair r1 = android.util.Pair.create(r9, r1)
            int r9 = com.google.android.gms.internal.ads.zzjz.zzaso
            com.google.android.gms.internal.ads.zzkb r7 = r7.zzao(r9)
            com.google.android.gms.internal.ads.zzoj r7 = r7.zzaul
            int r9 = r3.id
            int r11 = r3.zzafj
            java.lang.Object r13 = r1.second
            java.lang.String r13 = (java.lang.String) r13
            r14 = 12
            r7.zzbe(r14)
            int r14 = r7.readInt()
            com.google.android.gms.internal.ads.zzkf r15 = new com.google.android.gms.internal.ads.zzkf
            r15.<init>(r14)
            r6 = 0
        L_0x01a4:
            if (r6 >= r14) goto L_0x0674
            int r10 = r7.getPosition()
            int r2 = r7.readInt()
            r44 = r14
            if (r2 <= 0) goto L_0x01b4
            r8 = 1
            goto L_0x01b5
        L_0x01b4:
            r8 = 0
        L_0x01b5:
            java.lang.String r14 = "childAtomSize should be positive"
            com.google.android.gms.internal.ads.zzoc.checkArgument(r8, r14)
            int r8 = r7.readInt()
            r37 = r4
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaqw
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaqx
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzasu
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatg
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaqy
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaqz
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzara
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzauf
            if (r8 == r4) goto L_0x04c5
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaug
            if (r8 != r4) goto L_0x01e6
            goto L_0x04c5
        L_0x01e6:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzard
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzasv
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzari
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzark
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzarm
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzarp
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzarn
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaro
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatt
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatu
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzarg
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzarh
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzare
            if (r8 == r4) goto L_0x02b8
            int r4 = com.google.android.gms.internal.ads.zzjz.zzauj
            if (r8 != r4) goto L_0x0220
            goto L_0x02b8
        L_0x0220:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzate
            if (r8 == r4) goto L_0x0249
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatp
            if (r8 == r4) goto L_0x0249
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatq
            if (r8 == r4) goto L_0x0249
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatr
            if (r8 == r4) goto L_0x0249
            int r4 = com.google.android.gms.internal.ads.zzjz.zzats
            if (r8 != r4) goto L_0x0235
            goto L_0x0249
        L_0x0235:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaui
            if (r8 != r4) goto L_0x02f8
            java.lang.String r4 = java.lang.Integer.toString(r9)
            java.lang.String r5 = "application/x-camera-motion"
            r8 = -1
            r14 = 0
            com.google.android.gms.internal.ads.zzgw r4 = com.google.android.gms.internal.ads.zzgw.zza(r4, r5, r14, r8, r14)
            r15.zzafz = r4
            goto L_0x02f8
        L_0x0249:
            int r4 = r10 + 8
            r5 = 8
            int r4 = r4 + r5
            r7.zzbe(r4)
            r22 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            int r4 = com.google.android.gms.internal.ads.zzjz.zzate
            if (r8 != r4) goto L_0x0263
            java.lang.String r4 = "application/ttml+xml"
        L_0x025c:
            r30 = r22
            r32 = 0
            r23 = r4
            goto L_0x029b
        L_0x0263:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatp
            if (r8 != r4) goto L_0x027d
            int r4 = r2 + -8
            int r4 = r4 - r5
            byte[] r5 = new byte[r4]
            r8 = 0
            r7.zze(r5, r8, r4)
            java.util.List r4 = java.util.Collections.singletonList(r5)
            java.lang.String r5 = "application/x-quicktime-tx3g"
            r32 = r4
            r30 = r22
            r23 = r5
            goto L_0x029b
        L_0x027d:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatq
            if (r8 != r4) goto L_0x0284
            java.lang.String r4 = "application/x-mp4-vtt"
            goto L_0x025c
        L_0x0284:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzatr
            if (r8 != r4) goto L_0x0291
            java.lang.String r4 = "application/ttml+xml"
            r23 = r4
            r30 = r16
            r32 = 0
            goto L_0x029b
        L_0x0291:
            int r4 = com.google.android.gms.internal.ads.zzjz.zzats
            if (r8 != r4) goto L_0x02b2
            r4 = 1
            r15.zzave = r4
            java.lang.String r4 = "application/x-mp4-cea-608"
            goto L_0x025c
        L_0x029b:
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = 0
            r28 = -1
            r29 = 0
            r27 = r13
            com.google.android.gms.internal.ads.zzgw r4 = com.google.android.gms.internal.ads.zzgw.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r32)
            r15.zzafz = r4
            goto L_0x02f8
        L_0x02b2:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x02b8:
            int r4 = r10 + 8
            r5 = 8
            int r4 = r4 + r5
            r7.zzbe(r4)
            if (r48 == 0) goto L_0x02cb
            int r4 = r7.readUnsignedShort()
            r5 = 6
            r7.zzbf(r5)
            goto L_0x02d1
        L_0x02cb:
            r4 = 8
            r7.zzbf(r4)
            r4 = 0
        L_0x02d1:
            if (r4 == 0) goto L_0x0303
            r5 = 1
            if (r4 != r5) goto L_0x02d7
            goto L_0x0303
        L_0x02d7:
            r5 = 2
            if (r4 != r5) goto L_0x02f8
            r4 = 16
            r7.zzbf(r4)
            long r4 = r7.readLong()
            double r4 = java.lang.Double.longBitsToDouble(r4)
            long r4 = java.lang.Math.round(r4)
            int r5 = (int) r4
            int r4 = r7.zzis()
            r22 = r4
            r4 = 20
            r7.zzbf(r4)
            goto L_0x031d
        L_0x02f8:
            r40 = r1
            r41 = r3
            r42 = r11
            r39 = r12
        L_0x0300:
            r1 = 3
            goto L_0x065a
        L_0x0303:
            int r5 = r7.readUnsignedShort()
            r22 = r5
            r5 = 6
            r7.zzbf(r5)
            int r5 = r7.zzir()
            r23 = r5
            r5 = 1
            if (r4 != r5) goto L_0x031b
            r4 = 16
            r7.zzbf(r4)
        L_0x031b:
            r5 = r23
        L_0x031d:
            int r4 = r7.getPosition()
            r23 = r5
            int r5 = com.google.android.gms.internal.ads.zzjz.zzasv
            if (r8 != r5) goto L_0x032e
            int r8 = zza((com.google.android.gms.internal.ads.zzoj) r7, (int) r10, (int) r2, (com.google.android.gms.internal.ads.zzkf) r15, (int) r6)
            r7.zzbe(r4)
        L_0x032e:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzari
            if (r8 != r5) goto L_0x0335
            java.lang.String r5 = "audio/ac3"
            goto L_0x037f
        L_0x0335:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzark
            if (r8 != r5) goto L_0x033c
            java.lang.String r5 = "audio/eac3"
            goto L_0x037f
        L_0x033c:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzarm
            if (r8 != r5) goto L_0x0343
            java.lang.String r5 = "audio/vnd.dts"
            goto L_0x037f
        L_0x0343:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzarn
            if (r8 == r5) goto L_0x037d
            int r5 = com.google.android.gms.internal.ads.zzjz.zzaro
            if (r8 != r5) goto L_0x034c
            goto L_0x037d
        L_0x034c:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzarp
            if (r8 != r5) goto L_0x0353
            java.lang.String r5 = "audio/vnd.dts.hd;profile=lbr"
            goto L_0x037f
        L_0x0353:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzatt
            if (r8 != r5) goto L_0x035a
            java.lang.String r5 = "audio/3gpp"
            goto L_0x037f
        L_0x035a:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzatu
            if (r8 != r5) goto L_0x0361
            java.lang.String r5 = "audio/amr-wb"
            goto L_0x037f
        L_0x0361:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzarg
            if (r8 == r5) goto L_0x037a
            int r5 = com.google.android.gms.internal.ads.zzjz.zzarh
            if (r8 != r5) goto L_0x036a
            goto L_0x037a
        L_0x036a:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzare
            if (r8 != r5) goto L_0x0371
            java.lang.String r5 = "audio/mpeg"
            goto L_0x037f
        L_0x0371:
            int r5 = com.google.android.gms.internal.ads.zzjz.zzauj
            if (r8 != r5) goto L_0x0378
            java.lang.String r5 = "audio/alac"
            goto L_0x037f
        L_0x0378:
            r5 = 0
            goto L_0x037f
        L_0x037a:
            java.lang.String r5 = "audio/raw"
            goto L_0x037f
        L_0x037d:
            java.lang.String r5 = "audio/vnd.dts.hd"
        L_0x037f:
            r8 = r4
            r39 = r12
            r4 = r22
            r33 = r23
            r34 = 0
        L_0x0388:
            int r12 = r8 - r10
            if (r12 >= r2) goto L_0x0482
            r7.zzbe(r8)
            int r12 = r7.readInt()
            r40 = r1
            if (r12 <= 0) goto L_0x0399
            r1 = 1
            goto L_0x039a
        L_0x0399:
            r1 = 0
        L_0x039a:
            com.google.android.gms.internal.ads.zzoc.checkArgument(r1, r14)
            int r1 = r7.readInt()
            r41 = r3
            int r3 = com.google.android.gms.internal.ads.zzjz.zzase
            if (r1 == r3) goto L_0x0410
            if (r48 == 0) goto L_0x03ae
            int r3 = com.google.android.gms.internal.ads.zzjz.zzarf
            if (r1 != r3) goto L_0x03ae
            goto L_0x0410
        L_0x03ae:
            int r3 = com.google.android.gms.internal.ads.zzjz.zzarj
            if (r1 != r3) goto L_0x03c4
            int r1 = r8 + 8
            r7.zzbe(r1)
            java.lang.String r1 = java.lang.Integer.toString(r9)
            r3 = 0
            com.google.android.gms.internal.ads.zzgw r1 = com.google.android.gms.internal.ads.zzhk.zza(r7, r1, r13, r3)
            r15.zzafz = r1
        L_0x03c2:
            r3 = 0
            goto L_0x040b
        L_0x03c4:
            int r3 = com.google.android.gms.internal.ads.zzjz.zzarl
            if (r1 != r3) goto L_0x03d9
            int r1 = r8 + 8
            r7.zzbe(r1)
            java.lang.String r1 = java.lang.Integer.toString(r9)
            r3 = 0
            com.google.android.gms.internal.ads.zzgw r1 = com.google.android.gms.internal.ads.zzhk.zzb(r7, r1, r13, r3)
            r15.zzafz = r1
            goto L_0x03c2
        L_0x03d9:
            int r3 = com.google.android.gms.internal.ads.zzjz.zzarq
            if (r1 != r3) goto L_0x03fc
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = -1
            r29 = 0
            r30 = 0
            r31 = 0
            r23 = r5
            r27 = r4
            r28 = r33
            r32 = r13
            com.google.android.gms.internal.ads.zzgw r1 = com.google.android.gms.internal.ads.zzgw.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)
            r15.zzafz = r1
            goto L_0x03c2
        L_0x03fc:
            int r3 = com.google.android.gms.internal.ads.zzjz.zzauj
            if (r1 != r3) goto L_0x03c2
            byte[] r1 = new byte[r12]
            r7.zzbe(r8)
            r3 = 0
            r7.zze(r1, r3, r12)
            r34 = r1
        L_0x040b:
            r42 = r11
            r0 = -1
            goto L_0x0477
        L_0x0410:
            int r3 = com.google.android.gms.internal.ads.zzjz.zzase
            if (r1 != r3) goto L_0x0419
            r1 = r8
            r42 = r11
        L_0x0417:
            r0 = -1
            goto L_0x0445
        L_0x0419:
            int r1 = r7.getPosition()
        L_0x041d:
            int r3 = r1 - r8
            if (r3 >= r12) goto L_0x0441
            r7.zzbe(r1)
            int r3 = r7.readInt()
            if (r3 <= 0) goto L_0x042c
            r0 = 1
            goto L_0x042d
        L_0x042c:
            r0 = 0
        L_0x042d:
            com.google.android.gms.internal.ads.zzoc.checkArgument(r0, r14)
            int r0 = r7.readInt()
            r42 = r11
            int r11 = com.google.android.gms.internal.ads.zzjz.zzase
            if (r0 != r11) goto L_0x043b
            goto L_0x0417
        L_0x043b:
            int r1 = r1 + r3
            r0 = r43
            r11 = r42
            goto L_0x041d
        L_0x0441:
            r42 = r11
            r0 = -1
            r1 = -1
        L_0x0445:
            if (r1 == r0) goto L_0x0477
            android.util.Pair r1 = zzb(r7, r1)
            java.lang.Object r3 = r1.first
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r1 = r1.second
            r34 = r1
            byte[] r34 = (byte[]) r34
            java.lang.String r1 = "audio/mp4a-latm"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x0476
            android.util.Pair r1 = com.google.android.gms.internal.ads.zzob.zze(r34)
            java.lang.Object r4 = r1.first
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            java.lang.Object r1 = r1.second
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r1 = r1.intValue()
            r5 = r3
            r33 = r4
            r4 = r1
            goto L_0x0477
        L_0x0476:
            r5 = r3
        L_0x0477:
            int r8 = r8 + r12
            r0 = r43
            r1 = r40
            r3 = r41
            r11 = r42
            goto L_0x0388
        L_0x0482:
            r40 = r1
            r41 = r3
            r42 = r11
            r0 = -1
            com.google.android.gms.internal.ads.zzgw r1 = r15.zzafz
            if (r1 != 0) goto L_0x0300
            if (r5 == 0) goto L_0x0300
            java.lang.String r1 = "audio/raw"
            boolean r1 = r1.equals(r5)
            if (r1 == 0) goto L_0x049a
            r29 = 2
            goto L_0x049c
        L_0x049a:
            r29 = -1
        L_0x049c:
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = -1
            if (r34 != 0) goto L_0x04ab
            r30 = 0
            goto L_0x04b1
        L_0x04ab:
            java.util.List r1 = java.util.Collections.singletonList(r34)
            r30 = r1
        L_0x04b1:
            r31 = 0
            r32 = 0
            r23 = r5
            r27 = r4
            r28 = r33
            r33 = r13
            com.google.android.gms.internal.ads.zzgw r1 = com.google.android.gms.internal.ads.zzgw.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33)
            r15.zzafz = r1
            goto L_0x0300
        L_0x04c5:
            r40 = r1
            r41 = r3
            r42 = r11
            r39 = r12
            r0 = -1
            int r1 = r10 + 8
            r3 = 8
            int r1 = r1 + r3
            r7.zzbe(r1)
            r1 = 16
            r7.zzbf(r1)
            int r27 = r7.readUnsignedShort()
            int r28 = r7.readUnsignedShort()
            r3 = 1065353216(0x3f800000, float:1.0)
            r4 = 50
            r7.zzbf(r4)
            int r4 = r7.getPosition()
            int r5 = com.google.android.gms.internal.ads.zzjz.zzasu
            if (r8 != r5) goto L_0x04f9
            int r8 = zza((com.google.android.gms.internal.ads.zzoj) r7, (int) r10, (int) r2, (com.google.android.gms.internal.ads.zzkf) r15, (int) r6)
            r7.zzbe(r4)
        L_0x04f9:
            r3 = 0
            r23 = 0
            r30 = 0
            r32 = 1065353216(0x3f800000, float:1.0)
            r33 = 0
            r34 = -1
        L_0x0504:
            int r5 = r4 - r10
            if (r5 >= r2) goto L_0x063f
            r7.zzbe(r4)
            int r5 = r7.getPosition()
            int r11 = r7.readInt()
            if (r11 != 0) goto L_0x051c
            int r12 = r7.getPosition()
            int r12 = r12 - r10
            if (r12 == r2) goto L_0x063f
        L_0x051c:
            if (r11 <= 0) goto L_0x0520
            r12 = 1
            goto L_0x0521
        L_0x0520:
            r12 = 0
        L_0x0521:
            com.google.android.gms.internal.ads.zzoc.checkArgument(r12, r14)
            int r12 = r7.readInt()
            int r0 = com.google.android.gms.internal.ads.zzjz.zzasc
            if (r12 != r0) goto L_0x054c
            if (r23 != 0) goto L_0x0530
            r0 = 1
            goto L_0x0531
        L_0x0530:
            r0 = 0
        L_0x0531:
            com.google.android.gms.internal.ads.zzoc.checkState(r0)
            int r5 = r5 + 8
            r7.zzbe(r5)
            com.google.android.gms.internal.ads.zzos r0 = com.google.android.gms.internal.ads.zzos.zzf(r7)
            java.util.List<byte[]> r5 = r0.zzafg
            int r12 = r0.zzaqu
            r15.zzaqu = r12
            if (r3 != 0) goto L_0x0549
            float r0 = r0.zzbgf
            r32 = r0
        L_0x0549:
            java.lang.String r0 = "video/avc"
            goto L_0x0569
        L_0x054c:
            int r0 = com.google.android.gms.internal.ads.zzjz.zzasd
            if (r12 != r0) goto L_0x0570
            if (r23 != 0) goto L_0x0554
            r0 = 1
            goto L_0x0555
        L_0x0554:
            r0 = 0
        L_0x0555:
            com.google.android.gms.internal.ads.zzoc.checkState(r0)
            int r5 = r5 + 8
            r7.zzbe(r5)
            com.google.android.gms.internal.ads.zzoy r0 = com.google.android.gms.internal.ads.zzoy.zzh(r7)
            java.util.List<byte[]> r5 = r0.zzafg
            int r0 = r0.zzaqu
            r15.zzaqu = r0
            java.lang.String r0 = "video/hevc"
        L_0x0569:
            r23 = r0
            r30 = r5
        L_0x056d:
            r1 = 3
            goto L_0x0639
        L_0x0570:
            int r0 = com.google.android.gms.internal.ads.zzjz.zzauh
            if (r12 != r0) goto L_0x0586
            if (r23 != 0) goto L_0x0578
            r0 = 1
            goto L_0x0579
        L_0x0578:
            r0 = 0
        L_0x0579:
            com.google.android.gms.internal.ads.zzoc.checkState(r0)
            int r0 = com.google.android.gms.internal.ads.zzjz.zzauf
            if (r8 != r0) goto L_0x0583
            java.lang.String r0 = "video/x-vnd.on2.vp8"
            goto L_0x0594
        L_0x0583:
            java.lang.String r0 = "video/x-vnd.on2.vp9"
            goto L_0x0594
        L_0x0586:
            int r0 = com.google.android.gms.internal.ads.zzjz.zzarb
            if (r12 != r0) goto L_0x0597
            if (r23 != 0) goto L_0x058e
            r0 = 1
            goto L_0x058f
        L_0x058e:
            r0 = 0
        L_0x058f:
            com.google.android.gms.internal.ads.zzoc.checkState(r0)
            java.lang.String r0 = "video/3gpp"
        L_0x0594:
            r23 = r0
            goto L_0x056d
        L_0x0597:
            int r0 = com.google.android.gms.internal.ads.zzjz.zzase
            if (r12 != r0) goto L_0x05b8
            if (r23 != 0) goto L_0x059f
            r0 = 1
            goto L_0x05a0
        L_0x059f:
            r0 = 0
        L_0x05a0:
            com.google.android.gms.internal.ads.zzoc.checkState(r0)
            android.util.Pair r0 = zzb(r7, r5)
            java.lang.Object r5 = r0.first
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r0 = r0.second
            byte[] r0 = (byte[]) r0
            java.util.List r0 = java.util.Collections.singletonList(r0)
            r30 = r0
            r23 = r5
            goto L_0x056d
        L_0x05b8:
            int r0 = com.google.android.gms.internal.ads.zzjz.zzatd
            if (r12 != r0) goto L_0x05d1
            int r5 = r5 + 8
            r7.zzbe(r5)
            int r0 = r7.zzis()
            int r3 = r7.zzis()
            float r0 = (float) r0
            float r3 = (float) r3
            float r32 = r0 / r3
            r1 = 3
            r3 = 1
            goto L_0x0639
        L_0x05d1:
            int r0 = com.google.android.gms.internal.ads.zzjz.zzaud
            if (r12 != r0) goto L_0x0603
            int r0 = r5 + 8
        L_0x05d7:
            int r12 = r0 - r5
            if (r12 >= r11) goto L_0x05fa
            r7.zzbe(r0)
            int r12 = r7.readInt()
            int r1 = r7.readInt()
            r22 = r3
            int r3 = com.google.android.gms.internal.ads.zzjz.zzaue
            if (r1 != r3) goto L_0x05f4
            byte[] r1 = r7.data
            int r12 = r12 + r0
            byte[] r0 = java.util.Arrays.copyOfRange(r1, r0, r12)
            goto L_0x05fd
        L_0x05f4:
            int r0 = r0 + r12
            r3 = r22
            r1 = 16
            goto L_0x05d7
        L_0x05fa:
            r22 = r3
            r0 = 0
        L_0x05fd:
            r33 = r0
            r3 = r22
            goto L_0x056d
        L_0x0603:
            r22 = r3
            int r0 = com.google.android.gms.internal.ads.zzjz.zzauc
            if (r12 != r0) goto L_0x0636
            int r0 = r7.readUnsignedByte()
            r1 = 3
            r7.zzbf(r1)
            if (r0 != 0) goto L_0x0637
            int r0 = r7.readUnsignedByte()
            if (r0 == 0) goto L_0x0631
            r3 = 1
            if (r0 == r3) goto L_0x062c
            r3 = 2
            if (r0 == r3) goto L_0x0627
            if (r0 == r1) goto L_0x0622
            goto L_0x0637
        L_0x0622:
            r3 = r22
            r34 = 3
            goto L_0x0639
        L_0x0627:
            r3 = r22
            r34 = 2
            goto L_0x0639
        L_0x062c:
            r3 = r22
            r34 = 1
            goto L_0x0639
        L_0x0631:
            r3 = r22
            r34 = 0
            goto L_0x0639
        L_0x0636:
            r1 = 3
        L_0x0637:
            r3 = r22
        L_0x0639:
            int r4 = r4 + r11
            r0 = -1
            r1 = 16
            goto L_0x0504
        L_0x063f:
            r1 = 3
            if (r23 == 0) goto L_0x065a
            java.lang.String r22 = java.lang.Integer.toString(r9)
            r24 = 0
            r25 = -1
            r26 = -1
            r29 = -1082130432(0xffffffffbf800000, float:-1.0)
            r35 = 0
            r36 = 0
            r31 = r42
            com.google.android.gms.internal.ads.zzgw r0 = com.google.android.gms.internal.ads.zzgw.zza(r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36)
            r15.zzafz = r0
        L_0x065a:
            int r10 = r10 + r2
            r7.zzbe(r10)
            int r6 = r6 + 1
            r0 = r43
            r14 = r44
            r4 = r37
            r12 = r39
            r1 = r40
            r3 = r41
            r11 = r42
            r2 = 0
            r8 = -1
            r10 = 8
            goto L_0x01a4
        L_0x0674:
            r40 = r1
            r41 = r3
            r37 = r4
            r39 = r12
            int r0 = com.google.android.gms.internal.ads.zzjz.zzask
            r1 = r43
            com.google.android.gms.internal.ads.zzkc r0 = r1.zzap(r0)
            if (r0 == 0) goto L_0x06e1
            int r1 = com.google.android.gms.internal.ads.zzjz.zzasl
            com.google.android.gms.internal.ads.zzkb r0 = r0.zzao(r1)
            if (r0 != 0) goto L_0x068f
            goto L_0x06e1
        L_0x068f:
            com.google.android.gms.internal.ads.zzoj r0 = r0.zzaul
            r1 = 8
            r0.zzbe(r1)
            int r1 = r0.readInt()
            int r1 = com.google.android.gms.internal.ads.zzjz.zzak(r1)
            int r2 = r0.zzis()
            long[] r3 = new long[r2]
            long[] r4 = new long[r2]
            r5 = 0
        L_0x06a7:
            if (r5 >= r2) goto L_0x06da
            r6 = 1
            if (r1 != r6) goto L_0x06b1
            long r7 = r0.zzit()
            goto L_0x06b5
        L_0x06b1:
            long r7 = r0.zzip()
        L_0x06b5:
            r3[r5] = r7
            if (r1 != r6) goto L_0x06be
            long r7 = r0.readLong()
            goto L_0x06c3
        L_0x06be:
            int r7 = r0.readInt()
            long r7 = (long) r7
        L_0x06c3:
            r4[r5] = r7
            short r7 = r0.readShort()
            if (r7 != r6) goto L_0x06d2
            r7 = 2
            r0.zzbf(r7)
            int r5 = r5 + 1
            goto L_0x06a7
        L_0x06d2:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Unsupported media rate."
            r0.<init>(r1)
            throw r0
        L_0x06da:
            android.util.Pair r0 = android.util.Pair.create(r3, r4)
            r1 = r0
            r0 = 0
            goto L_0x06e6
        L_0x06e1:
            r0 = 0
            android.util.Pair r1 = android.util.Pair.create(r0, r0)
        L_0x06e6:
            com.google.android.gms.internal.ads.zzgw r2 = r15.zzafz
            if (r2 != 0) goto L_0x06eb
            return r0
        L_0x06eb:
            com.google.android.gms.internal.ads.zzks r0 = new com.google.android.gms.internal.ads.zzks
            int r11 = r41.id
            r2 = r40
            java.lang.Object r2 = r2.first
            java.lang.Long r2 = (java.lang.Long) r2
            long r13 = r2.longValue()
            com.google.android.gms.internal.ads.zzgw r2 = r15.zzafz
            int r3 = r15.zzave
            com.google.android.gms.internal.ads.zzkr[] r4 = r15.zzavd
            int r5 = r15.zzaqu
            java.lang.Object r6 = r1.first
            r23 = r6
            long[] r23 = (long[]) r23
            java.lang.Object r1 = r1.second
            r24 = r1
            long[] r24 = (long[]) r24
            r10 = r0
            r12 = r39
            r15 = r37
            r17 = r18
            r19 = r2
            r20 = r3
            r21 = r4
            r22 = r5
            r10.<init>(r11, r12, r13, r15, r17, r19, r20, r21, r22, r23, r24)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzke.zza(com.google.android.gms.internal.ads.zzkc, com.google.android.gms.internal.ads.zzkb, long, com.google.android.gms.internal.ads.zziv, boolean):com.google.android.gms.internal.ads.zzks");
    }

    private static Pair<String, byte[]> zzb(zzoj zzoj, int i) {
        zzoj.zzbe(i + 8 + 4);
        zzoj.zzbf(1);
        zzc(zzoj);
        zzoj.zzbf(2);
        int readUnsignedByte = zzoj.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            zzoj.zzbf(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            zzoj.zzbf(zzoj.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            zzoj.zzbf(2);
        }
        zzoj.zzbf(1);
        zzc(zzoj);
        int readUnsignedByte2 = zzoj.readUnsignedByte();
        String str = null;
        if (readUnsignedByte2 == 32) {
            str = "video/mp4v-es";
        } else if (readUnsignedByte2 == 33) {
            str = "video/avc";
        } else if (readUnsignedByte2 != 35) {
            if (readUnsignedByte2 != 64) {
                if (readUnsignedByte2 == 107) {
                    return Pair.create("audio/mpeg", (Object) null);
                }
                if (readUnsignedByte2 == 165) {
                    str = "audio/ac3";
                } else if (readUnsignedByte2 != 166) {
                    switch (readUnsignedByte2) {
                        case 102:
                        case 103:
                        case 104:
                            break;
                        default:
                            switch (readUnsignedByte2) {
                                case 169:
                                case 172:
                                    return Pair.create("audio/vnd.dts", (Object) null);
                                case 170:
                                case 171:
                                    return Pair.create("audio/vnd.dts.hd", (Object) null);
                            }
                    }
                } else {
                    str = "audio/eac3";
                }
            }
            str = "audio/mp4a-latm";
        } else {
            str = "video/hevc";
        }
        zzoj.zzbf(12);
        zzoj.zzbf(1);
        int zzc = zzc(zzoj);
        byte[] bArr = new byte[zzc];
        zzoj.zze(bArr, 0, zzc);
        return Pair.create(str, bArr);
    }

    private static int zzc(zzoj zzoj) {
        int readUnsignedByte = zzoj.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = zzoj.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }

    public static zzku zza(zzks zzks, zzkc zzkc, zzjh zzjh) throws zzhd {
        zzkg zzkg;
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        int[] iArr;
        long[] jArr;
        int[] iArr2;
        long[] jArr2;
        long j;
        long j2;
        long[] jArr3;
        int[] iArr3;
        boolean z2;
        int i5;
        long[] jArr4;
        int[] iArr4;
        int[] iArr5;
        int i6;
        int i7;
        zzks zzks2 = zzks;
        zzkc zzkc2 = zzkc;
        zzjh zzjh2 = zzjh;
        zzkb zzao = zzkc2.zzao(zzjz.zzatl);
        if (zzao != null) {
            zzkg = new zzki(zzao);
        } else {
            zzkb zzao2 = zzkc2.zzao(zzjz.zzatm);
            if (zzao2 != null) {
                zzkg = new zzkh(zzao2);
            } else {
                throw new zzhd("Track has no sample table size information");
            }
        }
        int zzgq = zzkg.zzgq();
        if (zzgq == 0) {
            return new zzku(new long[0], new int[0], 0, new long[0], new int[0]);
        }
        zzkb zzao3 = zzkc2.zzao(zzjz.zzatn);
        if (zzao3 == null) {
            zzao3 = zzkc2.zzao(zzjz.zzato);
            z = true;
        } else {
            z = false;
        }
        zzoj zzoj = zzao3.zzaul;
        zzoj zzoj2 = zzkc2.zzao(zzjz.zzatk).zzaul;
        zzoj zzoj3 = zzkc2.zzao(zzjz.zzath).zzaul;
        zzkb zzao4 = zzkc2.zzao(zzjz.zzati);
        zzoj zzoj4 = zzao4 != null ? zzao4.zzaul : null;
        zzkb zzao5 = zzkc2.zzao(zzjz.zzatj);
        zzoj zzoj5 = zzao5 != null ? zzao5.zzaul : null;
        zzkd zzkd = new zzkd(zzoj2, zzoj, z);
        zzoj3.zzbe(12);
        int zzis = zzoj3.zzis() - 1;
        int zzis2 = zzoj3.zzis();
        int zzis3 = zzoj3.zzis();
        if (zzoj5 != null) {
            zzoj5.zzbe(12);
            i = zzoj5.zzis();
        } else {
            i = 0;
        }
        int i8 = -1;
        if (zzoj4 != null) {
            zzoj4.zzbe(12);
            i2 = zzoj4.zzis();
            if (i2 > 0) {
                i8 = zzoj4.zzis() - 1;
            } else {
                zzoj4 = null;
            }
        } else {
            i2 = 0;
        }
        long j3 = 0;
        if (!(zzkg.zzgs() && "audio/raw".equals(zzks2.zzafz.zzafe) && zzis == 0 && i == 0 && i2 == 0)) {
            jArr2 = new long[zzgq];
            iArr = new int[zzgq];
            jArr = new long[zzgq];
            int i9 = i2;
            iArr2 = new int[zzgq];
            int i10 = i9;
            zzoj zzoj6 = zzoj3;
            int i11 = zzis3;
            int i12 = i;
            int i13 = i8;
            long j4 = 0;
            long j5 = 0;
            int i14 = 0;
            int i15 = 0;
            int i16 = 0;
            int i17 = 0;
            int i18 = zzis2;
            int i19 = zzis;
            int i20 = 0;
            while (i20 < zzgq) {
                long j6 = j4;
                int i21 = i14;
                while (i21 == 0) {
                    zzoc.checkState(zzkd.zzgp());
                    j6 = zzkd.zzauq;
                    i21 = zzkd.zzaup;
                    i19 = i19;
                    i11 = i11;
                }
                int i22 = i19;
                int i23 = i11;
                if (zzoj5 != null) {
                    while (i17 == 0 && i12 > 0) {
                        i17 = zzoj5.zzis();
                        i16 = zzoj5.readInt();
                        i12--;
                    }
                    i17--;
                }
                int i24 = i16;
                jArr2[i20] = j6;
                iArr[i20] = zzkg.zzgr();
                if (iArr[i20] > i15) {
                    i6 = zzgq;
                    i15 = iArr[i20];
                } else {
                    i6 = zzgq;
                }
                zzkg zzkg2 = zzkg;
                jArr[i20] = j5 + ((long) i24);
                iArr2[i20] = zzoj4 == null ? 1 : 0;
                if (i20 == i13) {
                    iArr2[i20] = 1;
                    i10--;
                    if (i10 > 0) {
                        i13 = zzoj4.zzis() - 1;
                    }
                }
                int i25 = i10;
                int i26 = i13;
                int i27 = i23;
                j5 += (long) i27;
                i18--;
                if (i18 != 0 || i22 <= 0) {
                    i7 = i22;
                } else {
                    i7 = i22 - 1;
                    i18 = zzoj6.zzis();
                    i27 = zzoj6.zzis();
                }
                int i28 = i7;
                i20++;
                i13 = i26;
                zzgq = i6;
                i14 = i21 - 1;
                i16 = i24;
                i19 = i28;
                j4 = j6 + ((long) iArr[i20]);
                zzkg zzkg3 = zzkg2;
                i11 = i27;
                i10 = i25;
                zzkg = zzkg3;
            }
            i4 = zzgq;
            int i29 = i19;
            zzoc.checkArgument(i17 == 0);
            while (i12 > 0) {
                zzoc.checkArgument(zzoj5.zzis() == 0);
                zzoj5.readInt();
                i12--;
            }
            if (i10 == 0 && i18 == 0 && i14 == 0 && i29 == 0) {
                zzks2 = zzks;
            } else {
                int i30 = i10;
                zzks2 = zzks;
                int i31 = zzks2.id;
                StringBuilder sb = new StringBuilder(JfifUtil.MARKER_RST7);
                sb.append("Inconsistent stbl box for track ");
                sb.append(i31);
                sb.append(": remainingSynchronizationSamples ");
                sb.append(i30);
                sb.append(", remainingSamplesAtTimestampDelta ");
                sb.append(i18);
                sb.append(", remainingSamplesInChunk ");
                sb.append(i14);
                sb.append(", remainingTimestampDeltaChanges ");
                sb.append(i29);
                Log.w("AtomParsers", sb.toString());
            }
            j = j5;
            i3 = i15;
        } else {
            i4 = zzgq;
            zzkg zzkg4 = zzkg;
            int i32 = zzkd.length;
            long[] jArr5 = new long[i32];
            int[] iArr6 = new int[i32];
            while (zzkd.zzgp()) {
                int i33 = zzkd.index;
                jArr5[i33] = zzkd.zzauq;
                iArr6[i33] = zzkd.zzaup;
            }
            int zzgr = zzkg4.zzgr();
            long j7 = (long) zzis3;
            int i34 = 8192 / zzgr;
            int i35 = 0;
            for (int zzf : iArr6) {
                i35 += zzoq.zzf(zzf, i34);
            }
            long[] jArr6 = new long[i35];
            int[] iArr7 = new int[i35];
            long[] jArr7 = new long[i35];
            int[] iArr8 = new int[i35];
            int i36 = 0;
            int i37 = 0;
            int i38 = 0;
            int i39 = 0;
            while (i36 < iArr6.length) {
                int i40 = iArr6[i36];
                long j8 = jArr5[i36];
                int i41 = i37;
                int i42 = i39;
                while (i40 > 0) {
                    int min = Math.min(i34, i40);
                    jArr6[i38] = j8;
                    iArr7[i38] = zzgr * min;
                    i42 = Math.max(i42, iArr7[i38]);
                    jArr7[i38] = ((long) i41) * j7;
                    iArr8[i38] = 1;
                    j8 += (long) iArr7[i38];
                    i41 += min;
                    i40 -= min;
                    i38++;
                    iArr6 = iArr6;
                    jArr5 = jArr5;
                }
                int[] iArr9 = iArr6;
                long[] jArr8 = jArr5;
                i36++;
                i39 = i42;
                i37 = i41;
            }
            zzkm zzkm = new zzkm(jArr6, iArr7, i39, jArr7, iArr8);
            jArr2 = zzkm.zzamq;
            iArr = zzkm.zzamp;
            int i43 = zzkm.zzawo;
            jArr = zzkm.zzawp;
            iArr2 = zzkm.zzawq;
            i3 = i43;
            j = 0;
        }
        if (zzks2.zzaxl == null || zzjh.zzgk()) {
            zzoq.zza(jArr, 1000000, zzks2.zzcv);
            return new zzku(jArr2, iArr, i3, jArr, iArr2);
        }
        long[] jArr9 = zzks2.zzaxl;
        if (jArr9.length == 1 && zzks2.type == 1 && jArr.length >= 2) {
            long j9 = zzks2.zzaxm[0];
            long zza = zzoq.zza(jArr9[0], zzks2.zzcv, zzks2.zzaxi) + j9;
            if (jArr[0] <= j9 && j9 < jArr[1] && jArr[jArr.length - 1] < zza && zza <= j) {
                long j10 = j - zza;
                long zza2 = zzoq.zza(j9 - jArr[0], (long) zzks2.zzafz.zzafp, zzks2.zzcv);
                long zza3 = zzoq.zza(j10, (long) zzks2.zzafz.zzafp, zzks2.zzcv);
                if (!(zza2 == 0 && zza3 == 0) && zza2 <= 2147483647L && zza3 <= 2147483647L) {
                    int i44 = (int) zza2;
                    zzjh zzjh3 = zzjh;
                    zzjh3.zzafr = i44;
                    zzjh3.zzafs = (int) zza3;
                    zzoq.zza(jArr, 1000000, zzks2.zzcv);
                    return new zzku(jArr2, iArr, i3, jArr, iArr2);
                }
            }
        }
        long[] jArr10 = zzks2.zzaxl;
        if (jArr10.length == 1) {
            char c = 0;
            if (jArr10[0] == 0) {
                int i45 = 0;
                while (i45 < jArr.length) {
                    jArr[i45] = zzoq.zza(jArr[i45] - zzks2.zzaxm[c], 1000000, zzks2.zzcv);
                    i45++;
                    c = 0;
                }
                return new zzku(jArr2, iArr, i3, jArr, iArr2);
            }
        }
        boolean z3 = zzks2.type == 1;
        int i46 = 0;
        boolean z4 = false;
        int i47 = 0;
        int i48 = 0;
        while (true) {
            long[] jArr11 = zzks2.zzaxl;
            j2 = -1;
            if (i46 >= jArr11.length) {
                break;
            }
            int[] iArr10 = iArr;
            long j11 = zzks2.zzaxm[i46];
            if (j11 != -1) {
                long zza4 = zzoq.zza(jArr11[i46], zzks2.zzcv, zzks2.zzaxi);
                int zzb = zzoq.zzb(jArr, j11, true, true);
                int zzb2 = zzoq.zzb(jArr, j11 + zza4, z3, false);
                i47 += zzb2 - zzb;
                z4 |= i48 != zzb;
                i48 = zzb2;
            }
            i46++;
            iArr = iArr10;
        }
        int[] iArr11 = iArr;
        boolean z5 = (i47 != i4) | z4;
        long[] jArr12 = z5 ? new long[i47] : jArr2;
        int[] iArr12 = z5 ? new int[i47] : iArr11;
        if (z5) {
            i3 = 0;
        }
        int[] iArr13 = z5 ? new int[i47] : iArr2;
        long[] jArr13 = new long[i47];
        int i49 = i3;
        int i50 = 0;
        int i51 = 0;
        while (true) {
            long[] jArr14 = zzks2.zzaxl;
            if (i50 >= jArr14.length) {
                break;
            }
            long[] jArr15 = jArr12;
            long[] jArr16 = jArr13;
            long j12 = zzks2.zzaxm[i50];
            long j13 = jArr14[i50];
            if (j12 != j2) {
                long j14 = zzks2.zzcv;
                iArr4 = iArr13;
                i5 = i50;
                int zzb3 = zzoq.zzb(jArr, j12, true, true);
                int zzb4 = zzoq.zzb(jArr, zzoq.zza(j13, j14, zzks2.zzaxi) + j12, z3, false);
                if (z5) {
                    int i52 = zzb4 - zzb3;
                    jArr4 = jArr15;
                    System.arraycopy(jArr2, zzb3, jArr4, i51, i52);
                    z2 = z3;
                    iArr5 = iArr11;
                    System.arraycopy(iArr5, zzb3, iArr12, i51, i52);
                    System.arraycopy(iArr2, zzb3, iArr4, i51, i52);
                } else {
                    jArr4 = jArr15;
                    z2 = z3;
                    iArr5 = iArr11;
                }
                int i53 = i49;
                while (zzb3 < zzb4) {
                    long[] jArr17 = jArr2;
                    int[] iArr14 = iArr2;
                    long j15 = j12;
                    jArr16[i51] = zzoq.zza(j3, 1000000, zzks2.zzaxi) + zzoq.zza(jArr[zzb3] - j12, 1000000, zzks2.zzcv);
                    if (z5 && iArr12[i51] > i53) {
                        i53 = iArr5[zzb3];
                    }
                    i51++;
                    zzb3++;
                    jArr2 = jArr17;
                    j12 = j15;
                    iArr2 = iArr14;
                }
                jArr3 = jArr2;
                iArr3 = iArr2;
                i49 = i53;
            } else {
                iArr3 = iArr2;
                iArr4 = iArr13;
                i5 = i50;
                jArr4 = jArr15;
                z2 = z3;
                iArr5 = iArr11;
                jArr3 = jArr2;
            }
            j3 += j13;
            i50 = i5 + 1;
            jArr2 = jArr3;
            iArr11 = iArr5;
            iArr13 = iArr4;
            jArr12 = jArr4;
            z3 = z2;
            iArr2 = iArr3;
            jArr13 = jArr16;
            j2 = -1;
        }
        long[] jArr18 = jArr12;
        long[] jArr19 = jArr13;
        int[] iArr15 = iArr13;
        boolean z6 = false;
        for (int i54 = 0; i54 < iArr15.length && !z6; i54++) {
            z6 |= (iArr15[i54] & 1) != 0;
        }
        if (z6) {
            return new zzku(jArr18, iArr12, i49, jArr19, iArr15);
        }
        throw new zzhd("The edited sample sequence does not contain a sync sample.");
    }

    public static zzle zza(zzkb zzkb, boolean z) {
        if (z) {
            return null;
        }
        zzoj zzoj = zzkb.zzaul;
        zzoj.zzbe(8);
        while (zzoj.zzin() >= 8) {
            int position = zzoj.getPosition();
            int readInt = zzoj.readInt();
            if (zzoj.readInt() == zzjz.zzatw) {
                zzoj.zzbe(position);
                int i = position + readInt;
                zzoj.zzbf(12);
                while (true) {
                    if (zzoj.getPosition() >= i) {
                        break;
                    }
                    int position2 = zzoj.getPosition();
                    int readInt2 = zzoj.readInt();
                    if (zzoj.readInt() == zzjz.zzatx) {
                        zzoj.zzbe(position2);
                        int i2 = position2 + readInt2;
                        zzoj.zzbf(8);
                        ArrayList arrayList = new ArrayList();
                        while (zzoj.getPosition() < i2) {
                            zzle.zza zzd = zzkl.zzd(zzoj);
                            if (zzd != null) {
                                arrayList.add(zzd);
                            }
                        }
                        if (!arrayList.isEmpty()) {
                            return new zzle((List<? extends zzle.zza>) arrayList);
                        }
                    } else {
                        zzoj.zzbf(readInt2 - 8);
                    }
                }
                return null;
            }
            zzoj.zzbf(readInt - 8);
        }
        return null;
    }

    private static int zza(zzoj zzoj, int i, int i2, zzkf zzkf, int i3) {
        zzkr zzkr;
        zzoj zzoj2 = zzoj;
        int position = zzoj.getPosition();
        while (true) {
            boolean z = false;
            if (position - i >= i2) {
                return 0;
            }
            zzoj.zzbe(position);
            int readInt = zzoj.readInt();
            zzoc.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (zzoj.readInt() == zzjz.zzasq) {
                int i4 = position + 8;
                Pair pair = null;
                Integer num = null;
                zzkr zzkr2 = null;
                boolean z2 = false;
                while (i4 - position < readInt) {
                    zzoj.zzbe(i4);
                    int readInt2 = zzoj.readInt();
                    int readInt3 = zzoj.readInt();
                    if (readInt3 == zzjz.zzasw) {
                        num = Integer.valueOf(zzoj.readInt());
                    } else if (readInt3 == zzjz.zzasr) {
                        zzoj.zzbf(4);
                        z2 = zzoj.readInt() == zzavc;
                    } else if (readInt3 == zzjz.zzass) {
                        int i5 = i4 + 8;
                        while (true) {
                            if (i5 - i4 >= readInt2) {
                                zzkr = null;
                                break;
                            }
                            zzoj.zzbe(i5);
                            int readInt4 = zzoj.readInt();
                            if (zzoj.readInt() == zzjz.zzast) {
                                zzoj.zzbf(6);
                                boolean z3 = zzoj.readUnsignedByte() == 1;
                                int readUnsignedByte = zzoj.readUnsignedByte();
                                byte[] bArr = new byte[16];
                                zzoj.zze(bArr, 0, 16);
                                zzkr = new zzkr(z3, readUnsignedByte, bArr);
                            } else {
                                i5 += readInt4;
                            }
                        }
                        zzkr2 = zzkr;
                    }
                    i4 += readInt2;
                }
                if (z2) {
                    zzoc.checkArgument(num != null, "frma atom is mandatory");
                    if (zzkr2 != null) {
                        z = true;
                    }
                    zzoc.checkArgument(z, "schi->tenc atom is mandatory");
                    pair = Pair.create(num, zzkr2);
                }
                if (pair != null) {
                    zzkf.zzavd[i3] = (zzkr) pair.second;
                    return ((Integer) pair.first).intValue();
                }
            }
            zzkf zzkf2 = zzkf;
            position += readInt;
        }
    }
}
