package com.google.android.gms.internal.ads;

final class zzhv implements Runnable {
    private final /* synthetic */ zzhr zzahf;
    private final /* synthetic */ int zzahm;
    private final /* synthetic */ long zzahn;
    private final /* synthetic */ long zzaho;

    zzhv(zzhr zzhr, int i, long j, long j2) {
        this.zzahf = zzhr;
        this.zzahm = i;
        this.zzahn = j;
        this.zzaho = j2;
    }

    public final void run() {
        this.zzahf.zzahg.zza(this.zzahm, this.zzahn, this.zzaho);
    }
}
