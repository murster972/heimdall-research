package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import java.util.Set;

public final class zzcdw implements zzdxg<zzcdu> {
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<zzcds> zzfsx;
    private final zzdxp<Set<zzcdt>> zzfsy;

    private zzcdw(zzdxp<zzcds> zzdxp, zzdxp<Set<zzcdt>> zzdxp2, zzdxp<Clock> zzdxp3) {
        this.zzfsx = zzdxp;
        this.zzfsy = zzdxp2;
        this.zzfcz = zzdxp3;
    }

    public static zzcdw zzl(zzdxp<zzcds> zzdxp, zzdxp<Set<zzcdt>> zzdxp2, zzdxp<Clock> zzdxp3) {
        return new zzcdw(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzcdu(this.zzfsx.get(), this.zzfsy.get(), this.zzfcz.get());
    }
}
