package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

final class zzdjf extends zzdik<zzdpi, zzdlj> {
    zzdjf(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdlj zzdlj = (zzdlj) obj;
        return new zzdof(zzdlj.zzass().toByteArray(), zzdlj.zzath().zzatn());
    }
}
