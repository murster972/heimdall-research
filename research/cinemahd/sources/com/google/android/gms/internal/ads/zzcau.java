package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzt;

final /* synthetic */ class zzcau implements zzt {
    private final zzbpm zzfqr;

    private zzcau(zzbpm zzbpm) {
        this.zzfqr = zzbpm;
    }

    static zzt zza(zzbpm zzbpm) {
        return new zzcau(zzbpm);
    }

    public final void zztv() {
        this.zzfqr.onAdLeftApplication();
    }
}
