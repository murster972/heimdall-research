package com.google.android.gms.internal.ads;

import java.util.concurrent.atomic.AtomicBoolean;

public final class zzbly implements zzbqb, zzps {
    private final zzczl zzfbs;
    private final zzbpd zzffh;
    private final zzbqf zzffi;
    private final AtomicBoolean zzffj = new AtomicBoolean();
    private final AtomicBoolean zzffk = new AtomicBoolean();

    public zzbly(zzczl zzczl, zzbpd zzbpd, zzbqf zzbqf) {
        this.zzfbs = zzczl;
        this.zzffh = zzbpd;
        this.zzffi = zzbqf;
    }

    private final void zzagp() {
        if (this.zzffj.compareAndSet(false, true)) {
            this.zzffh.onAdImpression();
        }
    }

    public final synchronized void onAdLoaded() {
        if (this.zzfbs.zzglj != 1) {
            zzagp();
        }
    }

    public final void zza(zzpt zzpt) {
        if (this.zzfbs.zzglj == 1 && zzpt.zzbnq) {
            zzagp();
        }
        if (zzpt.zzbnq && this.zzffk.compareAndSet(false, true)) {
            this.zzffi.zzahi();
        }
    }
}
