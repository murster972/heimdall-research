package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcjo implements zzdxg<zzcjk> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzblg> zzfyl;

    public zzcjo(zzdxp<Context> zzdxp, zzdxp<zzblg> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfyl = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcjk(this.zzejv.get(), this.zzfyl.get());
    }
}
