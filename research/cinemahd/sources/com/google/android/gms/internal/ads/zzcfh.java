package com.google.android.gms.internal.ads;

import android.content.Context;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.Callable;

final /* synthetic */ class zzcfh implements Callable {
    private final Context zzcri;

    zzcfh(Context context) {
        this.zzcri = context;
    }

    public final Object call() {
        CookieManager zzbd = zzq.zzks().zzbd(this.zzcri);
        return zzbd != null ? zzbd.getCookie("googleads.g.doubleclick.net") : "";
    }
}
