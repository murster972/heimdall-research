package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzq;

@TargetApi(24)
public class zzawm extends zzawn {
    private static boolean zze(int i, int i2, int i3) {
        return Math.abs(i - i2) <= i3;
    }

    public final boolean zza(Activity activity, Configuration configuration) {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcns)).booleanValue()) {
            return false;
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnu)).booleanValue()) {
            return activity.isInMultiWindowMode();
        }
        zzve.zzou();
        int zza = zzayk.zza((Context) activity, configuration.screenHeightDp);
        int zza2 = zzayk.zza((Context) activity, configuration.screenWidthDp);
        zzq.zzkq();
        DisplayMetrics zza3 = zzawb.zza((WindowManager) activity.getApplicationContext().getSystemService("window"));
        int i = zza3.heightPixels;
        int i2 = zza3.widthPixels;
        int identifier = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        int dimensionPixelSize = identifier > 0 ? activity.getResources().getDimensionPixelSize(identifier) : 0;
        int round = ((int) Math.round(((double) activity.getResources().getDisplayMetrics().density) + 0.5d)) * ((Integer) zzve.zzoy().zzd(zzzn.zzcnr)).intValue();
        if (!(zze(i, zza + dimensionPixelSize, round) && zze(i2, zza2, round))) {
            return true;
        }
        return false;
    }
}
