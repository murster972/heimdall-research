package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzmn;

public interface zzmq<T extends zzmn> {
    void zza(T t);
}
