package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.internal.zzq;

final /* synthetic */ class zzchd implements zzdgf {
    private final zzcua zzfwh;

    zzchd(zzcua zzcua) {
        this.zzfwh = zzcua;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfwh.zzs(zzq.zzkq().zzd((Bundle) obj));
    }
}
