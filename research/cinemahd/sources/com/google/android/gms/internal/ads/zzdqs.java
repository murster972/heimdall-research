package com.google.android.gms.internal.ads;

final class zzdqs {
    private final byte[] buffer;
    private final zzdrb zzhic;

    private zzdqs(int i) {
        this.buffer = new byte[i];
        this.zzhic = zzdrb.zzw(this.buffer);
    }

    public final zzdqk zzaya() {
        this.zzhic.zzazd();
        return new zzdqu(this.buffer);
    }

    public final zzdrb zzayb() {
        return this.zzhic;
    }

    /* synthetic */ zzdqs(int i, zzdqj zzdqj) {
        this(i);
    }
}
