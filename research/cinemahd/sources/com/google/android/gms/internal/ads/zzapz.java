package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.Collections;
import java.util.List;

public final class zzapz extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzapz> CREATOR = new zzapy();
    public final boolean zzdln;
    public final List<String> zzdlo;

    public zzapz() {
        this(false, Collections.emptyList());
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.zzdln);
        SafeParcelWriter.b(parcel, 3, this.zzdlo, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzapz(boolean z, List<String> list) {
        this.zzdln = z;
        this.zzdlo = list;
    }
}
