package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import java.util.List;

final class zzxt extends zzagt {
    private final OnInitializationCompleteListener zzcff;
    private final /* synthetic */ zzxq zzcfg;

    private zzxt(zzxq zzxq, OnInitializationCompleteListener onInitializationCompleteListener) {
        this.zzcfg = zzxq;
        this.zzcff = onInitializationCompleteListener;
    }

    public final void zzc(List<zzagn> list) throws RemoteException {
        this.zzcff.onInitializationComplete(zzxq.zzb(list));
    }

    /* synthetic */ zzxt(zzxq zzxq, OnInitializationCompleteListener onInitializationCompleteListener, zzxu zzxu) {
        this(zzxq, onInitializationCompleteListener);
    }
}
