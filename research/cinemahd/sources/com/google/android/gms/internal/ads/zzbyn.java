package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import java.util.concurrent.Executor;

public final class zzbyn implements zzdxg<zzbyl> {
    private final zzdxp<zzaxk> zzepv;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<Clock> zzfcz;

    public zzbyn(zzdxp<zzaxk> zzdxp, zzdxp<Clock> zzdxp2, zzdxp<Executor> zzdxp3) {
        this.zzepv = zzdxp;
        this.zzfcz = zzdxp2;
        this.zzfcv = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzbyl(this.zzepv.get(), this.zzfcz.get(), this.zzfcv.get());
    }
}
