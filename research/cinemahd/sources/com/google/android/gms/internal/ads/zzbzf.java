package com.google.android.gms.internal.ads;

public final class zzbzf {
    public final int type = 2;
    public final String zzcc;
    public final String zzfps;
    public final zzabu zzfpt;

    public zzbzf(String str, String str2) {
        this.zzcc = str;
        this.zzfps = str2;
        this.zzfpt = null;
    }

    public zzbzf(String str, zzabu zzabu) {
        this.zzcc = str;
        this.zzfps = null;
        this.zzfpt = zzabu;
    }
}
