package com.google.android.gms.internal.ads;

final class zzazq implements zzdgt<T> {
    private final /* synthetic */ zzazp zzdwr;
    private final /* synthetic */ zzazn zzdws;

    zzazq(zzazo zzazo, zzazp zzazp, zzazn zzazn) {
        this.zzdwr = zzazp;
        this.zzdws = zzazn;
    }

    public final void onSuccess(T t) {
        this.zzdwr.zzh(t);
    }

    public final void zzb(Throwable th) {
        this.zzdws.run();
    }
}
