package com.google.android.gms.internal.ads;

public final class zzcen implements zzdxg<zzceo> {
    private final zzdxp<zzbfx> zzejt;
    private final zzdxp<zzcec> zzfsb;

    private zzcen(zzdxp<zzcec> zzdxp, zzdxp<zzbfx> zzdxp2) {
        this.zzfsb = zzdxp;
        this.zzejt = zzdxp2;
    }

    public static zzcen zzaa(zzdxp<zzcec> zzdxp, zzdxp<zzbfx> zzdxp2) {
        return new zzcen(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzceo(this.zzfsb.get(), this.zzejt.get());
    }
}
