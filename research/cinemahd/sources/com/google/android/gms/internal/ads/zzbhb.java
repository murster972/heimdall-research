package com.google.android.gms.internal.ads;

import android.content.Context;

final class zzbhb implements zzcww {
    private final /* synthetic */ zzbgr zzerr;
    private Context zzewj;
    private String zzewk;

    private zzbhb(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    public final zzcwx zzaeb() {
        zzdxm.zza(this.zzewj, Context.class);
        zzdxm.zza(this.zzewk, String.class);
        return new zzbha(this.zzerr, this.zzewj, this.zzewk);
    }

    public final /* synthetic */ zzcww zzbt(Context context) {
        this.zzewj = (Context) zzdxm.checkNotNull(context);
        return this;
    }

    public final /* synthetic */ zzcww zzfq(String str) {
        this.zzewk = (String) zzdxm.checkNotNull(str);
        return this;
    }
}
