package com.google.android.gms.internal.ads;

final /* synthetic */ class zzakj implements zzazn {
    private final zzazl zzbru;

    zzakj(zzazl zzazl) {
        this.zzbru = zzazl;
    }

    public final void run() {
        this.zzbru.setException(new zzajr("Cannot get Javascript Engine"));
    }
}
