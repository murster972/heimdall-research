package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionProvider;

public final class zzf extends SessionProvider {
    private final CastOptions zzhk;
    private final zzw zzjd;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzf(android.content.Context r3, com.google.android.gms.cast.framework.CastOptions r4, com.google.android.gms.internal.cast.zzw r5) {
        /*
            r2 = this;
            java.util.List r0 = r4.y()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0013
            java.lang.String r0 = r4.v()
            java.lang.String r0 = com.google.android.gms.cast.CastMediaControlIntent.a(r0)
            goto L_0x001f
        L_0x0013:
            java.lang.String r0 = r4.v()
            java.util.List r1 = r4.y()
            java.lang.String r0 = com.google.android.gms.cast.CastMediaControlIntent.a(r0, r1)
        L_0x001f:
            r2.<init>(r3, r0)
            r2.zzhk = r4
            r2.zzjd = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzf.<init>(android.content.Context, com.google.android.gms.cast.framework.CastOptions, com.google.android.gms.internal.cast.zzw):void");
    }

    public final Session createSession(String str) {
        return new CastSession(getContext(), getCategory(), str, this.zzhk, Cast.c, new zzg(), new zzal(getContext(), this.zzhk, this.zzjd));
    }

    public final boolean isSessionRecoverable() {
        return this.zzhk.w();
    }
}
