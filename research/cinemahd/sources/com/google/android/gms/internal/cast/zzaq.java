package com.google.android.gms.internal.cast;

public final class zzaq {
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1 != 4) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String zzb(com.google.android.gms.cast.MediaMetadata r6) {
        /*
            java.lang.String r0 = "com.google.android.gms.cast.metadata.SUBTITLE"
            boolean r1 = r6.e(r0)
            java.lang.String r2 = "com.google.android.gms.cast.metadata.COMPOSER"
            java.lang.String r3 = "com.google.android.gms.cast.metadata.ALBUM_ARTIST"
            java.lang.String r4 = "com.google.android.gms.cast.metadata.ARTIST"
            if (r1 != 0) goto L_0x003c
            int r1 = r6.u()
            r5 = 1
            if (r1 == r5) goto L_0x003a
            r5 = 2
            if (r1 == r5) goto L_0x0037
            r5 = 3
            if (r1 == r5) goto L_0x001f
            r2 = 4
            if (r1 == r2) goto L_0x0035
            goto L_0x003c
        L_0x001f:
            boolean r1 = r6.e(r4)
            if (r1 != 0) goto L_0x0035
            boolean r1 = r6.e(r3)
            if (r1 == 0) goto L_0x002d
            r0 = r3
            goto L_0x003c
        L_0x002d:
            boolean r1 = r6.e(r2)
            if (r1 == 0) goto L_0x003c
            r0 = r2
            goto L_0x003c
        L_0x0035:
            r0 = r4
            goto L_0x003c
        L_0x0037:
            java.lang.String r0 = "com.google.android.gms.cast.metadata.SERIES_TITLE"
            goto L_0x003c
        L_0x003a:
            java.lang.String r0 = "com.google.android.gms.cast.metadata.STUDIO"
        L_0x003c:
            java.lang.String r6 = r6.f(r0)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzaq.zzb(com.google.android.gms.cast.MediaMetadata):java.lang.String");
    }
}
