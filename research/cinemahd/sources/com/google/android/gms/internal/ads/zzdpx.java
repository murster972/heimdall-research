package com.google.android.gms.internal.ads;

import com.facebook.imageutils.JfifUtil;

public final class zzdpx {
    private final byte[] zzhhh = new byte[256];
    private int zzhhi;
    private int zzhhj;

    public zzdpx(byte[] bArr) {
        for (int i = 0; i < 256; i++) {
            this.zzhhh[i] = (byte) i;
        }
        byte b = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            byte[] bArr2 = this.zzhhh;
            b = (b + bArr2[i2] + bArr[i2 % bArr.length]) & 255;
            byte b2 = bArr2[i2];
            bArr2[i2] = bArr2[b];
            bArr2[b] = b2;
        }
        this.zzhhi = 0;
        this.zzhhj = 0;
    }

    public final void zzt(byte[] bArr) {
        int i = this.zzhhi;
        int i2 = this.zzhhj;
        for (int i3 = 0; i3 < bArr.length; i3++) {
            i = (i + 1) & JfifUtil.MARKER_FIRST_BYTE;
            byte[] bArr2 = this.zzhhh;
            i2 = (i2 + bArr2[i]) & JfifUtil.MARKER_FIRST_BYTE;
            byte b = bArr2[i];
            bArr2[i] = bArr2[i2];
            bArr2[i2] = b;
            bArr[i3] = (byte) (bArr2[(bArr2[i] + bArr2[i2]) & JfifUtil.MARKER_FIRST_BYTE] ^ bArr[i3]);
        }
        this.zzhhi = i;
        this.zzhhj = i2;
    }
}
