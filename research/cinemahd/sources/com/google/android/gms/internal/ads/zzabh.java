package com.google.android.gms.internal.ads;

public final class zzabh {
    private static zzaan<String> zzcug = zzaan.zzi("gads:native:engine_js_url_with_protocol", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/production/native_ads.js");
    public static zzaan<String> zzcuh = zzaan.zzi("gads:sdk_core_location", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40-loader.html");
    private static zzaan<String> zzcui = zzaan.zzi("gads:sdk_core_js_location", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/production/sdk-core-v40-impl.js");
}
