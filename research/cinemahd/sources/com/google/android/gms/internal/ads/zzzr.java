package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Build;
import com.google.android.gms.ads.internal.zzq;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Future;
import okhttp3.internal.cache.DiskLruCache;

public final class zzzr {
    private String zzbma = null;
    private String zzcrj;
    private Map<String, String> zzcrk;
    private Context zzup = null;

    public zzzr(Context context, String str) {
        String str2;
        this.zzup = context;
        this.zzbma = str;
        this.zzcrj = zzaav.zzcsv.get();
        this.zzcrk = new LinkedHashMap();
        this.zzcrk.put("s", "gmob_sdk");
        this.zzcrk.put("v", "3");
        this.zzcrk.put("os", Build.VERSION.RELEASE);
        this.zzcrk.put("sdk", Build.VERSION.SDK);
        Map<String, String> map = this.zzcrk;
        zzq.zzkq();
        map.put("device", zzawb.zzwl());
        Map<String, String> map2 = this.zzcrk;
        if (context.getApplicationContext() != null) {
            str2 = context.getApplicationContext().getPackageName();
        } else {
            str2 = context.getPackageName();
        }
        map2.put("app", str2);
        Map<String, String> map3 = this.zzcrk;
        zzq.zzkq();
        map3.put("is_lite_sdk", zzawb.zzay(context) ? DiskLruCache.VERSION_1 : "0");
        Future<zzaqt> zzx = zzq.zzlb().zzx(this.zzup);
        try {
            this.zzcrk.put("network_coarse", Integer.toString(zzx.get().zzdmo));
            this.zzcrk.put("network_fine", Integer.toString(zzx.get().zzdmp));
        } catch (Exception e) {
            zzq.zzku().zza(e, "CsiConfiguration.CsiConfiguration");
        }
    }

    /* access modifiers changed from: package-private */
    public final Context getContext() {
        return this.zzup;
    }

    /* access modifiers changed from: package-private */
    public final String zzkk() {
        return this.zzbma;
    }

    /* access modifiers changed from: package-private */
    public final String zzqk() {
        return this.zzcrj;
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> zzql() {
        return this.zzcrk;
    }
}
