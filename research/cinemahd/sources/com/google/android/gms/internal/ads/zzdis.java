package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

public interface zzdis<P> {
    P zza(zzdiq<P> zzdiq) throws GeneralSecurityException;

    Class<P> zzarz();
}
