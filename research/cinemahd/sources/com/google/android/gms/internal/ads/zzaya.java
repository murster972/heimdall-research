package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

public final class zzaya {
    private final Object lock = new Object();
    private long zzdut;
    private long zzduu = Long.MIN_VALUE;

    public zzaya(long j) {
        this.zzdut = j;
    }

    public final boolean tryAcquire() {
        synchronized (this.lock) {
            long a2 = zzq.zzkx().a();
            if (this.zzduu + this.zzdut > a2) {
                return false;
            }
            this.zzduu = a2;
            return true;
        }
    }

    public final void zzfb(long j) {
        synchronized (this.lock) {
            this.zzdut = j;
        }
    }
}
