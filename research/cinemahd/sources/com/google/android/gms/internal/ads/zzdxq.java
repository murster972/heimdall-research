package com.google.android.gms.internal.ads;

public final class zzdxq<T> implements zzdxp<T> {
    private static final Object zzhzz = new Object();
    private volatile Object zzduw = zzhzz;
    private volatile zzdxp<T> zziaa;

    private zzdxq(zzdxp<T> zzdxp) {
        this.zziaa = zzdxp;
    }

    public static <P extends zzdxp<T>, T> zzdxp<T> zzan(P p) {
        return ((p instanceof zzdxq) || (p instanceof zzdxd)) ? p : new zzdxq((zzdxp) zzdxm.checkNotNull(p));
    }

    public final T get() {
        T t = this.zzduw;
        if (t != zzhzz) {
            return t;
        }
        zzdxp<T> zzdxp = this.zziaa;
        if (zzdxp == null) {
            return this.zzduw;
        }
        T t2 = zzdxp.get();
        this.zzduw = t2;
        this.zziaa = null;
        return t2;
    }
}
