package com.google.android.gms.internal.ads;

public final class zzddq {
    private static final zzddr zzgtr;
    private static volatile zzddr zzgts;

    static {
        zzdds zzdds = new zzdds();
        zzgtr = zzdds;
        zzgts = zzdds;
    }

    public static zzddr zzaqs() {
        return zzgts;
    }
}
