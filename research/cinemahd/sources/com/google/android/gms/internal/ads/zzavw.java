package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzavw implements Runnable {
    private final Context zzcey;
    private final String zzdbl;
    private final zzavx zzdrv;

    zzavw(zzavx zzavx, Context context, String str) {
        this.zzdrv = zzavx;
        this.zzcey = context;
        this.zzdbl = str;
    }

    public final void run() {
        this.zzdrv.zzp(this.zzcey, this.zzdbl);
    }
}
