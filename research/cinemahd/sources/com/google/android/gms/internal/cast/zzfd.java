package com.google.android.gms.internal.cast;

final class zzfd extends zzfg {
    private final /* synthetic */ zzfc zzabn;

    zzfd(zzfc zzfc) {
        this.zzabn = zzfc;
    }

    public final void doFrame(long j) {
        zzfc.zza(this.zzabn);
        zzfc zzfc = this.zzabn;
        if (!zzfc.zzb(zzfc.animator) && !this.zzabn.animator.isStarted() && !this.zzabn.zzfg()) {
            if (this.zzabn.zzabj != null) {
                this.zzabn.zzabj.run();
            }
            this.zzabn.animator.start();
        }
    }
}
