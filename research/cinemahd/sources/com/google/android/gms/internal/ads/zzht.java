package com.google.android.gms.internal.ads;

final class zzht implements Runnable {
    private final /* synthetic */ zzhr zzahf;
    private final /* synthetic */ String zzahi;
    private final /* synthetic */ long zzahj;
    private final /* synthetic */ long zzahk;

    zzht(zzhr zzhr, String str, long j, long j2) {
        this.zzahf = zzhr;
        this.zzahi = str;
        this.zzahj = j;
        this.zzahk = j2;
    }

    public final void run() {
        this.zzahf.zzahg.zza(this.zzahi, this.zzahj, this.zzahk);
    }
}
