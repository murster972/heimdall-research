package com.google.android.gms.internal.ads;

public interface zzjm {
    long getDurationUs();

    long zzdz(long j);

    boolean zzgh();
}
