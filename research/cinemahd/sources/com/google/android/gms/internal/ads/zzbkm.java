package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Iterator;
import java.util.concurrent.Executor;

final class zzbkm extends zzbkk {
    private final View view;
    private final zzbdi zzcza;
    private final Executor zzfci;
    private final zzczk zzfdo;
    private final zzbme zzfdz;
    private final zzbwz zzfea;
    private final zzbsy zzfeb;
    private final zzdxa<zzcok> zzfec;
    private zzuj zzfed;
    private final Context zzup;

    zzbkm(zzbmg zzbmg, Context context, zzczk zzczk, View view2, zzbdi zzbdi, zzbme zzbme, zzbwz zzbwz, zzbsy zzbsy, zzdxa<zzcok> zzdxa, Executor executor) {
        super(zzbmg);
        this.zzup = context;
        this.view = view2;
        this.zzcza = zzbdi;
        this.zzfdo = zzczk;
        this.zzfdz = zzbme;
        this.zzfea = zzbwz;
        this.zzfeb = zzbsy;
        this.zzfec = zzdxa;
        this.zzfci = executor;
    }

    public final zzxb getVideoController() {
        try {
            return this.zzfdz.getVideoController();
        } catch (zzdab unused) {
            return null;
        }
    }

    public final void zza(ViewGroup viewGroup, zzuj zzuj) {
        zzbdi zzbdi;
        if (viewGroup != null && (zzbdi = this.zzcza) != null) {
            zzbdi.zza(zzbey.zzb(zzuj));
            viewGroup.setMinimumHeight(zzuj.heightPixels);
            viewGroup.setMinimumWidth(zzuj.widthPixels);
            this.zzfed = zzuj;
        }
    }

    public final zzczk zzafz() {
        boolean z;
        zzuj zzuj = this.zzfed;
        if (zzuj != null) {
            return zzczy.zze(zzuj);
        }
        zzczl zzczl = this.zzffc;
        if (zzczl.zzgma) {
            Iterator<String> it2 = zzczl.zzgli.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                String next = it2.next();
                if (next != null && next.contains("FirstParty")) {
                    z = true;
                    break;
                }
            }
            if (!z) {
                return new zzczk(this.view.getWidth(), this.view.getHeight(), false);
            }
        }
        return zzczy.zza(this.zzffc.zzgln, this.zzfdo);
    }

    public final View zzaga() {
        return this.view;
    }

    public final int zzage() {
        return this.zzfbl.zzgmi.zzgmf.zzgmb;
    }

    public final void zzagf() {
        this.zzfci.execute(new zzbkl(this));
        super.zzagf();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzagg() {
        if (this.zzfea.zzajs() != null) {
            try {
                this.zzfea.zzajs().zza(this.zzfec.get(), ObjectWrapper.a(this.zzup));
            } catch (RemoteException e) {
                zzayu.zzc("RemoteException when notifyAdLoad is called", e);
            }
        }
    }

    public final void zzjy() {
        this.zzfeb.zzaia();
    }
}
