package com.google.android.gms.internal.ads;

import org.json.JSONObject;

public final class zzbwx implements zzdxg<zzbwv> {
    private final zzdxp<JSONObject> zzety;
    private final zzdxp<zzczl> zzfda;

    public zzbwx(zzdxp<zzczl> zzdxp, zzdxp<JSONObject> zzdxp2) {
        this.zzfda = zzdxp;
        this.zzety = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzbwv(this.zzfda.get(), this.zzety.get());
    }
}
