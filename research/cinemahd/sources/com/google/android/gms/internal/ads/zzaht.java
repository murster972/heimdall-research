package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaht implements Runnable {
    private final zzahr zzcyq;
    private final String zzcyr;

    zzaht(zzahr zzahr, String str) {
        this.zzcyq = zzahr;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzcyq.zzda(this.zzcyr);
    }
}
