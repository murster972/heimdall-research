package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

final /* synthetic */ class zzbzd implements zzded {
    static final zzded zzdoq = new zzbzd();

    private zzbzd() {
    }

    public final Object apply(Object obj) {
        ArrayList arrayList = new ArrayList();
        for (zzbzf zzbzf : (List) obj) {
            if (zzbzf != null) {
                arrayList.add(zzbzf);
            }
        }
        return arrayList;
    }
}
