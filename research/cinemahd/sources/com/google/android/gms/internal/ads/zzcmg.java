package com.google.android.gms.internal.ads;

public final class zzcmg implements zzdxg<zzcmd> {
    private final zzdxp<zzbra> zzerj;
    private final zzdxp<zzbpm> zzesd;
    private final zzdxp<zzboq> zzesg;
    private final zzdxp<zzbpd> zzesj;
    private final zzdxp<zzbpw> zzesq;
    private final zzdxp<zzbqj> zzesu;
    private final zzdxp<zzbtj> zzesz;
    private final zzdxp<zzboz> zzfaa;
    private final zzdxp<zzbte> zzgap;

    public zzcmg(zzdxp<zzboq> zzdxp, zzdxp<zzbpd> zzdxp2, zzdxp<zzbpm> zzdxp3, zzdxp<zzbpw> zzdxp4, zzdxp<zzboz> zzdxp5, zzdxp<zzbra> zzdxp6, zzdxp<zzbtj> zzdxp7, zzdxp<zzbqj> zzdxp8, zzdxp<zzbte> zzdxp9) {
        this.zzesg = zzdxp;
        this.zzesj = zzdxp2;
        this.zzesd = zzdxp3;
        this.zzesq = zzdxp4;
        this.zzfaa = zzdxp5;
        this.zzerj = zzdxp6;
        this.zzesz = zzdxp7;
        this.zzesu = zzdxp8;
        this.zzgap = zzdxp9;
    }

    public final /* synthetic */ Object get() {
        return new zzcmd(this.zzesg.get(), this.zzesj.get(), this.zzesd.get(), this.zzesq.get(), this.zzfaa.get(), this.zzerj.get(), this.zzesz.get(), this.zzesu.get(), this.zzgap.get());
    }
}
