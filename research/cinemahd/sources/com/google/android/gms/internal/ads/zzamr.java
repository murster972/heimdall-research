package com.google.android.gms.internal.ads;

import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public final class zzamr {
    public static int zza(AdRequest.ErrorCode errorCode) {
        int i = zzamu.zzdej[errorCode.ordinal()];
        if (i == 2) {
            return 1;
        }
        if (i != 3) {
            return i != 4 ? 0 : 3;
        }
        return 2;
    }

    public static MediationAdRequest zza(zzug zzug, boolean z) {
        AdRequest.Gender gender;
        List<String> list = zzug.zzcca;
        HashSet hashSet = list != null ? new HashSet(list) : null;
        Date date = new Date(zzug.zzcby);
        int i = zzug.zzcbz;
        if (i == 1) {
            gender = AdRequest.Gender.MALE;
        } else if (i != 2) {
            gender = AdRequest.Gender.UNKNOWN;
        } else {
            gender = AdRequest.Gender.FEMALE;
        }
        return new MediationAdRequest(date, gender, hashSet, z, zzug.zzmi);
    }
}
