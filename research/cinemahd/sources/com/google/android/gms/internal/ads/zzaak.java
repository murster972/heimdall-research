package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzaak extends IInterface {
    void zza(zzaaf zzaaf) throws RemoteException;
}
