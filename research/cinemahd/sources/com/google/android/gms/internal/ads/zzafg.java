package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzc;
import java.util.Map;

final class zzafg implements zzafn<zzbdi> {
    zzafg() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (zzbdi.zzaao() != null) {
            zzbdi.zzaao().zzmm();
        }
        zzc zzzw = zzbdi.zzzw();
        if (zzzw != null) {
            zzzw.close();
            return;
        }
        zzc zzzx = zzbdi.zzzx();
        if (zzzx != null) {
            zzzx.close();
        } else {
            zzayu.zzez("A GMSG tried to close something that wasn't an overlay.");
        }
    }
}
