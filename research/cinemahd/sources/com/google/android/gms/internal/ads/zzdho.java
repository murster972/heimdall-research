package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class zzdho<V> extends zzdgm<V> {
    /* access modifiers changed from: private */
    public zzdhe<V> zzgxk;
    /* access modifiers changed from: private */
    public ScheduledFuture<?> zzgxl;

    private zzdho(zzdhe<V> zzdhe) {
        this.zzgxk = (zzdhe) zzdei.checkNotNull(zzdhe);
    }

    static <V> zzdhe<V> zzb(zzdhe<V> zzdhe, long j, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        zzdho zzdho = new zzdho(zzdhe);
        zzdhq zzdhq = new zzdhq(zzdho);
        zzdho.zzgxl = scheduledExecutorService.schedule(zzdhq, j, timeUnit);
        zzdhe.addListener(zzdhq, zzdgl.INSTANCE);
        return zzdho;
    }

    /* access modifiers changed from: protected */
    public final void afterDone() {
        maybePropagateCancellationTo(this.zzgxk);
        ScheduledFuture<?> scheduledFuture = this.zzgxl;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
        this.zzgxk = null;
        this.zzgxl = null;
    }

    /* access modifiers changed from: protected */
    public final String pendingToString() {
        zzdhe<V> zzdhe = this.zzgxk;
        ScheduledFuture<?> scheduledFuture = this.zzgxl;
        if (zzdhe == null) {
            return null;
        }
        String valueOf = String.valueOf(zzdhe);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("inputFuture=[");
        sb.append(valueOf);
        sb.append("]");
        String sb2 = sb.toString();
        if (scheduledFuture == null) {
            return sb2;
        }
        long delay = scheduledFuture.getDelay(TimeUnit.MILLISECONDS);
        if (delay <= 0) {
            return sb2;
        }
        String valueOf2 = String.valueOf(sb2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 43);
        sb3.append(valueOf2);
        sb3.append(", remaining delay=[");
        sb3.append(delay);
        sb3.append(" ms]");
        return sb3.toString();
    }
}
