package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzub extends RemoteCreator<zzvs> {
    public zzub() {
        super("com.google.android.gms.ads.AdLoaderBuilderCreatorImpl");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object getRemoteCreator(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilderCreator");
        if (queryLocalInterface instanceof zzvs) {
            return (zzvs) queryLocalInterface;
        }
        return new zzvr(iBinder);
    }

    public final zzvn zza(Context context, String str, zzalc zzalc) {
        try {
            IBinder zzc = ((zzvs) getRemoteCreatorInstance(context)).zzc(ObjectWrapper.a(context), str, zzalc, 19649000);
            if (zzc == null) {
                return null;
            }
            IInterface queryLocalInterface = zzc.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
            if (queryLocalInterface instanceof zzvn) {
                return (zzvn) queryLocalInterface;
            }
            return new zzvp(zzc);
        } catch (RemoteException | RemoteCreator.RemoteCreatorException e) {
            zzayu.zzd("Could not create remote builder for AdLoader.", e);
            return null;
        }
    }
}
