package com.google.android.gms.internal.ads;

import java.nio.charset.Charset;

public final class zzdvu {
    private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final Object zzhtt = new Object();

    public static void zza(zzdvq zzdvq, zzdvq zzdvq2) {
        zzdvs zzdvs = zzdvq.zzhtm;
        if (zzdvs != null) {
            zzdvq2.zzhtm = (zzdvs) zzdvs.clone();
        }
    }
}
