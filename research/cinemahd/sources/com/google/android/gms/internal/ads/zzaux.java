package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzaux implements Parcelable.Creator<zzauu> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        zzuj zzuj = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 == 2) {
                str2 = SafeParcelReader.o(parcel, a2);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                zzuj = (zzuj) SafeParcelReader.a(parcel, a2, zzuj.CREATOR);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzauu(str, str2, zzuj);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzauu[i];
    }
}
