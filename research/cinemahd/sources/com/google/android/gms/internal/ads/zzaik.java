package com.google.android.gms.internal.ads;

final /* synthetic */ class zzaik implements Runnable {
    private final String zzcyr;
    private final zzaih zzczc;

    zzaik(zzaih zzaih, String str) {
        this.zzczc = zzaih;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzczc.zzdd(this.zzcyr);
    }
}
