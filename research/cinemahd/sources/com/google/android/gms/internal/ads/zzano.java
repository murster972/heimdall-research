package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.rtb.RtbAdapter;

public class zzano {
    public static zzani zzdp(String str) throws RemoteException {
        try {
            return new zzann((RtbAdapter) Class.forName(str, false, zzano.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
        } catch (Throwable unused) {
            throw new RemoteException();
        }
    }
}
