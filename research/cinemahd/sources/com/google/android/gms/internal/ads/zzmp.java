package com.google.android.gms.internal.ads;

public final class zzmp extends zzhg {
    private static final Object zzbcy = new Object();
    private final boolean zzagu;
    private final boolean zzagv;
    private final long zzbcz;
    private final long zzbda;
    private final long zzbdb;
    private final long zzbdc;

    public zzmp(long j, boolean z) {
        this(j, j, 0, 0, z, false);
    }

    public final zzhl zza(int i, zzhl zzhl, boolean z, long j) {
        zzoc.zzc(i, 0, 1);
        boolean z2 = this.zzagu;
        long j2 = this.zzbda;
        zzhl.zzagi = null;
        zzhl.zzags = -9223372036854775807L;
        zzhl.zzagt = -9223372036854775807L;
        zzhl.zzagu = z2;
        zzhl.zzagv = false;
        zzhl.zzagy = 0;
        zzhl.zzagj = j2;
        zzhl.zzagw = 0;
        zzhl.zzagx = 0;
        zzhl.zzagz = 0;
        return zzhl;
    }

    public final int zzc(Object obj) {
        return zzbcy.equals(obj) ? 0 : -1;
    }

    public final int zzev() {
        return 1;
    }

    public final int zzew() {
        return 1;
    }

    private zzmp(long j, long j2, long j3, long j4, boolean z, boolean z2) {
        this.zzbcz = j;
        this.zzbda = j2;
        this.zzbdb = 0;
        this.zzbdc = 0;
        this.zzagu = z;
        this.zzagv = false;
    }

    public final zzhi zza(int i, zzhi zzhi, boolean z) {
        zzoc.zzc(i, 0, 1);
        Object obj = z ? zzbcy : null;
        return zzhi.zza(obj, obj, 0, this.zzbcz, 0, false);
    }
}
