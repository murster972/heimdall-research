package com.google.android.gms.internal.ads;

public final class zzaqt {
    public final float zzboh;
    public final int zzdgf;
    public final int zzdgg;
    private final int zzdmh;
    private final boolean zzdmi;
    private final boolean zzdmj;
    private final int zzdmk;
    private final int zzdml;
    private final int zzdmm;
    private final String zzdmn;
    public final int zzdmo;
    public final int zzdmp;
    private final int zzdmq;
    private final boolean zzdmr;
    private final int zzdms;
    private final double zzdmt;
    private final boolean zzdmu;
    private final String zzdmv;
    private final String zzdmw;
    public final boolean zzdmx;
    public final boolean zzdmy;
    public final String zzdmz;
    public final boolean zzdna;
    public final boolean zzdnb;
    public final boolean zzdnc;
    public final String zzdnd;
    public final String zzdne;
    public final String zzdnf;
    private final boolean zzdng;

    zzaqt(int i, boolean z, boolean z2, String str, String str2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str3, String str4, String str5, int i2, int i3, int i4, int i5, int i6, int i7, float f, int i8, int i9, double d, boolean z8, boolean z9, int i10, String str6, boolean z10, String str7) {
        this.zzdmh = i;
        this.zzdmx = z;
        this.zzdmy = z2;
        this.zzdmn = str;
        this.zzdmz = str2;
        this.zzdna = z3;
        this.zzdnb = z4;
        this.zzdnc = z5;
        this.zzdmi = z6;
        this.zzdmj = z7;
        this.zzdnd = str3;
        this.zzdne = str4;
        this.zzdmk = i2;
        this.zzdmo = i3;
        this.zzdmp = i4;
        this.zzdmq = i5;
        this.zzdml = i6;
        this.zzdmm = i7;
        this.zzboh = f;
        this.zzdgf = i8;
        this.zzdgg = i9;
        this.zzdmt = d;
        this.zzdmu = z8;
        this.zzdmr = z9;
        this.zzdms = i10;
        this.zzdmv = str6;
        this.zzdng = z10;
        this.zzdnf = str5;
        this.zzdmw = str7;
    }
}
