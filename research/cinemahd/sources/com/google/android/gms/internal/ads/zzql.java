package com.google.android.gms.internal.ads;

import android.view.View;

final class zzql implements Runnable {
    private final /* synthetic */ zzqi zzbpz;
    private final /* synthetic */ View zzbqa;

    zzql(zzqi zzqi, View view) {
        this.zzbpz = zzqi;
        this.zzbqa = view;
    }

    public final void run() {
        this.zzbpz.zzi(this.zzbqa);
    }
}
