package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcqn implements zzcty<Bundle> {
    private final zzuo zzgey;

    public zzcqn(zzuo zzuo) {
        this.zzgey = zzuo;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        zzuo zzuo = this.zzgey;
        if (zzuo != null) {
            int i = zzuo.orientation;
            if (i == 1) {
                bundle.putString("avo", "p");
            } else if (i == 2) {
                bundle.putString("avo", "l");
            }
        }
    }
}
