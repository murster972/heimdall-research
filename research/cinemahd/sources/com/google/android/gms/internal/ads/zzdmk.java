package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdmk extends zzdrt<zzdmk, zza> implements zzdtg {
    private static volatile zzdtn<zzdmk> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmk zzhbr;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;
    private zzdmn zzhbq;

    public static final class zza extends zzdrt.zzb<zzdmk, zza> implements zzdtg {
        private zza() {
            super(zzdmk.zzhbr);
        }

        public final zza zzam(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmk) this.zzhmp).zzs(zzdqk);
            return this;
        }

        public final zza zzb(zzdmn zzdmn) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmk) this.zzhmp).zza(zzdmn);
            return this;
        }

        public final zza zzej(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmk) this.zzhmp).setVersion(0);
            return this;
        }

        /* synthetic */ zza(zzdml zzdml) {
            this();
        }
    }

    static {
        zzdmk zzdmk = new zzdmk();
        zzhbr = zzdmk;
        zzdrt.zza(zzdmk.class, zzdmk);
    }

    private zzdmk() {
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdmn zzdmn) {
        zzdmn.getClass();
        this.zzhbq = zzdmn;
    }

    public static zzdmk zzal(zzdqk zzdqk) throws zzdse {
        return (zzdmk) zzdrt.zza(zzhbr, zzdqk);
    }

    public static zza zzaun() {
        return (zza) zzhbr.zzazt();
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }

    public final zzdmn zzaum() {
        zzdmn zzdmn = this.zzhbq;
        return zzdmn == null ? zzdmn.zzaus() : zzdmn;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdml.zzdk[i - 1]) {
            case 1:
                return new zzdmk();
            case 2:
                return new zza((zzdml) null);
            case 3:
                return zzdrt.zza((zzdte) zzhbr, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", new Object[]{"zzhaa", "zzhbq", "zzhab"});
            case 4:
                return zzhbr;
            case 5:
                zzdtn<zzdmk> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmk.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbr);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
