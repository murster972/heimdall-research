package com.google.android.gms.internal.ads;

import android.util.JsonWriter;
import com.google.android.gms.common.util.Clock;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public final class zzcec {
    private final Clock zzbmq;

    public zzcec(Clock clock) {
        this.zzbmq = clock;
    }

    public final void zza(List<Object> list, String str, String str2, Object... objArr) {
        if (zzabc.zzctx.get().booleanValue()) {
            long b = this.zzbmq.b();
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            try {
                jsonWriter.beginObject();
                jsonWriter.name(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP).value(b);
                jsonWriter.name("source").value(str);
                jsonWriter.name("event").value(str2);
                jsonWriter.name("components").beginArray();
                for (Object obj : list) {
                    jsonWriter.value(obj.toString());
                }
                jsonWriter.endArray();
                jsonWriter.name("params").beginArray();
                int length = objArr.length;
                for (int i = 0; i < length; i++) {
                    Object obj2 = objArr[i];
                    jsonWriter.value(obj2 != null ? obj2.toString() : null);
                }
                jsonWriter.endArray();
                jsonWriter.endObject();
                jsonWriter.flush();
                jsonWriter.close();
            } catch (IOException e) {
                zzayu.zzc("unable to log", e);
            }
            String valueOf = String.valueOf(stringWriter.toString());
            zzayu.zzey(valueOf.length() != 0 ? "AD-DBG ".concat(valueOf) : new String("AD-DBG "));
        }
    }
}
