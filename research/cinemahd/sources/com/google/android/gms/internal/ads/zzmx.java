package com.google.android.gms.internal.ads;

import android.text.TextUtils;

public final class zzmx {
    public final int viewportHeight;
    public final int viewportWidth;
    public final String zzbdi;
    public final String zzbdj;
    public final boolean zzbdk;
    public final boolean zzbdl;
    public final int zzbdm;
    public final int zzbdn;
    public final int zzbdo;
    public final boolean zzbdp;
    public final boolean zzbdq;
    public final boolean zzbdr;

    public zzmx() {
        this((String) null, (String) null, false, true, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, true, true, Integer.MAX_VALUE, Integer.MAX_VALUE, true);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && zzmx.class == obj.getClass()) {
            zzmx zzmx = (zzmx) obj;
            return this.zzbdl == zzmx.zzbdl && this.zzbdm == zzmx.zzbdm && this.zzbdn == zzmx.zzbdn && this.zzbdp == zzmx.zzbdp && this.zzbdq == zzmx.zzbdq && this.zzbdr == zzmx.zzbdr && this.viewportWidth == zzmx.viewportWidth && this.viewportHeight == zzmx.viewportHeight && this.zzbdo == zzmx.zzbdo && TextUtils.equals((CharSequence) null, (CharSequence) null) && TextUtils.equals((CharSequence) null, (CharSequence) null);
        }
    }

    public final int hashCode() {
        String str = null;
        return (((((((((((((((((((str.hashCode() * 31) + str.hashCode()) * 31 * 31) + (this.zzbdl ? 1 : 0)) * 31) + this.zzbdm) * 31) + this.zzbdn) * 31) + this.zzbdo) * 31) + (this.zzbdp ? 1 : 0)) * 31) + (this.zzbdq ? 1 : 0)) * 31) + (this.zzbdr ? 1 : 0)) * 31) + this.viewportWidth) * 31) + this.viewportHeight;
    }

    private zzmx(String str, String str2, boolean z, boolean z2, int i, int i2, int i3, boolean z3, boolean z4, int i4, int i5, boolean z5) {
        this.zzbdi = null;
        this.zzbdj = null;
        this.zzbdk = false;
        this.zzbdl = true;
        this.zzbdm = Integer.MAX_VALUE;
        this.zzbdn = Integer.MAX_VALUE;
        this.zzbdo = Integer.MAX_VALUE;
        this.zzbdp = true;
        this.zzbdq = true;
        this.viewportWidth = Integer.MAX_VALUE;
        this.viewportHeight = Integer.MAX_VALUE;
        this.zzbdr = true;
    }
}
