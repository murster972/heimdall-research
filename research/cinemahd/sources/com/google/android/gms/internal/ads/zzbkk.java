package com.google.android.gms.internal.ads;

import android.view.View;
import android.view.ViewGroup;

public abstract class zzbkk extends zzbmd {
    protected zzbkk(zzbmg zzbmg) {
        super(zzbmg);
    }

    public abstract zzxb getVideoController();

    public abstract void zza(ViewGroup viewGroup, zzuj zzuj);

    public abstract zzczk zzafz();

    public abstract View zzaga();

    public abstract int zzage();

    public abstract void zzjy();
}
