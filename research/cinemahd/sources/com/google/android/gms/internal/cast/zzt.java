package com.google.android.gms.internal.cast;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class zzt extends AnimatorListenerAdapter {
    private final /* synthetic */ zzs zzjm;

    zzt(zzs zzs) {
        this.zzjm = zzs;
    }

    public final void onAnimationEnd(Animator animator) {
        this.zzjm.zzjl.zzaw();
    }
}
