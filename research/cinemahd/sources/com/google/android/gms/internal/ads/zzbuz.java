package com.google.android.gms.internal.ads;

public final class zzbuz implements zzdxg<zzbva> {
    private final zzdxp<zzbpd> zzfjk;
    private final zzdxp<zzczl> zzfjl;

    private zzbuz(zzdxp<zzbpd> zzdxp, zzdxp<zzczl> zzdxp2) {
        this.zzfjk = zzdxp;
        this.zzfjl = zzdxp2;
    }

    public static zzbuz zzk(zzdxp<zzbpd> zzdxp, zzdxp<zzczl> zzdxp2) {
        return new zzbuz(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzbva(this.zzfjk.get(), this.zzfjl.get());
    }
}
