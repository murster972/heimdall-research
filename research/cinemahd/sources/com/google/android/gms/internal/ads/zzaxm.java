package com.google.android.gms.internal.ads;

final class zzaxm implements zzy {
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ zzaxr zzduh;

    zzaxm(zzaxk zzaxk, String str, zzaxr zzaxr) {
        this.zzdug = str;
        this.zzduh = zzaxr;
    }

    public final void zzc(zzae zzae) {
        String str = this.zzdug;
        String exc = zzae.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(exc).length());
        sb.append("Failed to load URL: ");
        sb.append(str);
        sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        sb.append(exc);
        zzayu.zzez(sb.toString());
        this.zzduh.zzb(null);
    }
}
