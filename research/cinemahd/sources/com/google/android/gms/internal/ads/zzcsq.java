package com.google.android.gms.internal.ads;

import android.content.pm.PackageInfo;

public final class zzcsq implements zzdxg<zzcsn> {
    private final zzdxp<zzavu> zzemi;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<PackageInfo> zzfur;

    public zzcsq(zzdxp<zzdhd> zzdxp, zzdxp<zzczu> zzdxp2, zzdxp<PackageInfo> zzdxp3, zzdxp<zzavu> zzdxp4) {
        this.zzfcv = zzdxp;
        this.zzfep = zzdxp2;
        this.zzfur = zzdxp3;
        this.zzemi = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcsn(this.zzfcv.get(), this.zzfep.get(), this.zzfur.get(), this.zzemi.get());
    }
}
