package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcxx implements zzdxg<zzcxt<zzcbi, zzcbb>> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdax> zzelm;
    private final zzdxp<zzdbn> zzeln;

    public zzcxx(zzdxp<Context> zzdxp, zzdxp<zzdax> zzdxp2, zzdxp<zzdbn> zzdxp3) {
        this.zzejv = zzdxp;
        this.zzelm = zzdxp2;
        this.zzeln = zzdxp3;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: com.google.android.gms.internal.ads.zzcxr} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v2, resolved type: com.google.android.gms.internal.ads.zzcxg} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: com.google.android.gms.internal.ads.zzcxg} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object get() {
        /*
            r9 = this;
            com.google.android.gms.internal.ads.zzdxp<android.content.Context> r0 = r9.zzejv
            java.lang.Object r0 = r0.get()
            android.content.Context r0 = (android.content.Context) r0
            com.google.android.gms.internal.ads.zzdxp<com.google.android.gms.internal.ads.zzdax> r1 = r9.zzelm
            java.lang.Object r1 = r1.get()
            r4 = r1
            com.google.android.gms.internal.ads.zzdax r4 = (com.google.android.gms.internal.ads.zzdax) r4
            com.google.android.gms.internal.ads.zzdxp<com.google.android.gms.internal.ads.zzdbn> r1 = r9.zzeln
            java.lang.Object r1 = r1.get()
            com.google.android.gms.internal.ads.zzdbn r1 = (com.google.android.gms.internal.ads.zzdbn) r1
            com.google.android.gms.internal.ads.zzzc<java.lang.Integer> r2 = com.google.android.gms.internal.ads.zzzn.zzcql
            com.google.android.gms.internal.ads.zzzj r3 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r2 = r3.zzd(r2)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            if (r2 <= 0) goto L_0x0044
            com.google.android.gms.internal.ads.zzcxg r8 = new com.google.android.gms.internal.ads.zzcxg
            com.google.android.gms.internal.ads.zzdbh r2 = com.google.android.gms.internal.ads.zzdbh.Rewarded
            com.google.android.gms.internal.ads.zzdbb r3 = r1.zzb(r2, r0)
            com.google.android.gms.internal.ads.zzcxr r5 = new com.google.android.gms.internal.ads.zzcxr
            r5.<init>()
            com.google.android.gms.internal.ads.zzcxf r6 = new com.google.android.gms.internal.ads.zzcxf
            r6.<init>()
            com.google.android.gms.internal.ads.zzdhd r7 = com.google.android.gms.internal.ads.zzazd.zzdwe
            r2 = r8
            r2.<init>(r3, r4, r5, r6, r7)
            goto L_0x0049
        L_0x0044:
            com.google.android.gms.internal.ads.zzcxr r8 = new com.google.android.gms.internal.ads.zzcxr
            r8.<init>()
        L_0x0049:
            java.lang.String r0 = "Cannot return null from a non-@Nullable @Provides method"
            java.lang.Object r0 = com.google.android.gms.internal.ads.zzdxm.zza(r8, (java.lang.String) r0)
            com.google.android.gms.internal.ads.zzcxt r0 = (com.google.android.gms.internal.ads.zzcxt) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcxx.get():java.lang.Object");
    }
}
