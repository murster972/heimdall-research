package com.google.android.gms.internal.cast;

import com.facebook.ads.AdError;
import org.json.JSONObject;

final class zzce extends zzch {
    private final /* synthetic */ zzcb zzwi;
    private final /* synthetic */ int zzwk;
    private final /* synthetic */ String zzwl;
    private final /* synthetic */ JSONObject zzwm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzce(zzcb zzcb, int i, String str, JSONObject jSONObject) {
        super(zzcb);
        this.zzwi = zzcb;
        this.zzwk = i;
        this.zzwl = str;
        this.zzwm = jSONObject;
    }

    public final void execute() {
        int i = this.zzwk;
        int i2 = 5;
        if (i != 2) {
            i2 = i != 3 ? i != 4 ? i != 5 ? i != 6 ? 0 : 4 : 3 : 2 : 1;
        }
        if (i2 == 0) {
            this.zzwq.zza(-1, AdError.INTERNAL_ERROR_CODE, (Object) null);
            zzcb.zzbf.w("sendPlayerRequest for unsupported playerState: %d", Integer.valueOf(this.zzwk));
            return;
        }
        this.zzwi.zza(this.zzwl, i2, this.zzwm, this.zzwq);
    }
}
