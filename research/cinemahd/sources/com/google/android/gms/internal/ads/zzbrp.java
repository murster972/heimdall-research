package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

public final class zzbrp implements zzdxg<Set<zzbsu<zzbph>>> {
    private final zzbrm zzfim;

    private zzbrp(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbrp zzg(zzbrm zzbrm) {
        return new zzbrp(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.emptySet(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
