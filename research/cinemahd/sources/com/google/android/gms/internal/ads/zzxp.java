package com.google.android.gms.internal.ads;

import android.content.Context;

final /* synthetic */ class zzxp implements Runnable {
    private final zzxq zzcex;
    private final Context zzcey;

    zzxp(zzxq zzxq, Context context) {
        this.zzcex = zzxq;
        this.zzcey = context;
    }

    public final void run() {
        this.zzcex.getRewardedVideoAdInstance(this.zzcey);
    }
}
