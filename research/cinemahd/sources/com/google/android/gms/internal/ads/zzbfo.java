package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import com.google.android.gms.common.util.PlatformVersion;

final class zzbfo {
    private static Boolean zzeid;

    private zzbfo() {
    }

    @TargetApi(19)
    static void zza(WebView webView, String str) {
        if (!PlatformVersion.f() || !zzb(webView)) {
            String valueOf = String.valueOf(str);
            webView.loadUrl(valueOf.length() != 0 ? "javascript:".concat(valueOf) : new String("javascript:"));
            return;
        }
        webView.evaluateJavascript(str, (ValueCallback) null);
    }

    @TargetApi(19)
    private static boolean zzb(WebView webView) {
        boolean booleanValue;
        synchronized (zzbfo.class) {
            if (zzeid == null) {
                try {
                    webView.evaluateJavascript("(function(){})()", (ValueCallback) null);
                    zzeid = true;
                } catch (IllegalStateException unused) {
                    zzeid = false;
                }
            }
            booleanValue = zzeid.booleanValue();
        }
        return booleanValue;
    }
}
