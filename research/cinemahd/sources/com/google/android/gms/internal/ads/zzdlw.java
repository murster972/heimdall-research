package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

public final class zzdlw extends zzdrt<zzdlw, zza> implements zzdtg {
    private static volatile zzdtn<zzdlw> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlw zzhaz;
    private int zzhaf;

    public static final class zza extends zzdrt.zzb<zzdlw, zza> implements zzdtg {
        private zza() {
            super(zzdlw.zzhaz);
        }

        /* synthetic */ zza(zzdlx zzdlx) {
            this();
        }
    }

    static {
        zzdlw zzdlw = new zzdlw();
        zzhaz = zzdlw;
        zzdrt.zza(zzdlw.class, zzdlw);
    }

    private zzdlw() {
    }

    public static zzdlw zzaf(zzdqk zzdqk) throws zzdse {
        return (zzdlw) zzdrt.zza(zzhaz, zzdqk);
    }

    public final int getKeySize() {
        return this.zzhaf;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlx.zzdk[i - 1]) {
            case 1:
                return new zzdlw();
            case 2:
                return new zza((zzdlx) null);
            case 3:
                return zzdrt.zza((zzdte) zzhaz, "\u0000\u0001\u0000\u0000\u0002\u0002\u0001\u0000\u0000\u0000\u0002\u000b", new Object[]{"zzhaf"});
            case 4:
                return zzhaz;
            case 5:
                zzdtn<zzdlw> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlw.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhaz);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
