package com.google.android.gms.internal.ads;

final class zzqg implements Runnable {
    private final /* synthetic */ zzqh zzbpd;

    zzqg(zzqh zzqh) {
        this.zzbpd = zzqh;
    }

    public final void run() {
        synchronized (this.zzbpd.lock) {
            if (!this.zzbpd.foreground || !this.zzbpd.zzbpe) {
                zzayu.zzea("App is still foreground");
            } else {
                boolean unused = this.zzbpd.foreground = false;
                zzayu.zzea("App went background");
                for (zzqj zzp : this.zzbpd.zzbpf) {
                    try {
                        zzp.zzp(false);
                    } catch (Exception e) {
                        zzayu.zzc("", e);
                    }
                }
            }
        }
    }
}
