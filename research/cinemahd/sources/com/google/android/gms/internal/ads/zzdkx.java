package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.util.logging.Logger;

class zzdkx implements zzdis<zzdio> {
    private static final Logger logger = Logger.getLogger(zzdkx.class.getName());

    static class zza implements zzdio {
        private final zzdiq<zzdio> zzgzq;
        private final byte[] zzhae;

        private zza(zzdiq<zzdio> zzdiq) {
            this.zzhae = new byte[]{0};
            this.zzgzq = zzdiq;
        }

        public final byte[] zzl(byte[] bArr) throws GeneralSecurityException {
            if (this.zzgzq.zzasm().zzask().equals(zzdnw.LEGACY)) {
                return zzdoi.zza(this.zzgzq.zzasm().zzasl(), this.zzgzq.zzasm().zzasi().zzl(zzdoi.zza(bArr, this.zzhae)));
            }
            return zzdoi.zza(this.zzgzq.zzasm().zzasl(), this.zzgzq.zzasm().zzasi().zzl(bArr));
        }
    }

    zzdkx() {
    }

    public final /* synthetic */ Object zza(zzdiq zzdiq) throws GeneralSecurityException {
        return new zza(zzdiq);
    }

    public final Class<zzdio> zzarz() {
        return zzdio.class;
    }
}
