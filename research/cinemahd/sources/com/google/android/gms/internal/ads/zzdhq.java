package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class zzdhq<V> implements Runnable {
    private zzdho<V> zzgxm;

    zzdhq(zzdho<V> zzdho) {
        this.zzgxm = zzdho;
    }

    public final void run() {
        zzdhe zza;
        String str;
        zzdho<V> zzdho = this.zzgxm;
        if (zzdho != null && (zza = zzdho.zzgxk) != null) {
            this.zzgxm = null;
            if (zza.isDone()) {
                zzdho.setFuture(zza);
                return;
            }
            try {
                ScheduledFuture zzb = zzdho.zzgxl;
                ScheduledFuture unused = zzdho.zzgxl = null;
                str = "Timed out";
                if (zzb != null) {
                    long abs = Math.abs(zzb.getDelay(TimeUnit.MILLISECONDS));
                    if (abs > 10) {
                        StringBuilder sb = new StringBuilder(str.length() + 66);
                        sb.append(str);
                        sb.append(" (timeout delayed by ");
                        sb.append(abs);
                        sb.append(" ms after scheduled time)");
                        str = sb.toString();
                    }
                }
                String valueOf = String.valueOf(str);
                String valueOf2 = String.valueOf(zza);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 2 + String.valueOf(valueOf2).length());
                sb2.append(valueOf);
                sb2.append(": ");
                sb2.append(valueOf2);
                zzdho.setException(new zzdhp(sb2.toString()));
                zza.cancel(true);
            } catch (Throwable th) {
                zza.cancel(true);
                throw th;
            }
        }
    }
}
