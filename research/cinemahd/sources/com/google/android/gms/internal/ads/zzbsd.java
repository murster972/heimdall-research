package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

public final class zzbsd implements zzdxg<Set<zzbsu<zzps>>> {
    private final zzbrm zzfim;

    private zzbsd(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbsd zzu(zzbrm zzbrm) {
        return new zzbsd(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.emptySet(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
