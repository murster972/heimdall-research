package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinEventParameters;
import com.google.ads.mediation.AbstractAdViewAdapter;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class zzclk<AdT> implements zzcio<AdT> {
    private static Bundle zzm(Bundle bundle) {
        return bundle == null ? new Bundle() : new Bundle(bundle);
    }

    /* access modifiers changed from: protected */
    public abstract zzdhe<AdT> zza(zzczu zzczu, Bundle bundle);

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return !TextUtils.isEmpty(zzczl.zzglr.optString(AbstractAdViewAdapter.AD_UNIT_ID_PARAMETER, ""));
    }

    public final zzdhe<AdT> zzb(zzczt zzczt, zzczl zzczl) {
        zzczl zzczl2 = zzczl;
        String optString = zzczl2.zzglr.optString(AbstractAdViewAdapter.AD_UNIT_ID_PARAMETER, "");
        zzczu zzczu = zzczt.zzgmh.zzfgl;
        zzczw zzgk = new zzczw().zzg(zzczu.zzgml).zzd(zzczu.zzblm).zzc(zzczu.zzgmj).zzgk(zzczu.zzgmm).zzc(zzczu.zzgmk).zzb(zzczu.zzgmn).zzc(zzczu.zzgmo).zzb(zzczu.zzddz).zzb(zzczu.zzgmp).zzb(zzczu.zzgmq).zzgk(optString);
        Bundle zzm = zzm(zzczu.zzgml.zzccf);
        Bundle zzm2 = zzm(zzm.getBundle("com.google.ads.mediation.admob.AdMobAdapter"));
        zzm2.putInt("gw", 1);
        String optString2 = zzczl2.zzglr.optString("mad_hac", (String) null);
        if (optString2 != null) {
            zzm2.putString("mad_hac", optString2);
        }
        String optString3 = zzczl2.zzglr.optString("adJson", (String) null);
        if (optString3 != null) {
            zzm2.putString("_ad", optString3);
        }
        zzm2.putBoolean("_noRefresh", true);
        Iterator<String> keys = zzczl2.zzglu.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            String optString4 = zzczl2.zzglu.optString(next, (String) null);
            if (next != null) {
                zzm2.putString(next, optString4);
            }
        }
        zzm.putBundle("com.google.ads.mediation.admob.AdMobAdapter", zzm2);
        zzug zzug = zzczu.zzgml;
        zzug zzug2 = r5;
        zzug zzug3 = new zzug(zzug.versionCode, zzug.zzcby, zzm2, zzug.zzcbz, zzug.zzcca, zzug.zzccb, zzug.zzabo, zzug.zzbkh, zzug.zzccc, zzug.zzccd, zzug.zzmi, zzug.zzcce, zzm, zzug.zzccg, zzug.zzcch, zzug.zzcci, zzug.zzccj, zzug.zzcck, zzug.zzccm, zzug.zzabp, zzug.zzabq, zzug.zzccl);
        zzczu zzaos = zzgk.zzg(zzug2).zzaos();
        Bundle bundle = new Bundle();
        zzczt zzczt2 = zzczt;
        zzczn zzczn = zzczt2.zzgmi.zzgmf;
        Bundle bundle2 = new Bundle();
        bundle2.putStringArrayList("nofill_urls", new ArrayList(zzczn.zzdbt));
        bundle2.putInt("refresh_interval", zzczn.zzgmb);
        bundle2.putString("gws_query_id", zzczn.zzbzo);
        bundle.putBundle("parent_common_config", bundle2);
        String str = zzczt2.zzgmh.zzfgl.zzgmm;
        Bundle bundle3 = new Bundle();
        bundle3.putString("initial_ad_unit_id", str);
        zzczl zzczl3 = zzczl;
        bundle3.putString("allocation_id", zzczl3.zzdcm);
        bundle3.putStringArrayList(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_CLICK_URLS, new ArrayList(zzczl3.zzdbq));
        bundle3.putStringArrayList("imp_urls", new ArrayList(zzczl3.zzdbr));
        bundle3.putStringArrayList("manual_tracking_urls", new ArrayList(zzczl3.zzdkm));
        bundle3.putStringArrayList("fill_urls", new ArrayList(zzczl3.zzglm));
        bundle3.putStringArrayList("video_start_urls", new ArrayList(zzczl3.zzdkz));
        bundle3.putStringArrayList("video_reward_urls", new ArrayList(zzczl3.zzdla));
        bundle3.putStringArrayList("video_complete_urls", new ArrayList(zzczl3.zzgll));
        bundle3.putString(AppLovinEventParameters.CHECKOUT_TRANSACTION_IDENTIFIER, zzczl3.zzdcx);
        bundle3.putString("valid_from_timestamp", zzczl3.zzdcy);
        bundle3.putBoolean("is_closable_area_disabled", zzczl3.zzblf);
        if (zzczl3.zzdky != null) {
            Bundle bundle4 = new Bundle();
            bundle4.putInt("rb_amount", zzczl3.zzdky.zzdno);
            bundle4.putString("rb_type", zzczl3.zzdky.type);
            bundle3.putParcelableArray("rewards", new Bundle[]{bundle4});
        }
        bundle.putBundle("parent_ad_config", bundle3);
        return zza(zzaos, bundle);
    }
}
