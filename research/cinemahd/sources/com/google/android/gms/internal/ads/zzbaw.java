package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

public final class zzbaw {
    public final boolean zzdze;
    public final int zzdzf;
    public final int zzdzg;
    public final int zzdzh;
    private final String zzdzi;
    public final int zzdzj;
    public final int zzdzk;
    public final int zzdzl;
    public final int zzdzm;
    public final boolean zzdzn;
    public final int zzdzo;

    public zzbaw(String str) {
        JSONObject jSONObject = null;
        if (str != null) {
            try {
                jSONObject = new JSONObject(str);
            } catch (JSONException unused) {
            }
        }
        this.zzdze = zza(jSONObject, "aggressive_media_codec_release", zzzn.zzchh);
        this.zzdzf = zzb(jSONObject, "byte_buffer_precache_limit", zzzn.zzcgq);
        this.zzdzg = zzb(jSONObject, "exo_cache_buffer_size", zzzn.zzcgw);
        this.zzdzh = zzb(jSONObject, "exo_connect_timeout_millis", zzzn.zzcgm);
        this.zzdzi = zzc(jSONObject, "exo_player_version", zzzn.zzcgl);
        this.zzdzj = zzb(jSONObject, "exo_read_timeout_millis", zzzn.zzcgn);
        this.zzdzk = zzb(jSONObject, "load_check_interval_bytes", zzzn.zzcgo);
        this.zzdzl = zzb(jSONObject, "player_precache_limit", zzzn.zzcgp);
        this.zzdzm = zzb(jSONObject, "socket_receive_buffer_size", zzzn.zzcgr);
        this.zzdzn = zza(jSONObject, "use_cache_data_source", zzzn.zzcnc);
        this.zzdzo = zzb(jSONObject, "min_retry_count", zzzn.zzcgt);
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.google.android.gms.internal.ads.zzzc, com.google.android.gms.internal.ads.zzzc<java.lang.Boolean>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean zza(org.json.JSONObject r1, java.lang.String r2, com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r3) {
        /*
            com.google.android.gms.internal.ads.zzzj r0 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r3 = r0.zzd(r3)
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            boolean r1 = zza((org.json.JSONObject) r1, (java.lang.String) r2, (boolean) r3)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbaw.zza(org.json.JSONObject, java.lang.String, com.google.android.gms.internal.ads.zzzc):boolean");
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [com.google.android.gms.internal.ads.zzzc, com.google.android.gms.internal.ads.zzzc<java.lang.Integer>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int zzb(org.json.JSONObject r0, java.lang.String r1, com.google.android.gms.internal.ads.zzzc<java.lang.Integer> r2) {
        /*
            if (r0 == 0) goto L_0x0007
            int r0 = r0.getInt(r1)     // Catch:{ JSONException -> 0x0007 }
            return r0
        L_0x0007:
            com.google.android.gms.internal.ads.zzzj r0 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r0.zzd(r2)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbaw.zzb(org.json.JSONObject, java.lang.String, com.google.android.gms.internal.ads.zzzc):int");
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [com.google.android.gms.internal.ads.zzzc<java.lang.String>, com.google.android.gms.internal.ads.zzzc] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String zzc(org.json.JSONObject r0, java.lang.String r1, com.google.android.gms.internal.ads.zzzc<java.lang.String> r2) {
        /*
            if (r0 == 0) goto L_0x0007
            java.lang.String r0 = r0.getString(r1)     // Catch:{ JSONException -> 0x0007 }
            return r0
        L_0x0007:
            com.google.android.gms.internal.ads.zzzj r0 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r0.zzd(r2)
            java.lang.String r0 = (java.lang.String) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbaw.zzc(org.json.JSONObject, java.lang.String, com.google.android.gms.internal.ads.zzzc):java.lang.String");
    }

    private static boolean zza(JSONObject jSONObject, String str, boolean z) {
        if (jSONObject != null) {
            try {
                return jSONObject.getBoolean(str);
            } catch (JSONException unused) {
            }
        }
        return z;
    }
}
