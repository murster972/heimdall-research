package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbbh implements Runnable {
    private final String zzcyr;
    private final zzbbc zzebm;

    zzbbh(zzbbc zzbbc, String str) {
        this.zzebm = zzbbc;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzebm.zzff(this.zzcyr);
    }
}
