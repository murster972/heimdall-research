package com.google.android.gms.internal.ads;

import java.util.Map;

public interface zzafn<ContextT> {
    void zza(ContextT contextt, Map<String, String> map);
}
