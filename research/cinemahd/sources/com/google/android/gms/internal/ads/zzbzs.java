package com.google.android.gms.internal.ads;

import java.util.HashMap;
import okhttp3.internal.cache.DiskLruCache;

final /* synthetic */ class zzbzs implements zzps {
    private final zzbdi zzehp;

    zzbzs(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void zza(zzpt zzpt) {
        zzbdi zzbdi = this.zzehp;
        HashMap hashMap = new HashMap();
        hashMap.put("isVisible", zzpt.zzbnq ? DiskLruCache.VERSION_1 : "0");
        zzbdi.zza("onAdVisibilityChanged", hashMap);
    }
}
