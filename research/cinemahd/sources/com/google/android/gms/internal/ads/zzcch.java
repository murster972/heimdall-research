package com.google.android.gms.internal.ads;

public final class zzcch implements zzdxg<zzbsu<zzbph>> {
    private final zzdxp<zzccg> zzfdq;
    private final zzcci zzfrq;

    private zzcch(zzcci zzcci, zzdxp<zzccg> zzdxp) {
        this.zzfrq = zzcci;
        this.zzfdq = zzdxp;
    }

    public static zzcch zza(zzcci zzcci, zzdxp<zzccg> zzdxp) {
        return new zzcch(zzcci, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), zzazd.zzdwi), "Cannot return null from a non-@Nullable @Provides method");
    }
}
