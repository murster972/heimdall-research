package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.webkit.JsResult;

final class zzbdf implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsResult zzeea;

    zzbdf(JsResult jsResult) {
        this.zzeea = jsResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.zzeea.confirm();
    }
}
