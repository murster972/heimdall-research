package com.google.android.gms.internal.ads;

import android.os.RemoteException;

final class zzamn implements Runnable {
    private final /* synthetic */ zzamf zzdef;

    zzamn(zzamf zzamf) {
        this.zzdef = zzamf;
    }

    public final void run() {
        try {
            this.zzdef.zzdds.onAdLoaded();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
