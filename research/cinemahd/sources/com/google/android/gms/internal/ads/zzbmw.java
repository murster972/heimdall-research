package com.google.android.gms.internal.ads;

public final class zzbmw implements zzdxg<zzczl> {
    private final zzbmt zzfgf;

    private zzbmw(zzbmt zzbmt) {
        this.zzfgf = zzbmt;
    }

    public static zzbmw zzc(zzbmt zzbmt) {
        return new zzbmw(zzbmt);
    }

    public static zzczl zzd(zzbmt zzbmt) {
        return (zzczl) zzdxm.zza(zzbmt.zzagx(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzd(this.zzfgf);
    }
}
