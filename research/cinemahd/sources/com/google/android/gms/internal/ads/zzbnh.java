package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

public final class zzbnh implements zzdxg<zzavd> {
    private final zzdxp<zzavp> zzelx;
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<zzczu> zzfep;

    private zzbnh(zzdxp<Clock> zzdxp, zzdxp<zzavp> zzdxp2, zzdxp<zzczu> zzdxp3) {
        this.zzfcz = zzdxp;
        this.zzelx = zzdxp2;
        this.zzfep = zzdxp3;
    }

    public static zzbnh zzg(zzdxp<Clock> zzdxp, zzdxp<zzavp> zzdxp2, zzdxp<zzczu> zzdxp3) {
        return new zzbnh(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return (zzavd) zzdxm.zza(this.zzelx.get().zza(this.zzfcz.get(), this.zzfep.get().zzgmm), "Cannot return null from a non-@Nullable @Provides method");
    }
}
