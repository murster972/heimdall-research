package com.google.android.gms.internal.ads;

final class zzhu implements Runnable {
    private final /* synthetic */ zzhr zzahf;
    private final /* synthetic */ zzit zzahl;

    zzhu(zzhr zzhr, zzit zzit) {
        this.zzahf = zzhr;
        this.zzahl = zzit;
    }

    public final void run() {
        this.zzahl.zzge();
        this.zzahf.zzahg.zzb(this.zzahl);
    }
}
