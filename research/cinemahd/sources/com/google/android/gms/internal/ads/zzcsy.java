package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

public final class zzcsy implements zzdxg<zzcsz> {
    private final zzdxp<PackageInfo> zzfur;
    private final zzdxp<ApplicationInfo> zzfvc;

    private zzcsy(zzdxp<ApplicationInfo> zzdxp, zzdxp<PackageInfo> zzdxp2) {
        this.zzfvc = zzdxp;
        this.zzfur = zzdxp2;
    }

    public static zzcsy zzar(zzdxp<ApplicationInfo> zzdxp, zzdxp<PackageInfo> zzdxp2) {
        return new zzcsy(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcsz(this.zzfvc.get(), this.zzfur.get());
    }
}
