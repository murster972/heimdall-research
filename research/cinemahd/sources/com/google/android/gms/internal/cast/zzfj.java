package com.google.android.gms.internal.cast;

import android.os.Handler;
import android.os.Looper;

final class zzfj extends zzfe {
    private Handler handler;

    public zzfj(Looper looper) {
        this.handler = new Handler(looper);
    }

    public final void zza(zzfg zzfg) {
        this.handler.postDelayed(zzfg.zzfj(), 0);
    }
}
