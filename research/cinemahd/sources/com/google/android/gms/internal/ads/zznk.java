package com.google.android.gms.internal.ads;

public final class zznk {
    public final byte[] data;
    private final int offset = 0;

    public zznk(byte[] bArr, int i) {
        this.data = bArr;
    }

    public final int zzaz(int i) {
        return this.offset + i;
    }
}
