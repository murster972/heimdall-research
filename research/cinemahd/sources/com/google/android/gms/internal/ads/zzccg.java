package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzccg implements zzbph {
    private final zzbdi zzcza;

    zzccg(zzbdi zzbdi) {
        this.zzcza = !((Boolean) zzve.zzoy().zzd(zzzn.zzciv)).booleanValue() ? null : zzbdi;
    }

    public final void zzbv(Context context) {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi != null) {
            zzbdi.onPause();
        }
    }

    public final void zzbw(Context context) {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi != null) {
            zzbdi.onResume();
        }
    }

    public final void zzbx(Context context) {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi != null) {
            zzbdi.destroy();
        }
    }
}
