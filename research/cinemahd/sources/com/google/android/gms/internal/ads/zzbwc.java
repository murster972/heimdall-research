package com.google.android.gms.internal.ads;

public final class zzbwc implements zzdxg<zzbxa> {
    private final zzbvy zzfla;
    private final zzdxp<zzbvr> zzflc;

    public zzbwc(zzbvy zzbvy, zzdxp<zzbvr> zzdxp) {
        this.zzfla = zzbvy;
        this.zzflc = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbxa) zzdxm.zza(this.zzflc.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
