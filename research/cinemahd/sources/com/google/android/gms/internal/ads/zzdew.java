package com.google.android.gms.internal.ads;

final class zzdew<E> extends zzdep<E> {
    private final zzdeu<E> zzguj;

    zzdew(zzdeu<E> zzdeu, int i) {
        super(zzdeu.size(), i);
        this.zzguj = zzdeu;
    }

    /* access modifiers changed from: protected */
    public final E get(int i) {
        return this.zzguj.get(i);
    }
}
