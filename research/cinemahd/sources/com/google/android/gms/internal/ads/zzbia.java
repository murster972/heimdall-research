package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.gass.zzf;

public final class zzbia implements zzdxg<zzf> {
    private final zzdxp<Context> zzejv;

    public zzbia(zzdxp<Context> zzdxp) {
        this.zzejv = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzf) zzdxm.zza(new zzf(this.zzejv.get(), zzq.zzle().zzxb()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
