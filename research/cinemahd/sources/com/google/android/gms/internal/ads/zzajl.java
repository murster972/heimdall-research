package com.google.android.gms.internal.ads;

final class zzajl implements zzazn {
    private final /* synthetic */ zzajf zzdag;

    zzajl(zzajj zzajj, zzajf zzajf) {
        this.zzdag = zzajf;
    }

    public final void run() {
        zzavs.zzed("Rejecting reference for JS Engine.");
        this.zzdag.reject();
    }
}
