package com.google.android.gms.internal.ads;

import java.util.NoSuchElementException;

final class zzdfd extends zzdfp<T> {
    private boolean zzgur;
    private final /* synthetic */ Object zzgus;

    zzdfd(Object obj) {
        this.zzgus = obj;
    }

    public final boolean hasNext() {
        return !this.zzgur;
    }

    public final T next() {
        if (!this.zzgur) {
            this.zzgur = true;
            return this.zzgus;
        }
        throw new NoSuchElementException();
    }
}
