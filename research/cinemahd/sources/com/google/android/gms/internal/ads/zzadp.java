package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzadp extends IInterface {
    void zzb(zzade zzade) throws RemoteException;
}
