package com.google.android.gms.internal.ads;

final /* synthetic */ class zzahw implements Runnable {
    private final zzahr zzcyq;
    private final String zzcyr;

    zzahw(zzahr zzahr, String str) {
        this.zzcyq = zzahr;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzcyq.zzcz(this.zzcyr);
    }
}
