package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

final /* synthetic */ class zzbre implements zzbrn {
    private final zzsy.zza zzfhy;

    zzbre(zzsy.zza zza) {
        this.zzfhy = zza;
    }

    public final void zzp(Object obj) {
        ((zzbri) obj).zza(this.zzfhy);
    }
}
