package com.google.android.gms.internal.ads;

public final class zzna extends zzmt {
    private final Object data;
    private final int zzbcg;

    public zzna(zzms zzms, int i) {
        this(zzms, i, 0, (Object) null);
    }

    private zzna(zzms zzms, int i, int i2, Object obj) {
        super(zzms, i);
        this.zzbcg = 0;
        this.data = null;
    }
}
