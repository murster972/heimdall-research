package com.google.android.gms.internal.ads;

public interface zzbov {
    void onAdClosed();

    void onAdLeftApplication();

    void onAdOpened();

    void onRewardedVideoCompleted();

    void onRewardedVideoStarted();

    void zzb(zzare zzare, String str, String str2);
}
