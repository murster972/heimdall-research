package com.google.android.gms.internal.ads;

public abstract class zzbvm {
    public abstract zzbwt zza(zzbmt zzbmt, zzbxe zzbxe, zzbyg zzbyg);

    public abstract zzbwu zza(zzbmt zzbmt, zzbxe zzbxe, zzbvy zzbvy);

    public abstract zzbmz<zzbmj> zzadc();

    public abstract zzbou zzadd();

    public abstract zzdaf<zzcaj> zzade();
}
