package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;

public final class zzcvy implements zzdxg<ApplicationInfo> {
    private final zzcvw zzgih;

    public zzcvy(zzcvw zzcvw) {
        this.zzgih = zzcvw;
    }

    public static ApplicationInfo zzb(zzcvw zzcvw) {
        return (ApplicationInfo) zzdxm.zza(zzcvw.zzany(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzb(this.zzgih);
    }
}
