package com.google.android.gms.internal.ads;

final class zzbap implements Runnable {
    private final /* synthetic */ zzbai zzdyo;
    private final /* synthetic */ boolean zzdyr;

    zzbap(zzbai zzbai, boolean z) {
        this.zzdyo = zzbai;
        this.zzdyr = z;
    }

    public final void run() {
        this.zzdyo.zzd("windowVisibilityChanged", "isVisible", String.valueOf(this.zzdyr));
    }
}
