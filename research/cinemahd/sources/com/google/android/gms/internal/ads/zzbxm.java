package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbxm implements Runnable {
    private final zzbxj zzfnp;
    private final zzbxz zzfnr;

    zzbxm(zzbxj zzbxj, zzbxz zzbxz) {
        this.zzfnp = zzbxj;
        this.zzfnr = zzbxz;
    }

    public final void run() {
        this.zzfnp.zzd(this.zzfnr);
    }
}
