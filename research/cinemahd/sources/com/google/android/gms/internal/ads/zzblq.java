package com.google.android.gms.internal.ads;

final /* synthetic */ class zzblq implements Runnable {
    private final Runnable zzfau;
    private final zzblo zzfez;

    zzblq(zzblo zzblo, Runnable runnable) {
        this.zzfez = zzblo;
        this.zzfau = runnable;
    }

    public final void run() {
        this.zzfez.zze(this.zzfau);
    }
}
