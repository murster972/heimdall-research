package com.google.android.gms.internal.cast;

import android.annotation.TargetApi;
import android.view.Choreographer;

public abstract class zzfg {
    private Runnable zzabp;
    private Choreographer.FrameCallback zzabq;

    public abstract void doFrame(long j);

    /* access modifiers changed from: package-private */
    @TargetApi(16)
    public final Choreographer.FrameCallback zzfi() {
        if (this.zzabq == null) {
            this.zzabq = new zzfh(this);
        }
        return this.zzabq;
    }

    /* access modifiers changed from: package-private */
    public final Runnable zzfj() {
        if (this.zzabp == null) {
            this.zzabp = new zzfi(this);
        }
        return this.zzabp;
    }
}
