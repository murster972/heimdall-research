package com.google.android.gms.internal.ads;

import android.view.View;

final /* synthetic */ class zzcbw implements View.OnClickListener {
    private final zzcbp zzfrj;

    zzcbw(zzcbp zzcbp) {
        this.zzfrj = zzcbp;
    }

    public final void onClick(View view) {
        this.zzfrj.zzad(view);
    }
}
