package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.internal.ads.zzbo;
import com.google.android.gms.internal.ads.zzbs;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public final class zzdr extends zzdi {
    private zzdr(Context context, String str, boolean z) {
        super(context, str, z);
    }

    public static zzdr zza(String str, Context context, boolean z) {
        zzdi.zza(context, z);
        return new zzdr(context, str, z);
    }

    /* access modifiers changed from: protected */
    public final List<Callable<Void>> zza(zzei zzei, Context context, zzbs.zza.zzb zzb, zzbo.zza zza) {
        if (zzei.zzbx() == null || !this.zzvh) {
            return super.zza(zzei, context, zzb, zza);
        }
        int zzbr = zzei.zzbr();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(super.zza(zzei, context, zzb, zza));
        arrayList.add(new zzfe(zzei, "/Mc2CnEeRQtKVC/fPg/SyzEdyC3gZRl7Pq4ep+r3/uKWbAMRLI7OunrDNLnYZuDh", "6mFBYTN64dqZuFHXRYjKBuCFVskXKkuG5eXtMJOzijI=", zzb, zzbr, 24));
        return arrayList;
    }
}
