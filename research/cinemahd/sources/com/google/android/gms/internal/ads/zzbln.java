package com.google.android.gms.internal.ads;

import java.util.concurrent.atomic.AtomicReference;

final /* synthetic */ class zzbln implements Runnable {
    private final AtomicReference zzfew;

    zzbln(AtomicReference atomicReference) {
        this.zzfew = atomicReference;
    }

    public final void run() {
        Runnable runnable = (Runnable) this.zzfew.getAndSet((Object) null);
        if (runnable != null) {
            runnable.run();
        }
    }
}
