package com.google.android.gms.internal.ads;

final /* synthetic */ class zzddd implements Runnable {
    private final String zzcyr;
    private final zzdda zzgrp;

    zzddd(zzdda zzdda, String str) {
        this.zzgrp = zzdda;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzgrp.zzgp(this.zzcyr);
    }
}
