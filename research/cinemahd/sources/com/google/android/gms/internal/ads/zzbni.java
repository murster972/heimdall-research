package com.google.android.gms.internal.ads;

public final class zzbni implements zzdxg<zzbsu<zzbqx>> {
    public static zzbsu<zzbqx> zza(zzbnb zzbnb, zzbnk zzbnk) {
        return (zzbsu) zzdxm.zza(new zzbsu(zzbnk, zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
