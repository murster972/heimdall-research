package com.google.android.gms.internal.ads;

import androidx.collection.SimpleArrayMap;

public final class zzbxb {
    zzadj zzfmq;
    zzadi zzfmr;
    zzadv zzfms;
    zzadu zzfmt;
    zzahh zzfmu;
    final SimpleArrayMap<String, zzadp> zzfmv = new SimpleArrayMap<>();
    final SimpleArrayMap<String, zzado> zzfmw = new SimpleArrayMap<>();

    public final zzbxb zza(zzadu zzadu) {
        this.zzfmt = zzadu;
        return this;
    }

    public final zzbwz zzajw() {
        return new zzbwz(this);
    }

    public final zzbxb zzb(zzadj zzadj) {
        this.zzfmq = zzadj;
        return this;
    }

    public final zzbxb zzb(zzadi zzadi) {
        this.zzfmr = zzadi;
        return this;
    }

    public final zzbxb zzb(zzadv zzadv) {
        this.zzfms = zzadv;
        return this;
    }

    public final zzbxb zzb(zzahh zzahh) {
        this.zzfmu = zzahh;
        return this;
    }

    public final zzbxb zzb(String str, zzadp zzadp, zzado zzado) {
        this.zzfmv.put(str, zzadp);
        this.zzfmw.put(str, zzado);
        return this;
    }
}
