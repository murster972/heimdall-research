package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.Objects;
import com.unity3d.ads.metadata.MediationMetaData;

public final class zzaxi {
    public final int count;
    public final String name;
    private final double zzdtx;
    private final double zzdty;
    public final double zzdtz;

    public zzaxi(String str, double d, double d2, double d3, int i) {
        this.name = str;
        this.zzdty = d;
        this.zzdtx = d2;
        this.zzdtz = d3;
        this.count = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzaxi)) {
            return false;
        }
        zzaxi zzaxi = (zzaxi) obj;
        if (Objects.a(this.name, zzaxi.name) && this.zzdtx == zzaxi.zzdtx && this.zzdty == zzaxi.zzdty && this.count == zzaxi.count && Double.compare(this.zzdtz, zzaxi.zzdtz) == 0) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return Objects.a(this.name, Double.valueOf(this.zzdtx), Double.valueOf(this.zzdty), Double.valueOf(this.zzdtz), Integer.valueOf(this.count));
    }

    public final String toString() {
        Objects.ToStringHelper a2 = Objects.a((Object) this);
        a2.a(MediationMetaData.KEY_NAME, this.name);
        a2.a("minBound", Double.valueOf(this.zzdty));
        a2.a("maxBound", Double.valueOf(this.zzdtx));
        a2.a("percent", Double.valueOf(this.zzdtz));
        a2.a("count", Integer.valueOf(this.count));
        return a2.toString();
    }
}
