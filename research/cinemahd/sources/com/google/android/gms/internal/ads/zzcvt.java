package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import java.util.concurrent.ScheduledExecutorService;

public final class zzcvt implements zzdxg<zzcvr> {
    private final zzdxp<zzakp> zzeqj;
    private final zzdxp<ScheduledExecutorService> zzfpr;
    private final zzdxp<ApplicationInfo> zzfvc;
    private final zzdxp<Boolean> zzgif;

    public zzcvt(zzdxp<zzakp> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<Boolean> zzdxp3, zzdxp<ApplicationInfo> zzdxp4) {
        this.zzeqj = zzdxp;
        this.zzfpr = zzdxp2;
        this.zzgif = zzdxp3;
        this.zzfvc = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcvr(this.zzeqj.get(), this.zzfpr.get(), this.zzgif.get().booleanValue(), this.zzfvc.get());
    }
}
