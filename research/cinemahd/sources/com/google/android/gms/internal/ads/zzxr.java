package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.initialization.InitializationStatus;
import java.util.HashMap;
import java.util.Map;

final /* synthetic */ class zzxr implements InitializationStatus {
    private final zzxq zzcex;

    zzxr(zzxq zzxq) {
        this.zzcex = zzxq;
    }

    public final Map getAdapterStatusMap() {
        zzxq zzxq = this.zzcex;
        HashMap hashMap = new HashMap();
        hashMap.put("com.google.android.gms.ads.MobileAds", new zzxu(zzxq));
        return hashMap;
    }
}
