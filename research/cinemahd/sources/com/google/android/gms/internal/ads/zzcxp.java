package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

public final class zzcxp {
    public static <T> void zza(AtomicReference<T> atomicReference, zzcxo<T> zzcxo) {
        T t = atomicReference.get();
        if (t != null) {
            try {
                zzcxo.zzt(t);
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }
}
