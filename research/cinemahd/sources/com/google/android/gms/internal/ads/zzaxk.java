package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.Map;

public final class zzaxk {
    private static zzu zzdua;
    private static final Object zzdub = new Object();
    @Deprecated
    private static final zzaxo<Void> zzduc = new zzaxn();

    public zzaxk(Context context) {
        zzbh(context.getApplicationContext() != null ? context.getApplicationContext() : context);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.gms.internal.ads.zzu zzbh(android.content.Context r3) {
        /*
            java.lang.Object r0 = zzdub
            monitor-enter(r0)
            com.google.android.gms.internal.ads.zzu r1 = zzdua     // Catch:{ all -> 0x0036 }
            if (r1 != 0) goto L_0x0032
            com.google.android.gms.internal.ads.zzzn.initialize(r3)     // Catch:{ all -> 0x0036 }
            boolean r1 = com.google.android.gms.common.util.ClientLibraryUtils.a()     // Catch:{ all -> 0x0036 }
            if (r1 != 0) goto L_0x0024
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r1 = com.google.android.gms.internal.ads.zzzn.zzcng     // Catch:{ all -> 0x0036 }
            com.google.android.gms.internal.ads.zzzj r2 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x0036 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ all -> 0x0036 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ all -> 0x0036 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x0036 }
            if (r1 == 0) goto L_0x0024
            r1 = 1
            goto L_0x0025
        L_0x0024:
            r1 = 0
        L_0x0025:
            if (r1 == 0) goto L_0x002c
            com.google.android.gms.internal.ads.zzu r3 = com.google.android.gms.internal.ads.zzaxd.zzbg(r3)     // Catch:{ all -> 0x0036 }
            goto L_0x0030
        L_0x002c:
            com.google.android.gms.internal.ads.zzu r3 = com.google.android.gms.internal.ads.zzba.zza(r3)     // Catch:{ all -> 0x0036 }
        L_0x0030:
            zzdua = r3     // Catch:{ all -> 0x0036 }
        L_0x0032:
            com.google.android.gms.internal.ads.zzu r3 = zzdua     // Catch:{ all -> 0x0036 }
            monitor-exit(r0)     // Catch:{ all -> 0x0036 }
            return r3
        L_0x0036:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0036 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzaxk.zzbh(android.content.Context):com.google.android.gms.internal.ads.zzu");
    }

    public static zzdhe<zzo> zzeq(String str) {
        zzazl zzazl = new zzazl();
        zzdua.zze(new zzaxq(str, zzazl));
        return zzazl;
    }

    public final zzdhe<String> zza(int i, String str, Map<String, String> map, byte[] bArr) {
        String str2 = str;
        zzaxr zzaxr = new zzaxr((zzaxn) null);
        zzaxm zzaxm = new zzaxm(this, str2, zzaxr);
        zzayo zzayo = new zzayo((String) null);
        zzaxp zzaxp = new zzaxp(this, i, str, zzaxr, zzaxm, bArr, map, zzayo);
        if (zzayo.isEnabled()) {
            try {
                zzayo.zza(str2, "GET", zzaxp.getHeaders(), zzaxp.zzf());
            } catch (zzb e) {
                zzayu.zzez(e.getMessage());
            }
        }
        zzdua.zze(zzaxp);
        return zzaxr;
    }

    public final zzdhe<String> zzc(String str, Map<String, String> map) {
        return zza(0, str, map, (byte[]) null);
    }
}
