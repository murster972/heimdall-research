package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public final class zzbxr {
    private final zzbxa zzflg;
    private final zzcaj zzfnx;
    private final zzcbn zzfod;
    private final zzbjq zzfoe;
    private final Context zzup;

    public zzbxr(Context context, zzcbn zzcbn, zzcaj zzcaj, zzbjq zzbjq, zzbxa zzbxa) {
        this.zzup = context;
        this.zzfod = zzcbn;
        this.zzfnx = zzcaj;
        this.zzfoe = zzbjq;
        this.zzflg = zzbxa;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzbdi zzbdi, Map map) {
        zzayu.zzey("Hiding native ads overlay.");
        zzbdi.getView().setVisibility(8);
        this.zzfoe.zzbf(false);
    }

    public final View zzakj() throws zzbdv {
        zzbdi zza = this.zzfod.zza(zzuj.zzg(this.zzup), false);
        zza.getView().setVisibility(8);
        zza.zza("/sendMessageToSdk", (zzafn<? super zzbdi>) new zzbxu(this));
        zza.zza("/adMuted", (zzafn<? super zzbdi>) new zzbxt(this));
        this.zzfnx.zza(new WeakReference(zza), "/loadHtml", new zzbxw(this));
        this.zzfnx.zza(new WeakReference(zza), "/showOverlay", new zzbxv(this));
        this.zzfnx.zza(new WeakReference(zza), "/hideOverlay", new zzbxy(this));
        return zza.getView();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(zzbdi zzbdi, Map map) {
        zzayu.zzey("Showing native ads overlay.");
        zzbdi.getView().setVisibility(0);
        this.zzfoe.zzbf(true);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(zzbdi zzbdi, Map map) {
        this.zzflg.zzaip();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(zzbdi zzbdi, Map map) {
        this.zzfnx.zza("sendMessageToNativeJs", (Map<String, ?>) map);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Map map, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("messageType", "htmlLoaded");
        hashMap.put("id", (String) map.get("id"));
        this.zzfnx.zza("sendMessageToNativeJs", (Map<String, ?>) hashMap);
    }
}
