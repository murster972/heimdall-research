package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.applovin.sdk.AppLovinEventTypes;
import com.google.android.gms.dynamic.ObjectWrapper;

final class zzuw extends zzvb<zzvu> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ String zzcdg;
    private final /* synthetic */ zzup zzcdi;
    private final /* synthetic */ zzuj zzcdj;

    zzuw(zzup zzup, Context context, zzuj zzuj, String str) {
        this.zzcdi = zzup;
        this.val$context = context;
        this.zzcdj = zzuj;
        this.zzcdg = str;
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.a(this.val$context), this.zzcdj, this.zzcdg, 19649000);
    }

    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, AppLovinEventTypes.USER_EXECUTED_SEARCH);
        return new zzyd();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzccx.zza(this.val$context, this.zzcdj, this.zzcdg, (zzalc) null, 3);
    }
}
