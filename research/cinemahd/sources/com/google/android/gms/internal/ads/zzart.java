package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardItem;

public final class zzart implements RewardItem {
    private final zzare zzdns;

    public zzart(zzare zzare) {
        this.zzdns = zzare;
    }

    public final int getAmount() {
        zzare zzare = this.zzdns;
        if (zzare == null) {
            return 0;
        }
        try {
            return zzare.getAmount();
        } catch (RemoteException e) {
            zzayu.zzd("Could not forward getAmount to RewardItem", e);
            return 0;
        }
    }

    public final String getType() {
        zzare zzare = this.zzdns;
        if (zzare == null) {
            return null;
        }
        try {
            return zzare.getType();
        } catch (RemoteException e) {
            zzayu.zzd("Could not forward getType to RewardItem", e);
            return null;
        }
    }
}
