package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.TimeUnit;

public final class zzbay {
    private final zzazb zzdij;
    private final String zzdiy;
    private final zzaae zzdxw;
    private boolean zzdya;
    private final zzaac zzeao;
    private final zzaxg zzeap = new zzaxl().zza("min_1", Double.MIN_VALUE, 1.0d).zza("1_5", 1.0d, 5.0d).zza("5_10", 5.0d, 10.0d).zza("10_20", 10.0d, 20.0d).zza("20_30", 20.0d, 30.0d).zza("30_max", 30.0d, Double.MAX_VALUE).zzxa();
    private final long[] zzeaq;
    private final String[] zzear;
    private boolean zzeas = false;
    private boolean zzeat = false;
    private boolean zzeau = false;
    private boolean zzeav = false;
    private zzbag zzeaw;
    private boolean zzeax;
    private boolean zzeay;
    private long zzeaz = -1;
    private final Context zzup;

    public zzbay(Context context, zzazb zzazb, String str, zzaae zzaae, zzaac zzaac) {
        this.zzup = context;
        this.zzdij = zzazb;
        this.zzdiy = str;
        this.zzdxw = zzaae;
        this.zzeao = zzaac;
        String str2 = (String) zzve.zzoy().zzd(zzzn.zzcha);
        if (str2 == null) {
            this.zzear = new String[0];
            this.zzeaq = new long[0];
            return;
        }
        String[] split = TextUtils.split(str2, ",");
        this.zzear = new String[split.length];
        this.zzeaq = new long[split.length];
        for (int i = 0; i < split.length; i++) {
            try {
                this.zzeaq[i] = Long.parseLong(split[i]);
            } catch (NumberFormatException e) {
                zzayu.zzd("Unable to parse frame hash target time number.", e);
                this.zzeaq[i] = -1;
            }
        }
    }

    public final void onStop() {
        if (zzabm.zzcux.get().booleanValue() && !this.zzeax) {
            Bundle bundle = new Bundle();
            bundle.putString("type", "native-player-metrics");
            bundle.putString("request", this.zzdiy);
            bundle.putString("player", this.zzeaw.zzxo());
            for (zzaxi next : this.zzeap.zzwz()) {
                String valueOf = String.valueOf(next.name);
                bundle.putString(valueOf.length() != 0 ? "fps_c_".concat(valueOf) : new String("fps_c_"), Integer.toString(next.count));
                String valueOf2 = String.valueOf(next.name);
                bundle.putString(valueOf2.length() != 0 ? "fps_p_".concat(valueOf2) : new String("fps_p_"), Double.toString(next.zzdtz));
            }
            int i = 0;
            while (true) {
                long[] jArr = this.zzeaq;
                if (i < jArr.length) {
                    String str = this.zzear[i];
                    if (str != null) {
                        String valueOf3 = String.valueOf(Long.valueOf(jArr[i]));
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf3).length() + 3);
                        sb.append("fh_");
                        sb.append(valueOf3);
                        bundle.putString(sb.toString(), str);
                    }
                    i++;
                } else {
                    zzq.zzkq().zza(this.zzup, this.zzdij.zzbma, "gmob-apps", bundle, true);
                    this.zzeax = true;
                    return;
                }
            }
        }
    }

    public final void zzb(zzbag zzbag) {
        zzzv.zza(this.zzdxw, this.zzeao, "vpc2");
        this.zzeas = true;
        zzaae zzaae = this.zzdxw;
        if (zzaae != null) {
            zzaae.zzh("vpn", zzbag.zzxo());
        }
        this.zzeaw = zzbag;
    }

    public final void zzc(zzbag zzbag) {
        if (this.zzeau && !this.zzeav) {
            if (zzavs.zzvs() && !this.zzeav) {
                zzavs.zzed("VideoMetricsMixin first frame");
            }
            zzzv.zza(this.zzdxw, this.zzeao, "vff2");
            this.zzeav = true;
        }
        long nanoTime = zzq.zzkx().nanoTime();
        if (this.zzdya && this.zzeay && this.zzeaz != -1) {
            this.zzeap.zza(((double) TimeUnit.SECONDS.toNanos(1)) / ((double) (nanoTime - this.zzeaz)));
        }
        this.zzeay = this.zzdya;
        this.zzeaz = nanoTime;
        long longValue = ((Long) zzve.zzoy().zzd(zzzn.zzchb)).longValue();
        long currentPosition = (long) zzbag.getCurrentPosition();
        int i = 0;
        while (true) {
            String[] strArr = this.zzear;
            if (i >= strArr.length) {
                return;
            }
            if (strArr[i] != null || longValue <= Math.abs(currentPosition - this.zzeaq[i])) {
                zzbag zzbag2 = zzbag;
                i++;
            } else {
                String[] strArr2 = this.zzear;
                int i2 = 8;
                Bitmap bitmap = zzbag.getBitmap(8, 8);
                long j = 63;
                int i3 = 0;
                long j2 = 0;
                while (i3 < i2) {
                    long j3 = j;
                    long j4 = j2;
                    int i4 = 0;
                    while (i4 < i2) {
                        int pixel = bitmap.getPixel(i4, i3);
                        j4 |= ((Color.blue(pixel) + Color.red(pixel)) + Color.green(pixel) > 128 ? 1 : 0) << ((int) j3);
                        i4++;
                        j3--;
                        i2 = 8;
                    }
                    i3++;
                    j2 = j4;
                    i2 = 8;
                    j = j3;
                }
                strArr2[i] = String.format("%016X", new Object[]{Long.valueOf(j2)});
                return;
            }
        }
    }

    public final void zzer() {
        if (this.zzeas && !this.zzeat) {
            zzzv.zza(this.zzdxw, this.zzeao, "vfr2");
            this.zzeat = true;
        }
    }

    public final void zzyi() {
        this.zzdya = true;
        if (this.zzeat && !this.zzeau) {
            zzzv.zza(this.zzdxw, this.zzeao, "vfp2");
            this.zzeau = true;
        }
    }

    public final void zzyj() {
        this.zzdya = false;
    }
}
