package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzbux implements zzdxg<zzbuy> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<Integer> zzfdc;
    private final zzdxp<zzbdi> zzfef;
    private final zzdxp<zzczl> zzffb;

    private zzbux(zzdxp<Context> zzdxp, zzdxp<zzbdi> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzazb> zzdxp4, zzdxp<Integer> zzdxp5) {
        this.zzejv = zzdxp;
        this.zzfef = zzdxp2;
        this.zzffb = zzdxp3;
        this.zzfav = zzdxp4;
        this.zzfdc = zzdxp5;
    }

    public static zzbux zzc(zzdxp<Context> zzdxp, zzdxp<zzbdi> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzazb> zzdxp4, zzdxp<Integer> zzdxp5) {
        return new zzbux(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5);
    }

    public final /* synthetic */ Object get() {
        return new zzbuy(this.zzejv.get(), this.zzfef.get(), this.zzffb.get(), this.zzfav.get(), this.zzfdc.get().intValue());
    }
}
