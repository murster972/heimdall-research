package com.google.android.gms.internal.ads;

import com.facebook.common.time.Clock;
import java.io.IOException;
import java.util.ArrayList;
import java.util.IdentityHashMap;

final class zzmd implements zzlz, zzmc {
    private zzmr zzadg;
    private zzmc zzbae;
    public final zzlz[] zzbbg;
    private final IdentityHashMap<zzmo, Integer> zzbbh = new IdentityHashMap<>();
    private int zzbbi;
    private zzlz[] zzbbj;
    private zzmn zzbbk;

    public zzmd(zzlz... zzlzArr) {
        this.zzbbg = zzlzArr;
    }

    public final void zza(zzmc zzmc, long j) {
        this.zzbae = zzmc;
        zzlz[] zzlzArr = this.zzbbg;
        this.zzbbi = zzlzArr.length;
        for (zzlz zza : zzlzArr) {
            zza.zza(this, j);
        }
    }

    public final void zzee(long j) {
        for (zzlz zzee : this.zzbbj) {
            zzee.zzee(j);
        }
    }

    public final boolean zzef(long j) {
        return this.zzbbk.zzef(j);
    }

    public final long zzeg(long j) {
        long zzeg = this.zzbbj[0].zzeg(j);
        int i = 1;
        while (true) {
            zzlz[] zzlzArr = this.zzbbj;
            if (i >= zzlzArr.length) {
                return zzeg;
            }
            if (zzlzArr[i].zzeg(zzeg) == zzeg) {
                i++;
            } else {
                throw new IllegalStateException("Children seeked to different positions");
            }
        }
    }

    public final void zzhf() throws IOException {
        for (zzlz zzhf : this.zzbbg) {
            zzhf.zzhf();
        }
    }

    public final zzmr zzhg() {
        return this.zzadg;
    }

    public final long zzhh() {
        return this.zzbbk.zzhh();
    }

    public final long zzhi() {
        long zzhi = this.zzbbg[0].zzhi();
        int i = 1;
        while (true) {
            zzlz[] zzlzArr = this.zzbbg;
            if (i >= zzlzArr.length) {
                if (zzhi != -9223372036854775807L) {
                    zzlz[] zzlzArr2 = this.zzbbj;
                    int length = zzlzArr2.length;
                    int i2 = 0;
                    while (i2 < length) {
                        zzlz zzlz = zzlzArr2[i2];
                        if (zzlz == this.zzbbg[0] || zzlz.zzeg(zzhi) == zzhi) {
                            i2++;
                        } else {
                            throw new IllegalStateException("Children seeked to different positions");
                        }
                    }
                }
                return zzhi;
            } else if (zzlzArr[i].zzhi() == -9223372036854775807L) {
                i++;
            } else {
                throw new IllegalStateException("Child reported discontinuity");
            }
        }
    }

    public final long zzhj() {
        long j = Long.MAX_VALUE;
        for (zzlz zzhj : this.zzbbj) {
            long zzhj2 = zzhj.zzhj();
            if (zzhj2 != Long.MIN_VALUE) {
                j = Math.min(j, zzhj2);
            }
        }
        if (j == Clock.MAX_TIME) {
            return Long.MIN_VALUE;
        }
        return j;
    }

    public final long zza(zzne[] zzneArr, boolean[] zArr, zzmo[] zzmoArr, boolean[] zArr2, long j) {
        int i;
        zzne[] zzneArr2 = zzneArr;
        zzmo[] zzmoArr2 = zzmoArr;
        int[] iArr = new int[zzneArr2.length];
        int[] iArr2 = new int[zzneArr2.length];
        for (int i2 = 0; i2 < zzneArr2.length; i2++) {
            if (zzmoArr2[i2] == null) {
                i = -1;
            } else {
                i = this.zzbbh.get(zzmoArr2[i2]).intValue();
            }
            iArr[i2] = i;
            iArr2[i2] = -1;
            if (zzneArr2[i2] != null) {
                zzms zzid = zzneArr2[i2].zzid();
                int i3 = 0;
                while (true) {
                    zzlz[] zzlzArr = this.zzbbg;
                    if (i3 >= zzlzArr.length) {
                        break;
                    } else if (zzlzArr[i3].zzhg().zza(zzid) != -1) {
                        iArr2[i2] = i3;
                        break;
                    } else {
                        i3++;
                    }
                }
            }
        }
        this.zzbbh.clear();
        zzmo[] zzmoArr3 = new zzmo[zzneArr2.length];
        zzmo[] zzmoArr4 = new zzmo[zzneArr2.length];
        zzne[] zzneArr3 = new zzne[zzneArr2.length];
        ArrayList arrayList = new ArrayList(this.zzbbg.length);
        long j2 = j;
        int i4 = 0;
        while (i4 < this.zzbbg.length) {
            for (int i5 = 0; i5 < zzneArr2.length; i5++) {
                zzne zzne = null;
                zzmoArr4[i5] = iArr[i5] == i4 ? zzmoArr2[i5] : null;
                if (iArr2[i5] == i4) {
                    zzne = zzneArr2[i5];
                }
                zzneArr3[i5] = zzne;
            }
            ArrayList arrayList2 = arrayList;
            zzne[] zzneArr4 = zzneArr3;
            int i6 = i4;
            long zza = this.zzbbg[i4].zza(zzneArr3, zArr, zzmoArr4, zArr2, j2);
            if (i6 == 0) {
                j2 = zza;
            } else if (zza != j2) {
                throw new IllegalStateException("Children enabled at different positions");
            }
            boolean z = false;
            for (int i7 = 0; i7 < zzneArr2.length; i7++) {
                boolean z2 = true;
                if (iArr2[i7] == i6) {
                    zzoc.checkState(zzmoArr4[i7] != null);
                    zzmoArr3[i7] = zzmoArr4[i7];
                    this.zzbbh.put(zzmoArr4[i7], Integer.valueOf(i6));
                    z = true;
                } else if (iArr[i7] == i6) {
                    if (zzmoArr4[i7] != null) {
                        z2 = false;
                    }
                    zzoc.checkState(z2);
                }
            }
            if (z) {
                arrayList2.add(this.zzbbg[i6]);
            }
            i4 = i6 + 1;
            arrayList = arrayList2;
            zzneArr3 = zzneArr4;
        }
        ArrayList arrayList3 = arrayList;
        System.arraycopy(zzmoArr3, 0, zzmoArr2, 0, zzmoArr3.length);
        this.zzbbj = new zzlz[arrayList3.size()];
        arrayList3.toArray(this.zzbbj);
        this.zzbbk = new zzlq(this.zzbbj);
        return j2;
    }

    public final void zza(zzlz zzlz) {
        int i = this.zzbbi - 1;
        this.zzbbi = i;
        if (i <= 0) {
            int i2 = 0;
            for (zzlz zzhg : this.zzbbg) {
                i2 += zzhg.zzhg().length;
            }
            zzms[] zzmsArr = new zzms[i2];
            zzlz[] zzlzArr = this.zzbbg;
            int length = zzlzArr.length;
            int i3 = 0;
            int i4 = 0;
            while (i3 < length) {
                zzmr zzhg2 = zzlzArr[i3].zzhg();
                int i5 = zzhg2.length;
                int i6 = i4;
                int i7 = 0;
                while (i7 < i5) {
                    zzmsArr[i6] = zzhg2.zzav(i7);
                    i7++;
                    i6++;
                }
                i3++;
                i4 = i6;
            }
            this.zzadg = new zzmr(zzmsArr);
            this.zzbae.zza(this);
        }
    }

    public final /* synthetic */ void zza(zzmn zzmn) {
        if (this.zzadg != null) {
            this.zzbae.zza(this);
        }
    }
}
