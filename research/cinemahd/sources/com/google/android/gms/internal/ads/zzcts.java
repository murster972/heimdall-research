package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public final class zzcts implements zzcub<zzctt> {
    private final zzazb zzbll;
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcts(zzdhd zzdhd, Context context, zzazb zzazb) {
        this.zzfov = zzdhd;
        this.zzup = context;
        this.zzbll = zzazb;
    }

    public final zzdhe<zzctt> zzanc() {
        return this.zzfov.zzd(new zzctv(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzctt zzanp() throws Exception {
        boolean a2 = Wrappers.a(this.zzup).a();
        zzq.zzkq();
        boolean zzay = zzawb.zzay(this.zzup);
        String str = this.zzbll.zzbma;
        zzq.zzks();
        boolean zzwq = zzawh.zzwq();
        zzq.zzkq();
        return new zzctt(a2, zzay, str, zzwq, zzawb.zzav(this.zzup), DynamiteModule.b(this.zzup, ModuleDescriptor.MODULE_ID), DynamiteModule.a(this.zzup, ModuleDescriptor.MODULE_ID));
    }
}
