package com.google.android.gms.internal.ads;

import android.content.Context;

public final class zzcrz implements zzdxg<zzcrx> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdhd> zzfcv;

    private zzcrz(zzdxp<Context> zzdxp, zzdxp<zzdhd> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcrz zzap(zzdxp<Context> zzdxp, zzdxp<zzdhd> zzdxp2) {
        return new zzcrz(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcrx(this.zzejv.get(), this.zzfcv.get());
    }
}
