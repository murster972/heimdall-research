package com.google.android.gms.internal.ads;

public final class zzbkg extends zzre {
    private final zzvu zzbqy;
    private final zzbke zzfdv;

    public zzbkg(zzbke zzbke, zzvu zzvu) {
        this.zzfdv = zzbke;
        this.zzbqy = zzvu;
    }

    public final void zza(zzrl zzrl) {
        this.zzfdv.zza(zzrl);
    }

    public final zzvu zzdm() {
        return this.zzbqy;
    }
}
