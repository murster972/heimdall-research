package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzvu extends IInterface {
    void destroy() throws RemoteException;

    Bundle getAdMetadata() throws RemoteException;

    String getAdUnitId() throws RemoteException;

    String getMediationAdapterClassName() throws RemoteException;

    zzxb getVideoController() throws RemoteException;

    boolean isLoading() throws RemoteException;

    boolean isReady() throws RemoteException;

    void pause() throws RemoteException;

    void resume() throws RemoteException;

    void setImmersiveMode(boolean z) throws RemoteException;

    void setManualImpressionsEnabled(boolean z) throws RemoteException;

    void setUserId(String str) throws RemoteException;

    void showInterstitial() throws RemoteException;

    void stopLoading() throws RemoteException;

    void zza(zzaak zzaak) throws RemoteException;

    void zza(zzaoy zzaoy) throws RemoteException;

    void zza(zzape zzape, String str) throws RemoteException;

    void zza(zzaro zzaro) throws RemoteException;

    void zza(zzrg zzrg) throws RemoteException;

    void zza(zzuj zzuj) throws RemoteException;

    void zza(zzuo zzuo) throws RemoteException;

    void zza(zzvg zzvg) throws RemoteException;

    void zza(zzvh zzvh) throws RemoteException;

    void zza(zzvx zzvx) throws RemoteException;

    void zza(zzwc zzwc) throws RemoteException;

    void zza(zzwi zzwi) throws RemoteException;

    void zza(zzxh zzxh) throws RemoteException;

    void zza(zzyw zzyw) throws RemoteException;

    boolean zza(zzug zzug) throws RemoteException;

    void zzbr(String str) throws RemoteException;

    IObjectWrapper zzjx() throws RemoteException;

    void zzjy() throws RemoteException;

    zzuj zzjz() throws RemoteException;

    String zzka() throws RemoteException;

    zzxa zzkb() throws RemoteException;

    zzwc zzkc() throws RemoteException;

    zzvh zzkd() throws RemoteException;
}
