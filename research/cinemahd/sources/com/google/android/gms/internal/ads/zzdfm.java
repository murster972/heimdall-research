package com.google.android.gms.internal.ads;

final class zzdfm<E> extends zzdfb<E> {
    private final transient E zzgve;
    private transient int zzgvf;

    zzdfm(E e) {
        this.zzgve = zzdei.checkNotNull(e);
    }

    public final boolean contains(Object obj) {
        return this.zzgve.equals(obj);
    }

    public final int hashCode() {
        int i = this.zzgvf;
        if (i != 0) {
            return i;
        }
        int hashCode = this.zzgve.hashCode();
        this.zzgvf = hashCode;
        return hashCode;
    }

    public final int size() {
        return 1;
    }

    public final String toString() {
        String obj = this.zzgve.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 2);
        sb.append('[');
        sb.append(obj);
        sb.append(']');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final int zza(Object[] objArr, int i) {
        objArr[i] = this.zzgve;
        return i + 1;
    }

    /* renamed from: zzaqx */
    public final zzdfp<E> iterator() {
        return new zzdfd(this.zzgve);
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzari() {
        return this.zzgvf != 0;
    }

    /* access modifiers changed from: package-private */
    public final zzdeu<E> zzarj() {
        return zzdeu.zzaf(this.zzgve);
    }

    zzdfm(E e, int i) {
        this.zzgve = e;
        this.zzgvf = i;
    }
}
