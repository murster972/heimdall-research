package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Predicate;

final /* synthetic */ class zzaij implements Predicate {
    private final zzafn zzczb;

    zzaij(zzafn zzafn) {
        this.zzczb = zzafn;
    }

    public final boolean apply(Object obj) {
        zzafn zzafn = (zzafn) obj;
        return (zzafn instanceof zzaiq) && zzaiq.zza((zzaiq) zzafn).equals(this.zzczb);
    }
}
