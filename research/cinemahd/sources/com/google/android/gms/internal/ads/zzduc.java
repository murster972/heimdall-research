package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

final class zzduc {
    private static final Class<?> zzhqr = zzbca();
    private static final zzdus<?, ?> zzhqs = zzbq(false);
    private static final zzdus<?, ?> zzhqt = zzbq(true);
    private static final zzdus<?, ?> zzhqu = new zzduu();

    public static void zza(int i, List<Double> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzg(i, list, z);
        }
    }

    static int zzaa(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            i = 0;
            while (i2 < size) {
                i += zzdrb.zzfl(zzdss.getLong(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzdrb.zzfl(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzab(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            i = 0;
            while (i2 < size) {
                i += zzdrb.zzgf(zzdrw.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzdrb.zzgf(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzac(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            i = 0;
            while (i2 < size) {
                i += zzdrb.zzga(zzdrw.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzdrb.zzga(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzad(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            i = 0;
            while (i2 < size) {
                i += zzdrb.zzgb(zzdrw.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzdrb.zzgb(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzae(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            i = 0;
            while (i2 < size) {
                i += zzdrb.zzgc(zzdrw.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzdrb.zzgc(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzaf(List<?> list) {
        return list.size() << 2;
    }

    static int zzag(List<?> list) {
        return list.size() << 3;
    }

    static int zzah(List<?> list) {
        return list.size();
    }

    public static void zzb(int i, List<Float> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzf(i, list, z);
        }
    }

    public static zzdus<?, ?> zzbbx() {
        return zzhqs;
    }

    public static zzdus<?, ?> zzbby() {
        return zzhqt;
    }

    public static zzdus<?, ?> zzbbz() {
        return zzhqu;
    }

    private static Class<?> zzbca() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> zzbcb() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    private static zzdus<?, ?> zzbq(boolean z) {
        try {
            Class<?> zzbcb = zzbcb();
            if (zzbcb == null) {
                return null;
            }
            return (zzdus) zzbcb.getConstructor(new Class[]{Boolean.TYPE}).newInstance(new Object[]{Boolean.valueOf(z)});
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void zzc(int i, List<Long> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzc(i, list, z);
        }
    }

    public static void zzd(int i, List<Long> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzd(i, list, z);
        }
    }

    public static void zze(int i, List<Long> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzn(i, list, z);
        }
    }

    public static void zzf(int i, List<Long> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zze(i, list, z);
        }
    }

    public static void zzg(int i, List<Long> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzl(i, list, z);
        }
    }

    public static void zzh(int i, List<Integer> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zza(i, list, z);
        }
    }

    public static void zzi(Class<?> cls) {
        Class<?> cls2;
        if (!zzdrt.class.isAssignableFrom(cls) && (cls2 = zzhqr) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static void zzj(int i, List<Integer> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzm(i, list, z);
        }
    }

    public static void zzk(int i, List<Integer> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzb(i, list, z);
        }
    }

    public static void zzl(int i, List<Integer> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzk(i, list, z);
        }
    }

    public static void zzm(int i, List<Integer> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzh(i, list, z);
        }
    }

    public static void zzn(int i, List<Boolean> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzi(i, list, z);
        }
    }

    static int zzo(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return zzy(list) + (list.size() * zzdrb.zzfz(i));
    }

    static int zzp(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzz(list) + (size * zzdrb.zzfz(i));
    }

    static int zzq(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzaa(list) + (size * zzdrb.zzfz(i));
    }

    static int zzr(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzab(list) + (size * zzdrb.zzfz(i));
    }

    static int zzs(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzac(list) + (size * zzdrb.zzfz(i));
    }

    static int zzt(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzad(list) + (size * zzdrb.zzfz(i));
    }

    static int zzu(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzae(list) + (size * zzdrb.zzfz(i));
    }

    static int zzv(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzdrb.zzai(i, 0);
    }

    static int zzw(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzdrb.zzm(i, 0);
    }

    static int zzx(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzdrb.zzi(i, true);
    }

    static int zzy(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            i = 0;
            while (i2 < size) {
                i += zzdrb.zzfj(zzdss.getLong(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzdrb.zzfj(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzz(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            i = 0;
            while (i2 < size) {
                i += zzdrb.zzfk(zzdss.getLong(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzdrb.zzfk(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static void zza(int i, List<String> list, zzdvl zzdvl) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zza(i, list);
        }
    }

    public static void zzb(int i, List<zzdqk> list, zzdvl zzdvl) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzb(i, list);
        }
    }

    static int zzc(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int zzfz = zzdrb.zzfz(i) * size;
        if (list instanceof zzdsl) {
            zzdsl zzdsl = (zzdsl) list;
            while (i4 < size) {
                Object zzgm = zzdsl.zzgm(i4);
                if (zzgm instanceof zzdqk) {
                    i3 = zzdrb.zzbf((zzdqk) zzgm);
                } else {
                    i3 = zzdrb.zzhh((String) zzgm);
                }
                zzfz += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof zzdqk) {
                    i2 = zzdrb.zzbf((zzdqk) obj);
                } else {
                    i2 = zzdrb.zzhh((String) obj);
                }
                zzfz += i2;
                i4++;
            }
        }
        return zzfz;
    }

    static int zzd(int i, List<zzdqk> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zzfz = size * zzdrb.zzfz(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            zzfz += zzdrb.zzbf(list.get(i2));
        }
        return zzfz;
    }

    static boolean zzg(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static void zzi(int i, List<Integer> list, zzdvl zzdvl, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzj(i, list, z);
        }
    }

    public static void zza(int i, List<?> list, zzdvl zzdvl, zzdua zzdua) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zza(i, list, zzdua);
        }
    }

    public static void zzb(int i, List<?> list, zzdvl zzdvl, zzdua zzdua) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzdvl.zzb(i, list, zzdua);
        }
    }

    static <T> void zza(zzdsx zzdsx, T t, T t2, long j) {
        zzduy.zza((Object) t, j, zzdsx.zze(zzduy.zzp(t, j), zzduy.zzp(t2, j)));
    }

    static int zzd(int i, List<zzdte> list, zzdua zzdua) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzdrb.zzc(i, list.get(i3), zzdua);
        }
        return i2;
    }

    static <T, FT extends zzdro<FT>> void zza(zzdri<FT> zzdri, T t, T t2) {
        zzdrm<FT> zzal = zzdri.zzal(t2);
        if (!zzal.zzhjp.isEmpty()) {
            zzdri.zzam(t).zza(zzal);
        }
    }

    static <T, UT, UB> void zza(zzdus<UT, UB> zzdus, T t, T t2) {
        zzdus.zzh(t, zzdus.zzj(zzdus.zzbb(t), zzdus.zzbb(t2)));
    }

    static int zzc(int i, Object obj, zzdua zzdua) {
        if (obj instanceof zzdsj) {
            return zzdrb.zza(i, (zzdsj) obj);
        }
        return zzdrb.zzb(i, (zzdte) obj, zzdua);
    }

    static <UT, UB> UB zza(int i, List<Integer> list, zzdsa zzdsa, UB ub, zzdus<UT, UB> zzdus) {
        UB ub2;
        if (zzdsa == null) {
            return ub;
        }
        if (!(list instanceof RandomAccess)) {
            Iterator<Integer> it2 = list.iterator();
            loop1:
            while (true) {
                ub2 = ub;
                while (it2.hasNext()) {
                    int intValue = it2.next().intValue();
                    if (!zzdsa.zzf(intValue)) {
                        ub = zza(i, intValue, ub2, zzdus);
                        it2.remove();
                    }
                }
                break loop1;
            }
        } else {
            int size = list.size();
            ub2 = ub;
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue2 = list.get(i3).intValue();
                if (zzdsa.zzf(intValue2)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue2));
                    }
                    i2++;
                } else {
                    ub2 = zza(i, intValue2, ub2, zzdus);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        }
        return ub2;
    }

    static int zzc(int i, List<?> list, zzdua zzdua) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zzfz = zzdrb.zzfz(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof zzdsj) {
                i2 = zzdrb.zza((zzdsj) obj);
            } else {
                i2 = zzdrb.zzb((zzdte) obj, zzdua);
            }
            zzfz += i2;
        }
        return zzfz;
    }

    static <UT, UB> UB zza(int i, int i2, UB ub, zzdus<UT, UB> zzdus) {
        if (ub == null) {
            ub = zzdus.zzbci();
        }
        zzdus.zza(ub, i, (long) i2);
        return ub;
    }
}
