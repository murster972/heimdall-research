package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.RemoteException;

public abstract class zzvl extends zzgb implements zzvm {
    public zzvl() {
        super("com.google.android.gms.ads.internal.client.IAdLoader");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            zzb((zzug) zzge.zza(parcel, zzug.CREATOR));
            parcel2.writeNoException();
        } else if (i == 2) {
            String mediationAdapterClassName = getMediationAdapterClassName();
            parcel2.writeNoException();
            parcel2.writeString(mediationAdapterClassName);
        } else if (i == 3) {
            boolean isLoading = isLoading();
            parcel2.writeNoException();
            zzge.writeBoolean(parcel2, isLoading);
        } else if (i == 4) {
            String zzka = zzka();
            parcel2.writeNoException();
            parcel2.writeString(zzka);
        } else if (i != 5) {
            return false;
        } else {
            zza((zzug) zzge.zza(parcel, zzug.CREATOR), parcel.readInt());
            parcel2.writeNoException();
        }
        return true;
    }
}
