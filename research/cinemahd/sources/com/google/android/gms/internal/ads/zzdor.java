package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.interfaces.ECPublicKey;

public final class zzdor implements zzdie {
    private static final byte[] zzgzh = new byte[0];
    private final String zzhfn;
    private final byte[] zzhfo;
    private final zzdow zzhfp;
    private final zzdop zzhfq;
    private final zzdot zzhfr;

    public zzdor(ECPublicKey eCPublicKey, byte[] bArr, String str, zzdow zzdow, zzdop zzdop) throws GeneralSecurityException {
        zzdov.zza(eCPublicKey.getW(), eCPublicKey.getParams().getCurve());
        this.zzhfr = new zzdot(eCPublicKey);
        this.zzhfo = bArr;
        this.zzhfn = str;
        this.zzhfp = zzdow;
        this.zzhfq = zzdop;
    }

    public final byte[] zzc(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        zzdos zza = this.zzhfr.zza(this.zzhfn, this.zzhfo, bArr2, this.zzhfq.zzasr(), this.zzhfp);
        byte[] zzc = this.zzhfq.zzm(zza.zzaxe()).zzc(bArr, zzgzh);
        byte[] zzaxd = zza.zzaxd();
        return ByteBuffer.allocate(zzaxd.length + zzc.length).put(zzaxd).put(zzc).array();
    }
}
