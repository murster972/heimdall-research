package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzc;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzcaj {
    private final zzbdr zzbmj;
    private final zzazb zzdij;
    private final zzdq zzefv;
    private final Executor zzfci;
    private final zza zzfpj;
    /* access modifiers changed from: private */
    public final zzcas zzfqf = new zzcas((zzcao) null);
    private final zzaft zzfqg;
    private zzdhe<zzbdi> zzfqh;
    private final Context zzup;

    zzcaj(zzcat zzcat) {
        this.zzup = zzcat.zzup;
        this.zzfci = zzcat.zzfci;
        this.zzefv = zzcat.zzefv;
        this.zzdij = zzcat.zzdij;
        this.zzfpj = zzcat.zzfpj;
        this.zzbmj = zzcat.zzbmj;
        this.zzfqg = new zzaft();
    }

    public final synchronized void destroy() {
        if (this.zzfqh != null) {
            zzdgs.zza(this.zzfqh, new zzcao(this), this.zzfci);
            this.zzfqh = null;
        }
    }

    public final synchronized void zza(String str, zzafn<Object> zzafn) {
        if (this.zzfqh != null) {
            zzdgs.zza(this.zzfqh, new zzcan(this, str, zzafn), this.zzfci);
        }
    }

    public final synchronized void zzakq() {
        this.zzfqh = zzdgs.zzb(zzbdr.zza(this.zzup, this.zzdij, (String) zzve.zzoy().zzd(zzzn.zzclx), this.zzefv, this.zzfpj), new zzcam(this), this.zzfci);
        zzazh.zza(this.zzfqh, "NativeJavascriptExecutor.initializeEngine");
    }

    public final synchronized void zzb(String str, zzafn<Object> zzafn) {
        if (this.zzfqh != null) {
            zzdgs.zza(this.zzfqh, new zzcaq(this, str, zzafn), this.zzfci);
        }
    }

    public final synchronized zzdhe<JSONObject> zzc(String str, JSONObject jSONObject) {
        if (this.zzfqh == null) {
            return zzdgs.zzaj(null);
        }
        return zzdgs.zzb(this.zzfqh, new zzcal(this, str, jSONObject), this.zzfci);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbdi zzm(zzbdi zzbdi) {
        zzbdi.zza("/result", (zzafn<? super zzbdi>) this.zzfqg);
        zzbev zzaaa = zzbdi.zzaaa();
        zzcas zzcas = this.zzfqf;
        zzaaa.zza((zzty) null, zzcas, zzcas, zzcas, zzcas, false, (zzafq) null, new zzc(this.zzup, (zzato) null, (zzapz) null), (zzaon) null, (zzato) null);
        return zzbdi;
    }

    public final synchronized void zza(String str, Map<String, ?> map) {
        if (this.zzfqh != null) {
            zzdgs.zza(this.zzfqh, new zzcap(this, str, map), this.zzfci);
        }
    }

    public final <T> void zza(WeakReference<T> weakReference, String str, zzafn<T> zzafn) {
        zza(str, (zzafn<Object>) new zzcaw(this, weakReference, str, zzafn, (zzcao) null));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(String str, JSONObject jSONObject, zzbdi zzbdi) throws Exception {
        return this.zzfqg.zza(zzbdi, str, jSONObject);
    }
}
