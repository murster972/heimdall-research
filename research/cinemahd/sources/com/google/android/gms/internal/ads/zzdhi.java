package com.google.android.gms.internal.ads;

final class zzdhi implements Runnable {
    private final /* synthetic */ Runnable zzgxf;
    private final /* synthetic */ zzdhf zzgxg;

    zzdhi(zzdhf zzdhf, Runnable runnable) {
        this.zzgxg = zzdhf;
        this.zzgxf = runnable;
    }

    public final void run() {
        this.zzgxg.zzgxb = false;
        this.zzgxf.run();
    }
}
