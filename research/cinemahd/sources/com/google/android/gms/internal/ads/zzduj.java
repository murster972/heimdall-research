package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map;

final class zzduj implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private Iterator<Map.Entry<K, V>> zzhqv;
    private final /* synthetic */ zzdub zzhqw;
    private boolean zzhqz;

    private zzduj(zzdub zzdub) {
        this.zzhqw = zzdub;
        this.pos = -1;
    }

    private final Iterator<Map.Entry<K, V>> zzbcc() {
        if (this.zzhqv == null) {
            this.zzhqv = this.zzhqw.zzhqn.entrySet().iterator();
        }
        return this.zzhqv;
    }

    public final boolean hasNext() {
        if (this.pos + 1 < this.zzhqw.zzhqm.size() || (!this.zzhqw.zzhqn.isEmpty() && zzbcc().hasNext())) {
            return true;
        }
        return false;
    }

    public final /* synthetic */ Object next() {
        this.zzhqz = true;
        int i = this.pos + 1;
        this.pos = i;
        if (i < this.zzhqw.zzhqm.size()) {
            return (Map.Entry) this.zzhqw.zzhqm.get(this.pos);
        }
        return (Map.Entry) zzbcc().next();
    }

    public final void remove() {
        if (this.zzhqz) {
            this.zzhqz = false;
            this.zzhqw.zzbbv();
            if (this.pos < this.zzhqw.zzhqm.size()) {
                zzdub zzdub = this.zzhqw;
                int i = this.pos;
                this.pos = i - 1;
                Object unused = zzdub.zzgx(i);
                return;
            }
            zzbcc().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    /* synthetic */ zzduj(zzdub zzdub, zzdue zzdue) {
        this(zzdub);
    }
}
