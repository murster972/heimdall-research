package com.google.android.gms.internal.cast;

import com.facebook.ads.AdError;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class zzcw extends zzdc {
    private final List<zzed> zzxt = Collections.synchronizedList(new ArrayList());

    public zzcw(String str, String str2, String str3) {
        super(str, str2, (String) null);
    }

    /* access modifiers changed from: protected */
    public final void zza(zzed zzed) {
        this.zzxt.add(zzed);
    }

    public void zzeq() {
        synchronized (this.zzxt) {
            for (zzed zzv : this.zzxt) {
                zzv.zzv(AdError.CACHE_ERROR_CODE);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final List<zzed> zzer() {
        return this.zzxt;
    }
}
