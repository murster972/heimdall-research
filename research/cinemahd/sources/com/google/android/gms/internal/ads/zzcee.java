package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;

public final class zzcee {
    public static Set<zzbsu<zzbph>> zza(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<AppEventListener>> zzb(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<zzbqb>> zzc(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<zzbow>> zzd(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<zzbov>> zze(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<zzbpe>> zzf(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<zzty>> zzg(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<zzdcx>> zzh(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    public static Set<zzbsu<zzbqx>> zzi(zzceo zzceo, Executor executor) {
        return zzc(zzceo, executor);
    }

    private static <T> Set<zzbsu<T>> zzc(T t, Executor executor) {
        if (zzabc.zzctx.get().booleanValue()) {
            return Collections.singleton(new zzbsu(t, executor));
        }
        return Collections.emptySet();
    }
}
