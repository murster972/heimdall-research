package com.google.android.gms.internal.cast;

import com.google.android.gms.cast.framework.media.uicontroller.UIController;

public abstract class zzbt extends UIController {
    private boolean zzri = true;

    public boolean zzee() {
        return this.zzri;
    }

    public abstract void zzg(long j);

    public void zzk(boolean z) {
        this.zzri = z;
    }
}
