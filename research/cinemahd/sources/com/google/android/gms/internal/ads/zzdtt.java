package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.charset.Charset;

final class zzdtt extends zzdqk {
    static final int[] zzhpu = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170, 1836311903, Integer.MAX_VALUE};
    private final int zzhpv;
    /* access modifiers changed from: private */
    public final zzdqk zzhpw;
    /* access modifiers changed from: private */
    public final zzdqk zzhpx;
    private final int zzhpy;
    private final int zzhpz;

    private zzdtt(zzdqk zzdqk, zzdqk zzdqk2) {
        this.zzhpw = zzdqk;
        this.zzhpx = zzdqk2;
        this.zzhpy = zzdqk.size();
        this.zzhpv = this.zzhpy + zzdqk2.size();
        this.zzhpz = Math.max(zzdqk.zzaxw(), zzdqk2.zzaxw()) + 1;
    }

    static zzdqk zza(zzdqk zzdqk, zzdqk zzdqk2) {
        if (zzdqk2.size() == 0) {
            return zzdqk;
        }
        if (zzdqk.size() == 0) {
            return zzdqk2;
        }
        int size = zzdqk.size() + zzdqk2.size();
        if (size < 128) {
            return zzb(zzdqk, zzdqk2);
        }
        if (zzdqk instanceof zzdtt) {
            zzdtt zzdtt = (zzdtt) zzdqk;
            if (zzdtt.zzhpx.size() + zzdqk2.size() < 128) {
                return new zzdtt(zzdtt.zzhpw, zzb(zzdtt.zzhpx, zzdqk2));
            } else if (zzdtt.zzhpw.zzaxw() > zzdtt.zzhpx.zzaxw() && zzdtt.zzaxw() > zzdqk2.zzaxw()) {
                return new zzdtt(zzdtt.zzhpw, new zzdtt(zzdtt.zzhpx, zzdqk2));
            }
        }
        if (size >= zzhpu[Math.max(zzdqk.zzaxw(), zzdqk2.zzaxw()) + 1]) {
            return new zzdtt(zzdqk, zzdqk2);
        }
        return new zzdtv((zzdtw) null).zzc(zzdqk, zzdqk2);
    }

    private static zzdqk zzb(zzdqk zzdqk, zzdqk zzdqk2) {
        int size = zzdqk.size();
        int size2 = zzdqk2.size();
        byte[] bArr = new byte[(size + size2)];
        zzdqk.zza(bArr, 0, 0, size);
        zzdqk2.zza(bArr, 0, size, size2);
        return zzdqk.zzv(bArr);
    }

    public final boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzdqk)) {
            return false;
        }
        zzdqk zzdqk = (zzdqk) obj;
        if (this.zzhpv != zzdqk.size()) {
            return false;
        }
        if (this.zzhpv == 0) {
            return true;
        }
        int zzaxy = zzaxy();
        int zzaxy2 = zzdqk.zzaxy();
        if (zzaxy != 0 && zzaxy2 != 0 && zzaxy != zzaxy2) {
            return false;
        }
        zzdty zzdty = new zzdty(this, (zzdtw) null);
        zzdqr zzdqr = (zzdqr) zzdty.next();
        zzdty zzdty2 = new zzdty(zzdqk, (zzdtw) null);
        zzdqr zzdqr2 = (zzdqr) zzdty2.next();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int size = zzdqr.size() - i;
            int size2 = zzdqr2.size() - i2;
            int min = Math.min(size, size2);
            if (i == 0) {
                z = zzdqr.zza(zzdqr2, i2, min);
            } else {
                z = zzdqr2.zza(zzdqr, i, min);
            }
            if (!z) {
                return false;
            }
            i3 += min;
            int i4 = this.zzhpv;
            if (i3 < i4) {
                if (min == size) {
                    zzdqr = (zzdqr) zzdty.next();
                    i = 0;
                } else {
                    i += min;
                }
                if (min == size2) {
                    zzdqr2 = (zzdqr) zzdty2.next();
                    i2 = 0;
                } else {
                    i2 += min;
                }
            } else if (i3 == i4) {
                return true;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public final int size() {
        return this.zzhpv;
    }

    /* renamed from: zzaxs */
    public final zzdqp iterator() {
        return new zzdtw(this);
    }

    public final boolean zzaxu() {
        int zzg = this.zzhpw.zzg(0, 0, this.zzhpy);
        zzdqk zzdqk = this.zzhpx;
        if (zzdqk.zzg(zzg, 0, zzdqk.size()) == 0) {
            return true;
        }
        return false;
    }

    public final zzdqw zzaxv() {
        return new zzdqx(new zzdtx(this));
    }

    /* access modifiers changed from: protected */
    public final int zzaxw() {
        return this.zzhpz;
    }

    /* access modifiers changed from: protected */
    public final boolean zzaxx() {
        return this.zzhpv >= zzhpu[this.zzhpz];
    }

    public final byte zzfe(int i) {
        zzdqk.zzz(i, this.zzhpv);
        return zzff(i);
    }

    /* access modifiers changed from: package-private */
    public final byte zzff(int i) {
        int i2 = this.zzhpy;
        if (i < i2) {
            return this.zzhpw.zzff(i);
        }
        return this.zzhpx.zzff(i - i2);
    }

    /* access modifiers changed from: protected */
    public final int zzg(int i, int i2, int i3) {
        int i4 = i2 + i3;
        int i5 = this.zzhpy;
        if (i4 <= i5) {
            return this.zzhpw.zzg(i, i2, i3);
        }
        if (i2 >= i5) {
            return this.zzhpx.zzg(i, i2 - i5, i3);
        }
        int i6 = i5 - i2;
        return this.zzhpx.zzg(this.zzhpw.zzg(i, i2, i6), 0, i3 - i6);
    }

    /* access modifiers changed from: protected */
    public final int zzh(int i, int i2, int i3) {
        int i4 = i2 + i3;
        int i5 = this.zzhpy;
        if (i4 <= i5) {
            return this.zzhpw.zzh(i, i2, i3);
        }
        if (i2 >= i5) {
            return this.zzhpx.zzh(i, i2 - i5, i3);
        }
        int i6 = i5 - i2;
        return this.zzhpx.zzh(this.zzhpw.zzh(i, i2, i6), 0, i3 - i6);
    }

    public final zzdqk zzy(int i, int i2) {
        int zzi = zzdqk.zzi(i, i2, this.zzhpv);
        if (zzi == 0) {
            return zzdqk.zzhhx;
        }
        if (zzi == this.zzhpv) {
            return this;
        }
        int i3 = this.zzhpy;
        if (i2 <= i3) {
            return this.zzhpw.zzy(i, i2);
        }
        if (i >= i3) {
            return this.zzhpx.zzy(i - i3, i2 - i3);
        }
        zzdqk zzdqk = this.zzhpw;
        return new zzdtt(zzdqk.zzy(i, zzdqk.size()), this.zzhpx.zzy(0, i2 - this.zzhpy));
    }

    /* synthetic */ zzdtt(zzdqk zzdqk, zzdqk zzdqk2, zzdtw zzdtw) {
        this(zzdqk, zzdqk2);
    }

    /* access modifiers changed from: protected */
    public final void zzb(byte[] bArr, int i, int i2, int i3) {
        int i4 = i + i3;
        int i5 = this.zzhpy;
        if (i4 <= i5) {
            this.zzhpw.zzb(bArr, i, i2, i3);
        } else if (i >= i5) {
            this.zzhpx.zzb(bArr, i - i5, i2, i3);
        } else {
            int i6 = i5 - i;
            this.zzhpw.zzb(bArr, i, i2, i6);
            this.zzhpx.zzb(bArr, 0, i2 + i6, i3 - i6);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdqh zzdqh) throws IOException {
        this.zzhpw.zza(zzdqh);
        this.zzhpx.zza(zzdqh);
    }

    /* access modifiers changed from: protected */
    public final String zza(Charset charset) {
        return new String(toByteArray(), charset);
    }
}
