package com.google.android.gms.internal.ads;

import java.util.Set;

public final class zzdcz implements zzdxg<zzdcq> {
    private final zzdxp<Set<zzbsu<zzdcx>>> zzfeo;

    private zzdcz(zzdxp<Set<zzbsu<zzdcx>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzdcz zzam(zzdxp<Set<zzbsu<zzdcx>>> zzdxp) {
        return new zzdcz(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzdcq(this.zzfeo.get());
    }
}
