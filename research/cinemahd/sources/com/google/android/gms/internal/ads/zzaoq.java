package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import android.webkit.WebView;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzaoq {
    private static final Object lock = new Object();
    private static boolean zzdgk = false;
    private static boolean zzxx = false;
    private zzddf zzdgl;

    private final void zzq(Context context) {
        synchronized (lock) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue() && !zzdgk) {
                try {
                    zzdgk = true;
                    this.zzdgl = (zzddf) zzayx.zza(context, "com.google.android.gms.ads.omid.DynamiteOmid", zzaop.zzbtz);
                } catch (zzayz e) {
                    zzayu.zze("#007 Could not call remote method.", e);
                }
            }
        }
    }

    public final String getVersion(Context context) {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue()) {
            return null;
        }
        try {
            zzq(context);
            String valueOf = String.valueOf(this.zzdgl.getVersion());
            return valueOf.length() != 0 ? "a.".concat(valueOf) : new String("a.");
        } catch (RemoteException | NullPointerException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return null;
        }
    }

    public final IObjectWrapper zza(String str, WebView webView, String str2, String str3, String str4) {
        return zza(str, webView, str2, str3, str4, "Google");
    }

    public final void zzab(IObjectWrapper iObjectWrapper) {
        synchronized (lock) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue()) {
                if (zzxx) {
                    try {
                        this.zzdgl.zzab(iObjectWrapper);
                    } catch (RemoteException | NullPointerException e) {
                        zzayu.zze("#007 Could not call remote method.", e);
                    }
                }
            }
        }
    }

    public final void zzac(IObjectWrapper iObjectWrapper) {
        synchronized (lock) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue()) {
                if (zzxx) {
                    try {
                        this.zzdgl.zzac(iObjectWrapper);
                    } catch (RemoteException | NullPointerException e) {
                        zzayu.zze("#007 Could not call remote method.", e);
                    }
                }
            }
        }
    }

    public final void zzb(IObjectWrapper iObjectWrapper, View view) {
        synchronized (lock) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue()) {
                if (zzxx) {
                    try {
                        this.zzdgl.zzc(iObjectWrapper, ObjectWrapper.a(view));
                    } catch (RemoteException | NullPointerException e) {
                        zzayu.zze("#007 Could not call remote method.", e);
                    }
                }
            }
        }
    }

    public final boolean zzp(Context context) {
        synchronized (lock) {
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue()) {
                return false;
            }
            if (zzxx) {
                return true;
            }
            try {
                zzq(context);
                boolean zzas = this.zzdgl.zzas(ObjectWrapper.a(context));
                zzxx = zzas;
                return zzas;
            } catch (RemoteException e) {
                e = e;
                zzayu.zze("#007 Could not call remote method.", e);
                return false;
            } catch (NullPointerException e2) {
                e = e2;
                zzayu.zze("#007 Could not call remote method.", e);
                return false;
            }
        }
    }

    public final IObjectWrapper zza(String str, WebView webView, String str2, String str3, String str4, String str5) {
        synchronized (lock) {
            try {
                if (((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue()) {
                    if (zzxx) {
                        try {
                            return this.zzdgl.zza(str, ObjectWrapper.a(webView), str2, str3, str4, str5);
                        } catch (RemoteException | NullPointerException e) {
                            zzayu.zze("#007 Could not call remote method.", e);
                            return null;
                        }
                    }
                }
                return null;
            } catch (Throwable th) {
                th = th;
                throw th;
            }
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, View view) {
        synchronized (lock) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcoc)).booleanValue()) {
                if (zzxx) {
                    try {
                        this.zzdgl.zzb(iObjectWrapper, ObjectWrapper.a(view));
                    } catch (RemoteException | NullPointerException e) {
                        zzayu.zze("#007 Could not call remote method.", e);
                    }
                }
            }
        }
    }
}
