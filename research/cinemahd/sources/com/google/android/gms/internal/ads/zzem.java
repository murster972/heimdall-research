package com.google.android.gms.internal.ads;

final class zzem implements Runnable {
    private final /* synthetic */ zzei zzyc;

    zzem(zzei zzei) {
        this.zzyc = zzei;
    }

    public final void run() {
        zzzn.initialize(this.zzyc.zzup);
    }
}
