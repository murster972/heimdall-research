package com.google.android.gms.internal.ads;

import java.util.HashMap;

public final class zzdu extends zzcj<Integer, Long> {
    public long zzwx;
    public long zzwy;

    public zzdu() {
        this.zzwx = -1;
        this.zzwy = -1;
    }

    /* access modifiers changed from: protected */
    public final void zzap(String str) {
        HashMap zzaq = zzcj.zzaq(str);
        if (zzaq != null) {
            this.zzwx = ((Long) zzaq.get(0)).longValue();
            this.zzwy = ((Long) zzaq.get(1)).longValue();
        }
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> zzbk() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, Long.valueOf(this.zzwx));
        hashMap.put(1, Long.valueOf(this.zzwy));
        return hashMap;
    }

    public zzdu(String str) {
        this();
        zzap(str);
    }
}
