package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdro;
import java.io.IOException;
import java.util.Map;

abstract class zzdri<T extends zzdro<T>> {
    zzdri() {
    }

    /* access modifiers changed from: package-private */
    public abstract int zza(Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    public abstract Object zza(zzdrg zzdrg, zzdte zzdte, int i);

    /* access modifiers changed from: package-private */
    public abstract <UT, UB> UB zza(zzdtu zzdtu, Object obj, zzdrg zzdrg, zzdrm<T> zzdrm, UB ub, zzdus<UT, UB> zzdus) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(zzdqk zzdqk, Object obj, zzdrg zzdrg, zzdrm<T> zzdrm) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(zzdtu zzdtu, Object obj, zzdrg zzdrg, zzdrm<T> zzdrm) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(zzdvl zzdvl, Map.Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract zzdrm<T> zzal(Object obj);

    /* access modifiers changed from: package-private */
    public abstract zzdrm<T> zzam(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zzan(Object obj);

    /* access modifiers changed from: package-private */
    public abstract boolean zzj(zzdte zzdte);
}
