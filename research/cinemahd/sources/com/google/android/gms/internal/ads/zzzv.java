package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

public final class zzzv {
    public static boolean zza(zzaae zzaae, zzaac zzaac, String... strArr) {
        if (zzaae == null || zzaac == null || !zzaae.zzcsb || zzaac == null) {
            return false;
        }
        return zzaae.zza(zzaac, zzq.zzkx().a(), strArr);
    }

    public static zzaac zzb(zzaae zzaae) {
        if (zzaae == null) {
            return null;
        }
        return zzaae.zzex(zzq.zzkx().a());
    }
}
