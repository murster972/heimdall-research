package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import java.util.HashSet;

public final class zzdak implements zzavk, zzbow {
    private final zzavp zzdpz;
    private final HashSet<zzavd> zzgnb = new HashSet<>();
    private final Context zzup;

    public zzdak(Context context, zzavp zzavp) {
        this.zzup = context;
        this.zzdpz = zzavp;
    }

    public final synchronized void onAdFailedToLoad(int i) {
        if (i != 3) {
            this.zzdpz.zzb(this.zzgnb);
        }
    }

    public final synchronized void zza(HashSet<zzavd> hashSet) {
        this.zzgnb.clear();
        this.zzgnb.addAll(hashSet);
    }

    public final Bundle zzaov() {
        return this.zzdpz.zza(this.zzup, (zzavk) this);
    }
}
