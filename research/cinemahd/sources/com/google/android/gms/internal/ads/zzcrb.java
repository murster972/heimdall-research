package com.google.android.gms.internal.ads;

final /* synthetic */ class zzcrb implements zzded {
    private final zzcqz zzgfi;

    zzcrb(zzcqz zzcqz) {
        this.zzgfi = zzcqz;
    }

    public final Object apply(Object obj) {
        return new zzcqw(false, false, ((Boolean) obj).booleanValue());
    }
}
