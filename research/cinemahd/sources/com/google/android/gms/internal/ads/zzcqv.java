package com.google.android.gms.internal.ads;

import android.os.Bundle;

public final class zzcqv implements zzcub<Object> {
    private static final Object lock = new Object();
    private final String zzcdu;
    private final String zzdir;
    private final zzczu zzfgl;
    private final zzbnk zzgfb;
    private final zzdak zzgfc;

    public zzcqv(String str, String str2, zzbnk zzbnk, zzdak zzdak, zzczu zzczu) {
        this.zzdir = str;
        this.zzcdu = str2;
        this.zzgfb = zzbnk;
        this.zzgfc = zzdak;
        this.zzfgl = zzczu;
    }

    public final zzdhe<Object> zzanc() {
        Bundle bundle = new Bundle();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpa)).booleanValue()) {
            this.zzgfb.zzf(this.zzfgl.zzgml);
            bundle.putAll(this.zzgfc.zzaov());
        }
        return zzdgs.zzaj(new zzcqu(this, bundle));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(Bundle bundle, Bundle bundle2) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpa)).booleanValue()) {
            bundle2.putBundle("quality_signals", bundle);
        } else {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcoz)).booleanValue()) {
                synchronized (lock) {
                    this.zzgfb.zzf(this.zzfgl.zzgml);
                    bundle2.putBundle("quality_signals", this.zzgfc.zzaov());
                }
            } else {
                this.zzgfb.zzf(this.zzfgl.zzgml);
                bundle2.putBundle("quality_signals", this.zzgfc.zzaov());
            }
        }
        bundle2.putString("seq_num", this.zzdir);
        bundle2.putString("session_id", this.zzcdu);
    }
}
