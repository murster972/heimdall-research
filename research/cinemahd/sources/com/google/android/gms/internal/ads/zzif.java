package com.google.android.gms.internal.ads;

final class zzif {
    /* access modifiers changed from: private */
    public final zzhc zzadi;
    /* access modifiers changed from: private */
    public final long zzaex;
    /* access modifiers changed from: private */
    public final long zzakg;

    private zzif(zzhc zzhc, long j, long j2) {
        this.zzadi = zzhc;
        this.zzakg = j;
        this.zzaex = j2;
    }

    /* synthetic */ zzif(zzhc zzhc, long j, long j2, zzhz zzhz) {
        this(zzhc, j, j2);
    }
}
