package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzaou extends IInterface {
    IBinder zzae(IObjectWrapper iObjectWrapper) throws RemoteException;
}
