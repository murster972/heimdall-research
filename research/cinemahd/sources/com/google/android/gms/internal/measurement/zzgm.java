package com.google.android.gms.internal.measurement;

public interface zzgm extends zzgp, Cloneable {
    zzgm zza(zzgn zzgn);

    zzgm zza(byte[] bArr) throws zzfn;

    zzgm zza(byte[] bArr, zzeq zzeq) throws zzfn;

    zzgn zzt();

    zzgn zzu();
}
