package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

final class zzdss extends zzdqe<Long> implements zzdsb<Long>, zzdtq, RandomAccess {
    private static final zzdss zzhon;
    private int size;
    private long[] zzhoo;

    static {
        zzdss zzdss = new zzdss(new long[0], 0);
        zzhon = zzdss;
        zzdss.zzaxq();
    }

    zzdss() {
        this(new long[10], 0);
    }

    private final void zzfb(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
    }

    private final String zzfc(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        long longValue = ((Long) obj).longValue();
        zzaxr();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
        long[] jArr = this.zzhoo;
        if (i2 < jArr.length) {
            System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
        } else {
            long[] jArr2 = new long[(((i2 * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            System.arraycopy(this.zzhoo, i, jArr2, i + 1, this.size - i);
            this.zzhoo = jArr2;
        }
        this.zzhoo[i] = longValue;
        this.size++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Long> collection) {
        zzaxr();
        zzdrv.checkNotNull(collection);
        if (!(collection instanceof zzdss)) {
            return super.addAll(collection);
        }
        zzdss zzdss = (zzdss) collection;
        int i = zzdss.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.zzhoo;
            if (i3 > jArr.length) {
                this.zzhoo = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(zzdss.zzhoo, 0, this.zzhoo, this.size, zzdss.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdss)) {
            return super.equals(obj);
        }
        zzdss zzdss = (zzdss) obj;
        if (this.size != zzdss.size) {
            return false;
        }
        long[] jArr = zzdss.zzhoo;
        for (int i = 0; i < this.size; i++) {
            if (this.zzhoo[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(getLong(i));
    }

    public final long getLong(int i) {
        zzfb(i);
        return this.zzhoo[i];
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + zzdrv.zzfq(this.zzhoo[i2]);
        }
        return i;
    }

    public final boolean remove(Object obj) {
        zzaxr();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Long.valueOf(this.zzhoo[i]))) {
                long[] jArr = this.zzhoo;
                System.arraycopy(jArr, i + 1, jArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zzaxr();
        if (i2 >= i) {
            long[] jArr = this.zzhoo;
            System.arraycopy(jArr, i2, jArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        zzaxr();
        zzfb(i);
        long[] jArr = this.zzhoo;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    public final int size() {
        return this.size;
    }

    public final /* synthetic */ zzdsb zzfd(int i) {
        if (i >= this.size) {
            return new zzdss(Arrays.copyOf(this.zzhoo, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    public final void zzfr(long j) {
        zzaxr();
        int i = this.size;
        long[] jArr = this.zzhoo;
        if (i == jArr.length) {
            long[] jArr2 = new long[(((i * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.zzhoo = jArr2;
        }
        long[] jArr3 = this.zzhoo;
        int i2 = this.size;
        this.size = i2 + 1;
        jArr3[i2] = j;
    }

    private zzdss(long[] jArr, int i) {
        this.zzhoo = jArr;
        this.size = i;
    }

    public final /* synthetic */ Object remove(int i) {
        zzaxr();
        zzfb(i);
        long[] jArr = this.zzhoo;
        long j = jArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Long.valueOf(j);
    }

    public final /* synthetic */ boolean add(Object obj) {
        zzfr(((Long) obj).longValue());
        return true;
    }
}
