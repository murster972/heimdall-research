package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;

public interface zzcir<AdT, AdapterT, ListenerT extends zzbpu> {
    void zza(zzczt zzczt, zzczl zzczl, zzcip<AdapterT, ListenerT> zzcip) throws zzdab;

    AdT zzb(zzczt zzczt, zzczl zzczl, zzcip<AdapterT, ListenerT> zzcip) throws zzdab, zzclr;
}
