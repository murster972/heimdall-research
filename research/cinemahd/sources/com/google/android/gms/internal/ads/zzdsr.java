package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

final class zzdsr implements zzdtz {
    private static final zzdtb zzhom = new zzdsu();
    private final zzdtb zzhol;

    public zzdsr() {
        this(new zzdst(zzdru.zzbaa(), zzbaz()));
    }

    private static boolean zza(zzdtc zzdtc) {
        return zzdtc.zzbbg() == zzdrt.zze.zzhna;
    }

    private static zzdtb zzbaz() {
        try {
            return (zzdtb) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (Exception unused) {
            return zzhom;
        }
    }

    public final <T> zzdua<T> zzg(Class<T> cls) {
        Class<zzdrt> cls2 = zzdrt.class;
        zzduc.zzi(cls);
        zzdtc zzf = this.zzhol.zzf(cls);
        if (zzf.zzbbh()) {
            if (cls2.isAssignableFrom(cls)) {
                return zzdtk.zza(zzduc.zzbbz(), zzdrj.zzazk(), zzf.zzbbi());
            }
            return zzdtk.zza(zzduc.zzbbx(), zzdrj.zzazl(), zzf.zzbbi());
        } else if (cls2.isAssignableFrom(cls)) {
            if (zza(zzf)) {
                return zzdti.zza(cls, zzf, zzdto.zzbbk(), zzdso.zzbay(), zzduc.zzbbz(), zzdrj.zzazk(), zzdsz.zzbbe());
            }
            return zzdti.zza(cls, zzf, zzdto.zzbbk(), zzdso.zzbay(), zzduc.zzbbz(), (zzdri<?>) null, zzdsz.zzbbe());
        } else if (zza(zzf)) {
            return zzdti.zza(cls, zzf, zzdto.zzbbj(), zzdso.zzbax(), zzduc.zzbbx(), zzdrj.zzazl(), zzdsz.zzbbd());
        } else {
            return zzdti.zza(cls, zzf, zzdto.zzbbj(), zzdso.zzbax(), zzduc.zzbby(), (zzdri<?>) null, zzdsz.zzbbd());
        }
    }

    private zzdsr(zzdtb zzdtb) {
        this.zzhol = (zzdtb) zzdrv.zza(zzdtb, "messageInfoFactory");
    }
}
