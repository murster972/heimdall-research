package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

public final class zzbjs {
    public static JSONObject zza(zzczl zzczl) {
        try {
            return new JSONObject(zzczl.zzdks);
        } catch (JSONException unused) {
            return null;
        }
    }
}
