package com.google.android.gms.internal.cast;

public abstract class zzk extends zzb implements zzj {
    public zzk() {
        super("com.google.android.gms.cast.framework.internal.IMediaRouter");
    }

    /* JADX WARNING: type inference failed for: r5v2, types: [android.os.IInterface] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean dispatchTransaction(int r2, android.os.Parcel r3, android.os.Parcel r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            switch(r2) {
                case 1: goto L_0x008f;
                case 2: goto L_0x007c;
                case 3: goto L_0x006d;
                case 4: goto L_0x0056;
                case 5: goto L_0x004b;
                case 6: goto L_0x0043;
                case 7: goto L_0x0037;
                case 8: goto L_0x0027;
                case 9: goto L_0x001b;
                case 10: goto L_0x000d;
                case 11: goto L_0x0005;
                default: goto L_0x0003;
            }
        L_0x0003:
            r2 = 0
            return r2
        L_0x0005:
            r1.zzau()
            r4.writeNoException()
            goto L_0x00b9
        L_0x000d:
            r1.zzs()
            r4.writeNoException()
            r2 = 12451009(0xbdfcc1, float:1.744758E-38)
            r4.writeInt(r2)
            goto L_0x00b9
        L_0x001b:
            java.lang.String r2 = r1.zzat()
            r4.writeNoException()
            r4.writeString(r2)
            goto L_0x00b9
        L_0x0027:
            java.lang.String r2 = r3.readString()
            android.os.Bundle r2 = r1.zzm(r2)
            r4.writeNoException()
            com.google.android.gms.internal.cast.zzc.zzb(r4, r2)
            goto L_0x00b9
        L_0x0037:
            boolean r2 = r1.zzas()
            r4.writeNoException()
            com.google.android.gms.internal.cast.zzc.writeBoolean(r4, r2)
            goto L_0x00b9
        L_0x0043:
            r1.zzar()
            r4.writeNoException()
            goto L_0x00b9
        L_0x004b:
            java.lang.String r2 = r3.readString()
            r1.zzl(r2)
            r4.writeNoException()
            goto L_0x00b9
        L_0x0056:
            android.os.Parcelable$Creator r2 = android.os.Bundle.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.cast.zzc.zza((android.os.Parcel) r3, r2)
            android.os.Bundle r2 = (android.os.Bundle) r2
            int r3 = r3.readInt()
            boolean r2 = r1.zzb(r2, r3)
            r4.writeNoException()
            com.google.android.gms.internal.cast.zzc.writeBoolean(r4, r2)
            goto L_0x00b9
        L_0x006d:
            android.os.Parcelable$Creator r2 = android.os.Bundle.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.cast.zzc.zza((android.os.Parcel) r3, r2)
            android.os.Bundle r2 = (android.os.Bundle) r2
            r1.zzd(r2)
            r4.writeNoException()
            goto L_0x00b9
        L_0x007c:
            android.os.Parcelable$Creator r2 = android.os.Bundle.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.cast.zzc.zza((android.os.Parcel) r3, r2)
            android.os.Bundle r2 = (android.os.Bundle) r2
            int r3 = r3.readInt()
            r1.zza((android.os.Bundle) r2, (int) r3)
            r4.writeNoException()
            goto L_0x00b9
        L_0x008f:
            android.os.Parcelable$Creator r2 = android.os.Bundle.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.cast.zzc.zza((android.os.Parcel) r3, r2)
            android.os.Bundle r2 = (android.os.Bundle) r2
            android.os.IBinder r3 = r3.readStrongBinder()
            if (r3 != 0) goto L_0x009f
            r3 = 0
            goto L_0x00b3
        L_0x009f:
            java.lang.String r5 = "com.google.android.gms.cast.framework.internal.IMediaRouterCallback"
            android.os.IInterface r5 = r3.queryLocalInterface(r5)
            boolean r0 = r5 instanceof com.google.android.gms.internal.cast.zzl
            if (r0 == 0) goto L_0x00ad
            r3 = r5
            com.google.android.gms.internal.cast.zzl r3 = (com.google.android.gms.internal.cast.zzl) r3
            goto L_0x00b3
        L_0x00ad:
            com.google.android.gms.internal.cast.zzm r5 = new com.google.android.gms.internal.cast.zzm
            r5.<init>(r3)
            r3 = r5
        L_0x00b3:
            r1.zza((android.os.Bundle) r2, (com.google.android.gms.internal.cast.zzl) r3)
            r4.writeNoException()
        L_0x00b9:
            r2 = 1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.cast.zzk.dispatchTransaction(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
