package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zaat implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zaak f3912a;

    private zaat(zaak zaak) {
        this.f3912a = zaak;
    }

    public final void onConnected(Bundle bundle) {
        this.f3912a.k.a(new zaar(this.f3912a));
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.f3912a.b.lock();
        try {
            if (this.f3912a.a(connectionResult)) {
                this.f3912a.d();
                this.f3912a.b();
            } else {
                this.f3912a.b(connectionResult);
            }
        } finally {
            this.f3912a.b.unlock();
        }
    }

    public final void onConnectionSuspended(int i) {
    }

    /* synthetic */ zaat(zaak zaak, zaal zaal) {
        this(zaak);
    }
}
