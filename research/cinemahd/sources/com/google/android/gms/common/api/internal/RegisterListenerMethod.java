package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AnyClient;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class RegisterListenerMethod<A extends Api.AnyClient, L> {
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void a(A a2, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException;

    public abstract ListenerHolder.ListenerKey<L> b();

    public abstract Feature[] c();

    public final boolean d() {
        throw null;
    }
}
