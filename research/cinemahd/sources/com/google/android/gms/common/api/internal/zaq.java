package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;

public final class zaq implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /* renamed from: a  reason: collision with root package name */
    public final Api<?> f3952a;
    private final boolean b;
    private zar c;

    public zaq(Api<?> api, boolean z) {
        this.f3952a = api;
        this.b = z;
    }

    public final void a(zar zar) {
        this.c = zar;
    }

    public final void onConnected(Bundle bundle) {
        a();
        this.c.onConnected(bundle);
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        a();
        this.c.a(connectionResult, this.f3952a, this.b);
    }

    public final void onConnectionSuspended(int i) {
        a();
        this.c.onConnectionSuspended(i);
    }

    private final void a() {
        Preconditions.a(this.c, (Object) "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }
}
