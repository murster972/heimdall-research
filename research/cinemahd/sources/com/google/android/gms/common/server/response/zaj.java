package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.server.response.FastJsonResponse;

public final class zaj implements Parcelable.Creator<zam> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        int i = 0;
        FastJsonResponse.Field field = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 2) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                field = (FastJsonResponse.Field) SafeParcelReader.a(parcel, a2, FastJsonResponse.Field.CREATOR);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zam(i, str, field);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zam[i];
    }
}
