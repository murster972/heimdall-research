package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.internal.base.zal;

final class zabb extends zal {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zaaw f3916a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zabb(zaaw zaaw, Looper looper) {
        super(looper);
        this.f3916a = zaaw;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            this.f3916a.l();
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GoogleApiClientImpl", sb.toString());
        } else {
            this.f3916a.j();
        }
    }
}
