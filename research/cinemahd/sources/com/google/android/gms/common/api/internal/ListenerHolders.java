package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

public class ListenerHolders {

    /* renamed from: a  reason: collision with root package name */
    private final Set<ListenerHolder<?>> f3901a = Collections.newSetFromMap(new WeakHashMap());

    public final void a() {
        for (ListenerHolder<?> a2 : this.f3901a) {
            a2.a();
        }
        this.f3901a.clear();
    }

    public static <L> ListenerHolder<L> a(L l, Looper looper, String str) {
        Preconditions.a(l, (Object) "Listener must not be null");
        Preconditions.a(looper, (Object) "Looper must not be null");
        Preconditions.a(str, (Object) "Listener type must not be null");
        return new ListenerHolder<>(looper, l, str);
    }
}
