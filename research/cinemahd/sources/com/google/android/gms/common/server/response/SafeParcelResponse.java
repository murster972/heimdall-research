package com.google.android.gms.common.server.response;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.common.util.MapUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SafeParcelResponse extends FastSafeParcelableJsonResponse {
    public static final Parcelable.Creator<SafeParcelResponse> CREATOR = new zap();

    /* renamed from: a  reason: collision with root package name */
    private final int f4012a;
    private final Parcel b;
    private final int c = 2;
    private final zak d;
    private final String e;
    private int f;
    private int g;

    SafeParcelResponse(int i, Parcel parcel, zak zak) {
        this.f4012a = i;
        Preconditions.a(parcel);
        this.b = parcel;
        this.d = zak;
        zak zak2 = this.d;
        if (zak2 == null) {
            this.e = null;
        } else {
            this.e = zak2.t();
        }
        this.f = 2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0005, code lost:
        if (r0 != 1) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final android.os.Parcel b() {
        /*
            r2 = this;
            int r0 = r2.f
            if (r0 == 0) goto L_0x0008
            r1 = 1
            if (r0 == r1) goto L_0x0010
            goto L_0x001a
        L_0x0008:
            android.os.Parcel r0 = r2.b
            int r0 = com.google.android.gms.common.internal.safeparcel.SafeParcelWriter.a(r0)
            r2.g = r0
        L_0x0010:
            android.os.Parcel r0 = r2.b
            int r1 = r2.g
            com.google.android.gms.common.internal.safeparcel.SafeParcelWriter.a(r0, r1)
            r0 = 2
            r2.f = r0
        L_0x001a:
            android.os.Parcel r0 = r2.b
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.SafeParcelResponse.b():android.os.Parcel");
    }

    public Map<String, FastJsonResponse.Field<?, ?>> a() {
        zak zak = this.d;
        if (zak == null) {
            return null;
        }
        return zak.e(this.e);
    }

    public String toString() {
        Preconditions.a(this.d, (Object) "Cannot convert to JSON on client side.");
        Parcel b2 = b();
        b2.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        a(sb, this.d.e(this.e), b2);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zak zak;
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4012a);
        SafeParcelWriter.a(parcel, 2, b(), false);
        int i2 = this.c;
        if (i2 == 0) {
            zak = null;
        } else if (i2 == 1) {
            zak = this.d;
        } else if (i2 == 2) {
            zak = this.d;
        } else {
            StringBuilder sb = new StringBuilder(34);
            sb.append("Invalid creation type: ");
            sb.append(i2);
            throw new IllegalStateException(sb.toString());
        }
        SafeParcelWriter.a(parcel, 3, (Parcelable) zak, i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public Object a(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    private final void a(StringBuilder sb, Map<String, FastJsonResponse.Field<?, ?>> map, Parcel parcel) {
        SparseArray sparseArray = new SparseArray();
        for (Map.Entry next : map.entrySet()) {
            sparseArray.put(((FastJsonResponse.Field) next.getValue()).s(), next);
        }
        sb.append('{');
        int b2 = SafeParcelReader.b(parcel);
        boolean z = false;
        while (parcel.dataPosition() < b2) {
            int a2 = SafeParcelReader.a(parcel);
            Map.Entry entry = (Map.Entry) sparseArray.get(SafeParcelReader.a(a2));
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                FastJsonResponse.Field field = (FastJsonResponse.Field) entry.getValue();
                sb.append("\"");
                sb.append((String) entry.getKey());
                sb.append("\":");
                if (field.t()) {
                    int i = field.d;
                    switch (i) {
                        case 0:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, Integer.valueOf(SafeParcelReader.w(parcel, a2))));
                            break;
                        case 1:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, SafeParcelReader.c(parcel, a2)));
                            break;
                        case 2:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, Long.valueOf(SafeParcelReader.y(parcel, a2))));
                            break;
                        case 3:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, Float.valueOf(SafeParcelReader.u(parcel, a2))));
                            break;
                        case 4:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, Double.valueOf(SafeParcelReader.t(parcel, a2))));
                            break;
                        case 5:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, SafeParcelReader.a(parcel, a2)));
                            break;
                        case 6:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, Boolean.valueOf(SafeParcelReader.s(parcel, a2))));
                            break;
                        case 7:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, SafeParcelReader.o(parcel, a2)));
                            break;
                        case 8:
                        case 9:
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, SafeParcelReader.g(parcel, a2)));
                            break;
                        case 10:
                            Bundle f2 = SafeParcelReader.f(parcel, a2);
                            HashMap hashMap = new HashMap();
                            for (String str : f2.keySet()) {
                                hashMap.put(str, f2.getString(str));
                            }
                            a(sb, (FastJsonResponse.Field<?, ?>) field, FastJsonResponse.a(field, hashMap));
                            break;
                        case 11:
                            throw new IllegalArgumentException("Method does not accept concrete type.");
                        default:
                            StringBuilder sb2 = new StringBuilder(36);
                            sb2.append("Unknown field out type = ");
                            sb2.append(i);
                            throw new IllegalArgumentException(sb2.toString());
                    }
                } else if (field.e) {
                    sb.append("[");
                    switch (field.d) {
                        case 0:
                            ArrayUtils.a(sb, SafeParcelReader.j(parcel, a2));
                            break;
                        case 1:
                            ArrayUtils.a(sb, (T[]) SafeParcelReader.d(parcel, a2));
                            break;
                        case 2:
                            ArrayUtils.a(sb, SafeParcelReader.l(parcel, a2));
                            break;
                        case 3:
                            ArrayUtils.a(sb, SafeParcelReader.i(parcel, a2));
                            break;
                        case 4:
                            ArrayUtils.a(sb, SafeParcelReader.h(parcel, a2));
                            break;
                        case 5:
                            ArrayUtils.a(sb, (T[]) SafeParcelReader.b(parcel, a2));
                            break;
                        case 6:
                            ArrayUtils.a(sb, SafeParcelReader.e(parcel, a2));
                            break;
                        case 7:
                            ArrayUtils.a(sb, SafeParcelReader.p(parcel, a2));
                            break;
                        case 8:
                        case 9:
                        case 10:
                            throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                        case 11:
                            Parcel[] n = SafeParcelReader.n(parcel, a2);
                            int length = n.length;
                            for (int i2 = 0; i2 < length; i2++) {
                                if (i2 > 0) {
                                    sb.append(",");
                                }
                                n[i2].setDataPosition(0);
                                a(sb, field.u(), n[i2]);
                            }
                            break;
                        default:
                            throw new IllegalStateException("Unknown field type out.");
                    }
                    sb.append("]");
                } else {
                    switch (field.d) {
                        case 0:
                            sb.append(SafeParcelReader.w(parcel, a2));
                            break;
                        case 1:
                            sb.append(SafeParcelReader.c(parcel, a2));
                            break;
                        case 2:
                            sb.append(SafeParcelReader.y(parcel, a2));
                            break;
                        case 3:
                            sb.append(SafeParcelReader.u(parcel, a2));
                            break;
                        case 4:
                            sb.append(SafeParcelReader.t(parcel, a2));
                            break;
                        case 5:
                            sb.append(SafeParcelReader.a(parcel, a2));
                            break;
                        case 6:
                            sb.append(SafeParcelReader.s(parcel, a2));
                            break;
                        case 7:
                            String o = SafeParcelReader.o(parcel, a2);
                            sb.append("\"");
                            sb.append(JsonUtils.a(o));
                            sb.append("\"");
                            break;
                        case 8:
                            byte[] g2 = SafeParcelReader.g(parcel, a2);
                            sb.append("\"");
                            sb.append(Base64Utils.a(g2));
                            sb.append("\"");
                            break;
                        case 9:
                            byte[] g3 = SafeParcelReader.g(parcel, a2);
                            sb.append("\"");
                            sb.append(Base64Utils.b(g3));
                            sb.append("\"");
                            break;
                        case 10:
                            Bundle f3 = SafeParcelReader.f(parcel, a2);
                            Set<String> keySet = f3.keySet();
                            keySet.size();
                            sb.append("{");
                            boolean z2 = true;
                            for (String str2 : keySet) {
                                if (!z2) {
                                    sb.append(",");
                                }
                                sb.append("\"");
                                sb.append(str2);
                                sb.append("\"");
                                sb.append(":");
                                sb.append("\"");
                                sb.append(JsonUtils.a(f3.getString(str2)));
                                sb.append("\"");
                                z2 = false;
                            }
                            sb.append("}");
                            break;
                        case 11:
                            Parcel m = SafeParcelReader.m(parcel, a2);
                            m.setDataPosition(0);
                            a(sb, field.u(), m);
                            break;
                        default:
                            throw new IllegalStateException("Unknown field type out");
                    }
                }
                z = true;
            }
        }
        if (parcel.dataPosition() == b2) {
            sb.append('}');
            return;
        }
        StringBuilder sb3 = new StringBuilder(37);
        sb3.append("Overread allowed size end=");
        sb3.append(b2);
        throw new SafeParcelReader.ParseException(sb3.toString(), parcel);
    }

    public boolean b(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    private final void a(StringBuilder sb, FastJsonResponse.Field<?, ?> field, Object obj) {
        if (field.c) {
            ArrayList arrayList = (ArrayList) obj;
            sb.append("[");
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    sb.append(",");
                }
                a(sb, field.b, arrayList.get(i));
            }
            sb.append("]");
            return;
        }
        a(sb, field.b, obj);
    }

    private static void a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"");
                sb.append(JsonUtils.a(obj.toString()));
                sb.append("\"");
                return;
            case 8:
                sb.append("\"");
                sb.append(Base64Utils.a((byte[]) obj));
                sb.append("\"");
                return;
            case 9:
                sb.append("\"");
                sb.append(Base64Utils.b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                MapUtils.a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                StringBuilder sb2 = new StringBuilder(26);
                sb2.append("Unknown type = ");
                sb2.append(i);
                throw new IllegalArgumentException(sb2.toString());
        }
    }
}
