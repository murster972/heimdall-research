package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public class SignInButtonConfig extends AbstractSafeParcelable {
    public static final Parcelable.Creator<SignInButtonConfig> CREATOR = new zao();

    /* renamed from: a  reason: collision with root package name */
    private final int f3993a;
    private final int b;
    private final int c;
    @Deprecated
    private final Scope[] d;

    SignInButtonConfig(int i, int i2, int i3, Scope[] scopeArr) {
        this.f3993a = i;
        this.b = i2;
        this.c = i3;
        this.d = scopeArr;
    }

    public int s() {
        return this.b;
    }

    public int t() {
        return this.c;
    }

    @Deprecated
    public Scope[] u() {
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f3993a);
        SafeParcelWriter.a(parcel, 2, s());
        SafeParcelWriter.a(parcel, 3, t());
        SafeParcelWriter.a(parcel, 4, (T[]) u(), i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public SignInButtonConfig(int i, int i2, Scope[] scopeArr) {
        this(1, i, i2, (Scope[]) null);
    }
}
