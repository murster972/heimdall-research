package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.BackgroundDetector;

final class zabi implements BackgroundDetector.BackgroundStateChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ GoogleApiManager f3922a;

    zabi(GoogleApiManager googleApiManager) {
        this.f3922a = googleApiManager;
    }

    public final void a(boolean z) {
        this.f3922a.m.sendMessage(this.f3922a.m.obtainMessage(1, Boolean.valueOf(z)));
    }
}
