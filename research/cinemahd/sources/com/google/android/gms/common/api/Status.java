package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class Status extends AbstractSafeParcelable implements Result, ReflectedParcelable {
    public static final Parcelable.Creator<Status> CREATOR = new zzb();
    public static final Status e = new Status(0);
    public static final Status f = new Status(14);
    public static final Status g = new Status(8);
    public static final Status h = new Status(15);
    public static final Status i = new Status(16);

    /* renamed from: a  reason: collision with root package name */
    private final int f3888a;
    private final int b;
    private final String c;
    private final PendingIntent d;

    static {
        new Status(17);
        new Status(18);
    }

    Status(int i2, int i3, String str, PendingIntent pendingIntent) {
        this.f3888a = i2;
        this.b = i3;
        this.c = str;
        this.d = pendingIntent;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        if (this.f3888a != status.f3888a || this.b != status.b || !Objects.a(this.c, status.c) || !Objects.a(this.d, status.d)) {
            return false;
        }
        return true;
    }

    public final Status getStatus() {
        return this;
    }

    public final int hashCode() {
        return Objects.a(Integer.valueOf(this.f3888a), Integer.valueOf(this.b), this.c, this.d);
    }

    public final int s() {
        return this.b;
    }

    public final String t() {
        return this.c;
    }

    public final String toString() {
        Objects.ToStringHelper a2 = Objects.a((Object) this);
        a2.a("statusCode", w());
        a2.a("resolution", this.d);
        return a2.toString();
    }

    public final boolean u() {
        return this.d != null;
    }

    public final boolean v() {
        return this.b <= 0;
    }

    public final String w() {
        String str = this.c;
        if (str != null) {
            return str;
        }
        return CommonStatusCodes.a(this.b);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, s());
        SafeParcelWriter.a(parcel, 2, t(), false);
        SafeParcelWriter.a(parcel, 3, (Parcelable) this.d, i2, false);
        SafeParcelWriter.a(parcel, 1000, this.f3888a);
        SafeParcelWriter.a(parcel, a2);
    }

    public Status(int i2) {
        this(i2, (String) null);
    }

    public Status(int i2, String str) {
        this(1, i2, str, (PendingIntent) null);
    }

    public Status(int i2, String str, PendingIntent pendingIntent) {
        this(1, i2, str, pendingIntent);
    }
}
