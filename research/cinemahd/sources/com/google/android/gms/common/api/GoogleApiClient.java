package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl;
import com.google.android.gms.common.api.internal.LifecycleActivity;
import com.google.android.gms.common.api.internal.zaaw;
import com.google.android.gms.common.api.internal.zacm;
import com.google.android.gms.common.api.internal.zaj;
import com.google.android.gms.common.api.internal.zaq;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zaa;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;

public abstract class GoogleApiClient {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Set<GoogleApiClient> f3885a = Collections.newSetFromMap(new WeakHashMap());

    public interface ConnectionCallbacks {
        void onConnected(Bundle bundle);

        void onConnectionSuspended(int i);
    }

    public interface OnConnectionFailedListener {
        void onConnectionFailed(ConnectionResult connectionResult);
    }

    public <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t) {
        throw new UnsupportedOperationException();
    }

    public abstract void a();

    public abstract void a(OnConnectionFailedListener onConnectionFailedListener);

    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public abstract boolean a(Api<?> api);

    public abstract void b();

    public abstract void b(OnConnectionFailedListener onConnectionFailedListener);

    public void b(zacm zacm) {
        throw new UnsupportedOperationException();
    }

    public Context c() {
        throw new UnsupportedOperationException();
    }

    public Looper d() {
        throw new UnsupportedOperationException();
    }

    public abstract boolean e();

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private Account f3886a;
        private final Set<Scope> b = new HashSet();
        private final Set<Scope> c = new HashSet();
        private int d;
        private View e;
        private String f;
        private String g;
        private final Map<Api<?>, ClientSettings.OptionalApiSettings> h = new ArrayMap();
        private final Context i;
        private final Map<Api<?>, Api.ApiOptions> j = new ArrayMap();
        private LifecycleActivity k;
        private int l = -1;
        private OnConnectionFailedListener m;
        private Looper n;
        private GoogleApiAvailability o = GoogleApiAvailability.a();
        private Api.AbstractClientBuilder<? extends zad, SignInOptions> p = zaa.c;
        private final ArrayList<ConnectionCallbacks> q = new ArrayList<>();
        private final ArrayList<OnConnectionFailedListener> r = new ArrayList<>();

        public Builder(Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        public final Builder a(ConnectionCallbacks connectionCallbacks) {
            Preconditions.a(connectionCallbacks, (Object) "Listener must not be null");
            this.q.add(connectionCallbacks);
            return this;
        }

        public final ClientSettings b() {
            SignInOptions signInOptions = SignInOptions.i;
            if (this.j.containsKey(zaa.e)) {
                signInOptions = (SignInOptions) this.j.get(zaa.e);
            }
            return new ClientSettings(this.f3886a, this.b, this.h, this.d, this.e, this.f, this.g, signInOptions);
        }

        public final Builder a(OnConnectionFailedListener onConnectionFailedListener) {
            Preconditions.a(onConnectionFailedListener, (Object) "Listener must not be null");
            this.r.add(onConnectionFailedListener);
            return this;
        }

        public final <O extends Api.ApiOptions.HasOptions> Builder a(Api<O> api, O o2) {
            Preconditions.a(api, (Object) "Api must not be null");
            Preconditions.a(o2, (Object) "Null options are not permitted for this Api");
            this.j.put(api, o2);
            List<Scope> impliedScopes = api.c().getImpliedScopes(o2);
            this.c.addAll(impliedScopes);
            this.b.addAll(impliedScopes);
            return this;
        }

        public final GoogleApiClient a() {
            Preconditions.a(!this.j.isEmpty(), (Object) "must call addApi() to add at least one API");
            ClientSettings b2 = b();
            Api api = null;
            Map<Api<?>, ClientSettings.OptionalApiSettings> e2 = b2.e();
            ArrayMap arrayMap = new ArrayMap();
            ArrayMap arrayMap2 = new ArrayMap();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (Api next : this.j.keySet()) {
                Api.ApiOptions apiOptions = this.j.get(next);
                boolean z2 = e2.get(next) != null;
                arrayMap.put(next, Boolean.valueOf(z2));
                zaq zaq = new zaq(next, z2);
                arrayList.add(zaq);
                Api.AbstractClientBuilder d2 = next.d();
                Api api2 = next;
                Api.Client buildClient = d2.buildClient(this.i, this.n, b2, apiOptions, zaq, zaq);
                arrayMap2.put(api2.a(), buildClient);
                if (d2.getPriority() == 1) {
                    z = apiOptions != null;
                }
                if (buildClient.providesSignIn()) {
                    if (api == null) {
                        api = api2;
                    } else {
                        String b3 = api2.b();
                        String b4 = api.b();
                        StringBuilder sb = new StringBuilder(String.valueOf(b3).length() + 21 + String.valueOf(b4).length());
                        sb.append(b3);
                        sb.append(" cannot be used with ");
                        sb.append(b4);
                        throw new IllegalStateException(sb.toString());
                    }
                }
            }
            if (api != null) {
                if (!z) {
                    Preconditions.b(this.f3886a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", api.b());
                    Preconditions.b(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", api.b());
                } else {
                    String b5 = api.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b5).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b5);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            zaaw zaaw = new zaaw(this.i, new ReentrantLock(), this.n, b2, this.o, this.p, arrayMap, this.q, this.r, arrayMap2, this.l, zaaw.a((Iterable<Api.Client>) arrayMap2.values(), true), arrayList, false);
            synchronized (GoogleApiClient.f3885a) {
                GoogleApiClient.f3885a.add(zaaw);
            }
            if (this.l >= 0) {
                zaj.b(this.k).a(this.l, zaaw, this.m);
            }
            return zaaw;
        }
    }

    public <C extends Api.Client> C a(Api.AnyClientKey<C> anyClientKey) {
        throw new UnsupportedOperationException();
    }

    public void a(zacm zacm) {
        throw new UnsupportedOperationException();
    }
}
