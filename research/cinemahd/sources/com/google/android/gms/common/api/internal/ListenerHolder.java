package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.base.zal;

public final class ListenerHolder<L> {

    /* renamed from: a  reason: collision with root package name */
    private volatile L f3898a;

    public static final class ListenerKey<L> {

        /* renamed from: a  reason: collision with root package name */
        private final L f3899a;
        private final String b;

        ListenerKey(L l, String str) {
            this.f3899a = l;
            this.b = str;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ListenerKey)) {
                return false;
            }
            ListenerKey listenerKey = (ListenerKey) obj;
            return this.f3899a == listenerKey.f3899a && this.b.equals(listenerKey.b);
        }

        public final int hashCode() {
            return (System.identityHashCode(this.f3899a) * 31) + this.b.hashCode();
        }
    }

    public interface Notifier<L> {
        void a();

        void a(L l);
    }

    private final class zaa extends zal {
        public zaa(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            boolean z = true;
            if (message.what != 1) {
                z = false;
            }
            Preconditions.a(z);
            ListenerHolder.this.a((Notifier) message.obj);
        }
    }

    ListenerHolder(Looper looper, L l, String str) {
        new zaa(looper);
        Preconditions.a(l, (Object) "Listener must not be null");
        this.f3898a = l;
        Preconditions.b(str);
        new ListenerKey(l, str);
    }

    public final void a() {
        this.f3898a = null;
    }

    /* access modifiers changed from: package-private */
    public final void a(Notifier<? super L> notifier) {
        L l = this.f3898a;
        if (l == null) {
            notifier.a();
            return;
        }
        try {
            notifier.a(l);
        } catch (RuntimeException e) {
            notifier.a();
            throw e;
        }
    }
}
