package com.google.android.gms.common.internal;

public final class zzh {

    /* renamed from: a  reason: collision with root package name */
    private final String f4004a;
    private final String b;
    private final int c = 129;
    private final boolean d;

    public zzh(String str, String str2, boolean z, int i, boolean z2) {
        this.b = str;
        this.f4004a = str2;
        this.d = z2;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        return this.f4004a;
    }
}
