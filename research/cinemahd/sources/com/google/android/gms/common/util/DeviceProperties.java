package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

public final class DeviceProperties {

    /* renamed from: a  reason: collision with root package name */
    private static Boolean f4019a;
    private static Boolean b;
    private static Boolean c;
    private static Boolean d;

    private DeviceProperties() {
    }

    public static boolean a(Context context) {
        if (c == null) {
            PackageManager packageManager = context.getPackageManager();
            c = Boolean.valueOf(packageManager.hasSystemFeature("com.google.android.feature.services_updater") && packageManager.hasSystemFeature("cn.google.services"));
        }
        return c.booleanValue();
    }

    @TargetApi(21)
    public static boolean b(Context context) {
        return e(context);
    }

    @TargetApi(20)
    public static boolean c(Context context) {
        if (f4019a == null) {
            f4019a = Boolean.valueOf(PlatformVersion.g() && context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return f4019a.booleanValue();
    }

    @TargetApi(26)
    public static boolean d(Context context) {
        if (!c(context)) {
            return false;
        }
        if (PlatformVersion.j()) {
            return e(context) && !PlatformVersion.k();
        }
        return true;
    }

    @TargetApi(21)
    private static boolean e(Context context) {
        if (b == null) {
            b = Boolean.valueOf(PlatformVersion.h() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return b.booleanValue();
    }

    public static boolean f(Context context) {
        if (d == null) {
            d = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return d.booleanValue();
    }

    public static boolean a() {
        return "user".equals(Build.TYPE);
    }
}
