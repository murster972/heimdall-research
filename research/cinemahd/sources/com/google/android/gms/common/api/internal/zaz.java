package com.google.android.gms.common.api.internal;

import android.util.Log;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.Collections;
import java.util.Map;

final class zaz implements OnCompleteListener<Map<zai<?>, String>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zax f3958a;

    private zaz(zax zax) {
        this.f3958a = zax;
    }

    public final void a(Task<Map<zai<?>, String>> task) {
        this.f3958a.f.lock();
        try {
            if (this.f3958a.n) {
                if (task.d()) {
                    Map unused = this.f3958a.o = new ArrayMap(this.f3958a.f3957a.size());
                    for (zaw zak : this.f3958a.f3957a.values()) {
                        this.f3958a.o.put(zak.zak(), ConnectionResult.e);
                    }
                } else if (task.a() instanceof AvailabilityException) {
                    AvailabilityException availabilityException = (AvailabilityException) task.a();
                    if (this.f3958a.l) {
                        Map unused2 = this.f3958a.o = new ArrayMap(this.f3958a.f3957a.size());
                        for (zaw zaw : this.f3958a.f3957a.values()) {
                            zai zak2 = zaw.zak();
                            ConnectionResult a2 = availabilityException.a(zaw);
                            if (this.f3958a.a((zaw<?>) zaw, a2)) {
                                this.f3958a.o.put(zak2, new ConnectionResult(16));
                            } else {
                                this.f3958a.o.put(zak2, a2);
                            }
                        }
                    } else {
                        Map unused3 = this.f3958a.o = availabilityException.a();
                    }
                    ConnectionResult unused4 = this.f3958a.r = this.f3958a.d();
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", task.a());
                    Map unused5 = this.f3958a.o = Collections.emptyMap();
                    ConnectionResult unused6 = this.f3958a.r = new ConnectionResult(8);
                }
                if (this.f3958a.p != null) {
                    this.f3958a.o.putAll(this.f3958a.p);
                    ConnectionResult unused7 = this.f3958a.r = this.f3958a.d();
                }
                if (this.f3958a.r == null) {
                    this.f3958a.b();
                    this.f3958a.c();
                } else {
                    boolean unused8 = this.f3958a.n = false;
                    this.f3958a.e.a(this.f3958a.r);
                }
                this.f3958a.i.signalAll();
                this.f3958a.f.unlock();
            }
        } finally {
            this.f3958a.f.unlock();
        }
    }
}
