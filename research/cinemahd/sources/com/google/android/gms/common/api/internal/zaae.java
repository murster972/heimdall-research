package com.google.android.gms.common.api.internal;

import android.app.Activity;
import androidx.collection.ArraySet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.Preconditions;

public class zaae extends zal {
    private final ArraySet<zai<?>> f = new ArraySet<>();
    private GoogleApiManager g;

    private zaae(LifecycleFragment lifecycleFragment) {
        super(lifecycleFragment);
        this.f3897a.a("ConnectionlessLifecycleHelper", (LifecycleCallback) this);
    }

    public static void a(Activity activity, GoogleApiManager googleApiManager, zai<?> zai) {
        LifecycleFragment a2 = LifecycleCallback.a(activity);
        zaae zaae = (zaae) a2.a("ConnectionlessLifecycleHelper", zaae.class);
        if (zaae == null) {
            zaae = new zaae(a2);
        }
        zaae.g = googleApiManager;
        Preconditions.a(zai, (Object) "ApiKey cannot be null");
        zaae.f.add(zai);
        googleApiManager.a(zaae);
    }

    private final void i() {
        if (!this.f.isEmpty()) {
            this.g.a(this);
        }
    }

    public void c() {
        super.c();
        i();
    }

    public void d() {
        super.d();
        i();
    }

    public void e() {
        super.e();
        this.g.b(this);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.g.b();
    }

    /* access modifiers changed from: package-private */
    public final ArraySet<zai<?>> h() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final void a(ConnectionResult connectionResult, int i) {
        this.g.a(connectionResult, i);
    }
}
