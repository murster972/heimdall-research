package com.google.android.gms.common.util;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import icepick.Icepick;

public final class CrashUtils {
    static {
        new String[]{Icepick.ANDROID_PREFIX, "com.android.", "dalvik.", Icepick.JAVA_PREFIX, "javax."};
    }

    public static boolean a(Context context, Throwable th) {
        return a(context, th, 536870912);
    }

    private static boolean a(Context context, Throwable th, int i) {
        try {
            Preconditions.a(context);
            Preconditions.a(th);
            return false;
        } catch (Exception e) {
            Log.e("CrashUtils", "Error adding exception to DropBox!", e);
            return false;
        }
    }
}
