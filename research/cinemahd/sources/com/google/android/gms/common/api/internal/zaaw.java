package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClientEventManager;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.ClientLibraryUtils;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.locks.Lock;

public final class zaaw extends GoogleApiClient implements zabt {
    private final Lock b;
    private boolean c;
    private final GmsClientEventManager d;
    private zabs e = null;
    private final int f;
    private final Context g;
    private final Looper h;
    final Queue<BaseImplementation$ApiMethodImpl<?, ?>> i = new LinkedList();
    private volatile boolean j;
    private long k;
    private long l;
    private final zabb m;
    private final GoogleApiAvailability n;
    private zabq o;
    final Map<Api.AnyClientKey<?>, Api.Client> p;
    Set<Scope> q;
    private final ClientSettings r;
    private final Map<Api<?>, Boolean> s;
    private final Api.AbstractClientBuilder<? extends zad, SignInOptions> t;
    private final ListenerHolders u;
    private final ArrayList<zaq> v;
    private Integer w;
    Set<zacm> x;
    final zacp y;
    private final GmsClientEventManager.GmsClientEventState z;

    public zaaw(Context context, Lock lock, Looper looper, ClientSettings clientSettings, GoogleApiAvailability googleApiAvailability, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder, Map<Api<?>, Boolean> map, List<GoogleApiClient.ConnectionCallbacks> list, List<GoogleApiClient.OnConnectionFailedListener> list2, Map<Api.AnyClientKey<?>, Api.Client> map2, int i2, int i3, ArrayList<zaq> arrayList, boolean z2) {
        Looper looper2 = looper;
        this.k = ClientLibraryUtils.a() ? 10000 : 120000;
        this.l = 5000;
        this.q = new HashSet();
        this.u = new ListenerHolders();
        this.w = null;
        this.x = null;
        this.z = new zaax(this);
        this.g = context;
        this.b = lock;
        this.c = false;
        this.d = new GmsClientEventManager(looper, this.z);
        this.h = looper2;
        this.m = new zabb(this, looper);
        this.n = googleApiAvailability;
        this.f = i2;
        if (this.f >= 0) {
            this.w = Integer.valueOf(i3);
        }
        this.s = map;
        this.p = map2;
        this.v = arrayList;
        this.y = new zacp(this.p);
        for (GoogleApiClient.ConnectionCallbacks a2 : list) {
            this.d.a(a2);
        }
        for (GoogleApiClient.OnConnectionFailedListener a3 : list2) {
            this.d.a(a3);
        }
        this.r = clientSettings;
        this.t = abstractClientBuilder;
    }

    private static String c(int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    /* access modifiers changed from: private */
    public final void j() {
        this.b.lock();
        try {
            if (this.j) {
                k();
            }
        } finally {
            this.b.unlock();
        }
    }

    private final void k() {
        this.d.b();
        this.e.connect();
    }

    /* access modifiers changed from: private */
    public final void l() {
        this.b.lock();
        try {
            if (g()) {
                k();
            }
        } finally {
            this.b.unlock();
        }
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t2) {
        Preconditions.a(t2.getClientKey() != null, (Object) "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.getClientKey());
        String b2 = t2.getApi() != null ? t2.getApi().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        Preconditions.a(containsKey, (Object) sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.j) {
                this.i.add(t2);
                while (!this.i.isEmpty()) {
                    BaseImplementation$ApiMethodImpl remove = this.i.remove();
                    this.y.a(remove);
                    remove.setFailedResult(Status.g);
                }
                return t2;
            } else {
                T a2 = this.e.a(t2);
                this.b.unlock();
                return a2;
            }
        } finally {
            this.b.unlock();
        }
    }

    public final void b() {
        this.b.lock();
        try {
            this.y.a();
            if (this.e != null) {
                this.e.disconnect();
            }
            this.u.a();
            for (BaseImplementation$ApiMethodImpl baseImplementation$ApiMethodImpl : this.i) {
                baseImplementation$ApiMethodImpl.zaa((zacs) null);
                baseImplementation$ApiMethodImpl.cancel();
            }
            this.i.clear();
            if (this.e != null) {
                g();
                this.d.a();
                this.b.unlock();
            }
        } finally {
            this.b.unlock();
        }
    }

    public final Context c() {
        return this.g;
    }

    public final Looper d() {
        return this.h;
    }

    public final boolean e() {
        zabs zabs = this.e;
        return zabs != null && zabs.isConnected();
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        if (!this.j) {
            return false;
        }
        this.j = false;
        this.m.removeMessages(2);
        this.m.removeMessages(1);
        zabq zabq = this.o;
        if (zabq != null) {
            zabq.a();
            this.o = null;
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final boolean h() {
        this.b.lock();
        try {
            if (this.x == null) {
                this.b.unlock();
                return false;
            }
            boolean z2 = !this.x.isEmpty();
            this.b.unlock();
            return z2;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final String i() {
        StringWriter stringWriter = new StringWriter();
        a("", (FileDescriptor) null, new PrintWriter(stringWriter), (String[]) null);
        return stringWriter.toString();
    }

    private final void b(int i2) {
        Integer num = this.w;
        if (num == null) {
            this.w = Integer.valueOf(i2);
        } else if (num.intValue() != i2) {
            String c2 = c(i2);
            String c3 = c(this.w.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 51 + String.valueOf(c3).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(c2);
            sb.append(". Mode was already set to ");
            sb.append(c3);
            throw new IllegalStateException(sb.toString());
        }
        if (this.e == null) {
            boolean z2 = false;
            boolean z3 = false;
            for (Api.Client next : this.p.values()) {
                if (next.requiresSignIn()) {
                    z2 = true;
                }
                if (next.providesSignIn()) {
                    z3 = true;
                }
            }
            int intValue = this.w.intValue();
            if (intValue != 1) {
                if (intValue == 2) {
                    if (z2) {
                        if (this.c) {
                            this.e = new zax(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, true);
                            return;
                        }
                        this.e = zas.a(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v);
                        return;
                    }
                }
            } else if (!z2) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            } else if (z3) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            if (!this.c || z3) {
                this.e = new zabe(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this);
                return;
            }
            this.e = new zax(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, false);
        }
    }

    public final <C extends Api.Client> C a(Api.AnyClientKey<C> anyClientKey) {
        C c2 = (Api.Client) this.p.get(anyClientKey);
        Preconditions.a(c2, (Object) "Appropriate Api was not requested.");
        return c2;
    }

    public final boolean a(Api<?> api) {
        Api.Client client;
        if (e() && (client = this.p.get(api.a())) != null && client.isConnected()) {
            return true;
        }
        return false;
    }

    public final void a() {
        this.b.lock();
        try {
            boolean z2 = false;
            if (this.f >= 0) {
                if (this.w != null) {
                    z2 = true;
                }
                Preconditions.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<Api.Client>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            a(this.w.intValue());
        } finally {
            this.b.unlock();
        }
    }

    public final void a(int i2) {
        this.b.lock();
        boolean z2 = true;
        if (!(i2 == 3 || i2 == 1 || i2 == 2)) {
            z2 = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i2);
            Preconditions.a(z2, (Object) sb.toString());
            b(i2);
            k();
        } finally {
            this.b.unlock();
        }
    }

    public final void b(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.d.b(onConnectionFailedListener);
    }

    public final void b(zacm zacm) {
        this.b.lock();
        try {
            if (this.x == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.x.remove(zacm)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!h()) {
                this.e.a();
            }
        } finally {
            this.b.unlock();
        }
    }

    public final void a(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.d.a(onConnectionFailedListener);
    }

    public final void a(Bundle bundle) {
        while (!this.i.isEmpty()) {
            a(this.i.remove());
        }
        this.d.a(bundle);
    }

    public final void a(ConnectionResult connectionResult) {
        if (!this.n.b(this.g, connectionResult.s())) {
            g();
        }
        if (!this.j) {
            this.d.a(connectionResult);
            this.d.a();
        }
    }

    public final void a(int i2, boolean z2) {
        if (i2 == 1 && !z2 && !this.j) {
            this.j = true;
            if (this.o == null && !ClientLibraryUtils.a()) {
                this.o = this.n.a(this.g.getApplicationContext(), (zabr) new zabc(this));
            }
            zabb zabb = this.m;
            zabb.sendMessageDelayed(zabb.obtainMessage(1), this.k);
            zabb zabb2 = this.m;
            zabb2.sendMessageDelayed(zabb2.obtainMessage(2), this.l);
        }
        this.y.b();
        this.d.a(i2);
        this.d.a();
        if (i2 == 2) {
            k();
        }
    }

    public final void a(zacm zacm) {
        this.b.lock();
        try {
            if (this.x == null) {
                this.x = new HashSet();
            }
            this.x.add(zacm);
        } finally {
            this.b.unlock();
        }
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("mContext=").println(this.g);
        printWriter.append(str).append("mResuming=").print(this.j);
        printWriter.append(" mWorkQueue.size()=").print(this.i.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.y.f3940a.size());
        zabs zabs = this.e;
        if (zabs != null) {
            zabs.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public static int a(Iterable<Api.Client> iterable, boolean z2) {
        boolean z3 = false;
        boolean z4 = false;
        for (Api.Client next : iterable) {
            if (next.requiresSignIn()) {
                z3 = true;
            }
            if (next.providesSignIn()) {
                z4 = true;
            }
        }
        if (!z3) {
            return 3;
        }
        if (!z4 || !z2) {
            return 1;
        }
        return 2;
    }
}
