package com.google.android.gms.common.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Objects {

    public static final class ToStringHelper {

        /* renamed from: a  reason: collision with root package name */
        private final List<String> f3990a;
        private final Object b;

        private ToStringHelper(Object obj) {
            Preconditions.a(obj);
            this.b = obj;
            this.f3990a = new ArrayList();
        }

        public final ToStringHelper a(String str, Object obj) {
            List<String> list = this.f3990a;
            Preconditions.a(str);
            String str2 = str;
            String valueOf = String.valueOf(obj);
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 1 + String.valueOf(valueOf).length());
            sb.append(str2);
            sb.append("=");
            sb.append(valueOf);
            list.add(sb.toString());
            return this;
        }

        public final String toString() {
            StringBuilder sb = new StringBuilder(100);
            sb.append(this.b.getClass().getSimpleName());
            sb.append('{');
            int size = this.f3990a.size();
            for (int i = 0; i < size; i++) {
                sb.append(this.f3990a.get(i));
                if (i < size - 1) {
                    sb.append(", ");
                }
            }
            sb.append('}');
            return sb.toString();
        }
    }

    private Objects() {
        throw new AssertionError("Uninstantiable");
    }

    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static int a(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static ToStringHelper a(Object obj) {
        return new ToStringHelper(obj);
    }
}
