package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;

@KeepName
public final class DataHolder extends AbstractSafeParcelable implements Closeable {
    public static final Parcelable.Creator<DataHolder> CREATOR = new zac();

    /* renamed from: a  reason: collision with root package name */
    private final int f3964a;
    private final String[] b;
    private Bundle c;
    private final CursorWindow[] d;
    private final int e;
    private final Bundle f;
    private int[] g;
    private boolean h = false;
    private boolean i = true;

    public static class zaa extends RuntimeException {
    }

    static {
        new zab(new String[0], (String) null);
    }

    DataHolder(int i2, String[] strArr, CursorWindow[] cursorWindowArr, int i3, Bundle bundle) {
        this.f3964a = i2;
        this.b = strArr;
        this.d = cursorWindowArr;
        this.e = i3;
        this.f = bundle;
    }

    public final void close() {
        synchronized (this) {
            if (!this.h) {
                this.h = true;
                for (CursorWindow close : this.d) {
                    close.close();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        try {
            if (this.i && this.d.length > 0 && !isClosed()) {
                close();
                String obj = toString();
                StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 178);
                sb.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                sb.append(obj);
                sb.append(")");
                Log.e("DataBuffer", sb.toString());
            }
        } finally {
            super.finalize();
        }
    }

    public final boolean isClosed() {
        boolean z;
        synchronized (this) {
            z = this.h;
        }
        return z;
    }

    public final Bundle s() {
        return this.f;
    }

    public final int t() {
        return this.e;
    }

    public final void u() {
        this.c = new Bundle();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            String[] strArr = this.b;
            if (i3 >= strArr.length) {
                break;
            }
            this.c.putInt(strArr[i3], i3);
            i3++;
        }
        this.g = new int[this.d.length];
        int i4 = 0;
        while (true) {
            CursorWindow[] cursorWindowArr = this.d;
            if (i2 < cursorWindowArr.length) {
                this.g[i2] = i4;
                i4 += this.d[i2].getNumRows() - (i4 - cursorWindowArr[i2].getStartPosition());
                i2++;
            } else {
                return;
            }
        }
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.b, false);
        SafeParcelWriter.a(parcel, 2, (T[]) this.d, i2, false);
        SafeParcelWriter.a(parcel, 3, t());
        SafeParcelWriter.a(parcel, 4, s(), false);
        SafeParcelWriter.a(parcel, 1000, this.f3964a);
        SafeParcelWriter.a(parcel, a2);
        if ((i2 & 1) != 0) {
            close();
        }
    }

    public static class Builder {
        private Builder(String[] strArr, String str) {
            Preconditions.a(strArr);
            String[] strArr2 = strArr;
            new ArrayList();
            new HashMap();
        }

        /* synthetic */ Builder(String[] strArr, String str, zab zab) {
            this(strArr, (String) null);
        }
    }
}
