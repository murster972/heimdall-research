package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.util.Log;
import androidx.collection.LruCache;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.internal.base.zak;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

public final class ImageManager {
    /* access modifiers changed from: private */
    public static final Object i = new Object();
    /* access modifiers changed from: private */
    public static HashSet<Uri> j = new HashSet<>();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Context f3965a;
    /* access modifiers changed from: private */
    public final Handler b;
    /* access modifiers changed from: private */
    public final ExecutorService c;
    /* access modifiers changed from: private */
    public final zaa d;
    /* access modifiers changed from: private */
    public final zak e;
    /* access modifiers changed from: private */
    public final Map<zaa, ImageReceiver> f;
    /* access modifiers changed from: private */
    public final Map<Uri, ImageReceiver> g;
    /* access modifiers changed from: private */
    public final Map<Uri, Long> h;

    @KeepName
    private final class ImageReceiver extends ResultReceiver {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f3966a;
        /* access modifiers changed from: private */
        public final ArrayList<zaa> b;
        private final /* synthetic */ ImageManager c;

        public final void onReceiveResult(int i, Bundle bundle) {
            this.c.c.execute(new zab(this.f3966a, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }
    }

    private static final class zaa extends LruCache<zab, Bitmap> {
    }

    private final class zab implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f3967a;
        private final ParcelFileDescriptor b;

        public zab(Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.f3967a = uri;
            this.b = parcelFileDescriptor;
        }

        public final void run() {
            boolean z;
            Bitmap bitmap;
            Asserts.b("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            ParcelFileDescriptor parcelFileDescriptor = this.b;
            boolean z2 = false;
            Bitmap bitmap2 = null;
            if (parcelFileDescriptor != null) {
                try {
                    bitmap2 = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                } catch (OutOfMemoryError e) {
                    String valueOf = String.valueOf(this.f3967a);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                    sb.append("OOM while loading bitmap for uri: ");
                    sb.append(valueOf);
                    Log.e("ImageManager", sb.toString(), e);
                    z2 = true;
                }
                try {
                    this.b.close();
                } catch (IOException e2) {
                    Log.e("ImageManager", "closed failed", e2);
                }
                z = z2;
                bitmap = bitmap2;
            } else {
                bitmap = null;
                z = false;
            }
            CountDownLatch countDownLatch = new CountDownLatch(1);
            ImageManager.this.b.post(new zad(this.f3967a, bitmap, z, countDownLatch));
            try {
                countDownLatch.await();
            } catch (InterruptedException unused) {
                String valueOf2 = String.valueOf(this.f3967a);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 32);
                sb2.append("Latch interrupted while posting ");
                sb2.append(valueOf2);
                Log.w("ImageManager", sb2.toString());
            }
        }
    }

    private final class zad implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f3968a;
        private final Bitmap b;
        private final CountDownLatch c;
        private boolean d;

        public zad(Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.f3968a = uri;
            this.b = bitmap;
            this.d = z;
            this.c = countDownLatch;
        }

        public final void run() {
            Asserts.a("OnBitmapLoadedRunnable must be executed in the main thread");
            boolean z = this.b != null;
            if (ImageManager.this.d != null) {
                if (this.d) {
                    ImageManager.this.d.evictAll();
                    System.gc();
                    this.d = false;
                    ImageManager.this.b.post(this);
                    return;
                } else if (z) {
                    ImageManager.this.d.put(new zab(this.f3968a), this.b);
                }
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.g.remove(this.f3968a);
            if (imageReceiver != null) {
                ArrayList a2 = imageReceiver.b;
                int size = a2.size();
                for (int i = 0; i < size; i++) {
                    zaa zaa = (zaa) a2.get(i);
                    if (z) {
                        zaa.a(ImageManager.this.f3965a, this.b, false);
                    } else {
                        ImageManager.this.h.put(this.f3968a, Long.valueOf(SystemClock.elapsedRealtime()));
                        zaa.a(ImageManager.this.f3965a, ImageManager.this.e, false);
                    }
                    ImageManager.this.f.remove(zaa);
                }
            }
            this.c.countDown();
            synchronized (ImageManager.i) {
                ImageManager.j.remove(this.f3968a);
            }
        }
    }
}
