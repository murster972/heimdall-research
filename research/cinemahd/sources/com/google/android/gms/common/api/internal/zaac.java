package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

final class zaac implements PendingResult.StatusListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BasePendingResult f3904a;
    private final /* synthetic */ zaab b;

    zaac(zaab zaab, BasePendingResult basePendingResult) {
        this.b = zaab;
        this.f3904a = basePendingResult;
    }

    public final void a(Status status) {
        this.b.f3903a.remove(this.f3904a);
    }
}
