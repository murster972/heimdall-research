package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zac;
import com.google.android.gms.signin.internal.zaj;
import java.lang.ref.WeakReference;

final class zaar extends zac {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<zaak> f3911a;

    zaar(zaak zaak) {
        this.f3911a = new WeakReference<>(zaak);
    }

    public final void a(zaj zaj) {
        zaak zaak = (zaak) this.f3911a.get();
        if (zaak != null) {
            zaak.f3908a.a((zabf) new zaas(this, zaak, zaak, zaj));
        }
    }
}
