package com.google.android.gms.common.images;

import android.net.Uri;
import com.google.android.gms.common.internal.Objects;

final class zab {

    /* renamed from: a  reason: collision with root package name */
    public final Uri f3971a;

    public zab(Uri uri) {
        this.f3971a = uri;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zab)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return Objects.a(((zab) obj).f3971a, this.f3971a);
    }

    public final int hashCode() {
        return Objects.a(this.f3971a);
    }
}
