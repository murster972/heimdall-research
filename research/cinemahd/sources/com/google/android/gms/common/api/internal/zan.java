package com.google.android.gms.common.api.internal;

import android.app.Dialog;
import android.content.DialogInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;

final class zan implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final zam f3950a;
    final /* synthetic */ zal b;

    zan(zal zal, zam zam) {
        this.b = zal;
        this.f3950a = zam;
    }

    public final void run() {
        if (this.b.b) {
            ConnectionResult a2 = this.f3950a.a();
            if (a2.v()) {
                zal zal = this.b;
                zal.f3897a.startActivityForResult(GoogleApiActivity.a(zal.a(), a2.u(), this.f3950a.b(), false), 1);
            } else if (this.b.e.c(a2.s())) {
                zal zal2 = this.b;
                zal2.e.a(zal2.a(), this.b.f3897a, a2.s(), 2, this.b);
            } else if (a2.s() == 18) {
                Dialog a3 = GoogleApiAvailability.a(this.b.a(), (DialogInterface.OnCancelListener) this.b);
                zal zal3 = this.b;
                zal3.e.a(zal3.a().getApplicationContext(), (zabr) new zao(this, a3));
            } else {
                this.b.a(a2, this.f3950a.b());
            }
        }
    }
}
