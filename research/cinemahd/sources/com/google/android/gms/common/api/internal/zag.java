package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zag<ResultT> extends zac {

    /* renamed from: a  reason: collision with root package name */
    private final TaskApiCall<Api.AnyClient, ResultT> f3945a;
    private final TaskCompletionSource<ResultT> b;
    private final StatusExceptionMapper c;

    public zag(int i, TaskApiCall<Api.AnyClient, ResultT> taskApiCall, TaskCompletionSource<ResultT> taskCompletionSource, StatusExceptionMapper statusExceptionMapper) {
        super(i);
        this.b = taskCompletionSource;
        this.f3945a = taskApiCall;
        this.c = statusExceptionMapper;
    }

    public final void a(GoogleApiManager.zaa<?> zaa) throws DeadObjectException {
        try {
            this.f3945a.doExecute(zaa.f(), this.b);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            a(zab.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }

    public final Feature[] b(GoogleApiManager.zaa<?> zaa) {
        return this.f3945a.zabt();
    }

    public final boolean c(GoogleApiManager.zaa<?> zaa) {
        return this.f3945a.shouldAutoResolveMissingFeatures();
    }

    public final void a(Status status) {
        this.b.b(this.c.a(status));
    }

    public final void a(RuntimeException runtimeException) {
        this.b.b((Exception) runtimeException);
    }

    public final void a(zaab zaab, boolean z) {
        zaab.a(this.b, z);
    }
}
