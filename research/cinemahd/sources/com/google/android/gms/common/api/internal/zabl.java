package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.GoogleApiManager;

final class zabl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ConnectionResult f3925a;
    private final /* synthetic */ GoogleApiManager.zaa b;

    zabl(GoogleApiManager.zaa zaa, ConnectionResult connectionResult) {
        this.b = zaa;
        this.f3925a = connectionResult;
    }

    public final void run() {
        this.b.onConnectionFailed(this.f3925a);
    }
}
