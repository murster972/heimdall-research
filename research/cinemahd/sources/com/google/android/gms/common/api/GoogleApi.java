package com.google.android.gms.common.api;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolders;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.common.api.internal.RegistrationMethods;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.common.api.internal.UnregisterListenerMethod;
import com.google.android.gms.common.api.internal.zaae;
import com.google.android.gms.common.api.internal.zabp;
import com.google.android.gms.common.api.internal.zace;
import com.google.android.gms.common.api.internal.zai;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class GoogleApi<O extends Api.ApiOptions> {
    private final Api<O> mApi;
    private final Context mContext;
    private final int mId;
    private final O zabh;
    private final zai<O> zabi;
    private final Looper zabj;
    private final GoogleApiClient zabk;
    private final StatusExceptionMapper zabl;
    protected final GoogleApiManager zabm;

    public static class Settings {
        public static final Settings c = new Builder().a();

        /* renamed from: a  reason: collision with root package name */
        public final StatusExceptionMapper f3882a;
        public final Looper b;

        private Settings(StatusExceptionMapper statusExceptionMapper, Account account, Looper looper) {
            this.f3882a = statusExceptionMapper;
            this.b = looper;
        }

        public static class Builder {

            /* renamed from: a  reason: collision with root package name */
            private StatusExceptionMapper f3883a;
            private Looper b;

            public Builder a(StatusExceptionMapper statusExceptionMapper) {
                Preconditions.a(statusExceptionMapper, (Object) "StatusExceptionMapper must not be null.");
                this.f3883a = statusExceptionMapper;
                return this;
            }

            public Builder a(Looper looper) {
                Preconditions.a(looper, (Object) "Looper must not be null.");
                this.b = looper;
                return this;
            }

            public Settings a() {
                if (this.f3883a == null) {
                    this.f3883a = new ApiExceptionMapper();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new Settings(this.f3883a, this.b);
            }
        }
    }

    protected GoogleApi(Context context, Api<O> api, Looper looper) {
        Preconditions.a(context, (Object) "Null context is not permitted.");
        Preconditions.a(api, (Object) "Api must not be null.");
        Preconditions.a(looper, (Object) "Looper must not be null.");
        this.mContext = context.getApplicationContext();
        this.mApi = api;
        this.zabh = null;
        this.zabj = looper;
        this.zabi = zai.a(api);
        this.zabk = new zabp(this);
        this.zabm = GoogleApiManager.a(this.mContext);
        this.mId = this.zabm.a();
        this.zabl = new ApiExceptionMapper();
    }

    private final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T zaa(int i, T t) {
        t.zau();
        this.zabm.a(this, i, (BaseImplementation$ApiMethodImpl<? extends Result, Api.AnyClient>) t);
        return t;
    }

    public GoogleApiClient asGoogleApiClient() {
        return this.zabk;
    }

    /* access modifiers changed from: protected */
    public ClientSettings.Builder createClientSettingsBuilder() {
        Account account;
        Set<Scope> set;
        GoogleSignInAccount a2;
        GoogleSignInAccount a3;
        ClientSettings.Builder builder = new ClientSettings.Builder();
        O o = this.zabh;
        if (!(o instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) || (a3 = ((Api.ApiOptions.HasGoogleSignInAccountOptions) o).a()) == null) {
            O o2 = this.zabh;
            account = o2 instanceof Api.ApiOptions.HasAccountOptions ? ((Api.ApiOptions.HasAccountOptions) o2).c() : null;
        } else {
            account = a3.s();
        }
        ClientSettings.Builder a4 = builder.a(account);
        O o3 = this.zabh;
        if (!(o3 instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) || (a2 = ((Api.ApiOptions.HasGoogleSignInAccountOptions) o3).a()) == null) {
            set = Collections.emptySet();
        } else {
            set = a2.A();
        }
        return a4.a((Collection<Scope>) set).a(this.mContext.getClass().getName()).b(this.mContext.getPackageName());
    }

    /* access modifiers changed from: protected */
    public Task<Boolean> disconnectService() {
        return this.zabm.b((GoogleApi<?>) this);
    }

    public <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T doBestEffortWrite(T t) {
        return zaa(2, t);
    }

    public <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T doRead(T t) {
        return zaa(0, t);
    }

    @Deprecated
    public <A extends Api.AnyClient, T extends RegisterListenerMethod<A, ?>, U extends UnregisterListenerMethod<A, ?>> Task<Void> doRegisterEventListener(T t, U u) {
        Preconditions.a(t);
        Preconditions.a(u);
        Preconditions.a(t.b(), (Object) "Listener has already been released.");
        Preconditions.a(u.a(), (Object) "Listener has already been released.");
        Preconditions.a(t.b().equals(u.a()), (Object) "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.zabm.a(this, (RegisterListenerMethod<Api.AnyClient, ?>) t, (UnregisterListenerMethod<Api.AnyClient, ?>) u);
    }

    public Task<Boolean> doUnregisterEventListener(ListenerHolder.ListenerKey<?> listenerKey) {
        Preconditions.a(listenerKey, (Object) "Listener key cannot be null.");
        return this.zabm.a(this, listenerKey);
    }

    public <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T doWrite(T t) {
        return zaa(1, t);
    }

    public final Api<O> getApi() {
        return this.mApi;
    }

    public O getApiOptions() {
        return this.zabh;
    }

    public Context getApplicationContext() {
        return this.mContext;
    }

    public final int getInstanceId() {
        return this.mId;
    }

    public Looper getLooper() {
        return this.zabj;
    }

    public <L> ListenerHolder<L> registerListener(L l, String str) {
        return ListenerHolders.a(l, this.zabj, str);
    }

    public final zai<O> zak() {
        return this.zabi;
    }

    public <TResult, A extends Api.AnyClient> Task<TResult> doBestEffortWrite(TaskApiCall<A, TResult> taskApiCall) {
        return zaa(2, taskApiCall);
    }

    public <TResult, A extends Api.AnyClient> Task<TResult> doRead(TaskApiCall<A, TResult> taskApiCall) {
        return zaa(0, taskApiCall);
    }

    public <TResult, A extends Api.AnyClient> Task<TResult> doWrite(TaskApiCall<A, TResult> taskApiCall) {
        return zaa(1, taskApiCall);
    }

    private final <TResult, A extends Api.AnyClient> Task<TResult> zaa(int i, TaskApiCall<A, TResult> taskApiCall) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.zabm.a(this, i, taskApiCall, taskCompletionSource, this.zabl);
        return taskCompletionSource.a();
    }

    public Api.Client zaa(Looper looper, GoogleApiManager.zaa<O> zaa) {
        return this.mApi.d().buildClient(this.mContext, looper, createClientSettingsBuilder().a(), this.zabh, zaa, zaa);
    }

    public zace zaa(Context context, Handler handler) {
        return new zace(context, handler, createClientSettingsBuilder().a());
    }

    public <A extends Api.AnyClient> Task<Void> doRegisterEventListener(RegistrationMethods<A, ?> registrationMethods) {
        Preconditions.a(registrationMethods);
        Preconditions.a(registrationMethods.f3902a.b(), (Object) "Listener has already been released.");
        Preconditions.a(registrationMethods.b.a(), (Object) "Listener has already been released.");
        return this.zabm.a(this, (RegisterListenerMethod<Api.AnyClient, ?>) registrationMethods.f3902a, (UnregisterListenerMethod<Api.AnyClient, ?>) registrationMethods.b);
    }

    @Deprecated
    public GoogleApi(Context context, Api<O> api, O o, Looper looper, StatusExceptionMapper statusExceptionMapper) {
        this(context, api, o, new Settings.Builder().a(looper).a(statusExceptionMapper).a());
    }

    public GoogleApi(Activity activity, Api<O> api, O o, Settings settings) {
        Preconditions.a(activity, (Object) "Null activity is not permitted.");
        Preconditions.a(api, (Object) "Api must not be null.");
        Preconditions.a(settings, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.mContext = activity.getApplicationContext();
        this.mApi = api;
        this.zabh = o;
        this.zabj = settings.b;
        this.zabi = zai.a(this.mApi, this.zabh);
        this.zabk = new zabp(this);
        this.zabm = GoogleApiManager.a(this.mContext);
        this.mId = this.zabm.a();
        this.zabl = settings.f3882a;
        if (!(activity instanceof GoogleApiActivity)) {
            zaae.a(activity, this.zabm, this.zabi);
        }
        this.zabm.a((GoogleApi<?>) this);
    }

    public GoogleApi(Context context, Api<O> api, O o, Settings settings) {
        Preconditions.a(context, (Object) "Null context is not permitted.");
        Preconditions.a(api, (Object) "Api must not be null.");
        Preconditions.a(settings, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.mContext = context.getApplicationContext();
        this.mApi = api;
        this.zabh = o;
        this.zabj = settings.b;
        this.zabi = zai.a(this.mApi, this.zabh);
        this.zabk = new zabp(this);
        this.zabm = GoogleApiManager.a(this.mContext);
        this.mId = this.zabm.a();
        this.zabl = settings.f3882a;
        this.zabm.a((GoogleApi<?>) this);
    }

    @Deprecated
    public GoogleApi(Activity activity, Api<O> api, O o, StatusExceptionMapper statusExceptionMapper) {
        this(activity, api, o, new Settings.Builder().a(statusExceptionMapper).a(activity.getMainLooper()).a());
    }

    @Deprecated
    public GoogleApi(Context context, Api<O> api, O o, StatusExceptionMapper statusExceptionMapper) {
        this(context, api, o, new Settings.Builder().a(statusExceptionMapper).a());
    }
}
