package com.google.android.gms.common.api.internal;

import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Map;
import java.util.Set;

public final class zak {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayMap<zai<?>, ConnectionResult> f3948a = new ArrayMap<>();
    private final ArrayMap<zai<?>, String> b = new ArrayMap<>();
    private final TaskCompletionSource<Map<zai<?>, String>> c = new TaskCompletionSource<>();
    private int d;
    private boolean e = false;

    public zak(Iterable<? extends GoogleApi<?>> iterable) {
        for (GoogleApi zak : iterable) {
            this.f3948a.put(zak.zak(), null);
        }
        this.d = this.f3948a.keySet().size();
    }

    public final Task<Map<zai<?>, String>> a() {
        return this.c.a();
    }

    public final Set<zai<?>> b() {
        return this.f3948a.keySet();
    }

    public final void a(zai<?> zai, ConnectionResult connectionResult, String str) {
        this.f3948a.put(zai, connectionResult);
        this.b.put(zai, str);
        this.d--;
        if (!connectionResult.w()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.c.a((Exception) new AvailabilityException(this.f3948a));
            return;
        }
        this.c.a(this.b);
    }
}
