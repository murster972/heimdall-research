package com.google.android.gms.common;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.base.R$drawable;
import com.google.android.gms.base.R$string;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.LifecycleFragment;
import com.google.android.gms.common.api.internal.zabq;
import com.google.android.gms.common.api.internal.zabr;
import com.google.android.gms.common.internal.ConnectionErrorMessages;
import com.google.android.gms.common.internal.DialogRedirect;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.DeviceProperties;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.base.zal;

public class GoogleApiAvailability extends GoogleApiAvailabilityLight {
    private static final Object d = new Object();
    private static final GoogleApiAvailability e = new GoogleApiAvailability();
    private String c;

    @SuppressLint({"HandlerLeak"})
    private class zaa extends zal {

        /* renamed from: a  reason: collision with root package name */
        private final Context f3876a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public zaa(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.f3876a = context.getApplicationContext();
        }

        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                Log.w("GoogleApiAvailability", sb.toString());
                return;
            }
            int c = GoogleApiAvailability.this.c(this.f3876a);
            if (GoogleApiAvailability.this.c(c)) {
                GoogleApiAvailability.this.d(this.f3876a, c);
            }
        }
    }

    GoogleApiAvailability() {
    }

    public static GoogleApiAvailability a() {
        return e;
    }

    public boolean b(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, i2, onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    public int c(Context context) {
        return super.c(context);
    }

    public void d(Context context, int i) {
        a(context, i, (String) null, a(context, i, 0, "n"));
    }

    public Dialog a(Activity activity, int i, int i2) {
        return a(activity, i, i2, (DialogInterface.OnCancelListener) null);
    }

    public final boolean c(int i) {
        return super.c(i);
    }

    private final String b() {
        String str;
        synchronized (d) {
            str = this.c;
        }
        return str;
    }

    public Dialog a(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return a((Context) activity, i, DialogRedirect.a(activity, a((Context) activity, i, "d"), i2), onCancelListener);
    }

    /* access modifiers changed from: package-private */
    public final void d(Context context) {
        new zaa(context).sendEmptyMessageDelayed(1, 120000);
    }

    public final boolean a(Activity activity, LifecycleFragment lifecycleFragment, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a((Context) activity, i, DialogRedirect.a(lifecycleFragment, a((Context) activity, i, "d"), 2), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    public final String b(int i) {
        return super.b(i);
    }

    public final boolean a(Context context, ConnectionResult connectionResult, int i) {
        PendingIntent a2 = a(context, connectionResult);
        if (a2 == null) {
            return false;
        }
        a(context, connectionResult.s(), (String) null, GoogleApiActivity.a(context, a2, i));
        return true;
    }

    public static Dialog a(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, (AttributeSet) null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(ConnectionErrorMessages.b(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        a(activity, (Dialog) create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    public final zabq a(Context context, zabr zabr) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        zabq zabq = new zabq(zabr);
        context.registerReceiver(zabq, intentFilter);
        zabq.a(context);
        if (a(context, "com.google.android.gms")) {
            return zabq;
        }
        zabr.a();
        zabq.a();
        return null;
    }

    public int a(Context context, int i) {
        return super.a(context, i);
    }

    public Intent a(Context context, int i, String str) {
        return super.a(context, i, str);
    }

    public PendingIntent a(Context context, int i, int i2) {
        return super.a(context, i, i2);
    }

    public PendingIntent a(Context context, ConnectionResult connectionResult) {
        if (connectionResult.v()) {
            return connectionResult.u();
        }
        return a(context, connectionResult.s(), 0);
    }

    static Dialog a(Context context, int i, DialogRedirect dialogRedirect, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(ConnectionErrorMessages.b(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String a2 = ConnectionErrorMessages.a(context, i);
        if (a2 != null) {
            builder.setPositiveButton(a2, dialogRedirect);
        }
        String e2 = ConnectionErrorMessages.e(context, i);
        if (e2 != null) {
            builder.setTitle(e2);
        }
        return builder.create();
    }

    static void a(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            SupportErrorDialogFragment.a(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        ErrorDialogFragment.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @TargetApi(20)
    private final void a(Context context, int i, String str, PendingIntent pendingIntent) {
        int i2;
        if (i == 18) {
            d(context);
        } else if (pendingIntent != null) {
            String d2 = ConnectionErrorMessages.d(context, i);
            String c2 = ConnectionErrorMessages.c(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.b(true);
            builder.a(true);
            builder.b((CharSequence) d2);
            builder.a((NotificationCompat.Style) new NotificationCompat.BigTextStyle().a((CharSequence) c2));
            if (DeviceProperties.c(context)) {
                Preconditions.b(PlatformVersion.g());
                builder.b(context.getApplicationInfo().icon);
                builder.a(2);
                if (DeviceProperties.d(context)) {
                    builder.a(R$drawable.common_full_open_on_phone, resources.getString(R$string.common_open_on_phone), pendingIntent);
                } else {
                    builder.a(pendingIntent);
                }
            } else {
                builder.b(17301642);
                builder.c((CharSequence) resources.getString(R$string.common_google_play_services_notification_ticker));
                builder.a(System.currentTimeMillis());
                builder.a(pendingIntent);
                builder.a((CharSequence) c2);
            }
            if (PlatformVersion.k()) {
                Preconditions.b(PlatformVersion.k());
                String b = b();
                if (b == null) {
                    b = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel(b);
                    String b2 = ConnectionErrorMessages.b(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel(b, b2, 4));
                    } else if (!b2.equals(notificationChannel.getName())) {
                        notificationChannel.setName(b2);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                builder.b(b);
            }
            Notification a2 = builder.a();
            if (i == 1 || i == 2 || i == 3) {
                i2 = 10436;
                GooglePlayServicesUtilLight.sCanceledAvailabilityNotification.set(false);
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, a2);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
