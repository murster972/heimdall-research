package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import java.util.Collections;

public final class zaav implements zabd {

    /* renamed from: a  reason: collision with root package name */
    private final zabe f3914a;

    public zaav(zabe zabe) {
        this.f3914a = zabe;
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    public final void a(ConnectionResult connectionResult, Api<?> api, boolean z) {
    }

    public final void begin() {
        for (Api.Client disconnect : this.f3914a.f.values()) {
            disconnect.disconnect();
        }
        this.f3914a.m.q = Collections.emptySet();
    }

    public final void connect() {
        this.f3914a.b();
    }

    public final boolean disconnect() {
        return true;
    }

    public final void onConnected(Bundle bundle) {
    }

    public final void onConnectionSuspended(int i) {
    }
}
