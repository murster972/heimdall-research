package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zag implements BaseGmsClient.BaseOnConnectionFailedListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ GoogleApiClient.OnConnectionFailedListener f4001a;

    zag(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.f4001a = onConnectionFailedListener;
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.f4001a.onConnectionFailed(connectionResult);
    }
}
