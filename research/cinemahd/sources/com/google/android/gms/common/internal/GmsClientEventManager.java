package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.base.zal;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public final class GmsClientEventManager implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final GmsClientEventState f3985a;
    private final ArrayList<GoogleApiClient.ConnectionCallbacks> b = new ArrayList<>();
    private final ArrayList<GoogleApiClient.ConnectionCallbacks> c = new ArrayList<>();
    private final ArrayList<GoogleApiClient.OnConnectionFailedListener> d = new ArrayList<>();
    private volatile boolean e = false;
    private final AtomicInteger f = new AtomicInteger(0);
    private boolean g = false;
    private final Handler h;
    private final Object i = new Object();

    public interface GmsClientEventState {
        Bundle getConnectionHint();

        boolean isConnected();
    }

    public GmsClientEventManager(Looper looper, GmsClientEventState gmsClientEventState) {
        this.f3985a = gmsClientEventState;
        this.h = new zal(looper, this);
    }

    public final void a() {
        this.e = false;
        this.f.incrementAndGet();
    }

    public final void b() {
        this.e = true;
    }

    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 1) {
            GoogleApiClient.ConnectionCallbacks connectionCallbacks = (GoogleApiClient.ConnectionCallbacks) message.obj;
            synchronized (this.i) {
                if (this.e && this.f3985a.isConnected() && this.b.contains(connectionCallbacks)) {
                    connectionCallbacks.onConnected(this.f3985a.getConnectionHint());
                }
            }
            return true;
        }
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i2);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }

    public final void b(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Preconditions.a(onConnectionFailedListener);
        synchronized (this.i) {
            if (!this.d.remove(onConnectionFailedListener)) {
                String valueOf = String.valueOf(onConnectionFailedListener);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
        }
    }

    public final void a(Bundle bundle) {
        boolean z = true;
        Preconditions.b(Looper.myLooper() == this.h.getLooper(), "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.i) {
            Preconditions.b(!this.g);
            this.h.removeMessages(1);
            this.g = true;
            if (this.c.size() != 0) {
                z = false;
            }
            Preconditions.b(z);
            ArrayList arrayList = new ArrayList(this.b);
            int i2 = this.f.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                GoogleApiClient.ConnectionCallbacks connectionCallbacks = (GoogleApiClient.ConnectionCallbacks) obj;
                if (!this.e || !this.f3985a.isConnected() || this.f.get() != i2) {
                    break;
                } else if (!this.c.contains(connectionCallbacks)) {
                    connectionCallbacks.onConnected(bundle);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    public final void a(int i2) {
        Preconditions.b(Looper.myLooper() == this.h.getLooper(), "onUnintentionalDisconnection must only be called on the Handler thread");
        this.h.removeMessages(1);
        synchronized (this.i) {
            this.g = true;
            ArrayList arrayList = new ArrayList(this.b);
            int i3 = this.f.get();
            int size = arrayList.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList.get(i4);
                i4++;
                GoogleApiClient.ConnectionCallbacks connectionCallbacks = (GoogleApiClient.ConnectionCallbacks) obj;
                if (!this.e || this.f.get() != i3) {
                    break;
                } else if (this.b.contains(connectionCallbacks)) {
                    connectionCallbacks.onConnectionSuspended(i2);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0053, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.google.android.gms.common.ConnectionResult r8) {
        /*
            r7 = this;
            android.os.Looper r0 = android.os.Looper.myLooper()
            android.os.Handler r1 = r7.h
            android.os.Looper r1 = r1.getLooper()
            r2 = 0
            r3 = 1
            if (r0 != r1) goto L_0x0010
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            java.lang.String r1 = "onConnectionFailure must only be called on the Handler thread"
            com.google.android.gms.common.internal.Preconditions.b(r0, r1)
            android.os.Handler r0 = r7.h
            r0.removeMessages(r3)
            java.lang.Object r0 = r7.i
            monitor-enter(r0)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0056 }
            java.util.ArrayList<com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener> r3 = r7.d     // Catch:{ all -> 0x0056 }
            r1.<init>(r3)     // Catch:{ all -> 0x0056 }
            java.util.concurrent.atomic.AtomicInteger r3 = r7.f     // Catch:{ all -> 0x0056 }
            int r3 = r3.get()     // Catch:{ all -> 0x0056 }
            int r4 = r1.size()     // Catch:{ all -> 0x0056 }
        L_0x002f:
            if (r2 >= r4) goto L_0x0054
            java.lang.Object r5 = r1.get(r2)     // Catch:{ all -> 0x0056 }
            int r2 = r2 + 1
            com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener r5 = (com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener) r5     // Catch:{ all -> 0x0056 }
            boolean r6 = r7.e     // Catch:{ all -> 0x0056 }
            if (r6 == 0) goto L_0x0052
            java.util.concurrent.atomic.AtomicInteger r6 = r7.f     // Catch:{ all -> 0x0056 }
            int r6 = r6.get()     // Catch:{ all -> 0x0056 }
            if (r6 == r3) goto L_0x0046
            goto L_0x0052
        L_0x0046:
            java.util.ArrayList<com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener> r6 = r7.d     // Catch:{ all -> 0x0056 }
            boolean r6 = r6.contains(r5)     // Catch:{ all -> 0x0056 }
            if (r6 == 0) goto L_0x002f
            r5.onConnectionFailed(r8)     // Catch:{ all -> 0x0056 }
            goto L_0x002f
        L_0x0052:
            monitor-exit(r0)     // Catch:{ all -> 0x0056 }
            return
        L_0x0054:
            monitor-exit(r0)     // Catch:{ all -> 0x0056 }
            return
        L_0x0056:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0056 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.GmsClientEventManager.a(com.google.android.gms.common.ConnectionResult):void");
    }

    public final void a(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        Preconditions.a(connectionCallbacks);
        synchronized (this.i) {
            if (this.b.contains(connectionCallbacks)) {
                String valueOf = String.valueOf(connectionCallbacks);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 62);
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.b.add(connectionCallbacks);
            }
        }
        if (this.f3985a.isConnected()) {
            Handler handler = this.h;
            handler.sendMessage(handler.obtainMessage(1, connectionCallbacks));
        }
    }

    public final void a(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Preconditions.a(onConnectionFailedListener);
        synchronized (this.i) {
            if (this.d.contains(onConnectionFailedListener)) {
                String valueOf = String.valueOf(onConnectionFailedListener);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.d.add(onConnectionFailedListener);
            }
        }
    }
}
