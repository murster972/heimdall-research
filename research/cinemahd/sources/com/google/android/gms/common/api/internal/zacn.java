package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;

final class zacn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Result f3938a;
    private final /* synthetic */ zacm b;

    zacn(zacm zacm, Result result) {
        this.b = zacm;
        this.f3938a = result;
    }

    public final void run() {
        try {
            BasePendingResult.zadm.set(true);
            this.b.h.sendMessage(this.b.h.obtainMessage(0, this.b.f3937a.a(this.f3938a)));
            BasePendingResult.zadm.set(false);
            zacm.a(this.f3938a);
            GoogleApiClient googleApiClient = (GoogleApiClient) this.b.g.get();
            if (googleApiClient != null) {
                googleApiClient.b(this.b);
            }
        } catch (RuntimeException e) {
            this.b.h.sendMessage(this.b.h.obtainMessage(1, e));
            BasePendingResult.zadm.set(false);
            zacm.a(this.f3938a);
            GoogleApiClient googleApiClient2 = (GoogleApiClient) this.b.g.get();
            if (googleApiClient2 != null) {
                googleApiClient2.b(this.b);
            }
        } catch (Throwable th) {
            BasePendingResult.zadm.set(false);
            zacm.a(this.f3938a);
            GoogleApiClient googleApiClient3 = (GoogleApiClient) this.b.g.get();
            if (googleApiClient3 != null) {
                googleApiClient3.b(this.b);
            }
            throw th;
        }
    }
}
