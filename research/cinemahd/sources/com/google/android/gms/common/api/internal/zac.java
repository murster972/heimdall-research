package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.GoogleApiManager;

public abstract class zac extends zab {
    public zac(int i) {
        super(i);
    }

    public abstract Feature[] b(GoogleApiManager.zaa<?> zaa);

    public abstract boolean c(GoogleApiManager.zaa<?> zaa);
}
