package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.Preconditions;
import java.lang.ref.WeakReference;

final class zaam implements BaseGmsClient.ConnectionProgressReportCallbacks {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<zaak> f3910a;
    private final Api<?> b;
    /* access modifiers changed from: private */
    public final boolean c;

    public zaam(zaak zaak, Api<?> api, boolean z) {
        this.f3910a = new WeakReference<>(zaak);
        this.b = api;
        this.c = z;
    }

    public final void a(ConnectionResult connectionResult) {
        zaak zaak = (zaak) this.f3910a.get();
        if (zaak != null) {
            Preconditions.b(Looper.myLooper() == zaak.f3908a.m.d(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            zaak.b.lock();
            try {
                if (zaak.a(0)) {
                    if (!connectionResult.w()) {
                        zaak.b(connectionResult, this.b, this.c);
                    }
                    if (zaak.a()) {
                        zaak.b();
                    }
                    zaak.b.unlock();
                }
            } finally {
                zaak.b.unlock();
            }
        }
    }
}
