package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.server.response.FastJsonResponse;

public final class zaa extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zaa> CREATOR = new zab();

    /* renamed from: a  reason: collision with root package name */
    private final int f4010a;
    private final StringToIntConverter b;

    zaa(int i, StringToIntConverter stringToIntConverter) {
        this.f4010a = i;
        this.b = stringToIntConverter;
    }

    public static zaa a(FastJsonResponse.FieldConverter<?, ?> fieldConverter) {
        if (fieldConverter instanceof StringToIntConverter) {
            return new zaa((StringToIntConverter) fieldConverter);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    public final FastJsonResponse.FieldConverter<?, ?> s() {
        StringToIntConverter stringToIntConverter = this.b;
        if (stringToIntConverter != null) {
            return stringToIntConverter;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4010a);
        SafeParcelWriter.a(parcel, 2, (Parcelable) this.b, i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    private zaa(StringToIntConverter stringToIntConverter) {
        this.f4010a = 1;
        this.b = stringToIntConverter;
    }
}
