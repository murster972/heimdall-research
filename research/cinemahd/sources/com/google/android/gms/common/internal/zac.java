package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

final class zac extends DialogRedirect {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Intent f3997a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ int c;

    zac(Intent intent, Activity activity, int i) {
        this.f3997a = intent;
        this.b = activity;
        this.c = i;
    }

    public final void a() {
        Intent intent = this.f3997a;
        if (intent != null) {
            this.b.startActivityForResult(intent, this.c);
        }
    }
}
