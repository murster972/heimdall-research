package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.Map;

public final class zal extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zal> CREATOR = new zao();

    /* renamed from: a  reason: collision with root package name */
    private final int f4014a;
    final String b;
    final ArrayList<zam> c;

    zal(int i, String str, ArrayList<zam> arrayList) {
        this.f4014a = i;
        this.b = str;
        this.c = arrayList;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4014a);
        SafeParcelWriter.a(parcel, 2, this.b, false);
        SafeParcelWriter.c(parcel, 3, this.c, false);
        SafeParcelWriter.a(parcel, a2);
    }

    zal(String str, Map<String, FastJsonResponse.Field<?, ?>> map) {
        ArrayList<zam> arrayList;
        this.f4014a = 1;
        this.b = str;
        if (map == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList<>();
            for (String next : map.keySet()) {
                arrayList.add(new zam(next, map.get(next)));
            }
        }
        this.c = arrayList;
    }
}
