package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Keep;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class LifecycleCallback {

    /* renamed from: a  reason: collision with root package name */
    protected final LifecycleFragment f3897a;

    protected LifecycleCallback(LifecycleFragment lifecycleFragment) {
        this.f3897a = lifecycleFragment;
    }

    protected static LifecycleFragment a(LifecycleActivity lifecycleActivity) {
        if (lifecycleActivity.c()) {
            return zzd.a(lifecycleActivity.b());
        }
        if (lifecycleActivity.d()) {
            return zza.a(lifecycleActivity.a());
        }
        throw new IllegalArgumentException("Can't get fragment for unexpected activity.");
    }

    @Keep
    private static LifecycleFragment getChimeraLifecycleFragmentImpl(LifecycleActivity lifecycleActivity) {
        throw new IllegalStateException("Method not available in SDK.");
    }

    public void a(int i, int i2, Intent intent) {
    }

    public void a(Bundle bundle) {
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public void b() {
    }

    public void b(Bundle bundle) {
    }

    public void c() {
    }

    public void d() {
    }

    public void e() {
    }

    public static LifecycleFragment a(Activity activity) {
        return a(new LifecycleActivity(activity));
    }

    public Activity a() {
        return this.f3897a.a();
    }
}
