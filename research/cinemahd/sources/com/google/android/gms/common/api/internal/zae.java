package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl;
import com.google.android.gms.common.api.internal.GoogleApiManager;

public final class zae<A extends BaseImplementation$ApiMethodImpl<? extends Result, Api.AnyClient>> extends zab {

    /* renamed from: a  reason: collision with root package name */
    private final A f3944a;

    public zae(int i, A a2) {
        super(i);
        this.f3944a = a2;
    }

    public final void a(GoogleApiManager.zaa<?> zaa) throws DeadObjectException {
        try {
            this.f3944a.run(zaa.f());
        } catch (RuntimeException e) {
            a(e);
        }
    }

    public final void a(Status status) {
        this.f3944a.setFailedResult(status);
    }

    public final void a(RuntimeException runtimeException) {
        String simpleName = runtimeException.getClass().getSimpleName();
        String localizedMessage = runtimeException.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.f3944a.setFailedResult(new Status(10, sb.toString()));
    }

    public final void a(zaab zaab, boolean z) {
        zaab.a((BasePendingResult<? extends Result>) this.f3944a, z);
    }
}
