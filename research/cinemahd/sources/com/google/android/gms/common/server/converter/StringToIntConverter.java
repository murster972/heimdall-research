package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;

public final class StringToIntConverter extends AbstractSafeParcelable implements FastJsonResponse.FieldConverter<String, Integer> {
    public static final Parcelable.Creator<StringToIntConverter> CREATOR = new zac();

    /* renamed from: a  reason: collision with root package name */
    private final int f4008a;
    private final HashMap<String, Integer> b;
    private final SparseArray<String> c;

    StringToIntConverter(int i, ArrayList<zaa> arrayList) {
        this.f4008a = i;
        this.b = new HashMap<>();
        this.c = new SparseArray<>();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            zaa zaa2 = arrayList.get(i2);
            i2++;
            zaa zaa3 = zaa2;
            a(zaa3.b, zaa3.c);
        }
    }

    public final StringToIntConverter a(String str, int i) {
        this.b.put(str, Integer.valueOf(i));
        this.c.put(i, str);
        return this;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4008a);
        ArrayList arrayList = new ArrayList();
        for (String next : this.b.keySet()) {
            arrayList.add(new zaa(next, this.b.get(next).intValue()));
        }
        SafeParcelWriter.c(parcel, 2, arrayList, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public final /* synthetic */ Object a(Object obj) {
        String str = this.c.get(((Integer) obj).intValue());
        return (str != null || !this.b.containsKey("gms_unknown")) ? str : "gms_unknown";
    }

    public static final class zaa extends AbstractSafeParcelable {
        public static final Parcelable.Creator<zaa> CREATOR = new zad();

        /* renamed from: a  reason: collision with root package name */
        private final int f4009a;
        final String b;
        final int c;

        zaa(int i, String str, int i2) {
            this.f4009a = i;
            this.b = str;
            this.c = i2;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            int a2 = SafeParcelWriter.a(parcel);
            SafeParcelWriter.a(parcel, 1, this.f4009a);
            SafeParcelWriter.a(parcel, 2, this.b, false);
            SafeParcelWriter.a(parcel, 3, this.c);
            SafeParcelWriter.a(parcel, a2);
        }

        zaa(String str, int i) {
            this.f4009a = 1;
            this.b = str;
            this.c = i;
        }
    }

    public StringToIntConverter() {
        this.f4008a = 1;
        this.b = new HashMap<>();
        this.c = new SparseArray<>();
    }
}
