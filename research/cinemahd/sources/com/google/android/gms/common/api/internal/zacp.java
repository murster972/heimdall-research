package com.google.android.gms.common.api.internal;

import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public final class zacp {
    public static final Status d = new Status(8, "The connection to Google Play services was lost");
    private static final BasePendingResult<?>[] e = new BasePendingResult[0];

    /* renamed from: a  reason: collision with root package name */
    final Set<BasePendingResult<?>> f3940a = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    private final zacs b = new zacq(this);
    private final Map<Api.AnyClientKey<?>, Api.Client> c;

    public zacp(Map<Api.AnyClientKey<?>, Api.Client> map) {
        this.c = map;
    }

    /* access modifiers changed from: package-private */
    public final void a(BasePendingResult<? extends Result> basePendingResult) {
        this.f3940a.add(basePendingResult);
        basePendingResult.zaa(this.b);
    }

    public final void b() {
        for (BasePendingResult zab : (BasePendingResult[]) this.f3940a.toArray(e)) {
            zab.zab(d);
        }
    }

    /* JADX WARNING: type inference failed for: r5v0, types: [com.google.android.gms.common.api.ResultCallback, com.google.android.gms.common.api.internal.zacs, com.google.android.gms.common.api.zac, com.google.android.gms.common.api.internal.zacq] */
    public final void a() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.f3940a.toArray(e)) {
            ? r5 = 0;
            basePendingResult.zaa((zacs) r5);
            if (basePendingResult.zam() != null) {
                basePendingResult.setResultCallback(r5);
                IBinder serviceBrokerBinder = this.c.get(((BaseImplementation$ApiMethodImpl) basePendingResult).getClientKey()).getServiceBrokerBinder();
                if (basePendingResult.isReady()) {
                    basePendingResult.zaa((zacs) new zacr(basePendingResult, r5, serviceBrokerBinder, r5));
                } else if (serviceBrokerBinder == null || !serviceBrokerBinder.isBinderAlive()) {
                    basePendingResult.zaa((zacs) r5);
                    basePendingResult.cancel();
                    r5.a(basePendingResult.zam().intValue());
                } else {
                    zacr zacr = new zacr(basePendingResult, r5, serviceBrokerBinder, r5);
                    basePendingResult.zaa((zacs) zacr);
                    try {
                        serviceBrokerBinder.linkToDeath(zacr, 0);
                    } catch (RemoteException unused) {
                        basePendingResult.cancel();
                        r5.a(basePendingResult.zam().intValue());
                    }
                }
                this.f3940a.remove(basePendingResult);
            } else if (basePendingResult.zat()) {
                this.f3940a.remove(basePendingResult);
            }
        }
    }
}
