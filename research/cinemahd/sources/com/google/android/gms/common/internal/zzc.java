package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzc implements Parcelable.Creator<zza> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        Bundle bundle = null;
        Feature[] featureArr = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                bundle = SafeParcelReader.f(parcel, a2);
            } else if (a3 == 2) {
                featureArr = (Feature[]) SafeParcelReader.b(parcel, a2, Feature.CREATOR);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                i = SafeParcelReader.w(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zza(bundle, featureArr, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zza[i];
    }
}
