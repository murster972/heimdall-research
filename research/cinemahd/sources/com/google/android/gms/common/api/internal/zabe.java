package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class zabe implements zabs, zar {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Lock f3918a;
    private final Condition b;
    private final Context c;
    private final GoogleApiAvailabilityLight d;
    private final zabg e;
    final Map<Api.AnyClientKey<?>, Api.Client> f;
    final Map<Api.AnyClientKey<?>, ConnectionResult> g = new HashMap();
    private final ClientSettings h;
    private final Map<Api<?>, Boolean> i;
    private final Api.AbstractClientBuilder<? extends zad, SignInOptions> j;
    /* access modifiers changed from: private */
    public volatile zabd k;
    int l;
    final zaaw m;
    final zabt n;

    public zabe(Context context, zaaw zaaw, Lock lock, Looper looper, GoogleApiAvailabilityLight googleApiAvailabilityLight, Map<Api.AnyClientKey<?>, Api.Client> map, ClientSettings clientSettings, Map<Api<?>, Boolean> map2, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder, ArrayList<zaq> arrayList, zabt zabt) {
        this.c = context;
        this.f3918a = lock;
        this.d = googleApiAvailabilityLight;
        this.f = map;
        this.h = clientSettings;
        this.i = map2;
        this.j = abstractClientBuilder;
        this.m = zaaw;
        this.n = zabt;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            zaq zaq = arrayList.get(i2);
            i2++;
            zaq.a(this);
        }
        this.e = new zabg(this, looper);
        this.b = lock.newCondition();
        this.k = new zaav(this);
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t) {
        t.zau();
        return this.k.a(t);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.f3918a.lock();
        try {
            this.k = new zaak(this, this.h, this.i, this.d, this.j, this.f3918a, this.c);
            this.k.begin();
            this.b.signalAll();
        } finally {
            this.f3918a.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.f3918a.lock();
        try {
            this.m.g();
            this.k = new zaah(this);
            this.k.begin();
            this.b.signalAll();
        } finally {
            this.f3918a.unlock();
        }
    }

    public final void connect() {
        this.k.connect();
    }

    public final void disconnect() {
        if (this.k.disconnect()) {
            this.g.clear();
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append(str).append("mState=").println(this.k);
        for (Api next : this.i.keySet()) {
            printWriter.append(str).append(next.b()).println(":");
            this.f.get(next.a()).dump(concat, fileDescriptor, printWriter, strArr);
        }
    }

    public final boolean isConnected() {
        return this.k instanceof zaah;
    }

    public final void onConnected(Bundle bundle) {
        this.f3918a.lock();
        try {
            this.k.onConnected(bundle);
        } finally {
            this.f3918a.unlock();
        }
    }

    public final void onConnectionSuspended(int i2) {
        this.f3918a.lock();
        try {
            this.k.onConnectionSuspended(i2);
        } finally {
            this.f3918a.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(ConnectionResult connectionResult) {
        this.f3918a.lock();
        try {
            this.k = new zaav(this);
            this.k.begin();
            this.b.signalAll();
        } finally {
            this.f3918a.unlock();
        }
    }

    public final void a() {
        if (isConnected()) {
            ((zaah) this.k).a();
        }
    }

    public final void a(ConnectionResult connectionResult, Api<?> api, boolean z) {
        this.f3918a.lock();
        try {
            this.k.a(connectionResult, api, z);
        } finally {
            this.f3918a.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(zabf zabf) {
        this.e.sendMessage(this.e.obtainMessage(1, zabf));
    }

    /* access modifiers changed from: package-private */
    public final void a(RuntimeException runtimeException) {
        this.e.sendMessage(this.e.obtainMessage(2, runtimeException));
    }
}
