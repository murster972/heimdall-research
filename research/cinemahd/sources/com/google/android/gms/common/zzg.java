package com.google.android.gms.common;

import java.util.Arrays;

final class zzg extends zzd {
    private final byte[] b;

    zzg(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.b = bArr;
    }

    /* access modifiers changed from: package-private */
    public final byte[] b() {
        return this.b;
    }
}
