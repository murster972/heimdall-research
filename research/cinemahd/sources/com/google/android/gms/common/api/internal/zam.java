package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.Preconditions;

final class zam {

    /* renamed from: a  reason: collision with root package name */
    private final int f3949a;
    private final ConnectionResult b;

    zam(ConnectionResult connectionResult, int i) {
        Preconditions.a(connectionResult);
        this.b = connectionResult;
        this.f3949a = i;
    }

    /* access modifiers changed from: package-private */
    public final ConnectionResult a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        return this.f3949a;
    }
}
