package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class ConnectionResult extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ConnectionResult> CREATOR = new zza();
    public static final int SUCCESS = 0;
    public static final ConnectionResult e = new ConnectionResult(0);

    /* renamed from: a  reason: collision with root package name */
    private final int f3873a;
    private final int b;
    private final PendingIntent c;
    private final String d;

    ConnectionResult(int i, int i2, PendingIntent pendingIntent, String str) {
        this.f3873a = i;
        this.b = i2;
        this.c = pendingIntent;
        this.d = str;
    }

    static String b(int i) {
        if (i == 99) {
            return "UNFINISHED";
        }
        if (i == 1500) {
            return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        switch (i) {
            case -1:
                return "UNKNOWN";
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            case 11:
                return "LICENSE_CHECK_FAILED";
            default:
                switch (i) {
                    case 13:
                        return "CANCELED";
                    case 14:
                        return "TIMEOUT";
                    case 15:
                        return "INTERRUPTED";
                    case 16:
                        return "API_UNAVAILABLE";
                    case 17:
                        return "SIGN_IN_FAILED";
                    case 18:
                        return "SERVICE_UPDATING";
                    case 19:
                        return "SERVICE_MISSING_PERMISSION";
                    case 20:
                        return "RESTRICTED_PROFILE";
                    case 21:
                        return "API_VERSION_UPDATE_REQUIRED";
                    default:
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("UNKNOWN_ERROR_CODE(");
                        sb.append(i);
                        sb.append(")");
                        return sb.toString();
                }
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ConnectionResult)) {
            return false;
        }
        ConnectionResult connectionResult = (ConnectionResult) obj;
        return this.b == connectionResult.b && Objects.a(this.c, connectionResult.c) && Objects.a(this.d, connectionResult.d);
    }

    public final int hashCode() {
        return Objects.a(Integer.valueOf(this.b), this.c, this.d);
    }

    public final int s() {
        return this.b;
    }

    public final String t() {
        return this.d;
    }

    public final String toString() {
        Objects.ToStringHelper a2 = Objects.a((Object) this);
        a2.a("statusCode", b(this.b));
        a2.a("resolution", this.c);
        a2.a("message", this.d);
        return a2.toString();
    }

    public final PendingIntent u() {
        return this.c;
    }

    public final boolean v() {
        return (this.b == 0 || this.c == null) ? false : true;
    }

    public final boolean w() {
        return this.b == 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f3873a);
        SafeParcelWriter.a(parcel, 2, s());
        SafeParcelWriter.a(parcel, 3, (Parcelable) u(), i, false);
        SafeParcelWriter.a(parcel, 4, t(), false);
        SafeParcelWriter.a(parcel, a2);
    }

    public ConnectionResult(int i) {
        this(i, (PendingIntent) null, (String) null);
    }

    public ConnectionResult(int i, PendingIntent pendingIntent) {
        this(i, pendingIntent, (String) null);
    }

    public ConnectionResult(int i, PendingIntent pendingIntent, String str) {
        this(1, i, pendingIntent, str);
    }
}
