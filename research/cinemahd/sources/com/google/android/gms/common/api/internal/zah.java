package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zah extends zad<Boolean> {
    private final ListenerHolder.ListenerKey<?> b;

    public zah(ListenerHolder.ListenerKey<?> listenerKey, TaskCompletionSource<Boolean> taskCompletionSource) {
        super(4, taskCompletionSource);
        this.b = listenerKey;
    }

    public final /* bridge */ /* synthetic */ void a(zaab zaab, boolean z) {
    }

    public final Feature[] b(GoogleApiManager.zaa<?> zaa) {
        zabw zabw = zaa.i().get(this.b);
        if (zabw == null) {
            return null;
        }
        return zabw.f3931a.c();
    }

    public final boolean c(GoogleApiManager.zaa<?> zaa) {
        zabw zabw = zaa.i().get(this.b);
        return zabw != null && zabw.f3931a.d();
    }

    public final void d(GoogleApiManager.zaa<?> zaa) throws RemoteException {
        zabw remove = zaa.i().remove(this.b);
        if (remove != null) {
            remove.b.a(zaa.f(), this.f3943a);
            remove.f3931a.a();
            return;
        }
        this.f3943a.b(false);
    }
}
