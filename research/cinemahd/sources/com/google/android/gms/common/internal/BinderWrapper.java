package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;

@KeepName
public final class BinderWrapper implements Parcelable {
    public static final Parcelable.Creator<BinderWrapper> CREATOR = new zzb();

    /* renamed from: a  reason: collision with root package name */
    private IBinder f3978a;

    public BinderWrapper() {
        this.f3978a = null;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeStrongBinder(this.f3978a);
    }

    public BinderWrapper(IBinder iBinder) {
        this.f3978a = null;
        this.f3978a = iBinder;
    }

    private BinderWrapper(Parcel parcel) {
        this.f3978a = null;
        this.f3978a = parcel.readStrongBinder();
    }

    /* synthetic */ BinderWrapper(Parcel parcel, zzb zzb) {
        this(parcel);
    }
}
