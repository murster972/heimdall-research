package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

public class TaskUtil {
    public static void a(Status status, TaskCompletionSource<Void> taskCompletionSource) {
        a(status, (Object) null, taskCompletionSource);
    }

    public static <TResult> void a(Status status, TResult tresult, TaskCompletionSource<TResult> taskCompletionSource) {
        if (status.v()) {
            taskCompletionSource.a(tresult);
        } else {
            taskCompletionSource.a((Exception) new ApiException(status));
        }
    }
}
