package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class zau implements zabt {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zas f3954a;

    private zau(zas zas) {
        this.f3954a = zas;
    }

    public final void a(Bundle bundle) {
        this.f3954a.l.lock();
        try {
            this.f3954a.a(bundle);
            ConnectionResult unused = this.f3954a.i = ConnectionResult.e;
            this.f3954a.c();
        } finally {
            this.f3954a.l.unlock();
        }
    }

    /* synthetic */ zau(zas zas, zat zat) {
        this(zas);
    }

    public final void a(ConnectionResult connectionResult) {
        this.f3954a.l.lock();
        try {
            ConnectionResult unused = this.f3954a.i = connectionResult;
            this.f3954a.c();
        } finally {
            this.f3954a.l.unlock();
        }
    }

    public final void a(int i, boolean z) {
        this.f3954a.l.lock();
        try {
            if (!this.f3954a.k && this.f3954a.j != null) {
                if (this.f3954a.j.w()) {
                    boolean unused = this.f3954a.k = true;
                    this.f3954a.d.onConnectionSuspended(i);
                    this.f3954a.l.unlock();
                    return;
                }
            }
            boolean unused2 = this.f3954a.k = false;
            this.f3954a.a(i, z);
        } finally {
            this.f3954a.l.unlock();
        }
    }
}
