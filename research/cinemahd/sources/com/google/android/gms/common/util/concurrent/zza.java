package com.google.android.gms.common.util.concurrent;

import android.os.Process;

final class zza implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Runnable f4024a;
    private final int b;

    public zza(Runnable runnable, int i) {
        this.f4024a = runnable;
        this.b = i;
    }

    public final void run() {
        Process.setThreadPriority(this.b);
        this.f4024a.run();
    }
}
