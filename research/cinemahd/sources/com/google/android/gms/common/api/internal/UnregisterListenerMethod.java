package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AnyClient;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class UnregisterListenerMethod<A extends Api.AnyClient, L> {
    public abstract ListenerHolder.ListenerKey<L> a();

    /* access modifiers changed from: protected */
    public abstract void a(A a2, TaskCompletionSource<Boolean> taskCompletionSource) throws RemoteException;
}
