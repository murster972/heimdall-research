package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

final class zabc extends zabr {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<zaaw> f3917a;

    zabc(zaaw zaaw) {
        this.f3917a = new WeakReference<>(zaaw);
    }

    public final void a() {
        zaaw zaaw = (zaaw) this.f3917a.get();
        if (zaaw != null) {
            zaaw.j();
        }
    }
}
