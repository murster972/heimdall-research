package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.common.R$string;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.StringResourceValueReader;
import com.google.android.gms.common.internal.zzp;

@Deprecated
public final class GoogleServices {
    private static final Object c = new Object();
    private static GoogleServices d;

    /* renamed from: a  reason: collision with root package name */
    private final String f3895a;
    private final Status b;

    GoogleServices(Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(R$string.common_google_play_services_unknown_issue));
        if (identifier != 0) {
            int integer = resources.getInteger(identifier);
        }
        String a2 = zzp.a(context);
        a2 = a2 == null ? new StringResourceValueReader(context).a("google_app_id") : a2;
        if (TextUtils.isEmpty(a2)) {
            this.b = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.f3895a = null;
            return;
        }
        this.f3895a = a2;
        this.b = Status.e;
    }

    public static Status a(Context context) {
        Status status;
        Preconditions.a(context, (Object) "Context must not be null.");
        synchronized (c) {
            if (d == null) {
                d = new GoogleServices(context);
            }
            status = d.b;
        }
        return status;
    }

    public static String a() {
        return a("getGoogleAppId").f3895a;
    }

    private static GoogleServices a(String str) {
        GoogleServices googleServices;
        synchronized (c) {
            if (d != null) {
                googleServices = d;
            } else {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
                sb.append("Initialize must be called before ");
                sb.append(str);
                sb.append(".");
                throw new IllegalStateException(sb.toString());
            }
        }
        return googleServices;
    }
}
