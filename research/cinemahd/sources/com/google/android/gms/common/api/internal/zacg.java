package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zaj;

final class zacg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zaj f3936a;
    private final /* synthetic */ zace b;

    zacg(zace zace, zaj zaj) {
        this.b = zace;
        this.f3936a = zaj;
    }

    public final void run() {
        this.b.b(this.f3936a);
    }
}
