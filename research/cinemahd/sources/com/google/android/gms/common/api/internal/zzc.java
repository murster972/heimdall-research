package com.google.android.gms.common.api.internal;

final class zzc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ LifecycleCallback f3961a;
    private final /* synthetic */ String b;
    private final /* synthetic */ zzd c;

    zzc(zzd zzd, LifecycleCallback lifecycleCallback, String str) {
        this.c = zzd;
        this.f3961a = lifecycleCallback;
        this.b = str;
    }

    public final void run() {
        if (this.c.b > 0) {
            this.f3961a.a(this.c.c != null ? this.c.c.getBundle(this.b) : null);
        }
        if (this.c.b >= 2) {
            this.f3961a.d();
        }
        if (this.c.b >= 3) {
            this.f3961a.c();
        }
        if (this.c.b >= 4) {
            this.f3961a.e();
        }
        if (this.c.b >= 5) {
            this.f3961a.b();
        }
    }
}
