package com.google.android.gms.common.internal;

import android.content.Intent;
import androidx.fragment.app.Fragment;

final class zad extends DialogRedirect {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Intent f3998a;
    private final /* synthetic */ Fragment b;
    private final /* synthetic */ int c;

    zad(Intent intent, Fragment fragment, int i) {
        this.f3998a = intent;
        this.b = fragment;
        this.c = i;
    }

    public final void a() {
        Intent intent = this.f3998a;
        if (intent != null) {
            this.b.startActivityForResult(intent, this.c);
        }
    }
}
