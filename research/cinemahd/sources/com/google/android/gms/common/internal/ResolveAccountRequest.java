package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public class ResolveAccountRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ResolveAccountRequest> CREATOR = new zam();

    /* renamed from: a  reason: collision with root package name */
    private final int f3991a;
    private final Account b;
    private final int c;
    private final GoogleSignInAccount d;

    ResolveAccountRequest(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.f3991a = i;
        this.b = account;
        this.c = i2;
        this.d = googleSignInAccount;
    }

    public int getSessionId() {
        return this.c;
    }

    public Account s() {
        return this.b;
    }

    public GoogleSignInAccount t() {
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f3991a);
        SafeParcelWriter.a(parcel, 2, (Parcelable) s(), i, false);
        SafeParcelWriter.a(parcel, 3, getSessionId());
        SafeParcelWriter.a(parcel, 4, (Parcelable) t(), i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public ResolveAccountRequest(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }
}
