package com.google.android.gms.common.api.internal;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import androidx.collection.ArrayMap;
import androidx.collection.ArraySet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import com.google.android.gms.common.api.internal.BackgroundDetector;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.GoogleApiAvailabilityCache;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.SimpleClientAdapter;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.base.zal;
import com.google.android.gms.signin.zad;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class GoogleApiManager implements Handler.Callback {
    public static final Status n = new Status(4, "Sign-out occurred while this API call was in progress.");
    /* access modifiers changed from: private */
    public static final Status o = new Status(4, "The user must be signed in to make this API call.");
    /* access modifiers changed from: private */
    public static final Object p = new Object();
    private static GoogleApiManager q;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public long f3891a = 5000;
    /* access modifiers changed from: private */
    public long b = 120000;
    /* access modifiers changed from: private */
    public long c = 10000;
    /* access modifiers changed from: private */
    public final Context d;
    /* access modifiers changed from: private */
    public final GoogleApiAvailability e;
    /* access modifiers changed from: private */
    public final GoogleApiAvailabilityCache f;
    private final AtomicInteger g = new AtomicInteger(1);
    private final AtomicInteger h = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public final Map<zai<?>, zaa<?>> i = new ConcurrentHashMap(5, 0.75f, 1);
    /* access modifiers changed from: private */
    public zaae j = null;
    /* access modifiers changed from: private */
    public final Set<zai<?>> k = new ArraySet();
    private final Set<zai<?>> l = new ArraySet();
    /* access modifiers changed from: private */
    public final Handler m;

    private class zac implements zach, BaseGmsClient.ConnectionProgressReportCallbacks {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Api.Client f3894a;
        /* access modifiers changed from: private */
        public final zai<?> b;
        private IAccountAccessor c = null;
        private Set<Scope> d = null;
        /* access modifiers changed from: private */
        public boolean e = false;

        public zac(Api.Client client, zai<?> zai) {
            this.f3894a = client;
            this.b = zai;
        }

        public final void a(ConnectionResult connectionResult) {
            GoogleApiManager.this.m.post(new zabo(this, connectionResult));
        }

        public final void b(ConnectionResult connectionResult) {
            ((zaa) GoogleApiManager.this.i.get(this.b)).a(connectionResult);
        }

        public final void a(IAccountAccessor iAccountAccessor, Set<Scope> set) {
            if (iAccountAccessor == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                b(new ConnectionResult(4));
                return;
            }
            this.c = iAccountAccessor;
            this.d = set;
            a();
        }

        /* access modifiers changed from: private */
        public final void a() {
            IAccountAccessor iAccountAccessor;
            if (this.e && (iAccountAccessor = this.c) != null) {
                this.f3894a.getRemoteService(iAccountAccessor, this.d);
            }
        }
    }

    private GoogleApiManager(Context context, Looper looper, GoogleApiAvailability googleApiAvailability) {
        this.d = context;
        this.m = new zal(looper, this);
        this.e = googleApiAvailability;
        this.f = new GoogleApiAvailabilityCache(googleApiAvailability);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(6));
    }

    public static GoogleApiManager a(Context context) {
        GoogleApiManager googleApiManager;
        synchronized (p) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                q = new GoogleApiManager(context.getApplicationContext(), handlerThread.getLooper(), GoogleApiAvailability.a());
            }
            googleApiManager = q;
        }
        return googleApiManager;
    }

    public static GoogleApiManager c() {
        GoogleApiManager googleApiManager;
        synchronized (p) {
            Preconditions.a(q, (Object) "Must guarantee manager is non-null before using getInstance");
            googleApiManager = q;
        }
        return googleApiManager;
    }

    /* access modifiers changed from: package-private */
    public final void b(zaae zaae) {
        synchronized (p) {
            if (this.j == zaae) {
                this.j = null;
                this.k.clear();
            }
        }
    }

    public boolean handleMessage(Message message) {
        zaa zaa2;
        int i2 = message.what;
        long j2 = 300000;
        switch (i2) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j2 = 10000;
                }
                this.c = j2;
                this.m.removeMessages(12);
                for (zai<?> obtainMessage : this.i.keySet()) {
                    Handler handler = this.m;
                    handler.sendMessageDelayed(handler.obtainMessage(12, obtainMessage), this.c);
                }
                break;
            case 2:
                zak zak = (zak) message.obj;
                Iterator<zai<?>> it2 = zak.b().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    } else {
                        zai next = it2.next();
                        zaa zaa3 = this.i.get(next);
                        if (zaa3 == null) {
                            zak.a(next, new ConnectionResult(13), (String) null);
                            break;
                        } else if (zaa3.c()) {
                            zak.a(next, ConnectionResult.e, zaa3.f().getEndpointPackageName());
                        } else if (zaa3.k() != null) {
                            zak.a(next, zaa3.k(), (String) null);
                        } else {
                            zaa3.a(zak);
                            zaa3.a();
                        }
                    }
                }
            case 3:
                for (zaa next2 : this.i.values()) {
                    next2.j();
                    next2.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                zabv zabv = (zabv) message.obj;
                zaa zaa4 = this.i.get(zabv.c.zak());
                if (zaa4 == null) {
                    c(zabv.c);
                    zaa4 = this.i.get(zabv.c.zak());
                }
                if (zaa4.d() && this.h.get() != zabv.b) {
                    zabv.f3930a.a(n);
                    zaa4.h();
                    break;
                } else {
                    zaa4.a(zabv.f3930a);
                    break;
                }
                break;
            case 5:
                int i3 = message.arg1;
                ConnectionResult connectionResult = (ConnectionResult) message.obj;
                Iterator<zaa<?>> it3 = this.i.values().iterator();
                while (true) {
                    if (it3.hasNext()) {
                        zaa2 = it3.next();
                        if (zaa2.b() == i3) {
                        }
                    } else {
                        zaa2 = null;
                    }
                }
                if (zaa2 == null) {
                    StringBuilder sb = new StringBuilder(76);
                    sb.append("Could not find API instance ");
                    sb.append(i3);
                    sb.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb.toString(), new Exception());
                    break;
                } else {
                    String b2 = this.e.b(connectionResult.s());
                    String t = connectionResult.t();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b2).length() + 69 + String.valueOf(t).length());
                    sb2.append("Error resolution was canceled by the user, original error message: ");
                    sb2.append(b2);
                    sb2.append(": ");
                    sb2.append(t);
                    zaa2.a(new Status(17, sb2.toString()));
                    break;
                }
            case 6:
                if (PlatformVersion.a() && (this.d.getApplicationContext() instanceof Application)) {
                    BackgroundDetector.a((Application) this.d.getApplicationContext());
                    BackgroundDetector.b().a((BackgroundDetector.BackgroundStateChangeListener) new zabi(this));
                    if (!BackgroundDetector.b().a(true)) {
                        this.c = 300000;
                        break;
                    }
                }
                break;
            case 7:
                c((GoogleApi<?>) (GoogleApi) message.obj);
                break;
            case 9:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).e();
                    break;
                }
                break;
            case 10:
                for (zai<?> remove : this.l) {
                    this.i.remove(remove).h();
                }
                this.l.clear();
                break;
            case 11:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).g();
                    break;
                }
                break;
            case 12:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).l();
                    break;
                }
                break;
            case 14:
                zaaf zaaf = (zaaf) message.obj;
                zai<?> b3 = zaaf.b();
                if (this.i.containsKey(b3)) {
                    zaaf.a().a(Boolean.valueOf(this.i.get(b3).a(false)));
                    break;
                } else {
                    zaaf.a().a(false);
                    break;
                }
            case 15:
                zab zab2 = (zab) message.obj;
                if (this.i.containsKey(zab2.f3893a)) {
                    this.i.get(zab2.f3893a).a(zab2);
                    break;
                }
                break;
            case 16:
                zab zab3 = (zab) message.obj;
                if (this.i.containsKey(zab3.f3893a)) {
                    this.i.get(zab3.f3893a).b(zab3);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    public class zaa<O extends Api.ApiOptions> implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, zar {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<zab> f3892a = new LinkedList();
        /* access modifiers changed from: private */
        public final Api.Client b;
        private final Api.AnyClient c;
        private final zai<O> d;
        private final zaab e;
        private final Set<zak> f = new HashSet();
        private final Map<ListenerHolder.ListenerKey<?>, zabw> g = new HashMap();
        private final int h;
        private final zace i;
        private boolean j;
        private final List<zab> k = new ArrayList();
        private ConnectionResult l = null;

        public zaa(GoogleApi<O> googleApi) {
            this.b = googleApi.zaa(GoogleApiManager.this.m.getLooper(), (zaa<O>) this);
            Api.Client client = this.b;
            if (client instanceof SimpleClientAdapter) {
                this.c = ((SimpleClientAdapter) client).b();
            } else {
                this.c = client;
            }
            this.d = googleApi.zak();
            this.e = new zaab();
            this.h = googleApi.getInstanceId();
            if (this.b.requiresSignIn()) {
                this.i = googleApi.zaa(GoogleApiManager.this.d, GoogleApiManager.this.m);
            } else {
                this.i = null;
            }
        }

        private final boolean b(ConnectionResult connectionResult) {
            synchronized (GoogleApiManager.p) {
                if (GoogleApiManager.this.j == null || !GoogleApiManager.this.k.contains(this.d)) {
                    return false;
                }
                GoogleApiManager.this.j.b(connectionResult, this.h);
                return true;
            }
        }

        private final void c(zab zab) {
            zab.a(this.e, d());
            try {
                zab.a((zaa<?>) this);
            } catch (DeadObjectException unused) {
                onConnectionSuspended(1);
                this.b.disconnect();
            }
        }

        /* access modifiers changed from: private */
        public final void n() {
            j();
            c(ConnectionResult.e);
            q();
            Iterator<zabw> it2 = this.g.values().iterator();
            while (it2.hasNext()) {
                zabw next = it2.next();
                if (a(next.f3931a.c()) != null) {
                    it2.remove();
                } else {
                    try {
                        next.f3931a.a(this.c, new TaskCompletionSource());
                    } catch (DeadObjectException unused) {
                        onConnectionSuspended(1);
                        this.b.disconnect();
                    } catch (RemoteException unused2) {
                        it2.remove();
                    }
                }
            }
            p();
            r();
        }

        /* access modifiers changed from: private */
        public final void o() {
            j();
            this.j = true;
            this.e.c();
            GoogleApiManager.this.m.sendMessageDelayed(Message.obtain(GoogleApiManager.this.m, 9, this.d), GoogleApiManager.this.f3891a);
            GoogleApiManager.this.m.sendMessageDelayed(Message.obtain(GoogleApiManager.this.m, 11, this.d), GoogleApiManager.this.b);
            GoogleApiManager.this.f.a();
        }

        private final void p() {
            ArrayList arrayList = new ArrayList(this.f3892a);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                zab zab = (zab) obj;
                if (!this.b.isConnected()) {
                    return;
                }
                if (b(zab)) {
                    this.f3892a.remove(zab);
                }
            }
        }

        private final void q() {
            if (this.j) {
                GoogleApiManager.this.m.removeMessages(11, this.d);
                GoogleApiManager.this.m.removeMessages(9, this.d);
                this.j = false;
            }
        }

        private final void r() {
            GoogleApiManager.this.m.removeMessages(12, this.d);
            GoogleApiManager.this.m.sendMessageDelayed(GoogleApiManager.this.m.obtainMessage(12, this.d), GoogleApiManager.this.c);
        }

        public final void a(ConnectionResult connectionResult) {
            Preconditions.a(GoogleApiManager.this.m);
            this.b.disconnect();
            onConnectionFailed(connectionResult);
        }

        public final boolean d() {
            return this.b.requiresSignIn();
        }

        public final void e() {
            Preconditions.a(GoogleApiManager.this.m);
            if (this.j) {
                a();
            }
        }

        public final Api.Client f() {
            return this.b;
        }

        public final void g() {
            Status status;
            Preconditions.a(GoogleApiManager.this.m);
            if (this.j) {
                q();
                if (GoogleApiManager.this.e.c(GoogleApiManager.this.d) == 18) {
                    status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
                } else {
                    status = new Status(8, "API failed to connect while resuming due to an unknown error.");
                }
                a(status);
                this.b.disconnect();
            }
        }

        public final void h() {
            Preconditions.a(GoogleApiManager.this.m);
            a(GoogleApiManager.n);
            this.e.b();
            for (ListenerHolder.ListenerKey zah : (ListenerHolder.ListenerKey[]) this.g.keySet().toArray(new ListenerHolder.ListenerKey[this.g.size()])) {
                a((zab) new zah(zah, new TaskCompletionSource()));
            }
            c(new ConnectionResult(4));
            if (this.b.isConnected()) {
                this.b.onUserSignOut(new zabm(this));
            }
        }

        public final Map<ListenerHolder.ListenerKey<?>, zabw> i() {
            return this.g;
        }

        public final void j() {
            Preconditions.a(GoogleApiManager.this.m);
            this.l = null;
        }

        public final ConnectionResult k() {
            Preconditions.a(GoogleApiManager.this.m);
            return this.l;
        }

        public final boolean l() {
            return a(true);
        }

        /* access modifiers changed from: package-private */
        public final zad m() {
            zace zace = this.i;
            if (zace == null) {
                return null;
            }
            return zace.b();
        }

        public final void onConnected(Bundle bundle) {
            if (Looper.myLooper() == GoogleApiManager.this.m.getLooper()) {
                n();
            } else {
                GoogleApiManager.this.m.post(new zabj(this));
            }
        }

        public final void onConnectionFailed(ConnectionResult connectionResult) {
            Preconditions.a(GoogleApiManager.this.m);
            zace zace = this.i;
            if (zace != null) {
                zace.w();
            }
            j();
            GoogleApiManager.this.f.a();
            c(connectionResult);
            if (connectionResult.s() == 4) {
                a(GoogleApiManager.o);
            } else if (this.f3892a.isEmpty()) {
                this.l = connectionResult;
            } else if (!b(connectionResult) && !GoogleApiManager.this.b(connectionResult, this.h)) {
                if (connectionResult.s() == 18) {
                    this.j = true;
                }
                if (this.j) {
                    GoogleApiManager.this.m.sendMessageDelayed(Message.obtain(GoogleApiManager.this.m, 9, this.d), GoogleApiManager.this.f3891a);
                    return;
                }
                String a2 = this.d.a();
                StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 38);
                sb.append("API: ");
                sb.append(a2);
                sb.append(" is not available on this device.");
                a(new Status(17, sb.toString()));
            }
        }

        public final void onConnectionSuspended(int i2) {
            if (Looper.myLooper() == GoogleApiManager.this.m.getLooper()) {
                o();
            } else {
                GoogleApiManager.this.m.post(new zabk(this));
            }
        }

        public final void a(ConnectionResult connectionResult, Api<?> api, boolean z) {
            if (Looper.myLooper() == GoogleApiManager.this.m.getLooper()) {
                onConnectionFailed(connectionResult);
            } else {
                GoogleApiManager.this.m.post(new zabl(this, connectionResult));
            }
        }

        private final void c(ConnectionResult connectionResult) {
            for (zak next : this.f) {
                String str = null;
                if (Objects.a(connectionResult, ConnectionResult.e)) {
                    str = this.b.getEndpointPackageName();
                }
                next.a(this.d, connectionResult, str);
            }
            this.f.clear();
        }

        private final boolean b(zab zab) {
            if (!(zab instanceof zac)) {
                c(zab);
                return true;
            }
            zac zac = (zac) zab;
            Feature a2 = a(zac.b(this));
            if (a2 == null) {
                c(zab);
                return true;
            } else if (zac.c(this)) {
                zab zab2 = new zab(this.d, a2, (zabi) null);
                int indexOf = this.k.indexOf(zab2);
                if (indexOf >= 0) {
                    zab zab3 = this.k.get(indexOf);
                    GoogleApiManager.this.m.removeMessages(15, zab3);
                    GoogleApiManager.this.m.sendMessageDelayed(Message.obtain(GoogleApiManager.this.m, 15, zab3), GoogleApiManager.this.f3891a);
                    return false;
                }
                this.k.add(zab2);
                GoogleApiManager.this.m.sendMessageDelayed(Message.obtain(GoogleApiManager.this.m, 15, zab2), GoogleApiManager.this.f3891a);
                GoogleApiManager.this.m.sendMessageDelayed(Message.obtain(GoogleApiManager.this.m, 16, zab2), GoogleApiManager.this.b);
                ConnectionResult connectionResult = new ConnectionResult(2, (PendingIntent) null);
                if (b(connectionResult)) {
                    return false;
                }
                GoogleApiManager.this.b(connectionResult, this.h);
                return false;
            } else {
                zac.a((RuntimeException) new UnsupportedApiCallException(a2));
                return false;
            }
        }

        public final void a(zab zab) {
            Preconditions.a(GoogleApiManager.this.m);
            if (!this.b.isConnected()) {
                this.f3892a.add(zab);
                ConnectionResult connectionResult = this.l;
                if (connectionResult == null || !connectionResult.v()) {
                    a();
                } else {
                    onConnectionFailed(this.l);
                }
            } else if (b(zab)) {
                r();
            } else {
                this.f3892a.add(zab);
            }
        }

        /* access modifiers changed from: package-private */
        public final boolean c() {
            return this.b.isConnected();
        }

        public final void a(Status status) {
            Preconditions.a(GoogleApiManager.this.m);
            for (zab a2 : this.f3892a) {
                a2.a(status);
            }
            this.f3892a.clear();
        }

        /* access modifiers changed from: private */
        public final boolean a(boolean z) {
            Preconditions.a(GoogleApiManager.this.m);
            if (!this.b.isConnected() || this.g.size() != 0) {
                return false;
            }
            if (this.e.a()) {
                if (z) {
                    r();
                }
                return false;
            }
            this.b.disconnect();
            return true;
        }

        public final void a() {
            Preconditions.a(GoogleApiManager.this.m);
            if (!this.b.isConnected() && !this.b.isConnecting()) {
                int a2 = GoogleApiManager.this.f.a(GoogleApiManager.this.d, this.b);
                if (a2 != 0) {
                    onConnectionFailed(new ConnectionResult(a2, (PendingIntent) null));
                    return;
                }
                zac zac = new zac(this.b, this.d);
                if (this.b.requiresSignIn()) {
                    this.i.a((zach) zac);
                }
                this.b.connect(zac);
            }
        }

        public final int b() {
            return this.h;
        }

        /* access modifiers changed from: private */
        public final void b(zab zab) {
            Feature[] b2;
            if (this.k.remove(zab)) {
                GoogleApiManager.this.m.removeMessages(15, zab);
                GoogleApiManager.this.m.removeMessages(16, zab);
                Feature b3 = zab.b;
                ArrayList arrayList = new ArrayList(this.f3892a.size());
                for (zab zab2 : this.f3892a) {
                    if ((zab2 instanceof zac) && (b2 = ((zac) zab2).b(this)) != null && ArrayUtils.a((T[]) b2, b3)) {
                        arrayList.add(zab2);
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    zab zab3 = (zab) obj;
                    this.f3892a.remove(zab3);
                    zab3.a((RuntimeException) new UnsupportedApiCallException(b3));
                }
            }
        }

        public final void a(zak zak) {
            Preconditions.a(GoogleApiManager.this.m);
            this.f.add(zak);
        }

        private final Feature a(Feature[] featureArr) {
            if (!(featureArr == null || featureArr.length == 0)) {
                Feature[] availableFeatures = this.b.getAvailableFeatures();
                if (availableFeatures == null) {
                    availableFeatures = new Feature[0];
                }
                ArrayMap arrayMap = new ArrayMap(availableFeatures.length);
                for (Feature feature : availableFeatures) {
                    arrayMap.put(feature.s(), Long.valueOf(feature.t()));
                }
                for (Feature feature2 : featureArr) {
                    if (!arrayMap.containsKey(feature2.s()) || ((Long) arrayMap.get(feature2.s())).longValue() < feature2.t()) {
                        return feature2;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: private */
        public final void a(zab zab) {
            if (!this.k.contains(zab) || this.j) {
                return;
            }
            if (!this.b.isConnected()) {
                a();
            } else {
                p();
            }
        }
    }

    private static class zab {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final zai<?> f3893a;
        /* access modifiers changed from: private */
        public final Feature b;

        private zab(zai<?> zai, Feature feature) {
            this.f3893a = zai;
            this.b = feature;
        }

        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof zab)) {
                zab zab = (zab) obj;
                if (!Objects.a(this.f3893a, zab.f3893a) || !Objects.a(this.b, zab.b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        public final int hashCode() {
            return Objects.a(this.f3893a, this.b);
        }

        public final String toString() {
            Objects.ToStringHelper a2 = Objects.a((Object) this);
            a2.a("key", this.f3893a);
            a2.a("feature", this.b);
            return a2.toString();
        }

        /* synthetic */ zab(zai zai, Feature feature, zabi zabi) {
            this(zai, feature);
        }
    }

    private final void c(GoogleApi<?> googleApi) {
        zai<?> zak = googleApi.zak();
        zaa zaa2 = this.i.get(zak);
        if (zaa2 == null) {
            zaa2 = new zaa(googleApi);
            this.i.put(zak, zaa2);
        }
        if (zaa2.d()) {
            this.l.add(zak);
        }
        zaa2.a();
    }

    public final void b() {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(3));
    }

    public final Task<Boolean> b(GoogleApi<?> googleApi) {
        zaaf zaaf = new zaaf(googleApi.zak());
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(14, zaaf));
        return zaaf.a().a();
    }

    public final int a() {
        return this.g.getAndIncrement();
    }

    /* access modifiers changed from: package-private */
    public final boolean b(ConnectionResult connectionResult, int i2) {
        return this.e.a(this.d, connectionResult, i2);
    }

    public final void a(GoogleApi<?> googleApi) {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(7, googleApi));
    }

    public final void a(zaae zaae) {
        synchronized (p) {
            if (this.j != zaae) {
                this.j = zaae;
                this.k.clear();
            }
            this.k.addAll(zaae.h());
        }
    }

    public final Task<Map<zai<?>, String>> a(Iterable<? extends GoogleApi<?>> iterable) {
        zak zak = new zak(iterable);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(2, zak));
        return zak.a();
    }

    public final <O extends Api.ApiOptions> void a(GoogleApi<O> googleApi, int i2, BaseImplementation$ApiMethodImpl<? extends Result, Api.AnyClient> baseImplementation$ApiMethodImpl) {
        zae zae = new zae(i2, baseImplementation$ApiMethodImpl);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new zabv(zae, this.h.get(), googleApi)));
    }

    public final <O extends Api.ApiOptions, ResultT> void a(GoogleApi<O> googleApi, int i2, TaskApiCall<Api.AnyClient, ResultT> taskApiCall, TaskCompletionSource<ResultT> taskCompletionSource, StatusExceptionMapper statusExceptionMapper) {
        zag zag = new zag(i2, taskApiCall, taskCompletionSource, statusExceptionMapper);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new zabv(zag, this.h.get(), googleApi)));
    }

    public final <O extends Api.ApiOptions> Task<Void> a(GoogleApi<O> googleApi, RegisterListenerMethod<Api.AnyClient, ?> registerListenerMethod, UnregisterListenerMethod<Api.AnyClient, ?> unregisterListenerMethod) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        zaf zaf = new zaf(new zabw(registerListenerMethod, unregisterListenerMethod), taskCompletionSource);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(8, new zabv(zaf, this.h.get(), googleApi)));
        return taskCompletionSource.a();
    }

    public final <O extends Api.ApiOptions> Task<Boolean> a(GoogleApi<O> googleApi, ListenerHolder.ListenerKey<?> listenerKey) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        zah zah = new zah(listenerKey, taskCompletionSource);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(13, new zabv(zah, this.h.get(), googleApi)));
        return taskCompletionSource.a();
    }

    /* access modifiers changed from: package-private */
    public final PendingIntent a(zai<?> zai, int i2) {
        zad m2;
        zaa zaa2 = this.i.get(zai);
        if (zaa2 == null || (m2 = zaa2.m()) == null) {
            return null;
        }
        return PendingIntent.getActivity(this.d, i2, m2.getSignInIntent(), 134217728);
    }

    public final void a(ConnectionResult connectionResult, int i2) {
        if (!b(connectionResult, i2)) {
            Handler handler = this.m;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, connectionResult));
        }
    }
}
