package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zaad implements OnCompleteListener<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TaskCompletionSource f3905a;
    private final /* synthetic */ zaab b;

    zaad(zaab zaab, TaskCompletionSource taskCompletionSource) {
        this.b = zaab;
        this.f3905a = taskCompletionSource;
    }

    public final void a(Task<TResult> task) {
        this.b.b.remove(this.f3905a);
    }
}
