package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.internal.zac;
import com.google.android.gms.signin.internal.zaj;
import com.google.android.gms.signin.zaa;
import com.google.android.gms.signin.zad;
import java.util.Set;

public final class zace extends zac implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static Api.AbstractClientBuilder<? extends zad, SignInOptions> h = zaa.c;

    /* renamed from: a  reason: collision with root package name */
    private final Context f3934a;
    private final Handler b;
    private final Api.AbstractClientBuilder<? extends zad, SignInOptions> c;
    private Set<Scope> d;
    private ClientSettings e;
    private zad f;
    /* access modifiers changed from: private */
    public zach g;

    public zace(Context context, Handler handler, ClientSettings clientSettings) {
        this(context, handler, clientSettings, h);
    }

    public final void a(zach zach) {
        zad zad = this.f;
        if (zad != null) {
            zad.disconnect();
        }
        this.e.a(Integer.valueOf(System.identityHashCode(this)));
        Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder = this.c;
        Context context = this.f3934a;
        Looper looper = this.b.getLooper();
        ClientSettings clientSettings = this.e;
        this.f = (zad) abstractClientBuilder.buildClient(context, looper, clientSettings, clientSettings.i(), this, this);
        this.g = zach;
        Set<Scope> set = this.d;
        if (set == null || set.isEmpty()) {
            this.b.post(new zacf(this));
        } else {
            this.f.connect();
        }
    }

    public final zad b() {
        return this.f;
    }

    public final void onConnected(Bundle bundle) {
        this.f.a(this);
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.g.b(connectionResult);
    }

    public final void onConnectionSuspended(int i) {
        this.f.disconnect();
    }

    public final void w() {
        zad zad = this.f;
        if (zad != null) {
            zad.disconnect();
        }
    }

    public zace(Context context, Handler handler, ClientSettings clientSettings, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder) {
        this.f3934a = context;
        this.b = handler;
        Preconditions.a(clientSettings, (Object) "ClientSettings must not be null");
        this.e = clientSettings;
        this.d = clientSettings.h();
        this.c = abstractClientBuilder;
    }

    /* access modifiers changed from: private */
    public final void b(zaj zaj) {
        ConnectionResult s = zaj.s();
        if (s.w()) {
            ResolveAccountResponse t = zaj.t();
            ConnectionResult t2 = t.t();
            if (!t2.w()) {
                String valueOf = String.valueOf(t2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.g.b(t2);
                this.f.disconnect();
                return;
            }
            this.g.a(t.s(), this.d);
        } else {
            this.g.b(s);
        }
        this.f.disconnect();
    }

    public final void a(zaj zaj) {
        this.b.post(new zacg(this, zaj));
    }
}
