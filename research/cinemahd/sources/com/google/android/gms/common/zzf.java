package com.google.android.gms.common;

import java.lang.ref.WeakReference;

abstract class zzf extends zzd {
    private static final WeakReference<byte[]> c = new WeakReference<>((Object) null);
    private WeakReference<byte[]> b = c;

    zzf(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: package-private */
    public final byte[] b() {
        byte[] bArr;
        synchronized (this) {
            bArr = (byte[]) this.b.get();
            if (bArr == null) {
                bArr = zzd();
                this.b = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    /* access modifiers changed from: protected */
    public abstract byte[] zzd();
}
