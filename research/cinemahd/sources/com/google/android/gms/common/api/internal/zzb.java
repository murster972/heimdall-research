package com.google.android.gms.common.api.internal;

final class zzb implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ LifecycleCallback f3960a;
    private final /* synthetic */ String b;
    private final /* synthetic */ zza c;

    zzb(zza zza, LifecycleCallback lifecycleCallback, String str) {
        this.c = zza;
        this.f3960a = lifecycleCallback;
        this.b = str;
    }

    public final void run() {
        if (this.c.b > 0) {
            this.f3960a.a(this.c.c != null ? this.c.c.getBundle(this.b) : null);
        }
        if (this.c.b >= 2) {
            this.f3960a.d();
        }
        if (this.c.b >= 3) {
            this.f3960a.c();
        }
        if (this.c.b >= 4) {
            this.f3960a.e();
        }
        if (this.c.b >= 5) {
            this.f3960a.b();
        }
    }
}
