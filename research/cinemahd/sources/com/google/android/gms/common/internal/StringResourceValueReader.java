package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.common.R$string;

public class StringResourceValueReader {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f3996a;
    private final String b = this.f3996a.getResourcePackageName(R$string.common_google_play_services_unknown_issue);

    public StringResourceValueReader(Context context) {
        Preconditions.a(context);
        this.f3996a = context.getResources();
    }

    public String a(String str) {
        int identifier = this.f3996a.getIdentifier(str, "string", this.b);
        if (identifier == 0) {
            return null;
        }
        return this.f3996a.getString(identifier);
    }
}
