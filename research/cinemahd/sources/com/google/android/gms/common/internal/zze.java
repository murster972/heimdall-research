package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.internal.GmsClientSupervisor;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.internal.common.zzi;
import java.util.HashMap;

final class zze extends GmsClientSupervisor implements Handler.Callback {
    /* access modifiers changed from: private */
    public final HashMap<GmsClientSupervisor.zza, zzg> c = new HashMap<>();
    /* access modifiers changed from: private */
    public final Context d;
    /* access modifiers changed from: private */
    public final Handler e;
    /* access modifiers changed from: private */
    public final ConnectionTracker f;
    private final long g;
    /* access modifiers changed from: private */
    public final long h;

    zze(Context context) {
        this.d = context.getApplicationContext();
        this.e = new zzi(context.getMainLooper(), this);
        this.f = ConnectionTracker.a();
        this.g = 5000;
        this.h = 300000;
    }

    /* access modifiers changed from: protected */
    public final boolean a(GmsClientSupervisor.zza zza, ServiceConnection serviceConnection, String str) {
        boolean d2;
        Preconditions.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            zzg zzg = this.c.get(zza);
            if (zzg == null) {
                zzg = new zzg(this, zza);
                zzg.a(serviceConnection, serviceConnection, str);
                zzg.a(str);
                this.c.put(zza, zzg);
            } else {
                this.e.removeMessages(0, zza);
                if (!zzg.a(serviceConnection)) {
                    zzg.a(serviceConnection, serviceConnection, str);
                    int c2 = zzg.c();
                    if (c2 == 1) {
                        serviceConnection.onServiceConnected(zzg.b(), zzg.a());
                    } else if (c2 == 2) {
                        zzg.a(str);
                    }
                } else {
                    String valueOf = String.valueOf(zza);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d2 = zzg.d();
        }
        return d2;
    }

    /* access modifiers changed from: protected */
    public final void b(GmsClientSupervisor.zza zza, ServiceConnection serviceConnection, String str) {
        Preconditions.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            zzg zzg = this.c.get(zza);
            if (zzg == null) {
                String valueOf = String.valueOf(zza);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (zzg.a(serviceConnection)) {
                zzg.a(serviceConnection, str);
                if (zzg.e()) {
                    this.e.sendMessageDelayed(this.e.obtainMessage(0, zza), this.g);
                }
            } else {
                String valueOf2 = String.valueOf(zza);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            synchronized (this.c) {
                GmsClientSupervisor.zza zza = (GmsClientSupervisor.zza) message.obj;
                zzg zzg = this.c.get(zza);
                if (zzg != null && zzg.e()) {
                    if (zzg.d()) {
                        zzg.b("GmsClientSupervisor");
                    }
                    this.c.remove(zza);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            synchronized (this.c) {
                GmsClientSupervisor.zza zza2 = (GmsClientSupervisor.zza) message.obj;
                zzg zzg2 = this.c.get(zza2);
                if (zzg2 != null && zzg2.c() == 3) {
                    String valueOf = String.valueOf(zza2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = zzg2.b();
                    if (b == null) {
                        b = zza2.a();
                    }
                    if (b == null) {
                        b = new ComponentName(zza2.b(), "unknown");
                    }
                    zzg2.onServiceDisconnected(b);
                }
            }
            return true;
        }
    }
}
