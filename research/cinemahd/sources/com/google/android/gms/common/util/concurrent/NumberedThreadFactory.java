package com.google.android.gms.common.util.concurrent;

import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NumberedThreadFactory implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final String f4023a;
    private final AtomicInteger b;
    private final ThreadFactory c;

    public NumberedThreadFactory(String str) {
        this(str, 0);
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = this.c.newThread(new zza(runnable, 0));
        String str = this.f4023a;
        int andIncrement = this.b.getAndIncrement();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13);
        sb.append(str);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        newThread.setName(sb.toString());
        return newThread;
    }

    private NumberedThreadFactory(String str, int i) {
        this.b = new AtomicInteger();
        this.c = Executors.defaultThreadFactory();
        Preconditions.a(str, (Object) "Name must not be null");
        this.f4023a = str;
    }
}
