package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.List;

public final class WakeLockEvent extends StatsEvent {
    public static final Parcelable.Creator<WakeLockEvent> CREATOR = new zza();

    /* renamed from: a  reason: collision with root package name */
    private final int f4017a;
    private final long b;
    private int c;
    private final String d;
    private final String e;
    private final String f;
    private final int g;
    private final List<String> h;
    private final String i;
    private final long j;
    private int k;
    private final String l;
    private final float m;
    private final long n;
    private final boolean o;
    private long p = -1;

    WakeLockEvent(int i2, long j2, int i3, String str, int i4, List<String> list, String str2, long j3, int i5, String str3, String str4, float f2, long j4, String str5, boolean z) {
        this.f4017a = i2;
        this.b = j2;
        this.c = i3;
        this.d = str;
        this.e = str3;
        this.f = str5;
        this.g = i4;
        this.h = list;
        this.i = str2;
        this.j = j3;
        this.k = i5;
        this.l = str4;
        this.m = f2;
        this.n = j4;
        this.o = z;
    }

    public final int s() {
        return this.c;
    }

    public final long t() {
        return this.b;
    }

    public final long u() {
        return this.p;
    }

    public final String v() {
        String str;
        String str2 = this.d;
        int i2 = this.g;
        List<String> list = this.h;
        String str3 = "";
        if (list == null) {
            str = str3;
        } else {
            str = TextUtils.join(",", list);
        }
        int i3 = this.k;
        String str4 = this.e;
        if (str4 == null) {
            str4 = str3;
        }
        String str5 = this.l;
        if (str5 == null) {
            str5 = str3;
        }
        float f2 = this.m;
        String str6 = this.f;
        if (str6 != null) {
            str3 = str6;
        }
        boolean z = this.o;
        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 51 + String.valueOf(str).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(str3).length());
        sb.append("\t");
        sb.append(str2);
        sb.append("\t");
        sb.append(i2);
        sb.append("\t");
        sb.append(str);
        sb.append("\t");
        sb.append(i3);
        sb.append("\t");
        sb.append(str4);
        sb.append("\t");
        sb.append(str5);
        sb.append("\t");
        sb.append(f2);
        sb.append("\t");
        sb.append(str3);
        sb.append("\t");
        sb.append(z);
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4017a);
        SafeParcelWriter.a(parcel, 2, t());
        SafeParcelWriter.a(parcel, 4, this.d, false);
        SafeParcelWriter.a(parcel, 5, this.g);
        SafeParcelWriter.b(parcel, 6, this.h, false);
        SafeParcelWriter.a(parcel, 8, this.j);
        SafeParcelWriter.a(parcel, 10, this.e, false);
        SafeParcelWriter.a(parcel, 11, s());
        SafeParcelWriter.a(parcel, 12, this.i, false);
        SafeParcelWriter.a(parcel, 13, this.l, false);
        SafeParcelWriter.a(parcel, 14, this.k);
        SafeParcelWriter.a(parcel, 15, this.m);
        SafeParcelWriter.a(parcel, 16, this.n);
        SafeParcelWriter.a(parcel, 17, this.f, false);
        SafeParcelWriter.a(parcel, 18, this.o);
        SafeParcelWriter.a(parcel, a2);
    }
}
