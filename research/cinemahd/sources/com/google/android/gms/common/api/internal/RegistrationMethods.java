package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AnyClient;

public class RegistrationMethods<A extends Api.AnyClient, L> {

    /* renamed from: a  reason: collision with root package name */
    public final RegisterListenerMethod<A, L> f3902a;
    public final UnregisterListenerMethod<A, L> b;
}
