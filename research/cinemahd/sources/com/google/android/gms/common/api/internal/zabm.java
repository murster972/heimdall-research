package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zabm implements BaseGmsClient.SignOutCallbacks {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GoogleApiManager.zaa f3926a;

    zabm(GoogleApiManager.zaa zaa) {
        this.f3926a = zaa;
    }

    public final void a() {
        GoogleApiManager.this.m.post(new zabn(this));
    }
}
