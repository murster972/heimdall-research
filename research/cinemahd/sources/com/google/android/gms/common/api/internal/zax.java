package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.util.concurrent.HandlerExecutor;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class zax implements zabs {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Map<Api.AnyClientKey<?>, zaw<?>> f3957a = new HashMap();
    private final Map<Api.AnyClientKey<?>, zaw<?>> b = new HashMap();
    private final Map<Api<?>, Boolean> c;
    private final GoogleApiManager d;
    /* access modifiers changed from: private */
    public final zaaw e;
    /* access modifiers changed from: private */
    public final Lock f;
    private final Looper g;
    private final GoogleApiAvailabilityLight h;
    /* access modifiers changed from: private */
    public final Condition i;
    private final ClientSettings j;
    private final boolean k;
    /* access modifiers changed from: private */
    public final boolean l;
    private final Queue<BaseImplementation$ApiMethodImpl<?, ?>> m = new LinkedList();
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public Map<zai<?>, ConnectionResult> o;
    /* access modifiers changed from: private */
    public Map<zai<?>, ConnectionResult> p;
    private zaaa q;
    /* access modifiers changed from: private */
    public ConnectionResult r;

    public zax(Context context, Lock lock, Looper looper, GoogleApiAvailabilityLight googleApiAvailabilityLight, Map<Api.AnyClientKey<?>, Api.Client> map, ClientSettings clientSettings, Map<Api<?>, Boolean> map2, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder, ArrayList<zaq> arrayList, zaaw zaaw, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.f = lock;
        this.g = looper;
        this.i = lock.newCondition();
        this.h = googleApiAvailabilityLight;
        this.e = zaaw;
        this.c = map2;
        this.j = clientSettings;
        this.k = z;
        HashMap hashMap = new HashMap();
        for (Api next : map2.keySet()) {
            hashMap.put(next.a(), next);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            zaq zaq = arrayList.get(i2);
            i2++;
            zaq zaq2 = zaq;
            hashMap2.put(zaq2.f3952a, zaq2);
        }
        boolean z5 = true;
        boolean z6 = false;
        boolean z7 = true;
        boolean z8 = false;
        for (Map.Entry next2 : map.entrySet()) {
            Api api = (Api) hashMap.get(next2.getKey());
            Api.Client client = (Api.Client) next2.getValue();
            if (client.requiresGooglePlayServices()) {
                z3 = z7;
                z4 = !this.c.get(api).booleanValue() ? true : z8;
                z2 = true;
            } else {
                z2 = z6;
                z4 = z8;
                z3 = false;
            }
            zaw zaw = r1;
            zaw zaw2 = new zaw(context, api, looper, client, (zaq) hashMap2.get(api), clientSettings, abstractClientBuilder);
            this.f3957a.put((Api.AnyClientKey) next2.getKey(), zaw);
            if (client.requiresSignIn()) {
                this.b.put((Api.AnyClientKey) next2.getKey(), zaw);
            }
            z8 = z4;
            z7 = z3;
            z6 = z2;
        }
        this.l = (!z6 || z7 || z8) ? false : z5;
        this.d = GoogleApiManager.c();
    }

    private final <T extends BaseImplementation$ApiMethodImpl<? extends Result, ? extends Api.AnyClient>> boolean b(T t) {
        Api.AnyClientKey clientKey = t.getClientKey();
        ConnectionResult a2 = a((Api.AnyClientKey<?>) clientKey);
        if (a2 == null || a2.s() != 4) {
            return false;
        }
        t.setFailedResult(new Status(4, (String) null, this.d.a((zai<?>) this.f3957a.get(clientKey).zak(), System.identityHashCode(this.e))));
        return true;
    }

    /* access modifiers changed from: private */
    public final void c() {
        while (!this.m.isEmpty()) {
            a(this.m.remove());
        }
        this.e.a((Bundle) null);
    }

    /* access modifiers changed from: private */
    public final ConnectionResult d() {
        ConnectionResult connectionResult = null;
        ConnectionResult connectionResult2 = null;
        int i2 = 0;
        int i3 = 0;
        for (zaw next : this.f3957a.values()) {
            Api api = next.getApi();
            ConnectionResult connectionResult3 = this.o.get(next.zak());
            if (!connectionResult3.w() && (!this.c.get(api).booleanValue() || connectionResult3.v() || this.h.c(connectionResult3.s()))) {
                if (connectionResult3.s() != 4 || !this.k) {
                    int priority = api.c().getPriority();
                    if (connectionResult == null || i2 > priority) {
                        connectionResult = connectionResult3;
                        i2 = priority;
                    }
                } else {
                    int priority2 = api.c().getPriority();
                    if (connectionResult2 == null || i3 > priority2) {
                        connectionResult2 = connectionResult3;
                        i3 = priority2;
                    }
                }
            }
        }
        return (connectionResult == null || connectionResult2 == null || i2 <= i3) ? connectionResult : connectionResult2;
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t) {
        Api.AnyClientKey clientKey = t.getClientKey();
        if (this.k && b(t)) {
            return t;
        }
        this.e.y.a(t);
        return this.f3957a.get(clientKey).doWrite(t);
    }

    public final void a() {
    }

    public final void connect() {
        this.f.lock();
        try {
            if (!this.n) {
                this.n = true;
                this.o = null;
                this.p = null;
                this.q = null;
                this.r = null;
                this.d.b();
                this.d.a((Iterable<? extends GoogleApi<?>>) this.f3957a.values()).a((Executor) new HandlerExecutor(this.g), new zaz(this));
                this.f.unlock();
            }
        } finally {
            this.f.unlock();
        }
    }

    public final void disconnect() {
        this.f.lock();
        try {
            this.n = false;
            this.o = null;
            this.p = null;
            if (this.q == null) {
                this.r = null;
                while (!this.m.isEmpty()) {
                    BaseImplementation$ApiMethodImpl remove = this.m.remove();
                    remove.zaa((zacs) null);
                    remove.cancel();
                }
                this.i.signalAll();
                return;
            }
            this.q.a();
            throw null;
        } finally {
            this.f.unlock();
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public final boolean isConnected() {
        this.f.lock();
        try {
            return this.o != null && this.r == null;
        } finally {
            this.f.unlock();
        }
    }

    public final ConnectionResult a(Api<?> api) {
        return a(api.a());
    }

    private final ConnectionResult a(Api.AnyClientKey<?> anyClientKey) {
        this.f.lock();
        try {
            zaw zaw = this.f3957a.get(anyClientKey);
            if (this.o != null && zaw != null) {
                return this.o.get(zaw.zak());
            }
            this.f.unlock();
            return null;
        } finally {
            this.f.unlock();
        }
    }

    /* access modifiers changed from: private */
    public final void b() {
        ClientSettings clientSettings = this.j;
        if (clientSettings == null) {
            this.e.q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(clientSettings.h());
        Map<Api<?>, ClientSettings.OptionalApiSettings> e2 = this.j.e();
        for (Api next : e2.keySet()) {
            ConnectionResult a2 = a((Api<?>) next);
            if (a2 != null && a2.w()) {
                hashSet.addAll(e2.get(next).f3982a);
            }
        }
        this.e.q = hashSet;
    }

    /* access modifiers changed from: private */
    public final boolean a(zaw<?> zaw, ConnectionResult connectionResult) {
        return !connectionResult.w() && !connectionResult.v() && this.c.get(zaw.getApi()).booleanValue() && zaw.a().requiresGooglePlayServices() && this.h.c(connectionResult.s());
    }
}
