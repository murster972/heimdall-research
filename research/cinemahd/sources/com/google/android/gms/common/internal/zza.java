package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zza extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zza> CREATOR = new zzc();

    /* renamed from: a  reason: collision with root package name */
    Bundle f4002a;
    Feature[] b;
    private int c;

    zza(Bundle bundle, Feature[] featureArr, int i) {
        this.f4002a = bundle;
        this.b = featureArr;
        this.c = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4002a, false);
        SafeParcelWriter.a(parcel, 2, (T[]) this.b, i, false);
        SafeParcelWriter.a(parcel, 3, this.c);
        SafeParcelWriter.a(parcel, a2);
    }

    public zza() {
    }
}
