package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.Collections;

final class zabo implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ConnectionResult f3928a;
    private final /* synthetic */ GoogleApiManager.zac b;

    zabo(GoogleApiManager.zac zac, ConnectionResult connectionResult) {
        this.b = zac;
        this.f3928a = connectionResult;
    }

    public final void run() {
        if (this.f3928a.w()) {
            boolean unused = this.b.e = true;
            if (this.b.f3894a.requiresSignIn()) {
                this.b.a();
                return;
            }
            try {
                this.b.f3894a.getRemoteService((IAccountAccessor) null, Collections.emptySet());
            } catch (SecurityException unused2) {
                ((GoogleApiManager.zaa) GoogleApiManager.this.i.get(this.b.b)).onConnectionFailed(new ConnectionResult(10));
            }
        } else {
            ((GoogleApiManager.zaa) GoogleApiManager.this.i.get(this.b.b)).onConnectionFailed(this.f3928a);
        }
    }
}
