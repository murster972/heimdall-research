package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class zav implements zabt {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zas f3955a;

    private zav(zas zas) {
        this.f3955a = zas;
    }

    public final void a(Bundle bundle) {
        this.f3955a.l.lock();
        try {
            ConnectionResult unused = this.f3955a.j = ConnectionResult.e;
            this.f3955a.c();
        } finally {
            this.f3955a.l.unlock();
        }
    }

    /* synthetic */ zav(zas zas, zat zat) {
        this(zas);
    }

    public final void a(ConnectionResult connectionResult) {
        this.f3955a.l.lock();
        try {
            ConnectionResult unused = this.f3955a.j = connectionResult;
            this.f3955a.c();
        } finally {
            this.f3955a.l.unlock();
        }
    }

    public final void a(int i, boolean z) {
        this.f3955a.l.lock();
        try {
            if (this.f3955a.k) {
                boolean unused = this.f3955a.k = false;
                this.f3955a.a(i, z);
                return;
            }
            boolean unused2 = this.f3955a.k = true;
            this.f3955a.c.onConnectionSuspended(i);
            this.f3955a.l.unlock();
        } finally {
            this.f3955a.l.unlock();
        }
    }
}
