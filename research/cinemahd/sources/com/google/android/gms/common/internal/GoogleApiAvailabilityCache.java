package com.google.android.gms.common.internal;

import android.content.Context;
import android.util.SparseIntArray;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;

public class GoogleApiAvailabilityCache {

    /* renamed from: a  reason: collision with root package name */
    private final SparseIntArray f3988a;
    private GoogleApiAvailabilityLight b;

    public GoogleApiAvailabilityCache() {
        this(GoogleApiAvailability.a());
    }

    public int a(Context context, Api.Client client) {
        Preconditions.a(context);
        Preconditions.a(client);
        if (!client.requiresGooglePlayServices()) {
            return 0;
        }
        int minApkVersion = client.getMinApkVersion();
        int i = this.f3988a.get(minApkVersion, -1);
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        while (true) {
            if (i2 < this.f3988a.size()) {
                int keyAt = this.f3988a.keyAt(i2);
                if (keyAt > minApkVersion && this.f3988a.get(keyAt) == 0) {
                    i = 0;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        if (i == -1) {
            i = this.b.a(context, minApkVersion);
        }
        this.f3988a.put(minApkVersion, i);
        return i;
    }

    public GoogleApiAvailabilityCache(GoogleApiAvailabilityLight googleApiAvailabilityLight) {
        this.f3988a = new SparseIntArray();
        Preconditions.a(googleApiAvailabilityLight);
        this.b = googleApiAvailabilityLight;
    }

    public void a() {
        this.f3988a.clear();
    }
}
