package com.google.android.gms.common.api.internal;

import android.os.IBinder;
import com.google.android.gms.common.api.zac;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;

final class zacr implements IBinder.DeathRecipient, zacs {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<BasePendingResult<?>> f3942a;
    private final WeakReference<zac> b;
    private final WeakReference<IBinder> c;

    private zacr(BasePendingResult<?> basePendingResult, zac zac, IBinder iBinder) {
        this.b = new WeakReference<>(zac);
        this.f3942a = new WeakReference<>(basePendingResult);
        this.c = new WeakReference<>(iBinder);
    }

    public final void a(BasePendingResult<?> basePendingResult) {
        a();
    }

    public final void binderDied() {
        a();
    }

    private final void a() {
        BasePendingResult basePendingResult = (BasePendingResult) this.f3942a.get();
        zac zac = (zac) this.b.get();
        if (!(zac == null || basePendingResult == null)) {
            zac.a(basePendingResult.zam().intValue());
        }
        IBinder iBinder = (IBinder) this.c.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException unused) {
            }
        }
    }

    /* synthetic */ zacr(BasePendingResult basePendingResult, zac zac, IBinder iBinder, zacq zacq) {
        this(basePendingResult, (zac) null, iBinder);
    }
}
