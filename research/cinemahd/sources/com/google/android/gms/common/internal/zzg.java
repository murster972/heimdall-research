package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.internal.GmsClientSupervisor;
import com.google.android.gms.common.stats.ConnectionTracker;
import java.util.HashMap;
import java.util.Map;

final class zzg implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private final Map<ServiceConnection, ServiceConnection> f4003a = new HashMap();
    private int b = 2;
    private boolean c;
    private IBinder d;
    private final GmsClientSupervisor.zza e;
    private ComponentName f;
    private final /* synthetic */ zze g;

    public zzg(zze zze, GmsClientSupervisor.zza zza) {
        this.g = zze;
        this.e = zza;
    }

    public final void a(String str) {
        this.b = 3;
        this.c = this.g.f.a(this.g.d, str, this.e.a(this.g.d), this, this.e.c());
        if (this.c) {
            this.g.e.sendMessageDelayed(this.g.e.obtainMessage(1, this.e), this.g.h);
            return;
        }
        this.b = 2;
        try {
            this.g.f.a(this.g.d, this);
        } catch (IllegalArgumentException unused) {
        }
    }

    public final void b(String str) {
        this.g.e.removeMessages(1, this.e);
        this.g.f.a(this.g.d, this);
        this.c = false;
        this.b = 2;
    }

    public final int c() {
        return this.b;
    }

    public final boolean d() {
        return this.c;
    }

    public final boolean e() {
        return this.f4003a.isEmpty();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.g.c) {
            this.g.e.removeMessages(1, this.e);
            this.d = iBinder;
            this.f = componentName;
            for (ServiceConnection onServiceConnected : this.f4003a.values()) {
                onServiceConnected.onServiceConnected(componentName, iBinder);
            }
            this.b = 1;
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.g.c) {
            this.g.e.removeMessages(1, this.e);
            this.d = null;
            this.f = componentName;
            for (ServiceConnection onServiceDisconnected : this.f4003a.values()) {
                onServiceDisconnected.onServiceDisconnected(componentName);
            }
            this.b = 2;
        }
    }

    public final ComponentName b() {
        return this.f;
    }

    public final void a(ServiceConnection serviceConnection, ServiceConnection serviceConnection2, String str) {
        ConnectionTracker unused = this.g.f;
        Context unused2 = this.g.d;
        this.e.a(this.g.d);
        this.f4003a.put(serviceConnection, serviceConnection2);
    }

    public final void a(ServiceConnection serviceConnection, String str) {
        ConnectionTracker unused = this.g.f;
        Context unused2 = this.g.d;
        this.f4003a.remove(serviceConnection);
    }

    public final boolean a(ServiceConnection serviceConnection) {
        return this.f4003a.containsKey(serviceConnection);
    }

    public final IBinder a() {
        return this.d;
    }
}
