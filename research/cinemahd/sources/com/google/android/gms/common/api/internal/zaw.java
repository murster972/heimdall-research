package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;

public final class zaw<O extends Api.ApiOptions> extends GoogleApi<O> {

    /* renamed from: a  reason: collision with root package name */
    private final Api.Client f3956a;
    private final zaq b;
    private final ClientSettings c;
    private final Api.AbstractClientBuilder<? extends zad, SignInOptions> d;

    public zaw(Context context, Api<O> api, Looper looper, Api.Client client, zaq zaq, ClientSettings clientSettings, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder) {
        super(context, api, looper);
        this.f3956a = client;
        this.b = zaq;
        this.c = clientSettings;
        this.d = abstractClientBuilder;
        this.zabm.a((GoogleApi<?>) this);
    }

    public final Api.Client a() {
        return this.f3956a;
    }

    public final Api.Client zaa(Looper looper, GoogleApiManager.zaa<O> zaa) {
        this.b.a(zaa);
        return this.f3956a;
    }

    public final zace zaa(Context context, Handler handler) {
        return new zace(context, handler, this.c, this.d);
    }
}
