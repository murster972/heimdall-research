package com.google.android.gms.common.api.internal;

abstract class zabf {

    /* renamed from: a  reason: collision with root package name */
    private final zabd f3919a;

    protected zabf(zabd zabd) {
        this.f3919a = zabd;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void a(zabe zabe) {
        zabe.f3918a.lock();
        try {
            if (zabe.k == this.f3919a) {
                a();
                zabe.f3918a.unlock();
            }
        } finally {
            zabe.f3918a.unlock();
        }
    }
}
