package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.internal.zaj;
import com.google.android.gms.signin.zad;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public final class zaak implements zabd {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final zabe f3908a;
    /* access modifiers changed from: private */
    public final Lock b;
    /* access modifiers changed from: private */
    public final Context c;
    /* access modifiers changed from: private */
    public final GoogleApiAvailabilityLight d;
    private ConnectionResult e;
    private int f;
    private int g = 0;
    private int h;
    private final Bundle i = new Bundle();
    private final Set<Api.AnyClientKey> j = new HashSet();
    /* access modifiers changed from: private */
    public zad k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public IAccountAccessor o;
    private boolean p;
    private boolean q;
    private final ClientSettings r;
    private final Map<Api<?>, Boolean> s;
    private final Api.AbstractClientBuilder<? extends zad, SignInOptions> t;
    private ArrayList<Future<?>> u = new ArrayList<>();

    public zaak(zabe zabe, ClientSettings clientSettings, Map<Api<?>, Boolean> map, GoogleApiAvailabilityLight googleApiAvailabilityLight, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder, Lock lock, Context context) {
        this.f3908a = zabe;
        this.r = clientSettings;
        this.s = map;
        this.d = googleApiAvailabilityLight;
        this.t = abstractClientBuilder;
        this.b = lock;
        this.c = context;
    }

    /* access modifiers changed from: private */
    public final boolean a() {
        this.h--;
        int i2 = this.h;
        if (i2 > 0) {
            return false;
        }
        if (i2 < 0) {
            Log.w("GoogleApiClientConnecting", this.f3908a.m.i());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            b(new ConnectionResult(8, (PendingIntent) null));
            return false;
        }
        ConnectionResult connectionResult = this.e;
        if (connectionResult == null) {
            return true;
        }
        this.f3908a.l = this.f;
        b(connectionResult);
        return false;
    }

    private static String b(int i2) {
        return i2 != 0 ? i2 != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    /* access modifiers changed from: private */
    public final void b() {
        if (this.h == 0) {
            if (!this.m || this.n) {
                ArrayList arrayList = new ArrayList();
                this.g = 1;
                this.h = this.f3908a.f.size();
                for (Api.AnyClientKey next : this.f3908a.f.keySet()) {
                    if (!this.f3908a.g.containsKey(next)) {
                        arrayList.add(this.f3908a.f.get(next));
                    } else if (a()) {
                        c();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.u.add(zabh.a().submit(new zaaq(this, arrayList)));
                }
            }
        }
    }

    private final void c() {
        this.f3908a.c();
        zabh.a().execute(new zaal(this));
        zad zad = this.k;
        if (zad != null) {
            if (this.p) {
                zad.a(this.o, this.q);
            }
            a(false);
        }
        for (Api.AnyClientKey<?> anyClientKey : this.f3908a.g.keySet()) {
            this.f3908a.f.get(anyClientKey).disconnect();
        }
        this.f3908a.n.a(this.i.isEmpty() ? null : this.i);
    }

    /* access modifiers changed from: private */
    public final void d() {
        this.m = false;
        this.f3908a.m.q = Collections.emptySet();
        for (Api.AnyClientKey next : this.j) {
            if (!this.f3908a.g.containsKey(next)) {
                this.f3908a.g.put(next, new ConnectionResult(17, (PendingIntent) null));
            }
        }
    }

    private final void e() {
        ArrayList<Future<?>> arrayList = this.u;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Future<?> future = arrayList.get(i2);
            i2++;
            future.cancel(true);
        }
        this.u.clear();
    }

    /* access modifiers changed from: private */
    public final Set<Scope> f() {
        ClientSettings clientSettings = this.r;
        if (clientSettings == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(clientSettings.h());
        Map<Api<?>, ClientSettings.OptionalApiSettings> e2 = this.r.e();
        for (Api next : e2.keySet()) {
            if (!this.f3908a.g.containsKey(next.a())) {
                hashSet.addAll(e2.get(next).f3982a);
            }
        }
        return hashSet;
    }

    public final void begin() {
        this.f3908a.g.clear();
        this.m = false;
        this.e = null;
        this.g = 0;
        this.l = true;
        this.n = false;
        this.p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (Api next : this.s.keySet()) {
            Api.Client client = this.f3908a.f.get(next.a());
            z |= next.c().getPriority() == 1;
            boolean booleanValue = this.s.get(next).booleanValue();
            if (client.requiresSignIn()) {
                this.m = true;
                if (booleanValue) {
                    this.j.add(next.a());
                } else {
                    this.l = false;
                }
            }
            hashMap.put(client, new zaam(this, next, booleanValue));
        }
        if (z) {
            this.m = false;
        }
        if (this.m) {
            this.r.a(Integer.valueOf(System.identityHashCode(this.f3908a.m)));
            zaat zaat = new zaat(this, (zaal) null);
            Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder = this.t;
            Context context = this.c;
            Looper d2 = this.f3908a.m.d();
            ClientSettings clientSettings = this.r;
            this.k = (zad) abstractClientBuilder.buildClient(context, d2, clientSettings, clientSettings.i(), zaat, zaat);
        }
        this.h = this.f3908a.f.size();
        this.u.add(zabh.a().submit(new zaan(this, hashMap)));
    }

    public final void connect() {
    }

    public final boolean disconnect() {
        e();
        a(true);
        this.f3908a.a((ConnectionResult) null);
        return true;
    }

    public final void onConnected(Bundle bundle) {
        if (a(1)) {
            if (bundle != null) {
                this.i.putAll(bundle);
            }
            if (a()) {
                c();
            }
        }
    }

    public final void onConnectionSuspended(int i2) {
        b(new ConnectionResult(8, (PendingIntent) null));
    }

    /* access modifiers changed from: private */
    public final void a(zaj zaj) {
        if (a(0)) {
            ConnectionResult s2 = zaj.s();
            if (s2.w()) {
                ResolveAccountResponse t2 = zaj.t();
                ConnectionResult t3 = t2.t();
                if (!t3.w()) {
                    String valueOf = String.valueOf(t3);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GoogleApiClientConnecting", sb.toString(), new Exception());
                    b(t3);
                    return;
                }
                this.n = true;
                this.o = t2.s();
                this.p = t2.u();
                this.q = t2.v();
                b();
            } else if (a(s2)) {
                d();
                b();
            } else {
                b(s2);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r7 != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.google.android.gms.common.ConnectionResult r5, com.google.android.gms.common.api.Api<?> r6, boolean r7) {
        /*
            r4 = this;
            com.google.android.gms.common.api.Api$BaseClientBuilder r0 = r6.c()
            int r0 = r0.getPriority()
            r1 = 0
            r2 = 1
            if (r7 == 0) goto L_0x0024
            boolean r7 = r5.v()
            if (r7 == 0) goto L_0x0014
        L_0x0012:
            r7 = 1
            goto L_0x0022
        L_0x0014:
            com.google.android.gms.common.GoogleApiAvailabilityLight r7 = r4.d
            int r3 = r5.s()
            android.content.Intent r7 = r7.a((int) r3)
            if (r7 == 0) goto L_0x0021
            goto L_0x0012
        L_0x0021:
            r7 = 0
        L_0x0022:
            if (r7 == 0) goto L_0x002d
        L_0x0024:
            com.google.android.gms.common.ConnectionResult r7 = r4.e
            if (r7 == 0) goto L_0x002c
            int r7 = r4.f
            if (r0 >= r7) goto L_0x002d
        L_0x002c:
            r1 = 1
        L_0x002d:
            if (r1 == 0) goto L_0x0033
            r4.e = r5
            r4.f = r0
        L_0x0033:
            com.google.android.gms.common.api.internal.zabe r7 = r4.f3908a
            java.util.Map<com.google.android.gms.common.api.Api$AnyClientKey<?>, com.google.android.gms.common.ConnectionResult> r7 = r7.g
            com.google.android.gms.common.api.Api$AnyClientKey r6 = r6.a()
            r7.put(r6, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zaak.b(com.google.android.gms.common.ConnectionResult, com.google.android.gms.common.api.Api, boolean):void");
    }

    /* access modifiers changed from: private */
    public final void b(ConnectionResult connectionResult) {
        e();
        a(!connectionResult.v());
        this.f3908a.a(connectionResult);
        this.f3908a.n.a(connectionResult);
    }

    public final void a(ConnectionResult connectionResult, Api<?> api, boolean z) {
        if (a(1)) {
            b(connectionResult, api, z);
            if (a()) {
                c();
            }
        }
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t2) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    /* access modifiers changed from: private */
    public final boolean a(ConnectionResult connectionResult) {
        return this.l && !connectionResult.v();
    }

    private final void a(boolean z) {
        zad zad = this.k;
        if (zad != null) {
            if (zad.isConnected() && z) {
                this.k.a();
            }
            this.k.disconnect();
            this.o = null;
        }
    }

    /* access modifiers changed from: private */
    public final boolean a(int i2) {
        if (this.g == i2) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.f3908a.m.i());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GoogleApiClientConnecting", sb.toString());
        int i3 = this.h;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i3);
        Log.w("GoogleApiClientConnecting", sb2.toString());
        String b2 = b(this.g);
        String b3 = b(i2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(b2).length() + 70 + String.valueOf(b3).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(b2);
        sb3.append(" but received callback for step ");
        sb3.append(b3);
        Log.wtf("GoogleApiClientConnecting", sb3.toString(), new Exception());
        b(new ConnectionResult(8, (PendingIntent) null));
        return false;
    }
}
