package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class zaab {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Map<BasePendingResult<?>, Boolean> f3903a = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */
    public final Map<TaskCompletionSource<?>, Boolean> b = Collections.synchronizedMap(new WeakHashMap());

    /* access modifiers changed from: package-private */
    public final void a(BasePendingResult<? extends Result> basePendingResult, boolean z) {
        this.f3903a.put(basePendingResult, Boolean.valueOf(z));
        basePendingResult.addStatusListener(new zaac(this, basePendingResult));
    }

    public final void b() {
        a(false, GoogleApiManager.n);
    }

    public final void c() {
        a(true, zacp.d);
    }

    /* access modifiers changed from: package-private */
    public final <TResult> void a(TaskCompletionSource<TResult> taskCompletionSource, boolean z) {
        this.b.put(taskCompletionSource, Boolean.valueOf(z));
        taskCompletionSource.a().a(new zaad(this, taskCompletionSource));
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return !this.f3903a.isEmpty() || !this.b.isEmpty();
    }

    private final void a(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.f3903a) {
            hashMap = new HashMap(this.f3903a);
        }
        synchronized (this.b) {
            hashMap2 = new HashMap(this.b);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).zab(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((TaskCompletionSource) entry2.getKey()).b((Exception) new ApiException(status));
            }
        }
    }
}
