package com.google.android.gms.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;

public class GoogleSignatureVerifier {
    private static GoogleSignatureVerifier b;

    /* renamed from: a  reason: collision with root package name */
    private final Context f3878a;

    private GoogleSignatureVerifier(Context context) {
        this.f3878a = context.getApplicationContext();
    }

    public static GoogleSignatureVerifier a(Context context) {
        Preconditions.a(context);
        synchronized (GoogleSignatureVerifier.class) {
            if (b == null) {
                zzc.a(context);
                b = new GoogleSignatureVerifier(context);
            }
        }
        return b;
    }

    public static boolean a(PackageInfo packageInfo, boolean z) {
        zzd zzd;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                zzd = a(packageInfo, zzi.f4030a);
            } else {
                zzd = a(packageInfo, zzi.f4030a[0]);
            }
            if (zzd != null) {
                return true;
            }
        }
        return false;
    }

    public boolean a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (a(packageInfo, false)) {
            return true;
        }
        if (a(packageInfo, true)) {
            if (GooglePlayServicesUtilLight.honorsDebugCertificates(this.f3878a)) {
                return true;
            }
            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        }
        return false;
    }

    private static zzd a(PackageInfo packageInfo, zzd... zzdArr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        zzg zzg = new zzg(signatureArr[0].toByteArray());
        for (int i = 0; i < zzdArr.length; i++) {
            if (zzdArr[i].equals(zzg)) {
                return zzdArr[i];
            }
        }
        return null;
    }
}
