package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.server.response.FastJsonResponse;

public final class zam extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zam> CREATOR = new zaj();

    /* renamed from: a  reason: collision with root package name */
    private final int f4015a;
    final String b;
    final FastJsonResponse.Field<?, ?> c;

    zam(int i, String str, FastJsonResponse.Field<?, ?> field) {
        this.f4015a = i;
        this.b = str;
        this.c = field;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4015a);
        SafeParcelWriter.a(parcel, 2, this.b, false);
        SafeParcelWriter.a(parcel, 3, (Parcelable) this.c, i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    zam(String str, FastJsonResponse.Field<?, ?> field) {
        this.f4015a = 1;
        this.b = str;
        this.c = field;
    }
}
