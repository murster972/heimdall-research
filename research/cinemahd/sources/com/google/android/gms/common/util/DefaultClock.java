package com.google.android.gms.common.util;

import android.os.SystemClock;

public class DefaultClock implements Clock {

    /* renamed from: a  reason: collision with root package name */
    private static final DefaultClock f4018a = new DefaultClock();

    private DefaultClock() {
    }

    public static Clock c() {
        return f4018a;
    }

    public long a() {
        return SystemClock.elapsedRealtime();
    }

    public long b() {
        return System.currentTimeMillis();
    }

    public long nanoTime() {
        return System.nanoTime();
    }
}
