package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class zan implements Parcelable.Creator<zak> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ArrayList<zal> arrayList = null;
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 2) {
                arrayList = SafeParcelReader.c(parcel, a2, zal.CREATOR);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                str = SafeParcelReader.o(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zak(i, arrayList, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zak[i];
    }
}
