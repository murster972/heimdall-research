package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import java.util.ArrayList;

final class zaaq extends zaau {
    private final ArrayList<Api.Client> b;
    private final /* synthetic */ zaak c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zaaq(zaak zaak, ArrayList<Api.Client> arrayList) {
        super(zaak, (zaal) null);
        this.c = zaak;
        this.b = arrayList;
    }

    public final void a() {
        this.c.f3908a.m.q = this.c.f();
        ArrayList<Api.Client> arrayList = this.b;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Api.Client client = arrayList.get(i);
            i++;
            client.getRemoteService(this.c.o, this.c.f3908a.m.q);
        }
    }
}
