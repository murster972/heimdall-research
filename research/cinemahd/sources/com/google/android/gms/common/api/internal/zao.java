package com.google.android.gms.common.api.internal;

import android.app.Dialog;

final class zao extends zabr {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Dialog f3951a;
    private final /* synthetic */ zan b;

    zao(zan zan, Dialog dialog) {
        this.b = zan;
        this.f3951a = dialog;
    }

    public final void a() {
        this.b.b.g();
        if (this.f3951a.isShowing()) {
            this.f3951a.dismiss();
        }
    }
}
