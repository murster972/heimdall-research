package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Result;

public final class zabp<O extends Api.ApiOptions> extends zaag {
    private final GoogleApi<O> c;

    public zabp(GoogleApi<O> googleApi) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.c = googleApi;
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t) {
        return this.c.doWrite(t);
    }

    public final void a(zacm zacm) {
    }

    public final void b(zacm zacm) {
    }

    public final Context c() {
        return this.c.getApplicationContext();
    }

    public final Looper d() {
        return this.c.getLooper();
    }
}
