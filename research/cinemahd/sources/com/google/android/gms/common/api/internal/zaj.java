package com.google.android.gms.common.api.internal;

import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class zaj extends zal {
    private final SparseArray<zaa> f = new SparseArray<>();

    private class zaa implements GoogleApiClient.OnConnectionFailedListener {

        /* renamed from: a  reason: collision with root package name */
        public final int f3947a;
        public final GoogleApiClient b;
        public final GoogleApiClient.OnConnectionFailedListener c;

        public zaa(int i, GoogleApiClient googleApiClient, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            this.f3947a = i;
            this.b = googleApiClient;
            this.c = onConnectionFailedListener;
            googleApiClient.a((GoogleApiClient.OnConnectionFailedListener) this);
        }

        public final void onConnectionFailed(ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            zaj.this.b(connectionResult, this.f3947a);
        }
    }

    private zaj(LifecycleFragment lifecycleFragment) {
        super(lifecycleFragment);
        this.f3897a.a("AutoManageHelper", (LifecycleCallback) this);
    }

    public static zaj b(LifecycleActivity lifecycleActivity) {
        LifecycleFragment a2 = LifecycleCallback.a(lifecycleActivity);
        zaj zaj = (zaj) a2.a("AutoManageHelper", zaj.class);
        if (zaj != null) {
            return zaj;
        }
        return new zaj(a2);
    }

    public final void a(int i, GoogleApiClient googleApiClient, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Preconditions.a(googleApiClient, (Object) "GoogleApiClient instance cannot be null");
        boolean z = this.f.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        Preconditions.b(z, sb.toString());
        zam zam = this.c.get();
        boolean z2 = this.b;
        String valueOf = String.valueOf(zam);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.f.put(i, new zaa(i, googleApiClient, onConnectionFailedListener));
        if (this.b && zam == null) {
            String valueOf2 = String.valueOf(googleApiClient);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            googleApiClient.a();
        }
    }

    public void d() {
        super.d();
        boolean z = this.b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (this.c.get() == null) {
            for (int i = 0; i < this.f.size(); i++) {
                zaa b = b(i);
                if (b != null) {
                    b.b.a();
                }
            }
        }
    }

    public void e() {
        super.e();
        for (int i = 0; i < this.f.size(); i++) {
            zaa b = b(i);
            if (b != null) {
                b.b.b();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        for (int i = 0; i < this.f.size(); i++) {
            zaa b = b(i);
            if (b != null) {
                b.b.a();
            }
        }
    }

    private final zaa b(int i) {
        if (this.f.size() <= i) {
            return null;
        }
        SparseArray<zaa> sparseArray = this.f;
        return sparseArray.get(sparseArray.keyAt(i));
    }

    public final void a(int i) {
        zaa zaa2 = this.f.get(i);
        this.f.remove(i);
        if (zaa2 != null) {
            zaa2.b.b((GoogleApiClient.OnConnectionFailedListener) zaa2);
            zaa2.b.b();
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.f.size(); i++) {
            zaa b = b(i);
            if (b != null) {
                printWriter.append(str).append("GoogleApiClient #").print(b.f3947a);
                printWriter.println(":");
                b.b.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(ConnectionResult connectionResult, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        zaa zaa2 = this.f.get(i);
        if (zaa2 != null) {
            a(i);
            GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener = zaa2.c;
            if (onConnectionFailedListener != null) {
                onConnectionFailedListener.onConnectionFailed(connectionResult);
            }
        }
    }
}
