package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zaf implements BaseGmsClient.BaseConnectionCallbacks {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ GoogleApiClient.ConnectionCallbacks f4000a;

    zaf(GoogleApiClient.ConnectionCallbacks connectionCallbacks) {
        this.f4000a = connectionCallbacks;
    }

    public final void onConnected(Bundle bundle) {
        this.f4000a.onConnected(bundle);
    }

    public final void onConnectionSuspended(int i) {
        this.f4000a.onConnectionSuspended(i);
    }
}
