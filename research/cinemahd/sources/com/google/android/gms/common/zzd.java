package com.google.android.gms.common;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.zzi;
import com.google.android.gms.common.internal.zzk;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

abstract class zzd extends zzk {

    /* renamed from: a  reason: collision with root package name */
    private int f4029a;

    protected zzd(byte[] bArr) {
        Preconditions.a(bArr.length == 25);
        this.f4029a = Arrays.hashCode(bArr);
    }

    protected static byte[] b(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract byte[] b();

    public boolean equals(Object obj) {
        IObjectWrapper zzb;
        if (obj != null && (obj instanceof zzi)) {
            try {
                zzi zzi = (zzi) obj;
                if (zzi.zzc() != hashCode() || (zzb = zzi.zzb()) == null) {
                    return false;
                }
                return Arrays.equals(b(), (byte[]) ObjectWrapper.a(zzb));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            }
        }
        return false;
    }

    public int hashCode() {
        return this.f4029a;
    }

    public final IObjectWrapper zzb() {
        return ObjectWrapper.a(b());
    }

    public final int zzc() {
        return hashCode();
    }
}
