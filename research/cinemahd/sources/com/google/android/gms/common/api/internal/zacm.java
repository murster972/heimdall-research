package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.internal.Preconditions;
import java.lang.ref.WeakReference;

public final class zacm<R extends Result> extends TransformedResult<R> implements ResultCallback<R> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ResultTransform<? super R, ? extends Result> f3937a = null;
    /* access modifiers changed from: private */
    public zacm<? extends Result> b = null;
    private volatile ResultCallbacks<? super R> c = null;
    private PendingResult<R> d = null;
    /* access modifiers changed from: private */
    public final Object e = new Object();
    private Status f = null;
    /* access modifiers changed from: private */
    public final WeakReference<GoogleApiClient> g;
    /* access modifiers changed from: private */
    public final zaco h;
    private boolean i = false;

    public zacm(WeakReference<GoogleApiClient> weakReference) {
        Preconditions.a(weakReference, (Object) "GoogleApiClient reference must not be null");
        this.g = weakReference;
        GoogleApiClient googleApiClient = (GoogleApiClient) this.g.get();
        this.h = new zaco(this, googleApiClient != null ? googleApiClient.d() : Looper.getMainLooper());
    }

    private final void b() {
        if (this.f3937a != null || this.c != null) {
            GoogleApiClient googleApiClient = (GoogleApiClient) this.g.get();
            if (!(this.i || this.f3937a == null || googleApiClient == null)) {
                googleApiClient.a(this);
                this.i = true;
            }
            Status status = this.f;
            if (status != null) {
                b(status);
                return;
            }
            PendingResult<R> pendingResult = this.d;
            if (pendingResult != null) {
                pendingResult.setResultCallback(this);
            }
        }
    }

    private final boolean c() {
        return (this.c == null || ((GoogleApiClient) this.g.get()) == null) ? false : true;
    }

    public final <S extends Result> TransformedResult<S> a(ResultTransform<? super R, ? extends S> resultTransform) {
        zacm<? extends Result> zacm;
        synchronized (this.e) {
            boolean z = true;
            Preconditions.b(this.f3937a == null, "Cannot call then() twice.");
            if (this.c != null) {
                z = false;
            }
            Preconditions.b(z, "Cannot call then() and andFinally() on the same TransformedResult.");
            this.f3937a = resultTransform;
            zacm = new zacm<>(this.g);
            this.b = zacm;
            b();
        }
        return zacm;
    }

    public final void onResult(R r) {
        synchronized (this.e) {
            if (!r.getStatus().v()) {
                a(r.getStatus());
                a((Result) r);
            } else if (this.f3937a != null) {
                zacc.a().submit(new zacn(this, r));
            } else if (c()) {
                this.c.a(r);
            }
        }
    }

    private final void b(Status status) {
        synchronized (this.e) {
            if (this.f3937a != null) {
                Status a2 = this.f3937a.a(status);
                Preconditions.a(a2, (Object) "onFailure must not return null");
                this.b.a(a2);
            } else if (c()) {
                this.c.a(status);
            }
        }
    }

    public final void a(PendingResult<?> pendingResult) {
        synchronized (this.e) {
            this.d = pendingResult;
            b();
        }
    }

    /* access modifiers changed from: private */
    public final void a(Status status) {
        synchronized (this.e) {
            this.f = status;
            b(this.f);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = null;
    }

    /* access modifiers changed from: private */
    public static void a(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).release();
            } catch (RuntimeException e2) {
                String valueOf = String.valueOf(result);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e2);
            }
        }
    }
}
