package com.google.android.gms.common.wrappers;

import android.content.Context;

public class Wrappers {
    private static Wrappers b = new Wrappers();

    /* renamed from: a  reason: collision with root package name */
    private PackageManagerWrapper f4027a = null;

    public static PackageManagerWrapper a(Context context) {
        return b.b(context);
    }

    private final synchronized PackageManagerWrapper b(Context context) {
        if (this.f4027a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.f4027a = new PackageManagerWrapper(context);
        }
        return this.f4027a;
    }
}
