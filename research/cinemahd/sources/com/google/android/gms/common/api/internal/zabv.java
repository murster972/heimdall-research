package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.GoogleApi;

public final class zabv {

    /* renamed from: a  reason: collision with root package name */
    public final zab f3930a;
    public final int b;
    public final GoogleApi<?> c;

    public zabv(zab zab, int i, GoogleApi<?> googleApi) {
        this.f3930a = zab;
        this.b = i;
        this.c = googleApi;
    }
}
