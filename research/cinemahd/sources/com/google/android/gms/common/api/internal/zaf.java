package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zaf extends zad<Void> {
    private final RegisterListenerMethod<Api.AnyClient, ?> b;
    private final UnregisterListenerMethod<Api.AnyClient, ?> c;

    public zaf(zabw zabw, TaskCompletionSource<Void> taskCompletionSource) {
        super(3, taskCompletionSource);
        this.b = zabw.f3931a;
        this.c = zabw.b;
    }

    public final /* bridge */ /* synthetic */ void a(zaab zaab, boolean z) {
    }

    public final Feature[] b(GoogleApiManager.zaa<?> zaa) {
        return this.b.c();
    }

    public final boolean c(GoogleApiManager.zaa<?> zaa) {
        return this.b.d();
    }

    public final void d(GoogleApiManager.zaa<?> zaa) throws RemoteException {
        this.b.a(zaa.f(), this.f3943a);
        if (this.b.b() != null) {
            zaa.i().put(this.b.b(), new zabw(this.b, this.c));
        }
    }
}
