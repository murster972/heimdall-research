package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class zaag extends GoogleApiClient {
    private final String b;

    public zaag(String str) {
        this.b = str;
    }

    public boolean a(Api<?> api) {
        throw new UnsupportedOperationException(this.b);
    }

    public void b() {
        throw new UnsupportedOperationException(this.b);
    }

    public boolean e() {
        throw new UnsupportedOperationException(this.b);
    }

    public void a() {
        throw new UnsupportedOperationException(this.b);
    }

    public void b(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        throw new UnsupportedOperationException(this.b);
    }

    public void a(GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        throw new UnsupportedOperationException(this.b);
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }
}
