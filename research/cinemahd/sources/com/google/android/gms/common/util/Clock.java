package com.google.android.gms.common.util;

public interface Clock {
    long a();

    long b();

    long nanoTime();
}
