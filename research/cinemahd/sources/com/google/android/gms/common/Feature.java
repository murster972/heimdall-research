package com.google.android.gms.common;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.unity3d.ads.metadata.MediationMetaData;

public class Feature extends AbstractSafeParcelable {
    public static final Parcelable.Creator<Feature> CREATOR = new zzb();

    /* renamed from: a  reason: collision with root package name */
    private final String f3875a;
    @Deprecated
    private final int b;
    private final long c;

    public Feature(String str, long j) {
        this.f3875a = str;
        this.c = j;
        this.b = -1;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Feature) {
            Feature feature = (Feature) obj;
            if (((s() == null || !s().equals(feature.s())) && (s() != null || feature.s() != null)) || t() != feature.t()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Objects.a(s(), Long.valueOf(t()));
    }

    public String s() {
        return this.f3875a;
    }

    public long t() {
        long j = this.c;
        return j == -1 ? (long) this.b : j;
    }

    public String toString() {
        Objects.ToStringHelper a2 = Objects.a((Object) this);
        a2.a(MediationMetaData.KEY_NAME, s());
        a2.a(MediationMetaData.KEY_VERSION, Long.valueOf(t()));
        return a2.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, s(), false);
        SafeParcelWriter.a(parcel, 2, this.b);
        SafeParcelWriter.a(parcel, 3, t());
        SafeParcelWriter.a(parcel, a2);
    }

    public Feature(String str, int i, long j) {
        this.f3875a = str;
        this.b = i;
        this.c = j;
    }
}
