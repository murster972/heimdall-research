package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.internal.Objects;

public final class zai<O extends Api.ApiOptions> {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f3946a = true;
    private final int b;
    private final Api<O> c;
    private final O d;

    private zai(Api<O> api, O o) {
        this.c = api;
        this.d = o;
        this.b = Objects.a(this.c, this.d);
    }

    public static <O extends Api.ApiOptions> zai<O> a(Api<O> api, O o) {
        return new zai<>(api, o);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zai)) {
            return false;
        }
        zai zai = (zai) obj;
        return !this.f3946a && !zai.f3946a && Objects.a(this.c, zai.c) && Objects.a(this.d, zai.d);
    }

    public final int hashCode() {
        return this.b;
    }

    public static <O extends Api.ApiOptions> zai<O> a(Api<O> api) {
        return new zai<>(api);
    }

    public final String a() {
        return this.c.b();
    }

    private zai(Api<O> api) {
        this.c = api;
        this.d = null;
        this.b = System.identityHashCode(this);
    }
}
