package com.google.android.gms.common.api.internal;

abstract class zaau implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zaak f3913a;

    private zaau(zaak zaak) {
        this.f3913a = zaak;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public void run() {
        this.f3913a.b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.f3913a.b.unlock();
            }
        } catch (RuntimeException e) {
            this.f3913a.f3908a.a(e);
        } finally {
            this.f3913a.b.unlock();
        }
    }

    /* synthetic */ zaau(zaak zaak, zaal zaal) {
        this(zaak);
    }
}
