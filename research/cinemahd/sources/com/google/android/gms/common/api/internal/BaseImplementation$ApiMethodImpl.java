package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.AnyClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.SimpleClientAdapter;

public abstract class BaseImplementation$ApiMethodImpl<R extends Result, A extends Api.AnyClient> extends BasePendingResult<R> implements BaseImplementation$ResultHolder<R> {
    private final Api<?> mApi;
    private final Api.AnyClientKey<A> mClientKey;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    @Deprecated
    protected BaseImplementation$ApiMethodImpl(Api.AnyClientKey<A> anyClientKey, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        Preconditions.a(googleApiClient, (Object) "GoogleApiClient must not be null");
        Preconditions.a(anyClientKey);
        this.mClientKey = anyClientKey;
        this.mApi = null;
    }

    /* access modifiers changed from: protected */
    public abstract void doExecute(A a2) throws RemoteException;

    public final Api<?> getApi() {
        return this.mApi;
    }

    public final Api.AnyClientKey<A> getClientKey() {
        return this.mClientKey;
    }

    /* access modifiers changed from: protected */
    public void onSetFailedResult(R r) {
    }

    public final void run(A a2) throws DeadObjectException {
        if (a2 instanceof SimpleClientAdapter) {
            a2 = ((SimpleClientAdapter) a2).b();
        }
        try {
            doExecute(a2);
        } catch (DeadObjectException e) {
            setFailedResult((RemoteException) e);
            throw e;
        } catch (RemoteException e2) {
            setFailedResult(e2);
        }
    }

    public final void setFailedResult(Status status) {
        Preconditions.a(!status.v(), (Object) "Failed result must not be success");
        Result createFailedResult = createFailedResult(status);
        setResult(createFailedResult);
        onSetFailedResult(createFailedResult);
    }

    public /* bridge */ /* synthetic */ void setResult(Object obj) {
        super.setResult((Result) obj);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected BaseImplementation$ApiMethodImpl(Api<?> api, GoogleApiClient googleApiClient) {
        super(googleApiClient);
        Preconditions.a(googleApiClient, (Object) "GoogleApiClient must not be null");
        Preconditions.a(api, (Object) "Api must not be null");
        this.mClientKey = api.a();
        this.mApi = api;
    }

    private void setFailedResult(RemoteException remoteException) {
        setFailedResult(new Status(8, remoteException.getLocalizedMessage(), (PendingIntent) null));
    }

    protected BaseImplementation$ApiMethodImpl(BasePendingResult.CallbackHandler<R> callbackHandler) {
        super(callbackHandler);
        this.mClientKey = null;
        this.mApi = null;
    }
}
