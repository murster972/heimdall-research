package com.google.android.gms.common.internal;

import android.content.Intent;
import com.google.android.gms.common.api.internal.LifecycleFragment;

final class zae extends DialogRedirect {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Intent f3999a;
    private final /* synthetic */ LifecycleFragment b;
    private final /* synthetic */ int c;

    zae(Intent intent, LifecycleFragment lifecycleFragment, int i) {
        this.f3999a = intent;
        this.b = lifecycleFragment;
        this.c = i;
    }

    public final void a() {
        Intent intent = this.f3999a;
        if (intent != null) {
            this.b.startActivityForResult(intent, this.c);
        }
    }
}
