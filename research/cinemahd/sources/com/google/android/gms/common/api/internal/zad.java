package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zad<T> extends zac {

    /* renamed from: a  reason: collision with root package name */
    protected final TaskCompletionSource<T> f3943a;

    public zad(int i, TaskCompletionSource<T> taskCompletionSource) {
        super(i);
        this.f3943a = taskCompletionSource;
    }

    public void a(Status status) {
        this.f3943a.b((Exception) new ApiException(status));
    }

    /* access modifiers changed from: protected */
    public abstract void d(GoogleApiManager.zaa<?> zaa) throws RemoteException;

    public void a(RuntimeException runtimeException) {
        this.f3943a.b((Exception) runtimeException);
    }

    public final void a(GoogleApiManager.zaa<?> zaa) throws DeadObjectException {
        try {
            d(zaa);
        } catch (DeadObjectException e) {
            a(zab.a((RemoteException) e));
            throw e;
        } catch (RemoteException e2) {
            a(zab.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }
}
