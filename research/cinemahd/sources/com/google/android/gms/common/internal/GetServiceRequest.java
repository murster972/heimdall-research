package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public class GetServiceRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<GetServiceRequest> CREATOR = new zzd();

    /* renamed from: a  reason: collision with root package name */
    private final int f3984a;
    private final int b;
    private int c;
    String d;
    IBinder e;
    Scope[] f;
    Bundle g;
    Account h;
    Feature[] i;
    Feature[] j;
    private boolean k;
    private int l;

    public GetServiceRequest(int i2) {
        this.f3984a = 4;
        this.c = GoogleApiAvailabilityLight.f3877a;
        this.b = i2;
        this.k = true;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f3984a);
        SafeParcelWriter.a(parcel, 2, this.b);
        SafeParcelWriter.a(parcel, 3, this.c);
        SafeParcelWriter.a(parcel, 4, this.d, false);
        SafeParcelWriter.a(parcel, 5, this.e, false);
        SafeParcelWriter.a(parcel, 6, (T[]) this.f, i2, false);
        SafeParcelWriter.a(parcel, 7, this.g, false);
        SafeParcelWriter.a(parcel, 8, (Parcelable) this.h, i2, false);
        SafeParcelWriter.a(parcel, 10, (T[]) this.i, i2, false);
        SafeParcelWriter.a(parcel, 11, (T[]) this.j, i2, false);
        SafeParcelWriter.a(parcel, 12, this.k);
        SafeParcelWriter.a(parcel, 13, this.l);
        SafeParcelWriter.a(parcel, a2);
    }

    GetServiceRequest(int i2, int i3, int i4, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, Feature[] featureArr, Feature[] featureArr2, boolean z, int i5) {
        this.f3984a = i2;
        this.b = i3;
        this.c = i4;
        if ("com.google.android.gms".equals(str)) {
            this.d = "com.google.android.gms";
        } else {
            this.d = str;
        }
        if (i2 < 2) {
            this.h = iBinder != null ? AccountAccessor.a(IAccountAccessor.Stub.a(iBinder)) : null;
        } else {
            this.e = iBinder;
            this.h = account;
        }
        this.f = scopeArr;
        this.g = bundle;
        this.i = featureArr;
        this.j = featureArr2;
        this.k = z;
        this.l = i5;
    }
}
