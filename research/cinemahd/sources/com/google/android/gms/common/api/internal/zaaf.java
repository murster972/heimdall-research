package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;

final class zaaf {

    /* renamed from: a  reason: collision with root package name */
    private final zai<?> f3906a;
    private final TaskCompletionSource<Boolean> b = new TaskCompletionSource<>();

    public zaaf(zai<?> zai) {
        this.f3906a = zai;
    }

    public final TaskCompletionSource<Boolean> a() {
        return this.b;
    }

    public final zai<?> b() {
        return this.f3906a;
    }
}
