package com.google.android.gms.common.util.concurrent;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.internal.common.zzi;
import java.util.concurrent.Executor;

public class HandlerExecutor implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final Handler f4022a;

    public HandlerExecutor(Looper looper) {
        this.f4022a = new zzi(looper);
    }

    public void execute(Runnable runnable) {
        this.f4022a.post(runnable);
    }
}
