package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;

public final class zabw {

    /* renamed from: a  reason: collision with root package name */
    public final RegisterListenerMethod<Api.AnyClient, ?> f3931a;
    public final UnregisterListenerMethod<Api.AnyClient, ?> b;

    public zabw(RegisterListenerMethod<Api.AnyClient, ?> registerListenerMethod, UnregisterListenerMethod<Api.AnyClient, ?> unregisterListenerMethod) {
        this.f3931a = registerListenerMethod;
        this.b = unregisterListenerMethod;
    }
}
