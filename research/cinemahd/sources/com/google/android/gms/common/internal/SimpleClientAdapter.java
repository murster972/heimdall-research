package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.common.api.Api;

public class SimpleClientAdapter<T extends IInterface> extends GmsClient<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Api.SimpleClient<T> f3995a;

    public Api.SimpleClient<T> b() {
        return this.f3995a;
    }

    /* access modifiers changed from: protected */
    public T createServiceInterface(IBinder iBinder) {
        return this.f3995a.a(iBinder);
    }

    public int getMinApkVersion() {
        return super.getMinApkVersion();
    }

    /* access modifiers changed from: protected */
    public String getServiceDescriptor() {
        return this.f3995a.c();
    }

    /* access modifiers changed from: protected */
    public String getStartServiceAction() {
        return this.f3995a.b();
    }

    /* access modifiers changed from: protected */
    public void onSetConnectState(int i, T t) {
        this.f3995a.a(i, t);
    }
}
