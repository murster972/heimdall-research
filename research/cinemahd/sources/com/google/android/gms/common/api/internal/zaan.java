package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.GoogleApiAvailabilityCache;
import java.util.ArrayList;
import java.util.Map;

final class zaan extends zaau {
    private final Map<Api.Client, zaam> b;
    final /* synthetic */ zaak c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zaan(zaak zaak, Map<Api.Client, zaam> map) {
        super(zaak, (zaal) null);
        this.c = zaak;
        this.b = map;
    }

    public final void a() {
        GoogleApiAvailabilityCache googleApiAvailabilityCache = new GoogleApiAvailabilityCache(this.c.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (Api.Client next : this.b.keySet()) {
            if (!next.requiresGooglePlayServices() || this.b.get(next).c) {
                arrayList2.add(next);
            } else {
                arrayList.add(next);
            }
        }
        int i = -1;
        int i2 = 0;
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                i = googleApiAvailabilityCache.a(this.c.c, (Api.Client) obj);
                if (i != 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList2.size();
            while (i2 < size2) {
                Object obj2 = arrayList2.get(i2);
                i2++;
                i = googleApiAvailabilityCache.a(this.c.c, (Api.Client) obj2);
                if (i == 0) {
                    break;
                }
            }
        }
        if (i != 0) {
            this.c.f3908a.a((zabf) new zaao(this, this.c, new ConnectionResult(i, (PendingIntent) null)));
            return;
        }
        if (this.c.m) {
            this.c.k.connect();
        }
        for (Api.Client next2 : this.b.keySet()) {
            BaseGmsClient.ConnectionProgressReportCallbacks connectionProgressReportCallbacks = this.b.get(next2);
            if (!next2.requiresGooglePlayServices() || googleApiAvailabilityCache.a(this.c.c, next2) == 0) {
                next2.connect(connectionProgressReportCallbacks);
            } else {
                this.c.f3908a.a((zabf) new zaap(this, this.c, connectionProgressReportCallbacks));
            }
        }
    }
}
