package com.google.android.gms.common.api.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import com.google.android.gms.common.util.PlatformVersion;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public final class BackgroundDetector implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    private static final BackgroundDetector e = new BackgroundDetector();

    /* renamed from: a  reason: collision with root package name */
    private final AtomicBoolean f3889a = new AtomicBoolean();
    private final AtomicBoolean b = new AtomicBoolean();
    private final ArrayList<BackgroundStateChangeListener> c = new ArrayList<>();
    private boolean d = false;

    public interface BackgroundStateChangeListener {
        void a(boolean z);
    }

    private BackgroundDetector() {
    }

    public static void a(Application application) {
        synchronized (e) {
            if (!e.d) {
                application.registerActivityLifecycleCallbacks(e);
                application.registerComponentCallbacks(e);
                e.d = true;
            }
        }
    }

    public static BackgroundDetector b() {
        return e;
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.f3889a.compareAndSet(true, false);
        this.b.set(true);
        if (compareAndSet) {
            b(false);
        }
    }

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
    }

    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.f3889a.compareAndSet(true, false);
        this.b.set(true);
        if (compareAndSet) {
            b(false);
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public final void onTrimMemory(int i) {
        if (i == 20 && this.f3889a.compareAndSet(false, true)) {
            this.b.set(true);
            b(true);
        }
    }

    private final void b(boolean z) {
        synchronized (e) {
            ArrayList<BackgroundStateChangeListener> arrayList = this.c;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                BackgroundStateChangeListener backgroundStateChangeListener = arrayList.get(i);
                i++;
                backgroundStateChangeListener.a(z);
            }
        }
    }

    @TargetApi(16)
    public final boolean a(boolean z) {
        if (!this.b.get()) {
            if (!PlatformVersion.c()) {
                return z;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.b.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.f3889a.set(true);
            }
        }
        return a();
    }

    public final boolean a() {
        return this.f3889a.get();
    }

    public final void a(BackgroundStateChangeListener backgroundStateChangeListener) {
        synchronized (e) {
            this.c.add(backgroundStateChangeListener);
        }
    }
}
