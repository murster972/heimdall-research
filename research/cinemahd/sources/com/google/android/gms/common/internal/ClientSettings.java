package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.view.View;
import androidx.collection.ArraySet;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.signin.SignInOptions;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class ClientSettings {

    /* renamed from: a  reason: collision with root package name */
    private final Account f3980a;
    private final Set<Scope> b;
    private final Set<Scope> c;
    private final Map<Api<?>, OptionalApiSettings> d;
    private final String e;
    private final String f;
    private final SignInOptions g;
    private Integer h;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private Account f3981a;
        private ArraySet<Scope> b;
        private Map<Api<?>, OptionalApiSettings> c;
        private int d = 0;
        private View e;
        private String f;
        private String g;
        private SignInOptions h = SignInOptions.i;

        public final Builder a(Account account) {
            this.f3981a = account;
            return this;
        }

        public final Builder b(String str) {
            this.f = str;
            return this;
        }

        public final Builder a(Collection<Scope> collection) {
            if (this.b == null) {
                this.b = new ArraySet<>();
            }
            this.b.addAll(collection);
            return this;
        }

        public final Builder a(String str) {
            this.g = str;
            return this;
        }

        public final ClientSettings a() {
            return new ClientSettings(this.f3981a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
        }
    }

    public static final class OptionalApiSettings {

        /* renamed from: a  reason: collision with root package name */
        public final Set<Scope> f3982a;
    }

    public ClientSettings(Account account, Set<Scope> set, Map<Api<?>, OptionalApiSettings> map, int i, View view, String str, String str2, SignInOptions signInOptions) {
        this.f3980a = account;
        this.b = set == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(set);
        this.d = map == null ? Collections.EMPTY_MAP : map;
        this.e = str;
        this.f = str2;
        this.g = signInOptions;
        HashSet hashSet = new HashSet(this.b);
        for (OptionalApiSettings optionalApiSettings : this.d.values()) {
            hashSet.addAll(optionalApiSettings.f3982a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    public final Account a() {
        return this.f3980a;
    }

    public final Account b() {
        Account account = this.f3980a;
        if (account != null) {
            return account;
        }
        return new Account(BaseGmsClient.DEFAULT_ACCOUNT, "com.google");
    }

    public final Set<Scope> c() {
        return this.c;
    }

    public final Integer d() {
        return this.h;
    }

    public final Map<Api<?>, OptionalApiSettings> e() {
        return this.d;
    }

    public final String f() {
        return this.f;
    }

    public final String g() {
        return this.e;
    }

    public final Set<Scope> h() {
        return this.b;
    }

    public final SignInOptions i() {
        return this.g;
    }

    public final void a(Integer num) {
        this.h = num;
    }
}
