package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;

final class zas implements zabs {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3953a;
    private final zaaw b;
    /* access modifiers changed from: private */
    public final zabe c;
    /* access modifiers changed from: private */
    public final zabe d;
    private final Map<Api.AnyClientKey<?>, zabe> e;
    private final Set<SignInConnectionListener> f = Collections.newSetFromMap(new WeakHashMap());
    private final Api.Client g;
    private Bundle h;
    /* access modifiers changed from: private */
    public ConnectionResult i = null;
    /* access modifiers changed from: private */
    public ConnectionResult j = null;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public final Lock l;
    private int m = 0;

    private zas(Context context, zaaw zaaw, Lock lock, Looper looper, GoogleApiAvailabilityLight googleApiAvailabilityLight, Map<Api.AnyClientKey<?>, Api.Client> map, Map<Api.AnyClientKey<?>, Api.Client> map2, ClientSettings clientSettings, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder, Api.Client client, ArrayList<zaq> arrayList, ArrayList<zaq> arrayList2, Map<Api<?>, Boolean> map3, Map<Api<?>, Boolean> map4) {
        this.f3953a = context;
        this.b = zaaw;
        this.l = lock;
        this.g = client;
        Context context2 = context;
        Lock lock2 = lock;
        Looper looper2 = looper;
        GoogleApiAvailabilityLight googleApiAvailabilityLight2 = googleApiAvailabilityLight;
        zabe zabe = r3;
        zabe zabe2 = new zabe(context2, this.b, lock2, looper2, googleApiAvailabilityLight2, map2, (ClientSettings) null, map4, (Api.AbstractClientBuilder<? extends zad, SignInOptions>) null, arrayList2, new zau(this, (zat) null));
        this.c = zabe;
        this.d = new zabe(context2, this.b, lock2, looper2, googleApiAvailabilityLight2, map, clientSettings, map3, abstractClientBuilder, arrayList, new zav(this, (zat) null));
        ArrayMap arrayMap = new ArrayMap();
        for (Api.AnyClientKey<?> put : map2.keySet()) {
            arrayMap.put(put, this.c);
        }
        for (Api.AnyClientKey<?> put2 : map.keySet()) {
            arrayMap.put(put2, this.d);
        }
        this.e = Collections.unmodifiableMap(arrayMap);
    }

    public static zas a(Context context, zaaw zaaw, Lock lock, Looper looper, GoogleApiAvailabilityLight googleApiAvailabilityLight, Map<Api.AnyClientKey<?>, Api.Client> map, ClientSettings clientSettings, Map<Api<?>, Boolean> map2, Api.AbstractClientBuilder<? extends zad, SignInOptions> abstractClientBuilder, ArrayList<zaq> arrayList) {
        Map<Api<?>, Boolean> map3 = map2;
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        Api.Client client = null;
        for (Map.Entry next : map.entrySet()) {
            Api.Client client2 = (Api.Client) next.getValue();
            if (client2.providesSignIn()) {
                client = client2;
            }
            if (client2.requiresSignIn()) {
                arrayMap.put((Api.AnyClientKey) next.getKey(), client2);
            } else {
                arrayMap2.put((Api.AnyClientKey) next.getKey(), client2);
            }
        }
        Preconditions.b(!arrayMap.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        ArrayMap arrayMap3 = new ArrayMap();
        ArrayMap arrayMap4 = new ArrayMap();
        for (Api next2 : map2.keySet()) {
            Api.AnyClientKey<?> a2 = next2.a();
            if (arrayMap.containsKey(a2)) {
                arrayMap3.put(next2, map3.get(next2));
            } else if (arrayMap2.containsKey(a2)) {
                arrayMap4.put(next2, map3.get(next2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            zaq zaq = arrayList.get(i2);
            i2++;
            zaq zaq2 = zaq;
            if (arrayMap3.containsKey(zaq2.f3952a)) {
                arrayList2.add(zaq2);
            } else if (arrayMap4.containsKey(zaq2.f3952a)) {
                arrayList3.add(zaq2);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new zas(context, zaaw, lock, looper, googleApiAvailabilityLight, arrayMap, arrayMap2, clientSettings, abstractClientBuilder, client, arrayList2, arrayList3, arrayMap3, arrayMap4);
    }

    private final boolean b(BaseImplementation$ApiMethodImpl<? extends Result, ? extends Api.AnyClient> baseImplementation$ApiMethodImpl) {
        Api.AnyClientKey<? extends Api.AnyClient> clientKey = baseImplementation$ApiMethodImpl.getClientKey();
        Preconditions.a(this.e.containsKey(clientKey), (Object) "GoogleApiClient is not configured to use the API required for this call.");
        return this.e.get(clientKey).equals(this.d);
    }

    /* access modifiers changed from: private */
    public final void c() {
        ConnectionResult connectionResult;
        if (b(this.i)) {
            if (b(this.j) || e()) {
                int i2 = this.m;
                if (i2 != 1) {
                    if (i2 != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        this.m = 0;
                        return;
                    }
                    this.b.a(this.h);
                }
                d();
                this.m = 0;
                return;
            }
            ConnectionResult connectionResult2 = this.j;
            if (connectionResult2 == null) {
                return;
            }
            if (this.m == 1) {
                d();
                return;
            }
            a(connectionResult2);
            this.c.disconnect();
        } else if (this.i == null || !b(this.j)) {
            ConnectionResult connectionResult3 = this.i;
            if (connectionResult3 != null && (connectionResult = this.j) != null) {
                if (this.d.l < this.c.l) {
                    connectionResult3 = connectionResult;
                }
                a(connectionResult3);
            }
        } else {
            this.d.disconnect();
            a(this.i);
        }
    }

    private final void d() {
        for (SignInConnectionListener onComplete : this.f) {
            onComplete.onComplete();
        }
        this.f.clear();
    }

    private final boolean e() {
        ConnectionResult connectionResult = this.j;
        return connectionResult != null && connectionResult.s() == 4;
    }

    public final void connect() {
        this.m = 2;
        this.k = false;
        this.j = null;
        this.i = null;
        this.c.connect();
        this.d.connect();
    }

    public final void disconnect() {
        this.j = null;
        this.i = null;
        this.m = 0;
        this.c.disconnect();
        this.d.disconnect();
        d();
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("authClient").println(":");
        this.d.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append(str).append("anonClient").println(":");
        this.c.dump(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    public final boolean isConnected() {
        this.l.lock();
        try {
            boolean z = true;
            if (!this.c.isConnected() || (!this.d.isConnected() && !e() && this.m != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.l.unlock();
        }
    }

    private final PendingIntent b() {
        if (this.g == null) {
            return null;
        }
        return PendingIntent.getActivity(this.f3953a, System.identityHashCode(this.b), this.g.getSignInIntent(), 134217728);
    }

    private static boolean b(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.w();
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t) {
        if (!b((BaseImplementation$ApiMethodImpl<? extends Result, ? extends Api.AnyClient>) t)) {
            return this.c.a(t);
        }
        if (!e()) {
            return this.d.a(t);
        }
        t.setFailedResult(new Status(4, (String) null, b()));
        return t;
    }

    public final void a() {
        this.c.a();
        this.d.a();
    }

    private final void a(ConnectionResult connectionResult) {
        int i2 = this.m;
        if (i2 != 1) {
            if (i2 != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.m = 0;
            }
            this.b.a(connectionResult);
        }
        d();
        this.m = 0;
    }

    /* access modifiers changed from: private */
    public final void a(int i2, boolean z) {
        this.b.a(i2, z);
        this.j = null;
        this.i = null;
    }

    /* access modifiers changed from: private */
    public final void a(Bundle bundle) {
        Bundle bundle2 = this.h;
        if (bundle2 == null) {
            this.h = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }
}
