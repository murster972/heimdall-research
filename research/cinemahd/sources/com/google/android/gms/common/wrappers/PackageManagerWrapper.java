package com.google.android.gms.common.wrappers;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Process;
import com.google.android.gms.common.util.PlatformVersion;

public class PackageManagerWrapper {

    /* renamed from: a  reason: collision with root package name */
    private final Context f4026a;

    public PackageManagerWrapper(Context context) {
        this.f4026a = context;
    }

    public ApplicationInfo a(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f4026a.getPackageManager().getApplicationInfo(str, i);
    }

    public PackageInfo b(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f4026a.getPackageManager().getPackageInfo(str, i);
    }

    @TargetApi(19)
    public final boolean a(int i, String str) {
        if (PlatformVersion.f()) {
            try {
                ((AppOpsManager) this.f4026a.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException unused) {
                return false;
            }
        } else {
            String[] packagesForUid = this.f4026a.getPackageManager().getPackagesForUid(i);
            if (!(str == null || packagesForUid == null)) {
                for (String equals : packagesForUid) {
                    if (str.equals(equals)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public CharSequence b(String str) throws PackageManager.NameNotFoundException {
        return this.f4026a.getPackageManager().getApplicationLabel(this.f4026a.getPackageManager().getApplicationInfo(str, 0));
    }

    public int a(String str) {
        return this.f4026a.checkCallingOrSelfPermission(str);
    }

    public int a(String str, String str2) {
        return this.f4026a.getPackageManager().checkPermission(str, str2);
    }

    public boolean a() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return InstantApps.a(this.f4026a);
        }
        if (!PlatformVersion.k() || (nameForUid = this.f4026a.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.f4026a.getPackageManager().isInstantApp(nameForUid);
    }
}
