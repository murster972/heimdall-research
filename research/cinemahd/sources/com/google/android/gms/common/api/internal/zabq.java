package com.google.android.gms.common.api.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class zabq extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private Context f3929a;
    private final zabr b;

    public zabq(zabr zabr) {
        this.b = zabr;
    }

    public final void a(Context context) {
        this.f3929a = context;
    }

    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if ("com.google.android.gms".equals(data != null ? data.getSchemeSpecificPart() : null)) {
            this.b.a();
            a();
        }
    }

    public final synchronized void a() {
        if (this.f3929a != null) {
            this.f3929a.unregisterReceiver(this);
        }
        this.f3929a = null;
    }
}
