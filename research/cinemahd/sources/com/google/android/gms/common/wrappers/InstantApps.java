package com.google.android.gms.common.wrappers;

import android.content.Context;
import com.google.android.gms.common.util.PlatformVersion;

public class InstantApps {

    /* renamed from: a  reason: collision with root package name */
    private static Context f4025a;
    private static Boolean b;

    public static synchronized boolean a(Context context) {
        synchronized (InstantApps.class) {
            Context applicationContext = context.getApplicationContext();
            if (f4025a == null || b == null || f4025a != applicationContext) {
                b = null;
                if (PlatformVersion.k()) {
                    b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        b = true;
                    } catch (ClassNotFoundException unused) {
                        b = false;
                    }
                }
                f4025a = applicationContext;
                boolean booleanValue = b.booleanValue();
                return booleanValue;
            }
            boolean booleanValue2 = b.booleanValue();
            return booleanValue2;
        }
    }
}
