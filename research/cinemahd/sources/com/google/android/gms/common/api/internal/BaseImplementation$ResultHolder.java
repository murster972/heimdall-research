package com.google.android.gms.common.api.internal;

public interface BaseImplementation$ResultHolder<R> {
    void setResult(R r);
}
