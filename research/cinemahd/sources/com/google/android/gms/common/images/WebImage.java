package com.google.android.gms.common.images;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class WebImage extends AbstractSafeParcelable {
    public static final Parcelable.Creator<WebImage> CREATOR = new zae();

    /* renamed from: a  reason: collision with root package name */
    private final int f3969a;
    private final Uri b;
    private final int c;
    private final int d;

    WebImage(int i, Uri uri, int i2, int i3) {
        this.f3969a = i;
        this.b = uri;
        this.c = i2;
        this.d = i3;
    }

    private static Uri a(JSONObject jSONObject) {
        if (jSONObject.has(ReportDBAdapter.ReportColumns.COLUMN_URL)) {
            try {
                return Uri.parse(jSONObject.getString(ReportDBAdapter.ReportColumns.COLUMN_URL));
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof WebImage)) {
            WebImage webImage = (WebImage) obj;
            return Objects.a(this.b, webImage.b) && this.c == webImage.c && this.d == webImage.d;
        }
    }

    public final int hashCode() {
        return Objects.a(this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
    }

    public final int s() {
        return this.d;
    }

    public final Uri t() {
        return this.b;
    }

    public final String toString() {
        return String.format(Locale.US, "Image %dx%d %s", new Object[]{Integer.valueOf(this.c), Integer.valueOf(this.d), this.b.toString()});
    }

    public final int u() {
        return this.c;
    }

    public final JSONObject v() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(ReportDBAdapter.ReportColumns.COLUMN_URL, this.b.toString());
            jSONObject.put("width", this.c);
            jSONObject.put("height", this.d);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f3969a);
        SafeParcelWriter.a(parcel, 2, (Parcelable) t(), i, false);
        SafeParcelWriter.a(parcel, 3, u());
        SafeParcelWriter.a(parcel, 4, s());
        SafeParcelWriter.a(parcel, a2);
    }

    public WebImage(Uri uri, int i, int i2) throws IllegalArgumentException {
        this(1, uri, i, i2);
        if (uri == null) {
            throw new IllegalArgumentException("url cannot be null");
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("width and height must not be negative");
        }
    }

    public WebImage(Uri uri) throws IllegalArgumentException {
        this(uri, 0, 0);
    }

    public WebImage(JSONObject jSONObject) throws IllegalArgumentException {
        this(a(jSONObject), jSONObject.optInt("width", 0), jSONObject.optInt("height", 0));
    }
}
