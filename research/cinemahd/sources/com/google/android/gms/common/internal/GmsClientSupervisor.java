package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public abstract class GmsClientSupervisor {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f3986a = new Object();
    private static GmsClientSupervisor b;

    protected static final class zza {
        private static final Uri f = new Uri.Builder().scheme("content").authority("com.google.android.gms.chimera").build();

        /* renamed from: a  reason: collision with root package name */
        private final String f3987a;
        private final String b;
        private final ComponentName c = null;
        private final int d;
        private final boolean e;

        public zza(String str, String str2, int i, boolean z) {
            Preconditions.b(str);
            this.f3987a = str;
            Preconditions.b(str2);
            this.b = str2;
            this.d = i;
            this.e = z;
        }

        public final ComponentName a() {
            return this.c;
        }

        public final String b() {
            return this.b;
        }

        public final int c() {
            return this.d;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            return Objects.a(this.f3987a, zza.f3987a) && Objects.a(this.b, zza.b) && Objects.a(this.c, zza.c) && this.d == zza.d && this.e == zza.e;
        }

        public final int hashCode() {
            return Objects.a(this.f3987a, this.b, this.c, Integer.valueOf(this.d), Boolean.valueOf(this.e));
        }

        public final String toString() {
            String str = this.f3987a;
            return str == null ? this.c.flattenToString() : str;
        }

        public final Intent a(Context context) {
            if (this.f3987a == null) {
                return new Intent().setComponent(this.c);
            }
            Intent intent = null;
            if (this.e) {
                Bundle bundle = new Bundle();
                bundle.putString("serviceActionBundleKey", this.f3987a);
                Bundle call = context.getContentResolver().call(f, "serviceIntentCall", (String) null, bundle);
                if (call != null) {
                    intent = (Intent) call.getParcelable("serviceResponseIntentKey");
                }
                if (intent == null) {
                    String valueOf = String.valueOf(this.f3987a);
                    Log.w("ConnectionStatusConfig", valueOf.length() != 0 ? "Dynamic lookup for intent failed for action: ".concat(valueOf) : new String("Dynamic lookup for intent failed for action: "));
                }
            }
            if (intent == null) {
                return new Intent(this.f3987a).setPackage(this.b);
            }
            return intent;
        }
    }

    public static GmsClientSupervisor a(Context context) {
        synchronized (f3986a) {
            if (b == null) {
                b = new zze(context.getApplicationContext());
            }
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(zza zza2, ServiceConnection serviceConnection, String str);

    /* access modifiers changed from: protected */
    public abstract void b(zza zza2, ServiceConnection serviceConnection, String str);

    public final void a(String str, String str2, int i, ServiceConnection serviceConnection, String str3, boolean z) {
        b(new zza(str, str2, i, z), serviceConnection, str3);
    }
}
