package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.util.concurrent.NumberedThreadFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class zabh {

    /* renamed from: a  reason: collision with root package name */
    private static final ExecutorService f3921a = Executors.newFixedThreadPool(2, new NumberedThreadFactory("GAC_Executor"));

    public static ExecutorService a() {
        return f3921a;
    }
}
