package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class zak extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zak> CREATOR = new zan();

    /* renamed from: a  reason: collision with root package name */
    private final int f4013a;
    private final HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> b;
    private final String c;

    zak(int i, ArrayList<zal> arrayList, String str) {
        this.f4013a = i;
        HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            zal zal = arrayList.get(i2);
            String str2 = zal.b;
            HashMap hashMap2 = new HashMap();
            int size2 = zal.c.size();
            for (int i3 = 0; i3 < size2; i3++) {
                zam zam = zal.c.get(i3);
                hashMap2.put(zam.b, zam.c);
            }
            hashMap.put(str2, hashMap2);
        }
        this.b = hashMap;
        Preconditions.a(str);
        this.c = str;
        s();
    }

    public final Map<String, FastJsonResponse.Field<?, ?>> e(String str) {
        return this.b.get(str);
    }

    public final void s() {
        for (String str : this.b.keySet()) {
            Map map = this.b.get(str);
            for (String str2 : map.keySet()) {
                ((FastJsonResponse.Field) map.get(str2)).a(this);
            }
        }
    }

    public final String t() {
        return this.c;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (String next : this.b.keySet()) {
            sb.append(next);
            sb.append(":\n");
            Map map = this.b.get(next);
            for (String str : map.keySet()) {
                sb.append("  ");
                sb.append(str);
                sb.append(": ");
                sb.append(map.get(str));
            }
        }
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4013a);
        ArrayList arrayList = new ArrayList();
        for (String next : this.b.keySet()) {
            arrayList.add(new zal(next, this.b.get(next)));
        }
        SafeParcelWriter.c(parcel, 2, arrayList, false);
        SafeParcelWriter.a(parcel, 3, this.c, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
