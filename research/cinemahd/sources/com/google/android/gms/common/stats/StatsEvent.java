package com.google.android.gms.common.stats;

import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class StatsEvent extends AbstractSafeParcelable implements ReflectedParcelable {
    public abstract int s();

    public abstract long t();

    public String toString() {
        long t = t();
        int s = s();
        long u = u();
        String v = v();
        StringBuilder sb = new StringBuilder(String.valueOf(v).length() + 53);
        sb.append(t);
        sb.append("\t");
        sb.append(s);
        sb.append("\t");
        sb.append(u);
        sb.append(v);
        return sb.toString();
    }

    public abstract long u();

    public abstract String v();
}
