package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class zao implements Parcelable.Creator<zal> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        int i = 0;
        ArrayList<zam> arrayList = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 2) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                arrayList = SafeParcelReader.c(parcel, a2, zam.CREATOR);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zal(i, str, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zal[i];
    }
}
