package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.internal.GmsClientEventManager;

final class zaax implements GmsClientEventManager.GmsClientEventState {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zaaw f3915a;

    zaax(zaaw zaaw) {
        this.f3915a = zaaw;
    }

    public final Bundle getConnectionHint() {
        return null;
    }

    public final boolean isConnected() {
        return this.f3915a.e();
    }
}
