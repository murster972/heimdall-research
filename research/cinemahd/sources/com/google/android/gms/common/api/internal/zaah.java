package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.SimpleClientAdapter;

public final class zaah implements zabd {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final zabe f3907a;
    private boolean b = false;

    public zaah(zabe zabe) {
        this.f3907a = zabe;
    }

    public final <A extends Api.AnyClient, T extends BaseImplementation$ApiMethodImpl<? extends Result, A>> T a(T t) {
        try {
            this.f3907a.m.y.a(t);
            zaaw zaaw = this.f3907a.m;
            Api.Client client = zaaw.p.get(t.getClientKey());
            Preconditions.a(client, (Object) "Appropriate Api was not requested.");
            if (client.isConnected() || !this.f3907a.g.containsKey(t.getClientKey())) {
                boolean z = client instanceof SimpleClientAdapter;
                Api.AnyClient anyClient = client;
                if (z) {
                    anyClient = ((SimpleClientAdapter) client).b();
                }
                t.run(anyClient);
                return t;
            }
            t.setFailedResult(new Status(17));
            return t;
        } catch (DeadObjectException unused) {
            this.f3907a.a((zabf) new zaai(this, this));
        }
    }

    public final void a(ConnectionResult connectionResult, Api<?> api, boolean z) {
    }

    public final void begin() {
    }

    public final void connect() {
        if (this.b) {
            this.b = false;
            this.f3907a.a((zabf) new zaaj(this, this));
        }
    }

    public final boolean disconnect() {
        if (this.b) {
            return false;
        }
        if (this.f3907a.m.h()) {
            this.b = true;
            for (zacm a2 : this.f3907a.m.x) {
                a2.a();
            }
            return false;
        }
        this.f3907a.a((ConnectionResult) null);
        return true;
    }

    public final void onConnected(Bundle bundle) {
    }

    public final void onConnectionSuspended(int i) {
        this.f3907a.a((ConnectionResult) null);
        this.f3907a.n.a(i, this.b);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.b) {
            this.b = false;
            this.f3907a.m.y.a();
            disconnect();
        }
    }
}
