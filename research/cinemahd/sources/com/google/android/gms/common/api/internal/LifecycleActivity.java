package com.google.android.gms.common.api.internal;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.common.internal.Preconditions;

public class LifecycleActivity {

    /* renamed from: a  reason: collision with root package name */
    private final Object f3896a;

    public LifecycleActivity(Activity activity) {
        Preconditions.a(activity, (Object) "Activity must not be null");
        this.f3896a = activity;
    }

    public Activity a() {
        return (Activity) this.f3896a;
    }

    public FragmentActivity b() {
        return (FragmentActivity) this.f3896a;
    }

    public boolean c() {
        return this.f3896a instanceof FragmentActivity;
    }

    public final boolean d() {
        return this.f3896a instanceof Activity;
    }
}
