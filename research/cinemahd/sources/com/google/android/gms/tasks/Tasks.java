package com.google.android.gms.tasks;

import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

public final class Tasks {
    private Tasks() {
    }

    public static <TResult> Task<TResult> a(Executor executor, Callable<TResult> callable) {
        Preconditions.a(executor, (Object) "Executor must not be null");
        Preconditions.a(callable, (Object) "Callback must not be null");
        zzu zzu = new zzu();
        executor.execute(new zzv(zzu, callable));
        return zzu;
    }
}
