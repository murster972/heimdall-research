package com.google.android.gms.tasks;

import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;

final class zzu<TResult> extends Task<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final Object f4164a = new Object();
    private final zzr<TResult> b = new zzr<>();
    private boolean c;
    private volatile boolean d;
    private TResult e;
    private Exception f;

    zzu() {
    }

    private final void f() {
        Preconditions.b(this.c, "Task is not yet complete");
    }

    private final void g() {
        Preconditions.b(!this.c, "Task is already complete");
    }

    private final void h() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    private final void i() {
        synchronized (this.f4164a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    public final Exception a() {
        Exception exc;
        synchronized (this.f4164a) {
            exc = this.f;
        }
        return exc;
    }

    public final TResult b() {
        TResult tresult;
        synchronized (this.f4164a) {
            f();
            h();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    public final boolean c() {
        return this.d;
    }

    public final boolean d() {
        boolean z;
        synchronized (this.f4164a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    public final boolean e() {
        synchronized (this.f4164a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    public final Task<TResult> a(OnCompleteListener<TResult> onCompleteListener) {
        return a(TaskExecutors.f4157a, onCompleteListener);
    }

    public final Task<TResult> a(Executor executor, OnCompleteListener<TResult> onCompleteListener) {
        this.b.a(new zzi(executor, onCompleteListener));
        i();
        return this;
    }

    public final <TContinuationResult> Task<TContinuationResult> a(Executor executor, Continuation<TResult, TContinuationResult> continuation) {
        zzu zzu = new zzu();
        this.b.a(new zzc(executor, continuation, zzu));
        i();
        return zzu;
    }

    public final boolean b(TResult tresult) {
        synchronized (this.f4164a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    public final void a(TResult tresult) {
        synchronized (this.f4164a) {
            g();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    public final boolean b(Exception exc) {
        Preconditions.a(exc, (Object) "Exception must not be null");
        synchronized (this.f4164a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    public final void a(Exception exc) {
        Preconditions.a(exc, (Object) "Exception must not be null");
        synchronized (this.f4164a) {
            g();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }
}
