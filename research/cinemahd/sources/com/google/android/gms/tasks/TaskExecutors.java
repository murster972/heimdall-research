package com.google.android.gms.tasks;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

public final class TaskExecutors {

    /* renamed from: a  reason: collision with root package name */
    public static final Executor f4157a = new zza();

    private static final class zza implements Executor {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f4158a = new Handler(Looper.getMainLooper());

        public final void execute(Runnable runnable) {
            this.f4158a.post(runnable);
        }
    }

    static {
        new zzt();
    }

    private TaskExecutors() {
    }
}
