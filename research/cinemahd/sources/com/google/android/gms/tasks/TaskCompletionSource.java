package com.google.android.gms.tasks;

public class TaskCompletionSource<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final zzu<TResult> f4156a = new zzu<>();

    public void a(TResult tresult) {
        this.f4156a.a(tresult);
    }

    public boolean b(TResult tresult) {
        return this.f4156a.b(tresult);
    }

    public void a(Exception exc) {
        this.f4156a.a(exc);
    }

    public boolean b(Exception exc) {
        return this.f4156a.b(exc);
    }

    public Task<TResult> a() {
        return this.f4156a;
    }
}
