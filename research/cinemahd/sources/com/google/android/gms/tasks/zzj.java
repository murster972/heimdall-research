package com.google.android.gms.tasks;

final class zzj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Task f4162a;
    private final /* synthetic */ zzi b;

    zzj(zzi zzi, Task task) {
        this.b = zzi;
        this.f4162a = task;
    }

    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.a(this.f4162a);
            }
        }
    }
}
