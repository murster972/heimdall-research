package com.google.android.gms.tasks;

public interface Continuation<TResult, TContinuationResult> {
    TContinuationResult a(Task<TResult> task) throws Exception;
}
