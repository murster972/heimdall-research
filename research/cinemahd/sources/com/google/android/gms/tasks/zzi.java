package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class zzi<TResult> implements zzq<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f4161a;
    /* access modifiers changed from: private */
    public final Object b = new Object();
    /* access modifiers changed from: private */
    public OnCompleteListener<TResult> c;

    public zzi(Executor executor, OnCompleteListener<TResult> onCompleteListener) {
        this.f4161a = executor;
        this.c = onCompleteListener;
    }

    public final void a(Task<TResult> task) {
        synchronized (this.b) {
            if (this.c != null) {
                this.f4161a.execute(new zzj(this, task));
            }
        }
    }
}
