package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class zzc<TResult, TContinuationResult> implements zzq<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f4159a;
    /* access modifiers changed from: private */
    public final Continuation<TResult, TContinuationResult> b;
    /* access modifiers changed from: private */
    public final zzu<TContinuationResult> c;

    public zzc(Executor executor, Continuation<TResult, TContinuationResult> continuation, zzu<TContinuationResult> zzu) {
        this.f4159a = executor;
        this.b = continuation;
        this.c = zzu;
    }

    public final void a(Task<TResult> task) {
        this.f4159a.execute(new zzd(this, task));
    }
}
