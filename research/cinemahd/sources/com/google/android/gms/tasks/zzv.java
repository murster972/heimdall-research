package com.google.android.gms.tasks;

import java.util.concurrent.Callable;

final class zzv implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzu f4165a;
    private final /* synthetic */ Callable b;

    zzv(zzu zzu, Callable callable) {
        this.f4165a = zzu;
        this.b = callable;
    }

    public final void run() {
        try {
            this.f4165a.a(this.b.call());
        } catch (Exception e) {
            this.f4165a.a(e);
        }
    }
}
