package com.google.android.gms.tasks;

final class zzd implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Task f4160a;
    private final /* synthetic */ zzc b;

    zzd(zzc zzc, Task task) {
        this.b = zzc;
        this.f4160a = task;
    }

    public final void run() {
        if (this.f4160a.c()) {
            this.b.c.e();
            return;
        }
        try {
            this.b.c.a(this.b.b.a(this.f4160a));
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                this.b.c.a((Exception) e.getCause());
            } else {
                this.b.c.a((Exception) e);
            }
        } catch (Exception e2) {
            this.b.c.a(e2);
        }
    }
}
