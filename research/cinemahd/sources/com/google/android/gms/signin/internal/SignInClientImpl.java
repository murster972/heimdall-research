package com.google.android.gms.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.internal.Storage;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.GmsClient;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zad;

public class SignInClientImpl extends GmsClient<zaf> implements zad {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f4151a;
    private final ClientSettings b;
    private final Bundle c;
    private Integer d;

    private SignInClientImpl(Context context, Looper looper, boolean z, ClientSettings clientSettings, Bundle bundle, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 44, clientSettings, connectionCallbacks, onConnectionFailedListener);
        this.f4151a = true;
        this.b = clientSettings;
        this.c = bundle;
        this.d = clientSettings.d();
    }

    public final void a(IAccountAccessor iAccountAccessor, boolean z) {
        try {
            ((zaf) getService()).a(iAccountAccessor, this.d.intValue(), z);
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    public final void connect() {
        connect(new BaseGmsClient.LegacyClientCallbackAdapter());
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ IInterface createServiceInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof zaf) {
            return (zaf) queryLocalInterface;
        }
        return new zag(iBinder);
    }

    /* access modifiers changed from: protected */
    public Bundle getGetServiceRequestExtraArgs() {
        if (!getContext().getPackageName().equals(this.b.g())) {
            this.c.putString("com.google.android.gms.signin.internal.realClientPackageName", this.b.g());
        }
        return this.c;
    }

    public int getMinApkVersion() {
        return GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    /* access modifiers changed from: protected */
    public String getServiceDescriptor() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    /* access modifiers changed from: protected */
    public String getStartServiceAction() {
        return "com.google.android.gms.signin.service.START";
    }

    public boolean requiresSignIn() {
        return this.f4151a;
    }

    public final void a() {
        try {
            ((zaf) getService()).b(this.d.intValue());
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    public final void a(zad zad) {
        Preconditions.a(zad, (Object) "Expecting a valid ISignInCallbacks");
        try {
            Account b2 = this.b.b();
            GoogleSignInAccount googleSignInAccount = null;
            if (BaseGmsClient.DEFAULT_ACCOUNT.equals(b2.name)) {
                googleSignInAccount = Storage.a(getContext()).a();
            }
            ((zaf) getService()).a(new zah(new ResolveAccountRequest(b2, this.d.intValue(), googleSignInAccount)), zad);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                zad.a(new zaj(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    public SignInClientImpl(Context context, Looper looper, boolean z, ClientSettings clientSettings, SignInOptions signInOptions, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, true, clientSettings, a(clientSettings), connectionCallbacks, onConnectionFailedListener);
    }

    public static Bundle a(ClientSettings clientSettings) {
        SignInOptions i = clientSettings.i();
        Integer d2 = clientSettings.d();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", clientSettings.a());
        if (d2 != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", d2.intValue());
        }
        if (i != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", i.g());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", i.f());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", i.d());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", i.e());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", i.b());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", i.h());
            if (i.a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", i.a().longValue());
            }
            if (i.c() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", i.c().longValue());
            }
        }
        return bundle;
    }
}
