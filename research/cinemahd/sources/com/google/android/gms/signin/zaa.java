package com.google.android.gms.signin;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.signin.internal.SignInClientImpl;

public final class zaa {

    /* renamed from: a  reason: collision with root package name */
    private static final Api.ClientKey<SignInClientImpl> f4155a = new Api.ClientKey<>();
    private static final Api.ClientKey<SignInClientImpl> b = new Api.ClientKey<>();
    public static final Api.AbstractClientBuilder<SignInClientImpl, SignInOptions> c = new zab();
    private static final Api.AbstractClientBuilder<SignInClientImpl, Object> d = new zac();
    public static final Api<SignInOptions> e = new Api<>("SignIn.API", c, f4155a);

    static {
        new Scope("profile");
        new Scope("email");
        new Api("SignIn.INTERNAL_API", d, b);
    }
}
