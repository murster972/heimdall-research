package com.google.android.gms.signin;

import com.google.android.gms.common.api.Api;

public final class SignInOptions implements Api.ApiOptions.Optional {
    public static final SignInOptions i = new SignInOptions(false, false, (String) null, false, (String) null, false, (Long) null, (Long) null);

    /* renamed from: a  reason: collision with root package name */
    private final boolean f4150a = false;
    private final boolean b = false;
    private final String c = null;
    private final boolean d = false;
    private final String e = null;
    private final boolean f = false;
    private final Long g = null;
    private final Long h = null;

    public static final class zaa {
    }

    static {
        new zaa();
    }

    private SignInOptions(boolean z, boolean z2, String str, boolean z3, String str2, boolean z4, Long l, Long l2) {
    }

    public final Long a() {
        return this.g;
    }

    public final String b() {
        return this.e;
    }

    public final Long c() {
        return this.h;
    }

    public final String d() {
        return this.c;
    }

    public final boolean e() {
        return this.d;
    }

    public final boolean f() {
        return this.b;
    }

    public final boolean g() {
        return this.f4150a;
    }

    public final boolean h() {
        return this.f;
    }
}
