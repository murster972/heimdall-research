package com.google.android.gms.signin.internal;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zaj extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zaj> CREATOR = new zak();

    /* renamed from: a  reason: collision with root package name */
    private final int f4154a;
    private final ConnectionResult b;
    private final ResolveAccountResponse c;

    zaj(int i, ConnectionResult connectionResult, ResolveAccountResponse resolveAccountResponse) {
        this.f4154a = i;
        this.b = connectionResult;
        this.c = resolveAccountResponse;
    }

    public final ConnectionResult s() {
        return this.b;
    }

    public final ResolveAccountResponse t() {
        return this.c;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4154a);
        SafeParcelWriter.a(parcel, 2, (Parcelable) this.b, i, false);
        SafeParcelWriter.a(parcel, 3, (Parcelable) this.c, i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public zaj(int i) {
        this(new ConnectionResult(8, (PendingIntent) null), (ResolveAccountResponse) null);
    }

    private zaj(ConnectionResult connectionResult, ResolveAccountResponse resolveAccountResponse) {
        this(1, connectionResult, (ResolveAccountResponse) null);
    }
}
