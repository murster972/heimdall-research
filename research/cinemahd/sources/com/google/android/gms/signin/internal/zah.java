package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zah extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zah> CREATOR = new zai();

    /* renamed from: a  reason: collision with root package name */
    private final int f4153a;
    private final ResolveAccountRequest b;

    zah(int i, ResolveAccountRequest resolveAccountRequest) {
        this.f4153a = i;
        this.b = resolveAccountRequest;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4153a);
        SafeParcelWriter.a(parcel, 2, (Parcelable) this.b, i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public zah(ResolveAccountRequest resolveAccountRequest) {
        this(1, resolveAccountRequest);
    }
}
