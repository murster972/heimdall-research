package com.google.android.gms.base;

public final class R$string {
    public static final int common_google_play_services_enable_button = 2131755196;
    public static final int common_google_play_services_enable_text = 2131755197;
    public static final int common_google_play_services_enable_title = 2131755198;
    public static final int common_google_play_services_install_button = 2131755199;
    public static final int common_google_play_services_install_text = 2131755200;
    public static final int common_google_play_services_install_title = 2131755201;
    public static final int common_google_play_services_notification_channel_name = 2131755202;
    public static final int common_google_play_services_notification_ticker = 2131755203;
    public static final int common_google_play_services_unsupported_text = 2131755205;
    public static final int common_google_play_services_update_button = 2131755206;
    public static final int common_google_play_services_update_text = 2131755207;
    public static final int common_google_play_services_update_title = 2131755208;
    public static final int common_google_play_services_updating_text = 2131755209;
    public static final int common_google_play_services_wear_update_text = 2131755210;
    public static final int common_open_on_phone = 2131755211;
    public static final int common_signin_button_text = 2131755212;
    public static final int common_signin_button_text_long = 2131755213;

    private R$string() {
    }
}
