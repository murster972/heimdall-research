package com.google.android.gms.cast.framework;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.cast.zzb;

public interface zzt extends IInterface {

    public static abstract class zza extends zzb implements zzt {
        public static zzt a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.ISession");
            if (queryLocalInterface instanceof zzt) {
                return (zzt) queryLocalInterface;
            }
            return new zzu(iBinder);
        }
    }

    void c(int i) throws RemoteException;

    void e(int i) throws RemoteException;

    void f(int i) throws RemoteException;

    boolean isConnected() throws RemoteException;

    boolean isConnecting() throws RemoteException;

    IObjectWrapper j() throws RemoteException;

    boolean n() throws RemoteException;

    boolean r() throws RemoteException;
}
