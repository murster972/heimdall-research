package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Keep;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.internal.cast.zzfo;

@Keep
public class HelpTextView extends LinearLayout implements zzi {
    private TextView zzkq;
    private TextView zzkr;

    @Keep
    public HelpTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private static void zza(TextView textView, CharSequence charSequence) {
        textView.setText(charSequence);
        textView.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
    }

    @Keep
    public View asView() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.zzkq = (TextView) zzfo.checkNotNull((TextView) findViewById(R$id.cast_featurehighlight_help_text_header_view));
        this.zzkr = (TextView) zzfo.checkNotNull((TextView) findViewById(R$id.cast_featurehighlight_help_text_body_view));
    }

    @Keep
    public void setText(CharSequence charSequence, CharSequence charSequence2) {
        zza(this.zzkq, charSequence);
        zza(this.zzkr, charSequence2);
    }
}
