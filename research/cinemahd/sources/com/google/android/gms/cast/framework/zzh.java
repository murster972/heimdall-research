package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.cast.LaunchOptions;

public interface zzh extends IInterface {
    void a(String str, LaunchOptions launchOptions) throws RemoteException;

    void a(String str, String str2) throws RemoteException;

    void d(int i) throws RemoteException;

    void zzj(String str) throws RemoteException;

    int zzs() throws RemoteException;
}
