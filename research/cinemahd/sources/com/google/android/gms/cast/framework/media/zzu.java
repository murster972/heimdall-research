package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzea;

final class zzu extends RemoteMediaClient.zzc {
    private final /* synthetic */ RemoteMediaClient d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzu(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient) {
        super(remoteMediaClient, googleApiClient);
        this.d = remoteMediaClient;
    }

    /* access modifiers changed from: protected */
    public final void a(zzdd zzdd) throws zzea {
        this.d.c.zzb(this.f3820a);
    }
}
