package com.google.android.gms.cast.framework;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.cast.zza;
import com.google.android.gms.internal.cast.zzc;

public final class zzw extends zza implements zzv {
    zzw(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ISessionManager");
    }

    public final void a(zzx zzx) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) zzx);
        zzb(2, zza);
    }

    public final void b(zzx zzx) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) zzx);
        zzb(3, zza);
    }

    public final IObjectWrapper g() throws RemoteException {
        Parcel zza = zza(1, zza());
        IObjectWrapper a2 = IObjectWrapper.Stub.a(zza.readStrongBinder());
        zza.recycle();
        return a2;
    }

    public final void zza(boolean z, boolean z2) throws RemoteException {
        Parcel zza = zza();
        zzc.writeBoolean(zza, true);
        zzc.writeBoolean(zza, z2);
        zzb(6, zza);
    }

    public final IObjectWrapper zzae() throws RemoteException {
        Parcel zza = zza(7, zza());
        IObjectWrapper a2 = IObjectWrapper.Stub.a(zza.readStrongBinder());
        zza.recycle();
        return a2;
    }

    public final void a(zzn zzn) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) zzn);
        zzb(4, zza);
    }

    public final void b(zzn zzn) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) zzn);
        zzb(5, zza);
    }
}
