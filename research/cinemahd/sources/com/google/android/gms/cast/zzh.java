package com.google.android.gms.cast;

import android.os.RemoteException;
import com.facebook.ads.AdError;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder;
import com.google.android.gms.internal.cast.zzdd;

final class zzh extends Cast.zza {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f3868a;
    private final /* synthetic */ LaunchOptions b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzh(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str, LaunchOptions launchOptions) {
        super(googleApiClient);
        this.f3868a = str;
        this.b = launchOptions;
    }

    /* renamed from: zza */
    public final void doExecute(zzdd zzdd) throws RemoteException {
        try {
            zzdd.zza(this.f3868a, this.b, (BaseImplementation$ResultHolder<Cast.ApplicationConnectionResult>) this);
        } catch (IllegalStateException unused) {
            zzp(AdError.INTERNAL_ERROR_CODE);
        }
    }
}
