package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class zzal implements Parcelable.Creator<MediaMetadata> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ArrayList<WebImage> arrayList = null;
        Bundle bundle = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                arrayList = SafeParcelReader.c(parcel, a2, WebImage.CREATOR);
            } else if (a3 == 3) {
                bundle = SafeParcelReader.f(parcel, a2);
            } else if (a3 != 4) {
                SafeParcelReader.A(parcel, a2);
            } else {
                i = SafeParcelReader.w(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new MediaMetadata(arrayList, bundle, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaMetadata[i];
    }
}
