package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.cast.zza;
import com.google.android.gms.internal.cast.zzc;

public final class zzk extends zza implements zzj {
    zzk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ICastContext");
    }

    public final void a(zzf zzf) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) zzf);
        zzb(3, zza);
    }

    public final void b(zzf zzf) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (IInterface) zzf);
        zzb(4, zza);
    }

    public final boolean i() throws RemoteException {
        Parcel zza = zza(12, zza());
        boolean zza2 = zzc.zza(zza);
        zza.recycle();
        return zza2;
    }

    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.cast.framework.zzv s() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zza()
            r1 = 5
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0011
            r1 = 0
            goto L_0x0025
        L_0x0011:
            java.lang.String r2 = "com.google.android.gms.cast.framework.ISessionManager"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.cast.framework.zzv
            if (r3 == 0) goto L_0x001f
            r1 = r2
            com.google.android.gms.cast.framework.zzv r1 = (com.google.android.gms.cast.framework.zzv) r1
            goto L_0x0025
        L_0x001f:
            com.google.android.gms.cast.framework.zzw r2 = new com.google.android.gms.cast.framework.zzw
            r2.<init>(r1)
            r1 = r2
        L_0x0025:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.zzk.s():com.google.android.gms.cast.framework.zzv");
    }

    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.cast.framework.zzp t() throws android.os.RemoteException {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.zza()
            r1 = 6
            android.os.Parcel r0 = r4.zza(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0011
            r1 = 0
            goto L_0x0025
        L_0x0011:
            java.lang.String r2 = "com.google.android.gms.cast.framework.IDiscoveryManager"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.cast.framework.zzp
            if (r3 == 0) goto L_0x001f
            r1 = r2
            com.google.android.gms.cast.framework.zzp r1 = (com.google.android.gms.cast.framework.zzp) r1
            goto L_0x0025
        L_0x001f:
            com.google.android.gms.cast.framework.zzq r2 = new com.google.android.gms.cast.framework.zzq
            r2.<init>(r1)
            r1 = r2
        L_0x0025:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.zzk.t():com.google.android.gms.cast.framework.zzp");
    }

    public final boolean u() throws RemoteException {
        Parcel zza = zza(2, zza());
        boolean zza2 = zzc.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final Bundle v() throws RemoteException {
        Parcel zza = zza(1, zza());
        Bundle bundle = (Bundle) zzc.zza(zza, Bundle.CREATOR);
        zza.recycle();
        return bundle;
    }
}
