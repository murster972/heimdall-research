package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.cast.framework.R$dimen;
import com.google.android.gms.internal.cast.zzfo;

final class zzj {

    /* renamed from: a  reason: collision with root package name */
    private final Rect f3805a = new Rect();
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private final zza f;

    zzj(zza zza) {
        this.f = (zza) zzfo.checkNotNull(zza);
        Resources resources = zza.getResources();
        this.b = resources.getDimensionPixelSize(R$dimen.cast_libraries_material_featurehighlight_inner_radius);
        this.c = resources.getDimensionPixelOffset(R$dimen.cast_libraries_material_featurehighlight_inner_margin);
        this.d = resources.getDimensionPixelSize(R$dimen.cast_libraries_material_featurehighlight_text_max_width);
        this.e = resources.getDimensionPixelSize(R$dimen.cast_libraries_material_featurehighlight_text_horizontal_offset);
    }

    /* access modifiers changed from: package-private */
    public final void a(Rect rect, Rect rect2) {
        View b2 = this.f.b();
        boolean z = false;
        if (rect.isEmpty() || rect2.isEmpty()) {
            b2.layout(0, 0, 0, 0);
        } else {
            int centerY = rect.centerY();
            int centerX = rect.centerX();
            if (centerY < rect2.centerY()) {
                z = true;
            }
            int max = Math.max(this.b * 2, rect.height()) / 2;
            int i = this.c;
            int i2 = centerY + max + i;
            if (z) {
                a(b2, rect2.width(), rect2.bottom - i2);
                int a2 = a(b2, rect2.left, rect2.right, b2.getMeasuredWidth(), centerX);
                b2.layout(a2, i2, b2.getMeasuredWidth() + a2, b2.getMeasuredHeight() + i2);
            } else {
                int i3 = (centerY - max) - i;
                a(b2, rect2.width(), i3 - rect2.top);
                int a3 = a(b2, rect2.left, rect2.right, b2.getMeasuredWidth(), centerX);
                b2.layout(a3, i3 - b2.getMeasuredHeight(), b2.getMeasuredWidth() + a3, i3);
            }
        }
        this.f3805a.set(b2.getLeft(), b2.getTop(), b2.getRight(), b2.getBottom());
        this.f.c().a(rect, this.f3805a);
        this.f.d().a(rect);
    }

    private final int a(View view, int i, int i2, int i3, int i4) {
        int i5;
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i6 = i3 / 2;
        if (i4 - i <= i2 - i4) {
            i5 = (i4 - i6) + this.e;
        } else {
            i5 = (i4 - i6) - this.e;
        }
        int i7 = marginLayoutParams.leftMargin;
        if (i5 - i7 < i) {
            return i + i7;
        }
        int i8 = marginLayoutParams.rightMargin;
        return (i5 + i3) + i8 > i2 ? (i2 - i3) - i8 : i5;
    }

    private final void a(View view, int i, int i2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        view.measure(View.MeasureSpec.makeMeasureSpec(Math.min((i - marginLayoutParams.leftMargin) - marginLayoutParams.rightMargin, this.d), 1073741824), View.MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE));
    }
}
