package com.google.android.gms.cast.framework.media.widget;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.cast.zzab;

final class zza implements zzab {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ExpandedControllerActivity f3838a;

    zza(ExpandedControllerActivity expandedControllerActivity) {
        this.f3838a = expandedControllerActivity;
    }

    @TargetApi(23)
    public final void zza(Bitmap bitmap) {
        if (bitmap != null) {
            if (this.f3838a.C != null) {
                this.f3838a.C.setVisibility(8);
                if (PlatformVersion.i()) {
                    this.f3838a.C.setTextAppearance(this.f3838a.s);
                } else {
                    this.f3838a.C.setTextAppearance(this.f3838a.getApplicationContext(), this.f3838a.s);
                }
                this.f3838a.C.setTextColor(this.f3838a.n);
            }
            if (this.f3838a.B != null) {
                this.f3838a.B.setVisibility(0);
                this.f3838a.B.setImageBitmap(bitmap);
            }
        }
    }
}
