package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;

public final class zzad extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzad> CREATOR = new zzae();

    /* renamed from: a  reason: collision with root package name */
    private final zzab f3861a;
    private final zzab b;

    public zzad(zzab zzab, zzab zzab2) {
        this.f3861a = zzab;
        this.b = zzab2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzad)) {
            return false;
        }
        zzad zzad = (zzad) obj;
        return zzdk.zza(this.f3861a, zzad.f3861a) && zzdk.zza(this.b, zzad.b);
    }

    public final int hashCode() {
        return Objects.a(this.f3861a, this.b);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, (Parcelable) this.f3861a, i, false);
        SafeParcelWriter.a(parcel, 3, (Parcelable) this.b, i, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
