package com.google.android.gms.cast;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.internal.cast.zzdk;
import org.json.JSONException;
import org.json.JSONObject;

public final class TextTrackStyle extends AbstractSafeParcelable {
    public static final Parcelable.Creator<TextTrackStyle> CREATOR = new zzbz();

    /* renamed from: a  reason: collision with root package name */
    private float f3779a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private String i;
    private int j;
    private int k;
    private String l;
    private JSONObject m;

    TextTrackStyle(float f2, int i2, int i3, int i4, int i5, int i6, int i7, int i8, String str, int i9, int i10, String str2) {
        this.f3779a = f2;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
        this.g = i7;
        this.h = i8;
        this.i = str;
        this.j = i9;
        this.k = i10;
        this.l = str2;
        String str3 = this.l;
        if (str3 != null) {
            try {
                this.m = new JSONObject(str3);
            } catch (JSONException unused) {
                this.m = null;
                this.l = null;
            }
        } else {
            this.m = null;
        }
    }

    private static String j(int i2) {
        return String.format("#%02X%02X%02X%02X", new Object[]{Integer.valueOf(Color.red(i2)), Integer.valueOf(Color.green(i2)), Integer.valueOf(Color.blue(i2)), Integer.valueOf(Color.alpha(i2))});
    }

    public final int A() {
        return this.g;
    }

    public final int B() {
        return this.h;
    }

    public final int C() {
        return this.f;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.json.JSONObject D() {
        /*
            r8 = this;
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.lang.String r1 = "fontScale"
            float r2 = r8.f3779a     // Catch:{ JSONException -> 0x0107 }
            double r2 = (double) r2     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0107 }
            int r1 = r8.b     // Catch:{ JSONException -> 0x0107 }
            if (r1 == 0) goto L_0x001c
            java.lang.String r1 = "foregroundColor"
            int r2 = r8.b     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r2 = j(r2)     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x001c:
            int r1 = r8.c     // Catch:{ JSONException -> 0x0107 }
            if (r1 == 0) goto L_0x002b
            java.lang.String r1 = "backgroundColor"
            int r2 = r8.c     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r2 = j(r2)     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x002b:
            int r1 = r8.d     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r2 = "NONE"
            r3 = 3
            r4 = 1
            r5 = 2
            java.lang.String r6 = "edgeType"
            if (r1 == 0) goto L_0x0058
            if (r1 == r4) goto L_0x0052
            if (r1 == r5) goto L_0x004c
            if (r1 == r3) goto L_0x0046
            r7 = 4
            if (r1 == r7) goto L_0x0040
            goto L_0x005b
        L_0x0040:
            java.lang.String r1 = "DEPRESSED"
            r0.put(r6, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x005b
        L_0x0046:
            java.lang.String r1 = "RAISED"
            r0.put(r6, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x005b
        L_0x004c:
            java.lang.String r1 = "DROP_SHADOW"
            r0.put(r6, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x005b
        L_0x0052:
            java.lang.String r1 = "OUTLINE"
            r0.put(r6, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x005b
        L_0x0058:
            r0.put(r6, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x005b:
            int r1 = r8.e     // Catch:{ JSONException -> 0x0107 }
            if (r1 == 0) goto L_0x006a
            java.lang.String r1 = "edgeColor"
            int r6 = r8.e     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r6 = j(r6)     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r6)     // Catch:{ JSONException -> 0x0107 }
        L_0x006a:
            int r1 = r8.f     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r6 = "NORMAL"
            java.lang.String r7 = "windowType"
            if (r1 == 0) goto L_0x0081
            if (r1 == r4) goto L_0x007d
            if (r1 == r5) goto L_0x0077
            goto L_0x0084
        L_0x0077:
            java.lang.String r1 = "ROUNDED_CORNERS"
            r0.put(r7, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x0084
        L_0x007d:
            r0.put(r7, r6)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x0084
        L_0x0081:
            r0.put(r7, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x0084:
            int r1 = r8.g     // Catch:{ JSONException -> 0x0107 }
            if (r1 == 0) goto L_0x0093
            java.lang.String r1 = "windowColor"
            int r2 = r8.g     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r2 = j(r2)     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x0093:
            int r1 = r8.f     // Catch:{ JSONException -> 0x0107 }
            if (r1 != r5) goto L_0x009e
            java.lang.String r1 = "windowRoundedCornerRadius"
            int r2 = r8.h     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x009e:
            java.lang.String r1 = r8.i     // Catch:{ JSONException -> 0x0107 }
            if (r1 == 0) goto L_0x00a9
            java.lang.String r1 = "fontFamily"
            java.lang.String r2 = r8.i     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x00a9:
            int r1 = r8.j     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r2 = "fontGenericFamily"
            switch(r1) {
                case 0: goto L_0x00d5;
                case 1: goto L_0x00cf;
                case 2: goto L_0x00c9;
                case 3: goto L_0x00c3;
                case 4: goto L_0x00bd;
                case 5: goto L_0x00b7;
                case 6: goto L_0x00b1;
                default: goto L_0x00b0;
            }
        L_0x00b0:
            goto L_0x00da
        L_0x00b1:
            java.lang.String r1 = "SMALL_CAPITALS"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00da
        L_0x00b7:
            java.lang.String r1 = "CURSIVE"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00da
        L_0x00bd:
            java.lang.String r1 = "CASUAL"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00da
        L_0x00c3:
            java.lang.String r1 = "MONOSPACED_SERIF"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00da
        L_0x00c9:
            java.lang.String r1 = "SERIF"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00da
        L_0x00cf:
            java.lang.String r1 = "MONOSPACED_SANS_SERIF"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00da
        L_0x00d5:
            java.lang.String r1 = "SANS_SERIF"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
        L_0x00da:
            int r1 = r8.k     // Catch:{ JSONException -> 0x0107 }
            java.lang.String r2 = "fontStyle"
            if (r1 == 0) goto L_0x00f9
            if (r1 == r4) goto L_0x00f3
            if (r1 == r5) goto L_0x00ed
            if (r1 == r3) goto L_0x00e7
            goto L_0x00fc
        L_0x00e7:
            java.lang.String r1 = "BOLD_ITALIC"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00fc
        L_0x00ed:
            java.lang.String r1 = "ITALIC"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00fc
        L_0x00f3:
            java.lang.String r1 = "BOLD"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x0107 }
            goto L_0x00fc
        L_0x00f9:
            r0.put(r2, r6)     // Catch:{ JSONException -> 0x0107 }
        L_0x00fc:
            org.json.JSONObject r1 = r8.m     // Catch:{ JSONException -> 0x0107 }
            if (r1 == 0) goto L_0x0107
            java.lang.String r1 = "customData"
            org.json.JSONObject r2 = r8.m     // Catch:{ JSONException -> 0x0107 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0107 }
        L_0x0107:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.TextTrackStyle.D():org.json.JSONObject");
    }

    public final void a(float f2) {
        this.f3779a = f2;
    }

    public final void b(int i2) {
        this.c = i2;
    }

    public final void c(int i2) {
        this.e = i2;
    }

    public final void d(int i2) {
        if (i2 < 0 || i2 > 4) {
            throw new IllegalArgumentException("invalid edgeType");
        }
        this.d = i2;
    }

    public final void e(String str) {
        this.i = str;
    }

    public final boolean equals(Object obj) {
        JSONObject jSONObject;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TextTrackStyle)) {
            return false;
        }
        TextTrackStyle textTrackStyle = (TextTrackStyle) obj;
        if ((this.m == null) != (textTrackStyle.m == null)) {
            return false;
        }
        JSONObject jSONObject2 = this.m;
        return (jSONObject2 == null || (jSONObject = textTrackStyle.m) == null || JsonUtils.a(jSONObject2, jSONObject)) && this.f3779a == textTrackStyle.f3779a && this.b == textTrackStyle.b && this.c == textTrackStyle.c && this.d == textTrackStyle.d && this.e == textTrackStyle.e && this.f == textTrackStyle.f && this.h == textTrackStyle.h && zzdk.zza(this.i, textTrackStyle.i) && this.j == textTrackStyle.j && this.k == textTrackStyle.k;
    }

    public final void f(int i2) {
        this.b = i2;
    }

    public final void g(int i2) {
        this.g = i2;
    }

    public final void h(int i2) {
        if (i2 >= 0) {
            this.h = i2;
            return;
        }
        throw new IllegalArgumentException("invalid windowCornerRadius");
    }

    public final int hashCode() {
        return Objects.a(Float.valueOf(this.f3779a), Integer.valueOf(this.b), Integer.valueOf(this.c), Integer.valueOf(this.d), Integer.valueOf(this.e), Integer.valueOf(this.f), Integer.valueOf(this.g), Integer.valueOf(this.h), this.i, Integer.valueOf(this.j), Integer.valueOf(this.k), String.valueOf(this.m));
    }

    public final void i(int i2) {
        if (i2 < 0 || i2 > 2) {
            throw new IllegalArgumentException("invalid windowType");
        }
        this.f = i2;
    }

    public final int s() {
        return this.c;
    }

    public final int t() {
        return this.e;
    }

    public final int u() {
        return this.d;
    }

    public final String v() {
        return this.i;
    }

    public final int w() {
        return this.j;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        JSONObject jSONObject = this.m;
        this.l = jSONObject == null ? null : jSONObject.toString();
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, x());
        SafeParcelWriter.a(parcel, 3, z());
        SafeParcelWriter.a(parcel, 4, s());
        SafeParcelWriter.a(parcel, 5, u());
        SafeParcelWriter.a(parcel, 6, t());
        SafeParcelWriter.a(parcel, 7, C());
        SafeParcelWriter.a(parcel, 8, A());
        SafeParcelWriter.a(parcel, 9, B());
        SafeParcelWriter.a(parcel, 10, v(), false);
        SafeParcelWriter.a(parcel, 11, w());
        SafeParcelWriter.a(parcel, 12, y());
        SafeParcelWriter.a(parcel, 13, this.l, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public final float x() {
        return this.f3779a;
    }

    public final int y() {
        return this.k;
    }

    public final int z() {
        return this.b;
    }

    private static int f(String str) {
        if (str != null && str.length() == 9 && str.charAt(0) == '#') {
            try {
                return Color.argb(Integer.parseInt(str.substring(7, 9), 16), Integer.parseInt(str.substring(1, 3), 16), Integer.parseInt(str.substring(3, 5), 16), Integer.parseInt(str.substring(5, 7), 16));
            } catch (NumberFormatException unused) {
            }
        }
        return 0;
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        this.f3779a = (float) jSONObject.optDouble("fontScale", 1.0d);
        this.b = f(jSONObject.optString("foregroundColor"));
        this.c = f(jSONObject.optString(ViewProps.BACKGROUND_COLOR));
        if (jSONObject.has("edgeType")) {
            String string = jSONObject.getString("edgeType");
            if ("NONE".equals(string)) {
                this.d = 0;
            } else if ("OUTLINE".equals(string)) {
                this.d = 1;
            } else if ("DROP_SHADOW".equals(string)) {
                this.d = 2;
            } else if ("RAISED".equals(string)) {
                this.d = 3;
            } else if ("DEPRESSED".equals(string)) {
                this.d = 4;
            }
        }
        this.e = f(jSONObject.optString("edgeColor"));
        if (jSONObject.has("windowType")) {
            String string2 = jSONObject.getString("windowType");
            if ("NONE".equals(string2)) {
                this.f = 0;
            } else if ("NORMAL".equals(string2)) {
                this.f = 1;
            } else if ("ROUNDED_CORNERS".equals(string2)) {
                this.f = 2;
            }
        }
        this.g = f(jSONObject.optString("windowColor"));
        if (this.f == 2) {
            this.h = jSONObject.optInt("windowRoundedCornerRadius", 0);
        }
        this.i = jSONObject.optString(ViewProps.FONT_FAMILY, (String) null);
        if (jSONObject.has("fontGenericFamily")) {
            String string3 = jSONObject.getString("fontGenericFamily");
            if ("SANS_SERIF".equals(string3)) {
                this.j = 0;
            } else if ("MONOSPACED_SANS_SERIF".equals(string3)) {
                this.j = 1;
            } else if ("SERIF".equals(string3)) {
                this.j = 2;
            } else if ("MONOSPACED_SERIF".equals(string3)) {
                this.j = 3;
            } else if ("CASUAL".equals(string3)) {
                this.j = 4;
            } else if ("CURSIVE".equals(string3)) {
                this.j = 5;
            } else if ("SMALL_CAPITALS".equals(string3)) {
                this.j = 6;
            }
        }
        if (jSONObject.has(ViewProps.FONT_STYLE)) {
            String string4 = jSONObject.getString(ViewProps.FONT_STYLE);
            if ("NORMAL".equals(string4)) {
                this.k = 0;
            } else if ("BOLD".equals(string4)) {
                this.k = 1;
            } else if ("ITALIC".equals(string4)) {
                this.k = 2;
            } else if ("BOLD_ITALIC".equals(string4)) {
                this.k = 3;
            }
        }
        this.m = jSONObject.optJSONObject("customData");
    }

    public final void e(int i2) {
        if (i2 < 0 || i2 > 6) {
            throw new IllegalArgumentException("invalid fontGenericFamily");
        }
        this.j = i2;
    }

    public TextTrackStyle() {
        this(1.0f, 0, 0, -1, 0, -1, 0, 0, (String) null, -1, -1, (String) null);
    }
}
