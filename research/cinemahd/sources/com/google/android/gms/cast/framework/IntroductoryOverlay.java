package com.google.android.gms.cast.framework;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.cast.zzn;
import com.google.android.gms.internal.cast.zzr;

public interface IntroductoryOverlay {

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Activity f3790a;
        private final View b;
        private int c;
        private String d;
        private OnOverlayDismissedListener e;
        private boolean f;
        private float g;
        private String h;

        public Builder(Activity activity, MenuItem menuItem) {
            Preconditions.a(activity);
            this.f3790a = activity;
            Preconditions.a(menuItem);
            this.b = menuItem.getActionView();
        }

        public Builder a(int i) {
            this.c = this.f3790a.getResources().getColor(i);
            return this;
        }

        public final Activity b() {
            return this.f3790a;
        }

        public Builder c() {
            this.f = true;
            return this;
        }

        public final View d() {
            return this.b;
        }

        public final OnOverlayDismissedListener e() {
            return this.e;
        }

        public final int f() {
            return this.c;
        }

        public final boolean g() {
            return this.f;
        }

        public final String h() {
            return this.d;
        }

        public final String i() {
            return this.h;
        }

        public final float j() {
            return this.g;
        }

        public Builder a(String str) {
            this.d = str;
            return this;
        }

        public Builder a(OnOverlayDismissedListener onOverlayDismissedListener) {
            this.e = onOverlayDismissedListener;
            return this;
        }

        public IntroductoryOverlay a() {
            if (PlatformVersion.c()) {
                return new zzn(this);
            }
            return new zzr(this);
        }
    }

    public interface OnOverlayDismissedListener {
        void a();
    }

    public static class zza {
        public static void a(Context context) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("googlecast-introOverlayShown", true).apply();
        }

        public static boolean b(Context context) {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("googlecast-introOverlayShown", false);
        }
    }

    void remove();

    void show();
}
