package com.google.android.gms.cast.framework;

public final class R$id {
    public static final int ad_background_image_view = 2131296342;
    public static final int ad_container = 2131296346;
    public static final int ad_image_view = 2131296351;
    public static final int ad_in_progress_label = 2131296352;
    public static final int ad_label = 2131296353;
    public static final int ad_skip_button = 2131296358;
    public static final int ad_skip_text = 2131296359;
    public static final int audio_list_view = 2131296385;
    public static final int background_image_view = 2131296391;
    public static final int background_place_holder_image_view = 2131296392;
    public static final int blurred_background_image_view = 2131296407;
    public static final int button = 2131296438;
    public static final int button_0 = 2131296441;
    public static final int button_1 = 2131296442;
    public static final int button_2 = 2131296443;
    public static final int button_3 = 2131296444;
    public static final int button_play_pause_toggle = 2131296451;
    public static final int cast_button_type_closed_caption = 2131296463;
    public static final int cast_button_type_custom = 2131296464;
    public static final int cast_button_type_empty = 2131296465;
    public static final int cast_button_type_forward_30_seconds = 2131296466;
    public static final int cast_button_type_mute_toggle = 2131296467;
    public static final int cast_button_type_play_pause_toggle = 2131296468;
    public static final int cast_button_type_rewind_30_seconds = 2131296469;
    public static final int cast_button_type_skip_next = 2131296470;
    public static final int cast_button_type_skip_previous = 2131296471;
    public static final int cast_featurehighlight_help_text_body_view = 2131296472;
    public static final int cast_featurehighlight_help_text_header_view = 2131296473;
    public static final int cast_featurehighlight_view = 2131296474;
    public static final int center = 2131296485;
    public static final int container_all = 2131296512;
    public static final int container_current = 2131296513;
    public static final int controllers = 2131296523;
    public static final int end_text = 2131296589;
    public static final int end_text_container = 2131296590;
    public static final int expanded_controller_layout = 2131296619;
    public static final int icon_view = 2131296684;
    public static final int live_indicator_dot = 2131296762;
    public static final int live_indicator_text = 2131296763;
    public static final int live_indicators = 2131296764;
    public static final int loading_indicator = 2131296767;
    public static final int progressBar = 2131296953;
    public static final int radio = 2131296964;
    public static final int seek_bar = 2131297029;
    public static final int seek_bar_indicators = 2131297030;
    public static final int start_text = 2131297068;
    public static final int start_text_container = 2131297069;
    public static final int status_text = 2131297072;
    public static final int subtitle_view = 2131297080;
    public static final int tab_host = 2131297091;
    public static final int text = 2131297106;
    public static final int textTitle = 2131297112;
    public static final int text_list_view = 2131297135;
    public static final int title_view = 2131297157;
    public static final int toolbar = 2131297158;
    public static final int tooltip = 2131297163;
    public static final int tooltip_container = 2131297164;

    private R$id() {
    }
}
