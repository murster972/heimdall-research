package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class VideoInfo extends AbstractSafeParcelable {
    public static final Parcelable.Creator<VideoInfo> CREATOR = new zzcb();

    /* renamed from: a  reason: collision with root package name */
    private int f3781a;
    private int b;
    private int c;

    VideoInfo(int i, int i2, int i3) {
        this.f3781a = i;
        this.b = i2;
        this.c = i3;
    }

    static VideoInfo a(JSONObject jSONObject) {
        int i;
        if (jSONObject == null) {
            return null;
        }
        try {
            String string = jSONObject.getString("hdrType");
            char c2 = 65535;
            int hashCode = string.hashCode();
            if (hashCode != 3218) {
                if (hashCode != 103158) {
                    if (hashCode != 113729) {
                        if (hashCode == 99136405) {
                            if (string.equals("hdr10")) {
                                c2 = 1;
                            }
                        }
                    } else if (string.equals("sdr")) {
                        c2 = 3;
                    }
                } else if (string.equals("hdr")) {
                    c2 = 2;
                }
            } else if (string.equals("dv")) {
                c2 = 0;
            }
            if (c2 == 0) {
                i = 3;
            } else if (c2 == 1) {
                i = 2;
            } else if (c2 == 2) {
                i = 4;
            } else if (c2 != 3) {
                Log.d("VideoInfo", String.format(Locale.ROOT, "Unknown HDR type: %s", new Object[]{string}));
                i = 0;
            } else {
                i = 1;
            }
            return new VideoInfo(jSONObject.getInt("width"), jSONObject.getInt("height"), i);
        } catch (JSONException e) {
            Log.d("VideoInfo", String.format(Locale.ROOT, "Error while creating a VideoInfo instance from JSON: %s", new Object[]{e.getMessage()}));
            return null;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VideoInfo)) {
            return false;
        }
        VideoInfo videoInfo = (VideoInfo) obj;
        return this.b == videoInfo.t() && this.f3781a == videoInfo.u() && this.c == videoInfo.s();
    }

    public final int hashCode() {
        return Objects.a(Integer.valueOf(this.b), Integer.valueOf(this.f3781a), Integer.valueOf(this.c));
    }

    public final int s() {
        return this.c;
    }

    public final int t() {
        return this.b;
    }

    public final int u() {
        return this.f3781a;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, u());
        SafeParcelWriter.a(parcel, 3, t());
        SafeParcelWriter.a(parcel, 4, s());
        SafeParcelWriter.a(parcel, a2);
    }
}
