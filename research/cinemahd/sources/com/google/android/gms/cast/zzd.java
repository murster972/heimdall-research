package com.google.android.gms.cast;

import android.os.Parcelable;

public final class zzd implements Parcelable.Creator<ApplicationMetadata> {
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r11) {
        /*
            r10 = this;
            int r0 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.b(r11)
            r1 = 0
            r3 = r1
            r4 = r3
            r5 = r4
            r6 = r5
            r7 = r6
            r8 = r7
            r9 = r8
        L_0x000c:
            int r1 = r11.dataPosition()
            if (r1 >= r0) goto L_0x004b
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r11)
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((int) r1)
            switch(r2) {
                case 2: goto L_0x0046;
                case 3: goto L_0x0041;
                case 4: goto L_0x003a;
                case 5: goto L_0x0035;
                case 6: goto L_0x0030;
                case 7: goto L_0x0026;
                case 8: goto L_0x0021;
                default: goto L_0x001d;
            }
        L_0x001d:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.A(r11, r1)
            goto L_0x000c
        L_0x0021:
            java.lang.String r9 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r11, r1)
            goto L_0x000c
        L_0x0026:
            android.os.Parcelable$Creator r2 = android.net.Uri.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r11, (int) r1, r2)
            r8 = r1
            android.net.Uri r8 = (android.net.Uri) r8
            goto L_0x000c
        L_0x0030:
            java.lang.String r7 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r11, r1)
            goto L_0x000c
        L_0x0035:
            java.util.ArrayList r6 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.q(r11, r1)
            goto L_0x000c
        L_0x003a:
            android.os.Parcelable$Creator<com.google.android.gms.common.images.WebImage> r2 = com.google.android.gms.common.images.WebImage.CREATOR
            java.util.ArrayList r5 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r11, r1, r2)
            goto L_0x000c
        L_0x0041:
            java.lang.String r4 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r11, r1)
            goto L_0x000c
        L_0x0046:
            java.lang.String r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r11, r1)
            goto L_0x000c
        L_0x004b:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.r(r11, r0)
            com.google.android.gms.cast.ApplicationMetadata r11 = new com.google.android.gms.cast.ApplicationMetadata
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.zzd.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ApplicationMetadata[i];
    }
}
