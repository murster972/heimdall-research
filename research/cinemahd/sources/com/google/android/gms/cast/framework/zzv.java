package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzv extends IInterface {
    void a(zzn zzn) throws RemoteException;

    void a(zzx zzx) throws RemoteException;

    void b(zzn zzn) throws RemoteException;

    void b(zzx zzx) throws RemoteException;

    IObjectWrapper g() throws RemoteException;

    void zza(boolean z, boolean z2) throws RemoteException;

    IObjectWrapper zzae() throws RemoteException;
}
