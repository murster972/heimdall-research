package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import java.util.Locale;

public class LaunchOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<LaunchOptions> CREATOR = new zzai();

    /* renamed from: a  reason: collision with root package name */
    private boolean f3766a;
    private String b;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private LaunchOptions f3767a = new LaunchOptions();

        public final Builder a(Locale locale) {
            this.f3767a.e(zzdk.zza(locale));
            return this;
        }

        public final LaunchOptions a() {
            return this.f3767a;
        }
    }

    LaunchOptions(boolean z, String str) {
        this.f3766a = z;
        this.b = str;
    }

    public void e(String str) {
        this.b = str;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LaunchOptions)) {
            return false;
        }
        LaunchOptions launchOptions = (LaunchOptions) obj;
        return this.f3766a == launchOptions.f3766a && zzdk.zza(this.b, launchOptions.b);
    }

    public int hashCode() {
        return Objects.a(Boolean.valueOf(this.f3766a), this.b);
    }

    public String s() {
        return this.b;
    }

    public boolean t() {
        return this.f3766a;
    }

    public String toString() {
        return String.format("LaunchOptions(relaunchIfRunning=%b, language=%s)", new Object[]{Boolean.valueOf(this.f3766a), this.b});
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, t());
        SafeParcelWriter.a(parcel, 3, s(), false);
        SafeParcelWriter.a(parcel, a2);
    }

    public LaunchOptions() {
        this(false, zzdk.zza(Locale.getDefault()));
    }
}
