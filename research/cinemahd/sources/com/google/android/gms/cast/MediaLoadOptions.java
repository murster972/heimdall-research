package com.google.android.gms.cast;

import org.json.JSONObject;

public class MediaLoadOptions {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3770a;
    private long b;
    private double c;
    private long[] d;
    private JSONObject e;
    private String f;
    private String g;

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private boolean f3771a = true;
        private long b = 0;
        private double c = 1.0d;
        private long[] d = null;
        private JSONObject e = null;
        private String f = null;
        private String g = null;

        public Builder a(boolean z) {
            this.f3771a = z;
            return this;
        }

        public Builder a(long j) {
            this.b = j;
            return this;
        }

        public MediaLoadOptions a() {
            return new MediaLoadOptions(this.f3771a, this.b, this.c, this.d, this.e, this.f, this.g);
        }
    }

    private MediaLoadOptions(boolean z, long j, double d2, long[] jArr, JSONObject jSONObject, String str, String str2) {
        this.f3770a = z;
        this.b = j;
        this.c = d2;
        this.d = jArr;
        this.e = jSONObject;
        this.f = str;
        this.g = str2;
    }

    public long[] a() {
        return this.d;
    }

    public boolean b() {
        return this.f3770a;
    }

    public String c() {
        return this.f;
    }

    public String d() {
        return this.g;
    }

    public JSONObject e() {
        return this.e;
    }

    public long f() {
        return this.b;
    }

    public double g() {
        return this.c;
    }
}
