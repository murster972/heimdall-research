package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.media.zzb;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

public class ImagePicker {

    /* renamed from: a  reason: collision with root package name */
    private final zzb f3809a = new zza();

    private class zza extends zzb.zza {
        private zza() {
        }

        public final WebImage a(MediaMetadata mediaMetadata, int i) {
            return ImagePicker.this.a(mediaMetadata, i);
        }

        public final IObjectWrapper k() {
            return ObjectWrapper.a(ImagePicker.this);
        }

        public final int zzs() {
            return 12451009;
        }

        public final WebImage a(MediaMetadata mediaMetadata, ImageHints imageHints) {
            return ImagePicker.this.a(mediaMetadata, imageHints);
        }
    }

    @Deprecated
    public WebImage a(MediaMetadata mediaMetadata, int i) {
        if (mediaMetadata == null || !mediaMetadata.v()) {
            return null;
        }
        return mediaMetadata.t().get(0);
    }

    public WebImage a(MediaMetadata mediaMetadata, ImageHints imageHints) {
        return a(mediaMetadata, imageHints.t());
    }

    public final zzb a() {
        return this.f3809a;
    }
}
