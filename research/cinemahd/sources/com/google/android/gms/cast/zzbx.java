package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import com.vungle.warren.model.ReportDBAdapter;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbx extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzbx> CREATOR = new zzby();

    /* renamed from: a  reason: collision with root package name */
    private final String f3866a;
    private final int b;
    private final int c;
    private final String d;

    public zzbx(String str, int i, int i2, String str2) {
        this.f3866a = str;
        this.b = i;
        this.c = i2;
        this.d = str2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzbx)) {
            return false;
        }
        zzbx zzbx = (zzbx) obj;
        return zzdk.zza(this.f3866a, zzbx.f3866a) && zzdk.zza(Integer.valueOf(this.b), Integer.valueOf(zzbx.b)) && zzdk.zza(Integer.valueOf(this.c), Integer.valueOf(zzbx.c)) && zzdk.zza(zzbx.d, this.d);
    }

    public final int hashCode() {
        return Objects.a(this.f3866a, Integer.valueOf(this.b), Integer.valueOf(this.c), this.d);
    }

    public final JSONObject s() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(ReportDBAdapter.ReportColumns.COLUMN_URL, this.f3866a);
        jSONObject.put("protocolType", this.b);
        jSONObject.put("initialTime", this.c);
        jSONObject.put("hlsSegmentFormat", this.d);
        return jSONObject;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.f3866a, false);
        SafeParcelWriter.a(parcel, 3, this.b);
        SafeParcelWriter.a(parcel, 4, this.c);
        SafeParcelWriter.a(parcel, 5, this.d, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
