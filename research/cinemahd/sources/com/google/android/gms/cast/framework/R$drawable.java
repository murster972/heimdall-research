package com.google.android.gms.cast.framework;

public final class R$drawable {
    public static final int cast_abc_scrubber_control_off_mtrl_alpha = 2131230886;
    public static final int cast_abc_scrubber_control_to_pressed_mtrl_000 = 2131230887;
    public static final int cast_abc_scrubber_control_to_pressed_mtrl_005 = 2131230888;
    public static final int cast_abc_scrubber_primary_mtrl_alpha = 2131230889;
    public static final int cast_album_art_placeholder = 2131230890;
    public static final int cast_album_art_placeholder_large = 2131230891;
    public static final int cast_expanded_controller_actionbar_bg_gradient_light = 2131230892;
    public static final int cast_expanded_controller_bg_gradient_light = 2131230893;
    public static final int cast_expanded_controller_live_indicator_drawable = 2131230894;
    public static final int cast_expanded_controller_seekbar_thumb = 2131230895;
    public static final int cast_expanded_controller_seekbar_track = 2131230896;
    public static final int cast_ic_expanded_controller_closed_caption = 2131230897;
    public static final int cast_ic_expanded_controller_forward30 = 2131230898;
    public static final int cast_ic_expanded_controller_mute = 2131230899;
    public static final int cast_ic_expanded_controller_pause = 2131230900;
    public static final int cast_ic_expanded_controller_play = 2131230901;
    public static final int cast_ic_expanded_controller_rewind30 = 2131230902;
    public static final int cast_ic_expanded_controller_skip_next = 2131230903;
    public static final int cast_ic_expanded_controller_skip_previous = 2131230904;
    public static final int cast_ic_expanded_controller_stop = 2131230905;
    public static final int cast_ic_mini_controller_closed_caption = 2131230906;
    public static final int cast_ic_mini_controller_forward30 = 2131230907;
    public static final int cast_ic_mini_controller_mute = 2131230908;
    public static final int cast_ic_mini_controller_pause = 2131230909;
    public static final int cast_ic_mini_controller_pause_large = 2131230910;
    public static final int cast_ic_mini_controller_play = 2131230911;
    public static final int cast_ic_mini_controller_play_large = 2131230912;
    public static final int cast_ic_mini_controller_rewind30 = 2131230913;
    public static final int cast_ic_mini_controller_skip_next = 2131230914;
    public static final int cast_ic_mini_controller_skip_prev = 2131230915;
    public static final int cast_ic_mini_controller_stop = 2131230916;
    public static final int cast_ic_mini_controller_stop_large = 2131230917;
    public static final int cast_ic_notification_disconnect = 2131230922;
    public static final int cast_ic_notification_forward = 2131230923;
    public static final int cast_ic_notification_forward10 = 2131230924;
    public static final int cast_ic_notification_forward30 = 2131230925;
    public static final int cast_ic_notification_pause = 2131230927;
    public static final int cast_ic_notification_play = 2131230928;
    public static final int cast_ic_notification_rewind = 2131230929;
    public static final int cast_ic_notification_rewind10 = 2131230930;
    public static final int cast_ic_notification_rewind30 = 2131230931;
    public static final int cast_ic_notification_skip_next = 2131230932;
    public static final int cast_ic_notification_skip_prev = 2131230933;
    public static final int cast_ic_notification_small_icon = 2131230934;
    public static final int cast_ic_notification_stop_live_stream = 2131230935;
    public static final int cast_ic_stop_circle_filled_grey600 = 2131230936;
    public static final int cast_ic_stop_circle_filled_white = 2131230937;
    public static final int cast_mini_controller_gradient_light = 2131230938;
    public static final int cast_mini_controller_progress_drawable = 2131230939;
    public static final int cast_rounded_corner = 2131230940;
    public static final int cast_skip_ad_label_border = 2131230941;
    public static final int quantum_ic_art_track_grey600_48 = 2131231320;
    public static final int quantum_ic_bigtop_updates_white_24 = 2131231321;
    public static final int quantum_ic_cast_connected_white_24 = 2131231322;
    public static final int quantum_ic_cast_white_36 = 2131231323;
    public static final int quantum_ic_clear_white_24 = 2131231324;
    public static final int quantum_ic_closed_caption_grey600_36 = 2131231325;
    public static final int quantum_ic_closed_caption_white_36 = 2131231326;
    public static final int quantum_ic_forward_10_white_24 = 2131231327;
    public static final int quantum_ic_forward_30_grey600_36 = 2131231328;
    public static final int quantum_ic_forward_30_white_24 = 2131231329;
    public static final int quantum_ic_forward_30_white_36 = 2131231330;
    public static final int quantum_ic_keyboard_arrow_down_white_36 = 2131231331;
    public static final int quantum_ic_pause_circle_filled_grey600_36 = 2131231332;
    public static final int quantum_ic_pause_circle_filled_white_36 = 2131231333;
    public static final int quantum_ic_pause_grey600_36 = 2131231334;
    public static final int quantum_ic_pause_grey600_48 = 2131231335;
    public static final int quantum_ic_pause_white_24 = 2131231336;
    public static final int quantum_ic_play_arrow_grey600_36 = 2131231337;
    public static final int quantum_ic_play_arrow_grey600_48 = 2131231338;
    public static final int quantum_ic_play_arrow_white_24 = 2131231339;
    public static final int quantum_ic_play_circle_filled_grey600_36 = 2131231340;
    public static final int quantum_ic_play_circle_filled_white_36 = 2131231341;
    public static final int quantum_ic_refresh_white_24 = 2131231342;
    public static final int quantum_ic_replay_10_white_24 = 2131231343;
    public static final int quantum_ic_replay_30_grey600_36 = 2131231344;
    public static final int quantum_ic_replay_30_white_24 = 2131231345;
    public static final int quantum_ic_replay_30_white_36 = 2131231346;
    public static final int quantum_ic_replay_white_24 = 2131231347;
    public static final int quantum_ic_skip_next_grey600_36 = 2131231348;
    public static final int quantum_ic_skip_next_white_24 = 2131231349;
    public static final int quantum_ic_skip_next_white_36 = 2131231350;
    public static final int quantum_ic_skip_previous_grey600_36 = 2131231351;
    public static final int quantum_ic_skip_previous_white_24 = 2131231352;
    public static final int quantum_ic_skip_previous_white_36 = 2131231353;
    public static final int quantum_ic_stop_grey600_36 = 2131231354;
    public static final int quantum_ic_stop_grey600_48 = 2131231355;
    public static final int quantum_ic_stop_white_24 = 2131231356;
    public static final int quantum_ic_volume_off_grey600_36 = 2131231357;
    public static final int quantum_ic_volume_off_white_36 = 2131231358;
    public static final int quantum_ic_volume_up_grey600_36 = 2131231359;
    public static final int quantum_ic_volume_up_white_36 = 2131231360;

    private R$drawable() {
    }
}
