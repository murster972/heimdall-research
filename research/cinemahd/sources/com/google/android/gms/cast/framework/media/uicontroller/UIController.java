package com.google.android.gms.cast.framework.media.uicontroller;

import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

public class UIController {
    private RemoteMediaClient zzid;

    /* access modifiers changed from: protected */
    public RemoteMediaClient getRemoteMediaClient() {
        return this.zzid;
    }

    public void onMediaStatusUpdated() {
    }

    public void onSendingRemoteMediaRequest() {
    }

    public void onSessionConnected(CastSession castSession) {
        if (castSession != null) {
            this.zzid = castSession.h();
        } else {
            this.zzid = null;
        }
    }

    public void onSessionEnded() {
        this.zzid = null;
    }
}
