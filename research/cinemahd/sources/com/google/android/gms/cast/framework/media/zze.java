package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.cast.zza;
import java.util.ArrayList;
import java.util.List;

public final class zze extends zza implements zzd {
    zze(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.media.INotificationActionsProvider");
    }

    public final int[] l() throws RemoteException {
        Parcel zza = zza(4, zza());
        int[] createIntArray = zza.createIntArray();
        zza.recycle();
        return createIntArray;
    }

    public final List<NotificationAction> p() throws RemoteException {
        Parcel zza = zza(3, zza());
        ArrayList<NotificationAction> createTypedArrayList = zza.createTypedArrayList(NotificationAction.CREATOR);
        zza.recycle();
        return createTypedArrayList;
    }
}
