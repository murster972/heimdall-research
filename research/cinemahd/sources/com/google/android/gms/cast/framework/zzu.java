package com.google.android.gms.cast.framework;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.cast.zza;
import com.google.android.gms.internal.cast.zzc;

public final class zzu extends zza implements zzt {
    zzu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ISession");
    }

    public final void c(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        zzb(15, zza);
    }

    public final void e(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        zzb(13, zza);
    }

    public final void f(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        zzb(12, zza);
    }

    public final boolean isConnected() throws RemoteException {
        Parcel zza = zza(5, zza());
        boolean zza2 = zzc.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final boolean isConnecting() throws RemoteException {
        Parcel zza = zza(6, zza());
        boolean zza2 = zzc.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final IObjectWrapper j() throws RemoteException {
        Parcel zza = zza(1, zza());
        IObjectWrapper a2 = IObjectWrapper.Stub.a(zza.readStrongBinder());
        zza.recycle();
        return a2;
    }

    public final boolean n() throws RemoteException {
        Parcel zza = zza(9, zza());
        boolean zza2 = zzc.zza(zza);
        zza.recycle();
        return zza2;
    }

    public final boolean r() throws RemoteException {
        Parcel zza = zza(10, zza());
        boolean zza2 = zzc.zza(zza);
        zza.recycle();
        return zza2;
    }
}
