package com.google.android.gms.cast;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.cast.zzdd;

final class zze extends Api.AbstractClientBuilder<zzdd, Cast.CastOptions> {
    zze() {
    }

    public final /* synthetic */ Api.Client buildClient(Context context, Looper looper, ClientSettings clientSettings, Object obj, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Cast.CastOptions castOptions = (Cast.CastOptions) obj;
        Preconditions.a(castOptions, (Object) "Setting the API options is required.");
        return new zzdd(context, looper, clientSettings, castOptions.f3763a, (long) castOptions.d, castOptions.b, castOptions.c, connectionCallbacks, onConnectionFailedListener);
    }
}
