package com.google.android.gms.cast.framework.media.uicontroller;

import android.view.View;

final class zzg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ UIMediaController f3831a;

    zzg(UIMediaController uIMediaController) {
        this.f3831a = uIMediaController;
    }

    public final void onClick(View view) {
        this.f3831a.e(view);
    }
}
