package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.internal.cast.zza;
import com.google.android.gms.internal.cast.zzc;

public final class zzm extends zza implements zzl {
    zzm(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.ICastSession");
    }

    public final void onConnected(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (Parcelable) bundle);
        zzb(1, zza);
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (Parcelable) connectionResult);
        zzb(3, zza);
    }

    public final void onConnectionSuspended(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        zzb(2, zza);
    }

    public final void zza(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) throws RemoteException {
        Parcel zza = zza();
        zzc.zza(zza, (Parcelable) applicationMetadata);
        zza.writeString(str);
        zza.writeString(str2);
        zzc.writeBoolean(zza, z);
        zzb(4, zza);
    }

    public final void zzi(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        zzb(5, zza);
    }

    public final void zza(boolean z, int i) throws RemoteException {
        Parcel zza = zza();
        zzc.writeBoolean(zza, z);
        zza.writeInt(0);
        zzb(6, zza);
    }
}
