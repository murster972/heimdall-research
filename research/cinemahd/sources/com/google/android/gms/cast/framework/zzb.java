package com.google.android.gms.cast.framework;

import android.os.Parcelable;

public final class zzb implements Parcelable.Creator<CastOptions> {
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r18) {
        /*
            r17 = this;
            r0 = r18
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.b(r18)
            r2 = 0
            r3 = 0
            r4 = 0
            r7 = r3
            r8 = r7
            r10 = r8
            r12 = r10
            r14 = r4
            r9 = 0
            r11 = 0
            r13 = 0
            r16 = 0
        L_0x0014:
            int r2 = r18.dataPosition()
            if (r2 >= r1) goto L_0x0060
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r18)
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((int) r2)
            switch(r3) {
                case 2: goto L_0x005b;
                case 3: goto L_0x0056;
                case 4: goto L_0x0051;
                case 5: goto L_0x0047;
                case 6: goto L_0x0042;
                case 7: goto L_0x0038;
                case 8: goto L_0x0033;
                case 9: goto L_0x002e;
                case 10: goto L_0x0029;
                default: goto L_0x0025;
            }
        L_0x0025:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.A(r0, r2)
            goto L_0x0014
        L_0x0029:
            boolean r16 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.s(r0, r2)
            goto L_0x0014
        L_0x002e:
            double r14 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.t(r0, r2)
            goto L_0x0014
        L_0x0033:
            boolean r13 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.s(r0, r2)
            goto L_0x0014
        L_0x0038:
            android.os.Parcelable$Creator<com.google.android.gms.cast.framework.media.CastMediaOptions> r3 = com.google.android.gms.cast.framework.media.CastMediaOptions.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r0, (int) r2, r3)
            r12 = r2
            com.google.android.gms.cast.framework.media.CastMediaOptions r12 = (com.google.android.gms.cast.framework.media.CastMediaOptions) r12
            goto L_0x0014
        L_0x0042:
            boolean r11 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.s(r0, r2)
            goto L_0x0014
        L_0x0047:
            android.os.Parcelable$Creator<com.google.android.gms.cast.LaunchOptions> r3 = com.google.android.gms.cast.LaunchOptions.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r0, (int) r2, r3)
            r10 = r2
            com.google.android.gms.cast.LaunchOptions r10 = (com.google.android.gms.cast.LaunchOptions) r10
            goto L_0x0014
        L_0x0051:
            boolean r9 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.s(r0, r2)
            goto L_0x0014
        L_0x0056:
            java.util.ArrayList r8 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.q(r0, r2)
            goto L_0x0014
        L_0x005b:
            java.lang.String r7 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r0, r2)
            goto L_0x0014
        L_0x0060:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.r(r0, r1)
            com.google.android.gms.cast.framework.CastOptions r0 = new com.google.android.gms.cast.framework.CastOptions
            r6 = r0
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r16)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.zzb.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CastOptions[i];
    }
}
