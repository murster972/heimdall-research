package com.google.android.gms.cast.framework.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import org.json.JSONObject;

public class MediaIntentReceiver extends BroadcastReceiver {
    /* access modifiers changed from: protected */
    public void a(Context context, String str, Intent intent) {
    }

    /* access modifiers changed from: protected */
    public void a(Session session) {
        RemoteMediaClient b;
        if ((session instanceof CastSession) && (b = b((CastSession) session)) != null && !b.q()) {
            b.c((JSONObject) null);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Session session) {
        RemoteMediaClient b;
        if ((session instanceof CastSession) && (b = b((CastSession) session)) != null && !b.q()) {
            b.d((JSONObject) null);
        }
    }

    /* access modifiers changed from: protected */
    public void c(Session session) {
        if (session instanceof CastSession) {
            a((CastSession) session);
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            SessionManager c = CastContext.a(context).c();
            char c2 = 65535;
            switch (action.hashCode()) {
                case -1699820260:
                    if (action.equals("com.google.android.gms.cast.framework.action.REWIND")) {
                        c2 = 4;
                        break;
                    }
                    break;
                case -945151566:
                    if (action.equals("com.google.android.gms.cast.framework.action.SKIP_NEXT")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case -945080078:
                    if (action.equals("com.google.android.gms.cast.framework.action.SKIP_PREV")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case -668151673:
                    if (action.equals("com.google.android.gms.cast.framework.action.STOP_CASTING")) {
                        c2 = 5;
                        break;
                    }
                    break;
                case -124479363:
                    if (action.equals("com.google.android.gms.cast.framework.action.DISCONNECT")) {
                        c2 = 6;
                        break;
                    }
                    break;
                case 235550565:
                    if (action.equals("com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 1362116196:
                    if (action.equals("com.google.android.gms.cast.framework.action.FORWARD")) {
                        c2 = 3;
                        break;
                    }
                    break;
                case 1997055314:
                    if (action.equals("android.intent.action.MEDIA_BUTTON")) {
                        c2 = 7;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    c(c.b());
                    return;
                case 1:
                    a(c.b());
                    return;
                case 2:
                    b(c.b());
                    return;
                case 3:
                    a(c.b(), intent.getLongExtra("googlecast-extra_skip_step_ms", 0));
                    return;
                case 4:
                    b(c.b(), intent.getLongExtra("googlecast-extra_skip_step_ms", 0));
                    return;
                case 5:
                    c.a(true);
                    return;
                case 6:
                    c.a(false);
                    return;
                case 7:
                    a(c.b(), intent);
                    return;
                default:
                    a(context, action, intent);
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Session session, long j) {
        if (session instanceof CastSession) {
            a((CastSession) session, j);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Session session, long j) {
        if (session instanceof CastSession) {
            a((CastSession) session, -j);
        }
    }

    private static RemoteMediaClient b(CastSession castSession) {
        if (castSession == null || !castSession.b()) {
            return null;
        }
        return castSession.h();
    }

    /* access modifiers changed from: protected */
    public void a(Session session, Intent intent) {
        KeyEvent keyEvent;
        if ((session instanceof CastSession) && intent.hasExtra("android.intent.extra.KEY_EVENT") && (keyEvent = (KeyEvent) intent.getExtras().get("android.intent.extra.KEY_EVENT")) != null && keyEvent.getAction() == 0 && keyEvent.getKeyCode() == 85) {
            a((CastSession) session);
        }
    }

    private static void a(CastSession castSession) {
        RemoteMediaClient b = b(castSession);
        if (b != null) {
            b.v();
        }
    }

    private static void a(CastSession castSession, long j) {
        RemoteMediaClient b;
        if (j != 0 && (b = b(castSession)) != null && !b.m() && !b.q()) {
            b.a(b.b() + j);
        }
    }
}
