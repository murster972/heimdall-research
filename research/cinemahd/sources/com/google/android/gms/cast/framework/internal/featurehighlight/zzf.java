package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class zzf extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Runnable f3803a;
    private final /* synthetic */ zza b;

    zzf(zza zza, Runnable runnable) {
        this.b = zza;
        this.f3803a = runnable;
    }

    public final void onAnimationEnd(Animator animator) {
        this.b.setVisibility(8);
        Animator unused = this.b.h = null;
        Runnable runnable = this.f3803a;
        if (runnable != null) {
            runnable.run();
        }
    }
}
