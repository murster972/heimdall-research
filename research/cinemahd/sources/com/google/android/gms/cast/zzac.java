package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzac implements Parcelable.Creator<zzab> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        float f = 0.0f;
        float f2 = 0.0f;
        float f3 = 0.0f;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                f = SafeParcelReader.u(parcel, a2);
            } else if (a3 == 3) {
                f2 = SafeParcelReader.u(parcel, a2);
            } else if (a3 != 4) {
                SafeParcelReader.A(parcel, a2);
            } else {
                f3 = SafeParcelReader.u(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzab(f, f2, f3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzab[i];
    }
}
