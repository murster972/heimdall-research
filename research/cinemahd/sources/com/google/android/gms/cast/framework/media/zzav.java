package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

final class zzav implements ResultCallback<Status> {

    /* renamed from: a  reason: collision with root package name */
    private final long f3843a;
    private final /* synthetic */ RemoteMediaClient.zza b;

    zzav(RemoteMediaClient.zza zza, long j) {
        this.b = zza;
        this.f3843a = j;
    }

    public final /* synthetic */ void onResult(Result result) {
        Status status = (Status) result;
        if (!status.v()) {
            RemoteMediaClient.this.c.zza(this.f3843a, status.s());
        }
    }
}
