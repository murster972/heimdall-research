package com.google.android.gms.cast.framework.media.widget;

import android.os.Looper;
import com.google.android.gms.cast.AdBreakClipInfo;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.internal.cast.zzez;
import java.util.TimerTask;

final class zzc extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AdBreakClipInfo f3840a;
    final /* synthetic */ RemoteMediaClient b;
    final /* synthetic */ ExpandedControllerActivity c;

    zzc(ExpandedControllerActivity expandedControllerActivity, AdBreakClipInfo adBreakClipInfo, RemoteMediaClient remoteMediaClient) {
        this.c = expandedControllerActivity;
        this.f3840a = adBreakClipInfo;
        this.b = remoteMediaClient;
    }

    public final void run() {
        new zzez(Looper.getMainLooper()).post(new zzd(this));
    }
}
