package com.google.android.gms.cast.framework.media;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TabHost;
import androidx.fragment.app.DialogFragment;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.cast.framework.R$layout;
import com.google.android.gms.cast.framework.R$string;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class TracksChooserDialogFragment extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3823a;
    private List<MediaTrack> b;
    private List<MediaTrack> c;
    private long[] d;
    /* access modifiers changed from: private */
    public Dialog e;
    private RemoteMediaClient f;
    private MediaInfo g;
    private long[] h;

    /* access modifiers changed from: private */
    public final void a(zzbf zzbf, zzbf zzbf2) {
        if (!this.f3823a || !this.f.k()) {
            f();
            return;
        }
        ArrayList arrayList = new ArrayList();
        MediaTrack a2 = zzbf.a();
        if (!(a2 == null || a2.u() == -1)) {
            arrayList.add(Long.valueOf(a2.u()));
        }
        MediaTrack a3 = zzbf2.a();
        if (a3 != null) {
            arrayList.add(Long.valueOf(a3.u()));
        }
        long[] jArr = this.d;
        if (jArr != null && jArr.length > 0) {
            HashSet hashSet = new HashSet();
            for (MediaTrack u : this.c) {
                hashSet.add(Long.valueOf(u.u()));
            }
            for (MediaTrack u2 : this.b) {
                hashSet.add(Long.valueOf(u2.u()));
            }
            for (long j : this.d) {
                if (!hashSet.contains(Long.valueOf(j))) {
                    arrayList.add(Long.valueOf(j));
                }
            }
        }
        long[] jArr2 = new long[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            jArr2[i] = ((Long) arrayList.get(i)).longValue();
        }
        Arrays.sort(jArr2);
        this.f.a(jArr2);
        f();
    }

    public static TracksChooserDialogFragment e() {
        return new TracksChooserDialogFragment();
    }

    private final void f() {
        Dialog dialog = this.e;
        if (dialog != null) {
            dialog.cancel();
            this.e = null;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3823a = true;
        this.c = new ArrayList();
        this.b = new ArrayList();
        this.d = new long[0];
        CastSession a2 = CastContext.a(getContext()).c().a();
        if (a2 == null || !a2.b()) {
            this.f3823a = false;
            return;
        }
        this.f = a2.h();
        RemoteMediaClient remoteMediaClient = this.f;
        if (remoteMediaClient == null || !remoteMediaClient.k() || this.f.e() == null) {
            this.f3823a = false;
            return;
        }
        long[] jArr = this.h;
        if (jArr != null) {
            this.d = jArr;
        } else {
            MediaStatus f2 = this.f.f();
            if (f2 != null) {
                this.d = f2.s();
            }
        }
        MediaInfo mediaInfo = this.g;
        if (mediaInfo == null) {
            mediaInfo = this.f.e();
        }
        if (mediaInfo == null) {
            this.f3823a = false;
            return;
        }
        List<MediaTrack> x = mediaInfo.x();
        if (x == null) {
            this.f3823a = false;
            return;
        }
        this.c = a(x, 2);
        this.b = a(x, 1);
        if (!this.b.isEmpty()) {
            List<MediaTrack> list = this.b;
            MediaTrack.Builder builder = new MediaTrack.Builder(-1, 1);
            builder.d(getActivity().getString(R$string.cast_tracks_chooser_dialog_none));
            builder.a(2);
            builder.a("");
            list.add(0, builder.a());
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        int a2 = a(this.b, this.d, 0);
        int a3 = a(this.c, this.d, -1);
        zzbf zzbf = new zzbf(getActivity(), this.b, a2);
        zzbf zzbf2 = new zzbf(getActivity(), this.c, a3);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View inflate = getActivity().getLayoutInflater().inflate(R$layout.cast_tracks_chooser_dialog_layout, (ViewGroup) null);
        ListView listView = (ListView) inflate.findViewById(R$id.text_list_view);
        ListView listView2 = (ListView) inflate.findViewById(R$id.audio_list_view);
        TabHost tabHost = (TabHost) inflate.findViewById(R$id.tab_host);
        tabHost.setup();
        if (zzbf.getCount() == 0) {
            listView.setVisibility(4);
        } else {
            listView.setAdapter(zzbf);
            TabHost.TabSpec newTabSpec = tabHost.newTabSpec("textTab");
            newTabSpec.setContent(R$id.text_list_view);
            newTabSpec.setIndicator(getActivity().getString(R$string.cast_tracks_chooser_dialog_subtitles));
            tabHost.addTab(newTabSpec);
        }
        if (zzbf2.getCount() <= 1) {
            listView2.setVisibility(4);
        } else {
            listView2.setAdapter(zzbf2);
            TabHost.TabSpec newTabSpec2 = tabHost.newTabSpec("audioTab");
            newTabSpec2.setContent(R$id.audio_list_view);
            newTabSpec2.setIndicator(getActivity().getString(R$string.cast_tracks_chooser_dialog_audio));
            tabHost.addTab(newTabSpec2);
        }
        builder.setView(inflate).setPositiveButton(getActivity().getString(R$string.cast_tracks_chooser_dialog_ok), new zzbe(this, zzbf, zzbf2)).setNegativeButton(R$string.cast_tracks_chooser_dialog_cancel, new zzbd(this));
        Dialog dialog = this.e;
        if (dialog != null) {
            dialog.cancel();
            this.e = null;
        }
        this.e = builder.create();
        return this.e;
    }

    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage((Message) null);
        }
        super.onDestroyView();
    }

    private static ArrayList<MediaTrack> a(List<MediaTrack> list, int i) {
        ArrayList<MediaTrack> arrayList = new ArrayList<>();
        if (list != null) {
            for (MediaTrack next : list) {
                if (next.y() == i) {
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    private static int a(List<MediaTrack> list, long[] jArr, int i) {
        if (!(jArr == null || list == null)) {
            for (int i2 = 0; i2 < list.size(); i2++) {
                for (long j : jArr) {
                    if (j == list.get(i2).u()) {
                        return i2;
                    }
                }
            }
        }
        return i;
    }
}
