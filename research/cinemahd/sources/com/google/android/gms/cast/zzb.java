package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzb implements Parcelable.Creator<AdBreakInfo> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String[] strArr = null;
        long j = 0;
        long j2 = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    j = SafeParcelReader.y(parcel, a2);
                    break;
                case 3:
                    str = SafeParcelReader.o(parcel, a2);
                    break;
                case 4:
                    j2 = SafeParcelReader.y(parcel, a2);
                    break;
                case 5:
                    z = SafeParcelReader.s(parcel, a2);
                    break;
                case 6:
                    strArr = SafeParcelReader.p(parcel, a2);
                    break;
                case 7:
                    z2 = SafeParcelReader.s(parcel, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel, b);
        return new AdBreakInfo(j, str, j2, z, strArr, z2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AdBreakInfo[i];
    }
}
