package com.google.android.gms.cast.framework.media;

import android.widget.RadioButton;
import android.widget.TextView;

final class zzbh {

    /* renamed from: a  reason: collision with root package name */
    final TextView f3851a;
    final RadioButton b;

    private zzbh(zzbf zzbf, TextView textView, RadioButton radioButton) {
        this.f3851a = textView;
        this.b = radioButton;
    }
}
