package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.zzam;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzea;

final class zzad extends RemoteMediaClient.zzc {
    private final /* synthetic */ MediaInfo d;
    private final /* synthetic */ zzam e = null;
    private final /* synthetic */ MediaLoadOptions f;
    private final /* synthetic */ RemoteMediaClient g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzad(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, MediaInfo mediaInfo, zzam zzam, MediaLoadOptions mediaLoadOptions) {
        super(remoteMediaClient, googleApiClient);
        this.g = remoteMediaClient;
        this.d = mediaInfo;
        this.f = mediaLoadOptions;
    }

    /* access modifiers changed from: protected */
    public final void a(zzdd zzdd) throws zzea {
        this.g.c.zza(this.f3820a, this.d, this.e, this.f);
    }
}
