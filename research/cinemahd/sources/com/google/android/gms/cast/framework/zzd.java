package com.google.android.gms.cast.framework;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzd extends zzo {

    /* renamed from: a  reason: collision with root package name */
    private final CastStateListener f3858a;

    public zzd(CastStateListener castStateListener) {
        this.f3858a = castStateListener;
    }

    public final void a(int i) {
        this.f3858a.a(i);
    }

    public final int zzs() {
        return 12451009;
    }

    public final IObjectWrapper zzt() {
        return ObjectWrapper.a(this.f3858a);
    }
}
