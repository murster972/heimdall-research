package com.google.android.gms.cast.framework.media;

public abstract class NotificationActionsProvider {

    /* renamed from: a  reason: collision with root package name */
    private final zzd f3815a;

    public final zzd a() {
        return this.f3815a;
    }
}
