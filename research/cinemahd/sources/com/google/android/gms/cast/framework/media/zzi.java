package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.AppVisibilityListener;

final class zzi implements AppVisibilityListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaNotificationService f3853a;

    zzi(MediaNotificationService mediaNotificationService) {
        this.f3853a = mediaNotificationService;
    }

    public final void e() {
        this.f3853a.stopForeground(true);
    }

    public final void f() {
        if (this.f3853a.o != null) {
            MediaNotificationService mediaNotificationService = this.f3853a;
            mediaNotificationService.startForeground(1, mediaNotificationService.o);
            return;
        }
        this.f3853a.stopForeground(true);
    }
}
