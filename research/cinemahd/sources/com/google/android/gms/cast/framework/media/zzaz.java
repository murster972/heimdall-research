package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import java.util.Set;
import java.util.TimerTask;

final class zzaz extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ RemoteMediaClient.zze f3847a;

    zzaz(RemoteMediaClient.zze zze, RemoteMediaClient remoteMediaClient) {
        this.f3847a = zze;
    }

    public final void run() {
        RemoteMediaClient.zze zze = this.f3847a;
        RemoteMediaClient.this.a((Set<RemoteMediaClient.ProgressListener>) zze.f3822a);
        RemoteMediaClient.this.b.postDelayed(this, this.f3847a.b);
    }
}
