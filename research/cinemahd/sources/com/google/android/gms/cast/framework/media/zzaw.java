package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Status;

final class zzaw implements RemoteMediaClient.MediaChannelResult {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Status f3844a;

    zzaw(RemoteMediaClient.zzb zzb, Status status) {
        this.f3844a = status;
    }

    public final Status getStatus() {
        return this.f3844a;
    }
}
