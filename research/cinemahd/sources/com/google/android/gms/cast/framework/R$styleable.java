package com.google.android.gms.cast.framework;

import com.yoku.marumovie.R;

public final class R$styleable {
    public static final int[] CastExpandedController = {R.attr.castAdBreakMarkerColor, R.attr.castAdInProgressLabelTextAppearance, R.attr.castAdInProgressTextColor, R.attr.castAdLabelColor, R.attr.castAdLabelTextAppearance, R.attr.castAdLabelTextColor, R.attr.castButtonColor, R.attr.castClosedCaptionsButtonDrawable, R.attr.castControlButtons, R.attr.castExpandedControllerLoadingIndicatorColor, R.attr.castForward30ButtonDrawable, R.attr.castLiveIndicatorColor, R.attr.castMuteToggleButtonDrawable, R.attr.castPauseButtonDrawable, R.attr.castPlayButtonDrawable, R.attr.castRewind30ButtonDrawable, R.attr.castSeekBarProgressAndThumbColor, R.attr.castSeekBarProgressDrawable, R.attr.castSeekBarThumbDrawable, R.attr.castSkipNextButtonDrawable, R.attr.castSkipPreviousButtonDrawable, R.attr.castStopButtonDrawable};
    public static final int CastExpandedController_castAdBreakMarkerColor = 0;
    public static final int CastExpandedController_castAdInProgressLabelTextAppearance = 1;
    public static final int CastExpandedController_castAdInProgressTextColor = 2;
    public static final int CastExpandedController_castAdLabelColor = 3;
    public static final int CastExpandedController_castAdLabelTextAppearance = 4;
    public static final int CastExpandedController_castAdLabelTextColor = 5;
    public static final int CastExpandedController_castButtonColor = 6;
    public static final int CastExpandedController_castClosedCaptionsButtonDrawable = 7;
    public static final int CastExpandedController_castControlButtons = 8;
    public static final int CastExpandedController_castExpandedControllerLoadingIndicatorColor = 9;
    public static final int CastExpandedController_castForward30ButtonDrawable = 10;
    public static final int CastExpandedController_castLiveIndicatorColor = 11;
    public static final int CastExpandedController_castMuteToggleButtonDrawable = 12;
    public static final int CastExpandedController_castPauseButtonDrawable = 13;
    public static final int CastExpandedController_castPlayButtonDrawable = 14;
    public static final int CastExpandedController_castRewind30ButtonDrawable = 15;
    public static final int CastExpandedController_castSeekBarProgressAndThumbColor = 16;
    public static final int CastExpandedController_castSeekBarProgressDrawable = 17;
    public static final int CastExpandedController_castSeekBarThumbDrawable = 18;
    public static final int CastExpandedController_castSkipNextButtonDrawable = 19;
    public static final int CastExpandedController_castSkipPreviousButtonDrawable = 20;
    public static final int CastExpandedController_castStopButtonDrawable = 21;
    public static final int[] CastIntroOverlay = {R.attr.castBackgroundColor, R.attr.castButtonBackgroundColor, R.attr.castButtonText, R.attr.castButtonTextAppearance, R.attr.castFocusRadius, R.attr.castTitleTextAppearance};
    public static final int CastIntroOverlay_castBackgroundColor = 0;
    public static final int CastIntroOverlay_castButtonBackgroundColor = 1;
    public static final int CastIntroOverlay_castButtonText = 2;
    public static final int CastIntroOverlay_castButtonTextAppearance = 3;
    public static final int CastIntroOverlay_castFocusRadius = 4;
    public static final int CastIntroOverlay_castTitleTextAppearance = 5;
    public static final int[] CastMiniController = {R.attr.castBackground, R.attr.castButtonColor, R.attr.castClosedCaptionsButtonDrawable, R.attr.castControlButtons, R.attr.castForward30ButtonDrawable, R.attr.castLargePauseButtonDrawable, R.attr.castLargePlayButtonDrawable, R.attr.castLargeStopButtonDrawable, R.attr.castMiniControllerLoadingIndicatorColor, R.attr.castMuteToggleButtonDrawable, R.attr.castPauseButtonDrawable, R.attr.castPlayButtonDrawable, R.attr.castProgressBarColor, R.attr.castRewind30ButtonDrawable, R.attr.castShowImageThumbnail, R.attr.castSkipNextButtonDrawable, R.attr.castSkipPreviousButtonDrawable, R.attr.castStopButtonDrawable, R.attr.castSubtitleTextAppearance, R.attr.castTitleTextAppearance};
    public static final int CastMiniController_castBackground = 0;
    public static final int CastMiniController_castButtonColor = 1;
    public static final int CastMiniController_castClosedCaptionsButtonDrawable = 2;
    public static final int CastMiniController_castControlButtons = 3;
    public static final int CastMiniController_castForward30ButtonDrawable = 4;
    public static final int CastMiniController_castLargePauseButtonDrawable = 5;
    public static final int CastMiniController_castLargePlayButtonDrawable = 6;
    public static final int CastMiniController_castLargeStopButtonDrawable = 7;
    public static final int CastMiniController_castMiniControllerLoadingIndicatorColor = 8;
    public static final int CastMiniController_castMuteToggleButtonDrawable = 9;
    public static final int CastMiniController_castPauseButtonDrawable = 10;
    public static final int CastMiniController_castPlayButtonDrawable = 11;
    public static final int CastMiniController_castProgressBarColor = 12;
    public static final int CastMiniController_castRewind30ButtonDrawable = 13;
    public static final int CastMiniController_castShowImageThumbnail = 14;
    public static final int CastMiniController_castSkipNextButtonDrawable = 15;
    public static final int CastMiniController_castSkipPreviousButtonDrawable = 16;
    public static final int CastMiniController_castStopButtonDrawable = 17;
    public static final int CastMiniController_castSubtitleTextAppearance = 18;
    public static final int CastMiniController_castTitleTextAppearance = 19;
    public static final int[] CustomCastTheme = {R.attr.castExpandedControllerStyle, R.attr.castIntroOverlayStyle, R.attr.castMiniControllerStyle};
    public static final int CustomCastTheme_castExpandedControllerStyle = 0;
    public static final int CustomCastTheme_castIntroOverlayStyle = 1;
    public static final int CustomCastTheme_castMiniControllerStyle = 2;

    private R$styleable() {
    }
}
