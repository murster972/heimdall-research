package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzab extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzab> CREATOR = new zzac();

    /* renamed from: a  reason: collision with root package name */
    private final float f3860a;
    private final float b;
    private final float c;

    public zzab(float f, float f2, float f3) {
        this.f3860a = f;
        this.b = f2;
        this.c = f3;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzab)) {
            return false;
        }
        zzab zzab = (zzab) obj;
        return this.f3860a == zzab.f3860a && this.b == zzab.b && this.c == zzab.c;
    }

    public final int hashCode() {
        return Objects.a(Float.valueOf(this.f3860a), Float.valueOf(this.b), Float.valueOf(this.c));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.f3860a);
        SafeParcelWriter.a(parcel, 3, this.b);
        SafeParcelWriter.a(parcel, 4, this.c);
        SafeParcelWriter.a(parcel, a2);
    }
}
