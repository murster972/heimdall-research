package com.google.android.gms.cast;

import com.google.android.gms.common.Feature;

public final class zzaf {

    /* renamed from: a  reason: collision with root package name */
    private static final Feature f3862a = new Feature("client_side_logging", 1);
    public static final Feature[] b = {f3862a};
}
