package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import java.util.Arrays;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdBreakInfo extends AbstractSafeParcelable {
    public static final Parcelable.Creator<AdBreakInfo> CREATOR = new zzb();

    /* renamed from: a  reason: collision with root package name */
    private final long f3759a;
    private final String b;
    private final long c;
    private final boolean d;
    private String[] e;
    private final boolean f;

    public AdBreakInfo(long j, String str, long j2, boolean z, String[] strArr, boolean z2) {
        this.f3759a = j;
        this.b = str;
        this.c = j2;
        this.d = z;
        this.e = strArr;
        this.f = z2;
    }

    static AdBreakInfo a(JSONObject jSONObject) {
        String[] strArr;
        if (jSONObject != null && jSONObject.has("id") && jSONObject.has(ViewProps.POSITION)) {
            try {
                String string = jSONObject.getString("id");
                long j = (long) (((double) jSONObject.getLong(ViewProps.POSITION)) * 1000.0d);
                boolean optBoolean = jSONObject.optBoolean("isWatched");
                long optLong = (long) (((double) jSONObject.optLong("duration")) * 1000.0d);
                JSONArray optJSONArray = jSONObject.optJSONArray("breakClipIds");
                if (optJSONArray != null) {
                    String[] strArr2 = new String[optJSONArray.length()];
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        strArr2[i] = optJSONArray.getString(i);
                    }
                    strArr = strArr2;
                } else {
                    strArr = null;
                }
                return new AdBreakInfo(j, string, optLong, optBoolean, strArr, jSONObject.optBoolean("isEmbedded"));
            } catch (JSONException e2) {
                Log.d("AdBreakInfo", String.format(Locale.ROOT, "Error while creating an AdBreakInfo from JSON: %s", new Object[]{e2.getMessage()}));
            }
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdBreakInfo)) {
            return false;
        }
        AdBreakInfo adBreakInfo = (AdBreakInfo) obj;
        return zzdk.zza(this.b, adBreakInfo.b) && this.f3759a == adBreakInfo.f3759a && this.c == adBreakInfo.c && this.d == adBreakInfo.d && Arrays.equals(this.e, adBreakInfo.e) && this.f == adBreakInfo.f;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String[] s() {
        return this.e;
    }

    public long t() {
        return this.c;
    }

    public String u() {
        return this.b;
    }

    public long v() {
        return this.f3759a;
    }

    public boolean w() {
        return this.f;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, v());
        SafeParcelWriter.a(parcel, 3, u(), false);
        SafeParcelWriter.a(parcel, 4, t());
        SafeParcelWriter.a(parcel, 5, x());
        SafeParcelWriter.a(parcel, 6, s(), false);
        SafeParcelWriter.a(parcel, 7, w());
        SafeParcelWriter.a(parcel, a2);
    }

    public boolean x() {
        return this.d;
    }

    public final JSONObject y() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", this.b);
            jSONObject.put(ViewProps.POSITION, ((double) this.f3759a) / 1000.0d);
            jSONObject.put("isWatched", this.d);
            jSONObject.put("isEmbedded", this.f);
            jSONObject.put("duration", ((double) this.c) / 1000.0d);
            if (this.e != null) {
                JSONArray jSONArray = new JSONArray();
                for (String put : this.e) {
                    jSONArray.put(put);
                }
                jSONObject.put("breakClipIds", jSONArray);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
