package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzn extends IInterface {
    void a(int i) throws RemoteException;

    int zzs() throws RemoteException;

    IObjectWrapper zzt() throws RemoteException;
}
