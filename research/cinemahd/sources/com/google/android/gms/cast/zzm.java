package com.google.android.gms.cast;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.Status;

final class zzm implements Cast.ApplicationConnectionResult {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Status f3871a;

    zzm(Cast.zza zza, Status status) {
        this.f3871a = status;
    }

    public final ApplicationMetadata getApplicationMetadata() {
        return null;
    }

    public final String getApplicationStatus() {
        return null;
    }

    public final String getSessionId() {
        return null;
    }

    public final Status getStatus() {
        return this.f3871a;
    }

    public final boolean getWasLaunched() {
        return false;
    }
}
