package com.google.android.gms.cast;

import android.os.Parcelable;

public final class zzaj implements Parcelable.Creator<MediaInfo> {
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r23) {
        /*
            r22 = this;
            r0 = r23
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.b(r23)
            r2 = 0
            r4 = 0
            r5 = 0
            r11 = r2
            r20 = r11
            r7 = r4
            r9 = r7
            r10 = r9
            r13 = r10
            r14 = r13
            r15 = r14
            r16 = r15
            r17 = r16
            r18 = r17
            r19 = r18
            r8 = 0
        L_0x001c:
            int r2 = r23.dataPosition()
            if (r2 >= r1) goto L_0x0088
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r23)
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((int) r2)
            switch(r3) {
                case 2: goto L_0x0083;
                case 3: goto L_0x007e;
                case 4: goto L_0x0079;
                case 5: goto L_0x006f;
                case 6: goto L_0x006a;
                case 7: goto L_0x0063;
                case 8: goto L_0x0059;
                case 9: goto L_0x0054;
                case 10: goto L_0x004d;
                case 11: goto L_0x0046;
                case 12: goto L_0x0041;
                case 13: goto L_0x0036;
                case 14: goto L_0x0031;
                default: goto L_0x002d;
            }
        L_0x002d:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.A(r0, r2)
            goto L_0x001c
        L_0x0031:
            long r20 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.y(r0, r2)
            goto L_0x001c
        L_0x0036:
            android.os.Parcelable$Creator<com.google.android.gms.cast.VastAdsRequest> r3 = com.google.android.gms.cast.VastAdsRequest.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r0, (int) r2, r3)
            r19 = r2
            com.google.android.gms.cast.VastAdsRequest r19 = (com.google.android.gms.cast.VastAdsRequest) r19
            goto L_0x001c
        L_0x0041:
            java.lang.String r18 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r0, r2)
            goto L_0x001c
        L_0x0046:
            android.os.Parcelable$Creator<com.google.android.gms.cast.AdBreakClipInfo> r3 = com.google.android.gms.cast.AdBreakClipInfo.CREATOR
            java.util.ArrayList r17 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r0, r2, r3)
            goto L_0x001c
        L_0x004d:
            android.os.Parcelable$Creator<com.google.android.gms.cast.AdBreakInfo> r3 = com.google.android.gms.cast.AdBreakInfo.CREATOR
            java.util.ArrayList r16 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r0, r2, r3)
            goto L_0x001c
        L_0x0054:
            java.lang.String r15 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r0, r2)
            goto L_0x001c
        L_0x0059:
            android.os.Parcelable$Creator<com.google.android.gms.cast.TextTrackStyle> r3 = com.google.android.gms.cast.TextTrackStyle.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r0, (int) r2, r3)
            r14 = r2
            com.google.android.gms.cast.TextTrackStyle r14 = (com.google.android.gms.cast.TextTrackStyle) r14
            goto L_0x001c
        L_0x0063:
            android.os.Parcelable$Creator<com.google.android.gms.cast.MediaTrack> r3 = com.google.android.gms.cast.MediaTrack.CREATOR
            java.util.ArrayList r13 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r0, r2, r3)
            goto L_0x001c
        L_0x006a:
            long r11 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.y(r0, r2)
            goto L_0x001c
        L_0x006f:
            android.os.Parcelable$Creator<com.google.android.gms.cast.MediaMetadata> r3 = com.google.android.gms.cast.MediaMetadata.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r0, (int) r2, r3)
            r10 = r2
            com.google.android.gms.cast.MediaMetadata r10 = (com.google.android.gms.cast.MediaMetadata) r10
            goto L_0x001c
        L_0x0079:
            java.lang.String r9 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r0, r2)
            goto L_0x001c
        L_0x007e:
            int r8 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.w(r0, r2)
            goto L_0x001c
        L_0x0083:
            java.lang.String r7 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r0, r2)
            goto L_0x001c
        L_0x0088:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.r(r0, r1)
            com.google.android.gms.cast.MediaInfo r0 = new com.google.android.gms.cast.MediaInfo
            r6 = r0
            r6.<init>(r7, r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19, r20)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.zzaj.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaInfo[i];
    }
}
