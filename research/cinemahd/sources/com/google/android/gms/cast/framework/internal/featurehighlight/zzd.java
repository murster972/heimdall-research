package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.view.View;

final class zzd implements View.OnLayoutChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Runnable f3801a = null;
    private final /* synthetic */ zza b;

    zzd(zza zza, Runnable runnable) {
        this.b = zza;
    }

    public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        Runnable runnable = this.f3801a;
        if (runnable != null) {
            runnable.run();
        }
        this.b.a();
        this.b.removeOnLayoutChangeListener(this);
    }
}
