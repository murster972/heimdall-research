package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public class ImageHints extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ImageHints> CREATOR = new zzf();

    /* renamed from: a  reason: collision with root package name */
    private final int f3808a;
    private final int b;
    private final int c;

    public ImageHints(int i, int i2, int i3) {
        this.f3808a = i;
        this.b = i2;
        this.c = i3;
    }

    public int s() {
        return this.c;
    }

    public int t() {
        return this.f3808a;
    }

    public int u() {
        return this.b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, t());
        SafeParcelWriter.a(parcel, 3, u());
        SafeParcelWriter.a(parcel, 4, s());
        SafeParcelWriter.a(parcel, a2);
    }
}
