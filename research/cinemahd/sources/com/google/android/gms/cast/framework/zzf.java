package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzf extends IInterface {
    void e() throws RemoteException;

    void f() throws RemoteException;

    int zzs() throws RemoteException;

    IObjectWrapper zzt() throws RemoteException;
}
