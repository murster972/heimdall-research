package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzby implements Parcelable.Creator<zzbx> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 == 3) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 4) {
                i2 = SafeParcelReader.w(parcel, a2);
            } else if (a3 != 5) {
                SafeParcelReader.A(parcel, a2);
            } else {
                str2 = SafeParcelReader.o(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzbx(str, i, i2, str2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbx[i];
    }
}
