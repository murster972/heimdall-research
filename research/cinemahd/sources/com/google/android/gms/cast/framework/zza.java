package com.google.android.gms.cast.framework;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zza extends zzg {

    /* renamed from: a  reason: collision with root package name */
    private final AppVisibilityListener f3856a;

    public zza(AppVisibilityListener appVisibilityListener) {
        this.f3856a = appVisibilityListener;
    }

    public final void e() {
        this.f3856a.e();
    }

    public final void f() {
        this.f3856a.f();
    }

    public final int zzs() {
        return 12451009;
    }

    public final IObjectWrapper zzt() {
        return ObjectWrapper.a(this.f3856a);
    }
}
