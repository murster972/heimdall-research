package com.google.android.gms.cast.framework.media.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.google.android.gms.cast.framework.R$attr;
import com.google.android.gms.cast.framework.R$dimen;
import com.google.android.gms.cast.framework.R$drawable;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.cast.framework.R$layout;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.R$style;
import com.google.android.gms.cast.framework.R$styleable;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.cast.zzdw;

public class MiniControllerFragment extends Fragment implements ControlButtonsContainer {
    private static final zzdw y = new zzdw("MiniControllerFragment");

    /* renamed from: a  reason: collision with root package name */
    private boolean f3837a;
    private int b;
    private int c;
    private TextView d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int[] i;
    private ImageView[] j = new ImageView[3];
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private UIMediaController x;

    private final void a(RelativeLayout relativeLayout, int i2, int i3) {
        ImageView imageView = (ImageView) relativeLayout.findViewById(i2);
        int i4 = this.i[i3];
        if (i4 == R$id.cast_button_type_empty) {
            imageView.setVisibility(4);
        } else if (i4 == R$id.cast_button_type_custom) {
        } else {
            if (i4 == R$id.cast_button_type_play_pause_toggle) {
                int i5 = this.l;
                int i6 = this.m;
                int i7 = this.n;
                if (this.k == 1) {
                    i5 = this.o;
                    i6 = this.p;
                    i7 = this.q;
                }
                Drawable a2 = zze.a(getContext(), this.h, i5);
                Drawable a3 = zze.a(getContext(), this.h, i6);
                Drawable a4 = zze.a(getContext(), this.h, i7);
                imageView.setImageDrawable(a3);
                ProgressBar progressBar = new ProgressBar(getContext());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(8, i2);
                layoutParams.addRule(6, i2);
                layoutParams.addRule(5, i2);
                layoutParams.addRule(7, i2);
                layoutParams.addRule(15);
                progressBar.setLayoutParams(layoutParams);
                progressBar.setVisibility(8);
                Drawable indeterminateDrawable = progressBar.getIndeterminateDrawable();
                int i8 = this.g;
                if (!(i8 == 0 || indeterminateDrawable == null)) {
                    indeterminateDrawable.setColorFilter(i8, PorterDuff.Mode.SRC_IN);
                }
                relativeLayout.addView(progressBar);
                this.x.a(imageView, a2, a3, a4, progressBar, true);
            } else if (i4 == R$id.cast_button_type_skip_previous) {
                imageView.setImageDrawable(zze.a(getContext(), this.h, this.r));
                imageView.setContentDescription(getResources().getString(R$string.cast_skip_prev));
                this.x.b((View) imageView, 0);
            } else if (i4 == R$id.cast_button_type_skip_next) {
                imageView.setImageDrawable(zze.a(getContext(), this.h, this.s));
                imageView.setContentDescription(getResources().getString(R$string.cast_skip_next));
                this.x.a((View) imageView, 0);
            } else if (i4 == R$id.cast_button_type_rewind_30_seconds) {
                imageView.setImageDrawable(zze.a(getContext(), this.h, this.t));
                imageView.setContentDescription(getResources().getString(R$string.cast_rewind_30));
                this.x.b((View) imageView, 30000);
            } else if (i4 == R$id.cast_button_type_forward_30_seconds) {
                imageView.setImageDrawable(zze.a(getContext(), this.h, this.u));
                imageView.setContentDescription(getResources().getString(R$string.cast_forward_30));
                this.x.a((View) imageView, 30000);
            } else if (i4 == R$id.cast_button_type_mute_toggle) {
                imageView.setImageDrawable(zze.a(getContext(), this.h, this.v));
                this.x.a(imageView);
            } else if (i4 == R$id.cast_button_type_closed_caption) {
                imageView.setImageDrawable(zze.a(getContext(), this.h, this.w));
                this.x.a((View) imageView);
            }
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.x = new UIMediaController(getActivity());
        View inflate = layoutInflater.inflate(R$layout.cast_mini_controller, viewGroup);
        inflate.setVisibility(8);
        this.x.c(inflate, 8);
        RelativeLayout relativeLayout = (RelativeLayout) inflate.findViewById(R$id.container_current);
        int i2 = this.e;
        if (i2 != 0) {
            relativeLayout.setBackgroundResource(i2);
        }
        ImageView imageView = (ImageView) inflate.findViewById(R$id.icon_view);
        TextView textView = (TextView) inflate.findViewById(R$id.title_view);
        if (this.b != 0) {
            textView.setTextAppearance(getActivity(), this.b);
        }
        this.d = (TextView) inflate.findViewById(R$id.subtitle_view);
        if (this.c != 0) {
            this.d.setTextAppearance(getActivity(), this.c);
        }
        ProgressBar progressBar = (ProgressBar) inflate.findViewById(R$id.progressBar);
        if (this.f != 0) {
            ((LayerDrawable) progressBar.getProgressDrawable()).setColorFilter(this.f, PorterDuff.Mode.SRC_IN);
        }
        this.x.a(textView, "com.google.android.gms.cast.metadata.TITLE");
        this.x.a(this.d);
        this.x.a(progressBar);
        this.x.b((View) relativeLayout);
        if (this.f3837a) {
            this.x.a(imageView, new ImageHints(2, getResources().getDimensionPixelSize(R$dimen.cast_mini_controller_icon_width), getResources().getDimensionPixelSize(R$dimen.cast_mini_controller_icon_height)), R$drawable.cast_album_art_placeholder);
        } else {
            imageView.setVisibility(8);
        }
        this.j[0] = (ImageView) relativeLayout.findViewById(R$id.button_0);
        this.j[1] = (ImageView) relativeLayout.findViewById(R$id.button_1);
        this.j[2] = (ImageView) relativeLayout.findViewById(R$id.button_2);
        a(relativeLayout, R$id.button_0, 0);
        a(relativeLayout, R$id.button_1, 1);
        a(relativeLayout, R$id.button_2, 2);
        return inflate;
    }

    public void onDestroy() {
        UIMediaController uIMediaController = this.x;
        if (uIMediaController != null) {
            uIMediaController.a();
            this.x = null;
        }
        super.onDestroy();
    }

    public void onInflate(Context context, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(context, attributeSet, bundle);
        if (this.i == null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.CastMiniController, R$attr.castMiniControllerStyle, R$style.CastMiniController);
            this.f3837a = obtainStyledAttributes.getBoolean(R$styleable.CastMiniController_castShowImageThumbnail, true);
            this.b = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castTitleTextAppearance, 0);
            this.c = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castSubtitleTextAppearance, 0);
            this.e = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castBackground, 0);
            this.f = obtainStyledAttributes.getColor(R$styleable.CastMiniController_castProgressBarColor, 0);
            this.g = obtainStyledAttributes.getColor(R$styleable.CastMiniController_castMiniControllerLoadingIndicatorColor, this.f);
            this.h = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castButtonColor, 0);
            this.l = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castPlayButtonDrawable, 0);
            this.m = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castPauseButtonDrawable, 0);
            this.n = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castStopButtonDrawable, 0);
            this.o = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castPlayButtonDrawable, 0);
            this.p = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castPauseButtonDrawable, 0);
            this.q = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castStopButtonDrawable, 0);
            this.r = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castSkipPreviousButtonDrawable, 0);
            this.s = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castSkipNextButtonDrawable, 0);
            this.t = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castRewind30ButtonDrawable, 0);
            this.u = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castForward30ButtonDrawable, 0);
            this.v = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castMuteToggleButtonDrawable, 0);
            this.w = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castClosedCaptionsButtonDrawable, 0);
            int resourceId = obtainStyledAttributes.getResourceId(R$styleable.CastMiniController_castControlButtons, 0);
            if (resourceId != 0) {
                TypedArray obtainTypedArray = context.getResources().obtainTypedArray(resourceId);
                Preconditions.a(obtainTypedArray.length() == 3);
                this.i = new int[obtainTypedArray.length()];
                for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
                    this.i[i2] = obtainTypedArray.getResourceId(i2, 0);
                }
                obtainTypedArray.recycle();
                if (this.f3837a) {
                    this.i[0] = R$id.cast_button_type_empty;
                }
                this.k = 0;
                for (int i3 : this.i) {
                    if (i3 != R$id.cast_button_type_empty) {
                        this.k++;
                    }
                }
            } else {
                y.w("Unable to read attribute castControlButtons.", new Object[0]);
                int i4 = R$id.cast_button_type_empty;
                this.i = new int[]{i4, i4, i4};
            }
            obtainStyledAttributes.recycle();
        }
    }
}
