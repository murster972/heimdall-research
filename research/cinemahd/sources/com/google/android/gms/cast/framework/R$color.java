package com.google.android.gms.cast.framework;

public final class R$color {
    public static final int cast_expanded_controller_ad_break_marker_color = 2131099724;
    public static final int cast_expanded_controller_ad_container_white_stripe_color = 2131099725;
    public static final int cast_expanded_controller_ad_in_progress_text_color = 2131099726;
    public static final int cast_expanded_controller_ad_label_background_color = 2131099727;
    public static final int cast_expanded_controller_ad_label_text_color = 2131099728;
    public static final int cast_expanded_controller_background_color = 2131099729;
    public static final int cast_expanded_controller_live_indicator_color = 2131099730;
    public static final int cast_expanded_controller_loading_indicator_color = 2131099731;
    public static final int cast_expanded_controller_progress_text_color = 2131099732;
    public static final int cast_expanded_controller_seek_bar_progress_background_tint_color = 2131099733;
    public static final int cast_expanded_controller_text_color = 2131099734;
    public static final int cast_intro_overlay_background_color = 2131099735;
    public static final int cast_intro_overlay_button_background_color = 2131099736;
    public static final int cast_libraries_material_featurehighlight_outer_highlight_default_color = 2131099737;
    public static final int cast_libraries_material_featurehighlight_text_body_color = 2131099738;
    public static final int cast_libraries_material_featurehighlight_text_header_color = 2131099739;
    public static final int cast_mini_controller_loading_indicator_color = 2131099740;
    public static final int cast_seekbar_progress_thumb_color = 2131099741;

    private R$color() {
    }
}
