package com.google.android.gms.cast;

import android.app.PendingIntent;
import android.os.RemoteException;
import android.text.TextUtils;
import com.facebook.ads.AdError;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzdo;

final class zzl extends zzdo {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f3870a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzl(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient);
        this.f3870a = str;
    }

    /* renamed from: zza */
    public final void doExecute(zzdd zzdd) throws RemoteException {
        if (TextUtils.isEmpty(this.f3870a)) {
            setResult(createFailedResult(new Status(AdError.INTERNAL_ERROR_CODE, "IllegalArgument: sessionId cannot be null or empty", (PendingIntent) null)));
            return;
        }
        try {
            zzdd.zza(this.f3870a, (BaseImplementation$ResultHolder<Status>) this);
        } catch (IllegalStateException unused) {
            zzp(AdError.INTERNAL_ERROR_CODE);
        }
    }
}
