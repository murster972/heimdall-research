package com.google.android.gms.cast.framework;

public final class R$layout {
    public static final int cast_expanded_controller_activity = 2131492938;
    public static final int cast_help_text = 2131492939;
    public static final int cast_intro_overlay = 2131492940;
    public static final int cast_mini_controller = 2131492942;
    public static final int cast_tracks_chooser_dialog_layout = 2131492943;
    public static final int cast_tracks_chooser_dialog_row_layout = 2131492944;

    private R$layout() {
    }
}
