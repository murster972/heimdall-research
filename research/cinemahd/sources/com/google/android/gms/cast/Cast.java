package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.cast.zzcv;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzdv;
import java.io.IOException;

public final class Cast {

    /* renamed from: a  reason: collision with root package name */
    private static final Api.AbstractClientBuilder<zzdd, CastOptions> f3762a = new zze();
    public static final Api<CastOptions> b = new Api<>("Cast.API", f3762a, zzdv.zzzf);
    public static final CastApi c = new CastApi.zza();

    public interface ApplicationConnectionResult extends Result {
        ApplicationMetadata getApplicationMetadata();

        String getApplicationStatus();

        String getSessionId();

        boolean getWasLaunched();
    }

    @Deprecated
    public interface CastApi {

        public static final class zza implements CastApi {
            public final PendingResult<ApplicationConnectionResult> a(GoogleApiClient googleApiClient, String str, LaunchOptions launchOptions) {
                return googleApiClient.a(new zzh(this, googleApiClient, str, launchOptions));
            }

            public final PendingResult<Status> b(GoogleApiClient googleApiClient, String str, String str2) {
                return googleApiClient.a(new zzf(this, googleApiClient, str, str2));
            }

            private final PendingResult<ApplicationConnectionResult> a(GoogleApiClient googleApiClient, String str, String str2, zzag zzag) {
                return googleApiClient.a(new zzi(this, googleApiClient, str, str2, (zzag) null));
            }

            public final PendingResult<Status> b(GoogleApiClient googleApiClient, String str) {
                return googleApiClient.a(new zzl(this, googleApiClient, str));
            }

            public final PendingResult<ApplicationConnectionResult> a(GoogleApiClient googleApiClient, String str, String str2) {
                return a(googleApiClient, str, str2, (zzag) null);
            }

            public final double b(GoogleApiClient googleApiClient) throws IllegalStateException {
                return ((zzdd) googleApiClient.a(zzdv.zzzf)).getVolume();
            }

            public final void a(GoogleApiClient googleApiClient, double d) throws IOException, IllegalArgumentException, IllegalStateException {
                try {
                    ((zzdd) googleApiClient.a(zzdv.zzzf)).setVolume(d);
                } catch (RemoteException unused) {
                    throw new IOException("service error");
                }
            }

            public final void a(GoogleApiClient googleApiClient, boolean z) throws IOException, IllegalStateException {
                try {
                    ((zzdd) googleApiClient.a(zzdv.zzzf)).setMute(z);
                } catch (RemoteException unused) {
                    throw new IOException("service error");
                }
            }

            public final boolean a(GoogleApiClient googleApiClient) throws IllegalStateException {
                return ((zzdd) googleApiClient.a(zzdv.zzzf)).isMute();
            }

            public final void a(GoogleApiClient googleApiClient, String str, MessageReceivedCallback messageReceivedCallback) throws IOException, IllegalStateException {
                try {
                    ((zzdd) googleApiClient.a(zzdv.zzzf)).setMessageReceivedCallbacks(str, messageReceivedCallback);
                } catch (RemoteException unused) {
                    throw new IOException("service error");
                }
            }

            public final void a(GoogleApiClient googleApiClient, String str) throws IOException, IllegalArgumentException {
                try {
                    ((zzdd) googleApiClient.a(zzdv.zzzf)).removeMessageReceivedCallbacks(str);
                } catch (RemoteException unused) {
                    throw new IOException("service error");
                }
            }
        }

        PendingResult<ApplicationConnectionResult> a(GoogleApiClient googleApiClient, String str, LaunchOptions launchOptions);

        PendingResult<ApplicationConnectionResult> a(GoogleApiClient googleApiClient, String str, String str2);

        void a(GoogleApiClient googleApiClient, double d) throws IOException, IllegalArgumentException, IllegalStateException;

        void a(GoogleApiClient googleApiClient, String str) throws IOException, IllegalArgumentException;

        void a(GoogleApiClient googleApiClient, String str, MessageReceivedCallback messageReceivedCallback) throws IOException, IllegalStateException;

        void a(GoogleApiClient googleApiClient, boolean z) throws IOException, IllegalStateException;

        boolean a(GoogleApiClient googleApiClient) throws IllegalStateException;

        double b(GoogleApiClient googleApiClient) throws IllegalStateException;

        PendingResult<Status> b(GoogleApiClient googleApiClient, String str);

        PendingResult<Status> b(GoogleApiClient googleApiClient, String str, String str2);
    }

    public static class Listener {
        public void onActiveInputStateChanged(int i) {
        }

        public void onApplicationDisconnected(int i) {
        }

        public void onApplicationMetadataChanged(ApplicationMetadata applicationMetadata) {
        }

        public void onApplicationStatusChanged() {
        }

        public void onStandbyStateChanged(int i) {
        }

        public void onVolumeChanged() {
        }
    }

    public interface MessageReceivedCallback {
        void onMessageReceived(CastDevice castDevice, String str, String str2);
    }

    static abstract class zza extends zzcv<ApplicationConnectionResult> {
        public zza(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        public /* synthetic */ Result createFailedResult(Status status) {
            return new zzm(this, status);
        }

        /* renamed from: zza */
        public void doExecute(zzdd zzdd) throws RemoteException {
        }
    }

    private Cast() {
    }

    public static final class CastOptions implements Api.ApiOptions.HasOptions {

        /* renamed from: a  reason: collision with root package name */
        final CastDevice f3763a;
        final Listener b;
        final Bundle c;
        /* access modifiers changed from: private */
        public final int d;

        public static final class Builder {

            /* renamed from: a  reason: collision with root package name */
            CastDevice f3764a;
            Listener b;
            /* access modifiers changed from: private */
            public int c = 0;
            /* access modifiers changed from: private */
            public Bundle d;

            public Builder(CastDevice castDevice, Listener listener) {
                Preconditions.a(castDevice, (Object) "CastDevice parameter cannot be null");
                Preconditions.a(listener, (Object) "CastListener parameter cannot be null");
                this.f3764a = castDevice;
                this.b = listener;
            }

            public final Builder a(Bundle bundle) {
                this.d = bundle;
                return this;
            }

            public final CastOptions a() {
                return new CastOptions(this, (zze) null);
            }
        }

        private CastOptions(Builder builder) {
            this.f3763a = builder.f3764a;
            this.b = builder.b;
            this.d = builder.c;
            this.c = builder.d;
        }

        /* synthetic */ CastOptions(Builder builder, zze zze) {
            this(builder);
        }
    }
}
