package com.google.android.gms.cast.framework;

import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.cast.zzdw;

public final class zze {
    private static final zzdw b = new zzdw("DiscoveryManager");

    /* renamed from: a  reason: collision with root package name */
    private final zzp f3859a;

    zze(zzp zzp) {
        this.f3859a = zzp;
    }

    public final IObjectWrapper a() {
        try {
            return this.f3859a.zzae();
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "getWrappedThis", zzp.class.getSimpleName());
            return null;
        }
    }
}
