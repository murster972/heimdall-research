package com.google.android.gms.cast.framework;

import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.internal.cast.zzb;

public interface zzr extends IInterface {

    public static abstract class zza extends zzb implements zzr {
        public static zzr a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.IReconnectionService");
            if (queryLocalInterface instanceof zzr) {
                return (zzr) queryLocalInterface;
            }
            return new zzs(iBinder);
        }
    }

    int a(Intent intent, int i, int i2) throws RemoteException;

    IBinder a(Intent intent) throws RemoteException;

    void d() throws RemoteException;

    void onDestroy() throws RemoteException;
}
