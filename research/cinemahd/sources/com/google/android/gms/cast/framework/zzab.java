package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzab extends IInterface {
    void a(boolean z) throws RemoteException;

    void b(Bundle bundle) throws RemoteException;

    void c(Bundle bundle) throws RemoteException;

    void e(Bundle bundle) throws RemoteException;

    void f(Bundle bundle) throws RemoteException;

    IObjectWrapper h() throws RemoteException;

    long q() throws RemoteException;

    int zzs() throws RemoteException;
}
