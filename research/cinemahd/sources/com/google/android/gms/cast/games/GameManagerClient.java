package com.google.android.gms.cast.games;

import com.google.android.gms.common.api.Result;
import org.json.JSONObject;

@Deprecated
public final class GameManagerClient {

    @Deprecated
    public interface GameManagerInstanceResult extends Result {
    }

    @Deprecated
    public interface GameManagerResult extends Result {
    }

    @Deprecated
    public interface Listener {
        void a(GameManagerState gameManagerState, GameManagerState gameManagerState2);

        void a(String str, JSONObject jSONObject);
    }
}
