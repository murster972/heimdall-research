package com.google.android.gms.cast.framework.media.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.R$attr;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.google.android.gms.cast.AdBreakClipInfo;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.R$drawable;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.cast.framework.R$layout;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.cast.framework.R$style;
import com.google.android.gms.cast.framework.R$styleable;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIController;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.cast.zzaa;
import com.google.android.gms.internal.cast.zzab;
import com.google.android.gms.internal.cast.zzaq;
import com.google.android.gms.internal.cast.zzar;
import com.google.android.gms.internal.cast.zzbt;
import com.google.android.gms.internal.cast.zzbw;
import com.google.android.gms.internal.cast.zzbx;
import com.google.android.gms.internal.cast.zzby;
import com.google.android.gms.internal.cast.zzca;
import java.util.Timer;

public class ExpandedControllerActivity extends AppCompatActivity implements ControlButtonsContainer {
    private View A;
    /* access modifiers changed from: private */
    public ImageView B;
    /* access modifiers changed from: private */
    public TextView C;
    private TextView D;
    /* access modifiers changed from: private */
    public TextView E;
    private TextView F;
    private zzaa G;
    private UIMediaController H;
    private SessionManager I;
    /* access modifiers changed from: private */
    public boolean J;
    private boolean K;
    private Timer L;

    /* renamed from: a  reason: collision with root package name */
    private final SessionManagerListener<CastSession> f3834a = new zzb(this, (zza) null);
    private final RemoteMediaClient.Listener b = new zza(this, (zza) null);
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    /* access modifiers changed from: private */
    public int n;
    private int o;
    private int p;
    private int q;
    private int r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public TextView t;
    private SeekBar u;
    private ImageView v;
    private ImageView w;
    private int[] x;
    private ImageView[] y = new ImageView[4];
    private View z;

    private class zza implements RemoteMediaClient.Listener {
        private zza() {
        }

        public final void onAdBreakStatusUpdated() {
            ExpandedControllerActivity.this.f();
        }

        public final void onMetadataUpdated() {
            ExpandedControllerActivity.this.d();
        }

        public final void onPreloadStatusUpdated() {
        }

        public final void onQueueStatusUpdated() {
        }

        public final void onSendingRemoteMediaRequest() {
            ExpandedControllerActivity.this.t.setText(ExpandedControllerActivity.this.getResources().getString(R$string.cast_expanded_controller_loading));
        }

        public final void onStatusUpdated() {
            RemoteMediaClient f = ExpandedControllerActivity.this.c();
            if (f != null && f.k()) {
                boolean unused = ExpandedControllerActivity.this.J = false;
                ExpandedControllerActivity.this.e();
                ExpandedControllerActivity.this.f();
            } else if (!ExpandedControllerActivity.this.J) {
                ExpandedControllerActivity.this.finish();
            }
        }

        /* synthetic */ zza(ExpandedControllerActivity expandedControllerActivity, zza zza) {
            this();
        }
    }

    private class zzb implements SessionManagerListener<CastSession> {
        private zzb() {
        }

        public final /* synthetic */ void onSessionEnded(Session session, int i) {
            ExpandedControllerActivity.this.finish();
        }

        public final /* bridge */ /* synthetic */ void onSessionEnding(Session session) {
        }

        public final /* bridge */ /* synthetic */ void onSessionResumeFailed(Session session, int i) {
        }

        public final /* bridge */ /* synthetic */ void onSessionResumed(Session session, boolean z) {
        }

        public final /* bridge */ /* synthetic */ void onSessionResuming(Session session, String str) {
        }

        public final /* bridge */ /* synthetic */ void onSessionStartFailed(Session session, int i) {
        }

        public final /* bridge */ /* synthetic */ void onSessionStarted(Session session, String str) {
        }

        public final /* bridge */ /* synthetic */ void onSessionStarting(Session session) {
        }

        public final /* bridge */ /* synthetic */ void onSessionSuspended(Session session, int i) {
        }

        /* synthetic */ zzb(ExpandedControllerActivity expandedControllerActivity, zza zza) {
            this();
        }
    }

    private final void a(View view, int i2, int i3, UIMediaController uIMediaController) {
        ImageView imageView = (ImageView) view.findViewById(i2);
        if (i3 == R$id.cast_button_type_empty) {
            imageView.setVisibility(4);
        } else if (i3 == R$id.cast_button_type_custom) {
        } else {
            if (i3 == R$id.cast_button_type_play_pause_toggle) {
                imageView.setBackgroundResource(this.c);
                Drawable b2 = zze.b(this, this.q, this.e);
                Drawable b3 = zze.b(this, this.q, this.d);
                Drawable b4 = zze.b(this, this.q, this.f);
                imageView.setImageDrawable(b3);
                uIMediaController.a(imageView, b3, b2, b4, (View) null, false);
            } else if (i3 == R$id.cast_button_type_skip_previous) {
                imageView.setBackgroundResource(this.c);
                imageView.setImageDrawable(zze.b(this, this.q, this.g));
                imageView.setContentDescription(getResources().getString(R$string.cast_skip_prev));
                uIMediaController.b((View) imageView, 0);
            } else if (i3 == R$id.cast_button_type_skip_next) {
                imageView.setBackgroundResource(this.c);
                imageView.setImageDrawable(zze.b(this, this.q, this.h));
                imageView.setContentDescription(getResources().getString(R$string.cast_skip_next));
                uIMediaController.a((View) imageView, 0);
            } else if (i3 == R$id.cast_button_type_rewind_30_seconds) {
                imageView.setBackgroundResource(this.c);
                imageView.setImageDrawable(zze.b(this, this.q, this.i));
                imageView.setContentDescription(getResources().getString(R$string.cast_rewind_30));
                uIMediaController.b((View) imageView, 30000);
            } else if (i3 == R$id.cast_button_type_forward_30_seconds) {
                imageView.setBackgroundResource(this.c);
                imageView.setImageDrawable(zze.b(this, this.q, this.j));
                imageView.setContentDescription(getResources().getString(R$string.cast_forward_30));
                uIMediaController.a((View) imageView, 30000);
            } else if (i3 == R$id.cast_button_type_mute_toggle) {
                imageView.setBackgroundResource(this.c);
                imageView.setImageDrawable(zze.b(this, this.q, this.k));
                uIMediaController.a(imageView);
            } else if (i3 == R$id.cast_button_type_closed_caption) {
                imageView.setBackgroundResource(this.c);
                imageView.setImageDrawable(zze.b(this, this.q, this.l));
                uIMediaController.a((View) imageView);
            }
        }
    }

    /* access modifiers changed from: private */
    public final RemoteMediaClient c() {
        CastSession a2 = this.I.a();
        if (a2 == null || !a2.b()) {
            return null;
        }
        return a2.h();
    }

    /* access modifiers changed from: private */
    public final void d() {
        MediaInfo e2;
        MediaMetadata y2;
        ActionBar supportActionBar;
        RemoteMediaClient c2 = c();
        if (c2 != null && c2.k() && (e2 = c2.e()) != null && (y2 = e2.y()) != null && (supportActionBar = getSupportActionBar()) != null) {
            supportActionBar.b((CharSequence) y2.f("com.google.android.gms.cast.metadata.TITLE"));
            supportActionBar.a((CharSequence) zzaq.zzb(y2));
        }
    }

    /* access modifiers changed from: private */
    public final void e() {
        CastDevice g2;
        CastSession a2 = this.I.a();
        if (!(a2 == null || (g2 = a2.g()) == null)) {
            String u2 = g2.u();
            if (!TextUtils.isEmpty(u2)) {
                this.t.setText(getResources().getString(R$string.cast_casting_to_device, new Object[]{u2}));
                return;
            }
        }
        this.t.setText("");
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    public final void f() {
        MediaStatus mediaStatus;
        String str;
        Drawable drawable;
        Bitmap bitmap;
        Bitmap a2;
        RemoteMediaClient c2 = c();
        String str2 = null;
        if (c2 == null) {
            mediaStatus = null;
        } else {
            mediaStatus = c2.f();
        }
        if (mediaStatus != null && mediaStatus.I()) {
            if (PlatformVersion.d() && this.w.getVisibility() == 8 && (drawable = this.v.getDrawable()) != null && (drawable instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) drawable).getBitmap()) != null && (a2 = zze.a(this, bitmap, 0.25f, 7.5f)) != null) {
                this.w.setImageBitmap(a2);
                this.w.setVisibility(0);
            }
            AdBreakClipInfo u2 = mediaStatus.u();
            if (u2 != null) {
                str = u2.A();
                str2 = u2.y();
            } else {
                str = null;
            }
            if (!TextUtils.isEmpty(str2)) {
                this.G.zza(Uri.parse(str2));
                this.A.setVisibility(8);
            } else {
                this.C.setVisibility(0);
                this.A.setVisibility(0);
                this.B.setVisibility(8);
            }
            TextView textView = this.D;
            if (TextUtils.isEmpty(str)) {
                str = getResources().getString(R$string.cast_ad_label);
            }
            textView.setText(str);
            if (PlatformVersion.i()) {
                this.D.setTextAppearance(this.r);
            } else {
                this.D.setTextAppearance(this, this.r);
            }
            this.z.setVisibility(0);
            a(u2, c2);
            return;
        }
        this.F.setVisibility(8);
        this.E.setVisibility(8);
        this.z.setVisibility(8);
        if (PlatformVersion.d()) {
            this.w.setVisibility(8);
            this.w.setImageBitmap((Bitmap) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.I = CastContext.a((Context) this).c();
        if (this.I.a() == null) {
            finish();
        }
        this.H = new UIMediaController(this);
        this.H.a(this.b);
        setContentView(R$layout.cast_expanded_controller_activity);
        TypedArray obtainStyledAttributes = obtainStyledAttributes(new int[]{R$attr.selectableItemBackgroundBorderless});
        this.c = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = obtainStyledAttributes((AttributeSet) null, R$styleable.CastExpandedController, com.google.android.gms.cast.framework.R$attr.castExpandedControllerStyle, R$style.CastExpandedController);
        this.q = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castButtonColor, 0);
        this.d = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castPlayButtonDrawable, 0);
        this.e = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castPauseButtonDrawable, 0);
        this.f = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castStopButtonDrawable, 0);
        this.g = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castSkipPreviousButtonDrawable, 0);
        this.h = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castSkipNextButtonDrawable, 0);
        this.i = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castRewind30ButtonDrawable, 0);
        this.j = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castForward30ButtonDrawable, 0);
        this.k = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castMuteToggleButtonDrawable, 0);
        this.l = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castClosedCaptionsButtonDrawable, 0);
        int resourceId = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castControlButtons, 0);
        if (resourceId != 0) {
            TypedArray obtainTypedArray = getResources().obtainTypedArray(resourceId);
            Preconditions.a(obtainTypedArray.length() == 4);
            this.x = new int[obtainTypedArray.length()];
            for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
                this.x[i2] = obtainTypedArray.getResourceId(i2, 0);
            }
            obtainTypedArray.recycle();
        } else {
            int i3 = R$id.cast_button_type_empty;
            this.x = new int[]{i3, i3, i3, i3};
        }
        this.p = obtainStyledAttributes2.getColor(R$styleable.CastExpandedController_castExpandedControllerLoadingIndicatorColor, 0);
        this.m = getResources().getColor(obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castAdLabelColor, 0));
        this.n = getResources().getColor(obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castAdInProgressTextColor, 0));
        this.o = getResources().getColor(obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castAdLabelTextColor, 0));
        this.r = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castAdLabelTextAppearance, 0);
        this.s = obtainStyledAttributes2.getResourceId(R$styleable.CastExpandedController_castAdInProgressLabelTextAppearance, 0);
        obtainStyledAttributes2.recycle();
        View findViewById = findViewById(R$id.expanded_controller_layout);
        UIMediaController uIMediaController = this.H;
        this.v = (ImageView) findViewById.findViewById(R$id.background_image_view);
        this.w = (ImageView) findViewById.findViewById(R$id.blurred_background_image_view);
        View findViewById2 = findViewById.findViewById(R$id.background_place_holder_image_view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        uIMediaController.a(this.v, new ImageHints(4, displayMetrics.widthPixels, displayMetrics.heightPixels), findViewById2);
        this.t = (TextView) findViewById.findViewById(R$id.status_text);
        ProgressBar progressBar = (ProgressBar) findViewById.findViewById(R$id.loading_indicator);
        Drawable indeterminateDrawable = progressBar.getIndeterminateDrawable();
        int i4 = this.p;
        if (i4 != 0) {
            indeterminateDrawable.setColorFilter(i4, PorterDuff.Mode.SRC_IN);
        }
        uIMediaController.c((View) progressBar);
        TextView textView = (TextView) findViewById.findViewById(R$id.start_text);
        TextView textView2 = (TextView) findViewById.findViewById(R$id.end_text);
        this.u = (SeekBar) findViewById.findViewById(R$id.seek_bar);
        new zzar(this, uIMediaController, this.u);
        uIMediaController.a((View) textView, (UIController) new zzby(textView, uIMediaController.d()));
        uIMediaController.a((View) textView2, (UIController) new zzbw(textView2, uIMediaController.d()));
        View findViewById3 = findViewById.findViewById(R$id.live_indicators);
        UIMediaController uIMediaController2 = this.H;
        uIMediaController2.a(findViewById3, (UIController) new zzbx(findViewById3, uIMediaController2.d()));
        RelativeLayout relativeLayout = (RelativeLayout) findViewById.findViewById(R$id.tooltip_container);
        zzca zzca = new zzca(relativeLayout, this.u, this.H.d());
        this.H.a((View) relativeLayout, (UIController) zzca);
        this.H.a((zzbt) zzca);
        this.y[0] = (ImageView) findViewById.findViewById(R$id.button_0);
        this.y[1] = (ImageView) findViewById.findViewById(R$id.button_1);
        this.y[2] = (ImageView) findViewById.findViewById(R$id.button_2);
        this.y[3] = (ImageView) findViewById.findViewById(R$id.button_3);
        a(findViewById, R$id.button_0, this.x[0], uIMediaController);
        a(findViewById, R$id.button_1, this.x[1], uIMediaController);
        a(findViewById, R$id.button_play_pause_toggle, R$id.cast_button_type_play_pause_toggle, uIMediaController);
        a(findViewById, R$id.button_2, this.x[2], uIMediaController);
        a(findViewById, R$id.button_3, this.x[3], uIMediaController);
        this.z = findViewById(R$id.ad_container);
        this.B = (ImageView) this.z.findViewById(R$id.ad_image_view);
        this.A = this.z.findViewById(R$id.ad_background_image_view);
        this.D = (TextView) this.z.findViewById(R$id.ad_label);
        this.D.setTextColor(this.o);
        this.D.setBackgroundColor(this.m);
        this.C = (TextView) this.z.findViewById(R$id.ad_in_progress_label);
        this.F = (TextView) findViewById(R$id.ad_skip_text);
        this.E = (TextView) findViewById(R$id.ad_skip_button);
        this.E.setOnClickListener(new zzb(this));
        setSupportActionBar((Toolbar) findViewById(R$id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().d(true);
            getSupportActionBar().b(R$drawable.quantum_ic_keyboard_arrow_down_white_36);
        }
        e();
        d();
        this.G = new zzaa(getApplicationContext(), new ImageHints(-1, this.B.getWidth(), this.B.getHeight()));
        this.G.zza((zzab) new zza(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.G.clear();
        UIMediaController uIMediaController = this.H;
        if (uIMediaController != null) {
            uIMediaController.a((RemoteMediaClient.Listener) null);
            this.H.a();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return true;
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        CastContext.a((Context) this).c().b(this.f3834a, CastSession.class);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        CastContext.a((Context) this).c().a(this.f3834a, CastSession.class);
        CastSession a2 = CastContext.a((Context) this).c().a();
        if (a2 == null || (!a2.b() && !a2.c())) {
            finish();
        }
        RemoteMediaClient c2 = c();
        this.J = c2 == null || !c2.k();
        e();
        f();
        super.onResume();
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            int systemUiVisibility = getWindow().getDecorView().getSystemUiVisibility() ^ 2;
            if (PlatformVersion.c()) {
                systemUiVisibility ^= 4;
            }
            if (PlatformVersion.f()) {
                systemUiVisibility ^= 4096;
            }
            getWindow().getDecorView().setSystemUiVisibility(systemUiVisibility);
            if (PlatformVersion.e()) {
                setImmersive(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public final void a(AdBreakClipInfo adBreakClipInfo, RemoteMediaClient remoteMediaClient) {
        if (!this.J && !remoteMediaClient.l()) {
            this.E.setVisibility(8);
            if (adBreakClipInfo != null && adBreakClipInfo.C() != -1) {
                if (!this.K) {
                    zzc zzc = new zzc(this, adBreakClipInfo, remoteMediaClient);
                    this.L = new Timer();
                    this.L.scheduleAtFixedRate(zzc, 0, 500);
                    this.K = true;
                }
                float C2 = (float) (adBreakClipInfo.C() - remoteMediaClient.a());
                if (C2 <= 0.0f) {
                    this.F.setVisibility(8);
                    if (this.K) {
                        this.L.cancel();
                        this.K = false;
                    }
                    this.E.setVisibility(0);
                    this.E.setClickable(true);
                    return;
                }
                this.F.setVisibility(0);
                this.F.setText(getResources().getString(R$string.cast_expanded_controller_skip_ad_text, new Object[]{Integer.valueOf((int) Math.ceil((double) (C2 / 1000.0f)))}));
                this.E.setClickable(false);
            }
        }
    }
}
