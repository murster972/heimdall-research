package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.GestureDetectorCompat;
import com.facebook.imageutils.JfifUtil;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.internal.cast.zzfc;
import com.google.android.gms.internal.cast.zzfl;
import com.google.android.gms.internal.cast.zzfo;

public final class zza extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f3798a = new int[2];
    private final Rect b = new Rect();
    private final Rect c = new Rect();
    /* access modifiers changed from: private */
    public final OuterHighlightDrawable d;
    private final InnerZoneDrawable e;
    private zzi f;
    private View g;
    /* access modifiers changed from: private */
    public Animator h;
    private final zzj i;
    private final GestureDetectorCompat j;
    private GestureDetectorCompat k;
    /* access modifiers changed from: private */
    public zzh l;
    private boolean m;

    public zza(Context context) {
        super(context);
        setId(R$id.cast_featurehighlight_view);
        setWillNotDraw(false);
        this.e = new InnerZoneDrawable(context);
        this.e.setCallback(this);
        this.d = new OuterHighlightDrawable(context);
        this.d.setCallback(this);
        this.i = new zzj(this);
        this.j = new GestureDetectorCompat(context, new zzb(this));
        this.j.a(false);
        setVisibility(8);
    }

    /* access modifiers changed from: private */
    public final Animator e() {
        InnerZoneDrawable innerZoneDrawable = this.e;
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator duration = ObjectAnimator.ofFloat(innerZoneDrawable, "scale", new float[]{1.0f, 1.1f}).setDuration(500);
        ObjectAnimator duration2 = ObjectAnimator.ofFloat(innerZoneDrawable, "scale", new float[]{1.1f, 1.0f}).setDuration(500);
        ObjectAnimator duration3 = ObjectAnimator.ofPropertyValuesHolder(innerZoneDrawable, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("pulseScale", new float[]{1.1f, 2.0f}), PropertyValuesHolder.ofFloat("pulseAlpha", new float[]{1.0f, 0.0f})}).setDuration(500);
        animatorSet.play(duration);
        animatorSet.play(duration2).with(duration3).after(duration);
        animatorSet.setInterpolator(zzfl.zzfm());
        animatorSet.setStartDelay(500);
        zzfc.zza(animatorSet, -1, (Runnable) null);
        return animatorSet;
    }

    public final void a(zzi zzi) {
        this.f = (zzi) zzfo.checkNotNull(zzi);
        addView(zzi.asView(), 0);
    }

    public final void b(Runnable runnable) {
        ObjectAnimator duration = ObjectAnimator.ofFloat(this.f.asView(), "alpha", new float[]{0.0f}).setDuration(200);
        duration.setInterpolator(zzfl.zzfl());
        float exactCenterX = this.b.exactCenterX() - this.d.a();
        float exactCenterY = this.b.exactCenterY() - this.d.b();
        OuterHighlightDrawable outerHighlightDrawable = this.d;
        PropertyValuesHolder ofFloat = PropertyValuesHolder.ofFloat("scale", new float[]{0.0f});
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt("alpha", new int[]{0});
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(outerHighlightDrawable, new PropertyValuesHolder[]{ofFloat, PropertyValuesHolder.ofFloat("translationX", new float[]{0.0f, exactCenterX}), PropertyValuesHolder.ofFloat("translationY", new float[]{0.0f, exactCenterY}), ofInt});
        ofPropertyValuesHolder.setInterpolator(zzfl.zzfl());
        Animator duration2 = ofPropertyValuesHolder.setDuration(200);
        Animator a2 = this.e.a();
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(new Animator[]{duration, duration2, a2});
        animatorSet.addListener(new zzg(this, runnable));
        a((Animator) animatorSet);
    }

    public final void c(Runnable runnable) {
        ObjectAnimator duration = ObjectAnimator.ofFloat(this.f.asView(), "alpha", new float[]{0.0f}).setDuration(200);
        duration.setInterpolator(zzfl.zzfl());
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.d, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{1.125f}), PropertyValuesHolder.ofInt("alpha", new int[]{0})});
        ofPropertyValuesHolder.setInterpolator(zzfl.zzfl());
        Animator duration2 = ofPropertyValuesHolder.setDuration(200);
        Animator a2 = this.e.a();
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(new Animator[]{duration, duration2, a2});
        animatorSet.addListener(new zzf(this, runnable));
        a((Animator) animatorSet);
    }

    /* access modifiers changed from: protected */
    public final boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams;
    }

    /* access modifiers changed from: package-private */
    public final InnerZoneDrawable d() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-2, -2);
    }

    public final ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        canvas.save();
        this.d.draw(canvas);
        this.e.draw(canvas);
        View view = this.g;
        if (view != null) {
            if (view.getParent() != null) {
                Bitmap createBitmap = Bitmap.createBitmap(this.g.getWidth(), this.g.getHeight(), Bitmap.Config.ARGB_8888);
                this.g.draw(new Canvas(createBitmap));
                int c2 = this.d.c();
                int red = Color.red(c2);
                int green = Color.green(c2);
                int blue = Color.blue(c2);
                for (int i2 = 0; i2 < createBitmap.getHeight(); i2++) {
                    for (int i3 = 0; i3 < createBitmap.getWidth(); i3++) {
                        int pixel = createBitmap.getPixel(i3, i2);
                        if (Color.alpha(pixel) != 0) {
                            createBitmap.setPixel(i3, i2, Color.argb(Color.alpha(pixel), red, green, blue));
                        }
                    }
                }
                Rect rect = this.b;
                canvas.drawBitmap(createBitmap, (float) rect.left, (float) rect.top, (Paint) null);
            }
            canvas.restore();
            return;
        }
        throw new IllegalStateException("Neither target view nor drawable was set");
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        View view = this.g;
        if (view != null) {
            if (view.getParent() != null) {
                int[] iArr = this.f3798a;
                View view2 = this.g;
                getLocationInWindow(iArr);
                int i6 = iArr[0];
                int i7 = iArr[1];
                view2.getLocationInWindow(iArr);
                iArr[0] = iArr[0] - i6;
                iArr[1] = iArr[1] - i7;
            }
            Rect rect = this.b;
            int[] iArr2 = this.f3798a;
            rect.set(iArr2[0], iArr2[1], iArr2[0] + this.g.getWidth(), this.f3798a[1] + this.g.getHeight());
            this.c.set(i2, i3, i4, i5);
            this.d.setBounds(this.c);
            this.e.setBounds(this.c);
            this.i.a(this.b, this.c);
            return;
        }
        throw new IllegalStateException("Target view must be set before layout");
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        setMeasuredDimension(ViewGroup.resolveSize(View.MeasureSpec.getSize(i2), i2), ViewGroup.resolveSize(View.MeasureSpec.getSize(i3), i3));
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.m = this.b.contains((int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (this.m) {
            GestureDetectorCompat gestureDetectorCompat = this.k;
            if (gestureDetectorCompat != null) {
                gestureDetectorCompat.a(motionEvent);
                if (actionMasked == 1) {
                    motionEvent = MotionEvent.obtain(motionEvent);
                    motionEvent.setAction(3);
                }
            }
            if (this.g.getParent() != null) {
                this.g.onTouchEvent(motionEvent);
            }
        } else {
            this.j.a(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.d || drawable == this.e || drawable == null;
    }

    /* access modifiers changed from: protected */
    public final ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new ViewGroup.MarginLayoutParams(layoutParams);
    }

    public final void a(View view, View view2, boolean z, zzh zzh) {
        this.g = (View) zzfo.checkNotNull(view);
        this.l = (zzh) zzfo.checkNotNull(zzh);
        this.k = new GestureDetectorCompat(getContext(), new zzc(this, view, true, zzh));
        this.k.a(false);
        setVisibility(4);
    }

    public final void a(Runnable runnable) {
        addOnLayoutChangeListener(new zzd(this, (Runnable) null));
    }

    public final void a() {
        if (this.g != null) {
            setVisibility(0);
            ObjectAnimator duration = ObjectAnimator.ofFloat(this.f.asView(), "alpha", new float[]{0.0f, 1.0f}).setDuration(350);
            duration.setInterpolator(zzfl.zzfk());
            Animator b2 = this.d.b(this.b.exactCenterX() - this.d.a(), this.b.exactCenterY() - this.d.b());
            ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.e, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{0.0f, 1.0f}), PropertyValuesHolder.ofInt("alpha", new int[]{0, JfifUtil.MARKER_FIRST_BYTE})});
            ofPropertyValuesHolder.setInterpolator(zzfl.zzfk());
            Animator duration2 = ofPropertyValuesHolder.setDuration(350);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(new Animator[]{duration, b2, duration2});
            animatorSet.addListener(new zze(this));
            a((Animator) animatorSet);
            return;
        }
        throw new IllegalStateException("Target view must be set before animation");
    }

    /* access modifiers changed from: package-private */
    public final OuterHighlightDrawable c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final View b() {
        return this.f.asView();
    }

    public final void a(int i2) {
        this.d.a(i2);
    }

    private final void a(Animator animator) {
        Animator animator2 = this.h;
        if (animator2 != null) {
            animator2.cancel();
        }
        this.h = animator;
        this.h.start();
    }

    /* access modifiers changed from: private */
    public final boolean a(float f2, float f3) {
        return this.c.contains(Math.round(f2), Math.round(f3));
    }
}
