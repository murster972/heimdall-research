package com.google.android.gms.cast.framework.media;

import android.graphics.Bitmap;
import com.google.android.gms.cast.framework.media.MediaNotificationService;
import com.google.android.gms.internal.cast.zzab;

final class zzj implements zzab {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaNotificationService.zzb f3854a;
    private final /* synthetic */ MediaNotificationService b;

    zzj(MediaNotificationService mediaNotificationService, MediaNotificationService.zzb zzb) {
        this.b = mediaNotificationService;
        this.f3854a = zzb;
    }

    public final void zza(Bitmap bitmap) {
        MediaNotificationService.zzb zzb = this.f3854a;
        zzb.b = bitmap;
        MediaNotificationService.zzb unused = this.b.n = zzb;
        this.b.a();
    }
}
