package com.google.android.gms.cast;

import android.os.RemoteException;
import com.facebook.ads.AdError;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.cast.zzdd;

final class zzi extends Cast.zza {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f3869a;
    private final /* synthetic */ String b;
    private final /* synthetic */ zzag c = null;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzi(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str, String str2, zzag zzag) {
        super(googleApiClient);
        this.f3869a = str;
        this.b = str2;
    }

    /* renamed from: zza */
    public final void doExecute(zzdd zzdd) throws RemoteException {
        try {
            zzdd.zza(this.f3869a, this.b, this.c, this);
        } catch (IllegalStateException unused) {
            zzp(AdError.INTERNAL_ERROR_CODE);
        }
    }
}
