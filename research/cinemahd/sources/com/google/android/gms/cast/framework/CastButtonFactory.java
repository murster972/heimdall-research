package com.google.android.gms.cast.framework;

import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;
import androidx.core.view.MenuItemCompat;
import androidx.mediarouter.app.MediaRouteActionProvider;
import androidx.mediarouter.app.MediaRouteDialogFactory;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.cast.zzdw;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class CastButtonFactory {

    /* renamed from: a  reason: collision with root package name */
    private static final List<WeakReference<MenuItem>> f3782a = new ArrayList();

    static {
        new zzdw("CastButtonFactory");
        new ArrayList();
    }

    private CastButtonFactory() {
    }

    public static MenuItem a(Context context, Menu menu, int i) {
        return a(context, menu, i, (MediaRouteDialogFactory) null);
    }

    private static MenuItem a(Context context, Menu menu, int i, MediaRouteDialogFactory mediaRouteDialogFactory) {
        Preconditions.a("Must be called from the main thread.");
        Preconditions.a(menu);
        MenuItem findItem = menu.findItem(i);
        if (findItem != null) {
            try {
                a(context, findItem, (MediaRouteDialogFactory) null);
                f3782a.add(new WeakReference(findItem));
                return findItem;
            } catch (IllegalArgumentException unused) {
                throw new IllegalArgumentException(String.format(Locale.ROOT, "menu item with ID %d doesn't have a MediaRouteActionProvider.", new Object[]{Integer.valueOf(i)}));
            }
        } else {
            throw new IllegalArgumentException(String.format(Locale.ROOT, "menu doesn't contain a menu item whose ID is %d.", new Object[]{Integer.valueOf(i)}));
        }
    }

    private static void a(Context context, MenuItem menuItem, MediaRouteDialogFactory mediaRouteDialogFactory) throws IllegalArgumentException {
        Preconditions.a("Must be called from the main thread.");
        MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) MenuItemCompat.a(menuItem);
        if (mediaRouteActionProvider != null) {
            CastContext b = CastContext.b(context);
            if (b != null) {
                mediaRouteActionProvider.setRouteSelector(b.b());
                return;
            }
            return;
        }
        throw new IllegalArgumentException();
    }
}
