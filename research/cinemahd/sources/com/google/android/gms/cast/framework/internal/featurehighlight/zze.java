package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class zze extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zza f3802a;

    zze(zza zza) {
        this.f3802a = zza;
    }

    public final void onAnimationEnd(Animator animator) {
        zza zza = this.f3802a;
        Animator unused = zza.h = zza.e();
        this.f3802a.h.start();
    }
}
