package com.google.android.gms.cast.framework.media;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.cast.zzc;

public interface zzb extends IInterface {

    public static abstract class zza extends com.google.android.gms.internal.cast.zzb implements zzb {
        public zza() {
            super("com.google.android.gms.cast.framework.media.IImagePicker");
        }

        /* access modifiers changed from: protected */
        public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                WebImage a2 = a((MediaMetadata) zzc.zza(parcel, MediaMetadata.CREATOR), parcel.readInt());
                parcel2.writeNoException();
                zzc.zzb(parcel2, a2);
            } else if (i == 2) {
                IObjectWrapper k = k();
                parcel2.writeNoException();
                zzc.zza(parcel2, (IInterface) k);
            } else if (i == 3) {
                int zzs = zzs();
                parcel2.writeNoException();
                parcel2.writeInt(zzs);
            } else if (i != 4) {
                return false;
            } else {
                WebImage a3 = a((MediaMetadata) zzc.zza(parcel, MediaMetadata.CREATOR), (ImageHints) zzc.zza(parcel, ImageHints.CREATOR));
                parcel2.writeNoException();
                zzc.zzb(parcel2, a3);
            }
            return true;
        }
    }

    WebImage a(MediaMetadata mediaMetadata, int i) throws RemoteException;

    WebImage a(MediaMetadata mediaMetadata, ImageHints imageHints) throws RemoteException;

    IObjectWrapper k() throws RemoteException;

    int zzs() throws RemoteException;
}
