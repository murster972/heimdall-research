package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.cast.zzal;
import com.google.android.gms.internal.cast.zzdw;
import com.google.android.gms.internal.cast.zzdx;
import com.google.android.gms.internal.cast.zze;
import com.google.android.gms.internal.cast.zzg;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class CastSession extends Session {
    /* access modifiers changed from: private */
    public static final zzdw n = new zzdw("CastSession");
    private final Context d;
    /* access modifiers changed from: private */
    public final Set<Cast.Listener> e = new HashSet();
    /* access modifiers changed from: private */
    public final zzl f;
    private final CastOptions g;
    /* access modifiers changed from: private */
    public final Cast.CastApi h;
    /* access modifiers changed from: private */
    public final zzal i;
    /* access modifiers changed from: private */
    public GoogleApiClient j;
    /* access modifiers changed from: private */
    public RemoteMediaClient k;
    private CastDevice l;
    /* access modifiers changed from: private */
    public Cast.ApplicationConnectionResult m;

    private class zza implements ResultCallback<Cast.ApplicationConnectionResult> {

        /* renamed from: a  reason: collision with root package name */
        private String f3786a;

        zza(String str) {
            this.f3786a = str;
        }

        public final /* synthetic */ void onResult(Result result) {
            Cast.ApplicationConnectionResult applicationConnectionResult = (Cast.ApplicationConnectionResult) result;
            Cast.ApplicationConnectionResult unused = CastSession.this.m = applicationConnectionResult;
            try {
                if (applicationConnectionResult.getStatus().v()) {
                    CastSession.n.d("%s() -> success result", this.f3786a);
                    RemoteMediaClient unused2 = CastSession.this.k = new RemoteMediaClient(new zzdx((String) null), CastSession.this.h);
                    try {
                        CastSession.this.k.a(CastSession.this.j);
                        CastSession.this.k.w();
                        CastSession.this.k.t();
                        CastSession.this.i.zza(CastSession.this.k, CastSession.this.g());
                    } catch (IOException e) {
                        CastSession.n.zzc(e, "Exception when setting GoogleApiClient.", new Object[0]);
                        RemoteMediaClient unused3 = CastSession.this.k = null;
                    }
                    CastSession.this.f.zza(applicationConnectionResult.getApplicationMetadata(), applicationConnectionResult.getApplicationStatus(), applicationConnectionResult.getSessionId(), applicationConnectionResult.getWasLaunched());
                    return;
                }
                CastSession.n.d("%s() -> failure result", this.f3786a);
                CastSession.this.f.zzi(applicationConnectionResult.getStatus().s());
            } catch (RemoteException e2) {
                CastSession.n.zza(e2, "Unable to call %s on %s.", "methods", zzl.class.getSimpleName());
            }
        }
    }

    private class zzc extends Cast.Listener {
        private zzc() {
        }

        public final void onActiveInputStateChanged(int i) {
            for (Cast.Listener onActiveInputStateChanged : new HashSet(CastSession.this.e)) {
                onActiveInputStateChanged.onActiveInputStateChanged(i);
            }
        }

        public final void onApplicationDisconnected(int i) {
            CastSession.this.d(i);
            CastSession.this.c(i);
            for (Cast.Listener onApplicationDisconnected : new HashSet(CastSession.this.e)) {
                onApplicationDisconnected.onApplicationDisconnected(i);
            }
        }

        public final void onApplicationMetadataChanged(ApplicationMetadata applicationMetadata) {
            for (Cast.Listener onApplicationMetadataChanged : new HashSet(CastSession.this.e)) {
                onApplicationMetadataChanged.onApplicationMetadataChanged(applicationMetadata);
            }
        }

        public final void onApplicationStatusChanged() {
            for (Cast.Listener onApplicationStatusChanged : new HashSet(CastSession.this.e)) {
                onApplicationStatusChanged.onApplicationStatusChanged();
            }
        }

        public final void onStandbyStateChanged(int i) {
            for (Cast.Listener onStandbyStateChanged : new HashSet(CastSession.this.e)) {
                onStandbyStateChanged.onStandbyStateChanged(i);
            }
        }

        public final void onVolumeChanged() {
            for (Cast.Listener onVolumeChanged : new HashSet(CastSession.this.e)) {
                onVolumeChanged.onVolumeChanged();
            }
        }
    }

    private class zzd implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private zzd() {
        }

        public final void onConnected(Bundle bundle) {
            try {
                if (CastSession.this.k != null) {
                    try {
                        CastSession.this.k.w();
                        CastSession.this.k.t();
                    } catch (IOException e) {
                        CastSession.n.zzc(e, "Exception when setting GoogleApiClient.", new Object[0]);
                        RemoteMediaClient unused = CastSession.this.k = null;
                    }
                }
                CastSession.this.f.onConnected(bundle);
            } catch (RemoteException e2) {
                CastSession.n.zza(e2, "Unable to call %s on %s.", "onConnected", zzl.class.getSimpleName());
            }
        }

        public final void onConnectionFailed(ConnectionResult connectionResult) {
            try {
                CastSession.this.f.onConnectionFailed(connectionResult);
            } catch (RemoteException e) {
                CastSession.n.zza(e, "Unable to call %s on %s.", "onConnectionFailed", zzl.class.getSimpleName());
            }
        }

        public final void onConnectionSuspended(int i) {
            try {
                CastSession.this.f.onConnectionSuspended(i);
            } catch (RemoteException e) {
                CastSession.n.zza(e, "Unable to call %s on %s.", "onConnectionSuspended", zzl.class.getSimpleName());
            }
        }
    }

    public CastSession(Context context, String str, String str2, CastOptions castOptions, Cast.CastApi castApi, zzg zzg, zzal zzal) {
        super(context, str, str2);
        this.d = context.getApplicationContext();
        this.g = castOptions;
        this.h = castApi;
        this.i = zzal;
        this.f = zze.zza(context, castOptions, f(), (zzh) new zzb());
    }

    private final void e(Bundle bundle) {
        this.l = CastDevice.b(bundle);
        if (this.l != null) {
            GoogleApiClient googleApiClient = this.j;
            if (googleApiClient != null) {
                googleApiClient.b();
                this.j = null;
            }
            boolean z = true;
            n.d("Acquiring a connection to Google Play Services for %s", this.l);
            zzd zzd2 = new zzd();
            Context context = this.d;
            CastDevice castDevice = this.l;
            CastOptions castOptions = this.g;
            zzc zzc2 = new zzc();
            Bundle bundle2 = new Bundle();
            bundle2.putBoolean("com.google.android.gms.cast.EXTRA_CAST_FRAMEWORK_NOTIFICATION_ENABLED", (castOptions == null || castOptions.s() == null || castOptions.s().v() == null) ? false : true);
            if (castOptions == null || castOptions.s() == null || !castOptions.s().w()) {
                z = false;
            }
            bundle2.putBoolean("com.google.android.gms.cast.EXTRA_CAST_REMOTE_CONTROL_NOTIFICATION_ENABLED", z);
            GoogleApiClient.Builder builder = new GoogleApiClient.Builder(context);
            Api<Cast.CastOptions> api = Cast.b;
            Cast.CastOptions.Builder builder2 = new Cast.CastOptions.Builder(castDevice, zzc2);
            builder2.a(bundle2);
            builder.a(api, builder2.a());
            builder.a((GoogleApiClient.ConnectionCallbacks) zzd2);
            builder.a((GoogleApiClient.OnConnectionFailedListener) zzd2);
            this.j = builder.a();
            this.j.a();
        } else if (d()) {
            a(8);
        } else {
            b(8);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
        this.l = CastDevice.b(bundle);
    }

    /* access modifiers changed from: protected */
    public void b(Bundle bundle) {
        this.l = CastDevice.b(bundle);
    }

    /* access modifiers changed from: protected */
    public void c(Bundle bundle) {
        e(bundle);
    }

    /* access modifiers changed from: protected */
    public void d(Bundle bundle) {
        e(bundle);
    }

    public CastDevice g() {
        Preconditions.a("Must be called from the main thread.");
        return this.l;
    }

    public RemoteMediaClient h() {
        Preconditions.a("Must be called from the main thread.");
        return this.k;
    }

    public double i() throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        GoogleApiClient googleApiClient = this.j;
        if (googleApiClient != null) {
            return this.h.b(googleApiClient);
        }
        return 0.0d;
    }

    public boolean j() throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        GoogleApiClient googleApiClient = this.j;
        if (googleApiClient != null) {
            return this.h.a(googleApiClient);
        }
        return false;
    }

    /* access modifiers changed from: private */
    public final void d(int i2) {
        this.i.zzn(i2);
        GoogleApiClient googleApiClient = this.j;
        if (googleApiClient != null) {
            googleApiClient.b();
            this.j = null;
        }
        this.l = null;
        RemoteMediaClient remoteMediaClient = this.k;
        if (remoteMediaClient != null) {
            remoteMediaClient.a((GoogleApiClient) null);
            this.k = null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        try {
            this.f.zza(z, 0);
        } catch (RemoteException e2) {
            n.zza(e2, "Unable to call %s on %s.", "disconnectFromDevice", zzl.class.getSimpleName());
        }
        c(0);
    }

    public void b(boolean z) throws IOException, IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        GoogleApiClient googleApiClient = this.j;
        if (googleApiClient != null) {
            this.h.a(googleApiClient, z);
        }
    }

    private class zzb extends zzi {
        private zzb() {
        }

        public final void a(String str, String str2) {
            if (CastSession.this.j != null) {
                CastSession.this.h.a(CastSession.this.j, str, str2).setResultCallback(new zza("joinApplication"));
            }
        }

        public final void d(int i) {
            CastSession.this.d(i);
        }

        public final void zzj(String str) {
            if (CastSession.this.j != null) {
                CastSession.this.h.b(CastSession.this.j, str);
            }
        }

        public final int zzs() {
            return 12451009;
        }

        public final void a(String str, LaunchOptions launchOptions) {
            if (CastSession.this.j != null) {
                CastSession.this.h.a(CastSession.this.j, str, launchOptions).setResultCallback(new zza("launchApplication"));
            }
        }
    }

    public void b(Cast.Listener listener) {
        Preconditions.a("Must be called from the main thread.");
        if (listener != null) {
            this.e.remove(listener);
        }
    }

    public void a(double d2) throws IOException {
        Preconditions.a("Must be called from the main thread.");
        GoogleApiClient googleApiClient = this.j;
        if (googleApiClient != null) {
            this.h.a(googleApiClient, d2);
        }
    }

    public void a(Cast.Listener listener) {
        Preconditions.a("Must be called from the main thread.");
        if (listener != null) {
            this.e.add(listener);
        }
    }

    public long a() {
        Preconditions.a("Must be called from the main thread.");
        RemoteMediaClient remoteMediaClient = this.k;
        if (remoteMediaClient == null) {
            return 0;
        }
        return remoteMediaClient.j() - this.k.b();
    }
}
