package com.google.android.gms.cast.framework.media;

import android.content.DialogInterface;

final class zzbe implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzbf f3849a;
    private final /* synthetic */ zzbf b;
    private final /* synthetic */ TracksChooserDialogFragment c;

    zzbe(TracksChooserDialogFragment tracksChooserDialogFragment, zzbf zzbf, zzbf zzbf2) {
        this.c = tracksChooserDialogFragment;
        this.f3849a = zzbf;
        this.b = zzbf2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.c.a(this.f3849a, this.b);
    }
}
