package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.internal.cast.zzdk;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaInfo extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<MediaInfo> CREATOR = new zzaj();

    /* renamed from: a  reason: collision with root package name */
    private final String f3768a;
    private int b;
    private String c;
    private MediaMetadata d;
    private long e;
    private List<MediaTrack> f;
    private TextTrackStyle g;
    private String h;
    private List<AdBreakInfo> i;
    private List<AdBreakClipInfo> j;
    private String k;
    private VastAdsRequest l;
    private long m;
    private JSONObject n;

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final MediaInfo f3769a;

        public Builder(String str) throws IllegalArgumentException {
            this.f3769a = new MediaInfo(str);
        }

        public Builder a(int i) throws IllegalArgumentException {
            this.f3769a.b(i);
            return this;
        }

        public Builder a(String str) {
            this.f3769a.e(str);
            return this;
        }

        public Builder a(MediaMetadata mediaMetadata) {
            this.f3769a.a(mediaMetadata);
            return this;
        }

        public Builder a(long j) throws IllegalArgumentException {
            this.f3769a.h(j);
            return this;
        }

        public Builder a(List<MediaTrack> list) {
            this.f3769a.a(list);
            return this;
        }

        public Builder a(TextTrackStyle textTrackStyle) {
            this.f3769a.a(textTrackStyle);
            return this;
        }

        public MediaInfo a() {
            return this.f3769a;
        }
    }

    MediaInfo(String str, int i2, String str2, MediaMetadata mediaMetadata, long j2, List<MediaTrack> list, TextTrackStyle textTrackStyle, String str3, List<AdBreakInfo> list2, List<AdBreakClipInfo> list3, String str4, VastAdsRequest vastAdsRequest, long j3) {
        this.f3768a = str;
        this.b = i2;
        this.c = str2;
        this.d = mediaMetadata;
        this.e = j2;
        this.f = list;
        this.g = textTrackStyle;
        this.h = str3;
        String str5 = this.h;
        if (str5 != null) {
            try {
                this.n = new JSONObject(str5);
            } catch (JSONException unused) {
                this.n = null;
                this.h = null;
            }
        } else {
            this.n = null;
        }
        this.i = list2;
        this.j = list3;
        this.k = str4;
        this.l = vastAdsRequest;
        this.m = j3;
    }

    public int A() {
        return this.b;
    }

    public TextTrackStyle B() {
        return this.g;
    }

    public VastAdsRequest C() {
        return this.l;
    }

    public final JSONObject D() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("contentId", this.f3768a);
            int i2 = this.b;
            jSONObject.put("streamType", i2 != 1 ? i2 != 2 ? "NONE" : "LIVE" : "BUFFERED");
            if (this.c != null) {
                jSONObject.put("contentType", this.c);
            }
            if (this.d != null) {
                jSONObject.put("metadata", this.d.w());
            }
            if (this.e <= -1) {
                jSONObject.put("duration", JSONObject.NULL);
            } else {
                jSONObject.put("duration", ((double) this.e) / 1000.0d);
            }
            if (this.f != null) {
                JSONArray jSONArray = new JSONArray();
                for (MediaTrack z : this.f) {
                    jSONArray.put(z.z());
                }
                jSONObject.put("tracks", jSONArray);
            }
            if (this.g != null) {
                jSONObject.put("textTrackStyle", this.g.D());
            }
            if (this.n != null) {
                jSONObject.put("customData", this.n);
            }
            if (this.k != null) {
                jSONObject.put("entity", this.k);
            }
            if (this.i != null) {
                JSONArray jSONArray2 = new JSONArray();
                for (AdBreakInfo y : this.i) {
                    jSONArray2.put(y.y());
                }
                jSONObject.put("breaks", jSONArray2);
            }
            if (this.j != null) {
                JSONArray jSONArray3 = new JSONArray();
                for (AdBreakClipInfo D : this.j) {
                    jSONArray3.put(D.D());
                }
                jSONObject.put("breakClips", jSONArray3);
            }
            if (this.l != null) {
                jSONObject.put("vmapAdsRequest", this.l.u());
            }
            if (this.m != -1) {
                jSONObject.put("startAbsoluteTime", ((double) this.m) / 1000.0d);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public final long E() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final void a(MediaMetadata mediaMetadata) {
        this.d = mediaMetadata;
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) throws IllegalArgumentException {
        if (i2 < -1 || i2 > 2) {
            throw new IllegalArgumentException("invalid stream type");
        }
        this.b = i2;
    }

    /* access modifiers changed from: package-private */
    public final void e(String str) {
        this.c = str;
    }

    public boolean equals(Object obj) {
        JSONObject jSONObject;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaInfo)) {
            return false;
        }
        MediaInfo mediaInfo = (MediaInfo) obj;
        if ((this.n == null) != (mediaInfo.n == null)) {
            return false;
        }
        JSONObject jSONObject2 = this.n;
        return (jSONObject2 == null || (jSONObject = mediaInfo.n) == null || JsonUtils.a(jSONObject2, jSONObject)) && zzdk.zza(this.f3768a, mediaInfo.f3768a) && this.b == mediaInfo.b && zzdk.zza(this.c, mediaInfo.c) && zzdk.zza(this.d, mediaInfo.d) && this.e == mediaInfo.e && zzdk.zza(this.f, mediaInfo.f) && zzdk.zza(this.g, mediaInfo.g) && zzdk.zza(this.i, mediaInfo.i) && zzdk.zza(this.j, mediaInfo.j) && zzdk.zza(this.k, mediaInfo.k) && zzdk.zza(this.l, mediaInfo.l) && this.m == mediaInfo.m;
    }

    /* access modifiers changed from: package-private */
    public final void h(long j2) throws IllegalArgumentException {
        if (j2 >= 0 || j2 == -1) {
            this.e = j2;
            return;
        }
        throw new IllegalArgumentException("Invalid stream duration");
    }

    public int hashCode() {
        return Objects.a(this.f3768a, Integer.valueOf(this.b), this.c, this.d, Long.valueOf(this.e), String.valueOf(this.n), this.f, this.g, this.i, this.j, this.k, this.l, Long.valueOf(this.m));
    }

    public List<AdBreakClipInfo> s() {
        List<AdBreakClipInfo> list = this.j;
        if (list == null) {
            return null;
        }
        return Collections.unmodifiableList(list);
    }

    public List<AdBreakInfo> t() {
        List<AdBreakInfo> list = this.i;
        if (list == null) {
            return null;
        }
        return Collections.unmodifiableList(list);
    }

    public String u() {
        return this.f3768a;
    }

    public String v() {
        return this.c;
    }

    public String w() {
        return this.k;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        JSONObject jSONObject = this.n;
        this.h = jSONObject == null ? null : jSONObject.toString();
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, u(), false);
        SafeParcelWriter.a(parcel, 3, A());
        SafeParcelWriter.a(parcel, 4, v(), false);
        SafeParcelWriter.a(parcel, 5, (Parcelable) y(), i2, false);
        SafeParcelWriter.a(parcel, 6, z());
        SafeParcelWriter.c(parcel, 7, x(), false);
        SafeParcelWriter.a(parcel, 8, (Parcelable) B(), i2, false);
        SafeParcelWriter.a(parcel, 9, this.h, false);
        SafeParcelWriter.c(parcel, 10, t(), false);
        SafeParcelWriter.c(parcel, 11, s(), false);
        SafeParcelWriter.a(parcel, 12, w(), false);
        SafeParcelWriter.a(parcel, 13, (Parcelable) C(), i2, false);
        SafeParcelWriter.a(parcel, 14, this.m);
        SafeParcelWriter.a(parcel, a2);
    }

    public List<MediaTrack> x() {
        return this.f;
    }

    public MediaMetadata y() {
        return this.d;
    }

    public long z() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final void a(List<MediaTrack> list) {
        this.f = list;
    }

    public void a(TextTrackStyle textTrackStyle) {
        this.g = textTrackStyle;
    }

    public final void b(List<AdBreakInfo> list) {
        this.i = list;
    }

    /* access modifiers changed from: package-private */
    public final void a(JSONObject jSONObject) throws JSONException {
        int i2 = 0;
        if (jSONObject.has("breaks")) {
            JSONArray jSONArray = jSONObject.getJSONArray("breaks");
            this.i = new ArrayList(jSONArray.length());
            int i3 = 0;
            while (true) {
                if (i3 < jSONArray.length()) {
                    AdBreakInfo a2 = AdBreakInfo.a(jSONArray.getJSONObject(i3));
                    if (a2 == null) {
                        this.i.clear();
                        break;
                    } else {
                        this.i.add(a2);
                        i3++;
                    }
                } else {
                    break;
                }
            }
        }
        if (jSONObject.has("breakClips")) {
            JSONArray jSONArray2 = jSONObject.getJSONArray("breakClips");
            this.j = new ArrayList(jSONArray2.length());
            while (i2 < jSONArray2.length()) {
                AdBreakClipInfo a3 = AdBreakClipInfo.a(jSONArray2.getJSONObject(i2));
                if (a3 != null) {
                    this.j.add(a3);
                    i2++;
                } else {
                    this.j.clear();
                    return;
                }
            }
        }
    }

    MediaInfo(String str) throws IllegalArgumentException {
        this(str, -1, (String) null, (MediaMetadata) null, -1, (List<MediaTrack>) null, (TextTrackStyle) null, (String) null, (List<AdBreakInfo>) null, (List<AdBreakClipInfo>) null, (String) null, (VastAdsRequest) null, -1);
        if (str == null) {
            throw new IllegalArgumentException("contentID cannot be null");
        }
    }

    MediaInfo(JSONObject jSONObject) throws JSONException {
        this(jSONObject.getString("contentId"), -1, (String) null, (MediaMetadata) null, -1, (List<MediaTrack>) null, (TextTrackStyle) null, (String) null, (List<AdBreakInfo>) null, (List<AdBreakClipInfo>) null, (String) null, (VastAdsRequest) null, -1);
        MediaInfo mediaInfo;
        JSONObject jSONObject2 = jSONObject;
        String string = jSONObject2.getString("streamType");
        if ("NONE".equals(string)) {
            mediaInfo = this;
            mediaInfo.b = 0;
        } else {
            mediaInfo = this;
            if ("BUFFERED".equals(string)) {
                mediaInfo.b = 1;
            } else if ("LIVE".equals(string)) {
                mediaInfo.b = 2;
            } else {
                mediaInfo.b = -1;
            }
        }
        mediaInfo.c = jSONObject2.optString("contentType", (String) null);
        if (jSONObject2.has("metadata")) {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("metadata");
            mediaInfo.d = new MediaMetadata(jSONObject3.getInt("metadataType"));
            mediaInfo.d.a(jSONObject3);
        }
        mediaInfo.e = -1;
        if (jSONObject2.has("duration") && !jSONObject2.isNull("duration")) {
            double optDouble = jSONObject2.optDouble("duration", 0.0d);
            if (!Double.isNaN(optDouble) && !Double.isInfinite(optDouble)) {
                mediaInfo.e = (long) (optDouble * 1000.0d);
            }
        }
        if (jSONObject2.has("tracks")) {
            mediaInfo.f = new ArrayList();
            JSONArray jSONArray = jSONObject2.getJSONArray("tracks");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                mediaInfo.f.add(new MediaTrack(jSONArray.getJSONObject(i2)));
            }
        } else {
            mediaInfo.f = null;
        }
        if (jSONObject2.has("textTrackStyle")) {
            JSONObject jSONObject4 = jSONObject2.getJSONObject("textTrackStyle");
            TextTrackStyle textTrackStyle = new TextTrackStyle();
            textTrackStyle.a(jSONObject4);
            mediaInfo.g = textTrackStyle;
        } else {
            mediaInfo.g = null;
        }
        a(jSONObject);
        mediaInfo.n = jSONObject2.optJSONObject("customData");
        if (jSONObject2.has("entity")) {
            mediaInfo.k = jSONObject2.getString("entity");
        }
        mediaInfo.l = VastAdsRequest.a(jSONObject2.optJSONObject("vmapAdsRequest"));
    }
}
