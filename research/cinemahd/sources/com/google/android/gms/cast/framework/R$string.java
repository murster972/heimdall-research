package com.google.android.gms.cast.framework;

public final class R$string {
    public static final int cast_ad_label = 2131755100;
    public static final int cast_casting_to_device = 2131755101;
    public static final int cast_closed_captions = 2131755102;
    public static final int cast_closed_captions_unavailable = 2131755103;
    public static final int cast_connecting_to_device = 2131755105;
    public static final int cast_disconnect = 2131755106;
    public static final int cast_expanded_controller_ad_image_description = 2131755107;
    public static final int cast_expanded_controller_ad_in_progress = 2131755108;
    public static final int cast_expanded_controller_background_image = 2131755109;
    public static final int cast_expanded_controller_live_head_description = 2131755110;
    public static final int cast_expanded_controller_live_stream_indicator = 2131755111;
    public static final int cast_expanded_controller_loading = 2131755112;
    public static final int cast_expanded_controller_skip_ad_label = 2131755113;
    public static final int cast_expanded_controller_skip_ad_text = 2131755114;
    public static final int cast_forward = 2131755115;
    public static final int cast_forward_10 = 2131755116;
    public static final int cast_forward_30 = 2131755117;
    public static final int cast_intro_overlay_button_text = 2131755118;
    public static final int cast_invalid_stream_duration_text = 2131755119;
    public static final int cast_invalid_stream_position_text = 2131755120;
    public static final int cast_live_label = 2131755121;
    public static final int cast_mute = 2131755122;
    public static final int cast_pause = 2131755127;
    public static final int cast_play = 2131755128;
    public static final int cast_rewind = 2131755129;
    public static final int cast_rewind_10 = 2131755130;
    public static final int cast_rewind_30 = 2131755131;
    public static final int cast_seek_bar = 2131755132;
    public static final int cast_skip_next = 2131755133;
    public static final int cast_skip_prev = 2131755134;
    public static final int cast_stop = 2131755135;
    public static final int cast_stop_live_stream = 2131755136;
    public static final int cast_tracks_chooser_dialog_audio = 2131755137;
    public static final int cast_tracks_chooser_dialog_cancel = 2131755138;
    public static final int cast_tracks_chooser_dialog_closed_captions = 2131755139;
    public static final int cast_tracks_chooser_dialog_default_track_name = 2131755140;
    public static final int cast_tracks_chooser_dialog_none = 2131755141;
    public static final int cast_tracks_chooser_dialog_ok = 2131755142;
    public static final int cast_tracks_chooser_dialog_subtitles = 2131755143;
    public static final int cast_unmute = 2131755144;

    private R$string() {
    }
}
