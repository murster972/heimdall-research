package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.internal.cast.zzdk;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaStatus extends AbstractSafeParcelable {
    public static final Parcelable.Creator<MediaStatus> CREATOR = new zzav();

    /* renamed from: a  reason: collision with root package name */
    private MediaInfo f3776a;
    private long b;
    private int c;
    private double d;
    private int e;
    private int f;
    private long g;
    private long h;
    private double i;
    private boolean j;
    private long[] k;
    private int l;
    private int m;
    private String n;
    private JSONObject o;
    private int p;
    private final ArrayList<MediaQueueItem> q;
    private boolean r;
    private AdBreakStatus s;
    private VideoInfo t;
    private final SparseArray<Integer> u;

    MediaStatus(MediaInfo mediaInfo, long j2, int i2, double d2, int i3, int i4, long j3, long j4, double d3, boolean z, long[] jArr, int i5, int i6, String str, int i7, List<MediaQueueItem> list, boolean z2, AdBreakStatus adBreakStatus, VideoInfo videoInfo) {
        List<MediaQueueItem> list2 = list;
        this.q = new ArrayList<>();
        this.u = new SparseArray<>();
        this.f3776a = mediaInfo;
        this.b = j2;
        this.c = i2;
        this.d = d2;
        this.e = i3;
        this.f = i4;
        this.g = j3;
        this.h = j4;
        this.i = d3;
        this.j = z;
        this.k = jArr;
        this.l = i5;
        this.m = i6;
        this.n = str;
        String str2 = this.n;
        if (str2 != null) {
            try {
                this.o = new JSONObject(str2);
            } catch (JSONException unused) {
                this.o = null;
                this.n = null;
            }
        } else {
            this.o = null;
        }
        this.p = i7;
        if (list2 != null && !list.isEmpty()) {
            a((MediaQueueItem[]) list2.toArray(new MediaQueueItem[list.size()]));
        }
        this.r = z2;
        this.s = adBreakStatus;
        this.t = videoInfo;
    }

    private static boolean a(int i2, int i3, int i4, int i5) {
        if (i2 != 1) {
            return false;
        }
        if (i3 != 1) {
            if (i3 == 2) {
                return i5 != 2;
            }
            if (i3 != 3) {
                return true;
            }
        }
        return i4 == 0;
    }

    public int A() {
        return this.e;
    }

    public int B() {
        return this.m;
    }

    public int C() {
        return this.q.size();
    }

    public int D() {
        return this.p;
    }

    public long E() {
        return this.g;
    }

    public double F() {
        return this.i;
    }

    public VideoInfo G() {
        return this.t;
    }

    public boolean H() {
        return this.j;
    }

    public boolean I() {
        return this.r;
    }

    public final long J() {
        return this.b;
    }

    public final void a(boolean z) {
        this.r = z;
    }

    public Integer b(int i2) {
        return this.u.get(i2);
    }

    public MediaQueueItem c(int i2) {
        Integer num = this.u.get(i2);
        if (num == null) {
            return null;
        }
        return this.q.get(num.intValue());
    }

    public MediaQueueItem d(int i2) {
        return c(i2);
    }

    public boolean equals(Object obj) {
        JSONObject jSONObject;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaStatus)) {
            return false;
        }
        MediaStatus mediaStatus = (MediaStatus) obj;
        if ((this.o == null) == (mediaStatus.o == null) && this.b == mediaStatus.b && this.c == mediaStatus.c && this.d == mediaStatus.d && this.e == mediaStatus.e && this.f == mediaStatus.f && this.g == mediaStatus.g && this.i == mediaStatus.i && this.j == mediaStatus.j && this.l == mediaStatus.l && this.m == mediaStatus.m && this.p == mediaStatus.p && Arrays.equals(this.k, mediaStatus.k) && zzdk.zza(Long.valueOf(this.h), Long.valueOf(mediaStatus.h)) && zzdk.zza(this.q, mediaStatus.q) && zzdk.zza(this.f3776a, mediaStatus.f3776a)) {
            JSONObject jSONObject2 = this.o;
            return (jSONObject2 == null || (jSONObject = mediaStatus.o) == null || JsonUtils.a(jSONObject2, jSONObject)) && this.r == mediaStatus.I() && zzdk.zza(this.s, mediaStatus.s) && zzdk.zza(this.t, mediaStatus.t) && zzdk.zza(null, null) && Objects.a((Object) null, (Object) null);
        }
    }

    public boolean h(long j2) {
        return (j2 & this.h) != 0;
    }

    public int hashCode() {
        return Objects.a(this.f3776a, Long.valueOf(this.b), Integer.valueOf(this.c), Double.valueOf(this.d), Integer.valueOf(this.e), Integer.valueOf(this.f), Long.valueOf(this.g), Long.valueOf(this.h), Double.valueOf(this.i), Boolean.valueOf(this.j), Integer.valueOf(Arrays.hashCode(this.k)), Integer.valueOf(this.l), Integer.valueOf(this.m), String.valueOf(this.o), Integer.valueOf(this.p), this.q, Boolean.valueOf(this.r), this.s, this.t, null, null);
    }

    public long[] s() {
        return this.k;
    }

    public AdBreakStatus t() {
        return this.s;
    }

    public AdBreakClipInfo u() {
        List<AdBreakClipInfo> s2;
        AdBreakStatus adBreakStatus = this.s;
        if (!(adBreakStatus == null || this.f3776a == null)) {
            String s3 = adBreakStatus.s();
            if (!TextUtils.isEmpty(s3) && (s2 = this.f3776a.s()) != null && !s2.isEmpty()) {
                for (AdBreakClipInfo next : s2) {
                    if (s3.equals(next.x())) {
                        return next;
                    }
                }
            }
        }
        return null;
    }

    public int v() {
        return this.c;
    }

    public int w() {
        return this.f;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        JSONObject jSONObject = this.o;
        this.n = jSONObject == null ? null : jSONObject.toString();
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, (Parcelable) y(), i2, false);
        SafeParcelWriter.a(parcel, 3, this.b);
        SafeParcelWriter.a(parcel, 4, v());
        SafeParcelWriter.a(parcel, 5, z());
        SafeParcelWriter.a(parcel, 6, A());
        SafeParcelWriter.a(parcel, 7, w());
        SafeParcelWriter.a(parcel, 8, E());
        SafeParcelWriter.a(parcel, 9, this.h);
        SafeParcelWriter.a(parcel, 10, F());
        SafeParcelWriter.a(parcel, 11, H());
        SafeParcelWriter.a(parcel, 12, s(), false);
        SafeParcelWriter.a(parcel, 13, x());
        SafeParcelWriter.a(parcel, 14, B());
        SafeParcelWriter.a(parcel, 15, this.n, false);
        SafeParcelWriter.a(parcel, 16, this.p);
        SafeParcelWriter.c(parcel, 17, this.q, false);
        SafeParcelWriter.a(parcel, 18, I());
        SafeParcelWriter.a(parcel, 19, (Parcelable) t(), i2, false);
        SafeParcelWriter.a(parcel, 20, (Parcelable) G(), i2, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public int x() {
        return this.l;
    }

    public MediaInfo y() {
        return this.f3776a;
    }

    public double z() {
        return this.d;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:178:0x02eb, code lost:
        if (r15 == false) goto L_0x030a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0326  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0357  */
    /* JADX WARNING: Removed duplicated region for block: B:215:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(org.json.JSONObject r14, int r15) throws org.json.JSONException {
        /*
            r13 = this;
            java.lang.String r0 = "mediaSessionId"
            long r0 = r14.getLong(r0)
            long r2 = r13.b
            r4 = 0
            r5 = 1
            int r6 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r6 == 0) goto L_0x0012
            r13.b = r0
            r0 = 1
            goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            java.lang.String r1 = "playerState"
            boolean r2 = r14.has(r1)
            r3 = 3
            r6 = 2
            if (r2 == 0) goto L_0x0091
            java.lang.String r1 = r14.getString(r1)
            java.lang.String r2 = "IDLE"
            boolean r2 = r1.equals(r2)
            r7 = 4
            if (r2 == 0) goto L_0x002c
            r1 = 1
            goto L_0x004b
        L_0x002c:
            java.lang.String r2 = "PLAYING"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0036
            r1 = 2
            goto L_0x004b
        L_0x0036:
            java.lang.String r2 = "PAUSED"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0040
            r1 = 3
            goto L_0x004b
        L_0x0040:
            java.lang.String r2 = "BUFFERING"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x004a
            r1 = 4
            goto L_0x004b
        L_0x004a:
            r1 = 0
        L_0x004b:
            int r2 = r13.e
            if (r1 == r2) goto L_0x0053
            r13.e = r1
            r0 = r0 | 2
        L_0x0053:
            if (r1 != r5) goto L_0x0091
            java.lang.String r1 = "idleReason"
            boolean r2 = r14.has(r1)
            if (r2 == 0) goto L_0x0091
            java.lang.String r1 = r14.getString(r1)
            java.lang.String r2 = "CANCELLED"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x006b
            r7 = 2
            goto L_0x0089
        L_0x006b:
            java.lang.String r2 = "INTERRUPTED"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0075
            r7 = 3
            goto L_0x0089
        L_0x0075:
            java.lang.String r2 = "FINISHED"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x007f
            r7 = 1
            goto L_0x0089
        L_0x007f:
            java.lang.String r2 = "ERROR"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0088
            goto L_0x0089
        L_0x0088:
            r7 = 0
        L_0x0089:
            int r1 = r13.f
            if (r7 == r1) goto L_0x0091
            r13.f = r7
            r0 = r0 | 2
        L_0x0091:
            java.lang.String r1 = "playbackRate"
            boolean r2 = r14.has(r1)
            if (r2 == 0) goto L_0x00a7
            double r1 = r14.getDouble(r1)
            double r7 = r13.d
            int r9 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r9 == 0) goto L_0x00a7
            r13.d = r1
            r0 = r0 | 2
        L_0x00a7:
            java.lang.String r1 = "currentTime"
            boolean r2 = r14.has(r1)
            if (r2 == 0) goto L_0x00c7
            double r1 = r14.getDouble(r1)
            r7 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r1 = r1 * r7
            long r1 = (long) r1
            long r7 = r13.g
            int r9 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x00c5
            r13.g = r1
            r0 = r0 | 2
        L_0x00c5:
            r0 = r0 | 128(0x80, float:1.794E-43)
        L_0x00c7:
            java.lang.String r1 = "supportedMediaCommands"
            boolean r2 = r14.has(r1)
            if (r2 == 0) goto L_0x00dd
            long r1 = r14.getLong(r1)
            long r7 = r13.h
            int r9 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x00dd
            r13.h = r1
            r0 = r0 | 2
        L_0x00dd:
            java.lang.String r1 = "volume"
            boolean r2 = r14.has(r1)
            if (r2 == 0) goto L_0x010a
            r15 = r15 & r5
            if (r15 != 0) goto L_0x010a
            org.json.JSONObject r15 = r14.getJSONObject(r1)
            java.lang.String r1 = "level"
            double r1 = r15.getDouble(r1)
            double r7 = r13.i
            int r9 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x00fc
            r13.i = r1
            r0 = r0 | 2
        L_0x00fc:
            java.lang.String r1 = "muted"
            boolean r15 = r15.getBoolean(r1)
            boolean r1 = r13.j
            if (r15 == r1) goto L_0x010a
            r13.j = r15
            r0 = r0 | 2
        L_0x010a:
            java.lang.String r15 = "activeTrackIds"
            boolean r1 = r14.has(r15)
            r2 = 0
            if (r1 == 0) goto L_0x014a
            org.json.JSONArray r15 = r14.getJSONArray(r15)
            int r1 = r15.length()
            long[] r7 = new long[r1]
            r8 = 0
        L_0x011e:
            if (r8 >= r1) goto L_0x0129
            long r9 = r15.getLong(r8)
            r7[r8] = r9
            int r8 = r8 + 1
            goto L_0x011e
        L_0x0129:
            long[] r15 = r13.k
            if (r15 != 0) goto L_0x012f
        L_0x012d:
            r15 = 1
            goto L_0x0145
        L_0x012f:
            int r15 = r15.length
            if (r15 == r1) goto L_0x0133
            goto L_0x012d
        L_0x0133:
            r15 = 0
        L_0x0134:
            if (r15 >= r1) goto L_0x0144
            long[] r8 = r13.k
            r9 = r8[r15]
            r11 = r7[r15]
            int r8 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r8 == 0) goto L_0x0141
            goto L_0x012d
        L_0x0141:
            int r15 = r15 + 1
            goto L_0x0134
        L_0x0144:
            r15 = 0
        L_0x0145:
            if (r15 == 0) goto L_0x0152
            r13.k = r7
            goto L_0x0152
        L_0x014a:
            long[] r15 = r13.k
            r7 = r2
            if (r15 == 0) goto L_0x0151
            r15 = 1
            goto L_0x0152
        L_0x0151:
            r15 = 0
        L_0x0152:
            if (r15 == 0) goto L_0x0158
            r13.k = r7
            r0 = r0 | 2
        L_0x0158:
            java.lang.String r15 = "customData"
            boolean r1 = r14.has(r15)
            if (r1 == 0) goto L_0x016a
            org.json.JSONObject r15 = r14.getJSONObject(r15)
            r13.o = r15
            r13.n = r2
            r0 = r0 | 2
        L_0x016a:
            java.lang.String r15 = "media"
            boolean r1 = r14.has(r15)
            if (r1 == 0) goto L_0x0195
            org.json.JSONObject r15 = r14.getJSONObject(r15)
            com.google.android.gms.cast.MediaInfo r1 = new com.google.android.gms.cast.MediaInfo
            r1.<init>((org.json.JSONObject) r15)
            com.google.android.gms.cast.MediaInfo r7 = r13.f3776a
            if (r7 == 0) goto L_0x0187
            if (r7 == 0) goto L_0x018b
            boolean r7 = r7.equals(r1)
            if (r7 != 0) goto L_0x018b
        L_0x0187:
            r13.f3776a = r1
            r0 = r0 | 2
        L_0x018b:
            java.lang.String r1 = "metadata"
            boolean r15 = r15.has(r1)
            if (r15 == 0) goto L_0x0195
            r0 = r0 | 4
        L_0x0195:
            java.lang.String r15 = "currentItemId"
            boolean r15 = r14.has(r15)
            if (r15 == 0) goto L_0x01ab
            java.lang.String r15 = "currentItemId"
            int r15 = r14.getInt(r15)
            int r1 = r13.c
            if (r1 == r15) goto L_0x01ab
            r13.c = r15
            r0 = r0 | 2
        L_0x01ab:
            java.lang.String r15 = "preloadedItemId"
            int r15 = r14.optInt(r15, r4)
            int r1 = r13.m
            if (r1 == r15) goto L_0x01b9
            r13.m = r15
            r0 = r0 | 16
        L_0x01b9:
            java.lang.String r15 = "loadingItemId"
            int r15 = r14.optInt(r15, r4)
            int r1 = r13.l
            if (r1 == r15) goto L_0x01c7
            r13.l = r15
            r0 = r0 | 2
        L_0x01c7:
            com.google.android.gms.cast.MediaInfo r15 = r13.f3776a
            if (r15 != 0) goto L_0x01cd
            r15 = -1
            goto L_0x01d1
        L_0x01cd:
            int r15 = r15.A()
        L_0x01d1:
            int r1 = r13.e
            int r7 = r13.f
            int r8 = r13.l
            boolean r15 = a(r1, r7, r8, r15)
            if (r15 != 0) goto L_0x02ee
            java.lang.String r15 = "repeatMode"
            boolean r15 = r14.has(r15)
            if (r15 == 0) goto L_0x0256
            java.lang.String r15 = "repeatMode"
            java.lang.String r15 = r14.getString(r15)
            if (r15 == 0) goto L_0x0239
            r1 = -1
            int r7 = r15.hashCode()
            switch(r7) {
                case -1118317585: goto L_0x0214;
                case -962896020: goto L_0x020a;
                case 1645938909: goto L_0x0200;
                case 1645952171: goto L_0x01f6;
                default: goto L_0x01f5;
            }
        L_0x01f5:
            goto L_0x021d
        L_0x01f6:
            java.lang.String r7 = "REPEAT_OFF"
            boolean r15 = r15.equals(r7)
            if (r15 == 0) goto L_0x021d
            r1 = 0
            goto L_0x021d
        L_0x0200:
            java.lang.String r7 = "REPEAT_ALL"
            boolean r15 = r15.equals(r7)
            if (r15 == 0) goto L_0x021d
            r1 = 1
            goto L_0x021d
        L_0x020a:
            java.lang.String r7 = "REPEAT_SINGLE"
            boolean r15 = r15.equals(r7)
            if (r15 == 0) goto L_0x021d
            r1 = 2
            goto L_0x021d
        L_0x0214:
            java.lang.String r7 = "REPEAT_ALL_AND_SHUFFLE"
            boolean r15 = r15.equals(r7)
            if (r15 == 0) goto L_0x021d
            r1 = 3
        L_0x021d:
            if (r1 == 0) goto L_0x0235
            if (r1 == r5) goto L_0x0230
            if (r1 == r6) goto L_0x022b
            if (r1 == r3) goto L_0x0226
            goto L_0x0239
        L_0x0226:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)
            goto L_0x0239
        L_0x022b:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
            goto L_0x0239
        L_0x0230:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)
            goto L_0x0239
        L_0x0235:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)
        L_0x0239:
            if (r2 != 0) goto L_0x023e
            int r15 = r13.p
            goto L_0x0242
        L_0x023e:
            int r15 = r2.intValue()
        L_0x0242:
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)
            int r1 = r13.p
            int r2 = r15.intValue()
            if (r1 == r2) goto L_0x0256
            int r15 = r15.intValue()
            r13.p = r15
            r15 = 1
            goto L_0x0257
        L_0x0256:
            r15 = 0
        L_0x0257:
            java.lang.String r1 = "items"
            boolean r1 = r14.has(r1)
            if (r1 == 0) goto L_0x02eb
            java.lang.String r1 = "items"
            org.json.JSONArray r1 = r14.getJSONArray(r1)
            int r2 = r1.length()
            android.util.SparseArray r3 = new android.util.SparseArray
            r3.<init>()
            r6 = 0
        L_0x026f:
            if (r6 >= r2) goto L_0x0285
            org.json.JSONObject r7 = r1.getJSONObject(r6)
            java.lang.String r8 = "itemId"
            int r7 = r7.getInt(r8)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r3.put(r6, r7)
            int r6 = r6 + 1
            goto L_0x026f
        L_0x0285:
            com.google.android.gms.cast.MediaQueueItem[] r6 = new com.google.android.gms.cast.MediaQueueItem[r2]
            r7 = r15
            r15 = 0
        L_0x0289:
            if (r15 >= r2) goto L_0x02dd
            java.lang.Object r8 = r3.get(r15)
            java.lang.Integer r8 = (java.lang.Integer) r8
            org.json.JSONObject r9 = r1.getJSONObject(r15)
            int r10 = r8.intValue()
            com.google.android.gms.cast.MediaQueueItem r10 = r13.c(r10)
            if (r10 == 0) goto L_0x02b5
            boolean r9 = r10.a(r9)
            r7 = r7 | r9
            r6[r15] = r10
            int r8 = r8.intValue()
            java.lang.Integer r8 = r13.b(r8)
            int r8 = r8.intValue()
            if (r15 == r8) goto L_0x02da
            goto L_0x02d9
        L_0x02b5:
            int r7 = r8.intValue()
            int r8 = r13.c
            if (r7 != r8) goto L_0x02d2
            com.google.android.gms.cast.MediaInfo r7 = r13.f3776a
            if (r7 == 0) goto L_0x02d2
            com.google.android.gms.cast.MediaQueueItem$Builder r8 = new com.google.android.gms.cast.MediaQueueItem$Builder
            r8.<init>((com.google.android.gms.cast.MediaInfo) r7)
            com.google.android.gms.cast.MediaQueueItem r7 = r8.a()
            r6[r15] = r7
            r7 = r6[r15]
            r7.a(r9)
            goto L_0x02d9
        L_0x02d2:
            com.google.android.gms.cast.MediaQueueItem r7 = new com.google.android.gms.cast.MediaQueueItem
            r7.<init>((org.json.JSONObject) r9)
            r6[r15] = r7
        L_0x02d9:
            r7 = 1
        L_0x02da:
            int r15 = r15 + 1
            goto L_0x0289
        L_0x02dd:
            java.util.ArrayList<com.google.android.gms.cast.MediaQueueItem> r15 = r13.q
            int r15 = r15.size()
            if (r15 == r2) goto L_0x02e7
            r15 = 1
            goto L_0x02e8
        L_0x02e7:
            r15 = r7
        L_0x02e8:
            r13.a((com.google.android.gms.cast.MediaQueueItem[]) r6)
        L_0x02eb:
            if (r15 == 0) goto L_0x030a
            goto L_0x0308
        L_0x02ee:
            r13.c = r4
            r13.l = r4
            r13.m = r4
            java.util.ArrayList<com.google.android.gms.cast.MediaQueueItem> r15 = r13.q
            boolean r15 = r15.isEmpty()
            if (r15 != 0) goto L_0x030a
            r13.p = r4
            java.util.ArrayList<com.google.android.gms.cast.MediaQueueItem> r15 = r13.q
            r15.clear()
            android.util.SparseArray<java.lang.Integer> r15 = r13.u
            r15.clear()
        L_0x0308:
            r0 = r0 | 8
        L_0x030a:
            java.lang.String r15 = "breakStatus"
            org.json.JSONObject r15 = r14.optJSONObject(r15)
            com.google.android.gms.cast.AdBreakStatus r15 = com.google.android.gms.cast.AdBreakStatus.a(r15)
            com.google.android.gms.cast.AdBreakStatus r1 = r13.s
            if (r1 != 0) goto L_0x031a
            if (r15 != 0) goto L_0x0324
        L_0x031a:
            com.google.android.gms.cast.AdBreakStatus r1 = r13.s
            if (r1 == 0) goto L_0x032d
            boolean r1 = r1.equals(r15)
            if (r1 != 0) goto L_0x032d
        L_0x0324:
            if (r15 == 0) goto L_0x0327
            r4 = 1
        L_0x0327:
            r13.r = r4
            r13.s = r15
            r0 = r0 | 32
        L_0x032d:
            java.lang.String r15 = "videoInfo"
            org.json.JSONObject r15 = r14.optJSONObject(r15)
            com.google.android.gms.cast.VideoInfo r15 = com.google.android.gms.cast.VideoInfo.a(r15)
            com.google.android.gms.cast.VideoInfo r1 = r13.t
            if (r1 != 0) goto L_0x033d
            if (r15 != 0) goto L_0x0347
        L_0x033d:
            com.google.android.gms.cast.VideoInfo r1 = r13.t
            if (r1 == 0) goto L_0x034b
            boolean r1 = r1.equals(r15)
            if (r1 != 0) goto L_0x034b
        L_0x0347:
            r13.t = r15
            r0 = r0 | 64
        L_0x034b:
            java.lang.String r15 = "breakInfo"
            boolean r15 = r14.has(r15)
            if (r15 == 0) goto L_0x0362
            com.google.android.gms.cast.MediaInfo r15 = r13.f3776a
            if (r15 == 0) goto L_0x0362
            java.lang.String r1 = "breakInfo"
            org.json.JSONObject r14 = r14.getJSONObject(r1)
            r15.a((org.json.JSONObject) r14)
            r0 = r0 | 2
        L_0x0362:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.MediaStatus.a(org.json.JSONObject, int):int");
    }

    public MediaStatus(JSONObject jSONObject) throws JSONException {
        this((MediaInfo) null, 0, 0, 0.0d, 0, 0, 0, 0, 0.0d, false, (long[]) null, 0, 0, (String) null, 0, (List<MediaQueueItem>) null, false, (AdBreakStatus) null, (VideoInfo) null);
        a(jSONObject, 0);
    }

    private final void a(MediaQueueItem[] mediaQueueItemArr) {
        this.q.clear();
        this.u.clear();
        for (int i2 = 0; i2 < mediaQueueItemArr.length; i2++) {
            MediaQueueItem mediaQueueItem = mediaQueueItemArr[i2];
            this.q.add(mediaQueueItem);
            this.u.put(mediaQueueItem.u(), Integer.valueOf(i2));
        }
    }
}
