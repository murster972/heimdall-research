package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzai implements Parcelable.Creator<LaunchOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        boolean z = false;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                z = SafeParcelReader.s(parcel, a2);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                str = SafeParcelReader.o(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new LaunchOptions(z, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LaunchOptions[i];
    }
}
