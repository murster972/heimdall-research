package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class zzr implements Parcelable.Creator<NotificationOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        ArrayList<String> arrayList = null;
        int[] iArr = null;
        String str = null;
        IBinder iBinder = null;
        long j = 0;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        int i21 = 0;
        int i22 = 0;
        int i23 = 0;
        int i24 = 0;
        int i25 = 0;
        int i26 = 0;
        int i27 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    arrayList = SafeParcelReader.q(parcel2, a2);
                    break;
                case 3:
                    iArr = SafeParcelReader.j(parcel2, a2);
                    break;
                case 4:
                    j = SafeParcelReader.y(parcel2, a2);
                    break;
                case 5:
                    str = SafeParcelReader.o(parcel2, a2);
                    break;
                case 6:
                    i = SafeParcelReader.w(parcel2, a2);
                    break;
                case 7:
                    i2 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 8:
                    i3 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 9:
                    i4 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 10:
                    i5 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 11:
                    i6 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 12:
                    i7 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 13:
                    i8 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 14:
                    i9 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 15:
                    i10 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 16:
                    i11 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 17:
                    i12 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 18:
                    i13 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 19:
                    i14 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 20:
                    i15 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 21:
                    i16 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 22:
                    i17 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 23:
                    i18 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 24:
                    i19 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 25:
                    i20 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 26:
                    i21 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 27:
                    i22 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 28:
                    i23 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 29:
                    i24 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 30:
                    i25 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 31:
                    i26 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 32:
                    i27 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 33:
                    iBinder = SafeParcelReader.v(parcel2, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel2, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel2, b);
        return new NotificationOptions(arrayList, iArr, j, str, i, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, i19, i20, i21, i22, i23, i24, i25, i26, i27, iBinder);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new NotificationOptions[i];
    }
}
