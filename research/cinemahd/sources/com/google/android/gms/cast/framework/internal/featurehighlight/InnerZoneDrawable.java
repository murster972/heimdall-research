package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.annotation.Keep;
import com.google.android.gms.cast.framework.R$dimen;
import com.google.android.gms.cast.framework.R$integer;
import com.google.android.gms.internal.cast.zzfl;

class InnerZoneDrawable extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private final Paint f3796a = new Paint();
    private final Paint b = new Paint();
    private final Rect c = new Rect();
    private final int d;
    private final int e;
    private float f;
    private float g = 1.0f;
    private float h;
    private float i;
    private float j;
    private float k;

    public InnerZoneDrawable(Context context) {
        Resources resources = context.getResources();
        this.d = resources.getDimensionPixelSize(R$dimen.cast_libraries_material_featurehighlight_inner_radius);
        this.e = resources.getInteger(R$integer.cast_libraries_material_featurehighlight_pulse_base_alpha);
        this.f3796a.setAntiAlias(true);
        this.f3796a.setStyle(Paint.Style.FILL);
        this.b.setAntiAlias(true);
        this.b.setStyle(Paint.Style.FILL);
        this.f3796a.setColor(-1);
        this.b.setColor(-1);
        invalidateSelf();
    }

    public final void a(Rect rect) {
        this.c.set(rect);
        this.h = this.c.exactCenterX();
        this.i = this.c.exactCenterY();
        this.f = Math.max((float) this.d, Math.max(((float) this.c.width()) / 2.0f, ((float) this.c.height()) / 2.0f));
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        float f2 = this.k;
        if (f2 > 0.0f) {
            float f3 = this.f * this.j;
            this.b.setAlpha((int) (((float) this.e) * f2));
            canvas.drawCircle(this.h, this.i, f3, this.b);
        }
        canvas.drawCircle(this.h, this.i, this.f * this.g, this.f3796a);
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i2) {
        this.f3796a.setAlpha(i2);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f3796a.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @Keep
    public void setPulseAlpha(float f2) {
        this.k = f2;
        invalidateSelf();
    }

    @Keep
    public void setPulseScale(float f2) {
        this.j = f2;
        invalidateSelf();
    }

    @Keep
    public void setScale(float f2) {
        this.g = f2;
        invalidateSelf();
    }

    public final Animator a() {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{0.0f}), PropertyValuesHolder.ofInt("alpha", new int[]{0}), PropertyValuesHolder.ofFloat("pulseScale", new float[]{0.0f}), PropertyValuesHolder.ofFloat("pulseAlpha", new float[]{0.0f})});
        ofPropertyValuesHolder.setInterpolator(zzfl.zzfl());
        return ofPropertyValuesHolder.setDuration(200);
    }
}
