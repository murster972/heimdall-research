package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

final class zzc extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ View f3800a;
    private final /* synthetic */ boolean b = true;
    private final /* synthetic */ zzh c;

    zzc(zza zza, View view, boolean z, zzh zzh) {
        this.f3800a = view;
        this.c = zzh;
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        if (this.f3800a.getParent() != null) {
            this.f3800a.performClick();
        }
        if (!this.b) {
            return true;
        }
        this.c.zzav();
        return true;
    }
}
