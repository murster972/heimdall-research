package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzae implements Parcelable.Creator<zzad> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        zzab zzab = null;
        zzab zzab2 = null;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                zzab = (zzab) SafeParcelReader.a(parcel, a2, zzab.CREATOR);
            } else if (a3 != 3) {
                SafeParcelReader.A(parcel, a2);
            } else {
                zzab2 = (zzab) SafeParcelReader.a(parcel, a2, zzab.CREATOR);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzad(zzab, zzab2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzad[i];
    }
}
