package com.google.android.gms.cast.framework;

public final class R$style {
    public static final int CastExpandedController = 2131820760;
    public static final int CastIntroOverlay = 2131820761;
    public static final int CastMiniController = 2131820762;
    public static final int CustomCastTheme = 2131820767;
    public static final int TextAppearance_CastExpandedController_AdInProgressLabel = 2131820929;
    public static final int TextAppearance_CastExpandedController_AdLabel = 2131820930;
    public static final int TextAppearance_CastIntroOverlay_Button = 2131820931;
    public static final int TextAppearance_CastIntroOverlay_Title = 2131820932;
    public static final int TextAppearance_CastMiniController_Subtitle = 2131820933;
    public static final int TextAppearance_CastMiniController_Title = 2131820934;

    private R$style() {
    }
}
