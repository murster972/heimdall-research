package com.google.android.gms.cast.framework.media.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.widget.AbsSeekBar;
import android.widget.SeekBar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.graphics.drawable.DrawableCompat;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.cast.zzdw;
import java.lang.reflect.Field;

public final class zze {

    /* renamed from: a  reason: collision with root package name */
    private static final zzdw f3842a = new zzdw("WidgetUtil");

    @TargetApi(17)
    public static Bitmap a(Context context, Bitmap bitmap, float f, float f2) {
        f3842a.d("Begin blurring bitmap %s, original width = %d, original height = %d.", bitmap, Integer.valueOf(bitmap.getWidth()), Integer.valueOf(bitmap.getHeight()));
        int round = Math.round(((float) bitmap.getWidth()) * 0.25f);
        int round2 = Math.round(((float) bitmap.getHeight()) * 0.25f);
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, round, round2, false);
        Bitmap createBitmap = Bitmap.createBitmap(round, round2, createScaledBitmap.getConfig());
        RenderScript create = RenderScript.create(context);
        Allocation createFromBitmap = Allocation.createFromBitmap(create, createScaledBitmap);
        Allocation createTyped = Allocation.createTyped(create, createFromBitmap.getType());
        ScriptIntrinsicBlur create2 = ScriptIntrinsicBlur.create(create, createFromBitmap.getElement());
        create2.setInput(createFromBitmap);
        create2.setRadius(7.5f);
        create2.forEach(createTyped);
        createTyped.copyTo(createBitmap);
        create.destroy();
        f3842a.d("End blurring bitmap %s, original width = %d, original height = %d.", createScaledBitmap, Integer.valueOf(round), Integer.valueOf(round2));
        return createBitmap;
    }

    public static Drawable b(Context context, int i, int i2) {
        return a(context, i, i2, 0, 17170443);
    }

    private static Field b() {
        try {
            Field declaredField = AbsSeekBar.class.getDeclaredField("mThumb");
            if (declaredField.getType() != Drawable.class) {
                return null;
            }
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Exception unused) {
            f3842a.w("Failed to get the thumb field of the SeekBar.", new Object[0]);
            return null;
        }
    }

    public static Drawable a(Context context, int i, int i2) {
        return a(context, i, i2, 16842800, 0);
    }

    private static Drawable a(Context context, int i, int i2, int i3, int i4) {
        ColorStateList colorStateList;
        int i5;
        Drawable i6 = DrawableCompat.i(context.getResources().getDrawable(i2).mutate());
        DrawableCompat.a(i6, PorterDuff.Mode.SRC_IN);
        if (i != 0) {
            colorStateList = ContextCompat.b(context, i);
        } else {
            if (i3 != 0) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[]{i3});
                i5 = obtainStyledAttributes.getColor(0, 0);
                obtainStyledAttributes.recycle();
            } else {
                i5 = ContextCompat.a(context, i4);
            }
            int[] iArr = {i5, ColorUtils.d(i5, 128)};
            colorStateList = new ColorStateList(new int[][]{new int[]{16842910}, new int[]{-16842910}}, iArr);
        }
        DrawableCompat.a(i6, colorStateList);
        return i6;
    }

    @TargetApi(16)
    public static Drawable a(SeekBar seekBar) {
        if (PlatformVersion.c()) {
            return seekBar.getThumb();
        }
        try {
            Field b = b();
            if (b != null) {
                return (Drawable) b.get(seekBar);
            }
            return null;
        } catch (Exception unused) {
            f3842a.w("Failed to get the thumb of the SeekBar. The SeekBar might look incorrect", new Object[0]);
            return null;
        }
    }

    public static boolean a() {
        if (!PlatformVersion.c() && b() == null) {
            return false;
        }
        return true;
    }
}
