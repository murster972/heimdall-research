package com.google.android.gms.cast.framework.media;

import android.annotation.TargetApi;
import android.net.Uri;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.common.util.PlatformVersion;
import java.util.Locale;

public class MediaUtils {
    private MediaUtils() {
    }

    public static Uri a(MediaInfo mediaInfo, int i) {
        MediaMetadata y;
        if (mediaInfo == null || (y = mediaInfo.y()) == null || y.t() == null || y.t().size() <= i) {
            return null;
        }
        return y.t().get(i).t();
    }

    @TargetApi(21)
    public static Locale a(MediaTrack mediaTrack) {
        if (mediaTrack.v() == null) {
            return null;
        }
        if (PlatformVersion.h()) {
            return Locale.forLanguageTag(mediaTrack.v());
        }
        String[] split = mediaTrack.v().split("-");
        if (split.length == 1) {
            return new Locale(split[0]);
        }
        return new Locale(split[0], split[1]);
    }
}
