package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public class NotificationAction extends AbstractSafeParcelable {
    public static final Parcelable.Creator<NotificationAction> CREATOR = new zzp();

    /* renamed from: a  reason: collision with root package name */
    private final String f3814a;
    private final int b;
    private final String c;

    public static final class Builder {
        public final Builder a(String str) {
            if (!TextUtils.isEmpty(str)) {
                return this;
            }
            throw new IllegalArgumentException("action cannot be null or an empty string.");
        }
    }

    NotificationAction(String str, int i, String str2) {
        this.f3814a = str;
        this.b = i;
        this.c = str2;
    }

    public String s() {
        return this.f3814a;
    }

    public String t() {
        return this.c;
    }

    public int u() {
        return this.b;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, s(), false);
        SafeParcelWriter.a(parcel, 3, u());
        SafeParcelWriter.a(parcel, 4, t(), false);
        SafeParcelWriter.a(parcel, a2);
    }
}
