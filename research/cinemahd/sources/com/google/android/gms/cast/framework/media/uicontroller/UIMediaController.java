package com.google.android.gms.cast.framework.media.uicontroller;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.TracksChooserDialogFragment;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.cast.zzat;
import com.google.android.gms.internal.cast.zzau;
import com.google.android.gms.internal.cast.zzax;
import com.google.android.gms.internal.cast.zzaz;
import com.google.android.gms.internal.cast.zzba;
import com.google.android.gms.internal.cast.zzbc;
import com.google.android.gms.internal.cast.zzbd;
import com.google.android.gms.internal.cast.zzbf;
import com.google.android.gms.internal.cast.zzbg;
import com.google.android.gms.internal.cast.zzbh;
import com.google.android.gms.internal.cast.zzbk;
import com.google.android.gms.internal.cast.zzbl;
import com.google.android.gms.internal.cast.zzbn;
import com.google.android.gms.internal.cast.zzbo;
import com.google.android.gms.internal.cast.zzbp;
import com.google.android.gms.internal.cast.zzbq;
import com.google.android.gms.internal.cast.zzbt;
import com.google.android.gms.internal.cast.zzbv;
import com.google.android.gms.internal.cast.zzdw;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class UIMediaController implements SessionManagerListener<CastSession>, RemoteMediaClient.Listener {
    private static final zzdw h = new zzdw("UIMediaController");

    /* renamed from: a  reason: collision with root package name */
    private final Activity f3824a;
    private final SessionManager b;
    private final Map<View, List<UIController>> c = new HashMap();
    private final Set<zzbt> d = new HashSet();
    zzbh e = new zzbh();
    private RemoteMediaClient.Listener f;
    private RemoteMediaClient g;

    public UIMediaController(Activity activity) {
        this.f3824a = activity;
        CastContext b2 = CastContext.b((Context) activity);
        this.b = b2 != null ? b2.c() : null;
        if (this.b != null) {
            SessionManager c2 = CastContext.a((Context) activity).c();
            c2.a(this, CastSession.class);
            a((Session) c2.a());
        }
    }

    /* renamed from: a */
    public void onSessionEnding(CastSession castSession) {
    }

    /* renamed from: a */
    public void onSessionResuming(CastSession castSession, String str) {
    }

    public void a(RemoteMediaClient.Listener listener) {
        Preconditions.a("Must be called from the main thread.");
        this.f = listener;
    }

    public RemoteMediaClient b() {
        Preconditions.a("Must be called from the main thread.");
        return this.g;
    }

    /* renamed from: b */
    public void onSessionStarting(CastSession castSession) {
    }

    public boolean c() {
        Preconditions.a("Must be called from the main thread.");
        return this.g != null;
    }

    /* access modifiers changed from: protected */
    public void d(View view) {
        RemoteMediaClient b2 = b();
        if (b2 != null && b2.k() && (this.f3824a instanceof FragmentActivity)) {
            TracksChooserDialogFragment e2 = TracksChooserDialogFragment.e();
            FragmentActivity fragmentActivity = (FragmentActivity) this.f3824a;
            FragmentTransaction b3 = fragmentActivity.getSupportFragmentManager().b();
            Fragment b4 = fragmentActivity.getSupportFragmentManager().b("TRACKS_CHOOSER_DIALOG_TAG");
            if (b4 != null) {
                b3.a(b4);
            }
            e2.show(b3, "TRACKS_CHOOSER_DIALOG_TAG");
        }
    }

    /* renamed from: d */
    public void onSessionSuspended(CastSession castSession, int i) {
    }

    /* access modifiers changed from: protected */
    public void e(View view) {
        CastMediaOptions s = CastContext.a((Context) this.f3824a).a().s();
        if (s != null && !TextUtils.isEmpty(s.s())) {
            ComponentName componentName = new ComponentName(this.f3824a.getApplicationContext(), s.s());
            Intent intent = new Intent();
            intent.setComponent(componentName);
            this.f3824a.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void f(View view) {
        RemoteMediaClient b2 = b();
        if (b2 != null && b2.k()) {
            b2.c((JSONObject) null);
        }
    }

    /* access modifiers changed from: protected */
    public void g(View view) {
        RemoteMediaClient b2 = b();
        if (b2 != null && b2.k()) {
            b2.d((JSONObject) null);
        }
    }

    public void onAdBreakStatusUpdated() {
        f();
        RemoteMediaClient.Listener listener = this.f;
        if (listener != null) {
            listener.onAdBreakStatusUpdated();
        }
    }

    public void onMetadataUpdated() {
        f();
        RemoteMediaClient.Listener listener = this.f;
        if (listener != null) {
            listener.onMetadataUpdated();
        }
    }

    public void onPreloadStatusUpdated() {
        f();
        RemoteMediaClient.Listener listener = this.f;
        if (listener != null) {
            listener.onPreloadStatusUpdated();
        }
    }

    public void onQueueStatusUpdated() {
        f();
        RemoteMediaClient.Listener listener = this.f;
        if (listener != null) {
            listener.onQueueStatusUpdated();
        }
    }

    public void onSendingRemoteMediaRequest() {
        for (List<UIController> it2 : this.c.values()) {
            for (UIController onSendingRemoteMediaRequest : it2) {
                onSendingRemoteMediaRequest.onSendingRemoteMediaRequest();
            }
        }
        RemoteMediaClient.Listener listener = this.f;
        if (listener != null) {
            listener.onSendingRemoteMediaRequest();
        }
    }

    public void onStatusUpdated() {
        f();
        RemoteMediaClient.Listener listener = this.f;
        if (listener != null) {
            listener.onStatusUpdated();
        }
    }

    public void a() {
        Preconditions.a("Must be called from the main thread.");
        e();
        this.c.clear();
        SessionManager sessionManager = this.b;
        if (sessionManager != null) {
            sessionManager.b(this, CastSession.class);
        }
        this.f = null;
    }

    public void b(View view, int i) {
        Preconditions.a("Must be called from the main thread.");
        view.setOnClickListener(new zzc(this));
        b(view, (UIController) new zzbp(view, i));
    }

    public void c(View view) {
        Preconditions.a("Must be called from the main thread.");
        b(view, (UIController) new zzba(view));
    }

    private final void f() {
        for (List<UIController> it2 : this.c.values()) {
            for (UIController onMediaStatusUpdated : it2) {
                onMediaStatusUpdated.onMediaStatusUpdated();
            }
        }
    }

    public void c(View view, int i) {
        Preconditions.a("Must be called from the main thread.");
        b(view, (UIController) new zzbv(view, i));
    }

    public void b(View view, long j) {
        Preconditions.a("Must be called from the main thread.");
        view.setOnClickListener(new zze(this, j));
        b(view, (UIController) new zzbk(view, this.e));
    }

    /* access modifiers changed from: protected */
    public void c(ImageView imageView) {
        RemoteMediaClient b2 = b();
        if (b2 != null && b2.k()) {
            b2.v();
        }
    }

    public void a(ImageView imageView, Drawable drawable, Drawable drawable2, Drawable drawable3, View view, boolean z) {
        Preconditions.a("Must be called from the main thread.");
        imageView.setOnClickListener(new zza(this));
        b((View) imageView, (UIController) new zzbf(imageView, this.f3824a, drawable, drawable2, drawable3, view, z));
    }

    public void b(View view) {
        Preconditions.a("Must be called from the main thread.");
        view.setOnClickListener(new zzg(this));
        b(view, (UIController) new zzaz(view));
    }

    private final void e() {
        if (c()) {
            this.e.onSessionEnded();
            for (List<UIController> it2 : this.c.values()) {
                for (UIController onSessionEnded : it2) {
                    onSessionEnded.onSessionEnded();
                }
            }
            this.g.b((RemoteMediaClient.Listener) this);
            this.g = null;
        }
    }

    /* access modifiers changed from: protected */
    public void c(View view, long j) {
        RemoteMediaClient b2 = b();
        if (b2 != null && b2.k()) {
            if (this.e.zzdo()) {
                long b3 = b2.b() + j;
                zzbh zzbh = this.e;
                b2.a(Math.min(b3, ((long) zzbh.zzds()) + zzbh.zzdt()));
                return;
            }
            b2.a(b2.b() + j);
        }
    }

    /* access modifiers changed from: protected */
    public void d(View view, long j) {
        RemoteMediaClient b2 = b();
        if (b2 != null && b2.k()) {
            if (this.e.zzdo()) {
                long b3 = b2.b() - j;
                zzbh zzbh = this.e;
                b2.a(Math.max(b3, ((long) zzbh.zzdr()) + zzbh.zzdt()));
                return;
            }
            b2.a(b2.b() - j);
        }
    }

    public void a(View view, int i) {
        Preconditions.a("Must be called from the main thread.");
        view.setOnClickListener(new zzb(this));
        b(view, (UIController) new zzbo(view, i));
    }

    /* access modifiers changed from: protected */
    public void b(ImageView imageView) {
        CastSession a2 = CastContext.a(this.f3824a.getApplicationContext()).c().a();
        if (a2 != null && a2.b()) {
            try {
                a2.b(!a2.j());
            } catch (IOException | IllegalArgumentException e2) {
                h.e("Unable to call CastSession.setMute(boolean).", e2);
            }
        }
    }

    public void a(View view, long j) {
        Preconditions.a("Must be called from the main thread.");
        view.setOnClickListener(new zzd(this, j));
        b(view, (UIController) new zzau(view, this.e));
    }

    public void a(ProgressBar progressBar) {
        a(progressBar, 1000);
    }

    public void a(ProgressBar progressBar, long j) {
        Preconditions.a("Must be called from the main thread.");
        b((View) progressBar, (UIController) new zzbg(progressBar, j));
    }

    /* renamed from: c */
    public void onSessionStartFailed(CastSession castSession, int i) {
        e();
    }

    public final zzbh d() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void b(SeekBar seekBar) {
        if (this.c.containsKey(seekBar)) {
            for (UIController uIController : this.c.get(seekBar)) {
                if (uIController instanceof zzbl) {
                    ((zzbl) uIController).zzk(true);
                }
            }
        }
        for (zzbt zzk : this.d) {
            zzk.zzk(true);
        }
        RemoteMediaClient b2 = b();
        if (b2 != null && b2.k()) {
            b2.a(((long) seekBar.getProgress()) + this.e.zzdt());
        }
    }

    public final void a(SeekBar seekBar, long j, zzbn zzbn) {
        Preconditions.a("Must be called from the main thread.");
        seekBar.setOnSeekBarChangeListener(new zzf(this, zzbn, seekBar));
        b((View) seekBar, (UIController) new zzbl(seekBar, j, this.e, zzbn));
    }

    public void a(View view) {
        Preconditions.a("Must be called from the main thread.");
        view.setOnClickListener(new zzh(this));
        b(view, (UIController) new zzat(view, this.f3824a));
    }

    public void a(ImageView imageView) {
        Preconditions.a("Must be called from the main thread.");
        imageView.setOnClickListener(new zzi(this));
        b((View) imageView, (UIController) new zzbd(imageView, this.f3824a));
    }

    public void a(TextView textView, String str) {
        Preconditions.a("Must be called from the main thread.");
        a(textView, (List<String>) Collections.singletonList(str));
    }

    /* renamed from: b */
    public void onSessionStarted(CastSession castSession, String str) {
        a((Session) castSession);
    }

    public void a(TextView textView, List<String> list) {
        Preconditions.a("Must be called from the main thread.");
        b((View) textView, (UIController) new zzbc(textView, list));
    }

    /* renamed from: b */
    public void onSessionResumeFailed(CastSession castSession, int i) {
        e();
    }

    private final void b(View view, UIController uIController) {
        if (this.b != null) {
            List list = this.c.get(view);
            if (list == null) {
                list = new ArrayList();
                this.c.put(view, list);
            }
            list.add(uIController);
            if (c()) {
                uIController.onSessionConnected(this.b.a());
                f();
            }
        }
    }

    public void a(ImageView imageView, ImageHints imageHints, int i) {
        Preconditions.a("Must be called from the main thread.");
        b((View) imageView, (UIController) new zzax(imageView, this.f3824a, imageHints, i, (View) null));
    }

    public void a(ImageView imageView, ImageHints imageHints, View view) {
        Preconditions.a("Must be called from the main thread.");
        b((View) imageView, (UIController) new zzax(imageView, this.f3824a, imageHints, 0, view));
    }

    public void a(View view, UIController uIController) {
        Preconditions.a("Must be called from the main thread.");
        b(view, uIController);
    }

    public void a(TextView textView) {
        Preconditions.a("Must be called from the main thread.");
        b((View) textView, (UIController) new zzbq(textView));
    }

    public final void a(zzbt zzbt) {
        this.d.add(zzbt);
    }

    /* access modifiers changed from: protected */
    public void a(SeekBar seekBar) {
        if (this.c.containsKey(seekBar)) {
            for (UIController uIController : this.c.get(seekBar)) {
                if (uIController instanceof zzbl) {
                    ((zzbl) uIController).zzk(false);
                }
            }
        }
        for (zzbt zzk : this.d) {
            zzk.zzk(false);
        }
    }

    /* access modifiers changed from: protected */
    public void a(SeekBar seekBar, int i, boolean z) {
        if (z) {
            for (zzbt zzg : this.d) {
                zzg.zzg(((long) i) + this.e.zzdt());
            }
        }
    }

    /* renamed from: a */
    public void onSessionResumed(CastSession castSession, boolean z) {
        a((Session) castSession);
    }

    /* renamed from: a */
    public void onSessionEnded(CastSession castSession, int i) {
        e();
    }

    private final void a(Session session) {
        if (!c() && (session instanceof CastSession) && session.b()) {
            CastSession castSession = (CastSession) session;
            this.g = castSession.h();
            RemoteMediaClient remoteMediaClient = this.g;
            if (remoteMediaClient != null) {
                remoteMediaClient.a((RemoteMediaClient.Listener) this);
                this.e.onSessionConnected(castSession);
                for (List<UIController> it2 : this.c.values()) {
                    for (UIController onSessionConnected : it2) {
                        onSessionConnected.onSessionConnected(castSession);
                    }
                }
                f();
            }
        }
    }
}
