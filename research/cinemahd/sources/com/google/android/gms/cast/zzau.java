package com.google.android.gms.cast;

import org.json.JSONObject;

public final class zzau {

    /* renamed from: a  reason: collision with root package name */
    private long f3865a;
    private int b = 0;
    private JSONObject c;

    public final zzau a(long j) {
        this.f3865a = j;
        return this;
    }

    public final zzau a(int i) {
        this.b = i;
        return this;
    }

    public final zzau a(JSONObject jSONObject) {
        this.c = jSONObject;
        return this;
    }

    public final zzas a() {
        return new zzas(this.f3865a, this.b, false, this.c, (zzat) null);
    }
}
