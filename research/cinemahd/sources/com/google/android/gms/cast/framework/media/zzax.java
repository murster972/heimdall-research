package com.google.android.gms.cast.framework.media;

import android.util.Log;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.cast.zzec;
import org.json.JSONObject;

final class zzax implements zzec {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ RemoteMediaClient.zzc f3845a;

    zzax(RemoteMediaClient.zzc zzc, RemoteMediaClient remoteMediaClient) {
        this.f3845a = zzc;
    }

    public final void zza(long j, int i, Object obj) {
        try {
            this.f3845a.setResult(new RemoteMediaClient.zzd(new Status(i), obj instanceof JSONObject ? (JSONObject) obj : null));
        } catch (IllegalStateException e) {
            Log.e("RemoteMediaClient", "Result already set when calling onRequestCompleted", e);
        }
    }

    public final void zzd(long j) {
        try {
            this.f3845a.setResult((RemoteMediaClient.MediaChannelResult) this.f3845a.createFailedResult(new Status(2103)));
        } catch (IllegalStateException e) {
            Log.e("RemoteMediaClient", "Result already set when calling onRequestReplaced", e);
        }
    }
}
