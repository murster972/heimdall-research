package com.google.android.gms.cast;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ApplicationMetadata extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ApplicationMetadata> CREATOR = new zzd();

    /* renamed from: a  reason: collision with root package name */
    private String f3761a;
    private String b;
    private List<String> c;
    private String d;
    private Uri e;
    private String f;

    ApplicationMetadata(String str, String str2, List<WebImage> list, List<String> list2, String str3, Uri uri, String str4) {
        this.f3761a = str;
        this.b = str2;
        this.c = list2;
        this.d = str3;
        this.e = uri;
        this.f = str4;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ApplicationMetadata)) {
            return false;
        }
        ApplicationMetadata applicationMetadata = (ApplicationMetadata) obj;
        return zzdk.zza(this.f3761a, applicationMetadata.f3761a) && zzdk.zza(this.b, applicationMetadata.b) && zzdk.zza(this.c, applicationMetadata.c) && zzdk.zza(this.d, applicationMetadata.d) && zzdk.zza(this.e, applicationMetadata.e) && zzdk.zza(this.f, applicationMetadata.f);
    }

    public int hashCode() {
        return Objects.a(this.f3761a, this.b, this.c, this.d, this.e, this.f);
    }

    public String s() {
        return this.f3761a;
    }

    public List<WebImage> t() {
        return null;
    }

    public String toString() {
        String str = this.f3761a;
        String str2 = this.b;
        List<String> list = this.c;
        int size = list == null ? 0 : list.size();
        String str3 = this.d;
        String valueOf = String.valueOf(this.e);
        String str4 = this.f;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 110 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(valueOf).length() + String.valueOf(str4).length());
        sb.append("applicationId: ");
        sb.append(str);
        sb.append(", name: ");
        sb.append(str2);
        sb.append(", namespaces.count: ");
        sb.append(size);
        sb.append(", senderAppIdentifier: ");
        sb.append(str3);
        sb.append(", senderAppLaunchUrl: ");
        sb.append(valueOf);
        sb.append(", iconUrl: ");
        sb.append(str4);
        return sb.toString();
    }

    public String u() {
        return this.b;
    }

    public String v() {
        return this.d;
    }

    public List<String> w() {
        return Collections.unmodifiableList(this.c);
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, s(), false);
        SafeParcelWriter.a(parcel, 3, u(), false);
        SafeParcelWriter.c(parcel, 4, t(), false);
        SafeParcelWriter.b(parcel, 5, w(), false);
        SafeParcelWriter.a(parcel, 6, v(), false);
        SafeParcelWriter.a(parcel, 7, (Parcelable) this.e, i, false);
        SafeParcelWriter.a(parcel, 8, this.f, false);
        SafeParcelWriter.a(parcel, a2);
    }

    private ApplicationMetadata() {
        this.c = new ArrayList();
    }
}
