package com.google.android.gms.cast.framework;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CastOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<CastOptions> CREATOR = new zzb();

    /* renamed from: a  reason: collision with root package name */
    private String f3784a;
    private final List<String> b;
    private final boolean c;
    private final LaunchOptions d;
    private final boolean e;
    private final CastMediaOptions f;
    private final boolean g;
    private final double h;
    private final boolean i;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private String f3785a;
        private List<String> b = new ArrayList();
        private boolean c;
        private LaunchOptions d = new LaunchOptions();
        private boolean e = true;
        private CastMediaOptions f = new CastMediaOptions.Builder().a();
        private boolean g = true;
        private double h = 0.05000000074505806d;

        public final Builder a(String str) {
            this.f3785a = str;
            return this;
        }

        public final Builder b(boolean z) {
            this.e = z;
            return this;
        }

        public final Builder a(LaunchOptions launchOptions) {
            this.d = launchOptions;
            return this;
        }

        public final Builder a(CastMediaOptions castMediaOptions) {
            this.f = castMediaOptions;
            return this;
        }

        public final Builder a(boolean z) {
            this.g = z;
            return this;
        }

        public final Builder a(double d2) throws IllegalArgumentException {
            if (d2 <= 0.0d || d2 > 0.5d) {
                throw new IllegalArgumentException("volumeDelta must be greater than 0 and less or equal to 0.5");
            }
            this.h = d2;
            return this;
        }

        public final CastOptions a() {
            return new CastOptions(this.f3785a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, false);
        }
    }

    CastOptions(String str, List<String> list, boolean z, LaunchOptions launchOptions, boolean z2, CastMediaOptions castMediaOptions, boolean z3, double d2, boolean z4) {
        int i2;
        this.f3784a = TextUtils.isEmpty(str) ? "" : str;
        if (list == null) {
            i2 = 0;
        } else {
            i2 = list.size();
        }
        this.b = new ArrayList(i2);
        if (i2 > 0) {
            this.b.addAll(list);
        }
        this.c = z;
        this.d = launchOptions == null ? new LaunchOptions() : launchOptions;
        this.e = z2;
        this.f = castMediaOptions;
        this.g = z3;
        this.h = d2;
        this.i = z4;
    }

    public CastMediaOptions s() {
        return this.f;
    }

    public boolean t() {
        return this.g;
    }

    public LaunchOptions u() {
        return this.d;
    }

    public String v() {
        return this.f3784a;
    }

    public boolean w() {
        return this.e;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, v(), false);
        SafeParcelWriter.b(parcel, 3, y(), false);
        SafeParcelWriter.a(parcel, 4, x());
        SafeParcelWriter.a(parcel, 5, (Parcelable) u(), i2, false);
        SafeParcelWriter.a(parcel, 6, w());
        SafeParcelWriter.a(parcel, 7, (Parcelable) s(), i2, false);
        SafeParcelWriter.a(parcel, 8, t());
        SafeParcelWriter.a(parcel, 9, z());
        SafeParcelWriter.a(parcel, 10, this.i);
        SafeParcelWriter.a(parcel, a2);
    }

    public boolean x() {
        return this.c;
    }

    public List<String> y() {
        return Collections.unmodifiableList(this.b);
    }

    public double z() {
        return this.h;
    }
}
