package com.google.android.gms.cast.framework.media.uicontroller;

import android.view.View;

final class zze implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ long f3829a;
    private final /* synthetic */ UIMediaController b;

    zze(UIMediaController uIMediaController, long j) {
        this.b = uIMediaController;
        this.f3829a = j;
    }

    public final void onClick(View view) {
        this.b.d(view, this.f3829a);
    }
}
