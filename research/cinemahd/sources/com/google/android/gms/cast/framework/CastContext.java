package com.google.android.gms.cast.framework;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.KeyEvent;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.cast.zzcx;
import com.google.android.gms.internal.cast.zzdw;
import com.google.android.gms.internal.cast.zze;
import com.google.android.gms.internal.cast.zzf;
import com.google.android.gms.internal.cast.zzj;
import com.google.android.gms.internal.cast.zzw;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CastContext {
    private static final zzdw i = new zzdw("CastContext");
    private static CastContext j;

    /* renamed from: a  reason: collision with root package name */
    private final Context f3783a;
    private final zzj b;
    private final SessionManager c;
    private final zze d;
    private final CastOptions e;
    private zzw f = new zzw(MediaRouter.a(this.f3783a));
    private zzf g;
    private final List<SessionProvider> h;

    private CastContext(Context context, CastOptions castOptions, List<SessionProvider> list) {
        zzp zzp;
        zze zze;
        zzv zzv;
        this.f3783a = context.getApplicationContext();
        this.e = castOptions;
        this.h = list;
        i();
        this.b = zze.zza(this.f3783a, castOptions, (zzj) this.f, h());
        SessionManager sessionManager = null;
        try {
            zzp = this.b.t();
        } catch (RemoteException e2) {
            i.zza(e2, "Unable to call %s on %s.", "getDiscoveryManagerImpl", zzj.class.getSimpleName());
            zzp = null;
        }
        if (zzp == null) {
            zze = null;
        } else {
            zze = new zze(zzp);
        }
        this.d = zze;
        try {
            zzv = this.b.s();
        } catch (RemoteException e3) {
            i.zza(e3, "Unable to call %s on %s.", "getSessionManagerImpl", zzj.class.getSimpleName());
            zzv = null;
        }
        this.c = zzv != null ? new SessionManager(zzv, this.f3783a) : sessionManager;
        new MediaNotificationManager(this.c);
        SessionManager sessionManager2 = this.c;
        if (sessionManager2 != null) {
            new PrecacheManager(this.e, sessionManager2, new zzcx(this.f3783a));
        }
    }

    public static CastContext a(Context context) throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        if (j == null) {
            OptionsProvider c2 = c(context.getApplicationContext());
            j = new CastContext(context, c2.getCastOptions(context.getApplicationContext()), c2.getAdditionalSessionProviders(context.getApplicationContext()));
        }
        return j;
    }

    public static CastContext b(Context context) throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        try {
            return a(context);
        } catch (RuntimeException e2) {
            i.e("Failed to load module from Google Play services. Cast will not work properly. Might due to outdated Google Play services. Ignoring this failure silently.", e2);
            return null;
        }
    }

    private static OptionsProvider c(Context context) throws IllegalStateException {
        try {
            Bundle bundle = Wrappers.a(context).a(context.getPackageName(), 128).metaData;
            if (bundle == null) {
                i.e("Bundle is null", new Object[0]);
            }
            String string = bundle.getString("com.google.android.gms.cast.framework.OPTIONS_PROVIDER_CLASS_NAME");
            if (string != null) {
                return (OptionsProvider) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            }
            throw new IllegalStateException("The fully qualified name of the implementation of OptionsProvider must be provided as a metadata in the AndroidManifest.xml with key com.google.android.gms.cast.framework.OPTIONS_PROVIDER_CLASS_NAME.");
        } catch (PackageManager.NameNotFoundException | ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | NullPointerException | InvocationTargetException e2) {
            throw new IllegalStateException("Failed to initialize CastContext.", e2);
        }
    }

    public static CastContext g() {
        Preconditions.a("Must be called from the main thread.");
        return j;
    }

    private final Map<String, IBinder> h() {
        HashMap hashMap = new HashMap();
        zzf zzf = this.g;
        if (zzf != null) {
            hashMap.put(zzf.getCategory(), this.g.zzaq());
        }
        List<SessionProvider> list = this.h;
        if (list != null) {
            for (SessionProvider next : list) {
                Preconditions.a(next, (Object) "Additional SessionProvider must not be null.");
                String category = next.getCategory();
                Preconditions.a(category, (Object) "Category for SessionProvider must not be null or empty string.");
                Preconditions.a(!hashMap.containsKey(category), (Object) String.format("SessionProvider for category %s already added", new Object[]{category}));
                hashMap.put(category, next.zzaq());
            }
        }
        return hashMap;
    }

    private final void i() {
        if (!TextUtils.isEmpty(this.e.v())) {
            this.g = new zzf(this.f3783a, this.e, this.f);
        } else {
            this.g = null;
        }
    }

    public boolean d() throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        try {
            return this.b.u();
        } catch (RemoteException e2) {
            i.zza(e2, "Unable to call %s on %s.", "isApplicationVisible", zzj.class.getSimpleName());
            return false;
        }
    }

    public final boolean e() {
        Preconditions.a("Must be called from the main thread.");
        try {
            return this.b.i();
        } catch (RemoteException e2) {
            i.zza(e2, "Unable to call %s on %s.", "hasActivityInRecents", zzj.class.getSimpleName());
            return false;
        }
    }

    public final zze f() {
        Preconditions.a("Must be called from the main thread.");
        return this.d;
    }

    public MediaRouteSelector b() throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        try {
            return MediaRouteSelector.a(this.b.v());
        } catch (RemoteException e2) {
            i.zza(e2, "Unable to call %s on %s.", "getMergedSelectorAsBundle", zzj.class.getSimpleName());
            return null;
        }
    }

    public CastOptions a() throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        return this.e;
    }

    @Deprecated
    public void b(AppVisibilityListener appVisibilityListener) throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        if (appVisibilityListener != null) {
            try {
                this.b.b(new zza(appVisibilityListener));
            } catch (RemoteException e2) {
                i.zza(e2, "Unable to call %s on %s.", "addVisibilityChangeListener", zzj.class.getSimpleName());
            }
        }
    }

    public SessionManager c() throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        return this.c;
    }

    @Deprecated
    public void a(AppVisibilityListener appVisibilityListener) throws IllegalStateException, NullPointerException {
        Preconditions.a("Must be called from the main thread.");
        Preconditions.a(appVisibilityListener);
        try {
            this.b.a(new zza(appVisibilityListener));
        } catch (RemoteException e2) {
            i.zza(e2, "Unable to call %s on %s.", "addVisibilityChangeListener", zzj.class.getSimpleName());
        }
    }

    public void b(CastStateListener castStateListener) throws IllegalStateException {
        Preconditions.a("Must be called from the main thread.");
        if (castStateListener != null) {
            this.c.b(castStateListener);
        }
    }

    public void a(CastStateListener castStateListener) throws IllegalStateException, NullPointerException {
        Preconditions.a("Must be called from the main thread.");
        Preconditions.a(castStateListener);
        this.c.a(castStateListener);
    }

    public boolean a(KeyEvent keyEvent) {
        CastSession a2;
        Preconditions.a("Must be called from the main thread.");
        if (PlatformVersion.c() || (a2 = this.c.a()) == null || !a2.b()) {
            return false;
        }
        double z = a().z();
        boolean z2 = keyEvent.getAction() == 0;
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 24) {
            a(a2, z, z2);
            return true;
        } else if (keyCode != 25) {
            return false;
        } else {
            a(a2, -z, z2);
            return true;
        }
    }

    private static boolean a(CastSession castSession, double d2, boolean z) {
        if (z) {
            try {
                double i2 = castSession.i() + d2;
                double d3 = 1.0d;
                if (i2 <= 1.0d) {
                    d3 = i2;
                }
                castSession.a(d3);
            } catch (IOException | IllegalStateException e2) {
                i.e("Unable to call CastSession.setVolume(double).", e2);
            }
        }
        return true;
    }
}
