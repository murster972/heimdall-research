package com.google.android.gms.cast;

import org.json.JSONObject;

public final class zzas {

    /* renamed from: a  reason: collision with root package name */
    private final long f3864a;
    private final int b;
    private final boolean c;
    private final JSONObject d;

    private zzas(long j, int i, boolean z, JSONObject jSONObject) {
        this.f3864a = j;
        this.b = i;
        this.c = z;
        this.d = jSONObject;
    }

    public final JSONObject a() {
        return this.d;
    }

    public final long b() {
        return this.f3864a;
    }

    public final int c() {
        return this.b;
    }

    public final boolean d() {
        return this.c;
    }

    /* synthetic */ zzas(long j, int i, boolean z, JSONObject jSONObject, zzat zzat) {
        this(j, i, false, jSONObject);
    }
}
