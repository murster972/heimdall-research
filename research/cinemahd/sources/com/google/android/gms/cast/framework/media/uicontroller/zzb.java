package com.google.android.gms.cast.framework.media.uicontroller;

import android.view.View;

final class zzb implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ UIMediaController f3826a;

    zzb(UIMediaController uIMediaController) {
        this.f3826a = uIMediaController;
    }

    public final void onClick(View view) {
        this.f3826a.f(view);
    }
}
