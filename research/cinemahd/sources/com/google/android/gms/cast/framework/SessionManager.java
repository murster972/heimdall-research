package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.cast.zzdw;

public class SessionManager {
    private static final zzdw b = new zzdw("SessionManager");

    /* renamed from: a  reason: collision with root package name */
    private final zzv f3794a;

    public SessionManager(zzv zzv, Context context) {
        this.f3794a = zzv;
    }

    public CastSession a() {
        Preconditions.a("Must be called from the main thread.");
        Session b2 = b();
        if (b2 == null || !(b2 instanceof CastSession)) {
            return null;
        }
        return (CastSession) b2;
    }

    public Session b() {
        Preconditions.a("Must be called from the main thread.");
        try {
            return (Session) ObjectWrapper.a(this.f3794a.g());
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "getWrappedCurrentSession", zzv.class.getSimpleName());
            return null;
        }
    }

    public final IObjectWrapper c() {
        try {
            return this.f3794a.zzae();
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "getWrappedThis", zzv.class.getSimpleName());
            return null;
        }
    }

    public void a(boolean z) {
        Preconditions.a("Must be called from the main thread.");
        try {
            this.f3794a.zza(true, z);
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "endCurrentSession", zzv.class.getSimpleName());
        }
    }

    public <T extends Session> void b(SessionManagerListener<T> sessionManagerListener, Class cls) {
        Preconditions.a(cls);
        Preconditions.a("Must be called from the main thread.");
        if (sessionManagerListener != null) {
            try {
                this.f3794a.b((zzx) new zzaf(sessionManagerListener, cls));
            } catch (RemoteException e) {
                b.zza(e, "Unable to call %s on %s.", "removeSessionManagerListener", zzv.class.getSimpleName());
            }
        }
    }

    public <T extends Session> void a(SessionManagerListener<T> sessionManagerListener, Class<T> cls) throws NullPointerException {
        Preconditions.a(sessionManagerListener);
        Preconditions.a(cls);
        Preconditions.a("Must be called from the main thread.");
        try {
            this.f3794a.a((zzx) new zzaf(sessionManagerListener, cls));
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "addSessionManagerListener", zzv.class.getSimpleName());
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(CastStateListener castStateListener) {
        if (castStateListener != null) {
            try {
                this.f3794a.b((zzn) new zzd(castStateListener));
            } catch (RemoteException e) {
                b.zza(e, "Unable to call %s on %s.", "removeCastStateListener", zzv.class.getSimpleName());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(CastStateListener castStateListener) throws NullPointerException {
        Preconditions.a(castStateListener);
        try {
            this.f3794a.a((zzn) new zzd(castStateListener));
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "addCastStateListener", zzv.class.getSimpleName());
        }
    }
}
