package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzea;
import org.json.JSONObject;

final class zzaf extends RemoteMediaClient.zzc {
    private final /* synthetic */ JSONObject d;
    private final /* synthetic */ RemoteMediaClient e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaf(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, JSONObject jSONObject) {
        super(remoteMediaClient, googleApiClient);
        this.e = remoteMediaClient;
        this.d = jSONObject;
    }

    /* access modifiers changed from: protected */
    public final void a(zzdd zzdd) throws zzea {
        this.e.c.zza(this.f3820a, 0, -1, (MediaQueueItem[]) null, 1, (Integer) null, this.d);
    }
}
