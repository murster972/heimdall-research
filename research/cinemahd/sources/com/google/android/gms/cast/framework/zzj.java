package com.google.android.gms.cast.framework;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.internal.cast.zzb;

public interface zzj extends IInterface {

    public static abstract class zza extends zzb implements zzj {
        public static zzj a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.ICastContext");
            if (queryLocalInterface instanceof zzj) {
                return (zzj) queryLocalInterface;
            }
            return new zzk(iBinder);
        }
    }

    void a(zzf zzf) throws RemoteException;

    void b(zzf zzf) throws RemoteException;

    boolean i() throws RemoteException;

    zzv s() throws RemoteException;

    zzp t() throws RemoteException;

    boolean u() throws RemoteException;

    Bundle v() throws RemoteException;
}
