package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.cast.framework.R$dimen;
import com.google.android.gms.cast.framework.R$drawable;
import com.google.android.gms.cast.framework.R$string;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class NotificationOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<NotificationOptions> CREATOR = new zzr();
    /* access modifiers changed from: private */
    public static final List<String> G = Arrays.asList(new String[]{"com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK", "com.google.android.gms.cast.framework.action.STOP_CASTING"});
    /* access modifiers changed from: private */
    public static final int[] H = {0, 1};
    private final int A;
    private final int B;
    private final int C;
    private final int D;
    private final int E;
    private final zzd F;

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f3816a;
    private final int[] b;
    private final long c;
    private final String d;
    private final int e;
    private final int f;
    private final int g;
    private final int h;
    private final int i;
    private final int j;
    private final int k;
    private final int l;
    private final int m;
    private final int n;
    private final int o;
    private final int p;
    private final int q;
    private final int r;
    private final int s;
    private final int t;
    private final int u;
    private final int v;
    private final int w;
    private final int x;
    private final int y;
    private final int z;

    /* JADX WARNING: type inference failed for: r1v31, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NotificationOptions(java.util.List<java.lang.String> r7, int[] r8, long r9, java.lang.String r11, int r12, int r13, int r14, int r15, int r16, int r17, int r18, int r19, int r20, int r21, int r22, int r23, int r24, int r25, int r26, int r27, int r28, int r29, int r30, int r31, int r32, int r33, int r34, int r35, int r36, int r37, int r38, android.os.IBinder r39) {
        /*
            r6 = this;
            r0 = r6
            r1 = r7
            r2 = r8
            r3 = r39
            r6.<init>()
            r4 = 0
            if (r1 == 0) goto L_0x0013
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>(r7)
            r0.f3816a = r5
            goto L_0x0015
        L_0x0013:
            r0.f3816a = r4
        L_0x0015:
            if (r2 == 0) goto L_0x001f
            int r1 = r2.length
            int[] r1 = java.util.Arrays.copyOf(r8, r1)
            r0.b = r1
            goto L_0x0021
        L_0x001f:
            r0.b = r4
        L_0x0021:
            r1 = r9
            r0.c = r1
            r1 = r11
            r0.d = r1
            r1 = r12
            r0.e = r1
            r1 = r13
            r0.f = r1
            r1 = r14
            r0.g = r1
            r1 = r15
            r0.h = r1
            r1 = r16
            r0.i = r1
            r1 = r17
            r0.j = r1
            r1 = r18
            r0.k = r1
            r1 = r19
            r0.l = r1
            r1 = r20
            r0.m = r1
            r1 = r21
            r0.n = r1
            r1 = r22
            r0.o = r1
            r1 = r23
            r0.p = r1
            r1 = r24
            r0.q = r1
            r1 = r25
            r0.r = r1
            r1 = r26
            r0.s = r1
            r1 = r27
            r0.t = r1
            r1 = r28
            r0.u = r1
            r1 = r29
            r0.v = r1
            r1 = r30
            r0.w = r1
            r1 = r31
            r0.x = r1
            r1 = r32
            r0.y = r1
            r1 = r33
            r0.z = r1
            r1 = r34
            r0.A = r1
            r1 = r35
            r0.B = r1
            r1 = r36
            r0.C = r1
            r1 = r37
            r0.D = r1
            r1 = r38
            r0.E = r1
            if (r3 != 0) goto L_0x0092
            goto L_0x00a5
        L_0x0092:
            java.lang.String r1 = "com.google.android.gms.cast.framework.media.INotificationActionsProvider"
            android.os.IInterface r1 = r3.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.cast.framework.media.zzd
            if (r2 == 0) goto L_0x00a0
            r4 = r1
            com.google.android.gms.cast.framework.media.zzd r4 = (com.google.android.gms.cast.framework.media.zzd) r4
            goto L_0x00a5
        L_0x00a0:
            com.google.android.gms.cast.framework.media.zze r4 = new com.google.android.gms.cast.framework.media.zze
            r4.<init>(r3)
        L_0x00a5:
            r0.F = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.media.NotificationOptions.<init>(java.util.List, int[], long, java.lang.String, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, android.os.IBinder):void");
    }

    public int A() {
        return this.h;
    }

    public int B() {
        return this.o;
    }

    public int C() {
        return this.p;
    }

    public int D() {
        return this.n;
    }

    public int E() {
        return this.i;
    }

    public int F() {
        return this.j;
    }

    public long G() {
        return this.c;
    }

    public int H() {
        return this.e;
    }

    public int I() {
        return this.f;
    }

    public int J() {
        return this.t;
    }

    public String K() {
        return this.d;
    }

    public final int L() {
        return this.r;
    }

    public final int M() {
        return this.u;
    }

    public final int N() {
        return this.v;
    }

    public final int O() {
        return this.w;
    }

    public final int P() {
        return this.x;
    }

    public final int Q() {
        return this.y;
    }

    public final int R() {
        return this.z;
    }

    public final int S() {
        return this.A;
    }

    public final int T() {
        return this.B;
    }

    public final int U() {
        return this.C;
    }

    public final int V() {
        return this.D;
    }

    public final int W() {
        return this.E;
    }

    public final zzd X() {
        return this.F;
    }

    public List<String> s() {
        return this.f3816a;
    }

    public int t() {
        return this.s;
    }

    public int[] u() {
        int[] iArr = this.b;
        return Arrays.copyOf(iArr, iArr.length);
    }

    public int v() {
        return this.q;
    }

    public int w() {
        return this.l;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.b(parcel, 2, s(), false);
        SafeParcelWriter.a(parcel, 3, u(), false);
        SafeParcelWriter.a(parcel, 4, G());
        SafeParcelWriter.a(parcel, 5, K(), false);
        SafeParcelWriter.a(parcel, 6, H());
        SafeParcelWriter.a(parcel, 7, I());
        SafeParcelWriter.a(parcel, 8, z());
        SafeParcelWriter.a(parcel, 9, A());
        SafeParcelWriter.a(parcel, 10, E());
        SafeParcelWriter.a(parcel, 11, F());
        SafeParcelWriter.a(parcel, 12, y());
        SafeParcelWriter.a(parcel, 13, w());
        SafeParcelWriter.a(parcel, 14, x());
        SafeParcelWriter.a(parcel, 15, D());
        SafeParcelWriter.a(parcel, 16, B());
        SafeParcelWriter.a(parcel, 17, C());
        SafeParcelWriter.a(parcel, 18, v());
        SafeParcelWriter.a(parcel, 19, this.r);
        SafeParcelWriter.a(parcel, 20, t());
        SafeParcelWriter.a(parcel, 21, J());
        SafeParcelWriter.a(parcel, 22, this.u);
        SafeParcelWriter.a(parcel, 23, this.v);
        SafeParcelWriter.a(parcel, 24, this.w);
        SafeParcelWriter.a(parcel, 25, this.x);
        SafeParcelWriter.a(parcel, 26, this.y);
        SafeParcelWriter.a(parcel, 27, this.z);
        SafeParcelWriter.a(parcel, 28, this.A);
        SafeParcelWriter.a(parcel, 29, this.B);
        SafeParcelWriter.a(parcel, 30, this.C);
        SafeParcelWriter.a(parcel, 31, this.D);
        SafeParcelWriter.a(parcel, 32, this.E);
        zzd zzd = this.F;
        SafeParcelWriter.a(parcel, 33, zzd == null ? null : zzd.asBinder(), false);
        SafeParcelWriter.a(parcel, a2);
    }

    public int x() {
        return this.m;
    }

    public int y() {
        return this.k;
    }

    public int z() {
        return this.g;
    }

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private String f3817a;
        private List<String> b = NotificationOptions.G;
        private NotificationActionsProvider c;
        private int[] d = NotificationOptions.H;
        private int e = R$drawable.cast_ic_notification_small_icon;
        private int f = R$drawable.cast_ic_notification_stop_live_stream;
        private int g = R$drawable.cast_ic_notification_pause;
        private int h = R$drawable.cast_ic_notification_play;
        private int i = R$drawable.cast_ic_notification_skip_next;
        private int j = R$drawable.cast_ic_notification_skip_prev;
        private int k = R$drawable.cast_ic_notification_forward;
        private int l = R$drawable.cast_ic_notification_forward10;
        private int m = R$drawable.cast_ic_notification_forward30;
        private int n = R$drawable.cast_ic_notification_rewind;
        private int o = R$drawable.cast_ic_notification_rewind10;
        private int p = R$drawable.cast_ic_notification_rewind30;
        private int q = R$drawable.cast_ic_notification_disconnect;
        private long r = 10000;

        public final Builder a(List<String> list, int[] iArr) {
            if (list == null && iArr != null) {
                throw new IllegalArgumentException("When setting actions to null, you must also set compatActionIndices to null.");
            } else if (list == null || iArr != null) {
                if (list == null || iArr == null) {
                    this.b = NotificationOptions.G;
                    this.d = NotificationOptions.H;
                } else {
                    int size = list.size();
                    if (iArr.length <= size) {
                        for (int i2 : iArr) {
                            if (i2 < 0 || i2 >= size) {
                                throw new IllegalArgumentException(String.format(Locale.ROOT, "Index %d in compatActionIndices out of range: [0, %d]", new Object[]{Integer.valueOf(i2), Integer.valueOf(size - 1)}));
                            }
                        }
                        this.b = new ArrayList(list);
                        this.d = Arrays.copyOf(iArr, iArr.length);
                    } else {
                        throw new IllegalArgumentException(String.format(Locale.ROOT, "Invalid number of compat actions: %d > %d.", new Object[]{Integer.valueOf(iArr.length), Integer.valueOf(size)}));
                    }
                }
                return this;
            } else {
                throw new IllegalArgumentException("When setting compatActionIndices to null, you must also set actions to null.");
            }
        }

        public final Builder a(String str) {
            this.f3817a = str;
            return this;
        }

        public final NotificationOptions a() {
            IBinder iBinder;
            NotificationActionsProvider notificationActionsProvider = this.c;
            if (notificationActionsProvider == null) {
                iBinder = null;
            } else {
                iBinder = notificationActionsProvider.a().asBinder();
            }
            NotificationOptions notificationOptions = r2;
            NotificationOptions notificationOptions2 = new NotificationOptions(this.b, this.d, this.r, this.f3817a, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, R$dimen.cast_notification_image_size, R$string.cast_casting_to_device, R$string.cast_stop_live_stream, R$string.cast_pause, R$string.cast_play, R$string.cast_skip_next, R$string.cast_skip_prev, R$string.cast_forward, R$string.cast_forward_10, R$string.cast_forward_30, R$string.cast_rewind, R$string.cast_rewind_10, R$string.cast_rewind_30, R$string.cast_disconnect, iBinder);
            return notificationOptions;
        }
    }
}
