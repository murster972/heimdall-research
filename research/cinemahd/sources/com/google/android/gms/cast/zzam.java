package com.google.android.gms.cast;

import android.text.TextUtils;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.internal.cast.zzef;
import com.unity3d.ads.metadata.MediationMetaData;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzam {
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty((CharSequence) null)) {
                jSONObject.put("id", (Object) null);
            }
            if (!TextUtils.isEmpty((CharSequence) null)) {
                jSONObject.put("entity", (Object) null);
            }
            if (!TextUtils.isEmpty((CharSequence) null)) {
                jSONObject.put(MediationMetaData.KEY_NAME, (Object) null);
            }
            String zza = zzef.zza(0);
            if (zza != null) {
                jSONObject.put("repeatMode", zza);
            }
            jSONObject.put("startIndex", 0);
            jSONObject.put("startTime", 0.0d);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof zzam) && TextUtils.equals((CharSequence) null, (CharSequence) null) && TextUtils.equals((CharSequence) null, (CharSequence) null) && TextUtils.equals((CharSequence) null, (CharSequence) null) && Objects.a((Object) null, (Object) null) && Objects.a((Object) null, (Object) null);
    }

    public final int hashCode() {
        return Objects.a(null, null, 0, null, null, 0, null, 0, Double.valueOf(0.0d));
    }
}
