package com.google.android.gms.cast.framework.media;

import android.os.Parcelable;

public final class zza implements Parcelable.Creator<CastMediaOptions> {
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r10) {
        /*
            r9 = this;
            int r0 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.b(r10)
            r1 = 0
            r2 = 0
            r4 = r1
            r5 = r4
            r6 = r5
            r7 = r6
            r8 = 0
        L_0x000b:
            int r1 = r10.dataPosition()
            if (r1 >= r0) goto L_0x004a
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r10)
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((int) r1)
            r3 = 2
            if (r2 == r3) goto L_0x0045
            r3 = 3
            if (r2 == r3) goto L_0x0040
            r3 = 4
            if (r2 == r3) goto L_0x003b
            r3 = 5
            if (r2 == r3) goto L_0x0031
            r3 = 6
            if (r2 == r3) goto L_0x002c
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.A(r10, r1)
            goto L_0x000b
        L_0x002c:
            boolean r8 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.s(r10, r1)
            goto L_0x000b
        L_0x0031:
            android.os.Parcelable$Creator<com.google.android.gms.cast.framework.media.NotificationOptions> r2 = com.google.android.gms.cast.framework.media.NotificationOptions.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a((android.os.Parcel) r10, (int) r1, r2)
            r7 = r1
            com.google.android.gms.cast.framework.media.NotificationOptions r7 = (com.google.android.gms.cast.framework.media.NotificationOptions) r7
            goto L_0x000b
        L_0x003b:
            android.os.IBinder r6 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.v(r10, r1)
            goto L_0x000b
        L_0x0040:
            java.lang.String r5 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r10, r1)
            goto L_0x000b
        L_0x0045:
            java.lang.String r4 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r10, r1)
            goto L_0x000b
        L_0x004a:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.r(r10, r0)
            com.google.android.gms.cast.framework.media.CastMediaOptions r10 = new com.google.android.gms.cast.framework.media.CastMediaOptions
            r3 = r10
            r3.<init>(r4, r5, r6, r7, r8)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.media.zza.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CastMediaOptions[i];
    }
}
