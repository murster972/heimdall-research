package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.Status;

final class zzay implements RemoteMediaClient.MediaChannelResult {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Status f3846a;

    zzay(RemoteMediaClient.zzc zzc, Status status) {
        this.f3846a = status;
    }

    public final Status getStatus() {
        return this.f3846a;
    }
}
