package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class AdBreakClipInfo extends AbstractSafeParcelable {
    public static final Parcelable.Creator<AdBreakClipInfo> CREATOR = new zza();

    /* renamed from: a  reason: collision with root package name */
    private final String f3758a;
    private final String b;
    private final long c;
    private final String d;
    private final String e;
    private final String f;
    private String g;
    private String h;
    private String i;
    private final long j;
    private final String k;
    private final VastAdsRequest l;
    private JSONObject m;

    AdBreakClipInfo(String str, String str2, long j2, String str3, String str4, String str5, String str6, String str7, String str8, long j3, String str9, VastAdsRequest vastAdsRequest) {
        this.f3758a = str;
        this.b = str2;
        this.c = j2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = str8;
        this.j = j3;
        this.k = str9;
        this.l = vastAdsRequest;
        if (!TextUtils.isEmpty(this.g)) {
            try {
                this.m = new JSONObject(str6);
            } catch (JSONException e2) {
                Log.w("AdBreakClipInfo", String.format(Locale.ROOT, "Error creating AdBreakClipInfo: %s", new Object[]{e2.getMessage()}));
                this.g = null;
                this.m = new JSONObject();
            }
        } else {
            this.m = new JSONObject();
        }
    }

    static AdBreakClipInfo a(JSONObject jSONObject) {
        long j2;
        String str;
        JSONObject jSONObject2 = jSONObject;
        if (jSONObject2 == null || !jSONObject2.has("id")) {
            return null;
        }
        try {
            String string = jSONObject2.getString("id");
            long optLong = (long) (((double) jSONObject2.optLong("duration")) * 1000.0d);
            String optString = jSONObject2.optString("clickThroughUrl", (String) null);
            String optString2 = jSONObject2.optString("contentUrl", (String) null);
            String optString3 = jSONObject2.optString("mimeType", (String) null);
            if (optString3 == null) {
                optString3 = jSONObject2.optString("contentType", (String) null);
            }
            String str2 = optString3;
            String optString4 = jSONObject2.optString("title", (String) null);
            JSONObject optJSONObject = jSONObject2.optJSONObject("customData");
            String optString5 = jSONObject2.optString("contentId", (String) null);
            String optString6 = jSONObject2.optString("posterUrl", (String) null);
            long j3 = -1;
            if (jSONObject2.has("whenSkippable")) {
                j2 = optLong;
                j3 = (long) (((double) ((Integer) jSONObject2.get("whenSkippable")).intValue()) * 1000.0d);
            } else {
                j2 = optLong;
            }
            String optString7 = jSONObject2.optString("hlsSegmentFormat", (String) null);
            VastAdsRequest a2 = VastAdsRequest.a(jSONObject2.optJSONObject("vastAdsRequest"));
            if (optJSONObject != null) {
                if (optJSONObject.length() != 0) {
                    str = optJSONObject.toString();
                    return new AdBreakClipInfo(string, optString4, j2, optString2, str2, optString, str, optString5, optString6, j3, optString7, a2);
                }
            }
            str = null;
            return new AdBreakClipInfo(string, optString4, j2, optString2, str2, optString, str, optString5, optString6, j3, optString7, a2);
        } catch (JSONException e2) {
            Log.d("AdBreakClipInfo", String.format(Locale.ROOT, "Error while creating an AdBreakClipInfo from JSON: %s", new Object[]{e2.getMessage()}));
            return null;
        }
    }

    public String A() {
        return this.b;
    }

    public VastAdsRequest B() {
        return this.l;
    }

    public long C() {
        return this.j;
    }

    public final JSONObject D() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", this.f3758a);
            jSONObject.put("duration", ((double) this.c) / 1000.0d);
            if (this.j != -1) {
                jSONObject.put("whenSkippable", ((double) this.j) / 1000.0d);
            }
            if (this.h != null) {
                jSONObject.put("contentId", this.h);
            }
            if (this.e != null) {
                jSONObject.put("contentType", this.e);
            }
            if (this.b != null) {
                jSONObject.put("title", this.b);
            }
            if (this.d != null) {
                jSONObject.put("contentUrl", this.d);
            }
            if (this.f != null) {
                jSONObject.put("clickThroughUrl", this.f);
            }
            if (this.m != null) {
                jSONObject.put("customData", this.m);
            }
            if (this.i != null) {
                jSONObject.put("posterUrl", this.i);
            }
            if (this.k != null) {
                jSONObject.put("hlsSegmentFormat", this.k);
            }
            if (this.l != null) {
                jSONObject.put("vastAdsRequest", this.l.u());
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdBreakClipInfo)) {
            return false;
        }
        AdBreakClipInfo adBreakClipInfo = (AdBreakClipInfo) obj;
        return zzdk.zza(this.f3758a, adBreakClipInfo.f3758a) && zzdk.zza(this.b, adBreakClipInfo.b) && this.c == adBreakClipInfo.c && zzdk.zza(this.d, adBreakClipInfo.d) && zzdk.zza(this.e, adBreakClipInfo.e) && zzdk.zza(this.f, adBreakClipInfo.f) && zzdk.zza(this.g, adBreakClipInfo.g) && zzdk.zza(this.h, adBreakClipInfo.h) && zzdk.zza(this.i, adBreakClipInfo.i) && this.j == adBreakClipInfo.j && zzdk.zza(this.k, adBreakClipInfo.k) && zzdk.zza(this.l, adBreakClipInfo.l);
    }

    public int hashCode() {
        return Objects.a(this.f3758a, this.b, Long.valueOf(this.c), this.d, this.e, this.f, this.g, this.h, this.i, Long.valueOf(this.j), this.k, this.l);
    }

    public String s() {
        return this.f;
    }

    public String t() {
        return this.h;
    }

    public String u() {
        return this.d;
    }

    public long v() {
        return this.c;
    }

    public String w() {
        return this.k;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, x(), false);
        SafeParcelWriter.a(parcel, 3, A(), false);
        SafeParcelWriter.a(parcel, 4, v());
        SafeParcelWriter.a(parcel, 5, u(), false);
        SafeParcelWriter.a(parcel, 6, z(), false);
        SafeParcelWriter.a(parcel, 7, s(), false);
        SafeParcelWriter.a(parcel, 8, this.g, false);
        SafeParcelWriter.a(parcel, 9, t(), false);
        SafeParcelWriter.a(parcel, 10, y(), false);
        SafeParcelWriter.a(parcel, 11, C());
        SafeParcelWriter.a(parcel, 12, w(), false);
        SafeParcelWriter.a(parcel, 13, (Parcelable) B(), i2, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public String x() {
        return this.f3758a;
    }

    public String y() {
        return this.i;
    }

    public String z() {
        return this.e;
    }
}
