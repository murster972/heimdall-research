package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzea;

final class zzv extends RemoteMediaClient.zzc {
    private final /* synthetic */ long[] d;
    private final /* synthetic */ RemoteMediaClient e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzv(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, long[] jArr) {
        super(remoteMediaClient, googleApiClient);
        this.e = remoteMediaClient;
        this.d = jArr;
    }

    /* access modifiers changed from: protected */
    public final void a(zzdd zzdd) throws zzea {
        this.e.c.zza(this.f3820a, this.d);
    }
}
