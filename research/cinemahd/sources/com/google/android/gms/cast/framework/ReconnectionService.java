package com.google.android.gms.cast.framework;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.internal.cast.zzdw;
import com.google.android.gms.internal.cast.zze;

public class ReconnectionService extends Service {
    private static final zzdw b = new zzdw("ReconnectionService");

    /* renamed from: a  reason: collision with root package name */
    private zzr f3791a;

    public IBinder onBind(Intent intent) {
        try {
            return this.f3791a.a(intent);
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "onBind", zzr.class.getSimpleName());
            return null;
        }
    }

    public void onCreate() {
        CastContext a2 = CastContext.a((Context) this);
        this.f3791a = zze.zza(this, a2.c().c(), a2.f().a());
        try {
            this.f3791a.d();
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "onCreate", zzr.class.getSimpleName());
        }
        super.onCreate();
    }

    public void onDestroy() {
        try {
            this.f3791a.onDestroy();
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "onDestroy", zzr.class.getSimpleName());
        }
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        try {
            return this.f3791a.a(intent, i, i2);
        } catch (RemoteException e) {
            b.zza(e, "Unable to call %s on %s.", "onStartCommand", zzr.class.getSimpleName());
            return 1;
        }
    }
}
