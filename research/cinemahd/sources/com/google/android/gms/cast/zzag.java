package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzag extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzag> CREATOR = new zzah();

    /* renamed from: a  reason: collision with root package name */
    private int f3863a;

    zzag(int i) {
        this.f3863a = i;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof zzag) && this.f3863a == ((zzag) obj).f3863a;
    }

    public final int hashCode() {
        return Objects.a(Integer.valueOf(this.f3863a));
    }

    public final String toString() {
        int i = this.f3863a;
        return String.format("joinOptions(connectionType=%s)", new Object[]{i != 0 ? i != 2 ? "UNKNOWN" : "INVISIBLE" : "STRONG"});
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.f3863a);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzag() {
        this(0);
    }
}
