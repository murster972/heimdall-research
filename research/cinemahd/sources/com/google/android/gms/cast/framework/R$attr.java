package com.google.android.gms.cast.framework;

public final class R$attr {
    public static final int castAdBreakMarkerColor = 2130968705;
    public static final int castAdInProgressLabelTextAppearance = 2130968706;
    public static final int castAdInProgressTextColor = 2130968707;
    public static final int castAdLabelColor = 2130968708;
    public static final int castAdLabelTextAppearance = 2130968709;
    public static final int castAdLabelTextColor = 2130968710;
    public static final int castBackground = 2130968711;
    public static final int castBackgroundColor = 2130968712;
    public static final int castButtonBackgroundColor = 2130968713;
    public static final int castButtonColor = 2130968714;
    public static final int castButtonText = 2130968715;
    public static final int castButtonTextAppearance = 2130968716;
    public static final int castClosedCaptionsButtonDrawable = 2130968717;
    public static final int castControlButtons = 2130968718;
    public static final int castExpandedControllerLoadingIndicatorColor = 2130968719;
    public static final int castExpandedControllerStyle = 2130968720;
    public static final int castExpandedControllerToolbarStyle = 2130968721;
    public static final int castFocusRadius = 2130968722;
    public static final int castForward30ButtonDrawable = 2130968723;
    public static final int castIntroOverlayStyle = 2130968724;
    public static final int castLargePauseButtonDrawable = 2130968725;
    public static final int castLargePlayButtonDrawable = 2130968726;
    public static final int castLargeStopButtonDrawable = 2130968727;
    public static final int castLiveIndicatorColor = 2130968728;
    public static final int castMiniControllerLoadingIndicatorColor = 2130968729;
    public static final int castMiniControllerStyle = 2130968730;
    public static final int castMuteToggleButtonDrawable = 2130968731;
    public static final int castPauseButtonDrawable = 2130968732;
    public static final int castPlayButtonDrawable = 2130968733;
    public static final int castProgressBarColor = 2130968734;
    public static final int castRewind30ButtonDrawable = 2130968735;
    public static final int castSeekBarProgressAndThumbColor = 2130968736;
    public static final int castSeekBarProgressDrawable = 2130968737;
    public static final int castSeekBarThumbDrawable = 2130968738;
    public static final int castShowImageThumbnail = 2130968739;
    public static final int castSkipNextButtonDrawable = 2130968740;
    public static final int castSkipPreviousButtonDrawable = 2130968741;
    public static final int castStopButtonDrawable = 2130968742;
    public static final int castSubtitleTextAppearance = 2130968743;
    public static final int castTitleTextAppearance = 2130968744;

    private R$attr() {
    }
}
