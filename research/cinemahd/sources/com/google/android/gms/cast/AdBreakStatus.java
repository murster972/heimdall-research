package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class AdBreakStatus extends AbstractSafeParcelable {
    public static final Parcelable.Creator<AdBreakStatus> CREATOR = new zzc();

    /* renamed from: a  reason: collision with root package name */
    private final long f3760a;
    private final long b;
    private final String c;
    private final String d;
    private final long e;

    AdBreakStatus(long j, long j2, String str, String str2, long j3) {
        this.f3760a = j;
        this.b = j2;
        this.c = str;
        this.d = str2;
        this.e = j3;
    }

    static AdBreakStatus a(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        if (jSONObject2 == null || !jSONObject2.has("currentBreakTime") || !jSONObject2.has("currentBreakClipTime")) {
            return null;
        }
        try {
            long j = (long) (((double) jSONObject2.getLong("currentBreakTime")) * 1000.0d);
            long j2 = (long) (((double) jSONObject2.getLong("currentBreakClipTime")) * 1000.0d);
            String optString = jSONObject2.optString("breakId", (String) null);
            String optString2 = jSONObject2.optString("breakClipId", (String) null);
            long optLong = jSONObject2.optLong("whenSkippable", -1);
            return new AdBreakStatus(j, j2, optString, optString2, optLong != -1 ? (long) (((double) optLong) * 1000.0d) : optLong);
        } catch (JSONException e2) {
            Log.d("AdBreakInfo", String.format(Locale.ROOT, "Error while creating an AdBreakClipInfo from JSON: %s", new Object[]{e2.getMessage()}));
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdBreakStatus)) {
            return false;
        }
        AdBreakStatus adBreakStatus = (AdBreakStatus) obj;
        return this.f3760a == adBreakStatus.f3760a && this.b == adBreakStatus.b && zzdk.zza(this.c, adBreakStatus.c) && zzdk.zza(this.d, adBreakStatus.d) && this.e == adBreakStatus.e;
    }

    public int hashCode() {
        return Objects.a(Long.valueOf(this.f3760a), Long.valueOf(this.b), this.c, this.d, Long.valueOf(this.e));
    }

    public String s() {
        return this.d;
    }

    public String t() {
        return this.c;
    }

    public long u() {
        return this.b;
    }

    public long v() {
        return this.f3760a;
    }

    public long w() {
        return this.e;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, v());
        SafeParcelWriter.a(parcel, 3, u());
        SafeParcelWriter.a(parcel, 4, t(), false);
        SafeParcelWriter.a(parcel, 5, s(), false);
        SafeParcelWriter.a(parcel, 6, w());
        SafeParcelWriter.a(parcel, a2);
    }
}
