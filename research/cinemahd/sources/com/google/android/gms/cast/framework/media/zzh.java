package com.google.android.gms.cast.framework.media;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.TaskStackBuilder;

final class zzh extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MediaNotificationService f3852a;

    zzh(MediaNotificationService mediaNotificationService) {
        this.f3852a = mediaNotificationService;
    }

    public final void onReceive(Context context, Intent intent) {
        PendingIntent pendingIntent;
        ComponentName componentName = (ComponentName) intent.getParcelableExtra("targetActivity");
        Intent intent2 = new Intent();
        intent2.setComponent(componentName);
        if (this.f3852a.p.e()) {
            intent2.setFlags(603979776);
            pendingIntent = PendingIntent.getActivity(context, 1, intent2, 134217728);
        } else {
            TaskStackBuilder a2 = TaskStackBuilder.a((Context) this.f3852a);
            a2.a(componentName);
            a2.a(intent2);
            pendingIntent = a2.a(1, 134217728);
        }
        try {
            pendingIntent.send(context, 1, new Intent().setFlags(268435456));
        } catch (PendingIntent.CanceledException e) {
            MediaNotificationService.r.zza(e, "Sending PendingIntent failed", new Object[0]);
        }
    }
}
