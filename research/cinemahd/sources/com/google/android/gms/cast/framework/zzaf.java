package com.google.android.gms.cast.framework;

import android.os.RemoteException;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

public final class zzaf<T extends Session> extends zzy {

    /* renamed from: a  reason: collision with root package name */
    private final SessionManagerListener<T> f3857a;
    private final Class<T> b;

    public zzaf(SessionManagerListener<T> sessionManagerListener, Class<T> cls) {
        this.f3857a = sessionManagerListener;
        this.b = cls;
    }

    public final void zza(IObjectWrapper iObjectWrapper) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionStarting((Session) this.b.cast(session));
        }
    }

    public final void zzb(IObjectWrapper iObjectWrapper) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionEnding((Session) this.b.cast(session));
        }
    }

    public final void zzc(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionResumeFailed((Session) this.b.cast(session), i);
        }
    }

    public final void zzd(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionSuspended((Session) this.b.cast(session), i);
        }
    }

    public final int zzs() {
        return 12451009;
    }

    public final IObjectWrapper zzt() {
        return ObjectWrapper.a(this.f3857a);
    }

    public final void zza(IObjectWrapper iObjectWrapper, String str) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionStarted((Session) this.b.cast(session), str);
        }
    }

    public final void zzb(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionEnded((Session) this.b.cast(session), i);
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, int i) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionStartFailed((Session) this.b.cast(session), i);
        }
    }

    public final void zzb(IObjectWrapper iObjectWrapper, String str) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionResuming((Session) this.b.cast(session), str);
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, boolean z) throws RemoteException {
        SessionManagerListener<T> sessionManagerListener;
        Session session = (Session) ObjectWrapper.a(iObjectWrapper);
        if (this.b.isInstance(session) && (sessionManagerListener = this.f3857a) != null) {
            sessionManagerListener.onSessionResumed((Session) this.b.cast(session), z);
        }
    }
}
