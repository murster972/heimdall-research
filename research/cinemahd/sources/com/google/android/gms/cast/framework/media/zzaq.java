package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzea;
import org.json.JSONObject;

final class zzaq extends RemoteMediaClient.zzc {
    private final /* synthetic */ JSONObject d;
    private final /* synthetic */ RemoteMediaClient e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaq(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient, JSONObject jSONObject) {
        super(remoteMediaClient, googleApiClient);
        this.e = remoteMediaClient;
        this.d = jSONObject;
    }

    /* access modifiers changed from: protected */
    public final void a(zzdd zzdd) throws zzea {
        this.e.c.zzc(this.f3820a, this.d);
    }
}
