package com.google.android.gms.cast.framework.media.uicontroller;

import android.widget.SeekBar;
import com.google.android.gms.internal.cast.zzbn;

final class zzf implements SeekBar.OnSeekBarChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzbn f3830a;
    private final /* synthetic */ SeekBar b;
    private final /* synthetic */ UIMediaController c;

    zzf(UIMediaController uIMediaController, zzbn zzbn, SeekBar seekBar) {
        this.c = uIMediaController;
        this.f3830a = zzbn;
        this.b = seekBar;
    }

    public final void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        zzbn zzbn = this.f3830a;
        if (zzbn != null) {
            zzbn.zzde();
        }
        if (this.c.e.zzdo()) {
            if (z && i < this.c.e.zzdr()) {
                int zzdr = this.c.e.zzdr();
                this.b.setProgress(zzdr);
                this.c.a(seekBar, zzdr, true);
                return;
            } else if (z && i > this.c.e.zzds()) {
                int zzds = this.c.e.zzds();
                this.b.setProgress(zzds);
                this.c.a(seekBar, zzds, true);
                return;
            }
        }
        this.c.a(seekBar, i, z);
    }

    public final void onStartTrackingTouch(SeekBar seekBar) {
        this.c.a(seekBar);
    }

    public final void onStopTrackingTouch(SeekBar seekBar) {
        this.c.b(seekBar);
    }
}
