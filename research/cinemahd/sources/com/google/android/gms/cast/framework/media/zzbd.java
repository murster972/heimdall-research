package com.google.android.gms.cast.framework.media;

import android.app.Dialog;
import android.content.DialogInterface;

final class zzbd implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TracksChooserDialogFragment f3848a;

    zzbd(TracksChooserDialogFragment tracksChooserDialogFragment) {
        this.f3848a = tracksChooserDialogFragment;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.f3848a.e != null) {
            this.f3848a.e.cancel();
            Dialog unused = this.f3848a.e = null;
        }
    }
}
