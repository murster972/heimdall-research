package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzf implements Parcelable.Creator<ImageHints> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 3) {
                i2 = SafeParcelReader.w(parcel, a2);
            } else if (a3 != 4) {
                SafeParcelReader.A(parcel, a2);
            } else {
                i3 = SafeParcelReader.w(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new ImageHints(i, i2, i3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ImageHints[i];
    }
}
