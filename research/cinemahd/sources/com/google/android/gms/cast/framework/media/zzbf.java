package com.google.android.gms.cast.framework.media;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.framework.R$id;
import com.google.android.gms.cast.framework.R$layout;
import com.google.android.gms.cast.framework.R$string;
import java.util.ArrayList;
import java.util.List;

public final class zzbf extends ArrayAdapter<MediaTrack> implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3850a;
    private int b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zzbf(Context context, List<MediaTrack> list, int i) {
        super(context, R$layout.cast_tracks_chooser_dialog_row_layout, list == null ? new ArrayList<>() : list);
        this.b = -1;
        this.f3850a = context;
        this.b = i;
    }

    public final MediaTrack a() {
        int i = this.b;
        if (i < 0 || i >= getCount()) {
            return null;
        }
        return (MediaTrack) getItem(this.b);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        zzbh zzbh;
        String str;
        if (view == null) {
            view = ((LayoutInflater) this.f3850a.getSystemService("layout_inflater")).inflate(R$layout.cast_tracks_chooser_dialog_row_layout, viewGroup, false);
            zzbh = new zzbh(this, (TextView) view.findViewById(R$id.text), (RadioButton) view.findViewById(R$id.radio));
            view.setTag(zzbh);
        } else {
            zzbh = (zzbh) view.getTag();
        }
        if (zzbh == null) {
            return null;
        }
        zzbh.b.setTag(Integer.valueOf(i));
        zzbh.b.setChecked(this.b == i);
        view.setOnClickListener(this);
        MediaTrack mediaTrack = (MediaTrack) getItem(i);
        String w = mediaTrack.w();
        if (TextUtils.isEmpty(w)) {
            if (mediaTrack.x() == 2) {
                str = this.f3850a.getString(R$string.cast_tracks_chooser_dialog_closed_captions);
            } else {
                if (!TextUtils.isEmpty(mediaTrack.v())) {
                    String displayLanguage = MediaUtils.a(mediaTrack).getDisplayLanguage();
                    if (!TextUtils.isEmpty(displayLanguage)) {
                        str = displayLanguage;
                    }
                }
                w = this.f3850a.getString(R$string.cast_tracks_chooser_dialog_default_track_name, new Object[]{Integer.valueOf(i + 1)});
            }
            zzbh.f3851a.setText(str);
            return view;
        }
        str = w;
        zzbh.f3851a.setText(str);
        return view;
    }

    public final void onClick(View view) {
        this.b = ((Integer) ((zzbh) view.getTag()).b.getTag()).intValue();
        notifyDataSetChanged();
    }
}
