package com.google.android.gms.cast.framework;

public final class R$dimen {
    public static final int cast_expanded_controller_ad_background_layout_height = 2131165282;
    public static final int cast_expanded_controller_ad_background_layout_width = 2131165283;
    public static final int cast_expanded_controller_ad_container_layout_height = 2131165284;
    public static final int cast_expanded_controller_ad_label_layout_height = 2131165285;
    public static final int cast_expanded_controller_ad_layout_height = 2131165286;
    public static final int cast_expanded_controller_ad_layout_width = 2131165287;
    public static final int cast_expanded_controller_control_button_margin = 2131165288;
    public static final int cast_expanded_controller_control_toolbar_min_height = 2131165289;
    public static final int cast_expanded_controller_margin_between_seek_bar_and_control_buttons = 2131165290;
    public static final int cast_expanded_controller_margin_between_status_text_and_seek_bar = 2131165291;
    public static final int cast_expanded_controller_seekbar_disabled_alpha = 2131165292;
    public static final int cast_intro_overlay_button_margin_bottom = 2131165293;
    public static final int cast_intro_overlay_focus_radius = 2131165294;
    public static final int cast_intro_overlay_title_margin_top = 2131165295;
    public static final int cast_libraries_material_featurehighlight_center_horizontal_offset = 2131165296;
    public static final int cast_libraries_material_featurehighlight_center_threshold = 2131165297;
    public static final int cast_libraries_material_featurehighlight_inner_margin = 2131165298;
    public static final int cast_libraries_material_featurehighlight_inner_radius = 2131165299;
    public static final int cast_libraries_material_featurehighlight_outer_padding = 2131165300;
    public static final int cast_libraries_material_featurehighlight_text_body_size = 2131165301;
    public static final int cast_libraries_material_featurehighlight_text_header_size = 2131165302;
    public static final int cast_libraries_material_featurehighlight_text_horizontal_margin = 2131165303;
    public static final int cast_libraries_material_featurehighlight_text_horizontal_offset = 2131165304;
    public static final int cast_libraries_material_featurehighlight_text_max_width = 2131165305;
    public static final int cast_libraries_material_featurehighlight_text_vertical_space = 2131165306;
    public static final int cast_mini_controller_control_button_margin = 2131165307;
    public static final int cast_mini_controller_icon_height = 2131165308;
    public static final int cast_mini_controller_icon_width = 2131165309;
    public static final int cast_notification_image_size = 2131165310;
    public static final int cast_tracks_chooser_dialog_no_message_text_size = 2131165311;
    public static final int cast_tracks_chooser_dialog_row_text_size = 2131165312;

    private R$dimen() {
    }
}
