package com.google.android.gms.cast.framework.media.uicontroller;

import android.view.View;

final class zzd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ long f3828a;
    private final /* synthetic */ UIMediaController b;

    zzd(UIMediaController uIMediaController, long j) {
        this.b = uIMediaController;
        this.f3828a = j;
    }

    public final void onClick(View view) {
        this.b.c(view, this.f3828a);
    }
}
