package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class SessionProvider {
    private final String category;
    private final Context zzja;
    private final zza zzjb = new zza();

    private class zza extends zzaa {
        private zza() {
        }

        public final IObjectWrapper a(String str) {
            Session createSession = SessionProvider.this.createSession(str);
            if (createSession == null) {
                return null;
            }
            return createSession.f();
        }

        public final String getCategory() {
            return SessionProvider.this.getCategory();
        }

        public final boolean m() {
            return SessionProvider.this.isSessionRecoverable();
        }

        public final int zzs() {
            return 12451009;
        }
    }

    protected SessionProvider(Context context, String str) {
        Preconditions.a(context);
        this.zzja = context.getApplicationContext();
        Preconditions.b(str);
        this.category = str;
    }

    public abstract Session createSession(String str);

    public final String getCategory() {
        return this.category;
    }

    public final Context getContext() {
        return this.zzja;
    }

    public abstract boolean isSessionRecoverable();

    public final IBinder zzaq() {
        return this.zzjb;
    }
}
