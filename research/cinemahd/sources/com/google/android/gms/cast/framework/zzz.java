package com.google.android.gms.cast.framework;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzz extends IInterface {
    IObjectWrapper a(String str) throws RemoteException;

    String getCategory() throws RemoteException;

    boolean m() throws RemoteException;

    int zzs() throws RemoteException;
}
