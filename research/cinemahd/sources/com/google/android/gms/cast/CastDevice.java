package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CastDevice extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<CastDevice> CREATOR = new zzn();

    /* renamed from: a  reason: collision with root package name */
    private String f3765a;
    private String b;
    private InetAddress c;
    private String d;
    private String e;
    private String f;
    private int g;
    private List<WebImage> h;
    private int i;
    private int j;
    private String k;
    private String l;
    private int m;
    private String n;
    private byte[] o;
    private String p;

    CastDevice(String str, String str2, String str3, String str4, String str5, int i2, List<WebImage> list, int i3, int i4, String str6, String str7, int i5, String str8, byte[] bArr, String str9) {
        List<WebImage> list2;
        this.f3765a = e(str);
        this.b = e(str2);
        if (!TextUtils.isEmpty(this.b)) {
            try {
                this.c = InetAddress.getByName(this.b);
            } catch (UnknownHostException e2) {
                String str10 = this.b;
                String message = e2.getMessage();
                StringBuilder sb = new StringBuilder(String.valueOf(str10).length() + 48 + String.valueOf(message).length());
                sb.append("Unable to convert host address (");
                sb.append(str10);
                sb.append(") to ipaddress: ");
                sb.append(message);
                Log.i("CastDevice", sb.toString());
            }
        }
        this.d = e(str3);
        this.e = e(str4);
        this.f = e(str5);
        this.g = i2;
        if (list != null) {
            list2 = list;
        } else {
            list2 = new ArrayList<>();
        }
        this.h = list2;
        this.i = i3;
        this.j = i4;
        this.k = e(str6);
        this.l = str7;
        this.m = i5;
        this.n = str8;
        this.o = bArr;
        this.p = str9;
    }

    public static CastDevice b(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        bundle.setClassLoader(CastDevice.class.getClassLoader());
        return (CastDevice) bundle.getParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE");
    }

    private static String e(String str) {
        return str == null ? "" : str;
    }

    public void a(Bundle bundle) {
        if (bundle != null) {
            bundle.putParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE", this);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CastDevice)) {
            return false;
        }
        CastDevice castDevice = (CastDevice) obj;
        String str = this.f3765a;
        return str == null ? castDevice.f3765a == null : zzdk.zza(str, castDevice.f3765a) && zzdk.zza(this.c, castDevice.c) && zzdk.zza(this.e, castDevice.e) && zzdk.zza(this.d, castDevice.d) && zzdk.zza(this.f, castDevice.f) && this.g == castDevice.g && zzdk.zza(this.h, castDevice.h) && this.i == castDevice.i && this.j == castDevice.j && zzdk.zza(this.k, castDevice.k) && zzdk.zza(Integer.valueOf(this.m), Integer.valueOf(castDevice.m)) && zzdk.zza(this.n, castDevice.n) && zzdk.zza(this.l, castDevice.l) && zzdk.zza(this.f, castDevice.t()) && this.g == castDevice.x() && ((this.o == null && castDevice.o == null) || Arrays.equals(this.o, castDevice.o)) && zzdk.zza(this.p, castDevice.p);
    }

    public int hashCode() {
        String str = this.f3765a;
        if (str == null) {
            return 0;
        }
        return str.hashCode();
    }

    public String s() {
        if (this.f3765a.startsWith("__cast_nearby__")) {
            return this.f3765a.substring(16);
        }
        return this.f3765a;
    }

    public String t() {
        return this.f;
    }

    public String toString() {
        return String.format("\"%s\" (%s)", new Object[]{this.d, this.f3765a});
    }

    public String u() {
        return this.d;
    }

    public List<WebImage> v() {
        return Collections.unmodifiableList(this.h);
    }

    public String w() {
        return this.e;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.f3765a, false);
        SafeParcelWriter.a(parcel, 3, this.b, false);
        SafeParcelWriter.a(parcel, 4, u(), false);
        SafeParcelWriter.a(parcel, 5, w(), false);
        SafeParcelWriter.a(parcel, 6, t(), false);
        SafeParcelWriter.a(parcel, 7, x());
        SafeParcelWriter.c(parcel, 8, v(), false);
        SafeParcelWriter.a(parcel, 9, this.i);
        SafeParcelWriter.a(parcel, 10, this.j);
        SafeParcelWriter.a(parcel, 11, this.k, false);
        SafeParcelWriter.a(parcel, 12, this.l, false);
        SafeParcelWriter.a(parcel, 13, this.m);
        SafeParcelWriter.a(parcel, 14, this.n, false);
        SafeParcelWriter.a(parcel, 15, this.o, false);
        SafeParcelWriter.a(parcel, 16, this.p, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public int x() {
        return this.g;
    }
}
