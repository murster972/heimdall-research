package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzc implements Parcelable.Creator<AdBreakStatus> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 2) {
                j = SafeParcelReader.y(parcel, a2);
            } else if (a3 == 3) {
                j2 = SafeParcelReader.y(parcel, a2);
            } else if (a3 == 4) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 == 5) {
                str2 = SafeParcelReader.o(parcel, a2);
            } else if (a3 != 6) {
                SafeParcelReader.A(parcel, a2);
            } else {
                j3 = SafeParcelReader.y(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new AdBreakStatus(j, j2, str, str2, j3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AdBreakStatus[i];
    }
}
