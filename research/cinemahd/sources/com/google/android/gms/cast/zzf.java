package com.google.android.gms.cast;

import android.os.RemoteException;
import com.facebook.ads.AdError;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzdo;

final class zzf extends zzdo {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f3867a;
    private final /* synthetic */ String b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzf(Cast.CastApi.zza zza, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient);
        this.f3867a = str;
        this.b = str2;
    }

    /* renamed from: zza */
    public final void doExecute(zzdd zzdd) throws RemoteException {
        try {
            zzdd.zza(this.f3867a, this.b, (BaseImplementation$ResultHolder<Status>) this);
        } catch (IllegalArgumentException | IllegalStateException unused) {
            zzp(AdError.INTERNAL_ERROR_CODE);
        }
    }
}
