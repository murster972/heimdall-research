package com.google.android.gms.cast.framework.media.uicontroller;

import android.view.View;
import android.widget.ImageView;

final class zza implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ UIMediaController f3825a;

    zza(UIMediaController uIMediaController) {
        this.f3825a = uIMediaController;
    }

    public final void onClick(View view) {
        this.f3825a.c((ImageView) view);
    }
}
