package com.google.android.gms.cast.framework.media.uicontroller;

import android.view.View;

final class zzh implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ UIMediaController f3832a;

    zzh(UIMediaController uIMediaController) {
        this.f3832a = uIMediaController;
    }

    public final void onClick(View view) {
        this.f3832a.d(view);
    }
}
