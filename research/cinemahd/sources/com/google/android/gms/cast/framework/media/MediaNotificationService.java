package com.google.android.gms.cast.framework.media;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import com.google.android.gms.cast.framework.AppVisibilityListener;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.cast.zzaa;
import com.google.android.gms.internal.cast.zzdw;
import java.util.ArrayList;
import java.util.List;

public class MediaNotificationService extends Service {
    /* access modifiers changed from: private */
    public static final zzdw r = new zzdw("MediaNotificationService");

    /* renamed from: a  reason: collision with root package name */
    private NotificationOptions f3811a;
    private ImagePicker b;
    private ComponentName c;
    private ComponentName d;
    private List<String> e = new ArrayList();
    private int[] f;
    private zzd g;
    private long h;
    private zzaa i;
    private ImageHints j;
    private Resources k;
    private AppVisibilityListener l;
    private zza m;
    /* access modifiers changed from: private */
    public zzb n;
    /* access modifiers changed from: private */
    public Notification o;
    /* access modifiers changed from: private */
    public CastContext p;
    private final BroadcastReceiver q = new zzh(this);

    private static class zza {

        /* renamed from: a  reason: collision with root package name */
        public final MediaSessionCompat.Token f3812a;
        public final boolean b;
        public final int c;
        public final String d;
        public final String e;
        public final boolean f;
        public final boolean g;

        public zza(boolean z, int i, String str, String str2, MediaSessionCompat.Token token, boolean z2, boolean z3) {
            this.b = z;
            this.c = i;
            this.d = str;
            this.e = str2;
            this.f3812a = token;
            this.f = z2;
            this.g = z3;
        }
    }

    private static class zzb {

        /* renamed from: a  reason: collision with root package name */
        public final Uri f3813a;
        public Bitmap b;

        public zzb(WebImage webImage) {
            Uri uri;
            if (webImage == null) {
                uri = null;
            } else {
                uri = webImage.t();
            }
            this.f3813a = uri;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:87:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r10 = this;
            java.lang.Class<com.google.android.gms.cast.framework.media.NotificationActionsProvider> r0 = com.google.android.gms.cast.framework.media.NotificationActionsProvider.class
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r1 = r10.m
            if (r1 != 0) goto L_0x0007
            return
        L_0x0007:
            com.google.android.gms.cast.framework.media.MediaNotificationService$zzb r1 = r10.n
            r2 = 0
            if (r1 != 0) goto L_0x000e
            r1 = r2
            goto L_0x0010
        L_0x000e:
            android.graphics.Bitmap r1 = r1.b
        L_0x0010:
            androidx.core.app.NotificationCompat$Builder r3 = new androidx.core.app.NotificationCompat$Builder
            java.lang.String r4 = "cast_media_notification"
            r3.<init>(r10, r4)
            r3.a((android.graphics.Bitmap) r1)
            com.google.android.gms.cast.framework.media.NotificationOptions r1 = r10.f3811a
            int r1 = r1.H()
            r3.b((int) r1)
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r1 = r10.m
            java.lang.String r1 = r1.d
            r3.b((java.lang.CharSequence) r1)
            android.content.res.Resources r1 = r10.k
            com.google.android.gms.cast.framework.media.NotificationOptions r4 = r10.f3811a
            int r4 = r4.t()
            r5 = 1
            java.lang.Object[] r6 = new java.lang.Object[r5]
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r7 = r10.m
            java.lang.String r7 = r7.e
            r8 = 0
            r6[r8] = r7
            java.lang.String r1 = r1.getString(r4, r6)
            r3.a((java.lang.CharSequence) r1)
            r3.c((boolean) r5)
            r3.d((boolean) r8)
            r3.c((int) r5)
            android.content.ComponentName r1 = r10.d
            if (r1 != 0) goto L_0x0051
            goto L_0x006c
        L_0x0051:
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            android.content.ComponentName r2 = r10.d
            java.lang.String r4 = "targetActivity"
            r1.putExtra(r4, r2)
            android.content.ComponentName r2 = r10.d
            java.lang.String r2 = r2.flattenToString()
            r1.setAction(r2)
            r2 = 134217728(0x8000000, float:3.85186E-34)
            android.app.PendingIntent r2 = android.app.PendingIntent.getBroadcast(r10, r5, r1, r2)
        L_0x006c:
            if (r2 == 0) goto L_0x0071
            r3.a((android.app.PendingIntent) r2)
        L_0x0071:
            com.google.android.gms.cast.framework.media.zzd r1 = r10.g
            if (r1 == 0) goto L_0x01b9
            com.google.android.gms.internal.cast.zzdw r1 = r
            java.lang.Object[] r2 = new java.lang.Object[r8]
            java.lang.String r4 = "mActionsProvider != null"
            r1.i(r4, r2)
            com.google.android.gms.cast.framework.media.zzd r1 = r10.g     // Catch:{ RemoteException -> 0x01a1 }
            java.util.List r1 = r1.p()     // Catch:{ RemoteException -> 0x01a1 }
            com.google.android.gms.cast.framework.media.zzd r2 = r10.g     // Catch:{ RemoteException -> 0x01a1 }
            int[] r2 = r2.l()     // Catch:{ RemoteException -> 0x01a1 }
            if (r1 == 0) goto L_0x00b2
            boolean r4 = r1.isEmpty()
            if (r4 == 0) goto L_0x0093
            goto L_0x00b2
        L_0x0093:
            int r4 = r1.size()
            r6 = 5
            if (r4 <= r6) goto L_0x00b0
            com.google.android.gms.internal.cast.zzdw r4 = r
            java.lang.String r6 = r0.getSimpleName()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r7 = " provides more than 5 actions."
            java.lang.String r6 = r6.concat(r7)
            java.lang.Object[] r7 = new java.lang.Object[r8]
            r4.e(r6, r7)
            goto L_0x00c7
        L_0x00b0:
            r4 = 1
            goto L_0x00c8
        L_0x00b2:
            com.google.android.gms.internal.cast.zzdw r4 = r
            java.lang.String r6 = r0.getSimpleName()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r7 = " doesn't provide any action."
            java.lang.String r6 = r6.concat(r7)
            java.lang.Object[] r7 = new java.lang.Object[r8]
            r4.e(r6, r7)
        L_0x00c7:
            r4 = 0
        L_0x00c8:
            if (r4 == 0) goto L_0x01a0
            int r4 = r1.size()
            if (r2 == 0) goto L_0x00fa
            int r6 = r2.length
            if (r6 != 0) goto L_0x00d4
            goto L_0x00fa
        L_0x00d4:
            int r6 = r2.length
            r7 = 0
        L_0x00d6:
            if (r7 >= r6) goto L_0x00f8
            r9 = r2[r7]
            if (r9 < 0) goto L_0x00e2
            if (r9 < r4) goto L_0x00df
            goto L_0x00e2
        L_0x00df:
            int r7 = r7 + 1
            goto L_0x00d6
        L_0x00e2:
            com.google.android.gms.internal.cast.zzdw r4 = r
            java.lang.String r0 = r0.getSimpleName()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r6 = "provides a compact view action whose index is out of bounds."
            java.lang.String r0 = r0.concat(r6)
            java.lang.Object[] r6 = new java.lang.Object[r8]
            r4.e(r0, r6)
            goto L_0x010f
        L_0x00f8:
            r0 = 1
            goto L_0x0110
        L_0x00fa:
            com.google.android.gms.internal.cast.zzdw r4 = r
            java.lang.String r0 = r0.getSimpleName()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r6 = " doesn't provide any actions for compact view."
            java.lang.String r0 = r0.concat(r6)
            java.lang.Object[] r6 = new java.lang.Object[r8]
            r4.e(r0, r6)
        L_0x010f:
            r0 = 0
        L_0x0110:
            if (r0 != 0) goto L_0x0114
            goto L_0x01a0
        L_0x0114:
            java.lang.Object r0 = r2.clone()
            int[] r0 = (int[]) r0
            java.util.Iterator r1 = r1.iterator()
        L_0x011e:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x01d1
            java.lang.Object r2 = r1.next()
            com.google.android.gms.cast.framework.media.NotificationAction r2 = (com.google.android.gms.cast.framework.media.NotificationAction) r2
            com.google.android.gms.cast.framework.media.NotificationAction$Builder r4 = new com.google.android.gms.cast.framework.media.NotificationAction$Builder
            r4.<init>()
            java.lang.String r6 = r2.s()
            r4.a(r6)
            java.lang.String r4 = r2.s()
            java.lang.String r6 = "com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK"
            boolean r6 = r4.equals(r6)
            if (r6 != 0) goto L_0x016d
            java.lang.String r6 = "com.google.android.gms.cast.framework.action.SKIP_NEXT"
            boolean r6 = r4.equals(r6)
            if (r6 != 0) goto L_0x016d
            java.lang.String r6 = "com.google.android.gms.cast.framework.action.SKIP_PREV"
            boolean r6 = r4.equals(r6)
            if (r6 != 0) goto L_0x016d
            java.lang.String r6 = "com.google.android.gms.cast.framework.action.FORWARD"
            boolean r6 = r4.equals(r6)
            if (r6 != 0) goto L_0x016d
            java.lang.String r6 = "com.google.android.gms.cast.framework.action.REWIND"
            boolean r6 = r4.equals(r6)
            if (r6 != 0) goto L_0x016d
            java.lang.String r6 = "com.google.android.gms.cast.framework.action.STOP_CASTING"
            boolean r4 = r4.equals(r6)
            if (r4 == 0) goto L_0x016b
            goto L_0x016d
        L_0x016b:
            r4 = 0
            goto L_0x016e
        L_0x016d:
            r4 = 1
        L_0x016e:
            if (r4 == 0) goto L_0x0178
            java.lang.String r2 = r2.s()
            r10.a((androidx.core.app.NotificationCompat.Builder) r3, (java.lang.String) r2)
            goto L_0x011e
        L_0x0178:
            android.content.Intent r4 = new android.content.Intent
            java.lang.String r6 = r2.s()
            r4.<init>(r6)
            android.content.ComponentName r6 = r10.c
            r4.setComponent(r6)
            android.app.PendingIntent r4 = android.app.PendingIntent.getBroadcast(r10, r8, r4, r8)
            androidx.core.app.NotificationCompat$Action$Builder r6 = new androidx.core.app.NotificationCompat$Action$Builder
            int r7 = r2.u()
            java.lang.String r2 = r2.t()
            r6.<init>(r7, r2, r4)
            androidx.core.app.NotificationCompat$Action r2 = r6.a()
            r3.a((androidx.core.app.NotificationCompat.Action) r2)
            goto L_0x011e
        L_0x01a0:
            return
        L_0x01a1:
            r0 = move-exception
            com.google.android.gms.internal.cast.zzdw r1 = r
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = "getNotificationActions"
            r2[r8] = r3
            java.lang.Class<com.google.android.gms.cast.framework.media.zzd> r3 = com.google.android.gms.cast.framework.media.zzd.class
            java.lang.String r3 = r3.getSimpleName()
            r2[r5] = r3
            java.lang.String r3 = "Unable to call %s on %s."
            r1.zzc(r0, r3, r2)
            return
        L_0x01b9:
            java.util.List<java.lang.String> r0 = r10.e
            java.util.Iterator r0 = r0.iterator()
        L_0x01bf:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x01cf
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            r10.a((androidx.core.app.NotificationCompat.Builder) r3, (java.lang.String) r1)
            goto L_0x01bf
        L_0x01cf:
            int[] r0 = r10.f
        L_0x01d1:
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 21
            if (r1 < r2) goto L_0x01eb
            androidx.media.app.NotificationCompat$MediaStyle r1 = new androidx.media.app.NotificationCompat$MediaStyle
            r1.<init>()
            androidx.media.app.NotificationCompat$MediaStyle r0 = r1.a((int[]) r0)
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r1 = r10.m
            android.support.v4.media.session.MediaSessionCompat$Token r1 = r1.f3812a
            androidx.media.app.NotificationCompat$MediaStyle r0 = r0.a((android.support.v4.media.session.MediaSessionCompat.Token) r1)
            r3.a((androidx.core.app.NotificationCompat.Style) r0)
        L_0x01eb:
            android.app.Notification r0 = r3.a()
            r10.o = r0
            com.google.android.gms.cast.framework.CastContext r0 = r10.p
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x01ff
            android.app.Notification r0 = r10.o
            r10.startForeground(r5, r0)
            return
        L_0x01ff:
            r10.stopForeground(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.media.MediaNotificationService.a():void");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.p = CastContext.a((Context) this);
        CastMediaOptions s = this.p.a().s();
        this.f3811a = s.v();
        this.b = s.t();
        this.k = getResources();
        this.c = new ComponentName(getApplicationContext(), s.u());
        if (!TextUtils.isEmpty(this.f3811a.K())) {
            this.d = new ComponentName(getApplicationContext(), this.f3811a.K());
        } else {
            this.d = null;
        }
        this.g = this.f3811a.X();
        if (this.g == null) {
            this.e.addAll(this.f3811a.s());
            this.f = (int[]) this.f3811a.u().clone();
        } else {
            this.f = null;
        }
        this.h = this.f3811a.G();
        int dimensionPixelSize = this.k.getDimensionPixelSize(this.f3811a.L());
        this.j = new ImageHints(1, dimensionPixelSize, dimensionPixelSize);
        this.i = new zzaa(getApplicationContext(), this.j);
        this.l = new zzi(this);
        this.p.a(this.l);
        ComponentName componentName = this.d;
        if (componentName != null) {
            registerReceiver(this.q, new IntentFilter(componentName.flattenToString()));
        }
        if (PlatformVersion.k()) {
            NotificationChannel notificationChannel = new NotificationChannel("cast_media_notification", "Cast", 2);
            notificationChannel.setShowBadge(false);
            ((NotificationManager) getSystemService(NotificationManager.class)).createNotificationChannel(notificationChannel);
        }
    }

    public void onDestroy() {
        zzaa zzaa = this.i;
        if (zzaa != null) {
            zzaa.clear();
        }
        if (this.d != null) {
            try {
                unregisterReceiver(this.q);
            } catch (IllegalArgumentException e2) {
                r.zzc(e2, "Unregistering trampoline BroadcastReceiver failed", new Object[0]);
            }
        }
        ((NotificationManager) getSystemService("notification")).cancel(1);
        this.p.b(this.l);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00aa, code lost:
        if ((r1 != null && r7.b == r1.b && r7.c == r1.c && com.google.android.gms.internal.cast.zzdk.zza(r7.d, r1.d) && com.google.android.gms.internal.cast.zzdk.zza(r7.e, r1.e) && r7.f == r1.f && r7.g == r1.g) == false) goto L_0x00ac;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int onStartCommand(android.content.Intent r18, int r19, int r20) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = 2
            if (r1 == 0) goto L_0x00fb
            java.lang.String r3 = r18.getAction()
            java.lang.String r4 = "com.google.android.gms.cast.framework.action.UPDATE_NOTIFICATION"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x00fb
            java.lang.String r3 = "extra_media_info"
            android.os.Parcelable r3 = r1.getParcelableExtra(r3)
            com.google.android.gms.cast.MediaInfo r3 = (com.google.android.gms.cast.MediaInfo) r3
            r4 = 1
            r5 = 0
            if (r3 != 0) goto L_0x0021
            goto L_0x00f6
        L_0x0021:
            com.google.android.gms.cast.MediaMetadata r6 = r3.y()
            if (r6 != 0) goto L_0x0029
            goto L_0x00f6
        L_0x0029:
            java.lang.String r7 = "extra_remote_media_client_player_state"
            int r7 = r1.getIntExtra(r7, r5)
            if (r7 != 0) goto L_0x0033
            goto L_0x00f6
        L_0x0033:
            java.lang.String r8 = "extra_cast_device"
            android.os.Parcelable r8 = r1.getParcelableExtra(r8)
            com.google.android.gms.cast.CastDevice r8 = (com.google.android.gms.cast.CastDevice) r8
            if (r8 != 0) goto L_0x003f
            goto L_0x00f6
        L_0x003f:
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r15 = new com.google.android.gms.cast.framework.media.MediaNotificationService$zza
            if (r7 != r2) goto L_0x0045
            r10 = 1
            goto L_0x0046
        L_0x0045:
            r10 = 0
        L_0x0046:
            int r11 = r3.A()
            java.lang.String r3 = "com.google.android.gms.cast.metadata.TITLE"
            java.lang.String r12 = r6.f(r3)
            java.lang.String r13 = r8.u()
            java.lang.String r3 = "extra_media_session_token"
            android.os.Parcelable r3 = r1.getParcelableExtra(r3)
            r14 = r3
            android.support.v4.media.session.MediaSessionCompat$Token r14 = (android.support.v4.media.session.MediaSessionCompat.Token) r14
            java.lang.String r3 = "extra_can_skip_next"
            boolean r3 = r1.getBooleanExtra(r3, r5)
            java.lang.String r7 = "extra_can_skip_prev"
            boolean r16 = r1.getBooleanExtra(r7, r5)
            r9 = r15
            r7 = r15
            r15 = r3
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            java.lang.String r3 = "extra_media_notification_force_update"
            boolean r1 = r1.getBooleanExtra(r3, r5)
            if (r1 != 0) goto L_0x00ac
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r1 = r0.m
            if (r1 == 0) goto L_0x00a9
            boolean r3 = r7.b
            boolean r8 = r1.b
            if (r3 != r8) goto L_0x00a9
            int r3 = r7.c
            int r8 = r1.c
            if (r3 != r8) goto L_0x00a9
            java.lang.String r3 = r7.d
            java.lang.String r8 = r1.d
            boolean r3 = com.google.android.gms.internal.cast.zzdk.zza(r3, r8)
            if (r3 == 0) goto L_0x00a9
            java.lang.String r3 = r7.e
            java.lang.String r8 = r1.e
            boolean r3 = com.google.android.gms.internal.cast.zzdk.zza(r3, r8)
            if (r3 == 0) goto L_0x00a9
            boolean r3 = r7.f
            boolean r8 = r1.f
            if (r3 != r8) goto L_0x00a9
            boolean r3 = r7.g
            boolean r1 = r1.g
            if (r3 != r1) goto L_0x00a9
            r1 = 1
            goto L_0x00aa
        L_0x00a9:
            r1 = 0
        L_0x00aa:
            if (r1 != 0) goto L_0x00b1
        L_0x00ac:
            r0.m = r7
            r17.a()
        L_0x00b1:
            com.google.android.gms.cast.framework.media.MediaNotificationService$zzb r1 = new com.google.android.gms.cast.framework.media.MediaNotificationService$zzb
            com.google.android.gms.cast.framework.media.ImagePicker r3 = r0.b
            if (r3 == 0) goto L_0x00be
            com.google.android.gms.cast.framework.media.ImageHints r7 = r0.j
            com.google.android.gms.common.images.WebImage r3 = r3.a((com.google.android.gms.cast.MediaMetadata) r6, (com.google.android.gms.cast.framework.media.ImageHints) r7)
            goto L_0x00d0
        L_0x00be:
            boolean r3 = r6.v()
            if (r3 == 0) goto L_0x00cf
            java.util.List r3 = r6.t()
            java.lang.Object r3 = r3.get(r5)
            com.google.android.gms.common.images.WebImage r3 = (com.google.android.gms.common.images.WebImage) r3
            goto L_0x00d0
        L_0x00cf:
            r3 = 0
        L_0x00d0:
            r1.<init>(r3)
            com.google.android.gms.cast.framework.media.MediaNotificationService$zzb r3 = r0.n
            if (r3 == 0) goto L_0x00e2
            android.net.Uri r6 = r1.f3813a
            android.net.Uri r3 = r3.f3813a
            boolean r3 = com.google.android.gms.internal.cast.zzdk.zza(r6, r3)
            if (r3 == 0) goto L_0x00e2
            r5 = 1
        L_0x00e2:
            if (r5 != 0) goto L_0x00f5
            com.google.android.gms.internal.cast.zzaa r3 = r0.i
            com.google.android.gms.cast.framework.media.zzj r5 = new com.google.android.gms.cast.framework.media.zzj
            r5.<init>(r0, r1)
            r3.zza((com.google.android.gms.internal.cast.zzab) r5)
            com.google.android.gms.internal.cast.zzaa r3 = r0.i
            android.net.Uri r1 = r1.f3813a
            r3.zza((android.net.Uri) r1)
        L_0x00f5:
            r5 = 1
        L_0x00f6:
            if (r5 != 0) goto L_0x00fb
            r0.stopForeground(r4)
        L_0x00fb:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.media.MediaNotificationService.onStartCommand(android.content.Intent, int, int):int");
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(androidx.core.app.NotificationCompat.Builder r19, java.lang.String r20) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            int r3 = r20.hashCode()
            java.lang.String r6 = "com.google.android.gms.cast.framework.action.FORWARD"
            java.lang.String r7 = "com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK"
            java.lang.String r8 = "com.google.android.gms.cast.framework.action.STOP_CASTING"
            java.lang.String r9 = "com.google.android.gms.cast.framework.action.SKIP_PREV"
            java.lang.String r10 = "com.google.android.gms.cast.framework.action.SKIP_NEXT"
            java.lang.String r11 = "com.google.android.gms.cast.framework.action.REWIND"
            r12 = 0
            switch(r3) {
                case -1699820260: goto L_0x004d;
                case -945151566: goto L_0x0045;
                case -945080078: goto L_0x003d;
                case -668151673: goto L_0x0035;
                case -124479363: goto L_0x002b;
                case 235550565: goto L_0x0023;
                case 1362116196: goto L_0x001b;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x0055
        L_0x001b:
            boolean r3 = r2.equals(r6)
            if (r3 == 0) goto L_0x0055
            r3 = 3
            goto L_0x0056
        L_0x0023:
            boolean r3 = r2.equals(r7)
            if (r3 == 0) goto L_0x0055
            r3 = 0
            goto L_0x0056
        L_0x002b:
            java.lang.String r3 = "com.google.android.gms.cast.framework.action.DISCONNECT"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0055
            r3 = 6
            goto L_0x0056
        L_0x0035:
            boolean r3 = r2.equals(r8)
            if (r3 == 0) goto L_0x0055
            r3 = 5
            goto L_0x0056
        L_0x003d:
            boolean r3 = r2.equals(r9)
            if (r3 == 0) goto L_0x0055
            r3 = 2
            goto L_0x0056
        L_0x0045:
            boolean r3 = r2.equals(r10)
            if (r3 == 0) goto L_0x0055
            r3 = 1
            goto L_0x0056
        L_0x004d:
            boolean r3 = r2.equals(r11)
            if (r3 == 0) goto L_0x0055
            r3 = 4
            goto L_0x0056
        L_0x0055:
            r3 = -1
        L_0x0056:
            r13 = 30000(0x7530, double:1.4822E-319)
            r15 = 10000(0x2710, double:4.9407E-320)
            r5 = 134217728(0x8000000, float:3.85186E-34)
            java.lang.String r4 = "googlecast-extra_skip_step_ms"
            r17 = 0
            switch(r3) {
                case 0: goto L_0x01ad;
                case 1: goto L_0x0178;
                case 2: goto L_0x0143;
                case 3: goto L_0x00f0;
                case 4: goto L_0x009d;
                case 5: goto L_0x0070;
                case 6: goto L_0x0070;
                default: goto L_0x0063;
            }
        L_0x0063:
            com.google.android.gms.internal.cast.zzdw r1 = r
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r3[r12] = r2
            java.lang.String r2 = "Action: %s is not a pre-defined action."
            r1.e(r2, r3)
            return
        L_0x0070:
            android.content.Intent r2 = new android.content.Intent
            r2.<init>(r8)
            android.content.ComponentName r3 = r0.c
            r2.setComponent(r3)
            android.app.PendingIntent r2 = android.app.PendingIntent.getBroadcast(r0, r12, r2, r12)
            androidx.core.app.NotificationCompat$Action$Builder r3 = new androidx.core.app.NotificationCompat$Action$Builder
            com.google.android.gms.cast.framework.media.NotificationOptions r4 = r0.f3811a
            int r4 = r4.v()
            android.content.res.Resources r5 = r0.k
            com.google.android.gms.cast.framework.media.NotificationOptions r6 = r0.f3811a
            int r6 = r6.W()
            java.lang.String r5 = r5.getString(r6)
            r3.<init>(r4, r5, r2)
            androidx.core.app.NotificationCompat$Action r2 = r3.a()
            r1.a((androidx.core.app.NotificationCompat.Action) r2)
            return
        L_0x009d:
            long r2 = r0.h
            android.content.Intent r6 = new android.content.Intent
            r6.<init>(r11)
            android.content.ComponentName r7 = r0.c
            r6.setComponent(r7)
            r6.putExtra(r4, r2)
            android.app.PendingIntent r4 = android.app.PendingIntent.getBroadcast(r0, r12, r6, r5)
            com.google.android.gms.cast.framework.media.NotificationOptions r5 = r0.f3811a
            int r5 = r5.D()
            com.google.android.gms.cast.framework.media.NotificationOptions r6 = r0.f3811a
            int r6 = r6.T()
            int r7 = (r2 > r15 ? 1 : (r2 == r15 ? 0 : -1))
            if (r7 != 0) goto L_0x00cd
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r5 = r2.B()
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r6 = r2.U()
            goto L_0x00dd
        L_0x00cd:
            int r7 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
            if (r7 != 0) goto L_0x00dd
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r5 = r2.C()
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r6 = r2.V()
        L_0x00dd:
            androidx.core.app.NotificationCompat$Action$Builder r2 = new androidx.core.app.NotificationCompat$Action$Builder
            android.content.res.Resources r3 = r0.k
            java.lang.String r3 = r3.getString(r6)
            r2.<init>(r5, r3, r4)
            androidx.core.app.NotificationCompat$Action r2 = r2.a()
            r1.a((androidx.core.app.NotificationCompat.Action) r2)
            return
        L_0x00f0:
            long r2 = r0.h
            android.content.Intent r7 = new android.content.Intent
            r7.<init>(r6)
            android.content.ComponentName r6 = r0.c
            r7.setComponent(r6)
            r7.putExtra(r4, r2)
            android.app.PendingIntent r4 = android.app.PendingIntent.getBroadcast(r0, r12, r7, r5)
            com.google.android.gms.cast.framework.media.NotificationOptions r5 = r0.f3811a
            int r5 = r5.y()
            com.google.android.gms.cast.framework.media.NotificationOptions r6 = r0.f3811a
            int r6 = r6.Q()
            int r7 = (r2 > r15 ? 1 : (r2 == r15 ? 0 : -1))
            if (r7 != 0) goto L_0x0120
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r5 = r2.w()
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r6 = r2.R()
            goto L_0x0130
        L_0x0120:
            int r7 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
            if (r7 != 0) goto L_0x0130
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r5 = r2.x()
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r6 = r2.S()
        L_0x0130:
            androidx.core.app.NotificationCompat$Action$Builder r2 = new androidx.core.app.NotificationCompat$Action$Builder
            android.content.res.Resources r3 = r0.k
            java.lang.String r3 = r3.getString(r6)
            r2.<init>(r5, r3, r4)
            androidx.core.app.NotificationCompat$Action r2 = r2.a()
            r1.a((androidx.core.app.NotificationCompat.Action) r2)
            return
        L_0x0143:
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r2 = r0.m
            boolean r2 = r2.g
            if (r2 == 0) goto L_0x0157
            android.content.Intent r2 = new android.content.Intent
            r2.<init>(r9)
            android.content.ComponentName r3 = r0.c
            r2.setComponent(r3)
            android.app.PendingIntent r17 = android.app.PendingIntent.getBroadcast(r0, r12, r2, r12)
        L_0x0157:
            r2 = r17
            androidx.core.app.NotificationCompat$Action$Builder r3 = new androidx.core.app.NotificationCompat$Action$Builder
            com.google.android.gms.cast.framework.media.NotificationOptions r4 = r0.f3811a
            int r4 = r4.F()
            android.content.res.Resources r5 = r0.k
            com.google.android.gms.cast.framework.media.NotificationOptions r6 = r0.f3811a
            int r6 = r6.P()
            java.lang.String r5 = r5.getString(r6)
            r3.<init>(r4, r5, r2)
            androidx.core.app.NotificationCompat$Action r2 = r3.a()
            r1.a((androidx.core.app.NotificationCompat.Action) r2)
            return
        L_0x0178:
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r2 = r0.m
            boolean r2 = r2.f
            if (r2 == 0) goto L_0x018c
            android.content.Intent r2 = new android.content.Intent
            r2.<init>(r10)
            android.content.ComponentName r3 = r0.c
            r2.setComponent(r3)
            android.app.PendingIntent r17 = android.app.PendingIntent.getBroadcast(r0, r12, r2, r12)
        L_0x018c:
            r2 = r17
            androidx.core.app.NotificationCompat$Action$Builder r3 = new androidx.core.app.NotificationCompat$Action$Builder
            com.google.android.gms.cast.framework.media.NotificationOptions r4 = r0.f3811a
            int r4 = r4.E()
            android.content.res.Resources r5 = r0.k
            com.google.android.gms.cast.framework.media.NotificationOptions r6 = r0.f3811a
            int r6 = r6.O()
            java.lang.String r5 = r5.getString(r6)
            r3.<init>(r4, r5, r2)
            androidx.core.app.NotificationCompat$Action r2 = r3.a()
            r1.a((androidx.core.app.NotificationCompat.Action) r2)
            return
        L_0x01ad:
            com.google.android.gms.cast.framework.media.MediaNotificationService$zza r2 = r0.m
            int r3 = r2.c
            boolean r2 = r2.b
            r4 = 2
            if (r3 != r4) goto L_0x01c3
            com.google.android.gms.cast.framework.media.NotificationOptions r3 = r0.f3811a
            int r3 = r3.I()
            com.google.android.gms.cast.framework.media.NotificationOptions r4 = r0.f3811a
            int r4 = r4.J()
            goto L_0x01cf
        L_0x01c3:
            com.google.android.gms.cast.framework.media.NotificationOptions r3 = r0.f3811a
            int r3 = r3.z()
            com.google.android.gms.cast.framework.media.NotificationOptions r4 = r0.f3811a
            int r4 = r4.M()
        L_0x01cf:
            if (r2 == 0) goto L_0x01d2
            goto L_0x01d8
        L_0x01d2:
            com.google.android.gms.cast.framework.media.NotificationOptions r3 = r0.f3811a
            int r3 = r3.A()
        L_0x01d8:
            if (r2 == 0) goto L_0x01db
            goto L_0x01e1
        L_0x01db:
            com.google.android.gms.cast.framework.media.NotificationOptions r2 = r0.f3811a
            int r4 = r2.N()
        L_0x01e1:
            android.content.Intent r2 = new android.content.Intent
            r2.<init>(r7)
            android.content.ComponentName r5 = r0.c
            r2.setComponent(r5)
            android.app.PendingIntent r2 = android.app.PendingIntent.getBroadcast(r0, r12, r2, r12)
            androidx.core.app.NotificationCompat$Action$Builder r5 = new androidx.core.app.NotificationCompat$Action$Builder
            android.content.res.Resources r6 = r0.k
            java.lang.String r4 = r6.getString(r4)
            r5.<init>(r3, r4, r2)
            androidx.core.app.NotificationCompat$Action r2 = r5.a()
            r1.a((androidx.core.app.NotificationCompat.Action) r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cast.framework.media.MediaNotificationService.a(androidx.core.app.NotificationCompat$Builder, java.lang.String):void");
    }
}
