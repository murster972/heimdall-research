package com.google.android.gms.cast.framework.media;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import com.facebook.ads.AdError;
import com.google.android.gms.cast.AdBreakInfo;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.zzam;
import com.google.android.gms.cast.zzas;
import com.google.android.gms.cast.zzau;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.cast.zzcv;
import com.google.android.gms.internal.cast.zzdd;
import com.google.android.gms.internal.cast.zzdx;
import com.google.android.gms.internal.cast.zzdz;
import com.google.android.gms.internal.cast.zzea;
import com.google.android.gms.internal.cast.zzeb;
import com.google.android.gms.internal.cast.zzec;
import com.google.android.gms.internal.cast.zzez;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

public class RemoteMediaClient implements Cast.MessageReceivedCallback {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Object f3818a = new Object();
    /* access modifiers changed from: private */
    public final Handler b = new zzez(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final zzdx c;
    private final zza d = new zza();
    /* access modifiers changed from: private */
    public final Cast.CastApi e;
    private GoogleApiClient f;
    /* access modifiers changed from: private */
    public final List<Listener> g = new CopyOnWriteArrayList();
    final List<Callback> h = new CopyOnWriteArrayList();
    private final Map<ProgressListener, zze> i = new ConcurrentHashMap();
    private final Map<Long, zze> j = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public ParseAdsInfoCallback k;

    public static abstract class Callback {
        public void a() {
        }

        public void a(int[] iArr) {
        }

        public void a(int[] iArr, int i) {
        }

        public void a(MediaQueueItem[] mediaQueueItemArr) {
        }

        public void b() {
        }

        public void b(int[] iArr) {
        }

        public void c() {
        }

        public void c(int[] iArr) {
        }

        public void d() {
        }

        public void e() {
        }

        public void f() {
        }
    }

    @Deprecated
    public interface Listener {
        void onAdBreakStatusUpdated();

        void onMetadataUpdated();

        void onPreloadStatusUpdated();

        void onQueueStatusUpdated();

        void onSendingRemoteMediaRequest();

        void onStatusUpdated();
    }

    public interface MediaChannelResult extends Result {
    }

    public interface ParseAdsInfoCallback {
        List<AdBreakInfo> a(MediaStatus mediaStatus);

        boolean b(MediaStatus mediaStatus);
    }

    public interface ProgressListener {
        void onProgressUpdated(long j, long j2);
    }

    private class zza implements zzeb {

        /* renamed from: a  reason: collision with root package name */
        private GoogleApiClient f3819a;
        private long b = 0;

        public zza() {
        }

        public final void a(GoogleApiClient googleApiClient) {
            this.f3819a = googleApiClient;
        }

        public final void zza(String str, String str2, long j, String str3) {
            if (this.f3819a != null) {
                RemoteMediaClient.this.e.b(this.f3819a, str, str2).setResultCallback(new zzav(this, j));
                return;
            }
            throw new IllegalStateException("No GoogleApiClient available");
        }

        public final long zzr() {
            long j = this.b + 1;
            this.b = j;
            return j;
        }
    }

    private static class zzb extends BasePendingResult<MediaChannelResult> {
        zzb() {
            super((GoogleApiClient) null);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final MediaChannelResult createFailedResult(Status status) {
            return new zzaw(this, status);
        }
    }

    abstract class zzc extends zzcv<MediaChannelResult> {

        /* renamed from: a  reason: collision with root package name */
        zzec f3820a;
        private final boolean b;

        zzc(RemoteMediaClient remoteMediaClient, GoogleApiClient googleApiClient) {
            this(googleApiClient, false);
        }

        /* access modifiers changed from: package-private */
        public abstract void a(zzdd zzdd) throws zzea;

        public /* synthetic */ Result createFailedResult(Status status) {
            return new zzay(this, status);
        }

        /* access modifiers changed from: protected */
        public /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
            zzdd zzdd = (zzdd) anyClient;
            if (!this.b) {
                for (Listener onSendingRemoteMediaRequest : RemoteMediaClient.this.g) {
                    onSendingRemoteMediaRequest.onSendingRemoteMediaRequest();
                }
                for (Callback e : RemoteMediaClient.this.h) {
                    e.e();
                }
            }
            try {
                synchronized (RemoteMediaClient.this.f3818a) {
                    a(zzdd);
                }
            } catch (zzea unused) {
                setResult((MediaChannelResult) createFailedResult(new Status(AdError.BROKEN_MEDIA_ERROR_CODE)));
            }
        }

        zzc(GoogleApiClient googleApiClient, boolean z) {
            super(googleApiClient);
            this.b = z;
            this.f3820a = new zzax(this, RemoteMediaClient.this);
        }
    }

    private static final class zzd implements MediaChannelResult {

        /* renamed from: a  reason: collision with root package name */
        private final Status f3821a;

        zzd(Status status, JSONObject jSONObject) {
            this.f3821a = status;
        }

        public final Status getStatus() {
            return this.f3821a;
        }
    }

    private class zze {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Set<ProgressListener> f3822a = new HashSet();
        /* access modifiers changed from: private */
        public final long b;
        private final Runnable c;
        private boolean d;

        public zze(long j) {
            this.b = j;
            this.c = new zzaz(this, RemoteMediaClient.this);
        }

        public final void a(ProgressListener progressListener) {
            this.f3822a.add(progressListener);
        }

        public final void b(ProgressListener progressListener) {
            this.f3822a.remove(progressListener);
        }

        public final void c() {
            RemoteMediaClient.this.b.removeCallbacks(this.c);
            this.d = true;
            RemoteMediaClient.this.b.postDelayed(this.c, this.b);
        }

        public final void d() {
            RemoteMediaClient.this.b.removeCallbacks(this.c);
            this.d = false;
        }

        public final long e() {
            return this.b;
        }

        public final boolean a() {
            return !this.f3822a.isEmpty();
        }

        public final boolean b() {
            return this.d;
        }
    }

    static {
        String str = zzdx.NAMESPACE;
    }

    public RemoteMediaClient(zzdx zzdx, Cast.CastApi castApi) {
        this.e = castApi;
        Preconditions.a(zzdx);
        this.c = zzdx;
        this.c.zza((zzdz) new zzs(this));
        this.c.zza(this.d);
    }

    private final boolean x() {
        return this.f != null;
    }

    /* access modifiers changed from: private */
    public final void y() {
        for (zze next : this.j.values()) {
            if (k() && !next.b()) {
                next.c();
            } else if (!k() && next.b()) {
                next.d();
            }
            if (next.b() && (l() || o() || n())) {
                a((Set<ProgressListener>) next.f3822a);
            }
        }
    }

    public final void a(GoogleApiClient googleApiClient) {
        GoogleApiClient googleApiClient2 = this.f;
        if (googleApiClient2 != googleApiClient) {
            if (googleApiClient2 != null) {
                this.c.zzeq();
                try {
                    this.e.a(this.f, g());
                } catch (IOException unused) {
                }
                this.d.a((GoogleApiClient) null);
                this.b.removeCallbacksAndMessages((Object) null);
            }
            this.f = googleApiClient;
            GoogleApiClient googleApiClient3 = this.f;
            if (googleApiClient3 != null) {
                this.d.a(googleApiClient3);
            }
        }
    }

    public PendingResult<MediaChannelResult> b(JSONObject jSONObject) {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzaq zzaq = new zzaq(this, this.f, jSONObject);
        a((zzc) zzaq);
        return zzaq;
    }

    public PendingResult<MediaChannelResult> c(JSONObject jSONObject) {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzaf zzaf = new zzaf(this, this.f, jSONObject);
        a((zzc) zzaf);
        return zzaf;
    }

    public PendingResult<MediaChannelResult> d(JSONObject jSONObject) {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzae zzae = new zzae(this, this.f, jSONObject);
        a((zzc) zzae);
        return zzae;
    }

    public MediaInfo e() {
        MediaInfo mediaInfo;
        synchronized (this.f3818a) {
            Preconditions.a("Must be called from the main thread.");
            mediaInfo = this.c.getMediaInfo();
        }
        return mediaInfo;
    }

    public MediaStatus f() {
        MediaStatus mediaStatus;
        synchronized (this.f3818a) {
            Preconditions.a("Must be called from the main thread.");
            mediaStatus = this.c.getMediaStatus();
        }
        return mediaStatus;
    }

    public String g() {
        Preconditions.a("Must be called from the main thread.");
        return this.c.getNamespace();
    }

    public int h() {
        int A;
        synchronized (this.f3818a) {
            Preconditions.a("Must be called from the main thread.");
            MediaStatus f2 = f();
            A = f2 != null ? f2.A() : 1;
        }
        return A;
    }

    public MediaQueueItem i() {
        Preconditions.a("Must be called from the main thread.");
        MediaStatus f2 = f();
        if (f2 == null) {
            return null;
        }
        return f2.d(f2.B());
    }

    public long j() {
        long streamDuration;
        synchronized (this.f3818a) {
            Preconditions.a("Must be called from the main thread.");
            streamDuration = this.c.getStreamDuration();
        }
        return streamDuration;
    }

    public boolean k() {
        Preconditions.a("Must be called from the main thread.");
        return l() || p() || o() || n();
    }

    public boolean l() {
        Preconditions.a("Must be called from the main thread.");
        MediaStatus f2 = f();
        return f2 != null && f2.A() == 4;
    }

    public boolean m() {
        Preconditions.a("Must be called from the main thread.");
        MediaInfo e2 = e();
        return e2 != null && e2.A() == 2;
    }

    public boolean n() {
        Preconditions.a("Must be called from the main thread.");
        MediaStatus f2 = f();
        return (f2 == null || f2.x() == 0) ? false : true;
    }

    public boolean o() {
        Preconditions.a("Must be called from the main thread.");
        MediaStatus f2 = f();
        if (f2 == null) {
            return false;
        }
        if (f2.A() != 3) {
            return m() && c() == 2;
        }
        return true;
    }

    public void onMessageReceived(CastDevice castDevice, String str, String str2) {
        this.c.zzo(str2);
    }

    public boolean p() {
        Preconditions.a("Must be called from the main thread.");
        MediaStatus f2 = f();
        return f2 != null && f2.A() == 2;
    }

    public boolean q() {
        Preconditions.a("Must be called from the main thread.");
        MediaStatus f2 = f();
        return f2 != null && f2.I();
    }

    public PendingResult<MediaChannelResult> r() {
        return a((JSONObject) null);
    }

    public PendingResult<MediaChannelResult> s() {
        return b((JSONObject) null);
    }

    public PendingResult<MediaChannelResult> t() {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzu zzu = new zzu(this, this.f);
        a((zzc) zzu);
        return zzu;
    }

    public PendingResult<MediaChannelResult> u() {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzt zzt = new zzt(this, this.f);
        a((zzc) zzt);
        return zzt;
    }

    public void v() {
        Preconditions.a("Must be called from the main thread.");
        int h2 = h();
        if (h2 == 4 || h2 == 2) {
            r();
        } else {
            s();
        }
    }

    public final void w() throws IOException {
        GoogleApiClient googleApiClient = this.f;
        if (googleApiClient != null) {
            this.e.a(googleApiClient, g(), (Cast.MessageReceivedCallback) this);
        }
    }

    public long b() {
        long approximateStreamPosition;
        synchronized (this.f3818a) {
            Preconditions.a("Must be called from the main thread.");
            approximateStreamPosition = this.c.getApproximateStreamPosition();
        }
        return approximateStreamPosition;
    }

    public int c() {
        int w;
        synchronized (this.f3818a) {
            Preconditions.a("Must be called from the main thread.");
            MediaStatus f2 = f();
            w = f2 != null ? f2.w() : 0;
        }
        return w;
    }

    public MediaQueueItem d() {
        Preconditions.a("Must be called from the main thread.");
        MediaStatus f2 = f();
        if (f2 == null) {
            return null;
        }
        return f2.d(f2.x());
    }

    @Deprecated
    public PendingResult<MediaChannelResult> a(MediaInfo mediaInfo, boolean z, long j2) {
        return a(mediaInfo, new MediaLoadOptions.Builder().a(z).a(j2).a());
    }

    @Deprecated
    public void b(Listener listener) {
        Preconditions.a("Must be called from the main thread.");
        if (listener != null) {
            this.g.remove(listener);
        }
    }

    public void b(Callback callback) {
        Preconditions.a("Must be called from the main thread.");
        if (callback != null) {
            this.h.remove(callback);
        }
    }

    public PendingResult<MediaChannelResult> a(MediaInfo mediaInfo, MediaLoadOptions mediaLoadOptions) {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzad zzad = new zzad(this, this.f, mediaInfo, (zzam) null, mediaLoadOptions);
        a((zzc) zzad);
        return zzad;
    }

    public PendingResult<MediaChannelResult> a(JSONObject jSONObject) {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzao zzao = new zzao(this, this.f, jSONObject);
        a((zzc) zzao);
        return zzao;
    }

    public PendingResult<MediaChannelResult> a(long j2) {
        return a(j2, 0, (JSONObject) null);
    }

    public PendingResult<MediaChannelResult> a(long j2, int i2, JSONObject jSONObject) {
        zzas a2 = new zzau().a(j2).a(i2).a(jSONObject).a();
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzar zzar = new zzar(this, this.f, a2);
        a((zzc) zzar);
        return zzar;
    }

    public PendingResult<MediaChannelResult> a(long[] jArr) {
        Preconditions.a("Must be called from the main thread.");
        if (!x()) {
            return a(17, (String) null);
        }
        zzv zzv = new zzv(this, this.f, jArr);
        a((zzc) zzv);
        return zzv;
    }

    public long a() {
        long approximateAdBreakClipPositionMs;
        synchronized (this.f3818a) {
            Preconditions.a("Must be called from the main thread.");
            approximateAdBreakClipPositionMs = this.c.getApproximateAdBreakClipPositionMs();
        }
        return approximateAdBreakClipPositionMs;
    }

    @Deprecated
    public void a(Listener listener) {
        Preconditions.a("Must be called from the main thread.");
        if (listener != null) {
            this.g.add(listener);
        }
    }

    public void a(Callback callback) {
        Preconditions.a("Must be called from the main thread.");
        if (callback != null) {
            this.h.add(callback);
        }
    }

    public boolean a(ProgressListener progressListener, long j2) {
        Preconditions.a("Must be called from the main thread.");
        if (progressListener == null || this.i.containsKey(progressListener)) {
            return false;
        }
        zze zze2 = this.j.get(Long.valueOf(j2));
        if (zze2 == null) {
            zze2 = new zze(j2);
            this.j.put(Long.valueOf(j2), zze2);
        }
        zze2.a(progressListener);
        this.i.put(progressListener, zze2);
        if (!k()) {
            return true;
        }
        zze2.c();
        return true;
    }

    public void a(ProgressListener progressListener) {
        Preconditions.a("Must be called from the main thread.");
        zze remove = this.i.remove(progressListener);
        if (remove != null) {
            remove.b(progressListener);
            if (!remove.a()) {
                this.j.remove(Long.valueOf(remove.e()));
                remove.d();
            }
        }
    }

    public static PendingResult<MediaChannelResult> a(int i2, String str) {
        zzb zzb2 = new zzb();
        zzb2.setResult(zzb2.createFailedResult(new Status(i2, str)));
        return zzb2;
    }

    private final zzc a(zzc zzc2) {
        try {
            this.f.a(zzc2);
        } catch (IllegalArgumentException e2) {
            throw e2;
        } catch (Throwable unused) {
            zzc2.setResult((MediaChannelResult) zzc2.createFailedResult(new Status(AdError.BROKEN_MEDIA_ERROR_CODE)));
        }
        return zzc2;
    }

    /* access modifiers changed from: private */
    public final void a(Set<ProgressListener> set) {
        HashSet<ProgressListener> hashSet = new HashSet<>(set);
        if (p() || o() || l()) {
            for (ProgressListener onProgressUpdated : hashSet) {
                onProgressUpdated.onProgressUpdated(b(), j());
            }
        } else if (n()) {
            MediaQueueItem d2 = d();
            if (d2 != null && d2.v() != null) {
                for (ProgressListener onProgressUpdated2 : hashSet) {
                    onProgressUpdated2.onProgressUpdated(0, d2.v().z());
                }
            }
        } else {
            for (ProgressListener onProgressUpdated3 : hashSet) {
                onProgressUpdated3.onProgressUpdated(0, 0);
            }
        }
    }
}
