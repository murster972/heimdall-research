package com.google.android.gms.cast.framework.media;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface zzd extends IInterface {
    int[] l() throws RemoteException;

    List<NotificationAction> p() throws RemoteException;
}
