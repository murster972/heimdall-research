package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.internal.cast.zzdk;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.unity3d.ads.metadata.MediationMetaData;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaTrack extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<MediaTrack> CREATOR = new zzaw();

    /* renamed from: a  reason: collision with root package name */
    private long f3777a;
    private int b;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private String h;
    private JSONObject i;

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final MediaTrack f3778a;

        public Builder(long j, int i) throws IllegalArgumentException {
            this.f3778a = new MediaTrack(j, i);
        }

        public Builder a(String str) {
            this.f3778a.e(str);
            return this;
        }

        public Builder b(String str) {
            this.f3778a.f(str);
            return this;
        }

        public Builder c(String str) {
            this.f3778a.g(str);
            return this;
        }

        public Builder d(String str) {
            this.f3778a.h(str);
            return this;
        }

        public Builder a(int i) throws IllegalArgumentException {
            this.f3778a.b(i);
            return this;
        }

        public MediaTrack a() {
            return this.f3778a;
        }
    }

    MediaTrack(long j, int i2, String str, String str2, String str3, String str4, int i3, String str5) {
        this.f3777a = j;
        this.b = i2;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = i3;
        this.h = str5;
        String str6 = this.h;
        if (str6 != null) {
            try {
                this.i = new JSONObject(str6);
            } catch (JSONException unused) {
                this.i = null;
                this.h = null;
            }
        } else {
            this.i = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) throws IllegalArgumentException {
        if (i2 < 0 || i2 > 5) {
            StringBuilder sb = new StringBuilder(27);
            sb.append("invalid subtype ");
            sb.append(i2);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 == 0 || this.b == 1) {
            this.g = i2;
        } else {
            throw new IllegalArgumentException("subtypes are only valid for text tracks");
        }
    }

    public final void e(String str) {
        this.c = str;
    }

    public final boolean equals(Object obj) {
        JSONObject jSONObject;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaTrack)) {
            return false;
        }
        MediaTrack mediaTrack = (MediaTrack) obj;
        if ((this.i == null) != (mediaTrack.i == null)) {
            return false;
        }
        JSONObject jSONObject2 = this.i;
        return (jSONObject2 == null || (jSONObject = mediaTrack.i) == null || JsonUtils.a(jSONObject2, jSONObject)) && this.f3777a == mediaTrack.f3777a && this.b == mediaTrack.b && zzdk.zza(this.c, mediaTrack.c) && zzdk.zza(this.d, mediaTrack.d) && zzdk.zza(this.e, mediaTrack.e) && zzdk.zza(this.f, mediaTrack.f) && this.g == mediaTrack.g;
    }

    public final void f(String str) {
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public final void g(String str) {
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public final void h(String str) {
        this.e = str;
    }

    public final int hashCode() {
        return Objects.a(Long.valueOf(this.f3777a), Integer.valueOf(this.b), this.c, this.d, this.e, this.f, Integer.valueOf(this.g), String.valueOf(this.i));
    }

    public final String s() {
        return this.c;
    }

    public final String t() {
        return this.d;
    }

    public final long u() {
        return this.f3777a;
    }

    public final String v() {
        return this.f;
    }

    public final String w() {
        return this.e;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        JSONObject jSONObject = this.i;
        this.h = jSONObject == null ? null : jSONObject.toString();
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, u());
        SafeParcelWriter.a(parcel, 3, y());
        SafeParcelWriter.a(parcel, 4, s(), false);
        SafeParcelWriter.a(parcel, 5, t(), false);
        SafeParcelWriter.a(parcel, 6, w(), false);
        SafeParcelWriter.a(parcel, 7, v(), false);
        SafeParcelWriter.a(parcel, 8, x());
        SafeParcelWriter.a(parcel, 9, this.h, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public final int x() {
        return this.g;
    }

    public final int y() {
        return this.b;
    }

    public final JSONObject z() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("trackId", this.f3777a);
            int i2 = this.b;
            if (i2 == 1) {
                jSONObject.put("type", AdPreferences.TYPE_TEXT);
            } else if (i2 == 2) {
                jSONObject.put("type", "AUDIO");
            } else if (i2 == 3) {
                jSONObject.put("type", "VIDEO");
            }
            if (this.c != null) {
                jSONObject.put("trackContentId", this.c);
            }
            if (this.d != null) {
                jSONObject.put("trackContentType", this.d);
            }
            if (this.e != null) {
                jSONObject.put(MediationMetaData.KEY_NAME, this.e);
            }
            if (!TextUtils.isEmpty(this.f)) {
                jSONObject.put("language", this.f);
            }
            int i3 = this.g;
            if (i3 == 1) {
                jSONObject.put("subtype", "SUBTITLES");
            } else if (i3 == 2) {
                jSONObject.put("subtype", "CAPTIONS");
            } else if (i3 == 3) {
                jSONObject.put("subtype", "DESCRIPTIONS");
            } else if (i3 == 4) {
                jSONObject.put("subtype", "CHAPTERS");
            } else if (i3 == 5) {
                jSONObject.put("subtype", "METADATA");
            }
            if (this.i != null) {
                jSONObject.put("customData", this.i);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    MediaTrack(JSONObject jSONObject) throws JSONException {
        this(0, 0, (String) null, (String) null, (String) null, (String) null, -1, (String) null);
        this.f3777a = jSONObject.getLong("trackId");
        String string = jSONObject.getString("type");
        if (AdPreferences.TYPE_TEXT.equals(string)) {
            this.b = 1;
        } else if ("AUDIO".equals(string)) {
            this.b = 2;
        } else if ("VIDEO".equals(string)) {
            this.b = 3;
        } else {
            String valueOf = String.valueOf(string);
            throw new JSONException(valueOf.length() != 0 ? "invalid type: ".concat(valueOf) : new String("invalid type: "));
        }
        this.c = jSONObject.optString("trackContentId", (String) null);
        this.d = jSONObject.optString("trackContentType", (String) null);
        this.e = jSONObject.optString(MediationMetaData.KEY_NAME, (String) null);
        this.f = jSONObject.optString("language", (String) null);
        if (jSONObject.has("subtype")) {
            String string2 = jSONObject.getString("subtype");
            if ("SUBTITLES".equals(string2)) {
                this.g = 1;
            } else if ("CAPTIONS".equals(string2)) {
                this.g = 2;
            } else if ("DESCRIPTIONS".equals(string2)) {
                this.g = 3;
            } else if ("CHAPTERS".equals(string2)) {
                this.g = 4;
            } else if ("METADATA".equals(string2)) {
                this.g = 5;
            } else {
                this.g = -1;
            }
        } else {
            this.g = 0;
        }
        this.i = jSONObject.optJSONObject("customData");
    }

    MediaTrack(long j, int i2) throws IllegalArgumentException {
        this(0, 0, (String) null, (String) null, (String) null, (String) null, -1, (String) null);
        this.f3777a = j;
        if (i2 <= 0 || i2 > 3) {
            StringBuilder sb = new StringBuilder(24);
            sb.append("invalid type ");
            sb.append(i2);
            throw new IllegalArgumentException(sb.toString());
        }
        this.b = i2;
    }
}
