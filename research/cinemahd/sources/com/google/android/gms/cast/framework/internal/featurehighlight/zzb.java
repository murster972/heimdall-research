package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class zzb extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zza f3799a;

    zzb(zza zza) {
        this.f3799a = zza;
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        if (this.f3799a.a(x, y) && this.f3799a.d.a(x, y)) {
            return true;
        }
        this.f3799a.l.dismiss();
        return true;
    }
}
