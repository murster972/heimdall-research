package com.google.android.gms.cast.framework.media;

import com.google.android.gms.cast.AdBreakInfo;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.internal.cast.zzdz;
import java.util.List;

final class zzs implements zzdz {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ RemoteMediaClient f3855a;

    zzs(RemoteMediaClient remoteMediaClient) {
        this.f3855a = remoteMediaClient;
    }

    private final void a() {
        MediaStatus f;
        if (this.f3855a.k != null && (f = this.f3855a.f()) != null) {
            f.a(this.f3855a.k.b(f));
            List<AdBreakInfo> a2 = this.f3855a.k.a(f);
            MediaInfo e = this.f3855a.e();
            if (e != null) {
                e.b(a2);
            }
        }
    }

    public final void onAdBreakStatusUpdated() {
        for (RemoteMediaClient.Listener onAdBreakStatusUpdated : this.f3855a.g) {
            onAdBreakStatusUpdated.onAdBreakStatusUpdated();
        }
        for (RemoteMediaClient.Callback a2 : this.f3855a.h) {
            a2.a();
        }
    }

    public final void onMetadataUpdated() {
        a();
        for (RemoteMediaClient.Listener onMetadataUpdated : this.f3855a.g) {
            onMetadataUpdated.onMetadataUpdated();
        }
        for (RemoteMediaClient.Callback b : this.f3855a.h) {
            b.b();
        }
    }

    public final void onPreloadStatusUpdated() {
        for (RemoteMediaClient.Listener onPreloadStatusUpdated : this.f3855a.g) {
            onPreloadStatusUpdated.onPreloadStatusUpdated();
        }
        for (RemoteMediaClient.Callback c : this.f3855a.h) {
            c.c();
        }
    }

    public final void onQueueStatusUpdated() {
        for (RemoteMediaClient.Listener onQueueStatusUpdated : this.f3855a.g) {
            onQueueStatusUpdated.onQueueStatusUpdated();
        }
        for (RemoteMediaClient.Callback d : this.f3855a.h) {
            d.d();
        }
    }

    public final void onStatusUpdated() {
        a();
        this.f3855a.y();
        for (RemoteMediaClient.Listener onStatusUpdated : this.f3855a.g) {
            onStatusUpdated.onStatusUpdated();
        }
        for (RemoteMediaClient.Callback f : this.f3855a.h) {
            f.f();
        }
    }

    public final void zza(int[] iArr) {
        for (RemoteMediaClient.Callback a2 : this.f3855a.h) {
            a2.a(iArr);
        }
    }

    public final void zzb(int[] iArr) {
        for (RemoteMediaClient.Callback b : this.f3855a.h) {
            b.b(iArr);
        }
    }

    public final void zzc(int[] iArr) {
        for (RemoteMediaClient.Callback c : this.f3855a.h) {
            c.c(iArr);
        }
    }

    public final void zza(int[] iArr, int i) {
        for (RemoteMediaClient.Callback a2 : this.f3855a.h) {
            a2.a(iArr, i);
        }
    }

    public final void zzb(MediaQueueItem[] mediaQueueItemArr) {
        for (RemoteMediaClient.Callback a2 : this.f3855a.h) {
            a2.a(mediaQueueItemArr);
        }
    }
}
