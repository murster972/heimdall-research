package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.media.NotificationOptions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.cast.zzdw;

public class CastMediaOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<CastMediaOptions> CREATOR = new zza();
    private static final zzdw f = new zzdw("CastMediaOptions");

    /* renamed from: a  reason: collision with root package name */
    private final String f3806a;
    private final String b;
    private final zzb c;
    private final NotificationOptions d;
    private final boolean e;

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private String f3807a = MediaIntentReceiver.class.getName();
        private String b;
        private ImagePicker c;
        private NotificationOptions d = new NotificationOptions.Builder().a();

        public final Builder a(String str) {
            this.b = str;
            return this;
        }

        public final Builder a(NotificationOptions notificationOptions) {
            this.d = notificationOptions;
            return this;
        }

        public final CastMediaOptions a() {
            ImagePicker imagePicker = this.c;
            return new CastMediaOptions(this.f3807a, this.b, imagePicker == null ? null : imagePicker.a().asBinder(), this.d, false);
        }
    }

    CastMediaOptions(String str, String str2, IBinder iBinder, NotificationOptions notificationOptions, boolean z) {
        zzb zzb;
        this.f3806a = str;
        this.b = str2;
        if (iBinder == null) {
            zzb = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.IImagePicker");
            if (queryLocalInterface instanceof zzb) {
                zzb = (zzb) queryLocalInterface;
            } else {
                zzb = new zzc(iBinder);
            }
        }
        this.c = zzb;
        this.d = notificationOptions;
        this.e = z;
    }

    public String s() {
        return this.b;
    }

    public ImagePicker t() {
        zzb zzb = this.c;
        if (zzb == null) {
            return null;
        }
        try {
            return (ImagePicker) ObjectWrapper.a(zzb.k());
        } catch (RemoteException e2) {
            f.zza(e2, "Unable to call %s on %s.", "getWrappedClientObject", zzb.class.getSimpleName());
            return null;
        }
    }

    public String u() {
        return this.f3806a;
    }

    public NotificationOptions v() {
        return this.d;
    }

    public final boolean w() {
        return this.e;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, u(), false);
        SafeParcelWriter.a(parcel, 3, s(), false);
        zzb zzb = this.c;
        SafeParcelWriter.a(parcel, 4, zzb == null ? null : zzb.asBinder(), false);
        SafeParcelWriter.a(parcel, 5, (Parcelable) v(), i, false);
        SafeParcelWriter.a(parcel, 6, this.e);
        SafeParcelWriter.a(parcel, a2);
    }
}
