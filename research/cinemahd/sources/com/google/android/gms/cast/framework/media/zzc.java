package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.cast.zza;

public final class zzc extends zza implements zzb {
    zzc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.cast.framework.media.IImagePicker");
    }

    public final IObjectWrapper k() throws RemoteException {
        Parcel zza = zza(2, zza());
        IObjectWrapper a2 = IObjectWrapper.Stub.a(zza.readStrongBinder());
        zza.recycle();
        return a2;
    }
}
