package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import androidx.annotation.Keep;
import androidx.core.graphics.ColorUtils;
import com.google.android.gms.cast.framework.R$color;
import com.google.android.gms.cast.framework.R$dimen;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.internal.cast.zzfl;
import com.google.android.gms.internal.cast.zzfn;

class OuterHighlightDrawable extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private final int f3797a;
    private final int b;
    private final int c;
    private final Rect d = new Rect();
    private final Rect e = new Rect();
    private final Paint f = new Paint();
    private float g;
    private float h = 1.0f;
    private float i;
    private float j;
    private float k = 0.0f;
    private float l = 0.0f;
    private int m = 244;

    public OuterHighlightDrawable(Context context) {
        if (PlatformVersion.h()) {
            TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(16843827, typedValue, true);
            a(ColorUtils.d(typedValue.data, 244));
        } else {
            a(context.getResources().getColor(R$color.cast_libraries_material_featurehighlight_outer_highlight_default_color));
        }
        this.f.setAntiAlias(true);
        this.f.setStyle(Paint.Style.FILL);
        Resources resources = context.getResources();
        this.f3797a = resources.getDimensionPixelSize(R$dimen.cast_libraries_material_featurehighlight_center_threshold);
        this.b = resources.getDimensionPixelSize(R$dimen.cast_libraries_material_featurehighlight_center_horizontal_offset);
        this.c = resources.getDimensionPixelSize(R$dimen.cast_libraries_material_featurehighlight_outer_padding);
    }

    public final void a(int i2) {
        this.f.setColor(i2);
        this.m = this.f.getAlpha();
        invalidateSelf();
    }

    public final float b() {
        return this.j;
    }

    public final int c() {
        return this.f.getColor();
    }

    public void draw(Canvas canvas) {
        canvas.drawCircle(this.i + this.k, this.j + this.l, this.g * this.h, this.f);
    }

    public int getAlpha() {
        return this.f.getAlpha();
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i2) {
        this.f.setAlpha(i2);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @Keep
    public void setScale(float f2) {
        this.h = f2;
        invalidateSelf();
    }

    @Keep
    public void setTranslationX(float f2) {
        this.k = f2;
        invalidateSelf();
    }

    @Keep
    public void setTranslationY(float f2) {
        this.l = f2;
        invalidateSelf();
    }

    public final Animator b(float f2, float f3) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("scale", new float[]{0.0f, 1.0f}), PropertyValuesHolder.ofFloat("translationX", new float[]{f2, 0.0f}), PropertyValuesHolder.ofFloat("translationY", new float[]{f3, 0.0f}), PropertyValuesHolder.ofInt("alpha", new int[]{0, this.m})});
        ofPropertyValuesHolder.setInterpolator(zzfl.zzfk());
        return ofPropertyValuesHolder.setDuration(350);
    }

    public final void a(Rect rect, Rect rect2) {
        float f2;
        this.d.set(rect);
        this.e.set(rect2);
        float exactCenterX = rect.exactCenterX();
        float exactCenterY = rect.exactCenterY();
        Rect bounds = getBounds();
        if (Math.min(exactCenterY - ((float) bounds.top), ((float) bounds.bottom) - exactCenterY) < ((float) this.f3797a)) {
            this.i = exactCenterX;
            this.j = exactCenterY;
        } else {
            if (exactCenterX <= bounds.exactCenterX()) {
                f2 = rect2.exactCenterX() + ((float) this.b);
            } else {
                f2 = rect2.exactCenterX() - ((float) this.b);
            }
            this.i = f2;
            this.j = rect2.exactCenterY();
        }
        this.g = ((float) this.c) + Math.max(a(this.i, this.j, rect), a(this.i, this.j, rect2));
        invalidateSelf();
    }

    public final float a() {
        return this.i;
    }

    public final boolean a(float f2, float f3) {
        return zzfn.zza(f2, f3, this.i, this.j) < this.g;
    }

    private static float a(float f2, float f3, Rect rect) {
        float f4 = (float) rect.left;
        float f5 = (float) rect.top;
        float f6 = (float) rect.right;
        float f7 = (float) rect.bottom;
        float zza = zzfn.zza(f2, f3, f4, f5);
        float zza2 = zzfn.zza(f2, f3, f6, f5);
        float zza3 = zzfn.zza(f2, f3, f6, f7);
        float zza4 = zzfn.zza(f2, f3, f4, f7);
        if (zza > zza2 && zza > zza3 && zza > zza4) {
            zza4 = zza;
        } else if (zza2 > zza3 && zza2 > zza4) {
            zza4 = zza2;
        } else if (zza3 > zza4) {
            zza4 = zza3;
        }
        return (float) Math.ceil((double) zza4);
    }
}
