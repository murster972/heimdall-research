package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzbz implements Parcelable.Creator<TextTrackStyle> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        float f = 0.0f;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    f = SafeParcelReader.u(parcel2, a2);
                    break;
                case 3:
                    i = SafeParcelReader.w(parcel2, a2);
                    break;
                case 4:
                    i2 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 5:
                    i3 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 6:
                    i4 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 7:
                    i5 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 8:
                    i6 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 9:
                    i7 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 10:
                    str = SafeParcelReader.o(parcel2, a2);
                    break;
                case 11:
                    i8 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 12:
                    i9 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 13:
                    str2 = SafeParcelReader.o(parcel2, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel2, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel2, b);
        return new TextTrackStyle(f, i, i2, i3, i4, i5, i6, i7, str, i8, i9, str2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new TextTrackStyle[i];
    }
}
