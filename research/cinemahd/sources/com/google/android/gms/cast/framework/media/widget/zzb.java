package com.google.android.gms.cast.framework.media.widget;

import android.view.View;

final class zzb implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ExpandedControllerActivity f3839a;

    zzb(ExpandedControllerActivity expandedControllerActivity) {
        this.f3839a = expandedControllerActivity;
    }

    public final void onClick(View view) {
        if (this.f3839a.E.isClickable()) {
            this.f3839a.c().u();
        }
    }
}
