package com.google.android.gms.cast.framework.media.uicontroller;

import android.view.View;

final class zzc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ UIMediaController f3827a;

    zzc(UIMediaController uIMediaController) {
        this.f3827a = uIMediaController;
    }

    public final void onClick(View view) {
        this.f3827a.g(view);
    }
}
