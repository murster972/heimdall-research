package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.cast.zzdk;
import org.json.JSONException;
import org.json.JSONObject;

public class VastAdsRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<VastAdsRequest> CREATOR = new zzca();

    /* renamed from: a  reason: collision with root package name */
    private final String f3780a;
    private final String b;

    VastAdsRequest(String str, String str2) {
        this.f3780a = str;
        this.b = str2;
    }

    public static VastAdsRequest a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        return new VastAdsRequest(jSONObject.optString("adTagUrl", (String) null), jSONObject.optString("adsResponse", (String) null));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VastAdsRequest)) {
            return false;
        }
        VastAdsRequest vastAdsRequest = (VastAdsRequest) obj;
        return zzdk.zza(this.f3780a, vastAdsRequest.f3780a) && zzdk.zza(this.b, vastAdsRequest.b);
    }

    public int hashCode() {
        return Objects.a(this.f3780a, this.b);
    }

    public String s() {
        return this.f3780a;
    }

    public String t() {
        return this.b;
    }

    public final JSONObject u() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.f3780a != null) {
                jSONObject.put("adTagUrl", this.f3780a);
            }
            if (this.b != null) {
                jSONObject.put("adsResponse", this.b);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, s(), false);
        SafeParcelWriter.a(parcel, 3, t(), false);
        SafeParcelWriter.a(parcel, a2);
    }
}
