package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class zzn implements Parcelable.Creator<CastDevice> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        ArrayList<WebImage> arrayList = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        byte[] bArr = null;
        String str9 = null;
        int i = 0;
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    str = SafeParcelReader.o(parcel2, a2);
                    break;
                case 3:
                    str2 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 4:
                    str3 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 5:
                    str4 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 6:
                    str5 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 7:
                    i = SafeParcelReader.w(parcel2, a2);
                    break;
                case 8:
                    arrayList = SafeParcelReader.c(parcel2, a2, WebImage.CREATOR);
                    break;
                case 9:
                    i2 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 10:
                    i3 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 11:
                    str6 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 12:
                    str7 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 13:
                    i4 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 14:
                    str8 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 15:
                    bArr = SafeParcelReader.g(parcel2, a2);
                    break;
                case 16:
                    str9 = SafeParcelReader.o(parcel2, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel2, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel2, b);
        return new CastDevice(str, str2, str3, str4, str5, i, arrayList, i2, i3, str6, str7, i4, str8, bArr, str9);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CastDevice[i];
    }
}
