package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.ads.AudienceNetworkActivity;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.internal.cast.zzdk;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaQueueItem extends AbstractSafeParcelable {
    public static final Parcelable.Creator<MediaQueueItem> CREATOR = new zzao();

    /* renamed from: a  reason: collision with root package name */
    private MediaInfo f3774a;
    private int b;
    private boolean c;
    private double d;
    private double e;
    private double f;
    private long[] g;
    private String h;
    private JSONObject i;

    MediaQueueItem(MediaInfo mediaInfo, int i2, boolean z, double d2, double d3, double d4, long[] jArr, String str) {
        this.f3774a = mediaInfo;
        this.b = i2;
        this.c = z;
        this.d = d2;
        this.e = d3;
        this.f = d4;
        this.g = jArr;
        this.h = str;
        String str2 = this.h;
        if (str2 != null) {
            try {
                this.i = new JSONObject(str2);
            } catch (JSONException unused) {
                this.i = null;
                this.h = null;
            }
        } else {
            this.i = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void A() throws IllegalArgumentException {
        if (this.f3774a == null) {
            throw new IllegalArgumentException("media cannot be null.");
        } else if (Double.isNaN(this.d) || this.d < 0.0d) {
            throw new IllegalArgumentException("startTime cannot be negative or NaN.");
        } else if (Double.isNaN(this.e)) {
            throw new IllegalArgumentException("playbackDuration cannot be NaN.");
        } else if (Double.isNaN(this.f) || this.f < 0.0d) {
            throw new IllegalArgumentException("preloadTime cannot be negative or Nan.");
        }
    }

    public final boolean a(JSONObject jSONObject) throws JSONException {
        boolean z;
        long[] jArr;
        boolean z2;
        int i2;
        boolean z3 = false;
        if (jSONObject.has("media")) {
            this.f3774a = new MediaInfo(jSONObject.getJSONObject("media"));
            z = true;
        } else {
            z = false;
        }
        if (jSONObject.has("itemId") && this.b != (i2 = jSONObject.getInt("itemId"))) {
            this.b = i2;
            z = true;
        }
        if (jSONObject.has(AudienceNetworkActivity.AUTOPLAY) && this.c != (z2 = jSONObject.getBoolean(AudienceNetworkActivity.AUTOPLAY))) {
            this.c = z2;
            z = true;
        }
        if (jSONObject.has("startTime")) {
            double d2 = jSONObject.getDouble("startTime");
            if (Math.abs(d2 - this.d) > 1.0E-7d) {
                this.d = d2;
                z = true;
            }
        }
        if (jSONObject.has("playbackDuration")) {
            double d3 = jSONObject.getDouble("playbackDuration");
            if (Math.abs(d3 - this.e) > 1.0E-7d) {
                this.e = d3;
                z = true;
            }
        }
        if (jSONObject.has("preloadTime")) {
            double d4 = jSONObject.getDouble("preloadTime");
            if (Math.abs(d4 - this.f) > 1.0E-7d) {
                this.f = d4;
                z = true;
            }
        }
        if (jSONObject.has("activeTrackIds")) {
            JSONArray jSONArray = jSONObject.getJSONArray("activeTrackIds");
            int length = jSONArray.length();
            jArr = new long[length];
            for (int i3 = 0; i3 < length; i3++) {
                jArr[i3] = jSONArray.getLong(i3);
            }
            long[] jArr2 = this.g;
            if (jArr2 != null && jArr2.length == length) {
                int i4 = 0;
                while (true) {
                    if (i4 >= length) {
                        break;
                    } else if (this.g[i4] != jArr[i4]) {
                        break;
                    } else {
                        i4++;
                    }
                }
            }
            z3 = true;
        } else {
            jArr = null;
        }
        if (z3) {
            this.g = jArr;
            z = true;
        }
        if (!jSONObject.has("customData")) {
            return z;
        }
        this.i = jSONObject.getJSONObject("customData");
        return true;
    }

    public boolean equals(Object obj) {
        JSONObject jSONObject;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaQueueItem)) {
            return false;
        }
        MediaQueueItem mediaQueueItem = (MediaQueueItem) obj;
        if ((this.i == null) != (mediaQueueItem.i == null)) {
            return false;
        }
        JSONObject jSONObject2 = this.i;
        return (jSONObject2 == null || (jSONObject = mediaQueueItem.i) == null || JsonUtils.a(jSONObject2, jSONObject)) && zzdk.zza(this.f3774a, mediaQueueItem.f3774a) && this.b == mediaQueueItem.b && this.c == mediaQueueItem.c && this.d == mediaQueueItem.d && this.e == mediaQueueItem.e && this.f == mediaQueueItem.f && Arrays.equals(this.g, mediaQueueItem.g);
    }

    public int hashCode() {
        return Objects.a(this.f3774a, Integer.valueOf(this.b), Boolean.valueOf(this.c), Double.valueOf(this.d), Double.valueOf(this.e), Double.valueOf(this.f), Integer.valueOf(Arrays.hashCode(this.g)), String.valueOf(this.i));
    }

    public long[] s() {
        return this.g;
    }

    public boolean t() {
        return this.c;
    }

    public int u() {
        return this.b;
    }

    public MediaInfo v() {
        return this.f3774a;
    }

    public double w() {
        return this.e;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        JSONObject jSONObject = this.i;
        this.h = jSONObject == null ? null : jSONObject.toString();
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, (Parcelable) v(), i2, false);
        SafeParcelWriter.a(parcel, 3, u());
        SafeParcelWriter.a(parcel, 4, t());
        SafeParcelWriter.a(parcel, 5, y());
        SafeParcelWriter.a(parcel, 6, w());
        SafeParcelWriter.a(parcel, 7, x());
        SafeParcelWriter.a(parcel, 8, s(), false);
        SafeParcelWriter.a(parcel, 9, this.h, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public double x() {
        return this.f;
    }

    public double y() {
        return this.d;
    }

    public final JSONObject z() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("media", this.f3774a.D());
            if (this.b != 0) {
                jSONObject.put("itemId", this.b);
            }
            jSONObject.put(AudienceNetworkActivity.AUTOPLAY, this.c);
            jSONObject.put("startTime", this.d);
            if (this.e != Double.POSITIVE_INFINITY) {
                jSONObject.put("playbackDuration", this.e);
            }
            jSONObject.put("preloadTime", this.f);
            if (this.g != null) {
                JSONArray jSONArray = new JSONArray();
                for (long put : this.g) {
                    jSONArray.put(put);
                }
                jSONObject.put("activeTrackIds", jSONArray);
            }
            if (this.i != null) {
                jSONObject.put("customData", this.i);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final MediaQueueItem f3775a;

        public Builder(MediaInfo mediaInfo) throws IllegalArgumentException {
            this.f3775a = new MediaQueueItem(mediaInfo);
        }

        public MediaQueueItem a() {
            this.f3775a.A();
            return this.f3775a;
        }

        public Builder(JSONObject jSONObject) throws JSONException {
            this.f3775a = new MediaQueueItem(jSONObject);
        }
    }

    private MediaQueueItem(MediaInfo mediaInfo) throws IllegalArgumentException {
        this(mediaInfo, 0, true, 0.0d, Double.POSITIVE_INFINITY, 0.0d, (long[]) null, (String) null);
        if (mediaInfo == null) {
            throw new IllegalArgumentException("media cannot be null.");
        }
    }

    MediaQueueItem(JSONObject jSONObject) throws JSONException {
        this((MediaInfo) null, 0, true, 0.0d, Double.POSITIVE_INFINITY, 0.0d, (long[]) null, (String) null);
        a(jSONObject);
    }
}
