package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzaw implements Parcelable.Creator<MediaTrack> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        long j = 0;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    j = SafeParcelReader.y(parcel2, a2);
                    break;
                case 3:
                    i = SafeParcelReader.w(parcel2, a2);
                    break;
                case 4:
                    str = SafeParcelReader.o(parcel2, a2);
                    break;
                case 5:
                    str2 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 6:
                    str3 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 7:
                    str4 = SafeParcelReader.o(parcel2, a2);
                    break;
                case 8:
                    i2 = SafeParcelReader.w(parcel2, a2);
                    break;
                case 9:
                    str5 = SafeParcelReader.o(parcel2, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel2, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel2, b);
        return new MediaTrack(j, i, str, str2, str3, str4, i2, str5);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new MediaTrack[i];
    }
}
