package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.cast.zzdw;
import com.google.android.gms.internal.cast.zze;

public abstract class Session {
    private static final zzdw c = new zzdw("Session");

    /* renamed from: a  reason: collision with root package name */
    private final zzt f3792a;
    private final zza b = new zza();

    private class zza extends zzac {
        private zza() {
        }

        public final void a(boolean z) {
            Session.this.a(z);
        }

        public final void b(Bundle bundle) {
            Session.this.a(bundle);
        }

        public final void c(Bundle bundle) {
            Session.this.c(bundle);
        }

        public final void e(Bundle bundle) {
            Session.this.d(bundle);
        }

        public final void f(Bundle bundle) {
            Session.this.b(bundle);
        }

        public final IObjectWrapper h() {
            return ObjectWrapper.a(Session.this);
        }

        public final long q() {
            return Session.this.a();
        }

        public final int zzs() {
            return 12451009;
        }
    }

    protected Session(Context context, String str, String str2) {
        this.f3792a = zze.zza(context, str, str2, (zzab) this.b);
    }

    public long a() {
        Preconditions.a("Must be called from the main thread.");
        return 0;
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public abstract void a(boolean z);

    /* access modifiers changed from: protected */
    public void b(Bundle bundle) {
    }

    public boolean b() {
        Preconditions.a("Must be called from the main thread.");
        try {
            return this.f3792a.isConnected();
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "isConnected", zzt.class.getSimpleName());
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void c(Bundle bundle);

    public boolean c() {
        Preconditions.a("Must be called from the main thread.");
        try {
            return this.f3792a.isConnecting();
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "isConnecting", zzt.class.getSimpleName());
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void d(Bundle bundle);

    public boolean d() {
        Preconditions.a("Must be called from the main thread.");
        try {
            return this.f3792a.n();
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "isResuming", zzt.class.getSimpleName());
            return false;
        }
    }

    public boolean e() {
        Preconditions.a("Must be called from the main thread.");
        try {
            return this.f3792a.r();
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "isSuspended", zzt.class.getSimpleName());
            return false;
        }
    }

    public final IObjectWrapper f() {
        try {
            return this.f3792a.j();
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "getWrappedObject", zzt.class.getSimpleName());
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        try {
            this.f3792a.c(i);
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "notifyFailedToResumeSession", zzt.class.getSimpleName());
        }
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        try {
            this.f3792a.f(i);
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "notifyFailedToStartSession", zzt.class.getSimpleName());
        }
    }

    /* access modifiers changed from: protected */
    public final void c(int i) {
        try {
            this.f3792a.e(i);
        } catch (RemoteException e) {
            c.zza(e, "Unable to call %s on %s.", "notifySessionEnded", zzt.class.getSimpleName());
        }
    }
}
