package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.common.zzb;
import com.google.android.gms.internal.common.zzd;

public final class zzk extends zzb implements zzl {
    zzk(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    public final IObjectWrapper a(IObjectWrapper iObjectWrapper, String str, int i, IObjectWrapper iObjectWrapper2) throws RemoteException {
        Parcel zza = zza();
        zzd.zza(zza, (IInterface) iObjectWrapper);
        zza.writeString(str);
        zza.writeInt(i);
        zzd.zza(zza, (IInterface) iObjectWrapper2);
        Parcel zza2 = zza(2, zza);
        IObjectWrapper a2 = IObjectWrapper.Stub.a(zza2.readStrongBinder());
        zza2.recycle();
        return a2;
    }

    public final IObjectWrapper b(IObjectWrapper iObjectWrapper, String str, int i, IObjectWrapper iObjectWrapper2) throws RemoteException {
        Parcel zza = zza();
        zzd.zza(zza, (IInterface) iObjectWrapper);
        zza.writeString(str);
        zza.writeInt(i);
        zzd.zza(zza, (IInterface) iObjectWrapper2);
        Parcel zza2 = zza(3, zza);
        IObjectWrapper a2 = IObjectWrapper.Stub.a(zza2.readStrongBinder());
        zza2.recycle();
        return a2;
    }
}
