package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class zzg implements DynamiteModule.VersionPolicy {
    zzg() {
    }

    public final DynamiteModule.VersionPolicy.zza a(Context context, String str, DynamiteModule.VersionPolicy.zzb zzb) throws DynamiteModule.LoadingException {
        DynamiteModule.VersionPolicy.zza zza = new DynamiteModule.VersionPolicy.zza();
        zza.f4034a = zzb.a(context, str);
        zza.b = zzb.a(context, str, true);
        if (zza.f4034a == 0 && zza.b == 0) {
            zza.c = 0;
        } else if (zza.b >= zza.f4034a) {
            zza.c = 1;
        } else {
            zza.c = -1;
        }
        return zza;
    }
}
