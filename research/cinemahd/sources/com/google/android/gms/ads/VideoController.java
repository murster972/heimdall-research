package com.google.android.gms.ads;

import android.os.RemoteException;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzxb;
import com.google.android.gms.internal.ads.zzyt;

public final class VideoController {
    public static final int PLAYBACK_STATE_ENDED = 3;
    public static final int PLAYBACK_STATE_PAUSED = 2;
    public static final int PLAYBACK_STATE_PLAYING = 1;
    public static final int PLAYBACK_STATE_READY = 5;
    public static final int PLAYBACK_STATE_UNKNOWN = 0;

    /* renamed from: a  reason: collision with root package name */
    private final Object f3688a = new Object();
    private zzxb b;
    private VideoLifecycleCallbacks c;

    public static abstract class VideoLifecycleCallbacks {
        public void onVideoEnd() {
        }

        public void onVideoMute(boolean z) {
        }

        public void onVideoPause() {
        }

        public void onVideoPlay() {
        }

        public void onVideoStart() {
        }
    }

    public final float getAspectRatio() {
        synchronized (this.f3688a) {
            if (this.b == null) {
                return 0.0f;
            }
            try {
                float aspectRatio = this.b.getAspectRatio();
                return aspectRatio;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getAspectRatio on video controller.", e);
                return 0.0f;
            }
        }
    }

    public final int getPlaybackState() {
        synchronized (this.f3688a) {
            if (this.b == null) {
                return 0;
            }
            try {
                int playbackState = this.b.getPlaybackState();
                return playbackState;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getPlaybackState on video controller.", e);
                return 0;
            }
        }
    }

    public final float getVideoCurrentTime() {
        synchronized (this.f3688a) {
            if (this.b == null) {
                return 0.0f;
            }
            try {
                float zzpl = this.b.zzpl();
                return zzpl;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getCurrentTime on video controller.", e);
                return 0.0f;
            }
        }
    }

    public final float getVideoDuration() {
        synchronized (this.f3688a) {
            if (this.b == null) {
                return 0.0f;
            }
            try {
                float zzpk = this.b.zzpk();
                return zzpk;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getDuration on video controller.", e);
                return 0.0f;
            }
        }
    }

    public final VideoLifecycleCallbacks getVideoLifecycleCallbacks() {
        VideoLifecycleCallbacks videoLifecycleCallbacks;
        synchronized (this.f3688a) {
            videoLifecycleCallbacks = this.c;
        }
        return videoLifecycleCallbacks;
    }

    public final boolean hasVideoContent() {
        boolean z;
        synchronized (this.f3688a) {
            z = this.b != null;
        }
        return z;
    }

    public final boolean isClickToExpandEnabled() {
        synchronized (this.f3688a) {
            if (this.b == null) {
                return false;
            }
            try {
                boolean isClickToExpandEnabled = this.b.isClickToExpandEnabled();
                return isClickToExpandEnabled;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call isClickToExpandEnabled.", e);
                return false;
            }
        }
    }

    public final boolean isCustomControlsEnabled() {
        synchronized (this.f3688a) {
            if (this.b == null) {
                return false;
            }
            try {
                boolean isCustomControlsEnabled = this.b.isCustomControlsEnabled();
                return isCustomControlsEnabled;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call isUsingCustomPlayerControls.", e);
                return false;
            }
        }
    }

    public final boolean isMuted() {
        synchronized (this.f3688a) {
            if (this.b == null) {
                return true;
            }
            try {
                boolean isMuted = this.b.isMuted();
                return isMuted;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call isMuted on video controller.", e);
                return true;
            }
        }
    }

    public final void mute(boolean z) {
        synchronized (this.f3688a) {
            if (this.b != null) {
                try {
                    this.b.mute(z);
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call mute on video controller.", e);
                }
            }
        }
    }

    public final void pause() {
        synchronized (this.f3688a) {
            if (this.b != null) {
                try {
                    this.b.pause();
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call pause on video controller.", e);
                }
            }
        }
    }

    public final void play() {
        synchronized (this.f3688a) {
            if (this.b != null) {
                try {
                    this.b.play();
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call play on video controller.", e);
                }
            }
        }
    }

    public final void setVideoLifecycleCallbacks(VideoLifecycleCallbacks videoLifecycleCallbacks) {
        Preconditions.a(videoLifecycleCallbacks, (Object) "VideoLifecycleCallbacks may not be null.");
        synchronized (this.f3688a) {
            this.c = videoLifecycleCallbacks;
            if (this.b != null) {
                try {
                    this.b.zza(new zzyt(videoLifecycleCallbacks));
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call setVideoLifecycleCallbacks on video controller.", e);
                }
            }
        }
    }

    public final void stop() {
        synchronized (this.f3688a) {
            if (this.b != null) {
                try {
                    this.b.stop();
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call stop on video controller.", e);
                }
            }
        }
    }

    public final void zza(zzxb zzxb) {
        synchronized (this.f3688a) {
            this.b = zzxb;
            if (this.c != null) {
                setVideoLifecycleCallbacks(this.c);
            }
        }
    }

    public final zzxb zzdl() {
        zzxb zzxb;
        synchronized (this.f3688a) {
            zzxb = this.b;
        }
        return zzxb;
    }
}
