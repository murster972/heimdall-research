package com.google.android.gms.ads.formats;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.ads.zzadz;
import com.google.android.gms.internal.ads.zzaea;
import com.google.android.gms.internal.ads.zzul;
import com.google.android.gms.internal.ads.zzwb;
import com.google.android.gms.internal.ads.zzwc;
import com.google.android.gms.internal.ads.zzyu;

public final class PublisherAdViewOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<PublisherAdViewOptions> CREATOR = new zzc();

    /* renamed from: a  reason: collision with root package name */
    private final boolean f3701a;
    private final zzwc b;
    private AppEventListener c;
    private final IBinder d;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public boolean f3702a = false;
        /* access modifiers changed from: private */
        public AppEventListener b;
        /* access modifiers changed from: private */
        public ShouldDelayBannerRenderingListener c;

        public final PublisherAdViewOptions build() {
            return new PublisherAdViewOptions(this);
        }

        public final Builder setAppEventListener(AppEventListener appEventListener) {
            this.b = appEventListener;
            return this;
        }

        public final Builder setManualImpressionsEnabled(boolean z) {
            this.f3702a = z;
            return this;
        }

        public final Builder setShouldDelayBannerRenderingListener(ShouldDelayBannerRenderingListener shouldDelayBannerRenderingListener) {
            this.c = shouldDelayBannerRenderingListener;
            return this;
        }
    }

    private PublisherAdViewOptions(Builder builder) {
        this.f3701a = builder.f3702a;
        this.c = builder.b;
        AppEventListener appEventListener = this.c;
        zzyu zzyu = null;
        this.b = appEventListener != null ? new zzul(appEventListener) : null;
        this.d = builder.c != null ? new zzyu(builder.c) : zzyu;
    }

    public final AppEventListener getAppEventListener() {
        return this.c;
    }

    public final boolean getManualImpressionsEnabled() {
        return this.f3701a;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, getManualImpressionsEnabled());
        zzwc zzwc = this.b;
        SafeParcelWriter.a(parcel, 2, zzwc == null ? null : zzwc.asBinder(), false);
        SafeParcelWriter.a(parcel, 3, this.d, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public final zzwc zzjm() {
        return this.b;
    }

    public final zzaea zzjn() {
        return zzadz.zzw(this.d);
    }

    PublisherAdViewOptions(boolean z, IBinder iBinder, IBinder iBinder2) {
        this.f3701a = z;
        this.b = iBinder != null ? zzwb.zze(iBinder) : null;
        this.d = iBinder2;
    }
}
