package com.google.android.gms.ads.query;

import android.content.Context;

public class NativeQueryDataConfiguration extends QueryDataConfiguration {
    public NativeQueryDataConfiguration(Context context, String str) {
        super(context, str);
    }
}
