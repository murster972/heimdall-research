package com.google.android.gms.ads.formats;

import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.internal.ads.zzabt;

final /* synthetic */ class zze implements zzabt {

    /* renamed from: a  reason: collision with root package name */
    private final UnifiedNativeAdView f3705a;

    zze(UnifiedNativeAdView unifiedNativeAdView) {
        this.f3705a = unifiedNativeAdView;
    }

    public final void setMediaContent(UnifiedNativeAd.MediaContent mediaContent) {
        this.f3705a.a(mediaContent);
    }
}
