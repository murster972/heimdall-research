package com.google.android.gms.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.mediation.MediationExtrasReceiver;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.query.AdData;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzxj;
import com.google.android.gms.internal.ads.zzxm;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Date;
import java.util.Set;

public final class AdRequest {
    public static final String DEVICE_ID_EMULATOR = "B3EEABB8EE11C2BE770B684D95219ECB";
    public static final int ERROR_CODE_INTERNAL_ERROR = 0;
    public static final int ERROR_CODE_INVALID_REQUEST = 1;
    public static final int ERROR_CODE_NETWORK_ERROR = 2;
    public static final int ERROR_CODE_NO_FILL = 3;
    public static final int GENDER_FEMALE = 2;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_UNKNOWN = 0;
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_G = "G";
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_MA = "MA";
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_PG = "PG";
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_T = "T";
    public static final int MAX_CONTENT_URL_LENGTH = 512;
    @Deprecated
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_FALSE = 0;
    @Deprecated
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_TRUE = 1;
    @Deprecated
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_UNSPECIFIED = -1;

    /* renamed from: a  reason: collision with root package name */
    private final zzxj f3679a;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final zzxm f3680a = new zzxm();

        public Builder() {
            this.f3680a.zzcg("B3EEABB8EE11C2BE770B684D95219ECB");
        }

        public final Builder addCustomEventExtrasBundle(Class<? extends CustomEvent> cls, Bundle bundle) {
            this.f3680a.zzb(cls, bundle);
            return this;
        }

        public final Builder addKeyword(String str) {
            this.f3680a.zzcf(str);
            return this;
        }

        public final Builder addNetworkExtras(NetworkExtras networkExtras) {
            this.f3680a.zza(networkExtras);
            return this;
        }

        public final Builder addNetworkExtrasBundle(Class<? extends MediationExtrasReceiver> cls, Bundle bundle) {
            this.f3680a.zza(cls, bundle);
            if (cls.equals(AdMobAdapter.class) && bundle.getBoolean("_emulatorLiveAds")) {
                this.f3680a.zzch("B3EEABB8EE11C2BE770B684D95219ECB");
            }
            return this;
        }

        public final Builder addTestDevice(String str) {
            this.f3680a.zzcg(str);
            return this;
        }

        public final AdRequest build() {
            return new AdRequest(this);
        }

        public final Builder setAdData(AdData adData) {
            this.f3680a.zza(adData);
            return this;
        }

        @Deprecated
        public final Builder setBirthday(Date date) {
            this.f3680a.zza(date);
            return this;
        }

        public final Builder setContentUrl(String str) {
            Preconditions.a(str, (Object) "Content URL must be non-null.");
            Preconditions.a(str, (Object) "Content URL must be non-empty.");
            Preconditions.a(str.length() <= 512, "Content URL must not exceed %d in length.  Provided length was %d.", Integer.valueOf(AdRequest.MAX_CONTENT_URL_LENGTH), Integer.valueOf(str.length()));
            this.f3680a.zzci(str);
            return this;
        }

        @Deprecated
        public final Builder setGender(int i) {
            this.f3680a.zzcl(i);
            return this;
        }

        @Deprecated
        public final Builder setIsDesignedForFamilies(boolean z) {
            this.f3680a.zzaa(z);
            return this;
        }

        public final Builder setLocation(Location location) {
            this.f3680a.zza(location);
            return this;
        }

        @Deprecated
        public final Builder setMaxAdContentRating(String str) {
            this.f3680a.zzcm(str);
            return this;
        }

        public final Builder setRequestAgent(String str) {
            this.f3680a.zzck(str);
            return this;
        }

        @Deprecated
        public final Builder setTagForUnderAgeOfConsent(int i) {
            this.f3680a.zzcm(i);
            return this;
        }

        @Deprecated
        public final Builder tagForChildDirectedTreatment(boolean z) {
            this.f3680a.zzz(z);
            return this;
        }
    }

    @Deprecated
    @Retention(RetentionPolicy.SOURCE)
    public @interface MaxAdContentRating {
    }

    @Deprecated
    @Retention(RetentionPolicy.SOURCE)
    public @interface TagForUnderAgeOfConsent {
    }

    private AdRequest(Builder builder) {
        this.f3679a = new zzxj(builder.f3680a);
    }

    @Deprecated
    public final Date getBirthday() {
        return this.f3679a.getBirthday();
    }

    public final String getContentUrl() {
        return this.f3679a.getContentUrl();
    }

    public final <T extends CustomEvent> Bundle getCustomEventExtrasBundle(Class<T> cls) {
        return this.f3679a.getCustomEventExtrasBundle(cls);
    }

    @Deprecated
    public final int getGender() {
        return this.f3679a.getGender();
    }

    public final Set<String> getKeywords() {
        return this.f3679a.getKeywords();
    }

    public final Location getLocation() {
        return this.f3679a.getLocation();
    }

    @Deprecated
    public final <T extends NetworkExtras> T getNetworkExtras(Class<T> cls) {
        return this.f3679a.getNetworkExtras(cls);
    }

    public final <T extends MediationExtrasReceiver> Bundle getNetworkExtrasBundle(Class<T> cls) {
        return this.f3679a.getNetworkExtrasBundle(cls);
    }

    public final boolean isTestDevice(Context context) {
        return this.f3679a.isTestDevice(context);
    }

    public final zzxj zzdg() {
        return this.f3679a;
    }
}
