package com.google.android.gms.ads.rewarded;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzasv;

public final class RewardedAd {

    /* renamed from: a  reason: collision with root package name */
    private final zzasv f3744a;

    public RewardedAd(Context context, String str) {
        Preconditions.a(context, (Object) "context cannot be null");
        Preconditions.a(str, (Object) "adUnitID cannot be null");
        this.f3744a = new zzasv(context, str);
    }

    public final Bundle getAdMetadata() {
        return this.f3744a.getAdMetadata();
    }

    public final String getMediationAdapterClassName() {
        return this.f3744a.getMediationAdapterClassName();
    }

    public final RewardItem getRewardItem() {
        return this.f3744a.getRewardItem();
    }

    public final boolean isLoaded() {
        return this.f3744a.isLoaded();
    }

    public final void loadAd(AdRequest adRequest, RewardedAdLoadCallback rewardedAdLoadCallback) {
        this.f3744a.zza(adRequest.zzdg(), rewardedAdLoadCallback);
    }

    public final void setOnAdMetadataChangedListener(OnAdMetadataChangedListener onAdMetadataChangedListener) {
        this.f3744a.setOnAdMetadataChangedListener(onAdMetadataChangedListener);
    }

    public final void setServerSideVerificationOptions(ServerSideVerificationOptions serverSideVerificationOptions) {
        this.f3744a.setServerSideVerificationOptions(serverSideVerificationOptions);
    }

    public final void show(Activity activity, RewardedAdCallback rewardedAdCallback) {
        this.f3744a.show(activity, rewardedAdCallback);
    }

    public final void loadAd(PublisherAdRequest publisherAdRequest, RewardedAdLoadCallback rewardedAdLoadCallback) {
        this.f3744a.zza(publisherAdRequest.zzdg(), rewardedAdLoadCallback);
    }

    public final void show(Activity activity, RewardedAdCallback rewardedAdCallback, boolean z) {
        this.f3744a.show(activity, rewardedAdCallback, z);
    }
}
