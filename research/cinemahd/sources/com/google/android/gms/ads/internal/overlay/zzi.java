package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.google.android.gms.internal.ads.zzbdi;

public final class zzi {
    public final int index;
    public final ViewGroup parent;
    public final ViewGroup.LayoutParams zzdhj;
    public final Context zzup;

    public zzi(zzbdi zzbdi) throws zzg {
        this.zzdhj = zzbdi.getLayoutParams();
        ViewParent parent2 = zzbdi.getParent();
        this.zzup = zzbdi.zzzv();
        if (parent2 == null || !(parent2 instanceof ViewGroup)) {
            throw new zzg("Could not get the parent of the WebView for an overlay.");
        }
        this.parent = (ViewGroup) parent2;
        this.index = this.parent.indexOfChild(zzbdi.getView());
        this.parent.removeView(zzbdi.getView());
        zzbdi.zzax(true);
    }
}
