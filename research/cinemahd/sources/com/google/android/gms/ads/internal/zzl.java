package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.applovin.sdk.AppLovinEventParameters;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaak;
import com.google.android.gms.internal.ads.zzaar;
import com.google.android.gms.internal.ads.zzaoy;
import com.google.android.gms.internal.ads.zzape;
import com.google.android.gms.internal.ads.zzaro;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzazd;
import com.google.android.gms.internal.ads.zzdq;
import com.google.android.gms.internal.ads.zzdt;
import com.google.android.gms.internal.ads.zzrg;
import com.google.android.gms.internal.ads.zzug;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzuo;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzvg;
import com.google.android.gms.internal.ads.zzvh;
import com.google.android.gms.internal.ads.zzvt;
import com.google.android.gms.internal.ads.zzvx;
import com.google.android.gms.internal.ads.zzwc;
import com.google.android.gms.internal.ads.zzwi;
import com.google.android.gms.internal.ads.zzxa;
import com.google.android.gms.internal.ads.zzxb;
import com.google.android.gms.internal.ads.zzxh;
import com.google.android.gms.internal.ads.zzyw;
import java.util.Map;
import java.util.concurrent.Future;

public final class zzl extends zzvt {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final zzazb f3722a;
    private final zzuj b;
    /* access modifiers changed from: private */
    public final Future<zzdq> c = zzazd.zzdwe.zzd(new zzm(this));
    /* access modifiers changed from: private */
    public final Context d;
    private final zzo e;
    /* access modifiers changed from: private */
    public WebView f = new WebView(this.d);
    /* access modifiers changed from: private */
    public zzvh g;
    /* access modifiers changed from: private */
    public zzdq h;
    private AsyncTask<Void, Void, String> i;

    public zzl(Context context, zzuj zzuj, String str, zzazb zzazb) {
        this.d = context;
        this.f3722a = zzazb;
        this.b = zzuj;
        this.e = new zzo(str);
        g(0);
        this.f.setVerticalScrollBarEnabled(false);
        this.f.getSettings().setJavaScriptEnabled(true);
        this.f.setWebViewClient(new zzk(this));
        this.f.setOnTouchListener(new zzn(this));
    }

    /* access modifiers changed from: private */
    public final String c(String str) {
        if (this.h == null) {
            return str;
        }
        Uri parse = Uri.parse(str);
        try {
            parse = this.h.zza(parse, this.d, (View) null, (Activity) null);
        } catch (zzdt e2) {
            zzayu.zzd("Unable to process ad data", e2);
        }
        return parse.toString();
    }

    /* access modifiers changed from: private */
    public final void d(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        this.d.startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    public final int b(String str) {
        String queryParameter = Uri.parse(str).getQueryParameter("height");
        if (TextUtils.isEmpty(queryParameter)) {
            return 0;
        }
        try {
            zzve.zzou();
            return zzayk.zza(this.d, Integer.parseInt(queryParameter));
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public final void destroy() throws RemoteException {
        Preconditions.a("destroy must be called on the main UI thread.");
        this.i.cancel(true);
        this.c.cancel(true);
        this.f.destroy();
        this.f = null;
    }

    /* access modifiers changed from: package-private */
    public final void g(int i2) {
        if (this.f != null) {
            this.f.setLayoutParams(new ViewGroup.LayoutParams(-1, i2));
        }
    }

    public final Bundle getAdMetadata() {
        throw new IllegalStateException("Unused method");
    }

    public final String getAdUnitId() {
        throw new IllegalStateException("getAdUnitId not implemented");
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return null;
    }

    public final zzxb getVideoController() {
        return null;
    }

    public final boolean isLoading() throws RemoteException {
        return false;
    }

    public final boolean isReady() throws RemoteException {
        return false;
    }

    public final void pause() throws RemoteException {
        Preconditions.a("pause must be called on the main UI thread.");
    }

    public final void resume() throws RemoteException {
        Preconditions.a("resume must be called on the main UI thread.");
    }

    public final void setImmersiveMode(boolean z) {
        throw new IllegalStateException("Unused method");
    }

    public final void setManualImpressionsEnabled(boolean z) throws RemoteException {
    }

    public final void setUserId(String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void showInterstitial() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void stopLoading() throws RemoteException {
    }

    /* access modifiers changed from: package-private */
    public final String w() {
        String b2 = this.e.b();
        if (TextUtils.isEmpty(b2)) {
            b2 = "www.google.com";
        }
        String str = zzaar.zzcso.get();
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 8 + String.valueOf(str).length());
        sb.append("https://");
        sb.append(b2);
        sb.append(str);
        return sb.toString();
    }

    public final boolean zza(zzug zzug) throws RemoteException {
        Preconditions.a(this.f, (Object) "This Search Ad has already been torn down");
        this.e.a(zzug, this.f3722a);
        this.i = new zzp(this, (zzk) null).execute(new Void[0]);
        return true;
    }

    public final void zzbr(String str) {
        throw new IllegalStateException("Unused method");
    }

    public final IObjectWrapper zzjx() throws RemoteException {
        Preconditions.a("getAdFrame must be called on the main UI thread.");
        return ObjectWrapper.a(this.f);
    }

    public final void zzjy() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final zzuj zzjz() throws RemoteException {
        return this.b;
    }

    public final String zzka() throws RemoteException {
        return null;
    }

    public final zzxa zzkb() {
        return null;
    }

    public final zzwc zzkc() {
        throw new IllegalStateException("getIAppEventListener not implemented");
    }

    public final zzvh zzkd() {
        throw new IllegalStateException("getIAdListener not implemented");
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https://").appendEncodedPath(zzaar.zzcso.get());
        builder.appendQueryParameter(AppLovinEventParameters.SEARCH_QUERY, this.e.a());
        builder.appendQueryParameter("pubId", this.e.c());
        Map<String, String> d2 = this.e.d();
        for (String next : d2.keySet()) {
            builder.appendQueryParameter(next, d2.get(next));
        }
        Uri build = builder.build();
        zzdq zzdq = this.h;
        if (zzdq != null) {
            try {
                build = zzdq.zza(build, this.d);
            } catch (zzdt e2) {
                zzayu.zzd("Unable to process ad data", e2);
            }
        }
        String w = w();
        String encodedQuery = build.getEncodedQuery();
        StringBuilder sb = new StringBuilder(String.valueOf(w).length() + 1 + String.valueOf(encodedQuery).length());
        sb.append(w);
        sb.append("#");
        sb.append(encodedQuery);
        return sb.toString();
    }

    public final void zza(zzvh zzvh) throws RemoteException {
        this.g = zzvh;
    }

    public final void zza(zzwc zzwc) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzvx zzvx) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzuj zzuj) throws RemoteException {
        throw new IllegalStateException("AdSize must be set before initialization");
    }

    public final void zza(zzaoy zzaoy) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzape zzape, String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzaak zzaak) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzvg zzvg) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzwi zzwi) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzaro zzaro) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzyw zzyw) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzxh zzxh) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzuo zzuo) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzrg zzrg) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }
}
