package com.google.android.gms.ads.query;

import android.content.Context;

public class RewardedQueryDataConfiguration extends QueryDataConfiguration {
    public RewardedQueryDataConfiguration(Context context, String str) {
        super(context, str);
    }
}
