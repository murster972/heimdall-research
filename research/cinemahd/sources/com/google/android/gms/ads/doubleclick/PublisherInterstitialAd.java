package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzxn;

public final class PublisherInterstitialAd {

    /* renamed from: a  reason: collision with root package name */
    private final zzxn f3695a;

    public PublisherInterstitialAd(Context context) {
        this.f3695a = new zzxn(context, this);
        Preconditions.a(context, (Object) "Context cannot be null");
    }

    public final AdListener getAdListener() {
        return this.f3695a.getAdListener();
    }

    public final String getAdUnitId() {
        return this.f3695a.getAdUnitId();
    }

    public final AppEventListener getAppEventListener() {
        return this.f3695a.getAppEventListener();
    }

    public final String getMediationAdapterClassName() {
        return this.f3695a.getMediationAdapterClassName();
    }

    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.f3695a.getOnCustomRenderedAdLoadedListener();
    }

    public final boolean isLoaded() {
        return this.f3695a.isLoaded();
    }

    public final boolean isLoading() {
        return this.f3695a.isLoading();
    }

    public final void loadAd(PublisherAdRequest publisherAdRequest) {
        this.f3695a.zza(publisherAdRequest.zzdg());
    }

    public final void setAdListener(AdListener adListener) {
        this.f3695a.setAdListener(adListener);
    }

    public final void setAdUnitId(String str) {
        this.f3695a.setAdUnitId(str);
    }

    public final void setAppEventListener(AppEventListener appEventListener) {
        this.f3695a.setAppEventListener(appEventListener);
    }

    @Deprecated
    public final void setCorrelator(Correlator correlator) {
    }

    public final void setImmersiveMode(boolean z) {
        this.f3695a.setImmersiveMode(z);
    }

    public final void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.f3695a.setOnCustomRenderedAdLoadedListener(onCustomRenderedAdLoadedListener);
    }

    public final void show() {
        this.f3695a.show();
    }
}
