package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzve;

public final class zzq extends FrameLayout implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final ImageButton f3714a;
    private final zzy b;

    public zzq(Context context, zzp zzp, zzy zzy) {
        super(context);
        this.b = zzy;
        setOnClickListener(this);
        this.f3714a = new ImageButton(context);
        this.f3714a.setImageResource(17301527);
        this.f3714a.setBackgroundColor(0);
        this.f3714a.setOnClickListener(this);
        ImageButton imageButton = this.f3714a;
        zzve.zzou();
        int zza = zzayk.zza(context, zzp.paddingLeft);
        zzve.zzou();
        int zza2 = zzayk.zza(context, 0);
        zzve.zzou();
        int zza3 = zzayk.zza(context, zzp.paddingRight);
        zzve.zzou();
        imageButton.setPadding(zza, zza2, zza3, zzayk.zza(context, zzp.paddingBottom));
        this.f3714a.setContentDescription("Interstitial close button");
        ImageButton imageButton2 = this.f3714a;
        zzve.zzou();
        int zza4 = zzayk.zza(context, zzp.size + zzp.paddingLeft + zzp.paddingRight);
        zzve.zzou();
        addView(imageButton2, new FrameLayout.LayoutParams(zza4, zzayk.zza(context, zzp.size + zzp.paddingBottom), 17));
    }

    public final void onClick(View view) {
        zzy zzy = this.b;
        if (zzy != null) {
            zzy.zztl();
        }
    }

    public final void zzal(boolean z) {
        if (z) {
            this.f3714a.setVisibility(8);
        } else {
            this.f3714a.setVisibility(0);
        }
    }
}
