package com.google.android.gms.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.formats.OnPublisherAdViewLoadedListener;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzaby;
import com.google.android.gms.internal.ads.zzadi;
import com.google.android.gms.internal.ads.zzadj;
import com.google.android.gms.internal.ads.zzadv;
import com.google.android.gms.internal.ads.zzael;
import com.google.android.gms.internal.ads.zzaen;
import com.google.android.gms.internal.ads.zzaeo;
import com.google.android.gms.internal.ads.zzaep;
import com.google.android.gms.internal.ads.zzaeq;
import com.google.android.gms.internal.ads.zzaer;
import com.google.android.gms.internal.ads.zzakz;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzuc;
import com.google.android.gms.internal.ads.zzuh;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzvh;
import com.google.android.gms.internal.ads.zzvm;
import com.google.android.gms.internal.ads.zzvn;
import com.google.android.gms.internal.ads.zzxj;

public class AdLoader {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3677a;
    private final zzvm b;

    AdLoader(Context context, zzvm zzvm) {
        this(context, zzvm, zzuh.zzccn);
    }

    private final void a(zzxj zzxj) {
        try {
            this.b.zzb(zzuh.zza(this.f3677a, zzxj));
        } catch (RemoteException e) {
            zzayu.zzc("Failed to load ad.", e);
        }
    }

    @Deprecated
    public String getMediationAdapterClassName() {
        try {
            return this.b.zzka();
        } catch (RemoteException e) {
            zzayu.zzd("Failed to get the mediation adapter class name.", e);
            return null;
        }
    }

    public boolean isLoading() {
        try {
            return this.b.isLoading();
        } catch (RemoteException e) {
            zzayu.zzd("Failed to check if ad is loading.", e);
            return false;
        }
    }

    public void loadAd(AdRequest adRequest) {
        a(adRequest.zzdg());
    }

    public void loadAds(AdRequest adRequest, int i) {
        try {
            this.b.zza(zzuh.zza(this.f3677a, adRequest.zzdg()), i);
        } catch (RemoteException e) {
            zzayu.zzc("Failed to load ads.", e);
        }
    }

    public void loadAd(PublisherAdRequest publisherAdRequest) {
        a(publisherAdRequest.zzdg());
    }

    private AdLoader(Context context, zzvm zzvm, zzuh zzuh) {
        this.f3677a = context;
        this.b = zzvm;
    }

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Context f3678a;
        private final zzvn b;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Builder(Context context, String str) {
            this(context, zzve.zzov().zzb(context, str, new zzakz()));
            Preconditions.a(context, (Object) "context cannot be null");
        }

        public AdLoader build() {
            try {
                return new AdLoader(this.f3678a, this.b.zzpd());
            } catch (RemoteException e) {
                zzayu.zzc("Failed to build AdLoader.", e);
                return null;
            }
        }

        @Deprecated
        public Builder forAppInstallAd(NativeAppInstallAd.OnAppInstallAdLoadedListener onAppInstallAdLoadedListener) {
            try {
                this.b.zza((zzadi) new zzael(onAppInstallAdLoadedListener));
            } catch (RemoteException e) {
                zzayu.zzd("Failed to add app install ad listener", e);
            }
            return this;
        }

        @Deprecated
        public Builder forContentAd(NativeContentAd.OnContentAdLoadedListener onContentAdLoadedListener) {
            try {
                this.b.zza((zzadj) new zzaeo(onContentAdLoadedListener));
            } catch (RemoteException e) {
                zzayu.zzd("Failed to add content ad listener", e);
            }
            return this;
        }

        public Builder forCustomTemplateAd(String str, NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener onCustomTemplateAdLoadedListener, NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener) {
            zzaen zzaen;
            try {
                zzvn zzvn = this.b;
                zzaeq zzaeq = new zzaeq(onCustomTemplateAdLoadedListener);
                if (onCustomClickListener == null) {
                    zzaen = null;
                } else {
                    zzaen = new zzaen(onCustomClickListener);
                }
                zzvn.zza(str, zzaeq, zzaen);
            } catch (RemoteException e) {
                zzayu.zzd("Failed to add custom template ad listener", e);
            }
            return this;
        }

        public Builder forPublisherAdView(OnPublisherAdViewLoadedListener onPublisherAdViewLoadedListener, AdSize... adSizeArr) {
            if (adSizeArr == null || adSizeArr.length <= 0) {
                throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
            }
            try {
                this.b.zza(new zzaep(onPublisherAdViewLoadedListener), new zzuj(this.f3678a, adSizeArr));
            } catch (RemoteException e) {
                zzayu.zzd("Failed to add publisher banner ad listener", e);
            }
            return this;
        }

        public Builder forUnifiedNativeAd(UnifiedNativeAd.OnUnifiedNativeAdLoadedListener onUnifiedNativeAdLoadedListener) {
            try {
                this.b.zza((zzadv) new zzaer(onUnifiedNativeAdLoadedListener));
            } catch (RemoteException e) {
                zzayu.zzd("Failed to add google native ad listener", e);
            }
            return this;
        }

        public Builder withAdListener(AdListener adListener) {
            try {
                this.b.zzb((zzvh) new zzuc(adListener));
            } catch (RemoteException e) {
                zzayu.zzd("Failed to set AdListener.", e);
            }
            return this;
        }

        @Deprecated
        public Builder withCorrelator(Correlator correlator) {
            return this;
        }

        public Builder withNativeAdOptions(NativeAdOptions nativeAdOptions) {
            try {
                this.b.zza(new zzaby(nativeAdOptions));
            } catch (RemoteException e) {
                zzayu.zzd("Failed to specify native ad options", e);
            }
            return this;
        }

        public Builder withPublisherAdViewOptions(PublisherAdViewOptions publisherAdViewOptions) {
            try {
                this.b.zza(publisherAdViewOptions);
            } catch (RemoteException e) {
                zzayu.zzd("Failed to specify DFP banner ad options", e);
            }
            return this;
        }

        private Builder(Context context, zzvn zzvn) {
            this.f3678a = context;
            this.b = zzvn;
        }
    }
}
