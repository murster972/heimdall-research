package com.google.android.gms.ads.formats;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzacp;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzve;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class NativeAdViewHolder {
    public static WeakHashMap<View, NativeAdViewHolder> zzbkd = new WeakHashMap<>();

    /* renamed from: a  reason: collision with root package name */
    private zzacp f3700a;
    private WeakReference<View> b;

    public NativeAdViewHolder(View view, Map<String, View> map, Map<String, View> map2) {
        Preconditions.a(view, (Object) "ContainerView must not be null");
        if ((view instanceof NativeAdView) || (view instanceof UnifiedNativeAdView)) {
            zzayu.zzex("The provided containerView is of type of NativeAdView, which cannot be usedwith NativeAdViewHolder.");
        } else if (zzbkd.get(view) != null) {
            zzayu.zzex("The provided containerView is already in use with another NativeAdViewHolder.");
        } else {
            zzbkd.put(view, this);
            this.b = new WeakReference<>(view);
            this.f3700a = zzve.zzov().zza(view, a(map), a(map2));
        }
    }

    private static HashMap<String, View> a(Map<String, View> map) {
        if (map == null) {
            return new HashMap<>();
        }
        return new HashMap<>(map);
    }

    public final void setClickConfirmingView(View view) {
        try {
            this.f3700a.zze(ObjectWrapper.a(view));
        } catch (RemoteException e) {
            zzayu.zzc("Unable to call setClickConfirmingView on delegate", e);
        }
    }

    public final void setNativeAd(NativeAd nativeAd) {
        a((IObjectWrapper) nativeAd.zzjj());
    }

    public final void unregisterNativeAd() {
        zzacp zzacp = this.f3700a;
        if (zzacp != null) {
            try {
                zzacp.unregisterNativeAd();
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call unregisterNativeAd on delegate", e);
            }
        }
        WeakReference<View> weakReference = this.b;
        View view = weakReference != null ? (View) weakReference.get() : null;
        if (view != null) {
            zzbkd.remove(view);
        }
    }

    public final void setNativeAd(UnifiedNativeAd unifiedNativeAd) {
        a((IObjectWrapper) unifiedNativeAd.zzjj());
    }

    private final void a(IObjectWrapper iObjectWrapper) {
        WeakReference<View> weakReference = this.b;
        View view = weakReference != null ? (View) weakReference.get() : null;
        if (view == null) {
            zzayu.zzez("NativeAdViewHolder.setNativeAd containerView doesn't exist, returning");
            return;
        }
        if (!zzbkd.containsKey(view)) {
            zzbkd.put(view, this);
        }
        zzacp zzacp = this.f3700a;
        if (zzacp != null) {
            try {
                zzacp.zza(iObjectWrapper);
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call setNativeAd on delegate", e);
            }
        }
    }
}
