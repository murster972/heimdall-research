package com.google.android.gms.ads.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzg extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzg> CREATOR = new zzj();

    /* renamed from: a  reason: collision with root package name */
    private final String f3719a;
    public final boolean zzbkx;
    public final boolean zzbky;
    public final boolean zzbla;
    public final float zzblb;
    public final int zzblc;
    public final boolean zzbld;
    public final boolean zzble;
    public final boolean zzblf;

    public zzg(boolean z, boolean z2, boolean z3, float f, int i, boolean z4, boolean z5, boolean z6) {
        this(false, z2, (String) null, false, 0.0f, -1, z4, z5, z6);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.zzbkx);
        SafeParcelWriter.a(parcel, 3, this.zzbky);
        SafeParcelWriter.a(parcel, 4, this.f3719a, false);
        SafeParcelWriter.a(parcel, 5, this.zzbla);
        SafeParcelWriter.a(parcel, 6, this.zzblb);
        SafeParcelWriter.a(parcel, 7, this.zzblc);
        SafeParcelWriter.a(parcel, 8, this.zzbld);
        SafeParcelWriter.a(parcel, 9, this.zzble);
        SafeParcelWriter.a(parcel, 10, this.zzblf);
        SafeParcelWriter.a(parcel, a2);
    }

    zzg(boolean z, boolean z2, String str, boolean z3, float f, int i, boolean z4, boolean z5, boolean z6) {
        this.zzbkx = z;
        this.zzbky = z2;
        this.f3719a = str;
        this.zzbla = z3;
        this.zzblb = f;
        this.zzblc = i;
        this.zzbld = z4;
        this.zzble = z5;
        this.zzblf = z6;
    }
}
