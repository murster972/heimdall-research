package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.internal.ads.zzayu;

final class zzk extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzl f3721a;

    zzk(zzl zzl) {
        this.f3721a = zzl;
    }

    public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        if (this.f3721a.g != null) {
            try {
                this.f3721a.g.onAdFailedToLoad(0);
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith(this.f3721a.w())) {
            return false;
        }
        if (str.startsWith("gmsg://noAdLoaded")) {
            if (this.f3721a.g != null) {
                try {
                    this.f3721a.g.onAdFailedToLoad(3);
                } catch (RemoteException e) {
                    zzayu.zze("#007 Could not call remote method.", e);
                }
            }
            this.f3721a.g(0);
            return true;
        } else if (str.startsWith("gmsg://scriptLoadFailed")) {
            if (this.f3721a.g != null) {
                try {
                    this.f3721a.g.onAdFailedToLoad(0);
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
            this.f3721a.g(0);
            return true;
        } else if (str.startsWith("gmsg://adResized")) {
            if (this.f3721a.g != null) {
                try {
                    this.f3721a.g.onAdLoaded();
                } catch (RemoteException e3) {
                    zzayu.zze("#007 Could not call remote method.", e3);
                }
            }
            this.f3721a.g(this.f3721a.b(str));
            return true;
        } else if (str.startsWith("gmsg://")) {
            return true;
        } else {
            if (this.f3721a.g != null) {
                try {
                    this.f3721a.g.onAdLeftApplication();
                } catch (RemoteException e4) {
                    zzayu.zze("#007 Could not call remote method.", e4);
                }
            }
            this.f3721a.d(this.f3721a.c(str));
            return true;
        }
    }
}
