package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.zzaar;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzug;
import java.util.Map;
import java.util.TreeMap;

final class zzo {

    /* renamed from: a  reason: collision with root package name */
    private final String f3725a;
    private final Map<String, String> b = new TreeMap();
    private String c;
    private String d;

    public zzo(String str) {
        this.f3725a = str;
    }

    public final String a() {
        return this.c;
    }

    public final String b() {
        return this.d;
    }

    public final String c() {
        return this.f3725a;
    }

    public final Map<String, String> d() {
        return this.b;
    }

    public final void a(zzug zzug, zzazb zzazb) {
        this.c = zzug.zzccd.zzblv;
        Bundle bundle = zzug.zzccf;
        Bundle bundle2 = bundle != null ? bundle.getBundle(AdMobAdapter.class.getName()) : null;
        if (bundle2 != null) {
            String str = zzaar.zzcsn.get();
            for (String str2 : bundle2.keySet()) {
                if (str.equals(str2)) {
                    this.d = bundle2.getString(str2);
                } else if (str2.startsWith("csa_")) {
                    this.b.put(str2.substring(4), bundle2.getString(str2));
                }
            }
            this.b.put("SDKVersion", zzazb.zzbma);
        }
    }
}
