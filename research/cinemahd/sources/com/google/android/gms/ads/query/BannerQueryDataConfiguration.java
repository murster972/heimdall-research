package com.google.android.gms.ads.query;

import android.content.Context;
import com.google.android.gms.ads.AdSize;

public class BannerQueryDataConfiguration extends QueryDataConfiguration {
    private final AdSize c;

    public BannerQueryDataConfiguration(Context context, String str, AdSize adSize) {
        super(context, str);
        this.c = adSize;
    }

    public AdSize getAdSize() {
        return this.c;
    }
}
