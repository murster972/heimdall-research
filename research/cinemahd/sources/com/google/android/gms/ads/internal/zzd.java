package com.google.android.gms.ads.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzaju;
import com.google.android.gms.internal.ads.zzajx;
import com.google.android.gms.internal.ads.zzajy;
import com.google.android.gms.internal.ads.zzakc;
import com.google.android.gms.internal.ads.zzavf;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzazd;
import com.google.android.gms.internal.ads.zzazh;
import com.google.android.gms.internal.ads.zzdgs;
import com.google.android.gms.internal.ads.zzdhe;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.concurrent.Executor;
import org.json.JSONObject;

public final class zzd {

    /* renamed from: a  reason: collision with root package name */
    private Context f3717a;
    private long b = 0;

    private final void a(Context context, zzazb zzazb, boolean z, zzavf zzavf, String str, String str2, Runnable runnable) {
        if (zzq.zzkx().a() - this.b < 5000) {
            zzayu.zzez("Not retrying to fetch app settings");
            return;
        }
        this.b = zzq.zzkx().a();
        boolean z2 = true;
        if (zzavf != null) {
            if (!(zzq.zzkx().b() - zzavf.zzvj() > ((Long) zzve.zzoy().zzd(zzzn.zzcmt)).longValue()) && zzavf.zzvk()) {
                z2 = false;
            }
        }
        if (z2) {
            if (context == null) {
                zzayu.zzez("Context not provided to fetch application settings");
            } else if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext == null) {
                    applicationContext = context;
                }
                this.f3717a = applicationContext;
                zzakc zzb = zzq.zzld().zzb(this.f3717a, zzazb);
                zzajy<JSONObject> zzajy = zzajx.zzdaq;
                zzaju<I, O> zza = zzb.zza("google.afma.config.fetchAppSettings", zzajy, zzajy);
                try {
                    JSONObject jSONObject = new JSONObject();
                    if (!TextUtils.isEmpty(str)) {
                        jSONObject.put(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_APP_ID, str);
                    } else if (!TextUtils.isEmpty(str2)) {
                        jSONObject.put("ad_unit_id", str2);
                    }
                    jSONObject.put("is_init", z);
                    jSONObject.put("pn", context.getPackageName());
                    zzdhe<O> zzi = zza.zzi(jSONObject);
                    zzdhe<O> zzb2 = zzdgs.zzb(zzi, zzf.f3718a, (Executor) zzazd.zzdwj);
                    if (runnable != null) {
                        zzi.addListener(runnable, zzazd.zzdwj);
                    }
                    zzazh.zza(zzb2, "ConfigLoader.maybeFetchNewAppSettings");
                } catch (Exception e) {
                    zzayu.zzc("Error requesting application settings", e);
                }
            } else {
                zzayu.zzez("App settings could not be fetched. Required parameters missing");
            }
        }
    }

    public final void zza(Context context, zzazb zzazb, String str, Runnable runnable) {
        a(context, zzazb, true, (zzavf) null, str, (String) null, runnable);
    }

    public final void zza(Context context, zzazb zzazb, String str, zzavf zzavf) {
        a(context, zzazb, false, zzavf, zzavf != null ? zzavf.zzvm() : null, str, (Runnable) null);
    }
}
