package com.google.android.gms.ads;

import com.google.android.gms.internal.ads.zzyw;

public final class VideoOptions {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f3689a;
    private final boolean b;
    private final boolean c;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public boolean f3690a = true;
        /* access modifiers changed from: private */
        public boolean b = false;
        /* access modifiers changed from: private */
        public boolean c = false;

        public final VideoOptions build() {
            return new VideoOptions(this);
        }

        public final Builder setClickToExpandRequested(boolean z) {
            this.c = z;
            return this;
        }

        public final Builder setCustomControlsRequested(boolean z) {
            this.b = z;
            return this;
        }

        public final Builder setStartMuted(boolean z) {
            this.f3690a = z;
            return this;
        }
    }

    public VideoOptions(zzyw zzyw) {
        this.f3689a = zzyw.zzabv;
        this.b = zzyw.zzabw;
        this.c = zzyw.zzabx;
    }

    public final boolean getClickToExpandRequested() {
        return this.c;
    }

    public final boolean getCustomControlsRequested() {
        return this.b;
    }

    public final boolean getStartMuted() {
        return this.f3689a;
    }

    private VideoOptions(Builder builder) {
        this.f3689a = builder.f3690a;
        this.b = builder.b;
        this.c = builder.c;
    }
}
