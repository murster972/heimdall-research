package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.ads.zzbdi;
import com.google.android.gms.internal.ads.zzbeu;

final /* synthetic */ class zzf implements zzbeu {

    /* renamed from: a  reason: collision with root package name */
    private final zzc f3710a;

    zzf(zzc zzc) {
        this.f3710a = zzc;
    }

    public final void zzak(boolean z) {
        zzbdi zzbdi = this.f3710a.c;
        if (zzbdi != null) {
            zzbdi.zztr();
        }
    }
}
