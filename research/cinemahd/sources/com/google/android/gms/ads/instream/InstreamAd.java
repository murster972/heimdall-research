package com.google.android.gms.ads.instream;

import android.content.Context;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MediaAspectRatio;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzahl;
import com.google.android.gms.internal.ads.zzahq;

public abstract class InstreamAd {

    public static abstract class InstreamAdLoadCallback {
        public void onInstreamAdFailedToLoad(int i) {
        }

        public void onInstreamAdLoaded(InstreamAd instreamAd) {
        }
    }

    public static void load(Context context, String str, AdRequest adRequest, @MediaAspectRatio int i, InstreamAdLoadCallback instreamAdLoadCallback) {
        Preconditions.a(i == 2 || i == 3, (Object) "Instream ads only support Landscape and Portrait media aspect ratios");
        new zzahq(context, str).zza(instreamAdLoadCallback).zza(new zzahl(i)).zzry().loadAd(adRequest);
    }

    public abstract void destroy();

    public abstract float getAspectRatio();

    public abstract VideoController getVideoController();

    public abstract float getVideoCurrentTime();

    public abstract float getVideoDuration();

    /* access modifiers changed from: protected */
    public abstract void zza(InstreamAdView instreamAdView);

    public static void load(Context context, String str, InstreamAdLoadCallback instreamAdLoadCallback) {
        new zzahq(context, "").zza(instreamAdLoadCallback).zza(new zzahl(str)).zzry().loadAd(new PublisherAdRequest.Builder().build());
    }
}
