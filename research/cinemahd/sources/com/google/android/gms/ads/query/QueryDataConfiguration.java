package com.google.android.gms.ads.query;

import android.content.Context;

public class QueryDataConfiguration {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3743a;
    private final String b;

    QueryDataConfiguration(Context context, String str) {
        this.f3743a = context;
        this.b = str;
    }

    public String getAdUnitId() {
        return this.b;
    }

    public Context getContext() {
        return this.f3743a;
    }
}
