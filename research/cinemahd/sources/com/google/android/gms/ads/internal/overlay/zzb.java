package com.google.android.gms.ads.internal.overlay;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzavs;
import com.google.android.gms.internal.ads.zzawb;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;

public final class zzb {
    private static boolean a(Context context, Intent intent, zzt zzt) {
        try {
            String valueOf = String.valueOf(intent.toURI());
            zzavs.zzed(valueOf.length() != 0 ? "Launching an intent: ".concat(valueOf) : new String("Launching an intent: "));
            zzq.zzkq();
            zzawb.zza(context, intent);
            if (zzt == null) {
                return true;
            }
            zzt.zztv();
            return true;
        } catch (ActivityNotFoundException e) {
            zzayu.zzez(e.getMessage());
            return false;
        }
    }

    public static boolean zza(Context context, zzd zzd, zzt zzt) {
        int i = 0;
        if (zzd == null) {
            zzayu.zzez("No intent data for launcher overlay.");
            return false;
        }
        zzzn.initialize(context);
        Intent intent = zzd.intent;
        if (intent != null) {
            return a(context, intent, zzt);
        }
        Intent intent2 = new Intent();
        if (TextUtils.isEmpty(zzd.url)) {
            zzayu.zzez("Open GMSG did not contain a URL.");
            return false;
        }
        if (!TextUtils.isEmpty(zzd.mimeType)) {
            intent2.setDataAndType(Uri.parse(zzd.url), zzd.mimeType);
        } else {
            intent2.setData(Uri.parse(zzd.url));
        }
        intent2.setAction("android.intent.action.VIEW");
        if (!TextUtils.isEmpty(zzd.packageName)) {
            intent2.setPackage(zzd.packageName);
        }
        if (!TextUtils.isEmpty(zzd.zzdhf)) {
            String[] split = zzd.zzdhf.split("/", 2);
            if (split.length < 2) {
                String valueOf = String.valueOf(zzd.zzdhf);
                zzayu.zzez(valueOf.length() != 0 ? "Could not parse component name from open GMSG: ".concat(valueOf) : new String("Could not parse component name from open GMSG: "));
                return false;
            }
            intent2.setClassName(split[0], split[1]);
        }
        String str = zzd.zzdhg;
        if (!TextUtils.isEmpty(str)) {
            try {
                i = Integer.parseInt(str);
            } catch (NumberFormatException unused) {
                zzayu.zzez("Could not parse intent flags.");
            }
            intent2.addFlags(i);
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnk)).booleanValue()) {
            intent2.addFlags(268435456);
            intent2.putExtra("android.support.customtabs.extra.user_opt_out", true);
        } else {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnj)).booleanValue()) {
                zzq.zzkq();
                zzawb.zzb(context, intent2);
            }
        }
        return a(context, intent2, zzt);
    }
}
