package com.google.android.gms.ads.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzj implements Parcelable.Creator<zzg> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        float f = 0.0f;
        int i = 0;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a2)) {
                case 2:
                    z = SafeParcelReader.s(parcel, a2);
                    break;
                case 3:
                    z2 = SafeParcelReader.s(parcel, a2);
                    break;
                case 4:
                    str = SafeParcelReader.o(parcel, a2);
                    break;
                case 5:
                    z3 = SafeParcelReader.s(parcel, a2);
                    break;
                case 6:
                    f = SafeParcelReader.u(parcel, a2);
                    break;
                case 7:
                    i = SafeParcelReader.w(parcel, a2);
                    break;
                case 8:
                    z4 = SafeParcelReader.s(parcel, a2);
                    break;
                case 9:
                    z5 = SafeParcelReader.s(parcel, a2);
                    break;
                case 10:
                    z6 = SafeParcelReader.s(parcel, a2);
                    break;
                default:
                    SafeParcelReader.A(parcel, a2);
                    break;
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzg(z, z2, str, z3, f, i, z4, z5, z6);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzg[i];
    }
}
