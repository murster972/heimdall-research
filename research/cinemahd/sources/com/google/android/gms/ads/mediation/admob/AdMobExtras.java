package com.google.android.gms.ads.mediation.admob;

import android.os.Bundle;
import com.google.ads.mediation.NetworkExtras;

@Deprecated
public final class AdMobExtras implements NetworkExtras {

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f3734a;

    public AdMobExtras(Bundle bundle) {
        this.f3734a = bundle != null ? new Bundle(bundle) : null;
    }

    public final Bundle getExtras() {
        return this.f3734a;
    }
}
