package com.google.android.gms.ads.internal;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzapz;
import com.google.android.gms.internal.ads.zzato;
import com.google.android.gms.internal.ads.zzawb;
import java.util.List;
import java.util.Map;

public final class zzc {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3716a;
    private boolean b;
    private zzato c;
    private zzapz d = null;

    public zzc(Context context, zzato zzato, zzapz zzapz) {
        this.f3716a = context;
        this.c = zzato;
        if (this.d == null) {
            this.d = new zzapz();
        }
    }

    private final boolean a() {
        zzato zzato = this.c;
        return (zzato != null && zzato.zzuk().zzdox) || this.d.zzdln;
    }

    public final void recordClick() {
        this.b = true;
    }

    public final void zzbq(String str) {
        List<String> list;
        if (a()) {
            if (str == null) {
                str = "";
            }
            zzato zzato = this.c;
            if (zzato != null) {
                zzato.zza(str, (Map<String, String>) null, 3);
                return;
            }
            zzapz zzapz = this.d;
            if (zzapz.zzdln && (list = zzapz.zzdlo) != null) {
                for (String next : list) {
                    if (!TextUtils.isEmpty(next)) {
                        String replace = next.replace("{NAVIGATION_URL}", Uri.encode(str));
                        zzq.zzkq();
                        zzawb.zzb(this.f3716a, "", replace);
                    }
                }
            }
        }
    }

    public final boolean zzjq() {
        return !a() || this.b;
    }
}
