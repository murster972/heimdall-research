package com.google.android.gms.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaot;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzve;

public final class AdActivity extends Activity {
    public static final String CLASS_NAME = "com.google.android.gms.ads.AdActivity";
    public static final String SIMPLE_CLASS_NAME = "AdActivity";

    /* renamed from: a  reason: collision with root package name */
    private zzaot f3675a;

    private final void a() {
        zzaot zzaot = this.f3675a;
        if (zzaot != null) {
            try {
                zzaot.zzdf();
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i, int i2, Intent intent) {
        try {
            this.f3675a.onActivityResult(i, i2, intent);
        } catch (Exception e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        super.onActivityResult(i, i2, intent);
    }

    public final void onBackPressed() {
        boolean z = true;
        try {
            if (this.f3675a != null) {
                z = this.f3675a.zztm();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        if (z) {
            super.onBackPressed();
        }
    }

    public final void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            this.f3675a.zzad(ObjectWrapper.a(configuration));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3675a = zzve.zzov().zzb((Activity) this);
        zzaot zzaot = this.f3675a;
        if (zzaot == null) {
            zzayu.zze("#007 Could not call remote method.", (Throwable) null);
            finish();
            return;
        }
        try {
            zzaot.onCreate(bundle);
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        try {
            if (this.f3675a != null) {
                this.f3675a.onDestroy();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void onPause() {
        try {
            if (this.f3675a != null) {
                this.f3675a.onPause();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            finish();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public final void onRestart() {
        super.onRestart();
        try {
            if (this.f3675a != null) {
                this.f3675a.onRestart();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onResume() {
        super.onResume();
        try {
            if (this.f3675a != null) {
                this.f3675a.onResume();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onSaveInstanceState(Bundle bundle) {
        try {
            if (this.f3675a != null) {
                this.f3675a.onSaveInstanceState(bundle);
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            finish();
        }
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public final void onStart() {
        super.onStart();
        try {
            if (this.f3675a != null) {
                this.f3675a.onStart();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onStop() {
        try {
            if (this.f3675a != null) {
                this.f3675a.onStop();
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            finish();
        }
        super.onStop();
    }

    public final void setContentView(int i) {
        super.setContentView(i);
        a();
    }

    public final void setContentView(View view) {
        super.setContentView(view);
        a();
    }

    public final void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(view, layoutParams);
        a();
    }
}
