package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzvu;
import com.google.android.gms.internal.ads.zzxl;

public final class PublisherAdView extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private final zzxl f3694a;

    public PublisherAdView(Context context) {
        super(context);
        this.f3694a = new zzxl(this);
    }

    public final void destroy() {
        this.f3694a.destroy();
    }

    public final AdListener getAdListener() {
        return this.f3694a.getAdListener();
    }

    public final AdSize getAdSize() {
        return this.f3694a.getAdSize();
    }

    public final AdSize[] getAdSizes() {
        return this.f3694a.getAdSizes();
    }

    public final String getAdUnitId() {
        return this.f3694a.getAdUnitId();
    }

    public final AppEventListener getAppEventListener() {
        return this.f3694a.getAppEventListener();
    }

    public final String getMediationAdapterClassName() {
        return this.f3694a.getMediationAdapterClassName();
    }

    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.f3694a.getOnCustomRenderedAdLoadedListener();
    }

    public final VideoController getVideoController() {
        return this.f3694a.getVideoController();
    }

    public final VideoOptions getVideoOptions() {
        return this.f3694a.getVideoOptions();
    }

    public final boolean isLoading() {
        return this.f3694a.isLoading();
    }

    public final void loadAd(PublisherAdRequest publisherAdRequest) {
        this.f3694a.zza(publisherAdRequest.zzdg());
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3;
        int i4 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e) {
                zzayu.zzc("Unable to retrieve ad size.", e);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i3 = adSize.getHeightInPixels(context);
                i4 = widthInPixels;
            } else {
                i3 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            i4 = childAt.getMeasuredWidth();
            i3 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i4, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i3, getSuggestedMinimumHeight()), i2));
    }

    public final void pause() {
        this.f3694a.pause();
    }

    public final void recordManualImpression() {
        this.f3694a.recordManualImpression();
    }

    public final void resume() {
        this.f3694a.resume();
    }

    public final void setAdListener(AdListener adListener) {
        this.f3694a.setAdListener(adListener);
    }

    public final void setAdSizes(AdSize... adSizeArr) {
        if (adSizeArr == null || adSizeArr.length <= 0) {
            throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
        }
        this.f3694a.zza(adSizeArr);
    }

    public final void setAdUnitId(String str) {
        this.f3694a.setAdUnitId(str);
    }

    public final void setAppEventListener(AppEventListener appEventListener) {
        this.f3694a.setAppEventListener(appEventListener);
    }

    @Deprecated
    public final void setCorrelator(Correlator correlator) {
    }

    public final void setManualImpressionsEnabled(boolean z) {
        this.f3694a.setManualImpressionsEnabled(z);
    }

    public final void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.f3694a.setOnCustomRenderedAdLoadedListener(onCustomRenderedAdLoadedListener);
    }

    public final void setVideoOptions(VideoOptions videoOptions) {
        this.f3694a.setVideoOptions(videoOptions);
    }

    public final boolean zza(zzvu zzvu) {
        return this.f3694a.zza(zzvu);
    }

    public PublisherAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3694a = new zzxl(this, attributeSet, true);
        Preconditions.a(context, (Object) "Context cannot be null");
    }

    public PublisherAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3694a = new zzxl(this, attributeSet, true);
    }
}
