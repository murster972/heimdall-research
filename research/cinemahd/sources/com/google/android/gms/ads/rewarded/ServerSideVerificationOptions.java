package com.google.android.gms.ads.rewarded;

public class ServerSideVerificationOptions {

    /* renamed from: a  reason: collision with root package name */
    private final String f3745a;
    private final String b;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f3746a = "";
        /* access modifiers changed from: private */
        public String b = "";

        public final ServerSideVerificationOptions build() {
            return new ServerSideVerificationOptions(this);
        }

        public final Builder setCustomData(String str) {
            this.b = str;
            return this;
        }

        public final Builder setUserId(String str) {
            this.f3746a = str;
            return this;
        }
    }

    private ServerSideVerificationOptions(Builder builder) {
        this.f3745a = builder.f3746a;
        this.b = builder.b;
    }

    public String getCustomData() {
        return this.b;
    }

    public String getUserId() {
        return this.f3745a;
    }
}
