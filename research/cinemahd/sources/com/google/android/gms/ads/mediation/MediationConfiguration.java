package com.google.android.gms.ads.mediation;

import android.os.Bundle;
import com.google.android.gms.ads.AdFormat;

public class MediationConfiguration {

    /* renamed from: a  reason: collision with root package name */
    private final AdFormat f3730a;
    private final Bundle b;

    public MediationConfiguration(AdFormat adFormat, Bundle bundle) {
        this.f3730a = adFormat;
        this.b = bundle;
    }

    public AdFormat getFormat() {
        return this.f3730a;
    }

    public Bundle getServerParameters() {
        return this.b;
    }
}
