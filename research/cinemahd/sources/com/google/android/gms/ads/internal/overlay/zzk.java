package com.google.android.gms.ads.internal.overlay;

import android.graphics.drawable.Drawable;

final /* synthetic */ class zzk implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final zzl f3712a;
    private final Drawable b;

    zzk(zzl zzl, Drawable drawable) {
        this.f3712a = zzl;
        this.b = drawable;
    }

    public final void run() {
        zzl zzl = this.f3712a;
        zzl.f3713a.f3707a.getWindow().setBackgroundDrawable(this.b);
    }
}
