package com.google.android.gms.ads.formats;

import android.widget.ImageView;
import com.google.android.gms.internal.ads.zzabv;

final /* synthetic */ class zzd implements zzabv {

    /* renamed from: a  reason: collision with root package name */
    private final UnifiedNativeAdView f3704a;

    zzd(UnifiedNativeAdView unifiedNativeAdView) {
        this.f3704a = unifiedNativeAdView;
    }

    public final void setImageScaleType(ImageView.ScaleType scaleType) {
        this.f3704a.a(scaleType);
    }
}
