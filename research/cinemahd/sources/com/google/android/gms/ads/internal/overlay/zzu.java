package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzaos;
import com.google.android.gms.internal.ads.zzty;

public final class zzu extends zzaos {

    /* renamed from: a  reason: collision with root package name */
    private AdOverlayInfoParcel f3715a;
    private Activity b;
    private boolean c = false;
    private boolean d = false;

    public zzu(Activity activity, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.f3715a = adOverlayInfoParcel;
        this.b = activity;
    }

    private final synchronized void b() {
        if (!this.d) {
            if (this.f3715a.zzdhq != null) {
                this.f3715a.zzdhq.zzte();
            }
            this.d = true;
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) throws RemoteException {
    }

    public final void onBackPressed() throws RemoteException {
    }

    public final void onCreate(Bundle bundle) {
        zzo zzo;
        boolean z = false;
        if (bundle != null && bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false)) {
            z = true;
        }
        AdOverlayInfoParcel adOverlayInfoParcel = this.f3715a;
        if (adOverlayInfoParcel == null) {
            this.b.finish();
        } else if (z) {
            this.b.finish();
        } else {
            if (bundle == null) {
                zzty zzty = adOverlayInfoParcel.zzcbt;
                if (zzty != null) {
                    zzty.onAdClicked();
                }
                if (!(this.b.getIntent() == null || !this.b.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true) || (zzo = this.f3715a.zzdhq) == null)) {
                    zzo.zztf();
                }
            }
            zzq.zzko();
            Activity activity = this.b;
            AdOverlayInfoParcel adOverlayInfoParcel2 = this.f3715a;
            if (!zzb.zza(activity, adOverlayInfoParcel2.zzdhp, adOverlayInfoParcel2.zzdhu)) {
                this.b.finish();
            }
        }
    }

    public final void onDestroy() throws RemoteException {
        if (this.b.isFinishing()) {
            b();
        }
    }

    public final void onPause() throws RemoteException {
        zzo zzo = this.f3715a.zzdhq;
        if (zzo != null) {
            zzo.onPause();
        }
        if (this.b.isFinishing()) {
            b();
        }
    }

    public final void onRestart() throws RemoteException {
    }

    public final void onResume() throws RemoteException {
        if (this.c) {
            this.b.finish();
            return;
        }
        this.c = true;
        zzo zzo = this.f3715a.zzdhq;
        if (zzo != null) {
            zzo.onResume();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.c);
    }

    public final void onStart() throws RemoteException {
    }

    public final void onStop() throws RemoteException {
        if (this.b.isFinishing()) {
            b();
        }
    }

    public final void zzad(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final void zzdf() throws RemoteException {
    }

    public final boolean zztm() throws RemoteException {
        return false;
    }
}
