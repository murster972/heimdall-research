package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzdgf;
import com.google.android.gms.internal.ads.zzdgs;
import com.google.android.gms.internal.ads.zzdhe;
import org.json.JSONObject;

final /* synthetic */ class zzf implements zzdgf {

    /* renamed from: a  reason: collision with root package name */
    static final zzdgf f3718a = new zzf();

    private zzf() {
    }

    public final zzdhe zzf(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (jSONObject.optBoolean("isSuccessful", false)) {
            zzq.zzku().zzvf().zzeg(jSONObject.getString("appSettingsJson"));
        }
        return zzdgs.zzaj(null);
    }
}
