package com.google.android.gms.ads.query;

public class AdData {

    /* renamed from: a  reason: collision with root package name */
    private final QueryData f3741a;
    private final String b;

    public AdData(QueryData queryData, String str) {
        this.f3741a = queryData;
        this.b = str;
    }

    public static String getRequestId(String str) {
        return "";
    }

    public String getAdString() {
        return this.b;
    }

    public QueryData getQueryData() {
        return this.f3741a;
    }
}
