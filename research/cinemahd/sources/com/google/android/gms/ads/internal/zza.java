package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzatr;
import com.google.android.gms.internal.ads.zzbaj;
import com.google.android.gms.internal.ads.zzbcv;
import com.google.android.gms.internal.ads.zzsr;

public final class zza {
    public final zzbcv zzbko;
    public final zzbaj zzbkp;

    public zza(zzbcv zzbcv, zzbaj zzbaj, zzatr zzatr, zzsr zzsr) {
        this.zzbko = zzbcv;
        this.zzbkp = zzbaj;
    }
}
