package com.google.android.gms.ads.query;

public interface QueryDataGenerationCallback {
    void onFailure(String str);

    void onSuccess(QueryData queryData);
}
