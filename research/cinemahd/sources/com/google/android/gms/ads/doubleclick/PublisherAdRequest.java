package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.mediation.MediationExtrasReceiver;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.query.AdData;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzxj;
import com.google.android.gms.internal.ads.zzxm;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Date;
import java.util.List;
import java.util.Set;

public final class PublisherAdRequest {
    public static final String DEVICE_ID_EMULATOR = "B3EEABB8EE11C2BE770B684D95219ECB";
    public static final int ERROR_CODE_INTERNAL_ERROR = 0;
    public static final int ERROR_CODE_INVALID_REQUEST = 1;
    public static final int ERROR_CODE_NETWORK_ERROR = 2;
    public static final int ERROR_CODE_NO_FILL = 3;
    public static final int GENDER_FEMALE = 2;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_UNKNOWN = 0;
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_G = "G";
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_MA = "MA";
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_PG = "PG";
    @Deprecated
    public static final String MAX_AD_CONTENT_RATING_T = "T";
    @Deprecated
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_FALSE = 0;
    @Deprecated
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_TRUE = 1;
    @Deprecated
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_UNSPECIFIED = -1;

    /* renamed from: a  reason: collision with root package name */
    private final zzxj f3692a;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final zzxm f3693a = new zzxm();

        public final Builder addCategoryExclusion(String str) {
            this.f3693a.zzcl(str);
            return this;
        }

        public final Builder addCustomEventExtrasBundle(Class<? extends CustomEvent> cls, Bundle bundle) {
            this.f3693a.zzb(cls, bundle);
            return this;
        }

        public final Builder addCustomTargeting(String str, String str2) {
            this.f3693a.zze(str, str2);
            return this;
        }

        public final Builder addKeyword(String str) {
            this.f3693a.zzcf(str);
            return this;
        }

        public final Builder addNetworkExtras(NetworkExtras networkExtras) {
            this.f3693a.zza(networkExtras);
            return this;
        }

        public final Builder addNetworkExtrasBundle(Class<? extends MediationExtrasReceiver> cls, Bundle bundle) {
            this.f3693a.zza(cls, bundle);
            return this;
        }

        public final Builder addTestDevice(String str) {
            this.f3693a.zzcg(str);
            return this;
        }

        public final PublisherAdRequest build() {
            return new PublisherAdRequest(this);
        }

        public final Builder setAdData(AdData adData) {
            this.f3693a.zza(adData);
            return this;
        }

        @Deprecated
        public final Builder setBirthday(Date date) {
            this.f3693a.zza(date);
            return this;
        }

        public final Builder setContentUrl(String str) {
            Preconditions.a(str, (Object) "Content URL must be non-null.");
            Preconditions.a(str, (Object) "Content URL must be non-empty.");
            Preconditions.a(str.length() <= 512, "Content URL must not exceed %d in length.  Provided length was %d.", Integer.valueOf(AdRequest.MAX_CONTENT_URL_LENGTH), Integer.valueOf(str.length()));
            this.f3693a.zzci(str);
            return this;
        }

        @Deprecated
        public final Builder setGender(int i) {
            this.f3693a.zzcl(i);
            return this;
        }

        @Deprecated
        public final Builder setIsDesignedForFamilies(boolean z) {
            this.f3693a.zzaa(z);
            return this;
        }

        public final Builder setLocation(Location location) {
            this.f3693a.zza(location);
            return this;
        }

        @Deprecated
        public final Builder setManualImpressionsEnabled(boolean z) {
            this.f3693a.setManualImpressionsEnabled(z);
            return this;
        }

        @Deprecated
        public final Builder setMaxAdContentRating(String str) {
            this.f3693a.zzcm(str);
            return this;
        }

        public final Builder setPublisherProvidedId(String str) {
            this.f3693a.zzcj(str);
            return this;
        }

        public final Builder setRequestAgent(String str) {
            this.f3693a.zzck(str);
            return this;
        }

        @Deprecated
        public final Builder setTagForUnderAgeOfConsent(int i) {
            this.f3693a.zzcm(i);
            return this;
        }

        @Deprecated
        public final Builder tagForChildDirectedTreatment(boolean z) {
            this.f3693a.zzz(z);
            return this;
        }

        public final Builder addCustomTargeting(String str, List<String> list) {
            if (list != null) {
                this.f3693a.zze(str, TextUtils.join(",", list));
            }
            return this;
        }
    }

    @Deprecated
    @Retention(RetentionPolicy.SOURCE)
    public @interface MaxAdContentRating {
    }

    @Deprecated
    @Retention(RetentionPolicy.SOURCE)
    public @interface TagForUnderAgeOfConsent {
    }

    private PublisherAdRequest(Builder builder) {
        this.f3692a = new zzxj(builder.f3693a);
    }

    @Deprecated
    public static void updateCorrelator() {
    }

    @Deprecated
    public final Date getBirthday() {
        return this.f3692a.getBirthday();
    }

    public final String getContentUrl() {
        return this.f3692a.getContentUrl();
    }

    public final <T extends CustomEvent> Bundle getCustomEventExtrasBundle(Class<T> cls) {
        return this.f3692a.getCustomEventExtrasBundle(cls);
    }

    public final Bundle getCustomTargeting() {
        return this.f3692a.getCustomTargeting();
    }

    @Deprecated
    public final int getGender() {
        return this.f3692a.getGender();
    }

    public final Set<String> getKeywords() {
        return this.f3692a.getKeywords();
    }

    public final Location getLocation() {
        return this.f3692a.getLocation();
    }

    public final boolean getManualImpressionsEnabled() {
        return this.f3692a.getManualImpressionsEnabled();
    }

    @Deprecated
    public final <T extends NetworkExtras> T getNetworkExtras(Class<T> cls) {
        return this.f3692a.getNetworkExtras(cls);
    }

    public final <T extends MediationExtrasReceiver> Bundle getNetworkExtrasBundle(Class<T> cls) {
        return this.f3692a.getNetworkExtrasBundle(cls);
    }

    public final String getPublisherProvidedId() {
        return this.f3692a.getPublisherProvidedId();
    }

    public final boolean isTestDevice(Context context) {
        return this.f3692a.isTestDevice(context);
    }

    public final zzxj zzdg() {
        return this.f3692a;
    }
}
