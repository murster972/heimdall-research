package com.google.android.gms.ads.mediation;

import android.os.Bundle;

public interface MediationAdapter extends MediationExtrasReceiver {

    public static class zza {

        /* renamed from: a  reason: collision with root package name */
        private int f3729a;

        public final Bundle zzaby() {
            Bundle bundle = new Bundle();
            bundle.putInt("capabilities", this.f3729a);
            return bundle;
        }

        public final zza zzdf(int i) {
            this.f3729a = 1;
            return this;
        }
    }

    void onDestroy();

    void onPause();

    void onResume();
}
