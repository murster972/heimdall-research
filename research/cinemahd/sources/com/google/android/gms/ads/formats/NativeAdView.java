package com.google.android.gms.ads.formats;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzacm;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;

@Deprecated
public class NativeAdView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private final FrameLayout f3699a;
    private final zzacm b = a();

    public NativeAdView(Context context) {
        super(context);
        this.f3699a = a(context);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, View view) {
        try {
            this.b.zzb(str, ObjectWrapper.a(view));
        } catch (RemoteException e) {
            zzayu.zzc("Unable to call setAssetView on delegate", e);
        }
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        super.bringChildToFront(this.f3699a);
    }

    public void bringChildToFront(View view) {
        super.bringChildToFront(view);
        FrameLayout frameLayout = this.f3699a;
        if (frameLayout != view) {
            super.bringChildToFront(frameLayout);
        }
    }

    public void destroy() {
        try {
            this.b.destroy();
        } catch (RemoteException e) {
            zzayu.zzc("Unable to destroy native ad view", e);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        zzacm zzacm;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclq)).booleanValue() && (zzacm = this.b) != null) {
            try {
                zzacm.zzf(ObjectWrapper.a(motionEvent));
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call handleTouchEvent on delegate", e);
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public AdChoicesView getAdChoicesView() {
        View a2 = a(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW);
        if (a2 instanceof AdChoicesView) {
            return (AdChoicesView) a2;
        }
        return null;
    }

    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        zzacm zzacm = this.b;
        if (zzacm != null) {
            try {
                zzacm.zzc(ObjectWrapper.a(view), i);
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call onVisibilityChanged on delegate", e);
            }
        }
    }

    public void removeAllViews() {
        super.removeAllViews();
        super.addView(this.f3699a);
    }

    public void removeView(View view) {
        if (this.f3699a != view) {
            super.removeView(view);
        }
    }

    public void setAdChoicesView(AdChoicesView adChoicesView) {
        a(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW, adChoicesView);
    }

    public void setNativeAd(NativeAd nativeAd) {
        try {
            this.b.zza((IObjectWrapper) nativeAd.zzjj());
        } catch (RemoteException e) {
            zzayu.zzc("Unable to call setNativeAd on delegate", e);
        }
    }

    /* access modifiers changed from: protected */
    public final View a(String str) {
        try {
            IObjectWrapper zzco = this.b.zzco(str);
            if (zzco != null) {
                return (View) ObjectWrapper.a(zzco);
            }
            return null;
        } catch (RemoteException e) {
            zzayu.zzc("Unable to call getAssetView on delegate", e);
            return null;
        }
    }

    public NativeAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3699a = a(context);
    }

    private final FrameLayout a(Context context) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(frameLayout);
        return frameLayout;
    }

    public NativeAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3699a = a(context);
    }

    private final zzacm a() {
        Preconditions.a(this.f3699a, (Object) "createDelegate must be called after mOverlayFrame has been created");
        if (isInEditMode()) {
            return null;
        }
        return zzve.zzov().zza(this.f3699a.getContext(), (FrameLayout) this, this.f3699a);
    }

    @TargetApi(21)
    public NativeAdView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f3699a = a(context);
    }
}
