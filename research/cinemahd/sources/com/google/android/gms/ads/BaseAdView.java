package com.google.android.gms.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzty;
import com.google.android.gms.internal.ads.zzxl;

class BaseAdView extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    protected final zzxl f3682a;

    public BaseAdView(Context context, int i) {
        super(context);
        this.f3682a = new zzxl(this, i);
    }

    public void destroy() {
        this.f3682a.destroy();
    }

    public AdListener getAdListener() {
        return this.f3682a.getAdListener();
    }

    public AdSize getAdSize() {
        return this.f3682a.getAdSize();
    }

    public String getAdUnitId() {
        return this.f3682a.getAdUnitId();
    }

    public String getMediationAdapterClassName() {
        return this.f3682a.getMediationAdapterClassName();
    }

    public boolean isLoading() {
        return this.f3682a.isLoading();
    }

    public void loadAd(AdRequest adRequest) {
        this.f3682a.zza(adRequest.zzdg());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e) {
                zzayu.zzc("Unable to retrieve ad size.", e);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i3 = adSize.getHeightInPixels(context);
                i4 = widthInPixels;
            } else {
                i3 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            i4 = childAt.getMeasuredWidth();
            i3 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i4, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i3, getSuggestedMinimumHeight()), i2));
    }

    public void pause() {
        this.f3682a.pause();
    }

    public void resume() {
        this.f3682a.resume();
    }

    public void setAdListener(AdListener adListener) {
        this.f3682a.setAdListener(adListener);
        if (adListener == null) {
            this.f3682a.zza((zzty) null);
            this.f3682a.setAppEventListener((AppEventListener) null);
            return;
        }
        if (adListener instanceof zzty) {
            this.f3682a.zza((zzty) adListener);
        }
        if (adListener instanceof AppEventListener) {
            this.f3682a.setAppEventListener((AppEventListener) adListener);
        }
    }

    public void setAdSize(AdSize adSize) {
        this.f3682a.setAdSizes(adSize);
    }

    public void setAdUnitId(String str) {
        this.f3682a.setAdUnitId(str);
    }

    public BaseAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.f3682a = new zzxl(this, attributeSet, false, i);
    }

    public BaseAdView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        this.f3682a = new zzxl(this, attributeSet, false, i2);
    }
}
