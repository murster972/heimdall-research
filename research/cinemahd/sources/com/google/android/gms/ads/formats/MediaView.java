package com.google.android.gms.ads.formats;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.internal.ads.zzabt;
import com.google.android.gms.internal.ads.zzabv;

public class MediaView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private UnifiedNativeAd.MediaContent f3696a;
    private boolean b;
    private zzabt c;
    private ImageView.ScaleType d;
    private boolean e;
    private zzabv f;

    public MediaView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(zzabt zzabt) {
        this.c = zzabt;
        if (this.b) {
            zzabt.setMediaContent(this.f3696a);
        }
    }

    public void setImageScaleType(ImageView.ScaleType scaleType) {
        this.e = true;
        this.d = scaleType;
        zzabv zzabv = this.f;
        if (zzabv != null) {
            zzabv.setImageScaleType(this.d);
        }
    }

    public void setMediaContent(UnifiedNativeAd.MediaContent mediaContent) {
        this.b = true;
        this.f3696a = mediaContent;
        zzabt zzabt = this.c;
        if (zzabt != null) {
            zzabt.setMediaContent(mediaContent);
        }
    }

    public MediaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MediaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @TargetApi(21)
    public MediaView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(zzabv zzabv) {
        this.f = zzabv;
        if (this.e) {
            zzabv.setImageScaleType(this.d);
        }
    }
}
