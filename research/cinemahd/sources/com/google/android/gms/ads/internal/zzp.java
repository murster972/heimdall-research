package com.google.android.gms.ads.internal;

import android.os.AsyncTask;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzdq;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

final class zzp extends AsyncTask<Void, Void, String> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzl f3726a;

    private zzp(zzl zzl) {
        this.f3726a = zzl;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final String doInBackground(Void... voidArr) {
        try {
            zzdq unused = this.f3726a.h = (zzdq) this.f3726a.c.get(1000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            zzayu.zzd("", e);
        }
        return this.f3726a.b();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.f3726a.f != null && str != null) {
            this.f3726a.f.loadUrl(str);
        }
    }

    /* synthetic */ zzp(zzl zzl, zzk zzk) {
        this(zzl);
    }
}
