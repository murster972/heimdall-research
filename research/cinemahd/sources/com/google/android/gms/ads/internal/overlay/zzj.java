package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import com.google.android.gms.internal.ads.zzawt;

final class zzj extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private zzawt f3711a;
    boolean b;

    public zzj(Context context, String str, String str2) {
        super(context);
        this.f3711a = new zzawt(context, str);
        this.f3711a.zzx(str2);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.b) {
            return false;
        }
        this.f3711a.zzd(motionEvent);
        return false;
    }
}
