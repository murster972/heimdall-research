package com.google.android.gms.ads.impl;

public final class R {

    public static final class string {
        public static final int s1 = 2131755512;
        public static final int s2 = 2131755513;
        public static final int s3 = 2131755514;
        public static final int s4 = 2131755515;
        public static final int s5 = 2131755516;
        public static final int s6 = 2131755517;
        public static final int s7 = 2131755518;

        private string() {
        }
    }

    private R() {
    }
}
