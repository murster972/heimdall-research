package com.google.android.gms.ads.mediation.customevent;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.ads.zzayu;

@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter {

    /* renamed from: a  reason: collision with root package name */
    private View f3735a;
    private CustomEventBanner b;
    private CustomEventInterstitial c;
    private CustomEventNative d;

    static final class zza implements CustomEventBannerListener {

        /* renamed from: a  reason: collision with root package name */
        private final CustomEventAdapter f3736a;
        private final MediationBannerListener b;

        public zza(CustomEventAdapter customEventAdapter, MediationBannerListener mediationBannerListener) {
            this.f3736a = customEventAdapter;
            this.b = mediationBannerListener;
        }

        public final void onAdClicked() {
            zzayu.zzea("Custom event adapter called onAdClicked.");
            this.b.onAdClicked(this.f3736a);
        }

        public final void onAdClosed() {
            zzayu.zzea("Custom event adapter called onAdClosed.");
            this.b.onAdClosed(this.f3736a);
        }

        public final void onAdFailedToLoad(int i) {
            zzayu.zzea("Custom event adapter called onAdFailedToLoad.");
            this.b.onAdFailedToLoad(this.f3736a, i);
        }

        public final void onAdLeftApplication() {
            zzayu.zzea("Custom event adapter called onAdLeftApplication.");
            this.b.onAdLeftApplication(this.f3736a);
        }

        public final void onAdLoaded(View view) {
            zzayu.zzea("Custom event adapter called onAdLoaded.");
            this.f3736a.a(view);
            this.b.onAdLoaded(this.f3736a);
        }

        public final void onAdOpened() {
            zzayu.zzea("Custom event adapter called onAdOpened.");
            this.b.onAdOpened(this.f3736a);
        }
    }

    class zzc implements CustomEventInterstitialListener {

        /* renamed from: a  reason: collision with root package name */
        private final CustomEventAdapter f3738a;
        private final MediationInterstitialListener b;

        public zzc(CustomEventAdapter customEventAdapter, MediationInterstitialListener mediationInterstitialListener) {
            this.f3738a = customEventAdapter;
            this.b = mediationInterstitialListener;
        }

        public final void onAdClicked() {
            zzayu.zzea("Custom event adapter called onAdClicked.");
            this.b.onAdClicked(this.f3738a);
        }

        public final void onAdClosed() {
            zzayu.zzea("Custom event adapter called onAdClosed.");
            this.b.onAdClosed(this.f3738a);
        }

        public final void onAdFailedToLoad(int i) {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.b.onAdFailedToLoad(this.f3738a, i);
        }

        public final void onAdLeftApplication() {
            zzayu.zzea("Custom event adapter called onAdLeftApplication.");
            this.b.onAdLeftApplication(this.f3738a);
        }

        public final void onAdLoaded() {
            zzayu.zzea("Custom event adapter called onReceivedAd.");
            this.b.onAdLoaded(CustomEventAdapter.this);
        }

        public final void onAdOpened() {
            zzayu.zzea("Custom event adapter called onAdOpened.");
            this.b.onAdOpened(this.f3738a);
        }
    }

    private static <T> T a(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            String message = th.getMessage();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 46 + String.valueOf(message).length());
            sb.append("Could not instantiate custom event adapter: ");
            sb.append(str);
            sb.append(". ");
            sb.append(message);
            zzayu.zzez(sb.toString());
            return null;
        }
    }

    public final View getBannerView() {
        return this.f3735a;
    }

    public final void onDestroy() {
        CustomEventBanner customEventBanner = this.b;
        if (customEventBanner != null) {
            customEventBanner.onDestroy();
        }
        CustomEventInterstitial customEventInterstitial = this.c;
        if (customEventInterstitial != null) {
            customEventInterstitial.onDestroy();
        }
        CustomEventNative customEventNative = this.d;
        if (customEventNative != null) {
            customEventNative.onDestroy();
        }
    }

    public final void onPause() {
        CustomEventBanner customEventBanner = this.b;
        if (customEventBanner != null) {
            customEventBanner.onPause();
        }
        CustomEventInterstitial customEventInterstitial = this.c;
        if (customEventInterstitial != null) {
            customEventInterstitial.onPause();
        }
        CustomEventNative customEventNative = this.d;
        if (customEventNative != null) {
            customEventNative.onPause();
        }
    }

    public final void onResume() {
        CustomEventBanner customEventBanner = this.b;
        if (customEventBanner != null) {
            customEventBanner.onResume();
        }
        CustomEventInterstitial customEventInterstitial = this.c;
        if (customEventInterstitial != null) {
            customEventInterstitial.onResume();
        }
        CustomEventNative customEventNative = this.d;
        if (customEventNative != null) {
            customEventNative.onResume();
        }
    }

    public final void requestBannerAd(Context context, MediationBannerListener mediationBannerListener, Bundle bundle, AdSize adSize, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        Bundle bundle3;
        this.b = (CustomEventBanner) a(bundle.getString("class_name"));
        if (this.b == null) {
            mediationBannerListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 == null) {
            bundle3 = null;
        } else {
            bundle3 = bundle2.getBundle(bundle.getString("class_name"));
        }
        Context context2 = context;
        this.b.requestBannerAd(context2, new zza(this, mediationBannerListener), bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD), adSize, mediationAdRequest, bundle3);
    }

    public final void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        Bundle bundle3;
        this.c = (CustomEventInterstitial) a(bundle.getString("class_name"));
        if (this.c == null) {
            mediationInterstitialListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 == null) {
            bundle3 = null;
        } else {
            bundle3 = bundle2.getBundle(bundle.getString("class_name"));
        }
        Context context2 = context;
        this.c.requestInterstitialAd(context2, new zzc(this, mediationInterstitialListener), bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD), mediationAdRequest, bundle3);
    }

    public final void requestNativeAd(Context context, MediationNativeListener mediationNativeListener, Bundle bundle, NativeMediationAdRequest nativeMediationAdRequest, Bundle bundle2) {
        Bundle bundle3;
        this.d = (CustomEventNative) a(bundle.getString("class_name"));
        if (this.d == null) {
            mediationNativeListener.onAdFailedToLoad(this, 0);
            return;
        }
        if (bundle2 == null) {
            bundle3 = null;
        } else {
            bundle3 = bundle2.getBundle(bundle.getString("class_name"));
        }
        Context context2 = context;
        this.d.requestNativeAd(context2, new zzb(this, mediationNativeListener), bundle.getString(MediationRewardedVideoAdAdapter.CUSTOM_EVENT_SERVER_PARAMETER_FIELD), nativeMediationAdRequest, bundle3);
    }

    public final void showInterstitial() {
        this.c.showInterstitial();
    }

    static class zzb implements CustomEventNativeListener {

        /* renamed from: a  reason: collision with root package name */
        private final CustomEventAdapter f3737a;
        private final MediationNativeListener b;

        public zzb(CustomEventAdapter customEventAdapter, MediationNativeListener mediationNativeListener) {
            this.f3737a = customEventAdapter;
            this.b = mediationNativeListener;
        }

        public final void onAdClicked() {
            zzayu.zzea("Custom event adapter called onAdClicked.");
            this.b.onAdClicked(this.f3737a);
        }

        public final void onAdClosed() {
            zzayu.zzea("Custom event adapter called onAdClosed.");
            this.b.onAdClosed(this.f3737a);
        }

        public final void onAdFailedToLoad(int i) {
            zzayu.zzea("Custom event adapter called onAdFailedToLoad.");
            this.b.onAdFailedToLoad(this.f3737a, i);
        }

        public final void onAdImpression() {
            zzayu.zzea("Custom event adapter called onAdImpression.");
            this.b.onAdImpression(this.f3737a);
        }

        public final void onAdLeftApplication() {
            zzayu.zzea("Custom event adapter called onAdLeftApplication.");
            this.b.onAdLeftApplication(this.f3737a);
        }

        public final void onAdLoaded(NativeAdMapper nativeAdMapper) {
            zzayu.zzea("Custom event adapter called onAdLoaded.");
            this.b.onAdLoaded((MediationNativeAdapter) this.f3737a, nativeAdMapper);
        }

        public final void onAdOpened() {
            zzayu.zzea("Custom event adapter called onAdOpened.");
            this.b.onAdOpened(this.f3737a);
        }

        public final void onAdLoaded(UnifiedNativeAdMapper unifiedNativeAdMapper) {
            zzayu.zzea("Custom event adapter called onAdLoaded.");
            this.b.onAdLoaded((MediationNativeAdapter) this.f3737a, unifiedNativeAdMapper);
        }
    }

    /* access modifiers changed from: private */
    public final void a(View view) {
        this.f3735a = view;
    }
}
