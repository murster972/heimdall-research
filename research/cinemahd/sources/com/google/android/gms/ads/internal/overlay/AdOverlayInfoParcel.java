package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaew;
import com.google.android.gms.internal.ads.zzaey;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzbdi;
import com.google.android.gms.internal.ads.zzty;

public final class AdOverlayInfoParcel extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<AdOverlayInfoParcel> CREATOR = new zzm();
    public final int orientation;
    public final String url;
    public final zzazb zzbll;
    public final zzty zzcbt;
    public final zzaew zzcwq;
    public final zzaey zzcws;
    public final zzbdi zzcza;
    public final zzd zzdhp;
    public final zzo zzdhq;
    public final String zzdhr;
    public final boolean zzdhs;
    public final String zzdht;
    public final zzt zzdhu;
    public final int zzdhv;
    public final String zzdhw;
    public final zzg zzdhx;

    public AdOverlayInfoParcel(zzty zzty, zzo zzo, zzt zzt, zzbdi zzbdi, int i, zzazb zzazb, String str, zzg zzg, String str2, String str3) {
        this.zzdhp = null;
        this.zzcbt = null;
        this.zzdhq = zzo;
        this.zzcza = zzbdi;
        this.zzcwq = null;
        this.zzcws = null;
        this.zzdhr = str2;
        this.zzdhs = false;
        this.zzdht = str3;
        this.zzdhu = null;
        this.orientation = i;
        this.zzdhv = 1;
        this.url = null;
        this.zzbll = zzazb;
        this.zzdhw = str;
        this.zzdhx = zzg;
    }

    public static void zza(Intent intent, AdOverlayInfoParcel adOverlayInfoParcel) {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", adOverlayInfoParcel);
        intent.putExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", bundle);
    }

    public static AdOverlayInfoParcel zzc(Intent intent) {
        try {
            Bundle bundleExtra = intent.getBundleExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
            bundleExtra.setClassLoader(AdOverlayInfoParcel.class.getClassLoader());
            return (AdOverlayInfoParcel) bundleExtra.getParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
        } catch (Exception unused) {
            return null;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, (Parcelable) this.zzdhp, i, false);
        SafeParcelWriter.a(parcel, 3, ObjectWrapper.a(this.zzcbt).asBinder(), false);
        SafeParcelWriter.a(parcel, 4, ObjectWrapper.a(this.zzdhq).asBinder(), false);
        SafeParcelWriter.a(parcel, 5, ObjectWrapper.a(this.zzcza).asBinder(), false);
        SafeParcelWriter.a(parcel, 6, ObjectWrapper.a(this.zzcws).asBinder(), false);
        SafeParcelWriter.a(parcel, 7, this.zzdhr, false);
        SafeParcelWriter.a(parcel, 8, this.zzdhs);
        SafeParcelWriter.a(parcel, 9, this.zzdht, false);
        SafeParcelWriter.a(parcel, 10, ObjectWrapper.a(this.zzdhu).asBinder(), false);
        SafeParcelWriter.a(parcel, 11, this.orientation);
        SafeParcelWriter.a(parcel, 12, this.zzdhv);
        SafeParcelWriter.a(parcel, 13, this.url, false);
        SafeParcelWriter.a(parcel, 14, (Parcelable) this.zzbll, i, false);
        SafeParcelWriter.a(parcel, 16, this.zzdhw, false);
        SafeParcelWriter.a(parcel, 17, (Parcelable) this.zzdhx, i, false);
        SafeParcelWriter.a(parcel, 18, ObjectWrapper.a(this.zzcwq).asBinder(), false);
        SafeParcelWriter.a(parcel, a2);
    }

    public AdOverlayInfoParcel(zzty zzty, zzo zzo, zzt zzt, zzbdi zzbdi, boolean z, int i, zzazb zzazb) {
        this.zzdhp = null;
        this.zzcbt = zzty;
        this.zzdhq = zzo;
        this.zzcza = zzbdi;
        this.zzcwq = null;
        this.zzcws = null;
        this.zzdhr = null;
        this.zzdhs = z;
        this.zzdht = null;
        this.zzdhu = zzt;
        this.orientation = i;
        this.zzdhv = 2;
        this.url = null;
        this.zzbll = zzazb;
        this.zzdhw = null;
        this.zzdhx = null;
    }

    public AdOverlayInfoParcel(zzty zzty, zzo zzo, zzaew zzaew, zzaey zzaey, zzt zzt, zzbdi zzbdi, boolean z, int i, String str, zzazb zzazb) {
        this.zzdhp = null;
        this.zzcbt = zzty;
        this.zzdhq = zzo;
        this.zzcza = zzbdi;
        this.zzcwq = zzaew;
        this.zzcws = zzaey;
        this.zzdhr = null;
        this.zzdhs = z;
        this.zzdht = null;
        this.zzdhu = zzt;
        this.orientation = i;
        this.zzdhv = 3;
        this.url = str;
        this.zzbll = zzazb;
        this.zzdhw = null;
        this.zzdhx = null;
    }

    public AdOverlayInfoParcel(zzty zzty, zzo zzo, zzaew zzaew, zzaey zzaey, zzt zzt, zzbdi zzbdi, boolean z, int i, String str, String str2, zzazb zzazb) {
        this.zzdhp = null;
        this.zzcbt = zzty;
        this.zzdhq = zzo;
        this.zzcza = zzbdi;
        this.zzcwq = zzaew;
        this.zzcws = zzaey;
        this.zzdhr = str2;
        this.zzdhs = z;
        this.zzdht = str;
        this.zzdhu = zzt;
        this.orientation = i;
        this.zzdhv = 3;
        this.url = null;
        this.zzbll = zzazb;
        this.zzdhw = null;
        this.zzdhx = null;
    }

    public AdOverlayInfoParcel(zzd zzd, zzty zzty, zzo zzo, zzt zzt, zzazb zzazb) {
        this.zzdhp = zzd;
        this.zzcbt = zzty;
        this.zzdhq = zzo;
        this.zzcza = null;
        this.zzcwq = null;
        this.zzcws = null;
        this.zzdhr = null;
        this.zzdhs = false;
        this.zzdht = null;
        this.zzdhu = zzt;
        this.orientation = -1;
        this.zzdhv = 4;
        this.url = null;
        this.zzbll = zzazb;
        this.zzdhw = null;
        this.zzdhx = null;
    }

    AdOverlayInfoParcel(zzd zzd, IBinder iBinder, IBinder iBinder2, IBinder iBinder3, IBinder iBinder4, String str, boolean z, String str2, IBinder iBinder5, int i, int i2, String str3, zzazb zzazb, String str4, zzg zzg, IBinder iBinder6) {
        this.zzdhp = zzd;
        this.zzcbt = (zzty) ObjectWrapper.a(IObjectWrapper.Stub.a(iBinder));
        this.zzdhq = (zzo) ObjectWrapper.a(IObjectWrapper.Stub.a(iBinder2));
        this.zzcza = (zzbdi) ObjectWrapper.a(IObjectWrapper.Stub.a(iBinder3));
        this.zzcwq = (zzaew) ObjectWrapper.a(IObjectWrapper.Stub.a(iBinder6));
        this.zzcws = (zzaey) ObjectWrapper.a(IObjectWrapper.Stub.a(iBinder4));
        this.zzdhr = str;
        this.zzdhs = z;
        this.zzdht = str2;
        this.zzdhu = (zzt) ObjectWrapper.a(IObjectWrapper.Stub.a(iBinder5));
        this.orientation = i;
        this.zzdhv = i2;
        this.url = str3;
        this.zzbll = zzazb;
        this.zzdhw = str4;
        this.zzdhx = zzg;
    }
}
