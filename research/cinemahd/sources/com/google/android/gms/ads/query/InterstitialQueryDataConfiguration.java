package com.google.android.gms.ads.query;

import android.content.Context;

public class InterstitialQueryDataConfiguration extends QueryDataConfiguration {
    public InterstitialQueryDataConfiguration(Context context, String str) {
        super(context, str);
    }
}
