package com.google.android.gms.ads.mediation;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.VideoController;
import java.util.Map;

@Deprecated
public class NativeAdMapper {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f3731a;
    protected boolean b;
    protected Bundle c = new Bundle();
    protected View d;
    private View e;
    private VideoController f;
    private boolean g;

    public View getAdChoicesContent() {
        return this.d;
    }

    public final Bundle getExtras() {
        return this.c;
    }

    public final boolean getOverrideClickHandling() {
        return this.b;
    }

    public final boolean getOverrideImpressionRecording() {
        return this.f3731a;
    }

    public final VideoController getVideoController() {
        return this.f;
    }

    public void handleClick(View view) {
    }

    public boolean hasVideoContent() {
        return this.g;
    }

    public void recordImpression() {
    }

    public void setAdChoicesContent(View view) {
        this.d = view;
    }

    public final void setExtras(Bundle bundle) {
        this.c = bundle;
    }

    public void setHasVideoContent(boolean z) {
        this.g = z;
    }

    public void setMediaView(View view) {
        this.e = view;
    }

    public final void setOverrideClickHandling(boolean z) {
        this.b = z;
    }

    public final void setOverrideImpressionRecording(boolean z) {
        this.f3731a = z;
    }

    @Deprecated
    public void trackView(View view) {
    }

    public void trackViews(View view, Map<String, View> map, Map<String, View> map2) {
    }

    public void untrackView(View view) {
    }

    public final void zza(VideoController videoController) {
        this.f = videoController;
    }

    public final View zzabz() {
        return this.e;
    }
}
