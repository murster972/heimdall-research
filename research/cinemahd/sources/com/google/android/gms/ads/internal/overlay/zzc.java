package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.facebook.ads.AudienceNetworkActivity;
import com.google.android.gms.ads.internal.zzi;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaae;
import com.google.android.gms.internal.ads.zzaew;
import com.google.android.gms.internal.ads.zzaey;
import com.google.android.gms.internal.ads.zzafq;
import com.google.android.gms.internal.ads.zzaon;
import com.google.android.gms.internal.ads.zzaos;
import com.google.android.gms.internal.ads.zzato;
import com.google.android.gms.internal.ads.zzawb;
import com.google.android.gms.internal.ads.zzawh;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzbdi;
import com.google.android.gms.internal.ads.zzbdr;
import com.google.android.gms.internal.ads.zzbeu;
import com.google.android.gms.internal.ads.zzbev;
import com.google.android.gms.internal.ads.zzdq;
import com.google.android.gms.internal.ads.zzro;
import com.google.android.gms.internal.ads.zzsm;
import com.google.android.gms.internal.ads.zzty;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;
import java.util.Collections;
import okhttp3.internal.http2.Http2Connection;

public class zzc extends zzaos implements zzy {
    private static final int u = Color.argb(0, 0, 0, 0);

    /* renamed from: a  reason: collision with root package name */
    protected final Activity f3707a;
    AdOverlayInfoParcel b;
    zzbdi c;
    private zzi d;
    private zzq e;
    private boolean f = false;
    private FrameLayout g;
    private WebChromeClient.CustomViewCallback h;
    private boolean i = false;
    private boolean j = false;
    private zzj k;
    private boolean l = false;
    int m = 0;
    private final Object n = new Object();
    private Runnable o;
    private boolean p;
    private boolean q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = true;

    public zzc(Activity activity) {
        this.f3707a = activity;
    }

    private static void a(IObjectWrapper iObjectWrapper, View view) {
        if (iObjectWrapper != null && view != null) {
            zzq.zzlf().zza(iObjectWrapper, view);
        }
    }

    private final void b(boolean z) {
        int intValue = ((Integer) zzve.zzoy().zzd(zzzn.zzcnv)).intValue();
        zzp zzp = new zzp();
        zzp.size = 50;
        zzp.paddingLeft = z ? intValue : 0;
        zzp.paddingRight = z ? 0 : intValue;
        zzp.paddingTop = 0;
        zzp.paddingBottom = intValue;
        this.e = new zzq(this.f3707a, zzp, this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(z ? 11 : 9);
        zza(z, this.b.zzdhs);
        this.k.addView(this.e, layoutParams);
    }

    private final void c(boolean z) throws zzg {
        if (!this.q) {
            this.f3707a.requestWindowFeature(1);
        }
        Window window = this.f3707a.getWindow();
        if (window != null) {
            zzbdi zzbdi = this.b.zzcza;
            zzbev zzaaa = zzbdi != null ? zzbdi.zzaaa() : null;
            boolean z2 = false;
            boolean z3 = zzaaa != null && zzaaa.zzaat();
            this.l = false;
            if (z3) {
                int i2 = this.b.orientation;
                zzq.zzks();
                if (i2 == 6) {
                    if (this.f3707a.getResources().getConfiguration().orientation == 1) {
                        z2 = true;
                    }
                    this.l = z2;
                } else {
                    int i3 = this.b.orientation;
                    zzq.zzks();
                    if (i3 == 7) {
                        if (this.f3707a.getResources().getConfiguration().orientation == 2) {
                            z2 = true;
                        }
                        this.l = z2;
                    }
                }
            }
            boolean z4 = this.l;
            StringBuilder sb = new StringBuilder(46);
            sb.append("Delay onShow to next orientation change: ");
            sb.append(z4);
            zzayu.zzea(sb.toString());
            setRequestedOrientation(this.b.orientation);
            zzq.zzks();
            window.setFlags(Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE, Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE);
            zzayu.zzea("Hardware acceleration on the AdActivity window enabled.");
            if (!this.j) {
                this.k.setBackgroundColor(-16777216);
            } else {
                this.k.setBackgroundColor(u);
            }
            this.f3707a.setContentView(this.k);
            this.q = true;
            if (z) {
                try {
                    zzq.zzkr();
                    this.c = zzbdr.zza(this.f3707a, this.b.zzcza != null ? this.b.zzcza.zzzy() : null, this.b.zzcza != null ? this.b.zzcza.zzzz() : null, true, z3, (zzdq) null, this.b.zzbll, (zzaae) null, (zzi) null, this.b.zzcza != null ? this.b.zzcza.zzyo() : null, zzsm.zzmt(), (zzro) null, false);
                    zzbev zzaaa2 = this.c.zzaaa();
                    AdOverlayInfoParcel adOverlayInfoParcel = this.b;
                    zzaew zzaew = adOverlayInfoParcel.zzcwq;
                    zzaey zzaey = adOverlayInfoParcel.zzcws;
                    zzt zzt = adOverlayInfoParcel.zzdhu;
                    zzbdi zzbdi2 = adOverlayInfoParcel.zzcza;
                    zzaaa2.zza((zzty) null, zzaew, (zzo) null, zzaey, zzt, true, (zzafq) null, zzbdi2 != null ? zzbdi2.zzaaa().zzaas() : null, (zzaon) null, (zzato) null);
                    this.c.zzaaa().zza((zzbeu) new zzf(this));
                    AdOverlayInfoParcel adOverlayInfoParcel2 = this.b;
                    String str = adOverlayInfoParcel2.url;
                    if (str != null) {
                        this.c.loadUrl(str);
                    } else {
                        String str2 = adOverlayInfoParcel2.zzdht;
                        if (str2 != null) {
                            this.c.loadDataWithBaseURL(adOverlayInfoParcel2.zzdhr, str2, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8", (String) null);
                        } else {
                            throw new zzg("No URL or HTML to display in ad overlay.");
                        }
                    }
                    zzbdi zzbdi3 = this.b.zzcza;
                    if (zzbdi3 != null) {
                        zzbdi3.zzb(this);
                    }
                } catch (Exception e2) {
                    zzayu.zzc("Error obtaining webview.", e2);
                    throw new zzg("Could not obtain webview for the overlay.");
                }
            } else {
                this.c = this.b.zzcza;
                this.c.zzbr(this.f3707a);
            }
            this.c.zza(this);
            zzbdi zzbdi4 = this.b.zzcza;
            if (zzbdi4 != null) {
                a(zzbdi4.zzaae(), this.k);
            }
            ViewParent parent = this.c.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) parent).removeView(this.c.getView());
            }
            if (this.j) {
                this.c.zzaam();
            }
            zzbdi zzbdi5 = this.c;
            Activity activity = this.f3707a;
            AdOverlayInfoParcel adOverlayInfoParcel3 = this.b;
            zzbdi5.zza((ViewGroup) null, activity, adOverlayInfoParcel3.zzdhr, adOverlayInfoParcel3.zzdht);
            this.k.addView(this.c.getView(), -1, -1);
            if (!z && !this.l) {
                x();
            }
            b(z3);
            if (this.c.zzaac()) {
                zza(z3, true);
                return;
            }
            return;
        }
        throw new zzg("Invalid activity, no window available.");
    }

    private final void w() {
        if (this.f3707a.isFinishing() && !this.r) {
            this.r = true;
            zzbdi zzbdi = this.c;
            if (zzbdi != null) {
                zzbdi.zzde(this.m);
                synchronized (this.n) {
                    if (!this.p && this.c.zzaai()) {
                        this.o = new zze(this);
                        zzawb.zzdsr.postDelayed(this.o, ((Long) zzve.zzoy().zzd(zzzn.zzcjg)).longValue());
                        return;
                    }
                }
            }
            b();
        }
    }

    private final void x() {
        this.c.zztr();
    }

    public final void close() {
        this.m = 2;
        this.f3707a.finish();
    }

    public final void onActivityResult(int i2, int i3, Intent intent) {
    }

    public final void onBackPressed() {
        this.m = 0;
    }

    public void onCreate(Bundle bundle) {
        this.f3707a.requestWindowFeature(1);
        this.i = bundle != null && bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
        try {
            this.b = AdOverlayInfoParcel.zzc(this.f3707a.getIntent());
            if (this.b != null) {
                if (this.b.zzbll.zzdwa > 7500000) {
                    this.m = 3;
                }
                if (this.f3707a.getIntent() != null) {
                    this.t = this.f3707a.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
                }
                if (this.b.zzdhx != null) {
                    this.j = this.b.zzdhx.zzbkx;
                } else {
                    this.j = false;
                }
                if (this.j && this.b.zzdhx.zzblc != -1) {
                    new zzl(this).zzvr();
                }
                if (bundle == null) {
                    if (this.b.zzdhq != null && this.t) {
                        this.b.zzdhq.zztf();
                    }
                    if (!(this.b.zzdhv == 1 || this.b.zzcbt == null)) {
                        this.b.zzcbt.onAdClicked();
                    }
                }
                this.k = new zzj(this.f3707a, this.b.zzdhw, this.b.zzbll.zzbma);
                this.k.setId(1000);
                zzq.zzks().zzg(this.f3707a);
                int i2 = this.b.zzdhv;
                if (i2 == 1) {
                    c(false);
                } else if (i2 == 2) {
                    this.d = new zzi(this.b.zzcza);
                    c(false);
                } else if (i2 == 3) {
                    c(true);
                } else {
                    throw new zzg("Could not determine ad overlay type.");
                }
            } else {
                throw new zzg("Could not get info for ad overlay.");
            }
        } catch (zzg e2) {
            zzayu.zzez(e2.getMessage());
            this.m = 3;
            this.f3707a.finish();
        }
    }

    public final void onDestroy() {
        zzbdi zzbdi = this.c;
        if (zzbdi != null) {
            try {
                this.k.removeView(zzbdi.getView());
            } catch (NullPointerException unused) {
            }
        }
        w();
    }

    public final void onPause() {
        zztk();
        zzo zzo = this.b.zzdhq;
        if (zzo != null) {
            zzo.onPause();
        }
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue() && this.c != null && (!this.f3707a.isFinishing() || this.d == null)) {
            zzq.zzks();
            zzawh.zza(this.c);
        }
        w();
    }

    public final void onRestart() {
    }

    public final void onResume() {
        zzo zzo = this.b.zzdhq;
        if (zzo != null) {
            zzo.onResume();
        }
        a(this.f3707a.getResources().getConfiguration());
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue()) {
            zzbdi zzbdi = this.c;
            if (zzbdi == null || zzbdi.isDestroyed()) {
                zzayu.zzez("The webview does not exist. Ignoring action.");
                return;
            }
            zzq.zzks();
            zzawh.zzb(this.c);
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.i);
    }

    public final void onStart() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue()) {
            zzbdi zzbdi = this.c;
            if (zzbdi == null || zzbdi.isDestroyed()) {
                zzayu.zzez("The webview does not exist. Ignoring action.");
                return;
            }
            zzq.zzks();
            zzawh.zzb(this.c);
        }
    }

    public final void onStop() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue() && this.c != null && (!this.f3707a.isFinishing() || this.d == null)) {
            zzq.zzks();
            zzawh.zza(this.c);
        }
        w();
    }

    public final void setRequestedOrientation(int i2) {
        if (this.f3707a.getApplicationInfo().targetSdkVersion >= ((Integer) zzve.zzoy().zzd(zzzn.zzcpr)).intValue()) {
            if (this.f3707a.getApplicationInfo().targetSdkVersion <= ((Integer) zzve.zzoy().zzd(zzzn.zzcps)).intValue()) {
                if (Build.VERSION.SDK_INT >= ((Integer) zzve.zzoy().zzd(zzzn.zzcpt)).intValue()) {
                    if (Build.VERSION.SDK_INT <= ((Integer) zzve.zzoy().zzd(zzzn.zzcpu)).intValue()) {
                        return;
                    }
                }
            }
        }
        try {
            this.f3707a.setRequestedOrientation(i2);
        } catch (Throwable th) {
            zzq.zzku().zzb(th, "AdOverlay.setRequestedOrientation");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0018, code lost:
        r0 = (r0 = r6.b).zzdhx;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(boolean r7, boolean r8) {
        /*
            r6 = this;
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcjh
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r1.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0022
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r0 = r6.b
            if (r0 == 0) goto L_0x0022
            com.google.android.gms.ads.internal.zzg r0 = r0.zzdhx
            if (r0 == 0) goto L_0x0022
            boolean r0 = r0.zzble
            if (r0 == 0) goto L_0x0022
            r0 = 1
            goto L_0x0023
        L_0x0022:
            r0 = 0
        L_0x0023:
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r3 = com.google.android.gms.internal.ads.zzzn.zzcji
            com.google.android.gms.internal.ads.zzzj r4 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r3 = r4.zzd(r3)
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x0043
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r3 = r6.b
            if (r3 == 0) goto L_0x0043
            com.google.android.gms.ads.internal.zzg r3 = r3.zzdhx
            if (r3 == 0) goto L_0x0043
            boolean r3 = r3.zzblf
            if (r3 == 0) goto L_0x0043
            r3 = 1
            goto L_0x0044
        L_0x0043:
            r3 = 0
        L_0x0044:
            if (r7 == 0) goto L_0x005a
            if (r8 == 0) goto L_0x005a
            if (r0 == 0) goto L_0x005a
            if (r3 != 0) goto L_0x005a
            com.google.android.gms.internal.ads.zzaoo r7 = new com.google.android.gms.internal.ads.zzaoo
            com.google.android.gms.internal.ads.zzbdi r4 = r6.c
            java.lang.String r5 = "useCustomClose"
            r7.<init>(r4, r5)
            java.lang.String r4 = "Custom close has been disabled for interstitial ads in this ad slot."
            r7.zzds(r4)
        L_0x005a:
            com.google.android.gms.ads.internal.overlay.zzq r7 = r6.e
            if (r7 == 0) goto L_0x0069
            if (r3 != 0) goto L_0x0066
            if (r8 == 0) goto L_0x0065
            if (r0 != 0) goto L_0x0065
            goto L_0x0066
        L_0x0065:
            r1 = 0
        L_0x0066:
            r7.zzal(r1)
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.overlay.zzc.zza(boolean, boolean):void");
    }

    public final void zzad(IObjectWrapper iObjectWrapper) {
        a((Configuration) ObjectWrapper.a(iObjectWrapper));
    }

    public final void zzdf() {
        this.q = true;
    }

    public final void zztk() {
        AdOverlayInfoParcel adOverlayInfoParcel = this.b;
        if (adOverlayInfoParcel != null && this.f) {
            setRequestedOrientation(adOverlayInfoParcel.orientation);
        }
        if (this.g != null) {
            this.f3707a.setContentView(this.k);
            this.q = true;
            this.g.removeAllViews();
            this.g = null;
        }
        WebChromeClient.CustomViewCallback customViewCallback = this.h;
        if (customViewCallback != null) {
            customViewCallback.onCustomViewHidden();
            this.h = null;
        }
        this.f = false;
    }

    public final void zztl() {
        this.m = 1;
        this.f3707a.finish();
    }

    public final boolean zztm() {
        this.m = 0;
        zzbdi zzbdi = this.c;
        if (zzbdi == null) {
            return true;
        }
        boolean zzaah = zzbdi.zzaah();
        if (!zzaah) {
            this.c.zza("onbackblocked", Collections.emptyMap());
        }
        return zzaah;
    }

    public final void zztn() {
        this.k.removeView(this.e);
        b(true);
    }

    public final void zztq() {
        if (this.l) {
            this.l = false;
            x();
        }
    }

    public final void zzts() {
        this.k.b = true;
    }

    public final void zztt() {
        synchronized (this.n) {
            this.p = true;
            if (this.o != null) {
                zzawb.zzdsr.removeCallbacks(this.o);
                zzawb.zzdsr.post(this.o);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = r0.zzdhx;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(android.content.res.Configuration r6) {
        /*
            r5 = this;
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r0 = r5.b
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0010
            com.google.android.gms.ads.internal.zzg r0 = r0.zzdhx
            if (r0 == 0) goto L_0x0010
            boolean r0 = r0.zzbky
            if (r0 == 0) goto L_0x0010
            r0 = 1
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            com.google.android.gms.internal.ads.zzawh r3 = com.google.android.gms.ads.internal.zzq.zzks()
            android.app.Activity r4 = r5.f3707a
            boolean r6 = r3.zza((android.app.Activity) r4, (android.content.res.Configuration) r6)
            boolean r3 = r5.j
            r4 = 19
            if (r3 == 0) goto L_0x0023
            if (r0 == 0) goto L_0x0037
        L_0x0023:
            if (r6 != 0) goto L_0x0037
            int r6 = android.os.Build.VERSION.SDK_INT
            if (r6 < r4) goto L_0x0038
            com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel r6 = r5.b
            if (r6 == 0) goto L_0x0038
            com.google.android.gms.ads.internal.zzg r6 = r6.zzdhx
            if (r6 == 0) goto L_0x0038
            boolean r6 = r6.zzbld
            if (r6 == 0) goto L_0x0038
            r2 = 1
            goto L_0x0038
        L_0x0037:
            r1 = 0
        L_0x0038:
            android.app.Activity r6 = r5.f3707a
            android.view.Window r6 = r6.getWindow()
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcjj
            com.google.android.gms.internal.ads.zzzj r3 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r3.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0066
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r4) goto L_0x0066
            android.view.View r6 = r6.getDecorView()
            r0 = 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0062
            r0 = 5380(0x1504, float:7.539E-42)
            if (r2 == 0) goto L_0x0062
            r0 = 5894(0x1706, float:8.259E-42)
        L_0x0062:
            r6.setSystemUiVisibility(r0)
            return
        L_0x0066:
            r0 = 1024(0x400, float:1.435E-42)
            r3 = 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0082
            r6.addFlags(r0)
            r6.clearFlags(r3)
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r4) goto L_0x0088
            if (r2 == 0) goto L_0x0088
            android.view.View r6 = r6.getDecorView()
            r0 = 4098(0x1002, float:5.743E-42)
            r6.setSystemUiVisibility(r0)
            return
        L_0x0082:
            r6.addFlags(r3)
            r6.clearFlags(r0)
        L_0x0088:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.internal.overlay.zzc.a(android.content.res.Configuration):void");
    }

    public final void zza(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        this.g = new FrameLayout(this.f3707a);
        this.g.setBackgroundColor(-16777216);
        this.g.addView(view, -1, -1);
        this.f3707a.setContentView(this.g);
        this.q = true;
        this.h = customViewCallback;
        this.f = true;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        zzbdi zzbdi;
        zzo zzo;
        if (!this.s) {
            this.s = true;
            zzbdi zzbdi2 = this.c;
            if (zzbdi2 != null) {
                this.k.removeView(zzbdi2.getView());
                zzi zzi = this.d;
                if (zzi != null) {
                    this.c.zzbr(zzi.zzup);
                    this.c.zzax(false);
                    ViewGroup viewGroup = this.d.parent;
                    View view = this.c.getView();
                    zzi zzi2 = this.d;
                    viewGroup.addView(view, zzi2.index, zzi2.zzdhj);
                    this.d = null;
                } else if (this.f3707a.getApplicationContext() != null) {
                    this.c.zzbr(this.f3707a.getApplicationContext());
                }
                this.c = null;
            }
            AdOverlayInfoParcel adOverlayInfoParcel = this.b;
            if (!(adOverlayInfoParcel == null || (zzo = adOverlayInfoParcel.zzdhq) == null)) {
                zzo.zzte();
            }
            AdOverlayInfoParcel adOverlayInfoParcel2 = this.b;
            if (adOverlayInfoParcel2 != null && (zzbdi = adOverlayInfoParcel2.zzcza) != null) {
                a(zzbdi.zzaae(), this.b.zzcza.getView());
            }
        }
    }
}
