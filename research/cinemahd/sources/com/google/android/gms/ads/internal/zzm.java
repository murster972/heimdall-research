package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzdq;
import com.google.android.gms.internal.ads.zzdr;
import java.util.concurrent.Callable;

final class zzm implements Callable<zzdq> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzl f3723a;

    zzm(zzl zzl) {
        this.f3723a = zzl;
    }

    public final /* synthetic */ Object call() throws Exception {
        return new zzdq(zzdr.zza(this.f3723a.f3722a.zzbma, this.f3723a.d, false));
    }
}
