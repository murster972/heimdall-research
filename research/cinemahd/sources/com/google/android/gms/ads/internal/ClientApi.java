package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzr;
import com.google.android.gms.ads.internal.overlay.zzs;
import com.google.android.gms.ads.internal.overlay.zzu;
import com.google.android.gms.ads.internal.overlay.zzx;
import com.google.android.gms.ads.internal.overlay.zzz;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzacm;
import com.google.android.gms.internal.ads.zzacp;
import com.google.android.gms.internal.ads.zzalc;
import com.google.android.gms.internal.ads.zzaot;
import com.google.android.gms.internal.ads.zzapd;
import com.google.android.gms.internal.ads.zzarl;
import com.google.android.gms.internal.ads.zzasg;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzbfx;
import com.google.android.gms.internal.ads.zzbxh;
import com.google.android.gms.internal.ads.zzbxi;
import com.google.android.gms.internal.ads.zzcom;
import com.google.android.gms.internal.ads.zzcoo;
import com.google.android.gms.internal.ads.zzcor;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzvn;
import com.google.android.gms.internal.ads.zzvu;
import com.google.android.gms.internal.ads.zzwg;
import com.google.android.gms.internal.ads.zzwk;
import java.util.HashMap;

public class ClientApi extends zzwg {
    public final zzvu zza(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, zzalc zzalc, int i) {
        Context context = (Context) ObjectWrapper.a(iObjectWrapper);
        return new zzcoo(zzbfx.zza(context, zzalc, i), context, zzuj, str);
    }

    public final zzvu zzb(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, zzalc zzalc, int i) {
        Context context = (Context) ObjectWrapper.a(iObjectWrapper);
        return new zzcor(zzbfx.zza(context, zzalc, i), context, zzuj, str);
    }

    public final zzvu zzc(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, zzalc zzalc, int i) {
        Context context = (Context) ObjectWrapper.a(iObjectWrapper);
        return zzbfx.zza(context, zzalc, i).zzacj().zzfq(str).zzbt(context).zzaeb().zzaea();
    }

    public final zzwk zzc(IObjectWrapper iObjectWrapper) {
        return null;
    }

    public final zzapd zzd(IObjectWrapper iObjectWrapper) {
        return null;
    }

    public final zzvn zza(IObjectWrapper iObjectWrapper, String str, zzalc zzalc, int i) {
        Context context = (Context) ObjectWrapper.a(iObjectWrapper);
        return new zzcom(zzbfx.zza(context, zzalc, i), context, str);
    }

    public final zzaot zzb(IObjectWrapper iObjectWrapper) {
        Activity activity = (Activity) ObjectWrapper.a(iObjectWrapper);
        AdOverlayInfoParcel zzc = AdOverlayInfoParcel.zzc(activity.getIntent());
        if (zzc == null) {
            return new zzr(activity);
        }
        int i = zzc.zzdhv;
        if (i == 1) {
            return new zzs(activity);
        }
        if (i == 2) {
            return new zzx(activity);
        }
        if (i == 3) {
            return new zzz(activity);
        }
        if (i != 4) {
            return new zzr(activity);
        }
        return new zzu(activity, zzc);
    }

    public final zzacm zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) {
        return new zzbxh((FrameLayout) ObjectWrapper.a(iObjectWrapper), (FrameLayout) ObjectWrapper.a(iObjectWrapper2), 19649000);
    }

    public final zzarl zza(IObjectWrapper iObjectWrapper, zzalc zzalc, int i) {
        Context context = (Context) ObjectWrapper.a(iObjectWrapper);
        return zzbfx.zza(context, zzalc, i).zzacn().zzbu(context).zzaer().zzaep();
    }

    public final zzasg zzb(IObjectWrapper iObjectWrapper, String str, zzalc zzalc, int i) {
        Context context = (Context) ObjectWrapper.a(iObjectWrapper);
        return zzbfx.zza(context, zzalc, i).zzacn().zzbu(context).zzfr(str).zzaer().zzaeq();
    }

    public final zzwk zza(IObjectWrapper iObjectWrapper, int i) {
        return zzbfx.zzd((Context) ObjectWrapper.a(iObjectWrapper), i).zzacg();
    }

    public final zzvu zza(IObjectWrapper iObjectWrapper, zzuj zzuj, String str, int i) {
        return new zzl((Context) ObjectWrapper.a(iObjectWrapper), zzuj, str, new zzazb(19649000, i, true, false));
    }

    public final zzacp zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) {
        return new zzbxi((View) ObjectWrapper.a(iObjectWrapper), (HashMap) ObjectWrapper.a(iObjectWrapper2), (HashMap) ObjectWrapper.a(iObjectWrapper3));
    }
}
