package com.google.android.gms.ads.appopen;

import android.content.Context;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzrl;
import com.google.android.gms.internal.ads.zzrp;
import com.google.android.gms.internal.ads.zzvu;

public abstract class AppOpenAd {
    public static final int APP_OPEN_AD_ORIENTATION_LANDSCAPE = 2;
    public static final int APP_OPEN_AD_ORIENTATION_PORTRAIT = 1;

    public static abstract class AppOpenAdLoadCallback {
        public void onAppOpenAdFailedToLoad(int i) {
        }

        public void onAppOpenAdLoaded(AppOpenAd appOpenAd) {
        }
    }

    public @interface AppOpenAdOrientation {
    }

    public static void load(Context context, String str, AdRequest adRequest, @AppOpenAdOrientation int i, AppOpenAdLoadCallback appOpenAdLoadCallback) {
        Preconditions.a(context, (Object) "Context cannot be null.");
        Preconditions.a(str, (Object) "adUnitId cannot be null.");
        Preconditions.a(adRequest, (Object) "AdRequest cannot be null.");
        new zzrp(context, str, adRequest.zzdg(), i, appOpenAdLoadCallback).zzmn();
    }

    /* access modifiers changed from: protected */
    public abstract void zza(zzrl zzrl);

    /* access modifiers changed from: protected */
    public abstract zzvu zzdm();

    public static void load(Context context, String str, PublisherAdRequest publisherAdRequest, @AppOpenAdOrientation int i, AppOpenAdLoadCallback appOpenAdLoadCallback) {
        Preconditions.a(context, (Object) "Context cannot be null.");
        Preconditions.a(str, (Object) "adUnitId cannot be null.");
        Preconditions.a(publisherAdRequest, (Object) "PublisherAdRequest cannot be null.");
        new zzrp(context, str, publisherAdRequest.zzdg(), i, appOpenAdLoadCallback).zzmn();
    }
}
