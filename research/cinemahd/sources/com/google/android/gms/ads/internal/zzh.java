package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzazd;
import com.google.android.gms.internal.ads.zzdg;
import com.google.android.gms.internal.ads.zzdr;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public final class zzh implements zzdg, Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final List<Object[]> f3720a = new Vector();
    private final AtomicReference<zzdg> b = new AtomicReference<>();
    private Context c;
    private zzazb d;
    private CountDownLatch e = new CountDownLatch(1);

    public zzh(Context context, zzazb zzazb) {
        this.c = context;
        this.d = zzazb;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclm)).booleanValue()) {
            zzazd.zzdwe.execute(this);
            return;
        }
        zzve.zzou();
        if (zzayk.zzxe()) {
            zzazd.zzdwe.execute(this);
        } else {
            run();
        }
    }

    private final boolean a() {
        try {
            this.e.await();
            return true;
        } catch (InterruptedException e2) {
            zzayu.zzd("Interrupted during GADSignals creation.", e2);
            return false;
        }
    }

    private final void b() {
        if (!this.f3720a.isEmpty()) {
            for (Object[] next : this.f3720a) {
                if (next.length == 1) {
                    this.b.get().zza((MotionEvent) next[0]);
                } else if (next.length == 3) {
                    this.b.get().zza(((Integer) next[0]).intValue(), ((Integer) next[1]).intValue(), ((Integer) next[2]).intValue());
                }
            }
            this.f3720a.clear();
        }
    }

    public final void run() {
        boolean z = false;
        try {
            boolean z2 = this.d.zzdwb;
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcjf)).booleanValue() && z2) {
                z = true;
            }
            this.b.set(zzdr.zza(this.d.zzbma, a(this.c), z));
        } finally {
            this.e.countDown();
            this.c = null;
            this.d = null;
        }
    }

    public final String zza(Context context, View view, Activity activity) {
        zzdg zzdg = this.b.get();
        return zzdg != null ? zzdg.zza(context, view, activity) : "";
    }

    public final String zzb(Context context) {
        zzdg zzdg;
        if (!a() || (zzdg = this.b.get()) == null) {
            return "";
        }
        b();
        return zzdg.zzb(a(context));
    }

    private static Context a(Context context) {
        Context applicationContext = context.getApplicationContext();
        return applicationContext == null ? context : applicationContext;
    }

    public final String zza(Context context, String str, View view) {
        return zza(context, str, view, (Activity) null);
    }

    public final String zza(Context context, String str, View view, Activity activity) {
        zzdg zzdg;
        if (!a() || (zzdg = this.b.get()) == null) {
            return "";
        }
        b();
        return zzdg.zza(a(context), str, view, activity);
    }

    public final void zzb(View view) {
        zzdg zzdg = this.b.get();
        if (zzdg != null) {
            zzdg.zzb(view);
        }
    }

    public final void zza(MotionEvent motionEvent) {
        zzdg zzdg = this.b.get();
        if (zzdg != null) {
            b();
            zzdg.zza(motionEvent);
            return;
        }
        this.f3720a.add(new Object[]{motionEvent});
    }

    public final void zza(int i, int i2, int i3) {
        zzdg zzdg = this.b.get();
        if (zzdg != null) {
            b();
            zzdg.zza(i, i2, i3);
            return;
        }
        this.f3720a.add(new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)});
    }
}
