package com.google.android.gms.ads;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.reward.AdMetadataListener;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzty;
import com.google.android.gms.internal.ads.zzxn;

public final class InterstitialAd {

    /* renamed from: a  reason: collision with root package name */
    private final zzxn f3683a;

    public InterstitialAd(Context context) {
        this.f3683a = new zzxn(context);
        Preconditions.a(context, (Object) "Context cannot be null");
    }

    public final AdListener getAdListener() {
        return this.f3683a.getAdListener();
    }

    public final Bundle getAdMetadata() {
        return this.f3683a.getAdMetadata();
    }

    public final String getAdUnitId() {
        return this.f3683a.getAdUnitId();
    }

    public final String getMediationAdapterClassName() {
        return this.f3683a.getMediationAdapterClassName();
    }

    public final boolean isLoaded() {
        return this.f3683a.isLoaded();
    }

    public final boolean isLoading() {
        return this.f3683a.isLoading();
    }

    public final void loadAd(AdRequest adRequest) {
        this.f3683a.zza(adRequest.zzdg());
    }

    public final void setAdListener(AdListener adListener) {
        this.f3683a.setAdListener(adListener);
        if (adListener != null && (adListener instanceof zzty)) {
            this.f3683a.zza((zzty) adListener);
        } else if (adListener == null) {
            this.f3683a.zza((zzty) null);
        }
    }

    public final void setAdMetadataListener(AdMetadataListener adMetadataListener) {
        this.f3683a.setAdMetadataListener(adMetadataListener);
    }

    public final void setAdUnitId(String str) {
        this.f3683a.setAdUnitId(str);
    }

    public final void setImmersiveMode(boolean z) {
        this.f3683a.setImmersiveMode(z);
    }

    public final void setRewardedVideoAdListener(RewardedVideoAdListener rewardedVideoAdListener) {
        this.f3683a.setRewardedVideoAdListener(rewardedVideoAdListener);
    }

    public final void show() {
        this.f3683a.show();
    }

    public final void zzd(boolean z) {
        this.f3683a.zzd(true);
    }
}
