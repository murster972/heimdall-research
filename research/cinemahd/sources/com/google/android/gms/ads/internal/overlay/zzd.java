package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzd extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzd> CREATOR = new zza();

    /* renamed from: a  reason: collision with root package name */
    private final String f3708a;
    private final String b;
    public final Intent intent;
    public final String mimeType;
    public final String packageName;
    public final String url;
    public final String zzdhf;
    public final String zzdhg;

    public zzd(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this(str, str2, str3, str4, str5, str6, str7, (Intent) null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 2, this.f3708a, false);
        SafeParcelWriter.a(parcel, 3, this.url, false);
        SafeParcelWriter.a(parcel, 4, this.mimeType, false);
        SafeParcelWriter.a(parcel, 5, this.packageName, false);
        SafeParcelWriter.a(parcel, 6, this.zzdhf, false);
        SafeParcelWriter.a(parcel, 7, this.zzdhg, false);
        SafeParcelWriter.a(parcel, 8, this.b, false);
        SafeParcelWriter.a(parcel, 9, (Parcelable) this.intent, i, false);
        SafeParcelWriter.a(parcel, a2);
    }

    public zzd(Intent intent2) {
        this((String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, intent2);
    }

    public zzd(String str, String str2, String str3, String str4, String str5, String str6, String str7, Intent intent2) {
        this.f3708a = str;
        this.url = str2;
        this.mimeType = str3;
        this.packageName = str4;
        this.zzdhf = str5;
        this.zzdhg = str6;
        this.b = str7;
        this.intent = intent2;
    }
}
