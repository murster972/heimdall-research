package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.graphics.Bitmap;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzavo;
import com.google.android.gms.internal.ads.zzawb;
import com.google.android.gms.internal.ads.zzawh;

final class zzl extends zzavo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ zzc f3713a;

    private zzl(zzc zzc) {
        this.f3713a = zzc;
    }

    public final void zztu() {
        Bitmap zza = zzq.zzlj().zza(Integer.valueOf(this.f3713a.b.zzdhx.zzblc));
        if (zza != null) {
            zzawh zzks = zzq.zzks();
            zzc zzc = this.f3713a;
            Activity activity = zzc.f3707a;
            zzg zzg = zzc.b.zzdhx;
            zzawb.zzdsr.post(new zzk(this, zzks.zza(activity, zza, zzg.zzbla, zzg.zzblb)));
        }
    }
}
