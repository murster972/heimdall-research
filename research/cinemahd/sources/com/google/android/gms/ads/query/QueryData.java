package com.google.android.gms.ads.query;

import com.google.android.gms.internal.ads.zzapj;
import com.google.android.gms.internal.ads.zzxx;

public class QueryData {

    /* renamed from: a  reason: collision with root package name */
    private zzxx f3742a;

    public QueryData(zzxx zzxx) {
        this.f3742a = zzxx;
    }

    public static void generate(QueryDataConfiguration queryDataConfiguration, QueryDataGenerationCallback queryDataGenerationCallback) {
        new zzapj(queryDataConfiguration).zza(queryDataGenerationCallback);
    }

    public String getQuery() {
        return this.f3742a.getQuery();
    }
}
