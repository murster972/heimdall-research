package com.google.android.gms.ads.internal;

import android.view.MotionEvent;
import android.view.View;

final class zzn implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzl f3724a;

    zzn(zzl zzl) {
        this.f3724a = zzl;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f3724a.h == null) {
            return false;
        }
        this.f3724a.h.zza(motionEvent);
        return false;
    }
}
