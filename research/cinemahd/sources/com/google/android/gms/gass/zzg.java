package com.google.android.gms.gass;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.gass.internal.zzd;
import com.google.android.gms.gass.internal.zzm;
import com.google.android.gms.gass.internal.zzo;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

final class zzg implements BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener {

    /* renamed from: a  reason: collision with root package name */
    private zzd f4061a;
    private final String b;
    private final String c;
    private final int d = 1;
    private final LinkedBlockingQueue<zzo> e;
    private final HandlerThread f;
    private final AdShield2Logger g;
    private final long h;

    public zzg(Context context, int i, String str, String str2, String str3, AdShield2Logger adShield2Logger) {
        this.b = str;
        this.c = str2;
        this.g = adShield2Logger;
        this.f = new HandlerThread("GassDGClient");
        this.f.start();
        this.h = System.currentTimeMillis();
        this.f4061a = new zzd(context, this.f.getLooper(), this, this);
        this.e = new LinkedBlockingQueue<>();
        this.f4061a.checkAvailabilityAndConnect();
    }

    private final com.google.android.gms.gass.internal.zzg b() {
        try {
            return this.f4061a.b();
        } catch (DeadObjectException | IllegalStateException unused) {
            return null;
        }
    }

    private static zzo c() {
        return new zzo((byte[]) null);
    }

    public final zzo a(int i) {
        zzo zzo;
        try {
            zzo = this.e.poll(50000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e2) {
            a(2009, this.h, e2);
            zzo = null;
        }
        a(3004, this.h, (Exception) null);
        return zzo == null ? c() : zzo;
    }

    public final void onConnected(Bundle bundle) {
        com.google.android.gms.gass.internal.zzg b2 = b();
        if (b2 != null) {
            try {
                this.e.put(b2.a(new zzm(this.d, this.b, this.c)));
            } catch (Throwable th) {
                a(2010, this.h, new Exception(th));
            } finally {
                a();
                this.f.quit();
            }
        }
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            this.e.put(c());
        } catch (InterruptedException unused) {
        }
    }

    public final void onConnectionSuspended(int i) {
        try {
            this.e.put(c());
        } catch (InterruptedException unused) {
        }
    }

    private final void a() {
        zzd zzd = this.f4061a;
        if (zzd == null) {
            return;
        }
        if (zzd.isConnected() || this.f4061a.isConnecting()) {
            this.f4061a.disconnect();
        }
    }

    private final void a(int i, long j, Exception exc) {
        AdShield2Logger adShield2Logger = this.g;
        if (adShield2Logger != null) {
            adShield2Logger.a(i, System.currentTimeMillis() - j, exc);
        }
    }
}
