package com.google.android.gms.gass;

import android.content.Context;
import com.google.android.gms.gass.internal.zzo;

public final class zzd {
    public static zzo a(Context context, int i, String str, String str2, String str3, AdShield2Logger adShield2Logger) {
        return new zzg(context, 1, str, str2, str3, adShield2Logger).a(50000);
    }
}
