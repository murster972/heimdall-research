package com.google.android.gms.gass.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzgc;
import com.google.android.gms.internal.ads.zzge;

public final class zzi extends zzgc implements zzg {
    zzi(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gass.internal.IGassService");
    }

    public final zze a(zzc zzc) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, (Parcelable) zzc);
        Parcel transactAndReadException = transactAndReadException(1, obtainAndWriteInterfaceToken);
        zze zze = (zze) zzge.zza(transactAndReadException, zze.CREATOR);
        transactAndReadException.recycle();
        return zze;
    }

    public final void a(zzb zzb) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, (Parcelable) zzb);
        zza(2, obtainAndWriteInterfaceToken);
    }

    public final zzo a(zzm zzm) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, (Parcelable) zzm);
        Parcel transactAndReadException = transactAndReadException(3, obtainAndWriteInterfaceToken);
        zzo zzo = (zzo) zzge.zza(transactAndReadException, zzo.CREATOR);
        transactAndReadException.recycle();
        return zzo;
    }
}
