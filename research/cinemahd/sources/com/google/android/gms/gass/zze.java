package com.google.android.gms.gass;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.gass.internal.zzd;
import com.google.android.gms.gass.internal.zzg;
import com.google.android.gms.internal.ads.zzbs;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

final class zze implements BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener {

    /* renamed from: a  reason: collision with root package name */
    private zzd f4059a;
    private final String b;
    private final String c;
    private final LinkedBlockingQueue<zzbs.zza> d;
    private final HandlerThread e = new HandlerThread("GassClient");

    public zze(Context context, String str, String str2) {
        this.b = str;
        this.c = str2;
        this.e.start();
        this.f4059a = new zzd(context, this.e.getLooper(), this, this);
        this.d = new LinkedBlockingQueue<>();
        this.f4059a.checkAvailabilityAndConnect();
    }

    private final zzg b() {
        try {
            return this.f4059a.b();
        } catch (DeadObjectException | IllegalStateException unused) {
            return null;
        }
    }

    private static zzbs.zza c() {
        return (zzbs.zza) zzbs.zza.zzan().zzau(PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID).zzbaf();
    }

    public final zzbs.zza a(int i) {
        zzbs.zza zza;
        try {
            zza = this.d.poll(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException unused) {
            zza = null;
        }
        return zza == null ? c() : zza;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:6|7|11|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0038, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        a();
        r3.e.quit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r3.d.put(c());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002f, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0030, code lost:
        a();
        r3.e.quit();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0025 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onConnected(android.os.Bundle r4) {
        /*
            r3 = this;
            com.google.android.gms.gass.internal.zzg r4 = r3.b()
            if (r4 == 0) goto L_0x0041
            com.google.android.gms.gass.internal.zzc r0 = new com.google.android.gms.gass.internal.zzc     // Catch:{ all -> 0x0025 }
            java.lang.String r1 = r3.b     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = r3.c     // Catch:{ all -> 0x0025 }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.gass.internal.zze r4 = r4.a((com.google.android.gms.gass.internal.zzc) r0)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzbs$zza r4 = r4.s()     // Catch:{ all -> 0x0025 }
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.ads.zzbs$zza> r0 = r3.d     // Catch:{ all -> 0x0025 }
            r0.put(r4)     // Catch:{ all -> 0x0025 }
            r3.a()
            android.os.HandlerThread r4 = r3.e
            r4.quit()
            return
        L_0x0025:
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.ads.zzbs$zza> r4 = r3.d     // Catch:{ InterruptedException -> 0x0039, all -> 0x002f }
            com.google.android.gms.internal.ads.zzbs$zza r0 = c()     // Catch:{ InterruptedException -> 0x0039, all -> 0x002f }
            r4.put(r0)     // Catch:{ InterruptedException -> 0x0039, all -> 0x002f }
            goto L_0x0039
        L_0x002f:
            r4 = move-exception
            r3.a()
            android.os.HandlerThread r0 = r3.e
            r0.quit()
            throw r4
        L_0x0039:
            r3.a()
            android.os.HandlerThread r4 = r3.e
            r4.quit()
        L_0x0041:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.gass.zze.onConnected(android.os.Bundle):void");
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            this.d.put(c());
        } catch (InterruptedException unused) {
        }
    }

    public final void onConnectionSuspended(int i) {
        try {
            this.d.put(c());
        } catch (InterruptedException unused) {
        }
    }

    private final void a() {
        zzd zzd = this.f4059a;
        if (zzd == null) {
            return;
        }
        if (zzd.isConnected() || this.f4059a.isConnecting()) {
            this.f4059a.disconnect();
        }
    }
}
