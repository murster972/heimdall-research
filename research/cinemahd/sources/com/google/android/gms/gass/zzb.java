package com.google.android.gms.gass;

import android.content.Context;
import com.google.android.gms.internal.ads.zzsr;
import java.util.concurrent.Callable;

final /* synthetic */ class zzb implements Callable {

    /* renamed from: a  reason: collision with root package name */
    private final Context f4058a;

    zzb(Context context) {
        this.f4058a = context;
    }

    public final Object call() {
        return new zzsr(this.f4058a, "GLAS", (String) null);
    }
}
