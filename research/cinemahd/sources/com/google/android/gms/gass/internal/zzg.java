package com.google.android.gms.gass.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzg extends IInterface {
    zze a(zzc zzc) throws RemoteException;

    zzo a(zzm zzm) throws RemoteException;

    void a(zzb zzb) throws RemoteException;
}
