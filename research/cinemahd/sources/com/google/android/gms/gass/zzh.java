package com.google.android.gms.gass;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.gass.internal.zzb;
import com.google.android.gms.gass.internal.zzd;
import com.google.android.gms.internal.ads.zzddn;

final class zzh implements BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener {

    /* renamed from: a  reason: collision with root package name */
    private final zzd f4062a;
    private final zzddn b;
    private final Object c = new Object();
    private boolean d = false;
    private boolean e = false;

    zzh(Context context, Looper looper, zzddn zzddn) {
        this.b = zzddn;
        this.f4062a = new zzd(context, looper, this, this);
    }

    private final void b() {
        synchronized (this.c) {
            if (this.f4062a.isConnected() || this.f4062a.isConnecting()) {
                this.f4062a.disconnect();
            }
            Binder.flushPendingCommands();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        synchronized (this.c) {
            if (!this.d) {
                this.d = true;
                this.f4062a.checkAvailabilityAndConnect();
            }
        }
    }

    public final void onConnected(Bundle bundle) {
        synchronized (this.c) {
            if (!this.e) {
                this.e = true;
                try {
                    this.f4062a.b().a(new zzb(this.b.toByteArray()));
                    b();
                } catch (Exception unused) {
                    b();
                } catch (Throwable th) {
                    b();
                    throw th;
                }
            }
        }
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public final void onConnectionSuspended(int i) {
    }
}
