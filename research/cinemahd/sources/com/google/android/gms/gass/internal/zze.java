package com.google.android.gms.gass.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.ads.zzbs;
import com.google.android.gms.internal.ads.zzdrg;
import com.google.android.gms.internal.ads.zzdse;

public final class zze extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zze> CREATOR = new zzh();

    /* renamed from: a  reason: collision with root package name */
    private final int f4052a;
    private zzbs.zza b = null;
    private byte[] c;

    zze(int i, byte[] bArr) {
        this.f4052a = i;
        this.c = bArr;
        t();
    }

    private final void t() {
        if (this.b == null && this.c != null) {
            return;
        }
        if (this.b != null && this.c == null) {
            return;
        }
        if (this.b != null && this.c != null) {
            throw new IllegalStateException("Invalid internal representation - full");
        } else if (this.b == null && this.c == null) {
            throw new IllegalStateException("Invalid internal representation - empty");
        } else {
            throw new IllegalStateException("Impossible");
        }
    }

    public final zzbs.zza s() {
        if (!(this.b != null)) {
            try {
                this.b = zzbs.zza.zza(this.c, zzdrg.zzazi());
                this.c = null;
            } catch (zzdse e) {
                throw new IllegalStateException(e);
            }
        }
        t();
        return this.b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f4052a);
        byte[] bArr = this.c;
        if (bArr == null) {
            bArr = this.b.toByteArray();
        }
        SafeParcelWriter.a(parcel, 2, bArr, false);
        SafeParcelWriter.a(parcel, a2);
    }
}
