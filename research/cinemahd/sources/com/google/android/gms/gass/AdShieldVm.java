package com.google.android.gms.gass;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.ads.AdError;
import com.google.android.gms.gass.internal.Program;
import com.vungle.warren.ui.JavascriptBridge;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class AdShieldVm implements AdShieldHandle {

    /* renamed from: a  reason: collision with root package name */
    private Program f4048a;
    private final Context b;
    private final AdShield2Logger c;
    private Object d;

    public AdShieldVm(Context context, AdShield2Logger adShield2Logger) {
        this.b = context;
        this.c = adShield2Logger;
    }

    private final Object a(Class<?> cls, Program program) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            return cls.getDeclaredConstructor(new Class[]{Context.class, String.class, byte[].class, Object.class, Bundle.class, Integer.TYPE}).newInstance(new Object[]{this.b, "msa-r", program.a(), null, new Bundle(), 2});
        } catch (IllegalAccessException e) {
            a(2004, currentTimeMillis, (Exception) e);
            return null;
        } catch (InstantiationException e2) {
            a(2004, currentTimeMillis, (Exception) e2);
            return null;
        } catch (NoSuchMethodException e3) {
            a(2004, currentTimeMillis, (Exception) e3);
            return null;
        } catch (InvocationTargetException e4) {
            a(2004, currentTimeMillis, (Exception) e4);
            return null;
        }
    }

    private final synchronized int c(Object obj) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
        } catch (Exception e) {
            a(2006, currentTimeMillis, e);
            return 0;
        }
        return ((Integer) obj.getClass().getDeclaredMethod("lcs", new Class[0]).invoke(obj, new Object[0])).intValue();
    }

    public Program b() {
        return this.f4048a;
    }

    public Class<?> b(Program program) {
        File b2 = program.b();
        File d2 = program.d();
        long currentTimeMillis = System.currentTimeMillis();
        try {
            return new DexClassLoader(d2.getAbsolutePath(), b2.getAbsolutePath(), (String) null, this.b.getClassLoader()).loadClass("com.google.ccc.abuse.droidguard.DroidGuard");
        } catch (ClassNotFoundException e) {
            a(2008, currentTimeMillis, (Exception) e);
            return null;
        }
    }

    private final synchronized void b(Object obj) {
        if (this.d != null) {
            a();
        }
        this.d = obj;
    }

    public boolean a(Object obj) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            return ((Boolean) obj.getClass().getDeclaredMethod("init", new Class[0]).invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            a((int) AdError.INTERNAL_ERROR_CODE, currentTimeMillis, (Exception) e);
            return false;
        }
    }

    public AdShieldError a(Program program) {
        long currentTimeMillis = System.currentTimeMillis();
        Class<?> b2 = b(program);
        if (b2 == null) {
            return new AdShieldError(2, "lc");
        }
        Object a2 = a(b2, program);
        if (a2 == null) {
            return new AdShieldError(3, "it");
        }
        if (a(a2)) {
            int c2 = c(a2);
            if (c2 != 0) {
                this.c.a(4001, System.currentTimeMillis() - currentTimeMillis, Integer.toString(c2), (Map<String, String>) null);
                StringBuilder sb = new StringBuilder(13);
                sb.append("ci");
                sb.append(c2);
                return new AdShieldError(5, sb.toString());
            }
            b(a2);
            this.f4048a = program;
            a(3000, (Exception) null, currentTimeMillis);
            return null;
        }
        a(4000, (Exception) null, currentTimeMillis);
        return new AdShieldError(4, "ri");
    }

    public synchronized void a() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            this.d.getClass().getDeclaredMethod(JavascriptBridge.MraidHandler.CLOSE_ACTION, new Class[0]).invoke(this.d, new Object[0]);
            a((int) AdError.MEDIATION_ERROR_CODE, (Exception) null, currentTimeMillis);
        } catch (Exception e) {
            a(2003, currentTimeMillis, e);
        }
    }

    public synchronized String a(Context context, String str) {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.put("ctx", context);
        hashMap.put("aid", str);
        return a(a((Map<String, String>) null, (Map<String, Object>) hashMap));
    }

    public synchronized String a(Context context, String str, View view, Activity activity) {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.put("ctx", context);
        hashMap.put("aid", str);
        hashMap.put("view", view);
        hashMap.put("act", activity);
        return a(a((Map<String, String>) null, (Map<String, Object>) hashMap));
    }

    public synchronized String a(Context context, String str, String str2, View view, Activity activity) {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.put("ctx", context);
        hashMap.put("clickString", str2);
        hashMap.put("aid", str);
        hashMap.put("view", view);
        hashMap.put("act", activity);
        return a(a((Map<String, String>) null, (Map<String, Object>) hashMap));
    }

    public synchronized void a(String str, MotionEvent motionEvent) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("aid", str);
            hashMap.put("evt", motionEvent);
            this.d.getClass().getDeclaredMethod("he", new Class[]{Map.class}).invoke(this.d, new Object[]{hashMap});
            a(3003, (Exception) null, currentTimeMillis);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            a(2005, currentTimeMillis, (Exception) e);
        }
    }

    private static String a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return Base64.encodeToString(bArr, 11);
    }

    private final byte[] a(Map<String, String> map, Map<String, Object> map2) {
        long currentTimeMillis = System.currentTimeMillis();
        Object obj = this.d;
        if (obj == null) {
            return null;
        }
        try {
            return (byte[]) obj.getClass().getDeclaredMethod("xss", new Class[]{Map.class, Map.class}).invoke(this.d, new Object[]{null, map2});
        } catch (IllegalAccessException e) {
            a(2007, currentTimeMillis, (Exception) e);
            return null;
        } catch (NoSuchMethodException e2) {
            a(2007, currentTimeMillis, (Exception) e2);
            return null;
        } catch (InvocationTargetException e3) {
            a(2007, currentTimeMillis, (Exception) e3);
            return null;
        }
    }

    private final void a(int i, long j, Exception exc) {
        a(i, exc, j);
    }

    private final void a(int i, Exception exc, long j) {
        this.c.a(i, System.currentTimeMillis() - j, exc);
    }
}
