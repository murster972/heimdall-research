package com.google.android.gms.gass;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.internal.ads.zzddh;
import com.google.android.gms.internal.ads.zzddn;

public final class zzf {

    /* renamed from: a  reason: collision with root package name */
    private final Context f4060a;
    private final Looper b;

    public zzf(Context context, Looper looper) {
        this.f4060a = context;
        this.b = looper;
    }

    public final void a(String str) {
        new zzh(this.f4060a, this.b, (zzddn) zzddn.zzaqn().zzgt(this.f4060a.getPackageName()).zzb(zzddn.zza.BLOCKED_IMPRESSION).zza(zzddh.zzaqk().zzgs(str).zzb(zzddh.zza.BLOCKED_REASON_BACKGROUND)).zzbaf()).a();
    }
}
