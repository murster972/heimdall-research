package com.google.android.gms.gass;

import android.content.Context;
import com.google.android.gms.internal.ads.zzbm;
import com.google.android.gms.internal.ads.zzdem;
import com.google.android.gms.internal.ads.zzsr;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import java.util.Map;
import java.util.concurrent.Executor;

public class AdShield2Logger {

    /* renamed from: a  reason: collision with root package name */
    private final Context f4047a;
    private final Executor b;
    private final Task<zzsr> c;

    private AdShield2Logger(Context context, Executor executor, Task<zzsr> task) {
        this.f4047a = context;
        this.b = executor;
        this.c = task;
    }

    public static AdShield2Logger a(Context context, Executor executor) {
        return new AdShield2Logger(context, executor, Tasks.a(executor, new zzb(context)));
    }

    public Task<Boolean> a(int i, long j, Exception exc) {
        return a(i, j, exc, (String) null, (Map<String, String>) null);
    }

    public Task<Boolean> a(int i, long j, String str, Map<String, String> map) {
        return a(i, j, (Exception) null, str, map);
    }

    private final Task<Boolean> a(int i, long j, Exception exc, String str, Map<String, String> map) {
        zzbm.zza.C0033zza zzc = zzbm.zza.zzs().zzi(this.f4047a.getPackageName()).zzc(j);
        if (exc != null) {
            zzc.zzj(zzdem.zza(exc)).zzk(exc.getClass().getName());
        }
        if (str != null) {
            zzc.zzm(str);
        }
        if (map != null) {
            for (String next : map.keySet()) {
                zzc.zza(zzbm.zza.zzb.zzu().zzs(next).zzt(map.get(next)));
            }
        }
        return this.c.a(this.b, new zza(zzc, i));
    }
}
