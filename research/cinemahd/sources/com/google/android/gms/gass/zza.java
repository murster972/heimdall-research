package com.google.android.gms.gass;

import com.google.android.gms.internal.ads.zzbm;
import com.google.android.gms.internal.ads.zzsr;
import com.google.android.gms.internal.ads.zzsv;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;

final /* synthetic */ class zza implements Continuation {

    /* renamed from: a  reason: collision with root package name */
    private final zzbm.zza.C0033zza f4057a;
    private final int b;

    zza(zzbm.zza.C0033zza zza, int i) {
        this.f4057a = zza;
        this.b = i;
    }

    public final Object a(Task task) {
        zzbm.zza.C0033zza zza = this.f4057a;
        int i = this.b;
        if (!task.d()) {
            return false;
        }
        zzsv zzf = ((zzsr) task.b()).zzf(((zzbm.zza) zza.zzbaf()).toByteArray());
        zzf.zzbr(i);
        zzf.zzdn();
        return true;
    }
}
