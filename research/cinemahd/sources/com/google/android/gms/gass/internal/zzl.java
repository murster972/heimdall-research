package com.google.android.gms.gass.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class zzl implements Parcelable.Creator<zzm> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a2 = SafeParcelReader.a(parcel);
            int a3 = SafeParcelReader.a(a2);
            if (a3 == 1) {
                i = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 2) {
                i2 = SafeParcelReader.w(parcel, a2);
            } else if (a3 == 3) {
                str = SafeParcelReader.o(parcel, a2);
            } else if (a3 != 4) {
                SafeParcelReader.A(parcel, a2);
            } else {
                str2 = SafeParcelReader.o(parcel, a2);
            }
        }
        SafeParcelReader.r(parcel, b);
        return new zzm(i, i2, str, str2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzm[i];
    }
}
