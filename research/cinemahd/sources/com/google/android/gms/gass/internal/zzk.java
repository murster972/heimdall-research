package com.google.android.gms.gass.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.common.util.Hex;
import com.google.android.gms.internal.ads.zzdqk;
import com.google.android.gms.internal.ads.zzdse;
import com.google.android.gms.internal.ads.zzfz;
import java.io.File;

public final class zzk {

    /* renamed from: a  reason: collision with root package name */
    private final File f4053a;
    private final File b;
    private final SharedPreferences c;

    public zzk(Context context) {
        this.c = context.getSharedPreferences("pcvmspf", 0);
        File dir = context.getDir("pccache", 0);
        zzj.a(dir, false);
        this.f4053a = dir;
        File dir2 = context.getDir("tmppccache", 0);
        zzj.a(dir2, true);
        this.b = dir2;
    }

    private final zzfz b(int i) {
        String str;
        if (i == zzp.f4056a) {
            str = this.c.getString("LATMTD", (String) null);
        } else {
            str = i == zzp.b ? this.c.getString("FBAMTD", (String) null) : null;
        }
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            zzfz zzl = zzfz.zzl(zzdqk.zzu(Hex.a(str)));
            String zzcx = zzl.zzcx();
            if (zzj.a(zzcx, "pcam", this.f4053a).exists() && zzj.a(zzcx, "pcbc", this.f4053a).exists()) {
                return zzl;
            }
            return null;
        } catch (zzdse unused) {
        }
    }

    public final Program a(int i) {
        zzfz b2 = b(i);
        if (b2 == null) {
            return null;
        }
        String zzcx = b2.zzcx();
        return new Program(b2, zzj.a(zzcx, "pcam", this.f4053a), zzj.a(zzcx, "pcbc", this.f4053a), zzj.a(zzcx, "pcopt", this.f4053a));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0119, code lost:
        if (r0.commit() != false) goto L_0x011d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0149  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.google.android.gms.internal.ads.zzfy r8, com.google.android.gms.gass.internal.zzq r9) {
        /*
            r7 = this;
            com.google.android.gms.internal.ads.zzfz r9 = r8.zzct()
            java.lang.String r9 = r9.zzcx()
            com.google.android.gms.internal.ads.zzdqk r0 = r8.zzcu()
            byte[] r0 = r0.toByteArray()
            com.google.android.gms.internal.ads.zzdqk r1 = r8.zzcv()
            byte[] r1 = r1.toByteArray()
            boolean r2 = android.text.TextUtils.isEmpty(r9)
            java.lang.String r3 = "pcbc"
            java.lang.String r4 = "pcam"
            r5 = 0
            if (r2 != 0) goto L_0x0059
            if (r1 == 0) goto L_0x0059
            int r2 = r1.length
            if (r2 != 0) goto L_0x0029
            goto L_0x0059
        L_0x0029:
            java.io.File r2 = r7.b
            com.google.android.gms.gass.internal.zzj.a(r2)
            java.io.File r2 = r7.b
            r2.mkdirs()
            java.io.File r2 = r7.b
            java.io.File r2 = com.google.android.gms.gass.internal.zzj.a((java.lang.String) r9, (java.io.File) r2)
            r2.mkdirs()
            java.io.File r2 = r7.b
            java.io.File r2 = com.google.android.gms.gass.internal.zzj.a(r9, r4, r2)
            if (r0 == 0) goto L_0x004e
            int r6 = r0.length
            if (r6 <= 0) goto L_0x004e
            boolean r0 = com.google.android.gms.gass.internal.zzj.a((java.io.File) r2, (byte[]) r0)
            if (r0 != 0) goto L_0x004e
            goto L_0x0059
        L_0x004e:
            java.io.File r0 = r7.b
            java.io.File r9 = com.google.android.gms.gass.internal.zzj.a(r9, r3, r0)
            boolean r9 = com.google.android.gms.gass.internal.zzj.a((java.io.File) r9, (byte[]) r1)
            goto L_0x005a
        L_0x0059:
            r9 = 0
        L_0x005a:
            if (r9 != 0) goto L_0x005d
            return r5
        L_0x005d:
            com.google.android.gms.internal.ads.zzfz r9 = r8.zzct()
            java.lang.String r9 = r9.zzcx()
            boolean r0 = android.text.TextUtils.isEmpty(r9)
            r1 = 1
            if (r0 != 0) goto L_0x009e
            java.io.File r0 = r7.b
            java.io.File r0 = com.google.android.gms.gass.internal.zzj.a(r9, r4, r0)
            java.io.File r2 = r7.b
            java.io.File r2 = com.google.android.gms.gass.internal.zzj.a(r9, r3, r2)
            java.io.File r6 = r7.f4053a
            java.io.File r4 = com.google.android.gms.gass.internal.zzj.a(r9, r4, r6)
            java.io.File r6 = r7.f4053a
            java.io.File r9 = com.google.android.gms.gass.internal.zzj.a(r9, r3, r6)
            boolean r3 = r0.exists()
            if (r3 == 0) goto L_0x0090
            boolean r0 = r0.renameTo(r4)
            if (r0 == 0) goto L_0x009e
        L_0x0090:
            boolean r0 = r2.exists()
            if (r0 == 0) goto L_0x009e
            boolean r9 = r2.renameTo(r9)
            if (r9 == 0) goto L_0x009e
            r9 = 1
            goto L_0x009f
        L_0x009e:
            r9 = 0
        L_0x009f:
            if (r9 == 0) goto L_0x011c
            com.google.android.gms.internal.ads.zzfz$zza r9 = com.google.android.gms.internal.ads.zzfz.zzdc()
            com.google.android.gms.internal.ads.zzfz r0 = r8.zzct()
            java.lang.String r0 = r0.zzcx()
            com.google.android.gms.internal.ads.zzfz$zza r9 = r9.zzay(r0)
            com.google.android.gms.internal.ads.zzfz r0 = r8.zzct()
            java.lang.String r0 = r0.zzcy()
            com.google.android.gms.internal.ads.zzfz$zza r9 = r9.zzaz(r0)
            com.google.android.gms.internal.ads.zzfz r0 = r8.zzct()
            long r2 = r0.zzda()
            com.google.android.gms.internal.ads.zzfz$zza r9 = r9.zzdk(r2)
            com.google.android.gms.internal.ads.zzfz r0 = r8.zzct()
            long r2 = r0.zzdb()
            com.google.android.gms.internal.ads.zzfz$zza r9 = r9.zzdl(r2)
            com.google.android.gms.internal.ads.zzfz r8 = r8.zzct()
            long r2 = r8.zzcz()
            com.google.android.gms.internal.ads.zzfz$zza r8 = r9.zzdj(r2)
            com.google.android.gms.internal.ads.zzdte r8 = r8.zzbaf()
            com.google.android.gms.internal.ads.zzfz r8 = (com.google.android.gms.internal.ads.zzfz) r8
            int r9 = com.google.android.gms.gass.internal.zzp.f4056a
            com.google.android.gms.internal.ads.zzfz r9 = r7.b(r9)
            android.content.SharedPreferences r0 = r7.c
            android.content.SharedPreferences$Editor r0 = r0.edit()
            if (r9 == 0) goto L_0x010c
            java.lang.String r2 = r8.zzcx()
            java.lang.String r3 = r9.zzcx()
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x010c
            java.lang.String r9 = a((com.google.android.gms.internal.ads.zzfz) r9)
            java.lang.String r2 = "FBAMTD"
            r0.putString(r2, r9)
        L_0x010c:
            java.lang.String r8 = a((com.google.android.gms.internal.ads.zzfz) r8)
            java.lang.String r9 = "LATMTD"
            r0.putString(r9, r8)
            boolean r8 = r0.commit()
            if (r8 == 0) goto L_0x011c
            goto L_0x011d
        L_0x011c:
            r1 = 0
        L_0x011d:
            java.util.HashSet r8 = new java.util.HashSet
            r8.<init>()
            int r9 = com.google.android.gms.gass.internal.zzp.f4056a
            com.google.android.gms.internal.ads.zzfz r9 = r7.b(r9)
            if (r9 == 0) goto L_0x0131
            java.lang.String r9 = r9.zzcx()
            r8.add(r9)
        L_0x0131:
            int r9 = com.google.android.gms.gass.internal.zzp.b
            com.google.android.gms.internal.ads.zzfz r9 = r7.b(r9)
            if (r9 == 0) goto L_0x0140
            java.lang.String r9 = r9.zzcx()
            r8.add(r9)
        L_0x0140:
            java.io.File r9 = r7.f4053a
            java.io.File[] r9 = r9.listFiles()
            int r0 = r9.length
        L_0x0147:
            if (r5 >= r0) goto L_0x0161
            r2 = r9[r5]
            java.lang.String r2 = r2.getName()
            boolean r3 = r8.contains(r2)
            if (r3 != 0) goto L_0x015e
            java.io.File r3 = r7.f4053a
            java.io.File r2 = com.google.android.gms.gass.internal.zzj.a((java.lang.String) r2, (java.io.File) r3)
            com.google.android.gms.gass.internal.zzj.a(r2)
        L_0x015e:
            int r5 = r5 + 1
            goto L_0x0147
        L_0x0161:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.gass.internal.zzk.a(com.google.android.gms.internal.ads.zzfy, com.google.android.gms.gass.internal.zzq):boolean");
    }

    private static String a(zzfz zzfz) {
        return Hex.a(zzfz.zzaxk().toByteArray());
    }
}
