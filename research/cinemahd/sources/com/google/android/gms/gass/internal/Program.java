package com.google.android.gms.gass.internal;

import com.google.android.gms.internal.ads.zzfz;
import java.io.File;
import java.util.Arrays;

public class Program {

    /* renamed from: a  reason: collision with root package name */
    private final zzfz f4049a;
    private final File b;
    private final File c;
    private final File d;
    private byte[] e;

    public Program(zzfz zzfz, File file, File file2, File file3) {
        this.f4049a = zzfz;
        this.b = file;
        this.c = file3;
        this.d = file2;
    }

    public byte[] a() {
        if (this.e == null) {
            this.e = zzj.b(this.d);
        }
        byte[] bArr = this.e;
        if (bArr == null) {
            return null;
        }
        return Arrays.copyOf(bArr, bArr.length);
    }

    public File b() {
        return this.c;
    }

    public zzfz c() {
        return this.f4049a;
    }

    public File d() {
        return this.b;
    }

    public boolean e() {
        return a(3600);
    }

    public boolean f() {
        return System.currentTimeMillis() / 1000 > this.f4049a.zzcz();
    }

    public boolean a(long j) {
        return this.f4049a.zzcz() - (System.currentTimeMillis() / 1000) < j;
    }
}
