package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable;
import com.google.android.gms.auth.api.signin.internal.HashAccumulator;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GoogleSignInOptions extends AbstractSafeParcelable implements Api.ApiOptions.Optional, ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR = new zad();
    public static final Scope j = new Scope("profile");
    public static final Scope k = new Scope("openid");
    public static final Scope l = new Scope("https://www.googleapis.com/auth/games_lite");
    public static final Scope m = new Scope("https://www.googleapis.com/auth/games");

    /* renamed from: a  reason: collision with root package name */
    private final int f3753a;
    private final ArrayList<Scope> b;
    private Account c;
    private boolean d;
    private final boolean e;
    private final boolean f;
    private String g;
    private String h;
    private ArrayList<GoogleSignInOptionsExtensionParcelable> i;

    static {
        new Scope("email");
        new Builder().b().c().a();
        new Builder().a(l, new Scope[0]).a();
        new zac();
    }

    GoogleSignInOptions(int i2, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, ArrayList<GoogleSignInOptionsExtensionParcelable> arrayList2) {
        this(i2, arrayList, account, z, z2, z3, str, str2, a(arrayList2));
    }

    private static Map<Integer, GoogleSignInOptionsExtensionParcelable> a(List<GoogleSignInOptionsExtensionParcelable> list) {
        HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (GoogleSignInOptionsExtensionParcelable next : list) {
            hashMap.put(Integer.valueOf(next.s()), next);
        }
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004a, code lost:
        if (r3.c.equals(r4.s()) != false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0069, code lost:
        if (r3.g.equals(r4.v()) != false) goto L_0x006b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.google.android.gms.auth.api.signin.GoogleSignInOptions r4 = (com.google.android.gms.auth.api.signin.GoogleSignInOptions) r4     // Catch:{ ClassCastException -> 0x0085 }
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r3.i     // Catch:{ ClassCastException -> 0x0085 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 > 0) goto L_0x0085
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r4.i     // Catch:{ ClassCastException -> 0x0085 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 <= 0) goto L_0x0018
            goto L_0x0085
        L_0x0018:
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.b     // Catch:{ ClassCastException -> 0x0085 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0085 }
            java.util.ArrayList r2 = r4.u()     // Catch:{ ClassCastException -> 0x0085 }
            int r2 = r2.size()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r2) goto L_0x0085
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.b     // Catch:{ ClassCastException -> 0x0085 }
            java.util.ArrayList r2 = r4.u()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = r1.containsAll(r2)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != 0) goto L_0x0035
            goto L_0x0085
        L_0x0035:
            android.accounts.Account r1 = r3.c     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != 0) goto L_0x0040
            android.accounts.Account r1 = r4.s()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != 0) goto L_0x0085
            goto L_0x004c
        L_0x0040:
            android.accounts.Account r1 = r3.c     // Catch:{ ClassCastException -> 0x0085 }
            android.accounts.Account r2 = r4.s()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x0085
        L_0x004c:
            java.lang.String r1 = r3.g     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x005f
            java.lang.String r1 = r4.v()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x0085
            goto L_0x006b
        L_0x005f:
            java.lang.String r1 = r3.g     // Catch:{ ClassCastException -> 0x0085 }
            java.lang.String r2 = r4.v()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x0085
        L_0x006b:
            boolean r1 = r3.f     // Catch:{ ClassCastException -> 0x0085 }
            boolean r2 = r4.w()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r2) goto L_0x0085
            boolean r1 = r3.d     // Catch:{ ClassCastException -> 0x0085 }
            boolean r2 = r4.x()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r2) goto L_0x0085
            boolean r1 = r3.e     // Catch:{ ClassCastException -> 0x0085 }
            boolean r4 = r4.y()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r4) goto L_0x0085
            r4 = 1
            return r4
        L_0x0085:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.auth.api.signin.GoogleSignInOptions.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        ArrayList arrayList = new ArrayList();
        ArrayList<Scope> arrayList2 = this.b;
        int size = arrayList2.size();
        int i2 = 0;
        while (i2 < size) {
            Scope scope = arrayList2.get(i2);
            i2++;
            arrayList.add(scope.s());
        }
        Collections.sort(arrayList);
        return new HashAccumulator().a((Object) arrayList).a((Object) this.c).a((Object) this.g).a(this.f).a(this.d).a(this.e).a();
    }

    public Account s() {
        return this.c;
    }

    public ArrayList<GoogleSignInOptionsExtensionParcelable> t() {
        return this.i;
    }

    public ArrayList<Scope> u() {
        return new ArrayList<>(this.b);
    }

    public String v() {
        return this.g;
    }

    public boolean w() {
        return this.f;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f3753a);
        SafeParcelWriter.c(parcel, 2, u(), false);
        SafeParcelWriter.a(parcel, 3, (Parcelable) s(), i2, false);
        SafeParcelWriter.a(parcel, 4, x());
        SafeParcelWriter.a(parcel, 5, y());
        SafeParcelWriter.a(parcel, 6, w());
        SafeParcelWriter.a(parcel, 7, v(), false);
        SafeParcelWriter.a(parcel, 8, this.h, false);
        SafeParcelWriter.c(parcel, 9, t(), false);
        SafeParcelWriter.a(parcel, a2);
    }

    public boolean x() {
        return this.d;
    }

    public boolean y() {
        return this.e;
    }

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        private Set<Scope> f3754a = new HashSet();
        private boolean b;
        private boolean c;
        private boolean d;
        private String e;
        private Account f;
        private String g;
        private Map<Integer, GoogleSignInOptionsExtensionParcelable> h = new HashMap();

        public final Builder a(Scope scope, Scope... scopeArr) {
            this.f3754a.add(scope);
            this.f3754a.addAll(Arrays.asList(scopeArr));
            return this;
        }

        public final Builder b() {
            this.f3754a.add(GoogleSignInOptions.k);
            return this;
        }

        public final Builder c() {
            this.f3754a.add(GoogleSignInOptions.j);
            return this;
        }

        public final GoogleSignInOptions a() {
            if (this.f3754a.contains(GoogleSignInOptions.m) && this.f3754a.contains(GoogleSignInOptions.l)) {
                this.f3754a.remove(GoogleSignInOptions.l);
            }
            if (this.d && (this.f == null || !this.f3754a.isEmpty())) {
                b();
            }
            return new GoogleSignInOptions(3, new ArrayList(this.f3754a), this.f, this.d, this.b, this.c, this.e, this.g, this.h, (zac) null);
        }
    }

    private GoogleSignInOptions(int i2, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map<Integer, GoogleSignInOptionsExtensionParcelable> map) {
        this.f3753a = i2;
        this.b = arrayList;
        this.c = account;
        this.d = z;
        this.e = z2;
        this.f = z3;
        this.g = str;
        this.h = str2;
        this.i = new ArrayList<>(map.values());
    }

    /* synthetic */ GoogleSignInOptions(int i2, ArrayList arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map map, zac zac) {
        this(3, (ArrayList<Scope>) arrayList, account, z, z2, z3, str, str2, (Map<Integer, GoogleSignInOptionsExtensionParcelable>) map);
    }
}
