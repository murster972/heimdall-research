package com.google.android.gms.auth.api.signin.internal;

public class HashAccumulator {
    private static int b = 31;

    /* renamed from: a  reason: collision with root package name */
    private int f3756a = 1;

    public HashAccumulator a(Object obj) {
        this.f3756a = (b * this.f3756a) + (obj == null ? 0 : obj.hashCode());
        return this;
    }

    public final HashAccumulator a(boolean z) {
        this.f3756a = (b * this.f3756a) + (z ? 1 : 0);
        return this;
    }

    public int a() {
        return this.f3756a;
    }
}
