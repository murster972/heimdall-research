package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInAccount extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInAccount> CREATOR = new zab();
    private static Clock n = DefaultClock.c();

    /* renamed from: a  reason: collision with root package name */
    private final int f3752a;
    private String b;
    private String c;
    private String d;
    private String e;
    private Uri f;
    private String g;
    private long h;
    private String i;
    private List<Scope> j;
    private String k;
    private String l;
    private Set<Scope> m = new HashSet();

    GoogleSignInAccount(int i2, String str, String str2, String str3, String str4, Uri uri, String str5, long j2, String str6, List<Scope> list, String str7, String str8) {
        this.f3752a = i2;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = uri;
        this.g = str5;
        this.h = j2;
        this.i = str6;
        this.j = list;
        this.k = str7;
        this.l = str8;
    }

    private static GoogleSignInAccount a(String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Long l2, String str7, Set<Scope> set) {
        long longValue = (l2 == null ? Long.valueOf(n.b() / 1000) : l2).longValue();
        Preconditions.b(str7);
        Preconditions.a(set);
        return new GoogleSignInAccount(3, str, str2, str3, str4, uri, (String) null, longValue, str7, new ArrayList(set), str5, str6);
    }

    public static GoogleSignInAccount e(String str) throws JSONException {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        String optString = jSONObject.optString("photoUrl", (String) null);
        Uri parse = !TextUtils.isEmpty(optString) ? Uri.parse(optString) : null;
        long parseLong = Long.parseLong(jSONObject.getString("expirationTime"));
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("grantedScopes");
        int length = jSONArray.length();
        for (int i2 = 0; i2 < length; i2++) {
            hashSet.add(new Scope(jSONArray.getString(i2)));
        }
        GoogleSignInAccount a2 = a(jSONObject.optString("id"), jSONObject.optString("tokenId", (String) null), jSONObject.optString("email", (String) null), jSONObject.optString("displayName", (String) null), jSONObject.optString("givenName", (String) null), jSONObject.optString("familyName", (String) null), parse, Long.valueOf(parseLong), jSONObject.getString("obfuscatedIdentifier"), hashSet);
        a2.g = jSONObject.optString("serverAuthCode", (String) null);
        return a2;
    }

    public Set<Scope> A() {
        HashSet hashSet = new HashSet(this.j);
        hashSet.addAll(this.m);
        return hashSet;
    }

    public String B() {
        return this.g;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof GoogleSignInAccount)) {
            return false;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) obj;
        return googleSignInAccount.i.equals(this.i) && googleSignInAccount.A().equals(A());
    }

    public int hashCode() {
        return ((this.i.hashCode() + 527) * 31) + A().hashCode();
    }

    public Account s() {
        String str = this.d;
        if (str == null) {
            return null;
        }
        return new Account(str, "com.google");
    }

    public String t() {
        return this.e;
    }

    public String u() {
        return this.d;
    }

    public String v() {
        return this.l;
    }

    public String w() {
        return this.k;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = SafeParcelWriter.a(parcel);
        SafeParcelWriter.a(parcel, 1, this.f3752a);
        SafeParcelWriter.a(parcel, 2, x(), false);
        SafeParcelWriter.a(parcel, 3, y(), false);
        SafeParcelWriter.a(parcel, 4, u(), false);
        SafeParcelWriter.a(parcel, 5, t(), false);
        SafeParcelWriter.a(parcel, 6, (Parcelable) z(), i2, false);
        SafeParcelWriter.a(parcel, 7, B(), false);
        SafeParcelWriter.a(parcel, 8, this.h);
        SafeParcelWriter.a(parcel, 9, this.i, false);
        SafeParcelWriter.c(parcel, 10, this.j, false);
        SafeParcelWriter.a(parcel, 11, w(), false);
        SafeParcelWriter.a(parcel, 12, v(), false);
        SafeParcelWriter.a(parcel, a2);
    }

    public String x() {
        return this.b;
    }

    public String y() {
        return this.c;
    }

    public Uri z() {
        return this.f;
    }
}
