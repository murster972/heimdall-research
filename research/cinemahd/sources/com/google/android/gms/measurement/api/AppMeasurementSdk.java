package com.google.android.gms.measurement.api;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.internal.measurement.zzx;
import java.util.List;
import java.util.Map;

public class AppMeasurementSdk {

    /* renamed from: a  reason: collision with root package name */
    private final zzx f4148a;

    public AppMeasurementSdk(zzx zzx) {
        this.f4148a = zzx;
    }

    public static AppMeasurementSdk a(Context context) {
        return zzx.zza(context).zza();
    }

    public void b(String str, String str2, Bundle bundle) {
        this.f4148a.zza(str, str2, bundle);
    }

    public void c(Bundle bundle) {
        this.f4148a.zza(bundle);
    }

    public String d() {
        return this.f4148a.zzg();
    }

    public String e() {
        return this.f4148a.zzf();
    }

    public String f() {
        return this.f4148a.zzc();
    }

    public static AppMeasurementSdk a(Context context, String str, String str2, String str3, Bundle bundle) {
        return zzx.zza(context, str, str2, str3, bundle).zza();
    }

    public void b(String str) {
        this.f4148a.zzc(str);
    }

    public String c() {
        return this.f4148a.zzd();
    }

    public Bundle b(Bundle bundle) {
        return this.f4148a.zza(bundle, true);
    }

    public int c(String str) {
        return this.f4148a.zzd(str);
    }

    public void a(String str, String str2, Object obj) {
        this.f4148a.zza(str, str2, obj);
    }

    public String b() {
        return this.f4148a.zzi();
    }

    public Map<String, Object> a(String str, String str2, boolean z) {
        return this.f4148a.zza(str, str2, z);
    }

    public void a(String str, String str2, Bundle bundle) {
        this.f4148a.zzb(str, str2, bundle);
    }

    public List<Bundle> a(String str, String str2) {
        return this.f4148a.zzb(str, str2);
    }

    public long a() {
        return this.f4148a.zze();
    }

    public void a(String str) {
        this.f4148a.zzb(str);
    }

    public void a(Bundle bundle) {
        this.f4148a.zza(bundle, false);
    }

    public void a(Activity activity, String str, String str2) {
        this.f4148a.zza(activity, str, str2);
    }
}
