package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zze implements Callable<Integer> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SharedPreferences f4041a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Integer c;

    zze(SharedPreferences sharedPreferences, String str, Integer num) {
        this.f4041a = sharedPreferences;
        this.b = str;
        this.c = num;
    }

    public final /* synthetic */ Object call() throws Exception {
        return Integer.valueOf(this.f4041a.getInt(this.b, this.c.intValue()));
    }
}
