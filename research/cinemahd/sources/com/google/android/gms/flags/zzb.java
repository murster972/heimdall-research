package com.google.android.gms.flags;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.flags.ModuleDescriptor;

public final class zzb {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4046a = false;
    private zzc b = null;

    public final void a(Context context) {
        synchronized (this) {
            if (!this.f4046a) {
                try {
                    this.b = zzd.asInterface(DynamiteModule.a(context, DynamiteModule.k, ModuleDescriptor.MODULE_ID).a("com.google.android.gms.flags.impl.FlagProviderImpl"));
                    this.b.init(ObjectWrapper.a(context));
                    this.f4046a = true;
                } catch (RemoteException | DynamiteModule.LoadingException e) {
                    Log.w("FlagValueProvider", "Failed to initialize flags module.", e);
                }
            }
        }
    }
}
