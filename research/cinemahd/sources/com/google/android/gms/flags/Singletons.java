package com.google.android.gms.flags;

public final class Singletons {
    private static Singletons c;

    /* renamed from: a  reason: collision with root package name */
    private final FlagRegistry f4038a = new FlagRegistry();
    private final zzb b = new zzb();

    static {
        Singletons singletons = new Singletons();
        synchronized (Singletons.class) {
            c = singletons;
        }
    }

    private Singletons() {
    }

    public static FlagRegistry a() {
        return b().f4038a;
    }

    private static Singletons b() {
        Singletons singletons;
        synchronized (Singletons.class) {
            singletons = c;
        }
        return singletons;
    }

    public static zzb c() {
        return b().b;
    }
}
