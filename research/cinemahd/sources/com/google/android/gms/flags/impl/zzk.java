package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzk implements Callable<SharedPreferences> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Context f4045a;

    zzk(Context context) {
        this.f4045a = context;
    }

    public final /* synthetic */ Object call() throws Exception {
        return this.f4045a.getSharedPreferences("google_sdk_flags", 0);
    }
}
