package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.internal.flags.zze;

public final class zzj {

    /* renamed from: a  reason: collision with root package name */
    private static SharedPreferences f4044a;

    public static SharedPreferences a(Context context) throws Exception {
        SharedPreferences sharedPreferences;
        synchronized (SharedPreferences.class) {
            if (f4044a == null) {
                f4044a = (SharedPreferences) zze.zza(new zzk(context));
            }
            sharedPreferences = f4044a;
        }
        return sharedPreferences;
    }
}
