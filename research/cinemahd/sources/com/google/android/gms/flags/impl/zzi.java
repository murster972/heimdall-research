package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzi implements Callable<String> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SharedPreferences f4043a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    zzi(SharedPreferences sharedPreferences, String str, String str2) {
        this.f4043a = sharedPreferences;
        this.b = str;
        this.c = str2;
    }

    public final /* synthetic */ Object call() throws Exception {
        return this.f4043a.getString(this.b, this.c);
    }
}
