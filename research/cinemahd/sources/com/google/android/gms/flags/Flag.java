package com.google.android.gms.flags;

@Deprecated
public abstract class Flag<T> {

    @Deprecated
    public static class BooleanFlag extends Flag<Boolean> {
        public BooleanFlag(int i, String str, Boolean bool) {
            super(i, str, bool);
        }
    }

    private Flag(int i, String str, T t) {
        Singletons.a().a(this);
    }

    @Deprecated
    public static BooleanFlag a(int i, String str, Boolean bool) {
        return new BooleanFlag(i, str, bool);
    }
}
