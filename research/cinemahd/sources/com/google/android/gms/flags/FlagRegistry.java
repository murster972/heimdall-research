package com.google.android.gms.flags;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collection;

public class FlagRegistry {

    /* renamed from: a  reason: collision with root package name */
    private final Collection<Flag> f4037a = new ArrayList();

    public FlagRegistry() {
        new ArrayList();
        new ArrayList();
    }

    public final void a(Flag flag) {
        this.f4037a.add(flag);
    }

    public static void a(Context context) {
        Singletons.c().a(context);
    }
}
