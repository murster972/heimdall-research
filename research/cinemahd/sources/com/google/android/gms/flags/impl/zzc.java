package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzc implements Callable<Boolean> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SharedPreferences f4040a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Boolean c;

    zzc(SharedPreferences sharedPreferences, String str, Boolean bool) {
        this.f4040a = sharedPreferences;
        this.b = str;
        this.c = bool;
    }

    public final /* synthetic */ Object call() throws Exception {
        return Boolean.valueOf(this.f4040a.getBoolean(this.b, this.c.booleanValue()));
    }
}
