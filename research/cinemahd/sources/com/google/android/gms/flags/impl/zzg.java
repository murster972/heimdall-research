package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

final class zzg implements Callable<Long> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SharedPreferences f4042a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Long c;

    zzg(SharedPreferences sharedPreferences, String str, Long l) {
        this.f4042a = sharedPreferences;
        this.b = str;
        this.c = l;
    }

    public final /* synthetic */ Object call() throws Exception {
        return Long.valueOf(this.f4042a.getLong(this.b, this.c.longValue()));
    }
}
