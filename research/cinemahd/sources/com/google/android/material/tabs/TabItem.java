package com.google.android.material.tabs;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.TintTypedArray;
import com.google.android.material.R$styleable;

public class TabItem extends View {

    /* renamed from: a  reason: collision with root package name */
    public final CharSequence f4291a;
    public final Drawable b;
    public final int c;

    public TabItem(Context context) {
        this(context, (AttributeSet) null);
    }

    public TabItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TintTypedArray a2 = TintTypedArray.a(context, attributeSet, R$styleable.TabItem);
        this.f4291a = a2.e(R$styleable.TabItem_android_text);
        this.b = a2.b(R$styleable.TabItem_android_icon);
        this.c = a2.g(R$styleable.TabItem_android_layout, 0);
        a2.a();
    }
}
