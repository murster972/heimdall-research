package com.google.android.material.tabs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.TooltipCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.util.Pools$Pool;
import androidx.core.util.Pools$SimplePool;
import androidx.core.util.Pools$SynchronizedPool;
import androidx.core.view.MarginLayoutParamsCompat;
import androidx.core.view.PointerIconCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.TextViewCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.facebook.ads.AdError;
import com.google.android.material.R$attr;
import com.google.android.material.R$dimen;
import com.google.android.material.R$layout;
import com.google.android.material.R$style;
import com.google.android.material.R$styleable;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.internal.ViewUtils;
import com.google.android.material.resources.MaterialResources;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

@ViewPager.DecorView
public class TabLayout extends HorizontalScrollView {
    private static final Pools$Pool<Tab> O = new Pools$SynchronizedPool(16);
    boolean A;
    boolean B;
    boolean C;
    private BaseOnTabSelectedListener D;
    private final ArrayList<BaseOnTabSelectedListener> E;
    private BaseOnTabSelectedListener F;
    private ValueAnimator G;
    ViewPager H;
    private PagerAdapter I;
    private DataSetObserver J;
    private TabLayoutOnPageChangeListener K;
    private AdapterChangeListener L;
    private boolean M;
    private final Pools$Pool<TabView> N;

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<Tab> f4292a;
    private Tab b;
    /* access modifiers changed from: private */
    public final RectF c;
    private final SlidingTabIndicator d;
    int e;
    int f;
    int g;
    int h;
    int i;
    ColorStateList j;
    ColorStateList k;
    ColorStateList l;
    Drawable m;
    PorterDuff.Mode n;
    float o;
    float p;
    final int q;
    int r;
    private final int s;
    private final int t;
    private final int u;
    private int v;
    int w;
    int x;
    int y;
    int z;

    public interface BaseOnTabSelectedListener<T extends Tab> {
        void a(T t);

        void b(T t);

        void c(T t);
    }

    public interface OnTabSelectedListener extends BaseOnTabSelectedListener<Tab> {
    }

    private class PagerAdapterObserver extends DataSetObserver {
        PagerAdapterObserver() {
        }

        public void onChanged() {
            TabLayout.this.c();
        }

        public void onInvalidated() {
            TabLayout.this.c();
        }
    }

    public static class Tab {

        /* renamed from: a  reason: collision with root package name */
        private Drawable f4299a;
        /* access modifiers changed from: private */
        public CharSequence b;
        /* access modifiers changed from: private */
        public CharSequence c;
        private int d = -1;
        private View e;
        public TabLayout f;
        public TabView g;

        public int c() {
            return this.d;
        }

        public CharSequence d() {
            return this.b;
        }

        public boolean e() {
            TabLayout tabLayout = this.f;
            if (tabLayout != null) {
                return tabLayout.getSelectedTabPosition() == this.d;
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.f = null;
            this.g = null;
            this.f4299a = null;
            this.b = null;
            this.c = null;
            this.d = -1;
            this.e = null;
        }

        public void g() {
            TabLayout tabLayout = this.f;
            if (tabLayout != null) {
                tabLayout.c(this);
                return;
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        /* access modifiers changed from: package-private */
        public void h() {
            TabView tabView = this.g;
            if (tabView != null) {
                tabView.b();
            }
        }

        public View a() {
            return this.e;
        }

        public Drawable b() {
            return this.f4299a;
        }

        public Tab a(View view) {
            this.e = view;
            h();
            return this;
        }

        /* access modifiers changed from: package-private */
        public void b(int i) {
            this.d = i;
        }

        public Tab b(CharSequence charSequence) {
            if (TextUtils.isEmpty(this.c) && !TextUtils.isEmpty(charSequence)) {
                this.g.setContentDescription(charSequence);
            }
            this.b = charSequence;
            h();
            return this;
        }

        public Tab a(int i) {
            return a(LayoutInflater.from(this.g.getContext()).inflate(i, this.g, false));
        }

        public Tab a(Drawable drawable) {
            this.f4299a = drawable;
            h();
            return this;
        }

        public Tab a(CharSequence charSequence) {
            this.c = charSequence;
            h();
            return this;
        }
    }

    public static class TabLayoutOnPageChangeListener implements ViewPager.OnPageChangeListener {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<TabLayout> f4300a;
        private int b;
        private int c;

        public TabLayoutOnPageChangeListener(TabLayout tabLayout) {
            this.f4300a = new WeakReference<>(tabLayout);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.c = 0;
            this.b = 0;
        }

        public void onPageScrollStateChanged(int i) {
            this.b = this.c;
            this.c = i;
        }

        public void onPageScrolled(int i, float f, int i2) {
            TabLayout tabLayout = (TabLayout) this.f4300a.get();
            if (tabLayout != null) {
                boolean z = false;
                boolean z2 = this.c != 2 || this.b == 1;
                if (!(this.c == 2 && this.b == 0)) {
                    z = true;
                }
                tabLayout.a(i, f, z2, z);
            }
        }

        public void onPageSelected(int i) {
            TabLayout tabLayout = (TabLayout) this.f4300a.get();
            if (tabLayout != null && tabLayout.getSelectedTabPosition() != i && i < tabLayout.getTabCount()) {
                int i2 = this.c;
                tabLayout.b(tabLayout.b(i), i2 == 0 || (i2 == 2 && this.b == 0));
            }
        }
    }

    public static class ViewPagerOnTabSelectedListener implements OnTabSelectedListener {

        /* renamed from: a  reason: collision with root package name */
        private final ViewPager f4302a;

        public ViewPagerOnTabSelectedListener(ViewPager viewPager) {
            this.f4302a = viewPager;
        }

        public void a(Tab tab) {
            this.f4302a.setCurrentItem(tab.c());
        }

        public void b(Tab tab) {
        }

        public void c(Tab tab) {
        }
    }

    public TabLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    private TabView e(Tab tab) {
        Pools$Pool<TabView> pools$Pool = this.N;
        TabView acquire = pools$Pool != null ? pools$Pool.acquire() : null;
        if (acquire == null) {
            acquire = new TabView(getContext());
        }
        acquire.a(tab);
        acquire.setFocusable(true);
        acquire.setMinimumWidth(getTabMinWidth());
        if (TextUtils.isEmpty(tab.c)) {
            acquire.setContentDescription(tab.b);
        } else {
            acquire.setContentDescription(tab.c);
        }
        return acquire;
    }

    private LinearLayout.LayoutParams f() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
        a(layoutParams);
        return layoutParams;
    }

    private void g() {
        if (this.G == null) {
            this.G = new ValueAnimator();
            this.G.setInterpolator(AnimationUtils.b);
            this.G.setDuration((long) this.x);
            this.G.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    TabLayout.this.scrollTo(((Integer) valueAnimator.getAnimatedValue()).intValue(), 0);
                }
            });
        }
    }

    private int getDefaultHeight() {
        int size = this.f4292a.size();
        boolean z2 = false;
        int i2 = 0;
        while (true) {
            if (i2 < size) {
                Tab tab = this.f4292a.get(i2);
                if (tab != null && tab.b() != null && !TextUtils.isEmpty(tab.d())) {
                    z2 = true;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        return (!z2 || this.A) ? 48 : 72;
    }

    private int getTabMinWidth() {
        int i2 = this.s;
        if (i2 != -1) {
            return i2;
        }
        if (this.z == 0) {
            return this.u;
        }
        return 0;
    }

    private int getTabScrollRange() {
        return Math.max(0, ((this.d.getWidth() - getWidth()) - getPaddingLeft()) - getPaddingRight());
    }

    private void h() {
        int size = this.f4292a.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f4292a.get(i2).h();
        }
    }

    private void setSelectedTabView(int i2) {
        int childCount = this.d.getChildCount();
        if (i2 < childCount) {
            int i3 = 0;
            while (i3 < childCount) {
                View childAt = this.d.getChildAt(i3);
                boolean z2 = true;
                childAt.setSelected(i3 == i2);
                if (i3 != i2) {
                    z2 = false;
                }
                childAt.setActivated(z2);
                i3++;
            }
        }
    }

    public void addView(View view) {
        a(view);
    }

    public void b(BaseOnTabSelectedListener baseOnTabSelectedListener) {
        this.E.remove(baseOnTabSelectedListener);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int currentItem;
        d();
        PagerAdapter pagerAdapter = this.I;
        if (pagerAdapter != null) {
            int count = pagerAdapter.getCount();
            for (int i2 = 0; i2 < count; i2++) {
                a(b().b(this.I.getPageTitle(i2)), false);
            }
            ViewPager viewPager = this.H;
            if (viewPager != null && count > 0 && (currentItem = viewPager.getCurrentItem()) != getSelectedTabPosition() && currentItem < getTabCount()) {
                c(b(currentItem));
            }
        }
    }

    public void d() {
        for (int childCount = this.d.getChildCount() - 1; childCount >= 0; childCount--) {
            d(childCount);
        }
        Iterator<Tab> it2 = this.f4292a.iterator();
        while (it2.hasNext()) {
            Tab next = it2.next();
            it2.remove();
            next.f();
            b(next);
        }
        this.b = null;
    }

    public int getSelectedTabPosition() {
        Tab tab = this.b;
        if (tab != null) {
            return tab.c();
        }
        return -1;
    }

    public int getTabCount() {
        return this.f4292a.size();
    }

    public int getTabGravity() {
        return this.w;
    }

    public ColorStateList getTabIconTint() {
        return this.k;
    }

    public int getTabIndicatorGravity() {
        return this.y;
    }

    /* access modifiers changed from: package-private */
    public int getTabMaxWidth() {
        return this.r;
    }

    public int getTabMode() {
        return this.z;
    }

    public ColorStateList getTabRippleColor() {
        return this.l;
    }

    public Drawable getTabSelectedIndicator() {
        return this.m;
    }

    public ColorStateList getTabTextColors() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.H == null) {
            ViewParent parent = getParent();
            if (parent instanceof ViewPager) {
                a((ViewPager) parent, true, true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.M) {
            setupWithViewPager((ViewPager) null);
            this.M = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        for (int i2 = 0; i2 < this.d.getChildCount(); i2++) {
            View childAt = this.d.getChildAt(i2);
            if (childAt instanceof TabView) {
                ((TabView) childAt).a(canvas);
            }
        }
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int a2 = a(getDefaultHeight()) + getPaddingTop() + getPaddingBottom();
        int mode = View.MeasureSpec.getMode(i3);
        if (mode == Integer.MIN_VALUE) {
            i3 = View.MeasureSpec.makeMeasureSpec(Math.min(a2, View.MeasureSpec.getSize(i3)), 1073741824);
        } else if (mode == 0) {
            i3 = View.MeasureSpec.makeMeasureSpec(a2, 1073741824);
        }
        int size = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i2) != 0) {
            int i4 = this.t;
            if (i4 <= 0) {
                i4 = size - a(56);
            }
            this.r = i4;
        }
        super.onMeasure(i2, i3);
        if (getChildCount() == 1) {
            boolean z2 = false;
            View childAt = getChildAt(0);
            int i5 = this.z;
            if (i5 == 0 ? childAt.getMeasuredWidth() < getMeasuredWidth() : !(i5 != 1 || childAt.getMeasuredWidth() == getMeasuredWidth())) {
                z2 = true;
            }
            if (z2) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), HorizontalScrollView.getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom(), childAt.getLayoutParams().height));
            }
        }
    }

    public void setInlineLabel(boolean z2) {
        if (this.A != z2) {
            this.A = z2;
            for (int i2 = 0; i2 < this.d.getChildCount(); i2++) {
                View childAt = this.d.getChildAt(i2);
                if (childAt instanceof TabView) {
                    ((TabView) childAt).c();
                }
            }
            e();
        }
    }

    public void setInlineLabelResource(int i2) {
        setInlineLabel(getResources().getBoolean(i2));
    }

    @Deprecated
    public void setOnTabSelectedListener(BaseOnTabSelectedListener baseOnTabSelectedListener) {
        BaseOnTabSelectedListener baseOnTabSelectedListener2 = this.D;
        if (baseOnTabSelectedListener2 != null) {
            b(baseOnTabSelectedListener2);
        }
        this.D = baseOnTabSelectedListener;
        if (baseOnTabSelectedListener != null) {
            a(baseOnTabSelectedListener);
        }
    }

    /* access modifiers changed from: package-private */
    public void setScrollAnimatorListener(Animator.AnimatorListener animatorListener) {
        g();
        this.G.addListener(animatorListener);
    }

    public void setSelectedTabIndicator(Drawable drawable) {
        if (this.m != drawable) {
            this.m = drawable;
            ViewCompat.I(this.d);
        }
    }

    public void setSelectedTabIndicatorColor(int i2) {
        this.d.a(i2);
    }

    public void setSelectedTabIndicatorGravity(int i2) {
        if (this.y != i2) {
            this.y = i2;
            ViewCompat.I(this.d);
        }
    }

    @Deprecated
    public void setSelectedTabIndicatorHeight(int i2) {
        this.d.b(i2);
    }

    public void setTabGravity(int i2) {
        if (this.w != i2) {
            this.w = i2;
            e();
        }
    }

    public void setTabIconTint(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            h();
        }
    }

    public void setTabIconTintResource(int i2) {
        setTabIconTint(AppCompatResources.b(getContext(), i2));
    }

    public void setTabIndicatorFullWidth(boolean z2) {
        this.B = z2;
        ViewCompat.I(this.d);
    }

    public void setTabMode(int i2) {
        if (i2 != this.z) {
            this.z = i2;
            e();
        }
    }

    public void setTabRippleColor(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            for (int i2 = 0; i2 < this.d.getChildCount(); i2++) {
                View childAt = this.d.getChildAt(i2);
                if (childAt instanceof TabView) {
                    ((TabView) childAt).a(getContext());
                }
            }
        }
    }

    public void setTabRippleColorResource(int i2) {
        setTabRippleColor(AppCompatResources.b(getContext(), i2));
    }

    public void setTabTextColors(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            h();
        }
    }

    @Deprecated
    public void setTabsFromPagerAdapter(PagerAdapter pagerAdapter) {
        a(pagerAdapter, false);
    }

    public void setUnboundedRipple(boolean z2) {
        if (this.C != z2) {
            this.C = z2;
            for (int i2 = 0; i2 < this.d.getChildCount(); i2++) {
                View childAt = this.d.getChildAt(i2);
                if (childAt instanceof TabView) {
                    ((TabView) childAt).a(getContext());
                }
            }
        }
    }

    public void setUnboundedRippleResource(int i2) {
        setUnboundedRipple(getResources().getBoolean(i2));
    }

    public void setupWithViewPager(ViewPager viewPager) {
        a(viewPager, true);
    }

    public boolean shouldDelayChildPressedState() {
        return getTabScrollRange() > 0;
    }

    private class AdapterChangeListener implements ViewPager.OnAdapterChangeListener {

        /* renamed from: a  reason: collision with root package name */
        private boolean f4294a;

        AdapterChangeListener() {
        }

        public void a(ViewPager viewPager, PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2) {
            TabLayout tabLayout = TabLayout.this;
            if (tabLayout.H == viewPager) {
                tabLayout.a(pagerAdapter2, this.f4294a);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            this.f4294a = z;
        }
    }

    public TabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R$attr.tabStyle);
    }

    public void a(int i2, float f2, boolean z2) {
        a(i2, f2, z2, true);
    }

    public void addView(View view, int i2) {
        a(view);
    }

    public Tab b() {
        Tab a2 = a();
        a2.f = this;
        a2.g = e(a2);
        return a2;
    }

    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return generateDefaultLayoutParams();
    }

    private class SlidingTabIndicator extends LinearLayout {

        /* renamed from: a  reason: collision with root package name */
        private int f4296a;
        private final Paint b;
        private final GradientDrawable c;
        int d = -1;
        float e;
        private int f = -1;
        private int g = -1;
        private int h = -1;
        private ValueAnimator i;

        SlidingTabIndicator(Context context) {
            super(context);
            setWillNotDraw(false);
            this.b = new Paint();
            this.c = new GradientDrawable();
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            if (this.b.getColor() != i2) {
                this.b.setColor(i2);
                ViewCompat.I(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i2) {
            if (this.f4296a != i2) {
                this.f4296a = i2;
                ViewCompat.I(this);
            }
        }

        public void draw(Canvas canvas) {
            Drawable drawable = TabLayout.this.m;
            int i2 = 0;
            int intrinsicHeight = drawable != null ? drawable.getIntrinsicHeight() : 0;
            int i3 = this.f4296a;
            if (i3 >= 0) {
                intrinsicHeight = i3;
            }
            int i4 = TabLayout.this.y;
            if (i4 == 0) {
                i2 = getHeight() - intrinsicHeight;
                intrinsicHeight = getHeight();
            } else if (i4 == 1) {
                i2 = (getHeight() - intrinsicHeight) / 2;
                intrinsicHeight = (getHeight() + intrinsicHeight) / 2;
            } else if (i4 != 2) {
                if (i4 != 3) {
                    intrinsicHeight = 0;
                } else {
                    intrinsicHeight = getHeight();
                }
            }
            int i5 = this.g;
            if (i5 >= 0 && this.h > i5) {
                Drawable drawable2 = TabLayout.this.m;
                if (drawable2 == null) {
                    drawable2 = this.c;
                }
                Drawable i6 = DrawableCompat.i(drawable2);
                i6.setBounds(this.g, i2, this.h, intrinsicHeight);
                Paint paint = this.b;
                if (paint != null) {
                    if (Build.VERSION.SDK_INT == 21) {
                        i6.setColorFilter(paint.getColor(), PorterDuff.Mode.SRC_IN);
                    } else {
                        DrawableCompat.b(i6, paint.getColor());
                    }
                }
                i6.draw(canvas);
            }
            super.draw(canvas);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
            super.onLayout(z, i2, i3, i4, i5);
            ValueAnimator valueAnimator = this.i;
            if (valueAnimator == null || !valueAnimator.isRunning()) {
                b();
                return;
            }
            this.i.cancel();
            a(this.d, Math.round((1.0f - this.i.getAnimatedFraction()) * ((float) this.i.getDuration())));
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i2, int i3) {
            super.onMeasure(i2, i3);
            if (View.MeasureSpec.getMode(i2) == 1073741824) {
                TabLayout tabLayout = TabLayout.this;
                boolean z = true;
                if (tabLayout.z == 1 && tabLayout.w == 1) {
                    int childCount = getChildCount();
                    int i4 = 0;
                    for (int i5 = 0; i5 < childCount; i5++) {
                        View childAt = getChildAt(i5);
                        if (childAt.getVisibility() == 0) {
                            i4 = Math.max(i4, childAt.getMeasuredWidth());
                        }
                    }
                    if (i4 > 0) {
                        if (i4 * childCount <= getMeasuredWidth() - (TabLayout.this.a(16) * 2)) {
                            boolean z2 = false;
                            for (int i6 = 0; i6 < childCount; i6++) {
                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getChildAt(i6).getLayoutParams();
                                if (layoutParams.width != i4 || layoutParams.weight != 0.0f) {
                                    layoutParams.width = i4;
                                    layoutParams.weight = 0.0f;
                                    z2 = true;
                                }
                            }
                            z = z2;
                        } else {
                            TabLayout tabLayout2 = TabLayout.this;
                            tabLayout2.w = 0;
                            tabLayout2.a(false);
                        }
                        if (z) {
                            super.onMeasure(i2, i3);
                        }
                    }
                }
            }
        }

        public void onRtlPropertiesChanged(int i2) {
            super.onRtlPropertiesChanged(i2);
            if (Build.VERSION.SDK_INT < 23 && this.f != i2) {
                requestLayout();
                this.f = i2;
            }
        }

        private void b() {
            int i2;
            int i3;
            View childAt = getChildAt(this.d);
            if (childAt == null || childAt.getWidth() <= 0) {
                i3 = -1;
                i2 = -1;
            } else {
                i3 = childAt.getLeft();
                i2 = childAt.getRight();
                TabLayout tabLayout = TabLayout.this;
                if (!tabLayout.B && (childAt instanceof TabView)) {
                    a((TabView) childAt, tabLayout.c);
                    i3 = (int) TabLayout.this.c.left;
                    i2 = (int) TabLayout.this.c.right;
                }
                if (this.e > 0.0f && this.d < getChildCount() - 1) {
                    View childAt2 = getChildAt(this.d + 1);
                    int left = childAt2.getLeft();
                    int right = childAt2.getRight();
                    TabLayout tabLayout2 = TabLayout.this;
                    if (!tabLayout2.B && (childAt2 instanceof TabView)) {
                        a((TabView) childAt2, tabLayout2.c);
                        left = (int) TabLayout.this.c.left;
                        right = (int) TabLayout.this.c.right;
                    }
                    float f2 = this.e;
                    i3 = (int) ((((float) left) * f2) + ((1.0f - f2) * ((float) i3)));
                    i2 = (int) ((((float) right) * f2) + ((1.0f - f2) * ((float) i2)));
                }
            }
            b(i3, i2);
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                if (getChildAt(i2).getWidth() <= 0) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, float f2) {
            ValueAnimator valueAnimator = this.i;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.i.cancel();
            }
            this.d = i2;
            this.e = f2;
            b();
        }

        /* access modifiers changed from: package-private */
        public void a(final int i2, int i3) {
            ValueAnimator valueAnimator = this.i;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.i.cancel();
            }
            View childAt = getChildAt(i2);
            if (childAt == null) {
                b();
                return;
            }
            int left = childAt.getLeft();
            int right = childAt.getRight();
            TabLayout tabLayout = TabLayout.this;
            if (!tabLayout.B && (childAt instanceof TabView)) {
                a((TabView) childAt, tabLayout.c);
                left = (int) TabLayout.this.c.left;
                right = (int) TabLayout.this.c.right;
            }
            final int i4 = left;
            final int i5 = right;
            final int i6 = this.g;
            final int i7 = this.h;
            if (i6 != i4 || i7 != i5) {
                ValueAnimator valueAnimator2 = new ValueAnimator();
                this.i = valueAnimator2;
                valueAnimator2.setInterpolator(AnimationUtils.b);
                valueAnimator2.setDuration((long) i3);
                valueAnimator2.setFloatValues(new float[]{0.0f, 1.0f});
                valueAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float animatedFraction = valueAnimator.getAnimatedFraction();
                        SlidingTabIndicator.this.b(AnimationUtils.a(i6, i4, animatedFraction), AnimationUtils.a(i7, i5, animatedFraction));
                    }
                });
                valueAnimator2.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        SlidingTabIndicator slidingTabIndicator = SlidingTabIndicator.this;
                        slidingTabIndicator.d = i2;
                        slidingTabIndicator.e = 0.0f;
                    }
                });
                valueAnimator2.start();
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, int i3) {
            if (i2 != this.g || i3 != this.h) {
                this.g = i2;
                this.h = i3;
                ViewCompat.I(this);
            }
        }

        private void a(TabView tabView, RectF rectF) {
            int a2 = tabView.d();
            if (a2 < TabLayout.this.a(24)) {
                a2 = TabLayout.this.a(24);
            }
            int left = (tabView.getLeft() + tabView.getRight()) / 2;
            int i2 = a2 / 2;
            rectF.set((float) (left - i2), 0.0f, (float) (left + i2), 0.0f);
        }
    }

    class TabView extends LinearLayout {

        /* renamed from: a  reason: collision with root package name */
        private Tab f4301a;
        private TextView b;
        private ImageView c;
        private View d;
        private TextView e;
        private ImageView f;
        private Drawable g;
        private int h = 2;

        public TabView(Context context) {
            super(context);
            a(context);
            ViewCompat.b(this, TabLayout.this.e, TabLayout.this.f, TabLayout.this.g, TabLayout.this.h);
            setGravity(17);
            setOrientation(TabLayout.this.A ^ true ? 1 : 0);
            setClickable(true);
            ViewCompat.a((View) this, PointerIconCompat.a(getContext(), AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE));
        }

        /* access modifiers changed from: private */
        public int d() {
            int i2 = 0;
            int i3 = 0;
            boolean z = false;
            for (View view : new View[]{this.b, this.c, this.d}) {
                if (view != null && view.getVisibility() == 0) {
                    i3 = z ? Math.min(i3, view.getLeft()) : view.getLeft();
                    i2 = z ? Math.max(i2, view.getRight()) : view.getRight();
                    z = true;
                }
            }
            return i2 - i3;
        }

        /* access modifiers changed from: package-private */
        public final void b() {
            Tab tab = this.f4301a;
            Drawable drawable = null;
            View a2 = tab != null ? tab.a() : null;
            if (a2 != null) {
                ViewParent parent = a2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(a2);
                    }
                    addView(a2);
                }
                this.d = a2;
                TextView textView = this.b;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.c;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.c.setImageDrawable((Drawable) null);
                }
                this.e = (TextView) a2.findViewById(16908308);
                TextView textView2 = this.e;
                if (textView2 != null) {
                    this.h = TextViewCompat.d(textView2);
                }
                this.f = (ImageView) a2.findViewById(16908294);
            } else {
                View view = this.d;
                if (view != null) {
                    removeView(view);
                    this.d = null;
                }
                this.e = null;
                this.f = null;
            }
            boolean z = false;
            if (this.d == null) {
                if (this.c == null) {
                    ImageView imageView2 = (ImageView) LayoutInflater.from(getContext()).inflate(R$layout.design_layout_tab_icon, this, false);
                    addView(imageView2, 0);
                    this.c = imageView2;
                }
                if (!(tab == null || tab.b() == null)) {
                    drawable = DrawableCompat.i(tab.b()).mutate();
                }
                if (drawable != null) {
                    DrawableCompat.a(drawable, TabLayout.this.k);
                    PorterDuff.Mode mode = TabLayout.this.n;
                    if (mode != null) {
                        DrawableCompat.a(drawable, mode);
                    }
                }
                if (this.b == null) {
                    TextView textView3 = (TextView) LayoutInflater.from(getContext()).inflate(R$layout.design_layout_tab_text, this, false);
                    addView(textView3);
                    this.b = textView3;
                    this.h = TextViewCompat.d(this.b);
                }
                TextViewCompat.d(this.b, TabLayout.this.i);
                ColorStateList colorStateList = TabLayout.this.j;
                if (colorStateList != null) {
                    this.b.setTextColor(colorStateList);
                }
                a(this.b, this.c);
            } else if (!(this.e == null && this.f == null)) {
                a(this.e, this.f);
            }
            if (tab != null && !TextUtils.isEmpty(tab.c)) {
                setContentDescription(tab.c);
            }
            if (tab != null && tab.e()) {
                z = true;
            }
            setSelected(z);
        }

        /* access modifiers changed from: package-private */
        public final void c() {
            setOrientation(TabLayout.this.A ^ true ? 1 : 0);
            if (this.e == null && this.f == null) {
                a(this.b, this.c);
            } else {
                a(this.e, this.f);
            }
        }

        /* access modifiers changed from: protected */
        public void drawableStateChanged() {
            super.drawableStateChanged();
            int[] drawableState = getDrawableState();
            Drawable drawable = this.g;
            boolean z = false;
            if (drawable != null && drawable.isStateful()) {
                z = false | this.g.setState(drawableState);
            }
            if (z) {
                invalidate();
                TabLayout.this.invalidate();
            }
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(ActionBar.Tab.class.getName());
        }

        @TargetApi(14)
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(ActionBar.Tab.class.getName());
        }

        public void onMeasure(int i2, int i3) {
            Layout layout;
            int size = View.MeasureSpec.getSize(i2);
            int mode = View.MeasureSpec.getMode(i2);
            int tabMaxWidth = TabLayout.this.getTabMaxWidth();
            if (tabMaxWidth > 0 && (mode == 0 || size > tabMaxWidth)) {
                i2 = View.MeasureSpec.makeMeasureSpec(TabLayout.this.r, Integer.MIN_VALUE);
            }
            super.onMeasure(i2, i3);
            if (this.b != null) {
                float f2 = TabLayout.this.o;
                int i4 = this.h;
                ImageView imageView = this.c;
                boolean z = true;
                if (imageView == null || imageView.getVisibility() != 0) {
                    TextView textView = this.b;
                    if (textView != null && textView.getLineCount() > 1) {
                        f2 = TabLayout.this.p;
                    }
                } else {
                    i4 = 1;
                }
                float textSize = this.b.getTextSize();
                int lineCount = this.b.getLineCount();
                int d2 = TextViewCompat.d(this.b);
                int i5 = (f2 > textSize ? 1 : (f2 == textSize ? 0 : -1));
                if (i5 != 0 || (d2 >= 0 && i4 != d2)) {
                    if (TabLayout.this.z == 1 && i5 > 0 && lineCount == 1 && ((layout = this.b.getLayout()) == null || a(layout, 0, f2) > ((float) ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight())))) {
                        z = false;
                    }
                    if (z) {
                        this.b.setTextSize(0, f2);
                        this.b.setMaxLines(i4);
                        super.onMeasure(i2, i3);
                    }
                }
            }
        }

        public boolean performClick() {
            boolean performClick = super.performClick();
            if (this.f4301a == null) {
                return performClick;
            }
            if (!performClick) {
                playSoundEffect(0);
            }
            this.f4301a.g();
            return true;
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z && Build.VERSION.SDK_INT < 16) {
                sendAccessibilityEvent(4);
            }
            TextView textView = this.b;
            if (textView != null) {
                textView.setSelected(z);
            }
            ImageView imageView = this.c;
            if (imageView != null) {
                imageView.setSelected(z);
            }
            View view = this.d;
            if (view != null) {
                view.setSelected(z);
            }
        }

        /* JADX WARNING: type inference failed for: r2v3, types: [android.graphics.drawable.LayerDrawable] */
        /* JADX WARNING: type inference failed for: r0v3, types: [android.graphics.drawable.RippleDrawable] */
        /* access modifiers changed from: private */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(android.content.Context r7) {
            /*
                r6 = this;
                com.google.android.material.tabs.TabLayout r0 = com.google.android.material.tabs.TabLayout.this
                int r0 = r0.q
                r1 = 0
                if (r0 == 0) goto L_0x0021
                android.graphics.drawable.Drawable r7 = androidx.appcompat.content.res.AppCompatResources.c(r7, r0)
                r6.g = r7
                android.graphics.drawable.Drawable r7 = r6.g
                if (r7 == 0) goto L_0x0023
                boolean r7 = r7.isStateful()
                if (r7 == 0) goto L_0x0023
                android.graphics.drawable.Drawable r7 = r6.g
                int[] r0 = r6.getDrawableState()
                r7.setState(r0)
                goto L_0x0023
            L_0x0021:
                r6.g = r1
            L_0x0023:
                android.graphics.drawable.GradientDrawable r7 = new android.graphics.drawable.GradientDrawable
                r7.<init>()
                r0 = 0
                r7.setColor(r0)
                com.google.android.material.tabs.TabLayout r2 = com.google.android.material.tabs.TabLayout.this
                android.content.res.ColorStateList r2 = r2.l
                if (r2 == 0) goto L_0x007a
                android.graphics.drawable.GradientDrawable r2 = new android.graphics.drawable.GradientDrawable
                r2.<init>()
                r3 = 925353388(0x3727c5ac, float:1.0E-5)
                r2.setCornerRadius(r3)
                r3 = -1
                r2.setColor(r3)
                com.google.android.material.tabs.TabLayout r3 = com.google.android.material.tabs.TabLayout.this
                android.content.res.ColorStateList r3 = r3.l
                android.content.res.ColorStateList r3 = com.google.android.material.ripple.RippleUtils.a((android.content.res.ColorStateList) r3)
                int r4 = android.os.Build.VERSION.SDK_INT
                r5 = 21
                if (r4 < r5) goto L_0x0065
                android.graphics.drawable.RippleDrawable r0 = new android.graphics.drawable.RippleDrawable
                com.google.android.material.tabs.TabLayout r4 = com.google.android.material.tabs.TabLayout.this
                boolean r4 = r4.C
                if (r4 == 0) goto L_0x0058
                r7 = r1
            L_0x0058:
                com.google.android.material.tabs.TabLayout r4 = com.google.android.material.tabs.TabLayout.this
                boolean r4 = r4.C
                if (r4 == 0) goto L_0x005f
                goto L_0x0060
            L_0x005f:
                r1 = r2
            L_0x0060:
                r0.<init>(r3, r7, r1)
                r7 = r0
                goto L_0x007a
            L_0x0065:
                android.graphics.drawable.Drawable r1 = androidx.core.graphics.drawable.DrawableCompat.i(r2)
                androidx.core.graphics.drawable.DrawableCompat.a((android.graphics.drawable.Drawable) r1, (android.content.res.ColorStateList) r3)
                android.graphics.drawable.LayerDrawable r2 = new android.graphics.drawable.LayerDrawable
                r3 = 2
                android.graphics.drawable.Drawable[] r3 = new android.graphics.drawable.Drawable[r3]
                r3[r0] = r7
                r7 = 1
                r3[r7] = r1
                r2.<init>(r3)
                r7 = r2
            L_0x007a:
                androidx.core.view.ViewCompat.a((android.view.View) r6, (android.graphics.drawable.Drawable) r7)
                com.google.android.material.tabs.TabLayout r7 = com.google.android.material.tabs.TabLayout.this
                r7.invalidate()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.tabs.TabLayout.TabView.a(android.content.Context):void");
        }

        /* access modifiers changed from: private */
        public void a(Canvas canvas) {
            Drawable drawable = this.g;
            if (drawable != null) {
                drawable.setBounds(getLeft(), getTop(), getRight(), getBottom());
                this.g.draw(canvas);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(Tab tab) {
            if (tab != this.f4301a) {
                this.f4301a = tab;
                b();
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            a((Tab) null);
            setSelected(false);
        }

        private void a(TextView textView, ImageView imageView) {
            Tab tab = this.f4301a;
            Drawable mutate = (tab == null || tab.b() == null) ? null : DrawableCompat.i(this.f4301a.b()).mutate();
            Tab tab2 = this.f4301a;
            CharSequence d2 = tab2 != null ? tab2.d() : null;
            if (imageView != null) {
                if (mutate != null) {
                    imageView.setImageDrawable(mutate);
                    imageView.setVisibility(0);
                    setVisibility(0);
                } else {
                    imageView.setVisibility(8);
                    imageView.setImageDrawable((Drawable) null);
                }
            }
            boolean z = !TextUtils.isEmpty(d2);
            if (textView != null) {
                if (z) {
                    textView.setText(d2);
                    textView.setVisibility(0);
                    setVisibility(0);
                } else {
                    textView.setVisibility(8);
                    textView.setText((CharSequence) null);
                }
            }
            if (imageView != null) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) imageView.getLayoutParams();
                int a2 = (!z || imageView.getVisibility() != 0) ? 0 : TabLayout.this.a(8);
                if (TabLayout.this.A) {
                    if (a2 != MarginLayoutParamsCompat.a(marginLayoutParams)) {
                        MarginLayoutParamsCompat.a(marginLayoutParams, a2);
                        marginLayoutParams.bottomMargin = 0;
                        imageView.setLayoutParams(marginLayoutParams);
                        imageView.requestLayout();
                    }
                } else if (a2 != marginLayoutParams.bottomMargin) {
                    marginLayoutParams.bottomMargin = a2;
                    MarginLayoutParamsCompat.a(marginLayoutParams, 0);
                    imageView.setLayoutParams(marginLayoutParams);
                    imageView.requestLayout();
                }
            }
            Tab tab3 = this.f4301a;
            CharSequence a3 = tab3 != null ? tab3.c : null;
            if (z) {
                a3 = null;
            }
            TooltipCompat.a(this, a3);
        }

        private float a(Layout layout, int i2, float f2) {
            return layout.getLineWidth(i2) * (f2 / layout.getPaint().getTextSize());
        }
    }

    /* JADX INFO: finally extract failed */
    public TabLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f4292a = new ArrayList<>();
        this.c = new RectF();
        this.r = Integer.MAX_VALUE;
        this.E = new ArrayList<>();
        this.N = new Pools$SimplePool(12);
        setHorizontalScrollBarEnabled(false);
        this.d = new SlidingTabIndicator(context);
        super.addView(this.d, 0, new FrameLayout.LayoutParams(-2, -1));
        TypedArray c2 = ThemeEnforcement.c(context, attributeSet, R$styleable.TabLayout, i2, R$style.Widget_Design_TabLayout, R$styleable.TabLayout_tabTextAppearance);
        this.d.b(c2.getDimensionPixelSize(R$styleable.TabLayout_tabIndicatorHeight, -1));
        this.d.a(c2.getColor(R$styleable.TabLayout_tabIndicatorColor, 0));
        setSelectedTabIndicator(MaterialResources.b(context, c2, R$styleable.TabLayout_tabIndicator));
        setSelectedTabIndicatorGravity(c2.getInt(R$styleable.TabLayout_tabIndicatorGravity, 0));
        setTabIndicatorFullWidth(c2.getBoolean(R$styleable.TabLayout_tabIndicatorFullWidth, true));
        int dimensionPixelSize = c2.getDimensionPixelSize(R$styleable.TabLayout_tabPadding, 0);
        this.h = dimensionPixelSize;
        this.g = dimensionPixelSize;
        this.f = dimensionPixelSize;
        this.e = dimensionPixelSize;
        this.e = c2.getDimensionPixelSize(R$styleable.TabLayout_tabPaddingStart, this.e);
        this.f = c2.getDimensionPixelSize(R$styleable.TabLayout_tabPaddingTop, this.f);
        this.g = c2.getDimensionPixelSize(R$styleable.TabLayout_tabPaddingEnd, this.g);
        this.h = c2.getDimensionPixelSize(R$styleable.TabLayout_tabPaddingBottom, this.h);
        this.i = c2.getResourceId(R$styleable.TabLayout_tabTextAppearance, R$style.TextAppearance_Design_Tab);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(this.i, androidx.appcompat.R$styleable.TextAppearance);
        try {
            this.o = (float) obtainStyledAttributes.getDimensionPixelSize(androidx.appcompat.R$styleable.TextAppearance_android_textSize, 0);
            this.j = MaterialResources.a(context, obtainStyledAttributes, androidx.appcompat.R$styleable.TextAppearance_android_textColor);
            obtainStyledAttributes.recycle();
            if (c2.hasValue(R$styleable.TabLayout_tabTextColor)) {
                this.j = MaterialResources.a(context, c2, R$styleable.TabLayout_tabTextColor);
            }
            if (c2.hasValue(R$styleable.TabLayout_tabSelectedTextColor)) {
                this.j = a(this.j.getDefaultColor(), c2.getColor(R$styleable.TabLayout_tabSelectedTextColor, 0));
            }
            this.k = MaterialResources.a(context, c2, R$styleable.TabLayout_tabIconTint);
            this.n = ViewUtils.a(c2.getInt(R$styleable.TabLayout_tabIconTintMode, -1), (PorterDuff.Mode) null);
            this.l = MaterialResources.a(context, c2, R$styleable.TabLayout_tabRippleColor);
            this.x = c2.getInt(R$styleable.TabLayout_tabIndicatorAnimationDuration, 300);
            this.s = c2.getDimensionPixelSize(R$styleable.TabLayout_tabMinWidth, -1);
            this.t = c2.getDimensionPixelSize(R$styleable.TabLayout_tabMaxWidth, -1);
            this.q = c2.getResourceId(R$styleable.TabLayout_tabBackground, 0);
            this.v = c2.getDimensionPixelSize(R$styleable.TabLayout_tabContentStart, 0);
            this.z = c2.getInt(R$styleable.TabLayout_tabMode, 1);
            this.w = c2.getInt(R$styleable.TabLayout_tabGravity, 0);
            this.A = c2.getBoolean(R$styleable.TabLayout_tabInlineLabel, false);
            this.C = c2.getBoolean(R$styleable.TabLayout_tabUnboundedRipple, false);
            c2.recycle();
            Resources resources = getResources();
            this.p = (float) resources.getDimensionPixelSize(R$dimen.design_tab_text_size_2line);
            this.u = resources.getDimensionPixelSize(R$dimen.design_tab_scrollable_min_width);
            e();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    private void f(Tab tab) {
        for (int size = this.E.size() - 1; size >= 0; size--) {
            this.E.get(size).c(tab);
        }
    }

    private void h(Tab tab) {
        for (int size = this.E.size() - 1; size >= 0; size--) {
            this.E.get(size).b(tab);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, float f2, boolean z2, boolean z3) {
        int round = Math.round(((float) i2) + f2);
        if (round >= 0 && round < this.d.getChildCount()) {
            if (z3) {
                this.d.a(i2, f2);
            }
            ValueAnimator valueAnimator = this.G;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.G.cancel();
            }
            scrollTo(a(i2, f2), 0);
            if (z2) {
                setSelectedTabView(round);
            }
        }
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        a(view);
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        a(view);
    }

    public void setSelectedTabIndicator(int i2) {
        if (i2 != 0) {
            setSelectedTabIndicator(AppCompatResources.c(getContext(), i2));
        } else {
            setSelectedTabIndicator((Drawable) null);
        }
    }

    /* access modifiers changed from: protected */
    public boolean b(Tab tab) {
        return O.release(tab);
    }

    private void g(Tab tab) {
        for (int size = this.E.size() - 1; size >= 0; size--) {
            this.E.get(size).a(tab);
        }
    }

    public Tab b(int i2) {
        if (i2 < 0 || i2 >= getTabCount()) {
            return null;
        }
        return this.f4292a.get(i2);
    }

    /* access modifiers changed from: package-private */
    public void b(Tab tab, boolean z2) {
        Tab tab2 = this.b;
        if (tab2 != tab) {
            int c2 = tab != null ? tab.c() : -1;
            if (z2) {
                if ((tab2 == null || tab2.c() == -1) && c2 != -1) {
                    a(c2, 0.0f, true);
                } else {
                    c(c2);
                }
                if (c2 != -1) {
                    setSelectedTabView(c2);
                }
            }
            this.b = tab;
            if (tab2 != null) {
                h(tab2);
            }
            if (tab != null) {
                g(tab);
            }
        } else if (tab2 != null) {
            f(tab);
            c(tab.c());
        }
    }

    private void c(int i2) {
        if (i2 != -1) {
            if (getWindowToken() == null || !ViewCompat.E(this) || this.d.a()) {
                a(i2, 0.0f, true);
                return;
            }
            int scrollX = getScrollX();
            int a2 = a(i2, 0.0f);
            if (scrollX != a2) {
                g();
                this.G.setIntValues(new int[]{scrollX, a2});
                this.G.start();
            }
            this.d.a(i2, this.x);
        }
    }

    private void d(Tab tab) {
        this.d.addView(tab.g, tab.c(), f());
    }

    private void e() {
        ViewCompat.b(this.d, this.z == 0 ? Math.max(0, this.v - this.e) : 0, 0, 0, 0);
        int i2 = this.z;
        if (i2 == 0) {
            this.d.setGravity(8388611);
        } else if (i2 == 1) {
            this.d.setGravity(1);
        }
        a(true);
    }

    public void a(Tab tab) {
        a(tab, this.f4292a.isEmpty());
    }

    private void d(int i2) {
        TabView tabView = (TabView) this.d.getChildAt(i2);
        this.d.removeViewAt(i2);
        if (tabView != null) {
            tabView.a();
            this.N.release(tabView);
        }
        requestLayout();
    }

    public void a(Tab tab, boolean z2) {
        a(tab, this.f4292a.size(), z2);
    }

    public void a(Tab tab, int i2, boolean z2) {
        if (tab.f == this) {
            a(tab, i2);
            d(tab);
            if (z2) {
                tab.g();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
    }

    private void a(TabItem tabItem) {
        Tab b2 = b();
        CharSequence charSequence = tabItem.f4291a;
        if (charSequence != null) {
            b2.b(charSequence);
        }
        Drawable drawable = tabItem.b;
        if (drawable != null) {
            b2.a(drawable);
        }
        int i2 = tabItem.c;
        if (i2 != 0) {
            b2.a(i2);
        }
        if (!TextUtils.isEmpty(tabItem.getContentDescription())) {
            b2.a(tabItem.getContentDescription());
        }
        a(b2);
    }

    /* access modifiers changed from: package-private */
    public void c(Tab tab) {
        b(tab, true);
    }

    public void a(BaseOnTabSelectedListener baseOnTabSelectedListener) {
        if (!this.E.contains(baseOnTabSelectedListener)) {
            this.E.add(baseOnTabSelectedListener);
        }
    }

    /* access modifiers changed from: protected */
    public Tab a() {
        Tab acquire = O.acquire();
        return acquire == null ? new Tab() : acquire;
    }

    public void a(ViewPager viewPager, boolean z2) {
        a(viewPager, z2, false);
    }

    private void a(ViewPager viewPager, boolean z2, boolean z3) {
        ViewPager viewPager2 = this.H;
        if (viewPager2 != null) {
            TabLayoutOnPageChangeListener tabLayoutOnPageChangeListener = this.K;
            if (tabLayoutOnPageChangeListener != null) {
                viewPager2.removeOnPageChangeListener(tabLayoutOnPageChangeListener);
            }
            AdapterChangeListener adapterChangeListener = this.L;
            if (adapterChangeListener != null) {
                this.H.removeOnAdapterChangeListener(adapterChangeListener);
            }
        }
        BaseOnTabSelectedListener baseOnTabSelectedListener = this.F;
        if (baseOnTabSelectedListener != null) {
            b(baseOnTabSelectedListener);
            this.F = null;
        }
        if (viewPager != null) {
            this.H = viewPager;
            if (this.K == null) {
                this.K = new TabLayoutOnPageChangeListener(this);
            }
            this.K.a();
            viewPager.addOnPageChangeListener(this.K);
            this.F = new ViewPagerOnTabSelectedListener(viewPager);
            a(this.F);
            PagerAdapter adapter = viewPager.getAdapter();
            if (adapter != null) {
                a(adapter, z2);
            }
            if (this.L == null) {
                this.L = new AdapterChangeListener();
            }
            this.L.a(z2);
            viewPager.addOnAdapterChangeListener(this.L);
            a(viewPager.getCurrentItem(), 0.0f, true);
        } else {
            this.H = null;
            a((PagerAdapter) null, false);
        }
        this.M = z3;
    }

    /* access modifiers changed from: package-private */
    public void a(PagerAdapter pagerAdapter, boolean z2) {
        DataSetObserver dataSetObserver;
        PagerAdapter pagerAdapter2 = this.I;
        if (!(pagerAdapter2 == null || (dataSetObserver = this.J) == null)) {
            pagerAdapter2.unregisterDataSetObserver(dataSetObserver);
        }
        this.I = pagerAdapter;
        if (z2 && pagerAdapter != null) {
            if (this.J == null) {
                this.J = new PagerAdapterObserver();
            }
            pagerAdapter.registerDataSetObserver(this.J);
        }
        c();
    }

    private void a(Tab tab, int i2) {
        tab.b(i2);
        this.f4292a.add(i2, tab);
        int size = this.f4292a.size();
        while (true) {
            i2++;
            if (i2 < size) {
                this.f4292a.get(i2).b(i2);
            } else {
                return;
            }
        }
    }

    private void a(View view) {
        if (view instanceof TabItem) {
            a((TabItem) view);
            return;
        }
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    private void a(LinearLayout.LayoutParams layoutParams) {
        if (this.z == 1 && this.w == 0) {
            layoutParams.width = 0;
            layoutParams.weight = 1.0f;
            return;
        }
        layoutParams.width = -2;
        layoutParams.weight = 0.0f;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2) {
        return Math.round(getResources().getDisplayMetrics().density * ((float) i2));
    }

    private int a(int i2, float f2) {
        int i3 = 0;
        if (this.z != 0) {
            return 0;
        }
        View childAt = this.d.getChildAt(i2);
        int i4 = i2 + 1;
        View childAt2 = i4 < this.d.getChildCount() ? this.d.getChildAt(i4) : null;
        int width = childAt != null ? childAt.getWidth() : 0;
        if (childAt2 != null) {
            i3 = childAt2.getWidth();
        }
        int left = (childAt.getLeft() + (width / 2)) - (getWidth() / 2);
        int i5 = (int) (((float) (width + i3)) * 0.5f * f2);
        return ViewCompat.m(this) == 0 ? left + i5 : left - i5;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        for (int i2 = 0; i2 < this.d.getChildCount(); i2++) {
            View childAt = this.d.getChildAt(i2);
            childAt.setMinimumWidth(getTabMinWidth());
            a((LinearLayout.LayoutParams) childAt.getLayoutParams());
            if (z2) {
                childAt.requestLayout();
            }
        }
    }

    private static ColorStateList a(int i2, int i3) {
        return new ColorStateList(new int[][]{HorizontalScrollView.SELECTED_STATE_SET, HorizontalScrollView.EMPTY_STATE_SET}, new int[]{i3, i2});
    }
}
