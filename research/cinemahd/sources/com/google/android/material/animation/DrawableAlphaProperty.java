package com.google.android.material.animation;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Property;
import com.facebook.imageutils.JfifUtil;
import java.util.WeakHashMap;

public class DrawableAlphaProperty extends Property<Drawable, Integer> {
    public static final Property<Drawable, Integer> b = new DrawableAlphaProperty();

    /* renamed from: a  reason: collision with root package name */
    private final WeakHashMap<Drawable, Integer> f4170a = new WeakHashMap<>();

    private DrawableAlphaProperty() {
        super(Integer.class, "drawableAlphaCompat");
    }

    /* renamed from: a */
    public Integer get(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 19) {
            return Integer.valueOf(drawable.getAlpha());
        }
        if (this.f4170a.containsKey(drawable)) {
            return this.f4170a.get(drawable);
        }
        return Integer.valueOf(JfifUtil.MARKER_FIRST_BYTE);
    }

    /* renamed from: a */
    public void set(Drawable drawable, Integer num) {
        if (Build.VERSION.SDK_INT < 19) {
            this.f4170a.put(drawable, num);
        }
        drawable.setAlpha(num.intValue());
    }
}
