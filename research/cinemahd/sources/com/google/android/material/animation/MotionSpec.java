package com.google.android.material.animation;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;
import java.util.List;

public class MotionSpec {

    /* renamed from: a  reason: collision with root package name */
    private final SimpleArrayMap<String, MotionTiming> f4173a = new SimpleArrayMap<>();

    public MotionTiming a(String str) {
        if (b(str)) {
            return this.f4173a.get(str);
        }
        throw new IllegalArgumentException();
    }

    public boolean b(String str) {
        return this.f4173a.get(str) != null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || MotionSpec.class != obj.getClass()) {
            return false;
        }
        return this.f4173a.equals(((MotionSpec) obj).f4173a);
    }

    public int hashCode() {
        return this.f4173a.hashCode();
    }

    public String toString() {
        return 10 + MotionSpec.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " timings: " + this.f4173a + "}\n";
    }

    public void a(String str, MotionTiming motionTiming) {
        this.f4173a.put(str, motionTiming);
    }

    public long a() {
        int size = this.f4173a.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            MotionTiming d = this.f4173a.d(i);
            j = Math.max(j, d.a() + d.b());
        }
        return j;
    }

    public static MotionSpec a(Context context, TypedArray typedArray, int i) {
        int resourceId;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0) {
            return null;
        }
        return a(context, resourceId);
    }

    public static MotionSpec a(Context context, int i) {
        try {
            Animator loadAnimator = AnimatorInflater.loadAnimator(context, i);
            if (loadAnimator instanceof AnimatorSet) {
                return a((List<Animator>) ((AnimatorSet) loadAnimator).getChildAnimations());
            }
            if (loadAnimator == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(loadAnimator);
            return a((List<Animator>) arrayList);
        } catch (Exception e) {
            Log.w("MotionSpec", "Can't load animation resource ID #0x" + Integer.toHexString(i), e);
            return null;
        }
    }

    private static MotionSpec a(List<Animator> list) {
        MotionSpec motionSpec = new MotionSpec();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(motionSpec, list.get(i));
        }
        return motionSpec;
    }

    private static void a(MotionSpec motionSpec, Animator animator) {
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            motionSpec.a(objectAnimator.getPropertyName(), MotionTiming.a((ValueAnimator) objectAnimator));
            return;
        }
        throw new IllegalArgumentException("Animator must be an ObjectAnimator: " + animator);
    }
}
