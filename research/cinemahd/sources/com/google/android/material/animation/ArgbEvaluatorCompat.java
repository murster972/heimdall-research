package com.google.android.material.animation;

import android.animation.TypeEvaluator;
import com.facebook.imageutils.JfifUtil;

public class ArgbEvaluatorCompat implements TypeEvaluator<Integer> {

    /* renamed from: a  reason: collision with root package name */
    private static final ArgbEvaluatorCompat f4168a = new ArgbEvaluatorCompat();

    public static ArgbEvaluatorCompat a() {
        return f4168a;
    }

    /* renamed from: a */
    public Integer evaluate(float f, Integer num, Integer num2) {
        int intValue = num.intValue();
        float f2 = ((float) ((intValue >> 24) & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f;
        int intValue2 = num2.intValue();
        float pow = (float) Math.pow((double) (((float) ((intValue >> 16) & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f), 2.2d);
        float pow2 = (float) Math.pow((double) (((float) ((intValue >> 8) & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f), 2.2d);
        float pow3 = (float) Math.pow((double) (((float) (intValue & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f), 2.2d);
        float pow4 = (float) Math.pow((double) (((float) ((intValue2 >> 16) & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f), 2.2d);
        float pow5 = pow3 + (f * (((float) Math.pow((double) (((float) (intValue2 & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f), 2.2d)) - pow3));
        return Integer.valueOf((Math.round(((float) Math.pow((double) (pow + ((pow4 - pow) * f)), 0.45454545454545453d)) * 255.0f) << 16) | (Math.round((f2 + (((((float) ((intValue2 >> 24) & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f) - f2) * f)) * 255.0f) << 24) | (Math.round(((float) Math.pow((double) (pow2 + ((((float) Math.pow((double) (((float) ((intValue2 >> 8) & JfifUtil.MARKER_FIRST_BYTE)) / 255.0f), 2.2d)) - pow2) * f)), 0.45454545454545453d)) * 255.0f) << 8) | Math.round(((float) Math.pow((double) pow5, 0.45454545454545453d)) * 255.0f));
    }
}
