package com.google.android.material.animation;

public class Positioning {

    /* renamed from: a  reason: collision with root package name */
    public final int f4175a;
    public final float b;
    public final float c;

    public Positioning(int i, float f, float f2) {
        this.f4175a = i;
        this.b = f;
        this.c = f2;
    }
}
