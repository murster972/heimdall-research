package com.google.android.material.textfield;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.TintTypedArray;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.core.widget.TextViewCompat;
import androidx.customview.view.AbsSavedState;
import com.google.android.material.R$attr;
import com.google.android.material.R$color;
import com.google.android.material.R$dimen;
import com.google.android.material.R$id;
import com.google.android.material.R$layout;
import com.google.android.material.R$string;
import com.google.android.material.R$style;
import com.google.android.material.R$styleable;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.internal.CheckableImageButton;
import com.google.android.material.internal.CollapsingTextHelper;
import com.google.android.material.internal.DescendantOffsetUtils;
import com.google.android.material.internal.DrawableUtils;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.internal.ViewUtils;

public class TextInputLayout extends LinearLayout {
    private int A;
    private Drawable B;
    private final Rect C;
    private final RectF D;
    private Typeface E;
    private boolean F;
    private Drawable G;
    private CharSequence H;
    private CheckableImageButton I;
    private boolean J;
    private Drawable K;
    private Drawable L;
    private ColorStateList M;
    private boolean N;
    private PorterDuff.Mode O;
    private boolean P;
    private ColorStateList Q;
    private ColorStateList R;
    private final int S;
    private final int T;
    private int U;
    private final int V;
    private boolean W;

    /* renamed from: a  reason: collision with root package name */
    private final FrameLayout f4306a;
    EditText b;
    private CharSequence c;
    final CollapsingTextHelper c0;
    private final IndicatorViewController d;
    private boolean d0;
    boolean e;
    private ValueAnimator e0;
    private int f;
    private boolean f0;
    private boolean g;
    private boolean g0;
    private TextView h;
    /* access modifiers changed from: private */
    public boolean h0;
    private final int i;
    private final int j;
    private boolean k;
    private CharSequence l;
    private boolean m;
    private GradientDrawable n;
    private final int o;
    private final int p;
    private int q;
    private final int r;
    private float s;
    private float t;
    private float u;
    private float v;
    private int w;
    private final int x;
    private final int y;
    private int z;

    public static class AccessibilityDelegate extends AccessibilityDelegateCompat {

        /* renamed from: a  reason: collision with root package name */
        private final TextInputLayout f4310a;

        public AccessibilityDelegate(TextInputLayout textInputLayout) {
            this.f4310a = textInputLayout;
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
            EditText editText = this.f4310a.getEditText();
            Editable text = editText != null ? editText.getText() : null;
            CharSequence hint = this.f4310a.getHint();
            CharSequence error = this.f4310a.getError();
            CharSequence counterOverflowDescription = this.f4310a.getCounterOverflowDescription();
            boolean z = !TextUtils.isEmpty(text);
            boolean z2 = !TextUtils.isEmpty(hint);
            boolean z3 = !TextUtils.isEmpty(error);
            boolean z4 = false;
            boolean z5 = z3 || !TextUtils.isEmpty(counterOverflowDescription);
            if (z) {
                accessibilityNodeInfoCompat.h((CharSequence) text);
            } else if (z2) {
                accessibilityNodeInfoCompat.h(hint);
            }
            if (z2) {
                accessibilityNodeInfoCompat.d(hint);
                if (!z && z2) {
                    z4 = true;
                }
                accessibilityNodeInfoCompat.p(z4);
            }
            if (z5) {
                if (!z3) {
                    error = counterOverflowDescription;
                }
                accessibilityNodeInfoCompat.c(error);
                accessibilityNodeInfoCompat.f(true);
            }
        }

        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onPopulateAccessibilityEvent(view, accessibilityEvent);
            EditText editText = this.f4310a.getEditText();
            CharSequence text = editText != null ? editText.getText() : null;
            if (TextUtils.isEmpty(text)) {
                text = this.f4310a.getHint();
            }
            if (!TextUtils.isEmpty(text)) {
                accessibilityEvent.getText().add(text);
            }
        }
    }

    static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, (ClassLoader) null);
            }
        };
        CharSequence c;
        boolean d;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "TextInputLayout.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " error=" + this.c + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            TextUtils.writeToParcel(this.c, parcel, i);
            parcel.writeInt(this.d ? 1 : 0);
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.c = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.d = parcel.readInt() != 1 ? false : true;
        }
    }

    public TextInputLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    private void e() {
        int i2;
        Drawable drawable;
        if (this.n != null) {
            q();
            EditText editText = this.b;
            if (editText != null && this.q == 2) {
                if (editText.getBackground() != null) {
                    this.B = this.b.getBackground();
                }
                ViewCompat.a((View) this.b, (Drawable) null);
            }
            EditText editText2 = this.b;
            if (!(editText2 == null || this.q != 1 || (drawable = this.B) == null)) {
                ViewCompat.a((View) editText2, drawable);
            }
            int i3 = this.w;
            if (i3 > -1 && (i2 = this.z) != 0) {
                this.n.setStroke(i3, i2);
            }
            this.n.setCornerRadii(getCornerRadiiAsArray());
            this.n.setColor(this.A);
            invalidate();
        }
    }

    private void f() {
        Drawable drawable;
        if (this.G == null) {
            return;
        }
        if (this.N || this.P) {
            this.G = DrawableCompat.i(this.G).mutate();
            if (this.N) {
                DrawableCompat.a(this.G, this.M);
            }
            if (this.P) {
                DrawableCompat.a(this.G, this.O);
            }
            CheckableImageButton checkableImageButton = this.I;
            if (checkableImageButton != null && checkableImageButton.getDrawable() != (drawable = this.G)) {
                this.I.setImageDrawable(drawable);
            }
        }
    }

    private void g() {
        int i2 = this.q;
        if (i2 == 0) {
            this.n = null;
        } else if (i2 == 2 && this.k && !(this.n instanceof CutoutDrawable)) {
            this.n = new CutoutDrawable();
        } else if (!(this.n instanceof GradientDrawable)) {
            this.n = new GradientDrawable();
        }
    }

    private Drawable getBoxBackground() {
        int i2 = this.q;
        if (i2 == 1 || i2 == 2) {
            return this.n;
        }
        throw new IllegalStateException();
    }

    private float[] getCornerRadiiAsArray() {
        if (!ViewUtils.a(this)) {
            float f2 = this.s;
            float f3 = this.t;
            float f4 = this.u;
            float f5 = this.v;
            return new float[]{f2, f2, f3, f3, f4, f4, f5, f5};
        }
        float f6 = this.t;
        float f7 = this.s;
        float f8 = this.v;
        float f9 = this.u;
        return new float[]{f6, f6, f7, f7, f8, f8, f9, f9};
    }

    private int h() {
        EditText editText = this.b;
        if (editText == null) {
            return 0;
        }
        int i2 = this.q;
        if (i2 == 1) {
            return editText.getTop();
        }
        if (i2 != 2) {
            return 0;
        }
        return editText.getTop() + j();
    }

    private int i() {
        int i2 = this.q;
        if (i2 == 1) {
            return getBoxBackground().getBounds().top + this.r;
        }
        if (i2 != 2) {
            return getPaddingTop();
        }
        return getBoxBackground().getBounds().top - j();
    }

    private int j() {
        float d2;
        if (!this.k) {
            return 0;
        }
        int i2 = this.q;
        if (i2 == 0 || i2 == 1) {
            d2 = this.c0.d();
        } else if (i2 != 2) {
            return 0;
        } else {
            d2 = this.c0.d() / 2.0f;
        }
        return (int) d2;
    }

    private void k() {
        if (l()) {
            ((CutoutDrawable) this.n).b();
        }
    }

    private boolean l() {
        return this.k && !TextUtils.isEmpty(this.l) && (this.n instanceof CutoutDrawable);
    }

    private void m() {
        Drawable background;
        int i2 = Build.VERSION.SDK_INT;
        if ((i2 == 21 || i2 == 22) && (background = this.b.getBackground()) != null && !this.f0) {
            Drawable newDrawable = background.getConstantState().newDrawable();
            if (background instanceof DrawableContainer) {
                this.f0 = DrawableUtils.a((DrawableContainer) background, newDrawable.getConstantState());
            }
            if (!this.f0) {
                ViewCompat.a((View) this.b, newDrawable);
                this.f0 = true;
                o();
            }
        }
    }

    private boolean n() {
        EditText editText = this.b;
        return editText != null && (editText.getTransformationMethod() instanceof PasswordTransformationMethod);
    }

    private void o() {
        g();
        if (this.q != 0) {
            t();
        }
        v();
    }

    private void p() {
        if (l()) {
            RectF rectF = this.D;
            this.c0.a(rectF);
            a(rectF);
            ((CutoutDrawable) this.n).a(rectF);
        }
    }

    private void q() {
        int i2 = this.q;
        if (i2 == 1) {
            this.w = 0;
        } else if (i2 == 2 && this.U == 0) {
            this.U = this.R.getColorForState(getDrawableState(), this.R.getDefaultColor());
        }
    }

    private boolean r() {
        return this.F && (n() || this.J);
    }

    private void s() {
        Drawable background;
        EditText editText = this.b;
        if (editText != null && (background = editText.getBackground()) != null) {
            if (androidx.appcompat.widget.DrawableUtils.a(background)) {
                background = background.mutate();
            }
            DescendantOffsetUtils.a((ViewGroup) this, (View) this.b, new Rect());
            Rect bounds = background.getBounds();
            if (bounds.left != bounds.right) {
                Rect rect = new Rect();
                background.getPadding(rect);
                background.setBounds(bounds.left - rect.left, bounds.top, bounds.right + (rect.right * 2), this.b.getBottom());
            }
        }
    }

    private void setEditText(EditText editText) {
        if (this.b == null) {
            if (!(editText instanceof TextInputEditText)) {
                Log.i("TextInputLayout", "EditText added is not a TextInputEditText. Please switch to using that class instead.");
            }
            this.b = editText;
            o();
            setTextInputAccessibilityDelegate(new AccessibilityDelegate(this));
            if (!n()) {
                this.c0.c(this.b.getTypeface());
            }
            this.c0.a(this.b.getTextSize());
            int gravity = this.b.getGravity();
            this.c0.b((gravity & -113) | 48);
            this.c0.d(gravity);
            this.b.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable editable) {
                    TextInputLayout textInputLayout = TextInputLayout.this;
                    textInputLayout.b(!textInputLayout.h0);
                    TextInputLayout textInputLayout2 = TextInputLayout.this;
                    if (textInputLayout2.e) {
                        textInputLayout2.a(editable.length());
                    }
                }

                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }
            });
            if (this.Q == null) {
                this.Q = this.b.getHintTextColors();
            }
            if (this.k) {
                if (TextUtils.isEmpty(this.l)) {
                    this.c = this.b.getHint();
                    setHint(this.c);
                    this.b.setHint((CharSequence) null);
                }
                this.m = true;
            }
            if (this.h != null) {
                a(this.b.getText().length());
            }
            this.d.a();
            u();
            a(false, true);
            return;
        }
        throw new IllegalArgumentException("We already have an EditText, can only have one");
    }

    private void setHintInternal(CharSequence charSequence) {
        if (!TextUtils.equals(charSequence, this.l)) {
            this.l = charSequence;
            this.c0.a(charSequence);
            if (!this.W) {
                p();
            }
        }
    }

    private void t() {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.f4306a.getLayoutParams();
        int j2 = j();
        if (j2 != layoutParams.topMargin) {
            layoutParams.topMargin = j2;
            this.f4306a.requestLayout();
        }
    }

    private void u() {
        if (this.b != null) {
            if (r()) {
                if (this.I == null) {
                    this.I = (CheckableImageButton) LayoutInflater.from(getContext()).inflate(R$layout.design_text_input_password_icon, this.f4306a, false);
                    this.I.setImageDrawable(this.G);
                    this.I.setContentDescription(this.H);
                    this.f4306a.addView(this.I);
                    this.I.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            TextInputLayout.this.a(false);
                        }
                    });
                }
                EditText editText = this.b;
                if (editText != null && ViewCompat.n(editText) <= 0) {
                    this.b.setMinimumHeight(ViewCompat.n(this.I));
                }
                this.I.setVisibility(0);
                this.I.setChecked(this.J);
                if (this.K == null) {
                    this.K = new ColorDrawable();
                }
                this.K.setBounds(0, 0, this.I.getMeasuredWidth(), 1);
                Drawable[] a2 = TextViewCompat.a((TextView) this.b);
                if (a2[2] != this.K) {
                    this.L = a2[2];
                }
                TextViewCompat.a(this.b, a2[0], a2[1], this.K, a2[3]);
                this.I.setPadding(this.b.getPaddingLeft(), this.b.getPaddingTop(), this.b.getPaddingRight(), this.b.getPaddingBottom());
                return;
            }
            CheckableImageButton checkableImageButton = this.I;
            if (checkableImageButton != null && checkableImageButton.getVisibility() == 0) {
                this.I.setVisibility(8);
            }
            if (this.K != null) {
                Drawable[] a3 = TextViewCompat.a((TextView) this.b);
                if (a3[2] == this.K) {
                    TextViewCompat.a(this.b, a3[0], a3[1], this.L, a3[3]);
                    this.K = null;
                }
            }
        }
    }

    private void v() {
        if (this.q != 0 && this.n != null && this.b != null && getRight() != 0) {
            int left = this.b.getLeft();
            int h2 = h();
            int right = this.b.getRight();
            int bottom = this.b.getBottom() + this.o;
            if (this.q == 2) {
                int i2 = this.y;
                left += i2 / 2;
                h2 -= i2 / 2;
                right -= i2 / 2;
                bottom += i2 / 2;
            }
            this.n.setBounds(left, h2, right, bottom);
            e();
            s();
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof EditText) {
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(layoutParams);
            layoutParams2.gravity = (layoutParams2.gravity & -113) | 16;
            this.f4306a.addView(view, layoutParams2);
            this.f4306a.setLayoutParams(layoutParams);
            t();
            setEditText((EditText) view);
            return;
        }
        super.addView(view, i2, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        a(z2, false);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable background;
        TextView textView;
        EditText editText = this.b;
        if (editText != null && (background = editText.getBackground()) != null) {
            m();
            if (androidx.appcompat.widget.DrawableUtils.a(background)) {
                background = background.mutate();
            }
            if (this.d.c()) {
                background.setColorFilter(AppCompatDrawableManager.a(this.d.e(), PorterDuff.Mode.SRC_IN));
            } else if (!this.g || (textView = this.h) == null) {
                DrawableCompat.b(background);
                this.b.refreshDrawableState();
            } else {
                background.setColorFilter(AppCompatDrawableManager.a(textView.getCurrentTextColor(), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        TextView textView;
        if (this.n != null && this.q != 0) {
            EditText editText = this.b;
            boolean z2 = true;
            boolean z3 = editText != null && editText.hasFocus();
            EditText editText2 = this.b;
            if (editText2 == null || !editText2.isHovered()) {
                z2 = false;
            }
            if (this.q == 2) {
                if (!isEnabled()) {
                    this.z = this.V;
                } else if (this.d.c()) {
                    this.z = this.d.e();
                } else if (this.g && (textView = this.h) != null) {
                    this.z = textView.getCurrentTextColor();
                } else if (z3) {
                    this.z = this.U;
                } else if (z2) {
                    this.z = this.T;
                } else {
                    this.z = this.S;
                }
                if ((z2 || z3) && isEnabled()) {
                    this.w = this.y;
                } else {
                    this.w = this.x;
                }
                e();
            }
        }
    }

    public void dispatchProvideAutofillStructure(ViewStructure viewStructure, int i2) {
        EditText editText;
        if (this.c == null || (editText = this.b) == null) {
            super.dispatchProvideAutofillStructure(viewStructure, i2);
            return;
        }
        boolean z2 = this.m;
        this.m = false;
        CharSequence hint = editText.getHint();
        this.b.setHint(this.c);
        try {
            super.dispatchProvideAutofillStructure(viewStructure, i2);
        } finally {
            this.b.setHint(hint);
            this.m = z2;
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        this.h0 = true;
        super.dispatchRestoreInstanceState(sparseArray);
        this.h0 = false;
    }

    public void draw(Canvas canvas) {
        GradientDrawable gradientDrawable = this.n;
        if (gradientDrawable != null) {
            gradientDrawable.draw(canvas);
        }
        super.draw(canvas);
        if (this.k) {
            this.c0.a(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        if (!this.g0) {
            boolean z2 = true;
            this.g0 = true;
            super.drawableStateChanged();
            int[] drawableState = getDrawableState();
            if (!ViewCompat.E(this) || !isEnabled()) {
                z2 = false;
            }
            b(z2);
            c();
            v();
            d();
            CollapsingTextHelper collapsingTextHelper = this.c0;
            if (collapsingTextHelper != null ? collapsingTextHelper.a(drawableState) | false : false) {
                invalidate();
            }
            this.g0 = false;
        }
    }

    public int getBoxBackgroundColor() {
        return this.A;
    }

    public float getBoxCornerRadiusBottomEnd() {
        return this.u;
    }

    public float getBoxCornerRadiusBottomStart() {
        return this.v;
    }

    public float getBoxCornerRadiusTopEnd() {
        return this.t;
    }

    public float getBoxCornerRadiusTopStart() {
        return this.s;
    }

    public int getBoxStrokeColor() {
        return this.U;
    }

    public int getCounterMaxLength() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public CharSequence getCounterOverflowDescription() {
        TextView textView;
        if (!this.e || !this.g || (textView = this.h) == null) {
            return null;
        }
        return textView.getContentDescription();
    }

    public ColorStateList getDefaultHintTextColor() {
        return this.Q;
    }

    public EditText getEditText() {
        return this.b;
    }

    public CharSequence getError() {
        if (this.d.k()) {
            return this.d.d();
        }
        return null;
    }

    public int getErrorCurrentTextColors() {
        return this.d.e();
    }

    /* access modifiers changed from: package-private */
    public final int getErrorTextCurrentColor() {
        return this.d.e();
    }

    public CharSequence getHelperText() {
        if (this.d.l()) {
            return this.d.g();
        }
        return null;
    }

    public int getHelperTextCurrentTextColor() {
        return this.d.h();
    }

    public CharSequence getHint() {
        if (this.k) {
            return this.l;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final float getHintCollapsedTextHeight() {
        return this.c0.d();
    }

    /* access modifiers changed from: package-private */
    public final int getHintCurrentCollapsedTextColor() {
        return this.c0.f();
    }

    public CharSequence getPasswordVisibilityToggleContentDescription() {
        return this.H;
    }

    public Drawable getPasswordVisibilityToggleDrawable() {
        return this.G;
    }

    public Typeface getTypeface() {
        return this.E;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        EditText editText;
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.n != null) {
            v();
        }
        if (this.k && (editText = this.b) != null) {
            Rect rect = this.C;
            DescendantOffsetUtils.a((ViewGroup) this, (View) editText, rect);
            int compoundPaddingLeft = rect.left + this.b.getCompoundPaddingLeft();
            int compoundPaddingRight = rect.right - this.b.getCompoundPaddingRight();
            int i6 = i();
            this.c0.b(compoundPaddingLeft, rect.top + this.b.getCompoundPaddingTop(), compoundPaddingRight, rect.bottom - this.b.getCompoundPaddingBottom());
            this.c0.a(compoundPaddingLeft, i6, compoundPaddingRight, (i5 - i3) - getPaddingBottom());
            this.c0.m();
            if (l() && !this.W) {
                p();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        u();
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        setError(savedState.c);
        if (savedState.d) {
            a(true);
        }
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (this.d.c()) {
            savedState.c = getError();
        }
        savedState.d = this.J;
        return savedState;
    }

    public void setBoxBackgroundColor(int i2) {
        if (this.A != i2) {
            this.A = i2;
            e();
        }
    }

    public void setBoxBackgroundColorResource(int i2) {
        setBoxBackgroundColor(ContextCompat.a(getContext(), i2));
    }

    public void setBoxBackgroundMode(int i2) {
        if (i2 != this.q) {
            this.q = i2;
            o();
        }
    }

    public void setBoxStrokeColor(int i2) {
        if (this.U != i2) {
            this.U = i2;
            d();
        }
    }

    public void setCounterEnabled(boolean z2) {
        if (this.e != z2) {
            if (z2) {
                this.h = new AppCompatTextView(getContext());
                this.h.setId(R$id.textinput_counter);
                Typeface typeface = this.E;
                if (typeface != null) {
                    this.h.setTypeface(typeface);
                }
                this.h.setMaxLines(1);
                a(this.h, this.j);
                this.d.a(this.h, 2);
                EditText editText = this.b;
                if (editText == null) {
                    a(0);
                } else {
                    a(editText.getText().length());
                }
            } else {
                this.d.b(this.h, 2);
                this.h = null;
            }
            this.e = z2;
        }
    }

    public void setCounterMaxLength(int i2) {
        if (this.f != i2) {
            if (i2 > 0) {
                this.f = i2;
            } else {
                this.f = -1;
            }
            if (this.e) {
                EditText editText = this.b;
                a(editText == null ? 0 : editText.getText().length());
            }
        }
    }

    public void setDefaultHintTextColor(ColorStateList colorStateList) {
        this.Q = colorStateList;
        this.R = colorStateList;
        if (this.b != null) {
            b(false);
        }
    }

    public void setEnabled(boolean z2) {
        a((ViewGroup) this, z2);
        super.setEnabled(z2);
    }

    public void setError(CharSequence charSequence) {
        if (!this.d.k()) {
            if (!TextUtils.isEmpty(charSequence)) {
                setErrorEnabled(true);
            } else {
                return;
            }
        }
        if (!TextUtils.isEmpty(charSequence)) {
            this.d.a(charSequence);
        } else {
            this.d.i();
        }
    }

    public void setErrorEnabled(boolean z2) {
        this.d.a(z2);
    }

    public void setErrorTextAppearance(int i2) {
        this.d.b(i2);
    }

    public void setErrorTextColor(ColorStateList colorStateList) {
        this.d.a(colorStateList);
    }

    public void setHelperText(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (!a()) {
                setHelperTextEnabled(true);
            }
            this.d.b(charSequence);
        } else if (a()) {
            setHelperTextEnabled(false);
        }
    }

    public void setHelperTextColor(ColorStateList colorStateList) {
        this.d.b(colorStateList);
    }

    public void setHelperTextEnabled(boolean z2) {
        this.d.b(z2);
    }

    public void setHelperTextTextAppearance(int i2) {
        this.d.c(i2);
    }

    public void setHint(CharSequence charSequence) {
        if (this.k) {
            setHintInternal(charSequence);
            sendAccessibilityEvent(2048);
        }
    }

    public void setHintAnimationEnabled(boolean z2) {
        this.d0 = z2;
    }

    public void setHintEnabled(boolean z2) {
        if (z2 != this.k) {
            this.k = z2;
            if (!this.k) {
                this.m = false;
                if (!TextUtils.isEmpty(this.l) && TextUtils.isEmpty(this.b.getHint())) {
                    this.b.setHint(this.l);
                }
                setHintInternal((CharSequence) null);
            } else {
                CharSequence hint = this.b.getHint();
                if (!TextUtils.isEmpty(hint)) {
                    if (TextUtils.isEmpty(this.l)) {
                        setHint(hint);
                    }
                    this.b.setHint((CharSequence) null);
                }
                this.m = true;
            }
            if (this.b != null) {
                t();
            }
        }
    }

    public void setHintTextAppearance(int i2) {
        this.c0.a(i2);
        this.R = this.c0.b();
        if (this.b != null) {
            b(false);
            t();
        }
    }

    public void setPasswordVisibilityToggleContentDescription(int i2) {
        setPasswordVisibilityToggleContentDescription(i2 != 0 ? getResources().getText(i2) : null);
    }

    public void setPasswordVisibilityToggleDrawable(int i2) {
        setPasswordVisibilityToggleDrawable(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void setPasswordVisibilityToggleEnabled(boolean z2) {
        EditText editText;
        if (this.F != z2) {
            this.F = z2;
            if (!z2 && this.J && (editText = this.b) != null) {
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
            this.J = false;
            u();
        }
    }

    public void setPasswordVisibilityToggleTintList(ColorStateList colorStateList) {
        this.M = colorStateList;
        this.N = true;
        f();
    }

    public void setPasswordVisibilityToggleTintMode(PorterDuff.Mode mode) {
        this.O = mode;
        this.P = true;
        f();
    }

    public void setTextInputAccessibilityDelegate(AccessibilityDelegate accessibilityDelegate) {
        EditText editText = this.b;
        if (editText != null) {
            ViewCompat.a((View) editText, (AccessibilityDelegateCompat) accessibilityDelegate);
        }
    }

    public void setTypeface(Typeface typeface) {
        if (typeface != this.E) {
            this.E = typeface;
            this.c0.c(typeface);
            this.d.a(typeface);
            TextView textView = this.h;
            if (textView != null) {
                textView.setTypeface(typeface);
            }
        }
    }

    public TextInputLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R$attr.textInputStyle);
    }

    private void a(boolean z2, boolean z3) {
        ColorStateList colorStateList;
        TextView textView;
        boolean isEnabled = isEnabled();
        EditText editText = this.b;
        boolean z4 = true;
        boolean z5 = editText != null && !TextUtils.isEmpty(editText.getText());
        EditText editText2 = this.b;
        if (editText2 == null || !editText2.hasFocus()) {
            z4 = false;
        }
        boolean c2 = this.d.c();
        ColorStateList colorStateList2 = this.Q;
        if (colorStateList2 != null) {
            this.c0.a(colorStateList2);
            this.c0.b(this.Q);
        }
        if (!isEnabled) {
            this.c0.a(ColorStateList.valueOf(this.V));
            this.c0.b(ColorStateList.valueOf(this.V));
        } else if (c2) {
            this.c0.a(this.d.f());
        } else if (this.g && (textView = this.h) != null) {
            this.c0.a(textView.getTextColors());
        } else if (z4 && (colorStateList = this.R) != null) {
            this.c0.a(colorStateList);
        }
        if (z5 || (isEnabled() && (z4 || c2))) {
            if (z3 || this.W) {
                c(z2);
            }
        } else if (z3 || !this.W) {
            d(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.m;
    }

    public TextInputLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = new IndicatorViewController(this);
        this.C = new Rect();
        this.D = new RectF();
        this.c0 = new CollapsingTextHelper(this);
        setOrientation(1);
        setWillNotDraw(false);
        setAddStatesFromChildren(true);
        this.f4306a = new FrameLayout(context);
        this.f4306a.setAddStatesFromChildren(true);
        addView(this.f4306a);
        this.c0.b(AnimationUtils.f4167a);
        this.c0.a(AnimationUtils.f4167a);
        this.c0.b(8388659);
        TintTypedArray d2 = ThemeEnforcement.d(context, attributeSet, R$styleable.TextInputLayout, i2, R$style.Widget_Design_TextInputLayout, new int[0]);
        this.k = d2.a(R$styleable.TextInputLayout_hintEnabled, true);
        setHint(d2.e(R$styleable.TextInputLayout_android_hint));
        this.d0 = d2.a(R$styleable.TextInputLayout_hintAnimationEnabled, true);
        this.o = context.getResources().getDimensionPixelOffset(R$dimen.mtrl_textinput_box_bottom_offset);
        this.p = context.getResources().getDimensionPixelOffset(R$dimen.mtrl_textinput_box_label_cutout_padding);
        this.r = d2.b(R$styleable.TextInputLayout_boxCollapsedPaddingTop, 0);
        this.s = d2.a(R$styleable.TextInputLayout_boxCornerRadiusTopStart, 0.0f);
        this.t = d2.a(R$styleable.TextInputLayout_boxCornerRadiusTopEnd, 0.0f);
        this.u = d2.a(R$styleable.TextInputLayout_boxCornerRadiusBottomEnd, 0.0f);
        this.v = d2.a(R$styleable.TextInputLayout_boxCornerRadiusBottomStart, 0.0f);
        this.A = d2.a(R$styleable.TextInputLayout_boxBackgroundColor, 0);
        this.U = d2.a(R$styleable.TextInputLayout_boxStrokeColor, 0);
        this.x = context.getResources().getDimensionPixelSize(R$dimen.mtrl_textinput_box_stroke_width_default);
        this.y = context.getResources().getDimensionPixelSize(R$dimen.mtrl_textinput_box_stroke_width_focused);
        this.w = this.x;
        setBoxBackgroundMode(d2.d(R$styleable.TextInputLayout_boxBackgroundMode, 0));
        if (d2.g(R$styleable.TextInputLayout_android_textColorHint)) {
            ColorStateList a2 = d2.a(R$styleable.TextInputLayout_android_textColorHint);
            this.R = a2;
            this.Q = a2;
        }
        this.S = ContextCompat.a(context, R$color.mtrl_textinput_default_box_stroke_color);
        this.V = ContextCompat.a(context, R$color.mtrl_textinput_disabled_color);
        this.T = ContextCompat.a(context, R$color.mtrl_textinput_hovered_box_stroke_color);
        if (d2.g(R$styleable.TextInputLayout_hintTextAppearance, -1) != -1) {
            setHintTextAppearance(d2.g(R$styleable.TextInputLayout_hintTextAppearance, 0));
        }
        int g2 = d2.g(R$styleable.TextInputLayout_errorTextAppearance, 0);
        boolean a3 = d2.a(R$styleable.TextInputLayout_errorEnabled, false);
        int g3 = d2.g(R$styleable.TextInputLayout_helperTextTextAppearance, 0);
        boolean a4 = d2.a(R$styleable.TextInputLayout_helperTextEnabled, false);
        CharSequence e2 = d2.e(R$styleable.TextInputLayout_helperText);
        boolean a5 = d2.a(R$styleable.TextInputLayout_counterEnabled, false);
        setCounterMaxLength(d2.d(R$styleable.TextInputLayout_counterMaxLength, -1));
        this.j = d2.g(R$styleable.TextInputLayout_counterTextAppearance, 0);
        this.i = d2.g(R$styleable.TextInputLayout_counterOverflowTextAppearance, 0);
        this.F = d2.a(R$styleable.TextInputLayout_passwordToggleEnabled, false);
        this.G = d2.b(R$styleable.TextInputLayout_passwordToggleDrawable);
        this.H = d2.e(R$styleable.TextInputLayout_passwordToggleContentDescription);
        if (d2.g(R$styleable.TextInputLayout_passwordToggleTint)) {
            this.N = true;
            this.M = d2.a(R$styleable.TextInputLayout_passwordToggleTint);
        }
        if (d2.g(R$styleable.TextInputLayout_passwordToggleTintMode)) {
            this.P = true;
            this.O = ViewUtils.a(d2.d(R$styleable.TextInputLayout_passwordToggleTintMode, -1), (PorterDuff.Mode) null);
        }
        d2.a();
        setHelperTextEnabled(a4);
        setHelperText(e2);
        setHelperTextTextAppearance(g3);
        setErrorEnabled(a3);
        setErrorTextAppearance(g2);
        setCounterEnabled(a5);
        f();
        ViewCompat.h(this, 2);
    }

    public void setPasswordVisibilityToggleContentDescription(CharSequence charSequence) {
        this.H = charSequence;
        CheckableImageButton checkableImageButton = this.I;
        if (checkableImageButton != null) {
            checkableImageButton.setContentDescription(charSequence);
        }
    }

    public void setPasswordVisibilityToggleDrawable(Drawable drawable) {
        this.G = drawable;
        CheckableImageButton checkableImageButton = this.I;
        if (checkableImageButton != null) {
            checkableImageButton.setImageDrawable(drawable);
        }
    }

    private void c(boolean z2) {
        ValueAnimator valueAnimator = this.e0;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.e0.cancel();
        }
        if (!z2 || !this.d0) {
            this.c0.b(1.0f);
        } else {
            a(1.0f);
        }
        this.W = false;
        if (l()) {
            p();
        }
    }

    private void d(boolean z2) {
        ValueAnimator valueAnimator = this.e0;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.e0.cancel();
        }
        if (!z2 || !this.d0) {
            this.c0.b(0.0f);
        } else {
            a(0.0f);
        }
        if (l() && ((CutoutDrawable) this.n).a()) {
            k();
        }
        this.W = true;
    }

    public boolean a() {
        return this.d.l();
    }

    private static void a(ViewGroup viewGroup, boolean z2) {
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            childAt.setEnabled(z2);
            if (childAt instanceof ViewGroup) {
                a((ViewGroup) childAt, z2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        boolean z2 = this.g;
        if (this.f == -1) {
            this.h.setText(String.valueOf(i2));
            this.h.setContentDescription((CharSequence) null);
            this.g = false;
        } else {
            if (ViewCompat.c(this.h) == 1) {
                ViewCompat.g(this.h, 0);
            }
            this.g = i2 > this.f;
            boolean z3 = this.g;
            if (z2 != z3) {
                a(this.h, z3 ? this.i : this.j);
                if (this.g) {
                    ViewCompat.g(this.h, 1);
                }
            }
            this.h.setText(getContext().getString(R$string.character_counter_pattern, new Object[]{Integer.valueOf(i2), Integer.valueOf(this.f)}));
            this.h.setContentDescription(getContext().getString(R$string.character_counter_content_description, new Object[]{Integer.valueOf(i2), Integer.valueOf(this.f)}));
        }
        if (this.b != null && z2 != this.g) {
            b(false);
            d();
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(TextView textView, int i2) {
        boolean z2 = true;
        try {
            TextViewCompat.d(textView, i2);
            if (Build.VERSION.SDK_INT < 23 || textView.getTextColors().getDefaultColor() != -65281) {
                z2 = false;
            }
        } catch (Exception unused) {
        }
        if (z2) {
            TextViewCompat.d(textView, R$style.TextAppearance_AppCompat_Caption);
            textView.setTextColor(ContextCompat.a(getContext(), R$color.design_error));
        }
    }

    public void a(boolean z2) {
        if (this.F) {
            int selectionEnd = this.b.getSelectionEnd();
            if (n()) {
                this.b.setTransformationMethod((TransformationMethod) null);
                this.J = true;
            } else {
                this.b.setTransformationMethod(PasswordTransformationMethod.getInstance());
                this.J = false;
            }
            this.I.setChecked(this.J);
            if (z2) {
                this.I.jumpDrawablesToCurrentState();
            }
            this.b.setSelection(selectionEnd);
        }
    }

    private void a(RectF rectF) {
        float f2 = rectF.left;
        int i2 = this.p;
        rectF.left = f2 - ((float) i2);
        rectF.top -= (float) i2;
        rectF.right += (float) i2;
        rectF.bottom += (float) i2;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        if (this.c0.i() != f2) {
            if (this.e0 == null) {
                this.e0 = new ValueAnimator();
                this.e0.setInterpolator(AnimationUtils.b);
                this.e0.setDuration(167);
                this.e0.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        TextInputLayout.this.c0.b(((Float) valueAnimator.getAnimatedValue()).floatValue());
                    }
                });
            }
            this.e0.setFloatValues(new float[]{this.c0.i(), f2});
            this.e0.start();
        }
    }
}
