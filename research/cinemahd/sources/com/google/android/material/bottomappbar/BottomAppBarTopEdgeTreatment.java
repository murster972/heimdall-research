package com.google.android.material.bottomappbar;

import com.google.android.material.shape.EdgeTreatment;
import com.google.android.material.shape.ShapePath;

public class BottomAppBarTopEdgeTreatment extends EdgeTreatment {

    /* renamed from: a  reason: collision with root package name */
    private float f4201a;
    private float b;
    private float c;
    private float d;
    private float e;

    public BottomAppBarTopEdgeTreatment(float f, float f2, float f3) {
        this.b = f;
        this.f4201a = f2;
        this.d = f3;
        if (f3 >= 0.0f) {
            this.e = 0.0f;
            return;
        }
        throw new IllegalArgumentException("cradleVerticalOffset must be positive.");
    }

    public void a(float f, float f2, ShapePath shapePath) {
        float f3 = f;
        ShapePath shapePath2 = shapePath;
        float f4 = this.c;
        if (f4 == 0.0f) {
            shapePath2.a(f3, 0.0f);
            return;
        }
        float f5 = ((this.b * 2.0f) + f4) / 2.0f;
        float f6 = f2 * this.f4201a;
        float f7 = (f3 / 2.0f) + this.e;
        float f8 = (this.d * f2) + ((1.0f - f2) * f5);
        if (f8 / f5 >= 1.0f) {
            shapePath2.a(f3, 0.0f);
            return;
        }
        float f9 = f5 + f6;
        float f10 = f8 + f6;
        float sqrt = (float) Math.sqrt((double) ((f9 * f9) - (f10 * f10)));
        float f11 = f7 - sqrt;
        float f12 = f7 + sqrt;
        float degrees = (float) Math.toDegrees(Math.atan((double) (sqrt / f10)));
        float f13 = 90.0f - degrees;
        float f14 = f11 - f6;
        shapePath2.a(f14, 0.0f);
        float f15 = f6 * 2.0f;
        float f16 = degrees;
        shapePath.a(f14, 0.0f, f11 + f6, f15, 270.0f, degrees);
        shapePath.a(f7 - f5, (-f5) - f8, f7 + f5, f5 - f8, 180.0f - f13, (f13 * 2.0f) - 180.0f);
        shapePath.a(f12 - f6, 0.0f, f12 + f6, f15, 270.0f - f16, f16);
        shapePath2.a(f3, 0.0f);
    }

    /* access modifiers changed from: package-private */
    public float b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.f4201a;
    }

    /* access modifiers changed from: package-private */
    public float d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void e(float f) {
        this.e = f;
    }

    /* access modifiers changed from: package-private */
    public void b(float f) {
        this.b = f;
    }

    /* access modifiers changed from: package-private */
    public void c(float f) {
        this.f4201a = f;
    }

    /* access modifiers changed from: package-private */
    public void d(float f) {
        this.c = f;
    }

    /* access modifiers changed from: package-private */
    public float e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public float a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void a(float f) {
        this.d = f;
    }
}
