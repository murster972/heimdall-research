package com.google.android.material.bottomappbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ViewCompat;
import androidx.customview.view.AbsSavedState;
import com.google.android.material.R$dimen;
import com.google.android.material.R$style;
import com.google.android.material.R$styleable;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.resources.MaterialResources;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapePathModel;
import java.util.ArrayList;
import java.util.List;

public class BottomAppBar extends Toolbar implements CoordinatorLayout.AttachedBehavior {
    private final int P;
    /* access modifiers changed from: private */
    public final MaterialShapeDrawable Q;
    /* access modifiers changed from: private */
    public final BottomAppBarTopEdgeTreatment R;
    /* access modifiers changed from: private */
    public Animator S;
    /* access modifiers changed from: private */
    public Animator T;
    /* access modifiers changed from: private */
    public Animator U;
    /* access modifiers changed from: private */
    public int V;
    private boolean W;
    /* access modifiers changed from: private */
    public boolean c0 = true;
    AnimatorListenerAdapter d0 = new AnimatorListenerAdapter() {
        public void onAnimationStart(Animator animator) {
            BottomAppBar bottomAppBar = BottomAppBar.this;
            bottomAppBar.b(bottomAppBar.c0);
            BottomAppBar bottomAppBar2 = BottomAppBar.this;
            bottomAppBar2.a(bottomAppBar2.V, BottomAppBar.this.c0);
        }
    };

    public static class Behavior extends HideBottomViewOnScrollBehavior<BottomAppBar> {
        private final Rect d = new Rect();

        public Behavior() {
        }

        private boolean a(FloatingActionButton floatingActionButton, BottomAppBar bottomAppBar) {
            ((CoordinatorLayout.LayoutParams) floatingActionButton.getLayoutParams()).d = 17;
            bottomAppBar.a(floatingActionButton);
            return true;
        }

        /* access modifiers changed from: protected */
        public void b(BottomAppBar bottomAppBar) {
            super.b(bottomAppBar);
            FloatingActionButton b = bottomAppBar.m();
            if (b != null) {
                b.clearAnimation();
                b.animate().translationY(bottomAppBar.getFabTranslationY()).setInterpolator(AnimationUtils.d).setDuration(225);
            }
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        /* renamed from: a */
        public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, BottomAppBar bottomAppBar, int i) {
            FloatingActionButton b = bottomAppBar.m();
            if (b != null) {
                a(b, bottomAppBar);
                b.b(this.d);
                bottomAppBar.setFabDiameter(this.d.height());
            }
            if (!bottomAppBar.n()) {
                bottomAppBar.p();
            }
            coordinatorLayout.c((View) bottomAppBar, i);
            return super.onLayoutChild(coordinatorLayout, bottomAppBar, i);
        }

        /* renamed from: a */
        public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, BottomAppBar bottomAppBar, View view, View view2, int i, int i2) {
            return bottomAppBar.getHideOnScroll() && super.onStartNestedScroll(coordinatorLayout, bottomAppBar, view, view2, i, i2);
        }

        /* access modifiers changed from: protected */
        public void a(BottomAppBar bottomAppBar) {
            super.a(bottomAppBar);
            FloatingActionButton b = bottomAppBar.m();
            if (b != null) {
                b.a(this.d);
                b.clearAnimation();
                b.animate().translationY(((float) (-b.getPaddingBottom())) + ((float) (b.getMeasuredHeight() - this.d.height()))).setInterpolator(AnimationUtils.c).setDuration(175);
            }
        }
    }

    static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, (ClassLoader) null);
            }
        };
        int c;
        boolean d;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.c);
            parcel.writeInt(this.d ? 1 : 0);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.c = parcel.readInt();
            this.d = parcel.readInt() != 0;
        }
    }

    public BottomAppBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray c = ThemeEnforcement.c(context, attributeSet, R$styleable.BottomAppBar, i, R$style.Widget_MaterialComponents_BottomAppBar, new int[0]);
        ColorStateList a2 = MaterialResources.a(context, c, R$styleable.BottomAppBar_backgroundTint);
        this.V = c.getInt(R$styleable.BottomAppBar_fabAlignmentMode, 0);
        this.W = c.getBoolean(R$styleable.BottomAppBar_hideOnScroll, false);
        c.recycle();
        this.P = getResources().getDimensionPixelOffset(R$dimen.mtrl_bottomappbar_fabOffsetEndMode);
        this.R = new BottomAppBarTopEdgeTreatment((float) c.getDimensionPixelOffset(R$styleable.BottomAppBar_fabCradleMargin, 0), (float) c.getDimensionPixelOffset(R$styleable.BottomAppBar_fabCradleRoundedCornerRadius, 0), (float) c.getDimensionPixelOffset(R$styleable.BottomAppBar_fabCradleVerticalOffset, 0));
        ShapePathModel shapePathModel = new ShapePathModel();
        shapePathModel.a(this.R);
        this.Q = new MaterialShapeDrawable(shapePathModel);
        this.Q.a(true);
        this.Q.a(Paint.Style.FILL);
        DrawableCompat.a((Drawable) this.Q, a2);
        ViewCompat.a((View) this, (Drawable) this.Q);
    }

    private ActionMenuView getActionMenuView() {
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof ActionMenuView) {
                return (ActionMenuView) childAt;
            }
        }
        return null;
    }

    private float getFabTranslationX() {
        return (float) b(this.V);
    }

    /* access modifiers changed from: private */
    public float getFabTranslationY() {
        return a(this.c0);
    }

    private void l() {
        Animator animator = this.S;
        if (animator != null) {
            animator.cancel();
        }
        Animator animator2 = this.U;
        if (animator2 != null) {
            animator2.cancel();
        }
        Animator animator3 = this.T;
        if (animator3 != null) {
            animator3.cancel();
        }
    }

    /* access modifiers changed from: private */
    public FloatingActionButton m() {
        if (!(getParent() instanceof CoordinatorLayout)) {
            return null;
        }
        for (View next : ((CoordinatorLayout) getParent()).c((View) this)) {
            if (next instanceof FloatingActionButton) {
                return (FloatingActionButton) next;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r0 = r1.U;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        r0 = r1.T;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean n() {
        /*
            r1 = this;
            android.animation.Animator r0 = r1.S
            if (r0 == 0) goto L_0x000a
            boolean r0 = r0.isRunning()
            if (r0 != 0) goto L_0x001e
        L_0x000a:
            android.animation.Animator r0 = r1.U
            if (r0 == 0) goto L_0x0014
            boolean r0 = r0.isRunning()
            if (r0 != 0) goto L_0x001e
        L_0x0014:
            android.animation.Animator r0 = r1.T
            if (r0 == 0) goto L_0x0020
            boolean r0 = r0.isRunning()
            if (r0 == 0) goto L_0x0020
        L_0x001e:
            r0 = 1
            goto L_0x0021
        L_0x0020:
            r0 = 0
        L_0x0021:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomappbar.BottomAppBar.n():boolean");
    }

    private boolean o() {
        FloatingActionButton m = m();
        return m != null && m.b();
    }

    /* access modifiers changed from: private */
    public void p() {
        this.R.e(getFabTranslationX());
        FloatingActionButton m = m();
        this.Q.a((!this.c0 || !o()) ? 0.0f : 1.0f);
        if (m != null) {
            m.setTranslationY(getFabTranslationY());
            m.setTranslationX(getFabTranslationX());
        }
        ActionMenuView actionMenuView = getActionMenuView();
        if (actionMenuView != null) {
            actionMenuView.setAlpha(1.0f);
            if (!o()) {
                a(actionMenuView, 0, false);
            } else {
                a(actionMenuView, this.V, this.c0);
            }
        }
    }

    public ColorStateList getBackgroundTint() {
        return this.Q.b();
    }

    public CoordinatorLayout.Behavior<BottomAppBar> getBehavior() {
        return new Behavior();
    }

    public float getCradleVerticalOffset() {
        return this.R.a();
    }

    public int getFabAlignmentMode() {
        return this.V;
    }

    public float getFabCradleMargin() {
        return this.R.b();
    }

    public float getFabCradleRoundedCornerRadius() {
        return this.R.c();
    }

    public boolean getHideOnScroll() {
        return this.W;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        l();
        p();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        this.V = savedState.c;
        this.c0 = savedState.d;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.c = this.V;
        savedState.d = this.c0;
        return savedState;
    }

    public void setBackgroundTint(ColorStateList colorStateList) {
        DrawableCompat.a((Drawable) this.Q, colorStateList);
    }

    public void setCradleVerticalOffset(float f) {
        if (f != getCradleVerticalOffset()) {
            this.R.a(f);
            this.Q.invalidateSelf();
        }
    }

    public void setFabAlignmentMode(int i) {
        c(i);
        a(i, this.c0);
        this.V = i;
    }

    public void setFabCradleMargin(float f) {
        if (f != getFabCradleMargin()) {
            this.R.b(f);
            this.Q.invalidateSelf();
        }
    }

    public void setFabCradleRoundedCornerRadius(float f) {
        if (f != getFabCradleRoundedCornerRadius()) {
            this.R.c(f);
            this.Q.invalidateSelf();
        }
    }

    /* access modifiers changed from: package-private */
    public void setFabDiameter(int i) {
        float f = (float) i;
        if (f != this.R.d()) {
            this.R.d(f);
            this.Q.invalidateSelf();
        }
    }

    public void setHideOnScroll(boolean z) {
        this.W = z;
    }

    public void setSubtitle(CharSequence charSequence) {
    }

    public void setTitle(CharSequence charSequence) {
    }

    private void b(int i, List<Animator> list) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(m(), "translationX", new float[]{(float) b(i)});
        ofFloat.setDuration(300);
        list.add(ofFloat);
    }

    private void c(int i) {
        if (this.V != i && ViewCompat.E(this)) {
            Animator animator = this.T;
            if (animator != null) {
                animator.cancel();
            }
            ArrayList arrayList = new ArrayList();
            a(i, (List<Animator>) arrayList);
            b(i, (List<Animator>) arrayList);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(arrayList);
            this.T = animatorSet;
            this.T.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    Animator unused = BottomAppBar.this.T = null;
                }
            });
            this.T.start();
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (ViewCompat.E(this)) {
            Animator animator = this.S;
            if (animator != null) {
                animator.cancel();
            }
            ArrayList arrayList = new ArrayList();
            a(z && o(), (List<Animator>) arrayList);
            b(z, (List<Animator>) arrayList);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(arrayList);
            this.S = animatorSet;
            this.S.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    Animator unused = BottomAppBar.this.S = null;
                }
            });
            this.S.start();
        }
    }

    private void a(int i, List<Animator> list) {
        if (this.c0) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{this.R.e(), (float) b(i)});
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    BottomAppBar.this.R.e(((Float) valueAnimator.getAnimatedValue()).floatValue());
                    BottomAppBar.this.Q.invalidateSelf();
                }
            });
            ofFloat.setDuration(300);
            list.add(ofFloat);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, boolean z) {
        if (ViewCompat.E(this)) {
            Animator animator = this.U;
            if (animator != null) {
                animator.cancel();
            }
            ArrayList arrayList = new ArrayList();
            if (!o()) {
                i = 0;
                z = false;
            }
            a(i, z, (List<Animator>) arrayList);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(arrayList);
            this.U = animatorSet;
            this.U.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    Animator unused = BottomAppBar.this.U = null;
                }
            });
            this.U.start();
        }
    }

    private void b(boolean z, List<Animator> list) {
        FloatingActionButton m = m();
        if (m != null) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(m, "translationY", new float[]{a(z)});
            ofFloat.setDuration(300);
            list.add(ofFloat);
        }
    }

    private int b(int i) {
        int i2 = 1;
        boolean z = ViewCompat.m(this) == 1;
        if (i != 1) {
            return 0;
        }
        int measuredWidth = (getMeasuredWidth() / 2) - this.P;
        if (z) {
            i2 = -1;
        }
        return measuredWidth * i2;
    }

    private void b(FloatingActionButton floatingActionButton) {
        floatingActionButton.c((Animator.AnimatorListener) this.d0);
        floatingActionButton.d(this.d0);
    }

    private void a(final int i, final boolean z, List<Animator> list) {
        final ActionMenuView actionMenuView = getActionMenuView();
        if (actionMenuView != null) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(actionMenuView, "alpha", new float[]{1.0f});
            if ((this.c0 || (z && o())) && (this.V == 1 || i == 1)) {
                ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(actionMenuView, "alpha", new float[]{0.0f});
                ofFloat2.addListener(new AnimatorListenerAdapter() {

                    /* renamed from: a  reason: collision with root package name */
                    public boolean f4197a;

                    public void onAnimationCancel(Animator animator) {
                        this.f4197a = true;
                    }

                    public void onAnimationEnd(Animator animator) {
                        if (!this.f4197a) {
                            BottomAppBar.this.a(actionMenuView, i, z);
                        }
                    }
                });
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.setDuration(150);
                animatorSet.playSequentially(new Animator[]{ofFloat2, ofFloat});
                list.add(animatorSet);
            } else if (actionMenuView.getAlpha() < 1.0f) {
                list.add(ofFloat);
            }
        }
    }

    private void a(boolean z, List<Animator> list) {
        if (z) {
            this.R.e(getFabTranslationX());
        }
        float[] fArr = new float[2];
        fArr[0] = this.Q.a();
        fArr[1] = z ? 1.0f : 0.0f;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                BottomAppBar.this.Q.a(((Float) valueAnimator.getAnimatedValue()).floatValue());
            }
        });
        ofFloat.setDuration(300);
        list.add(ofFloat);
    }

    private float a(boolean z) {
        FloatingActionButton m = m();
        if (m == null) {
            return 0.0f;
        }
        Rect rect = new Rect();
        m.a(rect);
        float height = (float) rect.height();
        if (height == 0.0f) {
            height = (float) m.getMeasuredHeight();
        }
        float height2 = (float) (m.getHeight() - rect.height());
        float height3 = (-getCradleVerticalOffset()) + (height / 2.0f) + ((float) (m.getHeight() - rect.bottom));
        float paddingBottom = height2 - ((float) m.getPaddingBottom());
        float f = (float) (-getMeasuredHeight());
        if (z) {
            paddingBottom = height3;
        }
        return f + paddingBottom;
    }

    /* access modifiers changed from: private */
    public void a(ActionMenuView actionMenuView, int i, boolean z) {
        boolean z2 = ViewCompat.m(this) == 1;
        int i2 = 0;
        for (int i3 = 0; i3 < getChildCount(); i3++) {
            View childAt = getChildAt(i3);
            if ((childAt.getLayoutParams() instanceof Toolbar.LayoutParams) && (((Toolbar.LayoutParams) childAt.getLayoutParams()).f235a & 8388615) == 8388611) {
                i2 = Math.max(i2, z2 ? childAt.getLeft() : childAt.getRight());
            }
        }
        actionMenuView.setTranslationX((i != 1 || !z) ? 0.0f : (float) (i2 - (z2 ? actionMenuView.getRight() : actionMenuView.getLeft())));
    }

    /* access modifiers changed from: private */
    public void a(FloatingActionButton floatingActionButton) {
        b(floatingActionButton);
        floatingActionButton.a((Animator.AnimatorListener) this.d0);
        floatingActionButton.b((Animator.AnimatorListener) this.d0);
    }
}
