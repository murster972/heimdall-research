package com.google.android.material.bottomnavigation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.widget.TintTypedArray;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.customview.view.AbsSavedState;
import com.google.android.material.R$color;
import com.google.android.material.R$dimen;
import com.google.android.material.R$style;
import com.google.android.material.R$styleable;
import com.google.android.material.internal.ThemeEnforcement;

public class BottomNavigationView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private final MenuBuilder f4207a;
    private final BottomNavigationMenuView b;
    private final BottomNavigationPresenter c = new BottomNavigationPresenter();
    private MenuInflater d;
    /* access modifiers changed from: private */
    public OnNavigationItemSelectedListener e;
    /* access modifiers changed from: private */
    public OnNavigationItemReselectedListener f;

    public interface OnNavigationItemReselectedListener {
        void a(MenuItem menuItem);
    }

    public interface OnNavigationItemSelectedListener {
        boolean a(MenuItem menuItem);
    }

    static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, (ClassLoader) null);
            }
        };
        Bundle c;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private void a(Parcel parcel, ClassLoader classLoader) {
            this.c = parcel.readBundle(classLoader);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.c);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            a(parcel, classLoader);
        }
    }

    public BottomNavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f4207a = new BottomNavigationMenu(context);
        this.b = new BottomNavigationMenuView(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        this.b.setLayoutParams(layoutParams);
        this.c.a(this.b);
        this.c.a(1);
        this.b.setPresenter(this.c);
        this.f4207a.a((MenuPresenter) this.c);
        this.c.a(getContext(), this.f4207a);
        TintTypedArray d2 = ThemeEnforcement.d(context, attributeSet, R$styleable.BottomNavigationView, i, R$style.Widget_Design_BottomNavigationView, R$styleable.BottomNavigationView_itemTextAppearanceInactive, R$styleable.BottomNavigationView_itemTextAppearanceActive);
        if (d2.g(R$styleable.BottomNavigationView_itemIconTint)) {
            this.b.setIconTintList(d2.a(R$styleable.BottomNavigationView_itemIconTint));
        } else {
            BottomNavigationMenuView bottomNavigationMenuView = this.b;
            bottomNavigationMenuView.setIconTintList(bottomNavigationMenuView.a(16842808));
        }
        setItemIconSize(d2.c(R$styleable.BottomNavigationView_itemIconSize, getResources().getDimensionPixelSize(R$dimen.design_bottom_navigation_icon_size)));
        if (d2.g(R$styleable.BottomNavigationView_itemTextAppearanceInactive)) {
            setItemTextAppearanceInactive(d2.g(R$styleable.BottomNavigationView_itemTextAppearanceInactive, 0));
        }
        if (d2.g(R$styleable.BottomNavigationView_itemTextAppearanceActive)) {
            setItemTextAppearanceActive(d2.g(R$styleable.BottomNavigationView_itemTextAppearanceActive, 0));
        }
        if (d2.g(R$styleable.BottomNavigationView_itemTextColor)) {
            setItemTextColor(d2.a(R$styleable.BottomNavigationView_itemTextColor));
        }
        if (d2.g(R$styleable.BottomNavigationView_elevation)) {
            ViewCompat.b((View) this, (float) d2.c(R$styleable.BottomNavigationView_elevation, 0));
        }
        setLabelVisibilityMode(d2.e(R$styleable.BottomNavigationView_labelVisibilityMode, -1));
        setItemHorizontalTranslationEnabled(d2.a(R$styleable.BottomNavigationView_itemHorizontalTranslationEnabled, true));
        this.b.setItemBackgroundRes(d2.g(R$styleable.BottomNavigationView_itemBackground, 0));
        if (d2.g(R$styleable.BottomNavigationView_menu)) {
            a(d2.g(R$styleable.BottomNavigationView_menu, 0));
        }
        d2.a();
        addView(this.b, layoutParams);
        if (Build.VERSION.SDK_INT < 21) {
            a(context);
        }
        this.f4207a.a((MenuBuilder.Callback) new MenuBuilder.Callback() {
            public void a(MenuBuilder menuBuilder) {
            }

            public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
                if (BottomNavigationView.this.f != null && menuItem.getItemId() == BottomNavigationView.this.getSelectedItemId()) {
                    BottomNavigationView.this.f.a(menuItem);
                    return true;
                } else if (BottomNavigationView.this.e == null || BottomNavigationView.this.e.a(menuItem)) {
                    return false;
                } else {
                    return true;
                }
            }
        });
    }

    private MenuInflater getMenuInflater() {
        if (this.d == null) {
            this.d = new SupportMenuInflater(getContext());
        }
        return this.d;
    }

    public Drawable getItemBackground() {
        return this.b.getItemBackground();
    }

    @Deprecated
    public int getItemBackgroundResource() {
        return this.b.getItemBackgroundRes();
    }

    public int getItemIconSize() {
        return this.b.getItemIconSize();
    }

    public ColorStateList getItemIconTintList() {
        return this.b.getIconTintList();
    }

    public int getItemTextAppearanceActive() {
        return this.b.getItemTextAppearanceActive();
    }

    public int getItemTextAppearanceInactive() {
        return this.b.getItemTextAppearanceInactive();
    }

    public ColorStateList getItemTextColor() {
        return this.b.getItemTextColor();
    }

    public int getLabelVisibilityMode() {
        return this.b.getLabelVisibilityMode();
    }

    public int getMaxItemCount() {
        return 5;
    }

    public Menu getMenu() {
        return this.f4207a;
    }

    public int getSelectedItemId() {
        return this.b.getSelectedItemId();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        this.f4207a.b(savedState.c);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.c = new Bundle();
        this.f4207a.d(savedState.c);
        return savedState;
    }

    public void setItemBackground(Drawable drawable) {
        this.b.setItemBackground(drawable);
    }

    public void setItemBackgroundResource(int i) {
        this.b.setItemBackgroundRes(i);
    }

    public void setItemHorizontalTranslationEnabled(boolean z) {
        if (this.b.b() != z) {
            this.b.setItemHorizontalTranslationEnabled(z);
            this.c.a(false);
        }
    }

    public void setItemIconSize(int i) {
        this.b.setItemIconSize(i);
    }

    public void setItemIconSizeRes(int i) {
        setItemIconSize(getResources().getDimensionPixelSize(i));
    }

    public void setItemIconTintList(ColorStateList colorStateList) {
        this.b.setIconTintList(colorStateList);
    }

    public void setItemTextAppearanceActive(int i) {
        this.b.setItemTextAppearanceActive(i);
    }

    public void setItemTextAppearanceInactive(int i) {
        this.b.setItemTextAppearanceInactive(i);
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.b.setItemTextColor(colorStateList);
    }

    public void setLabelVisibilityMode(int i) {
        if (this.b.getLabelVisibilityMode() != i) {
            this.b.setLabelVisibilityMode(i);
            this.c.a(false);
        }
    }

    public void setOnNavigationItemReselectedListener(OnNavigationItemReselectedListener onNavigationItemReselectedListener) {
        this.f = onNavigationItemReselectedListener;
    }

    public void setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener onNavigationItemSelectedListener) {
        this.e = onNavigationItemSelectedListener;
    }

    public void setSelectedItemId(int i) {
        MenuItem findItem = this.f4207a.findItem(i);
        if (findItem != null && !this.f4207a.a(findItem, (MenuPresenter) this.c, 0)) {
            findItem.setChecked(true);
        }
    }

    public void a(int i) {
        this.c.b(true);
        getMenuInflater().inflate(i, this.f4207a);
        this.c.b(false);
        this.c.a(true);
    }

    private void a(Context context) {
        View view = new View(context);
        view.setBackgroundColor(ContextCompat.a(context, R$color.design_bottom_navigation_shadow_color));
        view.setLayoutParams(new FrameLayout.LayoutParams(-1, getResources().getDimensionPixelSize(R$dimen.design_bottom_navigation_shadow_height)));
        addView(view);
    }
}
