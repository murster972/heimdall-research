package com.google.android.material.bottomnavigation;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.SubMenuBuilder;

public class BottomNavigationPresenter implements MenuPresenter {

    /* renamed from: a  reason: collision with root package name */
    private MenuBuilder f4205a;
    private BottomNavigationMenuView b;
    private boolean c = false;
    private int d;

    static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f4206a;

        SavedState() {
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f4206a);
        }

        SavedState(Parcel parcel) {
            this.f4206a = parcel.readInt();
        }
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
    }

    public void a(BottomNavigationMenuView bottomNavigationMenuView) {
        this.b = bottomNavigationMenuView;
    }

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        return false;
    }

    public void b(boolean z) {
        this.c = z;
    }

    public boolean b() {
        return false;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public int getId() {
        return this.d;
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        this.f4205a = menuBuilder;
        this.b.a(this.f4205a);
    }

    public void a(boolean z) {
        if (!this.c) {
            if (z) {
                this.b.a();
            } else {
                this.b.c();
            }
        }
    }

    public void a(int i) {
        this.d = i;
    }

    public Parcelable a() {
        SavedState savedState = new SavedState();
        savedState.f4206a = this.b.getSelectedItemId();
        return savedState;
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.b.b(((SavedState) parcelable).f4206a);
        }
    }
}
