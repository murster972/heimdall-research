package com.google.android.material.bottomnavigation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.widget.TooltipCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.PointerIconCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.TextViewCompat;
import com.facebook.ads.AdError;
import com.google.android.material.R$dimen;
import com.google.android.material.R$drawable;
import com.google.android.material.R$id;
import com.google.android.material.R$layout;

public class BottomNavigationItemView extends FrameLayout implements MenuView.ItemView {
    private static final int[] m = {16842912};

    /* renamed from: a  reason: collision with root package name */
    private final int f4202a;
    private float b;
    private float c;
    private float d;
    private int e;
    private boolean f;
    private ImageView g;
    private final TextView h;
    private final TextView i;
    private int j;
    private MenuItemImpl k;
    private ColorStateList l;

    public BottomNavigationItemView(Context context) {
        this(context, (AttributeSet) null);
    }

    public void a(MenuItemImpl menuItemImpl, int i2) {
        this.k = menuItemImpl;
        setCheckable(menuItemImpl.isCheckable());
        setChecked(menuItemImpl.isChecked());
        setEnabled(menuItemImpl.isEnabled());
        setIcon(menuItemImpl.getIcon());
        setTitle(menuItemImpl.getTitle());
        setId(menuItemImpl.getItemId());
        if (!TextUtils.isEmpty(menuItemImpl.getContentDescription())) {
            setContentDescription(menuItemImpl.getContentDescription());
        }
        TooltipCompat.a(this, menuItemImpl.getTooltipText());
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
    }

    public boolean a() {
        return false;
    }

    public MenuItemImpl getItemData() {
        return this.k;
    }

    public int getItemPosition() {
        return this.j;
    }

    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 1);
        MenuItemImpl menuItemImpl = this.k;
        if (menuItemImpl != null && menuItemImpl.isCheckable() && this.k.isChecked()) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, m);
        }
        return onCreateDrawableState;
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
    }

    public void setChecked(boolean z) {
        TextView textView = this.i;
        textView.setPivotX((float) (textView.getWidth() / 2));
        TextView textView2 = this.i;
        textView2.setPivotY((float) textView2.getBaseline());
        TextView textView3 = this.h;
        textView3.setPivotX((float) (textView3.getWidth() / 2));
        TextView textView4 = this.h;
        textView4.setPivotY((float) textView4.getBaseline());
        int i2 = this.e;
        if (i2 != -1) {
            if (i2 == 0) {
                if (z) {
                    a(this.g, this.f4202a, 49);
                    a(this.i, 1.0f, 1.0f, 0);
                } else {
                    a(this.g, this.f4202a, 17);
                    a(this.i, 0.5f, 0.5f, 4);
                }
                this.h.setVisibility(4);
            } else if (i2 != 1) {
                if (i2 == 2) {
                    a(this.g, this.f4202a, 17);
                    this.i.setVisibility(8);
                    this.h.setVisibility(8);
                }
            } else if (z) {
                a(this.g, (int) (((float) this.f4202a) + this.b), 49);
                a(this.i, 1.0f, 1.0f, 0);
                TextView textView5 = this.h;
                float f2 = this.c;
                a(textView5, f2, f2, 4);
            } else {
                a(this.g, this.f4202a, 49);
                TextView textView6 = this.i;
                float f3 = this.d;
                a(textView6, f3, f3, 4);
                a(this.h, 1.0f, 1.0f, 0);
            }
        } else if (this.f) {
            if (z) {
                a(this.g, this.f4202a, 49);
                a(this.i, 1.0f, 1.0f, 0);
            } else {
                a(this.g, this.f4202a, 17);
                a(this.i, 0.5f, 0.5f, 4);
            }
            this.h.setVisibility(4);
        } else if (z) {
            a(this.g, (int) (((float) this.f4202a) + this.b), 49);
            a(this.i, 1.0f, 1.0f, 0);
            TextView textView7 = this.h;
            float f4 = this.c;
            a(textView7, f4, f4, 4);
        } else {
            a(this.g, this.f4202a, 49);
            TextView textView8 = this.i;
            float f5 = this.d;
            a(textView8, f5, f5, 4);
            a(this.h, 1.0f, 1.0f, 0);
        }
        refreshDrawableState();
        setSelected(z);
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.h.setEnabled(z);
        this.i.setEnabled(z);
        this.g.setEnabled(z);
        if (z) {
            ViewCompat.a((View) this, PointerIconCompat.a(getContext(), AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE));
        } else {
            ViewCompat.a((View) this, (PointerIconCompat) null);
        }
    }

    public void setIcon(Drawable drawable) {
        if (drawable != null) {
            Drawable.ConstantState constantState = drawable.getConstantState();
            if (constantState != null) {
                drawable = constantState.newDrawable();
            }
            drawable = DrawableCompat.i(drawable).mutate();
            DrawableCompat.a(drawable, this.l);
        }
        this.g.setImageDrawable(drawable);
    }

    public void setIconSize(int i2) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.g.getLayoutParams();
        layoutParams.width = i2;
        layoutParams.height = i2;
        this.g.setLayoutParams(layoutParams);
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.l = colorStateList;
        MenuItemImpl menuItemImpl = this.k;
        if (menuItemImpl != null) {
            setIcon(menuItemImpl.getIcon());
        }
    }

    public void setItemBackground(int i2) {
        setItemBackground(i2 == 0 ? null : ContextCompat.c(getContext(), i2));
    }

    public void setItemPosition(int i2) {
        this.j = i2;
    }

    public void setLabelVisibilityMode(int i2) {
        if (this.e != i2) {
            this.e = i2;
            if (this.k != null) {
                setChecked(this.k.isChecked());
            }
        }
    }

    public void setShifting(boolean z) {
        if (this.f != z) {
            this.f = z;
            if (this.k != null) {
                setChecked(this.k.isChecked());
            }
        }
    }

    public void setTextAppearanceActive(int i2) {
        TextViewCompat.d(this.i, i2);
        a(this.h.getTextSize(), this.i.getTextSize());
    }

    public void setTextAppearanceInactive(int i2) {
        TextViewCompat.d(this.h, i2);
        a(this.h.getTextSize(), this.i.getTextSize());
    }

    public void setTextColor(ColorStateList colorStateList) {
        if (colorStateList != null) {
            this.h.setTextColor(colorStateList);
            this.i.setTextColor(colorStateList);
        }
    }

    public void setTitle(CharSequence charSequence) {
        this.h.setText(charSequence);
        this.i.setText(charSequence);
        MenuItemImpl menuItemImpl = this.k;
        if (menuItemImpl == null || TextUtils.isEmpty(menuItemImpl.getContentDescription())) {
            setContentDescription(charSequence);
        }
    }

    public BottomNavigationItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BottomNavigationItemView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.j = -1;
        Resources resources = getResources();
        LayoutInflater.from(context).inflate(R$layout.design_bottom_navigation_item, this, true);
        setBackgroundResource(R$drawable.design_bottom_navigation_item_background);
        this.f4202a = resources.getDimensionPixelSize(R$dimen.design_bottom_navigation_margin);
        this.g = (ImageView) findViewById(R$id.icon);
        this.h = (TextView) findViewById(R$id.smallLabel);
        this.i = (TextView) findViewById(R$id.largeLabel);
        ViewCompat.h(this.h, 2);
        ViewCompat.h(this.i, 2);
        setFocusable(true);
        a(this.h.getTextSize(), this.i.getTextSize());
    }

    public void setItemBackground(Drawable drawable) {
        ViewCompat.a((View) this, drawable);
    }

    private void a(View view, int i2, int i3) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        layoutParams.topMargin = i2;
        layoutParams.gravity = i3;
        view.setLayoutParams(layoutParams);
    }

    private void a(View view, float f2, float f3, int i2) {
        view.setScaleX(f2);
        view.setScaleY(f3);
        view.setVisibility(i2);
    }

    private void a(float f2, float f3) {
        this.b = f2 - f3;
        this.c = (f3 * 1.0f) / f2;
        this.d = (f2 * 1.0f) / f3;
    }
}
