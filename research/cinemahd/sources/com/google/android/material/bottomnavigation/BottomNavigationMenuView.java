package com.google.android.material.bottomnavigation;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.R$attr;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import androidx.core.util.Pools$Pool;
import androidx.core.util.Pools$SynchronizedPool;
import androidx.core.view.ViewCompat;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.transition.AutoTransition;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;
import com.google.android.material.R$dimen;
import com.google.android.material.internal.TextScale;

public class BottomNavigationMenuView extends ViewGroup implements MenuView {
    private static final int[] y = {16842912};
    private static final int[] z = {-16842910};

    /* renamed from: a  reason: collision with root package name */
    private final TransitionSet f4203a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private final int f;
    private final View.OnClickListener g;
    private final Pools$Pool<BottomNavigationItemView> h;
    private boolean i;
    private int j;
    private BottomNavigationItemView[] k;
    private int l;
    private int m;
    private ColorStateList n;
    private int o;
    private ColorStateList p;
    private final ColorStateList q;
    private int r;
    private int s;
    private Drawable t;
    private int u;
    private int[] v;
    /* access modifiers changed from: private */
    public BottomNavigationPresenter w;
    /* access modifiers changed from: private */
    public MenuBuilder x;

    public BottomNavigationMenuView(Context context) {
        this(context, (AttributeSet) null);
    }

    private boolean a(int i2, int i3) {
        if (i2 == -1) {
            if (i3 > 3) {
                return true;
            }
        } else if (i2 == 0) {
            return true;
        }
        return false;
    }

    private BottomNavigationItemView getNewItem() {
        BottomNavigationItemView acquire = this.h.acquire();
        return acquire == null ? new BottomNavigationItemView(getContext()) : acquire;
    }

    public void c() {
        MenuBuilder menuBuilder = this.x;
        if (menuBuilder != null && this.k != null) {
            int size = menuBuilder.size();
            if (size != this.k.length) {
                a();
                return;
            }
            int i2 = this.l;
            for (int i3 = 0; i3 < size; i3++) {
                MenuItem item = this.x.getItem(i3);
                if (item.isChecked()) {
                    this.l = item.getItemId();
                    this.m = i3;
                }
            }
            if (i2 != this.l) {
                TransitionManager.a(this, this.f4203a);
            }
            boolean a2 = a(this.j, this.x.n().size());
            for (int i4 = 0; i4 < size; i4++) {
                this.w.b(true);
                this.k[i4].setLabelVisibilityMode(this.j);
                this.k[i4].setShifting(a2);
                this.k[i4].a((MenuItemImpl) this.x.getItem(i4), 0);
                this.w.b(false);
            }
        }
    }

    public ColorStateList getIconTintList() {
        return this.n;
    }

    public Drawable getItemBackground() {
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr == null || bottomNavigationItemViewArr.length <= 0) {
            return this.t;
        }
        return bottomNavigationItemViewArr[0].getBackground();
    }

    @Deprecated
    public int getItemBackgroundRes() {
        return this.u;
    }

    public int getItemIconSize() {
        return this.o;
    }

    public int getItemTextAppearanceActive() {
        return this.s;
    }

    public int getItemTextAppearanceInactive() {
        return this.r;
    }

    public ColorStateList getItemTextColor() {
        return this.p;
    }

    public int getLabelVisibilityMode() {
        return this.j;
    }

    public int getSelectedItemId() {
        return this.l;
    }

    public int getWindowAnimations() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        int i8 = 0;
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            if (childAt.getVisibility() != 8) {
                if (ViewCompat.m(this) == 1) {
                    int i10 = i6 - i8;
                    childAt.layout(i10 - childAt.getMeasuredWidth(), 0, i10, i7);
                } else {
                    childAt.layout(i8, 0, childAt.getMeasuredWidth() + i8, i7);
                }
                i8 += childAt.getMeasuredWidth();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int size = View.MeasureSpec.getSize(i2);
        int size2 = this.x.n().size();
        int childCount = getChildCount();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.f, 1073741824);
        if (!a(this.j, size2) || !this.i) {
            int min = Math.min(size / (size2 == 0 ? 1 : size2), this.d);
            int i4 = size - (size2 * min);
            for (int i5 = 0; i5 < childCount; i5++) {
                if (getChildAt(i5).getVisibility() != 8) {
                    int[] iArr = this.v;
                    iArr[i5] = min;
                    if (i4 > 0) {
                        iArr[i5] = iArr[i5] + 1;
                        i4--;
                    }
                } else {
                    this.v[i5] = 0;
                }
            }
        } else {
            View childAt = getChildAt(this.m);
            int i6 = this.e;
            if (childAt.getVisibility() != 8) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec(this.d, Integer.MIN_VALUE), makeMeasureSpec);
                i6 = Math.max(i6, childAt.getMeasuredWidth());
            }
            int i7 = size2 - (childAt.getVisibility() != 8 ? 1 : 0);
            int min2 = Math.min(size - (this.c * i7), Math.min(i6, this.d));
            int i8 = size - min2;
            int min3 = Math.min(i8 / (i7 == 0 ? 1 : i7), this.b);
            int i9 = i8 - (i7 * min3);
            int i10 = 0;
            while (i10 < childCount) {
                if (getChildAt(i10).getVisibility() != 8) {
                    this.v[i10] = i10 == this.m ? min2 : min3;
                    if (i9 > 0) {
                        int[] iArr2 = this.v;
                        iArr2[i10] = iArr2[i10] + 1;
                        i9--;
                    }
                } else {
                    this.v[i10] = 0;
                }
                i10++;
            }
        }
        int i11 = 0;
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt2 = getChildAt(i12);
            if (childAt2.getVisibility() != 8) {
                childAt2.measure(View.MeasureSpec.makeMeasureSpec(this.v[i12], 1073741824), makeMeasureSpec);
                childAt2.getLayoutParams().width = childAt2.getMeasuredWidth();
                i11 += childAt2.getMeasuredWidth();
            }
        }
        setMeasuredDimension(View.resolveSizeAndState(i11, View.MeasureSpec.makeMeasureSpec(i11, 1073741824), 0), View.resolveSizeAndState(this.f, makeMeasureSpec, 0));
    }

    public void setIconTintList(ColorStateList colorStateList) {
        this.n = colorStateList;
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView iconTintList : bottomNavigationItemViewArr) {
                iconTintList.setIconTintList(colorStateList);
            }
        }
    }

    public void setItemBackground(Drawable drawable) {
        this.t = drawable;
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView itemBackground : bottomNavigationItemViewArr) {
                itemBackground.setItemBackground(drawable);
            }
        }
    }

    public void setItemBackgroundRes(int i2) {
        this.u = i2;
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView itemBackground : bottomNavigationItemViewArr) {
                itemBackground.setItemBackground(i2);
            }
        }
    }

    public void setItemHorizontalTranslationEnabled(boolean z2) {
        this.i = z2;
    }

    public void setItemIconSize(int i2) {
        this.o = i2;
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView iconSize : bottomNavigationItemViewArr) {
                iconSize.setIconSize(i2);
            }
        }
    }

    public void setItemTextAppearanceActive(int i2) {
        this.s = i2;
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView bottomNavigationItemView : bottomNavigationItemViewArr) {
                bottomNavigationItemView.setTextAppearanceActive(i2);
                ColorStateList colorStateList = this.p;
                if (colorStateList != null) {
                    bottomNavigationItemView.setTextColor(colorStateList);
                }
            }
        }
    }

    public void setItemTextAppearanceInactive(int i2) {
        this.r = i2;
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView bottomNavigationItemView : bottomNavigationItemViewArr) {
                bottomNavigationItemView.setTextAppearanceInactive(i2);
                ColorStateList colorStateList = this.p;
                if (colorStateList != null) {
                    bottomNavigationItemView.setTextColor(colorStateList);
                }
            }
        }
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.p = colorStateList;
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView textColor : bottomNavigationItemViewArr) {
                textColor.setTextColor(colorStateList);
            }
        }
    }

    public void setLabelVisibilityMode(int i2) {
        this.j = i2;
    }

    public void setPresenter(BottomNavigationPresenter bottomNavigationPresenter) {
        this.w = bottomNavigationPresenter;
    }

    public BottomNavigationMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h = new Pools$SynchronizedPool(5);
        this.l = 0;
        this.m = 0;
        Resources resources = getResources();
        this.b = resources.getDimensionPixelSize(R$dimen.design_bottom_navigation_item_max_width);
        this.c = resources.getDimensionPixelSize(R$dimen.design_bottom_navigation_item_min_width);
        this.d = resources.getDimensionPixelSize(R$dimen.design_bottom_navigation_active_item_max_width);
        this.e = resources.getDimensionPixelSize(R$dimen.design_bottom_navigation_active_item_min_width);
        this.f = resources.getDimensionPixelSize(R$dimen.design_bottom_navigation_height);
        this.q = a(16842808);
        this.f4203a = new AutoTransition();
        this.f4203a.b(0);
        this.f4203a.a(115);
        this.f4203a.a((TimeInterpolator) new FastOutSlowInInterpolator());
        this.f4203a.a((Transition) new TextScale());
        this.g = new View.OnClickListener() {
            public void onClick(View view) {
                MenuItemImpl itemData = ((BottomNavigationItemView) view).getItemData();
                if (!BottomNavigationMenuView.this.x.a((MenuItem) itemData, (MenuPresenter) BottomNavigationMenuView.this.w, 0)) {
                    itemData.setChecked(true);
                }
            }
        };
        this.v = new int[5];
    }

    public void a(MenuBuilder menuBuilder) {
        this.x = menuBuilder;
    }

    public boolean b() {
        return this.i;
    }

    public ColorStateList a(int i2) {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(i2, typedValue, true)) {
            return null;
        }
        ColorStateList b2 = AppCompatResources.b(getContext(), typedValue.resourceId);
        if (!getContext().getTheme().resolveAttribute(R$attr.colorPrimary, typedValue, true)) {
            return null;
        }
        int i3 = typedValue.data;
        int defaultColor = b2.getDefaultColor();
        return new ColorStateList(new int[][]{z, y, ViewGroup.EMPTY_STATE_SET}, new int[]{b2.getColorForState(z, defaultColor), i3, defaultColor});
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        int size = this.x.size();
        for (int i3 = 0; i3 < size; i3++) {
            MenuItem item = this.x.getItem(i3);
            if (i2 == item.getItemId()) {
                this.l = i2;
                this.m = i3;
                item.setChecked(true);
                return;
            }
        }
    }

    public void a() {
        removeAllViews();
        BottomNavigationItemView[] bottomNavigationItemViewArr = this.k;
        if (bottomNavigationItemViewArr != null) {
            for (BottomNavigationItemView bottomNavigationItemView : bottomNavigationItemViewArr) {
                if (bottomNavigationItemView != null) {
                    this.h.release(bottomNavigationItemView);
                }
            }
        }
        if (this.x.size() == 0) {
            this.l = 0;
            this.m = 0;
            this.k = null;
            return;
        }
        this.k = new BottomNavigationItemView[this.x.size()];
        boolean a2 = a(this.j, this.x.n().size());
        for (int i2 = 0; i2 < this.x.size(); i2++) {
            this.w.b(true);
            this.x.getItem(i2).setCheckable(true);
            this.w.b(false);
            BottomNavigationItemView newItem = getNewItem();
            this.k[i2] = newItem;
            newItem.setIconTintList(this.n);
            newItem.setIconSize(this.o);
            newItem.setTextColor(this.q);
            newItem.setTextAppearanceInactive(this.r);
            newItem.setTextAppearanceActive(this.s);
            newItem.setTextColor(this.p);
            Drawable drawable = this.t;
            if (drawable != null) {
                newItem.setItemBackground(drawable);
            } else {
                newItem.setItemBackground(this.u);
            }
            newItem.setShifting(a2);
            newItem.setLabelVisibilityMode(this.j);
            newItem.a((MenuItemImpl) this.x.getItem(i2), 0);
            newItem.setItemPosition(i2);
            newItem.setOnClickListener(this.g);
            addView(newItem);
        }
        this.m = Math.min(this.x.size() - 1, this.m);
        this.x.getItem(this.m).setChecked(true);
    }
}
