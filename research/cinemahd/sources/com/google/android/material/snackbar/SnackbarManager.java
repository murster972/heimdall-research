package com.google.android.material.snackbar;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

class SnackbarManager {
    private static SnackbarManager e;

    /* renamed from: a  reason: collision with root package name */
    private final Object f4288a = new Object();
    private final Handler b = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            SnackbarManager.this.a((SnackbarRecord) message.obj);
            return true;
        }
    });
    private SnackbarRecord c;
    private SnackbarRecord d;

    interface Callback {
        void a(int i);

        void show();
    }

    private static class SnackbarRecord {

        /* renamed from: a  reason: collision with root package name */
        final WeakReference<Callback> f4290a;
        int b;
        boolean c;

        SnackbarRecord(int i, Callback callback) {
            this.f4290a = new WeakReference<>(callback);
            this.b = i;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Callback callback) {
            return callback != null && this.f4290a.get() == callback;
        }
    }

    private SnackbarManager() {
    }

    static SnackbarManager a() {
        if (e == null) {
            e = new SnackbarManager();
        }
        return e;
    }

    private boolean f(Callback callback) {
        SnackbarRecord snackbarRecord = this.c;
        return snackbarRecord != null && snackbarRecord.a(callback);
    }

    private boolean g(Callback callback) {
        SnackbarRecord snackbarRecord = this.d;
        return snackbarRecord != null && snackbarRecord.a(callback);
    }

    public void b(Callback callback) {
        synchronized (this.f4288a) {
            if (f(callback)) {
                this.c = null;
                if (this.d != null) {
                    b();
                }
            }
        }
    }

    public void c(Callback callback) {
        synchronized (this.f4288a) {
            if (f(callback)) {
                b(this.c);
            }
        }
    }

    public void d(Callback callback) {
        synchronized (this.f4288a) {
            if (f(callback) && !this.c.c) {
                this.c.c = true;
                this.b.removeCallbacksAndMessages(this.c);
            }
        }
    }

    public void e(Callback callback) {
        synchronized (this.f4288a) {
            if (f(callback) && this.c.c) {
                this.c.c = false;
                b(this.c);
            }
        }
    }

    public void a(int i, Callback callback) {
        synchronized (this.f4288a) {
            if (f(callback)) {
                this.c.b = i;
                this.b.removeCallbacksAndMessages(this.c);
                b(this.c);
                return;
            }
            if (g(callback)) {
                this.d.b = i;
            } else {
                this.d = new SnackbarRecord(i, callback);
            }
            if (this.c == null || !a(this.c, 4)) {
                this.c = null;
                b();
            }
        }
    }

    private void b() {
        SnackbarRecord snackbarRecord = this.d;
        if (snackbarRecord != null) {
            this.c = snackbarRecord;
            this.d = null;
            Callback callback = (Callback) this.c.f4290a.get();
            if (callback != null) {
                callback.show();
            } else {
                this.c = null;
            }
        }
    }

    private void b(SnackbarRecord snackbarRecord) {
        int i = snackbarRecord.b;
        if (i != -2) {
            if (i <= 0) {
                i = i == -1 ? 1500 : 2750;
            }
            this.b.removeCallbacksAndMessages(snackbarRecord);
            Handler handler = this.b;
            handler.sendMessageDelayed(Message.obtain(handler, 0, snackbarRecord), (long) i);
        }
    }

    public void a(Callback callback, int i) {
        synchronized (this.f4288a) {
            if (f(callback)) {
                a(this.c, i);
            } else if (g(callback)) {
                a(this.d, i);
            }
        }
    }

    public boolean a(Callback callback) {
        boolean z;
        synchronized (this.f4288a) {
            if (!f(callback)) {
                if (!g(callback)) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    private boolean a(SnackbarRecord snackbarRecord, int i) {
        Callback callback = (Callback) snackbarRecord.f4290a.get();
        if (callback == null) {
            return false;
        }
        this.b.removeCallbacksAndMessages(snackbarRecord);
        callback.a(i);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(SnackbarRecord snackbarRecord) {
        synchronized (this.f4288a) {
            if (this.c == snackbarRecord || this.d == snackbarRecord) {
                a(snackbarRecord, 2);
            }
        }
    }
}
