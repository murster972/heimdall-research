package com.google.android.material.snackbar;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.accessibility.AccessibilityManagerCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import com.facebook.imagepipeline.common.RotationOptions;
import com.google.android.material.R$attr;
import com.google.android.material.R$layout;
import com.google.android.material.R$styleable;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.SnackbarManager;
import java.util.List;

public abstract class BaseTransientBottomBar<B extends BaseTransientBottomBar<B>> {
    static final Handler j = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                ((BaseTransientBottomBar) message.obj).l();
                return true;
            } else if (i != 1) {
                return false;
            } else {
                ((BaseTransientBottomBar) message.obj).b(message.arg1);
                return true;
            }
        }
    });
    /* access modifiers changed from: private */
    public static final boolean k;
    private static final int[] l = {R$attr.snackbarStyle};

    /* renamed from: a  reason: collision with root package name */
    private final ViewGroup f4272a;
    private final Context b;
    protected final SnackbarBaseLayout c;
    /* access modifiers changed from: private */
    public final ContentViewCallback d;
    private int e;
    private List<BaseCallback<B>> f;
    private Behavior g;
    private final AccessibilityManager h;
    final SnackbarManager.Callback i = new SnackbarManager.Callback() {
        public void a(int i) {
            Handler handler = BaseTransientBottomBar.j;
            handler.sendMessage(handler.obtainMessage(1, i, 0, BaseTransientBottomBar.this));
        }

        public void show() {
            Handler handler = BaseTransientBottomBar.j;
            handler.sendMessage(handler.obtainMessage(0, BaseTransientBottomBar.this));
        }
    };

    public static abstract class BaseCallback<B> {
        public void a(B b) {
        }

        public void a(B b, int i) {
        }
    }

    public static class Behavior extends SwipeDismissBehavior<View> {
        private final BehaviorDelegate k = new BehaviorDelegate(this);

        public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            this.k.a(coordinatorLayout, view, motionEvent);
            return super.onInterceptTouchEvent(coordinatorLayout, view, motionEvent);
        }

        /* access modifiers changed from: private */
        public void a(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.k.a(baseTransientBottomBar);
        }

        public boolean a(View view) {
            return this.k.a(view);
        }
    }

    public static class BehaviorDelegate {

        /* renamed from: a  reason: collision with root package name */
        private SnackbarManager.Callback f4283a;

        public BehaviorDelegate(SwipeDismissBehavior<?> swipeDismissBehavior) {
            swipeDismissBehavior.b(0.1f);
            swipeDismissBehavior.a(0.6f);
            swipeDismissBehavior.a(0);
        }

        public void a(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.f4283a = baseTransientBottomBar.i;
        }

        public boolean a(View view) {
            return view instanceof SnackbarBaseLayout;
        }

        public void a(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked != 0) {
                if (actionMasked == 1 || actionMasked == 3) {
                    SnackbarManager.a().e(this.f4283a);
                }
            } else if (coordinatorLayout.a(view, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                SnackbarManager.a().d(this.f4283a);
            }
        }
    }

    protected interface OnAttachStateChangeListener {
        void onViewAttachedToWindow(View view);

        void onViewDetachedFromWindow(View view);
    }

    protected interface OnLayoutChangeListener {
        void a(View view, int i, int i2, int i3, int i4);
    }

    protected static class SnackbarBaseLayout extends FrameLayout {

        /* renamed from: a  reason: collision with root package name */
        private final AccessibilityManager f4284a;
        private final AccessibilityManagerCompat.TouchExplorationStateChangeListener b;
        private OnLayoutChangeListener c;
        private OnAttachStateChangeListener d;

        protected SnackbarBaseLayout(Context context) {
            this(context, (AttributeSet) null);
        }

        /* access modifiers changed from: private */
        public void setClickableOrFocusableBasedOnAccessibility(boolean z) {
            setClickable(!z);
            setFocusable(z);
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            OnAttachStateChangeListener onAttachStateChangeListener = this.d;
            if (onAttachStateChangeListener != null) {
                onAttachStateChangeListener.onViewAttachedToWindow(this);
            }
            ViewCompat.J(this);
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            OnAttachStateChangeListener onAttachStateChangeListener = this.d;
            if (onAttachStateChangeListener != null) {
                onAttachStateChangeListener.onViewDetachedFromWindow(this);
            }
            AccessibilityManagerCompat.b(this.f4284a, this.b);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            OnLayoutChangeListener onLayoutChangeListener = this.c;
            if (onLayoutChangeListener != null) {
                onLayoutChangeListener.a(this, i, i2, i3, i4);
            }
        }

        /* access modifiers changed from: package-private */
        public void setOnAttachStateChangeListener(OnAttachStateChangeListener onAttachStateChangeListener) {
            this.d = onAttachStateChangeListener;
        }

        /* access modifiers changed from: package-private */
        public void setOnLayoutChangeListener(OnLayoutChangeListener onLayoutChangeListener) {
            this.c = onLayoutChangeListener;
        }

        protected SnackbarBaseLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.SnackbarLayout);
            if (obtainStyledAttributes.hasValue(R$styleable.SnackbarLayout_elevation)) {
                ViewCompat.b((View) this, (float) obtainStyledAttributes.getDimensionPixelSize(R$styleable.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            this.f4284a = (AccessibilityManager) context.getSystemService("accessibility");
            this.b = new AccessibilityManagerCompat.TouchExplorationStateChangeListener() {
                public void onTouchExplorationStateChanged(boolean z) {
                    SnackbarBaseLayout.this.setClickableOrFocusableBasedOnAccessibility(z);
                }
            };
            AccessibilityManagerCompat.a(this.f4284a, this.b);
            setClickableOrFocusableBasedOnAccessibility(this.f4284a.isTouchExplorationEnabled());
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        k = i2 >= 16 && i2 <= 19;
    }

    protected BaseTransientBottomBar(ViewGroup viewGroup, View view, ContentViewCallback contentViewCallback) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null parent");
        } else if (view == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null content");
        } else if (contentViewCallback != null) {
            this.f4272a = viewGroup;
            this.d = contentViewCallback;
            this.b = viewGroup.getContext();
            ThemeEnforcement.a(this.b);
            this.c = (SnackbarBaseLayout) LayoutInflater.from(this.b).inflate(e(), this.f4272a, false);
            this.c.addView(view);
            ViewCompat.g(this.c, 1);
            ViewCompat.h(this.c, 1);
            ViewCompat.a((View) this.c, true);
            ViewCompat.a((View) this.c, (OnApplyWindowInsetsListener) new OnApplyWindowInsetsListener(this) {
                public WindowInsetsCompat a(View view, WindowInsetsCompat windowInsetsCompat) {
                    view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), windowInsetsCompat.b());
                    return windowInsetsCompat;
                }
            });
            ViewCompat.a((View) this.c, (AccessibilityDelegateCompat) new AccessibilityDelegateCompat() {
                public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
                    super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
                    accessibilityNodeInfoCompat.a(1048576);
                    accessibilityNodeInfoCompat.g(true);
                }

                public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
                    if (i != 1048576) {
                        return super.performAccessibilityAction(view, i, bundle);
                    }
                    BaseTransientBottomBar.this.b();
                    return true;
                }
            });
            this.h = (AccessibilityManager) this.b.getSystemService("accessibility");
        } else {
            throw new IllegalArgumentException("Transient bottom bar must have non-null callback");
        }
    }

    private int n() {
        int height = this.c.getHeight();
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? height + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin : height;
    }

    public void b() {
        a(3);
    }

    public int c() {
        return this.e;
    }

    public B d(int i2) {
        this.e = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public int e() {
        return g() ? R$layout.mtrl_layout_snackbar : R$layout.design_layout_snackbar;
    }

    public View f() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        TypedArray obtainStyledAttributes = this.b.obtainStyledAttributes(l);
        int resourceId = obtainStyledAttributes.getResourceId(0, -1);
        obtainStyledAttributes.recycle();
        if (resourceId != -1) {
            return true;
        }
        return false;
    }

    public boolean h() {
        return SnackbarManager.a().a(this.i);
    }

    /* access modifiers changed from: package-private */
    public void i() {
        SnackbarManager.a().c(this.i);
        List<BaseCallback<B>> list = this.f;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f.get(size).a(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        List<AccessibilityServiceInfo> enabledAccessibilityServiceList = this.h.getEnabledAccessibilityServiceList(1);
        if (enabledAccessibilityServiceList == null || !enabledAccessibilityServiceList.isEmpty()) {
            return false;
        }
        return true;
    }

    public void k() {
        SnackbarManager.a().a(c(), this.i);
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        if (this.c.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.LayoutParams) {
                CoordinatorLayout.LayoutParams layoutParams2 = (CoordinatorLayout.LayoutParams) layoutParams;
                SwipeDismissBehavior swipeDismissBehavior = this.g;
                if (swipeDismissBehavior == null) {
                    swipeDismissBehavior = d();
                }
                if (swipeDismissBehavior instanceof Behavior) {
                    ((Behavior) swipeDismissBehavior).a((BaseTransientBottomBar<?>) this);
                }
                swipeDismissBehavior.a((SwipeDismissBehavior.OnDismissListener) new SwipeDismissBehavior.OnDismissListener() {
                    public void a(View view) {
                        view.setVisibility(8);
                        BaseTransientBottomBar.this.a(0);
                    }

                    public void a(int i) {
                        if (i == 0) {
                            SnackbarManager.a().e(BaseTransientBottomBar.this.i);
                        } else if (i == 1 || i == 2) {
                            SnackbarManager.a().d(BaseTransientBottomBar.this.i);
                        }
                    }
                });
                layoutParams2.a((CoordinatorLayout.Behavior) swipeDismissBehavior);
                layoutParams2.g = 80;
            }
            this.f4272a.addView(this.c);
        }
        this.c.setOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(View view) {
            }

            public void onViewDetachedFromWindow(View view) {
                if (BaseTransientBottomBar.this.h()) {
                    BaseTransientBottomBar.j.post(new Runnable() {
                        public void run() {
                            BaseTransientBottomBar.this.c(3);
                        }
                    });
                }
            }
        });
        if (!ViewCompat.E(this.c)) {
            this.c.setOnLayoutChangeListener(new OnLayoutChangeListener() {
                public void a(View view, int i, int i2, int i3, int i4) {
                    BaseTransientBottomBar.this.c.setOnLayoutChangeListener((OnLayoutChangeListener) null);
                    if (BaseTransientBottomBar.this.j()) {
                        BaseTransientBottomBar.this.a();
                    } else {
                        BaseTransientBottomBar.this.i();
                    }
                }
            });
        } else if (j()) {
            a();
        } else {
            i();
        }
    }

    private void e(final int i2) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(new int[]{0, n()});
        valueAnimator.setInterpolator(AnimationUtils.b);
        valueAnimator.setDuration(250);
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                BaseTransientBottomBar.this.c(i2);
            }

            public void onAnimationStart(Animator animator) {
                BaseTransientBottomBar.this.d.b(0, RotationOptions.ROTATE_180);
            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            /* renamed from: a  reason: collision with root package name */
            private int f4274a = 0;

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                if (BaseTransientBottomBar.k) {
                    ViewCompat.f(BaseTransientBottomBar.this.c, intValue - this.f4274a);
                } else {
                    BaseTransientBottomBar.this.c.setTranslationY((float) intValue);
                }
                this.f4274a = intValue;
            }
        });
        valueAnimator.start();
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        SnackbarManager.a().a(this.i, i2);
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        if (!j() || this.c.getVisibility() != 0) {
            c(i2);
        } else {
            e(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        SnackbarManager.a().b(this.i);
        List<BaseCallback<B>> list = this.f;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f.get(size).a(this, i2);
            }
        }
        ViewParent parent = this.c.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public SwipeDismissBehavior<? extends View> d() {
        return new Behavior();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        final int n = n();
        if (k) {
            ViewCompat.f(this.c, n);
        } else {
            this.c.setTranslationY((float) n);
        }
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(new int[]{n, 0});
        valueAnimator.setInterpolator(AnimationUtils.b);
        valueAnimator.setDuration(250);
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                BaseTransientBottomBar.this.i();
            }

            public void onAnimationStart(Animator animator) {
                BaseTransientBottomBar.this.d.a(70, RotationOptions.ROTATE_180);
            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            /* renamed from: a  reason: collision with root package name */
            private int f4282a = n;

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                if (BaseTransientBottomBar.k) {
                    ViewCompat.f(BaseTransientBottomBar.this.c, intValue - this.f4282a);
                } else {
                    BaseTransientBottomBar.this.c.setTranslationY((float) intValue);
                }
                this.f4282a = intValue;
            }
        });
        valueAnimator.start();
    }
}
