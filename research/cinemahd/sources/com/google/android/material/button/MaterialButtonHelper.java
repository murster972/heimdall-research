package com.google.android.material.button;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ViewCompat;
import com.google.android.material.R$styleable;
import com.google.android.material.internal.ViewUtils;
import com.google.android.material.resources.MaterialResources;
import com.google.android.material.ripple.RippleUtils;

class MaterialButtonHelper {
    private static final boolean w = (Build.VERSION.SDK_INT >= 21);

    /* renamed from: a  reason: collision with root package name */
    private final MaterialButton f4216a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private PorterDuff.Mode h;
    private ColorStateList i;
    private ColorStateList j;
    private ColorStateList k;
    private final Paint l = new Paint(1);
    private final Rect m = new Rect();
    private final RectF n = new RectF();
    private GradientDrawable o;
    private Drawable p;
    private GradientDrawable q;
    private Drawable r;
    private GradientDrawable s;
    private GradientDrawable t;
    private GradientDrawable u;
    private boolean v = false;

    public MaterialButtonHelper(MaterialButton materialButton) {
        this.f4216a = materialButton;
    }

    private Drawable i() {
        this.o = new GradientDrawable();
        this.o.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.o.setColor(-1);
        this.p = DrawableCompat.i(this.o);
        DrawableCompat.a(this.p, this.i);
        PorterDuff.Mode mode = this.h;
        if (mode != null) {
            DrawableCompat.a(this.p, mode);
        }
        this.q = new GradientDrawable();
        this.q.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.q.setColor(-1);
        this.r = DrawableCompat.i(this.q);
        DrawableCompat.a(this.r, this.k);
        return a((Drawable) new LayerDrawable(new Drawable[]{this.p, this.r}));
    }

    @TargetApi(21)
    private Drawable j() {
        this.s = new GradientDrawable();
        this.s.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.s.setColor(-1);
        n();
        this.t = new GradientDrawable();
        this.t.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.t.setColor(0);
        this.t.setStroke(this.g, this.j);
        InsetDrawable a2 = a((Drawable) new LayerDrawable(new Drawable[]{this.s, this.t}));
        this.u = new GradientDrawable();
        this.u.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.u.setColor(-1);
        return new MaterialButtonBackgroundDrawable(RippleUtils.a(this.k), a2, this.u);
    }

    private GradientDrawable k() {
        if (!w || this.f4216a.getBackground() == null) {
            return null;
        }
        return (GradientDrawable) ((LayerDrawable) ((InsetDrawable) ((RippleDrawable) this.f4216a.getBackground()).getDrawable(0)).getDrawable()).getDrawable(0);
    }

    private GradientDrawable l() {
        if (!w || this.f4216a.getBackground() == null) {
            return null;
        }
        return (GradientDrawable) ((LayerDrawable) ((InsetDrawable) ((RippleDrawable) this.f4216a.getBackground()).getDrawable(0)).getDrawable()).getDrawable(1);
    }

    private void m() {
        if (w && this.t != null) {
            this.f4216a.setInternalBackground(j());
        } else if (!w) {
            this.f4216a.invalidate();
        }
    }

    private void n() {
        GradientDrawable gradientDrawable = this.s;
        if (gradientDrawable != null) {
            DrawableCompat.a((Drawable) gradientDrawable, this.i);
            PorterDuff.Mode mode = this.h;
            if (mode != null) {
                DrawableCompat.a((Drawable) this.s, mode);
            }
        }
    }

    public void a(TypedArray typedArray) {
        int i2 = 0;
        this.b = typedArray.getDimensionPixelOffset(R$styleable.MaterialButton_android_insetLeft, 0);
        this.c = typedArray.getDimensionPixelOffset(R$styleable.MaterialButton_android_insetRight, 0);
        this.d = typedArray.getDimensionPixelOffset(R$styleable.MaterialButton_android_insetTop, 0);
        this.e = typedArray.getDimensionPixelOffset(R$styleable.MaterialButton_android_insetBottom, 0);
        this.f = typedArray.getDimensionPixelSize(R$styleable.MaterialButton_cornerRadius, 0);
        this.g = typedArray.getDimensionPixelSize(R$styleable.MaterialButton_strokeWidth, 0);
        this.h = ViewUtils.a(typedArray.getInt(R$styleable.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.i = MaterialResources.a(this.f4216a.getContext(), typedArray, R$styleable.MaterialButton_backgroundTint);
        this.j = MaterialResources.a(this.f4216a.getContext(), typedArray, R$styleable.MaterialButton_strokeColor);
        this.k = MaterialResources.a(this.f4216a.getContext(), typedArray, R$styleable.MaterialButton_rippleColor);
        this.l.setStyle(Paint.Style.STROKE);
        this.l.setStrokeWidth((float) this.g);
        Paint paint = this.l;
        ColorStateList colorStateList = this.j;
        if (colorStateList != null) {
            i2 = colorStateList.getColorForState(this.f4216a.getDrawableState(), 0);
        }
        paint.setColor(i2);
        int q2 = ViewCompat.q(this.f4216a);
        int paddingTop = this.f4216a.getPaddingTop();
        int p2 = ViewCompat.p(this.f4216a);
        int paddingBottom = this.f4216a.getPaddingBottom();
        this.f4216a.setInternalBackground(w ? j() : i());
        ViewCompat.b(this.f4216a, q2 + this.b, paddingTop + this.d, p2 + this.c, paddingBottom + this.e);
    }

    /* access modifiers changed from: package-private */
    public ColorStateList b() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public void c(ColorStateList colorStateList) {
        if (this.i != colorStateList) {
            this.i = colorStateList;
            if (w) {
                n();
                return;
            }
            Drawable drawable = this.p;
            if (drawable != null) {
                DrawableCompat.a(drawable, this.i);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public ColorStateList e() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode f() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.v = true;
        this.f4216a.setSupportBackgroundTintList(this.i);
        this.f4216a.setSupportBackgroundTintMode(this.h);
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            Paint paint = this.l;
            int i2 = 0;
            if (colorStateList != null) {
                i2 = colorStateList.getColorForState(this.f4216a.getDrawableState(), 0);
            }
            paint.setColor(i2);
            m();
        }
    }

    /* access modifiers changed from: package-private */
    public ColorStateList c() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        GradientDrawable gradientDrawable;
        if (this.f != i2) {
            this.f = i2;
            if (w && this.s != null && this.t != null && this.u != null) {
                if (Build.VERSION.SDK_INT == 21) {
                    float f2 = ((float) i2) + 1.0E-5f;
                    k().setCornerRadius(f2);
                    l().setCornerRadius(f2);
                }
                float f3 = ((float) i2) + 1.0E-5f;
                this.s.setCornerRadius(f3);
                this.t.setCornerRadius(f3);
                this.u.setCornerRadius(f3);
            } else if (!w && (gradientDrawable = this.o) != null && this.q != null) {
                float f4 = ((float) i2) + 1.0E-5f;
                gradientDrawable.setCornerRadius(f4);
                this.q.setCornerRadius(f4);
                this.f4216a.invalidate();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        if (this.g != i2) {
            this.g = i2;
            this.l.setStrokeWidth((float) i2);
            m();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas) {
        if (canvas != null && this.j != null && this.g > 0) {
            this.m.set(this.f4216a.getBackground().getBounds());
            RectF rectF = this.n;
            Rect rect = this.m;
            int i2 = this.g;
            rectF.set(((float) rect.left) + (((float) i2) / 2.0f) + ((float) this.b), ((float) rect.top) + (((float) i2) / 2.0f) + ((float) this.d), (((float) rect.right) - (((float) i2) / 2.0f)) - ((float) this.c), (((float) rect.bottom) - (((float) i2) / 2.0f)) - ((float) this.e));
            float f2 = ((float) this.f) - (((float) this.g) / 2.0f);
            canvas.drawRoundRect(this.n, f2, f2, this.l);
        }
    }

    private InsetDrawable a(Drawable drawable) {
        return new InsetDrawable(drawable, this.b, this.d, this.c, this.e);
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        PorterDuff.Mode mode2;
        if (this.h != mode) {
            this.h = mode;
            if (w) {
                n();
                return;
            }
            Drawable drawable = this.p;
            if (drawable != null && (mode2 = this.h) != null) {
                DrawableCompat.a(drawable, mode2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        GradientDrawable gradientDrawable = this.u;
        if (gradientDrawable != null) {
            gradientDrawable.setBounds(this.b, this.d, i3 - this.c, i2 - this.e);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        GradientDrawable gradientDrawable;
        GradientDrawable gradientDrawable2;
        if (w && (gradientDrawable2 = this.s) != null) {
            gradientDrawable2.setColor(i2);
        } else if (!w && (gradientDrawable = this.o) != null) {
            gradientDrawable.setColor(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        Drawable drawable;
        if (this.k != colorStateList) {
            this.k = colorStateList;
            if (w && (this.f4216a.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.f4216a.getBackground()).setColor(colorStateList);
            } else if (!w && (drawable = this.r) != null) {
                DrawableCompat.a(drawable, colorStateList);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f;
    }
}
