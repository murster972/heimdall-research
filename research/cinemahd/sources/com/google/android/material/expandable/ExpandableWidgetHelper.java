package com.google.android.material.expandable;

import android.os.Bundle;
import android.view.View;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

public final class ExpandableWidgetHelper {

    /* renamed from: a  reason: collision with root package name */
    private final View f4232a;
    private boolean b = false;
    private int c = 0;

    public ExpandableWidgetHelper(ExpandableWidget expandableWidget) {
        this.f4232a = (View) expandableWidget;
    }

    private void d() {
        ViewParent parent = this.f4232a.getParent();
        if (parent instanceof CoordinatorLayout) {
            ((CoordinatorLayout) parent).a(this.f4232a);
        }
    }

    public void a(Bundle bundle) {
        this.b = bundle.getBoolean("expanded", false);
        this.c = bundle.getInt("expandedComponentIdHint", 0);
        if (this.b) {
            d();
        }
    }

    public boolean b() {
        return this.b;
    }

    public Bundle c() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("expanded", this.b);
        bundle.putInt("expandedComponentIdHint", this.c);
        return bundle;
    }

    public void a(int i) {
        this.c = i;
    }

    public int a() {
        return this.c;
    }
}
