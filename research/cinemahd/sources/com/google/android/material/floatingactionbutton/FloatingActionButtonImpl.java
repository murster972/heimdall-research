package com.google.android.material.floatingactionbutton;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ViewCompat;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.material.R$animator;
import com.google.android.material.R$color;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.animation.AnimatorSetCompat;
import com.google.android.material.animation.ImageMatrixProperty;
import com.google.android.material.animation.MatrixEvaluator;
import com.google.android.material.animation.MotionSpec;
import com.google.android.material.internal.CircularBorderDrawable;
import com.google.android.material.internal.StateListAnimator;
import com.google.android.material.internal.VisibilityAwareImageButton;
import com.google.android.material.ripple.RippleUtils;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import com.google.android.material.shadow.ShadowViewDelegate;
import java.util.ArrayList;
import java.util.Iterator;

class FloatingActionButtonImpl {
    static final TimeInterpolator B = AnimationUtils.c;
    static final int[] C = {16842919, 16842910};
    static final int[] D = {16843623, 16842908, 16842910};
    static final int[] E = {16842908, 16842910};
    static final int[] F = {16843623, 16842910};
    static final int[] G = {16842910};
    static final int[] H = new int[0];
    private ViewTreeObserver.OnPreDrawListener A;

    /* renamed from: a  reason: collision with root package name */
    int f4236a = 0;
    Animator b;
    MotionSpec c;
    MotionSpec d;
    private MotionSpec e;
    private MotionSpec f;
    private final StateListAnimator g;
    ShadowDrawableWrapper h;
    private float i;
    Drawable j;
    Drawable k;
    CircularBorderDrawable l;
    Drawable m;
    float n;
    float o;
    float p;
    int q;
    float r = 1.0f;
    private ArrayList<Animator.AnimatorListener> s;
    private ArrayList<Animator.AnimatorListener> t;
    final VisibilityAwareImageButton u;
    final ShadowViewDelegate v;
    private final Rect w = new Rect();
    private final RectF x = new RectF();
    private final RectF y = new RectF();
    private final Matrix z = new Matrix();

    private class DisabledElevationAnimation extends ShadowAnimatorImpl {
        DisabledElevationAnimation(FloatingActionButtonImpl floatingActionButtonImpl) {
            super();
        }

        /* access modifiers changed from: protected */
        public float a() {
            return 0.0f;
        }
    }

    private class ElevateToHoveredFocusedTranslationZAnimation extends ShadowAnimatorImpl {
        ElevateToHoveredFocusedTranslationZAnimation() {
            super();
        }

        /* access modifiers changed from: protected */
        public float a() {
            FloatingActionButtonImpl floatingActionButtonImpl = FloatingActionButtonImpl.this;
            return floatingActionButtonImpl.n + floatingActionButtonImpl.o;
        }
    }

    private class ElevateToPressedTranslationZAnimation extends ShadowAnimatorImpl {
        ElevateToPressedTranslationZAnimation() {
            super();
        }

        /* access modifiers changed from: protected */
        public float a() {
            FloatingActionButtonImpl floatingActionButtonImpl = FloatingActionButtonImpl.this;
            return floatingActionButtonImpl.n + floatingActionButtonImpl.p;
        }
    }

    interface InternalVisibilityChangedListener {
        void a();

        void b();
    }

    private class ResetElevationAnimation extends ShadowAnimatorImpl {
        ResetElevationAnimation() {
            super();
        }

        /* access modifiers changed from: protected */
        public float a() {
            return FloatingActionButtonImpl.this.n;
        }
    }

    private abstract class ShadowAnimatorImpl extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        private boolean f4240a;
        private float b;
        private float c;

        private ShadowAnimatorImpl() {
        }

        /* access modifiers changed from: protected */
        public abstract float a();

        public void onAnimationEnd(Animator animator) {
            FloatingActionButtonImpl.this.h.b(this.c);
            this.f4240a = false;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.f4240a) {
                this.b = FloatingActionButtonImpl.this.h.b();
                this.c = a();
                this.f4240a = true;
            }
            ShadowDrawableWrapper shadowDrawableWrapper = FloatingActionButtonImpl.this.h;
            float f = this.b;
            shadowDrawableWrapper.b(f + ((this.c - f) * valueAnimator.getAnimatedFraction()));
        }
    }

    FloatingActionButtonImpl(VisibilityAwareImageButton visibilityAwareImageButton, ShadowViewDelegate shadowViewDelegate) {
        this.u = visibilityAwareImageButton;
        this.v = shadowViewDelegate;
        this.g = new StateListAnimator();
        this.g.a(C, a((ShadowAnimatorImpl) new ElevateToPressedTranslationZAnimation()));
        this.g.a(D, a((ShadowAnimatorImpl) new ElevateToHoveredFocusedTranslationZAnimation()));
        this.g.a(E, a((ShadowAnimatorImpl) new ElevateToHoveredFocusedTranslationZAnimation()));
        this.g.a(F, a((ShadowAnimatorImpl) new ElevateToHoveredFocusedTranslationZAnimation()));
        this.g.a(G, a((ShadowAnimatorImpl) new ResetElevationAnimation()));
        this.g.a(H, a((ShadowAnimatorImpl) new DisabledElevationAnimation(this)));
        this.i = this.u.getRotation();
    }

    private void t() {
        if (this.A == null) {
            this.A = new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    FloatingActionButtonImpl.this.p();
                    return true;
                }
            };
        }
    }

    private MotionSpec u() {
        if (this.f == null) {
            this.f = MotionSpec.a(this.u.getContext(), R$animator.design_fab_hide_motion_spec);
        }
        return this.f;
    }

    private MotionSpec v() {
        if (this.e == null) {
            this.e = MotionSpec.a(this.u.getContext(), R$animator.design_fab_show_motion_spec);
        }
        return this.e;
    }

    private boolean w() {
        return ViewCompat.E(this.u) && !this.u.isInEditMode();
    }

    private void x() {
        if (Build.VERSION.SDK_INT == 19) {
            if (this.i % 90.0f != 0.0f) {
                if (this.u.getLayerType() != 1) {
                    this.u.setLayerType(1, (Paint) null);
                }
            } else if (this.u.getLayerType() != 0) {
                this.u.setLayerType(0, (Paint) null);
            }
        }
        ShadowDrawableWrapper shadowDrawableWrapper = this.h;
        if (shadowDrawableWrapper != null) {
            shadowDrawableWrapper.a(-this.i);
        }
        CircularBorderDrawable circularBorderDrawable = this.l;
        if (circularBorderDrawable != null) {
            circularBorderDrawable.b(-this.i);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i2) {
        Drawable[] drawableArr;
        this.j = DrawableCompat.i(a());
        DrawableCompat.a(this.j, colorStateList);
        if (mode != null) {
            DrawableCompat.a(this.j, mode);
        }
        this.k = DrawableCompat.i(a());
        DrawableCompat.a(this.k, RippleUtils.a(colorStateList2));
        if (i2 > 0) {
            this.l = a(i2, colorStateList);
            drawableArr = new Drawable[]{this.l, this.j, this.k};
        } else {
            this.l = null;
            drawableArr = new Drawable[]{this.j, this.k};
        }
        this.m = new LayerDrawable(drawableArr);
        Context context = this.u.getContext();
        Drawable drawable = this.m;
        float b2 = this.v.b();
        float f2 = this.n;
        this.h = new ShadowDrawableWrapper(context, drawable, b2, f2, f2 + this.p);
        this.h.a(false);
        this.v.a(this.h);
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        Drawable drawable = this.k;
        if (drawable != null) {
            DrawableCompat.a(drawable, RippleUtils.a(colorStateList));
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Rect rect) {
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public final void d(float f2) {
        if (this.p != f2) {
            this.p = f2;
            a(this.n, this.o, this.p);
        }
    }

    /* access modifiers changed from: package-private */
    public float e() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public float f() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public final MotionSpec g() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        if (this.u.getVisibility() == 0) {
            if (this.f4236a == 1) {
                return true;
            }
            return false;
        } else if (this.f4236a != 2) {
            return true;
        } else {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        if (this.u.getVisibility() != 0) {
            if (this.f4236a == 2) {
                return true;
            }
            return false;
        } else if (this.f4236a != 1) {
            return true;
        } else {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.g.a();
    }

    /* access modifiers changed from: package-private */
    public CircularBorderDrawable k() {
        return new CircularBorderDrawable();
    }

    /* access modifiers changed from: package-private */
    public GradientDrawable l() {
        return new GradientDrawable();
    }

    /* access modifiers changed from: package-private */
    public void m() {
        if (q()) {
            t();
            this.u.getViewTreeObserver().addOnPreDrawListener(this.A);
        }
    }

    /* access modifiers changed from: package-private */
    public void n() {
    }

    /* access modifiers changed from: package-private */
    public void o() {
        if (this.A != null) {
            this.u.getViewTreeObserver().removeOnPreDrawListener(this.A);
            this.A = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void p() {
        float rotation = this.u.getRotation();
        if (this.i != rotation) {
            this.i = rotation;
            x();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void r() {
        c(this.r);
    }

    /* access modifiers changed from: package-private */
    public final void s() {
        Rect rect = this.w;
        a(rect);
        b(rect);
        this.v.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    /* access modifiers changed from: package-private */
    public final void c(float f2) {
        this.r = f2;
        Matrix matrix = this.z;
        a(f2, matrix);
        this.u.setImageMatrix(matrix);
    }

    /* access modifiers changed from: package-private */
    public final void b(float f2) {
        if (this.o != f2) {
            this.o = f2;
            a(this.n, this.o, this.p);
        }
    }

    /* access modifiers changed from: package-private */
    public final MotionSpec d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void d(Animator.AnimatorListener animatorListener) {
        ArrayList<Animator.AnimatorListener> arrayList = this.s;
        if (arrayList != null) {
            arrayList.remove(animatorListener);
        }
    }

    public void c(Animator.AnimatorListener animatorListener) {
        ArrayList<Animator.AnimatorListener> arrayList = this.t;
        if (arrayList != null) {
            arrayList.remove(animatorListener);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(MotionSpec motionSpec) {
        this.c = motionSpec;
    }

    /* access modifiers changed from: package-private */
    public void b(Animator.AnimatorListener animatorListener) {
        if (this.s == null) {
            this.s = new ArrayList<>();
        }
        this.s.add(animatorListener);
    }

    /* access modifiers changed from: package-private */
    public void b(final InternalVisibilityChangedListener internalVisibilityChangedListener, final boolean z2) {
        if (!i()) {
            Animator animator = this.b;
            if (animator != null) {
                animator.cancel();
            }
            if (w()) {
                if (this.u.getVisibility() != 0) {
                    this.u.setAlpha(0.0f);
                    this.u.setScaleY(0.0f);
                    this.u.setScaleX(0.0f);
                    c(0.0f);
                }
                MotionSpec motionSpec = this.c;
                if (motionSpec == null) {
                    motionSpec = v();
                }
                AnimatorSet a2 = a(motionSpec, 1.0f, 1.0f, 1.0f);
                a2.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        FloatingActionButtonImpl floatingActionButtonImpl = FloatingActionButtonImpl.this;
                        floatingActionButtonImpl.f4236a = 0;
                        floatingActionButtonImpl.b = null;
                        InternalVisibilityChangedListener internalVisibilityChangedListener = internalVisibilityChangedListener;
                        if (internalVisibilityChangedListener != null) {
                            internalVisibilityChangedListener.a();
                        }
                    }

                    public void onAnimationStart(Animator animator) {
                        FloatingActionButtonImpl.this.u.a(0, z2);
                        FloatingActionButtonImpl floatingActionButtonImpl = FloatingActionButtonImpl.this;
                        floatingActionButtonImpl.f4236a = 2;
                        floatingActionButtonImpl.b = animator;
                    }
                });
                ArrayList<Animator.AnimatorListener> arrayList = this.s;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        a2.addListener(it2.next());
                    }
                }
                a2.start();
                return;
            }
            this.u.a(0, z2);
            this.u.setAlpha(1.0f);
            this.u.setScaleY(1.0f);
            this.u.setScaleX(1.0f);
            c(1.0f);
            if (internalVisibilityChangedListener != null) {
                internalVisibilityChangedListener.a();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        Drawable drawable = this.j;
        if (drawable != null) {
            DrawableCompat.a(drawable, colorStateList);
        }
        CircularBorderDrawable circularBorderDrawable = this.l;
        if (circularBorderDrawable != null) {
            circularBorderDrawable.a(colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        Drawable drawable = this.j;
        if (drawable != null) {
            DrawableCompat.a(drawable, mode);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2) {
        if (this.n != f2) {
            this.n = f2;
            a(this.n, this.o, this.p);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.q != i2) {
            this.q = i2;
            r();
        }
    }

    private void a(float f2, Matrix matrix) {
        matrix.reset();
        Drawable drawable = this.u.getDrawable();
        if (drawable != null && this.q != 0) {
            RectF rectF = this.x;
            RectF rectF2 = this.y;
            rectF.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            int i2 = this.q;
            rectF2.set(0.0f, 0.0f, (float) i2, (float) i2);
            matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
            int i3 = this.q;
            matrix.postScale(f2, f2, ((float) i3) / 2.0f, ((float) i3) / 2.0f);
        }
    }

    /* access modifiers changed from: package-private */
    public final Drawable b() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final void a(MotionSpec motionSpec) {
        this.d = motionSpec;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, float f3, float f4) {
        ShadowDrawableWrapper shadowDrawableWrapper = this.h;
        if (shadowDrawableWrapper != null) {
            shadowDrawableWrapper.a(f2, this.p + f2);
            s();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr) {
        this.g.a(iArr);
    }

    public void a(Animator.AnimatorListener animatorListener) {
        if (this.t == null) {
            this.t = new ArrayList<>();
        }
        this.t.add(animatorListener);
    }

    /* access modifiers changed from: package-private */
    public void a(final InternalVisibilityChangedListener internalVisibilityChangedListener, final boolean z2) {
        if (!h()) {
            Animator animator = this.b;
            if (animator != null) {
                animator.cancel();
            }
            if (w()) {
                MotionSpec motionSpec = this.d;
                if (motionSpec == null) {
                    motionSpec = u();
                }
                AnimatorSet a2 = a(motionSpec, 0.0f, 0.0f, 0.0f);
                a2.addListener(new AnimatorListenerAdapter() {

                    /* renamed from: a  reason: collision with root package name */
                    private boolean f4237a;

                    public void onAnimationCancel(Animator animator) {
                        this.f4237a = true;
                    }

                    public void onAnimationEnd(Animator animator) {
                        FloatingActionButtonImpl floatingActionButtonImpl = FloatingActionButtonImpl.this;
                        floatingActionButtonImpl.f4236a = 0;
                        floatingActionButtonImpl.b = null;
                        if (!this.f4237a) {
                            floatingActionButtonImpl.u.a(z2 ? 8 : 4, z2);
                            InternalVisibilityChangedListener internalVisibilityChangedListener = internalVisibilityChangedListener;
                            if (internalVisibilityChangedListener != null) {
                                internalVisibilityChangedListener.b();
                            }
                        }
                    }

                    public void onAnimationStart(Animator animator) {
                        FloatingActionButtonImpl.this.u.a(0, z2);
                        FloatingActionButtonImpl floatingActionButtonImpl = FloatingActionButtonImpl.this;
                        floatingActionButtonImpl.f4236a = 1;
                        floatingActionButtonImpl.b = animator;
                        this.f4237a = false;
                    }
                });
                ArrayList<Animator.AnimatorListener> arrayList = this.t;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        a2.addListener(it2.next());
                    }
                }
                a2.start();
                return;
            }
            this.u.a(z2 ? 8 : 4, z2);
            if (internalVisibilityChangedListener != null) {
                internalVisibilityChangedListener.b();
            }
        }
    }

    private AnimatorSet a(MotionSpec motionSpec, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.u, View.ALPHA, new float[]{f2});
        motionSpec.a(ViewProps.OPACITY).a((Animator) ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.u, View.SCALE_X, new float[]{f3});
        motionSpec.a("scale").a((Animator) ofFloat2);
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.u, View.SCALE_Y, new float[]{f3});
        motionSpec.a("scale").a((Animator) ofFloat3);
        arrayList.add(ofFloat3);
        a(f4, this.z);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.u, new ImageMatrixProperty(), new MatrixEvaluator(), new Matrix[]{new Matrix(this.z)});
        motionSpec.a("iconScale").a((Animator) ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        AnimatorSetCompat.a(animatorSet, arrayList);
        return animatorSet;
    }

    /* access modifiers changed from: package-private */
    public void a(Rect rect) {
        this.h.getPadding(rect);
    }

    /* access modifiers changed from: package-private */
    public CircularBorderDrawable a(int i2, ColorStateList colorStateList) {
        Context context = this.u.getContext();
        CircularBorderDrawable k2 = k();
        k2.a(ContextCompat.a(context, R$color.design_fab_stroke_top_outer_color), ContextCompat.a(context, R$color.design_fab_stroke_top_inner_color), ContextCompat.a(context, R$color.design_fab_stroke_end_inner_color), ContextCompat.a(context, R$color.design_fab_stroke_end_outer_color));
        k2.a((float) i2);
        k2.a(colorStateList);
        return k2;
    }

    /* access modifiers changed from: package-private */
    public GradientDrawable a() {
        GradientDrawable l2 = l();
        l2.setShape(1);
        l2.setColor(-1);
        return l2;
    }

    private ValueAnimator a(ShadowAnimatorImpl shadowAnimatorImpl) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(B);
        valueAnimator.setDuration(100);
        valueAnimator.addListener(shadowAnimatorImpl);
        valueAnimator.addUpdateListener(shadowAnimatorImpl);
        valueAnimator.setFloatValues(new float[]{0.0f, 1.0f});
        return valueAnimator;
    }
}
