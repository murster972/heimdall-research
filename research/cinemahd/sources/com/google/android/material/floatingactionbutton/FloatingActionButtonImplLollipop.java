package com.google.android.material.floatingactionbutton;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import androidx.core.graphics.drawable.DrawableCompat;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.material.internal.CircularBorderDrawable;
import com.google.android.material.internal.CircularBorderDrawableLollipop;
import com.google.android.material.internal.VisibilityAwareImageButton;
import com.google.android.material.ripple.RippleUtils;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import com.google.android.material.shadow.ShadowViewDelegate;
import java.util.ArrayList;

class FloatingActionButtonImplLollipop extends FloatingActionButtonImpl {
    private InsetDrawable I;

    static class AlwaysStatefulGradientDrawable extends GradientDrawable {
        AlwaysStatefulGradientDrawable() {
        }

        public boolean isStateful() {
            return true;
        }
    }

    FloatingActionButtonImplLollipop(VisibilityAwareImageButton visibilityAwareImageButton, ShadowViewDelegate shadowViewDelegate) {
        super(visibilityAwareImageButton, shadowViewDelegate);
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i) {
        Drawable drawable;
        this.j = DrawableCompat.i(a());
        DrawableCompat.a(this.j, colorStateList);
        if (mode != null) {
            DrawableCompat.a(this.j, mode);
        }
        if (i > 0) {
            this.l = a(i, colorStateList);
            drawable = new LayerDrawable(new Drawable[]{this.l, this.j});
        } else {
            this.l = null;
            drawable = this.j;
        }
        this.k = new RippleDrawable(RippleUtils.a(colorStateList2), drawable, (Drawable) null);
        Drawable drawable2 = this.k;
        this.m = drawable2;
        this.v.a(drawable2);
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        Drawable drawable = this.k;
        if (drawable instanceof RippleDrawable) {
            ((RippleDrawable) drawable).setColor(RippleUtils.a(colorStateList));
        } else {
            super.b(colorStateList);
        }
    }

    public float c() {
        return this.u.getElevation();
    }

    /* access modifiers changed from: package-private */
    public void j() {
    }

    /* access modifiers changed from: package-private */
    public CircularBorderDrawable k() {
        return new CircularBorderDrawableLollipop();
    }

    /* access modifiers changed from: package-private */
    public GradientDrawable l() {
        return new AlwaysStatefulGradientDrawable();
    }

    /* access modifiers changed from: package-private */
    public void n() {
        s();
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void b(Rect rect) {
        if (this.v.a()) {
            this.I = new InsetDrawable(this.k, rect.left, rect.top, rect.right, rect.bottom);
            this.v.a(this.I);
            return;
        }
        this.v.a(this.k);
    }

    /* access modifiers changed from: package-private */
    public void a(float f, float f2, float f3) {
        if (Build.VERSION.SDK_INT == 21) {
            this.u.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(FloatingActionButtonImpl.C, a(f, f3));
            stateListAnimator.addState(FloatingActionButtonImpl.D, a(f, f2));
            stateListAnimator.addState(FloatingActionButtonImpl.E, a(f, f2));
            stateListAnimator.addState(FloatingActionButtonImpl.F, a(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(this.u, ViewProps.ELEVATION, new float[]{f}).setDuration(0));
            int i = Build.VERSION.SDK_INT;
            if (i >= 22 && i <= 24) {
                VisibilityAwareImageButton visibilityAwareImageButton = this.u;
                arrayList.add(ObjectAnimator.ofFloat(visibilityAwareImageButton, View.TRANSLATION_Z, new float[]{visibilityAwareImageButton.getTranslationZ()}).setDuration(100));
            }
            arrayList.add(ObjectAnimator.ofFloat(this.u, View.TRANSLATION_Z, new float[]{0.0f}).setDuration(100));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(FloatingActionButtonImpl.B);
            stateListAnimator.addState(FloatingActionButtonImpl.G, animatorSet);
            stateListAnimator.addState(FloatingActionButtonImpl.H, a(0.0f, 0.0f));
            this.u.setStateListAnimator(stateListAnimator);
        }
        if (this.v.a()) {
            s();
        }
    }

    private Animator a(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(this.u, ViewProps.ELEVATION, new float[]{f}).setDuration(0)).with(ObjectAnimator.ofFloat(this.u, View.TRANSLATION_Z, new float[]{f2}).setDuration(100));
        animatorSet.setInterpolator(FloatingActionButtonImpl.B);
        return animatorSet;
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr) {
        if (Build.VERSION.SDK_INT != 21) {
            return;
        }
        if (this.u.isEnabled()) {
            this.u.setElevation(this.n);
            if (this.u.isPressed()) {
                this.u.setTranslationZ(this.p);
            } else if (this.u.isFocused() || this.u.isHovered()) {
                this.u.setTranslationZ(this.o);
            } else {
                this.u.setTranslationZ(0.0f);
            }
        } else {
            this.u.setElevation(0.0f);
            this.u.setTranslationZ(0.0f);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Rect rect) {
        if (this.v.a()) {
            float b = this.v.b();
            float c = c() + this.p;
            int ceil = (int) Math.ceil((double) ShadowDrawableWrapper.a(c, b, false));
            int ceil2 = (int) Math.ceil((double) ShadowDrawableWrapper.b(c, b, false));
            rect.set(ceil, ceil2, ceil, ceil2);
            return;
        }
        rect.set(0, 0, 0, 0);
    }
}
