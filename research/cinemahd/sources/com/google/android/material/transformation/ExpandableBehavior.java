package com.google.android.material.transformation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.expandable.ExpandableWidget;
import java.util.List;

public abstract class ExpandableBehavior extends CoordinatorLayout.Behavior<View> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f4311a = 0;

    public ExpandableBehavior() {
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(View view, View view2, boolean z, boolean z2);

    public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, View view, View view2) {
        ExpandableWidget expandableWidget = (ExpandableWidget) view2;
        if (!a(expandableWidget.a())) {
            return false;
        }
        this.f4311a = expandableWidget.a() ? 1 : 2;
        return a((View) expandableWidget, view, expandableWidget.a(), true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r3 = a(r3, r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onLayoutChild(androidx.coordinatorlayout.widget.CoordinatorLayout r3, final android.view.View r4, int r5) {
        /*
            r2 = this;
            boolean r5 = androidx.core.view.ViewCompat.E(r4)
            if (r5 != 0) goto L_0x002f
            com.google.android.material.expandable.ExpandableWidget r3 = r2.a(r3, r4)
            if (r3 == 0) goto L_0x002f
            boolean r5 = r3.a()
            boolean r5 = r2.a((boolean) r5)
            if (r5 == 0) goto L_0x002f
            boolean r5 = r3.a()
            if (r5 == 0) goto L_0x001e
            r5 = 1
            goto L_0x001f
        L_0x001e:
            r5 = 2
        L_0x001f:
            r2.f4311a = r5
            int r5 = r2.f4311a
            android.view.ViewTreeObserver r0 = r4.getViewTreeObserver()
            com.google.android.material.transformation.ExpandableBehavior$1 r1 = new com.google.android.material.transformation.ExpandableBehavior$1
            r1.<init>(r4, r5, r3)
            r0.addOnPreDrawListener(r1)
        L_0x002f:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.transformation.ExpandableBehavior.onLayoutChild(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int):boolean");
    }

    /* access modifiers changed from: protected */
    public ExpandableWidget a(CoordinatorLayout coordinatorLayout, View view) {
        List<View> b = coordinatorLayout.b(view);
        int size = b.size();
        for (int i = 0; i < size; i++) {
            View view2 = b.get(i);
            if (layoutDependsOn(coordinatorLayout, view, view2)) {
                return (ExpandableWidget) view2;
            }
        }
        return null;
    }

    public ExpandableBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private boolean a(boolean z) {
        if (z) {
            int i = this.f4311a;
            return i == 0 || i == 2;
        } else if (this.f4311a == 1) {
            return true;
        } else {
            return false;
        }
    }
}
