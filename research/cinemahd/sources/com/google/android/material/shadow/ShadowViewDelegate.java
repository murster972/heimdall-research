package com.google.android.material.shadow;

import android.graphics.drawable.Drawable;

public interface ShadowViewDelegate {
    void a(int i, int i2, int i3, int i4);

    void a(Drawable drawable);

    boolean a();

    float b();
}
