package com.google.android.material.card;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.cardview.widget.CardView;
import com.google.android.material.R$style;
import com.google.android.material.R$styleable;
import com.google.android.material.internal.ThemeEnforcement;

public class MaterialCardView extends CardView {
    private final MaterialCardViewHelper j = new MaterialCardViewHelper(this);

    public MaterialCardView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray c = ThemeEnforcement.c(context, attributeSet, R$styleable.MaterialCardView, i, R$style.Widget_MaterialComponents_CardView, new int[0]);
        this.j.a(c);
        c.recycle();
    }

    public int getStrokeColor() {
        return this.j.a();
    }

    public int getStrokeWidth() {
        return this.j.b();
    }

    public void setRadius(float f) {
        super.setRadius(f);
        this.j.c();
    }

    public void setStrokeColor(int i) {
        this.j.a(i);
    }

    public void setStrokeWidth(int i) {
        this.j.b(i);
    }
}
