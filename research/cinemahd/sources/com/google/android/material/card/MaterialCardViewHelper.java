package com.google.android.material.card;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import com.google.android.material.R$styleable;

class MaterialCardViewHelper {

    /* renamed from: a  reason: collision with root package name */
    private final MaterialCardView f4217a;
    private int b;
    private int c;

    public MaterialCardViewHelper(MaterialCardView materialCardView) {
        this.f4217a = materialCardView;
    }

    private void d() {
        this.f4217a.a(this.f4217a.getContentPaddingLeft() + this.c, this.f4217a.getContentPaddingTop() + this.c, this.f4217a.getContentPaddingRight() + this.c, this.f4217a.getContentPaddingBottom() + this.c);
    }

    private Drawable e() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(this.f4217a.getRadius());
        int i = this.b;
        if (i != -1) {
            gradientDrawable.setStroke(this.c, i);
        }
        return gradientDrawable;
    }

    public void a(TypedArray typedArray) {
        this.b = typedArray.getColor(R$styleable.MaterialCardView_strokeColor, -1);
        this.c = typedArray.getDimensionPixelSize(R$styleable.MaterialCardView_strokeWidth, 0);
        c();
        d();
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        this.c = i;
        c();
        d();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.f4217a.setForeground(e());
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.b = i;
        c();
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }
}
