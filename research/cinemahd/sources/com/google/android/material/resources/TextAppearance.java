package com.google.android.material.resources;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextPaint;
import android.util.Log;
import androidx.core.content.res.ResourcesCompat;
import com.google.android.material.R$styleable;

public class TextAppearance {

    /* renamed from: a  reason: collision with root package name */
    public final float f4264a;
    public final ColorStateList b;
    public final int c;
    public final int d;
    public final String e;
    public final ColorStateList f;
    public final float g;
    public final float h;
    public final float i;
    private final int j;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public Typeface l;

    public TextAppearance(Context context, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, R$styleable.TextAppearance);
        this.f4264a = obtainStyledAttributes.getDimension(R$styleable.TextAppearance_android_textSize, 0.0f);
        this.b = MaterialResources.a(context, obtainStyledAttributes, R$styleable.TextAppearance_android_textColor);
        MaterialResources.a(context, obtainStyledAttributes, R$styleable.TextAppearance_android_textColorHint);
        MaterialResources.a(context, obtainStyledAttributes, R$styleable.TextAppearance_android_textColorLink);
        this.c = obtainStyledAttributes.getInt(R$styleable.TextAppearance_android_textStyle, 0);
        this.d = obtainStyledAttributes.getInt(R$styleable.TextAppearance_android_typeface, 1);
        int a2 = MaterialResources.a(obtainStyledAttributes, R$styleable.TextAppearance_fontFamily, R$styleable.TextAppearance_android_fontFamily);
        this.j = obtainStyledAttributes.getResourceId(a2, 0);
        this.e = obtainStyledAttributes.getString(a2);
        obtainStyledAttributes.getBoolean(R$styleable.TextAppearance_textAllCaps, false);
        this.f = MaterialResources.a(context, obtainStyledAttributes, R$styleable.TextAppearance_android_shadowColor);
        this.g = obtainStyledAttributes.getFloat(R$styleable.TextAppearance_android_shadowDx, 0.0f);
        this.h = obtainStyledAttributes.getFloat(R$styleable.TextAppearance_android_shadowDy, 0.0f);
        this.i = obtainStyledAttributes.getFloat(R$styleable.TextAppearance_android_shadowRadius, 0.0f);
        obtainStyledAttributes.recycle();
    }

    public void b(Context context, TextPaint textPaint, ResourcesCompat.FontCallback fontCallback) {
        c(context, textPaint, fontCallback);
        ColorStateList colorStateList = this.b;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f2 = this.i;
        float f3 = this.g;
        float f4 = this.h;
        ColorStateList colorStateList2 = this.f;
        textPaint.setShadowLayer(f2, f3, f4, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    public void c(Context context, TextPaint textPaint, ResourcesCompat.FontCallback fontCallback) {
        if (TextAppearanceConfig.a()) {
            a(textPaint, a(context));
            return;
        }
        a(context, textPaint, fontCallback);
        if (!this.k) {
            a(textPaint, this.l);
        }
    }

    public Typeface a(Context context) {
        if (this.k) {
            return this.l;
        }
        if (!context.isRestricted()) {
            try {
                this.l = ResourcesCompat.a(context, this.j);
                if (this.l != null) {
                    this.l = Typeface.create(this.l, this.c);
                }
            } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
            } catch (Exception e2) {
                Log.d("TextAppearance", "Error loading font " + this.e, e2);
            }
        }
        a();
        this.k = true;
        return this.l;
    }

    public void a(Context context, final TextPaint textPaint, final ResourcesCompat.FontCallback fontCallback) {
        if (this.k) {
            a(textPaint, this.l);
            return;
        }
        a();
        if (context.isRestricted()) {
            this.k = true;
            a(textPaint, this.l);
            return;
        }
        try {
            ResourcesCompat.a(context, this.j, new ResourcesCompat.FontCallback() {
                public void a(Typeface typeface) {
                    TextAppearance textAppearance = TextAppearance.this;
                    Typeface unused = textAppearance.l = Typeface.create(typeface, textAppearance.c);
                    TextAppearance.this.a(textPaint, typeface);
                    boolean unused2 = TextAppearance.this.k = true;
                    fontCallback.a(typeface);
                }

                public void a(int i) {
                    TextAppearance.this.a();
                    boolean unused = TextAppearance.this.k = true;
                    fontCallback.a(i);
                }
            }, (Handler) null);
        } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
        } catch (Exception e2) {
            Log.d("TextAppearance", "Error loading font " + this.e, e2);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.l == null) {
            this.l = Typeface.create(this.e, this.c);
        }
        if (this.l == null) {
            int i2 = this.d;
            if (i2 == 1) {
                this.l = Typeface.SANS_SERIF;
            } else if (i2 == 2) {
                this.l = Typeface.SERIF;
            } else if (i2 != 3) {
                this.l = Typeface.DEFAULT;
            } else {
                this.l = Typeface.MONOSPACE;
            }
            Typeface typeface = this.l;
            if (typeface != null) {
                this.l = Typeface.create(typeface, this.c);
            }
        }
    }

    public void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int i2 = (~typeface.getStyle()) & this.c;
        textPaint.setFakeBoldText((i2 & 1) != 0);
        textPaint.setTextSkewX((i2 & 2) != 0 ? -0.25f : 0.0f);
        textPaint.setTextSize(this.f4264a);
    }
}
