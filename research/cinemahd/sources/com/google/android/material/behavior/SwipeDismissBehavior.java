package com.google.android.material.behavior;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;

public class SwipeDismissBehavior<V extends View> extends CoordinatorLayout.Behavior<V> {

    /* renamed from: a  reason: collision with root package name */
    ViewDragHelper f4191a;
    OnDismissListener b;
    private boolean c;
    private float d = 0.0f;
    private boolean e;
    int f = 2;
    float g = 0.5f;
    float h = 0.0f;
    float i = 0.5f;
    private final ViewDragHelper.Callback j = new ViewDragHelper.Callback() {

        /* renamed from: a  reason: collision with root package name */
        private int f4192a;
        private int b = -1;

        public void a(View view, int i) {
            this.b = i;
            this.f4192a = view.getLeft();
            ViewParent parent = view.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }

        public boolean b(View view, int i) {
            return this.b == -1 && SwipeDismissBehavior.this.a(view);
        }

        public void c(int i) {
            OnDismissListener onDismissListener = SwipeDismissBehavior.this.b;
            if (onDismissListener != null) {
                onDismissListener.a(i);
            }
        }

        public int b(View view, int i, int i2) {
            return view.getTop();
        }

        public void a(View view, float f, float f2) {
            boolean z;
            int i;
            OnDismissListener onDismissListener;
            this.b = -1;
            int width = view.getWidth();
            if (a(view, f)) {
                int left = view.getLeft();
                int i2 = this.f4192a;
                i = left < i2 ? i2 - width : i2 + width;
                z = true;
            } else {
                i = this.f4192a;
                z = false;
            }
            if (SwipeDismissBehavior.this.f4191a.d(i, view.getTop())) {
                ViewCompat.a(view, (Runnable) new SettleRunnable(view, z));
            } else if (z && (onDismissListener = SwipeDismissBehavior.this.b) != null) {
                onDismissListener.a(view);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x0023 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0030 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean a(android.view.View r7, float r8) {
            /*
                r6 = this;
                r0 = 0
                r1 = 0
                r2 = 1
                int r3 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r3 == 0) goto L_0x0032
                int r7 = androidx.core.view.ViewCompat.m(r7)
                if (r7 != r2) goto L_0x000f
                r7 = 1
                goto L_0x0010
            L_0x000f:
                r7 = 0
            L_0x0010:
                com.google.android.material.behavior.SwipeDismissBehavior r4 = com.google.android.material.behavior.SwipeDismissBehavior.this
                int r4 = r4.f
                r5 = 2
                if (r4 != r5) goto L_0x0018
                return r2
            L_0x0018:
                if (r4 != 0) goto L_0x0025
                if (r7 == 0) goto L_0x0021
                int r7 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r7 >= 0) goto L_0x0024
                goto L_0x0023
            L_0x0021:
                if (r3 <= 0) goto L_0x0024
            L_0x0023:
                r1 = 1
            L_0x0024:
                return r1
            L_0x0025:
                if (r4 != r2) goto L_0x0031
                if (r7 == 0) goto L_0x002c
                if (r3 <= 0) goto L_0x0031
                goto L_0x0030
            L_0x002c:
                int r7 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r7 >= 0) goto L_0x0031
            L_0x0030:
                r1 = 1
            L_0x0031:
                return r1
            L_0x0032:
                int r8 = r7.getLeft()
                int r0 = r6.f4192a
                int r8 = r8 - r0
                int r7 = r7.getWidth()
                float r7 = (float) r7
                com.google.android.material.behavior.SwipeDismissBehavior r0 = com.google.android.material.behavior.SwipeDismissBehavior.this
                float r0 = r0.g
                float r7 = r7 * r0
                int r7 = java.lang.Math.round(r7)
                int r8 = java.lang.Math.abs(r8)
                if (r8 < r7) goto L_0x004f
                r1 = 1
            L_0x004f:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.behavior.SwipeDismissBehavior.AnonymousClass1.a(android.view.View, float):boolean");
        }

        public int a(View view) {
            return view.getWidth();
        }

        public int a(View view, int i, int i2) {
            int i3;
            int i4;
            int width;
            boolean z = ViewCompat.m(view) == 1;
            int i5 = SwipeDismissBehavior.this.f;
            if (i5 != 0) {
                if (i5 != 1) {
                    i3 = this.f4192a - view.getWidth();
                    i4 = view.getWidth() + this.f4192a;
                } else if (z) {
                    i3 = this.f4192a;
                    width = view.getWidth();
                } else {
                    i3 = this.f4192a - view.getWidth();
                    i4 = this.f4192a;
                }
                return SwipeDismissBehavior.a(i3, i, i4);
            } else if (z) {
                i3 = this.f4192a - view.getWidth();
                i4 = this.f4192a;
                return SwipeDismissBehavior.a(i3, i, i4);
            } else {
                i3 = this.f4192a;
                width = view.getWidth();
            }
            i4 = width + i3;
            return SwipeDismissBehavior.a(i3, i, i4);
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            float width = ((float) this.f4192a) + (((float) view.getWidth()) * SwipeDismissBehavior.this.h);
            float width2 = ((float) this.f4192a) + (((float) view.getWidth()) * SwipeDismissBehavior.this.i);
            float f = (float) i;
            if (f <= width) {
                view.setAlpha(1.0f);
            } else if (f >= width2) {
                view.setAlpha(0.0f);
            } else {
                view.setAlpha(SwipeDismissBehavior.a(0.0f, 1.0f - SwipeDismissBehavior.b(width, width2, f), 1.0f));
            }
        }
    };

    public interface OnDismissListener {
        void a(int i);

        void a(View view);
    }

    private class SettleRunnable implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final View f4193a;
        private final boolean b;

        SettleRunnable(View view, boolean z) {
            this.f4193a = view;
            this.b = z;
        }

        public void run() {
            OnDismissListener onDismissListener;
            ViewDragHelper viewDragHelper = SwipeDismissBehavior.this.f4191a;
            if (viewDragHelper != null && viewDragHelper.a(true)) {
                ViewCompat.a(this.f4193a, (Runnable) this);
            } else if (this.b && (onDismissListener = SwipeDismissBehavior.this.b) != null) {
                onDismissListener.a(this.f4193a);
            }
        }
    }

    static float b(float f2, float f3, float f4) {
        return (f4 - f2) / (f3 - f2);
    }

    public void a(OnDismissListener onDismissListener) {
        this.b = onDismissListener;
    }

    public boolean a(View view) {
        return true;
    }

    public void b(float f2) {
        this.h = a(0.0f, f2, 1.0f);
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        boolean z = this.c;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.c = coordinatorLayout.a((View) v, (int) motionEvent.getX(), (int) motionEvent.getY());
            z = this.c;
        } else if (actionMasked == 1 || actionMasked == 3) {
            this.c = false;
        }
        if (!z) {
            return false;
        }
        a((ViewGroup) coordinatorLayout);
        return this.f4191a.b(motionEvent);
    }

    public boolean onTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        ViewDragHelper viewDragHelper = this.f4191a;
        if (viewDragHelper == null) {
            return false;
        }
        viewDragHelper.a(motionEvent);
        return true;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public void a(float f2) {
        this.i = a(0.0f, f2, 1.0f);
    }

    private void a(ViewGroup viewGroup) {
        ViewDragHelper viewDragHelper;
        if (this.f4191a == null) {
            if (this.e) {
                viewDragHelper = ViewDragHelper.a(viewGroup, this.d, this.j);
            } else {
                viewDragHelper = ViewDragHelper.a(viewGroup, this.j);
            }
            this.f4191a = viewDragHelper;
        }
    }

    static float a(float f2, float f3, float f4) {
        return Math.min(Math.max(f2, f3), f4);
    }

    static int a(int i2, int i3, int i4) {
        return Math.min(Math.max(i2, i3), i4);
    }

    public int a() {
        ViewDragHelper viewDragHelper = this.f4191a;
        if (viewDragHelper != null) {
            return viewDragHelper.f();
        }
        return 0;
    }
}
