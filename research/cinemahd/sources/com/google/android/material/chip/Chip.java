package com.google.android.material.chip;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.text.BidiFormatter;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.customview.widget.ExploreByTouchHelper;
import com.facebook.ads.AdError;
import com.facebook.react.modules.appstate.AppStateModule;
import com.google.android.material.R$string;
import com.google.android.material.R$style;
import com.google.android.material.animation.MotionSpec;
import com.google.android.material.chip.ChipDrawable;
import com.google.android.material.resources.TextAppearance;
import com.google.android.material.ripple.RippleUtils;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class Chip extends AppCompatCheckBox implements ChipDrawable.Delegate {
    /* access modifiers changed from: private */
    public static final Rect n = new Rect();
    private static final int[] o = {16842913};
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ChipDrawable f4218a;
    private RippleDrawable b;
    private View.OnClickListener c;
    private CompoundButton.OnCheckedChangeListener d;
    private boolean e;
    private int f = Integer.MIN_VALUE;
    private boolean g;
    private boolean h;
    private boolean i;
    private final ChipTouchHelper j;
    private final Rect k = new Rect();
    private final RectF l = new RectF();
    private final ResourcesCompat.FontCallback m = new ResourcesCompat.FontCallback() {
        public void a(int i) {
        }

        public void a(Typeface typeface) {
            Chip chip = Chip.this;
            chip.setText(chip.getText());
            Chip.this.requestLayout();
            Chip.this.invalidate();
        }
    };

    private class ChipTouchHelper extends ExploreByTouchHelper {
        ChipTouchHelper(Chip chip) {
            super(chip);
        }

        /* access modifiers changed from: protected */
        public int a(float f, float f2) {
            return (!Chip.this.f() || !Chip.this.getCloseIconTouchBounds().contains(f, f2)) ? -1 : 0;
        }

        /* access modifiers changed from: protected */
        public void a(List<Integer> list) {
            if (Chip.this.f()) {
                list.add(0);
            }
        }

        /* access modifiers changed from: protected */
        public void a(int i, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            if (Chip.this.f()) {
                CharSequence closeIconContentDescription = Chip.this.getCloseIconContentDescription();
                if (closeIconContentDescription != null) {
                    accessibilityNodeInfoCompat.b(closeIconContentDescription);
                } else {
                    CharSequence text = Chip.this.getText();
                    Context context = Chip.this.getContext();
                    int i2 = R$string.mtrl_chip_close_icon_content_description;
                    Object[] objArr = new Object[1];
                    if (TextUtils.isEmpty(text)) {
                        text = "";
                    }
                    objArr[0] = text;
                    accessibilityNodeInfoCompat.b((CharSequence) context.getString(i2, objArr).trim());
                }
                accessibilityNodeInfoCompat.c(Chip.this.getCloseIconTouchBoundsInt());
                accessibilityNodeInfoCompat.a(AccessibilityNodeInfoCompat.AccessibilityActionCompat.f);
                accessibilityNodeInfoCompat.h(Chip.this.isEnabled());
                return;
            }
            accessibilityNodeInfoCompat.b((CharSequence) "");
            accessibilityNodeInfoCompat.c(Chip.n);
        }

        /* access modifiers changed from: protected */
        public void a(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            accessibilityNodeInfoCompat.c(Chip.this.f4218a != null && Chip.this.f4218a.D());
            accessibilityNodeInfoCompat.a((CharSequence) Chip.class.getName());
            CharSequence text = Chip.this.getText();
            if (Build.VERSION.SDK_INT >= 23) {
                accessibilityNodeInfoCompat.h(text);
            } else {
                accessibilityNodeInfoCompat.b(text);
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(int i, int i2, Bundle bundle) {
            if (i2 == 16 && i == 0) {
                return Chip.this.b();
            }
            return false;
        }
    }

    public Chip(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
        ChipDrawable a2 = ChipDrawable.a(context, attributeSet, i2, R$style.Widget_MaterialComponents_Chip_Action);
        setChipDrawable(a2);
        this.j = new ChipTouchHelper(this);
        ViewCompat.a((View) this, (AccessibilityDelegateCompat) this.j);
        g();
        setChecked(this.e);
        a2.e(false);
        setText(a2.z());
        setEllipsize(a2.t());
        setIncludeFontPadding(false);
        if (getTextAppearance() != null) {
            a(getTextAppearance());
        }
        setSingleLine();
        setGravity(8388627);
        h();
    }

    private void e() {
        if (this.f == Integer.MIN_VALUE) {
            setFocusedVirtualView(-1);
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        ChipDrawable chipDrawable = this.f4218a;
        return (chipDrawable == null || chipDrawable.m() == null) ? false : true;
    }

    private void g() {
        if (Build.VERSION.SDK_INT >= 21) {
            setOutlineProvider(new ViewOutlineProvider() {
                @TargetApi(21)
                public void getOutline(View view, Outline outline) {
                    if (Chip.this.f4218a != null) {
                        Chip.this.f4218a.getOutline(outline);
                    } else {
                        outline.setAlpha(0.0f);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public RectF getCloseIconTouchBounds() {
        this.l.setEmpty();
        if (f()) {
            this.f4218a.a(this.l);
        }
        return this.l;
    }

    /* access modifiers changed from: private */
    public Rect getCloseIconTouchBoundsInt() {
        RectF closeIconTouchBounds = getCloseIconTouchBounds();
        this.k.set((int) closeIconTouchBounds.left, (int) closeIconTouchBounds.top, (int) closeIconTouchBounds.right, (int) closeIconTouchBounds.bottom);
        return this.k;
    }

    private TextAppearance getTextAppearance() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.A();
        }
        return null;
    }

    private void h() {
        ChipDrawable chipDrawable;
        if (!TextUtils.isEmpty(getText()) && (chipDrawable = this.f4218a) != null) {
            float j2 = chipDrawable.j() + this.f4218a.e() + this.f4218a.C() + this.f4218a.B();
            if ((this.f4218a.F() && this.f4218a.f() != null) || (this.f4218a.b() != null && this.f4218a.E() && isChecked())) {
                j2 += this.f4218a.w() + this.f4218a.v() + this.f4218a.g();
            }
            if (this.f4218a.H() && this.f4218a.m() != null) {
                j2 += this.f4218a.q() + this.f4218a.o() + this.f4218a.p();
            }
            if (((float) ViewCompat.p(this)) != j2) {
                ViewCompat.b(this, ViewCompat.q(this), getPaddingTop(), (int) j2, getPaddingBottom());
            }
        }
    }

    private void setCloseIconFocused(boolean z) {
        if (this.i != z) {
            this.i = z;
            refreshDrawableState();
        }
    }

    private void setCloseIconHovered(boolean z) {
        if (this.h != z) {
            this.h = z;
            refreshDrawableState();
        }
    }

    private void setCloseIconPressed(boolean z) {
        if (this.g != z) {
            this.g = z;
            refreshDrawableState();
        }
    }

    private void setFocusedVirtualView(int i2) {
        int i3 = this.f;
        if (i3 != i2) {
            if (i3 == 0) {
                setCloseIconFocused(false);
            }
            this.f = i2;
            if (i2 == 0) {
                setCloseIconFocused(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        return a(motionEvent) || this.j.a(motionEvent) || super.dispatchHoverEvent(motionEvent);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.j.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        ChipDrawable chipDrawable = this.f4218a;
        if ((chipDrawable == null || !chipDrawable.G()) ? false : this.f4218a.a(d())) {
            invalidate();
        }
    }

    public Drawable getCheckedIcon() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.b();
        }
        return null;
    }

    public ColorStateList getChipBackgroundColor() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.c();
        }
        return null;
    }

    public float getChipCornerRadius() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.d();
        }
        return 0.0f;
    }

    public Drawable getChipDrawable() {
        return this.f4218a;
    }

    public float getChipEndPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.e();
        }
        return 0.0f;
    }

    public Drawable getChipIcon() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.f();
        }
        return null;
    }

    public float getChipIconSize() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.g();
        }
        return 0.0f;
    }

    public ColorStateList getChipIconTint() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.h();
        }
        return null;
    }

    public float getChipMinHeight() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.i();
        }
        return 0.0f;
    }

    public float getChipStartPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.j();
        }
        return 0.0f;
    }

    public ColorStateList getChipStrokeColor() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.k();
        }
        return null;
    }

    public float getChipStrokeWidth() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.l();
        }
        return 0.0f;
    }

    @Deprecated
    public CharSequence getChipText() {
        return getText();
    }

    public Drawable getCloseIcon() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.m();
        }
        return null;
    }

    public CharSequence getCloseIconContentDescription() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.n();
        }
        return null;
    }

    public float getCloseIconEndPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.o();
        }
        return 0.0f;
    }

    public float getCloseIconSize() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.p();
        }
        return 0.0f;
    }

    public float getCloseIconStartPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.q();
        }
        return 0.0f;
    }

    public ColorStateList getCloseIconTint() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.s();
        }
        return null;
    }

    public TextUtils.TruncateAt getEllipsize() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.t();
        }
        return null;
    }

    public void getFocusedRect(Rect rect) {
        if (this.f == 0) {
            rect.set(getCloseIconTouchBoundsInt());
        } else {
            super.getFocusedRect(rect);
        }
    }

    public MotionSpec getHideMotionSpec() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.u();
        }
        return null;
    }

    public float getIconEndPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.v();
        }
        return 0.0f;
    }

    public float getIconStartPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.w();
        }
        return 0.0f;
    }

    public ColorStateList getRippleColor() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.x();
        }
        return null;
    }

    public MotionSpec getShowMotionSpec() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.y();
        }
        return null;
    }

    public CharSequence getText() {
        ChipDrawable chipDrawable = this.f4218a;
        return chipDrawable != null ? chipDrawable.z() : "";
    }

    public float getTextEndPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.B();
        }
        return 0.0f;
    }

    public float getTextStartPadding() {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            return chipDrawable.C();
        }
        return 0.0f;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 1);
        if (isChecked()) {
            CheckBox.mergeDrawableStates(onCreateDrawableState, o);
        }
        return onCreateDrawableState;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        ChipDrawable chipDrawable;
        if (TextUtils.isEmpty(getText()) || (chipDrawable = this.f4218a) == null || chipDrawable.J()) {
            super.onDraw(canvas);
            return;
        }
        int save = canvas.save();
        canvas.translate(b(this.f4218a), 0.0f);
        super.onDraw(canvas);
        canvas.restoreToCount(save);
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i2, Rect rect) {
        if (z) {
            setFocusedVirtualView(-1);
        } else {
            setFocusedVirtualView(Integer.MIN_VALUE);
        }
        invalidate();
        super.onFocusChanged(z, i2, rect);
        this.j.a(z, i2, rect);
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 7) {
            setCloseIconHovered(getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()));
        } else if (actionMasked == 10) {
            setCloseIconHovered(false);
        }
        return super.onHoverEvent(motionEvent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onKeyDown(int r7, android.view.KeyEvent r8) {
        /*
            r6 = this;
            int r0 = r8.getKeyCode()
            r1 = 61
            r2 = 0
            r3 = 1
            if (r0 == r1) goto L_0x0041
            r1 = 66
            if (r0 == r1) goto L_0x0031
            switch(r0) {
                case 21: goto L_0x0022;
                case 22: goto L_0x0012;
                case 23: goto L_0x0031;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x006d
        L_0x0012:
            boolean r0 = r8.hasNoModifiers()
            if (r0 == 0) goto L_0x006d
            boolean r0 = com.google.android.material.internal.ViewUtils.a(r6)
            r0 = r0 ^ r3
            boolean r2 = r6.a((boolean) r0)
            goto L_0x006d
        L_0x0022:
            boolean r0 = r8.hasNoModifiers()
            if (r0 == 0) goto L_0x006d
            boolean r0 = com.google.android.material.internal.ViewUtils.a(r6)
            boolean r2 = r6.a((boolean) r0)
            goto L_0x006d
        L_0x0031:
            int r0 = r6.f
            r1 = -1
            if (r0 == r1) goto L_0x003d
            if (r0 == 0) goto L_0x0039
            goto L_0x006d
        L_0x0039:
            r6.b()
            return r3
        L_0x003d:
            r6.performClick()
            return r3
        L_0x0041:
            boolean r0 = r8.hasNoModifiers()
            if (r0 == 0) goto L_0x0049
            r0 = 2
            goto L_0x0052
        L_0x0049:
            boolean r0 = r8.hasModifiers(r3)
            if (r0 == 0) goto L_0x0051
            r0 = 1
            goto L_0x0052
        L_0x0051:
            r0 = 0
        L_0x0052:
            if (r0 == 0) goto L_0x006d
            android.view.ViewParent r1 = r6.getParent()
            r4 = r6
        L_0x0059:
            android.view.View r4 = r4.focusSearch(r0)
            if (r4 == 0) goto L_0x0067
            if (r4 == r6) goto L_0x0067
            android.view.ViewParent r5 = r4.getParent()
            if (r5 == r1) goto L_0x0059
        L_0x0067:
            if (r4 == 0) goto L_0x006d
            r4.requestFocus()
            return r3
        L_0x006d:
            if (r2 == 0) goto L_0x0073
            r6.invalidate()
            return r3
        L_0x0073:
            boolean r7 = super.onKeyDown(r7, r8)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.onKeyDown(int, android.view.KeyEvent):boolean");
    }

    @TargetApi(24)
    public PointerIcon onResolvePointerIcon(MotionEvent motionEvent, int i2) {
        if (!getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()) || !isEnabled()) {
            return null;
        }
        return PointerIcon.getSystemIcon(getContext(), AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        if (r0 != 3) goto L_0x0040;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0049 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            int r0 = r6.getActionMasked()
            android.graphics.RectF r1 = r5.getCloseIconTouchBounds()
            float r2 = r6.getX()
            float r3 = r6.getY()
            boolean r1 = r1.contains(r2, r3)
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x0039
            if (r0 == r3) goto L_0x002b
            r4 = 2
            if (r0 == r4) goto L_0x0021
            r1 = 3
            if (r0 == r1) goto L_0x0034
            goto L_0x0040
        L_0x0021:
            boolean r0 = r5.g
            if (r0 == 0) goto L_0x0040
            if (r1 != 0) goto L_0x003e
            r5.setCloseIconPressed(r2)
            goto L_0x003e
        L_0x002b:
            boolean r0 = r5.g
            if (r0 == 0) goto L_0x0034
            r5.b()
            r0 = 1
            goto L_0x0035
        L_0x0034:
            r0 = 0
        L_0x0035:
            r5.setCloseIconPressed(r2)
            goto L_0x0041
        L_0x0039:
            if (r1 == 0) goto L_0x0040
            r5.setCloseIconPressed(r3)
        L_0x003e:
            r0 = 1
            goto L_0x0041
        L_0x0040:
            r0 = 0
        L_0x0041:
            if (r0 != 0) goto L_0x0049
            boolean r6 = super.onTouchEvent(r6)
            if (r6 == 0) goto L_0x004a
        L_0x0049:
            r2 = 1
        L_0x004a:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setBackground(Drawable drawable) {
        if (drawable == this.f4218a || drawable == this.b) {
            super.setBackground(drawable);
            return;
        }
        throw new UnsupportedOperationException("Do not set the background; Chip manages its own background drawable.");
    }

    public void setBackgroundColor(int i2) {
        throw new UnsupportedOperationException("Do not set the background color; Chip manages its own background drawable.");
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (drawable == this.f4218a || drawable == this.b) {
            super.setBackgroundDrawable(drawable);
            return;
        }
        throw new UnsupportedOperationException("Do not set the background drawable; Chip manages its own background drawable.");
    }

    public void setBackgroundResource(int i2) {
        throw new UnsupportedOperationException("Do not set the background resource; Chip manages its own background drawable.");
    }

    public void setBackgroundTintList(ColorStateList colorStateList) {
        throw new UnsupportedOperationException("Do not set the background tint list; Chip manages its own background drawable.");
    }

    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        throw new UnsupportedOperationException("Do not set the background tint mode; Chip manages its own background drawable.");
    }

    public void setCheckable(boolean z) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(z);
        }
    }

    public void setCheckableResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(i2);
        }
    }

    public void setChecked(boolean z) {
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable == null) {
            this.e = z;
        } else if (chipDrawable.D()) {
            boolean isChecked = isChecked();
            super.setChecked(z);
            if (isChecked != z && (onCheckedChangeListener = this.d) != null) {
                onCheckedChangeListener.onCheckedChanged(this, z);
            }
        }
    }

    public void setCheckedIcon(Drawable drawable) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(drawable);
        }
    }

    @Deprecated
    public void setCheckedIconEnabled(boolean z) {
        setCheckedIconVisible(z);
    }

    @Deprecated
    public void setCheckedIconEnabledResource(int i2) {
        setCheckedIconVisible(i2);
    }

    public void setCheckedIconResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.b(i2);
        }
    }

    public void setCheckedIconVisible(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.c(i2);
        }
    }

    public void setChipBackgroundColor(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(colorStateList);
        }
    }

    public void setChipBackgroundColorResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.d(i2);
        }
    }

    public void setChipCornerRadius(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(f2);
        }
    }

    public void setChipCornerRadiusResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.e(i2);
        }
    }

    public void setChipDrawable(ChipDrawable chipDrawable) {
        ChipDrawable chipDrawable2 = this.f4218a;
        if (chipDrawable2 != chipDrawable) {
            c(chipDrawable2);
            this.f4218a = chipDrawable;
            a(this.f4218a);
            if (RippleUtils.f4267a) {
                this.b = new RippleDrawable(RippleUtils.a(this.f4218a.x()), this.f4218a, (Drawable) null);
                this.f4218a.f(false);
                ViewCompat.a((View) this, (Drawable) this.b);
                return;
            }
            this.f4218a.f(true);
            ViewCompat.a((View) this, (Drawable) this.f4218a);
        }
    }

    public void setChipEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.b(f2);
        }
    }

    public void setChipEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.f(i2);
        }
    }

    public void setChipIcon(Drawable drawable) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.b(drawable);
        }
    }

    @Deprecated
    public void setChipIconEnabled(boolean z) {
        setChipIconVisible(z);
    }

    @Deprecated
    public void setChipIconEnabledResource(int i2) {
        setChipIconVisible(i2);
    }

    public void setChipIconResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.g(i2);
        }
    }

    public void setChipIconSize(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.c(f2);
        }
    }

    public void setChipIconSizeResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.h(i2);
        }
    }

    public void setChipIconTint(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.b(colorStateList);
        }
    }

    public void setChipIconTintResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.i(i2);
        }
    }

    public void setChipIconVisible(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.j(i2);
        }
    }

    public void setChipMinHeight(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.d(f2);
        }
    }

    public void setChipMinHeightResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.k(i2);
        }
    }

    public void setChipStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.e(f2);
        }
    }

    public void setChipStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.l(i2);
        }
    }

    public void setChipStrokeColor(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.c(colorStateList);
        }
    }

    public void setChipStrokeColorResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.m(i2);
        }
    }

    public void setChipStrokeWidth(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.f(f2);
        }
    }

    public void setChipStrokeWidthResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.n(i2);
        }
    }

    @Deprecated
    public void setChipText(CharSequence charSequence) {
        setText(charSequence);
    }

    @Deprecated
    public void setChipTextResource(int i2) {
        setText(getResources().getString(i2));
    }

    public void setCloseIcon(Drawable drawable) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.c(drawable);
        }
    }

    public void setCloseIconContentDescription(CharSequence charSequence) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(charSequence);
        }
    }

    @Deprecated
    public void setCloseIconEnabled(boolean z) {
        setCloseIconVisible(z);
    }

    @Deprecated
    public void setCloseIconEnabledResource(int i2) {
        setCloseIconVisible(i2);
    }

    public void setCloseIconEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.g(f2);
        }
    }

    public void setCloseIconEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.o(i2);
        }
    }

    public void setCloseIconResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.p(i2);
        }
    }

    public void setCloseIconSize(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.h(f2);
        }
    }

    public void setCloseIconSizeResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.q(i2);
        }
    }

    public void setCloseIconStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.i(f2);
        }
    }

    public void setCloseIconStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.r(i2);
        }
    }

    public void setCloseIconTint(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.d(colorStateList);
        }
    }

    public void setCloseIconTintResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.s(i2);
        }
    }

    public void setCloseIconVisible(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.t(i2);
        }
    }

    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int i2, int i3, int i4, int i5) {
        if (i2 != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (i4 == 0) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(i2, i3, i4, i5);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(int i2, int i3, int i4, int i5) {
        if (i2 != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (i4 == 0) {
            super.setCompoundDrawablesWithIntrinsicBounds(i2, i3, i4, i5);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setEllipsize(TextUtils.TruncateAt truncateAt) {
        if (this.f4218a != null) {
            if (truncateAt != TextUtils.TruncateAt.MARQUEE) {
                super.setEllipsize(truncateAt);
                ChipDrawable chipDrawable = this.f4218a;
                if (chipDrawable != null) {
                    chipDrawable.a(truncateAt);
                    return;
                }
                return;
            }
            throw new UnsupportedOperationException("Text within a chip are not allowed to scroll.");
        }
    }

    public void setGravity(int i2) {
        if (i2 != 8388627) {
            Log.w("Chip", "Chip text must be vertically center and start aligned");
        } else {
            super.setGravity(i2);
        }
    }

    public void setHideMotionSpec(MotionSpec motionSpec) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(motionSpec);
        }
    }

    public void setHideMotionSpecResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.u(i2);
        }
    }

    public void setIconEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.j(f2);
        }
    }

    public void setIconEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.v(i2);
        }
    }

    public void setIconStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.k(f2);
        }
    }

    public void setIconStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.w(i2);
        }
    }

    public void setLines(int i2) {
        if (i2 <= 1) {
            super.setLines(i2);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setMaxLines(int i2) {
        if (i2 <= 1) {
            super.setMaxLines(i2);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setMaxWidth(int i2) {
        super.setMaxWidth(i2);
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.x(i2);
        }
    }

    public void setMinLines(int i2) {
        if (i2 <= 1) {
            super.setMinLines(i2);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    /* access modifiers changed from: package-private */
    public void setOnCheckedChangeListenerInternal(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.d = onCheckedChangeListener;
    }

    public void setOnCloseIconClickListener(View.OnClickListener onClickListener) {
        this.c = onClickListener;
    }

    public void setRippleColor(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.e(colorStateList);
        }
    }

    public void setRippleColorResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.y(i2);
        }
    }

    public void setShowMotionSpec(MotionSpec motionSpec) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.b(motionSpec);
        }
    }

    public void setShowMotionSpecResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.z(i2);
        }
    }

    public void setSingleLine(boolean z) {
        if (z) {
            super.setSingleLine(z);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (this.f4218a != null) {
            if (charSequence == null) {
                charSequence = "";
            }
            CharSequence a2 = BidiFormatter.b().a(charSequence);
            if (this.f4218a.J()) {
                a2 = null;
            }
            super.setText(a2, bufferType);
            ChipDrawable chipDrawable = this.f4218a;
            if (chipDrawable != null) {
                chipDrawable.b(charSequence);
            }
        }
    }

    public void setTextAppearance(TextAppearance textAppearance) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.a(textAppearance);
        }
        if (getTextAppearance() != null) {
            getTextAppearance().c(getContext(), getPaint(), this.m);
            a(textAppearance);
        }
    }

    public void setTextAppearanceResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.A(i2);
        }
        setTextAppearance(getContext(), i2);
    }

    public void setTextEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.l(f2);
        }
    }

    public void setTextEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.B(i2);
        }
    }

    public void setTextStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.m(f2);
        }
    }

    public void setTextStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.C(i2);
        }
    }

    private void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", AppStateModule.APP_STATE_BACKGROUND) != null) {
                throw new UnsupportedOperationException("Do not set the background; Chip manages its own background drawable.");
            } else if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableLeft") != null) {
                throw new UnsupportedOperationException("Please set left drawable using R.attr#chipIcon.");
            } else if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableStart") != null) {
                throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
            } else if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableEnd") != null) {
                throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
            } else if (attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableRight") != null) {
                throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
            } else if (!attributeSet.getAttributeBooleanValue("http://schemas.android.com/apk/res/android", "singleLine", true) || attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "lines", 1) != 1 || attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "minLines", 1) != 1 || attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "maxLines", 1) != 1) {
                throw new UnsupportedOperationException("Chip does not support multi-line text");
            } else if (attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "gravity", 8388627) != 8388627) {
                Log.w("Chip", "Chip text must be vertically center and start aligned");
            }
        }
    }

    private float b(ChipDrawable chipDrawable) {
        float chipStartPadding = getChipStartPadding() + chipDrawable.a() + getTextStartPadding();
        return ViewCompat.m(this) == 0 ? chipStartPadding : -chipStartPadding;
    }

    private int[] d() {
        int i2 = 0;
        int i3 = isEnabled() ? 1 : 0;
        if (this.i) {
            i3++;
        }
        if (this.h) {
            i3++;
        }
        if (this.g) {
            i3++;
        }
        if (isChecked()) {
            i3++;
        }
        int[] iArr = new int[i3];
        if (isEnabled()) {
            iArr[0] = 16842910;
            i2 = 1;
        }
        if (this.i) {
            iArr[i2] = 16842908;
            i2++;
        }
        if (this.h) {
            iArr[i2] = 16843623;
            i2++;
        }
        if (this.g) {
            iArr[i2] = 16842919;
            i2++;
        }
        if (isChecked()) {
            iArr[i2] = 16842913;
        }
        return iArr;
    }

    private void c(ChipDrawable chipDrawable) {
        if (chipDrawable != null) {
            chipDrawable.a((ChipDrawable.Delegate) null);
        }
    }

    public void setCheckedIconVisible(boolean z) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.b(z);
        }
    }

    public void setChipIconVisible(boolean z) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.c(z);
        }
    }

    public void setCloseIconVisible(boolean z) {
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.d(z);
        }
    }

    public boolean b() {
        boolean z;
        playSoundEffect(0);
        View.OnClickListener onClickListener = this.c;
        if (onClickListener != null) {
            onClickListener.onClick(this);
            z = true;
        } else {
            z = false;
        }
        this.j.a(0, 1);
        return z;
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set left drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set right drawable using R.attr#closeIcon.");
        }
    }

    public void setTextAppearance(Context context, int i2) {
        super.setTextAppearance(context, i2);
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.A(i2);
        }
        if (getTextAppearance() != null) {
            getTextAppearance().c(context, getPaint(), this.m);
            a(getTextAppearance());
        }
    }

    public void setTextAppearance(int i2) {
        super.setTextAppearance(i2);
        ChipDrawable chipDrawable = this.f4218a;
        if (chipDrawable != null) {
            chipDrawable.A(i2);
        }
        if (getTextAppearance() != null) {
            getTextAppearance().c(getContext(), getPaint(), this.m);
            a(getTextAppearance());
        }
    }

    private void a(ChipDrawable chipDrawable) {
        chipDrawable.a((ChipDrawable.Delegate) this);
    }

    public void a() {
        h();
        requestLayout();
        if (Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    @SuppressLint({"PrivateApi"})
    private boolean a(MotionEvent motionEvent) {
        Class<ExploreByTouchHelper> cls = ExploreByTouchHelper.class;
        if (motionEvent.getAction() == 10) {
            try {
                Field declaredField = cls.getDeclaredField("j");
                declaredField.setAccessible(true);
                if (((Integer) declaredField.get(this.j)).intValue() != Integer.MIN_VALUE) {
                    Method declaredMethod = cls.getDeclaredMethod("i", new Class[]{Integer.TYPE});
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(this.j, new Object[]{Integer.MIN_VALUE});
                    return true;
                }
            } catch (NoSuchMethodException e2) {
                Log.e("Chip", "Unable to send Accessibility Exit event", e2);
            } catch (IllegalAccessException e3) {
                Log.e("Chip", "Unable to send Accessibility Exit event", e3);
            } catch (InvocationTargetException e4) {
                Log.e("Chip", "Unable to send Accessibility Exit event", e4);
            } catch (NoSuchFieldException e5) {
                Log.e("Chip", "Unable to send Accessibility Exit event", e5);
            }
        }
        return false;
    }

    private boolean a(boolean z) {
        e();
        if (z) {
            if (this.f == -1) {
                setFocusedVirtualView(0);
                return true;
            }
        } else if (this.f == 0) {
            setFocusedVirtualView(-1);
            return true;
        }
        return false;
    }

    private void a(TextAppearance textAppearance) {
        TextPaint paint = getPaint();
        paint.drawableState = this.f4218a.getState();
        textAppearance.b(getContext(), paint, this.m);
    }
}
