package com.google.android.material.chip;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.graphics.drawable.TintAwareDrawable;
import androidx.core.text.BidiFormatter;
import com.facebook.imageutils.JfifUtil;
import com.google.android.material.R$styleable;
import com.google.android.material.animation.MotionSpec;
import com.google.android.material.canvas.CanvasCompat;
import com.google.android.material.drawable.DrawableUtils;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.resources.MaterialResources;
import com.google.android.material.resources.TextAppearance;
import com.google.android.material.ripple.RippleUtils;
import java.lang.ref.WeakReference;
import java.util.Arrays;

public class ChipDrawable extends Drawable implements TintAwareDrawable, Drawable.Callback {
    private static final int[] k0 = {16842910};
    private float A;
    private float B;
    private float C;
    private float D;
    private float E;
    private float F;
    private final Context G;
    private final TextPaint H = new TextPaint(1);
    private final Paint I = new Paint(1);
    private final Paint J;
    private final Paint.FontMetrics K = new Paint.FontMetrics();
    private final RectF L = new RectF();
    private final PointF M = new PointF();
    private int N;
    private int O;
    private int P;
    private int Q;
    private boolean R;
    private int S;
    private int T = JfifUtil.MARKER_FIRST_BYTE;
    private ColorFilter U;
    private PorterDuffColorFilter V;
    private ColorStateList W;
    private PorterDuff.Mode X = PorterDuff.Mode.SRC_IN;
    private int[] Y;

    /* renamed from: a  reason: collision with root package name */
    private ColorStateList f4221a;
    private float b;
    private float c;
    private boolean c0;
    private ColorStateList d;
    private ColorStateList d0;
    private float e;
    private WeakReference<Delegate> e0 = new WeakReference<>((Object) null);
    private ColorStateList f;
    /* access modifiers changed from: private */
    public boolean f0 = true;
    private CharSequence g;
    private float g0;
    private CharSequence h;
    private TextUtils.TruncateAt h0;
    private TextAppearance i;
    private boolean i0;
    private final ResourcesCompat.FontCallback j = new ResourcesCompat.FontCallback() {
        public void a(int i) {
        }

        public void a(Typeface typeface) {
            boolean unused = ChipDrawable.this.f0 = true;
            ChipDrawable.this.I();
            ChipDrawable.this.invalidateSelf();
        }
    };
    private int j0;
    private boolean k;
    private Drawable l;
    private ColorStateList m;
    private float n;
    private boolean o;
    private Drawable p;
    private ColorStateList q;
    private float r;
    private CharSequence s;
    private boolean t;
    private boolean u;
    private Drawable v;
    private MotionSpec w;
    private MotionSpec x;
    private float y;
    private float z;

    public interface Delegate {
        void a();
    }

    private ChipDrawable(Context context) {
        this.G = context;
        this.g = "";
        this.H.density = context.getResources().getDisplayMetrics().density;
        this.J = null;
        Paint paint = this.J;
        if (paint != null) {
            paint.setStyle(Paint.Style.STROKE);
        }
        setState(k0);
        a(k0);
        this.i0 = true;
    }

    private float K() {
        if (R()) {
            return this.D + this.r + this.E;
        }
        return 0.0f;
    }

    private float L() {
        this.H.getFontMetrics(this.K);
        Paint.FontMetrics fontMetrics = this.K;
        return (fontMetrics.descent + fontMetrics.ascent) / 2.0f;
    }

    private boolean M() {
        return this.u && this.v != null && this.t;
    }

    private float N() {
        if (!this.f0) {
            return this.g0;
        }
        this.g0 = c(this.h);
        this.f0 = false;
        return this.g0;
    }

    private ColorFilter O() {
        ColorFilter colorFilter = this.U;
        return colorFilter != null ? colorFilter : this.V;
    }

    private boolean P() {
        return this.u && this.v != null && this.R;
    }

    private boolean Q() {
        return this.k && this.l != null;
    }

    private boolean R() {
        return this.o && this.p != null;
    }

    private void S() {
        this.d0 = this.c0 ? RippleUtils.a(this.f) : null;
    }

    private void b(Canvas canvas, Rect rect) {
        this.I.setColor(this.N);
        this.I.setStyle(Paint.Style.FILL);
        this.I.setColorFilter(O());
        this.L.set(rect);
        RectF rectF = this.L;
        float f2 = this.c;
        canvas.drawRoundRect(rectF, f2, f2, this.I);
    }

    private float c(CharSequence charSequence) {
        if (charSequence == null) {
            return 0.0f;
        }
        return this.H.measureText(charSequence, 0, charSequence.length());
    }

    private void d(Canvas canvas, Rect rect) {
        if (this.e > 0.0f) {
            this.I.setColor(this.O);
            this.I.setStyle(Paint.Style.STROKE);
            this.I.setColorFilter(O());
            RectF rectF = this.L;
            float f2 = this.e;
            rectF.set(((float) rect.left) + (f2 / 2.0f), ((float) rect.top) + (f2 / 2.0f), ((float) rect.right) - (f2 / 2.0f), ((float) rect.bottom) - (f2 / 2.0f));
            float f3 = this.c - (this.e / 2.0f);
            canvas.drawRoundRect(this.L, f3, f3, this.I);
        }
    }

    private void e(Canvas canvas, Rect rect) {
        if (R()) {
            c(rect, this.L);
            RectF rectF = this.L;
            float f2 = rectF.left;
            float f3 = rectF.top;
            canvas.translate(f2, f3);
            this.p.setBounds(0, 0, (int) this.L.width(), (int) this.L.height());
            this.p.draw(canvas);
            canvas.translate(-f2, -f3);
        }
    }

    private void g(Canvas canvas, Rect rect) {
        Paint paint = this.J;
        if (paint != null) {
            paint.setColor(ColorUtils.d(-16777216, 127));
            canvas.drawRect(rect, this.J);
            if (Q() || P()) {
                a(rect, this.L);
                canvas.drawRect(this.L, this.J);
            }
            if (this.h != null) {
                canvas.drawLine((float) rect.left, rect.exactCenterY(), (float) rect.right, rect.exactCenterY(), this.J);
            }
            if (R()) {
                c(rect, this.L);
                canvas.drawRect(this.L, this.J);
            }
            this.J.setColor(ColorUtils.d(-65536, 127));
            b(rect, this.L);
            canvas.drawRect(this.L, this.J);
            this.J.setColor(ColorUtils.d(-16711936, 127));
            d(rect, this.L);
            canvas.drawRect(this.L, this.J);
        }
    }

    private void h(Canvas canvas, Rect rect) {
        if (this.h != null) {
            Paint.Align a2 = a(rect, this.M);
            e(rect, this.L);
            if (this.i != null) {
                this.H.drawableState = getState();
                this.i.b(this.G, this.H, this.j);
            }
            this.H.setTextAlign(a2);
            int i2 = 0;
            boolean z2 = Math.round(N()) > Math.round(this.L.width());
            if (z2) {
                i2 = canvas.save();
                canvas.clipRect(this.L);
            }
            CharSequence charSequence = this.h;
            if (z2 && this.h0 != null) {
                charSequence = TextUtils.ellipsize(charSequence, this.H, this.L.width(), this.h0);
            }
            CharSequence charSequence2 = charSequence;
            int length = charSequence2.length();
            PointF pointF = this.M;
            canvas.drawText(charSequence2, 0, length, pointF.x, pointF.y, this.H);
            if (z2) {
                canvas.restoreToCount(i2);
            }
        }
    }

    public TextAppearance A() {
        return this.i;
    }

    public float B() {
        return this.C;
    }

    public float C() {
        return this.B;
    }

    public boolean D() {
        return this.t;
    }

    public boolean E() {
        return this.u;
    }

    public boolean F() {
        return this.k;
    }

    public boolean G() {
        return e(this.p);
    }

    public boolean H() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public void I() {
        Delegate delegate = (Delegate) this.e0.get();
        if (delegate != null) {
            delegate.a();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean J() {
        return this.i0;
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty() && getAlpha() != 0) {
            int i2 = 0;
            int i3 = this.T;
            if (i3 < 255) {
                i2 = CanvasCompat.a(canvas, (float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom, i3);
            }
            b(canvas, bounds);
            d(canvas, bounds);
            f(canvas, bounds);
            c(canvas, bounds);
            a(canvas, bounds);
            if (this.i0) {
                h(canvas, bounds);
            }
            e(canvas, bounds);
            g(canvas, bounds);
            if (this.T < 255) {
                canvas.restoreToCount(i2);
            }
        }
    }

    public void f(boolean z2) {
        if (this.c0 != z2) {
            this.c0 = z2;
            S();
            onStateChange(getState());
        }
    }

    public int getAlpha() {
        return this.T;
    }

    public ColorFilter getColorFilter() {
        return this.U;
    }

    public int getIntrinsicHeight() {
        return (int) this.b;
    }

    public int getIntrinsicWidth() {
        return Math.min(Math.round(this.y + a() + this.B + N() + this.C + K() + this.F), this.j0);
    }

    public int getOpacity() {
        return -3;
    }

    @TargetApi(21)
    public void getOutline(Outline outline) {
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.c);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), getIntrinsicHeight(), this.c);
        }
        outline.setAlpha(((float) getAlpha()) / 255.0f);
    }

    public float i() {
        return this.b;
    }

    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public boolean isStateful() {
        return f(this.f4221a) || f(this.d) || (this.c0 && f(this.d0)) || b(this.i) || M() || e(this.l) || e(this.v) || f(this.W);
    }

    public void j(int i2) {
        c(this.G.getResources().getBoolean(i2));
    }

    public void k(int i2) {
        d(this.G.getResources().getDimension(i2));
    }

    public float l() {
        return this.e;
    }

    public void m(int i2) {
        c(AppCompatResources.b(this.G, i2));
    }

    public void n(int i2) {
        f(this.G.getResources().getDimension(i2));
    }

    public float o() {
        return this.E;
    }

    @TargetApi(23)
    public boolean onLayoutDirectionChanged(int i2) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i2);
        if (Q()) {
            onLayoutDirectionChanged |= this.l.setLayoutDirection(i2);
        }
        if (P()) {
            onLayoutDirectionChanged |= this.v.setLayoutDirection(i2);
        }
        if (R()) {
            onLayoutDirectionChanged |= this.p.setLayoutDirection(i2);
        }
        if (!onLayoutDirectionChanged) {
            return true;
        }
        invalidateSelf();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i2) {
        boolean onLevelChange = super.onLevelChange(i2);
        if (Q()) {
            onLevelChange |= this.l.setLevel(i2);
        }
        if (P()) {
            onLevelChange |= this.v.setLevel(i2);
        }
        if (R()) {
            onLevelChange |= this.p.setLevel(i2);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        return a(iArr, r());
    }

    public void p(int i2) {
        c(AppCompatResources.c(this.G, i2));
    }

    public void q(int i2) {
        h(this.G.getResources().getDimension(i2));
    }

    public int[] r() {
        return this.Y;
    }

    public ColorStateList s() {
        return this.q;
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j2);
        }
    }

    public void setAlpha(int i2) {
        if (this.T != i2) {
            this.T = i2;
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.U != colorFilter) {
            this.U = colorFilter;
            invalidateSelf();
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.W != colorStateList) {
            this.W = colorStateList;
            onStateChange(getState());
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.X != mode) {
            this.X = mode;
            this.V = DrawableUtils.a(this, this.W, mode);
            invalidateSelf();
        }
    }

    public boolean setVisible(boolean z2, boolean z3) {
        boolean visible = super.setVisible(z2, z3);
        if (Q()) {
            visible |= this.l.setVisible(z2, z3);
        }
        if (P()) {
            visible |= this.v.setVisible(z2, z3);
        }
        if (R()) {
            visible |= this.p.setVisible(z2, z3);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    public TextUtils.TruncateAt t() {
        return this.h0;
    }

    public MotionSpec u() {
        return this.x;
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    public float v() {
        return this.A;
    }

    public float w() {
        return this.z;
    }

    public ColorStateList x() {
        return this.f;
    }

    public void y(int i2) {
        e(AppCompatResources.b(this.G, i2));
    }

    public CharSequence z() {
        return this.g;
    }

    public static ChipDrawable a(Context context, AttributeSet attributeSet, int i2, int i3) {
        ChipDrawable chipDrawable = new ChipDrawable(context);
        chipDrawable.a(attributeSet, i2, i3);
        return chipDrawable;
    }

    private void c(Canvas canvas, Rect rect) {
        if (Q()) {
            a(rect, this.L);
            RectF rectF = this.L;
            float f2 = rectF.left;
            float f3 = rectF.top;
            canvas.translate(f2, f3);
            this.l.setBounds(0, 0, (int) this.L.width(), (int) this.L.height());
            this.l.draw(canvas);
            canvas.translate(-f2, -f3);
        }
    }

    public void A(int i2) {
        a(new TextAppearance(this.G, i2));
    }

    public void B(int i2) {
        l(this.G.getResources().getDimension(i2));
    }

    public void C(int i2) {
        m(this.G.getResources().getDimension(i2));
    }

    public void i(int i2) {
        b(AppCompatResources.b(this.G, i2));
    }

    public float j() {
        return this.y;
    }

    public ColorStateList k() {
        return this.d;
    }

    public void l(int i2) {
        e(this.G.getResources().getDimension(i2));
    }

    public Drawable m() {
        Drawable drawable = this.p;
        if (drawable != null) {
            return DrawableCompat.h(drawable);
        }
        return null;
    }

    public CharSequence n() {
        return this.s;
    }

    public void o(int i2) {
        g(this.G.getResources().getDimension(i2));
    }

    public float p() {
        return this.r;
    }

    public float q() {
        return this.D;
    }

    public void r(int i2) {
        i(this.G.getResources().getDimension(i2));
    }

    public void s(int i2) {
        d(AppCompatResources.b(this.G, i2));
    }

    public void t(int i2) {
        d(this.G.getResources().getBoolean(i2));
    }

    public void u(int i2) {
        a(MotionSpec.a(this.G, i2));
    }

    public void v(int i2) {
        j(this.G.getResources().getDimension(i2));
    }

    public void w(int i2) {
        k(this.G.getResources().getDimension(i2));
    }

    public void x(int i2) {
        this.j0 = i2;
    }

    public MotionSpec y() {
        return this.w;
    }

    public void z(int i2) {
        b(MotionSpec.a(this.G, i2));
    }

    public void i(float f2) {
        if (this.D != f2) {
            this.D = f2;
            invalidateSelf();
            if (R()) {
                I();
            }
        }
    }

    public void j(float f2) {
        if (this.A != f2) {
            float a2 = a();
            this.A = f2;
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                I();
            }
        }
    }

    public void k(float f2) {
        if (this.z != f2) {
            float a2 = a();
            this.z = f2;
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                I();
            }
        }
    }

    public void l(float f2) {
        if (this.C != f2) {
            this.C = f2;
            invalidateSelf();
            I();
        }
    }

    public void m(float f2) {
        if (this.B != f2) {
            this.B = f2;
            invalidateSelf();
            I();
        }
    }

    private void a(AttributeSet attributeSet, int i2, int i3) {
        TypedArray c2 = ThemeEnforcement.c(this.G, attributeSet, R$styleable.Chip, i2, i3, new int[0]);
        a(MaterialResources.a(this.G, c2, R$styleable.Chip_chipBackgroundColor));
        d(c2.getDimension(R$styleable.Chip_chipMinHeight, 0.0f));
        a(c2.getDimension(R$styleable.Chip_chipCornerRadius, 0.0f));
        c(MaterialResources.a(this.G, c2, R$styleable.Chip_chipStrokeColor));
        f(c2.getDimension(R$styleable.Chip_chipStrokeWidth, 0.0f));
        e(MaterialResources.a(this.G, c2, R$styleable.Chip_rippleColor));
        b(c2.getText(R$styleable.Chip_android_text));
        a(MaterialResources.c(this.G, c2, R$styleable.Chip_android_textAppearance));
        int i4 = c2.getInt(R$styleable.Chip_android_ellipsize, 0);
        if (i4 == 1) {
            a(TextUtils.TruncateAt.START);
        } else if (i4 == 2) {
            a(TextUtils.TruncateAt.MIDDLE);
        } else if (i4 == 3) {
            a(TextUtils.TruncateAt.END);
        }
        c(c2.getBoolean(R$styleable.Chip_chipIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "chipIconVisible") != null)) {
            c(c2.getBoolean(R$styleable.Chip_chipIconEnabled, false));
        }
        b(MaterialResources.b(this.G, c2, R$styleable.Chip_chipIcon));
        b(MaterialResources.a(this.G, c2, R$styleable.Chip_chipIconTint));
        c(c2.getDimension(R$styleable.Chip_chipIconSize, 0.0f));
        d(c2.getBoolean(R$styleable.Chip_closeIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "closeIconVisible") != null)) {
            d(c2.getBoolean(R$styleable.Chip_closeIconEnabled, false));
        }
        c(MaterialResources.b(this.G, c2, R$styleable.Chip_closeIcon));
        d(MaterialResources.a(this.G, c2, R$styleable.Chip_closeIconTint));
        h(c2.getDimension(R$styleable.Chip_closeIconSize, 0.0f));
        a(c2.getBoolean(R$styleable.Chip_android_checkable, false));
        b(c2.getBoolean(R$styleable.Chip_checkedIconVisible, false));
        if (!(attributeSet == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconEnabled") == null || attributeSet.getAttributeValue("http://schemas.android.com/apk/res-auto", "checkedIconVisible") != null)) {
            b(c2.getBoolean(R$styleable.Chip_checkedIconEnabled, false));
        }
        a(MaterialResources.b(this.G, c2, R$styleable.Chip_checkedIcon));
        b(MotionSpec.a(this.G, c2, R$styleable.Chip_showMotionSpec));
        a(MotionSpec.a(this.G, c2, R$styleable.Chip_hideMotionSpec));
        e(c2.getDimension(R$styleable.Chip_chipStartPadding, 0.0f));
        k(c2.getDimension(R$styleable.Chip_iconStartPadding, 0.0f));
        j(c2.getDimension(R$styleable.Chip_iconEndPadding, 0.0f));
        m(c2.getDimension(R$styleable.Chip_textStartPadding, 0.0f));
        l(c2.getDimension(R$styleable.Chip_textEndPadding, 0.0f));
        i(c2.getDimension(R$styleable.Chip_closeIconStartPadding, 0.0f));
        g(c2.getDimension(R$styleable.Chip_closeIconEndPadding, 0.0f));
        b(c2.getDimension(R$styleable.Chip_chipEndPadding, 0.0f));
        x(c2.getDimensionPixelSize(R$styleable.Chip_android_maxWidth, Integer.MAX_VALUE));
        c2.recycle();
    }

    private void f(Canvas canvas, Rect rect) {
        this.I.setColor(this.P);
        this.I.setStyle(Paint.Style.FILL);
        this.L.set(rect);
        RectF rectF = this.L;
        float f2 = this.c;
        canvas.drawRoundRect(rectF, f2, f2, this.I);
    }

    private void b(Rect rect, RectF rectF) {
        rectF.set(rect);
        if (R()) {
            float f2 = this.F + this.E + this.r + this.D + this.C;
            if (DrawableCompat.e(this) == 0) {
                rectF.right = ((float) rect.right) - f2;
            } else {
                rectF.left = ((float) rect.left) + f2;
            }
        }
    }

    private void d(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (R()) {
            float f2 = this.F + this.E + this.r + this.D + this.C;
            if (DrawableCompat.e(this) == 0) {
                rectF.right = (float) rect.right;
                rectF.left = rectF.right - f2;
            } else {
                int i2 = rect.left;
                rectF.left = (float) i2;
                rectF.right = ((float) i2) + f2;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    private void e(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (this.h != null) {
            float a2 = this.y + a() + this.B;
            float K2 = this.F + K() + this.C;
            if (DrawableCompat.e(this) == 0) {
                rectF.left = ((float) rect.left) + a2;
                rectF.right = ((float) rect.right) - K2;
            } else {
                rectF.left = ((float) rect.left) + K2;
                rectF.right = ((float) rect.right) - a2;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    private static boolean f(ColorStateList colorStateList) {
        return colorStateList != null && colorStateList.isStateful();
    }

    private void c(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (R()) {
            float f2 = this.F + this.E;
            if (DrawableCompat.e(this) == 0) {
                rectF.right = ((float) rect.right) - f2;
                rectF.left = rectF.right - this.r;
            } else {
                rectF.left = ((float) rect.left) + f2;
                rectF.right = rectF.left + this.r;
            }
            float exactCenterY = rect.exactCenterY();
            float f3 = this.r;
            rectF.top = exactCenterY - (f3 / 2.0f);
            rectF.bottom = rectF.top + f3;
        }
    }

    private void f(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback((Drawable.Callback) null);
        }
    }

    public void f(float f2) {
        if (this.e != f2) {
            this.e = f2;
            this.I.setStrokeWidth(f2);
            invalidateSelf();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r0 = r0.b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(com.google.android.material.resources.TextAppearance r0) {
        /*
            if (r0 == 0) goto L_0x000e
            android.content.res.ColorStateList r0 = r0.b
            if (r0 == 0) goto L_0x000e
            boolean r0 = r0.isStateful()
            if (r0 == 0) goto L_0x000e
            r0 = 1
            goto L_0x000f
        L_0x000e:
            r0 = 0
        L_0x000f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.ChipDrawable.b(com.google.android.material.resources.TextAppearance):boolean");
    }

    public void b(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (this.g != charSequence) {
            this.g = charSequence;
            this.h = BidiFormatter.b().a(charSequence);
            this.f0 = true;
            invalidateSelf();
            I();
        }
    }

    public Drawable f() {
        Drawable drawable = this.l;
        if (drawable != null) {
            return DrawableCompat.h(drawable);
        }
        return null;
    }

    public void f(int i2) {
        b(this.G.getResources().getDimension(i2));
    }

    public ColorStateList h() {
        return this.m;
    }

    public void h(int i2) {
        c(this.G.getResources().getDimension(i2));
    }

    private void d(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
            DrawableCompat.a(drawable, DrawableCompat.e(this));
            drawable.setLevel(getLevel());
            drawable.setVisible(isVisible(), false);
            if (drawable == this.p) {
                if (drawable.isStateful()) {
                    drawable.setState(r());
                }
                DrawableCompat.a(drawable, this.q);
            } else if (drawable.isStateful()) {
                drawable.setState(getState());
            }
        }
    }

    public void h(float f2) {
        if (this.r != f2) {
            this.r = f2;
            invalidateSelf();
            if (R()) {
                I();
            }
        }
    }

    private static boolean e(Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }

    public void b(Drawable drawable) {
        Drawable f2 = f();
        if (f2 != drawable) {
            float a2 = a();
            this.l = drawable != null ? DrawableCompat.i(drawable).mutate() : null;
            float a3 = a();
            f(f2);
            if (Q()) {
                d(this.l);
            }
            invalidateSelf();
            if (a2 != a3) {
                I();
            }
        }
    }

    public ColorStateList c() {
        return this.f4221a;
    }

    public void g(int i2) {
        b(AppCompatResources.c(this.G, i2));
    }

    public void c(ColorStateList colorStateList) {
        if (this.d != colorStateList) {
            this.d = colorStateList;
            onStateChange(getState());
        }
    }

    public void e(int i2) {
        a(this.G.getResources().getDimension(i2));
    }

    public float g() {
        return this.n;
    }

    public void e(ColorStateList colorStateList) {
        if (this.f != colorStateList) {
            this.f = colorStateList;
            S();
            onStateChange(getState());
        }
    }

    public void g(float f2) {
        if (this.E != f2) {
            this.E = f2;
            invalidateSelf();
            if (R()) {
                I();
            }
        }
    }

    public void c(boolean z2) {
        if (this.k != z2) {
            boolean Q2 = Q();
            this.k = z2;
            boolean Q3 = Q();
            if (Q2 != Q3) {
                if (Q3) {
                    d(this.l);
                } else {
                    f(this.l);
                }
                invalidateSelf();
                I();
            }
        }
    }

    public void e(float f2) {
        if (this.y != f2) {
            this.y = f2;
            invalidateSelf();
            I();
        }
    }

    public void d(int i2) {
        a(AppCompatResources.b(this.G, i2));
    }

    public void b(ColorStateList colorStateList) {
        if (this.m != colorStateList) {
            this.m = colorStateList;
            if (Q()) {
                DrawableCompat.a(this.l, colorStateList);
            }
            onStateChange(getState());
        }
    }

    public void d(float f2) {
        if (this.b != f2) {
            this.b = f2;
            invalidateSelf();
            I();
        }
    }

    public float e() {
        return this.F;
    }

    /* access modifiers changed from: package-private */
    public void e(boolean z2) {
        this.i0 = z2;
    }

    public void c(float f2) {
        if (this.n != f2) {
            float a2 = a();
            this.n = f2;
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                I();
            }
        }
    }

    public float d() {
        return this.c;
    }

    public void b(boolean z2) {
        if (this.u != z2) {
            boolean P2 = P();
            this.u = z2;
            boolean P3 = P();
            if (P2 != P3) {
                if (P3) {
                    d(this.v);
                } else {
                    f(this.v);
                }
                invalidateSelf();
                I();
            }
        }
    }

    public void d(boolean z2) {
        if (this.o != z2) {
            boolean R2 = R();
            this.o = z2;
            boolean R3 = R();
            if (R2 != R3) {
                if (R3) {
                    d(this.p);
                } else {
                    f(this.p);
                }
                invalidateSelf();
                I();
            }
        }
    }

    public void c(Drawable drawable) {
        Drawable m2 = m();
        if (m2 != drawable) {
            float K2 = K();
            this.p = drawable != null ? DrawableCompat.i(drawable).mutate() : null;
            float K3 = K();
            f(m2);
            if (R()) {
                d(this.p);
            }
            invalidateSelf();
            if (K2 != K3) {
                I();
            }
        }
    }

    public Drawable b() {
        return this.v;
    }

    public void d(ColorStateList colorStateList) {
        if (this.q != colorStateList) {
            this.q = colorStateList;
            if (R()) {
                DrawableCompat.a(this.p, colorStateList);
            }
            onStateChange(getState());
        }
    }

    public void b(int i2) {
        a(AppCompatResources.c(this.G, i2));
    }

    public void b(MotionSpec motionSpec) {
        this.w = motionSpec;
    }

    public void b(float f2) {
        if (this.F != f2) {
            this.F = f2;
            invalidateSelf();
            I();
        }
    }

    public void c(int i2) {
        b(this.G.getResources().getBoolean(i2));
    }

    public void a(Delegate delegate) {
        this.e0 = new WeakReference<>(delegate);
    }

    public void a(RectF rectF) {
        d(getBounds(), rectF);
    }

    /* access modifiers changed from: package-private */
    public float a() {
        if (Q() || P()) {
            return this.z + this.n + this.A;
        }
        return 0.0f;
    }

    private void a(Canvas canvas, Rect rect) {
        if (P()) {
            a(rect, this.L);
            RectF rectF = this.L;
            float f2 = rectF.left;
            float f3 = rectF.top;
            canvas.translate(f2, f3);
            this.v.setBounds(0, 0, (int) this.L.width(), (int) this.L.height());
            this.v.draw(canvas);
            canvas.translate(-f2, -f3);
        }
    }

    private void a(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (Q() || P()) {
            float f2 = this.y + this.z;
            if (DrawableCompat.e(this) == 0) {
                rectF.left = ((float) rect.left) + f2;
                rectF.right = rectF.left + this.n;
            } else {
                rectF.right = ((float) rect.right) - f2;
                rectF.left = rectF.right - this.n;
            }
            float exactCenterY = rect.exactCenterY();
            float f3 = this.n;
            rectF.top = exactCenterY - (f3 / 2.0f);
            rectF.bottom = rectF.top + f3;
        }
    }

    /* access modifiers changed from: package-private */
    public Paint.Align a(Rect rect, PointF pointF) {
        pointF.set(0.0f, 0.0f);
        Paint.Align align = Paint.Align.LEFT;
        if (this.h != null) {
            float a2 = this.y + a() + this.B;
            if (DrawableCompat.e(this) == 0) {
                pointF.x = ((float) rect.left) + a2;
                align = Paint.Align.LEFT;
            } else {
                pointF.x = ((float) rect.right) - a2;
                align = Paint.Align.RIGHT;
            }
            pointF.y = ((float) rect.centerY()) - L();
        }
        return align;
    }

    public boolean a(int[] iArr) {
        if (Arrays.equals(this.Y, iArr)) {
            return false;
        }
        this.Y = iArr;
        if (R()) {
            return a(getState(), iArr);
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(int[] r6, int[] r7) {
        /*
            r5 = this;
            boolean r0 = super.onStateChange(r6)
            android.content.res.ColorStateList r1 = r5.f4221a
            r2 = 0
            if (r1 == 0) goto L_0x0010
            int r3 = r5.N
            int r1 = r1.getColorForState(r6, r3)
            goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            int r3 = r5.N
            r4 = 1
            if (r3 == r1) goto L_0x0019
            r5.N = r1
            r0 = 1
        L_0x0019:
            android.content.res.ColorStateList r1 = r5.d
            if (r1 == 0) goto L_0x0024
            int r3 = r5.O
            int r1 = r1.getColorForState(r6, r3)
            goto L_0x0025
        L_0x0024:
            r1 = 0
        L_0x0025:
            int r3 = r5.O
            if (r3 == r1) goto L_0x002c
            r5.O = r1
            r0 = 1
        L_0x002c:
            android.content.res.ColorStateList r1 = r5.d0
            if (r1 == 0) goto L_0x0037
            int r3 = r5.P
            int r1 = r1.getColorForState(r6, r3)
            goto L_0x0038
        L_0x0037:
            r1 = 0
        L_0x0038:
            int r3 = r5.P
            if (r3 == r1) goto L_0x0043
            r5.P = r1
            boolean r1 = r5.c0
            if (r1 == 0) goto L_0x0043
            r0 = 1
        L_0x0043:
            com.google.android.material.resources.TextAppearance r1 = r5.i
            if (r1 == 0) goto L_0x0052
            android.content.res.ColorStateList r1 = r1.b
            if (r1 == 0) goto L_0x0052
            int r3 = r5.Q
            int r1 = r1.getColorForState(r6, r3)
            goto L_0x0053
        L_0x0052:
            r1 = 0
        L_0x0053:
            int r3 = r5.Q
            if (r3 == r1) goto L_0x005a
            r5.Q = r1
            r0 = 1
        L_0x005a:
            int[] r1 = r5.getState()
            r3 = 16842912(0x10100a0, float:2.3694006E-38)
            boolean r1 = a((int[]) r1, (int) r3)
            if (r1 == 0) goto L_0x006d
            boolean r1 = r5.t
            if (r1 == 0) goto L_0x006d
            r1 = 1
            goto L_0x006e
        L_0x006d:
            r1 = 0
        L_0x006e:
            boolean r3 = r5.R
            if (r3 == r1) goto L_0x0088
            android.graphics.drawable.Drawable r3 = r5.v
            if (r3 == 0) goto L_0x0088
            float r0 = r5.a()
            r5.R = r1
            float r1 = r5.a()
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0087
            r0 = 1
            r1 = 1
            goto L_0x0089
        L_0x0087:
            r0 = 1
        L_0x0088:
            r1 = 0
        L_0x0089:
            android.content.res.ColorStateList r3 = r5.W
            if (r3 == 0) goto L_0x0093
            int r2 = r5.S
            int r2 = r3.getColorForState(r6, r2)
        L_0x0093:
            int r3 = r5.S
            if (r3 == r2) goto L_0x00a4
            r5.S = r2
            android.content.res.ColorStateList r0 = r5.W
            android.graphics.PorterDuff$Mode r2 = r5.X
            android.graphics.PorterDuffColorFilter r0 = com.google.android.material.drawable.DrawableUtils.a(r5, r0, r2)
            r5.V = r0
            r0 = 1
        L_0x00a4:
            android.graphics.drawable.Drawable r2 = r5.l
            boolean r2 = e((android.graphics.drawable.Drawable) r2)
            if (r2 == 0) goto L_0x00b3
            android.graphics.drawable.Drawable r2 = r5.l
            boolean r2 = r2.setState(r6)
            r0 = r0 | r2
        L_0x00b3:
            android.graphics.drawable.Drawable r2 = r5.v
            boolean r2 = e((android.graphics.drawable.Drawable) r2)
            if (r2 == 0) goto L_0x00c2
            android.graphics.drawable.Drawable r2 = r5.v
            boolean r6 = r2.setState(r6)
            r0 = r0 | r6
        L_0x00c2:
            android.graphics.drawable.Drawable r6 = r5.p
            boolean r6 = e((android.graphics.drawable.Drawable) r6)
            if (r6 == 0) goto L_0x00d1
            android.graphics.drawable.Drawable r6 = r5.p
            boolean r6 = r6.setState(r7)
            r0 = r0 | r6
        L_0x00d1:
            if (r0 == 0) goto L_0x00d6
            r5.invalidateSelf()
        L_0x00d6:
            if (r1 == 0) goto L_0x00db
            r5.I()
        L_0x00db:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.ChipDrawable.a(int[], int[]):boolean");
    }

    private static boolean a(int[] iArr, int i2) {
        if (iArr == null) {
            return false;
        }
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    public void a(ColorStateList colorStateList) {
        if (this.f4221a != colorStateList) {
            this.f4221a = colorStateList;
            onStateChange(getState());
        }
    }

    public void a(float f2) {
        if (this.c != f2) {
            this.c = f2;
            invalidateSelf();
        }
    }

    public void a(TextAppearance textAppearance) {
        if (this.i != textAppearance) {
            this.i = textAppearance;
            if (textAppearance != null) {
                textAppearance.c(this.G, this.H, this.j);
                this.f0 = true;
            }
            onStateChange(getState());
            I();
        }
    }

    public void a(TextUtils.TruncateAt truncateAt) {
        this.h0 = truncateAt;
    }

    public void a(CharSequence charSequence) {
        if (this.s != charSequence) {
            this.s = BidiFormatter.b().a(charSequence);
            invalidateSelf();
        }
    }

    public void a(int i2) {
        a(this.G.getResources().getBoolean(i2));
    }

    public void a(boolean z2) {
        if (this.t != z2) {
            this.t = z2;
            float a2 = a();
            if (!z2 && this.R) {
                this.R = false;
            }
            float a3 = a();
            invalidateSelf();
            if (a2 != a3) {
                I();
            }
        }
    }

    public void a(Drawable drawable) {
        if (this.v != drawable) {
            float a2 = a();
            this.v = drawable;
            float a3 = a();
            f(this.v);
            d(this.v);
            invalidateSelf();
            if (a2 != a3) {
                I();
            }
        }
    }

    public void a(MotionSpec motionSpec) {
        this.x = motionSpec;
    }
}
