package com.google.android.material.appbar;

import android.view.View;
import androidx.core.view.ViewCompat;

class ViewOffsetHelper {

    /* renamed from: a  reason: collision with root package name */
    private final View f4187a;
    private int b;
    private int c;
    private int d;
    private int e;

    public ViewOffsetHelper(View view) {
        this.f4187a = view;
    }

    private void d() {
        View view = this.f4187a;
        ViewCompat.f(view, this.d - (view.getTop() - this.b));
        View view2 = this.f4187a;
        ViewCompat.e(view2, this.e - (view2.getLeft() - this.c));
    }

    public boolean a(int i) {
        if (this.e == i) {
            return false;
        }
        this.e = i;
        d();
        return true;
    }

    public boolean b(int i) {
        if (this.d == i) {
            return false;
        }
        this.d = i;
        d();
        return true;
    }

    public void c() {
        this.b = this.f4187a.getTop();
        this.c = this.f4187a.getLeft();
        d();
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.d;
    }
}
