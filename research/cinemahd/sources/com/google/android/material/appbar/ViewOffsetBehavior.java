package com.google.android.material.appbar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

class ViewOffsetBehavior<V extends View> extends CoordinatorLayout.Behavior<V> {

    /* renamed from: a  reason: collision with root package name */
    private ViewOffsetHelper f4186a;
    private int b = 0;
    private int c = 0;

    public ViewOffsetBehavior() {
    }

    /* access modifiers changed from: protected */
    public void a(CoordinatorLayout coordinatorLayout, V v, int i) {
        coordinatorLayout.c((View) v, i);
    }

    public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, V v, int i) {
        a(coordinatorLayout, v, i);
        if (this.f4186a == null) {
            this.f4186a = new ViewOffsetHelper(v);
        }
        this.f4186a.c();
        int i2 = this.b;
        if (i2 != 0) {
            this.f4186a.b(i2);
            this.b = 0;
        }
        int i3 = this.c;
        if (i3 == 0) {
            return true;
        }
        this.f4186a.a(i3);
        this.c = 0;
        return true;
    }

    public boolean a(int i) {
        ViewOffsetHelper viewOffsetHelper = this.f4186a;
        if (viewOffsetHelper != null) {
            return viewOffsetHelper.b(i);
        }
        this.b = i;
        return false;
    }

    public ViewOffsetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public int a() {
        ViewOffsetHelper viewOffsetHelper = this.f4186a;
        if (viewOffsetHelper != null) {
            return viewOffsetHelper.b();
        }
        return 0;
    }
}
