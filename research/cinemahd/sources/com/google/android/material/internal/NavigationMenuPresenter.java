package com.google.android.material.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.view.menu.SubMenuBuilder;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.R$dimen;
import com.google.android.material.R$layout;
import java.util.ArrayList;

public class NavigationMenuPresenter implements MenuPresenter {

    /* renamed from: a  reason: collision with root package name */
    private NavigationMenuView f4250a;
    LinearLayout b;
    private MenuPresenter.Callback c;
    MenuBuilder d;
    private int e;
    NavigationMenuAdapter f;
    LayoutInflater g;
    int h;
    boolean i;
    ColorStateList j;
    ColorStateList k;
    Drawable l;
    int m;
    int n;
    private int o;
    int p;
    final View.OnClickListener q = new View.OnClickListener() {
        public void onClick(View view) {
            NavigationMenuPresenter.this.b(true);
            MenuItemImpl itemData = ((NavigationMenuItemView) view).getItemData();
            NavigationMenuPresenter navigationMenuPresenter = NavigationMenuPresenter.this;
            boolean a2 = navigationMenuPresenter.d.a((MenuItem) itemData, (MenuPresenter) navigationMenuPresenter, 0);
            if (itemData != null && itemData.isCheckable() && a2) {
                NavigationMenuPresenter.this.f.a(itemData);
            }
            NavigationMenuPresenter.this.b(false);
            NavigationMenuPresenter.this.a(false);
        }
    };

    private static class HeaderViewHolder extends ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    private class NavigationMenuAdapter extends RecyclerView.Adapter<ViewHolder> {

        /* renamed from: a  reason: collision with root package name */
        private final ArrayList<NavigationMenuItem> f4252a = new ArrayList<>();
        private MenuItemImpl b;
        private boolean c;

        NavigationMenuAdapter() {
            e();
        }

        private void e() {
            if (!this.c) {
                this.c = true;
                this.f4252a.clear();
                this.f4252a.add(new NavigationMenuHeaderItem());
                int size = NavigationMenuPresenter.this.d.n().size();
                int i = -1;
                boolean z = false;
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    MenuItemImpl menuItemImpl = NavigationMenuPresenter.this.d.n().get(i3);
                    if (menuItemImpl.isChecked()) {
                        a(menuItemImpl);
                    }
                    if (menuItemImpl.isCheckable()) {
                        menuItemImpl.c(false);
                    }
                    if (menuItemImpl.hasSubMenu()) {
                        SubMenu subMenu = menuItemImpl.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i3 != 0) {
                                this.f4252a.add(new NavigationMenuSeparatorItem(NavigationMenuPresenter.this.p, 0));
                            }
                            this.f4252a.add(new NavigationMenuTextItem(menuItemImpl));
                            int size2 = this.f4252a.size();
                            int size3 = subMenu.size();
                            boolean z2 = false;
                            for (int i4 = 0; i4 < size3; i4++) {
                                MenuItemImpl menuItemImpl2 = (MenuItemImpl) subMenu.getItem(i4);
                                if (menuItemImpl2.isVisible()) {
                                    if (!z2 && menuItemImpl2.getIcon() != null) {
                                        z2 = true;
                                    }
                                    if (menuItemImpl2.isCheckable()) {
                                        menuItemImpl2.c(false);
                                    }
                                    if (menuItemImpl.isChecked()) {
                                        a(menuItemImpl);
                                    }
                                    this.f4252a.add(new NavigationMenuTextItem(menuItemImpl2));
                                }
                            }
                            if (z2) {
                                a(size2, this.f4252a.size());
                            }
                        }
                    } else {
                        int groupId = menuItemImpl.getGroupId();
                        if (groupId != i) {
                            i2 = this.f4252a.size();
                            boolean z3 = menuItemImpl.getIcon() != null;
                            if (i3 != 0) {
                                i2++;
                                ArrayList<NavigationMenuItem> arrayList = this.f4252a;
                                int i5 = NavigationMenuPresenter.this.p;
                                arrayList.add(new NavigationMenuSeparatorItem(i5, i5));
                            }
                            z = z3;
                        } else if (!z && menuItemImpl.getIcon() != null) {
                            a(i2, this.f4252a.size());
                            z = true;
                        }
                        NavigationMenuTextItem navigationMenuTextItem = new NavigationMenuTextItem(menuItemImpl);
                        navigationMenuTextItem.b = z;
                        this.f4252a.add(navigationMenuTextItem);
                        i = groupId;
                    }
                }
                this.c = false;
            }
        }

        /* renamed from: a */
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) viewHolder.itemView;
                navigationMenuItemView.setIconTintList(NavigationMenuPresenter.this.k);
                NavigationMenuPresenter navigationMenuPresenter = NavigationMenuPresenter.this;
                if (navigationMenuPresenter.i) {
                    navigationMenuItemView.setTextAppearance(navigationMenuPresenter.h);
                }
                ColorStateList colorStateList = NavigationMenuPresenter.this.j;
                if (colorStateList != null) {
                    navigationMenuItemView.setTextColor(colorStateList);
                }
                Drawable drawable = NavigationMenuPresenter.this.l;
                ViewCompat.a((View) navigationMenuItemView, drawable != null ? drawable.getConstantState().newDrawable() : null);
                NavigationMenuTextItem navigationMenuTextItem = (NavigationMenuTextItem) this.f4252a.get(i);
                navigationMenuItemView.setNeedsEmptyIcon(navigationMenuTextItem.b);
                navigationMenuItemView.setHorizontalPadding(NavigationMenuPresenter.this.m);
                navigationMenuItemView.setIconPadding(NavigationMenuPresenter.this.n);
                navigationMenuItemView.a(navigationMenuTextItem.a(), 0);
            } else if (itemViewType == 1) {
                ((TextView) viewHolder.itemView).setText(((NavigationMenuTextItem) this.f4252a.get(i)).a().getTitle());
            } else if (itemViewType == 2) {
                NavigationMenuSeparatorItem navigationMenuSeparatorItem = (NavigationMenuSeparatorItem) this.f4252a.get(i);
                viewHolder.itemView.setPadding(0, navigationMenuSeparatorItem.b(), 0, navigationMenuSeparatorItem.a());
            }
        }

        public Bundle b() {
            Bundle bundle = new Bundle();
            MenuItemImpl menuItemImpl = this.b;
            if (menuItemImpl != null) {
                bundle.putInt("android:menu:checked", menuItemImpl.getItemId());
            }
            SparseArray sparseArray = new SparseArray();
            int size = this.f4252a.size();
            for (int i = 0; i < size; i++) {
                NavigationMenuItem navigationMenuItem = this.f4252a.get(i);
                if (navigationMenuItem instanceof NavigationMenuTextItem) {
                    MenuItemImpl a2 = ((NavigationMenuTextItem) navigationMenuItem).a();
                    View actionView = a2 != null ? a2.getActionView() : null;
                    if (actionView != null) {
                        ParcelableSparseArray parcelableSparseArray = new ParcelableSparseArray();
                        actionView.saveHierarchyState(parcelableSparseArray);
                        sparseArray.put(a2.getItemId(), parcelableSparseArray);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        public MenuItemImpl c() {
            return this.b;
        }

        public void d() {
            e();
            notifyDataSetChanged();
        }

        public int getItemCount() {
            return this.f4252a.size();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public int getItemViewType(int i) {
            NavigationMenuItem navigationMenuItem = this.f4252a.get(i);
            if (navigationMenuItem instanceof NavigationMenuSeparatorItem) {
                return 2;
            }
            if (navigationMenuItem instanceof NavigationMenuHeaderItem) {
                return 3;
            }
            if (navigationMenuItem instanceof NavigationMenuTextItem) {
                return ((NavigationMenuTextItem) navigationMenuItem).a().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            if (i == 0) {
                NavigationMenuPresenter navigationMenuPresenter = NavigationMenuPresenter.this;
                return new NormalViewHolder(navigationMenuPresenter.g, viewGroup, navigationMenuPresenter.q);
            } else if (i == 1) {
                return new SubheaderViewHolder(NavigationMenuPresenter.this.g, viewGroup);
            } else {
                if (i == 2) {
                    return new SeparatorViewHolder(NavigationMenuPresenter.this.g, viewGroup);
                }
                if (i != 3) {
                    return null;
                }
                return new HeaderViewHolder(NavigationMenuPresenter.this.b);
            }
        }

        /* renamed from: a */
        public void onViewRecycled(ViewHolder viewHolder) {
            if (viewHolder instanceof NormalViewHolder) {
                ((NavigationMenuItemView) viewHolder.itemView).d();
            }
        }

        private void a(int i, int i2) {
            while (i < i2) {
                ((NavigationMenuTextItem) this.f4252a.get(i)).b = true;
                i++;
            }
        }

        public void a(MenuItemImpl menuItemImpl) {
            if (this.b != menuItemImpl && menuItemImpl.isCheckable()) {
                MenuItemImpl menuItemImpl2 = this.b;
                if (menuItemImpl2 != null) {
                    menuItemImpl2.setChecked(false);
                }
                this.b = menuItemImpl;
                menuItemImpl.setChecked(true);
            }
        }

        public void a(Bundle bundle) {
            MenuItemImpl a2;
            View actionView;
            ParcelableSparseArray parcelableSparseArray;
            MenuItemImpl a3;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.c = true;
                int size = this.f4252a.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    NavigationMenuItem navigationMenuItem = this.f4252a.get(i2);
                    if ((navigationMenuItem instanceof NavigationMenuTextItem) && (a3 = ((NavigationMenuTextItem) navigationMenuItem).a()) != null && a3.getItemId() == i) {
                        a(a3);
                        break;
                    }
                    i2++;
                }
                this.c = false;
                e();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.f4252a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    NavigationMenuItem navigationMenuItem2 = this.f4252a.get(i3);
                    if (!(!(navigationMenuItem2 instanceof NavigationMenuTextItem) || (a2 = ((NavigationMenuTextItem) navigationMenuItem2).a()) == null || (actionView = a2.getActionView()) == null || (parcelableSparseArray = (ParcelableSparseArray) sparseParcelableArray.get(a2.getItemId())) == null)) {
                        actionView.restoreHierarchyState(parcelableSparseArray);
                    }
                }
            }
        }

        public void a(boolean z) {
            this.c = z;
        }
    }

    private static class NavigationMenuHeaderItem implements NavigationMenuItem {
        NavigationMenuHeaderItem() {
        }
    }

    private interface NavigationMenuItem {
    }

    private static class NavigationMenuSeparatorItem implements NavigationMenuItem {

        /* renamed from: a  reason: collision with root package name */
        private final int f4253a;
        private final int b;

        public NavigationMenuSeparatorItem(int i, int i2) {
            this.f4253a = i;
            this.b = i2;
        }

        public int a() {
            return this.b;
        }

        public int b() {
            return this.f4253a;
        }
    }

    private static class NavigationMenuTextItem implements NavigationMenuItem {

        /* renamed from: a  reason: collision with root package name */
        private final MenuItemImpl f4254a;
        boolean b;

        NavigationMenuTextItem(MenuItemImpl menuItemImpl) {
            this.f4254a = menuItemImpl;
        }

        public MenuItemImpl a() {
            return this.f4254a;
        }
    }

    private static class NormalViewHolder extends ViewHolder {
        public NormalViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(R$layout.design_navigation_item, viewGroup, false));
            this.itemView.setOnClickListener(onClickListener);
        }
    }

    private static class SeparatorViewHolder extends ViewHolder {
        public SeparatorViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R$layout.design_navigation_item_separator, viewGroup, false));
        }
    }

    private static class SubheaderViewHolder extends ViewHolder {
        public SubheaderViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R$layout.design_navigation_item_subheader, viewGroup, false));
        }
    }

    private static abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View view) {
            super(view);
        }
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        this.g = LayoutInflater.from(context);
        this.d = menuBuilder;
        this.p = context.getResources().getDimensionPixelOffset(R$dimen.design_navigation_separator_vertical_padding);
    }

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        return false;
    }

    public View b(int i2) {
        View inflate = this.g.inflate(i2, this.b, false);
        a(inflate);
        return inflate;
    }

    public boolean b() {
        return false;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public void c(int i2) {
        this.e = i2;
    }

    public int d() {
        return this.b.getChildCount();
    }

    public Drawable e() {
        return this.l;
    }

    public void f(int i2) {
        this.h = i2;
        this.i = true;
        a(false);
    }

    public int g() {
        return this.n;
    }

    public int getId() {
        return this.e;
    }

    public ColorStateList h() {
        return this.j;
    }

    public ColorStateList i() {
        return this.k;
    }

    public MenuItemImpl c() {
        return this.f.c();
    }

    public void d(int i2) {
        this.m = i2;
        a(false);
    }

    public void e(int i2) {
        this.n = i2;
        a(false);
    }

    public void b(ColorStateList colorStateList) {
        this.j = colorStateList;
        a(false);
    }

    public int f() {
        return this.m;
    }

    public void b(boolean z) {
        NavigationMenuAdapter navigationMenuAdapter = this.f;
        if (navigationMenuAdapter != null) {
            navigationMenuAdapter.a(z);
        }
    }

    public MenuView a(ViewGroup viewGroup) {
        if (this.f4250a == null) {
            this.f4250a = (NavigationMenuView) this.g.inflate(R$layout.design_navigation_menu, viewGroup, false);
            if (this.f == null) {
                this.f = new NavigationMenuAdapter();
            }
            this.b = (LinearLayout) this.g.inflate(R$layout.design_navigation_item_header, this.f4250a, false);
            this.f4250a.setAdapter(this.f);
        }
        return this.f4250a;
    }

    public void a(boolean z) {
        NavigationMenuAdapter navigationMenuAdapter = this.f;
        if (navigationMenuAdapter != null) {
            navigationMenuAdapter.d();
        }
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        MenuPresenter.Callback callback = this.c;
        if (callback != null) {
            callback.a(menuBuilder, z);
        }
    }

    public Parcelable a() {
        Bundle bundle = new Bundle();
        if (this.f4250a != null) {
            SparseArray sparseArray = new SparseArray();
            this.f4250a.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        NavigationMenuAdapter navigationMenuAdapter = this.f;
        if (navigationMenuAdapter != null) {
            bundle.putBundle("android:menu:adapter", navigationMenuAdapter.b());
        }
        if (this.b != null) {
            SparseArray sparseArray2 = new SparseArray();
            this.b.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.f4250a.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.f.a(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.b.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    public void a(MenuItemImpl menuItemImpl) {
        this.f.a(menuItemImpl);
    }

    public void a(View view) {
        this.b.addView(view);
        NavigationMenuView navigationMenuView = this.f4250a;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    public View a(int i2) {
        return this.b.getChildAt(i2);
    }

    public void a(ColorStateList colorStateList) {
        this.k = colorStateList;
        a(false);
    }

    public void a(Drawable drawable) {
        this.l = drawable;
        a(false);
    }

    public void a(WindowInsetsCompat windowInsetsCompat) {
        int e2 = windowInsetsCompat.e();
        if (this.o != e2) {
            this.o = e2;
            if (this.b.getChildCount() == 0) {
                NavigationMenuView navigationMenuView = this.f4250a;
                navigationMenuView.setPadding(0, this.o, 0, navigationMenuView.getPaddingBottom());
            }
        }
        ViewCompat.a((View) this.b, windowInsetsCompat);
    }
}
