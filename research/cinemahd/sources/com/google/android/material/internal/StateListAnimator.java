package com.google.android.material.internal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.StateSet;
import java.util.ArrayList;

public final class StateListAnimator {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<Tuple> f4257a = new ArrayList<>();
    private Tuple b = null;
    ValueAnimator c = null;
    private final Animator.AnimatorListener d = new AnimatorListenerAdapter() {
        public void onAnimationEnd(Animator animator) {
            StateListAnimator stateListAnimator = StateListAnimator.this;
            if (stateListAnimator.c == animator) {
                stateListAnimator.c = null;
            }
        }
    };

    static class Tuple {

        /* renamed from: a  reason: collision with root package name */
        final int[] f4259a;
        final ValueAnimator b;

        Tuple(int[] iArr, ValueAnimator valueAnimator) {
            this.f4259a = iArr;
            this.b = valueAnimator;
        }
    }

    private void b() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            this.c = null;
        }
    }

    public void a(int[] iArr, ValueAnimator valueAnimator) {
        Tuple tuple = new Tuple(iArr, valueAnimator);
        valueAnimator.addListener(this.d);
        this.f4257a.add(tuple);
    }

    public void a(int[] iArr) {
        Tuple tuple;
        int size = this.f4257a.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                tuple = null;
                break;
            }
            tuple = this.f4257a.get(i);
            if (StateSet.stateSetMatches(tuple.f4259a, iArr)) {
                break;
            }
            i++;
        }
        Tuple tuple2 = this.b;
        if (tuple != tuple2) {
            if (tuple2 != null) {
                b();
            }
            this.b = tuple;
            if (tuple != null) {
                a(tuple);
            }
        }
    }

    private void a(Tuple tuple) {
        this.c = tuple.b;
        this.c.start();
    }

    public void a() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.end();
            this.c = null;
        }
    }
}
