package com.google.android.material.circularreveal;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.util.Property;
import com.google.android.material.circularreveal.CircularRevealHelper;
import com.google.android.material.math.MathUtils;

public interface CircularRevealWidget extends CircularRevealHelper.Delegate {

    public static class CircularRevealEvaluator implements TypeEvaluator<RevealInfo> {
        public static final TypeEvaluator<RevealInfo> b = new CircularRevealEvaluator();

        /* renamed from: a  reason: collision with root package name */
        private final RevealInfo f4228a = new RevealInfo();

        /* renamed from: a */
        public RevealInfo evaluate(float f, RevealInfo revealInfo, RevealInfo revealInfo2) {
            this.f4228a.a(MathUtils.a(revealInfo.f4231a, revealInfo2.f4231a, f), MathUtils.a(revealInfo.b, revealInfo2.b, f), MathUtils.a(revealInfo.c, revealInfo2.c, f));
            return this.f4228a;
        }
    }

    public static class CircularRevealProperty extends Property<CircularRevealWidget, RevealInfo> {

        /* renamed from: a  reason: collision with root package name */
        public static final Property<CircularRevealWidget, RevealInfo> f4229a = new CircularRevealProperty("circularReveal");

        private CircularRevealProperty(String str) {
            super(RevealInfo.class, str);
        }

        /* renamed from: a */
        public RevealInfo get(CircularRevealWidget circularRevealWidget) {
            return circularRevealWidget.getRevealInfo();
        }

        /* renamed from: a */
        public void set(CircularRevealWidget circularRevealWidget, RevealInfo revealInfo) {
            circularRevealWidget.setRevealInfo(revealInfo);
        }
    }

    public static class CircularRevealScrimColorProperty extends Property<CircularRevealWidget, Integer> {

        /* renamed from: a  reason: collision with root package name */
        public static final Property<CircularRevealWidget, Integer> f4230a = new CircularRevealScrimColorProperty("circularRevealScrimColor");

        private CircularRevealScrimColorProperty(String str) {
            super(Integer.class, str);
        }

        /* renamed from: a */
        public Integer get(CircularRevealWidget circularRevealWidget) {
            return Integer.valueOf(circularRevealWidget.getCircularRevealScrimColor());
        }

        /* renamed from: a */
        public void set(CircularRevealWidget circularRevealWidget, Integer num) {
            circularRevealWidget.setCircularRevealScrimColor(num.intValue());
        }
    }

    public static class RevealInfo {

        /* renamed from: a  reason: collision with root package name */
        public float f4231a;
        public float b;
        public float c;

        public void a(float f, float f2, float f3) {
            this.f4231a = f;
            this.b = f2;
            this.c = f3;
        }

        private RevealInfo() {
        }

        public RevealInfo(float f, float f2, float f3) {
            this.f4231a = f;
            this.b = f2;
            this.c = f3;
        }
    }

    void a();

    void b();

    int getCircularRevealScrimColor();

    RevealInfo getRevealInfo();

    void setCircularRevealOverlayDrawable(Drawable drawable);

    void setCircularRevealScrimColor(int i);

    void setRevealInfo(RevealInfo revealInfo);
}
