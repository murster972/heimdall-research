package com.google.android.material.circularreveal.cardview;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import androidx.cardview.widget.CardView;
import com.google.android.material.circularreveal.CircularRevealHelper;
import com.google.android.material.circularreveal.CircularRevealWidget;

public class CircularRevealCardView extends CardView implements CircularRevealWidget {
    private final CircularRevealHelper j;

    public void a() {
        this.j.a();
        throw null;
    }

    public void b() {
        this.j.b();
        throw null;
    }

    public void draw(Canvas canvas) {
        CircularRevealHelper circularRevealHelper = this.j;
        if (circularRevealHelper == null) {
            super.draw(canvas);
        } else {
            circularRevealHelper.a(canvas);
            throw null;
        }
    }

    public Drawable getCircularRevealOverlayDrawable() {
        this.j.c();
        throw null;
    }

    public int getCircularRevealScrimColor() {
        this.j.d();
        throw null;
    }

    public CircularRevealWidget.RevealInfo getRevealInfo() {
        this.j.e();
        throw null;
    }

    public boolean isOpaque() {
        CircularRevealHelper circularRevealHelper = this.j;
        if (circularRevealHelper == null) {
            return super.isOpaque();
        }
        circularRevealHelper.f();
        throw null;
    }

    public void setCircularRevealOverlayDrawable(Drawable drawable) {
        this.j.a(drawable);
        throw null;
    }

    public void setCircularRevealScrimColor(int i) {
        this.j.a(i);
        throw null;
    }

    public void setRevealInfo(CircularRevealWidget.RevealInfo revealInfo) {
        this.j.a(revealInfo);
        throw null;
    }
}
