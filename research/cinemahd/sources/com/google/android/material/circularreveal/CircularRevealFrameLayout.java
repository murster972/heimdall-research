package com.google.android.material.circularreveal;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import com.google.android.material.circularreveal.CircularRevealWidget;

public class CircularRevealFrameLayout extends FrameLayout implements CircularRevealWidget {

    /* renamed from: a  reason: collision with root package name */
    private final CircularRevealHelper f4226a;

    public void a() {
        this.f4226a.a();
        throw null;
    }

    public void b() {
        this.f4226a.b();
        throw null;
    }

    @SuppressLint({"MissingSuperCall"})
    public void draw(Canvas canvas) {
        CircularRevealHelper circularRevealHelper = this.f4226a;
        if (circularRevealHelper == null) {
            super.draw(canvas);
        } else {
            circularRevealHelper.a(canvas);
            throw null;
        }
    }

    public Drawable getCircularRevealOverlayDrawable() {
        this.f4226a.c();
        throw null;
    }

    public int getCircularRevealScrimColor() {
        this.f4226a.d();
        throw null;
    }

    public CircularRevealWidget.RevealInfo getRevealInfo() {
        this.f4226a.e();
        throw null;
    }

    public boolean isOpaque() {
        CircularRevealHelper circularRevealHelper = this.f4226a;
        if (circularRevealHelper == null) {
            return super.isOpaque();
        }
        circularRevealHelper.f();
        throw null;
    }

    public void setCircularRevealOverlayDrawable(Drawable drawable) {
        this.f4226a.a(drawable);
        throw null;
    }

    public void setCircularRevealScrimColor(int i) {
        this.f4226a.a(i);
        throw null;
    }

    public void setRevealInfo(CircularRevealWidget.RevealInfo revealInfo) {
        this.f4226a.a(revealInfo);
        throw null;
    }
}
