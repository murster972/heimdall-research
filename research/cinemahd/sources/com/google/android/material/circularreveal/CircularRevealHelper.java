package com.google.android.material.circularreveal;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import com.google.android.material.circularreveal.CircularRevealWidget;

public class CircularRevealHelper {

    /* renamed from: a  reason: collision with root package name */
    public static final int f4227a;

    interface Delegate {
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f4227a = 2;
        } else if (i >= 18) {
            f4227a = 1;
        } else {
            f4227a = 0;
        }
    }

    public void a() {
        throw null;
    }

    public void a(int i) {
        throw null;
    }

    public void a(Canvas canvas) {
        throw null;
    }

    public void a(Drawable drawable) {
        throw null;
    }

    public void a(CircularRevealWidget.RevealInfo revealInfo) {
        throw null;
    }

    public void b() {
        throw null;
    }

    public Drawable c() {
        throw null;
    }

    public int d() {
        throw null;
    }

    public CircularRevealWidget.RevealInfo e() {
        throw null;
    }

    public boolean f() {
        throw null;
    }
}
