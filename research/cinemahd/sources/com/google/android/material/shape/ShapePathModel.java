package com.google.android.material.shape;

public class ShapePathModel {
    private static final CornerTreatment i = new CornerTreatment();
    private static final EdgeTreatment j = new EdgeTreatment();

    /* renamed from: a  reason: collision with root package name */
    private CornerTreatment f4271a;
    private CornerTreatment b;
    private CornerTreatment c;
    private CornerTreatment d;
    private EdgeTreatment e;
    private EdgeTreatment f;
    private EdgeTreatment g;
    private EdgeTreatment h;

    public ShapePathModel() {
        CornerTreatment cornerTreatment = i;
        this.f4271a = cornerTreatment;
        this.b = cornerTreatment;
        this.c = cornerTreatment;
        this.d = cornerTreatment;
        EdgeTreatment edgeTreatment = j;
        this.e = edgeTreatment;
        this.f = edgeTreatment;
        this.g = edgeTreatment;
        this.h = edgeTreatment;
    }

    public void a(EdgeTreatment edgeTreatment) {
        this.e = edgeTreatment;
    }

    public CornerTreatment b() {
        return this.d;
    }

    public CornerTreatment c() {
        return this.c;
    }

    public EdgeTreatment d() {
        return this.h;
    }

    public EdgeTreatment e() {
        return this.f;
    }

    public EdgeTreatment f() {
        return this.e;
    }

    public CornerTreatment g() {
        return this.f4271a;
    }

    public CornerTreatment h() {
        return this.b;
    }

    public EdgeTreatment a() {
        return this.g;
    }
}
