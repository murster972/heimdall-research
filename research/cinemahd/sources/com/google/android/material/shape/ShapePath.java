package com.google.android.material.shape;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

public class ShapePath {

    /* renamed from: a  reason: collision with root package name */
    public float f4269a;
    public float b;
    public float c;
    public float d;
    private final List<PathOperation> e = new ArrayList();

    public static class PathArcOperation extends PathOperation {
        private static final RectF h = new RectF();
        public float b;
        public float c;
        public float d;
        public float e;
        public float f;
        public float g;

        public PathArcOperation(float f2, float f3, float f4, float f5) {
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = f5;
        }

        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.f4270a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            h.set(this.b, this.c, this.d, this.e);
            path.arcTo(h, this.f, this.g, false);
            path.transform(matrix);
        }
    }

    public static class PathLineOperation extends PathOperation {
        /* access modifiers changed from: private */
        public float b;
        /* access modifiers changed from: private */
        public float c;

        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.f4270a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            path.lineTo(this.b, this.c);
            path.transform(matrix);
        }
    }

    public static abstract class PathOperation {

        /* renamed from: a  reason: collision with root package name */
        protected final Matrix f4270a = new Matrix();

        public abstract void a(Matrix matrix, Path path);
    }

    public ShapePath() {
        b(0.0f, 0.0f);
    }

    public void a(float f, float f2) {
        PathLineOperation pathLineOperation = new PathLineOperation();
        float unused = pathLineOperation.b = f;
        float unused2 = pathLineOperation.c = f2;
        this.e.add(pathLineOperation);
        this.c = f;
        this.d = f2;
    }

    public void b(float f, float f2) {
        this.f4269a = f;
        this.b = f2;
        this.c = f;
        this.d = f2;
        this.e.clear();
    }

    public void a(float f, float f2, float f3, float f4, float f5, float f6) {
        PathArcOperation pathArcOperation = new PathArcOperation(f, f2, f3, f4);
        pathArcOperation.f = f5;
        pathArcOperation.g = f6;
        this.e.add(pathArcOperation);
        double d2 = (double) (f5 + f6);
        this.c = ((f + f3) * 0.5f) + (((f3 - f) / 2.0f) * ((float) Math.cos(Math.toRadians(d2))));
        this.d = ((f2 + f4) * 0.5f) + (((f4 - f2) / 2.0f) * ((float) Math.sin(Math.toRadians(d2))));
    }

    public void a(Matrix matrix, Path path) {
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(matrix, path);
        }
    }
}
