package com.google.android.material.navigation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.appcompat.R$attr;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.widget.TintTypedArray;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.customview.view.AbsSavedState;
import com.google.android.material.R$style;
import com.google.android.material.R$styleable;
import com.google.android.material.internal.NavigationMenu;
import com.google.android.material.internal.NavigationMenuPresenter;
import com.google.android.material.internal.ScrimInsetsFrameLayout;
import com.google.android.material.internal.ThemeEnforcement;

public class NavigationView extends ScrimInsetsFrameLayout {
    private static final int[] i = {16842912};
    private static final int[] j = {-16842910};
    private final NavigationMenu d;
    private final NavigationMenuPresenter e;
    OnNavigationItemSelectedListener f;
    private final int g;
    private MenuInflater h;

    public interface OnNavigationItemSelectedListener {
        boolean a(MenuItem menuItem);
    }

    public NavigationView(Context context) {
        this(context, (AttributeSet) null);
    }

    private ColorStateList d(int i2) {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(i2, typedValue, true)) {
            return null;
        }
        ColorStateList b = AppCompatResources.b(getContext(), typedValue.resourceId);
        if (!getContext().getTheme().resolveAttribute(R$attr.colorPrimary, typedValue, true)) {
            return null;
        }
        int i3 = typedValue.data;
        int defaultColor = b.getDefaultColor();
        return new ColorStateList(new int[][]{j, i, FrameLayout.EMPTY_STATE_SET}, new int[]{b.getColorForState(j, defaultColor), i3, defaultColor});
    }

    private MenuInflater getMenuInflater() {
        if (this.h == null) {
            this.h = new SupportMenuInflater(getContext());
        }
        return this.h;
    }

    /* access modifiers changed from: protected */
    public void a(WindowInsetsCompat windowInsetsCompat) {
        this.e.a(windowInsetsCompat);
    }

    public View b(int i2) {
        return this.e.b(i2);
    }

    public void c(int i2) {
        this.e.b(true);
        getMenuInflater().inflate(i2, this.d);
        this.e.b(false);
        this.e.a(false);
    }

    public MenuItem getCheckedItem() {
        return this.e.c();
    }

    public int getHeaderCount() {
        return this.e.d();
    }

    public Drawable getItemBackground() {
        return this.e.e();
    }

    public int getItemHorizontalPadding() {
        return this.e.f();
    }

    public int getItemIconPadding() {
        return this.e.g();
    }

    public ColorStateList getItemIconTintList() {
        return this.e.i();
    }

    public ColorStateList getItemTextColor() {
        return this.e.h();
    }

    public Menu getMenu() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            i2 = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i2), this.g), 1073741824);
        } else if (mode == 0) {
            i2 = View.MeasureSpec.makeMeasureSpec(this.g, 1073741824);
        }
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.a());
        this.d.b(savedState.c);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.c = new Bundle();
        this.d.d(savedState.c);
        return savedState;
    }

    public void setCheckedItem(int i2) {
        MenuItem findItem = this.d.findItem(i2);
        if (findItem != null) {
            this.e.a((MenuItemImpl) findItem);
        }
    }

    public void setItemBackground(Drawable drawable) {
        this.e.a(drawable);
    }

    public void setItemBackgroundResource(int i2) {
        setItemBackground(ContextCompat.c(getContext(), i2));
    }

    public void setItemHorizontalPadding(int i2) {
        this.e.d(i2);
    }

    public void setItemHorizontalPaddingResource(int i2) {
        this.e.d(getResources().getDimensionPixelSize(i2));
    }

    public void setItemIconPadding(int i2) {
        this.e.e(i2);
    }

    public void setItemIconPaddingResource(int i2) {
        this.e.e(getResources().getDimensionPixelSize(i2));
    }

    public void setItemIconTintList(ColorStateList colorStateList) {
        this.e.a(colorStateList);
    }

    public void setItemTextAppearance(int i2) {
        this.e.f(i2);
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.e.b(colorStateList);
    }

    public void setNavigationItemSelectedListener(OnNavigationItemSelectedListener onNavigationItemSelectedListener) {
        this.f = onNavigationItemSelectedListener;
    }

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.ClassLoaderCreator<SavedState>() {
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }

            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, (ClassLoader) null);
            }
        };
        public Bundle c;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.c = parcel.readBundle(classLoader);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.c);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }
    }

    public NavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, com.google.android.material.R$attr.navigationViewStyle);
    }

    public View a(int i2) {
        return this.e.a(i2);
    }

    public NavigationView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ColorStateList colorStateList;
        int i3;
        boolean z;
        this.e = new NavigationMenuPresenter();
        this.d = new NavigationMenu(context);
        TintTypedArray d2 = ThemeEnforcement.d(context, attributeSet, R$styleable.NavigationView, i2, R$style.Widget_Design_NavigationView, new int[0]);
        ViewCompat.a((View) this, d2.b(R$styleable.NavigationView_android_background));
        if (d2.g(R$styleable.NavigationView_elevation)) {
            ViewCompat.b((View) this, (float) d2.c(R$styleable.NavigationView_elevation, 0));
        }
        ViewCompat.a((View) this, d2.a(R$styleable.NavigationView_android_fitsSystemWindows, false));
        this.g = d2.c(R$styleable.NavigationView_android_maxWidth, 0);
        if (d2.g(R$styleable.NavigationView_itemIconTint)) {
            colorStateList = d2.a(R$styleable.NavigationView_itemIconTint);
        } else {
            colorStateList = d(16842808);
        }
        if (d2.g(R$styleable.NavigationView_itemTextAppearance)) {
            i3 = d2.g(R$styleable.NavigationView_itemTextAppearance, 0);
            z = true;
        } else {
            z = false;
            i3 = 0;
        }
        ColorStateList a2 = d2.g(R$styleable.NavigationView_itemTextColor) ? d2.a(R$styleable.NavigationView_itemTextColor) : null;
        if (!z && a2 == null) {
            a2 = d(16842806);
        }
        Drawable b = d2.b(R$styleable.NavigationView_itemBackground);
        if (d2.g(R$styleable.NavigationView_itemHorizontalPadding)) {
            this.e.d(d2.c(R$styleable.NavigationView_itemHorizontalPadding, 0));
        }
        int c = d2.c(R$styleable.NavigationView_itemIconPadding, 0);
        this.d.a((MenuBuilder.Callback) new MenuBuilder.Callback() {
            public void a(MenuBuilder menuBuilder) {
            }

            public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
                OnNavigationItemSelectedListener onNavigationItemSelectedListener = NavigationView.this.f;
                return onNavigationItemSelectedListener != null && onNavigationItemSelectedListener.a(menuItem);
            }
        });
        this.e.c(1);
        this.e.a(context, (MenuBuilder) this.d);
        this.e.a(colorStateList);
        if (z) {
            this.e.f(i3);
        }
        this.e.b(a2);
        this.e.a(b);
        this.e.e(c);
        this.d.a((MenuPresenter) this.e);
        addView((View) this.e.a((ViewGroup) this));
        if (d2.g(R$styleable.NavigationView_menu)) {
            c(d2.g(R$styleable.NavigationView_menu, 0));
        }
        if (d2.g(R$styleable.NavigationView_headerLayout)) {
            b(d2.g(R$styleable.NavigationView_headerLayout, 0));
        }
        d2.a();
    }

    public void setCheckedItem(MenuItem menuItem) {
        MenuItem findItem = this.d.findItem(menuItem.getItemId());
        if (findItem != null) {
            this.e.a((MenuItemImpl) findItem);
            return;
        }
        throw new IllegalArgumentException("Called setCheckedItem(MenuItem) with an item that is not in the current menu.");
    }
}
