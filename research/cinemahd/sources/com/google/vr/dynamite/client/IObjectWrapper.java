package com.google.vr.dynamite.client;

import android.os.IInterface;
import com.google.a.a.b;

public interface IObjectWrapper extends IInterface {

    public static abstract class a extends b implements IObjectWrapper {
        public a() {
            super("com.google.vr.dynamite.client.IObjectWrapper");
        }
    }
}
