package com.google.vr.dynamite.client;

import java.util.Objects;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final int f4451a;
    private final int b;
    private final int c;

    public f(int i, int i2, int i3) {
        this.f4451a = i;
        this.b = i2;
        this.c = i3;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.f4451a == fVar.f4451a && this.b == fVar.b && this.c == fVar.c;
    }

    public final int hashCode() {
        return Objects.hash(new Object[]{Integer.valueOf(this.f4451a), Integer.valueOf(this.b), Integer.valueOf(this.c)});
    }

    public final String toString() {
        return String.format("%d.%d.%d", new Object[]{Integer.valueOf(this.f4451a), Integer.valueOf(this.b), Integer.valueOf(this.c)});
    }
}
