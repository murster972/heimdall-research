package com.google.vr.dynamite.client;

import java.util.Objects;

final class e {

    /* renamed from: a  reason: collision with root package name */
    private final String f4450a;
    private final String b;

    public e(String str, String str2) {
        this.f4450a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f4450a;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof e) {
            e eVar = (e) obj;
            return Objects.equals(this.f4450a, eVar.f4450a) && Objects.equals(this.b, eVar.b);
        }
    }

    public final int hashCode() {
        return (Objects.hashCode(this.f4450a) * 37) + Objects.hashCode(this.b);
    }

    public final String toString() {
        return "[packageName=" + this.f4450a + ",libraryName=" + this.b + "]";
    }
}
