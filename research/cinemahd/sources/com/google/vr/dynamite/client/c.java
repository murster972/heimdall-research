package com.google.vr.dynamite.client;

public final class c extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private final int f4448a = 1;

    public c(int i) {
    }

    public final String toString() {
        int i = this.f4448a;
        String str = i != 1 ? i != 2 ? "Unknown error" : "Package obsolete" : "Package not available";
        StringBuilder sb = new StringBuilder(str.length() + 17);
        sb.append("LoaderException{");
        sb.append(str);
        sb.append("}");
        return sb.toString();
    }
}
