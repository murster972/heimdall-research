package com.google.vr.dynamite.client;

import android.content.Context;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UsedByNative
public final class DynamiteClient {

    /* renamed from: a  reason: collision with root package name */
    private static final ArrayMap<e, d> f4447a = new ArrayMap<>();

    private DynamiteClient() {
    }

    @UsedByNative
    public static synchronized int checkVersion(Context context, String str, String str2, String str3) {
        e eVar;
        synchronized (DynamiteClient.class) {
            f fVar = null;
            if (str3 != null) {
                try {
                    if (!str3.isEmpty()) {
                        Matcher matcher = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)").matcher(str3);
                        if (!matcher.matches()) {
                            String valueOf = String.valueOf(str3);
                            Log.w("Version", valueOf.length() != 0 ? "Failed to parse version from: ".concat(valueOf) : new String("Failed to parse version from: "));
                        } else {
                            fVar = new f(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3)));
                        }
                    }
                } catch (RemoteException | c | IllegalArgumentException | IllegalStateException | SecurityException | UnsatisfiedLinkError e) {
                    String valueOf2 = String.valueOf(eVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf2).length() + 54);
                    sb.append("Failed to load native library ");
                    sb.append(valueOf2);
                    sb.append(" from remote package:\n  ");
                    Log.e("DynamiteClient", sb.toString(), e);
                    return -1;
                } catch (Throwable th) {
                    throw th;
                }
            }
            if (fVar == null) {
                String valueOf3 = String.valueOf(str3);
                throw new IllegalArgumentException(valueOf3.length() != 0 ? "Improperly formatted minVersion string: ".concat(valueOf3) : new String("Improperly formatted minVersion string: "));
            }
            eVar = new e(str, str2);
            d remoteLibraryLoaderFromInfo = getRemoteLibraryLoaderFromInfo(eVar);
            INativeLibraryLoader newNativeLibraryLoader = remoteLibraryLoaderFromInfo.a(context).newNativeLibraryLoader(ObjectWrapper.a(remoteLibraryLoaderFromInfo.b(context)), ObjectWrapper.a(context));
            if (newNativeLibraryLoader == null) {
                String valueOf4 = String.valueOf(eVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf4).length() + 72);
                sb2.append("Failed to load native library ");
                sb2.append(valueOf4);
                sb2.append(" from remote package: no loader available.");
                Log.e("DynamiteClient", sb2.toString());
                return -1;
            }
            int checkVersion = newNativeLibraryLoader.checkVersion(str3);
            return checkVersion;
        }
    }

    @UsedByNative
    public static synchronized ClassLoader getRemoteClassLoader(Context context, String str, String str2) {
        synchronized (DynamiteClient.class) {
            e eVar = new e(str, str2);
            try {
                Context b = getRemoteLibraryLoaderFromInfo(eVar).b(context);
                if (b == null) {
                    return null;
                }
                ClassLoader classLoader = b.getClassLoader();
                return classLoader;
            } catch (c e) {
                String valueOf = String.valueOf(eVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 52);
                sb.append("Failed to get remote Context");
                sb.append(valueOf);
                sb.append(" from remote package:\n  ");
                Log.e("DynamiteClient", sb.toString(), e);
                return null;
            }
        }
    }

    @UsedByNative
    private static synchronized d getRemoteLibraryLoaderFromInfo(e eVar) {
        d dVar;
        synchronized (DynamiteClient.class) {
            dVar = f4447a.get(eVar);
            if (dVar == null) {
                dVar = new d(eVar);
                f4447a.put(eVar, dVar);
            }
        }
        return dVar;
    }

    @UsedByNative
    public static synchronized long loadNativeRemoteLibrary(Context context, String str, String str2) {
        synchronized (DynamiteClient.class) {
            e eVar = new e(str, str2);
            d remoteLibraryLoaderFromInfo = getRemoteLibraryLoaderFromInfo(eVar);
            try {
                INativeLibraryLoader newNativeLibraryLoader = remoteLibraryLoaderFromInfo.a(context).newNativeLibraryLoader(ObjectWrapper.a(remoteLibraryLoaderFromInfo.b(context)), ObjectWrapper.a(context));
                if (newNativeLibraryLoader == null) {
                    String valueOf = String.valueOf(eVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 72);
                    sb.append("Failed to load native library ");
                    sb.append(valueOf);
                    sb.append(" from remote package: no loader available.");
                    Log.e("DynamiteClient", sb.toString());
                    return 0;
                }
                long initializeAndLoadNativeLibrary = newNativeLibraryLoader.initializeAndLoadNativeLibrary(str2);
                return initializeAndLoadNativeLibrary;
            } catch (RemoteException | c | IllegalArgumentException | IllegalStateException | SecurityException | UnsatisfiedLinkError e) {
                String valueOf2 = String.valueOf(eVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 54);
                sb2.append("Failed to load native library ");
                sb2.append(valueOf2);
                sb2.append(" from remote package:\n  ");
                Log.e("DynamiteClient", sb2.toString(), e);
                return 0;
            }
        }
    }
}
