package com.google.gson;

import com.google.gson.internal.C$Gson$Preconditions;
import com.google.gson.internal.LazilyParsedNumber;
import java.math.BigInteger;

public final class JsonPrimitive extends JsonElement {
    private static final Class<?>[] b = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};

    /* renamed from: a  reason: collision with root package name */
    private Object f4394a;

    public JsonPrimitive(Boolean bool) {
        a((Object) bool);
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        if (obj instanceof Character) {
            this.f4394a = String.valueOf(((Character) obj).charValue());
            return;
        }
        C$Gson$Preconditions.a((obj instanceof Number) || b(obj));
        this.f4394a = obj;
    }

    public double b() {
        return q() ? o().doubleValue() : Double.parseDouble(i());
    }

    public float c() {
        return q() ? o().floatValue() : Float.parseFloat(i());
    }

    public int d() {
        return q() ? o().intValue() : Integer.parseInt(i());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || JsonPrimitive.class != obj.getClass()) {
            return false;
        }
        JsonPrimitive jsonPrimitive = (JsonPrimitive) obj;
        if (this.f4394a == null) {
            if (jsonPrimitive.f4394a == null) {
                return true;
            }
            return false;
        } else if (!a(this) || !a(jsonPrimitive)) {
            if (!(this.f4394a instanceof Number) || !(jsonPrimitive.f4394a instanceof Number)) {
                return this.f4394a.equals(jsonPrimitive.f4394a);
            }
            double doubleValue = o().doubleValue();
            double doubleValue2 = jsonPrimitive.o().doubleValue();
            if (doubleValue == doubleValue2) {
                return true;
            }
            if (!Double.isNaN(doubleValue) || !Double.isNaN(doubleValue2)) {
                return false;
            }
            return true;
        } else if (o().longValue() == jsonPrimitive.o().longValue()) {
            return true;
        } else {
            return false;
        }
    }

    public long h() {
        return q() ? o().longValue() : Long.parseLong(i());
    }

    public int hashCode() {
        long doubleToLongBits;
        if (this.f4394a == null) {
            return 31;
        }
        if (a(this)) {
            doubleToLongBits = o().longValue();
        } else {
            Object obj = this.f4394a;
            if (!(obj instanceof Number)) {
                return obj.hashCode();
            }
            doubleToLongBits = Double.doubleToLongBits(o().doubleValue());
        }
        return (int) ((doubleToLongBits >>> 32) ^ doubleToLongBits);
    }

    public String i() {
        if (q()) {
            return o().toString();
        }
        if (p()) {
            return n().toString();
        }
        return (String) this.f4394a;
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        return (Boolean) this.f4394a;
    }

    public Number o() {
        Object obj = this.f4394a;
        return obj instanceof String ? new LazilyParsedNumber((String) obj) : (Number) obj;
    }

    public boolean p() {
        return this.f4394a instanceof Boolean;
    }

    public boolean q() {
        return this.f4394a instanceof Number;
    }

    public boolean r() {
        return this.f4394a instanceof String;
    }

    private static boolean b(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> isAssignableFrom : b) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    public JsonPrimitive(Number number) {
        a((Object) number);
    }

    public JsonPrimitive(String str) {
        a((Object) str);
    }

    JsonPrimitive(Object obj) {
        a(obj);
    }

    public boolean a() {
        if (p()) {
            return n().booleanValue();
        }
        return Boolean.parseBoolean(i());
    }

    private static boolean a(JsonPrimitive jsonPrimitive) {
        Object obj = jsonPrimitive.f4394a;
        if (!(obj instanceof Number)) {
            return false;
        }
        Number number = (Number) obj;
        if ((number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
            return true;
        }
        return false;
    }
}
