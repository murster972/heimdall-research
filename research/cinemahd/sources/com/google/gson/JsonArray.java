package com.google.gson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class JsonArray extends JsonElement implements Iterable<JsonElement> {

    /* renamed from: a  reason: collision with root package name */
    private final List<JsonElement> f4391a = new ArrayList();

    public void a(String str) {
        this.f4391a.add(str == null ? JsonNull.f4392a : new JsonPrimitive(str));
    }

    public double b() {
        if (this.f4391a.size() == 1) {
            return this.f4391a.get(0).b();
        }
        throw new IllegalStateException();
    }

    public float c() {
        if (this.f4391a.size() == 1) {
            return this.f4391a.get(0).c();
        }
        throw new IllegalStateException();
    }

    public int d() {
        if (this.f4391a.size() == 1) {
            return this.f4391a.get(0).d();
        }
        throw new IllegalStateException();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof JsonArray) && ((JsonArray) obj).f4391a.equals(this.f4391a));
    }

    public JsonElement get(int i) {
        return this.f4391a.get(i);
    }

    public long h() {
        if (this.f4391a.size() == 1) {
            return this.f4391a.get(0).h();
        }
        throw new IllegalStateException();
    }

    public int hashCode() {
        return this.f4391a.hashCode();
    }

    public String i() {
        if (this.f4391a.size() == 1) {
            return this.f4391a.get(0).i();
        }
        throw new IllegalStateException();
    }

    public Iterator<JsonElement> iterator() {
        return this.f4391a.iterator();
    }

    public int size() {
        return this.f4391a.size();
    }

    public void a(JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = JsonNull.f4392a;
        }
        this.f4391a.add(jsonElement);
    }

    public boolean a() {
        if (this.f4391a.size() == 1) {
            return this.f4391a.get(0).a();
        }
        throw new IllegalStateException();
    }
}
