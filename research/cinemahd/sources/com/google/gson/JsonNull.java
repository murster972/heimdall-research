package com.google.gson;

public final class JsonNull extends JsonElement {

    /* renamed from: a  reason: collision with root package name */
    public static final JsonNull f4392a = new JsonNull();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof JsonNull);
    }

    public int hashCode() {
        return JsonNull.class.hashCode();
    }
}
