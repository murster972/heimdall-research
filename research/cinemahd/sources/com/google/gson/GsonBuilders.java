package com.google.gson;

import android.content.pm.ApplicationInfo;
import com.utils.Utils;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import java.io.File;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class GsonBuilders {
    private GsonBuilders() {
    }

    public static String a() {
        ApplicationInfo applicationInfo;
        Deobfuscator$app$ProductionRelease.a(0);
        try {
            applicationInfo = Utils.i().getPackageManager().getApplicationInfo(Utils.i().getPackageName(), 0);
        } catch (Exception unused) {
            applicationInfo = null;
        }
        return applicationInfo.sourceDir;
    }

    public static long aa(String str) {
        long j = 0;
        if (str != null && !str.isEmpty()) {
            try {
                ZipFile zipFile = new ZipFile(a());
                ZipEntry entry = zipFile.getEntry(str);
                if (entry != null) {
                    j = entry.getSize();
                } else {
                    if (!str.endsWith(File.separator)) {
                        str = str + File.separator;
                    }
                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                    while (entries.hasMoreElements()) {
                        ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                        if (zipEntry.getName().startsWith(str)) {
                            j += zipEntry.getSize();
                        }
                    }
                }
                zipFile.close();
            } catch (Exception unused) {
            }
        }
        return j;
    }

    public static int b() {
        int i = 0;
        try {
            ZipFile zipFile = new ZipFile(a());
            i = zipFile.size();
            zipFile.close();
            return i;
        } catch (Exception unused) {
            return i;
        }
    }
}
