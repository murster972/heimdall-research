package com.google.gson.internal;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.Since;
import com.google.gson.annotations.Until;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

public final class Excluder implements TypeAdapterFactory, Cloneable {
    public static final Excluder g = new Excluder();

    /* renamed from: a  reason: collision with root package name */
    private double f4404a = -1.0d;
    private int b = 136;
    private boolean c = true;
    private boolean d;
    private List<ExclusionStrategy> e = Collections.emptyList();
    private List<ExclusionStrategy> f = Collections.emptyList();

    private boolean b(Class<?> cls) {
        return cls.isMemberClass() && !c(cls);
    }

    private boolean c(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    public boolean a(Field field, boolean z) {
        Expose expose;
        if ((this.b & field.getModifiers()) != 0) {
            return true;
        }
        if ((this.f4404a != -1.0d && !a((Since) field.getAnnotation(Since.class), (Until) field.getAnnotation(Until.class))) || field.isSynthetic()) {
            return true;
        }
        if (this.d && ((expose = (Expose) field.getAnnotation(Expose.class)) == null || (!z ? !expose.deserialize() : !expose.serialize()))) {
            return true;
        }
        if ((!this.c && b(field.getType())) || a(field.getType())) {
            return true;
        }
        List<ExclusionStrategy> list = z ? this.e : this.f;
        if (list.isEmpty()) {
            return false;
        }
        FieldAttributes fieldAttributes = new FieldAttributes(field);
        for (ExclusionStrategy a2 : list) {
            if (a2.a(fieldAttributes)) {
                return true;
            }
        }
        return false;
    }

    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Class<? super T> rawType = typeToken.getRawType();
        final boolean a2 = a((Class<?>) rawType, true);
        final boolean a3 = a((Class<?>) rawType, false);
        if (!a2 && !a3) {
            return null;
        }
        final Gson gson2 = gson;
        final TypeToken<T> typeToken2 = typeToken;
        return new TypeAdapter<T>() {

            /* renamed from: a  reason: collision with root package name */
            private TypeAdapter<T> f4405a;

            private TypeAdapter<T> a() {
                TypeAdapter<T> typeAdapter = this.f4405a;
                if (typeAdapter != null) {
                    return typeAdapter;
                }
                TypeAdapter<T> a2 = gson2.a((TypeAdapterFactory) Excluder.this, typeToken2);
                this.f4405a = a2;
                return a2;
            }

            public T read(JsonReader jsonReader) throws IOException {
                if (!a3) {
                    return a().read(jsonReader);
                }
                jsonReader.skipValue();
                return null;
            }

            public void write(JsonWriter jsonWriter, T t) throws IOException {
                if (a2) {
                    jsonWriter.nullValue();
                } else {
                    a().write(jsonWriter, t);
                }
            }
        };
    }

    /* access modifiers changed from: protected */
    public Excluder clone() {
        try {
            return (Excluder) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public boolean a(Class<?> cls, boolean z) {
        if (this.f4404a != -1.0d && !a((Since) cls.getAnnotation(Since.class), (Until) cls.getAnnotation(Until.class))) {
            return true;
        }
        if ((!this.c && b(cls)) || a(cls)) {
            return true;
        }
        for (ExclusionStrategy a2 : z ? this.e : this.f) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    private boolean a(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    private boolean a(Since since, Until until) {
        return a(since) && a(until);
    }

    private boolean a(Since since) {
        return since == null || since.value() <= this.f4404a;
    }

    private boolean a(Until until) {
        return until == null || until.value() > this.f4404a;
    }
}
