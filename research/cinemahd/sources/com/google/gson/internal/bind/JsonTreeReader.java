package com.google.gson.internal.bind;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;

public final class JsonTreeReader extends JsonReader {
    private static final Reader e = new Reader() {
        public void close() throws IOException {
            throw new AssertionError();
        }

        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }
    };
    private static final Object f = new Object();

    /* renamed from: a  reason: collision with root package name */
    private Object[] f4422a = new Object[32];
    private int b = 0;
    private String[] c = new String[32];
    private int[] d = new int[32];

    public JsonTreeReader(JsonElement jsonElement) {
        super(e);
        a((Object) jsonElement);
    }

    private void a(JsonToken jsonToken) throws IOException {
        if (peek() != jsonToken) {
            throw new IllegalStateException("Expected " + jsonToken + " but was " + peek() + locationString());
        }
    }

    private String locationString() {
        return " at path " + getPath();
    }

    private Object t() {
        return this.f4422a[this.b - 1];
    }

    private Object u() {
        Object[] objArr = this.f4422a;
        int i = this.b - 1;
        this.b = i;
        Object obj = objArr[i];
        objArr[this.b] = null;
        return obj;
    }

    public void beginArray() throws IOException {
        a(JsonToken.BEGIN_ARRAY);
        a((Object) ((JsonArray) t()).iterator());
        this.d[this.b - 1] = 0;
    }

    public void beginObject() throws IOException {
        a(JsonToken.BEGIN_OBJECT);
        a((Object) ((JsonObject) t()).n().iterator());
    }

    public void close() throws IOException {
        this.f4422a = new Object[]{f};
        this.b = 1;
    }

    public void endArray() throws IOException {
        a(JsonToken.END_ARRAY);
        u();
        u();
        int i = this.b;
        if (i > 0) {
            int[] iArr = this.d;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    public void endObject() throws IOException {
        a(JsonToken.END_OBJECT);
        u();
        u();
        int i = this.b;
        if (i > 0) {
            int[] iArr = this.d;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    public String getPath() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = 0;
        while (i < this.b) {
            Object[] objArr = this.f4422a;
            if (objArr[i] instanceof JsonArray) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.d[i]);
                    sb.append(']');
                }
            } else if (objArr[i] instanceof JsonObject) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('.');
                    String[] strArr = this.c;
                    if (strArr[i] != null) {
                        sb.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return sb.toString();
    }

    public boolean hasNext() throws IOException {
        JsonToken peek = peek();
        return (peek == JsonToken.END_OBJECT || peek == JsonToken.END_ARRAY) ? false : true;
    }

    public boolean nextBoolean() throws IOException {
        a(JsonToken.BOOLEAN);
        boolean a2 = ((JsonPrimitive) u()).a();
        int i = this.b;
        if (i > 0) {
            int[] iArr = this.d;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return a2;
    }

    public double nextDouble() throws IOException {
        JsonToken peek = peek();
        if (peek == JsonToken.NUMBER || peek == JsonToken.STRING) {
            double b2 = ((JsonPrimitive) t()).b();
            if (isLenient() || (!Double.isNaN(b2) && !Double.isInfinite(b2))) {
                u();
                int i = this.b;
                if (i > 0) {
                    int[] iArr = this.d;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return b2;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + b2);
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + peek + locationString());
    }

    public int nextInt() throws IOException {
        JsonToken peek = peek();
        if (peek == JsonToken.NUMBER || peek == JsonToken.STRING) {
            int d2 = ((JsonPrimitive) t()).d();
            u();
            int i = this.b;
            if (i > 0) {
                int[] iArr = this.d;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return d2;
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + peek + locationString());
    }

    public long nextLong() throws IOException {
        JsonToken peek = peek();
        if (peek == JsonToken.NUMBER || peek == JsonToken.STRING) {
            long h = ((JsonPrimitive) t()).h();
            u();
            int i = this.b;
            if (i > 0) {
                int[] iArr = this.d;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return h;
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + peek + locationString());
    }

    public String nextName() throws IOException {
        a(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) t()).next();
        String str = (String) entry.getKey();
        this.c[this.b - 1] = str;
        a(entry.getValue());
        return str;
    }

    public void nextNull() throws IOException {
        a(JsonToken.NULL);
        u();
        int i = this.b;
        if (i > 0) {
            int[] iArr = this.d;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    public String nextString() throws IOException {
        JsonToken peek = peek();
        if (peek == JsonToken.STRING || peek == JsonToken.NUMBER) {
            String i = ((JsonPrimitive) u()).i();
            int i2 = this.b;
            if (i2 > 0) {
                int[] iArr = this.d;
                int i3 = i2 - 1;
                iArr[i3] = iArr[i3] + 1;
            }
            return i;
        }
        throw new IllegalStateException("Expected " + JsonToken.STRING + " but was " + peek + locationString());
    }

    public JsonToken peek() throws IOException {
        if (this.b == 0) {
            return JsonToken.END_DOCUMENT;
        }
        Object t = t();
        if (t instanceof Iterator) {
            boolean z = this.f4422a[this.b - 2] instanceof JsonObject;
            Iterator it2 = (Iterator) t;
            if (!it2.hasNext()) {
                return z ? JsonToken.END_OBJECT : JsonToken.END_ARRAY;
            }
            if (z) {
                return JsonToken.NAME;
            }
            a(it2.next());
            return peek();
        } else if (t instanceof JsonObject) {
            return JsonToken.BEGIN_OBJECT;
        } else {
            if (t instanceof JsonArray) {
                return JsonToken.BEGIN_ARRAY;
            }
            if (t instanceof JsonPrimitive) {
                JsonPrimitive jsonPrimitive = (JsonPrimitive) t;
                if (jsonPrimitive.r()) {
                    return JsonToken.STRING;
                }
                if (jsonPrimitive.p()) {
                    return JsonToken.BOOLEAN;
                }
                if (jsonPrimitive.q()) {
                    return JsonToken.NUMBER;
                }
                throw new AssertionError();
            } else if (t instanceof JsonNull) {
                return JsonToken.NULL;
            } else {
                if (t == f) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    public void s() throws IOException {
        a(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) t()).next();
        a(entry.getValue());
        a((Object) new JsonPrimitive((String) entry.getKey()));
    }

    public void skipValue() throws IOException {
        if (peek() == JsonToken.NAME) {
            nextName();
            this.c[this.b - 2] = "null";
        } else {
            u();
            int i = this.b;
            if (i > 0) {
                this.c[i - 1] = "null";
            }
        }
        int i2 = this.b;
        if (i2 > 0) {
            int[] iArr = this.d;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    public String toString() {
        return JsonTreeReader.class.getSimpleName();
    }

    private void a(Object obj) {
        int i = this.b;
        Object[] objArr = this.f4422a;
        if (i == objArr.length) {
            Object[] objArr2 = new Object[(i * 2)];
            int[] iArr = new int[(i * 2)];
            String[] strArr = new String[(i * 2)];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            System.arraycopy(this.d, 0, iArr, 0, this.b);
            System.arraycopy(this.c, 0, strArr, 0, this.b);
            this.f4422a = objArr2;
            this.d = iArr;
            this.c = strArr;
        }
        Object[] objArr3 = this.f4422a;
        int i2 = this.b;
        this.b = i2 + 1;
        objArr3[i2] = obj;
    }
}
