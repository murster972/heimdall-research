package com.google.gson.internal.bind;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class JsonTreeWriter extends JsonWriter {
    private static final Writer d = new Writer() {
        public void close() throws IOException {
            throw new AssertionError();
        }

        public void flush() throws IOException {
            throw new AssertionError();
        }

        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    };
    private static final JsonPrimitive e = new JsonPrimitive("closed");

    /* renamed from: a  reason: collision with root package name */
    private final List<JsonElement> f4423a = new ArrayList();
    private String b;
    private JsonElement c = JsonNull.f4392a;

    public JsonTreeWriter() {
        super(d);
    }

    private void a(JsonElement jsonElement) {
        if (this.b != null) {
            if (!jsonElement.k() || getSerializeNulls()) {
                ((JsonObject) peek()).a(this.b, jsonElement);
            }
            this.b = null;
        } else if (this.f4423a.isEmpty()) {
            this.c = jsonElement;
        } else {
            JsonElement peek = peek();
            if (peek instanceof JsonArray) {
                ((JsonArray) peek).a(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }

    private JsonElement peek() {
        List<JsonElement> list = this.f4423a;
        return list.get(list.size() - 1);
    }

    public JsonWriter beginArray() throws IOException {
        JsonArray jsonArray = new JsonArray();
        a(jsonArray);
        this.f4423a.add(jsonArray);
        return this;
    }

    public JsonWriter beginObject() throws IOException {
        JsonObject jsonObject = new JsonObject();
        a(jsonObject);
        this.f4423a.add(jsonObject);
        return this;
    }

    public void close() throws IOException {
        if (this.f4423a.isEmpty()) {
            this.f4423a.add(e);
            return;
        }
        throw new IOException("Incomplete document");
    }

    public JsonWriter endArray() throws IOException {
        if (this.f4423a.isEmpty() || this.b != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof JsonArray) {
            List<JsonElement> list = this.f4423a;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public JsonWriter endObject() throws IOException {
        if (this.f4423a.isEmpty() || this.b != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof JsonObject) {
            List<JsonElement> list = this.f4423a;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public void flush() throws IOException {
    }

    public JsonWriter name(String str) throws IOException {
        if (this.f4423a.isEmpty() || this.b != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof JsonObject) {
            this.b = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public JsonWriter nullValue() throws IOException {
        a(JsonNull.f4392a);
        return this;
    }

    public JsonElement s() {
        if (this.f4423a.isEmpty()) {
            return this.c;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.f4423a);
    }

    public JsonWriter value(String str) throws IOException {
        if (str == null) {
            return nullValue();
        }
        a(new JsonPrimitive(str));
        return this;
    }

    public JsonWriter value(boolean z) throws IOException {
        a(new JsonPrimitive(Boolean.valueOf(z)));
        return this;
    }

    public JsonWriter value(Boolean bool) throws IOException {
        if (bool == null) {
            return nullValue();
        }
        a(new JsonPrimitive(bool));
        return this;
    }

    public JsonWriter value(double d2) throws IOException {
        if (isLenient() || (!Double.isNaN(d2) && !Double.isInfinite(d2))) {
            a(new JsonPrimitive((Number) Double.valueOf(d2)));
            return this;
        }
        throw new IllegalArgumentException("JSON forbids NaN and infinities: " + d2);
    }

    public JsonWriter value(long j) throws IOException {
        a(new JsonPrimitive((Number) Long.valueOf(j)));
        return this;
    }

    public JsonWriter value(Number number) throws IOException {
        if (number == null) {
            return nullValue();
        }
        if (!isLenient()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a(new JsonPrimitive(number));
        return this;
    }
}
