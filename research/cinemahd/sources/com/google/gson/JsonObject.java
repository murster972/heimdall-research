package com.google.gson;

import com.google.gson.internal.LinkedTreeMap;
import java.util.Map;
import java.util.Set;

public final class JsonObject extends JsonElement {

    /* renamed from: a  reason: collision with root package name */
    private final LinkedTreeMap<String, JsonElement> f4393a = new LinkedTreeMap<>();

    public void a(String str, JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = JsonNull.f4392a;
        }
        this.f4393a.put(str, jsonElement);
    }

    public JsonArray b(String str) {
        return (JsonArray) this.f4393a.get(str);
    }

    public JsonObject c(String str) {
        return (JsonObject) this.f4393a.get(str);
    }

    public boolean d(String str) {
        return this.f4393a.containsKey(str);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof JsonObject) && ((JsonObject) obj).f4393a.equals(this.f4393a));
    }

    public int hashCode() {
        return this.f4393a.hashCode();
    }

    public Set<Map.Entry<String, JsonElement>> n() {
        return this.f4393a.entrySet();
    }

    public Set<String> o() {
        return this.f4393a.keySet();
    }

    public void a(String str, String str2) {
        a(str, a((Object) str2));
    }

    public void a(String str, Number number) {
        a(str, a((Object) number));
    }

    public void a(String str, Boolean bool) {
        a(str, a((Object) bool));
    }

    private JsonElement a(Object obj) {
        return obj == null ? JsonNull.f4392a : new JsonPrimitive(obj);
    }

    public JsonElement a(String str) {
        return this.f4393a.get(str);
    }
}
