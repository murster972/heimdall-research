package com.google.ar.core;

import com.applovin.sdk.AppLovinErrorCodes;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableException;

/* 'enum' modifier removed */
final class Q extends Session.a {
    Q(String str, int i, int i2) {
        super(str, 23, AppLovinErrorCodes.NO_NETWORK, (byte) 0);
    }

    public final void a() throws UnavailableException {
        throw new UnavailableApkTooOldException();
    }
}
