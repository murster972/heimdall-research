package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.ResourceExhaustedException;

/* 'enum' modifier removed */
final class D extends Session.a {
    D(String str, int i, int i2) {
        super(str, 11, -11, (byte) 0);
    }

    public final void a() {
        throw new ResourceExhaustedException();
    }
}
