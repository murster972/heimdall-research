package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.CameraNotAvailableException;

/* 'enum' modifier removed */
final class F extends Session.a {
    F(String str, int i, int i2) {
        super(str, 13, -13, (byte) 0);
    }

    public final void a() throws CameraNotAvailableException {
        throw new CameraNotAvailableException();
    }
}
