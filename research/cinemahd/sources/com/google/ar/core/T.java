package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

/* 'enum' modifier removed */
final class T extends Session.a {
    T(String str, int i, int i2) {
        super(str, 25, -105, (byte) 0);
    }

    public final void a() throws UnavailableException {
        throw new UnavailableUserDeclinedInstallationException();
    }
}
