package com.google.ar.core;

import com.google.ar.core.Session;

/* 'enum' modifier removed */
final class K extends Session.a {
    K(String str, int i, int i2) {
        super(str, 18, -20, (byte) 0);
    }

    public final void a() {
        throw new IllegalStateException("You have changed the camera configuration. You must call ArImage.close for all acquired images before calling Session.resume.");
    }
}
