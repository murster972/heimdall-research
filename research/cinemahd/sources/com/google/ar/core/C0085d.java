package com.google.ar.core;

import com.google.ar.core.ArCoreApk;

/* 'enum' modifier removed */
/* renamed from: com.google.ar.core.d  reason: case insensitive filesystem */
final class C0085d extends ArCoreApk.Availability {
    C0085d(String str, int i, int i2) {
        super(str, 3, 100, (ae) null);
    }

    public final boolean isUnsupported() {
        return true;
    }
}
