package com.google.ar.core;

import a.a.b;
import android.graphics.Rect;
import android.media.Image;
import com.google.ar.core.exceptions.FatalException;
import java.nio.ByteBuffer;

public class ArImage extends a.a.a {
    long nativeHandle;

    class a extends b {

        /* renamed from: a  reason: collision with root package name */
        private final long f4321a;
        private final int b;

        public a(long j, int i) {
            this.f4321a = j;
            this.b = i;
        }

        public final ByteBuffer getBuffer() {
            return ArImage.this.nativeGetBuffer(this.f4321a, this.b).asReadOnlyBuffer();
        }

        public final int getPixelStride() {
            int access$100 = ArImage.this.nativeGetPixelStride(this.f4321a, this.b);
            if (access$100 != -1) {
                return access$100;
            }
            throw new FatalException("Unknown error in ArImage.Plane.getPixelStride().");
        }

        public final int getRowStride() {
            int access$000 = ArImage.this.nativeGetRowStride(this.f4321a, this.b);
            if (access$000 != -1) {
                return access$000;
            }
            throw new FatalException("Unknown error in ArImage.Plane.getRowStride().");
        }
    }

    public ArImage(long j) {
        this.nativeHandle = j;
    }

    private native void nativeClose(long j);

    /* access modifiers changed from: private */
    public native ByteBuffer nativeGetBuffer(long j, int i);

    private native int nativeGetFormat(long j);

    private native int nativeGetHeight(long j);

    private native int nativeGetNumberOfPlanes(long j);

    /* access modifiers changed from: private */
    public native int nativeGetPixelStride(long j, int i);

    /* access modifiers changed from: private */
    public native int nativeGetRowStride(long j, int i);

    private native long nativeGetTimestamp(long j);

    private native int nativeGetWidth(long j);

    static native void nativeLoadSymbols();

    public void close() {
        nativeClose(this.nativeHandle);
        this.nativeHandle = 0;
    }

    public Rect getCropRect() {
        throw new UnsupportedOperationException("Crop rect is unknown in this image.");
    }

    public int getFormat() {
        int nativeGetFormat = nativeGetFormat(this.nativeHandle);
        if (nativeGetFormat != -1) {
            return nativeGetFormat;
        }
        throw new FatalException("Unknown error in ArImage.getFormat().");
    }

    public int getHeight() {
        int nativeGetHeight = nativeGetHeight(this.nativeHandle);
        if (nativeGetHeight != -1) {
            return nativeGetHeight;
        }
        throw new FatalException("Unknown error in ArImage.getHeight().");
    }

    public Image.Plane[] getPlanes() {
        int nativeGetNumberOfPlanes = nativeGetNumberOfPlanes(this.nativeHandle);
        if (nativeGetNumberOfPlanes != -1) {
            a[] aVarArr = new a[nativeGetNumberOfPlanes];
            for (int i = 0; i < nativeGetNumberOfPlanes; i++) {
                aVarArr[i] = new a(this.nativeHandle, i);
            }
            return aVarArr;
        }
        throw new FatalException("Unknown error in ArImage.getPlanes().");
    }

    public long getTimestamp() {
        long nativeGetTimestamp = nativeGetTimestamp(this.nativeHandle);
        if (nativeGetTimestamp != -1) {
            return nativeGetTimestamp;
        }
        throw new FatalException("Unknown error in ArImage.getTimestamp().");
    }

    public int getWidth() {
        int nativeGetWidth = nativeGetWidth(this.nativeHandle);
        if (nativeGetWidth != -1) {
            return nativeGetWidth;
        }
        throw new FatalException("Unknown error in ArImage.getWidth().");
    }

    public void setCropRect(Rect rect) {
        throw new UnsupportedOperationException("This is a read-only image.");
    }

    public void setTimestamp(long j) {
        throw new UnsupportedOperationException("This is a read-only image.");
    }
}
