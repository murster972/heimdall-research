package com.google.ar.core;

import com.google.ar.core.ArCoreApk;

/* 'enum' modifier removed */
/* renamed from: com.google.ar.core.b  reason: case insensitive filesystem */
final class C0083b extends ArCoreApk.Availability {
    C0083b(String str, int i, int i2) {
        super(str, 1, 1, (ae) null);
    }

    public final boolean isTransient() {
        return true;
    }

    public final boolean isUnknown() {
        return true;
    }
}
