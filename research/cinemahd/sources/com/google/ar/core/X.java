package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.NotTrackingException;

/* 'enum' modifier removed */
final class X extends Session.a {
    X(String str, int i, int i2) {
        super(str, 5, -5, (byte) 0);
    }

    public final void a() {
        throw new NotTrackingException();
    }
}
