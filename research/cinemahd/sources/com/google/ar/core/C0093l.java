package com.google.ar.core;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: com.google.ar.core.l  reason: case insensitive filesystem */
final class C0093l extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ InstallActivity f4331a;

    C0093l(InstallActivity installActivity) {
        this.f4331a = installActivity;
    }

    public final void onAnimationEnd(Animator animator) {
        this.f4331a.showSpinner();
    }
}
