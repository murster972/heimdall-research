package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableException;

/* 'enum' modifier removed */
final class O extends Session.a {
    O(String str, int i, int i2) {
        super(str, 21, -100, (byte) 0);
    }

    public final void a() throws UnavailableException {
        throw new UnavailableArcoreNotInstalledException();
    }
}
