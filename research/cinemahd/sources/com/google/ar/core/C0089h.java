package com.google.ar.core;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.exceptions.FatalException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

/* renamed from: com.google.ar.core.h  reason: case insensitive filesystem */
final class C0089h extends ArCoreApk {
    private static final C0089h k = new C0089h();

    /* renamed from: a  reason: collision with root package name */
    Exception f4327a;
    private boolean b;
    private int c;
    private long d;
    /* access modifiers changed from: private */
    public ArCoreApk.Availability e;
    /* access modifiers changed from: private */
    public boolean f;
    private C0094m g;
    private boolean h;
    private boolean i;
    private int j;

    C0089h() {
    }

    private static ArCoreApk.InstallStatus a(Activity activity) throws UnavailableDeviceNotCompatibleException, UnavailableUserDeclinedInstallationException {
        PendingIntent b2 = ae.b(activity);
        if (b2 != null) {
            try {
                Log.i("ARCore-ArCoreApk", "Starting setup activity");
                activity.startIntentSender(b2.getIntentSender(), (Intent) null, 0, 0, 0);
                return ArCoreApk.InstallStatus.INSTALL_REQUESTED;
            } catch (IntentSender.SendIntentException | RuntimeException e2) {
                Log.w("ARCore-ArCoreApk", "Setup activity launch failed", e2);
            }
        }
        return ArCoreApk.InstallStatus.INSTALLED;
    }

    public static C0089h b() {
        return k;
    }

    private static boolean c() {
        return Build.VERSION.SDK_INT >= 24;
    }

    private final boolean c(Context context) {
        e(context);
        return this.i;
    }

    private static int d(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.google.ar.core", 4);
            int i2 = packageInfo.versionCode;
            if (i2 == 0 && (packageInfo.services == null || packageInfo.services.length == 0)) {
                return -1;
            }
            return i2;
        } catch (PackageManager.NameNotFoundException unused) {
            return -1;
        }
    }

    private final synchronized void e(Context context) {
        if (!this.h) {
            PackageManager packageManager = context.getPackageManager();
            String packageName = context.getPackageName();
            try {
                Bundle bundle = packageManager.getApplicationInfo(packageName, 128).metaData;
                if (bundle.containsKey("com.google.ar.core")) {
                    this.i = bundle.getString("com.google.ar.core").equals("required");
                    if (bundle.containsKey("com.google.ar.core.min_apk_version")) {
                        this.j = bundle.getInt("com.google.ar.core.min_apk_version");
                        ActivityInfo[] activityInfoArr = packageManager.getPackageInfo(packageName, 1).activities;
                        String canonicalName = InstallActivity.class.getCanonicalName();
                        int length = activityInfoArr.length;
                        boolean z = false;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= length) {
                                break;
                            } else if (canonicalName.equals(activityInfoArr[i2].name)) {
                                z = true;
                                break;
                            } else {
                                i2++;
                            }
                        }
                        if (!z) {
                            String valueOf = String.valueOf(canonicalName);
                            throw new FatalException(valueOf.length() != 0 ? "Application manifest must contain activity ".concat(valueOf) : new String("Application manifest must contain activity "));
                        } else {
                            this.h = true;
                        }
                    } else {
                        throw new FatalException("Application manifest must contain meta-data com.google.ar.core.min_apk_version");
                    }
                } else {
                    throw new FatalException("Application manifest must contain meta-data com.google.ar.core");
                }
            } catch (PackageManager.NameNotFoundException e2) {
                throw new FatalException("Could not load application package metadata", e2);
            } catch (PackageManager.NameNotFoundException e3) {
                throw new FatalException("Could not load application package info", e3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized C0094m a(Context context) {
        if (this.g == null) {
            C0094m mVar = new C0094m((byte) 0);
            mVar.a(context.getApplicationContext());
            this.g = mVar;
        }
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        if (this.f4327a == null) {
            this.c = 0;
        }
        this.b = false;
        if (this.g != null) {
            this.g.a();
            this.g = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean b(Context context) {
        e(context);
        return d(context) == 0 || d(context) >= this.j;
    }

    public final ArCoreApk.Availability checkAvailability(Context context) {
        ArCoreApk.Availability availability;
        if (!c()) {
            return ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE;
        }
        try {
            if (b(context)) {
                a();
                return ae.a(context);
            }
            synchronized (this) {
                if ((this.e == null || this.e.isUnknown()) && !this.f) {
                    this.f = true;
                    ae aeVar = new ae(this);
                    if (b(context)) {
                        availability = ArCoreApk.Availability.SUPPORTED_INSTALLED;
                    } else if (d(context) != -1) {
                        availability = ArCoreApk.Availability.SUPPORTED_APK_TOO_OLD;
                    } else if (c(context)) {
                        availability = ArCoreApk.Availability.SUPPORTED_NOT_INSTALLED;
                    } else {
                        a(context).a(context, (ArCoreApk.a) aeVar);
                    }
                    aeVar.a(availability);
                }
                if (this.e != null) {
                    ArCoreApk.Availability availability2 = this.e;
                    return availability2;
                } else if (this.f) {
                    ArCoreApk.Availability availability3 = ArCoreApk.Availability.UNKNOWN_CHECKING;
                    return availability3;
                } else {
                    Log.e("ARCore-ArCoreApk", "request not running but result is null?");
                    ArCoreApk.Availability availability4 = ArCoreApk.Availability.UNKNOWN_ERROR;
                    return availability4;
                }
            }
        } catch (FatalException e2) {
            Log.e("ARCore-ArCoreApk", "Error while checking app details and ARCore status", e2);
            return ArCoreApk.Availability.UNKNOWN_ERROR;
        }
    }

    public final ArCoreApk.InstallStatus requestInstall(Activity activity, boolean z) throws UnavailableDeviceNotCompatibleException, UnavailableUserDeclinedInstallationException {
        return requestInstall(activity, z, c(activity) ? ArCoreApk.InstallBehavior.REQUIRED : ArCoreApk.InstallBehavior.OPTIONAL, c(activity) ? ArCoreApk.UserMessageType.APPLICATION : ArCoreApk.UserMessageType.FEATURE);
    }

    public final ArCoreApk.InstallStatus requestInstall(Activity activity, boolean z, ArCoreApk.InstallBehavior installBehavior, ArCoreApk.UserMessageType userMessageType) throws UnavailableDeviceNotCompatibleException, UnavailableUserDeclinedInstallationException {
        if (!c()) {
            throw new UnavailableDeviceNotCompatibleException();
        } else if (b(activity)) {
            a();
            return a(activity);
        } else if (this.b) {
            return ArCoreApk.InstallStatus.INSTALL_REQUESTED;
        } else {
            Exception exc = this.f4327a;
            if (exc != null) {
                if (z) {
                    Log.w("ARCore-ArCoreApk", "Clearing previous failure: ", exc);
                    this.f4327a = null;
                } else if (exc instanceof UnavailableDeviceNotCompatibleException) {
                    throw ((UnavailableDeviceNotCompatibleException) exc);
                } else if (exc instanceof UnavailableUserDeclinedInstallationException) {
                    throw ((UnavailableUserDeclinedInstallationException) exc);
                } else if (exc instanceof RuntimeException) {
                    throw ((RuntimeException) exc);
                } else {
                    throw new RuntimeException("Unexpected exception type", exc);
                }
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            if (uptimeMillis - this.d > 5000) {
                this.c = 0;
            }
            this.c++;
            this.d = uptimeMillis;
            if (this.c <= 2) {
                try {
                    activity.startActivity(new Intent(activity, InstallActivity.class).putExtra("message", userMessageType).putExtra("behavior", installBehavior));
                    this.b = true;
                    return ArCoreApk.InstallStatus.INSTALL_REQUESTED;
                } catch (ActivityNotFoundException e2) {
                    throw new FatalException("Failed to launch InstallActivity", e2);
                }
            } else {
                throw new FatalException("Requesting ARCore installation too rapidly.");
            }
        }
    }
}
