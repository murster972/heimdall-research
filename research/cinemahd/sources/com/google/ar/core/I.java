package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.DataInvalidFormatException;

/* 'enum' modifier removed */
final class I extends Session.a {
    I(String str, int i, int i2) {
        super(str, 16, -18, (byte) 0);
    }

    public final void a() {
        throw new DataInvalidFormatException();
    }
}
