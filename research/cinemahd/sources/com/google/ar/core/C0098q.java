package com.google.ar.core;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.google.ar.core.ArCoreApk;

/* renamed from: com.google.ar.core.q  reason: case insensitive filesystem */
final class C0098q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Context f4336a;
    final /* synthetic */ ArCoreApk.a b;
    private final /* synthetic */ C0094m c;

    C0098q(C0094m mVar, Context context, ArCoreApk.a aVar) {
        this.c = mVar;
        this.f4336a = context;
        this.b = aVar;
    }

    public final void run() {
        try {
            this.c.d.a(this.f4336a.getApplicationInfo().packageName, C0094m.b(), new C0099r(this));
        } catch (RemoteException e) {
            Log.e("ARCore-InstallService", "requestInfo threw", e);
            this.b.a(ArCoreApk.Availability.UNKNOWN_ERROR);
        }
    }
}
