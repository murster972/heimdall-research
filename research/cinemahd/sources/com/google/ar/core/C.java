package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.DeadlineExceededException;

/* 'enum' modifier removed */
final class C extends Session.a {
    C(String str, int i, int i2) {
        super(str, 10, -10, (byte) 0);
    }

    public final void a() {
        throw new DeadlineExceededException();
    }
}
