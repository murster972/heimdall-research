package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.SessionNotPausedException;

/* 'enum' modifier removed */
final class W extends Session.a {
    W(String str, int i, int i2) {
        super(str, 4, -4, (byte) 0);
    }

    public final void a() {
        throw new SessionNotPausedException();
    }
}
