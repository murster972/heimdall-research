package com.google.ar.core;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.applovin.sdk.AppLovinErrorCodes;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.annotations.UsedByNative;
import com.google.ar.core.exceptions.ResourceExhaustedException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;
import java.util.HashMap;
import java.util.Map;

@UsedByNative("arcoreapk.cc")
class ArCoreApkJniAdapter {

    /* renamed from: a  reason: collision with root package name */
    private static Map<Class<? extends Throwable>, Integer> f4320a;

    static {
        HashMap hashMap = new HashMap();
        f4320a = hashMap;
        hashMap.put(IllegalArgumentException.class, -1);
        f4320a.put(ResourceExhaustedException.class, -11);
        f4320a.put(UnavailableArcoreNotInstalledException.class, -100);
        f4320a.put(UnavailableDeviceNotCompatibleException.class, -101);
        f4320a.put(UnavailableApkTooOldException.class, Integer.valueOf(AppLovinErrorCodes.NO_NETWORK));
        f4320a.put(UnavailableSdkTooOldException.class, -104);
        f4320a.put(UnavailableUserDeclinedInstallationException.class, -105);
    }

    ArCoreApkJniAdapter() {
    }

    private static int a(Throwable th) {
        Log.e("ARCore-ArCoreApkJniAdapter", "Exception details:", th);
        Class<?> cls = th.getClass();
        if (f4320a.containsKey(cls)) {
            return f4320a.get(cls).intValue();
        }
        return -2;
    }

    @UsedByNative("arcoreapk.cc")
    static int checkAvailability(Context context) {
        try {
            return ArCoreApk.getInstance().checkAvailability(context).nativeCode;
        } catch (Throwable th) {
            a(th);
            return ArCoreApk.Availability.UNKNOWN_ERROR.nativeCode;
        }
    }

    @UsedByNative("arcoreapk.cc")
    static int requestInstall(Activity activity, boolean z, int[] iArr) throws UnavailableDeviceNotCompatibleException, UnavailableUserDeclinedInstallationException {
        try {
            iArr[0] = ArCoreApk.getInstance().requestInstall(activity, z).nativeCode;
            return 0;
        } catch (Throwable th) {
            return a(th);
        }
    }

    @UsedByNative("arcoreapk.cc")
    static int requestInstallCustom(Activity activity, boolean z, int i, int i2, int[] iArr) throws UnavailableDeviceNotCompatibleException, UnavailableUserDeclinedInstallationException {
        try {
            iArr[0] = ArCoreApk.getInstance().requestInstall(activity, z, ArCoreApk.InstallBehavior.forNumber(i), ArCoreApk.UserMessageType.forNumber(i2)).nativeCode;
            return 0;
        } catch (Throwable th) {
            return a(th);
        }
    }
}
