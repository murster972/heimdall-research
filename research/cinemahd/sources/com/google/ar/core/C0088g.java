package com.google.ar.core;

import com.google.ar.core.ArCoreApk;

/* 'enum' modifier removed */
/* renamed from: com.google.ar.core.g  reason: case insensitive filesystem */
final class C0088g extends ArCoreApk.Availability {
    C0088g(String str, int i, int i2) {
        super(str, 6, 203, (ae) null);
    }

    public final boolean isSupported() {
        return true;
    }
}
