package com.google.ar.core;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.google.ar.core.t  reason: case insensitive filesystem */
final class C0101t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f4339a;
    final /* synthetic */ C0096o b;
    final /* synthetic */ C0094m c;

    C0101t(C0094m mVar, Activity activity, C0096o oVar) {
        this.c = mVar;
        this.f4339a = activity;
        this.b = oVar;
    }

    public final void run() {
        try {
            AtomicBoolean atomicBoolean = new AtomicBoolean(false);
            this.c.d.a(this.f4339a.getApplicationInfo().packageName, Collections.singletonList(C0094m.b()), new Bundle(), new C0102u(this, atomicBoolean));
            new Handler().postDelayed(new C0103v(this, atomicBoolean), 3000);
        } catch (RemoteException e) {
            Log.w("ARCore-InstallService", "requestInstall threw, launching fullscreen.", e);
            C0094m.b(this.f4339a, this.b);
        }
    }
}
