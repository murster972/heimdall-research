package com.google.ar.core;

public final class R {

    public static final class id {
        public static final int __arcore_cancelButton = 2131296270;
        public static final int __arcore_continueButton = 2131296271;
        public static final int __arcore_messageText = 2131296272;

        private id() {
        }
    }

    public static final class layout {
        public static final int __arcore_education = 2131492864;

        private layout() {
        }
    }

    public static final class string {
        public static final int __arcore_cancel = 2131755008;
        public static final int __arcore_continue = 2131755009;
        public static final int __arcore_install_app = 2131755010;
        public static final int __arcore_install_feature = 2131755011;
        public static final int __arcore_installing = 2131755012;

        private string() {
        }
    }

    private R() {
    }
}
