package com.google.ar.core;

import android.content.Context;

/* renamed from: com.google.ar.core.x  reason: case insensitive filesystem */
final class C0105x extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final Context f4343a;
    private final C0096o b;
    private volatile boolean c;

    C0105x(Context context, C0096o oVar) {
        this.f4343a = context;
        this.b = oVar;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = true;
    }

    public final void run() {
        while (!this.c) {
            if (C0089h.b().b(this.f4343a)) {
                this.b.a(C0095n.COMPLETED);
                return;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException unused) {
            }
        }
    }
}
