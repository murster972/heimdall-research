package com.google.ar.core;

import android.view.View;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

/* renamed from: com.google.ar.core.i  reason: case insensitive filesystem */
final class C0090i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ InstallActivity f4328a;

    C0090i(InstallActivity installActivity) {
        this.f4328a = installActivity;
    }

    public final void onClick(View view) {
        this.f4328a.finishWithFailure(new UnavailableUserDeclinedInstallationException());
    }
}
