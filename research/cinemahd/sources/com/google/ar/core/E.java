package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.NotYetAvailableException;

/* 'enum' modifier removed */
final class E extends Session.a {
    E(String str, int i, int i2) {
        super(str, 12, -12, (byte) 0);
    }

    public final void a() throws NotYetAvailableException {
        throw new NotYetAvailableException();
    }
}
