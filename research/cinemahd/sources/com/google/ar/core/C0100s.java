package com.google.ar.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.ar.core.exceptions.FatalException;

/* renamed from: com.google.ar.core.s  reason: case insensitive filesystem */
final class C0100s extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ C0096o f4338a;
    private final /* synthetic */ C0094m b;

    C0100s(C0094m mVar, C0096o oVar) {
        this.b = mVar;
        this.f4338a = oVar;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Bundle extras = intent.getExtras();
        if (!"com.google.android.play.core.install.ACTION_INSTALL_STATUS".equals(action) || extras == null || !extras.containsKey("install.status")) {
            this.f4338a.a((Exception) new FatalException("Unknown error from install service."));
            return;
        }
        this.b.c();
        int i = extras.getInt("install.status");
        if (i == 1 || i == 2 || i == 3) {
            this.f4338a.a(C0095n.ACCEPTED);
        } else if (i == 4) {
            this.f4338a.a(C0095n.COMPLETED);
        } else if (i != 6) {
            this.f4338a.a((Exception) new FatalException("Unknown error from install service."));
        } else {
            this.f4338a.a(C0095n.CANCELLED);
        }
    }
}
