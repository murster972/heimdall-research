package com.google.ar.core;

import com.google.ar.core.ArCoreApk;

/* 'enum' modifier removed */
/* renamed from: com.google.ar.core.e  reason: case insensitive filesystem */
final class C0086e extends ArCoreApk.Availability {
    C0086e(String str, int i, int i2) {
        super(str, 4, 201, (ae) null);
    }

    public final boolean isSupported() {
        return true;
    }
}
