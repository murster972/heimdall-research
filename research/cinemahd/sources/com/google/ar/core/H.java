package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.ImageInsufficientQualityException;

/* 'enum' modifier removed */
final class H extends Session.a {
    H(String str, int i, int i2) {
        super(str, 15, -17, (byte) 0);
    }

    public final void a() {
        throw new ImageInsufficientQualityException();
    }
}
