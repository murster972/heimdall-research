package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.FatalException;

/* 'enum' modifier removed */
final class U extends Session.a {
    U(String str, int i, int i2) {
        super(str, 2, -2, (byte) 0);
    }

    public final void a() {
        throw new FatalException();
    }
}
