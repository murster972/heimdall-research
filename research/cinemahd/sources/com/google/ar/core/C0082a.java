package com.google.ar.core;

import com.google.ar.core.ArCoreApk;

/* 'enum' modifier removed */
/* renamed from: com.google.ar.core.a  reason: case insensitive filesystem */
final class C0082a extends ArCoreApk.Availability {
    C0082a(String str, int i, int i2) {
        super(str, 0, 0, (ae) null);
    }

    public final boolean isUnknown() {
        return true;
    }
}
