package com.google.ar.core;

import android.view.View;

/* renamed from: com.google.ar.core.j  reason: case insensitive filesystem */
final class C0091j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ InstallActivity f4329a;

    C0091j(InstallActivity installActivity) {
        this.f4329a = installActivity;
    }

    public final void onClick(View view) {
        this.f4329a.animateToSpinner();
        this.f4329a.startInstaller();
    }
}
