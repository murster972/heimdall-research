package com.google.ar.core;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.google.a.b.a.a.a.e;
import com.google.ar.core.exceptions.FatalException;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.google.ar.core.u  reason: case insensitive filesystem */
final class C0102u extends e {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AtomicBoolean f4340a;
    private final /* synthetic */ C0101t b;

    C0102u(C0101t tVar, AtomicBoolean atomicBoolean) {
        this.b = tVar;
        this.f4340a = atomicBoolean;
    }

    public final void a() throws RemoteException {
    }

    public final void a(Bundle bundle) throws RemoteException {
        if (!this.f4340a.getAndSet(true)) {
            int i = bundle.getInt("error.code", -100);
            int i2 = bundle.getInt("install.status", 0);
            if (i2 == 4) {
                this.b.b.a(C0095n.COMPLETED);
            } else if (i != 0) {
                StringBuilder sb = new StringBuilder(51);
                sb.append("requestInstall = ");
                sb.append(i);
                sb.append(", launching fullscreen.");
                Log.w("ARCore-InstallService", sb.toString());
                C0101t tVar = this.b;
                C0094m.b(tVar.f4339a, tVar.b);
            } else if (bundle.containsKey("resolution.intent")) {
                C0101t tVar2 = this.b;
                C0094m.a(tVar2.f4339a, bundle, tVar2.b);
            } else if (i2 != 10) {
                switch (i2) {
                    case 1:
                    case 2:
                    case 3:
                        this.b.b.a(C0095n.ACCEPTED);
                        return;
                    case 4:
                        this.b.b.a(C0095n.COMPLETED);
                        return;
                    case 5:
                        this.b.b.a((Exception) new FatalException("Unexpected FAILED install status without error."));
                        return;
                    case 6:
                        this.b.b.a(C0095n.CANCELLED);
                        return;
                    default:
                        C0096o oVar = this.b.b;
                        StringBuilder sb2 = new StringBuilder(38);
                        sb2.append("Unexpected install status: ");
                        sb2.append(i2);
                        oVar.a((Exception) new FatalException(sb2.toString()));
                        return;
                }
            } else {
                this.b.b.a((Exception) new FatalException("Unexpected REQUIRES_UI_INTENT install status without an intent."));
            }
        }
    }

    public final void d(Bundle bundle) throws RemoteException {
    }
}
