package com.google.ar.core;

import com.google.ar.core.ArCoreApk;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.ar.core.z  reason: case insensitive filesystem */
class C0107z implements ArCoreApk.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AtomicReference f4344a;

    C0107z(AtomicReference atomicReference) {
        this.f4344a = atomicReference;
    }

    public void a(ArCoreApk.Availability availability) {
        this.f4344a.set(availability);
    }
}
