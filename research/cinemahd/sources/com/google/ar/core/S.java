package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;

/* 'enum' modifier removed */
final class S extends Session.a {
    S(String str, int i, int i2) {
        super(str, 24, -104, (byte) 0);
    }

    public final void a() throws UnavailableException {
        throw new UnavailableSdkTooOldException();
    }
}
