package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.SessionPausedException;

/* 'enum' modifier removed */
final class V extends Session.a {
    V(String str, int i, int i2) {
        super(str, 3, -3, (byte) 0);
    }

    public final void a() {
        throw new SessionPausedException();
    }
}
