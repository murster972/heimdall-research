package com.google.ar.core;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.google.a.b.a.a.a.e;
import com.google.ar.core.ArCoreApk;

/* renamed from: com.google.ar.core.r  reason: case insensitive filesystem */
final class C0099r extends e {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ C0098q f4337a;

    C0099r(C0098q qVar) {
        this.f4337a = qVar;
    }

    public final void a() throws RemoteException {
    }

    public final void a(Bundle bundle) throws RemoteException {
    }

    public final void d(Bundle bundle) throws RemoteException {
        ArCoreApk.a aVar;
        ArCoreApk.Availability availability;
        String str;
        int i = bundle.getInt("error.code", -100);
        if (i != -5) {
            if (i == -3) {
                str = "The Google Play application must be updated.";
            } else if (i != 0) {
                StringBuilder sb = new StringBuilder(33);
                sb.append("requestInfo returned: ");
                sb.append(i);
                str = sb.toString();
            } else {
                aVar = this.f4337a.b;
                availability = ArCoreApk.Availability.SUPPORTED_NOT_INSTALLED;
            }
            Log.e("ARCore-InstallService", str);
            aVar = this.f4337a.b;
            availability = ArCoreApk.Availability.UNKNOWN_ERROR;
        } else {
            Log.e("ARCore-InstallService", "The device is not supported.");
            aVar = this.f4337a.b;
            availability = ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE;
        }
        aVar.a(availability);
    }
}
