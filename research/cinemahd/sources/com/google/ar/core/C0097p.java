package com.google.ar.core;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

/* renamed from: com.google.ar.core.p  reason: case insensitive filesystem */
final class C0097p implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ C0094m f4335a;

    C0097p(C0094m mVar) {
        this.f4335a = mVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f4335a.a(iBinder);
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.f4335a.d();
    }
}
