package com.google.ar.core;

import com.google.ar.core.Session;

/* 'enum' modifier removed */
final class N extends Session.a {
    N(String str, int i, int i2) {
        super(str, 20, -15, (byte) 0);
    }

    public final void a() {
        throw new SecurityException("Internet permission is not granted");
    }
}
