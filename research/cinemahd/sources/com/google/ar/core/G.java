package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.AnchorNotSupportedForHostingException;

/* 'enum' modifier removed */
final class G extends Session.a {
    G(String str, int i, int i2) {
        super(str, 14, -16, (byte) 0);
    }

    public final void a() {
        throw new AnchorNotSupportedForHostingException();
    }
}
