package com.google.ar.core;

/* renamed from: com.google.ar.core.n  reason: case insensitive filesystem */
enum C0095n {
    ACCEPTED,
    CANCELLED,
    COMPLETED;

    static {
        ACCEPTED = new C0095n("ACCEPTED", 0);
        CANCELLED = new C0095n("CANCELLED", 1);
        COMPLETED = new C0095n("COMPLETED", 2);
        C0095n[] nVarArr = {ACCEPTED, CANCELLED, COMPLETED};
    }
}
