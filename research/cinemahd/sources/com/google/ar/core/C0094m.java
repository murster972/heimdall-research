package com.google.ar.core;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.google.a.b.a.a.a.a;
import com.google.a.b.a.a.a.b;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.exceptions.FatalException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.ar.core.m  reason: case insensitive filesystem */
class C0094m {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<Runnable> f4332a;
    private Context b;
    private volatile int c;
    /* access modifiers changed from: private */
    public a d;
    private BroadcastReceiver e;
    private Context f;
    private final ServiceConnection g;
    private final AtomicReference<C0105x> h;

    C0094m() {
    }

    C0094m(byte b2) {
        this();
        this.f4332a = new ArrayDeque();
        this.c = C0104w.f4342a;
        this.g = new C0097p(this);
        this.h = new AtomicReference<>();
    }

    /* access modifiers changed from: private */
    public static void a(Activity activity, Bundle bundle, C0096o oVar) {
        PendingIntent pendingIntent = (PendingIntent) bundle.getParcelable("resolution.intent");
        if (pendingIntent != null) {
            try {
                activity.startIntentSenderForResult(pendingIntent.getIntentSender(), 1234, new Intent(activity, activity.getClass()), 0, 0, 0);
            } catch (IntentSender.SendIntentException e2) {
                oVar.a((Exception) new FatalException("Installation Intent failed", e2));
            }
        } else {
            Log.e("ARCore-InstallService", "Did not get pending intent.");
            oVar.a((Exception) new FatalException("Installation intent failed to unparcel."));
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(IBinder iBinder) {
        a a2 = b.a(iBinder);
        Log.i("ARCore-InstallService", "Install service connected");
        this.d = a2;
        this.c = C0104w.c;
        for (Runnable run : this.f4332a) {
            run.run();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(java.lang.Runnable r3) throws com.google.ar.core.C0106y {
        /*
            r2 = this;
            monitor-enter(r2)
            int r0 = r2.c     // Catch:{ all -> 0x001f }
            r1 = 1
            int r0 = r0 - r1
            if (r0 == 0) goto L_0x0019
            if (r0 == r1) goto L_0x0012
            r1 = 2
            if (r0 == r1) goto L_0x000d
            goto L_0x0010
        L_0x000d:
            r3.run()     // Catch:{ all -> 0x001f }
        L_0x0010:
            monitor-exit(r2)
            return
        L_0x0012:
            java.util.Queue<java.lang.Runnable> r0 = r2.f4332a     // Catch:{ all -> 0x001f }
            r0.offer(r3)     // Catch:{ all -> 0x001f }
            monitor-exit(r2)
            return
        L_0x0019:
            com.google.ar.core.y r3 = new com.google.ar.core.y     // Catch:{ all -> 0x001f }
            r3.<init>()     // Catch:{ all -> 0x001f }
            throw r3     // Catch:{ all -> 0x001f }
        L_0x001f:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ar.core.C0094m.a(java.lang.Runnable):void");
    }

    /* access modifiers changed from: private */
    public static Bundle b() {
        Bundle bundle = new Bundle();
        bundle.putCharSequence("package.name", "com.google.ar.core");
        return bundle;
    }

    /* access modifiers changed from: private */
    public static void b(Activity activity, C0096o oVar) {
        try {
            activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.google.ar.core")));
        } catch (ActivityNotFoundException e2) {
            oVar.a((Exception) new FatalException("Failed to launch installer.", e2));
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        C0105x andSet = this.h.getAndSet((Object) null);
        if (andSet != null) {
            andSet.a();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void d() {
        Log.i("ARCore-InstallService", "Install service disconnected");
        this.c = C0104w.f4342a;
        this.d = null;
        c();
    }

    public synchronized void a() {
        c();
        int i = this.c - 1;
        if (i != 0) {
            if (i == 1 || i == 2) {
                this.b.unbindService(this.g);
                this.b = null;
                this.c = C0104w.f4342a;
            }
        }
        if (this.e != null) {
            this.f.unregisterReceiver(this.e);
        }
    }

    public void a(Activity activity, C0096o oVar) {
        C0105x xVar = new C0105x(activity, oVar);
        C0105x andSet = this.h.getAndSet(xVar);
        if (andSet != null) {
            andSet.a();
        }
        xVar.start();
        if (this.e == null) {
            this.e = new C0100s(this, oVar);
            this.f = activity;
            this.f.registerReceiver(this.e, new IntentFilter("com.google.android.play.core.install.ACTION_INSTALL_STATUS"));
        }
        try {
            a((Runnable) new C0101t(this, activity, oVar));
        } catch (C0106y unused) {
            Log.w("ARCore-InstallService", "requestInstall bind failed, launching fullscreen.");
            b(activity, oVar);
        }
    }

    public synchronized void a(Context context) {
        this.b = context;
        if (context.bindService(new Intent("com.google.android.play.core.install.BIND_INSTALL_SERVICE").setPackage("com.android.vending"), this.g, 1)) {
            this.c = C0104w.b;
            return;
        }
        this.c = C0104w.f4342a;
        this.b = null;
        Log.w("ARCore-InstallService", "bindService returned false.");
        context.unbindService(this.g);
    }

    public synchronized void a(Context context, ArCoreApk.a aVar) {
        try {
            a((Runnable) new C0098q(this, context, aVar));
        } catch (C0106y unused) {
            Log.e("ARCore-InstallService", "Play Store install service could not be bound.");
            aVar.a(ArCoreApk.Availability.UNKNOWN_ERROR);
        }
    }
}
