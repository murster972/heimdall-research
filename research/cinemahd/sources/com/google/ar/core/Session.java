package com.google.ar.core;

import android.content.Context;
import android.os.Build;
import com.google.ar.core.annotations.UsedByNative;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.FatalException;
import com.google.ar.core.exceptions.NotYetAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Session {
    static final int AR_ERROR_ANCHOR_NOT_SUPPORTED_FOR_HOSTING = -16;
    static final int AR_ERROR_CAMERA_NOT_AVAILABLE = -13;
    static final int AR_ERROR_CAMERA_PERMISSION_NOT_GRANTED = -9;
    static final int AR_ERROR_CLOUD_ANCHORS_NOT_CONFIGURED = -14;
    static final int AR_ERROR_DATA_INVALID_FORMAT = -18;
    static final int AR_ERROR_DATA_UNSUPPORTED_VERSION = -19;
    static final int AR_ERROR_DEADLINE_EXCEEDED = -10;
    static final int AR_ERROR_FATAL = -2;
    static final int AR_ERROR_ILLEGAL_STATE = -20;
    static final int AR_ERROR_IMAGE_INSUFFICIENT_QUALITY = -17;
    static final int AR_ERROR_INTERNET_PERMISSION_NOT_GRANTED = -15;
    static final int AR_ERROR_INVALID_ARGUMENT = -1;
    static final int AR_ERROR_MISSING_GL_CONTEXT = -7;
    static final int AR_ERROR_NOT_TRACKING = -5;
    static final int AR_ERROR_NOT_YET_AVAILABLE = -12;
    static final int AR_ERROR_RESOURCE_EXHAUSTED = -11;
    static final int AR_ERROR_SESSION_NOT_PAUSED = -4;
    static final int AR_ERROR_SESSION_PAUSED = -3;
    static final int AR_ERROR_TEXTURE_NOT_SET = -6;
    static final int AR_ERROR_UNSUPPORTED_CONFIGURATION = -8;
    static final int AR_SUCCESS = 0;
    static final int AR_TRACKABLE_AUGMENTED_IMAGE = 1095893252;
    static final int AR_TRACKABLE_BASE_TRACKABLE = 1095893248;
    static final int AR_TRACKABLE_NOT_VALID = 0;
    static final int AR_TRACKABLE_PLANE = 1095893249;
    static final int AR_TRACKABLE_POINT = 1095893250;
    static final int AR_TRACKABLE_UNKNOWN_TO_JAVA = -1;
    static final int AR_TRACKING_STATE_PAUSED = 1;
    static final int AR_TRACKING_STATE_STOPPED = 2;
    static final int AR_TRACKING_STATE_TRACKING = 0;
    static final int AR_UNAVAILABLE_APK_TOO_OLD = -103;
    static final int AR_UNAVAILABLE_ARCORE_NOT_INSTALLED = -100;
    static final int AR_UNAVAILABLE_DEVICE_NOT_COMPATIBLE = -101;
    static final int AR_UNAVAILABLE_SDK_TOO_OLD = -104;
    static final int AR_UNAVAILABLE_USER_DECLINED_INSTALLATION = -105;
    private static final String TAG = "ARCore-Session";
    private Context context;
    long nativeHandle;
    private final Object syncObject;

    enum a {
        ERROR_INVALID_ARGUMENT(-1),
        ERROR_FATAL(-2),
        ERROR_SESSION_PAUSED(Session.AR_ERROR_SESSION_PAUSED),
        ERROR_SESSION_NOT_PAUSED(Session.AR_ERROR_SESSION_NOT_PAUSED),
        ERROR_NOT_TRACKING(Session.AR_ERROR_NOT_TRACKING),
        ERROR_TEXTURE_NOT_SET(-6),
        ERROR_MISSING_GL_CONTEXT(-7),
        ERROR_UNSUPPORTED_CONFIGURATION(-8),
        ERROR_CAMERA_PERMISSION_NOT_GRANTED(Session.AR_ERROR_CAMERA_PERMISSION_NOT_GRANTED),
        ERROR_DEADLINE_EXCEEDED(Session.AR_ERROR_DEADLINE_EXCEEDED),
        ERROR_RESOURCE_EXHAUSTED(Session.AR_ERROR_RESOURCE_EXHAUSTED),
        ERROR_NOT_YET_AVAILABLE(Session.AR_ERROR_NOT_YET_AVAILABLE),
        ERROR_CAMERA_NOT_AVAILABLE(Session.AR_ERROR_CAMERA_NOT_AVAILABLE),
        ERROR_ANCHOR_NOT_SUPPORTED_FOR_HOSTING(Session.AR_ERROR_ANCHOR_NOT_SUPPORTED_FOR_HOSTING),
        ERROR_IMAGE_INSUFFICIENT_QUALITY(Session.AR_ERROR_IMAGE_INSUFFICIENT_QUALITY),
        ERROR_DATA_INVALID_FORMAT(Session.AR_ERROR_DATA_INVALID_FORMAT),
        ERROR_DATA_UNSUPPORTED_VERSION(Session.AR_ERROR_DATA_UNSUPPORTED_VERSION),
        ERROR_ILLEGAL_STATE(Session.AR_ERROR_ILLEGAL_STATE),
        ERROR_CLOUD_ANCHORS_NOT_CONFIGURED(Session.AR_ERROR_CLOUD_ANCHORS_NOT_CONFIGURED),
        ERROR_INTERNET_PERMISSION_NOT_GRANTED(Session.AR_ERROR_INTERNET_PERMISSION_NOT_GRANTED),
        UNAVAILABLE_ARCORE_NOT_INSTALLED(Session.AR_UNAVAILABLE_ARCORE_NOT_INSTALLED),
        UNAVAILABLE_DEVICE_NOT_COMPATIBLE(Session.AR_UNAVAILABLE_DEVICE_NOT_COMPATIBLE),
        UNAVAILABLE_APK_TOO_OLD(-103),
        UNAVAILABLE_SDK_TOO_OLD(Session.AR_UNAVAILABLE_SDK_TOO_OLD),
        UNAVAILABLE_USER_DECLINED_INSTALLATION(Session.AR_UNAVAILABLE_USER_DECLINED_INSTALLATION);
        
        private final int A;

        private a(int i) {
            this.A = i;
        }

        static a a(int i) {
            for (a aVar : (a[]) B.clone()) {
                if (aVar.A == i) {
                    return aVar;
                }
            }
            StringBuilder sb = new StringBuilder(34);
            sb.append("Unexpected error code: ");
            sb.append(i);
            throw new FatalException(sb.toString());
        }

        public abstract void a() throws UnavailableException, NotYetAvailableException, CameraNotAvailableException;
    }

    enum b {
        PLANE(Session.AR_TRACKABLE_PLANE, Plane.class),
        POINT(Session.AR_TRACKABLE_POINT, Point.class),
        AUGMENTED_IMAGE(Session.AR_TRACKABLE_AUGMENTED_IMAGE, AugmentedImage.class);
        
        /* access modifiers changed from: private */
        public final int d;
        private final Class<?> e;

        private b(int i, Class<? extends Trackable> cls) {
            this.d = i;
            this.e = cls;
        }

        public static b a(int i) {
            for (b bVar : a()) {
                if (bVar.d == i) {
                    return bVar;
                }
            }
            return null;
        }

        public static b a(Class<? extends Trackable> cls) {
            for (b bVar : a()) {
                if (bVar.e.equals(cls)) {
                    return bVar;
                }
            }
            return null;
        }

        private static b[] a() {
            return (b[]) f.clone();
        }

        public abstract Trackable a(long j, Session session);
    }

    protected Session() {
        this.syncObject = new Object();
        this.nativeHandle = 0;
        this.context = null;
    }

    Session(long j) {
        this.syncObject = new Object();
        this.nativeHandle = j;
    }

    public Session(Context context2) throws UnavailableArcoreNotInstalledException, UnavailableApkTooOldException, UnavailableSdkTooOldException {
        this.syncObject = new Object();
        System.loadLibrary("arcore_sdk_jni");
        this.context = context2;
        this.nativeHandle = nativeCreateSession(context2.getApplicationContext());
        loadDynamicSymbolsAfterSessionCreate();
    }

    static int getNativeTrackableFilterFromClass(Class<? extends Trackable> cls) {
        if (cls == Trackable.class) {
            return AR_TRACKABLE_BASE_TRACKABLE;
        }
        b a2 = b.a(cls);
        if (a2 != null) {
            return a2.d;
        }
        return -1;
    }

    static void loadDynamicSymbolsAfterSessionCreate() {
        if (Build.VERSION.SDK_INT >= 24) {
            ArImage.nativeLoadSymbols();
            ImageMetadata.nativeLoadSymbols();
        }
    }

    private native long[] nativeAcquireAllAnchors(long j);

    private native void nativeConfigure(long j, long j2);

    private native long nativeCreateAnchor(long j, Pose pose);

    private static native long nativeCreateSession(Context context2);

    private static native void nativeDestroySession(long j);

    private native long nativeGetCameraConfig(long j);

    private native void nativeGetConfig(long j, long j2);

    private native long[] nativeGetSupportedCameraConfigs(long j);

    private native long nativeHostCloudAnchor(long j, long j2);

    private native boolean nativeIsSupported(long j, long j2);

    private native void nativePause(long j);

    private native long nativeResolveCloudAnchor(long j, String str);

    private native void nativeResume(long j);

    private native int nativeSetCameraConfig(long j, long j2);

    private native void nativeSetCameraTextureName(long j, int i);

    private native void nativeSetDisplayGeometry(long j, int i, int i2, int i3);

    private native void nativeUpdate(long j, long j2);

    @UsedByNative("session_jni.cc")
    static void throwExceptionFromArStatus(int i) throws UnavailableException, NotYetAvailableException, CameraNotAvailableException {
        a.a(i).a();
    }

    public void configure(Config config) {
        nativeConfigure(this.nativeHandle, config.nativeHandle);
    }

    /* access modifiers changed from: package-private */
    public Collection<Anchor> convertNativeAnchorsToCollection(long[] jArr) {
        ArrayList arrayList = new ArrayList(jArr.length);
        for (long anchor : jArr) {
            arrayList.add(new Anchor(anchor, this));
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* access modifiers changed from: package-private */
    public List<CameraConfig> convertNativeCameraConfigsToCollection(long[] jArr) {
        ArrayList arrayList = new ArrayList(jArr.length);
        for (long cameraConfig : jArr) {
            arrayList.add(new CameraConfig(this, cameraConfig));
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* access modifiers changed from: package-private */
    public <T extends Trackable> Collection<T> convertNativeTrackablesToCollection(Class<T> cls, long[] jArr) {
        ArrayList arrayList = new ArrayList(jArr.length);
        for (long createTrackable : jArr) {
            Trackable createTrackable2 = createTrackable(createTrackable);
            if (createTrackable2 != null) {
                arrayList.add((Trackable) cls.cast(createTrackable2));
            }
        }
        return Collections.unmodifiableList(arrayList);
    }

    public Anchor createAnchor(Pose pose) {
        return new Anchor(nativeCreateAnchor(this.nativeHandle, pose), this);
    }

    /* access modifiers changed from: package-private */
    public Trackable createTrackable(long j) {
        b a2 = b.a(TrackableBase.internalGetType(this.nativeHandle, j));
        if (a2 != null) {
            return a2.a(j, this);
        }
        TrackableBase.internalReleaseNativeHandle(j);
        return null;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        long j = this.nativeHandle;
        if (j != 0) {
            nativeDestroySession(j);
        }
        super.finalize();
    }

    public Collection<Anchor> getAllAnchors() {
        return convertNativeAnchorsToCollection(nativeAcquireAllAnchors(this.nativeHandle));
    }

    public <T extends Trackable> Collection<T> getAllTrackables(Class<T> cls) {
        int nativeTrackableFilterFromClass = getNativeTrackableFilterFromClass(cls);
        return nativeTrackableFilterFromClass == -1 ? Collections.emptyList() : convertNativeTrackablesToCollection(cls, nativeAcquireAllTrackables(this.nativeHandle, nativeTrackableFilterFromClass));
    }

    public CameraConfig getCameraConfig() {
        return new CameraConfig(this, nativeGetCameraConfig(this.nativeHandle));
    }

    public Config getConfig() {
        Config config = new Config(this);
        getConfig(config);
        return config;
    }

    public void getConfig(Config config) {
        nativeGetConfig(this.nativeHandle, config.nativeHandle);
    }

    public List<CameraConfig> getSupportedCameraConfigs() {
        return convertNativeCameraConfigsToCollection(nativeGetSupportedCameraConfigs(this.nativeHandle));
    }

    public Anchor hostCloudAnchor(Anchor anchor) {
        return new Anchor(nativeHostCloudAnchor(this.nativeHandle, anchor.nativeHandle), this);
    }

    @Deprecated
    public boolean isSupported(Config config) {
        return nativeIsSupported(this.nativeHandle, config.nativeHandle);
    }

    /* access modifiers changed from: package-private */
    public native long[] nativeAcquireAllTrackables(long j, int i);

    public void pause() {
        nativePause(this.nativeHandle);
    }

    public Anchor resolveCloudAnchor(String str) {
        return new Anchor(nativeResolveCloudAnchor(this.nativeHandle, str), this);
    }

    public void resume() throws CameraNotAvailableException {
        nativeResume(this.nativeHandle);
    }

    public void setCameraConfig(CameraConfig cameraConfig) {
        nativeSetCameraConfig(this.nativeHandle, cameraConfig.nativeHandle);
    }

    public void setCameraTextureName(int i) {
        nativeSetCameraTextureName(this.nativeHandle, i);
    }

    public void setDisplayGeometry(int i, int i2, int i3) {
        nativeSetDisplayGeometry(this.nativeHandle, i, i2, i3);
    }

    public Frame update() throws CameraNotAvailableException {
        Frame frame;
        synchronized (this.syncObject) {
            frame = new Frame(this);
            nativeUpdate(this.nativeHandle, frame.nativeHandle);
        }
        return frame;
    }
}
