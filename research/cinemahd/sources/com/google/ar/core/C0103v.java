package com.google.ar.core;

import android.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.google.ar.core.v  reason: case insensitive filesystem */
final class C0103v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AtomicBoolean f4341a;
    private final /* synthetic */ C0101t b;

    C0103v(C0101t tVar, AtomicBoolean atomicBoolean) {
        this.b = tVar;
        this.f4341a = atomicBoolean;
    }

    public final void run() {
        if (!this.f4341a.getAndSet(true)) {
            Log.w("ARCore-InstallService", "requestInstall timed out, launching fullscreen.");
            C0101t tVar = this.b;
            C0094m.b(tVar.f4339a, tVar.b);
        }
    }
}
