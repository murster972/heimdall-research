package com.google.ar.core;

import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

/* renamed from: com.google.ar.core.o  reason: case insensitive filesystem */
class C0096o {

    /* renamed from: a  reason: collision with root package name */
    boolean f4334a = false;
    final /* synthetic */ InstallActivity b;

    C0096o(InstallActivity installActivity) {
        this.b = installActivity;
    }

    public void a(C0095n nVar) {
        synchronized (this.b) {
            if (!this.f4334a) {
                C0095n unused = this.b.lastEvent = nVar;
                int ordinal = nVar.ordinal();
                if (ordinal != 0) {
                    if (ordinal == 1) {
                        this.b.finishWithFailure(new UnavailableUserDeclinedInstallationException());
                    } else if (ordinal == 2) {
                        if (!this.b.waitingForCompletion) {
                            this.b.closeInstaller();
                        }
                        this.b.finishWithFailure((Exception) null);
                    }
                    this.f4334a = true;
                }
            }
        }
    }

    public void a(Exception exc) {
        synchronized (this.b) {
            if (!this.f4334a) {
                this.f4334a = true;
                C0095n unused = this.b.lastEvent = C0095n.CANCELLED;
                this.b.finishWithFailure(exc);
            }
        }
    }
}
