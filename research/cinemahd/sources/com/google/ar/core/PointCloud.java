package com.google.ar.core;

import com.google.ar.core.exceptions.DeadlineExceededException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class PointCloud {
    private long nativeHandle;
    private final Session session;

    protected PointCloud() {
        this.nativeHandle = 0;
        this.session = null;
        this.nativeHandle = 0;
    }

    PointCloud(Session session2, long j) {
        this.nativeHandle = 0;
        this.session = session2;
        this.nativeHandle = j;
    }

    private native ByteBuffer nativeGetData(long j, long j2);

    private native long nativeGetTimestamp(long j, long j2);

    private native void nativeReleasePointCloud(long j);

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        long j = this.nativeHandle;
        if (j != 0) {
            nativeReleasePointCloud(j);
        }
        super.finalize();
    }

    public FloatBuffer getPoints() {
        long j = this.nativeHandle;
        if (j != 0) {
            ByteBuffer nativeGetData = nativeGetData(this.session.nativeHandle, j);
            if (nativeGetData == null) {
                nativeGetData = ByteBuffer.allocateDirect(0);
            }
            return nativeGetData.order(ByteOrder.nativeOrder()).asFloatBuffer();
        }
        throw new DeadlineExceededException();
    }

    public long getTimestamp() {
        long j = this.nativeHandle;
        if (j != 0) {
            return nativeGetTimestamp(this.session.nativeHandle, j);
        }
        throw new DeadlineExceededException();
    }

    public void release() {
        nativeReleasePointCloud(this.nativeHandle);
        this.nativeHandle = 0;
    }
}
