package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.DataUnsupportedVersionException;

/* 'enum' modifier removed */
final class J extends Session.a {
    J(String str, int i, int i2) {
        super(str, 17, -19, (byte) 0);
    }

    public final void a() {
        throw new DataUnsupportedVersionException();
    }
}
