package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.TextureNotSetException;

/* 'enum' modifier removed */
final class Y extends Session.a {
    Y(String str, int i, int i2) {
        super(str, 6, -6, (byte) 0);
    }

    public final void a() {
        throw new TextureNotSetException();
    }
}
