package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.CloudAnchorsNotConfiguredException;

/* 'enum' modifier removed */
final class M extends Session.a {
    M(String str, int i, int i2) {
        super(str, 19, -14, (byte) 0);
    }

    public final void a() {
        throw new CloudAnchorsNotConfiguredException();
    }
}
