package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.MissingGlContextException;

/* 'enum' modifier removed */
final class Z extends Session.a {
    Z(String str, int i, int i2) {
        super(str, 7, -7, (byte) 0);
    }

    public final void a() {
        throw new MissingGlContextException();
    }
}
