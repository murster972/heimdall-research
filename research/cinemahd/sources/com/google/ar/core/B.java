package com.google.ar.core;

import com.google.ar.core.Session;

/* 'enum' modifier removed */
final class B extends Session.a {
    B(String str, int i, int i2) {
        super(str, 9, -9, (byte) 0);
    }

    public final void a() {
        throw new SecurityException("Camera permission is not granted");
    }
}
