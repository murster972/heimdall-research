package com.google.ar.core;

import com.google.ar.core.ArCoreApk;

/* 'enum' modifier removed */
/* renamed from: com.google.ar.core.f  reason: case insensitive filesystem */
final class C0087f extends ArCoreApk.Availability {
    C0087f(String str, int i, int i2) {
        super(str, 5, 202, (ae) null);
    }

    public final boolean isSupported() {
        return true;
    }
}
