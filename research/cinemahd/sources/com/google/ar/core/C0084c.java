package com.google.ar.core;

import com.google.ar.core.ArCoreApk;

/* 'enum' modifier removed */
/* renamed from: com.google.ar.core.c  reason: case insensitive filesystem */
final class C0084c extends ArCoreApk.Availability {
    C0084c(String str, int i, int i2) {
        super(str, 2, 2, (ae) null);
    }

    public final boolean isUnknown() {
        return true;
    }
}
