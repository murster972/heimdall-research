package com.google.ar.core;

import com.google.ar.core.Session;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableException;

/* 'enum' modifier removed */
final class P extends Session.a {
    P(String str, int i, int i2) {
        super(str, 22, -101, (byte) 0);
    }

    public final void a() throws UnavailableException {
        throw new UnavailableDeviceNotCompatibleException();
    }
}
