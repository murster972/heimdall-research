package butterknife;

public interface Unbinder {
    public static final Unbinder EMPTY = a.f1147a;

    void unbind();
}
