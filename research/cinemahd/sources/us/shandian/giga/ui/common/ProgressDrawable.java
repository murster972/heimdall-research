package us.shandian.giga.ui.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;

public class ProgressDrawable extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private float f7098a;
    private int b;
    private int c;

    public ProgressDrawable(Context context, int i, int i2) {
        this(ContextCompat.a(context, i), ContextCompat.a(context, i2));
    }

    public void a(float f) {
        this.f7098a = f;
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        Paint paint = new Paint();
        paint.setColor(this.b);
        float f = (float) width;
        float f2 = (float) height;
        Paint paint2 = paint;
        canvas.drawRect(0.0f, 0.0f, f, f2, paint2);
        paint.setColor(this.c);
        canvas.drawRect(0.0f, 0.0f, (float) ((int) (this.f7098a * f)), f2, paint2);
    }

    public int getOpacity() {
        return -1;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public ProgressDrawable(int i, int i2) {
        this.b = i;
        this.c = i2;
    }
}
