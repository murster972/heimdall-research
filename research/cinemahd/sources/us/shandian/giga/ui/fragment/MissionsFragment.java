package us.shandian.giga.ui.fragment;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.movie.AppComponent;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.helper.MoviesHelper;
import com.yoku.marumovie.R;
import javax.inject.Inject;
import us.shandian.giga.get.DownloadManager;
import us.shandian.giga.service.DownloadManagerService;
import us.shandian.giga.ui.adapter.MissionAdapter;

public abstract class MissionsFragment extends BaseFragment {
    /* access modifiers changed from: private */
    public DownloadManager c;
    /* access modifiers changed from: private */
    public DownloadManagerService.DMBinder d;
    private SharedPreferences e;
    private boolean f;
    private RecyclerView g;
    private MissionAdapter h;
    private GridLayoutManager i;
    private LinearLayoutManager j;
    private Activity k;
    private ServiceConnection l = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            DownloadManagerService.DMBinder unused = MissionsFragment.this.d = (DownloadManagerService.DMBinder) iBinder;
            MissionsFragment missionsFragment = MissionsFragment.this;
            DownloadManager unused2 = missionsFragment.c = missionsFragment.a(missionsFragment.d);
            MissionsFragment.this.e();
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }
    };
    @Inject
    MoviesHelper m;
    @Inject
    MoviesRepository n;

    /* access modifiers changed from: private */
    public void e() {
        this.h = new MissionAdapter(this.k, this.d, this.c, this.f, this.m);
        if (this.f) {
            this.g.setLayoutManager(this.j);
        } else {
            this.g.setLayoutManager(this.i);
        }
        this.g.setAdapter(this.h);
        this.e.edit().putBoolean("linear", this.f).commit();
    }

    /* access modifiers changed from: protected */
    public abstract DownloadManager a(DownloadManagerService.DMBinder dMBinder);

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.k = activity;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.missions, viewGroup, false);
        this.e = PreferenceManager.getDefaultSharedPreferences(getActivity());
        this.f = true;
        Intent intent = new Intent();
        intent.setClass(getActivity(), DownloadManagerService.class);
        getActivity().bindService(intent, this.l, 1);
        this.g = (RecyclerView) inflate.findViewById(R.id.mission_recycler);
        this.i = new GridLayoutManager(getActivity(), 2);
        this.j = new LinearLayoutManager(getActivity());
        this.g.setLayoutManager(this.i);
        setHasOptionsMenu(true);
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unbindService(this.l);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }
}
