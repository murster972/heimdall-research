package us.shandian.giga.ui.fragment;

import com.movie.data.repository.MoviesRepository;
import com.movie.ui.helper.MoviesHelper;
import dagger.MembersInjector;

public final class MissionsFragment_MembersInjector implements MembersInjector<MissionsFragment> {
    public static void a(MissionsFragment missionsFragment, MoviesHelper moviesHelper) {
        missionsFragment.m = moviesHelper;
    }

    public static void a(MissionsFragment missionsFragment, MoviesRepository moviesRepository) {
        missionsFragment.n = moviesRepository;
    }
}
